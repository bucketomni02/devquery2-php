<?php
include_once("common/inc/config.php");


?>
<!DOCTYPE HTML>
<html manifest="">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <title>sampleLogin</title>
    <script type="text/javascript">
        function show_grid(){
        if(document.getElementById('phpquery_user').value !=='' && document.getElementById('phpquery_pass').value !== '' && document.getElementById('phpquery_query').value !==''){
            var url_cs = '';
            var url_ = '';
           var phpquery_key = document.getElementById('phpquery_key').value.trim();
            var exe = true;
             if (document.getElementById('url_c').checked) {
                url_ = document.getElementById('url_c').value;
            }else if (document.getElementById('url_ch').checked) {
                url_ = document.getElementById('url_ch').value;
            }else if (document.getElementById('url_s').checked) {
                url_ = document.getElementById('url_s').value;
            }else if (document.getElementById('url_cg').checked) {
               if(document.getElementById( 'phpquery_key' ).value === ''){
                    exe = false;
                }
                url_ = document.getElementById('url_cg').value;
            }else if (document.getElementById('url_sg').checked) {
               if(document.getElementById( 'phpquery_key' ).value === ''){
                    exe = false;
                }
                url_ = document.getElementById('url_sg').value;
            }
            if(exe === true){
                if (document.getElementById('url_chk_h').checked) {
                    url_cs += "/"+document.getElementById('phpquery_user').value+"/"+document.getElementById('phpquery_pass').value;
                }else{
                    url_cs += "";
                }
                var query = "/"+document.getElementById('phpquery_query').value;
                if(phpquery_key !== ''){
                   query += "/"+phpquery_key;
                }
                var paramvalue = document.getElementById( 'phpquery_param' ).value.trim();
                if(paramvalue !== ''){
                    if(document.getElementById('param_chk').checked){
                        var param = paramvalue;
                        query += "/params/"+param;
                    }
                }
                    var IP = document.getElementById('IP').value;
                    var SISTEM = document.getElementById('SISTEM').value;
                    var url_w = "http://"+IP+"/"+SISTEM+"/#"+url_+url_cs+query;
                 	window.open(url_w, "phpQueryWindow");
                	document.axessample.action = url_w;
                    document.axessample.target = 'phpQueryWindow';
                    document.axessample.submit();
            }else{
                alert("グラフキーを入力してください。");
            }

        }else{
            alert("ユーザー名、パスワード、またはクエリー名を入力してください。");
        }
        }
        function displayParam()
        {
          if (document.getElementById('param_chk').checked) 
          {
             document.getElementById( 'phpquery_param' ).style.display = 'block';
          } else {
             document.getElementById( 'phpquery_param' ).style.display = 'none';
             document.getElementById( 'phpquery_param' ).value = '';
          }
        }
        function changeKey(key){
            if(key === 0){
                 document.getElementById( 'phpquery_key' ).placeholder = "ピボットキー";
            }else{
                 document.getElementById( 'phpquery_key' ).placeholder = "グラフキー";
            }
        }
    </script>
    <style type="text/css">
        .formSty{
            border-style: double;
            width: 500px;
            margin-left: auto;
            margin-right: auto;
            padding : 10px;
            background-color: aliceblue;
        }
        body {
            padding : 50px;
        }
        #btngp{
                text-align: left;
                padding-top: 10px;
                padding-bottom: 10px;
                padding-left: 90px;
        }
        .contxt{
            padding-left : 100px;
            padding-right : 50px;
         }
        .txt {
           height: 25px;
           width: 300px;
        }
     </style>
</head>
<body>
        <form name="axessample" id="axessample" class="formSty" target="phpQueryWindow" method="POST">
             <h3 style="color : #013760 ; text-align : center">SAMPLE AXES LOGIN</h3>
              <div class="contxt">
                <input type="hidden" class ="txt"  name="IP" id="IP" value = '<?=IP?>' />
                <input type="hidden" class ="txt"  name="SISTEM" id="SISTEM" value = '<?=SISTEM?>' />
                <input type="text" class ="txt" placeholder = "ユーザー" name="phpquery_user" id="phpquery_user" value = '' autofocus/>
                <br/><br/><input type="text" class ="txt" placeholder="パスワード" name="phpquery_pass" id="phpquery_pass" value = ''/>
                <br/><br/><input type="text" class ="txt" placeholder="クエリー名" name="phpquery_query" id="phpquery_query" value = ''/>
                <br/><br/><input type="text" class ="txt" placeholder="ピボットキー" name="phpquery_key" id="phpquery_key" value = ''/> 
                <br/><br/><input type="text" class ="txt" style="display : none" placeholder="パラメーター" name="phpquery_param" id="phpquery_param" value = ''/>
            </div>
            <div id="btngp">
            <input type="checkbox" id="param_chk" onchange = "displayParam()"/>パラメータ
              <br/><br/>
<!--            <input type="radio" name="url_key" id="url_key_d" value="data" onchange = "changeKey(0)" checked> データ
            <input type="radio" name="url_key" id="url_key_g" value="graph" onchange = "changeKey(1)"> グラフ
            <br/><br/>-->
            <input type="radio" name="url_chk" id="url_chk_h" value="HASH" checked> HASH
            <input type="radio" name="url_chk" id="url_chk_p" value="POST"> POST
            <br/><br/>
            <input type="radio" name="url_" id="url_c" value="call" checked onchange = "changeKey(0)"> CALL
            <input type="radio" name="url_" id="url_ch" value="callhtml" onchange = "changeKey(0)"> CALL HTML
            <input type="radio" name="url_" id="url_s" value="select" onchange = "changeKey(0)"> SELECT
            <br/><br/>
            <input type="radio" name="url_" id="url_cg" value="callgraph" onchange = "changeKey(1)"> CALL GRAPH
            <input type="radio" name="url_" id="url_sg" value="selectgraph" onchange = "changeKey(1)"> SELECT GRAPH
            </div>
            <div style=" padding-top : 10px;text-align : center;">
                <button type="button" name="shw_btn" onclick = "show_grid()" id = "shw_btn" style="background-color: #013760;border: none;color: white;padding: 4px 18px;font-size: 16px;cursor: pointer;border-radius: 10px;">表示</button>
            </div>
         </form>
</body>
</html>