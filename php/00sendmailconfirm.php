<?php

/***
インストールサーバーから単純メールが送信できるかのテストプログラム
**/

$secure = 'ssl';
$host = 'smtp.gmail.com';
$port = 465;
$gmailAccount = 'phpquery-info@omni-s.co.jp';
$gmailAccountPassword = 'infophpquery00';
$from = 'phpquery-info@omni-s.co.jp';
$to = 's-kabe@omni-s.co.jp';

require("common/lib/PHPMailer/PHPMailerAutoload.php");

$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = $secure; // secure transfer enabled REQUIRED for GMail
$mail->Host = $host;
$mail->Port = $port; // or 587
$mail->IsHTML(true);
$mail->Username = $gmailAccount;
$mail->Password = $gmailAccountPassword;
$mail->SetFrom($from);
$mail->Subject = "MailTest";
$mail->Body = "Mail Test Success!";
$mail->AddAddress($to);
 if(!$mail->Send())
{
echo "Mailer Error: " . $mail->ErrorInfo;
}
else
{
echo "Message has been sent";
}