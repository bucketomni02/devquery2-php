<?php

/***
インストールサーバーから単純メールが送信できるかのテストプログラム
**/
$secure = 'ssl';
$host= '74.125.203.108';
$port = 465;
$gmailAccount = 'mailerphpquery@gmail.com';
$gmailAccountPassword = 'mailerphpquery00';
$from = 'mailerphpquery@gmail.com';
$to1 = 'ayekyawzin@gmail.com';
$to2 = 'aye_kyaw_zin@omni-s.co.jp';

require("common/lib/PHPMailer/PHPMailerAutoload.php");

$mail = new PHPMailer(); // create a new object
$mail->CharSet  = 'iso-2022-jp';
$mail->Encoding = '7bit';
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = $secure; // secure transfer enabled REQUIRED for GMail
$mail->Host = $host;
$mail->Port = $port; // or 587
$mail->IsHTML(true);
$mail->Username = $gmailAccount;
$mail->Password = $gmailAccountPassword;
$mail->SetFrom($from);
$mail->Subject = "MailTest";
$mail->Body = "Mail Test Success!";
$mail->AddAddress($to1);
$mail->AddAddress($to2);
 if(!$mail->Send())
{
echo "Mailer Error: " . $mail->ErrorInfo;
}
else
{
echo "Message has been sent";
}