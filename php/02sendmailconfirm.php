<?php

/***
インストールサーバーから単純メールが送信できるかのテストプログラム
**/

$secure = 'ssl';
$host = '74.125.203.108';
$port = 465;
$gmailAccount = 'mailerphpquery@gmail.com';
$gmailAccountPassword = 'mailerphpquery00';
$from = 'mailerphpquery@gmail.com';
//$to = 's-kabe@omni-s.co.jp';
$to = 'aye_kyaw_zin@omni-s.co.jp';
//$to = 'shwe_yi_pyae_soan@omni-s.co.jp';

require("common/lib/PHPMailer/class.phpmailer.php");

mb_language('japanese');
mb_internal_encoding('UTF-8');

$mail = new PHPMailer(); // create a new object
$mail->CharSet  = 'iso-2022-jp';
$mail->Encoding = '7bit';
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = $secure; // secure transfer enabled REQUIRED for GMail
$mail->Host = $host;
$mail->Port = $port; // or 587
$mail->IsHTML(true);
$mail->Username = $gmailAccount;
$mail->Password = $gmailAccountPassword;
$mail->SetFrom($from);
$mail->Subject = mb_encode_mimeheader("おおおおおおおおおおおおおおおおおおおお", 'JIS');
$mail->Body = "いいいいいいいいいい!";
$mail->AddAddress($to);
//$mail->AddAttachment('tmp_passzip/sytest.php',mb_convert_encoding('aaaaaaa aaa------',"ISO-2022-JP-MS"));
//$mail->AddAttachment(mb_convert_encoding('あいうえお2.php',"ISO-2022-JP-MS"));
 if(!$mail->Send())
{
echo "Mailer Error: " . $mail->ErrorInfo;
}
else
{
echo "Message has been sent";
}