<?php

/***
インストールサーバーから単純メールが送信できるかのテストプログラム
**/

$host= '192.168.1.12';
$port = 25;
$gmailAccount = '';
$gmailAccountPassword = '';
$from = 'mailerphpquery@gmail.com';
$to = 's-kabe@omni-s.co.jp';
//$to = 'ayekyawzin1993@yahoo.com';

require("common/lib/PHPMailer/PHPMailerAutoload.php");

$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
$mail->Host = $host;
$mail->Port = $port; // or 587
$mail->IsHTML(true);
$mail->Username = $gmailAccount;
$mail->Password = $gmailAccountPassword;
$mail->SetFrom($from);
$mail->Subject = "MailTest";
$mail->Body = "Mail Test Success!";
$mail->AddAddress($to);
 if(!$mail->Send())
{
echo "Mailer Error: " . $mail->ErrorInfo;
}
else
{
echo "Message has been sent";
}