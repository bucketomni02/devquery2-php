<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
if(NOTOOLKIT === 1){
    require_once('ToolkitService.php');
}

/*
*-------------------------------------------------------*
* リクエスト
*-------------------------------------------------------*
*/
$updData = (isset($_POST['updData'])) ? json_decode($_POST['updData'],true) : array();
$tblName = (isset($_POST['tblName']))?$_POST['tblName'] : '';

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtnArray = array();
$rtn = 0;
$msg = '';
$rs = '';
$url = '';


/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

$rs = array();
$DB2CSV1PM = array();

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

 $rs =  cmExe($db2con,$tblName,$updData);
if($rs['result'] !== true){
    $rtn = 1;
    $msg = showMsg($rs['result']);
}else{
    $msg = showMsg('CL実行処理が正常に完了しました。');
}

/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'tblName' => $tblName,
    'updData' =>$updData
);
echo(json_encode($rtnArray));

function cmExe($db2con,$d1name,$updData){
    $rs = array();
    if(NOTOOLKIT === 1){

        //FDB2CSV1PGの取得
        $strSQL  = ' SELECT A.DGCLIB,A.DGCPGM,B.D1CSID,B.D1LIBL ';
        $strSQL .= ' FROM FDB2CSV1PG AS A ' ;
        $strSQL .= ' JOIN FDB2CSV1 AS B ON A.DGNAME = B.D1NAME ';
        $strSQL .= ' WHERE A.DGNAME = ? ';
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false){
            $rs = array('result' => 'FAIL_SEL');
        }else{
            $params = array(
                $d1name
            );
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $fdb2csv1pg[] = $row;
                }
                $fdb2csv1pg = umEx($fdb2csv1pg);
                $rs = array('result' => true);
            }
        }

        //FDB2CSV1PMの取得
        if($rs['result'] === true){

            $strSQL  = ' SELECT * ';
            $strSQL .= ' FROM FDB2CSV1PM AS A ' ;
            $strSQL .= ' WHERE A.DMNAME = ? ';
            $strSQL .= ' AND A.DMEVENT = ? ';
            $strSQL .= ' ORDER BY DMSEQ ';
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false){
                $rs = array('result' => 'FAIL_SEL');
            }else{
                $params = array(
                    $d1name,
                    '3'
                );
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $rs = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $fdb2csv1pm[] = $row;
                    }
                    $fdb2csv1pm = umEx($fdb2csv1pm);
                    $rs = array('result' => true);
                }
            }

        }

        //データベース切断
    	db2_close($db2con);

        $fdb2csv1 = umEx(FDB2CSV1_DATA($d1name));
        if($fdb2csv1[0]['D1RDB'] === '' || $fdb2csv1[0]['D1RDB'] === RDB || $fdb2csv1[0]['D1RDB'] === RDBNAME){
            //データベース接続
            $cldbcon = db2_connect($database, $user, $password,array(
                                'i5_naming'=>DB2_I5_NAMING_ON, 
                                'i5_libl'=>$fdb2csv1pg[0]['D1LIBL']
                                ));
        }else{
            $cldbcon = cmDB2ConRDB($fdb2csv1pg[0]['D1LIBL']);
        }
        if($cldbcon === false){
            $rs = array('result' => 'FAIL_SEL');
        }else{

            //オートコミットOFF
            $comrs = db2_autocommit($cldbcon,DB2_AUTOCOMMIT_OFF);
            if($comrs === false){
                $rs = array('result' => 'FAIL_SEL');
            }else{

                //トランザクションの開始
                db2_exec($cldbcon, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');

                //IBMi接続（データベース接続と共有）
                $conn = ToolkitService::getInstance($cldbcon, DB2_I5_NAMING_ON);
                $conn->setToolkitServiceParams(array('stateless' => true));

                //CCSIDをセット
                $conn->CLCommand('CHGJOB CCSID('.$fdb2csv1pg[0]['D1CSID'].')');

                //updDataループの前にリターン配列をセット。エラーが出た場合はresultにtrue以外を入れる
                $rs = array('result' => true);

                //updDataの数分ループしてCLを実行
                foreach($updData['page'] as $uKey => $uValue){

                    //パラメータリセット
                    $parm = array();

                    //パラメータの定義（最初のみRTCD固定、後はFDB2CSV1PMの数分addParameterChar）
                    $parm[] = $conn->AddParameterChar('Both', 2, 'RTCD', 'RTCD', '');

                    foreach($fdb2csv1pm as $mKey => $mValue){
                        $type  = $mValue['DMPTYPE'];
                        $len   = $mValue['DMLEN'];
                        $scal  = $mValue['DMSCAL'];
                        $scfd  = $mValue['DMSCFD'];
                        $scid  = $mValue['DMSCID'];
                        $value = $uValue[$scfd.'_'.$scid];
                        
                        //文字列の場合はAddParameterCharでセット
                        if($type === '01'){
                            $parm[] = $conn->AddParameterChar('Both',(int)$len,'PARAM'.$mKey,'PARAM'.$mKey,$value);
                        //数値の場合はAddParameterPackDecでセット
                        }else if($type === '02'){
                            $parm[] = $conn->AddParameterPackDec('Both',(int)$len,(int)$scal,'PARAM'.$mKey,'PARAM'.$mKey,$value);
                        }

                        e_log('CL実行中パラメータ：'.print_r($parm,true));

                    }

                    //プログラム実行
                    $result = $conn->PgmCall($fdb2csv1pg[0]['DGCPGM'], $fdb2csv1pg[0]['DGCLIB'], $parm, null, null);
                    //コールに失敗した場合はbreak;

                    if($result === false){
                        db2_rollback($cldbcon);
                        $rs = array('result' => 'CLの実行に失敗しました。<br/>CL連携の設定をご確認ください。');
                        break;
                    }else{
                        //RTCD受け取り
                        $rtcd = $result['io_param']['RTCD'];
                        if($rtcd === 'NG'){

                            //NGの場合、ROLLBACK
                            db2_rollback($cldbcon);

                            //エラーメッセージ
                            $rs = array('result' => 'リターンコードにNGが返されました。処理を中止します。');
                            break;
                        }
                    }

                }

                //すべての処理が正常の場合、COMMIT
                db2_commit($cldbcon);

                //IBM i 切断
                $conn->disconnect();

            }
        }
    }else{
        $rs = array('result' => 'CLの実行に失敗しました。<br/>CL連携の設定をご確認ください。');
    }

    return $rs;
}
