<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
if(NOTOOLKIT === 1){
    require_once('ToolkitService.php');
}


/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/**
*-------------------------------------------------------* 
* CL実行のため呼び出し
* 
* RESULT
*    01：存在する場合     true
*    02：存在しない場合   false
* 
* @param Object  $cldbcon      CL実行対象DBコネクション
* @param String  $qrynm        定義名
* @param Object  $updParamArr  CL実行対象の最新パラメータ
* @param String  $clFlg        CL実行の区分
*                              BEF:実行前
*                              AFT:実行後
* 
*-------------------------------------------------------*
*/
function callClExecute($cldbcon,$qrynm,$updParamArr,$clFlg){
    if(NOTOOLKIT === 1){
        $paramData = createParamArr($updParamArr);
        $rtn = 0;
        //CL実行フラグチェック
        if($clFlg === 'BEF'){
            $flg = 1;
        }else if($clFlg === 'AFT'){
            $flg = 2;
        }
        $db2con = cmDb2Con();
        cmSetPHPQUERY($db2con);
        //CL実行プログラムデータ取得
        $pgRs = getFDB2CSV1PG($db2con,$qrynm,$flg);
        if($pgRs['result'] !== true){
            $rtn = 1;
        }else{
            $fdb2csv1pg = $pgRs['data'];
            e_log('実行プログラム：'.print_r($fdb2csv1pg,true));
        }
        if($rtn === 0){
            //CL実行パラメータデータ取得
            $pmRs = getFDB2CSV1PM($db2con,$qrynm,$flg);
            if($pmRs['result'] !== true){
                $rtn = 1;
            }else{
                $fdb2csv1pm = $pmRs['data'];
                e_log('実行パラメータ：'.print_r($fdb2csv1pm,true));
            }
        }
        if($rtn === 0){
            $d1libl = $fdb2csv1pg[0]['D1LIBL'];
            $d1RDB =  $fdb2csv1pg[0]['D1RDB'];
            $dgpgm = $fdb2csv1pg[0]['DGPGM'];
            $dglib = $fdb2csv1pg[0]['DGLIB'];
        }
        //データベース切断
        cmDb2Close($db2con);

        //CL実行のため
        //IBMi接続（データベース接続と共有）
        $conn = ToolkitService::getInstance($cldbcon, DB2_I5_NAMING_ON);
        $conn->setToolkitServiceParams(array('stateless' => true));
        //パラメータループの前にリターン配列をセット。エラーが出た場合はresultにtrue以外を入れる
        $rs = array('result' => true);
        // CLパラメータのデータ件数ループ
        foreach($fdb2csv1pm as $mKey => $mValue){
            $type  = $mValue['DMPTYPE'];
            $len   = $mValue['DMLEN'];
            $scal  = $mValue['DMSCAL'];
            $scfd  = $mValue['DMSCFD'];
            $scid  = $mValue['DMSCID'];
            //フィールドじゃない場合
            if(cmMer($scfd) === ''){
                $value = $mValue['DMIZDT'];
                //文字列の場合はAddParameterCharでセット
                if($type === '01'){
                    $parm[] = $conn->AddParameterChar('Both',(int)$len,'PARAM'.$mKey,'PARAM'.$mKey,$value);
                //数値の場合はAddParameterPackDecでセット
                }else if($type === '02'){
                    $parm[] = $conn->AddParameterPackDec('Both',(int)$len,(int)$scal,'PARAM'.$mKey,'PARAM'.$mKey,$value);
                }
            }else{
                $idx = 0;
                if($mValue['DMFRTO'] == 1){
                    $idx = 0;
                }else if($mValue['DMFRTO'] == 2){
                    $idx = 1;
                }
                $value = $paramData[$scfd.'_'.$scid][$idx];
                //文字列の場合はAddParameterCharでセット
                if($type === '01'){
                    $parm[] = $conn->AddParameterChar('Both',(int)$len,'PARAM'.$mKey,'PARAM'.$mKey,$value);
                //数値の場合はAddParameterPackDecでセット
                }else if($type === '02'){
                    $parm[] = $conn->AddParameterPackDec('Both',(int)$len,(int)$scal,'PARAM'.$mKey,'PARAM'.$mKey,$value);
                }
            }
        }
        //プログラム実行
        $result = $conn->PgmCall($dgpgm, $dglib, $parm, null, null);
        //コールに失敗した場合はbreak;
        if($result){
            $rs = array('result' => 'CLの実行に失敗しました。<br/>CL連携の設定をご確認ください。');
            $rtn = 1;
        }else{
            e_log('実行結果'.print_r($result,true));


            /*$sql = " select * from ZAIWRK ";

            $stmt = db2_prepare($cldbcon, $sql);
            if ($stmt === false) {
                e_log("prepare fail " . db2_stmt_errormsg());
                e_log($strSQL);
                $data = false;
            } else {
                e_log('prepareに成功しました。');

                $params = array();
                $r = db2_execute($stmt, $params);

                if($r === false){
                    e_log('executeに失敗しました');
                    e_log(db2_stmt_errormsg());
                }else{
                    e_log('executeに成功しました');
                }

                if ($r === false) {
                    e_log("execute fail " . db2_stmt_errormsg());
                    $data = false;
                } else {
                    e_log('executeに成功しました。');

                    $data = array();
                    while ($row = db2_fetch_array($stmt)) {
                        $data[] = $row;
                    }

                    e_log('CLの結果テーブル:'.print_r($data,true));
                   // var_dump('CLの結果テーブル:'.print_r($data,true));
                }
            }*/
        }
    }else{
        $rtn = 0;
    }
    //IBM i 切断
    //$conn->disconnect();
    return $rtn;
}
/*$_SESSION['PHPQUERY']['RDBNM'] = 'S06B511R';
$db2LibCon = cmDB2ConRDB('QGPL QTEMP DEMO');
callClExecute($db2LibCon,'_00005',array(),'BEF');*/



function cmDb2ConLib2()
{
    
    $user     = 'QSECOFR';
    $password = 'PASSW0RD';
    $database = 'S06B511R';
    
    $option = array(
        'i5_naming' => DB2_I5_NAMING_ON,
        'i5_libl' => 'QTEMP QGPL DEMO'
    );
    
    $con = db2_connect($database, $user, $password, $option);
    
    return $con;
}
function getFDB2CSV1PG($db2con,$d1name,$flg){
    $data = array();
    $param = array();

    $strSQL  = ' SELECT  ';
    if($flg === 1){
        $strSQL .= ' A.DGBLIB AS DGLIB, ';
        $strSQL .= ' A.DGBPGM AS DGPGM, ';
    }else if($flg === 2){
        $strSQL .= ' A.DGALIB AS DGLIB, ';
        $strSQL .= ' A.DGAPGM AS DGPGM, ';
    }
    $strSQL .= ' B.D1CSID, ';
    $strSQL .= ' B.D1LIBL,';
    $strSQL .= ' B.D1RDB ';
    $strSQL .= ' FROM FDB2CSV1PG AS A ' ;
    $strSQL .= ' JOIN FDB2CSV1 AS B ON A.DGNAME = B.D1NAME ';
    $strSQL .= ' WHERE A.DGNAME = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false){
        $rs = array('result' => 'FAIL_SEL');
    }else{
        $params = array(
            $d1name
        );
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data'=> umEx($data));
        }
    }
    return $data;
}
function getFDB2CSV1PM($db2con,$d1name,$flg){
    $data = array();
    $param = array();

    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM FDB2CSV1PM AS A ' ;
    $strSQL .= ' WHERE A.DMNAME = ? ';
    if($flg === 1){
        $strSQL .= ' AND A.DMEVENT = 1  ';
    }else if($flg === 2){
        $strSQL .= ' AND A.DMEVENT = 2  ';
    }
    $strSQL .= ' ORDER BY DMSEQ  ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false){
        $rs = array('result' => 'FAIL_SEL');
    }else{
        $params = array(
            $d1name
        );
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data'=> umEx($data));
        }
    }
    return $data;
}

function createParamArr($paramList){
    $rtnArr = array();
    if(count($paramList)>0){
        foreach($paramList as $paramData){
            foreach($paramData[CNDSDATA] as $condData){
                if($condData['CDKBN'] === 'LIST' || $condData['CDKBN'] === 'NLIST'){
                    //処理なし
                }else {
                    $fldnmarr = explode( '.', $condData['CNDFLD']);
                    if(count($fldnmarr)>0){
                        $rtnArr[$fldnmarr[count($fldnmarr)-1].'_0']   =  $condData['CNDDATA'];
                    }else{
                        $rtnArr[$cndData['CNDFLD'].'_0']   =  $condData['CNDDATA'];;
                    }
                }
            }
        }
    }
    return $rtnArr;
}
/*

if($cldbcon === false){
    $rs = array('result' => 'FAIL_SEL');
}else{
    
    if($fdb2csv1pg[0]['D1RDB'] === ''){

    }else{

    }

    //IBMi接続（データベース接続と共有）
    $conn = ToolkitService::getInstance($cldbcon, DB2_I5_NAMING_ON);
    $conn->setToolkitServiceParams(array('stateless' => true));

    //updDataループの前にリターン配列をセット。エラーが出た場合はresultにtrue以外を入れる
    $rs = array('result' => true);

    //$parm[] = $conn->AddParameterChar('Both',2,'PARAM0','PARAM0','02');
    foreach($fdb2csv1pm as $mKey => $mValue){
        //var_dump('PMDATA'.$mKey.':'.print_r($mValue,true));
        $type  = $mValue['DMPTYPE'];
        $len   = $mValue['DMLEN'];
        $scal  = $mValue['DMSCAL'];
        $scfd  = $mValue['DMSCFD'];
        $scid  = $mValue['DMSCID'];
        //$value = $uValue[$scfd.'_'.$scid];
        if(cmMer($scfd) === ''){
            $value = $mValue['DMIZDT'];
            //文字列の場合はAddParameterCharでセット
            if($type === '01'){
                $parm[] = $conn->AddParameterChar('Both',(int)$len,'PARAM'.$mKey,'PARAM'.$mKey,$value);
            //数値の場合はAddParameterPackDecでセット
            }else if($type === '02'){
                $parm[] = $conn->AddParameterPackDec('Both',(int)$len,(int)$scal,'PARAM'.$mKey,'PARAM'.$mKey,$value);
            }
        }else{
            $value = $updData[$scfd.'_'.$scid];
            //文字列の場合はAddParameterCharでセット
            if($type === '01'){
                $parm[] = $conn->AddParameterChar('Both',(int)$len,'PARAM'.$mKey,'PARAM'.$mKey,$value);
            //数値の場合はAddParameterPackDecでセット
            }else if($type === '02'){
                $parm[] = $conn->AddParameterPackDec('Both',(int)$len,(int)$scal,'PARAM'.$mKey,'PARAM'.$mKey,$value);
            }
        }
    }
    //$parm[] = $conn->AddParameterChar('Both',2,'PARAM0','PARAM0','02');
    //プログラム実行
    $result = $conn->PgmCall($dgpgm, $dglib, $parm, null, null);

    //コールに失敗した場合はbreak;
    if($result === false){
        $rs = array('result' => 'CLの実行に失敗しました。<br/>CL連携の設定をご確認ください。');
        break;
    }else{
        var_dump($result);


        $sql = " select * from ZAIWRK ";

        $stmt = db2_prepare($cldbcon, $sql);
        if ($stmt === false) {
            echo("prepare fail " . db2_stmt_errormsg());
            echo($strSQL);
            $data = false;
        } else {
            echo('prepareに成功しました。');

            $params = array();
            $r = db2_execute($stmt, $params);

            if($r === false){
                echo('executeに失敗しました');
                echo(db2_stmt_errormsg());
            }else{
                echo('executeに成功しました');
            }

            if ($r === false) {
                echo("execute fail " . db2_stmt_errormsg());
                $data = false;
            } else {
                echo('executeに成功しました。');

                $data = array();
                while ($row = db2_fetch_array($stmt)) {
                    $data[] = $row;
                }

                var_dump($data);
            }
        }
    }

    //IBM i 切断
    $conn->disconnect();

}

var_dump($rs);
*/
