<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$success = true;
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$csvfile = (isset($_POST['csvfile']))?$_POST['csvfile']:'';
$PROC  = (isset($_POST['PROC']))?$_POST['PROC']:'';

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
          
            $rs = cmChkKenGen($db2con,'33',$userData[0]['WUSAUT']);//'33' => Import
       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('インポートの権限'));
            }
        }
    }
}

if($rtn === 0){
    if(cmMer($csvfile) === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_SET',array('インポートファイル'));
        $focus = 'csvfile';
    }
}
if($rtn === 0){
    if(strripos($csvfile, '\\') !== false) {
        $csvfile =substr($csvfile,12);
    }
    $ext = explode ('.', $csvfile);
    $ext = $ext [count ($ext) - 1];
    if($ext === 'csv'){
        $rtn = 0;
        
    }else{
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array('インポートファイル',array('ｃｓｖ','ファイル')));
        $focus = 'csvfile';
    }
  
}



/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'csvfile' => $csvfile,
    'PROC'   => $PROC,
    'FOCUS' => $focus,
    'FILE'=>$_FILES
);
//e_log('戻り値:'.print_r($rtnArray,true));


echo(json_encode($rtnArray));