<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../common/inc/common.validation.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
//FDB2CSV1PG
function chkFDB2CSV1PG($db2con, $FDB2CSV1PG) {
    $rs = true;
    $data = array();
    foreach ($FDB2CSV1PG as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['DGNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('FDB2CSV1PGのDGNAME'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DGNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PGのDGNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DGNAME'] !== '' && $res['DGNAME'] !== null) {
                $byte = checkByte($res['DGNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PGのDGNAME'));
                    break;
                }
            }
        }
        //実行前LIBチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DGBLIB'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PGのDGBLIB'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DGBLIB'] !== '' && $res['DGBLIB'] !== null) {
                $byte = checkByte($res['DGBLIB']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PGのDGBLIB'));
                    break;
                }
            }
        }
        //実行前PGMチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DGBPGM'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PGのDGBPGM'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DGBPGM'] !== '' && $res['DGBPGM'] !== null) {
                $byte = checkByte($res['DGBPGM']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PGのDGBPGM'));
                    break;
                }
            }
        }
        //実行中LIGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DGCLIB'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PGのDGCLIB'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DGCLIB'] !== '' && $res['DGCLIB'] !== null) {
                $byte = checkByte($res['DGCLIB']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PGのDGCLIB'));
                    break;
                }
            }
        }
        //実行中PGMチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DGCPGM'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PGのDGCPGM'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DGCPGM'] !== '' && $res['DGCPGM'] !== null) {
                $byte = checkByte($res['DGCPGM']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PGのDGCPGM'));
                    break;
                }
            }
        }
        //ボタン名称チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DGCBNM'], 50)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PGのDGCBNM'));
                break;
            }
        }
        //実行後LIBチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DGALIB'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PGのDGALIB'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DGALIB'] !== '' && $res['DGALIB'] !== null) {
                $byte = checkByte($res['DGALIB']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PGのDGALIB'));
                    break;
                }
            }
        }
        //実行後PGMチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DGAPGM'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PGのDGAPGM'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DGAPGM'] !== '' && $res['DGAPGM'] !== null) {
                $byte = checkByte($res['DGAPGM']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PGのDGAPGM'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//FDB2CSV1PM
function chkFDB2CSV1PM($db2con, $FDB2CSV1PM) {
    $rs = true;
    $data = array();
    foreach ($FDB2CSV1PM as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['DMNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('FDB2CSV1PMのDMNAME'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DMNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMNAME'));
                break;
            }
        }
        if ($rs === true) {
            $byte = checkByte($res['DMNAME']);
            if ($byte[1] > 0) {
                $rs = 'FAIL_BYTE';
                $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PMのDMNAME'));
                break;
            }
        }
        //実行イベントチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DMEVENT'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMEVENT'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DMEVENT'] !== '' && $res['DMEVENT'] !== null) {
                $byte = checkByte($res['DMEVENT']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PMのDMEVENT'));
                    break;
                }
            }
        }
        //順序番号チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DMSEQ'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('FDB2CSV1PMのDMSEQ', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DMSEQ'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMSEQ'));
                break;
            }
        }
        //目的チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DMUSAGE'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMUSAGE'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DMUSAGE'] !== '' && $res['DMUSAGE'] !== null) {
                $byte = checkByte($res['DMUSAGE']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PMのDMUSAGE'));
                    break;
                }
            }
        }
        //パラメータ名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DMPNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMPNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DMPNAME'] !== '' && $res['DMPNAME'] !== null) {
                $byte = checkByte($res['DMPNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PMのDMPNAME'));
                    break;
                }
            }
        }
        //型チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DMPTYPE'], 2)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMPTYPE'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DMPTYPE'] !== '' && $res['DMPTYPE'] !== null) {
                $byte = checkByte($res['DMPTYPE']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PMのDMPTYPE'));
                    break;
                }
            }
        }
        //長さチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DMLEN'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('FDB2CSV1PMのDMLEN', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DMLEN'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMLEN'));
                break;
            }
        }
        //スケールチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DMSCAL'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('FDB2CSV1PMのDMSCAL', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DMSCAL'], 2)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMSCAL'));
                break;
            }
        }
        //FROM TO FLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DMFRTO'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMFRTO'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DMFRTO'] !== '' && $res['DMFRTO'] !== null) {
                $byte = checkByte($res['DMFRTO']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PMのDMFRTO'));
                    break;
                }
            }
        }
        //初期値チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DMIZDT'], 100)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMIZDT'));
                break;
            }
        }
        //検索対象チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DMSCFG'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMSCFG'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DMSCFG'] !== '' && $res['DMSCFG'] !== null) {
                $byte = checkByte($res['DMSCFG']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PMのDMSCFG'));
                    break;
                }
            }
        }
        //フィールドIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DMSCID'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('FDB2CSV1PMのDMSCID', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DMSCID'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMSCID'));
                break;
            }
        }
        //選択条件フィールドチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DMSCFD'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMSCFD'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DMSCFD'] !== '' && $res['DMSCFD'] !== null) {
                $byte = checkByte($res['DMSCFD']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('FDB2CSV1PMのDMSCFD'));
                    break;
                }
            }
        }
        //親のIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DMMSEQ'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('FDB2CSV1PMのDMMSEQ', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DMMSEQ'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMMSEQ'));
                break;
            }
        }
        //子供のIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DMSSEQ'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('FDB2CSV1PMのDMSSEQ', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DMSSEQ'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('FDB2CSV1PMのDMSSEQ'));
                break;
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
// DB2GPK&DB2GPCチェック
function chkBSqlTypeClOpt($db2con, $FDB2CSV1PG, $FDB2CSV1PM) {
    $rs = true;
    $dataresult = array();
    //FDB2CSV1PG
    if ($rs === true) {
        $dbrs = chkFDB2CSV1PG($db2con, $FDB2CSV1PG);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //FDB2CSV1PM
    if ($rs === true) {
        $dbrs = chkFDB2CSV1PM($db2con, $FDB2CSV1PM);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array('result' => $rs, 'msg' => $msg);
    return $dataresult;
}
