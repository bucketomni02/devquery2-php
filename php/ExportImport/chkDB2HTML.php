<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../common/inc/common.validation.php");

//DB2HTMLT
function chkDB2HTMLT($db2con, $DB2HTMLT) {
    $rs = true;
    $data = array();
    foreach ($DB2HTMLT as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['HTMLTD'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2HTMLTのHTMLTD'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['HTMLTD'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2HTMLTのHTMLTD'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['HTMLTD'] !== '' && $res['HTMLTD'] !== null) {
                $byte = checkByte($res['HTMLTD']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2HTMLTのHTMLTD'));
                    break;
                }
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['HTMLTN'], 50)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2HTMLTのHTMLTN'));
                break;
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
// ｈｔｍｌチェック
function chkBSqlTypeHtmlOpt($db2con, $DB2HTMLT) {
    $rs = true;
    $dataresult = array();
    //DB2HTMLT
    if ($rs === true) {
        $dbrs = chkDB2HTMLT($db2con, $DB2HTMLT);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array('result' => $rs, 'msg' => $msg);
    return $dataresult;
}