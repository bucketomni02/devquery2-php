<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
//DB2WGDF
function chkDB2WGDF($db2con, $DB2WGDF) {
    $rs = true;
    $data = array();
    foreach ($DB2WGDF as $res) {
        /** グループ存在チェック**/
        if ($rs === true) {
            $dbRes = fnCheckDB2WQGR($db2con, $res['WGGID']);
            if ($dbRes === 'NOTEXIST_GET') {
                $rs = $dbRes;
                $msg = showMsg($dbRes, array('DB2WGDFのWGGID'));
                break;
            } else if ($dbRes !== true) {
                $rs = $dbRes;
                $msg = showMsg($dbRes, array('DB2WGDFのWGGID'));
                break;
            }
        }
        //グループチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WGGID'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WGDFのWGGID'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WGGID'] !== '' && $res['WGGID'] !== null) {
                $byte = checkByte($res['WGGID']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WGDFのWGGID'));
                    break;
                }
            }
        }
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['WGNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2WGDFのWGNAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WGNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WGDFのWGNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WGNAME'] !== '' && $res['WGNAME'] !== null) {
                $byte = checkByte($res['WGNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WGDFのWGNAME'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
// グループチェック
function chkBSqlTypeGroupOpt($db2con, $DB2WGDF) {
    $rs = true;
    $dataresult = array();
    //DB2WGDF
    if ($rs === true) {
        $dbrs = chkDB2WGDF($db2con, $DB2WGDF);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array('result' => $rs, 'msg' => $msg);
    return $dataresult;
}
/*
 *-------------------------------------------------------*
 * グループ存在チェック いたらtrue
 *-------------------------------------------------------*
*/
function fnCheckDB2WQGR($db2con, $WQGID) {
    $data = array();
    $rs = true;
    $strSQL = ' SELECT B.WQGID ';
    $strSQL.= ' FROM DB2WQGR  AS B ';
    $strSQL.= ' WHERE B.WQGID = ? ';
    $params = array($WQGID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}
