<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../common/inc/common.validation.php");
//DB2WDEF
function chkDB2WDEF($db2con, $DB2WDEF) {
    $rs = true;
    $data = array();
    foreach ($DB2WDEF as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['WDNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2WDEFのWDNAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WDNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDEFのWDNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WDNAME'] !== '' && $res['WDNAME'] !== null) {
                $byte = checkByte($res['WDNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDEFのWDNAME'));
                    break;
                }
            }
        }
        //クループID
        if ($rs === true) {
            if (!checkMaxLen($res['WDUID'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDEFのWDUID'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WDUID'] !== '' && $res['WDUID'] !== null) {
                $byte = checkByte($res['WDUID']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDEFのWDUID'));
                    break;
                }
            }
        }
        //ダウンロード可否１チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WDDWNL'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDEFのWDDWNL'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WDDWNL'] !== '' && $res['WDDWNL'] !== null) {
                $byte = checkByte($res['WDDWNL']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDEFのWDDWNL'));
                    break;
                }
            }
        }
        //ダウンロード可否２チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WDDWN2'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDEFのWDDWN2'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WDDWN2'] !== '' && $res['WDDWN2'] !== null) {
                $byte = checkByte($res['WDDWN2']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDEFのWDDWN2'));
                    break;
                }
            }
        }
        //ダウンロード先チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WDSERV'], 256)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDEFのWDSERV'));
                break;
            }
        }
        //ダウンロード時間チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WDCTFL'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDEFのWDCTFL'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WDCTFL'] !== '' && $res['WDCTFL'] !== null) {
                $byte = checkByte($res['WDCTFL']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDEFのWDCTFL'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
// ｈｔｍｌチェック
function chkBSqlTypeDwnKengenOpt($db2con, $DB2WDEF) {
    $rs = true;
    $dataresult = array();
    //DB2WDEF
    if ($rs === true) {
        $dbrs = chkDB2WDEF($db2con, $DB2WDEF);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array('result' => $rs, 'msg' => $msg);
    return $dataresult;
}
