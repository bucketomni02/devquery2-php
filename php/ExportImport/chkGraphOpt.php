<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../common/inc/common.validation.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
//DB2GPK
function chkDB2GPK($db2con, $DB2GPK) {
    $rs = true;
    $data = array();
    foreach ($DB2GPK as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['GPKQRY'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2GPKのGPKQRY'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['GPKQRY'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKQRY'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPKQRY'] !== null && $res['GPKQRY'] !== '') {
                $byte = checkByte($res['GPKQRY']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPKのGPKQRY'));
                    break;
                }
            }
        }
        //グラフキーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPKID'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKID'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPKID'] !== null && $res['GPKID'] !== '') {
                $byte = checkByte($res['GPKID']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPKのGPKID'));
                    break;
                }
            }
        }
        //グラフ名称チェック
        if ($ts === true) {
            if (!checkMaxLen($res['GPKNM'], 50)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKNM'));
                break;
            }
        }
        //形式チェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPKFM'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKFM'));
                break;
            }
        }
        //タイミングチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['GPKNUM'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2GPKのGPKNUM', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['GPKNUM'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKNUM'));
                break;
            }
        }
        //時間FORMATチェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPKDAT'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKDAT'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPKDAT'] !== null && $res['GPKDAT'] !== '') {
                $byte = checkByte($res['GPKDAT']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPKのGPKDAT'));
                    break;
                }
            }
        }
        //X軸チェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPKXCO'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKXCO'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPKXCO'] !== null && $res['GPKXCO'] !== '') {
                $byte = checkByte($res['GPKXCO']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPKのGPKXCO'));
                    break;
                }
            }
        }
        //X軸項目IDチェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPKXID'], 5)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKXID'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPKXID'] !== null && $res['GPKXID'] !== '') {
                $byte = checkByte($res['GPKXID']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPKのGPKXID'));
                    break;
                }
            }
        }
        //X軸名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPKXNM'], 50)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKXNM'));
                break;
            }
        }
        //Y軸名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPKYNM'], 50)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKYNM'));
                break;
            }
        }
        //Y軸の計算チェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPKCFG'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKCFG'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPKCFG'] !== null && $res['GPKCFG'] !== '') {
                $byte = checkByte($res['GPKCFG']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPKのGPKCFG'));
                    break;
                }
            }
        }
        //登録日付チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['GPKCUD'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2GPKのGPKCUD', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['GPKCUD'], 8)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKCUD'));
                break;
            }
        }
        //登録時間チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['GPKCUT'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2GPKのGPKCUT', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['GPKCUT'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKCUT'));
                break;
            }
        }
        //前回実行日チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['GPKBFD'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2GPKのGPKBFD', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['GPKBFD'], 8)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKBFD'));
                break;
            }
        }
        //前回時刻チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['GPKBFT'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2GPKのGPKBFT', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['GPKBFT'], 8)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKBFT'));
                break;
            }
        }
        //出力ファイルチェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPKTBL'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKTBL'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPKTBL'] !== null && $res['GPKTBL'] !== '') {
                $byte = checkByte($res['GPKTBL']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPKのGPKTBL'));
                    break;
                }
            }
        }
        //未日チェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPKEND'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKEND'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPKEND'] !== null && $res['GPKEND'] !== '') {
                $byte = checkByte($res['GPKEND']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPKのGPKEND'));
                    break;
                }
            }
        }
        //開始日チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['GPKFLG'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2GPKのGPKFLG', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['GPKFLG'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKFLG'));
                break;
            }
        }
        //RELOAD DATEチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['GPKJKD'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2GPKのGPKJKD', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['GPKJKD'], 8)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKJKD'));
                break;
            }
        }
        //RELOAD TIEM チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['GPKJKT'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2GPKのGPKJKT', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['GPKJKT'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPKのGPKJKT'));
                break;
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//DB2GPC
function chkDB2GPC($db2con, $DB2GPC) {
    $rs = true;
    $data = array();
    foreach ($DB2GPC as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['GPCQRY'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2GPCのGPCQRY'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['GPCQRY'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCQRY'));
            }
        }
        if ($rs === true) {
            $byte = checkByte($res['GPCQRY']);
            if ($byte[1] > 0) {
                $rs = 'FAIL_BYTE';
                $msg = showMsg('FAIL_BYTE', array('DB2GPCのGPCQRY'));
                break;
            }
        }
        //SEQチェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPCSEQ'], 5)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCSEQ'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPCSEQ'] !== null && $res['GPCSEQ'] !== '') {
                $byte = checkByte($res['GPCSEQ']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPCのGPCSEQ'));
                    break;
                }
            }
        }
        //グラフキーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPCID'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCID'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPCID'] !== null && $res['GPCID'] !== '') {
                $byte = checkByte($res['GPCID']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPCのGPCID'));
                    break;
                }
            }
        }
        //Y軸チェック
        if ($ts === true) {
            if (!checkMaxLen($res['GPCYCO'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCYCO'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPCYCO'] !== null && $res['GPCYCO'] !== '') {
                $byte = checkByte($res['GPCYCO']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPCのGPCYCO'));
                    break;
                }
            }
        }
        //Y軸IDチェック
        if ($ts === true) {
            if (!checkMaxLen($res['GPCYID'], 5)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCYID'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPCYID'] !== null && $res['GPCYID'] !== '') {
                $byte = checkByte($res['GPCYID']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPCのGPCYID'));
                    break;
                }
            }
        }
        //ラベルチェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPCLBL'], 20)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCLBL'));
                break;
            }
        }
        //形式チェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPCFM'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCFM'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPCFM'] !== null && $res['GPCFM'] !== '') {
                $byte = checkByte($res['GPCFM']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPCのGPCFM'));
                    break;
                }
            }
        }
        //計算チェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPCCAL'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCCAL'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPCCAL'] !== null && $res['GPCCAL'] !== '') {
                $byte = checkByte($res['GPCCAL']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPCのGPCCAL'));
                    break;
                }
            }
        }
        //グラフカラーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPCCLR'], 8)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCCLR'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPCCLR'] !== null && $res['GPCCLR'] !== '') {
                $byte = checkByte($res['GPCCLR']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPCのGPCCLR'));
                    break;
                }
            }
        }
        //SORTING SEQ チェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPCSOS'], 5)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCSOS'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPCSOS'] !== null && $res['GPCSOS'] !== '') {
                $byte = checkByte($res['GPCSOS']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPCのGPCSOS'));
                    break;
                }
            }
        }
        //順チェック
        if ($rs === true) {
            if (!checkMaxLen($res['GPCSFG'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2GPCのGPCSFG'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['GPCSFG'] !== null && $res['GPCSFG'] !== '') {
                $byte = checkByte($res['GPCSFG']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2GPCのGPCSFG'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
// DB2GPK&DB2GPCチェック
function chkBSqlTypeGphOpt($db2con, $DB2GPK, $DB2GPC) {
    $rs = true;
    $dataresult = array();
    //DB2GPK
    if ($rs === true) {
        $dbrs = chkDB2GPK($db2con, $DB2GPK);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //DB2GPC
    if ($rs === true) {
        $dbrs = chkDB2GPC($db2con, $DB2GPC);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array('result' => $rs, 'msg' => $msg);
    return $dataresult;
}

