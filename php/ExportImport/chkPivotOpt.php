<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../common/inc/common.validation.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
//DB2PMST
function chkDB2PMST($db2con, $DB2PMST) {
    $rs = true;
    $data = array();
    foreach ($DB2PMST as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['PMNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2PMSTのPMNAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['PMNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PMSTのPMNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['PMNAME'] !== '' && $res['PMNAME'] !== null) {
                $byte = checkByte($res['PMNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PMSTのPMNAME'));
                    break;
                }
            }
        }
        //ピボットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['PMPKEY'], 14)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PMSTのPMPKEY'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['PMPKEY'] !== '' && $res['PMPKEY'] !== null) {
                $byte = checkByte($res['PMPKEY']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PMSTのPMPKEY'));
                    break;
                }
            }
        }
        //記述名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['PMTEXT'], 50)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PMSTのPMTEXT'));
                break;
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//DB2PCOL
function chkDB2PCOL($db2con, $DB2PCOL) {
    $rs = true;
    $data = array();
    foreach ($DB2PCOL as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['WPNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2PCOLのWPNAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WPNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCOLのWPNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WPNAME'] !== '' && $res['WPNAME'] !== null) {
                $byte = checkByte($res['WPNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PCOLのWPNAME'));
                    break;
                }
            }
        }
        //ピボットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WPPKEY'], 14)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCOLのWPPKEY'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WPPKEY'] !== '' && $res['WPPKEY'] !== null) {
                $byte = checkByte($res['WPPKEY']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PCOLのWPPKEY'));
                    break;
                }
            }
        }
        //1:縦2:横 3:集チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WPPFLG'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCOLのWPPFLG'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WPPFLG'] !== '' && $res['WPPFLG'] !== null) {
                $byte = checkByte($res['WPPFLG']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PCOLのWPPFLG'));
                    break;
                }
            }
        }
        //SEQNチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WPSEQN'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2PCOLのWPSEQN', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WPSEQN'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCOLのWPSEQN'));
                break;
            }
        }
        //ファイルIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WPFILID'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2PCOLのWPFILID', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WPFILID'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCOLのWPFILID'));
                break;
            }
        }
        //フィールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WPFLD'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCOLのWPFLD'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WPFLD'] !== '' && $res['WPFLD'] !== null) {
                $byte = checkByte($res['WPFLD']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PCOLのWPFLD'));
                    break;
                }
            }
        }
        //非表示フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WPFHIDE'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCOLのWPFHIDE'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WPFHIDE'] !== '' && $res['WPFHIDE'] !== null) {
                $byte = checkByte($res['WPFHIDE']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PCOLのWPFHIDE'));
                    break;
                }
            }
        }
        //カラムソートチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WPSORT'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCOLのWPSORT'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WPSORT'] !== '' && $res['WPSORT'] !== null) {
                $byte = checkByte($res['WPSORT']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PCOLのWPSORT'));
                    break;
                }
            }
        }
        //カラムの合計チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WPSUMG'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCOLのWPSUMG'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WPSUMG'] !== '' && $res['WPSUMG'] !== null) {
                $byte = checkByte($res['WPSUMG']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PCOLのWPSUMG'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//DB2PCAL
function chkDB2PCAL($db2con, $DB2PCAL) {
    $rs = true;
    $data = array();
    foreach ($DB2PCAL as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['WCNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2PCALのWCNAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WCNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCALのWCNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WCNAME'] !== '' && $res['WCNAME'] !== null) {
                $byte = checkByte($res['WCNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PCALのWCNAME'));
                    break;
                }
            }
        }
        //ピボットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WCCKEY'], 14)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCALのWCCKEY'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WCCKEY'] !== '' && $res['WCCKEY'] !== null) {
                $byte = checkByte($res['WCCKEY']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2PCALのWCCKEY'));
                    break;
                }
            }
        }
        //SEQチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WCSEQN'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2PCALのWCSEQN', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WCSEQN'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCALのWCSEQN'));
                break;
            }
        }
        //計算式チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WCCALC'], 100)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2PCALのWCCALC'));
                break;
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
// Pivotチェック
function chkBSqlTypePivotOpt($db2con, $DB2PMST, $DB2PCOL, $DB2PCAL) {
    $rs = true;
    $dataresult = array();
    //DB2PMST
    if ($rs === true) {
        $dbrs = chkDB2PMST($db2con, $DB2PMST);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //DB2PCOL
    if ($rs === true) {
        $dbrs = chkDB2PCOL($db2con, $DB2PCOL);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //DB2PCAL
    if ($rs === true) {
        $dbrs = chkDB2PCAL($db2con, $DB2PCAL);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array('result' => $rs, 'msg' => $msg);
    return $dataresult;
}
