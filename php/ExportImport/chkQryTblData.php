<?php
/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../common/inc/common.validation.php");
include_once("../base/chkRDB.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */
//クエリーIDチェック
function fnChkExistD1NAME($db2con, $D1NAME)
{
    $data   = array();
    $rs     = true;
    $strSQL = ' SELECT A.D1NAME ';
    $strSQL .= ' FROM FDB2CSV1 AS A ';
    $strSQL .= ' WHERE A.D1NAME = ? ';
    $params = array(
        $D1NAME
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) > 0) {
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}
//BSQLDAT
function chkBSQLDAT($db2con, $BSQLDAT)
{
    $rs   = true;
    $data = array();
    foreach ($BSQLDAT as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['BSQLNM'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'BSQLDATのBSQLNM'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['BSQLNM'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLDATのBSQLNM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['BSQLNM'] !== '' && $res['BSQLNM'] !== null) {
                $byte = checkByte($res['BSQLNM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLDATのBSQLNM'
                    ));
                    break;
                }
            }
        }
        //SQLFLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['BSQLFLG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLDATのBSQLFLG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['BSQLFLG'] !== '' && $res['BSQLFLG'] !== null) {
                $byte = checkByte($res['BSQLFLG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLDATのBSQLFLG'
                    ));
                    break;
                }
            }
        }
        //実行SQLチェック
        if ($rs === true) {
            if (!checkMaxLen($res['BEXESQL'], 5000)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLDATのBEXESQL'
                ));
                break;
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//BSQLCND
function chkBSQLCND($db2con, $BSQLCND)
{
    $rs   = true;
    $data = array();
    foreach ($BSQLCND as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['CNQRYN'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'BSQLCNDのCNQRYN'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNQRYN'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNQRYN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNQRYN'] !== '' && $res['CNQRYN'] !== null) {
                $byte = checkByte($res['CNQRYN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNQRYN'
                    ));
                    break;
                }
            }
        }
        //条件順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CNDSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BSQLCNDのCNDSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CNDSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDSEQ'
                ));
                break;
            }
        }
        //条件区分チェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDKBN'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDKBN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDKBN'] !== '' && $res['CNDKBN'] !== null) {
                $byte = checkByte($res['CNDKBN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDKBN'
                    ));
                    break;
                }
            }
        }
        //条件名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDNM'], 60)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDNM'
                ));
                break;
            }
        }
        //WITH句の条件名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDWNM'], 60)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDWNM'
                ));
                break;
            }
        }
        //条件データタイプチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDDTYP'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDDTYP'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDDTYP'] !== '' && $res['CNDDTYP'] !== null) {
                $byte = checkByte($res['CNDDTYP']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDDTYP'
                    ));
                    break;
                }
            }
        }
        //検索タイプ区分チェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDSTKB'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDSTKB'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDSTKB'] !== '' && $res['CNDSTKB'] !== null) {
                $byte = checkByte($res['CNDSTKB']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDSTKB'
                    ));
                    break;
                }
            }
        }
        //検索条件データチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDDAT'], 128)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDDAT'
                ));
                break;
            }
        }
        //データのスペース数チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CNDBCNT'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BSQLCNDのCNDBCNT',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CNDBCNT'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDBCNT'
                ));
                break;
            }
        }
        //候補選択LIBチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCANL'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNCANL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCANL'] !== '' && $res['CNCANL'] !== null) {
                $byte = checkByte($res['CNCANL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNCANL'
                    ));
                    break;
                }
            }
        }
        //候補選択FILEチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCANF'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNCANF'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCANF'] !== '' && $res['CNCANF'] !== null) {
                $byte = checkByte($res['CNCANF']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNCANF'
                    ));
                    break;
                }
            }
        }
        //候補選択MBRチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCMBR'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNCMBR'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCMBR'] !== '' && $res['CNCMBR'] !== null) {
                $byte = checkByte($res['CNCMBR']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNCMBR'
                    ));
                    break;
                }
            }
        }
        //候補選択COLMチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCANC'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNCANC'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCANC'] !== '' && $res['CNCANC'] !== null) {
                $byte = checkByte($res['CNCANC']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNCANC'
                    ));
                    break;
                }
            }
        }
        //候補GROUPINGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCANG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNCANG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCANG'] !== '' && $res['CNCANG'] !== null) {
                $byte = checkByte($res['CNCANG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNCANG'
                    ));
                    break;
                }
            }
        }
        //名称表示FLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNNAMG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNNAMG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNNAMG'] !== '' && $res['CNNAMG'] !== null) {
                $byte = checkByte($res['CNNAMG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNNAMG'
                    ));
                    break;
                }
            }
        }
        //名称COLMチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNNAMC'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNNAMC'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNNAMC'] !== '' && $res['CNNAMC'] !== null) {
                $byte = checkByte($res['CNNAMC']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNNAMC'
                    ));
                    break;
                }
            }
        }
        //フォーマットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDFMT'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDFMT'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDFMT'] !== '' && $res['CNDFMT'] !== null) {
                $byte = checkByte($res['CNDFMT']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDFMT'
                    ));
                    break;
                }
            }
        }
        //フォーマットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDSFL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDSFL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDSFL'] !== '' && $res['CNDSFL'] !== null) {
                $byte = checkByte($res['CNDSFL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDSFL'
                    ));
                    break;
                }
            }
        }
        //初期値フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDFIN'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDFIN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDFIN'] !== '' && $res['CNDFIN'] !== null) {
                $byte = checkByte($res['CNDFIN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDFIN'
                    ));
                    break;
                }
            }
        }
        //+-フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDFPM'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDFPM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDFPM'] !== '' && $res['CNDFPM'] !== null) {
                $byte = checkByte($res['CNDFPM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDFPM'
                    ));
                    break;
                }
            }
        }
        //移動数チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CNDFDY'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BSQLCNDのCNDFDY',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CNDFDY'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDFDY'
                ));
                break;
            }
        }
        //YMDフラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDFFL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDFFL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDFFL'] !== '' && $res['CNDFFL'] !== null) {
                $byte = checkByte($res['CNDFFL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDFFL'
                    ));
                    break;
                }
            }
        }
        //初期値フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDTIN'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDTIN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDTIN'] !== '' && $res['CNDTIN'] !== null) {
                $byte = checkByte($res['CNDTIN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDTIN'
                    ));
                    break;
                }
            }
        }
        //+-フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDTPM'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDTPM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDTPM'] !== '' && $res['CNDTPM'] !== null) {
                $byte = checkByte($res['CNDTPM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDTPM'
                    ));
                    break;
                }
            }
        }
        //移動数チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CNDTDY'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BSQLCNDのCNDTDY',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CNDTDY'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDTDY'
                ));
                break;
            }
        }
        //YMDフラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDTFL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDTFL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDTFL'] !== '' && $res['CNDTFL'] !== null) {
                $byte = checkByte($res['CNDTFL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDTFL'
                    ));
                    break;
                }
            }
        }
        //COLM SORTチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCAST'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNCAST'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCAST'] !== '' && $res['CNCAST'] !== null) {
                $byte = checkByte($res['CNCAST']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNCAST'
                    ));
                    break;
                }
            }
        }
        //NAME SORTチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNNAST'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNNAST'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNNAST'] !== '' && $res['CNNAST'] !== null) {
                $byte = checkByte($res['CNNAST']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNNAST'
                    ));
                    break;
                }
            }
        }
        //SCH FLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDSCH'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSQLCNDのCNDSCH'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDSCH'] !== '' && $res['CNDSCH'] !== null) {
                $byte = checkByte($res['CNDSCH']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSQLCNDのCNDSCH'
                    ));
                    break;
                }
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//SQLのため検索データチェック
function chkBSqlTypeSearchSQL($db2con, $BSQLDAT, $BSQLCND)
{
    $rs         = true;
    $dataresult = array();
    //BSQLDAT
    if ($rs === true) {
        $dbrs = chkBSQLDAT($db2con, $BSQLDAT);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //BSQLCND
    if ($rs === true) {
        $dbrs = chkBSQLCND($db2con, $BSQLCND);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $dataresult;
}
//BQRYCND
function chkBQRYCND($db2con, $BQRYCND)
{
    $rs   = true;
    $data = array();
    foreach ($BQRYCND as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['CNQRYN'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'BQRYCNDのCNQRYN'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNQRYN'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNQRYN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNQRYN'] !== '' && $res['CNQRYN'] !== null) {
                $byte = checkByte($res['CNQRYN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNQRYN'
                    ));
                    break;
                }
            }
        }
        //ファイルチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CNFILID'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BQRYCNDのCNFILID',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CNFILID'], 4)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNFILID'
                ));
                break;
            }
        }
        //主条件順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CNMSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BQRYCNDのCNMSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CNMSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNMSEQ'
                ));
                break;
            }
        }
        //副条件順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CNSSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BQRYCNDのCNSSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CNSSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNSSEQ'
                ));
                break;
            }
        }
        //AND/ORチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNAOKB'], 2)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNAOKB'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNAOKB'] !== '' && $res['CNAOKB'] !== null) {
                $byte = checkByte($res['CNAOKB']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNAOKB'
                    ));
                    break;
                }
            }
        }
        //フィールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNFLDN'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNFLDN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNFLDN'] !== '' && $res['CNFLDN'] !== null) {
                $byte = checkByte($res['CNFLDN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNFLDN'
                    ));
                    break;
                }
            }
        }
        //条件区分チェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCKBN'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNCKBN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCKBN'] !== '' && $res['CNCKBN'] !== null) {
                $byte = checkByte($res['CNCKBN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNCKBN'
                    ));
                    break;
                }
            }
        }
        //検索タイプ区分チェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNSTKB'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNSTKB'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNSTKB'] !== '' && $res['CNSTKB'] !== null) {
                $byte = checkByte($res['CNSTKB']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNSTKB'
                    ));
                    break;
                }
            }
        }
        //候補選択LIBチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCANL'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNCANL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCANL'] !== '' && $res['CNCANL'] !== null) {
                $byte = checkByte($res['CNCANL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNCANL'
                    ));
                    break;
                }
            }
        }
        //候補選択FILEチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCANF'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNCANF'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCANF'] !== '' && $res['CNCANF'] !== null) {
                $byte = checkByte($res['CNCANF']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNCANF'
                    ));
                    break;
                }
            }
        }
        //候補選択COLMチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCANC'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNCANC'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCANC'] !== '' && $res['CNCANC'] !== null) {
                $byte = checkByte($res['CNCANC']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNCANC'
                    ));
                    break;
                }
            }
        }
        //候補GROUPINGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCANG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNCANG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCANG'] !== '' && $res['CNCANG'] !== null) {
                $byte = checkByte($res['CNCANG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNCANG'
                    ));
                    break;
                }
            }
        }
        //名称表示FLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNNAMG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNNAMG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNNAMG'] !== '' && $res['CNNAMG'] !== null) {
                $byte = checkByte($res['CNNAMG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNNAMG'
                    ));
                    break;
                }
            }
        }
        //名称COLMチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNNAMC'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNNAMC'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNNAMC'] !== '' && $res['CNNAMC'] !== null) {
                $byte = checkByte($res['CNNAMC']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNNAMC'
                    ));
                    break;
                }
            }
        }
        //フォーマットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDFMT'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNDFMT'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDFMT'] !== '' && $res['CNDFMT'] !== null) {
                $byte = checkByte($res['CNDFMT']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNDFMT'
                    ));
                    break;
                }
            }
        }
        //フォーマットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDSFL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNDSFL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDSFL'] !== '' && $res['CNDSFL'] !== null) {
                $byte = checkByte($res['CNDSFL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNDSFL'
                    ));
                    break;
                }
            }
        }
        //初期値フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDFIN'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNDFIN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDFIN'] !== '' && $res['CNDFIN'] !== null) {
                $byte = checkByte($res['CNDFIN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNDFIN'
                    ));
                    break;
                }
            }
        }
        //+-フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDFPM'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNDFPM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDFPM'] !== '' && $res['CNDFPM'] !== null) {
                $byte = checkByte($res['CNDFPM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNDFPM'
                    ));
                    break;
                }
            }
        }
        //移動数チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CNDFDY'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BQRYCNDのCNDFDY',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CNDFDY'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNDFDY'
                ));
                break;
            }
        }
        //YMDフラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDFFL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNDFFL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDFFL'] !== '' && $res['CNDFFL'] !== null) {
                $byte = checkByte($res['CNDFFL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNDFFL'
                    ));
                    break;
                }
            }
        }
        //初期値フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDTIN'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNDTIN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDTIN'] !== '' && $res['CNDTIN'] !== null) {
                $byte = checkByte($res['CNDTIN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNDTIN'
                    ));
                    break;
                }
            }
        }
        //+-フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDTPM'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNDTPM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDTPM'] !== '' && $res['CNDTPM'] !== null) {
                $byte = checkByte($res['CNDTPM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNDTPM'
                    ));
                    break;
                }
            }
        }
        //移動数チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CNDTDY'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BQRYCNDのCNDTDY',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CNDTDY'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNDTDY'
                ));
                break;
            }
        }
        //YMDフラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNDTFL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNDTFL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNDTFL'] !== '' && $res['CNDTFL'] !== null) {
                $byte = checkByte($res['CNDTFL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNDTFL'
                    ));
                    break;
                }
            }
        }
        //COLM SORTチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNCAST'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNCAST'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNCAST'] !== '' && $res['CNCAST'] !== null) {
                $byte = checkByte($res['CNCAST']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNCAST'
                    ));
                    break;
                }
            }
        }
        //NAME SORTチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CNNAST'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BQRYCNDのCNNAST'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CNNAST'] !== '' && $res['CNNAST'] !== null) {
                $byte = checkByte($res['CNNAST']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BQRYCNDのCNNAST'
                    ));
                    break;
                }
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//BCNDDAT
function chkBCNDDAT($db2con, $BCNDDAT)
{
    $rs   = true;
    $data = array();
    foreach ($BCNDDAT as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['CDQRYN'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'BCNDDATのCDQRYN'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CDQRYN'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BCNDDATのCDQRYN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res[''] !== '' && $res[''] !== null) {
                $byte = checkByte($res['CDQRYN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BCNDDATのCDQRYN'
                    ));
                    break;
                }
            }
        }
        //ファイルIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CDFILID'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BCNDDATのCDFILID',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CDFILID'], 4)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BCNDDATのCDFILID'
                ));
                break;
            }
        }
        //主条件順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CDMSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BCNDDATのCDMSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CDMSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BCNDDATのCDMSEQ'
                ));
                break;
            }
        }
        //副条件順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CDSSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BCNDDATのCDSSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CDSSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BCNDDATのCDSSEQ'
                ));
                break;
            }
        }
        //条件データコードチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CDCDCD'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BCNDDATのCDCDCD',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CDCDCD'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BCNDDATのCDCDCD'
                ));
                break;
            }
        }
        //条件データチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CDDAT'], 128)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BCNDDATのCDDAT'
                ));
                break;
            }
        }
        //条件データの空白チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['CDBCNT'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BCNDDATのCDBCNT',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['CDBCNT'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BCNDDATのCDBCNT'
                ));
                break;
            }
        }
        //フィールドFLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['CDFFLG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BCNDDATのCDFFLG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['CDFFLG'] !== '' && $res['CDFFLG'] !== null) {
                $byte = checkByte($res['CDFFLG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BCNDDATのCDFFLG'
                    ));
                    break;
                }
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//普通のクエリーのため検索データチェック
function chkBSqlTypeSearchQry($db2con, $BQRYCND, $BCNDDAT)
{
    $rs         = true;
    $dataresult = array();
    //BQRYCND
    if ($rs === true) {
        $dbrs = chkBQRYCND($db2con, $BQRYCND);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //BCNDDAT
    if ($rs === true) {
        $dbrs = chkBCNDDAT($db2con, $BCNDDAT);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $dataresult;
}
//FDB2CSV3
function chkFDB2CSV3($db2con, $FDB2CSV3)
{
    $rs   = true;
    $data = array();
    foreach ($FDB2CSV3 as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['D3NAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'FDB2CSV3のD3NAME'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3NAME'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3NAME'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3NAME'] !== '' && $res['D3NAME'] !== null) {
                $byte = checkByte($res['D3NAME']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3NAME'
                    ));
                    break;
                }
            }
        }
        //ファイルIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D3FILID'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV3のD3FILID',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D3FILID'], 2)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3FILID'
                ));
                break;
            }
        }
        //順序番号チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D3JSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV3のD3JSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D3JSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3JSEQ'
                ));
                break;
            }
        }
        //選択行チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D3CNT'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV3のD3CNT',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D3CNT'], 2)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3CNT'
                ));
                break;
            }
        }
        //AND/ORチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3ANDOR'], 2)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3ANDOR'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3ANDOR'] !== '' && $res['D3ANDOR'] !== null) {
                $byte = checkByte($res['D3ANDOR']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3ANDOR'
                    ));
                    break;
                }
            }
        }
        //フィールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3FLD'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3FLD'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3FLD'] !== '' && $res['D3FLD'] !== null) {
                $byte = checkByte($res['D3FLD']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3FLD'
                    ));
                    break;
                }
            }
        }
        //条件チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3CND'], 5)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3CND'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3CND'] !== '' && $res['D3CND'] !== null) {
                $byte = checkByte($res['D3CND']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3CND'
                    ));
                    break;
                }
            }
        }
        //データチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3DAT'], 128)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DAT'
                ));
                break;
            }
        }
        //WEB(*)チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3USEL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3USEL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3USEL'] !== '' && $res['D3USEL'] !== null) {
                $byte = checkByte($res['D3USEL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3USEL'
                    ));
                    break;
                }
            }
        }
        //FILED TYPEチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3TYPE'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3TYPE'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3TYPE'] !== '' && $res['D3TYPE'] !== null) {
                $byte = checkByte($res['D3TYPE']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3TYPE'
                    ));
                    break;
                }
            }
        }
        //FILED LENGTHチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D3LEN'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV3のD3LEN',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D3LEN'], 5)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3LEN'
                ));
                break;
            }
        }
        //候補選択LIBチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3CANL'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3CANL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3CANL'] !== '' && $res['D3CANL'] !== null) {
                $byte = checkByte($res['D3CANL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3CANL'
                    ));
                    break;
                }
            }
        }
        //候補選択FILEチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3CANF'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3CANF'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3CANF'] !== '' && $res['D3CANF'] !== null) {
                $byte = checkByte($res['D3CANF']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3CANF'
                    ));
                    break;
                }
            }
        }
        //候補選択COLUMNチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3CANC'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3CANC'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3CANC'] !== '' && $res['D3CANC'] !== null) {
                $byte = checkByte($res['D3CANC']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3CANC'
                    ));
                    break;
                }
            }
        }
        //候補GROUPINGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3CANG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3CANG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3CANG'] !== '' && $res['D3CANG'] !== null) {
                $byte = checkByte($res['D3CANG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3CANG'
                    ));
                    break;
                }
            }
        }
        //名称表示FLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3NAMG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3NAMG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3NAMG'] !== '' && $res['D3NAMG'] !== null) {
                $byte = checkByte($res['D3NAMG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3NAMG'
                    ));
                    break;
                }
            }
        }
        //名称COLUMNチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3NAMC'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3NAMC'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3NAMC'] !== '' && $res['D3NAMC'] !== null) {
                $byte = checkByte($res['D3NAMC']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3NAMC'
                    ));
                    break;
                }
            }
        }
        //フォーマットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3DFMT'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DFMT'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3DFMT'] !== '' && $res['D3DFMT'] !== null) {
                $byte = checkByte($res['D3DFMT']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3DFMT'
                    ));
                    break;
                }
            }
        }
        //フォーマット付チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3DSFL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DSFL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3DSFL'] !== '' && $res['D3DSFL'] !== null) {
                $byte = checkByte($res['D3DSFL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3DSFL'
                    ));
                    break;
                }
            }
        }
        //初期値フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3DFIN'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DFIN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3DFIN'] !== '' && $res['D3DFIN'] !== null) {
                $byte = checkByte($res['D3DFIN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3DFIN'
                    ));
                    break;
                }
            }
        }
        //+-フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3DFPM'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DFPM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3DFPM'] !== '' && $res['D3DFPM'] !== null) {
                $byte = checkByte($res['D3DFPM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3DFPM'
                    ));
                    break;
                }
            }
        }
        //移動数チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D3DFDY'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV3のD3DFDY',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D3DFDY'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DFDY'
                ));
                break;
            }
        }
        //YMDフラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3DFFL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DFFL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3DFFL'] !== '' && $res['D3DFFL'] !== null) {
                $byte = checkByte($res['D3DFFL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3DFFL'
                    ));
                    break;
                }
            }
        }
        //初期値フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3DTIN'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DTIN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3DTIN'] !== '' && $res['D3DTIN'] !== null) {
                $byte = checkByte($res['D3DTIN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3DTIN'
                    ));
                    break;
                }
            }
        }
        //+-フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3DTPM'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DTPM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3DTPM'] !== '' && $res['D3DTPM'] !== null) {
                $byte = checkByte($res['D3DTPM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3DTPM'
                    ));
                    break;
                }
            }
        }
        //移動数チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D3DTDY'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV3のD3DTDY',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D3DTDY'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DTDY'
                ));
                break;
            }
        }
        //YMDフラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3DTFL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3DTFL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3DTFL'] !== '' && $res['D3DTFL'] !== null) {
                $byte = checkByte($res['D3DTFL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3DTFL'
                    ));
                    break;
                }
            }
        }
        //COLM SORTチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3CAST'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3CAST'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3CAST'] !== '' && $res['D3CAST'] !== null) {
                $byte = checkByte($res['D3CAST']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3CAST'
                    ));
                    break;
                }
            }
        }
        //NAME SORTチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D3NAST'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV3のD3NAST'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D3NAST'] !== '' && $res['D3NAST'] !== null) {
                $byte = checkByte($res['D3NAST']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV3のD3NAST'
                    ));
                    break;
                }
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//黒いのため検索データチェック
function chkBSqlTypeSearchKuroi($db2con, $FDB2CSV3)
{
    $rs         = true;
    $dataresult = array();
    //FDB2CSV3
    if ($rs === true) {
        $dbrs = chkFDB2CSV3($db2con, $FDB2CSV3);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $dataresult;
}
//BSUMFLD
function chkBSUMFLD($db2con, $BSUMFLD)
{
    $rs   = true;
    $data = array();
    foreach ($BSUMFLD as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['SFQRYN'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'BSUMFLDのSFQRYN'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['SFQRYN'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSUMFLDのSFQRYN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['SFQRYN'] !== '' && $res['SFQRYN'] !== null) {
                $byte = checkByte($res['SFQRYN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSUMFLDのSFQRYN'
                    ));
                    break;
                }
            }
        }
        //ファイルIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['SFFILID'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BSUMFLDのSFFILID',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['SFFILID'], 4)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSUMFLDのSFFILID'
                ));
                break;
            }
        }
        //フィールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['SFFLDNM'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSUMFLDのSFFLDNM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['SFFLDNM'] !== '' && $res['SFFLDNM'] !== null) {
                $byte = checkByte($res['SFFLDNM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSUMFLDのSFFLDNM'
                    ));
                    break;
                }
            }
        }
        //サマリー順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['SFSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BSUMFLDのSFSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['SFSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSUMFLDのSFSEQ'
                ));
                break;
            }
        }
        //集計コードチェック
        if ($rs === true) {
            if (!checkMaxLen($res['SFGMES'], 5)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BSUMFLDのSFGMES'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['SFGMES'] !== '' && $res['SFGMES'] !== null) {
                $byte = checkByte($res['SFGMES']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BSUMFLDのSFGMES'
                    ));
                    break;
                }
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//結果データチェック
function chkBSqlTypeBSUMFLD($db2con, $BSUMFLD)
{
    $rs         = true;
    $dataresult = array();
    //BSUMFLD
    if ($rs === true) {
        $dbrs = chkBSUMFLD($db2con, $BSUMFLD);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $dataresult;
}
//FDB2CSV5
function chkFDB2CSV5($db2con, $FDB2CSV5)
{
    $rs   = true;
    $data = array();
    foreach ($FDB2CSV5 as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['D5NAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'FDB2CSV5のD5NAME'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D5NAME'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5NAME'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D5NAME'] !== '' && $res['D5NAME'] !== null) {
                $byte = checkByte($res['D5NAME']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV5のD5NAME'
                    ));
                    break;
                }
            }
        }
        //順序番号チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D5ASEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV5のD5ASEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D5ASEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5ASEQ'
                ));
                break;
            }
        }
        //フィールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D5FLD'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5FLD'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D5FLD'] !== '' && $res['D5FLD'] !== null) {
                $byte = checkByte($res['D5FLD']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV5のD5FLD'
                    ));
                    break;
                }
            }
        }
        //カラムヘディングチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D5HED'], 60)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5HED'
                ));
                break;
            }
        }
        //セル順序番号チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D5CSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV5のD5CSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D5CSEQ'], 4)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5CSEQ'
                ));
                break;
            }
        }
        //サマリーSEQチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D5GSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV5のD5GSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D5GSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5GSEQ'
                ));
                break;
            }
        }
        //合計方法チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D5GMES'], 5)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5GMES'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D5GMES'] !== '' && $res['D5GMES'] !== null) {
                $byte = checkByte($res['D5GMES']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV5のD5GMES'
                    ));
                    break;
                }
            }
        }
        //IPDCヘッダチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D5IPDC'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5IPDC'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D5IPDC'] !== '' && $res['D5IPDC'] !== null) {
                $byte = checkByte($res['D5IPDC']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV5のD5IPDC'
                    ));
                    break;
                }
            }
        }
        //WEB編集コードチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D5WEDT'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5WEDT'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D5WEDT'] !== '' && $res['D5WEDT'] !== null) {
                $byte = checkByte($res['D5WEDT']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV5のD5WEDT'
                    ));
                    break;
                }
            }
        }
        //タイプチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D5TYPE'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5TYPE'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D5TYPE'] !== '' && $res['D5TYPE'] !== null) {
                $byte = checkByte($res['D5TYPE']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV5のD5TYPE'
                    ));
                    break;
                }
            }
        }
        //長さチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D5LEN'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV5のD5LEN',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D5LEN'], 5)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5LEN'
                ));
                break;
            }
        }
        //昇順/降順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D5DEC'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV5のD5DEC',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D5DEC'], 2)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5DEC'
                ));
                break;
            }
        }
        //計算式チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D5EXP'], 1024)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5EXP'
                ));
                break;
            }
        }
        //ダウンロード可否チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D5DNLF'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5DNLF'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D5DNLF'] !== '' && $res['D5DNLF'] !== null) {
                $byte = checkByte($res['D5DNLF']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV5のD5DNLF'
                    ));
                    break;
                }
            }
        }
        //SORTSEQチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D5RSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV5のD5RSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D5RSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5RSEQ'
                ));
                break;
            }
        }
        //SORTDIRチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D5RSTP'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV5のD5RSTP'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D5RSTP'] !== '' && $res['D5RSTP'] !== null) {
                $byte = checkByte($res['D5RSTP']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV5のD5RSTP'
                    ));
                    break;
                }
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//結果データチェック
function chkBSqlTypeFDB2CSV5($db2con, $FDB2CSV5)
{
    $rs         = true;
    $dataresult = array();
    //FDB2CSV5
    if ($rs === true) {
        $dbrs = chkFDB2CSV5($db2con, $FDB2CSV5);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $dataresult;
}
//FDB2CSV2
function chkFDB2CSV2($db2con, $FDB2CSV2)
{
    $rs   = true;
    $data = array();
    foreach ($FDB2CSV2 as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['D2NAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'FDB2CSV2のD2NAME'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D2NAME'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2NAME'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D2NAME'] !== '' && $res['D2NAME'] !== null) {
                $byte = checkByte($res['D2NAME']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV2のD2NAME'
                    ));
                    break;
                }
            }
        }
        //ファイルＩＤチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D2FILID'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV2のD2FILID',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D2FILID'], 2)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2FILID'
                ));
                break;
            }
        }
        //フィールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D2FLD'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2FLD'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D2FLD'] !== '' && $res['D2FLD'] !== null) {
                $byte = checkByte($res['D2FLD']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV2のD2FLD'
                    ));
                    break;
                }
            }
        }
        //カラムヘディングチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D2HED'], 60)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2HED'
                ));
                break;
            }
        }
        //セル順序番号チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D2CSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV2のD2CSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D2CSEQ'], 4)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2CSEQ'
                ));
                break;
            }
        }
        //レコード順序チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D2RSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV2のD2RSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D2RSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2RSEQ'
                ));
                break;
            }
        }
        //レコード順指定チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D2RSTP'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2RSTP'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D2RSTP'] !== '' && $res['D2RSTP'] !== null) {
                $byte = checkByte($res['D2RSTP']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV2のD2RSTP'
                    ));
                    break;
                }
            }
        }
        //サマリーキーチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D2GSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV2のD2GSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D2GSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2GSEQ'
                ));
                break;
            }
        }
        //合計方法チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D2GMES'], 5)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2GMES'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D2GMES'] !== '' && $res['D2GMES'] !== null) {
                $byte = checkByte($res['D2GMES']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV2のD2GMES'
                    ));
                    break;
                }
            }
        }
        //IPDCヘッダチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D2IPDC'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2IPDC'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D2IPDC'] !== '' && $res['D2IPDC'] !== null) {
                $byte = checkByte($res['D2IPDC']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV2のD2IPDC'
                    ));
                    break;
                }
            }
        }
        //WEB編集コードチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D2WEDT'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2WEDT'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D2WEDT'] !== '' && $res['D2WEDT'] !== null) {
                $byte = checkByte($res['D2WEDT']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV2のD2WEDT'
                    ));
                    break;
                }
            }
        }
        //タイプチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D2TYPE'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2TYPE'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D2TYPE'] !== '' && $res['D2TYPE'] !== null) {
                $byte = checkByte($res['D2TYPE']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV2のD2TYPE'
                    ));
                    break;
                }
            }
        }
        //長さチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D2LEN'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV2のD2LEN',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D2LEN'], 5)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2LEN'
                ));
                break;
            }
        }
        //昇順/降順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D2DEC'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV2のD2DEC',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D2DEC'], 2)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2DEC'
                ));
                break;
            }
        }
        //ダウンロード可否チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D2DNLF'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV2のD2DNLF'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D2DNLF'] !== '' && $res['D2DNLF'] !== null) {
                $byte = checkByte($res['D2DNLF']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV2のD2DNLF'
                    ));
                    break;
                }
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//カラムチェック
function chkBSqlTypeFDB2CSV2($db2con, $FDB2CSV2)
{
    $rs         = true;
    $dataresult = array();
    //FDB2CSV2
    if ($rs === true) {
        $dbrs = chkFDB2CSV2($db2con, $FDB2CSV2);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $dataresult;
}
//FDB2CSV4
function chkFDB2CSV4($db2con, $FDB2CSV4)
{
    $rs   = true;
    $data = array();
    foreach ($FDB2CSV4 as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['D4NAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'FDB2CSV4のD4NAME'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4NAME'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4NAME'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4NAME'] !== '' && $res['D4NAME'] !== null) {
                $byte = checkByte($res['D4NAME']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4NAME'
                    ));
                    break;
                }
            }
        }
        //参照番号チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D4REFN'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV4のD4REFN',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D4REFN'], 2)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4REFN'
                ));
                break;
            }
        }
        //参照FILEチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4RFIL'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4RFIL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4RFIL'] !== '' && $res['D4RFIL'] !== null) {
                $byte = checkByte($res['D4RFIL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4RFIL'
                    ));
                    break;
                }
            }
        }
        //参照LIBチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4RLIB'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4RLIB'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4RLIB'] !== '' && $res['D4RLIB'] !== null) {
                $byte = checkByte($res['D4RLIB']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4RLIB'
                    ));
                    break;
                }
            }
        }
        //記述チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4TEXT'], 50)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4TEXT'
                ));
                break;
            }
        }
        //P-FIL-FLD1チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4PRFL1'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4PRFL1'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4PRFL1'] !== '' && $res['D4PRFL1'] !== null) {
                $byte = checkByte($res['D4PRFL1']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4PRFL1'
                    ));
                    break;
                }
            }
        }
        //R-FIL-FLD1チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4RFFL1'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4RFFL1'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4RFFL1'] !== '' && $res['D4RFFL1'] !== null) {
                $byte = checkByte($res['D4RFFL1']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4RFFL1'
                    ));
                    break;
                }
            }
        }
        //P-FIL-FLD2チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4PRFL2'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4PRFL2'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4PRFL2'] !== '' && $res['D4PRFL2'] !== null) {
                $byte = checkByte($res['D4PRFL2']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4PRFL2'
                    ));
                    break;
                }
            }
        }
        //R-FIL-FLD2チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4RFFL2'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4RFFL2'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4RFFL2'] !== '' && $res['D4RFFL2'] !== null) {
                $byte = checkByte($res['D4RFFL2']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4RFFL2'
                    ));
                    break;
                }
            }
        }
        //P-FIL-FLD3チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4PRFL3'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4PRFL3'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4PRFL3'] !== '' && $res['D4PRFL3'] !== null) {
                $byte = checkByte($res['D4PRFL3']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4PRFL3'
                    ));
                    break;
                }
            }
        }
        //R-FIL-FLD3チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4RFFL3'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4RFFL3'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4RFFL3'] !== '' && $res['D4RFFL3'] !== null) {
                $byte = checkByte($res['D4RFFL3']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4RFFL3'
                    ));
                    break;
                }
            }
        }
        //P-FIL-FLD4チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4PRFL4'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4PRFL4'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4PRFL4'] !== '' && $res['D4PRFL4'] !== null) {
                $byte = checkByte($res['D4PRFL4']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4PRFL4'
                    ));
                    break;
                }
            }
        }
        //R-FIL-FLD4チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4RFFL4'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4RFFL4'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4RFFL4'] !== '' && $res['D4RFFL4'] !== null) {
                $byte = checkByte($res['D4RFFL4']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4RFFL4'
                    ));
                    break;
                }
            }
        }
        //P-FIL-FLD5チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4PRFL5'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4PRFL5'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4PRFL5'] !== '' && $res['D4PRFL5'] !== null) {
                $byte = checkByte($res['D4PRFL5']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4PRFL5'
                    ));
                    break;
                }
            }
        }
        //R-FIL-FLD5チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D4RFFL5'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV4のD4RFFL5'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D4RFFL5'] !== '' && $res['D4RFFL5'] !== null) {
                $byte = checkByte($res['D4RFFL5']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV4のD4RFFL5'
                    ));
                    break;
                }
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//黒い画面のジョンチェック
function chkBSqlTypeFDB2CSV4($db2con, $FDB2CSV4)
{
    $rs         = true;
    $dataresult = array();
    //FDB2CSV4
    if ($rs === true) {
        $dbrs = chkFDB2CSV4($db2con, $FDB2CSV4);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $dataresult;
}
//BREFTBL
function chkBREFTBL($db2con, $BREFTBL)
{
    $rs   = true;
    $data = array();
    foreach ($BREFTBL as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['RTQRYN'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'BREFTBLのRTQRYN'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['RTQRYN'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFTBLのRTQRYN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['RTQRYN'] !== '' && $res['RTQRYN'] !== null) {
                $byte = checkByte($res['RTQRYN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BREFTBLのRTQRYN'
                    ));
                    break;
                }
            }
        }
        //参照順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['RTRSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BREFTBLのRTRSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['RTRSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFTBLのRTRSEQ'
                ));
                break;
            }
        }
        //参照ファイルチェック
        if ($rs === true) {
            if (!checkMaxLen($res['RTRFIL'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFTBLのRTRFIL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['RTRFIL'] !== '' && $res['RTRFIL'] !== null) {
                $byte = checkByte($res['RTRFIL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BREFTBLのRTRFIL'
                    ));
                    break;
                }
            }
        }
        //参照ライブラリチェック
        if ($rs === true) {
            if (!checkMaxLen($res['RTRLIB'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFTBLのRTRLIB'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['RTRLIB'] !== '' && $res['RTRLIB'] !== null) {
                $byte = checkByte($res['RTRLIB']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BREFTBLのRTRLIB'
                    ));
                    break;
                }
            }
        }
        //参照テーブル記述チェック
        if ($rs === true) {
            if (!checkMaxLen($res['RTDESC'], 50)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFTBLのRTDESC'
                ));
                break;
            }
        }
        //参照タイプチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['RTJTYP'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BREFTBLのRTJTYP',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['RTJTYP'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFTBLのRTJTYP'
                ));
                break;
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//BREFFLD
function chkBREFFLD($db2con, $BREFFLD)
{
    $rs   = true;
    $data = array();
    foreach ($BREFFLD as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['RFQRYN'], '', '');
            if ($chkQry['result'] !== true) {
                $rs  = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array(
                    'BREFFLDのRFQRYN'
                ));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['RFQRYN'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFFLDのRFQRYN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['RFQRYN'] !== '' && $res['RFQRYN'] !== null) {
                $byte = checkByte($res['RFQRYN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BREFFLDのRFQRYN'
                    ));
                    break;
                }
            }
        }
        //参照順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['RFRSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BREFFLDのRFRSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['RFRSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFFLDのRFRSEQ'
                ));
                break;
            }
        }
        //参照フィールド順チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['RFFSEQ'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BREFFLDのRFFSEQ',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['RFFSEQ'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFFLDのRFFSEQ'
                ));
                break;
            }
        }
        //参照先ファイルIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['RFPFID'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BREFFLDのRFPFID',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['RFPFID'], 4)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFFLDのRFPFID'
                ));
                break;
            }
        }
        //参照先フィールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['RFPFNM'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFFLDのRFPFNM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['RFPFNM'] !== '' && $res['RFPFNM'] !== null) {
                $byte = checkByte($res['RFPFNM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BREFFLDのRFPFNM'
                    ));
                    break;
                }
            }
        }
        //参照ファイルIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['RFRFID'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'BREFFLDのRFRFID',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['RFRFID'], 4)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFFLDのRFRFID'
                ));
                break;
            }
        }
        //参照フィールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['RFRFNM'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFFLDのRFRFNM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['RFRFNM'] !== '' && $res['RFRFNM'] !== null) {
                $byte = checkByte($res['RFRFNM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BREFFLDのRFRFNM'
                    ));
                    break;
                }
            }
        }
        //条件区分チェック
        if ($rs === true) {
            if (!checkMaxLen($res['RFRKBN'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'BREFFLDのRFRKBN'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['RFRKBN'] !== '' && $res['RFRKBN'] !== null) {
                $byte = checkByte($res['RFRKBN']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'BREFFLDのRFRKBN'
                    ));
                    break;
                }
            }
        }
    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
//普通のクエリーのジョンチェック
function chkBSqlTypeJoinQry($db2con, $BREFTBL, $BREFFLD)
{
    $rs         = true;
    $dataresult = array();
    //BREFTBL
    if ($rs === true) {
        $dbrs = chkBREFTBL($db2con, $BREFTBL);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //BREFFLD
    if ($rs === true) {
        $dbrs = chkBREFFLD($db2con, $BREFFLD);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $dataresult;
}
//FDB2CSV1
function chkFDB2CSV1($db2con, $FDB2CSV1)
{
    $rs   = true;
    $data = array();
    foreach ($FDB2CSV1 as $res) {
        //定義名がないチェック
        if ($rs === true) {
            if (cmMer($res['D1NAME']) === '') {
                $rs  = 'FAIL_REQ';
                $msg = showMsg('FAIL_REQ', array(
                    'FDB2CSV1のD1NAME'
                ));
                break;
            }
        }
        //定義名あるかどうかのチェック
        if ($rs === true) {
            $dbRes = fnChkExistD1NAME($db2con, $res['D1NAME']);
            if ($dbRes !== true) {
                $rs  = $dbRes;
                $msg = showMsg($dbRes, array(
                    'FDB2CSV1のD1NAME'
                ));
                break;
            }
        }
        //定義名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1NAME'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1NAME'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1NAME'] !== '' && $res['D1NAME'] !== null) {
                $byte = checkByte($res['D1NAME']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1NAME'
                    ));
                    break;
                }
            }
        }
        //ﾌﾟﾗｲﾏﾘｰ･ﾌｧｲﾙチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1FILE'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1FILE'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1FILE'] !== '' && $res['D1FILE'] !== null) {
                $byte = checkByte($res['D1FILE']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1FILE'
                    ));
                    break;
                }
            }
        }
        //ﾌﾟﾗｲﾏﾘｰ･ﾌｧｲﾙチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1FILMBR'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1FILMBR'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1FILMBR'] !== '' && $res['D1FILMBR'] !== null) {
                $byte = checkByte($res['D1FILMBR']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1FILMBR'
                    ));
                    break;
                }
            }
        }
        //ﾌﾟﾗｲﾏﾘｰ･ﾌｧｲﾙ･ﾗｲﾌﾞﾗﾘチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1FILLIB'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1FILLIB'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1FILLIB'] !== '' && $res['D1FILLIB'] !== null) {
                $byte = checkByte($res['D1FILLIB']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1FILLIB'
                    ));
                    break;
                }
            }
        }
        //記述チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1TEXT'], 50)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1TEXT'
                ));
                break;
            }
        }
        //実行時CCSIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D1CSID'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV1のD1CSID',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D1CSID'], 5)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CSID'
                ));
                break;
            }
        }
        //実行時LIBLチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1LIBL'], 2750)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1LIBL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1LIBL'] !== '' && $res['D1LIBL'] !== null) {
                $byte = checkByte($res['D1LIBL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1LIBL'
                    ));
                    break;
                }
            }
        }
        //実行前CMDチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1CMD'], 500)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CMD'
                ));
                break;
            }
        }
        //実行中CMDチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1CMDC'], 500)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CMDC'
                ));
                break;
            }
        }
        //ｲﾝﾌｫﾒｰｼｮﾝチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1INFO'], 1536)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1INFO'
                ));
                break;
            }
        }
        //INFOのフラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1INFG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1INFG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1INFG'] !== '' && $res['D1INFG'] !== null) {
                $byte = checkByte($res['D1INFG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1INFG'
                    ));
                    break;
                }
            }
        }
        //S実行保管期間チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D1HOKN'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV1のD1HOKN',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D1HOKN'], 4)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1HOKN'
                ));
                break;
            }
        }
        //CSV出力先チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1DIRE'], 256)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1DIRE'
                ));
                break;
            }
        }
        //EXCEK出力先チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1DIEX'], 256)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1DIEX'
                ));
                break;
            }
        }
        //CSV文字コードチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1CENC'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CENC'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1CENC'] !== '' && $res['D1CENC'] !== null) {
                $byte = checkByte($res['D1CENC']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1CENC'
                    ));
                    break;
                }
            }
        }
        //CSV改行コードチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1CKAI'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CKAI'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1CKAI'] !== '' && $res['D1CKAI'] !== null) {
                $byte = checkByte($res['D1CKAI']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1CKAI'
                    ));
                    break;
                }
            }
        }
        //CSVヘッダーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1CHED'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CHED'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1CHED'] !== '' && $res['D1CHED'] !== null) {
                $byte = checkByte($res['D1CHED']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1CHED'
                    ));
                    break;
                }
            }
        }
        //CSV区切り文字チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1CDLM'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CDLM'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1CDLM'] !== '' && $res['D1CDLM'] !== null) {
                $byte = checkByte($res['D1CDLM']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1CDLM'
                    ));
                    break;
                }
            }
        }
        //CSV囲い文字チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1CECL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CECL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1CECL'] !== '' && $res['D1CECL'] !== null) {
                $byte = checkByte($res['D1CECL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1CECL'
                    ));
                    break;
                }
            }
        }
        //CSVタイムFLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1CTFL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CTFL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1CTFL'] !== '' && $res['D1CTFL'] !== null) {
                $byte = checkByte($res['D1CTFL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1CTFL'
                    ));
                    break;
                }
            }
        }
        //実行後CMDチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1CMDE'], 500)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CMDE'
                ));
                break;
            }
        }
        //メニュー非表示チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1MFLG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1MFLG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1MFLG'] !== '' && $res['D1MFLG'] !== null) {
                $byte = checkByte($res['D1MFLG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1MFLG'
                    ));
                    break;
                }
            }
        }
        //メール0件時チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D10MAL'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD10MAL'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D10MAL'] !== '' && $res['D10MAL'] !== null) {
                $byte = checkByte($res['D10MAL']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD10MAL'
                    ));
                    break;
                }
            }
        }
        //ダウンロード形式チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1EDKB'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1EDKB'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1EDKB'] !== '' && $res['D1EDKB'] !== null) {
                $byte = checkByte($res['D1EDKB']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1EDKB'
                    ));
                    break;
                }
            }
        }
        //TEMPファイルチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1TMPF'], 50)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1TMPF'
                ));
                break;
            }
        }
        //シートチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D1SHET'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV1のD1SHET',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D1SHET'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1SHET'
                ));
                break;
            }
        }
        //開始列チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1TCOS'], 3)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1TCOS'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1TCOS'] !== '' && $res['D1TCOS'] !== null) {
                $byte = checkByte($res['D1TCOS']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1TCOS'
                    ));
                    break;
                }
            }
        }
        //開始行チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D1TROS'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV1のD1TROS',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D1TROS'], 10)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1TROS'
                ));
                break;
            }
        }
        //ヘッダー出力チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1THFG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1THFG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1THFG'] !== '' && $res['D1THFG'] !== null) {
                $byte = checkByte($res['D1THFG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1THFG'
                    ));
                    break;
                }
            }
        }
        //WEB作成フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1WEBF'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1WEBF'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1WEBF'] !== '' && $res['D1WEBF'] !== null) {
                $byte = checkByte($res['D1WEBF']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1WEBF'
                    ));
                    break;
                }
            }
        }
        //ラジオボタンFLGチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['D1DISF'])) {
                $rs  = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array(
                    'FDB2CSV1のD1DISF',
                    '数値'
                ));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['D1DISF'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1DISF'
                ));
                break;
            }
        }
        //カスタム名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DISCSN'], 30)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のDISCSN'
                ));
                break;
            }
        }
        //エクセルFLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1EFLG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1EFLG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1EFLG'] !== '' && $res['D1EFLG'] !== null) {
                $byte = checkByte($res['D1EFLG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1EFLG'
                    ));
                    break;
                }
            }
        }
        if($rs === true){
            if(!checkMaxLen($res['D1ETYP'],1)){
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN',array('FDB2CSV1のD1ETYP'));
                break;
            }
        }
        if($rs === true){
            if($res['D1ETYP'] !=='' && $res['D1ETYP'] !==null){
                $byte = checkByte($res['D1ETYP']);
                if($byte[1] > 0){
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE',array('FDB2CSV1のD1ETYP'));
                    break;
                }
            }
        }
        //区画名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1RDB'], 18)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1RDB'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1RDB'] !== '' && $res['D1RDB'] !== null) {
                $byte = checkByte($res['D1RDB']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1RDB'
                    ));
                    break;
                }
            }
        }
        //作成FLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1CFLG'], 1)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1CFLG'
                ));
                break;
            }
        }
        if ($rs === true) {
            if ($res['D1CFLG'] !== '' && $res['D1CFLG'] !== null) {
                $byte = checkByte($res['D1CFLG']);
                if ($byte[1] > 0) {
                    $rs  = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array(
                        'FDB2CSV1のD1CFLG'
                    ));
                    break;
                }
            }
        }
        //サーバチェック
        if ($rs === true) {
            if (!checkMaxLen($res['D1SERV'], 256)) {
                $rs  = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array(
                    'FDB2CSV1のD1SERV'
                ));
                break;
            }
        }
        //出力ファイルの実行時にユーザーが出力先を設定
        if($rs === true){
            if(!checkMaxLen($res['D1OUTJ'],1)){
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN',array('FDB2CSV1のD1OUTJ'));
                break;
            }
        }
        if($rs === true){
            if($res['D1OUTJ'] !=='' && $res['D1OUTJ'] !==null){
                $byte = checkByte($res['D1OUTJ']);
                if($byte[1] > 0){
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE',array('FDB2CSV1のD1OUTJ'));
                    break;
                }
            }
        }
        //出力ファイルのライブラリー
        if($rs === true){
            if(!checkMaxLen($res['D1OUTLIB'],10)){
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN',array('FDB2CSV1のD1OUTLIB'));
                break;
            }
        }
        if($rs === true){
            if($res['D1OUTLIB'] !=='' && $res['D1OUTLIB'] !==null){
                $byte = checkByte($res['D1OUTLIB']);
                if($byte[1] > 0){
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE',array('FDB2CSV1のD1OUTLIB'));
                    break;
                }
            }
        }
        //出力ファイルのファイル
        if($rs === true){
            if(!checkMaxLen($res['D1OUTFIL'],10)){
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN',array('FDB2CSV1のD1OUTFIL'));
                break;
            }
        }
        if($rs === true){
            if($res['D1OUTFIL'] !=='' && $res['D1OUTFIL'] !==null){
                $byte = checkByte($res['D1OUTFIL']);
                if($byte[1] > 0){
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE',array('FDB2CSV1のD1OUTFIL'));
                    break;
                }
            }
        }
        //出力ファイルのファイルの上書き
        if($rs === true){
            if(!checkMaxLen($res['D1OUTR'],1)){
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN',array('FDB2CSV1のD1OUTR'));
                break;
            }
        }
        if($rs === true){
            if($res['D1OUTR'] !=='' && $res['D1OUTR'] !==null){
                $byte = checkByte($res['D1OUTR']);
                if($byte[1] > 0){
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE',array('FDB2CSV1のD1OUTR'));
                    break;
                }
            }
        }
        //スケジュール出力ファイル設定
        if($rs === true){
            if(!checkMaxLen($res['D1OUTSE'],1)){
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN',array('FDB2CSV1のD1OUTSE'));
                break;
            }
        }
        if($rs === true){
            if($res['D1OUTSE'] !=='' && $res['D1OUTSE'] !==null){
                $byte = checkByte($res['D1OUTSE']);
                if($byte[1] > 0){
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE',array('FDB2CSV1のD1OUTSE'));
                    break;
                }
            }
        }
        //スケジュール出力ファイル上書き設定
        if($rs === true){
            if(!checkMaxLen($res['D1OUTSR'],1)){
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN',array('FDB2CSV1のD1OUTSR'));
                break;
            }
        }
        if($rs === true){
            if($res['D1OUTSR'] !=='' && $res['D1OUTSR'] !==null){
                $byte = checkByte($res['D1OUTSR']);
                if($byte[1] > 0){
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE',array('FDB2CSV1のD1OUTSR'));
                    break;
                }
            }
        }

    }
    $data = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $data;
}
// FDB2CSV1 クエリーTBチェック
function chkBSqlTypeFDB2CSV1($db2con, $FDB2CSV1)
{
    $rs         = true;
    $dataresult = array();
    //FDB2CSV1
    if ($rs === true) {
        $dbrs = chkFDB2CSV1($db2con, $FDB2CSV1);
        if ($dbrs['result'] !== true) {
            $rs  = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array(
        'result' => $rs,
        'msg' => $msg
    );
    return $dataresult;
}