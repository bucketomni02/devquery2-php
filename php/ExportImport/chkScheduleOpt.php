<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../common/inc/common.validation.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/

//DB2WSCD
function chkDB2WSCD($db2con, $DB2WSCD) {
    $rs = true;
    $data = array();
    foreach ($DB2WSCD as $res) {
        //クエリーとピボットIDチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['WSNAME'], $res['WSPKEY']);
            if ($chkQry['result'] === 'NOTEXIST_PIV') {
                $rs = 'NOTEXIST_GET';
                $msg = showMsg('NOTEXIST_GET', array('DB2WSCDのWSPKEY'));
                break;
            } else if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2WSCDのWSNAME'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WSNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WSNAME'] !== '' && $res['WSNAME'] !== null) {
                $byte = checkByte($res['WSNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WSCDのWSNAME'));
                    break;
                }
            }
        }
        //ピボットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WSPKEY'], 14)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSPKEY'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WSPKEY'] !== '' && $res['WSPKEY'] !== null) {
                $byte = checkByte($res['WSPKEY']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WSCDのWSPKEY'));
                    break;
                }
            }
        }
        //実行頻度チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WSFRQ'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSFRQ'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WSFRQ'] !== '' && $res['WSFRQ'] !== null) {
                $byte = checkByte($res['WSFRQ']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WSCDのWSFRQ'));
                    break;
                }
            }
        }
        //スケジュール-日チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WSODAY'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WSCDのWSODAY', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WSODAY'], 8)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSODAY'));
                break;
            }
        }
        //スケジュール-週チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WSWDAY'], 7)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSWDAY'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WSWDAY'] !== '' && $res['WSWDAY'] !== null) {
                $byte = checkByte($res['WSWDAY']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WSCDのWSWDAY'));
                    break;
                }
            }
        }
        //スケジュール-月チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WSMDAY'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WSCDのWSMDAY', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WSMDAY'], 2)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSMDAY'));
                break;
            }
        }
        //実行時分チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WSTIME'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WSCDのWSTIME', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WSTIME'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSTIME'));
                break;
            }
        }
        //サスペンドチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WSSPND'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSSPND'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WSSPND'] !== '' && $res['WSSPND'] !== null) {
                $byte = checkByte($res['WSSPND']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WSCDのWSSPND'));
                    break;
                }
            }
        }
        //実行日付チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WSBDAY'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WSCDのWSBDAY', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WSBDAY'], 8)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSBDAY'));
                break;
            }
        }
        //実行時分チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WSBTIM'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WSCDのWSBTIM', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WSBTIM'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSBTIM'));
                break;
            }
        }
        //S実行日付チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WSSDAY'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WSCDのWSSDAY', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WSSDAY'], 8)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSSDAY'));
                break;
            }
        }
        //S実行時分チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WSSTIM'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WSCDのWSSTIM', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WSSTIM'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSCDのWSSTIM'));
                break;
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//DB2WAUL
function chkDB2WAUL($db2con, $DB2WAUL) {
    $rs = true;
    $data = array();
    foreach ($DB2WAUL as $res) {
        //クエリーとピボットチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['WLNAME'], $res['WLPKEY']);
            if ($chkQry['result'] === 'NOTEXIST_PIV') {
                $rs = 'NOTEXIST_GET';
                $msg = showMsg('NOTEXIST_GET', array('DB2WAULのWLPKEY'));
                break;
            } else if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2WAULのWLNAME'));
                break;
            }
        }
        //定義名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WLNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAULのWLNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WLNAME'] !== '' && $res['WLNAME'] !== null) {
                $byte = checkByte($res['WLNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAULのWLNAME'));
                    break;
                }
            }
        }
        //ピボットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WLPKEY'], 14)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAULのWLPKEY'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WLPKEY'] !== '' && $res['WLPKEY'] !== null) {
                $byte = checkByte($res['WLPKEY']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAULのWLPKEY'));
                    break;
                }
            }
        }
        //宛先メールチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WLMAIL'], 256)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAULのWLMAIL'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WLMAIL'] !== '' && $res['WLMAIL'] !== null) {
                $byte = checkByte($res['WLMAIL']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAULのWLMAIL'));
                    break;
                }
            }
        }
        //LISTSEQチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WLSEQL'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WAULのWLSEQL', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WLSEQL'], 5)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAULのWLSEQL'));
                break;
            }
        }
        //選択するカラムチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WLDATA'], 256)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAULのWLDATA'));
                break;
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//DB2WAUT
function chkDB2WAUT($db2con, $DB2WAUT) {
    $rs = true;
    $data = array();
    foreach ($DB2WAUT as $res) {
        //クエリーとピボットチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['WANAME'], $res['WAPKEY']);
            if ($chkQry['result'] === 'NOTEXIST_PIV') {
                $rs = 'NOTEXIST_GET';
                $msg = showMsg('NOTEXIST_GET', array('DB2WAUTのWAPKEY'));
                break;
            } else if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2WAUTのWANAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WANAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWANAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WANAME'] !== '' && $res['WANAME'] !== null) {
                $byte = checkByte($res['WANAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWANAME'));
                    break;
                }
            }
        }
        //ピボットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WAPKEY'], 14)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWAPKEY'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WAPKEY'] !== '' && $res['WAPKEY'] !== null) {
                $byte = checkByte($res['WAPKEY']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWAPKEY'));
                    break;
                }
            }
        }
        //宛先メールアドレスチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WAMAIL'], 256)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWAMAIL'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WAMAIL'] !== '' && $res['WAMAIL'] !== null) {
                $byte = checkByte($res['WAMAIL']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWAMAIL'));
                    break;
                }
            }
        }
        //宛先メールCCチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WAMACC'], 256)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWAMACC'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WAMACC'] !== '' && $res['WAMACC'] !== null) {
                $byte = checkByte($res['WAMACC']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWAMACC'));
                    break;
                }
            }
        }
        //ＣＳＶ送付チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WACSV'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWACSV'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WACSV'] !== '' && $res['WACSV'] !== null) {
                $byte = checkByte($res['WACSV']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWACSV'));
                    break;
                }
            }
        }
        //ＥＸＣＥＬ送付チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WAXLS'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWAXLS'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WAXLS'] !== '' && $res['WAXLS'] !== null) {
                $byte = checkByte($res['WAXLS']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWAXLS'));
                    break;
                }
            }
        }
        //HTML送付チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WAHTML'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWAHTML'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WAHTML'] !== '' && $res['WAHTML'] !== null) {
                $byte = checkByte($res['WAHTML']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWAHTML'));
                    break;
                }
            }
        }
        //サスペンドチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WASPND'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWASPND'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WASPND'] !== '' && $res['WASPND'] !== null) {
                $byte = checkByte($res['WASPND']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWASPND'));
                    break;
                }
            }
        }
        //個別内容フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WASFLG'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWASFLG'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WASFLG'] !== '' && $res['WASFLG'] !== null) {
                $byte = checkByte($res['WASFLG']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWASFLG'));
                    break;
                }
            }
        }
        //差出人アドレスチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WAFRAD'], 256)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWAFRAD'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WAFRAD'] !== '' && $res['WAFRAD'] !== null) {
                $byte = checkByte($res['WAFRAD']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWAFRAD'));
                    break;
                }
            }
        }
        //差出人名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WAFRNM'], 128)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWAFRNM'));
                break;
            }
        }
        //メールタイトルチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WASUBJ'], 128)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWASUBJ'));
                break;
            }
        }
        //本文チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WABODY'], 1536)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWABODY'));
                break;
            }
        }
        //送信区分チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WASNKB'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWASNKB'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WASNKB'] !== '' && $res['WASNKB'] !== null) {
                $byte = checkByte($res['WASNKB']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWASNKB'));
                    break;
                }
            }
        }
        //メール形式チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WATYPE'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWATYPE'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WATYPE'] !== '' && $res['WATYPE'] !== null) {
                $byte = checkByte($res['WATYPE']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWATYPE'));
                    break;
                }
            }
        }
        //バーストFILIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WABAFID'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WAUTのWABAFID', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WABAFID'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWABAFID'));
                break;
            }
        }
        //バーストFLDチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WABAFLD'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWABAFLD'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WABAFLD'] !== '' && $res['WABAFLD'] !== null) {
                $byte = checkByte($res['WABAFLD']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWABAFLD'));
                    break;
                }
            }
        }
        //バーストFROMチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WABAFR'], 256)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWABAFR'));
                break;
            }
        }
        //バーストTOチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WABATO'], 256)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWABATO'));
                break;
            }
        }
        //抽出フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['WABFLG'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWABFLG'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['WABFLG'] !== '' && $res['WABFLG'] !== null) {
                $byte = checkByte($res['WABFLG']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WAUTのWABFLG'));
                    break;
                }
            }
        }
        //リストGKEYチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WABINC'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WAUTのWABINC', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WABINC'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWABINC'));
                break;
            }
        }
        //クエリ名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['WAQUNM'], 100)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWAQUNM'));
                break;
            }
        }
        //メール形式FONTSIZEチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['WAFONT'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WAUTのWAFONT', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['WAFONT'], 2)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WAUTのWAFONT'));
                break;
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//DB2WSOC
function chkDB2WSOC($db2con, $DB2WSOC) {
    $rs = true;
    $data = array();
    foreach ($DB2WSOC as $res) {
        //クエリーとピボットチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['SONAME'], $res['SOPKEY']);
            if ($chkQry['result'] === 'NOTEXIST_PIV') {
                $rs = 'NOTEXIST_GET';
                $msg = showMsg('NOTEXIST_GET', array('DB2WSOCのSOPKEY'));
                break;
            } else if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2WSOCのSONAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['SONAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSOCのSONAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['SONAME'] !== '' && $res['SONAME'] !== null) {
                $byte = checkByte($res['SONAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WSOCのSONAME'));
                    break;
                }
            }
        }
        //ピボットチェック
        if ($rs === true) {
            if (!checkMaxLen($res['SOPKEY'], 14)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSOCのSOPKEY'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['SOPKEY'] !== '' && $res['SOPKEY'] !== null) {
                $byte = checkByte($res['SOPKEY']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WSOCのSOPKEY'));
                    break;
                }
            }
        }
        //件メールフラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['SO0MAL'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WSOCのSO0MAL'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['SO0MAL'] !== '' && $res['SO0MAL'] !== null) {
                $byte = checkByte($res['SO0MAL']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WSOCのSO0MAL'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
// Scheduleチェック
function chkBSqlTypeScheduleOpt($db2con, $DB2WSCD, $DB2WAUT, $DB2WAUL, $DB2WSOC) {
    $rs = true;
    $dataresult = array();
    //DB2WSCD
    if ($rs === true) {
        $dbrs = chkDB2WSCD($db2con, $DB2WSCD);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //DB2WAUT
    if ($rs === true) {
        $dbrs = chkDB2WAUT($db2con, $DB2WAUT);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //DB2WAUL
    if ($rs === true) {
        $dbrs = chkDB2WAUL($db2con, $DB2WAUL);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //DB2WSOC
    if ($rs === true) {
        $dbrs = chkDB2WSOC($db2con, $DB2WSOC);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array('result' => $rs, 'msg' => $msg);
    return $dataresult;
}
