<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/

//DB2COLM
function chkDB2COLM($db2con, $DB2COLM) {
    $rs = true;
    $data = array();
    foreach ($DB2COLM as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['DCNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2COLMのDCNAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DCNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DCNAME'] !== '' && $res['DCNAME'] !== null) {
                $byte = checkByte($res['DCNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLMのDCNAME'));
                    break;
                }
            }
        }
        //フィールド1チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DCFILID1'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2COLMのDCFILID1', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DCFILID1'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFILID1'));
                break;
            }
        }
        //フィールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DCFLD1'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFLD1'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DCFLD1'] !== '' && $res['DCFLD1'] !== null) {
                $byte = checkByte($res['DCFLD1']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLMのDCFLD1'));
                    break;
                }
            }
        }
        //フィールド2チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DCFILID2'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2COLMのDCFILID2', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DCFILID2'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFILID2'));
                break;
            }
        }
        //フイールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DCFLD2'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFLD2'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DCFLD2'] !== '' && $res['DCFLD2'] !== null) {
                $byte = checkByte($res['DCFLD2']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLMのDCFLD2'));
                    break;
                }
            }
        }
        //ダウンロード可否１チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DCFILID3'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2COLMのDCFILID3', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DCFILID3'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFILID3'));
                break;
            }
        }
        //ダウンロード可否2チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DCFLD3'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFLD3'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DCFLD3'] !== '' && $res['DCFLD3'] !== null) {
                $byte = checkByte($res['DCFLD3']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLMのDCFLD3'));
                    break;
                }
            }
        }
        //フィールド4チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DCFILID4'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2COLMのDCFILID4', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DCFILID4'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFILID4'));
                break;
            }
        }
        //フイールド名4チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DCFLD4'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFLD4'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DCFLD4'] !== '' && $res['DCFLD4'] !== null) {
                $byte = checkByte($res['DCFLD4']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLMのDCFLD4'));
                    break;
                }
            }
        }
        //フィールド5チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DCFILID5'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2COLMのDCFILID5', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DCFILID5'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFILID5'));
                break;
            }
        }
        //フイールド名5チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DCFLD5'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFLD5'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DCFLD5'] !== '' && $res['DCFLD5'] !== null) {
                $byte = checkByte($res['DCFLD5']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLMのDCFLD5'));
                    break;
                }
            }
        }
        //フィールド6チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DCFILID6'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2COLMのDCFILID6', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DCFILID6'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFILID6'));
                break;
            }
        }
        //フイールド名6チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DCFLD6'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLMのDCFLD6'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DCFLD6'] !== '' && $res['DCFLD6'] !== null) {
                $byte = checkByte($res['DCFLD6']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLMのDCFLD6'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//DB2COLT
function chkDB2COLT($db2con, $DB2COLT) {
    $rs = true;
    $data = array();
    foreach ($DB2COLT as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['DTNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2COLTのDTNAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLTのDTNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTNAME'] !== '' && $res['DTNAME'] !== null) {
                $byte = checkByte($res['DTNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLTのDTNAME'));
                    break;
                }
            }
        }
        //SEQチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DTSEQ'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2COLTのDTSEQ', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DTSEQ'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLTのDTSEQ'));
                break;
            }
        }
        //フイールドチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DTFILID'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2COLTのDTFILID', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DTFILID'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLTのDTFILID'));
                break;
            }
        }
        //フイールド名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTFLD'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLTのDTFLD'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTFLD'] !== '' && $res['DTFLD'] !== null) {
                $byte = checkByte($res['DTFLD']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLTのDTFLD'));
                    break;
                }
            }
        }
        //合計チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTSUM'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLTのDTSUM'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTSUM'] !== '' && $res['DTSUM'] !== null) {
                $byte = checkByte($res['DTSUM']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLTのDTSUM'));
                    break;
                }
            }
        }
        //ダウンロード可否１チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTAVLG'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLTのDTAVLG'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTAVLG'] !== '' && $res['DTAVLG'] !== null) {
                $byte = checkByte($res['DTAVLG']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLTのDTAVLG'));
                    break;
                }
            }
        }
        //ダウンロード可否２チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTMINV'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLTのDTMINV'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTMINV'] !== '' && $res['DTMINV'] !== null) {
                $byte = checkByte($res['DTMINV']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLTのDTMINV'));
                    break;
                }
            }
        }
        //最大チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTMAXV'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLTのDTMAXV'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTMAXV'] !== '' && $res['DTMAXV'] !== null) {
                $byte = checkByte($res['DTMAXV']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLTのDTMAXV'));
                    break;
                }
            }
        }
        //カウントチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTCONT'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2COLTのDTCONT'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTCONT'] !== '' && $res['DTCONT'] !== null) {
                $byte = checkByte($res['DTCONT']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2COLTのDTCONT'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
// 制御チェック
function chkBSqlTypeSeigyoOpt($db2con, $DB2COLM, $DB2COLT) {
    $rs = true;
    $dataresult = array();
    //DB2COLM
    if ($rs === true) {
        $dbrs = chkDB2COLM($db2con, $DB2COLM);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //DB2COLT
    if ($rs === true) {
        $dbrs = chkDB2COLT($db2con, $DB2COLT);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array('result' => $rs, 'msg' => $msg);
    return $dataresult;
}
