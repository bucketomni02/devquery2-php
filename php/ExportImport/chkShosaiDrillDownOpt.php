<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../common/inc/common.validation.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/

//DB2WDTL
function chkDB2WDTL($db2con, $DB2WDTL) {
    $rs = true;
    $data = array();
    foreach ($DB2WDTL as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['DTNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2WDTLのDTNAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDTLのDTNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTNAME'] !== '' && $res['DTNAME'] !== null) {
                $byte = checkByte($res['DTNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDTLのDTNAME'));
                    break;
                }
            }
        }
        //FILIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DTFILID'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WDTLのDTFILID', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DTFILID'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDTLのDTFILID'));
                break;
            }
        }
        //FLDチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTFLD'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDTLのDTFLD'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTFLD'] !== '' && $res['DTFLD'] !== null) {
                $byte = checkByte($res['DTFLD']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDTLのDTFLD'));
                    break;
                }
            }
        }
        //ライブラリチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTLIBL'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDTLのDTLIBL'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTLIBL'] !== '' && $res['DTLIBL'] !== null) {
                $byte = checkByte($res['DTLIBL']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDTLのDTLIBL'));
                    break;
                }
            }
        }
        //ファイルチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTFILE'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDTLのDTFILE'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTFILE'] !== '' && $res['DTFILE'] !== null) {
                $byte = checkByte($res['DTFILE']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDTLのDTFILE'));
                    break;
                }
            }
        }
        //メンバーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DTMBR'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDTLのDTMBR'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DTMBR'] !== '' && $res['DTMBR'] !== null) {
                $byte = checkByte($res['DTMBR']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDTLのDTMBR'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//DB2WDFL
function chkDB2WDFL($db2con, $DB2WDFL) {
    $rs = true;
    $data = array();
    foreach ($DB2WDFL as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['DFNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2WDFLのDFNAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DFNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDFLのDFNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DFNAME'] !== '' && $res['DFNAME'] !== null) {
                $byte = checkByte($res['DFNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDFLのDFNAME'));
                    break;
                }
            }
        }
        //FILIDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DFFILID'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2WDFLのDFFILID', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DFFILID'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDFLのDFFILID'));
                break;
            }
        }
        //FLDチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DFFLD'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDFLのDFFLD'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DFFLD'] !== '' && $res['DFFLD'] !== null) {
                $byte = checkByte($res['DFFLD']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDFLのDFFLD'));
                    break;
                }
            }
        }
        //カラムチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DFCOLM'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDFLのDFCOLM'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DFCOLM'] !== '' && $res['DFCOLM'] !== null) {
                $byte = checkByte($res['DFCOLM']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDFLのDFCOLM'));
                    break;
                }
            }
        }
        //ラジオキーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DFFKEY'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDFLのDFFKEY'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DFFKEY'] !== '' && $res['DFFKEY'] !== null) {
                $byte = checkByte($res['DFFKEY']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDFLのDFFKEY'));
                    break;
                }
            }
        }
        //チェックキーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DFCHECK'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2WDFLのDFCHECK'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DFCHECK'] !== '' && $res['DFCHECK'] !== null) {
                $byte = checkByte($res['DFCHECK']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2WDFLのDFCHECK'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//DB2DRGS
function chkDB2DRGS($db2con, $DB2DRGS) {
    $rs = true;
    $data = array();
    foreach ($DB2DRGS as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['DRKMTN'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2DRGSのDRKMTN'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DRKMTN'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2DRGSのDRKMTN'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DRKMTN'] !== '' && $res['DRKMTN'] !== null) {
                $byte = checkByte($res['DRKMTN']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2DRGSのDRKMTN'));
                    break;
                }
            }
        }
        //クエリーCOLIDチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DRKFID'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2DRGSのDRKFID'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DRKFID'] !== '' && $res['DRKFID'] !== null) {
                $byte = checkByte($res['DRKFID']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2DRGSのDRKFID'));
                    break;
                }
            }
        }
        //クエリーCOLSEQチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DRKFLID'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2DRGSのDRKFLID'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DRKFLID'] !== '' && $res['DRKFLID'] !== null) {
                $byte = checkByte($res['DRKFLID']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2DRGSのDRKFLID'));
                    break;
                }
            }
        }
        //検索クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DRDMTN'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2DRGSのDRDMTN'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DRDMTN'] !== '' && $res['DRDMTN'] !== null) {
                $byte = checkByte($res['DRDMTN']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2DRGSのDRDMTN'));
                    break;
                }
            }
        }
        //検索COL名チェック
        if ($rs === true) {
            if (!checkMaxLen($res['DRDFNM'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2DRGSのDRDFNM'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DRDFNM'] !== '' && $res['DRDFNM'] !== null) {
                $byte = checkByte($res['DRDFNM']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2DRGSのDRDFNM'));
                    break;
                }
            }
        }
        //検索COL IDチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DRDFID'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2DRGSのDRDFID', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DRDFID'], 4)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2DRGSのDRDFID'));
                break;
            }
        }
        //カラムソートチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DRDJSQ'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2DRGSのDRDJSQ', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DRDJSQ'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2DRGSのDRDJSQ'));
                break;
            }
        }
        //カラムの合計チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['DRDCNT'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2DRGSのDRDCNT', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['DRDCNT'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2DRGSのDRDCNT'));
                break;
            }
        }
        //DRILL WEB FLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DRWEBF'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2DRGSのDRWEBF'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DRWEBF'] !== '' && $res['DRWEBF'] !== null) {
                $byte = checkByte($res['DRWEBF']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2DRGSのDRWEBF'));
                    break;
                }
            }
        }
        //検索WEB FLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['DRWEBK'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2DRGSのDRWEBK'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['DRWEBK'] !== '' && $res['DRWEBK'] !== null) {
                $byte = checkByte($res['DRWEBK']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2DRGSのDRWEBK'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
// 詳細とドリルダウンチェック
function chkBSqlTypeShosaiDrillOpt($db2con, $DB2WDTL, $DB2WDFL, $DB2DRGS) {
    $rs = true;
    $dataresult = array();
    //DB2WDTL
    if ($rs === true) {
        $dbrs = chkDB2WDTL($db2con, $DB2WDTL);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //DB2WDFL
    if ($rs === true) {
        $dbrs = chkDB2WDFL($db2con, $DB2WDFL);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //DB2DRGS
    if ($rs === true) {
        $dbrs = chkDB2DRGS($db2con, $DB2DRGS);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array('result' => $rs, 'msg' => $msg);
    return $dataresult;
}
