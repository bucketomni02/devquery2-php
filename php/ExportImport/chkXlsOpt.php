<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/

//DB2ECON
function chkDB2ECON($db2con, $DB2ECON) {
    $rs = true;
    $data = array();
    foreach ($DB2ECON as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['ECNAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2ECONのECNAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['ECNAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2ECONのECNAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['ECNAME'] !== '' && $res['ECNAME'] !== null) {
                $byte = checkByte($res['ECNAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2ECONのECNAME'));
                    break;
                }
            }
        }
        //抽出条件フラグチェック
        if ($rs === true) {
            if (!checkMaxLen($res['ECFLG'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2ECONのECFLG'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['ECFLG'] !== '' && $res['ECFLG'] !== null) {
                $byte = checkByte($res['ECFLG']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2ECONのECFLG'));
                    break;
                }
            }
        }
        //結果データ行チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['ECROWN'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2ECONのECROWN', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['ECROWN'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2ECONのECROWN'));
                break;
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
//DB2EINS
function chkDB2EINS($db2con, $DB2EINS) {
    $rs = true;
    $data = array();
    foreach ($DB2EINS as $res) {
        //クエリーチェック
        if ($rs === true) {
            $chkQry = array();
            $chkQry = cmChkQuery($db2con, '', $res['EINAME'], '', '');
            if ($chkQry['result'] !== true) {
                $rs = $chkQry['result'];
                $msg = showMsg($chkQry['result'], array('DB2EINSのEINAME'));
                break;
            }
        }
        //クエリーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['EINAME'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2EINSのEINAME'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['EINAME'] !== '' && $res['EINAME'] !== null) {
                $byte = checkByte($res['EINAME']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2EINSのEINAME'));
                    break;
                }
            }
        }
        //SEQチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['EISEQ'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2EINSのEISEQ', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['EISEQ'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2EINSのEISEQ'));
                break;
            }
        }
        //EXCEL列チェック
        if ($rs === true) {
            if (!checkMaxLen($res['EICOL'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2EINSのEICOL'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['EICOL'] !== '' && $res['EICOL'] !== null) {
                $byte = checkByte($res['EICOL']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2EINSのEICOL'));
                    break;
                }
            }
        }
        //EXCEL行チェック
        if ($rs === true) {
            if (!checkNuturalNum($res['EIROW'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2EINSのEIROW', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['EIROW'], 10)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2EINSのEIROW'));
                break;
            }
        }
        //文言チェック
        if ($rs === true) {
            if (!checkMaxLen($res['EITEXT'], 256)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2EINSのEITEXT'));
                break;
            }
        }
        //文字サイズPXチェック
        if ($rs === true) {
            if (!checkNuturalNum($res['EISIZE'])) {
                $rs = 'CHK_FMT';
                $msg = showMsg('CHK_FMT', array('DB2EINSのEISIZE', '数値'));
                break;
            }
        }
        if ($rs === true) {
            if (!checkMaxLen($res['EISIZE'], 3)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2EINSのEISIZE'));
                break;
            }
        }
        //カラーチェック
        if ($rs === true) {
            if (!checkMaxLen($res['EICOLR'], 7)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2EINSのEICOLR'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['EICOLR'] !== '' && $res['EICOLR'] !== null) {
                $byte = checkByte($res['EICOLR']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2EINSのEICOLR'));
                    break;
                }
            }
        }
        //BOLDFLGチェック
        if ($rs === true) {
            if (!checkMaxLen($res['EIBOLD'], 1)) {
                $rs = 'FAIL_MAXLEN';
                $msg = showMsg('FAIL_MAXLEN', array('DB2EINSのEIBOLD'));
                break;
            }
        }
        if ($rs === true) {
            if ($res['EIBOLD'] !== '' && $res['EIBOLD'] !== null) {
                $byte = checkByte($res['EIBOLD']);
                if ($byte[1] > 0) {
                    $rs = 'FAIL_BYTE';
                    $msg = showMsg('FAIL_BYTE', array('DB2EINSのEIBOLD'));
                    break;
                }
            }
        }
    }
    $data = array('result' => $rs, 'msg' => $msg);
    return $data;
}
// ｘｌｓチェック
function chkBSqlTypeXlsOpt($db2con, $DB2ECON, $DB2EINS) {
    $rs = true;
    $dataresult = array();
    //DB2ECON
    if ($rs === true) {
        $dbrs = chkDB2ECON($db2con, $DB2ECON);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    //DB2EINS
    if ($rs === true) {
        $dbrs = chkDB2EINS($db2con, $DB2EINS);
        if ($dbrs['result'] !== true) {
            $rs = $dbrs['result'];
            $msg = $dbrs['msg'];
        }
    }
    $dataresult = array('result' => $rs, 'msg' => $msg);
    return $dataresult;
}
