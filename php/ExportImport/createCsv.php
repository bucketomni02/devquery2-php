<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
require_once BASE_DIR.'/php/common/lib/mb_str_replace/mb_str_replace.php';
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$KENGEN=(isset($_POST['KENGEN'])?$_POST['KENGEN']:'');
$time=(isset($_POST['time'])?$_POST['time']:'');

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$datatest=array();
$wfname='';
$sfname='';
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
          
            $rs = cmChkKenGen($db2con,'32',$userData[0]['WUSAUT']);//'32' => Export
       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('エクスポートの権限'));
            }
        }
    }
}
if($rtn===0){
    if($KENGEN==='0'){//ユーザ権限
        $wfname='user'.$time;
        $sfname='ユーザー権限';
        $data=fnGetDB2WUSR($db2con);
        $datatest=$data;
        if($data['result'] !== true){
            $rtn = 1;
            $msg = showMsg($data['result']);
        }
    }else if($KENGEN==='1'){//グループ権限
        $wfname='group'.$time;
        $sfname='グループ権限';
        $data=fnGetDB2WUGR($db2con);
        if($data['result'] !== true){
            $rtn = 1;
            $msg = showMsg($data['result']);
        }
    }else if($KENGEN==='2'){//クエリー権限
        $wfname='query'.$time;
        $sfname='クエリー権限';
        $data=fnGetDB2WDEF($db2con);
        if($data['result'] !== true){
            $rtn = 1;
            $msg = showMsg($data['result']);
        }
    }
}

//CSV作成処理
if($rtn === 0){
    $data = umEx($data['data']);
    $fp = fopen(TEMP_DIR.$wfname.'.csv', 'w');
    foreach($data as $row){
        $csv = '';
        $rowcount=count($row);
        $innercount=0;
        foreach($row as $key => $value){
            //ユーザー権限だけ
            if($key === 'WUEDAY'){
                if(strlen($value) === 8){
                    $tmp = $value;
                    $value = substr($tmp,0,4).'/'.substr($tmp,4,2).'/'.substr($tmp,6,2);
                }
            }
            $value = mb_convert_encoding($value, 'sjis-win', 'UTF-8');
            $value = mb_str_replace('"', '""', $value, 'sjis-win');
            ++$innercount;
            if($rowcount>$innercount){//check last no by each row
                $csv .= '"'.$value.'",';
            }else{
                $csv .= '"'.$value.'"';
            }
        }
        fwrite($fp,$csv);
        fwrite($fp,"\r\n");
    }
    fclose($fp);
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'data' => $data,
    'wfname'=>$wfname,
    'sfname'=>$sfname
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザー取得
*-------------------------------------------------------*
*/

function fnGetDB2WUSR($db2con){
    $rs=true;
    $data = array();
    $params = array();

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';
    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SEL';
        }
    }
    $titleArray=array('ユーザID','ユーザー名','ActiveDirectoryユーザー','ユーザ権限','CSVダウンロード権限','EXCELダウンロード権限','メインカラー',
                    'カラー2','見出しカラー','カラー4','カラー5','実行回数','パスワード','サイズ',
                    '実行日付','権限','言語','標準ダウンロード先','タイムスタンプ','ライブラリー','作成したクエリーメニュー');
    //Gigi add ActiveDirectoryユーザー
    /* $titleArray=array('ユーザID','ユーザー名','ユーザ権限','CSVダウンロード権限','EXCELダウンロード権限','メインカラー',
                    'カラー2','見出しカラー','カラー4','カラー5','実行回数','パスワード','サイズ',
                    '実行日付','権限','言語','標準ダウンロード先','タイムスタンプ','ライブラリー','作成したクエリーメニュー');*/
    $data[]=$titleArray;
    if($rs=true){
		$strSQL  = ' SELECT B.WUUID,B.WUUNAM,B.WUAID,B.WUAUTH,B.WUDWNL,B.WUDWN2,B.WUCLR1, '; //Gigi add B.WUAID
        /*$strSQL  = ' SELECT B.WUUID,B.WUUNAM,B.WUAUTH,B.WUDWNL,B.WUDWN2,B.WUCLR1, '; */
        $strSQL .= ' B.WUCLR2,B.WUCLR3,B.WUCLR4,B.WUCLR5,B.WULANG,DECRYPT_CHAR(B.WUPSWE) AS WUPSWE ,B.WUSIZE, ';
        $strSQL .= ' B.WUEDAY,B.WULGNC,B.WUSAUT,B.WUSERV,B.WUCTFL,B.WULIBL,B.WUUQFG ';
        $strSQL .= ' FROM DB2WUSR as B ';
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs='FAIL_SEL';
        }else{
            $r = db2_execute($stmt);
            if($r === false){
                $rs='FAIL_SEL';
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
            }
        }
    }
    return array('result'=>$rs,'data'=>$data);

}
/*
*-------------------------------------------------------* 
* グループ取得
*-------------------------------------------------------*
*/
function fnGetDB2WUGR($db2con){
    $rs=true;
    $data = array();
    $params = array();
    $titleArray=array('ユーザID','グループID');
    $data[]=$titleArray;
    $strSQL  = ' SELECT B.WUUID,B.WUGID  ';
    $strSQL .= ' FROM DB2WUGR as B ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs='FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs='FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return array('result'=>$rs,'data'=>$data);

}
/*
*-------------------------------------------------------* 
* クエリー取得
*-------------------------------------------------------*
*/

function fnGetDB2WDEF($db2con){
    $rs=true;
    $data = array();
    $params = array();
    /*$titleArray=array('ユーザID','グループID','CSVダウンロード権限','EXCELダウンロード権限','クエリーダウンロード先','タイムスタンプ');*/
    $titleArray=array('ユーザID','グループID','CSVダウンロード権限','EXCELダウンロード権限','クエリーダウンロード先','タイムスタンプ','ダウンロード先名');//GIGI ADD ダウンロード先名
	$data[]=$titleArray;
    /* $strSQL  = ' SELECT B.WDUID,B.WDNAME,B.WDDWNL,B.WDDWN2,B.WDSERV,B.WDCTFL';*/
    $strSQL  = ' SELECT B.WDUID,B.WDNAME,B.WDDWNL,B.WDDWN2,B.WDSERV,B.WDCTFL,B.WDCNFL';//GIGI ADD WDCNFL
    $strSQL .= ' FROM DB2WDEF as B ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs='FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs='FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return array('result'=>$rs,'data'=>$data);

}
