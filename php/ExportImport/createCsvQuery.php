<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("getQryTblData.php");
require_once BASE_DIR.'/php/common/lib/mb_str_replace/mb_str_replace.php';
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$dataList=(isset($_POST['DATALIST'])?json_decode($_POST['DATALIST'],true):array());

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$datatest=array();
$wfname='';
$sfname='';
$rtn = 0;
$msg = '';
$csvName='クエリー情報.csv';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'32',$userData[0]['WUSAUT']);//'32' => Export
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('エクスポートの権限'));
            }
        }
    }
}
if($rtn===0){
    if(count($dataList)>0){
        foreach($dataList as $key=> $dataRes){
            if($dataRes['value']===true){
                if($dataRes['d1webf']===''){//黒い画面5250
                    $r=fnGetKuroiData($db2con,$dataRes['field']);
                    if($r['result']!==true){
                        $rtn=1;
                        $msg=showMsg($r['result']);
                        break;
                    }else{
                        $data[]=$r['data'];
                    }
                }else{
                    if($dataRes['d1cflg']==='1'){//SQLクエリー
                        $r=fnGetSqlData($db2con,$dataRes['field']);
                        if($r['result']!==true){
                            $rtn=1;
                            $msg=showMsg($r['result']);
                            break;
                        }else{
                            $data[]=$r['data'];
                        }

                    }else{//普通のクエリー
                        $r=fnGetQryData($db2con,$dataRes['field']);
                        if($r['result']!==true){
                            $rtn=1;
                            $msg=showMsg($r['result']);
                            break;
                        }else{
                            $data[]=$r['data'];
                        }
                    }
                }
            }
        }
    }else{
        $rtn=1;
        $msg = showMsg('CHK_SEL',array('クエリー'));
    }
    
}
//CSV作成処理
if($rtn===0){
    $fp = fopen(TEMP_DIR.$csvName, 'w');
    foreach($data as $datainner){
        foreach($datainner as $row){
            $csv = '';
            $rowcount=count($row);
            $innercount=0;
            foreach($row as $key => $value){
                $value = mb_convert_encoding($value, 'sjis-win', 'UTF-8');
                $value = mb_str_replace('"', '""', $value, 'sjis-win');
                ++$innercount;
                if($rowcount>$innercount){//check last no by each row
                    $csv .= '"'.$value.'",';
                    //$csv .= '"'.$value.'"'."\t";
                }else{
                    $csv .= '"'.$value.'"';
                }
            }
            fwrite($fp,$csv);
            fwrite($fp,"\r\n");
        }
    }
    fclose($fp);
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'data' => $data,
    'csvName'=>$csvName
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 黒い画面5250取得
*-------------------------------------------------------*
*/
function fnGetKuroiData($db2con,$d1name){
    $rs=true;
    $msg='';
    $data=[];
    $fdb2csv1=array();
    //FDB2CSV1⇒クエリーTB
    $r=getFDB2CSV1($db2con,$d1name);
    if($r['result']!==true){
        $rs= $r['result'];
        $msg = showMsg($r['errcd']);
    }else{
        $dataArr=$r['data'];
        foreach($dataArr as $res){
            $data[]=$res;
        }
    }
    //FDB2CSV2⇒カランTB
    if($rs===true){
        $r=getFDB2CSV2($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['result'];
            $msg = showMsg($r['errcd']);
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //FDB2CSV5⇒結果TB
    if($rs===true){
        $r=getFDB2CSV5($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['result'];
            $msg = showMsg($r['errcd']);
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //FDB2CSV4⇒ジョインTB
    if($rs===true){
        $r=getFDB2CSV4($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['result'];
            $msg = showMsg($r['errcd']);
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //FDB2CSV3⇒検索TB
    if($rs===true){
        $r=getFDB2CSV3($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['result'];
            $msg = showMsg($r['errcd']);
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //TBのオプションデータをもらう
    if($rs===true){
        $r=fnGetOptionData($db2con,$d1name);
        if($r['result'] !==true){
            $rs=$r['result'];
            $msg=$r['msg'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }        
    }
    return array('result'=>$rs,'msg'=>$msg,'data'=>$data);
}

/*
*-------------------------------------------------------* 
* 普通のクエリー取得
*-------------------------------------------------------*
*/
function fnGetQryData($db2con,$d1name){
    $rs=true;
    $data=[];
    $fdb2csv1=array();
    //FDB2CSV1⇒クエリーTB
    $r=getFDB2CSV1($db2con,$d1name);
    if($r['result']!==true){
        $rs= $r['errcd'];
    }else{
        $dataArr=$r['data'];
        foreach($dataArr as $res){
            $data[]=$res;
        }
    }
    //FDB2CSV2⇒カランTB
    if($rs===true){
        $r=getFDB2CSV2($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['errcd'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //FDB2CSV5⇒結果TB
    if($rs===true){
        $r=getFDB2CSV5($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['errcd'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //BSUMFLD=>結果TB
    if($rs===true){
        $r=getBSUMFLD($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['errcd'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //BREFTBL⇒ジョインTB
    if($rs===true){
        $r=getBREFTBL($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['errcd'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //BREFFLD⇒ジョインTB
    if($rs===true){
        $r=getBREFFLD($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['errcd'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //BQRYCND⇒検索TB
    if($rs===true){
        $r=getBQRYCND($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['errcd'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //BCNDDAT⇒検索TB
    if($rs===true){
        $r=getBCNDDAT($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['errcd'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //TBのオプションデータをもらう
    if($rs===true){
        $r=fnGetOptionData($db2con,$d1name);
        if($r['result'] !==true){
            $rs=$r['result'];
            $msg=$r['msg'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }        
    }

    return array('result'=>$rs,'data'=>$data);
}
/*
*-------------------------------------------------------* 
* 黒い画面5250取得
*-------------------------------------------------------*
*/
function fnGetSqlData($db2con,$d1name){
    $rs=true;
    $data=[];
    $fdb2csv1=array();
    //FDB2CSV1⇒クエリーTB
    $r=getFDB2CSV1($db2con,$d1name);
    if($r['result']!==true){
        $rs= $r['errcd'];
    }else{
        $dataArr=$r['data'];
        foreach($dataArr as $res){
            $data[]=$res;
        }
    }
    //FDB2CSV2⇒カランTB
    if($rs===true){
        $r=getFDB2CSV2($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['errcd'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //BSQLCND⇒検索TB
    if($rs===true){
        $r=getBSQLCND($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['errcd'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //BSQLDAT⇒検索TB
    if($rs===true){
        $r=getBSQLDAT($db2con,$d1name);
        if($r['result']!==true){
            $rs= $r['errcd'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }
    //TBのオプションデータをもらう
    if($rs===true){
        $r=fnGetOptionData($db2con,$d1name);
        if($r['result'] !==true){
            $rs=$r['result'];
            $msg=$r['msg'];
        }else{
            $dataArr=$r['data'];
            foreach($dataArr as $res){
                $data[]=$res;
            }
        }
    }

    return array('result'=>$rs,'data'=>$data);
}
/*
*-------------------------------------------------------* 
*別のライブラリーで使う
* 処理:選択したオプションのコピー呼び出し
*この機能はcheckOptionと違うのはFDB2CSV1とFDB2CSV2なら権限入れてない。
* パラメータ  ：①古いライブラリー名
*               :②クエリー名
*-------------------------------------------------------*
*/
function fnGetOptionData($db2con,$d1name){
    $rs=true;
    $msg='';
    $data=array();
    //TBのオプションデータをもらう
    $r = cmGetQryOptionInfo($db2con,$d1name);
    if($r['result'] !== true){
        $rs = $r['result'];
        $msg = showMsg($r['result']);
    }else{
        $dataOption = $r['data'];
        if(count($dataOption)>0){
            // グループと権限設定
            if($rs===true){
                if($dataOption[0]['A2']==="1"){
                    $r= fnGetDB2WGDF($db2con,$d1name);
                    if($r['result']!==true){
                        $rs= $r['result'];
                        $msg = showMsg($r['errcd']);
                    }else{
                        $dataArr=$r['data'];
                        foreach($dataArr as $res){
                            $data[]=$res;
                        }
                    }
                }
            }// グループと権限設定
            // HTMLテンプレート設定
            if($rs===true){
                if($dataOption[0]['A4']==="1"){
                    $r= fnGetHTMLTemp($db2con,$d1name);
                    if($r['result']!==true){
                        $rs= $r['result'];
                        $msg = showMsg($r['errcd']);
                    }else{
                        $dataArr=$r['data'];
                        foreach($dataArr as $res){
                            $data[]=$res;
                        }
                    }
                }
            }// HTMLテンプレート設定
            // EXCELテンプレート設定
            if($rs===true){
                if($dataOption[0]['A5']==="1"){
                    $r = fnGetXlsTemp($db2con,$d1name);
                    if($r['result'] !== true){
                        $rs = $r['result'];
                        $msg = showMsg($r['errcd']);
                    }else{
                        $XLSTemp = $r['data'];
                        if(count($XLSTemp)>0){
                            if(cmMer($XLSTemp[0]['D1EDKB'] === '2')){
                                if($rs === true){
                                    $r = fnGetXlsTemplateOpt($db2con,$d1name);
                                    if($r['result'] !== true){
                                        $rs = $r['result'];
                                        $msg = showMsg($r['errcd']);
                                    }else{
                                        $dataArr=$r['data'];
                                        foreach($dataArr as $res){
                                            $data[]=$res;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }// EXCELテンプレート設定
            // 詳細情報設定
            if($rs===true){
                if($dataOption[0]['A6']==="1"){
                    //shosai
                    $r = fnGetDB2WDTL($db2con,$d1name);
                    if($r['result'] !== true){
                        $rs = $r['result'];
                        $msg=showMsg($r['errcd']);
                    }else{
                        $dataArr=$r['data'];
                        foreach($dataArr as $res){
                            $data[]=$res;
                        }
                        $r=fnGetDB2WDFL($db2con,$d1name);
                        if($r['result'] !== true){
                            $rs = $r['result'];
                            $msg=showMsg($r['errcd']);
                        }else{
                            $dataArr=$r['data'];
                            foreach($dataArr as $res){
                                $data[]=$res;
                            }
                        }
                    }
                    //drilldown
                    if($rs===true){
                        $r = fnGetDB2DRGS($db2con,$d1name);
                        if($r['result'] !== true){
                            $rs = $r['result'];
                            $msg=showMsg($r['errcd']);
                        }else{
                            $dataArr=$r['data'];
                            foreach($dataArr as $res){
                                $data[]=$res;
                            }
                        }
                    }
                }
            }//end 詳細情報設定
            // ダウンロード権限設定
            if($rs===true){
                if($dataOption[0]['A7']==="1"){
                    $r = fnGetDwnKengenOpt($db2con,$d1name);
                    if($r['result'] !== true){
                        $rs = $r['result'];
                        $msg=showMsg($r['errcd']);
                    }else{
                        
                        $dataArr=$r['data'];
                        foreach($dataArr as $res){
                            $data[]=$res;
                        }
                    }
                }
            }// end ダウンロード権限設定
            // 制御レベール設定
            if($rs===true){
                if($dataOption[0]['B']==="1"){
                    $r = fnGetSeigyoOpt($db2con,$d1name);
                    if($r['result'] !== true){
                        $rs = $r['result'];
                        $msg=showMsg($r['errcd']);
                    }else{
                        $dataArr=$r['data'];
                        foreach($dataArr as $res){
                            $data[]=$res;
                        }
                    }
                }
            }// end 制御レベール設定
            // ピボット設定
            if($rs===true){
                if($dataOption[0]['C']==="1"){
                    $r = fnGetPivotOpt($db2con,$d1name);
                    if($r['result'] !== true){
                        $rs = $r['result'];
                        $msg=showMsg($r['errcd']);
                    }else{
                        $dataArr=$r['data'];
                        foreach($dataArr as $res){
                            $data[]=$res;
                        }
                    }
                }
            }// end ピボット設定
            // スケジュール設定
            if($rs===true){
                if($dataOption[0]['E']==="1"){
                    $r = fnGetScheduleOpt($db2con,$d1name);
                    if($r['result'] !== true){
                        $rs = $r['result'];
                        $msg=showMsg($r['errcd']);
                    }else{
                        $dataArr=$r['data'];
                        foreach($dataArr as $res){
                            $data[]=$res;
                        }
                    }
                }
            }// end スケジュール設定
            // CL連携設定
            if($rs===true){
                if($dataOption[0]['F']==="1"){
                    $r = fnGetCLSetOpt($db2con,$d1name);
                    if($r['result'] !== true){
                        $rs = $r['result'];
                        $msg=showMsg($r['errcd']);
                    }else{
                        $dataArr=$r['data'];
                        foreach($dataArr as $res){
                            $data[]=$res;
                        }
                    }
                }
            }// end CL連携設定
            // グラフ設定
            if($rs===true){
                if($dataOption[0]['G']==="1"){
                    $r = fnGetGraphOpt($db2con,$d1name);
                    if($r['result'] !==true){
                        $rs = $r['result'];
                        $msg=showMsg($r['errcd']);
                    }else{
                        $dataArr=$r['data'];
                        foreach($dataArr as $res){
                            $data[]=$res;
                        }
                    }
                }
            }// end グラフ設定
        }
    }//end of cmGetQry
    return array('result'=>$rs,'msg'=>$msg,'data'=>$data);
}

