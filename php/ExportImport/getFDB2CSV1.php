<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
//チェックがTRUEだけ、データ保存
$allCheckData=array();
$allcount=0;
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$sSearch = (isset($_POST['sSearch'])) ? $_POST['sSearch'] : '';
//全部のチェックのため、0⇒false,1⇒true,2⇒default
$allCheck = (isset($_POST['allCheck'])) ? $_POST['allCheck'] : '2';
$checkList=(isset($_POST['checkList'])?json_decode($_POST['checkList'],true):array());
$start = (isset($_POST['start'])) ? $_POST['start'] : '';
$length = (isset($_POST['length'])) ? $_POST['length'] : '';
$sort = (isset($_POST['sort'])) ? $_POST['sort'] : '';
$sortDir = (isset($_POST['sortDir'])) ? $_POST['sortDir'] : '';

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$authInfo = array();
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $auth = $rs['data'][0];
        $WUAUTH = cmMer($auth['WUAUTH']);
        if($WUAUTH === '2'){
            $SAUT = cmMer($auth['WUSAUT']);
            $rs = cmSelDB2AGRT($db2con,$SAUT);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg =  showMsg($rs['result']);
            }else{
                $authData = umEx($rs['data']);
                foreach($authData as $key => $value){
                        $authInfo[$value['AGMNID']] = $value['AGFLAG'];
                }
                if($authInfo['3'] !== '1' && $authInfo['6'] !== '1' && $authInfo['7'] !== '1'){
                    if($authInfo['8'] !== '1' && $authInfo['9'] !== '1' && $authInfo['10'] !== '1'){
                        if($authInfo['11'] !== '1' && $authInfo['12'] !== '1' && $authInfo['13'] !== '1'){
                            if($authInfo['14'] !== '1' && $authInfo['15'] !== '1' && $authInfo['16'] !== '1' && $authInfo['30'] !== '1'){
                                $rtn = 2;
                                 $msg =  showMsg('NOTEXIST',array('クエリー作成の権限'));
                            }
                        }
                    }
                }
            }
        }
    }
}
if($rtn === 0){
    $rs = fnGetAllCount($db2con,$WUAUTH,$sSearch,$licenseSql);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $allcount = $rs['data'];
    }
}
if($allCheck==='1'){
    if($rtn === 0){
        $rs = fnGetAllCheckData($db2con,$WUAUTH,$licenseSql);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allCheckData = $rs['data'];
        }
    }
}

if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$WUAUTH,$sSearch,$checkList,$allCheck,$start,$length,$sort,$sortDir,$licenseSql);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData' => umEx($data,true),
    'iTotalRecords' => $allcount,
    'allCheckData'=>$allCheckData
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/
function fnGetFDB2CSV1($db2con,$WUAUTH,$sSearch,$checkList,$allCheck,$start = '',$length = '',$sort = '',$sortDir = '',$licenseSql){
    $data = array();
    $params = array();
    $strSQL='';
    $strSQL .=' SELECT A.* FROM (';//for Grid paging
    $strSQL .= '  SELECT B.D1NAME,B.D1TEXT,D1WEBF,D1CFLG, ';
    if($allCheck==='0' || $allCheck==='2'){
        $strSQL .= ' \'0\' AS CHKFLG , ';
    }else{
        $strSQL .= ' \'1\' AS CHKFLG , ';
    }
    $strSQL .= ' ROWNUMBER() OVER( ';
    if(cmMer($sort) !== ''){//for Grid paging
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.D1NAME ASC ';
    }
    $strSQL .= ' ) as rownum ';

    $strSQL .= '    FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  D1NAME,D1TEXT,D1WEBF,D1CFLG ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '   ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  D1NAME,D1TEXT,D1WEBF,D1CFLG ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '   FDB2CSV1 AS B';
    }
    $strSQL .= ' WHERE B.D1NAME <> \'\' ';
    if($sSearch != ''){
        $strSQL .= ' AND (B.D1NAME LIKE ? OR B.D1TEXT LIKE ? ) ';
        array_push($params,'%'.$sSearch.'%','%'.$sSearch.'%');
    }
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND B.D1CFLG = \'\' ';
    }
    $strSQL .= ' ) as A ';//for Grid paging
    //抽出範囲指定
    if(($start != '')&&($length != '')){//for Grid paging
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    e_log('実行SQL:'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
            $err =db2_stmt_errormsg();
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $err =db2_stmt_errormsg();
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                if($allCheck==='2'){
                    foreach($checkList as $val){
                        if(cmMer($val['field'])===cmMer($row['D1NAME'])){
                            $row['CHKFLG']=$val['value'];
                        }
                    }
                }
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* クエリー作成のカウント
*-------------------------------------------------------*
*/
function fnGetAllCount($db2con,$WUAUTH,$sSearch,$licenseSql){
    $data = array();
    $totCount=0;
    $params = array();
    $strSQL='';
    $strSQL .= '  SELECT COUNT(B.D1NAME) AS TOTCOUNT ';
    $strSQL .= '    FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  D1NAME,D1TEXT,D1WEBF,D1CFLG ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '   ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  D1NAME,D1TEXT,D1WEBF,D1CFLG ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '   FDB2CSV1 AS B';
    }
    $strSQL .= ' WHERE B.D1NAME <> \'\' ';
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND B.D1CFLG = \'\' ';
    }
    if($sSearch != ''){
        $strSQL .= ' AND (B.D1NAME LIKE ? OR B.D1TEXT LIKE ? ) ';
        array_push($params,'%'.$sSearch.'%','%'.$sSearch.'%');
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
            $err =db2_stmt_errormsg();
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $err =db2_stmt_errormsg();
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $totCount = $row['TOTCOUNT'];
            }
            $data = array('result' => true,'data' => $totCount);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* クエリー作成のカウント
*-------------------------------------------------------*
*/
function fnGetAllCheckData($db2con,$WUAUTH,$licenseSql){
    $data = array();
    $totCount=0;
    $params = array();
    $strSQL='';
    $strSQL .= '  SELECT B.D1NAME,B.D1TEXT,D1WEBF,D1CFLG,\'1\' AS CHKFLG ';
    $strSQL .= '    FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  D1NAME,D1TEXT,D1WEBF,D1CFLG ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '   ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  D1NAME,D1TEXT,D1WEBF,D1CFLG ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '   FDB2CSV1 AS B';
    }
    $strSQL .= ' WHERE B.D1NAME <> \'\' ';
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND B.D1CFLG = \'\' ';
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
            $err =db2_stmt_errormsg();
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $err =db2_stmt_errormsg();
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,true));
        }
    }
    return $data;
}
