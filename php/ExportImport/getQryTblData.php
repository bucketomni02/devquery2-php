<?php
// FDB2CSV1取得
function getFDB2CSV1($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM FDB2CSV1 ';
    $strSQL .= '     WHERE D1NAME = ? ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'FDB2CSV1')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*FDB2CSV1
*return dataobj
*/
function getColFieldFDB2CSV1($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['D1NAME']=$val;break;
            case 2:$dataField['D1FILE']=$val;break;
            case 3:$dataField['D1FILMBR']=$val;break;
            case 4:$dataField['D1FILLIB']=$val;break;
            case 5:$dataField['D1TEXT']=$val;break;
            case 6:$dataField['D1CSID']=$val;break;
            case 7:$dataField['D1LIBL']=$val;break;
            case 8:$dataField['D1CMD']=$val;break;
            case 9:$dataField['D1CMDC']=$val;break;
            case 10:$dataField['D1INFO']=$val;break;
            case 11:$dataField['D1INFG']=$val;break;
            // ADD START
            case 12:$dataField['D1INFGR']=$val;break;
            // ADD END
            case 13:$dataField['D1HOKN']=$val;break;
            case 14:$dataField['D1DIRE']=$val;break;
            case 15:$dataField['D1DIEX']=$val;break;
            case 16:$dataField['D1CENC']=$val;break;
            case 17:$dataField['D1CKAI']=$val;break;
            case 18:$dataField['D1CHED']=$val;break;
            case 19:$dataField['D1CDLM']=$val;break;
            case 20:$dataField['D1CECL']=$val;break;
            case 21:$dataField['D1CTFL']=$val;break;
            case 22:$dataField['D1CMDE']=$val;break;
            case 23:$dataField['D1MFLG']=$val;break;
            case 24:$dataField['D10MAL']=$val;break;
            case 25:$dataField['D1EDKB']=$val;break;
            case 26:$dataField['D1TMPF']=$val;break;
            case 27:$dataField['D1SHET']=$val;break;
            case 28:$dataField['D1TCOS']=$val;break;
            case 29:$dataField['D1TROS']=$val;break;
            case 30:$dataField['D1THFG']=$val;break;
            case 31:$dataField['D1WEBF']=$val;break;
            case 32:$dataField['D1DISF']=$val;break;
            case 33:$dataField['DISCSN']=$val;break;
            case 34:$dataField['D1EFLG']=$val;break;
            case 35:$dataField['D1ETYP']=$val;break;
            case 36:$dataField['D1RDB']=$val;break;
            case 37:$dataField['D1CFLG']=$val;break;
            case 38:$dataField['D1SERV']=$val;break;
            case 39:$dataField['D1OUTJ']=$val;break;
            case 40:$dataField['D1OUTLIB']=$val;break;
            case 41:$dataField['D1OUTFIL']=$val;break;
            case 42:$dataField['D1OUTR']=$val;break;
            // ADD START
            case 43:$dataField['D1OUTA']=$val;break;
            // ADD END
            case 44:$dataField['D1OUTSE']=$val;break;
            case 45:$dataField['D1OUTSR']=$val;break;
            // ADD START
            case 46:$dataField['D1OUTSA']=$val;break;
            case 47:$dataField['D1SFLG']=$val;break;
            // ADD END
        }
    }
    return $dataField;
}

//FDB2CSV2取得
function getFDB2CSV2($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM FDB2CSV2 ';
    $strSQL .= '     WHERE D2NAME = ? ';
    $strSQL .= '     ORDER BY D2FILID ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'FDB2CSV2')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*FDB2CSV2
*return dataobj
*/
function getColFieldFDB2CSV2($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['D2NAME']=$val;break;
            case 2:$dataField['D2FILID']=$val;break;
            case 3:$dataField['D2FLD']=$val;break;
            case 4:$dataField['D2HED']=$val;break;
            case 5:$dataField['D2CSEQ']=$val;break;
            case 6:$dataField['D2RSEQ']=$val;break;
            case 7:$dataField['D2RSTP']=$val;break;
            case 8:$dataField['D2GSEQ']=$val;break;
            case 9:$dataField['D2GMES']=$val;break;
            case 10:$dataField['D2IPDC']=$val;break;
            case 11:$dataField['D2WEDT']=$val;break;
            case 12:$dataField['D2TYPE']=$val;break;
            case 13:$dataField['D2LEN']=$val;break;
            case 14:$dataField['D2DEC']=$val;break;
            case 15:$dataField['D2DNLF']=$val;break;
        }
    }
    return $dataField;
}

//FDB2CSV5取得
function getFDB2CSV5($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM FDB2CSV5 ';
    $strSQL .= '     WHERE D5NAME = ? ';
    $strSQL .= '     ORDER BY D5ASEQ ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'FDB2CSV5')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*FDB2CSV5
*return dataobj
*/
function getColFieldFDB2CSV5($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val,"UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['D5NAME']=$val;break;
            case 2:$dataField['D5ASEQ']=$val;break;
            case 3:$dataField['D5FLD']=$val;break;
            case 4:$dataField['D5HED']=$val;break;
            case 5:$dataField['D5CSEQ']=$val;break;
            case 6:$dataField['D5GSEQ']=$val;break;
            case 7:$dataField['D5GMES']=$val;break;
            case 8:$dataField['D5IPDC']=$val;break;
            case 9:$dataField['D5WEDT']=$val;break;
            case 10:$dataField['D5TYPE']=$val;break;
            case 11:$dataField['D5LEN']=$val;break;
            case 12:$dataField['D5DEC']=$val;break;
            case 13:$dataField['D5EXP']=$val;break;
            // ADD START
            case 14:$dataField['D5EXPD']=$val;break;
            // ADD END
            case 15:$dataField['D5DNLF']=$val;break;
            case 16:$dataField['D5RSEQ']=$val;break;
            case 17:$dataField['D5RSTP']=$val;break;
        }
    }
    return $dataField;
}
//BSUMFLD取得
function getBSUMFLD($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM BSUMFLD ';
    $strSQL .= '     WHERE SFQRYN = ? ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'BSUMFLD:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'BSUMFLD:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'BSUMFLD')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*BSUMFLD
*return dataobj
*/
function getColFieldBSUMFLD($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val,"UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['SFQRYN']=$val;break;
            case 2:$dataField['SFFILID']=$val;break;
            case 3:$dataField['SFFLDNM']=$val;break;
            case 4:$dataField['SFSEQ']=$val;break;
            case 5:$dataField['SFGMES']=$val;break;
        }
    }
    return $dataField;
}
//FDB2CSV4取得
function getFDB2CSV4($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM FDB2CSV4 ';
    $strSQL .= '     WHERE D4NAME = ? ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'FDB2CSV4:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV4:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'FDB2CSV4')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*FDB2CSV4
*return dataobj
*/
function getColFieldFDB2CSV4($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['D4NAME']=$val;break;
            case 2:$dataField['D4REFN']=$val;break;
            case 3:$dataField['D4RFIL']=$val;break;
            case 4:$dataField['D4RLIB']=$val;break;
            case 5:$dataField['D4TEXT']=$val;break;
            case 6:$dataField['D4PRFL1']=$val;break;
            case 7:$dataField['D4RFFL1']=$val;break;
            case 8:$dataField['D4PRFL2']=$val;break;
            case 9:$dataField['D4RFFL2']=$val;break;
            case 10:$dataField['D4PRFL3']=$val;break;
            case 11:$dataField['D4RFFL3']=$val;break;
            case 12:$dataField['D4PRFL4']=$val;break;
            case 13:$dataField['D4RFFL4']=$val;break;
            case 14:$dataField['D4PRFL5']=$val;break;
            case 15:$dataField['D4RFFL5']=$val;break;
        }
    }
    return $dataField;
}
//FDB2CSV3取得
function getFDB2CSV3($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM FDB2CSV3 ';
    $strSQL .= '     WHERE D3NAME = ? ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'FDB2CSV3:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV3:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'FDB2CSV3')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*FDB2CSV3
*return dataobj
*/
function getColFieldFDB2CSV3($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['D3NAME']=$val;break;
            case 2:$dataField['D3FILID']=$val;break;
            case 3:$dataField['D3JSEQ']=$val;break;
            case 4:$dataField['D3CNT']=$val;break;
            case 5:$dataField['D3ANDOR']=$val;break;
            case 6:$dataField['D3FLD']=$val;break;
            case 7:$dataField['D3CND']=$val;break;
            case 8:$dataField['D3DAT']=$val;break;
            case 9:$dataField['D3USEL']=$val;break;
            case 10:$dataField['D3TYPE']=$val;break;
            case 11:$dataField['D3LEN']=$val;break;
            case 12:$dataField['D3CANL']=$val;break;
            case 13:$dataField['D3CANF']=$val;break;
            case 14:$dataField['D3CMBR']=$val;break;
            case 15:$dataField['D3CANC']=$val;break;
            case 16:$dataField['D3CANG']=$val;break;
            case 17:$dataField['D3NAMG']=$val;break;
            case 18:$dataField['D3NAMC']=$val;break;
            case 19:$dataField['D3DFMT']=$val;break;
            case 20:$dataField['D3DSFL']=$val;break;
            case 21:$dataField['D3DFIN']=$val;break;
            case 22:$dataField['D3DFPM']=$val;break;
            case 23:$dataField['D3DFDY']=$val;break;
            case 24:$dataField['D3DFFL']=$val;break;
            case 25:$dataField['D3DTIN']=$val;break;
            case 26:$dataField['D3DTPM']=$val;break;
            case 27:$dataField['D3DTDY']=$val;break;
            case 28:$dataField['D3DTFL']=$val;break;
            case 29:$dataField['D3CAST']=$val;break;
            case 30:$dataField['D3NAST']=$val;break;
            case 31:$dataField['D3DSCH']=$val;break;
        }
    }
    return $dataField;
}
//BREFTBL取得
function getBREFTBL($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM BREFTBL ';
    $strSQL .= '     WHERE RTQRYN = ? ';
    $strSQL .= '     ORDER BY RTRSEQ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'BREFTBL:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'BREFTBL:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'BREFTBL')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*BREFTBL
*return dataobj
*/
function getColFieldBREFTBL($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['RTQRYN']=$val;break;
            case 2:$dataField['RTRSEQ']=$val;break;
            case 3:$dataField['RTRFIL']=$val;break;
            case 4:$dataField['RTRMBR']=$val;break;
            case 5:$dataField['RTRLIB']=$val;break;
            case 6:$dataField['RTDESC']=$val;break;
            case 7:$dataField['RTJTYP']=$val;break;
        }
    }
    return $dataField;
}
//BREFFLD取得
function getBREFFLD($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM BREFFLD ';
    $strSQL .= '     WHERE RFQRYN = ? ';
    $strSQL .= '     ORDER BY RFFSEQ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'BREFFLD:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'BREFFLD:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'BREFFLD')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*BREFFLD
*return dataobj
*/
function getColFieldBREFFLD($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['RFQRYN']=$val;break;
            case 2:$dataField['RFRSEQ']=$val;break;
            case 3:$dataField['RFFSEQ']=$val;break;
            case 4:$dataField['RFPFID']=$val;break;
            case 5:$dataField['RFPFNM']=$val;break;
            case 6:$dataField['RFRFID']=$val;break;
            case 7:$dataField['RFRFNM']=$val;break;
            case 8:$dataField['RFRKBN']=$val;break;
            case 9:$dataField['RFTEISU']=$val;break;
        }
    }
    return $dataField;
}
//BQRYCND取得
function getBQRYCND($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM BQRYCND ';
    $strSQL .= '     WHERE CNQRYN = ? ';
    $strSQL .= '     ORDER BY CNMSEQ ';
    $strSQL .= '        ,CNSSEQ ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'BQRYCND:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'BQRYCND:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'BQRYCND')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*BQRYCND
*return dataobj
*/
function getColFieldBQRYCND($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val,"UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['CNQRYN']=$val;break;
            case 2:$dataField['CNFILID']=$val;break;
            case 3:$dataField['CNMSEQ']=$val;break;
            case 4:$dataField['CNSSEQ']=$val;break;
            case 5:$dataField['CNAOKB']=$val;break;
            case 6:$dataField['CNFLDN']=$val;break;
            case 7:$dataField['CNCKBN']=$val;break;
            case 8:$dataField['CNSTKB']=$val;break;
            case 9:$dataField['CNCANL']=$val;break;
            case 10:$dataField['CNCANF']=$val;break;
            case 11:$dataField['CNCMBR']=$val;break;
            case 12:$dataField['CNCANC']=$val;break;
            case 13:$dataField['CNCANG']=$val;break;
            case 14:$dataField['CNNAMG']=$val;break;
            case 15:$dataField['CNNAMC']=$val;break;
            case 16:$dataField['CNDFMT']=$val;break;
            case 17:$dataField['CNDSFL']=$val;break;
            case 18:$dataField['CNDFIN']=$val;break;
            case 19:$dataField['CNDFPM']=$val;break;
            case 20:$dataField['CNDFDY']=$val;break;
            case 21:$dataField['CNDFFL']=$val;break;
            case 22:$dataField['CNDTIN']=$val;break;
            case 23:$dataField['CNDTPM']=$val;break;
            case 24:$dataField['CNDTDY']=$val;break;
            case 25:$dataField['CNDTFL']=$val;break;
            case 26:$dataField['CNCAST']=$val;break;
            case 27:$dataField['CNNAST']=$val;break;
            case 28:$dataField['CNDSCH']=$val;break;
        }
    }
    return $dataField;
}
//BCNDDAT取得
function getBCNDDAT($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM BCNDDAT ';
    $strSQL .= '     WHERE CDQRYN = ? ';
    $strSQL .= '     ORDER BY CDMSEQ ';
    $strSQL .= '            , CDSSEQ ';
    $strSQL .= '            , CDCDCD ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'BCNDDAT:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'BCNDDAT:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'BCNDDAT')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*BCNDDAT
*return dataobj
*/
function getColFieldBCNDDAT($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['CDQRYN']=$val;break;
            case 2:$dataField['CDFILID']=$val;break;
            case 3:$dataField['CDMSEQ']=$val;break;
            case 4:$dataField['CDSSEQ']=$val;break;
            case 5:$dataField['CDCDCD']=$val;break;
            case 6:$dataField['CDDAT']=$val;break;
            case 7:$dataField['CDBCNT']=$val;break;
            case 8:$dataField['CDFFLG']=$val;break;
        }
    }
    return $dataField;
}

//BSQLCND取得
function getBSQLCND($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM BSQLCND ';
    $strSQL .= '     WHERE CNQRYN = ? ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'BSQLCND:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'BSQLCND:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'BSQLCND')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*BSQLCND
*return dataobj
*/
function getColFieldBSQLCND($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['CNQRYN']=$val;break;
            case 2:$dataField['CNDSEQ']=$val;break;
            case 3:$dataField['CNDKBN']=$val;break;
            case 4:$dataField['CNDNM']=$val;break;
            case 5:$dataField['CNDWNM']=$val;break;
            case 6:$dataField['CNDDTYP']=$val;break;
            case 7:$dataField['CNDSTKB']=$val;break;
            case 8:$dataField['CNDDAT']=$val;break;
            case 9:$dataField['CNDBCNT']=$val;break;
            case 10:$dataField['CNCANL']=$val;break;
            case 11:$dataField['CNCANF']=$val;break;
            case 12:$dataField['CNCMBR']=$val;break;
            case 13:$dataField['CNCANC']=$val;break;
            case 14:$dataField['CNCANG']=$val;break;
            case 15:$dataField['CNNAMG']=$val;break;
            case 16:$dataField['CNNAMC']=$val;break;
            case 17:$dataField['CNDFMT']=$val;break;
            case 18:$dataField['CNDSFL']=$val;break;
            case 19:$dataField['CNDFIN']=$val;break;
            case 20:$dataField['CNDFPM']=$val;break;
            case 21:$dataField['CNDFDY']=$val;break;
            case 22:$dataField['CNDFFL']=$val;break;
            case 23:$dataField['CNDTIN']=$val;break;
            case 24:$dataField['CNDTPM']=$val;break;
            case 25:$dataField['CNDTDY']=$val;break;
            case 26:$dataField['CNDTFL']=$val;break;
            case 27:$dataField['CNCAST']=$val;break;
            case 28:$dataField['CNNAST']=$val;break;
            case 29:$dataField['CNDSCH']=$val;break;
            case 30:$dataField['CNCKBN']=$val;break;
        }
    }
    return $dataField;
}
//BSQLDAT取得
function getBSQLDAT($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM BSQLDAT ';
    $strSQL .= '     WHERE BSQLNM = ? ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'BSQLDAT:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'BSQLDAT:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'BSQLDAT')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*BSQLDAT
*return dataobj
*/
function getColFieldBSQLDAT($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['BSQLNM']=$val;break;
            case 2:$dataField['BSQLFLG']=$val;break;
            case 3:$dataField['BEXESQL']=$val;break;
        }
    }
    return $dataField;
}
/*
 *  DB2WGDF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetDB2WGDF($db2con,$QRYNM){
    $params = array();
    $data   = array();
    /* $strSQL  = ' SELECT ';
    $strSQL .= '     WGNAME, ';
    $strSQL .= '     WGGID, ';
    $strSQL .= '     WGQGFLG ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WGDF ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     WGNAME = ? '; */

    $strSQL  = ' SELECT ';
    $strSQL .= '     WGNAME, ';
    $strSQL .= '     WGGID, ';
    $strSQL .= '     WGQGFLG, ';
    $strSQL .= '     WGNRFLG ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WGDF ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     WGNAME = ? ';//gigi

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WGDF:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WGDF:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2WGDF')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2WGDF
*return dataobj
*/
function getColFieldDB2WGDF($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['WGNAME']=$val;break;
            case 2:$dataField['WGGID']=$val;break;
			case 3:$dataField['WGQGFLG']=$val;break;//GIGI
            case 4:$dataField['WGNRFLG']=$val;break;//GIGI
        }
    }
    return $dataField;
}
/*
 *  DB2HTMLT取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetHTMLTemp($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL  = ' SELECT ';
    $strSQL .= '     HTMLTD, ';
    $strSQL .= '     HTMLTN ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2HTMLT ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     HTMLTD = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2HTMLT:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2HTMLT:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2HTMLT')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2HTMLT
*return dataobj
*/
function getColFieldDB2HTMLT($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['HTMLTD']=$val;break;
            case 2:$dataField['HTMLTN']=$val;break;
        }
    }
    return $dataField;
}
/*
 *  FDB2CSV1取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetXlsTemp($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     D1NAME, ';
    $strSQL .= '     D1EDKB, ';
    $strSQL .= '     D1TMPF, ';
    $strSQL .= '     D1SHET, ';
    $strSQL .= '     D1TCOS, ';
    $strSQL .= '     D1TROS, ';
    $strSQL .= '     D1THFG ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 ';
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*
 *  FDB2CSV1取得
 * ユーザーにクエリー操作できる権限付与【ExcelTemplate権限】
 */
function fnGetDB2ECON($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT * FROM DB2ECON WHERE ECNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2ECON:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2ECON:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2ECON')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*
 *  DB2EINS
 * ユーザーにクエリー操作できる権限付与【ExcelTemplate権限】
 */
function fnGetDB2EINS($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT * FROM DB2EINS WHERE EINAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2EINS:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2EINS:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2EINS')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*
 *  XlsTemplate取得
 */
function fnGetXlsTemplateOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL = '  SELECT * FROM DB2ECON WHERE ECNAME = ?  ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2ECON:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2ECON:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2ECON')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $strSQL = ' SELECT * FROM DB2EINS WHERE EINAME = ?  ';
            $params = array($QRYNM);
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2EINS:'.db2_stmt_errormsg());
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2EINS:'.db2_stmt_errormsg());
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $row=array('TBLNAME'=>'DB2EINS')+$row;//最初のデータを追加
                        $data[] = $row;
                    }
                }
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2ECON
*return dataobj
*/
function getColFieldDB2ECON($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['ECNAME']=$val;break;
            case 2:$dataField['ECFLG']=$val;break;
            case 3:$dataField['ECROWN']=$val;break;
        }
    }
    return $dataField;
}
/*DB2EINS
*return dataobj
*/
function getColFieldDB2EINS($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['EINAME']=$val;break;
            case 2:$dataField['EISEQ']=$val;break;
            case 3:$dataField['EICOL']=$val;break;
            case 4:$dataField['EIROW']=$val;break;
            case 5:$dataField['EITEXT']=$val;break;
            case 6:$dataField['EISIZE']=$val;break;
            case 7:$dataField['EICOLR']=$val;break;
            case 8:$dataField['EIBOLD']=$val;break;
        }
    }
    return $dataField;
}
function fnGetShosai($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     DTNAME, ';
    $strSQL .= '     DTFILID, ';
    $strSQL .= '     DTFLD, ';
    $strSQL .= '     DTLIBL, ';
    $strSQL .= '     DTMBR, ';
    $strSQL .= '     DTFILE, ';
    $strSQL .= '     DFFILID, ';
    $strSQL .= '     DFCOLM, ';
    $strSQL .= '     DFFKEY, ';
    $strSQL .= '     DFCHECK ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WDTL A ';
    $strSQL .= ' LEFT JOIN DB2WDFL B ';
    $strSQL .= ' ON A.DTNAME = B.DFNAME ';
    $strSQL .= ' AND A.DTFILID = B.DFFILID ';
    $strSQL .= ' AND A.DTFLD = B.DFFLD ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.DTNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
//DB2WDTL
function fnGetDB2WDTL($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '    * FROM ';
    $strSQL .= ' DB2WDTL ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     DTNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WDTL:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WDTL:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2WDTL')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2WDTL
*return dataobj
*/
function getColFieldDB2WDTL($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['DTNAME']=$val;break;
            case 2:$dataField['DTFILID']=$val;break;
            case 3:$dataField['DTFLD']=$val;break;
            case 4:$dataField['DTLIBL']=$val;break;
            case 5:$dataField['DTFILE']=$val;break;
            case 6:$dataField['DTMBR']=$val;break;
        }
    }
    return $dataField;
}
//DB2WDFL
function fnGetDB2WDFL($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '    * FROM ';
    $strSQL .= ' DB2WDFL ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     DFNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WDFL:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WDFL:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2WDFL')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2WDFL
*return dataobj
*/
function getColFieldDB2WDFL($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['DFNAME']=$val;break;
            case 2:$dataField['DFFILID']=$val;break;
            case 3:$dataField['DFFLD']=$val;break;
            case 4:$dataField['DFCOLM']=$val;break;
            case 5:$dataField['DFFKEY']=$val;break;
            case 6:$dataField['DFCHECK']=$val;break;
        }
    }
    return $dataField;
}
//DB2DRGS
function fnGetDB2DRGS($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     * ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2DRGS  ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     DRKMTN = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2DRGS:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2DRGS:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2DRGS')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2DRGS
*return dataobj
*/
function getColFieldDB2DRGS($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['DRKMTN']=$val;break;
            case 2:$dataField['DRKFID']=$val;break;
            case 3:$dataField['DRKFLID']=$val;break;
            case 4:$dataField['DRDMTN']=$val;break;
            case 5:$dataField['DRDFNM']=$val;break;
            case 6:$dataField['DRDFID']=$val;break;
            case 7:$dataField['DRDJSQ']=$val;break;
            case 8:$dataField['DRDCNT']=$val;break;
            case 9:$dataField['DRWEBF']=$val;break;
            case 10:$dataField['DRWEBK']=$val;break;
            case 11:$dataField['DRDFLG']=$val;break;
            case 12:$dataField['DRSFLG']=$val;break;

        }
    }
    return $dataField;
}
/*
 *  DB2WDEF取得 D1NAMEは最初じゃない
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetDwnKengenOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     WDNAME, ';
    $strSQL .= '     WDUID, ';
    $strSQL .= '     WDDWNL, ';
    $strSQL .= '     WDDWN2, ';
    $strSQL .= '     WDSERV, ';
    $strSQL .= '     WDCTFL, ';
    $strSQL .= '     WDCNFL ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WDEF A ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.WDNAME = ? ';
    $strSQL .= ' AND (A.WDDWNL = \'1\' ';
    $strSQL .= ' OR   A.WDDWN2 = \'1\' )';

    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WDEF:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WDEF:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2WDEF')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2WDEF
*return dataobj
*/
function getColFieldDB2WDEF($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['WDNAME']=$val;break;
            case 2:$dataField['WDUID']=$val;break;
            case 3:$dataField['WDDWNL']=$val;break;
            case 4:$dataField['WDDWN2']=$val;break;
            case 5:$dataField['WDSERV']=$val;break;
            case 6:$dataField['WDCTFL']=$val;break;
            case 7:$dataField['WDCNFL']=$val;break;
        }
    }
    return $dataField;
}
/*
 *  DB2COLM取得
 */
function fnGetDB2COLM($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT * FROM DB2COLM WHERE DCNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLM:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLM:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2COLM')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*
 *  DB2COLT取得
 */
function fnGetDB2COLT($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT * FROM DB2COLT WHERE DTNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLT:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLT:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2COLT')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*
 *  SEIGYO取得
 */
function fnGetSeigyoOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL = '  SELECT * FROM DB2COLM WHERE DCNAME = ?  ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLM:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLM:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2COLM')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $strSQL = ' SELECT * FROM DB2COLT WHERE DTNAME = ?  ';
            $params = array($QRYNM);
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLT:'.db2_stmt_errormsg());
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLT:'.db2_stmt_errormsg());
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $row=array('TBLNAME'=>'DB2COLT')+$row;//最初のデータを追加
                        $data[] = $row;
                    }
                }
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2COLM
*return dataobj
*/
function getColFieldDB2COLM($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['DCNAME']=$val;break;
            case 2:$dataField['DCFILID1']=$val;break;
            case 3:$dataField['DCFLD1']=$val;break;
            case 4:$dataField['DCFILID2']=$val;break;
            case 5:$dataField['DCFLD2']=$val;break;
            case 6:$dataField['DCFILID3']=$val;break;
            case 7:$dataField['DCFLD3']=$val;break;
            case 8:$dataField['DCFILID4']=$val;break;
            case 9:$dataField['DCFLD4']=$val;break;
            case 10:$dataField['DCFILID5']=$val;break;
            case 11:$dataField['DCFLD5']=$val;break;
            case 12:$dataField['DCFILID6']=$val;break;
            case 13:$dataField['DCFLD6']=$val;break;
            case 14:$dataField['DCSUMF']=$val;break;
            case 15:$dataField['DCFNAME1']=$val;break;
            case 16:$dataField['DCFNAME2']=$val;break;
            case 17:$dataField['DCFNAME3']=$val;break;
            case 18:$dataField['DCFNAME4']=$val;break;
            case 19:$dataField['DCFNAME5']=$val;break;
            case 20:$dataField['DCFNAME6']=$val;break;
        }
    }
    return $dataField;
}
/*DB2COLT
*return dataobj
*/
function getColFieldDB2COLT($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['DTNAME']=$val;break;
            case 2:$dataField['DTSEQ']=$val;break;
            case 3:$dataField['DTFILID']=$val;break;
            case 4:$dataField['DTFLD']=$val;break;
            case 5:$dataField['DTSUM']=$val;break;
            case 6:$dataField['DTAVLG']=$val;break;
            case 7:$dataField['DTMINV']=$val;break;
            case 8:$dataField['DTMAXV']=$val;break;
            case 9:$dataField['DTCONT']=$val;break;
        }
    }
    return $dataField;
}
/*
 *  PIVOT取得
 */
function fnGetPivotOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL='';
    $strSQL= ' SELECT * FROM DB2PMST WHERE PMNAME = ? ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PMST:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PMST:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2PMST')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $strSQL= ' SELECT * FROM DB2PCOL WHERE WPNAME = ? ';
            $params = array($QRYNM);
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PCOL:'.db2_stmt_errormsg());
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PCOL:'.db2_stmt_errormsg());
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $row=array('TBLNAME'=>'DB2PCOL')+$row;//最初のデータを追加
                        $data[] = $row;
                    }
                    $strSQL= ' SELECT * FROM DB2PCAL WHERE WCNAME = ? ';
                    $params = array($QRYNM);
                    $stmt = db2_prepare($db2con,$strSQL);
                    if($stmt === false){
                        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PCAL:'.db2_stmt_errormsg());
                    }else{
                        $r = db2_execute($stmt,$params);
                        if($r === false){
                            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PCAL:'.db2_stmt_errormsg());
                        }else{
                            while($row = db2_fetch_assoc($stmt)){
                                $row=array('TBLNAME'=>'DB2PCAL')+$row;//最初のデータを追加
                                $data[] = $row;
                            }
                        }
                    }
                }
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2PMST
*return dataobj
*/
function getColFieldDB2PMST($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['PMNAME']=$val;break;
            case 2:$dataField['PMPKEY']=$val;break;
            case 3:$dataField['PMTEXT']=$val;break;
            case 4:$dataField['PMDFLG']=$val;break;
        }
    }
    return $dataField;
}
/*DB2PCOL
*return dataobj
*/
function getColFieldDB2PCOL($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['WPNAME']=$val;break;
            case 2:$dataField['WPPKEY']=$val;break;
            case 3:$dataField['WPPFLG']=$val;break;
            case 4:$dataField['WPSEQN']=$val;break;
            case 5:$dataField['WPFILID']=$val;break;
            case 6:$dataField['WPFLD']=$val;break;
            case 7:$dataField['WPFHIDE']=$val;break;
            case 8:$dataField['WPSORT']=$val;break;
            case 9:$dataField['WPSUMG']=$val;break;
        }
    }
    return $dataField;
}
/*DB2PCAL
*return dataobj
*/
function getColFieldDB2PCAL($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['WCNAME']=$val;break;
            case 2:$dataField['WCCKEY']=$val;break;
            case 3:$dataField['WCSEQN']=$val;break;
            case 4:$dataField['WCCALC']=$val;break;
        }
    }
    return $dataField;
}
/*
 *  Schedule取得
 */
function fnGetScheduleOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL='';
    $strSQL= ' SELECT * FROM DB2WSCD WHERE WSNAME= ? ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WSCD:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WSCD:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2WSCD')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $strSQL= ' SELECT * FROM DB2WAUT WHERE WANAME= ? ';
            $params = array($QRYNM);
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WAUT:'.db2_stmt_errormsg());
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WAUT:'.db2_stmt_errormsg());
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $row=array('TBLNAME'=>'DB2WAUT')+$row;//最初のデータを追加
                        $data[] = $row;
                    }
                    $strSQL= ' SELECT * FROM DB2WAUL WHERE WLNAME= ? ';
                    $params = array($QRYNM);
                    $stmt = db2_prepare($db2con,$strSQL);
                    if($stmt === false){
                        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WAUL:'.db2_stmt_errormsg());
                    }else{
                        $r = db2_execute($stmt,$params);
                        if($r === false){
                            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WAUL:'.db2_stmt_errormsg());
                        }else{
                            while($row = db2_fetch_assoc($stmt)){
                                $row=array('TBLNAME'=>'DB2WAUL')+$row;//最初のデータを追加
                                $data[] = $row;
                            }
                            $strSQL= ' SELECT * FROM DB2WSOC WHERE SONAME= ? ';
                            $params = array($QRYNM);
                            $stmt = db2_prepare($db2con,$strSQL);
                            if($stmt === false){
                                $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WSOC:'.db2_stmt_errormsg());
                            }else{
                                $r = db2_execute($stmt,$params);
                                if($r === false){
                                    $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WSOC:'.db2_stmt_errormsg());
                                }else{
                                    while($row = db2_fetch_assoc($stmt)){
                                        $row=array('TBLNAME'=>'DB2WSOC')+$row;//最初のデータを追加
                                        $data[] = $row;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2WSCD
*return dataobj
*/
function getColFieldDB2WSCD($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['WSNAME']=$val;break;
            case 2:$dataField['WSPKEY']=$val;break;
            case 3:$dataField['WSFRQ']=$val;break;
            case 4:$dataField['WSODAY']=$val;break;
            case 5:$dataField['WSWDAY']=$val;break;
            case 6:$dataField['WSMDAY']=$val;break;
            case 7:$dataField['WSTIME']=$val;break;
            case 8:$dataField['WSSPND']=$val;break;
            case 9:$dataField['WSBDAY']=$val;break;
            case 10:$dataField['WSBTIM']=$val;break;
            case 11:$dataField['WSSDAY']=$val;break;
            case 12:$dataField['WSSTIM']=$val;break;
            case 13:$dataField['WSQGFLG']=$val;break;
        }
    }
    return $dataField;
}
/*DB2WAUT
*return dataobj
*/
function getColFieldDB2WAUT($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['WANAME']=$val;break;
            case 2:$dataField['WAPKEY']=$val;break;
            case 3:$dataField['WAMAIL']=$val;break;
            case 4:$dataField['WAMACC']=$val;break;
            case 5:$dataField['WACSV']=$val;break;
            case 6:$dataField['WAXLS']=$val;break;
            case 7:$dataField['WAHTML']=$val;break;
            case 8:$dataField['WASPND']=$val;break;
            case 9:$dataField['WASFLG']=$val;break;
            case 10:$dataField['WAFRAD']=$val;break;
            case 11:$dataField['WAFRNM']=$val;break;
            case 12:$dataField['WASUBJ']=$val;break;
            case 13:$dataField['WABODY']=$val;break;
            case 14:$dataField['WASNKB']=$val;break;
            case 15:$dataField['WATYPE']=$val;break;
            case 16:$dataField['WABAFID']=$val;break;
            case 17:$dataField['WABAFLD']=$val;break;
            case 18:$dataField['WABAFR']=$val;break;
            case 19:$dataField['WABATO']=$val;break;
            case 20:$dataField['WABFLG']=$val;break;
            case 21:$dataField['WABINC']=$val;break;
            case 22:$dataField['WAQUNM']=$val;break;
            case 23:$dataField['WAFONT']=$val;break;
            case 24:$dataField['WAQGFLG']=$val;break;
            case 25:$dataField['WAPIV']=$val;break;
            case 26:$dataField['WANGFLG']=$val;break;
            case 27:$dataField['WAGPID']=$val;break;
          
        }
    }
    return $dataField;
}
/*DB2WAUL
*return dataobj
*/
function getColFieldDB2WAUL($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['WLNAME']=$val;break;
            case 2:$dataField['WLPKEY']=$val;break;
            case 3:$dataField['WLMAIL']=$val;break;
            case 4:$dataField['WLSEQL']=$val;break;
            case 5:$dataField['WLDATA']=$val;break;
        }
    }
    return $dataField;
}
/*DB2WSOC
*return dataobj
*/
function getColFieldDB2WSOC($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['SONAME']=$val;break;
            case 2:$dataField['SOPKEY']=$val;break;
            case 3:$dataField['SO0MAL']=$val;break;
            case 4:$dataField['SODIEX']=$val;break;
            case 5:$dataField['SOCTFL']=$val;break;
            case 6:$dataField['SOHOKN']=$val;break;
            case 7:$dataField['SODIRE']=$val;break;
            case 8:$dataField['SOOUTSE']=$val;break;
            case 9:$dataField['SOOUTSR']=$val;break;
            case 10:$dataField['SOOUTSA']=$val;break;
            case 11:$dataField['SOQGFLG']=$val;break;
        }
    }
    return $dataField;
}
/*
 *  CL連携設定取得
 */
function fnGetCLSetOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL='';
    $strSQL= ' SELECT * FROM FDB2CSV1PG WHERE DGNAME= ? ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'FDB2CSV1PG')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $strSQL= ' SELECT * FROM FDB2CSV1PM WHERE DMNAME = ? ';
            $params = array($QRYNM);
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg());
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg());
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $row=array('TBLNAME'=>'FDB2CSV1PM')+$row;//最初のデータを追加
                        $data[] = $row;
                    }
                }
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*FDB2CSV1PG
*return dataobj
*/
function getColFieldFDB2CSV1PG($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['DGNAME']=$val;break;
            case 2:$dataField['DGBLIB']=$val;break;
            case 3:$dataField['DGBPGM']=$val;break;
            case 4:$dataField['DGCLIB']=$val;break;
            case 5:$dataField['DGCPGM']=$val;break;
            case 6:$dataField['DGCBNM']=$val;break;
            case 7:$dataField['DGALIB']=$val;break;
            case 8:$dataField['DGAPGM']=$val;break;
        }
    }
    return $dataField;
}
/*FDB2CSV1PM
*return dataobj
*/
function getColFieldFDB2CSV1PM($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['DMNAME']=$val;break;
            case 2:$dataField['DMEVENT']=$val;break;
            case 3:$dataField['DMSEQ']=$val;break;
            case 4:$dataField['DMUSAGE']=$val;break;
            case 5:$dataField['DMPNAME']=$val;break;
            case 6:$dataField['DMPTYPE']=$val;break;
            case 7:$dataField['DMLEN']=$val;break;
            case 8:$dataField['DMSCAL']=$val;break;
            case 9:$dataField['DMFRTO']=$val;break;
            case 10:$dataField['DMIZDT']=$val;break;
            case 11:$dataField['DMSCFG']=$val;break;
            case 12:$dataField['DMSCID']=$val;break;
            case 13:$dataField['DMSCFD']=$val;break;
            case 14:$dataField['DMMSEQ']=$val;break;
            case 15:$dataField['DMSSEQ']=$val;break;
        }
    }
    return $dataField;
}
/*
 *  グラフ設定取得
 */
function fnGetGraphOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL='';
    $strSQL= ' SELECT * FROM DB2GPK WHERE GPKQRY = ? ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2GPK:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2GPK:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row=array('TBLNAME'=>'DB2GPK')+$row;//最初のデータを追加
                $data[] = $row;
            }
            $strSQL= ' SELECT GPCQRY,GPCSEQ,GPCID,GPCYCO,GPCYID, GPCLBL,GPCFM,GPCCAL,GPCCLR,GPCSOS,GPCSFG FROM DB2GPC WHERE GPCQRY = ? ';
            $params = array($QRYNM);
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2GPC:'.db2_stmt_errormsg());
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2GPC:'.db2_stmt_errormsg());
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $row=array('TBLNAME'=>'DB2GPC')+$row;//最初のデータを追加
                        $data[] = $row;
                    }
                }
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
/*DB2GPK
*return dataobj
*/
function getColFieldDB2GPK($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['GPKQRY']=$val;break;
            case 2:$dataField['GPKID']=$val;break;
            case 3:$dataField['GPKNM']=$val;break;
            case 4:$dataField['GPKFM']=$val;break;
            case 5:$dataField['GPKNUM']=$val;break;
            case 6:$dataField['GPKDAT']=$val;break;
            case 7:$dataField['GPKXCO']=$val;break;
            case 8:$dataField['GPKXID']=$val;break;
            case 9:$dataField['GPKXNM']=$val;break;
            case 10:$dataField['GPKYNM']=$val;break;
            case 11:$dataField['GPKCFG']=$val;break;
            case 12:$dataField['GPKCUD']=$val;break;
            case 13:$dataField['GPKCUT']=$val;break;
            case 14:$dataField['GPKBFD']=$val;break;
            case 15:$dataField['GPKBFT']=$val;break;
            case 16:$dataField['GPKTBL']=$val;break;
            case 17:$dataField['GPKEND']=$val;break;
            case 18:$dataField['GPKFLG']=$val;break;
            case 19:$dataField['GPKJKD']=$val;break;
            case 20:$dataField['GPKJKT']=$val;break;
            case 21:$dataField['GPKDFG']=$val;break;
        }
    }
    return $dataField;
}
/*DB2GPC
*return dataobj
*/
function getColFieldDB2GPC($data){
    $dataField=array();
    foreach($data as $key=>$val){
        $val = mb_convert_encoding($val, "UTF-8", "SJIS-win");
        switch($key){
            case 0:$dataField['TBLNAME']=$val;break;
            case 1:$dataField['GPCQRY']=$val;break;
            case 2:$dataField['GPCSEQ']=$val;break;
            case 3:$dataField['GPCID']=$val;break;
            case 4:$dataField['GPCYCO']=$val;break;
            case 5:$dataField['GPCYID']=$val;break;
            case 6:$dataField['GPCLBL']=$val;break;
            case 7:$dataField['GPCFM']=$val;break;
            case 8:$dataField['GPCCAL']=$val;break;
            case 9:$dataField['GPCCLR']=$val;break;
            case 10:$dataField['GPCSOS']=$val;break;
            case 11:$dataField['GPCSFG']=$val;break;
        }
    }
    return $dataField;
}
