<?php
// �y�}���zFDB2CSV1
function insFDB2CSV1($db2con,$fdb2csv1){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO FDB2CSV1 ';
    $strSQL .= '        ( ';
    $strSQL .= '            D1NAME, ';
    $strSQL .= '            D1FILE, ';
    $strSQL .= '            D1FILMBR, ';
    $strSQL .= '            D1FILLIB, ';
    $strSQL .= '            D1TEXT, ';
    $strSQL .= '            D1CSID, ';
    $strSQL .= '            D1LIBL, ';
    $strSQL .= '            D1CMD, ';
    $strSQL .= '            D1CMDC,';
    $strSQL .= '            D1INFO, ';
    $strSQL .= '            D1INFG, ';
    
    $strSQL .= '            D1INFGR, ';
    
    $strSQL .= '            D1HOKN, ';
    $strSQL .= '            D1DIRE, ';
    $strSQL .= '            D1DIEX, ';
    $strSQL .= '            D1CENC, ';
    $strSQL .= '            D1CKAI, ';
    $strSQL .= '            D1CHED, ';
    $strSQL .= '            D1CDLM,';
    $strSQL .= '            D1CECL, ';
    $strSQL .= '            D1CTFL, ';
    $strSQL .= '            D1CMDE, ';
    $strSQL .= '            D1MFLG, ';
    $strSQL .= '            D10MAL, ';
    $strSQL .= '            D1EDKB, ';
    $strSQL .= '            D1TMPF, ';
    $strSQL .= '            D1SHET, ';
    $strSQL .= '            D1TCOS,';
    $strSQL .= '            D1TROS, ';
    $strSQL .= '            D1THFG, ';
    $strSQL .= '            D1WEBF, ';
    $strSQL .= '            D1DISF, ';
    $strSQL .= '            DISCSN, ';
    $strSQL .= '            D1EFLG, ';
    $strSQL .= '            D1ETYP, ';
    $strSQL .= '            D1RDB, ';
    $strSQL .= '            D1CFLG, ';
    $strSQL .= '            D1SERV, ';
    $strSQL .= '            D1OUTJ, ';
    $strSQL .= '            D1OUTLIB, ';
    $strSQL .= '            D1OUTFIL, ';
    $strSQL .= '            D1OUTR, ';
    
    $strSQL .= '            D1OUTA, ';
    
    $strSQL .= '            D1OUTSE, ';
    $strSQL .= '            D1OUTSR, ';
    
    $strSQL .= '            D1OUTSA, ';
    $strSQL .= '            D1SFLG ';
    
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($fdb2csv1 as $dataField){
        $params =array(
            $dataField['D1NAME'],
            $dataField['D1FILE'],
            $dataField['D1FILMBR'],
            $dataField['D1FILLIB'],
            $dataField['D1TEXT'],
            $dataField['D1CSID'],
            $dataField['D1LIBL'],
            $dataField['D1CMD'],
            $dataField['D1CMDC'],
            $dataField['D1INFO'],
            $dataField['D1INFG'],
            
            $dataField['D1INFGR'],
            
            $dataField['D1HOKN'],
            $dataField['D1DIRE'],
            $dataField['D1DIEX'],
            $dataField['D1CENC'],
            $dataField['D1CKAI'],
            $dataField['D1CHED'],
            $dataField['D1CDLM'],
            $dataField['D1CECL'],
            $dataField['D1CTFL'],
            $dataField['D1CMDE'],
            $dataField['D1MFLG'],
            $dataField['D10MAL'],
            $dataField['D1EDKB'],
            $dataField['D1TMPF'],
            $dataField['D1SHET'],
            $dataField['D1TCOS'],
            $dataField['D1TROS'],
            $dataField['D1THFG'],
            $dataField['D1WEBF'],
            $dataField['D1DISF'],
            $dataField['DISCSN'],
            $dataField['D1EFLG'],
            $dataField['D1ETYP'],
            $dataField['D1RDB'],
            $dataField['D1CFLG'],
            $dataField['D1SERV'],
            $dataField['D1OUTJ'],
            $dataField['D1OUTLIB'],
            $dataField['D1OUTFIL'],
            $dataField['D1OUTR'],
            
            $dataField['D1OUTA'],
            
            $dataField['D1OUTSE'],
            $dataField['D1OUTSR'],
            
            $dataField['D1OUTSA'],
            $dataField['D1SFLG']
            
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zFDB2CSV2
function insFDB2CSV2($db2con,$fdb2csv2){
    $strSQL = '';
    $data   = array();
    $strSQL .= ' INSERT INTO FDB2CSV2 ';
    $strSQL .= '        ( ';
    $strSQL .= '            D2NAME, ';
    $strSQL .= '            D2FILID, ';
    $strSQL .= '            D2FLD, ';
    $strSQL .= '            D2HED, ';
    $strSQL .= '            D2CSEQ, ';
    $strSQL .= '            D2RSEQ, ';
    $strSQL .= '            D2RSTP, ';
    $strSQL .= '            D2GSEQ, ';
    $strSQL .= '            D2GMES,';
    $strSQL .= '            D2IPDC, ';
    $strSQL .= '            D2WEDT, ';
    $strSQL .= '            D2TYPE, ';
    $strSQL .= '            D2LEN, ';
    $strSQL .= '            D2DEC, ';
    $strSQL .= '            D2DNLF ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($fdb2csv2 as $dataField){
        $params =array(
            $dataField['D2NAME'],
            $dataField['D2FILID'],
            $dataField['D2FLD'],
            $dataField['D2HED'],
            $dataField['D2CSEQ'],
            $dataField['D2RSEQ'],
            $dataField['D2RSTP'],
            $dataField['D2GSEQ'],
            $dataField['D2GMES'],
            $dataField['D2IPDC'],
            $dataField['D2WEDT'],
            $dataField['D2TYPE'],
            $dataField['D2LEN'],
            $dataField['D2DEC'],
            $dataField['D2DNLF']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg());break;
        }else{            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zFDB2CSV5
function insFDB2CSV5($db2con,$fdb2csv5){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO FDB2CSV5 ';
    $strSQL .= '        ( ';
    $strSQL .= '            D5NAME, ';
    $strSQL .= '            D5ASEQ, ';
    $strSQL .= '            D5FLD, ';
    $strSQL .= '            D5HED, ';
    $strSQL .= '            D5CSEQ, ';
    $strSQL .= '            D5GSEQ, ';
    $strSQL .= '            D5GMES, ';
    $strSQL .= '            D5IPDC, ';
    $strSQL .= '            D5WEDT,';
    $strSQL .= '            D5TYPE, ';
    $strSQL .= '            D5LEN, ';
    $strSQL .= '            D5DEC, ';
    $strSQL .= '            D5EXP, ';
    $strSQL .= '            D5EXPD, ';
    $strSQL .= '            D5DNLF, ';
    $strSQL .= '            D5RSEQ, ';
    $strSQL .= '            D5RSTP ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($fdb2csv5 as $dataField){
        $params =array(
            $dataField['D5NAME'],
            $dataField['D5ASEQ'],
            $dataField['D5FLD'],
            $dataField['D5HED'],
            $dataField['D5CSEQ'],
            $dataField['D5GSEQ'],
            $dataField['D5GMES'],
            $dataField['D5IPDC'],
            $dataField['D5WEDT'],
            $dataField['D5TYPE'],
            $dataField['D5LEN'],
            $dataField['D5DEC'],
            $dataField['D5EXP'],
            $dataField['D5EXPD'],
            $dataField['D5DNLF'],
            $dataField['D5RSEQ'],
            $dataField['D5RSTP']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg());break;
        }else{            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zBSUMFLD
function insBSUMFLD($db2con,$BSUMFLD){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO BSUMFLD ';
    $strSQL .= '        ( ';
    $strSQL .= '            SFQRYN, ';
    $strSQL .= '            SFFILID, ';
    $strSQL .= '            SFFLDNM, ';
    $strSQL .= '            SFSEQ, ';
    $strSQL .= '            SFGMES ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($BSUMFLD as $dataField){
        $params =array(
            $dataField['SFQRYN'],
            $dataField['SFFILID'],
            $dataField['SFFLDNM'],
            $dataField['SFSEQ'],
            $dataField['SFGMES']);
        e_log('insert=>'.$strSQL.print_r($params,true));
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BSUMFLD:'.db2_stmt_errormsg());break;
        }else{            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'BSUMFLD:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zFDB2CSV4
function insFDB2CSV4($db2con,$fdb2csv4){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO FDB2CSV4 ';
    $strSQL .= '        ( ';
    $strSQL .= '            D4NAME, ';
    $strSQL .= '            D4REFN, ';
    $strSQL .= '            D4RFIL, ';
    $strSQL .= '            D4RLIB, ';
    $strSQL .= '            D4TEXT, ';
    $strSQL .= '            D4PRFL1, ';
    $strSQL .= '            D4RFFL1, ';
    $strSQL .= '            D4PRFL2, ';
    $strSQL .= '            D4RFFL2,';
    $strSQL .= '            D4PRFL3, ';
    $strSQL .= '            D4RFFL3, ';
    $strSQL .= '            D4PRFL4, ';
    $strSQL .= '            D4RFFL4, ';
    $strSQL .= '            D4PRFL5,  ';
    $strSQL .= '            D4RFFL5  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($fdb2csv4 as $dataField){
        $params =array(
            $dataField['D4NAME'],
            $dataField['D4REFN'],
            $dataField['D4RFIL'],
            $dataField['D4RLIB'],
            $dataField['D4TEXT'],
            $dataField['D4PRFL1'],
            $dataField['D4RFFL1'],
            $dataField['D4PRFL2'],
            $dataField['D4RFFL2'],
            $dataField['D4PRFL3'],
            $dataField['D4RFFL3'],
            $dataField['D4PRFL4'],
            $dataField['D4RFFL4'],
            $dataField['D4PRFL5'],
            $dataField['D4RFFL5']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV4:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV4:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zFDB2CSV3
function insFDB2CSV3($db2con,$fdb2csv3){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO FDB2CSV3 ';
    $strSQL .= '        ( ';
    $strSQL .= '            D3NAME, ';
    $strSQL .= '            D3FILID, ';
    $strSQL .= '            D3JSEQ, ';
    $strSQL .= '            D3CNT, ';
    $strSQL .= '            D3ANDOR, ';
    $strSQL .= '            D3FLD, ';
    $strSQL .= '            D3CND, ';
    $strSQL .= '            D3DAT, ';
    $strSQL .= '            D3USEL,';
    $strSQL .= '            D3TYPE, ';
    $strSQL .= '            D3LEN, ';
    $strSQL .= '            D3CANL, ';
    $strSQL .= '            D3CANF, ';
    $strSQL .= '            D3CMBR, ';
    $strSQL .= '            D3CANC, ';
    $strSQL .= '            D3CANG, ';
    $strSQL .= '            D3NAMG, ';
    $strSQL .= '            D3NAMC,';
    $strSQL .= '            D3DFMT, ';
    $strSQL .= '            D3DSFL, ';
    $strSQL .= '            D3DFIN, ';
    $strSQL .= '            D3DFPM, ';
    $strSQL .= '            D3DFDY, ';
    $strSQL .= '            D3DFFL, ';
    $strSQL .= '            D3DTIN, ';
    $strSQL .= '            D3DTPM, ';
    $strSQL .= '            D3DTDY,';
    $strSQL .= '            D3DTFL, ';
    $strSQL .= '            D3CAST, ';
    $strSQL .= '            D3NAST, ';
    $strSQL .= '            D3DSCH ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($fdb2csv3 as $dataField){
        $params =array(
            $dataField['D3NAME'],
            $dataField['D3FILID'],
            $dataField['D3JSEQ'],
            $dataField['D3CNT'],
            $dataField['D3ANDOR'],
            $dataField['D3FLD'],
            $dataField['D3CND'],
            $dataField['D3DAT'],
            $dataField['D3USEL'],
            $dataField['D3TYPE'],
            $dataField['D3LEN'],
            $dataField['D3CANL'],
            $dataField['D3CANF'],
            $dataField['D3CMBR'],
            $dataField['D3CANC'],
            $dataField['D3CANG'],
            $dataField['D3NAMG'],
            $dataField['D3NAMC'],
            $dataField['D3DFMT'],
            $dataField['D3DSFL'],
            $dataField['D3DFIN'],
            $dataField['D3DFPM'],
            $dataField['D3DFDY'],
            $dataField['D3DFFL'],
            $dataField['D3DTIN'],
            $dataField['D3DTPM'],
            $dataField['D3DTDY'],
            $dataField['D3DTFL'],
            $dataField['D3CAST'],
            $dataField['D3NAST'],
            $dataField['D3DSCH']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV3:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV3:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zBREFFLD
function insBREFFLD($db2con,$BREFFLD){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO BREFFLD ';
    $strSQL .= '        ( ';
    $strSQL .= '            RFQRYN, ';
    $strSQL .= '            RFRSEQ, ';
    $strSQL .= '            RFFSEQ, ';
    $strSQL .= '            RFPFID, ';
    $strSQL .= '            RFPFNM, ';
    $strSQL .= '            RFRFID, ';
    $strSQL .= '            RFRFNM, ';
    $strSQL .= '            RFRKBN, ';
    $strSQL .= '            RFTEISU  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($BREFFLD as $dataField){
        $params =array(
            $dataField['RFQRYN'],
            $dataField['RFRSEQ'],
            $dataField['RFFSEQ'],
            $dataField['RFPFID'],
            $dataField['RFPFNM'],
            $dataField['RFRFID'],
            $dataField['RFRFNM'],
            $dataField['RFRKBN'],
            $dataField['RFTEISU']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BREFFLD:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'BREFFLD:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zBREFTBL
function insBREFTBL($db2con,$BREFTBL){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO BREFTBL ';
    $strSQL .= '        ( ';
    $strSQL .= '            RTQRYN, ';
    $strSQL .= '            RTRSEQ, ';
    $strSQL .= '            RTRFIL, ';
    $strSQL .= '            RTRMBR, ';
    $strSQL .= '            RTRLIB, ';
    $strSQL .= '            RTDESC, ';
    $strSQL .= '            RTJTYP  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($BREFTBL as $dataField){
        $params =array(
            $dataField['RTQRYN'],
            $dataField['RTRSEQ'],
            $dataField['RTRFIL'],
            $dataField['RTRMBR'],
            $dataField['RTRLIB'],
            $dataField['RTDESC'],
            $dataField['RTJTYP']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BREFTBL:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'BREFTBL:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zBQRYCND
function insBQRYCND($db2con,$BQRYCND){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO BQRYCND ';
    $strSQL .= '        ( ';
    $strSQL .= '            CNQRYN, ';
    $strSQL .= '            CNFILID, ';
    $strSQL .= '            CNMSEQ, ';
    $strSQL .= '            CNSSEQ, ';
    $strSQL .= '            CNAOKB, ';
    $strSQL .= '            CNFLDN, ';
    $strSQL .= '            CNCKBN, ';
    $strSQL .= '            CNSTKB, ';
    $strSQL .= '            CNCANL,';
    $strSQL .= '            CNCANF, ';
    $strSQL .= '            CNCMBR, ';
    $strSQL .= '            CNCANC, ';
    $strSQL .= '            CNCANG, ';
    $strSQL .= '            CNNAMG, ';
    $strSQL .= '            CNNAMC, ';
    $strSQL .= '            CNDFMT, ';
    $strSQL .= '            CNDSFL, ';
    $strSQL .= '            CNDFIN,';
    $strSQL .= '            CNDFPM, ';
    $strSQL .= '            CNDFDY, ';
    $strSQL .= '            CNDFFL, ';
    $strSQL .= '            CNDTIN, ';
    $strSQL .= '            CNDTPM, ';
    $strSQL .= '            CNDTDY, ';
    $strSQL .= '            CNDTFL, ';
    $strSQL .= '            CNCAST, ';
    $strSQL .= '            CNNAST, ';
    $strSQL .= '            CNDSCH  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($BQRYCND as $dataField){
        $params =array(
            $dataField['CNQRYN'],
            $dataField['CNFILID'],
            $dataField['CNMSEQ'],
            $dataField['CNSSEQ'],
            $dataField['CNAOKB'],
            $dataField['CNFLDN'],
            $dataField['CNCKBN'],
            $dataField['CNSTKB'],
            $dataField['CNCANL'],
            $dataField['CNCANF'],
            $dataField['CNCMBR'],
            $dataField['CNCANC'],
            $dataField['CNCANG'],
            $dataField['CNNAMG'],
            $dataField['CNNAMC'],
            $dataField['CNDFMT'],
            $dataField['CNDSFL'],
            $dataField['CNDFIN'],
            $dataField['CNDFPM'],
            $dataField['CNDFDY'],
            $dataField['CNDFFL'],
            $dataField['CNDTIN'],
            $dataField['CNDTPM'],
            $dataField['CNDTDY'],
            $dataField['CNDTFL'],
            $dataField['CNCAST'],
            $dataField['CNNAST'],
            $dataField['CNDSCH']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BQRYCND:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'BQRYCND:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zBCNDDAT
function insBCNDDAT($db2con,$BCNDDAT){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO BCNDDAT ';
    $strSQL .= '        ( ';
    $strSQL .= '            CDQRYN, ';
    $strSQL .= '            CDFILID, ';
    $strSQL .= '            CDMSEQ, ';
    $strSQL .= '            CDSSEQ, ';
    $strSQL .= '            CDCDCD, ';
    $strSQL .= '            CDDAT, ';
    $strSQL .= '            CDBCNT, ';
    $strSQL .= '            CDFFLG';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($BCNDDAT as $dataField){
        $params =array(
            $dataField['CDQRYN'],
            $dataField['CDFILID'],
            $dataField['CDMSEQ'],
            $dataField['CDSSEQ'],
            $dataField['CDCDCD'],
            $dataField['CDDAT'],
            $dataField['CDBCNT'],
            $dataField['CDFFLG']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BCNDDAT:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'BCNDDAT:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zBSQLCND
function insBSQLCND($db2con,$BSQLCND){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO BSQLCND ';
    $strSQL .= '        ( ';
    $strSQL .= '            CNQRYN, ';
    $strSQL .= '            CNDSEQ, ';
    $strSQL .= '            CNDKBN, ';
    $strSQL .= '            CNDNM, ';
    $strSQL .= '            CNDWNM, ';
    $strSQL .= '            CNDDTYP, ';
    $strSQL .= '            CNDSTKB, ';
    $strSQL .= '            CNDDAT, ';
    $strSQL .= '            CNDBCNT,';
    $strSQL .= '            CNCANL, ';
    $strSQL .= '            CNCANF, ';
    $strSQL .= '            CNCMBR, ';
    $strSQL .= '            CNCANC, ';
    $strSQL .= '            CNCANG, ';
    $strSQL .= '            CNNAMG, ';
    $strSQL .= '            CNNAMC, ';
    $strSQL .= '            CNDFMT, ';
    $strSQL .= '            CNDSFL,';
    $strSQL .= '            CNDFIN, ';
    $strSQL .= '            CNDFPM, ';
    $strSQL .= '            CNDFDY, ';
    $strSQL .= '            CNDFFL, ';
    $strSQL .= '            CNDTIN, ';
    $strSQL .= '            CNDTPM, ';
    $strSQL .= '            CNDTDY, ';
    $strSQL .= '            CNDTFL, ';
    $strSQL .= '            CNCAST,';
    $strSQL .= '            CNNAST, ';
    $strSQL .= '            CNDSCH, ';
    $strSQL .= '            CNCKBN  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    e_log('BSQLCND=>'.$strSQL.print_r($BSQLCND,true));
    foreach($BSQLCND as $dataField){
        $params =array(
            $dataField['CNQRYN'],
            $dataField['CNDSEQ'],
            $dataField['CNDKBN'],
            $dataField['CNDNM'],
            $dataField['CNDWNM'],
            $dataField['CNDDTYP'],
            $dataField['CNDSTKB'],
            $dataField['CNDDAT'],
            $dataField['CNDBCNT'],
            $dataField['CNCANL'],
            $dataField['CNCANF'],
            $dataField['CNCMBR'],
            $dataField['CNCANC'],
            $dataField['CNCANG'],
            $dataField['CNNAMG'],
            $dataField['CNNAMC'],
            $dataField['CNDFMT'],
            $dataField['CNDSFL'],
            $dataField['CNDFIN'],
            $dataField['CNDFPM'],
            $dataField['CNDFDY'],
            $dataField['CNDFFL'],
            $dataField['CNDTIN'],
            $dataField['CNDTPM'],
            $dataField['CNDTDY'],
            $dataField['CNDTFL'],
            $dataField['CNCAST'],
            $dataField['CNNAST'],
            $dataField['CNDSCH'],
            $dataField['CNCKBN']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BSQLCND:'.db2_stmt_errormsg());
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'BSQLCND:'.db2_stmt_errormsg());
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zBSQLDAT
function insBSQLDAT($db2con,$BSQLDAT){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO BSQLDAT ';
    $strSQL .= '        ( ';
    $strSQL .= '            BSQLNM, ';
    $strSQL .= '            BSQLFLG, ';
    $strSQL .= '            BEXESQL  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,? ';
    $strSQL .= '        ) ';
e_log('BSQLDAT=>'.$strSQL.print_r($BSQLDAT,true));
    foreach($BSQLDAT as $dataField){
        $params =array(
            $dataField['BSQLNM'],
            $dataField['BSQLFLG'],
            $dataField['BEXESQL']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BSQLDAT:'.db2_stmt_errormsg());
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'BSQLDAT:'.db2_stmt_errormsg());
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2WGDF
function insDB2WGDF($db2con,$DB2WGDF){
    $strSQL = '';
    $data   = array();   
    /*$strSQL .= ' INSERT INTO DB2WGDF ';
    $strSQL .= '        ( ';
    $strSQL .= '          WGNAME, ';
    $strSQL .= '          WGGID,  ';
    $strSQL .= '          WGQGFLG ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2WGDF as $dataField){
        $params =array(
            $dataField['WGNAME'],
            $dataField['WGGID'],
            $dataField['WGQGFLG']
        );*/

    $strSQL .= ' INSERT INTO DB2WGDF ';
    $strSQL .= '        ( ';
    $strSQL .= '          WGNAME, ';
    $strSQL .= '          WGGID,  ';
    $strSQL .= '          WGQGFLG, ';
    $strSQL .= '          WGNRFLG ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2WGDF as $dataField){
         error_log("VALUES OF WGNRFLG ***** ".$dataField['WGNRFLG']);
        $params =array(
            $dataField['WGNAME'],
            $dataField['WGGID'],
            $dataField['WGQGFLG'],
            $dataField['WGNRFLG']
        );//gigi

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WGDF:'.db2_stmt_errormsg());
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WGDF:'.db2_stmt_errormsg());
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2HTMLT
function insDB2HTMLT($db2con,$DB2HTMLT){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2HTMLT ';
    $strSQL .= '        ( ';
    $strSQL .= '            HTMLTD, ';
    $strSQL .= '            HTMLTN';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,? ';
    $strSQL .= '        ) ';
    foreach($DB2HTMLT as $dataField){
        $params =array(
            $dataField['HTMLTD'],
            $dataField['HTMLTN']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2HTMLT:'.db2_stmt_errormsg());
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2HTMLT:'.db2_stmt_errormsg());
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2ECON
function insDB2ECON($db2con,$DB2ECON){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2ECON ';
    $strSQL .= '        ( ';
    $strSQL .= '            ECNAME, ';
    $strSQL .= '            ECFLG, ';
    $strSQL .= '            ECROWN ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2ECON as $dataField){
        $params =array(
            $dataField['ECNAME'],
            $dataField['ECFLG'],
            $dataField['ECROWN']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2ECON:'.db2_stmt_errormsg());
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2ECON:'.db2_stmt_errormsg());
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2EINS
function insDB2EINS($db2con,$DB2EINS){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2EINS ';
    $strSQL .= '        ( ';
    $strSQL .= '            EINAME, ';
    $strSQL .= '            EISEQ, ';
    $strSQL .= '            EICOL, ';
    $strSQL .= '            EIROW, ';
    $strSQL .= '            EITEXT, ';
    $strSQL .= '            EISIZE, ';
    $strSQL .= '            EICOLR, ';
    $strSQL .= '            EIBOLD ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2EINS as $dataField){
        $params =array(
            $dataField['EINAME'],
            $dataField['EISEQ'],
            $dataField['EICOL'],
            $dataField['EIROW'],
            $dataField['EITEXT'],
            $dataField['EISIZE'],
            $dataField['EICOLR'],
            $dataField['EIBOLD']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2EINS:'.db2_stmt_errormsg());
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2EINS:'.db2_stmt_errormsg());
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2WDTL
function insDB2WDTL($db2con,$DB2WDTL){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2WDTL ';
    $strSQL .= '        ( ';
    $strSQL .= '            DTNAME, ';
    $strSQL .= '            DTFILID, ';
    $strSQL .= '            DTFLD, ';
    $strSQL .= '            DTLIBL, ';
    $strSQL .= '            DTFILE, ';
    $strSQL .= '            DTMBR  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2WDTL as $dataField){
        $params =array(
            $dataField['DTNAME'],
            $dataField['DTFILID'],
            $dataField['DTFLD'],
            $dataField['DTLIBL'],
            $dataField['DTFILE'],
            $dataField['DTMBR']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WDTL:'.db2_stmt_errormsg());
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WDTL:'.db2_stmt_errormsg());
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2WDFL
function insDB2WDFL($db2con,$DB2WDFL){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2WDFL ';
    $strSQL .= '        ( ';
    $strSQL .= '            DFNAME, ';
    $strSQL .= '            DFFILID, ';
    $strSQL .= '            DFFLD, ';
    $strSQL .= '            DFCOLM, ';
    $strSQL .= '            DFFKEY, ';
    $strSQL .= '            DFCHECK  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2WDFL as $dataField){
        $params =array(
            $dataField['DFNAME'],
            $dataField['DFFILID'],
            $dataField['DFFLD'],
            $dataField['DFCOLM'],
            $dataField['DFFKEY'],
            $dataField['DFCHECK']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WDFL:'.db2_stmt_errormsg());
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WDFL:'.db2_stmt_errormsg());
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2DRGS
function insDB2DRGS($db2con,$DB2DRGS){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2DRGS ';
    $strSQL .= '        ( ';
    $strSQL .= '            DRKMTN, ';
    $strSQL .= '            DRKFID, ';
    $strSQL .= '            DRKFLID, ';
    $strSQL .= '            DRDMTN, ';
    $strSQL .= '            DRDFNM, ';
    $strSQL .= '            DRDFID, ';
    $strSQL .= '            DRDJSQ, ';
    $strSQL .= '            DRDCNT, ';
    $strSQL .= '            DRWEBF,';
    $strSQL .= '            DRWEBK, ';
    $strSQL .= '            DRDFLG, ';
    $strSQL .= '            DRSFLG  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2DRGS as $dataField){
        $params =array(
            $dataField['DRKMTN'],
            $dataField['DRKFID'],
            $dataField['DRKFLID'],
            $dataField['DRDMTN'],
            $dataField['DRDFNM'],
            $dataField['DRDFID'],
            $dataField['DRDJSQ'],
            $dataField['DRDCNT'],
            $dataField['DRWEBF'],
            $dataField['DRWEBK'],
            $dataField['DRDFLG'],
            $dataField['DRSFLG']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2DRGS:'.db2_stmt_errormsg());
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2DRGS:'.db2_stmt_errormsg());
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2WDEF
function insDB2WDEF($db2con,$DB2WDEF){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2WDEF ';
    $strSQL .= '        ( ';
    $strSQL .= '            WDNAME, ';
    $strSQL .= '            WDUID, ';
    $strSQL .= '            WDDWNL, ';
    $strSQL .= '            WDDWN2, ';
    $strSQL .= '            WDSERV, ';
    $strSQL .= '            WDCTFL, ';
    $strSQL .= '            WDCNFL ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2WDEF as $dataField){
        $params =array(
            $dataField['WDNAME'],
            $dataField['WDUID'],
            $dataField['WDDWNL'],
            $dataField['WDDWN2'],
            $dataField['WDSERV'],
            $dataField['WDCTFL'],
            $dataField['WDCNFL']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WDEF:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WDEF:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2COLM
function insDB2COLM($db2con,$DB2COLM){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2COLM ';
    $strSQL .= '        ( ';
    $strSQL .= '            DCNAME, ';
    $strSQL .= '            DCFILID1, ';
    $strSQL .= '            DCFLD1, ';
    $strSQL .= '            DCFILID2, ';
    $strSQL .= '            DCFLD2, ';
    $strSQL .= '            DCFILID3, ';
    $strSQL .= '            DCFLD3, ';
    $strSQL .= '            DCFILID4, ';
    $strSQL .= '            DCFLD4,';
    $strSQL .= '            DCFILID5, ';
    $strSQL .= '            DCFLD5, ';
    $strSQL .= '            DCFILID6, ';
    $strSQL .= '            DCFLD6, ';
    $strSQL .= '            DCSUMF, ';
    $strSQL .= '            DCFNAME1, ';
    $strSQL .= '            DCFNAME2, ';
    $strSQL .= '            DCFNAME3, ';
    $strSQL .= '            DCFNAME4, ';
    $strSQL .= '            DCFNAME5, ';
    $strSQL .= '            DCFNAME6 ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2COLM as $dataField){
        $params =array(
            $dataField['DCNAME'],
            $dataField['DCFILID1'],
            $dataField['DCFLD1'],
            $dataField['DCFILID2'],
            $dataField['DCFLD2'],
            $dataField['DCFILID3'],
            $dataField['DCFLD3'],
            $dataField['DCFILID4'],
            $dataField['DCFLD4'],
            $dataField['DCFILID5'],
            $dataField['DCFLD5'],
            $dataField['DCFILID6'],
            $dataField['DCFLD6'],
            $dataField['DCSUMF'],
            $dataField['DCFNAME1'],
            $dataField['DCFNAME2'],
            $dataField['DCFNAME3'],
            $dataField['DCFNAME4'],
            $dataField['DCFNAME5'],
            $dataField['DCFNAME6']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2COLM:'.db2_stmt_errormsg());break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2COLM:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2COLT
function insDB2COLT($db2con,$DB2COLT){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2COLT ';
    $strSQL .= '        ( ';
    $strSQL .= '            DTNAME, ';
    $strSQL .= '            DTSEQ, ';
    $strSQL .= '            DTFILID, ';
    $strSQL .= '            DTFLD, ';
    $strSQL .= '            DTSUM, ';
    $strSQL .= '            DTAVLG, ';
    $strSQL .= '            DTMINV, ';
    $strSQL .= '            DTMAXV, ';
    $strSQL .= '            DTCONT';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2COLT as $dataField){
        $params =array(
            $dataField['DTNAME'],
            $dataField['DTSEQ'],
            $dataField['DTFILID'],
            $dataField['DTFLD'],
            $dataField['DTSUM'],
            $dataField['DTAVLG'],
            $dataField['DTMINV'],
            $dataField['DTMAXV'],
            $dataField['DTCONT']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2COLT:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2COLT:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2PMST
function insDB2PMST($db2con,$DB2PMST){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2PMST ';
    $strSQL .= '        ( ';
    $strSQL .= '            PMNAME, ';
    $strSQL .= '            PMPKEY, ';
    $strSQL .= '            PMTEXT, ';
    $strSQL .= '            PMDFLG';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2PMST as $dataField){
        $params =array(
            $dataField['PMNAME'],
            $dataField['PMPKEY'],
            $dataField['PMTEXT'],
            $dataField['PMDFLG']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2PMST:'.db2_stmt_errormsg());break;
        }else{            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2PMST:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2PCOL
function insDB2PCOL($db2con,$DB2PCOL){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2PCOL ';
    $strSQL .= '        ( ';
    $strSQL .= '            WPNAME, ';
    $strSQL .= '            WPPKEY, ';
    $strSQL .= '            WPPFLG, ';
    $strSQL .= '            WPSEQN, ';
    $strSQL .= '            WPFILID, ';
    $strSQL .= '            WPFLD, ';
    $strSQL .= '            WPFHIDE, ';
    $strSQL .= '            WPSORT, ';
    $strSQL .= '            WPSUMG';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2PCOL as $dataField){
        $params =array(
            $dataField['WPNAME'],
            $dataField['WPPKEY'],
            $dataField['WPPFLG'],
            $dataField['WPSEQN'],
            $dataField['WPFILID'],
            $dataField['WPFLD'],
            $dataField['WPFHIDE'],
            $dataField['WPSORT'],
            $dataField['WPSUMG']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2PCOL:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2PCOL:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2PCAL
function insDB2PCAL($db2con,$DB2PCAL){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2PCAL ';
    $strSQL .= '        ( ';
    $strSQL .= '            WCNAME, ';
    $strSQL .= '            WCCKEY, ';
    $strSQL .= '            WCSEQN, ';
    $strSQL .= '            WCCALC';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2PCAL as $dataField){
        $params =array(
            $dataField['WCNAME'],
            $dataField['WCCKEY'],
            $dataField['WCSEQN'],
            $dataField['WCCALC']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2PCAL:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2PCAL:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2WSCD
function insDB2WSCD($db2con,$DB2WSCD){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2WSCD ';
    $strSQL .= '        ( ';
    $strSQL .= '            WSNAME, ';
    $strSQL .= '            WSPKEY, ';
    $strSQL .= '            WSFRQ, ';
    $strSQL .= '            WSODAY, ';
    $strSQL .= '            WSWDAY, ';
    $strSQL .= '            WSMDAY, ';
    $strSQL .= '            WSTIME, ';
    $strSQL .= '            WSSPND, ';
    $strSQL .= '            WSBDAY,';
    $strSQL .= '            WSBTIM, ';
    $strSQL .= '            WSSDAY, ';
    $strSQL .= '            WSSTIM, ';
    $strSQL .= '            WSQGFLG';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2WSCD as $dataField){
        $params =array(
            $dataField['WSNAME'],
            $dataField['WSPKEY'],
            $dataField['WSFRQ'],
            $dataField['WSODAY'],
            $dataField['WSWDAY'],
            $dataField['WSMDAY'],
            $dataField['WSTIME'],
            $dataField['WSSPND'],
            $dataField['WSBDAY'],
            $dataField['WSBTIM'],
            $dataField['WSSDAY'],
            $dataField['WSSTIM'],
            $dataField['WSQGFLG']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WSCD:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WSCD:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2WAUT
function insDB2WAUT($db2con,$DB2WAUT){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2WAUT ';
    $strSQL .= '        ( ';
    $strSQL .= '            WANAME, ';
    $strSQL .= '            WAPKEY, ';
    $strSQL .= '            WAMAIL, ';
    $strSQL .= '            WAMACC, ';
    $strSQL .= '            WACSV, ';
    $strSQL .= '            WAXLS, ';
    $strSQL .= '            WAHTML, ';
    $strSQL .= '            WASPND, ';
    $strSQL .= '            WASFLG,';
    $strSQL .= '            WAFRAD, ';
    $strSQL .= '            WAFRNM, ';
    $strSQL .= '            WASUBJ, ';
    $strSQL .= '            WABODY, ';
    $strSQL .= '            WASNKB, ';
    $strSQL .= '            WATYPE, ';
    $strSQL .= '            WABAFID, ';
    $strSQL .= '            WABAFLD, ';
    $strSQL .= '            WABAFR,';
    $strSQL .= '            WABATO, ';
    $strSQL .= '            WABFLG, ';
    $strSQL .= '            WABINC, ';
    $strSQL .= '            WAQUNM, ';
    $strSQL .= '            WAFONT, ';
    $strSQL .= '            WAQGFLG, ';
    $strSQL .= '            WAPIV, ';
    $strSQL .= '            WANGFLG, ';
    $strSQL .= '            WAGPID';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2WAUT as $dataField){
        $params =array(
            $dataField['WANAME'],
            $dataField['WAPKEY'],
            $dataField['WAMAIL'],
            $dataField['WAMACC'],
            $dataField['WACSV'],
            $dataField['WAXLS'],
            $dataField['WAHTML'],
            $dataField['WASPND'],
            $dataField['WASFLG'],
            $dataField['WAFRAD'],
            $dataField['WAFRNM'],
            $dataField['WASUBJ'],
            $dataField['WABODY'],
            $dataField['WASNKB'],
            $dataField['WATYPE'],
            $dataField['WABAFID'],
            $dataField['WABAFLD'],
            $dataField['WABAFR'],
            $dataField['WABATO'],
            $dataField['WABFLG'],
            $dataField['WABINC'],
            $dataField['WAQUNM'],
            $dataField['WAFONT'],
            $dataField['WAQGFLG'],
            $dataField['WAPIV'],
            $dataField['WANGFLG'],
            $dataField['WAGPID']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WAUT:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WAUT:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2WAUL
function insDB2WAUL($db2con,$DB2WAUL){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2WAUL ';
    $strSQL .= '        ( ';
    $strSQL .= '            WLNAME, ';
    $strSQL .= '            WLPKEY, ';
    $strSQL .= '            WLMAIL, ';
    $strSQL .= '            WLSEQL, ';
    $strSQL .= '            WLDATA';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2WAUL as $dataField){
        $params =array(
            $dataField['WLNAME'],
            $dataField['WLPKEY'],
            $dataField['WLMAIL'],
            $dataField['WLSEQL'],
            $dataField['WLDATA']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WAUL:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WAUL:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2WSOC
function insDB2WSOC($db2con,$DB2WSOC){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2WSOC ';
    $strSQL .= '        ( ';
    $strSQL .= '            SONAME, ';
    $strSQL .= '            SOPKEY, ';
    $strSQL .= '            SO0MAL, ';
    $strSQL .= '            SODIEX, ';
    $strSQL .= '            SOCTFL, ';
    $strSQL .= '            SOHOKN, ';
    $strSQL .= '            SODIRE, ';
    $strSQL .= '            SOOUTSE, ';
    $strSQL .= '            SOOUTSR, ';
    $strSQL .= '            SOOUTSA, ';
    $strSQL .= '            SOQGFLG ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2WSOC as $dataField1){
        $params =array(
            $dataField1['SONAME'],
            $dataField1['SOPKEY'],
            $dataField1['SO0MAL'],
            $dataField1['SODIEX'],
            $dataField1['SOCTFL'],
            $dataField1['SOHOKN'],
            $dataField1['SODIRE'],
            $dataField1['SOOUTSE'],
            $dataField1['SOOUTSR'],
            $dataField1['SOOUTSA'],
            $dataField1['SOQGFLG']
          
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WSOC:'.db2_stmt_errormsg());break;
        }else{            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2WSOC:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zFDB2CSV1PG
function insFDB2CSV1PG($db2con,$FDB2CSV1PG){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO FDB2CSV1PG ';
    $strSQL .= '        ( ';
    $strSQL .= '            DGNAME, ';
    $strSQL .= '            DGBLIB, ';
    $strSQL .= '            DGBPGM, ';
    $strSQL .= '            DGCLIB, ';
    $strSQL .= '            DGCPGM, ';
    $strSQL .= '            DGCBNM, ';
    $strSQL .= '            DGALIB, ';
    $strSQL .= '            DGAPGM';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($FDB2CSV1PG as $dataField){
        $params =array(
            $dataField['DGNAME'],
            $dataField['DGBLIB'],
            $dataField['DGBPGM'],
            $dataField['DGCLIB'],
            $dataField['DGCPGM'],
            $dataField['DGCBNM'],
            $dataField['DGALIB'],
            $dataField['DGAPGM']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zFDB2CSV1PM
function insFDB2CSV1PM($db2con,$FDB2CSV1PM){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO FDB2CSV1PM ';
    $strSQL .= '        ( ';
    $strSQL .= '            DMNAME, ';
    $strSQL .= '            DMEVENT, ';
    $strSQL .= '            DMSEQ, ';
    $strSQL .= '            DMUSAGE, ';
    $strSQL .= '            DMPNAME, ';
    $strSQL .= '            DMPTYPE, ';
    $strSQL .= '            DMLEN, ';
    $strSQL .= '            DMSCAL, ';
    $strSQL .= '            DMFRTO, ';
    $strSQL .= '            DMIZDT, ';
    $strSQL .= '            DMSCFG, ';
    $strSQL .= '            DMSCID, ';
    $strSQL .= '            DMSCFD, ';
    $strSQL .= '            DMMSEQ, ';
    $strSQL .= '            DMSSEQ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($FDB2CSV1PM as $dataField){
        $params =array(
            $dataField['DMNAME'],
            $dataField['DMEVENT'],
            $dataField['DMSEQ'],
            $dataField['DMUSAGE'],
            $dataField['DMPNAME'],
            $dataField['DMPTYPE'],
            $dataField['DMLEN'],
            $dataField['DMSCAL'],
            $dataField['DMFRTO'],
            $dataField['DMIZDT'],
            $dataField['DMSCFG'],
            $dataField['DMSCID'],
            $dataField['DMSCFD'],
            $dataField['DMMSEQ'],
            $dataField['DMSSEQ']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2GPK
function insDB2GPK($db2con,$DB2GPK){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2GPK ';
    $strSQL .= '        ( ';
    $strSQL .= '            GPKQRY, ';
    $strSQL .= '            GPKID, ';
    $strSQL .= '            GPKNM, ';
    $strSQL .= '            GPKFM, ';
    $strSQL .= '            GPKNUM, ';
	$strSQL .= '            GPKDAT, ';
    $strSQL .= '            GPKXCO, ';
    $strSQL .= '            GPKXID, ';
    $strSQL .= '            GPKXNM, ';
    $strSQL .= '            GPKYNM, ';
	$strSQL .= '            GPKCFG, ';
    $strSQL .= '            GPKCUD, ';
    $strSQL .= '            GPKCUT, ';
	$strSQL .= '            GPKBFD, ';
    $strSQL .= '            GPKBFT, ';
    $strSQL .= '            GPKTBL, ';
    $strSQL .= '            GPKEND, ';
    $strSQL .= '            GPKFLG, ';
    $strSQL .= '            GPKJKD, ';
    $strSQL .= '            GPKJKT, ';
    $strSQL .= '            GPKDFG';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2GPK as $dataField){
        $params =array(
            $dataField['GPKQRY'],
            $dataField['GPKID'],
            $dataField['GPKNM'],
            $dataField['GPKFM'],
            $dataField['GPKNUM'],
            $dataField['GPKDAT'],
            $dataField['GPKXCO'],
            $dataField['GPKXID'],
            $dataField['GPKXNM'],
            $dataField['GPKYNM'],
            $dataField['GPKCFG'],
            $dataField['GPKCUD'],
            $dataField['GPKCUT'],
            $dataField['GPKBFD'],
            $dataField['GPKBFT'],
            $dataField['GPKTBL'],
            $dataField['GPKEND'],
            $dataField['GPKFLG'],
            $dataField['GPKJKD'],
            $dataField['GPKJKT'],
            $dataField['GPKDFG']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2GPK:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2GPK:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// �y�}���zDB2GPC
function insDB2GPC($db2con,$DB2GPC){
    $strSQL = '';
    $data   = array();   
    $strSQL .= ' INSERT INTO DB2GPC ';
    $strSQL .= '        ( ';
    $strSQL .= '            GPCQRY, ';
    $strSQL .= '            GPCSEQ, ';
    $strSQL .= '            GPCID, ';
    $strSQL .= '            GPCYCO, ';
    $strSQL .= '            GPCYID, ';
    $strSQL .= '            GPCLBL, ';
    $strSQL .= '            GPCFM, ';
    $strSQL .= '            GPCCAL, ';
    $strSQL .= '            GPCCLR, ';
    $strSQL .= '            GPCSOS, ';
    $strSQL .= '            GPCSFG';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    foreach($DB2GPC as $dataField){
        $params =array(
            $dataField['GPCQRY'],
            $dataField['GPCSEQ'],
            $dataField['GPCID'],
            $dataField['GPCYCO'],
            $dataField['GPCYID'],
            $dataField['GPCLBL'],
            $dataField['GPCFM'],
            $dataField['GPCCAL'],
            $dataField['GPCCLR'],
            $dataField['GPCSOS'],
            $dataField['GPCSFG']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2GPC:'.db2_stmt_errormsg());break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array('result' => 'FAIL_INS','errcd'  => 'DB2GPC:'.db2_stmt_errormsg());break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}