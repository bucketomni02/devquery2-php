<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$csvfile = (isset($_POST['csvfile'])) ? $_POST['csvfile'] : '';
$USERFLG = (isset($_POST['USERFLG'])) ? $_POST['USERFLG'] : '';
$data = array();
$datatest = array();
$libcheck = 1;
$kengencheck = 1;
$langcheck = 1;
$msg = '';
$success = true;
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rtn = 0;
$csvblankcheck = '0';
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    } else {
        $userData = umEx($rs['data']);
        if ($userData[0]['WUAUTH'] === '2') {
            $rs = cmChkKenGen($db2con, '33', $userData[0]['WUSAUT']); //'33' => Import
            if ($rs['result'] !== true) {
                $rtn = 2;
                $msg = showMsg($rs['result'], array('インポートの権限'));
            }
        }
    }
}
if ($rtn === 0) {
    $res = db2_autocommit($db2con, DB2_AUTOCOMMIT_OFF);
    if ($res === false) {
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if ($rtn === 0) {
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}

if (isset($_FILES)) {
    error_log('files log = '.print_r($_FILES,true));
    $csvfile = $_FILES['csvfile']['name'];
    if ($csvfile !== '') {
        $ext = explode('.', $csvfile);
        $ext = $ext[count($ext) - 1];
        $tmp_csvfile = $_FILES['csvfile']['tmp_name'];
        $csvfile = $csvfile;
    } else {
        $csvfile = '';
    }
}
if ($USERFLG === '0') {
    $filename = $tmp_csvfile;
    $flg = '0';
} else if ($USERFLG === '1') {
    $filename = $tmp_csvfile;
    $flg = '1';
} else if($USERFLG === '2') {
    $filename = $tmp_csvfile;
    $flg = '2';
}
//e_log('TempFile'.print_r($filename,true));
$delimiter = ",";
if (($handle = fopen($filename, "r")) !== FALSE) {
    $fieldInfo = array();
    $row = 0;
    while (($data = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
        $num = count($data);
        $row++;
        $fieldData = array();
        for ($i = 0;$i < $num;$i++) {
            $fieldData[] = $data[$i];
        }
        $fieldInfo[$row] = $fieldData;
    }
    array_shift($fieldInfo);
    // unset( $fieldInfo[1]);
    fclose($handle);
    if (count($fieldInfo) === 0) {
        $csvblankcheck = '1';
    }
    if ($csvblankcheck === '1') {
        $rtn = 1;
        $msg = showMsg('FAIL_CSVFILE');
    } else {
        foreach ($fieldInfo as $rowno => $lines) {
            //	if($rowno !== 0){
            /*        if ($flg === '0') {
                $userArrdata = array();
                foreach ($lines as $key => $val) {
                    $val = mb_convert_encoding($val, "UTF-8", "SJIS");
                    if ($key == 0) {
                        $userArrdata['WUUID'] = $val;
                    } else if ($key == 1) {
                        $userArrdata['WUUNAM'] = $val;
                    } else if ($key == 2) {
                        $userArrdata['WUAUTH'] = $val;
                    } else if ($key == 3) {
                        $userArrdata['WUDWNL'] = $val;
                    } else if ($key == 4) {
                        $userArrdata['WUDWN2'] = $val;
                    } else if ($key == 5) {
                        $userArrdata['WUCLR1'] = $val;
                    } else if ($key == 6) {
                        $userArrdata['WUCLR2'] = $val;
                    } else if ($key == 7) {
                        $userArrdata['WUCLR3'] = $val;
                    } else if ($key == 8) {
                        $userArrdata['WUCLR4'] = $val;
                    } else if ($key == 9) {
                        $userArrdata['WUCLR5'] = $val;
                    } else if ($key == 10) {
                        $userArrdata['WULANG'] = $val;
                    } else if ($key == 11) {
                        $userArrdata['WUPSWE'] = $val;
                    } else if ($key == 12) {
                        $userArrdata['WUSIZE'] = $val;
                    } else if ($key == 13) {
                        $userArrdata['WUEDAY'] = $val;
                    } else if ($key == 14) {
                        $userArrdata['WULGNC'] = $val;
                    } else if ($key == 15) {
                        $userArrdata['WUSAUT'] = $val;
                    } else if ($key == 16) {
                        $userArrdata['WUSERV'] = $val;
                    } else if ($key == 17) {
                        $userArrdata['WUCTFL'] = $val;
                    } else if ($key == 18) {
                        $userArrdata['WULIBL'] = $val;
                    } else if ($key == 19) {
                        $userArrdata['WUUQFG'] = $val;
                    }
                }
                $userArr[$rowno] = $userArrdata;
                $readfile = $userArr;
            }*/
                    if ($flg === '0') {
                $userArrdata = array();
                foreach ($lines as $key => $val) {
                    $val = mb_convert_encoding($val, "UTF-8", "SJIS");
                    if ($key == 0) {
                        $userArrdata['WUUID'] = $val;
                    } else if ($key == 1) {
                        $userArrdata['WUUNAM'] = $val;
                    } else if ($key == 2) {
                        $userArrdata['WUAID'] = $val;//gigi add WUAID
                    } else if ($key == 3) {
                        $userArrdata['WUAUTH'] = $val;
                    } else if ($key == 4) {
                        $userArrdata['WUDWNL'] = $val;
                    } else if ($key == 5) {
                        $userArrdata['WUDWN2'] = $val;
                    } else if ($key == 6) {
                        $userArrdata['WUCLR1'] = $val;
                    } else if ($key == 7) {
                        $userArrdata['WUCLR2'] = $val;
                    } else if ($key == 8) {
                        $userArrdata['WUCLR3'] = $val;
                    } else if ($key == 9) {
                        $userArrdata['WUCLR4'] = $val;
                    } else if ($key == 10) {
                        $userArrdata['WUCLR5'] = $val;
                    } else if ($key == 11) {
                        $userArrdata['WULANG'] = $val;
                    } else if ($key == 12) {
                        $userArrdata['WUPSWE'] = $val;
                    } else if ($key == 13) {
                        $userArrdata['WUSIZE'] = $val;
                    } else if ($key == 14) {
                        $userArrdata['WUEDAY'] = $val;
                    } else if ($key == 15) {
                        $userArrdata['WULGNC'] = $val;
                    } else if ($key == 16) {
                        $userArrdata['WUSAUT'] = $val;
                    } else if ($key == 17) {
                        $userArrdata['WUSERV'] = $val;
                    } else if ($key == 18) {
                        $userArrdata['WUCTFL'] = $val;
                    } else if ($key == 19) {
                        $userArrdata['WULIBL'] = $val;
                    } else if ($key == 20) {
                        $userArrdata['WUUQFG'] = $val;
                    }
                }
                $userArr[$rowno] = $userArrdata;
                $readfile = $userArr;
            } else if ($flg === '1') {
                $groupArrdata = array();
                foreach ($lines as $key => $val) {
                    $val = mb_convert_encoding($val, "UTF-8", "SJIS");
                    if ($key == 0) {
                        $groupArrdata['WUUID'] = $val;
                    } else if ($key == 1) {
                        $groupArrdata['WUGID'] = $val;
                    }
                }
                $groupArr[$rowno] = $groupArrdata;
                $readfile = $groupArr;
            } /*else if ($flg === '2') {
                $queryArrdata = array();
                foreach ($lines as $key => $val) {
                    $val = mb_convert_encoding($val, "UTF-8", "SJIS");
                    if ($key == 0) {
                        $queryArrdata['WDUID'] = $val;
                    } else if ($key == 1) {
                        $queryArrdata['WDNAME'] = $val;
                    } else if ($key == 2) {
                        $queryArrdata['WDDWNL'] = $val;
                    } else if ($key == 3) {
                        $queryArrdata['WDDWN2'] = $val;
                    } else if ($key == 4) {
                        $queryArrdata['WDSERV'] = $val;
                    } else if ($key == 5) {
                        $queryArrdata['WDCTFL'] = $val;
                    }
                }
                $queryArr[$rowno] = $queryArrdata;
                $readfile = $queryArr;
            }*/

             else if ($flg === '2') {//GIGI ADD WDCNFL
                $queryArrdata = array();
                foreach ($lines as $key => $val) {
                    $val = mb_convert_encoding($val, "UTF-8", "SJIS");
                    if ($key == 0) {
                        $queryArrdata['WDUID'] = $val;
                    } else if ($key == 1) {
                        $queryArrdata['WDNAME'] = $val;
                    } else if ($key == 2) {
                        $queryArrdata['WDDWNL'] = $val;
                    } else if ($key == 3) {
                        $queryArrdata['WDDWN2'] = $val;
                    } else if ($key == 4) {
                        $queryArrdata['WDSERV'] = $val;
                    } else if ($key == 5) {
                        $queryArrdata['WDCTFL'] = $val;
                    }else if ($key == 6) {
                        $queryArrdata['WDCNFL'] = $val;//GIGI ADD WDCNFL
                    }
                }
                $queryArr[$rowno] = $queryArrdata;
                $readfile = $queryArr;
            }//GIGI ADD WDCNFL
            // }
            
        }
    }
}
//e_log('ReadFile Name' . print_r($readfile, true));
$blankdata = '0';
if ($csvblankcheck !== '1') {
    if ($flg === '0') {
        foreach ($readfile as $key1 => $userdata) {
            $key = $key1 + 1;
            if (count($userdata['WUUNAM']) !== 0) {
                $blankdata = '1';
                if ($rtn === 0) {
                    $userid = cmMer($userdata['WUUID']);
                    if ($userid === '') {
                        $rtn = 1;
                        $msg = showMsg('FAIL_REQ', array(array($key, '行目', ':', 'ユーザ', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($userdata['WUUID'], 20)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'ユーザ', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    $byte = checkByte($userdata['WUUID']);
                    if ($byte[1] > 0) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_BYTE', array(array($key, '行目', ':', 'ユーザ', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    if ($userdata['WUPSWE'] === '') {
                        $rtn = 1;
                        $msg = showMsg('FAIL_REQ', array(array($key, '行目', ':', 'パスワード')));
                    }
                }
                if ($rtn === 0) {
                    $PWD = cmMer($userdata['WUPSWE']);
                    if ($PWD === '') {
                        $rtn = 1;
                        $msg = showMsg('FAIL_CSVSPACE', array(array($key, '行目', ':')));
                    }
                }
                if ($rtn === 0) {
                    $byte = checkByte($userdata['WUPSWE']);
                    if ($byte[1] > 0) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_BYTE', array(array($key, '行目', ':', 'パスワード')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($userdata['WUPSWE'], 20)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'パスワード')));
                    }
                }
                if ($rtn === 0) {
                    $username = cmMer($userdata['WUUNAM']);
                    if ($username === '') {
                        $rtn = 1;
                        $msg = showMsg('FAIL_REQ', array(array($key, '行目', ':', 'ユーザー名')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($userdata['WUUNAM'], 32)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'ユーザー名')));
                    }
                }
                if ($rtn === 0) {
                    //必須チェック
                    if ($userdata['WUAUTH'] === '0' || $userdata['WUAUTH'] === '1'
					 || $userdata['WUAUTH'] === '2' || $userdata['WUAUTH'] === '3'
					 || $userdata['WUAUTH'] === '4') {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', 'ユーザ', '権限', 'の', '値')));
                    }
                }
                if ($rtn === 0) {
                    $userDL1 = cmMer($userdata['WUDWNL']);
                    if ($userDL1 === '' || $userdata['WUDWNL'] === '1') {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', 'CSV', 'ダウンロード権限')));
                    }
                }
                if ($rtn === 0) {
                    $userDL2 = cmMer($userdata['WUDWN2']);
                    if ($userDL2 === '' || $userdata['WUDWN2'] === '1') {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', 'EXCEL', 'ダウンロード権限')));
                    }
                }
                if ($rtn === 0) {
                    $userCL1 = cmMer($userdata['WUCLR1']);
                    e_log('color moe ' . $userCL1);
                    if ($userCL1 === '') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'red') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'blue') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'orange') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'purple') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'green') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'fb') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'muted') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'dark') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'pink') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'brown') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'sea-blue') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'banana') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'aqua') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR1'] === 'navy') {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', 'メインカラー')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($userdata['WUCLR2'], 20)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'カラー', '2')));
                    }
                }
                if ($rtn === 0) {
                    if (preg_match('/[^0-9A-Za-z_#@]+/', $userdata['WUCLR2'])) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_KEYENTRY', array(array($key, '行目', ':', 'カラー', '2')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($userdata['WUCLR4'], 20)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'カラー', '4')));
                    }
                }
                if ($rtn === 0) {
                    if (preg_match('/[^0-9A-Za-z_#@]+/', $userdata['WUCLR4'])) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_KEYENTRY', array(array($key, '行目', ':', 'カラー', '4')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($userdata['WUCLR5'], 20)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'カラー', '5')));
                    }
                }
                if ($rtn === 0) {
                    if (preg_match('/[^0-9A-Za-z_#@]+/', $userdata['WUCLR5'])) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_KEYENTRY', array(array($key, '行目', ':', 'カラー', '5')));
                    }
                }
                if ($rtn === 0) {
                    $userCL3 = cmMer($userdata['WUCLR3']);
                    if ($userCL3 === '') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'red') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'blue') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'orange') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'purple') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'green') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'fb') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'muted') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'dark') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'pink') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'brown') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'sea-blue') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'banana') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'gray') {
                        $rtn = 0;
                    } else if ($userdata['WUCLR3'] === 'navy') {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', '見出しカラー')));
                    }
                }
                if ($rtn === 0) {
                    if (!(checkDateVal($userdata['WUEDAY']))) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', '実行日付'))); //'実行日付の日付に誤りがあります。';
                        
                    }
                }
                if ($rtn === 0) {
                    if (!checkNum($userdata['WULGNC'])) {
                        $rtn = 1;
                        $msg = showMsg('CHK_NUMB', array(array($key, '行目', ':', '実行回数', 'の', '値')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($userdata['WULGNC'], 1)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', '実行回数', 'の', '値')));
                    }
                }
                if ($rtn === 0) {
                    $userSAUT = cmMer($userdata['WUSAUT']);
                    if ($userdata['WUAUTH'] === '2' && $userSAUT === '') {
                        $rtn = 1;
                        $msg = showMsg('FAIL_KENGEN', array(array($key, '行目', ':')));
                    }
                }
                if ($rtn === 0) {
                    $userSAUT = cmMer($userdata['WUSAUT']);
                    if ($userSAUT === '') {
                        $kengencheck = 0;
                    } else {
                        $kengencheck = 1;
                    }
                }
                if ($rtn === 0) {
                    $rs = fnCheckKenGen($db2con, $userdata['WUSAUT'], $kengencheck);
                    if ($rs === true) {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_CSVTAB', array(array($key, '行目', ':', '権限')));
                    }
                }
                if ($rtn === 0) {
                    $userLANG = cmMer($userdata['WULANG']);
                    if ($userLANG === '') {
                        $langcheck = 0;
                    } else {
                        $langcheck = 1;
                    }
                }
                if ($rtn === 0) {
                    $rs = fnCheckLANGID($db2con, $userdata['WULANG'], $langcheck);
                    if ($rs === true) {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_CSVTAB', array(array($key, '行目', ':', '言語')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($userdata['WUSERV'], 256)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', '標準ダウンロード先')));
                    }
                }
                if ($rtn === 0) {
                    //必須チェック
                    $userCTFL = cmMer($userdata['WUCTFL']);
                    if ($userCTFL === '' || $userdata['WUCTFL'] === '1') {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', 'タイムスタンプ')));
                    }
                }
                if ($rtn === 0) {
                    //必須チェック
                    $userLIBL = cmMer($userdata['WULIBL']);
                    if ($userLIBL === '') {
                        $libcheck = 0;
                    } else {
                        $libcheck = 1;
                    }
                }
                if ($rtn === 0) {
                    $userLIBL = cmMer($userdata['WULIBL']);
                    if ($userdata['WUAUTH'] === '0' && $userLIBL !== '') {
                        $rtn = 1;
                        $msg = showMsg('FAIL_LIBL', array(array($key, '行目', ':')));
                    }
                }
                if ($rtn === 0) {
                    $rs = fnCheckLIBL($db2con, $userdata['WULIBL'], $libcheck);
                    if ($rs === true) {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_CSVTAB', array(array($key, '行目', ':', 'ライブラリー')));
                    }
                }
                //ユーザが作成したクエリーのカラムチェック
                if ($rtn === 0) {
                    //必須チェック
                    $userUQFG = cmMer($userdata['WUUQFG']);
                    if ($userUQFG === '' || $userdata['WUUQFG'] === '1') {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', '作成したクエリーメニュー')));
                    }
                }
            }
        }
    }
    if ($flg === '1') {
        foreach ($readfile as $key1 => $groupdata) {
            $key = $key1 + 1;
            if (count($groupdata['WUGID']) !== 0) {
                $blankdata = '1';
                if ($rtn === 0) {
                    //必須チェック
                    $groupUID = cmMer($groupdata['WUUID']);
                    if ($groupUID == '') {
                        //$rtn = 1;
                        $msg = showMsg('FAIL_REQ', array(array($key, '行目', ':', 'ユーザー', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($groupdata['WUUID'], 10)) {
                        //$rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'ユーザー', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    $byte = checkByte($groupdata['WUUID']);
                    if ($byte[1] > 0) {
                        //$rtn = 1;
                        $msg = showMsg('FAIL_BYTE', array(array($key, '行目', ':', 'ユーザー', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    $rs = fnCheckUserID($db2con, $groupdata['WUUID']);
                    if ($rs === true) {
                        $rtn = 0;
                    } else {
                        //$rtn = 1;
                        $msg = showMsg('FAIL_CSVTAB', array(array($key, '行目', ':', 'ユーザー')));
                    }
                }
                if ($rtn === 0) {
                    $groupID = cmMer($groupdata['WUGID']);
                    if ($groupID === '') {
                        //$rtn = 1;
                        $msg = showMsg('FAIL_REQ', array(array($key, '行目', ':', 'グループ', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($groupdata['WUGID'], 10)) {
                        //$rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'グループ', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    $byte = checkByte($groupdata['WUGID']);
                    if ($byte[1] > 0) {
                        //$rtn = 1;
                        $msg = showMsg('FAIL_BYTE', array(array($key, '行目', ':', 'グループ', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    $rs = fnCheckGroupID($db2con, $groupdata['WUGID']);
                    if ($rs === true) {
                        $rtn = 0;
                    } else {
                        //$rtn = 1;
                        $msg = showMsg('FAIL_CSVTAB', array(array($key, '行目', ':', 'グループ')));
                    }
                }
           /*     $errors = array();
                $errors[] = $msg;
                if (count($errors) > 0) {
                    e_log('Count Error' . print_r(count($errors), true));
                    foreach ($errors as $e) {
                        $rtn = 1;
                        $msg = $e . "<br />";
                    }
                }   */
            }
        }
    }
    if ($flg === '2') {
        foreach ($readfile as $key1 => $querydata) {
            $key = $key1 + 1;
            if (count($querydata['WDNAME']) !== 0) {
                $blankdata = '1';
                if ($rtn === 0) {
                    //必須チェック
                    $queryUID = cmMer($querydata['WDUID']);
                    if ($queryUID === '') {
                        $rtn = 1;
                        $msg = showMsg('FAIL_REQ', array(array($key, '行目', ':', 'ユーザー', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($querydata['WDUID'], 10)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'ユーザー', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    $byte = checkByte($querydata['WDUID']);
                    if ($byte[1] > 0) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_BYTE', array(array($key, '行目', ':', 'ユーザー', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    $rs = fnCheckUserID($db2con, $querydata['WDUID']);
                    if ($rs === true) {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_CSVTAB', array(array($key, '行目', ':', 'ユーザー')));
                    }
                }
                if ($rtn === 0) {
                    //必須チェック
                    $queryID = cmMer($querydata['WDNAME']);
                    if ($queryID === '') {
                        $rtn = 1;
                        $msg = showMsg('FAIL_REQ', array(array($key, '行目', ':', 'クエリー', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($querydata['WDNAME'], 10)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'クエリー', 'ID')));
                    }
                }
                if ($rtn === 0) {
                    $rs = fnCheckQueryID($db2con, $querydata['WDNAME']);
                    if ($rs === true) {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_CSVTAB', array(array($key, '行目', ':', 'クエリー')));
                    }
                }
                if ($rtn === 0) {
                    $queryDL1 = cmMer($querydata['WDDWNL']);
                    if ($queryDL1 === '' || $querydata['WDDWNL'] === '1') {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', 'CSV', 'ダウンロード権限')));
                    }
                }
                if ($rtn === 0) {
                    $queryDL2 = cmMer($querydata['WDDWN2']);
                    if ($queryDL2 === '' || $querydata['WDDWN2'] === '1') {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', 'EXCEL', 'ダウンロード権限')));
                    }
                }
                if ($rtn === 0) {
                    if (!checkMaxLen($querydata['WDSERV'], 256)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($key, '行目', ':', 'クエリーダウンロード先')));
                    }
                }
                if ($rtn === 0) {
                    $queryCTFL = cmMer($querydata['WDCTFL']);
                    if ($queryCTFL === '' || $querydata['WDCTFL'] === '1') {
                        $rtn = 0;
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INSRT', array(array($key, '行目', ':', 'タイムスタンプ')));
                    }
                }
            }
        }
    }
    if ($blankdata === '0') {
        $rtn = 1;
        $msg = showMsg('FAIL_CSVERR');
    }
}
if ($flg === '0') {
    if ($rtn === 0) {
        $dupKey = '0';
        $key = '';
        for ($k = 1;$k <= count($readfile);$k++) {
            $str = trim($readfile[$k]['WUUID']);
            if ($str !== '') {
                for ($j = $k + 1;$j <= count($readfile);$j++) {
                    if ($str === trim($readfile[$j]['WUUID'])) {
                        $key = $j;
                        $dupKey = '2';
                        break;
                    }
                }
                if ($dupKey === '2') {
                    $rtn = 1;
                    $msg = showMsg('FAIL_DULP', array(array($key + 1, '行目', ':')));
                    break;
                }
            }
        }
    }
}
if ($flg === '1') {
    if ($rtn === 0) {
        $dupKey = '0';
        $key = '';
        for ($k = 1;$k <= count($readfile);$k++) {
            $str = trim($readfile[$k]['WUUID']) . trim($readfile[$k]['WUGID']);
            if ($str !== '') {
                for ($j = $k + 1;$j <= count($readfile);$j++) {
                    if ($str === trim($readfile[$j]['WUUID']) . trim($readfile[$j]['WUGID'])) {
                        $key = $j;
                        $dupKey = '2';
                        break;
                    }
                }
                if ($dupKey === '2') {
                    $rtn = 1;
                    $msg = showMsg('FAIL_DULP', array(array($key + 1, '行目', ':')));
                    break;
                }
            }
        }
    }
}
if ($flg === '2') {
    if ($rtn === 0) {
        $dupKey = '0';
        $key = '';
        for ($k = 1;$k <= count($readfile);$k++) {
            $str = trim($readfile[$k]['WDUID']) . trim($readfile[$k]['WDNAME']);
            if ($str !== '') {
                for ($j = $k + 1;$j <= count($readfile);$j++) {
                    if ($str === trim($readfile[$j]['WDUID']) . trim($readfile[$j]['WDNAME'])) {
                        $key = $j;
                        $dupKey = '2';
                        break;
                    }
                }
                if ($dupKey === '2') {
                    $rtn = 1;
                    $msg = showMsg('FAIL_DULP', array(array($key + 1, '行目', ':')));
                    break;
                }
            }
        }
    }
}
$kengen = false;
if ($flg === '0') {
    if ($rtn === 0) {
        //画面項目情報挿入
        if (count($readfile) > 0) {
            $rs = DeleteDB2WUSRB($db2con);
            if ($rs !== true) {
                $rtn = 1;
                $msg = showMsg('FAIL_DEL');
            } else {
                foreach ($readfile as $key1 => $userdata) {
                    $key = $key1 + 1;
                    $rs = insDB2WUSRB($db2con, $userdata);
                    if ($rs['result'] === true) {
                        if ($rs['flg'] === true) {
                            $kengen = true;
                        }
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_IMPORT', array(array($key, '行目')));
                    }
                }
            }
        }
        if ($kengen === false) {
            $rtn = 1;
            $msg = showMsg('FAIL_ADM');
        }
    }
}
if ($flg === '1') {
    if ($rtn === 0) {
        //画面項目情報挿入
        if (count($readfile) > 0) {
            $rs = DeleteDB2WUGRB($db2con);
            if ($rs !== true) {
                $rtn = 1;
                $msg = showMsg('FAIL_DEL');
            } else {
                foreach ($readfile as $key1 => $groupdata) {
                    $key = $key1 + 1;
                    $rs = insDB2WUGRB($db2con, $groupdata);
                    if ($rs['result'] !== true) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_IMPORT', array(array($key, '行目')));
                    }
                }
            }
        }
    }
}
if ($flg === '2') {
    if ($rtn === 0) {
        //画面項目情報挿入
        if (count($readfile) > 0) {
            $rs = DeleteDB2WDEFB($db2con);
            if ($rs !== true) {
                $rtn = 1;
                $msg = showMsg('FAIL_DEL');
            } else {
                foreach ($readfile as $key1 => $querydata) {
                    $key = $key1 + 1;
                    $rs = insDB2WDEFB($db2con, $querydata);
                    if ($rs['result'] !== true) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_IMPORT', array(array($key, '行目')));
                    }
                }
            }
        }
    }
}
if ($rtn === 1) {
    db2_rollback($db2con);
    //e_log('data rollback');
    
} else {
    db2_commit($db2con);
}
cmDb2Close($db2con);
$rtnAry = array('RTN' => $rtn, 'MSG' => $msg, 'success' => $success);
echo (json_encode($rtnAry));
//User****************************************************
function DeleteDB2WUSRB($db2con) {
    $rs = true;
    //構文
    $strSQL = 'DELETE FROM DB2WUSR';
    $params = array();
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_DEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_DEL';
            e_log("statement fail " . db2_stmt_errormsg());
        }
    }
    return $rs;
}
function DeleteDB2WUGRB($db2con) {
    $rs = true;
    //構文
    $strSQL = 'DELETE FROM DB2WUGR';
    $params = array();
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_DEL';
        e_log("statement fail " . db2_stmt_errormsg());
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_DEL';
            e_log("statement fail " . db2_stmt_errormsg());
        }
    }
    return $rs;
}
function DeleteDB2WDEFB($db2con) {
    $rs = true;
    //構文
    $strSQL = 'DELETE FROM DB2WDEF';
    $params = array();
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_DEL';
        e_log("statement fail " . db2_stmt_errormsg());
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_DEL';
            e_log("statement fail " . db2_stmt_errormsg());
        }
    }
    return $rs;
}
function insDB2WUSRB($db2con, $userdata) {
    $rs = true;
    $count = 0;
    $flg = false;
    if ($rs === true) {
        $sql = 'SET ENCRYPTION PASSWORD = \'' . RDB_KEY . '\'';
        $stmt = db2_prepare($db2con, $sql);
        if ($stmt === false) {
            $rs = false;
        } else {
            $r = db2_execute($stmt);
            if ($r === false) {
                $rs = false;
            }
        }
        if (count($userdata['WUUNAM']) !== 0) {
            $strSQL = ' INSERT ';
            /* $strSQL.= ' INTO DB2WUSR (WUUID,WUUNAM,WUAUTH,WUDWNL,WUDWN2,WUCLR1,WUCLR2,WUCLR3,WUCLR4,WUCLR5,WULANG,WUPSWE,WUSIZE,WUEDAY,WULGNC,WUSAUT,WUSERV,WUCTFL,WULIBL,WUUQFG) ';
            $strSQL.= ' VALUES (?,?,?,?,?,?,?,?,?,?,?,CAST(encrypt(CAST(? AS VARCHAR(10))) AS CHAR(32) FOR BIT DATA),?,?,?,?,?,?,?,?) ';
            $params = array($userdata['WUUID'], $userdata['WUUNAM'], $userdata['WUAUTH'], $userdata['WUDWNL'], $userdata['WUDWN2'], $userdata['WUCLR1'], $userdata['WUCLR2'], $userdata['WUCLR3'], $userdata['WUCLR4'], $userdata['WUCLR5'], $userdata['WULANG'], $userdata['WUPSWE'], $userdata['WUSIZE'], str_replace("/", "", $userdata['WUEDAY']), $userdata['WULGNC'], $userdata['WUSAUT'], $userdata['WUSERV'], $userdata['WUCTFL'], $userdata['WULIBL'], $userdata['WUUQFG']);*/

            $strSQL.= ' INTO DB2WUSR (WUUID,WUUNAM,WUAID,WUAUTH,WUDWNL,WUDWN2,WUCLR1,WUCLR2,WUCLR3,WUCLR4,WUCLR5,WULANG,WUPSWE,WUSIZE,WUEDAY,WULGNC,WUSAUT,WUSERV,WUCTFL,WULIBL,WUUQFG) ';
            $strSQL.= ' VALUES (?,?,?,?,?,?,?,?,?,?,?,?,CAST(encrypt(CAST(? AS VARCHAR(20))) AS CHAR(64) FOR BIT DATA),?,?,?,?,?,?,?,?) ';
            $params = array($userdata['WUUID'], $userdata['WUUNAM'], $userdata['WUAID'], $userdata['WUAUTH'], $userdata['WUDWNL'], $userdata['WUDWN2'], $userdata['WUCLR1'], $userdata['WUCLR2'], $userdata['WUCLR3'], $userdata['WUCLR4'], $userdata['WUCLR5'], $userdata['WULANG'], $userdata['WUPSWE'], $userdata['WUSIZE'], str_replace("/", "", $userdata['WUEDAY']), $userdata['WULGNC'], $userdata['WUSAUT'], $userdata['WUSERV'], $userdata['WUCTFL'], $userdata['WULIBL'], $userdata['WUUQFG']);
            //GIGI ADD WUAID

			$stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rs = 'FAIL_UPD';
                e_log("statement fail " . db2_stmt_errormsg());
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    e_log("statement fail " . db2_stmt_errormsg());
                    $rs = 'FAIL_UPD';
                } else {
                    $rs = true;
                    $data = $userdata['WUAUTH'];
                    if ($data === '1') {
                        $count++;
                    }
                }
            }
        }
    }
    if ($count > 0) {
        $flg = true;
    }
    return array('result' => $rs, 'flg' => $flg);
}
function insDB2WUGRB($db2con, $groupdata) {
    $rs = true;
    if ($rs === true) {
        if (count($groupdata['WUGID']) !== 0) {
            $strSQL = ' INSERT ';
            $strSQL.= ' INTO DB2WUGR (WUUID,WUGID) ';
            $strSQL.= ' VALUES (?,?) ';
            $params = array($groupdata['WUUID'], $groupdata['WUGID']);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rs = 'FAIL_UPD';
                e_log("statement fail " . db2_stmt_errormsg());
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    e_log("statement fail " . db2_stmt_errormsg());
                    $rs = 'FAIL_UPD';
                } else {
                    $rs = true;
                }
            }
        }
    }
    return array('result' => $rs);
}
function insDB2WDEFB($db2con, $querydata) {
    $rs = true;
    if ($rs === true) {
        if (count($querydata['WDNAME']) !== 0) {
            $strSQL = ' INSERT ';
            /*$strSQL.= ' INTO DB2WDEF (WDUID,WDNAME,WDDWNL,WDDWN2,WDSERV,WDCTFL) ';
            $strSQL.= ' VALUES (?,?,?,?,?,?) ';
            $params = array($querydata['WDUID'], $querydata['WDNAME'], $querydata['WDDWNL'], $querydata['WDDWN2'], $querydata['WDSERV'], $querydata['WDCTFL']);*/
            $strSQL.= ' INTO DB2WDEF (WDUID,WDNAME,WDDWNL,WDDWN2,WDSERV,WDCTFL,WDCNFL) ';//GIGI ADD WDCNFL
            $strSQL.= ' VALUES (?,?,?,?,?,?,?) ';//GIGI ADD WDCNFL
            $params = array($querydata['WDUID'], $querydata['WDNAME'], $querydata['WDDWNL'], $querydata['WDDWN2'], $querydata['WDSERV'], $querydata['WDCTFL'], $querydata['WDCNFL']);//GIGI ADD WDCNFL
			$stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rs = 'FAIL_UPD';
                e_log("statement fail " . db2_stmt_errormsg());
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    e_log("statement fail " . db2_stmt_errormsg());
                    $rs = 'FAIL_UPD';
                } else {
                    $rs = true;
                }
            }
        }
    }
    return array('result' => $rs);
}
//*********************************Check************************************
function fnCheckLIBL($db2con, $userlibl, $libcheck) {
    $data = array();
    $rs = true;
    $strSQL = ' SELECT LIBLNM ';
    $strSQL.= ' FROM  DB2LIBL ';
    $strSQL.= ' WHERE LIBLNM = ? ';
    $params = array($userlibl);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if ($libcheck === 0) {
                $rs = true;
            } else {
                if (count($data) === 0) {
                    $rs = false;
                }
            }
        }
    }
    return $rs;
}
function fnCheckKenGen($db2con, $kengen, $kengencheck) {
    $data = array();
    $rs = true;
    $strSQL = ' SELECT AMNAME ';
    $strSQL.= ' FROM  DB2AMST ';
    $strSQL.= ' WHERE AMNAME = ? ';
    $params = array($kengen);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if ($kengencheck === 0) {
                $rs = true;
            } else {
                if (count($data) === 0) {
                    $rs = false;
                }
            }
        }
    }
    return $rs;
}
function fnCheckUserID($db2con, $userid) {
    $data = array();
    $rs = true;
    $strSQL = ' SELECT WUUID ';
    $strSQL.= ' FROM  DB2WUSR ';
    $strSQL.= ' WHERE WUUID = ? ';
    $params = array($userid);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $rs = false;
            }
        }
    }
    return $rs;
}
function fnCheckGroupID($db2con, $groupid) {
    $data = array();
    $rs = true;
    $strSQL = ' SELECT WQGID  ';
    $strSQL.= ' FROM  DB2WQGR ';
    $strSQL.= ' WHERE WQGID  = ? ';
    $params = array($groupid);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $rs = false;
            }
        }
    }
    return $rs;
}
function fnCheckQueryID($db2con, $queryid) {
    $data = array();
    $rs = true;
    $strSQL = ' SELECT D1NAME   ';
    $strSQL.= ' FROM  FDB2CSV1 ';
    $strSQL.= ' WHERE D1NAME   = ? ';
    $params = array($queryid);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $rs = false;
            }
        }
    }
    return $rs;
}
function fnCheckLANGID($db2con, $langid, $langcheck) {
    $data = array();
    $rs = true;
    $strSQL = ' SELECT LANID ';
    $strSQL.= ' FROM  DB2LANG ';
    $strSQL.= ' WHERE LANID = ? ';
    $params = array($langid);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if ($langcheck === 0) {
                $rs = true;
            } else {
                if (count($data) === 0) {
                    $rs = false;
                }
            }
        }
    }
    return $rs;
}
