<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("getQryTblData.php");
include_once ("insQryTblData.php");
include_once ("chkQryTblData.php");
include_once ("chkDB2WGDF.php");
include_once ("chkDB2HTML.php");
include_once ("chkXlsOpt.php");
include_once ("chkSeigyoOpt.php");
include_once ("chkPivotOpt.php");
include_once ("chkShosaiDrillDownOpt.php");
include_once ("chkDwnKengenOpt.php");
include_once ("chkScheduleOpt.php");
include_once ("chkClOpt.php");
include_once ("chkGraphOpt.php");
require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
$csvfile = (isset($_POST['csvfile'])) ? $_POST['csvfile'] : '';
$USERFLG = (isset($_POST['USERFLG'])) ? $_POST['USERFLG'] : '';
/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$filename = '';
$data = array();
$libcheck = 1;
$kengencheck = 1;
$langcheck = 1;
$msg = '';
$success = true;
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
$rtn = 0;
$csvblankcheck = '0';
if ($rtn === 0) {
    //DB接続（no exist lib data)
    $db2con = cmDb2Con();
    if ($db2con === false) {
        $rtn = 1;
        $msg = showMsg('FAIL_DBCON');
    }
    cmSetPHPQUERY($db2con);
}
//オートコミットをOFFにする
if ($rtn === 0) {
    $res = db2_autocommit($db2con, DB2_AUTOCOMMIT_OFF);
    if ($res === false) {
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if ($rtn === 0) {
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    } else {
        $userData = umEx($rs['data']);
        if ($userData[0]['WUAUTH'] === '2') {
            $rs = cmChkKenGen($db2con, '33', $userData[0]['WUSAUT']); //'33' => Import
            if ($rs['result'] !== true) {
                $rtn = 2;
                $msg = showMsg($rs['result'], array('インポートの権限'));
            }
        }
    }
}
//ファイル名をもらう
if (isset($_FILES)) {
    $csvfile = $_FILES['csvfile']['name'];
    if ($csvfile !== '') {
        $ext = explode('.', $csvfile);
        $ext = $ext[count($ext) - 1];
        $filename = $_FILES['csvfile']['tmp_name'];
        $csvfile = $csvfile;
    } else {
        $csvfile = '';
    }
}
//variable array
$FDB2CSV1 = array();
$FDB2CSV2 = array();
$FDB2CSV5 = array();
$BSUMFLD = array();
$BREFTBL = array();
$BREFFLD = array();
$BQRYCND = array();
$BCNDDAT = array();
$BSQLCND = array();
$BSQLDAT = array();
$DB2WGDF = array();
$DB2HTMLT = array();
$DB2ECON = array();
$DB2EINS = array();
$DB2WDTL = array();
$DB2WDFL = array();
$DB2DRGS = array();
$DB2WDEF = array();
$DB2COLM = array();
$DB2COLT = array();
$DB2PMST = array();
$DB2PCOL = array();
$DB2PCAL = array();
$DB2WSCD = array();
$DB2WAUT = array();
$DB2WAUL = array();
$DB2WSOC = array();
$FDB2CSV1PG = array();
$FDB2CSV1PM = array();
$DB2GPK = array();
$DB2GPC = array();
//ファイルから、データをもらう
//$delimiter = "\t";
$delimiter = ",";
if (($handle = fopen($filename, "r")) !== FALSE) {
    $fieldInfo = array();
    $filedInfoInner = array();
    $keyData = '';
    $rowNo = 0;
    while (($data = fgetcsv($handle, 0, $delimiter, '"')) !== FALSE) {
        $num = count($data);
        $fieldData = array();
        $tblName = cmMer($data[0]);
        //TBのクエリー作成
        if ($tblName === 'FDB2CSV1') {
            $fieldData = getColFieldFDB2CSV1($data);
            $FDB2CSV1[] = $fieldData;
        } else if ($tblName === 'FDB2CSV2') {
            $fieldData = getColFieldFDB2CSV2($data);
            $FDB2CSV2[] = $fieldData;
        } else if ($tblName === 'FDB2CSV5') {
            $fieldData = getColFieldFDB2CSV5($data);
            $FDB2CSV5[] = $fieldData;
        } else if ($tblName === 'BSUMFLD') {
            $fieldData = getColFieldBSUMFLD($data);
            $BSUMFLD[] = $fieldData;
        } else if ($tblName === 'BREFTBL') {
            $fieldData = getColFieldBREFTBL($data);
            $BREFTBL[] = $fieldData;
        } else if ($tblName === 'BREFFLD') {
            $fieldData = getColFieldBREFFLD($data);
            $BREFFLD[] = $fieldData;
        } else if ($tblName === 'BQRYCND') {
            $fieldData = getColFieldBQRYCND($data);
            $BQRYCND[] = $fieldData;
        } else if ($tblName === 'BCNDDAT') {
            $fieldData = getColFieldBCNDDAT($data);
            $BCNDDAT[] = $fieldData;
        } else if ($tblName === 'FDB2CSV4') {
            $fieldData = getColFieldFDB2CSV4($data);
            $FDB2CSV4[] = $fieldData;
        } else if ($tblName === 'FDB2CSV3') {
            $fieldData = getColFieldFDB2CSV3($data);
            $FDB2CSV3[] = $fieldData;
        } else if ($tblName === 'BSQLCND') {
            $fieldData = getColFieldBSQLCND($data);
            $BSQLCND[] = $fieldData;
        } else if ($tblName === 'BSQLDAT') {
            $fieldData = getColFieldBSQLDAT($data);
            $BSQLDAT[] = $fieldData;
        } else if ($tblName === 'DB2WGDF') { //TBのオプションデータ
            $fieldData = getColFieldDB2WGDF($data); //グループと権限設定
            $DB2WGDF[] = $fieldData;
        } else if ($tblName === 'DB2HTMLT') {
            $fieldData = getColFieldDB2HTMLT($data); // HTMLテンプレート設定
            $DB2HTMLT[] = $fieldData;
        } else if ($tblName === 'DB2ECON') {
            $fieldData = getColFieldDB2ECON($data); // EXCELテンプレート設定
            $DB2ECON[] = $fieldData;
        } else if ($tblName === 'DB2EINS') {
            $fieldData = getColFieldDB2EINS($data); // EXCELテンプレート設定
            $DB2EINS[] = $fieldData;
        } else if ($tblName === 'DB2WDTL') {
            $fieldData = getColFieldDB2WDTL($data); // 詳細情報設定 shosai
            $DB2WDTL[] = $fieldData;
        } else if ($tblName === 'DB2WDFL') {
            $fieldData = getColFieldDB2WDFL($data); // 詳細情報設定 shosai
            $DB2WDFL[] = $fieldData;
        } else if ($tblName === 'DB2DRGS') {
            $fieldData = getColFieldDB2DRGS($data); // 詳細情報設定 drilldown
            $DB2DRGS[] = $fieldData;
        } else if ($tblName === 'DB2WDEF') {
            $fieldData = getColFieldDB2WDEF($data); // ダウンロード権限設定
            $DB2WDEF[] = $fieldData;
        } else if ($tblName === 'DB2COLM') {
            $fieldData = getColFieldDB2COLM($data); // 制御レベール設定
            $DB2COLM[] = $fieldData;
        } else if ($tblName === 'DB2COLT') {
            $fieldData = getColFieldDB2COLT($data); // 制御レベール設定
            $DB2COLT[] = $fieldData;
        } else if ($tblName === 'DB2PMST') {
            $fieldData = getColFieldDB2PMST($data); // ピボット設定
            $DB2PMST[] = $fieldData;
        } else if ($tblName === 'DB2PCOL') {
            $fieldData = getColFieldDB2PCOL($data); // ピボット設定
            $DB2PCOL[] = $fieldData;
        } else if ($tblName === 'DB2PCAL') {
            $fieldData = getColFieldDB2PCAL($data); // ピボット設定
            $DB2PCAL[] = $fieldData;
        } else if ($tblName === 'DB2WSCD') {
            $fieldData = getColFieldDB2WSCD($data); // スケジュール設定
            $DB2WSCD[] = $fieldData;
        } else if ($tblName === 'DB2WAUT') {
            $fieldData = getColFieldDB2WAUT($data); // スケジュール設定
            $DB2WAUT[] = $fieldData;
        } else if ($tblName === 'DB2WAUL') {
            $fieldData = getColFieldDB2WAUL($data); // スケジュール設定
            $DB2WAUL[] = $fieldData;
        } else if ($tblName === 'DB2WSOC') {
            $fieldData = getColFieldDB2WSOC($data); // スケジュール設定
            $DB2WSOC[] = $fieldData;
        } else if ($tblName === 'FDB2CSV1PG') {
            $fieldData = getColFieldFDB2CSV1PG($data); // CL連携設定
            $FDB2CSV1PG[] = $fieldData;
        } else if ($tblName === 'FDB2CSV1PM') {
            $fieldData = getColFieldFDB2CSV1PM($data); // CL連携設定
            $FDB2CSV1PM[] = $fieldData;
        } else if ($tblName === 'DB2GPK') {
            $fieldData = getColFieldDB2GPK($data); // グラフ設定
            $DB2GPK[] = $fieldData;
        } else if ($tblName === 'DB2GPC') {
            $fieldData = getColFieldDB2GPC($data); // グラフ設定
            $DB2GPC[] = $fieldData;
        }
        $fieldInfo[$rowNo] = $fieldData;
        $rowNo++;
    }
    fclose($handle);
}
if ($rtn === 0) {
    //TBのクエリー作成  チェック
    if (count($FDB2CSV1) > 0) {
        e_log(print_r($FDB2CSV1,true));
        $chkdata = chkBSqlTypeFDB2CSV1($db2con, $FDB2CSV1);
        if ($chkdata['result'] !== true) {
            $rtn = 1;
            $msg = $chkdata['msg'];
        }
    }
    //TBのクエリー作成保存
    if ($rtn === 0) {
        if (count($FDB2CSV1) > 0) {
            $resIns = insFDB2CSV1($db2con, $FDB2CSV1);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'FDB2CSV1登録完了.';
            }
        }
    }
    //$BREFTBL,$BREFFLD 普通のクエリーのジョンチェック
    if ($rtn === 0) {
        if (count($BREFTBL) > 0 || count($BREFFLD) > 0) {
            $chkdata = chkBSqlTypeJoinQry($db2con, $BREFTBL, $BREFFLD);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //$BREFTBL保存
    if ($rtn === 0) {
        if (count($BREFTBL) > 0) {
            $resIns = insBREFTBL($db2con, $BREFTBL);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'BREFTBL登録完了.';
            }
        }
    }
    //$BREFFLD保存
    if ($rtn === 0) {
        if (count($BREFFLD) > 0){
            $resIns = insBREFFLD($db2con, $BREFFLD);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'BREFTBL登録完了.';
            }
        }
    }
    //FDB2CSV4 黒い画面のジョンチェック
    if ($rtn === 0) {
        if (count($FDB2CSV4) > 0) {
            $chkdata = chkBSqlTypeFDB2CSV4($db2con, $FDB2CSV4);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //FDB2CSV4
    if ($rtn === 0) {
        if (count($FDB2CSV4) > 0) {
            $resIns = insFDB2CSV4($db2con, $FDB2CSV4);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'FDB2CSV4登録完了.';
            }
        }
    }
    //FDB2CSV2カラムチェック
    if ($rtn === 0) {
        if (count($FDB2CSV2) > 0) {
            $chkdata = chkBSqlTypeFDB2CSV2($db2con, $FDB2CSV2);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //FDB2CSV2カラム保存
    if ($rtn === 0) {
        if (count($FDB2CSV2) > 0) {
            $resIns = insFDB2CSV2($db2con, $FDB2CSV2);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'FDB2CSV2登録完了.';
            }
        }
    }
    //FDB2CSV5チェック
    if ($rtn === 0) {
        if (count($FDB2CSV5) > 0) {
            $chkdata = chkBSqlTypeFDB2CSV5($db2con, $FDB2CSV5);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //FDB2CSV5保存
    if ($rtn === 0) {
        if (count($FDB2CSV5) > 0) {
            $resIns = insFDB2CSV5($db2con, $FDB2CSV5);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'FDB2CSV5登録完了.';
            }
        }
    }
    //BSUMFLDチェック
    if ($rtn === 0) {
        if (count($BSUMFLD) > 0) {
            $chkdata = chkBSqlTypeBSUMFLD($db2con, $BSUMFLD);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //BSUMFLD保存
    if ($rtn === 0) {
        if (count($BSUMFLD) > 0) {
            $resIns = insBSUMFLD($db2con, $BSUMFLD);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'BSUMFLD登録完了.';
            }
        }
    }
    //FDB2CSV3チェック
    if ($rtn === 0) {
        if (count($FDB2CSV3) > 0) {
            $chkdata = chkBSqlTypeSearchKuroi($db2con, $FDB2CSV3);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //FDB2CSV3保存
    if ($rtn === 0) {
        if (count($FDB2CSV3) > 0) {
            if ($rtn === 0) {
                $resIns = insFDB2CSV3($db2con, $FDB2CSV3);
                if ($resIns['result'] !== true) {
                    $rtn = 1;
                    $msg = $resIns['errcd'];
                } else {
                    $rtn = 0;
                    $msg = 'FDB2CSV3登録完了.';
                }
            }
        }
    }
    //BQRYCND,BCNDDATチェック
    if ($rtn === 0) {
        if (count($BQRYCND) > 0 || count($BCNDDAT) > 0) {
            $chkdata = chkBSqlTypeSearchQry($db2con, $BQRYCND, $BCNDDAT);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //BQRYCND,BCNDDAT保存
    if ($rtn === 0) {
        if (count($BQRYCND) > 0) {
            $resIns = insBQRYCND($db2con, $BQRYCND);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'BQRYCND登録完了.';
            }
        }
    }
    if ($rtn === 0) {
        if (count($BCNDDAT) > 0) {
            $resIns = insBCNDDAT($db2con, $BCNDDAT);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'BCNDDAT登録完了.';
            }
        }
    }
    //BSQLDAT,BSQLCNDチェック
    if ($rtn === 0) {
        if (count($BSQLDAT) > 0 || count($BSQLCND) > 0) {
            $chkdata = chkBSqlTypeSearchSQL($db2con, $BSQLDAT, $BSQLCND);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //BSQLDAT,BSQLCND保存
    if ($rtn === 0) {
        if (count($BSQLDAT) > 0) {
            $resIns = insBSQLDAT($db2con, $BSQLDAT);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'BSQLDAT登録完了.';
            }
        }
    }
    if ($rtn === 0) {
        if (count($BSQLCND) > 0) {
            $resIns = insBSQLCND($db2con, $BSQLCND);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'BSQLCND登録完了.';
            }
        }
    }
    //グループ チェック
    if ($rtn === 0) {
        if (count($DB2WGDF) > 0) {
            $chkdata = chkBSqlTypeGroupOpt($db2con, $DB2WGDF);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //グループ 保存
    if ($rtn === 0) {
        if (count($DB2WGDF) > 0) {
            $resIns = insDB2WGDF($db2con, $DB2WGDF);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'DB2WGDF登録完了.';
            }
        }
    }
    //DB2HTMLTチェック
    if ($rtn === 0) {
        if (count($DB2HTMLT) > 0) {
            $chkdata = chkBSqlTypeHtmlOpt($db2con, $DB2HTMLT);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //DB2HTMLT  保存
    if ($rtn === 0) {
        if (count($DB2HTMLT) > 0) {
            if ($rtn === 0) {
                $resIns = insDB2HTMLT($db2con, $DB2HTMLT);
                if ($resIns['result'] !== true) {
                    $rtn = 1;
                    $msg = $resIns['errcd'];
                } else {
                    $rtn = 0;
                    $msg = 'DB2HTMLT登録完了.';
                }
            }
        }
    }
    //xlsチェック
    if ($rtn === 0) {
        if (count($DB2ECON) > 0) {
            $chkdata = chkBSqlTypeXlsOpt($db2con, $DB2ECON, $DB2EINS);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //xls保存
    if ($rtn === 0) {
        if (count($DB2ECON) > 0) {
            $resIns = insDB2ECON($db2con, $DB2ECON);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'DB2ECON登録完了.';
            }
        }
    }
    if ($rtn === 0) {
        if (count($DB2EINS) > 0) {
            $resIns = insDB2EINS($db2con, $DB2EINS);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'DB2EINS登録完了.';
            }
        }
    }
    // 制御レベール設定チェック
    if ($rtn === 0) {
        if (count($DB2COLM) > 0) {
            $chkdata = chkBSqlTypeSeigyoOpt($db2con, $DB2COLM, $DB2COLT);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    // 制御レベール設定保存
    if ($rtn === 0) {
        if (count($DB2COLM) > 0) {
            $resIns = insDB2COLM($db2con, $DB2COLM);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'DB2COLM登録完了.';
            }
        }
    }
    if ($rtn === 0) {
        if (count($DB2COLT) > 0) {
            $resIns = insDB2COLT($db2con, $DB2COLT);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'DB2EINS登録完了.';
            }
        }
    }
    // ピボット設定
    if ($rtn === 0) {
        if (count($DB2PMST) > 0) {
            $chkdata = chkBSqlTypePivotOpt($db2con, $DB2PMST, $DB2PCOL, $DB2PCAL);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    // ピボット設定保存
    if ($rtn === 0) {
        if (count($DB2PMST) > 0) {
            $resIns = insDB2PMST($db2con, $DB2PMST);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'DB2PMST登録完了.';
            }
        }
    }
    if ($rtn === 0) {
        if (count($DB2PCOL) > 0) {
            $resIns = insDB2PCOL($db2con, $DB2PCOL);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'DB2PCOL登録完了.';
            }
        }
    }
    if ($rtn === 0) {
        if (count($DB2PCAL) > 0) {
            $resIns = insDB2PCAL($db2con, $DB2PCAL);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'DB2PCAL登録完了.';
            }
        }
    }
    // 詳細情報設定チェック
    if ($rtn === 0) {
        if (count($DB2WDTL) > 0 || count($DB2WDFL) > 0 || count($DB2DRGS) > 0) {
            $chkdata = chkBSqlTypeShosaiDrillOpt($db2con, $DB2WDTL, $DB2WDFL, $DB2DRGS);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //詳細情報設定保存
    if ($rtn === 0) {
        if (count($DB2WDTL) > 0) {
            $resIns = insDB2WDTL($db2con, $DB2WDTL);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'DB2WDTL登録完了.';
            }
        }
    }
    if ($rtn === 0) {
        if (count($DB2WDFL) > 0) {
            $resIns = insDB2WDFL($db2con, $DB2WDFL);
            if ($resIns['result'] !== true) {
                $rtn = 1;
                $msg = $resIns['errcd'];
            } else {
                $rtn = 0;
                $msg = 'DB2WDFL登録完了.';
            }
        }
    }
    if ($rtn === 0) {
        if (count($DB2DRGS) > 0) {
            if ($rtn === 0) {
                $resIns = insDB2DRGS($db2con, $DB2DRGS);
                if ($resIns['result'] !== true) {
                    $rtn = 1;
                    $msg = $resIns['errcd'];
                } else {
                    $rtn = 0;
                    $msg = 'DB2DRGS登録完了.';
                }
            }
        }
    }
    //ダウンロード権限設定チェック
    if ($rtn === 0) {
        if (count($DB2WDEF) > 0) {
            $chkdata = chkBSqlTypeDwnKengenOpt($db2con, $DB2WDEF);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //ダウンロード権限設定保存
    if ($rtn === 0) {
        if (count($DB2WDEF) > 0) {
            if ($rtn === 0) {
                $resIns = insDB2WDEF($db2con, $DB2WDEF);
                if ($resIns['result'] !== true) {
                    $rtn = 1;
                    $msg = $resIns['errcd'];
                } else {
                    $rtn = 0;
                    $msg = 'DB2WDEF登録完了.';
                }
            }
        }
    }
    //schedule設定チェック
    if ($rtn === 0) {
        if (count($DB2WSCD) > 0 || count($DB2WAUT) > 0 || count($DB2WAUL) > 0 || count($DB2WSOC) > 0) {
            $chkdata = chkBSqlTypeScheduleOpt($db2con, $DB2WSCD, $DB2WAUT, $DB2WAUL, $DB2WSOC);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //schedule設定保存
    if ($rtn === 0) {
        //DB2WSCD
        if (count($DB2WSCD) > 0) {
            if ($rtn === 0) {
                $resIns = insDB2WSCD($db2con, $DB2WSCD);
                if ($resIns['result'] !== true) {
                    $rtn = 1;
                    $msg = $resIns['errcd'];
                } else {
                    $rtn = 0;
                    $msg = 'DB2WSCD登録完了.';
                }
            }
        }
        //DB2WAUT
        if ($rtn === 0) {
            if (count($DB2WAUT) > 0) {
                if ($rtn === 0) {
                    $resIns = insDB2WAUT($db2con, $DB2WAUT);
                    if ($resIns['result'] !== true) {
                        $rtn = 1;
                        $msg = $resIns['errcd'];
                    } else {
                        $rtn = 0;
                        $msg = 'DB2WAUT登録完了.';
                    }
                }
            }
        }
        //DB2WAUL
        if ($rtn === 0) {
            if (count($DB2WAUL) > 0) {
                if ($rtn === 0) {
                    $resIns = insDB2WAUL($db2con, $DB2WAUL);
                    if ($resIns['result'] !== true) {
                        $rtn = 1;
                        $msg = $resIns['errcd'];
                    } else {
                        $rtn = 0;
                        $msg = 'DB2WAUL登録完了.';
                    }
                }
            }
        }
        //DB2WSOC
        if ($rtn === 0) {
            if (count($DB2WSOC) > 0) {
                if ($rtn === 0) {
                    $resIns = insDB2WSOC($db2con, $DB2WSOC);
                    if ($resIns['result'] !== true) {
                        $rtn = 1;
                        $msg = $resIns['errcd'];
                    } else {
                        $rtn = 0;
                        $msg = 'DB2WSOC登録完了.';
                    }
                }
            }
        }
    }
    //cl設定チェック
    if ($rtn === 0) {
        if (count($FDB2CSV1PG) > 0) {
            $chkdata = chkBSqlTypeClOpt($db2con, $FDB2CSV1PG, $FDB2CSV1PM);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //cl設定保存
    if ($rtn === 0) {
        if (count($FDB2CSV1PG) > 0) {
            if ($rtn === 0) {
                if (count($FDB2CSV1PG) > 0) {
                    $resIns = insFDB2CSV1PG($db2con, $FDB2CSV1PG);
                    if ($resIns['result'] !== true) {
                        $rtn = 1;
                        $msg = $resIns['errcd'];
                    } else {
                        $rtn = 0;
                        $msg = 'FDB2CSV1PG登録完了.';
                    }
                }
            }
            if ($rtn === 0) {
                if (count($FDB2CSV1PM) > 0) {
                    $resIns = insFDB2CSV1PM($db2con, $FDB2CSV1PM);
                    if ($resIns['result'] !== true) {
                        $rtn = 1;
                        $msg = $resIns['errcd'];
                    } else {
                        $rtn = 0;
                        $msg = 'FDB2CSV1PM登録完了.';
                    }
                }
            }
        }
    }
    //グラフ設定チェック
    if ($rtn === 0) {
        if (count($DB2GPK) > 0) {
            $chkdata = chkBSqlTypeGphOpt($db2con, $DB2GPK, $DB2GPC);
            if ($chkdata['result'] !== true) {
                $rtn = 1;
                $msg = $chkdata['msg'];
            }
        }
    }
    //グラフ設定保存
    if ($rtn === 0) {
        if (count($DB2GPK) > 0) {
            if ($rtn === 0) {
                if (count($DB2GPK) > 0) {
                    $resIns = insDB2GPK($db2con, $DB2GPK);
                    if ($resIns['result'] !== true) {
                        $rtn = 1;
                        $msg = $resIns['errcd'];
                    } else {
                        $rtn = 0;
                        $msg = 'DB2GPK登録完了.';
                    }
                }
            }
            if ($rtn === 0) {
                if (count($DB2GPC) > 0) {
                    $resIns = insDB2GPC($db2con, $DB2GPC);
                    if ($resIns['result'] !== true) {
                        $rtn = 1;
                        $msg = $resIns['errcd'];
                    } else {
                        $rtn = 0;
                        $msg = 'DB2GPC登録完了.';
                    }
                }
            }
        }
    }
}
if ($rtn === 1) {
    db2_rollback($db2con);
} else {
    db2_commit($db2con);
}
cmDb2Close($db2con);
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'success' => $success
);
echo (json_encode($rtnAry));
