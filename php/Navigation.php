<?php
include_once ("licenseInfo.php");
include_once ("common/inc/config.php");
include_once ("common/inc/common.inc.php");
//$licenseType = 'FREE';
/*
 *-------------------------------------------------------*
 * リクエスト
 *-------------------------------------------------------*
*/
$touch = (isset($_POST['TOUCH']) ? $_POST['TOUCH'] : '0');
/*
 *-------------------------------------------------------*
 * 共通変数
 *-------------------------------------------------------*
*/
$side_Schedule = array();
$side_Group = array();
$side_NoGroup = array();
//ユーザが作成したクエリーのメニュー用
$sie_user_query = array();
//お気に入りに入れられたクエリー、ピボット、グラフ
$side_bookmark_query = array();
$FDB2CSV1 = array();
$dname = array();
$tabledata = array();
$rtn = array();
$scheduleFlg = false;
$exelogFlg = false;

$PROC = (isset($_POST ["PROC"]) ? $_POST ["PROC"] : '');//MSM add
$SEARCHKEY = (isset($_POST ["SEARCHKEY"]) ? $_POST ["SEARCHKEY"] : '');

/*
 *-------------------------------------------------------*
 * データ取得
 *-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$usrinfo = array();
if ($touch == '1') {
    $usrinfo = $_SESSION['PHPQUERYTOUCH']['user'];
    $scheduleFlg = $licenseSchedule;
    $pivotFlg = $licensePivot;
    $exelogFlg = $licenseLog;
    $graphFlg = $licenseGraph;
    $ifsFlg = $licenseIFS;
} else {
    $usrinfo = $_SESSION['PHPQUERY']['user'];
    $scheduleFlg = $licenseSchedule;
    $pivotFlg = $licensePivot;
    $exelogFlg = $licenseLog;
    $graphFlg = $licenseGraph;
    $ifsFlg = $licenseIFS;
}
$WUAUTH = '';
$WUUID = $usrinfo[0]['WUUID'];
//$rs = cmGetWUAUTH($db2con,$WUUID);
$rs = cmCheckUser($db2con, $WUUID);
if ($rs['result'] !== true) {
    $rtn = 1;
    $msg = showMsg($rs['result'], array('ユーザー'));
} else {
    $WUAUTH = $rs['data'][0]['WUAUTH'];
}
/**
 *①navigationDataというのはメニューリスト
 *②side_GroupNavというのはグループの中でクエリーリスト
 */
$FDB2CSV1 = cmGetFDB2CSV1($db2con, $rs);
$navigationData = fnGetNavigation($db2con, $WUUID, $WUAUTH, $licenseSchedule, $licenseCl, $licenseLog, $licenseQueryGroup, $ifsFlg);//190308

//ダッシュボードのメニュー
$side_Dashboard = array();
$DB2DSBLIST = fnGetDB2DSBList($db2con, $WUUID);
if ($DB2DSBLIST['result'] === true) {
    $side_Dashboard = $DB2DSBLIST['data'];
}
$side_Schedule = fnGetFile($db2con);
//ユーザに対してのクエリーの取得
//メニューに出す用
$DB2QHISList = fnGetDB2QHIS($db2con, $WUUID, $pivotFlg, $graphFlg,$licenseSql);
if ($DB2QHISList['result'] === true) {
    $side_user_query = $DB2QHISList['data'];
}
$DB2QBMKList = fnGetDB2QBMK($db2con, $WUUID, $pivotFlg, $graphFlg,$licenseSql);
if ($DB2QBMKList['result'] === true) {
    $side_bookmark_query = $DB2QBMKList['data'];
}
//
//error_log("side_bookmark_query".print_r($side_bookmark_query,true));
$side_GroupNav = fnGetNavGroup($db2con, $WUUID, $touch, $pivotFlg, $graphFlg, $licenseQueryGroup,$licenseSql);//190308
//error_log("side_GroupNav Data1**".print_r($side_GroupNav,true));
$side_NoGroup = cmGetNoGroup($db2con, $rs);
$kengenList = array();
$kengenListCount = 0;
//権限情報取得
if ($WUAUTH === '2') {
    $kengen = fnKengenList($db2con, $WUUID);
    if ($kengen['result'] !== false) {
        $kengenList = $kengen['authData'];
        $kengenListCount = $kengen['authCount'];
    }
}
//$WUDHFG = '';
$WUUQFG = '';//作成したクエリー
$WUUBFG = '';//お気に入りクエリー
//システムリスト
$userShowList = fnGetDB2WUSR($db2con, $WUUID);
if ($userShowList['result'] === true) {
    // $WUDHFG = $userShowList['data'][0]['WUDHFG'];
    $WUUQFG = $userShowList['data'][0]['WUUQFG'];
    $WUUBFG = $userShowList['data'][0]['WUUBFG'];
}
$tabledata = fnCreateTableData($FDB2CSV1);
//error_log("Tabledata Query".print_r($tabledata,true));
function fnGetQryFromGroup($db2con, $D1NAME, $PGKEY){
    $data = array();
   // $params = array();
    $strSQL.= ' SELECT * FROM ';
    $strSQL.= cmpivot_graphMerge(true,true);
    $strSQL.= ' WHERE SUB.SUBQRY = ? AND SUB.SUBKEY = ? ';
    $params = array($D1NAME, $PGKEY);
    $stmt = db2_prepare($db2con, $strSQL);

    if ($stmt === false) {
        error_log('error = '.db2_stmt_errormsg());
        $rs = '1';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true,'data'=> umEx($data));
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * 権限のあるグループを取得
 *-------------------------------------------------------*
*/
/*
*/
function fnGetNavGroup($db2con, $WUUID, $touch, $pivotFlg, $graphFlg, $licenseQueryGroup,$licenseSql) {
    $data = array();
    $rawdata = array();
    $strSQL.= '       SELECT ';
    $strSQL.= '           MGP, ';
    $strSQL.= '           S1GP , ';
    $strSQL.= '           S2GP, ';
    $strSQL.= '           FLG, ';
    $strSQL.= '           D1NAME, ';
    $strSQL.= '           D1TEXT, ';
    $strSQL.= '           WGNRFLG, ';
    $strSQL.= '           D1MFLG, ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '           SUBNME, ';
        $strSQL.= '           SUBKEY, ';
        $strSQL.= '           SUBFLG, ';
    }
    $strSQL.= '           WQUNAM, ';
    $strSQL.= '           HFLG, ';
    $strSQL.= '           SGP, ';
    $strSQL.='            QRYGFLG ';
    $strSQL.= '       FROM ';
    //ログインユーザが権限ある全てのグループ取得開始
    $strSQL.= '           ( ';
    //メイングループ取得開始
    $strSQL.= '               (SELECT ';
    $strSQL.= '                   GPTBL.MGP, ';
    $strSQL.= '                   GPTBL.S1GP , ';
    $strSQL.= '                   GPTBL.S2GP, ';
    $strSQL.= '                   GPTBL.FLG, ';
    $strSQL.= '                   D1NAME, ';
    $strSQL.= '                   D1TEXT, ';
    $strSQL.= '                   WGNRFLG, ';
    $strSQL.= '                   D1MFLG, ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                SUBNME, ';
        $strSQL.= '                SUBKEY, ';
        $strSQL.= '                SUBFLG, ';
    }
    $strSQL.= '                    WQUNAM, ';
    $strSQL.= '                    WQHFLG, ';
    $strSQL.= '                    QRYGFLG ';
    $strSQL.= '                    FROM ';
    $strSQL.= '                   ( ';
    $strSQL.= '                       SELECT ';
    $strSQL.= '                           WUGID MGP , ';
    $strSQL.= '                           \'\' S1GP , ';
    $strSQL.= '                           \'\' S2GP , ';
    $strSQL.= '                           X.WQUNAM, ';
    $strSQL.= '                           X.WQHFLG, ';
    $strSQL.= '                           0 AS FLG ';
    $strSQL.= '                       FROM ';
    $strSQL.= '                           DB2WUGR ';
    $strSQL.= '                       LEFT OUTER JOIN ';
    $strSQL.= '                           DB2WQGR AS X ';
    $strSQL.= '                       ON WUGID = WQGID ';
    $strSQL.= '                       WHERE ';
    $strSQL.= '                           WUUID = ? ';
    $strSQL.= '                   ) GPTBL ';
    $strSQL.= '               LEFT OUTER JOIN DB2WGDF AS G ';
    $strSQL.= '               ON GPTBL.MGP = WGGID ';
    //old query
    //$strSQL .= '               LEFT OUTER JOIN FDB2CSV1 AS B ';
    //new join
    $strSQL.= '                LEFT OUTER JOIN (';
    $strSQL.= '                         SELECT D1NAME,D1MFLG,D1TEXT, ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                     SUBNME, ';
        $strSQL.= '                     SUBKEY, ';
        $strSQL.= '                     SUBFLG, ';
    }
    $strSQL.= '                             \'\' AS QRYGFLG ';
    $strSQL.= '                         FROM FDB2CSV1 ';
    //$strSQL.= '               ON WGNAME = D1NAME ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                     LEFT OUTER JOIN  ';
        $strSQL.= cmpivot_graphMerge($pivotFlg, $graphFlg);
        $strSQL.= '                     ON D1NAME = SUB.SUBQRY ';
    }
    if(!$licenseSql){
        $strSQL.= '                     WHERE D1CFLG = \'\' ';
    }
    $strSQL.= '                         UNION ';
    $strSQL.= '                         SELECT QRYGID AS D1NAME, ';
    $strSQL.= '                             \'\' AS D1MFLG,QRYGNAME AS D1TEXT, ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                     SUBNME, ';
        $strSQL.= '                     SUBKEY, ';
        $strSQL.= '                     SUBFLG, ';
    }
    $strSQL.= '                             \'1\' AS QRYGFLG ';
    if($licenseQueryGroup){//190308
        $strSQL.= '                         FROM DB2QRYG ';
    }else{
        $strSQL.='                            FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') ';
    }   
    $strSQL.='                                LEFT JOIN (SELECT EMP1.QRYGSQRY,EMP1.QRYGSID ';
    $strSQL.='                                  FROM DB2QRYGS AS EMP1 ';      
    $strSQL.='                                  JOIN ';
    $strSQL.='                                      (SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
    $strSQL.='                                          FROM DB2QRYGS GROUP BY QRYGSID ';
    $strSQL.='                                      ) EMP2 ';
    $strSQL.='                                  ON EMP1.QRYGSID=EMP2.QRYGSID AND EMP1.QRYGSSEQ=EMP2.QRYGSSEQ  ';
    $strSQL.='                            ) EMP ON QRYGID=EMP.QRYGSID  ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                     LEFT OUTER JOIN  ';
        $strSQL.= cmpivot_graphMerge($pivotFlg, $graphFlg);
        $strSQL.= '                     ON QRYGSQRY = SUB.SUBQRY ';
    }
    $strSQL.= '                     ) AS Q ON WGNAME=Q.D1NAME ';


    $strSQL.= '               ) ';
    //　メイングループ取得終了
    $strSQL.= '           UNION ';
    //一段階のサブグループ開始
    $strSQL.= '               (SELECT ';
    $strSQL.= '                   GPTBL.MGP, ';
    $strSQL.= '                   GPTBL.S1GP , ';
    $strSQL.= '                   GPTBL.S2GP, ';
    $strSQL.= '                   GPTBL.FLG, ';
    $strSQL.= '                   D1NAME, ';
    $strSQL.= '                   D1TEXT, ';
    $strSQL.= '                   WGNRFLG, ';
    $strSQL.= '                   D1MFLG, ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '           SUBNME, ';
        $strSQL.= '           SUBKEY, ';
        $strSQL.= '           SUBFLG, ';
    }
    $strSQL.= '                   WQUNAM, ';
    $strSQL.= '                   WQHFLG, ';
    $strSQL.='                    QRYGFLG ';
    $strSQL.= '               FROM ';
    $strSQL.= '               ( ';
    $strSQL.= '                   SELECT ';
    $strSQL.= '                       GPMID MGP , ';
    $strSQL.= '                       GPSID S1GP , ';
    $strSQL.= '                       \'\' S2GP , ';
    $strSQL.= '                       WQUNAM, ';
    $strSQL.= '                       WQHFLG, ';
    $strSQL.= '                       1 AS FLG ';
    $strSQL.= '                   FROM ';
    $strSQL.= '                       DB2WUGR ';
    $strSQL.= '                   LEFT JOIN DB2WSGP ';
    $strSQL.= '                   ON WUGID = GPSID ';
    $strSQL.= '                   LEFT JOIN DB2WQGR ';
    $strSQL.= '                   ON WQGID = GPSID ';
    $strSQL.= '                   WHERE ';
    $strSQL.= '                   GPMID IN ';
    $strSQL.= '                       ( ';
    $strSQL.= '                           SELECT ';
    $strSQL.= '                               WUGR1.WUGID ';
    $strSQL.= '                           FROM ';
    $strSQL.= '                                ( ';
    $strSQL.= '                                SELECT ';
    $strSQL.= '                                    WUGID ';
    $strSQL.= '                                FROM ';
    $strSQL.= '                                    DB2WUGR ';
    $strSQL.= '                                WHERE ';
    $strSQL.= '                                    WUUID = ? ';
    $strSQL.= '                                ) WUGR1 ';
    $strSQL.= '                           LEFT OUTER JOIN ';
    $strSQL.= '                               DB2WQGR AS X ';
    $strSQL.= '                           ON WUGR1.WUGID = WQGID ';
    $strSQL.= '                       ) ';
    $strSQL.= '                   AND WUUID = ? ';
    $strSQL.= '               )GPTBL ';
    $strSQL.= '               LEFT OUTER JOIN DB2WGDF ';
    $strSQL.= '               ON GPTBL.S1GP = WGGID ';
    //OLD JOIN
    //$strSQL.= '               LEFT OUTER JOIN FDB2CSV1 AS B ';
    //$strSQL.= '               ON WGNAME = D1NAME ';
    $strSQL.= '                LEFT OUTER JOIN (';
    $strSQL.= '                         SELECT D1NAME,D1MFLG,D1TEXT, ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                         SUBNME, ';
        $strSQL.= '                         SUBKEY, ';
        $strSQL.= '                         SUBFLG, ';
    }
    $strSQL.= '                             \'\' AS QRYGFLG ';
    $strSQL.= '                         FROM FDB2CSV1 ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                     LEFT OUTER JOIN  ';
        $strSQL.= cmpivot_graphMerge($pivotFlg, $graphFlg);
        $strSQL.= '                     ON D1NAME = SUB.SUBQRY ';
    }
    if(!$licenseSql){
        $strSQL.= '                     WHERE D1CFLG = \'\' ';
    }
    $strSQL.= '                         UNION ';
    $strSQL.= '                         SELECT QRYGID AS D1NAME, ';
    $strSQL.= '                             \'\' AS D1MFLG,QRYGNAME AS D1TEXT, ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                         SUBNME, ';
        $strSQL.= '                         SUBKEY, ';
        $strSQL.= '                         SUBFLG, ';
    }
    $strSQL.= '                             \'1\' AS QRYGFLG  ';
    if($licenseQueryGroup){//190308
        $strSQL.= '                         FROM DB2QRYG ';
    }else{
        $strSQL.='                            FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') ';
    }
    $strSQL.='                          LEFT JOIN (SELECT EMP1.QRYGSQRY,EMP1.QRYGSID ';
    $strSQL.='                                  FROM DB2QRYGS AS EMP1 ';
    $strSQL.='                                  JOIN';
    $strSQL.='                                      (SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
    $strSQL.='                                          FROM DB2QRYGS GROUP BY QRYGSID ';
    $strSQL.='                                      ) EMP2  ';
    $strSQL.='                                      ON EMP1.QRYGSID=EMP1.QRYGSID AND EMP1.QRYGSSEQ=EMP2.QRYGSSEQ ';
    $strSQL.='                            ) EMP ON QRYGID=EMP.QRYGSID  ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                     LEFT OUTER JOIN  ';
        $strSQL.= cmpivot_graphMerge($pivotFlg, $graphFlg);
        $strSQL.= '                     ON QRYGSQRY = SUB.SUBQRY ';
    }
    $strSQL.= '                     ) AS Q ON WGNAME=Q.D1NAME ';

    $strSQL.= '               ) ';
    $strSQL.= '           UNION  ';
    $strSQL.= '               (SELECT ';
    $strSQL.= '                   GPTBL.MGP, ';
    $strSQL.= '                   GPTBL.S1GP , ';
    $strSQL.= '                   GPTBL.S2GP, ';
    $strSQL.= '                   GPTBL.FLG, ';
    $strSQL.= '                   D1NAME, ';
    $strSQL.= '                   D1TEXT,';
    $strSQL.= '                   WGNRFLG, ';
    $strSQL.= '                   D1MFLG, ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '           SUBNME, ';
        $strSQL.= '           SUBKEY, ';
        $strSQL.= '           SUBFLG, ';
    }
    $strSQL.= '                   WQUNAM, ';
    $strSQL.= '                   WQHFLG, ';
    $strSQL.='                    QRYGFLG ';
    $strSQL.= '               FROM ';
    $strSQL.= '               ( ';
    $strSQL.= '                   SELECT ';
    $strSQL.= '                       MGP, ';
    $strSQL.= '                       S1GP, ';
    $strSQL.= '                       GPSID S2GP, ';
    $strSQL.= '                       2 FLG  ';
    $strSQL.= '                   FROM ';
    $strSQL.= '                       (SELECT ';
    $strSQL.= '                           GPSID S1GP, ';
    $strSQL.= '                           GPMID MGP ';
    $strSQL.= '                       FROM ';
    $strSQL.= '                            (SELECT ';
    $strSQL.= '                                WSGP1.* ';
    $strSQL.= '                            FROM ';
    $strSQL.= '                            ( ';
    $strSQL.= '                                SELECT ';
    $strSQL.= '                                    WUGID ';
    $strSQL.= '                                FROM ';
    $strSQL.= '                                    DB2WUGR ';
    $strSQL.= '                                WHERE ';
    $strSQL.= '                                    WUUID = ? ';
    $strSQL.= '                                ) WUGR4 ';
    $strSQL.= '                            LEFT JOIN ';
    $strSQL.= '                                DB2WSGP WSGP1 ';
    $strSQL.= '                            ON WUGR4.WUGID = WSGP1.GPSID ';
    $strSQL.= '                           )  ';
    $strSQL.= '                           DB2WSGP ';
    $strSQL.= '                       WHERE ';
    $strSQL.= '                           GPMID IN  ';
    $strSQL.= '                            ( ';
    $strSQL.= '                                SELECT ';
    $strSQL.= '                                    WUGR2.WUGID ';
    $strSQL.= '                                FROM ';
    $strSQL.= '                                     ( ';
    $strSQL.= '                                     SELECT ';
    $strSQL.= '                                         WUGID ';
    $strSQL.= '                                     FROM ';
    $strSQL.= '                                         DB2WUGR ';
    $strSQL.= '                                     WHERE ';
    $strSQL.= '                                         WUUID = ? ';
    $strSQL.= '                                     ) WUGR2 ';
    $strSQL.= '                                LEFT OUTER JOIN ';
    $strSQL.= '                                    DB2WQGR AS X ';
    $strSQL.= '                                ON WUGR2.WUGID = WQGID ';
    $strSQL.= '                            )';
    $strSQL.= '                       ) S1 ';
    $strSQL.= '                       LEFT JOIN ';
    $strSQL.= '                       (SELECT ';
    $strSQL.= '                           GPMID, ';
    $strSQL.= '                           GPSID ';
    $strSQL.= '                       FROM ';
    $strSQL.= '                           DB2WSGP ';
    $strSQL.= '                       )S2 ';
    $strSQL.= '                       ON S1.S1GP = S2.GPMID ';
    $strSQL.= '                       WHERE GPSID<> \'\' ';
    $strSQL.= '               ) GPTBL ';
    $strSQL.= '               LEFT JOIN DB2WUGR ';
    $strSQL.= '               ON  GPTBL.S2GP  =  WUGID ';
    $strSQL.= '               LEFT OUTER JOIN DB2WQGR ';
    $strSQL.= '               ON GPTBL.S2GP = WQGID ';
    $strSQL.= '               LEFT OUTER JOIN DB2WGDF ';
    $strSQL.= '               ON GPTBL.S2GP  = WGGID ';
    //$strSQL.= '               LEFT OUTER JOIN FDB2CSV1 AS B ';
    //$strSQL.= '               ON WGNAME = D1NAME  ';
    $strSQL.= '                LEFT OUTER JOIN (';
    $strSQL.= '                         SELECT D1NAME,D1MFLG,D1TEXT, ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                         SUBNME, ';
        $strSQL.= '                         SUBKEY, ';
        $strSQL.= '                         SUBFLG, ';
    }
    $strSQL.= '                             \'\' AS QRYGFLG ';
    $strSQL.= '                         FROM FDB2CSV1 ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                     LEFT OUTER JOIN  ';
        $strSQL.= cmpivot_graphMerge($pivotFlg, $graphFlg);
        $strSQL.= '                     ON D1NAME = SUB.SUBQRY ';
    }
    if(!$licenseSql){
        $strSQL.= '                     WHERE D1CFLG = \'\' ';
    }
    $strSQL.= '                         UNION ';
    $strSQL.= '                         SELECT QRYGID AS D1NAME, ';
    $strSQL.= '                             \'\' AS D1MFLG,QRYGNAME AS D1TEXT, ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                         SUBNME, ';
        $strSQL.= '                         SUBKEY, ';
        $strSQL.= '                         SUBFLG, ';
    }
    $strSQL.= '                             \'1\' AS QRYGFLG ';
    if($licenseQueryGroup){//190308
        $strSQL.= '                         FROM DB2QRYG ';
    }else{
        $strSQL.='                            FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') ';
    }  
    $strSQL.='                          LEFT JOIN (SELECT EMP1.QRYGSQRY,EMP1.QRYGSID ';   
    $strSQL.='                                  FROM DB2QRYGS AS EMP1 ';    
    $strSQL.='                                  JOIN ';
    $strSQL.='                                      (SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
    $strSQL.='                                          FROM DB2QRYGS  GROUP BY QRYGSID ';
    $strSQL.='                                      ) EMP2  ';
    $strSQL.='                                      ON EMP1.QRYGSID=EMP2.QRYGSID AND EMP1.QRYGSSEQ=EMP2.QRYGSSEQ  ';
    $strSQL.='                            ) EMP ON QRYGID=EMP.QRYGSID  ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '                     LEFT OUTER JOIN  ';
        $strSQL.= cmpivot_graphMerge($pivotFlg, $graphFlg);
        $strSQL.= '                     ON QRYGSQRY = SUB.SUBQRY ';
    }
    $strSQL.= '                     ) AS Q ON WGNAME=Q.D1NAME ';

    $strSQL.= '               WHERE WUUID = ?) ';
    $strSQL.= '           ) ALLGP ';
    $strSQL.= '       LEFT JOIN ';
    $strSQL.= '           ( ';
    $strSQL.= '                (SELECT ';
    $strSQL.= '                   GPSID SGP ,WQHFLG HFLG ';
    $strSQL.= '               FROM ';
    $strSQL.= '                   DB2WUGR ';
    $strSQL.= '                   LEFT JOIN DB2WSGP ';
    $strSQL.= '                   ON WUGID = GPSID ';
    $strSQL.= '                   LEFT JOIN DB2WQGR ';
    $strSQL.= '                   ON WQGID = GPSID ';
    $strSQL.= '               WHERE ';
    $strSQL.= '                   GPMID IN  ';
    $strSQL.= '                       (SELECT ';
    $strSQL.= '                           WUGID ';
    $strSQL.= '                       FROM ';
    $strSQL.= '                           DB2WUGR ';
    $strSQL.= '                           LEFT OUTER JOIN ';
    $strSQL.= '                               DB2WQGR AS X ';
    $strSQL.= '                           ON WUGID = WQGID ';
    $strSQL.= '                       WHERE ';
    $strSQL.= '                           WUUID = ? ';
    $strSQL.= '                       ) ';
    $strSQL.= '               AND WUUID = ? ';
    $strSQL.= '               ) ';
    $strSQL.= '               UNION  ';
    $strSQL.= '               ( ';
    $strSQL.= '                   SELECT ';
    $strSQL.= '                       GPSID SGP ,WQHFLG HFLG ';
    $strSQL.= '                   FROM ';
    $strSQL.= '                       (SELECT ';
    $strSQL.= '                           GPSID S1GP, ';
    $strSQL.= '                           GPMID MGP ';
    $strSQL.= '                       FROM ';
    $strSQL.= '                           DB2WSGP ';
    $strSQL.= '                       WHERE ';
    $strSQL.= '                           GPMID IN  ';
    $strSQL.= '                            ( ';
    $strSQL.= '                                SELECT ';
    $strSQL.= '                                    WUGR3.WUGID ';
    $strSQL.= '                                FROM ';
    $strSQL.= '                                     ( ';
    $strSQL.= '                                     SELECT ';
    $strSQL.= '                                         WUGID ';
    $strSQL.= '                                     FROM ';
    $strSQL.= '                                         DB2WUGR ';
    $strSQL.= '                                     WHERE ';
    $strSQL.= '                                         WUUID = ? ';
    $strSQL.= '                                     ) WUGR3 ';
    $strSQL.= '                                LEFT OUTER JOIN ';
    $strSQL.= '                                    DB2WQGR AS X ';
    $strSQL.= '                                ON WUGR3.WUGID = WQGID ';
    $strSQL.= '                            )';
    $strSQL.= '                       ) S1 ';
    $strSQL.= '                   LEFT JOIN  ';
    $strSQL.= '                       (SELECT ';
    $strSQL.= '                           GPMID, ';
    $strSQL.= '                           GPSID ';
    $strSQL.= '                       FROM ';
    $strSQL.= '                           DB2WSGP ';
    $strSQL.= '                       )S2 ';
    $strSQL.= '                   ON S1.S1GP = S2.GPMID ';
    $strSQL.= '                   LEFT JOIN DB2WQGR ';
    $strSQL.= '                   ON WQGID = GPSID ';
    $strSQL.= '                   WHERE ';
    $strSQL.= '                       GPSID<> \'\' ';
    $strSQL.= '               ) ';
    $strSQL.= '           ) SGP ';
    $strSQL.= '       ON ALLGP.MGP = SGP.SGP ';
    $strSQL.= '       WHERE  SGP IS NULL ';
    $strSQL.= '       OR ( ';
    $strSQL.= '           (SGP IS NOT NULL ';
    $strSQL.= '           AND HFLG = \'\') ';
    $strSQL.= '       ) ';
    $strSQL.= '       ORDER BY ';
  //  $strSQL.= '           WGN, ';
  //  $strSQL.= '           G.WGNRFLG ASC, ';
    $strSQL.= '           MGP, ';

//    $strSQL.= '           CAST((CASE WHEN WGNRFLG = \'\' THEN 0 ELSE WGNRFLG END) AS INT) ASC, ';
    $strSQL.= '           CASE WHEN S1GP  = \'\' THEN 1 ELSE 0 END, ';
    $strSQL.= '           S1GP, ';
    $strSQL.= '           CASE WHEN S2GP  = \'\' THEN 1 ELSE 0 END, ';
    $strSQL.= '           S2GP, ';
    $strSQL.= '           CAST((CASE WHEN WGNRFLG = \'\' THEN 0 ELSE WGNRFLG END) AS INT) ASC, ';
    $strSQL.= '           D1NAME ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '           ,SUBFLG,SUBKEY';
    }
    $params = array($WUUID, $WUUID, $WUUID, $WUUID, $WUUID, $WUUID, $WUUID, $WUUID, $WUUID);
    error_log('NAVgroup dataSQL：' . $strSQL . print_r($params, true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            $tmp = '';
            $s1tmp = '';
            $s2tmp = '';
            $s1d = array();
            $s2d = array();
            $d = array();
            while ($row = db2_fetch_assoc($stmt)) {
                $rawdata[] = $row;
                if ($tmp == $row['MGP'] || $tmp == '') {
                    if (empty($row['S1GP'])) {
                        //e_log('add'.$row['MGP']);
                        if (count($s1d) > 0) {
                            $d[] = $s1d;
                            $s1d = array();
                        }
                        $d[] = $row;
                        $s1tmp = '';
                    } else {
                        if ($s1tmp == $row['S1GP'] || $s1tmp == '') {
                            if (empty($row['S2GP'])) {
                                //e_log('addS2'.$row['S1GP']);
                                if (count($s2d) > 0) {
                                    $s1d[] = $s2d;
                                    $s2d = array();
                                }
                                $s1d[] = $row;
                                $s2tmp = '';
                            } else {
                                if ($s2tmp == $row['S2GP'] || $s2tmp == '') {
                                    //e_log('addS2data'.$row['S2GP']);
                                    $s2d[] = $row;
                                } else {
                                    //e_log('addS2data'.$row['S2GP']);
                                    $s1d[] = $s2d;
                                    $s2d = array();
                                    $s2d[] = $row;
                                }
                                $s2tmp = $row['S2GP'];
                            }
                        } else {
                            $d[] = $s1d;
                            $s1d = array();
                            $s2tmp = '';
                            if (empty($row['S2GP'])) {
                                //e_log('addS2'.$row['S1GP']);
                                if (count($s2d) > 0) {
                                    $s1d[] = $s2d;
                                    $s2d = array();
                                }
                                $s1d[] = $row;
                                $s2tmp = '';
                            } else {
                                if ($s2tmp == $row['S2GP'] || $s2tmp == '') {
                                    $s2d[] = $row;
                                } else {
                                    //e_log('addS2data'.$row['S2GP']);
                                    $s1d[] = $s2d;
                                    $s2d = array();
                                    $s2d[] = $row;
                                }
                                $s2tmp = $row['S2GP'];
                            }
                        }
                        $s1tmp = $row['S1GP'];
                    }
                } else {
                    $data[] = $d;
                    $d = array();
                    $s1tmp = '';
                    $s2tmp = '';
                    if (empty($row['S1GP'])) {
                        //e_log('add'.$row['MGP']);
                        if (count($s1d) > 0) {
                            $d[] = $s1d;
                            $s1d = array();
                        }
                        $d[] = $row;
                        $s1tmp = '';
                    } else {
                        if ($s1tmp == $row['S1GP'] || $s1tmp == '') {
                            if (empty($row['S2GP'])) {
                                //e_log('addS2'.$row['S1GP']);
                                if (count($s2d) > 0) {
                                    $s1d[] = $s2d;
                                    $s2d = array();
                                }
                                $s1d[] = $row;
                                $s2tmp = '';
                            } else {
                                if ($s2tmp == $row['S2GP'] || $s2tmp == '') {
                                    $s2d[] = $row;
                                } else {
                                    //e_log('addS2data'.$row['S2GP']);
                                    $s1d[] = $s2d;
                                    $s2d = array();
                                    $s2d[] = $row;
                                }
                                $s2tmp = $row['S2GP'];
                            }
                        } else {
                            $d[] = $s1d;
                            $s1d = array();
                            $s2tmp = '';
                            if (empty($row['S2GP'])) {
                                //e_log('addS2'.$row['S1GP']);
                                if (count($s2d) > 0) {
                                    $s1d[] = $s2d;
                                    $s2d = array();
                                }
                                $s2tmp = '';
                            } else {
                                if ($s2tmp == $row['S2GP'] || $s2tmp == '') {
                                    $s2d[] = $row;
                                } else {
                                    //e_log('addS2data'.$row['S2GP']);
                                    $s1d[] = $s2d;
                                    $s2d = array();
                                    $s2d[] = $row;
                                }
                                $s2tmp = $row['S2GP'];
                            }
                        }
                        $s1tmp = $row['S1GP'];
                    }
                }
                $tmp = $row['MGP'];
            }
            if (count($d) > 0) {
                $data[] = $d;
            }
        }
    }
    //e_log('NAV result:'.print_r($rawdata,true));
  //  error_log('NEW NAV result:'.print_r($data,true));
    return $data;
}
//test start
function fnGetGroupTEST($db2con, $WUUID, $touch, $pivotFlg) {
    $data = array();
    $strSQL = '    SELECT ';
    $strSQL.= '        WQGID, ';
    $strSQL.= '        WQUNAM, ';
    $strSQL.= '        D1NAME, ';
    $strSQL.= '        D1TEXT, ';
    $strSQL.= '        D1MFLG, ';
    if ($touch !== '1' && $pivotFlg !== false) {
        $strSQL.= '        PMTEXT, ';
        $strSQL.= '        PMPKEY, ';
    }
    $strSQL.= '        SUBGP , ';
    $strSQL.= '        MGPID,';
    $strSQL.= '        MGPNAM ';
    $strSQL.= '    FROM ';
    $strSQL.= '        ( ';
    $strSQL.= '            ( ';
    $strSQL.= '                SELECT ';
    $strSQL.= '                    WUGID WQGID , ';
    $strSQL.= '                    WQUNAM , ';
    $strSQL.= '                    \'0\' AS SUBGP , ';
    $strSQL.= '                    WUGID AS MGPID, ';
    $strSQL.= '                    WQUNAM AS MGPNAM ';
    $strSQL.= '                FROM ';
    $strSQL.= '                    DB2WUGR ';
    $strSQL.= '                LEFT OUTER JOIN ';
    $strSQL.= '                    DB2WQGR ';
    $strSQL.= '                ON WUGID = WQGID ';
    $strSQL.= '                WHERE WUUID = ? ';
    $strSQL.= '            ) ';
    $strSQL.= '            UNION ';
    $strSQL.= '            ( ';
    $strSQL.= '                SELECT ';
    $strSQL.= '                    GPSID WQGID , ';
    $strSQL.= '                    GPSID WQUNAM , ';
    $strSQL.= '                    \'1\' AS SUBGP , ';
    $strSQL.= '                    GPMID AS MGPID, ';
    $strSQL.= '                    GPMID AS MGPNAM ';
    $strSQL.= '                FROM ';
    $strSQL.= '                    DB2WSGP ';
    $strSQL.= '                WHERE ';
    $strSQL.= '                    GPMID IN (SELECT ';
    $strSQL.= '                                WUGID ';
    $strSQL.= '                            FROM ';
    $strSQL.= '                                DB2WUGR       LEFT OUTER JOIN      DB2WQGR AS X       ON WUGID = WQGID ';
    $strSQL.= '                            WHERE ';
    $strSQL.= '                                WUUID = ? ';
    $strSQL.= '                            ) ';
    $strSQL.= '            ) ';
    $strSQL.= '        ) GPTBL ';
    $strSQL.= '    LEFT OUTER JOIN DB2WGDF ';
    $strSQL.= '    ON GPTBL.WQGID = WGGID ';
    $strSQL.= '    LEFT OUTER JOIN FDB2CSV1 AS B ';
    $strSQL.= '    ON WGNAME = D1NAME ';
    if ($touch !== '1' && $pivotFlg !== false) {
        $strSQL.= '    LEFT OUTER JOIN DB2PMST ';
        $strSQL.= '    ON D1NAME = PMNAME ';
    }
    $strSQL.= '    WHERE ';
    $strSQL.= '          WQUNAM <> \'\' ';
    $strSQL.= '    ORDER BY ';
    $strSQL.= '        MGPID, ';
    $strSQL.= '        SUBGP DESC, ';
    $strSQL.= '        WQGID ';
    $params = array($WUUID, $WUUID);
    //e_log('group testdataSQL：'.$strSQL);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            $tmp = '';
            $tmpNm = '';
            $d = array();
            $subgptmp = '';
            $subD = array();
            while ($row = db2_fetch_assoc($stmt)) {
                if ($tmp == $row['MGPID'] || $tmp == '') {
                    if ($row['SUBGP'] == '1') {
                        if ($subgptmp == $row['WQGID'] || $subgptmp == '') {
                            $subD[] = $row;
                        } else {
                            $subD['WQGID'] = $tmp;
                            $subD['WQUNAM'] = $tmpNm;
                            $d[] = $subD;
                            $subD = array();
                            $subD[] = $row;
                        }
                        $subgptmp = $row['WQGID'];
                    } else {
                        if (count($subD) > 0) {
                            $d[] = $subD;
                            $subD = array();
                        }
                        $d[] = $row;
                    }
                } else {
                    $tmp = $row['MGPID'];
                    $tmpNm = $row['MGPNAM'];
                    $subgptmp = '';
                    if ($row['SUBGP'] == '1') {
                        $data[] = $d;
                        $d = array();
                        if ($subgptmp == $row['WQGID'] || $subgptmp == '') {
                            $subD[] = $row;
                            $subD['WQGID'] = $tmp;
                            $subD['WQUNAM'] = $tmpNm;
                        } else {
                            if (count($subD) > 0) {
                                $d[] = $subD;
                                $subD = array();
                            }
                            $subD['WQGID'] = $tmp;
                            $subD['WQUNAM'] = $tmpNm;
                            $subD[] = $row;
                        }
                        $subgptmp = $row['WQGID'];
                    } else {
                        $subgptmp = '';
                        $data[] = $d;
                        $d = array();
                        $d[] = $row;
                    }
                }
                $tmp = $row['MGPID'];
                $tmpNm = $row['MGPNAM'];
            }
            if (count($d) > 0) {
                $data[] = $d;
            }
        }
    }
   // error_log('NEW NAV result Group:'.print_r($data,true));
    return $data;
}
//testend
/*
 *-------------------------------------------------------*
 * 権限のあるグループを取得
 *-------------------------------------------------------*
*/
/*
*/
function fnGetGroup($db2con, $WUUID, $touch, $pivotFlg) {
    $data = array();
    $strSQL = ' SELECT WQGID,WQUNAM,D1NAME,D1TEXT,D1MFLG ';
    if ($touch !== '1' && $pivotFlg !== false) {
        $strSQL.= ' ,PMTEXT,PMPKEY ';
    }
    $strSQL.= ' FROM DB2WUGR ';
    //    $strSQL .= ' LEFT OUTER JOIN DB2WQGR ';
    $strSQL.= ' LEFT OUTER JOIN DB2WQGR AS X ';
    $strSQL.= ' ON WUGID = WQGID ';
    $strSQL.= ' LEFT OUTER JOIN DB2WGDF ';
    $strSQL.= ' ON WQGID = WGGID ';
    //    $strSQL .= ' LEFT OUTER JOIN FDB2CSV1 ';
    $strSQL.= ' LEFT OUTER JOIN FDB2CSV1 AS B';
    $strSQL.= ' ON WGNAME = D1NAME ';
    if ($touch !== '1' && $pivotFlg !== false) {
        $strSQL.= ' LEFT OUTER JOIN DB2PMST ';
        $strSQL.= ' ON D1NAME = PMNAME ';
    }
    $strSQL.= ' WHERE WUUID = ? ';
    $strSQL.= ' AND WQUNAM <> \'\' ';
    $strSQL.= ' ORDER BY WQGID ';
    $params = array($WUUID);
    //e_log('group dataSQL：'.$strSQL);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            $tmp = '';
            $d = array();
            while ($row = db2_fetch_assoc($stmt)) {
                if ($tmp == $row['WQGID'] || $tmp == '') {
                    $d[] = $row;
                } else {
                    $data[] = $d;
                    $d = array();
                    $d[] = $row;
                }
                $tmp = $row['WQGID'];
            }
            if (count($d) > 0) {
                $data[] = $d;
            }
        }
    }
   // error_log('fnGetGroup:'.print_r($data,true));
    return $data;
}
/*
 *-------------------------------------------------------*
 * スケジュール自動実行CSV取得
 *-------------------------------------------------------*
*/
/*
*/
function fnGetFile($db2con) {
    $rtn = 0;
    $fdb2csv1Rdb = array();
    $strSQL = ' SELECT ';
    $strSQL.= '     B.D1RDB ';
    $strSQL.= ' FROM ';
    $strSQL.= '     DB2WHIS AS A ';
    $strSQL.= ' LEFT JOIN  FDB2CSV1 B ';
    $strSQL.= ' ON A.WHNAME = B.D1NAME ';
    $strSQL.= ' WHERE ';
    $strSQL.= '     B.D1RDB <> \'\' ';
    $strSQL.= ' AND B.D1RDB <> \''.RDB.'\' ';
    $strSQL.= ' AND A.WHQGFLG <> \'1\' ';
    $strSQL.= ' GROUP BY ';
    $strSQL.= '     B.D1RDB ';
    $params = array();
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $fdb2csv1Rdb = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $fdb2csv1Rdb = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $fdb2csv1Rdb[] = $row;
            }
        }
        //e_log('RDBARRSQL:'.$strSQL);
        //e_log('RDBARR:'.print_r($fdb2csv1Rdb,true));
        
    }
    if ($fdb2csv1Rdb !== false) {
        if (count($fdb2csv1Rdb) > 0) {
            $fdb2csv1Rdb = umEx($fdb2csv1Rdb);
            //e_log('一時テーブル作成開始');
            // 一時テーブル作成
            $createSQL = 'DECLARE GLOBAL TEMPORARY TABLE TABLES ';
            $createSQL.= ' ( ';
            $createSQL.= ' TABLE_NAME VARCHAR(128)';
            $createSQL.= ' ,DBNAME VARCHAR(128)';
            $createSQL.= ' ) WITH REPLACE ';
            //e_log($createSQL);
            $result = db2_exec($db2con, $createSQL);
            if ($result) {
                foreach ($fdb2csv1Rdb as $rdbnm) {
                    $_SESSION['PHPQUERY']['RDBNM'] = $rdbnm['D1RDB'];
                    $rdbCon = cmDB2ConRDB(SAVE_DB);
                    $res = createTmpSysTBL($rdbCon, $db2con);
                    cmDb2Close($rdbCon);
                }
            } else {
                //e_log('テーブル作成失敗');
                //e_log('テーブルerror'.db2_stmt_errormsg());
                $rtn = 1;
                $data = array('result' => 'FAIL_SEL', 'errcd' => 'テーブル作成失敗');
            }
        }
    } else {
        $rtn = 1;
    }
    /*if($rtn === 0){
    if(count($fdb2csv1Rdb) > 0){
    $tdata = array();
    $strSQL  =' SELECT * ';
    $strSQL .=' FROM QTEMP/TABLES A ';
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
    $tdata = array('result' => 'FAIL_SEL');
    //e_log('error'.db2_stmt_errormsg());
    $rtn = 1;
    }else{
    $params = array();
    $r = db2_execute($stmt,$params);
    
    if($r === false){
    $tdata = array('result' => 'FAIL_SEL');
    $rtn = 1;
    }else{
    while($row = db2_fetch_assoc($stmt)){
    $tdata[] = $row;
    }
    $tdata = umEx($tdata,false);
    $tdata = array('result' => true,'data' => $tdata);
    }
    e_log('temptable'.print_r($tdata,true));
    }
    }
    }*/
    $data = array();
    if ($rtn === 0) {
        $strSQL = ' SELECT A.WHNAME';
        $strSQL.= ' FROM DB2WHIS as A ';
        $strSQL.= ' LEFT JOIN (SELECT TABLE_NAME ,DBNAME FROM SYSIBM/TABLES WHERE DBNAME = ? ) AS B ';
        $strSQL.= ' ON A.WHOUTF = B.TABLE_NAME ';
        if (count($fdb2csv1Rdb) > 0) {
            $strSQL.= ' LEFT JOIN QTEMP/TABLES AS C ';
            $strSQL.= ' ON A.WHOUTF = C.TABLE_NAME ';
        }
        //$strSQL .= ' WHERE A.WHOUTF <> \'\' ';
        $strSQL.= ' WHERE A.WHPKEY = \'\' AND A.WHOUTF <> \'\' ';
        $strSQL.= ' AND A.WHQGFLG <> \'1\' ';
        if (count($fdb2csv1Rdb) > 0) {
            $strSQL.= ' AND (B.DBNAME <> \'\' ';
            $strSQL.= '      OR C.DBNAME <> \'\') ';
        } else {
            $strSQL.= ' AND (B.DBNAME <> \'\') ';
        }
        $strSQL.= ' GROUP BY WHNAME ';
        $strSQL.= ' ORDER BY WHNAME ';
        $params = array(SAVE_DB);
        //        e_log('Navigationshutoku'.$strSQL);
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $data = false;
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = false;
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
        }
    }
   // error_log('fnGetFile:'.print_r($data,true));
    return $data;
}
// create QTEMP.TABLES
function createTmpSysTBL($rdbCon, $db2con) {
    $rtn = 0;
    $rstbldata = array();
    $strSQL = ' SELECT  ';
    $strSQL.= '    A.TABLE_NAME ';
    $strSQL.= ' FROM SYSIBM/TABLES AS A';
    $strSQL.= ' WHERE A.DBNAME = ? ';
    $stmt = db2_prepare($rdbCon, $strSQL);
    if ($stmt === false) {
        $rstbldata = array('result' => 'FAIL_SEL');
        $rtn = 1;
    } else {
        $params = array(SAVE_DB);
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rstbldata = array('result' => 'FAIL_SEL');
            $rtn = 1;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $rstbldata[] = $row;
            }
            $rstbldata = array('result' => true, 'data' => $rstbldata);
            //e_log('取得したデータ：'.print_r($rstbldata,true));
            
        }
    }
    if ($rtn === 0) {
        if (count($rstbldata['data']) > 0) {
            $rstbldata = umEx($rstbldata['data']);
            $strInsSQL = ' INSERT INTO QTEMP/TABLES ';
            $strInsSQL.= ' ( ';
            $strInsSQL.= '    TABLE_NAME ';
            $strInsSQL.= '    ,DBNAME ';
            $strInsSQL.= ' ) ';
            $strInsSQL.= ' VALUES ( ?,?)';
            $stmt = db2_prepare($db2con, $strInsSQL);
            if ($stmt === false) {
                $data = array('result' => 'FAIL_INS', 'errcd' => 'TABLES:' . db2_stmt_errormsg());
                $result = 1;
            } else {
                foreach ($rstbldata as $tabledata) {
                    $params = array($tabledata['TABLE_NAME'], SAVE_DB);
                    //e_log($strInsSQL.print_r($params,true));
                    $res = db2_execute($stmt, $params);
                    if ($res === false) {
                        $data = array('result' => 'FAIL_INS', 'errcd' => 'TABLES:' . db2_stmt_errormsg());
                        $result = 1;
                        break;
                    } else {
                        $result = 0;
                        $data = array('result' => true);
                    }
                }
            }
        }
        if ($result !== 0) {
            $rtn = 1;
        }
    }
    return $rtn;
}
/*------------------------------------------------------*
 *Navigationメニューデータ取得
 *------------------------------------------------------*
*/
function fnGetNavigation($db2con, $WUUID, $WUAUTH, $SchduleFlg, $clFlg, $logFlg, $licenseQueryGroup, $ifsFlg) {
    $data = array();
    $params = array();
    $strSQL = '';
    $strSQL.= '   SELECT ';
    $strSQL.= '       A.*, ';
    $strSQL.= '       CASE  ';
    $strSQL.= '       WHEN  C.ITMTEXT IS NULL THEN D.ITMTEXT ';
    $strSQL.= '       WHEN TRIM(C.ITMTEXT) = \'\' THEN D.ITMTEXT';
    $strSQL.= '       ELSE C.ITMTEXT ';
    $strSQL.= '       END ITMTEXT ';
    $strSQL.= '   FROM ';
    $strSQL.= '       DB2MNAV A  ';
    $strSQL.= '   LEFT JOIN DB2MITM B  ';
    $strSQL.= '   ON A.NAVTEXT = B.ITMID  ';
    $strSQL.= '   LEFT JOIN (SELECT ';
    $strSQL.= '           * ';
    $strSQL.= '       FROM ';
    $strSQL.= '           DB2MTEXT ';
    $strSQL.= '       WHERE ';
    $strSQL.= '           LANCD = \'001\' ';
    $strSQL.= '       ) D ON B.ITMTEXT=  D.ITMNM ';
    $strSQL.= '   LEFT JOIN (SELECT ';
    $strSQL.= '           * ';
    $strSQL.= '       FROM ';
    $strSQL.= '           DB2MTEXT ';
    $strSQL.= '       WHERE ';
    if (cmMer($WUUID) === '') {
        $strSQL.= '           LANCD = \'001\' ';
    } else {
        $strSQL.= '           LANCD = (SELECT WULANG FROM DB2WUSR  WHERE WUUID = ?) ';
        array_push($params, $WUUID);
    }
    $strSQL.= '       ) C ON B.ITMTEXT=  C.ITMNM ';
    $strSQL.= '   WHERE NAVTEXT <> \'\' ';
    if (cmMer($WUUID) === '') {
        $strSQL.= '   AND NAVID = 1   '; //usercheck
    }
    if ($SchduleFlg === false) {
        $strSQL.= '   AND NAVID NOT IN (1,6,9)  '; //--scheduleFlg
    }
    if($ifsFlg === false){
        $strSQL.= '   AND NAVID NOT IN (7)  '; //ifsFlg        
    }
    if ($clFlg === false) {
        $strSQL.= '   AND NAVID NOT IN (8)  '; //clFlg        
    }
    if ($logFlg === false) {
        $strSQL.= '   AND NAVID NOT IN (12)  '; //logFlg        
    }
    if ($licenseQueryGroup === false) {
        $strSQL.= '   AND NAVID NOT IN (26)  '; //クエリーグループflg190308
    }
    if (($WUAUTH === '1' || $WUAUTH === '2' || $WUAUTH === '3' || $WUAUTH === '4') === false) {
        $strSQL.= '   AND NAVID IN (0,1,20,21,22,27) '; //一般ユーザ
        
    }
    $strSQL.= '   ORDER BY A.NAVORD ';
    error_log('ナビケション取得：'.$strSQL);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = umEx($data);
        }
    }
   // error_log('fnGetNavigation:'.print_r($data,true));
    return $data;
}
/*
 *-------------------------------------------------------*
 * 定義名:key　テーブル名:value 配列作成
 *-------------------------------------------------------*
*/
function fnCreateTableData($FDB2CSV1) {
    $data = array();
    foreach ($FDB2CSV1 as $key => $value) {
        $data[$value['D1NAME']] = $value['D1TEXT'];
    }
   // error_log('fnCreateTableData:'.print_r($data,true));
    return $data;

}
/*
 *-------------------------------------------------------*
 * 権限チェック グループに一つも権限を持っていなかった場合、false
 *-------------------------------------------------------*
*/
function fnCheckAuth($group, $dname) {
    $rs = false;
    foreach ($group as $key => $value) {
        if (in_array($value['D1NAME'], $dname)) {
            $rs = true;
            break;
        }
    }
    return $rs;
}
//権限情報取得
function fnKengenList($db2con, $WUUID) {
    $data = array();
    $strSQL = ' SELECT A.* FROM DB2WUSR AS A ';
    $strSQL.= ' WHERE WUUID = ? ';
    $params = array($WUUID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => false);
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => false);
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) > 0) {
                $WUSAUT = $data[0]['WUSAUT'];
                $res = cmSelDB2AGRT($db2con, $WUSAUT);
                if ($res['result'] === true) {
                    $authData = array();
                    $authCount = 0;
                    $data = umEx($res['data']);
                    foreach ($data as $key => $value) {
                        $authData[$value['AGMNID']] = $value['AGFLAG'];
                    }
                    if (count($data) > 0) {
                        $resCount = cmSelDB2AGRT($db2con, $WUSAUT, '1');
                        $authCount = count($resCount['data']);
                    }
                    $data = array('result' => true, 'authData' => $authData, 'authCount' => $authCount);
                } else {
                    $data = array('result' => false);
                }
            } else {
                $data = array('result' => false);
            }
        }
    }
  //  error_log('fnKengenList:'.print_r($data,true));
    return $data;
}
/**
 *ダッシュボードのメニュー
 */
function fnGetDB2DSBList($db2con, $USR_ID) {
    $data = array();
    $rs = true;
    $params = array();
    $strSQL = '';
    $strSQL = ' SELECT * FROM DB2DSB WHERE ';
    $strSQL.= ' DSBUID =?';
    $params = array($USR_ID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
   // error_log('fnGetDB2DSBList:'.print_r($data,true));
    return array('result' => $rs, 'data' => umEx($data, true));
}
/**
 *ユーザが作成したクエリーのメニュー
 */
function fnGetDB2QHIS($db2con, $USR_ID, $pivotFlg, $graphFlg,$licenseSql) {
    $data = array();
    $rs = true;
    $params = array();
    $strSQL = '';
    $strSQL = ' SELECT ';
    $strSQL.= ' * ';
    $strSQL.= ' FROM ';
    $strSQL.= ' FDB2CSV1  A  LEFT OUTER JOIN DB2QHIS B on A.D1NAME = B.DQNAME ';
    // クエリーにピボットとグラフがあったらメニューを出す
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= '               LEFT OUTER JOIN  ';
        $strSQL.= cmpivot_graphMerge($pivotFlg, $graphFlg);
        $strSQL.= '               ON D1NAME = SUB.SUBQRY ';
    }
    $strSQL.= ' WHERE ';
    $strSQL.= ' B.DQCUSR = ? ';
    if(!$licenseSql){
        $strSQL.= ' AND A.D1CFLG = \'\' ';
    }
    $params = array($USR_ID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
   //  error_log('fnGetDB2QHIS:'.print_r($data,true));
    return array('result' => $rs, 'data' => umEx($data, false));
}

/**
 *お気に入りのクエリーリスト
 */
function fnGetDB2QBMK($db2con, $USR_ID, $pivotFlg, $graphFlg,$licenseSql) {
    $data = array();
    $rs = true;
    $params = array();
    $strSQL = '';
    $strSQL.= ' SELECT QB.QBMUSR,QB.QBMFLG,QB.QBMQRY AS D1NAME,KY.D1TEXT ';
    if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
        $strSQL.= ',SUB.SUBQRY,SUB.SUBKEY,SUB.SUBNME ,CASE WHEN SUBFLG IS NULL THEN -1 ELSE SUBFLG END AS SUBFLG';
    }else{
        $strSQL .= " ,'' AS SUBFLG ";
    }
    $strSQL .= ' FROM DB2QBMK AS QB ';
        if ($touch !== '1' && ($pivotFlg === true || $graphFlg === true)) {
            $strSQL.= '               LEFT OUTER JOIN  ';
            $strSQL.= cmpivot_graphMerge($pivotFlg, $graphFlg);
            $strSQL.= ' ON  ';
            $strSQL.= ' QB.QBMKEY = SUB.SUBQRY ';
            $strSQL.= ' AND ((QB.QBMPIV = SUB.SUBKEY AND SUB.SUBFLG = 0) ';
            $strSQL.= ' OR (QB.QBMGPH = SUB.SUBKEY AND SUB.SUBFLG = 1)) ';
        }
    $strSQL.= ' LEFT JOIN ';
    $strSQL.= ' ( ';
    $strSQL.= ' SELECT A.QBMQRY AS QRYKEY,B.D1TEXT FROM DB2QBMK AS A ';
    $strSQL.= ' LEFT JOIN FDB2CSV1 AS B ';
    $strSQL.= ' ON A.QBMQRY = B.D1NAME WHERE A.QBMFLG = 0 ';
    if(!$licenseSql){
        $strSQL.= ' AND B.D1CFLG = \'\' ';
    }
    $strSQL.= ' UNION ';
    $strSQL.= ' SELECT A.QBMQRY AS QRYKEY,C.QRYGNAME AS D1TEXT FROM DB2QBMK AS A ';
    $strSQL.= ' LEFT JOIN DB2QRYG AS C ';
    $strSQL.= ' ON A.QBMQRY = C.QRYGID WHERE A.QBMFLG = 1 ';
    $strSQL.= ' )AS KY ';
    $strSQL.= ' ON QB.QBMQRY = KY.QRYKEY ';
    $strSQL.= ' WHERE QB.QBMUSR = ? ';
    if($pivotFlg !== true){
        $strSQL.= " AND QB.QBMPIV = '' ";
    }
    if($graphFlg !== true){
        $strSQL.= " AND QB.QBMGPH = '' ";
    }
    $strSQL.= ' ORDER BY QB.QBMQRY,SUBFLG,QB.QBMPIV,QB.QBMGPH';
    $params = array($USR_ID);
    e_log("fnGetDB2QHIS AKZ". $strSQL);

    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    e_log('お気に入りクエリー');
   // error_log('fnGetDB2QBMK:'.print_r($data,true));
    return array('result' => $rs, 'data' => umEx($data, false));
}
/**
 *ユーザが作成したクエリーのメニュー
 */
function fnGetDB2WUSR($db2con, $USR_ID) {
    $data = array();
    $rs = true;
    $params = array();
    $strSQL = '';
    $strSQL = ' SELECT ';
    $strSQL.= ' WUUQFG, ';
    $strSQL.= ' WUUBFG ';
    $strSQL.= ' FROM ';
    $strSQL.= ' DB2WUSR  ';
    $strSQL.= ' WHERE WUUID = ?  ';
    $params = array($USR_ID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
   //  error_log('fnGetDB2WUSR:'.print_r($data,true));
    return array('result' => $rs, 'data' => umEx($data, true));
}
/*
 *-------------------------------------------------------*
 * ナビゲーション配列作成
 *-------------------------------------------------------*
*/
$groupIndex = 0; //グループ配列の識別要素
$queryIndex = 0; //定義の識別要素
$authQueryArray = array(); //表示したクエリーの定義名を格納
$menuId = 0; //メニューのキー　必ず一意になるもの
$hyojiFlg = true;
if ($WUAUTH === '2') {
    if ($kengenList['3'] !== '1' && $kengenList['6'] !== '1' && $kengenList['7'] !== '1') {
        if ($kengenList['8'] !== '1' && $kengenList['9'] !== '1' && $kengenList['10'] !== '1') {
            if ($kengenList['11'] !== '1' && $kengenList['12'] !== '1' && $kengenList['13'] !== '1') {
                if ($kengenList['14'] !== '1' && $kengenList['15'] !== '1' && $kengenList['16'] !== '1' && $kengenList['30'] !== '1') {
                    $hyojiFlg = false;
                }
            }
        }
    }
}
//ダッシュボードメニュー
$navDKey = array_search('NAVDASHBOARD', array_map(function ($data) {
    return $data['NAVTEXT'];
}, $navigationData));
if ($graphFlg === true) {
    //error_log("groupindex".$groupIndex.'rtr'.print_r($rtn,true));
    $rtn[$groupIndex] = array();
    $rtn[$groupIndex]['children'] = array();
    if ($navDKey !== false) {
        $queryIndex = 0;
        $rtn[$groupIndex] = array('iconCls' => 'bookmark-icon',
                                            'id' => 'NavDashBoard', 
                                            'text' => $navigationData[$navDKey]['ITMTEXT'],
                                            'leaf' => ($navigationData[$navDKey]['NAVLEAF'] == 1) ? true : false,
                                            'lastGroupFlg' => (count($side_GroupNav) === 0 && count($side_Dashboard) > 0 
                                                                    && !($WUUQFG === '1' && $WUAUTH !== '0' && $hyojiFlg === true)) ? true : false //side_GroupNavはグループあるかどうかチェック
        );
        if (count($side_Dashboard) > 0) {
            foreach ($side_Dashboard as $key => $value) {
                $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'bookmark-icon', 
                                                                            'text' => $value['DSBNAME'], 
                                                                            'leaf' => true, 
                                                                            'id' => 'bookmark_' . $value['DSBID'] . '_' . $menuId, 
                                                                            'hrefTarget' => '#bookmark/' . $value['DSBID'], 
                                                                            'DSBID' => $value['DSBID'], 
                                                                            'tblName' => $value['DSBID'],
                                                                            'menu' => 'bookmark');
                $queryIndex++;
                $menuId++;
            }
        }
        $navNewDKey = array_search('NAVNEWDASHBOARD', array_map(function ($data) {
            return $data['NAVTEXT'];
        }, $navigationData));
        if ($navNewDKey !== false) {
            $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'bookmark-icon', 
                                                                            'text' => $navigationData[$navNewDKey]['ITMTEXT'],
                                                                            'leaf' => ($navigationData[$navNewDKey]['NAVLEAF'] == 1) ? true : false,
                                                                            'id' => 'NavNewDashBoard',
                                                                            'children' => array());
        }
        $groupIndex++;
    }
}
$_SESSION['PHPQUERY']['kengenFlg'] = $hyojiFlg;
$_SESSION['PHPQUERY']['WUUQFG'] = $WUUQFG;
$_SESSION['PHPQUERY']['WUUBFG'] = $WUUBFG;
//ユーザが作成したクエリーのメニュー作成
if ($WUUQFG === '1' && $WUAUTH !== '0' && $hyojiFlg === true) {
    $queryIndex = 0;

    $navigationIndex = 6;
    if($licenseSchedule === false){
        $navigationIndex = 5;
    }

    $rtn[$groupIndex] = array(
    'id' => 'NAVUSRQRY',
    //'text' => 'ユーザが作成したクエリー',
    'text' => $navigationData[$navigationIndex]['ITMTEXT'], 
    'leaf' => ($navigationData[$navigationIndex]['NAVLEAF'] == 1) ? true : false,
    'lastGroupFlg' => (count($side_GroupNav) === 0) ? true : false);
    if (count($side_user_query) > 0) {
        $liflg = false;
        foreach ($side_user_query as $key => $value) {
            if ($value['D1NAME'] !== null && $value['D1NAME'] !== '') {
                //ピボット設定の元クエリー非表示の場合は元クエリーを非表示
                if ($value['D1MFLG'] !== '1' && ($pivotQuery !== cmHsc(cmMer($value['D1NAME'])))) {
                    $liflg = true;
                    //定義を追加
                    $pivotQuery = '';
                    $rtn[$groupIndex]['children'][$queryIndex] = array('text' => cmHsc(cmMer($value['D1TEXT'])), 
                                                                                'id' => 'USR_QRY_' . $queryIndex . '_' . $menuId, 
                                                                                'leaf' => true, 'hrefTarget' => '#query/' . cmHsc(cmMer($value['D1NAME'])),
                                                                                'tblName' => cmHsc(cmMer($value['D1NAME'])),
                                                                                'menu' => 'query');
                    $queryIndex++;
                    $menuId++;
                }
                //ピボット設定のピボット名が登録してある場合、表示
                if (cmMer($value['SUBNME']) !== '') {
                    $liflg = true;
                    $pivotQuery = cmHsc(cmMer($value['D1NAME']));
                    switch ($value['SUBFLG']) {
                        case '0':
						//	error_log("first switch case 0****".cmHsc(cmMer($value['SUBKEY'])));
                            $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'pivot-icon',
                                'text' => cmHsc(cmMer($value['SUBNME'])),
                                'id' => 'USR_QRY_PIV_' . $queryIndex . '_' . $menuId,
                                'leaf' => true,
                                'PMPKEY' => cmHsc(cmMer($value['SUBKEY'])),
                                'hrefTarget' => '#query/' . cmHsc(cmMer($value['D1NAME'])),
                                'tblName' => cmHsc(cmMer($value['D1NAME'])),
                                'menu' => 'pivot');
                        break;
                        case '1':
						//	error_log("first switch case 1***".cmHsc(cmMer($value['SUBKEY'])));
                            $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'graph-icon',
                                    'text' => cmHsc(cmMer($value['SUBNME'])),
                                    'id' => 'USR_QRY_PIV_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                    'leaf' => true,
                                    'GPHKEY' => cmHsc(cmMer($value['SUBKEY'])),
                                    'hrefTarget' => '#query/' . cmHsc(cmMer($value['D1NAME'])),
                                    'tblName' => cmHsc(cmMer($value['D1NAME'])),
                                    'menu' => 'graph');
                        break;
                    }
                    $queryIndex++;
                    $menuId++;
                }
            }
        }
        if ($liflg === false) {
            //クエリーなしを追加
            $rtn[$groupIndex]['children'][$queryIndex] = array('text' => $navigationData[0]['ITMTEXT'],
                'leaf' => ($navigationData[0]['NAVLEAF'] == 1) ? true : false,
                'id' => 'noquery_' . $menuId);
            $menuId++;
        }
    } else {
        //クエリーなしを追加
        $rtn[$groupIndex]['children'][$queryIndex] = array('text' => $navigationData[0]['ITMTEXT'],
                'leaf' => ($navigationData[0]['NAVLEAF'] == 1) ? true : false,
                'id' => 'noquery_' . $menuId);
        $menuId++;
    }
    $groupIndex++;
}
$navBKey = array_search('NAVBMKQRY', array_map(function ($data) {
    return $data['NAVTEXT'];
}, $navigationData));
if ($WUUBFG === '1'){
    $queryIndex = 0;
    $rtn[$groupIndex] = array(
    'id' => 'NAVBMKQRY',
    'text' => $navigationData[$navBKey]['ITMTEXT'], 
    'leaf' => ($navigationData[$navBKey]['NAVLEAF'] == 1) ? true : false,
    'lastGroupFlg' => (count($side_GroupNav) === 0) ? true : false);
    if (count($side_bookmark_query) > 0) {
        $liflg = false;
        foreach ($side_bookmark_query as $key => $value) {
            $chksqlflg = true;$submenu = true;
            if ($value['D1NAME'] !== null && $value['D1NAME'] !== '') {
                if(!$licenseSql && $sgpVal['QRYGFLG'] === '1'){
                   // $chksql = fnGetQryFromGroup($db2con, $sgpVal['D1NAME']);
                    $chksql = cmGetQueryGroup($db2con,$value['D1NAME']);
                    if(count($chksql['data']) > 0){
                        foreach($chksql['data'] as $key => $val){
                            if($val['D1CFLG'] === '1'){
                                if (cmMer($sgpVal['SUBNME']) !== '') {
                                    $res = fnGetQryFromGroup($db2con, $val['QRYGSQRY'],cmMer($sgpVal['SUBKEY']));
                                    if(count($res['data']) > 0){
                                        $submenu = false;
                                    }
                                }
                            }else{
                                $chksqlflg = false;
                            }
                        }
                    }
                    if($chksqlflg){
                        continue;
                    }
                }
                //ピボット設定の元クエリー非表示の場合は元クエリーを非表示
               // if ($value['D1MFLG'] !== '1' && ($pivotQuery !== cmHsc(cmMer($value['D1NAME'])))) {
                if ($licensePivot === false || $value['SUBFLG'] === '-1') {
                    $liflg = true;
                    //定義を追加
                    $pivotQuery = '';
                   /* $rtn[$groupIndex]['children'][$queryIndex] = array(
                                                                'text' => cmHsc(cmMer($value['D1TEXT'])), 
                                                                'id' => 'USR_QRY_' . $queryIndex . '_' . $menuId, 
                                                                'leaf' => true, 'hrefTarget' => '#query/' . cmHsc(cmMer($value['D1NAME'])),
                                                                'tblName' => cmHsc(cmMer($value['D1NAME'])),
                                                                'menu' => 'query');*/
                    switch(cmMer($value['QBMFLG'])){
                        case '1':
                        $rtn[$groupIndex]['children'][$queryIndex] =  array(
                                                        'iconCls' => 'qgroup-icon',
                                                        'text' => cmHsc(cmMer($value['D1TEXT'])), 
                                                        'id' => 'NAV_QRYGPBMK_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                                        'leaf' => true, 
                                                        'hrefTarget' => '#query/' . cmHsc(cmMer($value['D1NAME'])),
                                                        'tblName' => cmHsc(cmMer($value['D1NAME'])),
                                                        'menu' => 'queryGp',
                                                        'groupIndex' => $groupIndex);
                        break;
                        default:
                        case '0':
                        $rtn[$groupIndex]['children'][$queryIndex] =  array(
                                                        'text' => cmHsc(cmMer($value['D1TEXT'])),
                                                        'id' => 'NAV_QRYBMK_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                                        'leaf' => true,
                                                        'hrefTarget' => '#query/' . cmHsc(cmMer($value['D1NAME'])),
                                                        'tblName' => cmHsc(cmMer($value['D1NAME'])),
                                                        'menu' => 'query',
                                                        'groupIndex' => $groupIndex);
                        break;
                   }
                    $queryIndex++;
                    $menuId++;
                }
                if(!$submenu){
                    $pivotQuery = cmHsc(cmMer($sgpVal['D1NAME']));
                    continue;
                }
                //ピボット設定のピボット名が登録してある場合、表示
                if (cmMer($value['SUBNME']) !== '') {
                    $liflg = true;
                    $pivotQuery = cmHsc(cmMer($value['D1NAME']));
                    switch ($value['SUBFLG']) {
                        case '0':
                            switch(cmMer($value['QBMFLG'])){
                                case '1':
								//	error_log("second switch case 1****".cmHsc(cmMer($value['SUBKEY'])));
                                    $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'pivot-icon',
                                       'text' => cmHsc(cmMer($value['SUBNME'])),
                                       'id' => 'BMK_QRY_PIVGP_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                       'leaf' => true,
                                       'PMPKEY' => cmHsc(cmMer($value['SUBKEY'])),
                                       'hrefTarget' => '#query/' . cmHsc(cmMer($value['D1NAME'])),
                                       'tblName' => cmHsc(cmMer($value['D1NAME'])),
                                       'menu' => 'pivotGp', 'groupIndex' => $groupIndex);

                                break;
                                default:
                                case '0':
								//	error_log("second swtich case 0 ****".cmHsc(cmMer($value['SUBKEY'])));
                                    $rtn[$groupIndex]['children'][$queryIndex] =  array('iconCls' => 'pivot-icon',
                                       'text' => cmHsc(cmMer($value['SUBNME'])),
                                       'id' => 'BMK_QRY_PIV_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                       'leaf' => true,
                                       'PMPKEY' => cmHsc(cmMer($value['SUBKEY'])),
                                       'hrefTarget' => '#query/' . cmHsc(cmMer($value['D1NAME'])),
                                       'tblName' => cmHsc(cmMer($value['D1NAME'])),
                                       'menu' => 'pivot', 'groupIndex' => $groupIndex);
                                break;
                            }
                        break;
                        case '1':
                            switch(cmMer($value['QBMFLG'])){
                                case '1':
                                    $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'graph-icon',
                                            'text' => cmHsc(cmMer($value['SUBNME'])),
                                            'id' => 'NAV_PIVGP_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                            'leaf' => true,
                                            'GPHKEY' => cmHsc(cmMer($value['SUBKEY'])),
                                            'hrefTarget' => '#query/' . cmHsc(cmMer($value['D1NAME'])),
                                            'tblName' => cmHsc(cmMer($value['D1NAME'])),
                                            'menu' => 'graphGp',
                                            'groupIndex' => $groupIndex);
                                break;
                                default:
                                case '0':
                                   $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'graph-icon',
                                            'text' => cmHsc(cmMer($value['SUBNME'])),
                                            'id' => 'NAV_PIV_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                            'leaf' => true,
                                            'GPHKEY' => cmHsc(cmMer($value['SUBKEY'])),
                                            'hrefTarget' => '#query/' . cmHsc(cmMer($value['D1NAME'])),
                                            'tblName' => cmHsc(cmMer($value['D1NAME'])),
                                            'menu' => 'graph',
                                            'groupIndex' => $groupIndex);
                                break;
                        }
                        break;
                    }
                    $queryIndex++;
                    $menuId++;
                }
            }
        }
        if ($liflg === false) {
            //クエリーなしを追加
            $rtn[$groupIndex]['children'][$queryIndex] = array('text' => $navigationData[0]['ITMTEXT'],
                'leaf' => ($navigationData[0]['NAVLEAF'] == 1) ? true : false,
                'id' => 'noquery_' . $menuId);
            $menuId++;
        }
    } else {
        //クエリーなしを追加
        $rtn[$groupIndex]['children'][$queryIndex] = array('text' => $navigationData[0]['ITMTEXT'],
                'leaf' => ($navigationData[0]['NAVLEAF'] == 1) ? true : false,
                'id' => 'noquery_' . $menuId);
        $menuId++;
    }
    $groupIndex++;
}
$queryIndex = 0;
if (count($side_GroupNav) > 0) {
    foreach ($side_GroupNav as $gpKey => $gpValue) {
        $lastGroupFlg = false;
        if (($gpKey + 1) === count($side_GroupNav)) {
            $lastGroupFlg = true;
        }
        //グループ追加
        $rtn[$groupIndex] = array();
        $pivotQuery = '';
        //定義追加
        $rtn[$groupIndex]['children'] = array();
        $liflg = false; //グループ内にユーザーが利用できる定義があるかのフラグ kalayar testing liflg
        //ｇｐ追加
        if (count($gpValue) > 0) {
            foreach ($gpValue as $sgpKey => $sgpVal) {
                $chksqlflg = true;$submenu = true;
                if ($sgpVal['FLG'] === 0) {
                    $rtn[$groupIndex]['text'] = cmHsc(cmMer($sgpVal['WQUNAM']));
                    $rtn[$groupIndex]['id'] = 'NAV_GP_' . $groupIndex;
                    $rtn[$groupIndex]['leaf'] = false;
                    $rtn[$groupIndex]['lastGroupFlg'] = $lastGroupFlg;
                    if ($sgpVal['D1NAME'] !== null && $sgpVal['D1NAME'] !== '') {
                        if(!$licenseSql && $sgpVal['QRYGFLG'] === '1'){
                           // $chksql = fnGetQryFromGroup($db2con, $sgpVal['D1NAME']);
                            $chksql = cmGetQueryGroup($db2con,$sgpVal['D1NAME']);
                            if(count($chksql['data']) > 0){
                                foreach($chksql['data'] as $key => $val){
                                    if($val['D1CFLG'] === '1'){
                                        if (cmMer($sgpVal['SUBNME']) !== '') {
                                            $res = fnGetQryFromGroup($db2con, $val['QRYGSQRY'],cmMer($sgpVal['SUBKEY']));
                                            if(count($res['data']) > 0){
                                                $submenu = false;
                                            }
                                        }
                                    }else{
                                        $chksqlflg = false;
                                    }
                                }
                            }
                            if($chksqlflg){
                                continue;
                            }
                        }
                        //ピボット設定の元クエリー非表示の場合は元クエリーを非表示
                        if ($sgpVal['D1MFLG'] !== '1' && ($pivotQuery !== cmHsc(cmMer($sgpVal['D1NAME'])))) {
                            $liflg = true;
                            //定義を追加
                            $pivotQuery = '';
                            switch(cmMer($sgpVal['QRYGFLG'])){
                                case '1':
                                    $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'qgroup-icon',
                                                                'text' => cmHsc(cmMer($sgpVal['D1TEXT'])),
                                                                'id' => 'NAV_QRYGP_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                                                'leaf' => true, 
                                                                'hrefTarget' => '#query/' . cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                'tblName' => cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                'menu' => 'queryGp',
                                                                'groupIndex' => $groupIndex);

                                break;
                                default:
                                    $rtn[$groupIndex]['children'][$queryIndex] = array('text' => cmHsc(cmMer($sgpVal['D1TEXT'])),
                                                                'id' => 'NAV_QRY_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                                                'leaf' => true,
                                                                'hrefTarget' => '#query/' . cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                'tblName' => cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                'menu' => 'query',
                                                                'groupIndex' => $groupIndex);
                                break;
                                
                            }
                            $queryIndex++;
                            $menuId++;
                        }
                        if(!$submenu){
                            $pivotQuery = cmHsc(cmMer($sgpVal['D1NAME']));
                            continue;
                        }
                        //ピボット設定のピボット名が登録してある場合、表示
                        if (cmMer($sgpVal['SUBNME']) !== '') {
                            $liflg = true;
                            $pivotQuery = cmHsc(cmMer($sgpVal['D1NAME']));
                            switch ($sgpVal['SUBFLG']) {
                                case '0':
                                    switch(cmMer($sgpVal['QRYGFLG'])){
                                        case '1':
											//	error_log("third switch case 1***".cmHsc(cmMer($sgpVal['SUBKEY'])));
                                                $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'pivot-icon',
                                                                                                'text' => cmHsc(cmMer($sgpVal['SUBNME'])),
                                                                                                'id' => 'NAV_PIVGP_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                                                                                'leaf' => true,
                                                                                                'PMPKEY' => cmHsc(cmMer($sgpVal['SUBKEY'])),
                                                                                                'hrefTarget' => '#query/' . cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                                                'tblName' => cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                                                'menu' => 'pivotGp', 'groupIndex' => $groupIndex);
                                        break;
                                        default:
										//		error_log("third switch case default***".cmHsc(cmMer($sgpVal['SUBKEY'])));
                                                $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'pivot-icon',
                                                                                                'text' => cmHsc(cmMer($sgpVal['SUBNME'])),
                                                                                                'id' => 'NAV_PIV_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                                                                                'leaf' => true,
                                                                                                'PMPKEY' => cmHsc(cmMer($sgpVal['SUBKEY'])),
                                                                                                'hrefTarget' => '#query/' . cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                                                'tblName' => cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                                                'menu' => 'pivot', 'groupIndex' => $groupIndex);
                                        break;
                                    }
                                    $queryIndex++;
                                    $menuId++;
                                break;
                                case '1':
                                    switch(cmMer($sgpVal['QRYGFLG'])){
                                        case '1':
                                                $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'graph-icon',
                                                                                                'text' => cmHsc(cmMer($sgpVal['SUBNME'])),
                                                                                                'id' => 'NAV_PIVGP_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                                                                                'leaf' => true,
                                                                                                'GPHKEY' => cmHsc(cmMer($sgpVal['SUBKEY'])),
                                                                                                'hrefTarget' => '#query/' . cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                                                'tblName' => cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                                                'menu' => 'graphGp',
                                                                                                'groupIndex' => $groupIndex);
                                        break;
                                        default:
                                                $rtn[$groupIndex]['children'][$queryIndex] = array('iconCls' => 'graph-icon',
                                                                                                'text' => cmHsc(cmMer($sgpVal['SUBNME'])),
                                                                                                'id' => 'NAV_PIV_' . $groupIndex . '_' . $queryIndex . '_' . $menuId,
                                                                                                'leaf' => true,
                                                                                                'GPHKEY' => cmHsc(cmMer($sgpVal['SUBKEY'])),
                                                                                                'hrefTarget' => '#query/' . cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                                                'tblName' => cmHsc(cmMer($sgpVal['D1NAME'])),
                                                                                                'menu' => 'graph',
                                                                                                'groupIndex' => $groupIndex);
                                        break;
                                    }

                                    $queryIndex++;
                                    $menuId++;
                                break;
                            }

                        }
                        $authQueryArray[] = cmHsc(cmMer($sgpVal['D1NAME']));
                    }/* else {
                        //クエリーなしを追加
                        $rtn[$groupIndex]['children'][$queryIndex] = array('text' => $navigationData[0]['ITMTEXT'],
                                                                                    'leaf' => ($navigationData[0]['NAVLEAF'] == 1) ? true : false,
                                                                                    'id' => 'noquery_' . $groupIndex . '_' . $menuId);
                        error_log("Value of gp esle*****".print_r($rtn[$groupIndex]['children'][$queryIndex],true));
                        $menuId++;
                        $liflg = true;
                    }*/
                    if ($liflg === false) {
                        //クエリーなしを追加
                        $rtn[$groupIndex]['children'][$queryIndex] = array('text' => $navigationData[0]['ITMTEXT'],
                                                                                    'leaf' => ($navigationData[0]['NAVLEAF'] == 1) ? true : false,
                                                                                    'id' => 'noquery_' . $groupIndex . '_' . $menuId);
                        $menuId++;
                    }
                    //$queryIndex = 0;
                    //$testgpIndex++;
                    
                } else {
                    if (count($sgpVal) > 0) {
                        $liflg = true;
                        /*e_log('サブグループがあるデータマスタグルぷデータ++++++++：'.$sgpKey.print_r($sgpVal,true));
                        e_log('Subgp1dataLLLLLLLLLLL'.print_r($sgpVal,true));*/
                        $subRs = array();
                        $pivotQuery = '';
                        $subRs['children'] = array();
                        $subQryIdx = 0;
                        $subLiflg = false;
                        foreach ($sgpVal as $s1Key => $s1Val) {
                            //e_log('MMM1subValArr********'.$s1Key.print_r($s1Val,true));
                            if ($s1Val['FLG'] === 1) {
                                $subRs['text'] = cmHsc(cmMer($s1Val['WQUNAM']));
                                $subRs['id'] = 'NAV_GP_' . $groupIndex . '_' . $queryIndex;
                                $subRs['leaf'] = false;
                                $subRs['lastGroupFlg'] = $lastGroupFlg;
                                if ($s1Val['D1NAME'] !== null && $s1Val['D1NAME'] !== '') {
                                    if(!$licenseSql && $s1Val['QRYGFLG'] === '1'){
                                       // $chksql = fnGetQryFromGroup($db2con, $sgpVal['D1NAME']);
                                        $chksql = cmGetQueryGroup($db2con,$s1Val['D1NAME']);
                                        if(count($chksql['data']) > 0){
                                            foreach($chksql['data'] as $key => $val){
                                                if($val['D1CFLG'] === '1'){
                                                    if (cmMer($s1Val['SUBNME']) !== '') {
                                                        $res = fnGetQryFromGroup($db2con, $val['QRYGSQRY'],cmMer($s1Val['SUBKEY']));
                                                        if(count($res['data']) > 0){
                                                            $submenu = false;
                                                        }
                                                    }
                                                }else{
                                                    $chksqlflg = false;
                                                }
                                            }
                                        }
                                        if($chksqlflg){
                                            continue;
                                        }
                                    }
                                    //ピボット設定の元クエリー非表示の場合は元クエリーを非表示
                                    if ($s1Val['D1MFLG'] !== '1' && ($pivotQuery !== cmHsc(cmMer($s1Val['D1NAME'])))) {
                                        $subLiflg = true;
                                        //定義を追加
                                        $pivotQuery = '';
                                        switch(cmMer($s1Val['QRYGFLG'])){
                                            case '1':
                                                    $subRs['children'][$subQryIdx] = array('iconCls' => 'qgroup-icon',
                                                                    'text' => cmHsc(cmMer($s1Val['D1TEXT'])),
                                                                    'id' => 'NAV_QRYGP_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $menuId,
                                                                    'leaf' => true,
                                                                    'hrefTarget' => '#query/' . cmHsc(cmMer($s1Val['D1NAME'])),
                                                                    'tblName' => cmHsc(cmMer($s1Val['D1NAME'])),
                                                                    'menu' => 'queryGp',
                                                                    'groupIndex' => $groupIndex);
                                            break;
                                            default:
                                                    $subRs['children'][$subQryIdx] = array('text' => cmHsc(cmMer($s1Val['D1TEXT'])),
                                                                    'id' => 'NAV_QRY_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $menuId,
                                                                    'leaf' => true,
                                                                    'hrefTarget' => '#query/' . cmHsc(cmMer($s1Val['D1NAME'])),
                                                                    'tblName' => cmHsc(cmMer($s1Val['D1NAME'])),
                                                                    'menu' => 'query',
                                                                    'groupIndex' => $groupIndex);
                                            break;
                                        }

                                        $subQryIdx++;
                                        $menuId++;
                                    }
                                    if(!$submenu){
                                        $pivotQuery = cmHsc(cmMer($sgpVal['D1NAME']));
                                        continue;
                                    }
                                    //ピボット設定のピボット名が登録してある場合、表示
                                    if (cmMer($s1Val['SUBNME']) !== '') {
                                        $subLiflg = true;
                                        $pivotQuery = cmHsc(cmMer($s1Val['D1NAME']));
                                        switch ($s1Val['SUBFLG']) {
                                            case '0':
                                                switch(cmMer($s1Val['QRYGFLG'])){
                                                    case '1':
													//	error_log("fourth switch case 1***".cmHsc(cmMer($s1Val['SUBKEY'])));
                                                        $subRs['children'][$subQryIdx] = array('iconCls' => 'pivot-icon',
                                                                                            'text' => cmHsc(cmMer($s1Val['SUBNME'])),
                                                                                            'id' => 'NAV_PIVGP_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $menuId,
                                                                                            'leaf' => true,
                                                                                            'PMPKEY' => cmHsc(cmMer($s1Val['SUBKEY'])),
                                                                                            'hrefTarget' => '#query/' . cmHsc(cmMer($s1Val['D1NAME'])),
                                                                                            'tblName' => cmHsc(cmMer($s1Val['D1NAME'])),
                                                                                            'menu' => 'pivotGp',
                                                                                            'groupIndex' => $groupIndex);

                                                    break;
                                                    default:
												//		error_log("fourth switch case default***".cmHsc(cmMer($s1Val['SUBKEY'])));
                                                        $subRs['children'][$subQryIdx] = array('iconCls' => 'pivot-icon',
                                                                                            'text' => cmHsc(cmMer($s1Val['SUBNME'])),
                                                                                            'id' => 'NAV_PIV_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $menuId,
                                                                                            'leaf' => true,
                                                                                            'PMPKEY' => cmHsc(cmMer($s1Val['SUBKEY'])),
                                                                                            'hrefTarget' => '#query/' . cmHsc(cmMer($s1Val['D1NAME'])),
                                                                                            'tblName' => cmHsc(cmMer($s1Val['D1NAME'])),
                                                                                            'menu' => 'pivot',
                                                                                            'groupIndex' => $groupIndex);

                                                    break;

                                                }
                                            break;
                                            case '1':
                                                switch(cmMer($s1Val['QRYGFLG'])){
                                                    case '1':
                                                        $subRs['children'][$subQryIdx] = array('iconCls' => 'graph-icon',
                                                                                                    'text' => cmHsc(cmMer($s1Val['SUBNME'])),
                                                                                                    'id' => 'NAV_PIVGP_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $menuId,
                                                                                                    'leaf' => true,
                                                                                                    'GPHKEY' => cmHsc(cmMer($s1Val['SUBKEY'])),
                                                                                                    'hrefTarget' => '#query/' . cmHsc(cmMer($s1Val['D1NAME'])),
                                                                                                    'tblName' => cmHsc(cmMer($s1Val['D1NAME'])),
                                                                                                    'menu' => 'graphGp',
                                                                                                    'groupIndex' => $groupIndex);
                                                    break;
                                                    default:
                                                        $subRs['children'][$subQryIdx] = array('iconCls' => 'graph-icon',
                                                                                                    'text' => cmHsc(cmMer($s1Val['SUBNME'])),
                                                                                                    'id' => 'NAV_PIV_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $menuId,
                                                                                                    'leaf' => true,
                                                                                                    'GPHKEY' => cmHsc(cmMer($s1Val['SUBKEY'])),
                                                                                                    'hrefTarget' => '#query/' . cmHsc(cmMer($s1Val['D1NAME'])),
                                                                                                    'tblName' => cmHsc(cmMer($s1Val['D1NAME'])),
                                                                                                    'menu' => 'graph',
                                                                                                    'groupIndex' => $groupIndex);
                                                    break;
                                                }
                                            break;
                                        }
                                        $subQryIdx++;
                                        $menuId++;
                                    }
                                    $authQueryArray[] = cmHsc(cmMer($s1Val['D1NAME']));
                                }
                                if ($subLiflg === false) {
                                    //クエリーなしを追加
                                    $subRs['children'][$subQryIdx] = array('text' => $navigationData[0]['ITMTEXT'],
                                                                                'leaf' => ($navigationData[0]['NAVLEAF'] == 1) ? true : false,
                                                                                'id' => 'noquery_' . $menuId);
                                    $menuId++;
                                }
                            } else {
                                if (count($s1Val) > 0) {
                                    //e_log('Subgp2data'.print_r($s1Val,true));
                                    $liflg = true;
                                    $subLiflg = true;
                                    $sub2Rs = array();
                                    $pivotQuery = '';
                                    $sub2Rs['children'] = array();
                                    $sub2QryIdx = 0;
                                    $sub2Liflg = false;
                                    foreach ($s1Val as $s2Key => $s2Val) {
                                        if ($s2Val['FLG'] === 2) {
                                            $sub2Rs['text'] = cmHsc(cmMer($s2Val['WQUNAM']));
                                            $sub2Rs['id'] = 'NAV_GP_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx;
                                            $sub2Rs['leaf'] = false;
                                            $sub2Rs['lastGroupFlg'] = $lastGroupFlg;
                                            if ($s2Val['D1NAME'] !== null && $s2Val['D1NAME'] !== '') {
                                                if(!$licenseSql && $s2Val['QRYGFLG'] === '1'){
                                                   // $chksql = fnGetQryFromGroup($db2con, $sgpVal['D1NAME']);
                                                    $chksql = cmGetQueryGroup($db2con,$s2Val['D1NAME']);
                                                    if(count($chksql['data']) > 0){
                                                        foreach($chksql['data'] as $key => $val){
                                                            if($val['D1CFLG'] === '1'){
                                                                if (cmMer($s2Val['SUBNME']) !== '') {
                                                                    $res = fnGetQryFromGroup($db2con, $val['QRYGSQRY'],cmMer($s2Val['SUBKEY']));
                                                                    if(count($res['data']) > 0){
                                                                        $submenu = false;
                                                                    }
                                                                }
                                                            }else{
                                                                $chksqlflg = false;
                                                            }
                                                        }
                                                    }
                                                    if($chksqlflg){
                                                        continue;
                                                    }
                                                }
                                                //ピボット設定の元クエリー非表示の場合は元クエリーを非表示
                                                if ($sub2Rs['D1MFLG'] !== '1' && ($pivotQuery !== cmHsc(cmMer($s2Val['D1NAME'])))) {
                                                    $sub2Liflg = true;
                                                    //定義を追加
                                                    $pivotQuery = '';
                                                    switch(cmMer($s2Val['QRYGFLG'])){
                                                        case '1':
                                                            $sub2Rs['children'][$sub2QryIdx] = array('iconCls' => 'qgroup-icon',
                                                                    'text' => cmHsc(cmMer($s2Val['D1TEXT'])),
                                                                    'id' => 'NAV_QRYGP_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $sub2QryIdx . '_' . $menuId,
                                                                    'leaf' => true,
                                                                    'hrefTarget' => '#query/' . cmHsc(cmMer($s2Val['D1NAME'])),
                                                                    'tblName' => cmHsc(cmMer($s2Val['D1NAME'])),
                                                                    'menu' => 'queryGp',
                                                                    'groupIndex' => $groupIndex);

                                                        break;
                                                        default:
                                                            $sub2Rs['children'][$sub2QryIdx] = array('text' => cmHsc(cmMer($s2Val['D1TEXT'])),
                                                                    'id' => 'NAV_QRY_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $sub2QryIdx . '_' . $menuId,
                                                                    'leaf' => true,
                                                                    'hrefTarget' => '#query/' . cmHsc(cmMer($s2Val['D1NAME'])),
                                                                    'tblName' => cmHsc(cmMer($s2Val['D1NAME'])),
                                                                    'menu' => 'query',
                                                                    'groupIndex' => $groupIndex);
                                                        break;
                                                    }

                                                    $sub2QryIdx++;
                                                    $menuId++;
                                                }
                                                if(!$submenu){
                                                    $pivotQuery = cmHsc(cmMer($sgpVal['D1NAME']));
                                                    continue;
                                                }
                                                //ピボット設定のピボット名が登録してある場合、表示
                                                if (cmMer($s2Val['SUBNME']) !== '') {
                                                    $sub2Liflg = true;
                                                    $pivotQuery = cmHsc(cmMer($s2Val['D1NAME']));
                                                    switch ($s2Val['SUBFLG']) {
                                                        case '0':
                                                            switch(cmMer($s2Val['QRYGFLG'])){
                                                                case '1':
															//		error_log("fifth switch case 1***".cmHsc(cmMer($s2Val['SUBKEY'])));
                                                                    $sub2Rs['children'][$sub2QryIdx] = array('iconCls' => 'pivot-icon',
                                                                                                                    'text' => cmHsc(cmMer($s2Val['SUBNME'])),
                                                                                                                    'id' => 'NAV_PIVGP_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $sub2QryIdx . '_' . $menuId,
                                                                                                                    'leaf' => true,
                                                                                                                    'PMPKEY' => cmHsc(cmMer($s2Val['SUBKEY'])),
                                                                                                                    'hrefTarget' => '#query/' . cmHsc(cmMer($s2Val['D1NAME'])),
                                                                                                                    'tblName' => cmHsc(cmMer($s2Val['D1NAME'])),
                                                                                                                    'menu' => 'pivotGp',
                                                                                                                    'groupIndex' => $groupIndex);
                                                                break;
                                                                default:
															//		error_log("fifth switch case default***".cmHsc(cmMer($s2Val['SUBKEY'])));
                                                                    $sub2Rs['children'][$sub2QryIdx] = array('iconCls' => 'pivot-icon',
                                                                                                                    'text' => cmHsc(cmMer($s2Val['SUBNME'])),
                                                                                                                    'id' => 'NAV_PIV_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $sub2QryIdx . '_' . $menuId,
                                                                                                                    'leaf' => true,
                                                                                                                    'PMPKEY' => cmHsc(cmMer($s2Val['SUBKEY'])),
                                                                                                                    'hrefTarget' => '#query/' . cmHsc(cmMer($s2Val['D1NAME'])),
                                                                                                                    'tblName' => cmHsc(cmMer($s2Val['D1NAME'])),
                                                                                                                    'menu' => 'pivot',
                                                                                                                    'groupIndex' => $groupIndex);
                                                                break;
                                                            }
                                                        break;
                                                        case '1':
                                                            switch(cmMer($s2Val['QRYGFLG'])){
                                                                case '1':
                                                                    $sub2Rs['children'][$sub2QryIdx] = array('iconCls' => 'graph-icon',
                                                                                                            'text' => cmHsc(cmMer($s2Val['SUBNME'])),
                                                                                                            'id' => 'NAV_PIVGP_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $sub2QryIdx . '_' . $menuId,
                                                                                                            'leaf' => true,
                                                                                                            'GPHKEY' => cmHsc(cmMer($s2Val['SUBKEY'])),
                                                                                                            'hrefTarget' => '#query/' . cmHsc(cmMer($s2Val['D1NAME'])),
                                                                                                            'tblName' => cmHsc(cmMer($s2Val['D1NAME'])),
                                                                                                            'menu' => 'graphGp',
                                                                                                            'groupIndex' => $groupIndex);
                                                                break;
                                                                default:
                                                                    $sub2Rs['children'][$sub2QryIdx] = array('iconCls' => 'graph-icon',
                                                                                                            'text' => cmHsc(cmMer($s2Val['SUBNME'])),
                                                                                                            'id' => 'NAV_PIV_' . $groupIndex . '_' . $queryIndex . '_' . $subQryIdx . '_' . $sub2QryIdx . '_' . $menuId,
                                                                                                            'leaf' => true,
                                                                                                            'GPHKEY' => cmHsc(cmMer($s2Val['SUBKEY'])),
                                                                                                            'hrefTarget' => '#query/' . cmHsc(cmMer($s2Val['D1NAME'])),
                                                                                                            'tblName' => cmHsc(cmMer($s2Val['D1NAME'])),
                                                                                                            'menu' => 'graph',
                                                                                                            'groupIndex' => $groupIndex);
                                                                break;
                                                            }

                                                        break;
                                                    }
                                                    $sub2QryIdx++;
                                                    $menuId++;
                                                }
                                                $authQueryArray[] = cmHsc(cmMer($s2Val['D1NAME']));
                                            }
                                            if ($sub2Liflg === false) {
                                                //クエリーなしを追加
                                                $sub2Rs['children'][$sub2QryIdx] = array('text' => $navigationData[0]['ITMTEXT'],
                                                                                            'leaf' => ($navigationData[0]['NAVLEAF'] == 1) ? true : false,
                                                                                            'id' => 'noquery_' . $menuId);
                                                $menuId++;
                                            }
                                        }
                                    }
                                    $subRs['children'][$subQryIdx] = $sub2Rs;
                                    $pivotQuery = '';
                                    $subQryIdx++;
                                    $menuId++;
                                }
                            }
                        }
                        //e_log('SUB1GPDATA'.$groupIndex.'_'.$queryIndex.print_r($subRs,true));
                        $rtn[$groupIndex]['children'][$queryIndex] = $subRs;
                        $pivotQuery = '';
                        $queryIndex++;
                        $menuId++;
                    } else {
                        $liflg = false;
                        
                    }
                }
            }
        }
        $queryIndex = 0;
        $groupIndex++;
    }
}
cmDb2Close($db2con);

//スケジュール実行結果
if ($scheduleFlg === true) {
    $queryIndex = 0;
    $rtn[$groupIndex] = array('id' => 'Schedule',
                                'text' => $navigationData[1]['ITMTEXT'],
                                'leaf' => ($navigationData[1]['NAVLEAF'] == 1) ? true : false);
    if (count($side_Schedule) > 0) {
        $liflg = false;
        foreach ($side_Schedule as $key => $value) {
            //権限チェック authQueryArrayにあるものだけ表示
            if (in_array(cmHsc(cmMer($value['WHNAME'])), $authQueryArray)) {
                $liflg = true;
                //定義を追加
                $rtn[$groupIndex]['children'][$queryIndex] = array('text' => cmHsc(cmMer($tabledata[$value['WHNAME']])), 'leaf' => true, 'id' => 'Schedule_' . cmHsc(cmMer($value['WHNAME'])) . '_' . $menuId, 'hrefTarget' => '#schedule/' . cmHsc(cmMer($value['WHNAME'])), 'tblName' => cmHsc(cmMer($value['WHNAME'])), 'menu' => 'schedule');
                $queryIndex++;
                $menuId++;
            }
        }
        if ($liflg === false) {
            //クエリーなしを追加
            $rtn[$groupIndex]['children'][$queryIndex] = array(
            /*'id'   => 'NAV'.'noquery_'.$menuId,
                'text' => 'クエリーが存在しません',
                'leaf' => true*/
            'text' => $navigationData[0]['ITMTEXT'], 'leaf' => ($navigationData[0]['NAVLEAF'] == 1) ? true : false, 'id' => 'noquery_' . $menuId);
            $menuId++;
        }
    } else {
        //クエリーなしを追加
        $rtn[$groupIndex]['children'][$queryIndex] = array(
        /*'id'   => 'noquery_'.$menuId,
            'text' => 'クエリーが存在しません',
            'leaf' => true*/
        'text' => $navigationData[0]['ITMTEXT'], 'leaf' => ($navigationData[0]['NAVLEAF'] == 1) ? true : false, 'id' => 'noquery_' . $menuId);
        $menuId++;
    }
    $groupIndex++;
}
//マスター(管理者のみ表示)
$masterIndex = 0;
//if($usrinfo[0]['WUAUTH'] == '1'  || $usrinfo[0]['WUAUTH'] == '2'){
//e_log('AUTHENTICATION:'.$usrinfo[0]['WUAUTH']);
if ($WUAUTH == '1' || $WUAUTH == '3' || $WUAUTH == '4' || ($WUAUTH == '2' && $kengenListCount > 0)) {
    //e_log('AUTHENTICATION①:'.$usrinfo[0]['WUAUTH']);
    //e_log('ナビケーションデータ①：'.print_r($navigationData,true));
    foreach ($navigationData as $navData) {
        if (intval($navData[NAVID]) === 2) {
            $rtn[$groupIndex] = array('text' => $navData['ITMTEXT'], 'leaf' => $navData['NAVLEAF'], 'id' => 'Master');
        }
        if ($navData[NAVMENU] === master) {
            //e_log('ナビケーションデータ：'.print_r($navData,true));
            //e_log('権限リスト：'.print_r($kengenList,true));
            $showFlg = true;
            //e_log('権限の情報取得①:'.print_r($usrinfo,true));
            if ($WUAUTH == '2') {
                e_log('	kokohaittano');
                if ($navData[NAVTEXT] === 'FRM00008' && $kengenList['1'] !== '1' && $kengenList['6'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00012' && $kengenList['2'] !== '1' && $kengenList['4'] !== '1' && $kengenList['5'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00017') {
                    if ($kengenList['3'] !== '1' && $kengenList['6'] !== '1' && $kengenList['7'] !== '1') {
                        if ($kengenList['8'] !== '1' && $kengenList['9'] !== '1' && $kengenList['10'] !== '1') {
                            if ($kengenList['11'] !== '1' && $kengenList['12'] !== '1' && $kengenList['13'] !== '1') {
                                if ($kengenList['14'] !== '1' && $kengenList['15'] !== '1' && $kengenList['16'] !== '1' && $kengenList['30'] !== '1') {
                                    $showFlg = false;
                                }
                            }
                        }
                    }
                } else if ($navData[NAVTEXT] === 'FRM00048' && $kengenList['17'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00059' && $kengenList['18'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00064' && $kengenList['19'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00068' && $kengenList['20'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00069' && $kengenList['21'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00070' && $kengenList['22'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00071' && $kengenList['23'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00074' && $kengenList['24'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00077' && $kengenList['25'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00080' && $kengenList['26'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00083' && $kengenList['28'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00084' && $kengenList['29'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00101' && $kengenList['27'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00122' && $kengenList['31'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00126' && $kengenList['32'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00127' && $kengenList['33'] !== '1') {
                    $showFlg = false;
                } else if ($navData[NAVTEXT] === 'FRM00137' && $kengenList['34'] !== '1') {
                    $showFlg = false;
                }
            } else if ($WUAUTH === '3' || $WUAUTH === '4') {
                //e_log('権限の情報取得:'.print_r($usrinfo,true));
                $showFlg = false;
                switch ($navData[NAVTEXT]) {
                    case 'FRM00008':
                    case 'FRM00012':
                    case 'FRM00017':
                    case 'FRM00048':
                    case 'FRM00064':
                    case 'FRM00071':
					case 'FRM00137':
                        $showFlg = true;
                    break;
                    default:
                    break;
                }
            }
            if ($showFlg === true) {
                $rtn[$groupIndex]['children'][$masterIndex] = array('text' => $navData['ITMTEXT'], 'leaf' => $navData['NAVLEAF'], 'hrefTarget' => $navData['NAVHREF'], 'menu' => $navData['NAVMENU'], 'id' => 'NAV' . $navData['NAVID']);
                $masterIndex++;
            }
        }
    }
}
echo (json_encode($rtn));