<?php
class Pivot {
    private $con;
    private $d1name;
    private $database;
    private $table;
    private $dataq;
    private $groupdata;
    private $orderdata;
    private $rowstart;
    private $rowend;
    private $columndata;
    private $calcdata;
    private $data_studio = array();
    private $columnupdata;
    private $getcheadingcol;
    private $colslists;
    private $countcalc;
    private $headingcoluline;
    private $searchcolumns;
    private $pgstart;
    private $pgend;
    private $totalcount;
    private $num_count;
    private $endcount;
    private $endpointnum;
    private $get_cheadingcol;
    /***/
    private $casewhen;
    private $shukeiselectColist;
    private $sortColData;
    private $sortCol;
    private $mainselect = '';
    private $getselect = '';
    /**全体カウント**/
    private $zentaiCount;
    /***横軸**/
    private $distinctGrouping; //get flag of total sum 
    private $groupingChk; // where condition to check column displayed
    
    /***縦軸**/
    private $Grouping; //get flag of total sum 
    private $gpChk; // where condition to check column displayed
    private $gpCol;
    private $shukeiGetColis;
    private $sortColUpData;
    private $shukeiYdisplayFlg;
    private $shukeiXdisplayFlg;
    private $xchecked;
    private $ychecked;
    /***/
    private $xcolselect;
    /***/
    
    //     inner subquery
    /***/
    private $innersql;
    private $innerupsql;
    /***/
    
    private $dataqcolumn;
    private $resucol; //get shikicolumn
    private $searchdata = array();
    private $wheresearch;
    private $columnY = array();
    private $columnX = array();
    private $columnC = array();
    private $num_formula = array();
    private $rtncolumn = array();
    private $whereparam = array(); //パラメータマーカー用の配列
    private $whereOnlyParam = array(); //where句のみのパラメータマーカー用配列
    
    public function Pivot($con, $database, $table, $d1name) {
        $this->con      = $con;
        $this->database = $database;
        $this->table    = $table;
        $this->d1name   = $d1name;
    }
    
    
    public function set_xcolSelect($xcolselect) {
        $this->xcolselect = $xcolselect;
    }
    public function get_xcolSelect() {
        return $this->xcolselect;
    }
    /**全体カウント**/
    public function set_zentaiCount($zentaiCount) {
        $this->zentaiCount = $zentaiCount;
    }
    public function get_zentaiCount() {
        return $this->zentaiCount;
    }    

    /**縦INNERカラム**/
    public function set_innerSql($innersql) {
        $this->innersql = $innersql;
    }
    public function get_innerSql() {
        return $this->innersql;
    }
    
    /**横INNERカラム**/
    public function set_innerUpSql($innerupsql) {
        $this->innerupsql = $innerupsql;
    }
    public function get_innerUpSql() {
        return $this->innerupsql;
    }
    
    public function set_distinctGrouping($distinctGrouping) {
        $this->distinctGrouping = $distinctGrouping;
    }
    public function get_distinctGrouping() {
        return $this->distinctGrouping;
    }
    
    public function set_groupingChk($groupingChk) {
        $this->groupingChk = $groupingChk;
    }
    public function get_groupingChk() {
        return $this->groupingChk;
    }
    public function set_Grouping($Grouping) {
        $this->Grouping = $Grouping;
    }
    public function get_Grouping() {
        return $this->Grouping;
    }
    
    public function set_gpChk($gpChk) {
        $this->gpChk = $gpChk;
    }
    public function get_gpChk() {
        return $this->gpChk;
    }
    
    public function set_gpCol($gpCol) {
        $this->gpCol = $gpCol;
    }
    public function get_gpCol() {
        return $this->gpCol;
    }
    
    public function set_columnY($columnY) {
        $this->columnY = $columnY;
    }
    public function get_columnY() {
        return $this->columnY;
    }
    public function set_columnX($columnX) {
        $this->columnX = $columnX;
    }
    public function get_columnX() {
        return $this->columnX;
    }
    
    public function set_columnC($columnC) {
        $this->columnC = $columnC;
    }
    public function get_columnC() {
        return $this->columnC;
    }
    
    public function set_num_formula($num_formula) {
        $this->num_formula = $num_formula;
    }
    public function get_num_formula() {
        return $this->num_formula;
    }
    
    public function set_dataq($dataq) {
        $this->dataq = $dataq;
    }
    public function get_dataq() {
        return $this->dataq;
    }
    
    public function set_searchdata($searchdata) {
        $this->searchdata[] = $searchdata;
    }
    public function get_searchdata() {
        return $this->searchdata;
    }
    
    public function set_rtncolumn($rtncolumn) {
        $this->rtncolumn[] = $rtncolumn;
    }
    public function get_rtncolumn() {
        $newrtncolumn = array();
        foreach ($this->rtncolumn as $value) {
            $newrtncolumn[] = array(
                array(
                    "COLUMN_HEADING" => cmHsc($value)
                )
            );
        }
        return $newrtncolumn;
    }
    public function set_dataqcolumn($dataqcolumn) {
        $this->dataqcolumn = $dataqcolumn;
    }
    public function get_dataqcolumn() {
        return $this->dataqcolumn;
    }
    public function set_casewhen($casewhen) {
        $this->casewhen = $casewhen;
    }
    public function get_casewhen() {
        return $this->casewhen;
    }
    public function set_resucol($resucol) {
        $this->resucol = $resucol;
    }
    public function get_resucol() {
        return $this->resucol;
    }
    public function set_whereparam($whereparam) {
        $this->whereparam[] = $whereparam;
    }
    public function get_whereparam() {
        return $this->whereparam;
    }
    public function set_whereOnlyParam($whereparam) {
        $this->whereOnlyParam[] = $whereparam;
    }
    public function get_whereOnlyParam() {
        return $this->whereOnlyParam;
    }
    public function set_wheresearch($wheresearch) {
        $this->wheresearch = $wheresearch;
    }
    public function get_wheresearch() {
        return $this->wheresearch;
    }
    
    public function set_Groupby($groupArr, $groupdata) {
        foreach ($groupArr as $key => $value) {
            if ($groupArr[$key]['HIDE'] !== '1') {
                if ($groupdata !== '') {
                    $groupdata .= ',' . $key;
                } else {
                    $groupdata .= $key;
                }
            }
        }
        $this->groupdata = $groupdata;
        
        return $this->groupdata;
    }
    
    public function get_Groupby() {
        return $this->groupdata;
    }
    
    public function set_Orderby($orderArr, $orderdata) {
        foreach ($orderArr as $key => $value) {
            if ($orderArr[$key]['HIDE'] !== '1') {
                if ($orderdata !== '') {
                    $orderdata .= ',' . $key;
                } else {
                    $orderdata .= 'order by ' . $key;
                }
            }
        }
        $this->orderdata = $orderdata;
        
        return $this->orderdata;
    }
    
    public function get_Orderby() {
        return $this->orderdata;
    }
    
    //配列の重複データを削除
    public function unique_trimArr($dataArray) {
        $datagetArr = array();
        $dataArr    = array();
        $datagetArr = array_unique($dataArray); //get unique column value
        $dataArr    = array_values(array_filter(array_map('trim', $datagetArr), 'strlen')); //array filter is to remove white space value and array_values is to start from zero index from arrays
        return $dataArr;
    }
    
    //縦軸配列、横軸配列、集計配列の重複データを削除
    public function unique_trimArrAll() {
        $this->columnY = $this->unique_trimArr($this->columnY);
        $this->columnX = $this->unique_trimArr($this->columnX);
        $this->columnC = $this->unique_trimArr($this->columnC);
    }
    
    //カラムスタート位置計算
    public function set_rowstart($flg, $rowstart, $rowend, $endpointnum) {
        if ($flg === '0' || $flg === '2') {
            $rowstart = 1;
        } else if ($flg === '9' || $flg === '3') { //for next btn and merge
            $rowendcount = (int) $rowend;
            $rowstart    = $rowendcount + 1;
        } else if ($flg === '8') { //for previous btn
            $rowstart = ((int) $rowstart) - ($endpointnum);
        } else if ($flg === '1') { //for search btn
            $rowstart = 1;
        } else if ($flg === '6') { //for reload
            $rowstart = $rowstart;
        } else {
            $rowstart = $rowstart;
        }
        
        $this->rowstart = $rowstart;
        
        return $this->rowstart;
    }
    
    //カラムスタート位置取得
    public function get_rowstart() {
        return $this->rowstart;
    }
    
    //カラム終了位置計算
    public function set_rowend($flg, $endcount, $rowstart, $rowend, $endpointnum) {
        if ($flg === '0' || $flg === '1' || $flg === '2') {
            $rowend = $endpointnum;
            if ($rowend > $endcount) {
                $rowend = $endcount;
            }
        } else if ($flg === '9' || $flg === '3') { //for next btn
            $rowendcount = (int) $rowend;
            //$rowend = $rowendcount*2;
            $rowend      = $rowendcount + $endpointnum;
            if ($rowend > $endcount) {
                $rowend = $endcount;
            }
        } else if ($flg === '8') { //for previous btn
            $rowend = (int) $rowstart - 1;
        } else if ($flg === '6') { //for reload
            $rowend = $rowend;
        } else if ($flg === '5' && $rowend === '') {
            $rowend = $endpointnum;
            if ($rowend > $endcount) {
                $rowend = $endcount;
            }
        }
        $this->rowend = $rowend;
        return $this->rowend;
    }
    
    //カラム終了位置取得
    public function get_rowend() {
        return $this->rowend;
    }
    
    public function sumcase_query($rtncolumn, $colslists, $calcdata, $num_function, $getcheading, $flg) {

        $ychecked = $this->ychecked;
        foreach ($rtncolumn as $value) {
            $this->set_rtncolumn($value[0]['COLUMN_HEADING']);
        }
        $colscount = ((int) $this->get_rowstart()) - 1;
        $dataq     = '';
        $casewhen  = '';
        if (count($this->get_columndata()) > 0) {
            $dataq .= ' , ';
            $casewhen .= ' , ';
        }
        //mmm修正
        //$dataq .= ' MAX(RNORDER) AS RNORDER ';
        $dataq .= ' MAX(XROWNUM) AS RNORDER ';
        $casewhen .= '  RNORDER ';
        $allcount    = 0;
        $dataqcolumn = '';
        foreach ($colslists as $ck => $cv) {
            foreach ($cv as $cvk => $cvv) {
                if (strpos(substr($cvk, -3), '_GP') === false) {
                    $cols[$ck][$cvk] = $cvv;
                }
            }
        }
        foreach ($cols as $key => $row) {
            $colscount++;
            $allcount++;
            $calccount = 0;
            /*sumqueryfunction*/
            foreach ($calcdata as $calckey => $calcvalue) {
                $calccount++;
                $sumdata  = '';
                $rowcount = 0;
                $run      = true;
                foreach ($row as $k => $v) {
                    //    if (strpos(substr($k, -2), 'GP') === false && strpos(substr($k, -1), 'S') === false) {
                    $qw = true;
                    if (($v == '' || is_null($v)) && (int)$colslists[$key][$k . '_GP'] === 1) {
                        $qw = false;
                        break;
                    }
                    if ($qw === true) {
                        $rowcount++;
                        if ($sumdata !== "") {
                            $sumdata .= ' and ';
                        }
                        if ($ychecked[$k]['TYPE'] === 'S' || $ychecked[$k]['TYPE'] === 'P' || $ychecked[$k]['TYPE'] === 'B') {
                            if ($v != null && $v != '') {
                                $sumdata .= $k . ' = ? ';
                                $this->set_whereparam($v);
                            } else {
                                $sumdata .= $k . ' IS NULL ';
                            }
                        } else {
                            $sumdata .= $k . ' =? ';
                            $this->set_whereparam($v);
                        }
                        $cscount = '';
                        if (count($colslists) - 1 === $key && $val === '') {
                            $run = false;
                        }
                    }
                    //  }
                }
                if ($sumdata !== '') {
                    $dataq .= ',sum(case when ' . $sumdata . ' then ' . $calcvalue . ' end) as C' . $colscount . $calcvalue;
                    $data_ = "char(X.C" . $colscount . $calcvalue . ")";
                    $casewhen .= ",case when " . $data_ . " IS NULL THEN '_' ELSE " . $data_ . " END AS C" . $colscount . "" . $calcvalue;
                    $this->mainselect .= " , C" . $colscount . $calcvalue;
                    $dataqcolumn .= ',C' . $colscount . '' . $calcvalue;
                } else {
                    $dataq .= ',sum(' . $calcvalue . ') as C' . $colscount . $calcvalue;
                    $data_ = "char(X.C" . $colscount . $calcvalue . ")";
                    $casewhen .= ",case when " . $data_ . " IS NULL THEN '_' ELSE " . $data_ . " END AS C" . $colscount . $calcvalue;
                    $this->mainselect .= " , C" . $colscount . $calcvalue;
                    $dataqcolumn .= ',C' . $colscount . $calcvalue;
                }
            }
            //          if($run === true){
            $SHIKI = '式';
            foreach ($getcheading as $key => $value) {
                $this->set_rtncolumn($value);
            }
            if (count($num_function) > 0) {
                $c = 0;
                foreach ($num_function as $key => $value) {
                    $SHIKI = substr($value, 0, strpos($value, ':'));
                    if ($SHIKI === '') {
                        $SHIKI = '式' . ($key + 1);
                    }
                    $this->set_rtncolumn($SHIKI);
                }
            }
            //}
        }
        $ecount    = $this->get_endcount();
        $rend      = $this->get_rowend();
        $lastTotal = true;
        if ($flg === '3' || $flg === '2') {
            if ($ecount !== $rend) {
                $lastTotal = false;
            }
        }
        
        if ($lastTotal === true) {
            /**横軸の合計フラグ**/
            $colup_data = $this->get_columnX();
            $ychecked   = $this->ychecked;
            $SUM        = '1';
            foreach ($ychecked as $ky => $va) {
                if ($ychecked[$ky]['HIDE'] !== '1') {
                    $SUM = $ychecked[$ky]['SUMG'];
                    break;
                }
            }
            //                     if($ychecked[$colup_data[0]]['SUMG'] === '' ){
            if ($SUM !== '1') {
                $colscount++;
                foreach ($calcdata as $calckey => $calcvalue) {
                    $dataq .= ' , ';
                    $dataqcolumn .= ' , ';
                    $dataq .= 'sum(' . $calcvalue . ') as C' . $colscount . '' . $calcvalue;
                    $data_ = "char(X.C" . $colscount . $calcvalue . ")";
                    $casewhen .= ",case when " . $data_ . " IS NULL THEN '_' ELSE " . $data_ . " END AS C" . $colscount . "" . $calcvalue;
                    $this->mainselect .= " , C" . $colscount . $calcvalue;
                    $dataqcolumn .= 'C' . $colscount . '' . $calcvalue;
                }
                $SHIKI = '式';
                foreach ($getcheading as $key => $value) {
                    $this->set_rtncolumn($value);
                }
                if (count($num_function) > 0) {
                    $c = 0;
                    foreach ($num_function as $key => $value) {
                        $SHIKI = substr($value, 0, strpos($value, ':'));
                        if ($SHIKI === '') {
                            $SHIKI = '式' . ($key + 1);
                        }
                        $this->set_rtncolumn($SHIKI);
                    }
                }
            }
        }
        $this->set_dataqcolumn($dataqcolumn);
        $casewhen .= $this->get_gpCol();
        $this->set_casewhen($casewhen);
        $this->set_dataq($dataq);
    }
    
    public function get_wheresearch_execute($chkcoldata, $wheresearch, $search_col_arr, $operator, $searchdata) {
        $val         = 0;
        $wheresearch = '';
        
        if (count($chkcoldata) > 0) {
            
            //$wheresearch .= 'where ';
            
            foreach ($chkcoldata as $i) {
                if ($val !== 0) {
                    if (isset($operator['FLG'])) {
                        $wheresearch .= $operator['FLG'];
                    } else {
                        $wheresearch .= ' and ';
                    }
                }
                $operate = '';
                if ($operator[$i] === '0' || $operator[$i] === '1' || $operator[$i] === '2') {
                    $operate .= 'LIKE  ? ';
                    if ($operator[$i] === '0') {
                        $this->set_whereparam($searchdata[$i] . '%');
                        $this->set_whereOnlyParam($searchdata[$i] . '%');
                        $wheresearch .= $search_col_arr[$i] . ' ' . $operate;
                    } else if ($operator[$i] === '1') {
                        $this->set_whereparam('%' . $searchdata[$i]);
                        $this->set_whereOnlyParam('%' . $searchdata[$i]);
                        $wheresearch .= ' rtrim( ' . $search_col_arr[$i] . ' ) ' . $operate;
                    } else {
                        $this->set_whereparam('%' . $searchdata[$i] . '%');
                        $this->set_whereOnlyParam('%' . $searchdata[$i] . '%');
                        $wheresearch .= $search_col_arr[$i] . ' ' . $operate;
                    }
                } else {
                    $wheresearch .= $search_col_arr[$i] . ' ' . $operator[$i] . ' ? ';
                    $this->set_whereparam($searchdata[$i]);
                    $this->set_whereOnlyParam($searchdata[$i]);
                }
                $val++;
            }
            
            if ($wheresearch !== '') {
                $wheresearch = 'where ' . $wheresearch;
            }
        }
        
        $this->set_wheresearch($wheresearch);
    }
    
    public function get_Column($colsname = '', $flg = false) {
        
        /************
        
        カラム名をQSYSからではなくFDB2CSV2から取得する為、修正
        取得するカラム名はリネームで他のソースに変更が起きないようにする。
        
        *************/
        
        /*
        $sql = "select column_name,column_heading,numeric_scale from qsys2.syscolumns where table_name = ? and table_schema = ? ";
        
        $param =array($this->table,$this->database);
        if($type !== ''){
        $sql .= 'and DATA_TYPE=?';
        array_push($param,$type);
        }
        if($colsname !== ''){
        $sql .= 'and column_name=?';
        array_push($param,$colsname);
        }
        $column_list_char = $this->dbFetchAll($sql,$param);
        
        
        if($flg !== false){
        foreach($column_list_char as $value){
        if($flg !== '3'){
        $this->set_rtncolumn($value['COLUMN_HEADING']);
        }
        }
        }
        return $column_list_char;
        */
        
        //$sql = "select column_name,column_heading,numeric_scale from qsys2.syscolumns where table_name = ? and table_schema = ? ";
        $sql = 'select A.d2hed as column_heading from ';
        $sql .= ' ( select d2name,d2filid,d2fld,d2hed from ' . MAINLIB.'/FDB2CSV2 ';
        $sql .= ' UNION ';
        $sql .= ' select d5name as d2name,9999 as d2filid,d5fld as d2fld,d5hed as d2hed from ' . MAINLIB.'/FDB2CSV5 ';
        $sql .= ' ) as A ';
        $sql .= ' where A.d2name = ? ';
        
        
        
        $param = array(
            $this->d1name
        );
        
        /*
        if($type !== ''){
        $sql .= 'and DATA_TYPE=?';
        array_push($param,$type);
        }
        */
        
        if ($colsname !== '') {
            $field = $this->splitField($colsname);
            $sql .= 'and A.d2filid = ? and A.d2fld = ? ';
            array_push($param, $field['FILID']);
            array_push($param, $field['FLD']);
        }
        //RDB対応でMMM修正
        /****開始***/
        //$column_list_char = $this->dbFetchAll($sql, $param);
        $column_list_char = $this->dbFetchAllFROMMAINLIBL($sql, $param);
        /****終了****/
        if ($flg !== false) {
            foreach ($column_list_char as $value) {
                if ($flg !== '3') {
                    $this->set_rtncolumn($value['COLUMN_HEADING']);
                }
            }
        }
        return $column_list_char;
        
    }
    
    /*
     *-------------------------------------------------------* 
     * カラム名をFILIDとFLDに分割してリターン
     * 
     * RESULT
     *    01：array [FILID] [FLD]
     * 
     * @param String  $colname カラム名
     *-------------------------------------------------------*
     */
    public function splitField($colname) {
        
        $rtnAry = array(
            'FILID' => '',
            'FLD' => ''
        );
        
        $tmp = explode('_', $colname);
        
        $filid = str_replace('_', '', $tmp[count($tmp) - 1]);
        
        $filidlen = strlen($filid);
        
        $fld = substr($colname, 0, strlen($colname) - $filidlen - 1);
        
        $rtnAry['FILID'] = $filid;
        $rtnAry['FLD']   = $fld;
        
        return $rtnAry;
        
    }
    
    /**TODO
    エラーSQLを許容してる為
    db2_prepareとdb2_executeに対して@を付ける
    **/
    public function dbFetchAll($sql, $param) {
        //e_log('実行SQL：'.$sql.print_r($param,true));
        $data = array();
        $rs   = '';
        //e_log(print_r($sql,true));
        $stmt = db2_prepare($this->con, $sql);
        if ($stmt) {
            $exe = db2_execute($stmt, $param);
            if ($exe === true) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                $rs = $data;
            } else {
                error_log("sql" . $sql);
                error_log("param" . print_R($param, true));
                error_log("statement fail " . db2_stmt_errormsg());
                $rs = false;
            }
        } else {
            error_log("sql" . $sql);
            error_log("param" . print_R($param, true));
            error_log("statement fail " . db2_stmt_errormsg());
            $rs = false;
        }
        return $rs;
    }
    //RDB対応でMMM修正
    /****開始***/
    /**TODO
    エラーSQLを許容してる為
    db2_prepareとMAINLIBLに対してのdb2_executeに対して@を付ける
    **/
    public function dbFetchAllFROMMAINLIBL($sql, $param) {
        $data = array();
        $rs   = '';
        $user     = RDB_USER;
        $password = RDB_PASSWORD;
        $database = RDB;
        $option   = array(
            'autocommit' => DB2_AUTOCOMMIT_ON,
            'i5_naming'=>DB2_I5_NAMING_ON,
            'i5_dbcs_alloc' => DB2_I5_DBCS_ALLOC_ON
        );
        
        $db2con = db2_connect($database, $user, $password, $option);
        
        db2_exec($db2con, 'SET SCHEMA ' . MAINLIB);

        $stmt = db2_prepare($db2con, $sql);
        if ($stmt) {
            $exe = db2_execute($stmt, $param);
            if ($exe === true) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                $rs = $data;
            } else {
                error_log("sql" . $sql);
                error_log("param" . print_R($param, true));
                error_log("statement fail " . db2_stmt_errormsg());
                $rs = false;
            }
        } else {
            error_log("sql" . $sql);
            error_log("param" . print_R($param, true));
            error_log("statement fail " . db2_stmt_errormsg());
            $rs = false;
        }
        db2_close($db2con);
        return $rs;
    }
    /****終了****/
    
    //SELECT句作成
    public function get_selectcol($countcolumn, $countcalc, $countcolumnup, $flg) {
        //e_log('LOGMMM: get_selectcol'.print_r($countcolumn,true).print_r($this->get_columnY(),true));
        $selectcol = ''; //SELECT句
        $gpChk     = '';
        $gpCol     = '';
        $incre     = 0; //インクリメント変数
        $Grouping  = '';
        //縦軸配列をループ
        foreach ($this->get_columnY() as $key => $value) {
            if ($this->xchecked[$value]['HIDE'] !== '1') {
                $incre += 1;
                if ($selectcol !== '') {
                    $selectcol .= ',';
                }
                $selectcol .= $value;
                $Grouping .= " ,  GROUPING(" . $value . ") AS " . $value . "GP ";
                $gpCol .= " , " . $value . "GP ";
                //引数3目にtrueを渡して$thisのrtncolumnにヘディングを格納
                $this->get_Column($value, $flg);
                //  if(($selectcol !== '') && ($incre < $countcolumn) || ($countcalc > 0 && $countcolumnup ==0)){
                // }
                if ($this->xchecked[$value]['SUMG'] === '1') {
                    if ($gpChk !== '') {
                        $gpChk .= ' AND ';
                    }
                    $chk = '';
                    foreach ($this->get_columnY() as $k => $v) {
                        if ($this->xchecked[$v]['HIDE'] !== '1') {
                            if ($chk !== '') {
                                $chk .= ' OR ';
                            } else {
                                $chk .= ' ( ';
                            }
                            if ($value !== $v) {
                                $chk .= $v . 'GP <> 0 ';
                            } else {
                                break;
                            }
                        }
                    }
                    $chk .= $value . 'GP <> 1 )';
                    
                    $gpChk .= $chk;
                }
            }
        }
        $this->set_gpCol($gpCol);
        $this->set_gpChk($gpChk);
        $this->set_Grouping($Grouping);
        return $selectcol;
    }
    
    
    function get_selectcol2($select, $countcolumnup, $countcalc) {
        
        $selectcol = ''; //SELECT句
        $incre     = 0; //インクリメント変数
        $casewhen  = $select;
        //集計配列をループ
        foreach ($this->get_columnC() as $key => $value) {
            $incre += 1;
            $getcheadingarr = $this->get_Column($value);
            $getcheading[]  = $getcheadingarr[0]['COLUMN_HEADING'];
            
            //横軸がないときのみSELECT句にSUMを追加
            if ($countcolumnup == 0) {
                $this->get_Column($value, true);
                $selectcol .= ' , sum(' . $value . ') as C1' . $value . ' ';
                $data_ = "char(C1" . $value . ")";
                $casewhen .= ", case when " . $data_ . " IS NULL THEN '_' ELSE " . $data_ . " END AS C1" . $value;
                $this->mainselect .= ', C1' . $value;
            }
        }
        if ($countcolumnup == 0) {
            $casewhen .= $this->get_gpCol();
            $this->set_casewhen($casewhen . ' , RNORDER ');
        }
        return array(
            $selectcol,
            $getcheading
        );
        
    }
    
    //計算式の$1$などをカラム名に変換
    public function numstr_array($num_formula, $t) {
        for ($i = 0; $i < count($num_formula); $i++) {
            $str          = $num_formula[$i];
            $str          = str_replace(array_keys($t), array_values($t), $str);
            $numstr_arr[] = $str;
        }
        return $numstr_arr;
    }
    
    public function distinct_columns() {
        $distinct         = '';
        $distinctGrouping = '';
        $count            = 0;
        $groupingChk      = '';
        $getcheadingcol   = array();
        foreach ($this->get_columnX() as $key => $value) {
            if ($this->ychecked[$value]['HIDE'] !== '1') {
                //                        $count++;
                $getcheadingcolarr = $this->get_Column($value);
                $getcheadingcolarr = $this->umEx($getcheadingcolarr, true);
                $getcheadingcol[]  = $getcheadingcolarr[0]['COLUMN_HEADING'];
                if ($distinct !== '') {
                    $distinct .= ',';
                }
                $distinct .= $value;
                $distinctGrouping .= ", GROUPING(" . $value . ") AS " . $value . 'GP ';
                /*    if($count !== count($this->get_columnX())){
                $distinct .= ',';
                }*/
                if ($this->ychecked[$value]['SUMG'] === '1') {
                    if ($groupingChk !== '') {
                        $groupingChk .= ' AND ';
                    }
                    $chk = '';
                    foreach ($this->get_columnX() as $k => $v) {
                        if ($this->ychecked[$v]['HIDE'] !== '1') {
                            if ($chk !== '') {
                                $chk .= ' OR ';
                            } else {
                                $chk .= ' ( ';
                            }
                            if ($value !== $v) {
                                $chk .= $v . 'GP <> 0 ';
                            } else {
                                break;
                            }
                        }
                    }
                    $chk .= $value . 'GP <> 1 )';
                    $groupingChk .= $chk;
                }
            }
        }
        //mmm修正
        //$this->set_distinctGrouping($distinctGrouping . ', MAX(RNORDER) AS RNORDER');
        $this->set_distinctGrouping($distinctGrouping . ', MAX(YROWNUM) AS RNORDER');
        $this->set_groupingChk($groupingChk);
        return array(
            $distinct,
            $getcheadingcol
        );
    }
    
    public function shiki_cols($colslists, $num_function, $selectcol, $flg) {
        $main       = $this->get_xcolSelect();
        $SHIKI      = '';
        $resu       = '';
        $resucol    = '';
        //  $st_charnull  ='';
        $sumgroupby = '';
        //    $count_d = 0;
        $count_d    = ((int) $this->get_rowstart()) - 1;
        $cols       = array();
        foreach ($colslists as $ck => $cv) {
            foreach ($cv as $cvk => $cvv) {
                if (strpos(substr($cvk, -3), '_GP') === false) {
                    $cols[$ck][$cvk] = $cvv;
                }
            }
        }
        foreach ($cols as $k => $row) {
            $count_d++;
            foreach ($num_function as $key => $value) {
                $SHIKI = substr($value, 0, strpos($value, ':'));
                if ($SHIKI !== '') {
                    $value = str_replace($SHIKI . ":", "", $value);
                }
                $cscount  = "";
                $cols     = '';
                $Acols    = '';
                $charnull = '';
                foreach ($this->get_columnC() as $calcd) {
                    $value = str_replace($calcd, 'C' . $count_d . $calcd, $value);
                    if ($key === 0) {
                        $cols .= (($cols !== '') ? ',' : '') . 'C' . $count_d . $calcd;
                        $Acols .= (($Acols !== '') ? ',' : '') . ' CHAR(C' . $count_d . $calcd . ') AS C' . $count_d . $calcd;
                        $charnull .= (($charnull !== '') ? ',' : '') . "CASE WHEN C" . $count_d . $calcd . " IS NULL THEN '_' ELSE C" . $count_d . $calcd . " END AS C" . $count_d . $calcd;
                        $main .= ' , C' . $count_d . $calcd;
                    }
                }
                $col_sum = ' CHAR(' . $value . ') AS  RES' . $count_d . '_' . $key; // as RESULT'.$count_d;
                $resu .= (($resu !== '') ? ',' : '') . $Acols . (($Acols !== '') ? ',' : '') . $col_sum;
                //    $resucol .= (($resu !== '')?',':'').$cols.(($cols !== '')?',':'').'RES'.$count_d.'_'.$key;
                //****/
                $resucol .= (($resucol !== '') ? ',' : '') . $charnull . (($charnull !== '') ? ',' : '') . "CASE WHEN RES" . $count_d . "_" . $key . " IS NULL THEN '_' ELSE RES" . $count_d . "_" . $key . " END AS RES" . $count_d . "_" . $key;
                $main .= ',RES' . $count_d . '_' . $key;
                //****/
                $sumgroupby .= (($sumgroupby !== '' && $cols !== '') ? (',' . $cols) : $cols);
            }
        }
        $ecount    = $this->get_endcount();
        $rend      = $this->get_rowend();
        $lastTotal = true;
        if ($flg === '3' || $flg === '2') {
            if ($ecount !== $rend) {
                $lastTotal = false;
            }
        }
        if ($lastTotal === true) {
            $colup_data = $this->get_columnX();
            $ychecked   = $this->ychecked;
            $SUM        = '1';
            foreach ($ychecked as $ky => $va) {
                if ($ychecked[$ky]['HIDE'] !== '1') {
                    $SUM = $ychecked[$ky]['SUMG'];
                    break;
                }
            }
            //             if($ychecked[$colup_data[0]]['SUMG'] === '' ){
            if ($SUM !== '1') {
                /***合計カラム***/
                $count_d++;
                foreach ($num_function as $key => $value) {
                    $SHIKI = substr($value, 0, strpos($value, ':'));
                    if ($SHIKI !== '') {
                        $value = str_replace($SHIKI . ":", "", $value);
                    }
                    $cols     = '';
                    $Acols    = '';
                    $charnull = '';
                    foreach ($this->get_columnC() as $calcd) {
                        $value = str_replace($calcd, 'C' . $count_d . $calcd, $value);
                        if ($key === 0) {
                            $cols .= (($cols !== '') ? ',' : '') . 'C' . $count_d . $calcd;
                            $Acols .= (($Acols !== '') ? ',' : '') . ' CHAR(C' . $count_d . $calcd . ') AS C' . $count_d . $calcd;
                            $charnull .= (($charnull !== '') ? ',' : '') . "CASE WHEN C" . $count_d . $calcd . " IS NULL THEN '_' ELSE C" . $count_d . $calcd . " END AS C" . $count_d . $calcd;
                            $main .= ' , C' . $count_d . $calcd;
                        }
                    }
                    $col_sum = ' CHAR(' . $value . ') AS  RES' . $count_d . '_' . $key; // as RESULT'.$count_d;
                    $resu .= (($resu !== '') ? ',' : '') . $Acols . (($Acols !== '') ? ',' : '') . $col_sum;
                    $resucol .= ' , ' . $charnull . (($charnull !== '') ? ',' : '') . "CASE WHEN RES" . $count_d . "_" . $key . " IS NULL THEN '_' ELSE RES" . $count_d . "_" . $key . " END AS RES" . $count_d . "_" . $key;
                    $main .= " ,RES" . $count_d . "_" . $key;
                    $sumgroupby .= (($sumgroupby !== '' && $cols !== '') ? (',' . $cols) : $cols);
                }
            }
        }
        if ($selectcol !== '') {
            $resu = ',' . $resu;
        }
        $resu .= $this->get_gpCol();
        $sumgroupby .= $this->get_gpCol();
        $resucol .= $this->get_gpCol();
        $this->set_resucol($resucol);
        $this->mainselect = $main;
        return array(
            $resu,
            $sumgroupby
        );
    }
    public function shiki_cols2($num_function) {
        $c          = 0;
        $sum_numfun = '';
        $casewhen   = $this->get_casewhen();
        foreach ($num_function as $key => $value) {
            $SHIKI = substr($value, 0, strpos($value, ':'));
            if ($SHIKI !== '') {
                $value = str_replace($SHIKI . ":", "", $value);
            } else {
                $SHIKI = '式' . ($key + 1);
            }
            $c++;
            $data_ = "RESULT" . $c;
            $casewhen .= ", case when " . $data_ . " IS NULL THEN '_' ELSE " . $data_ . " END AS RESULT" . $c;
            $sum_numfun .= ",CHAR(" . $value . ") as RESULT" . $c;
            //$this->set_mainSelect(" , RESULT".$c);
            $this->mainselect .= " , RESULT" . $c;
            $this->set_rtncolumn($SHIKI);
        }
        $this->set_casewhen($casewhen);
        return $sum_numfun;
    }
    public function end_pointnum($countcolumn, $countcalc, $num_count) {
        $endpointnum = (99 - $countcolumn) / ($countcalc + $num_count);
        if (is_float($endpointnum)) {
            $endpointnum = floor($endpointnum);
        }
        return $endpointnum;
    }
    
    public function end_count($sch, $distinct, $tablename_studio, $distinctgroup) {
        $groupingChk = ($this->get_groupingChk() !== '') ? ' WHERE ' . $this->get_groupingChk() : '';
        $sqlcount    = '';
        $sqlcount .= 'select count(*) as COUNT from ';
        $sqlcount .= '(select ' . $distinct . ' from ';
        $sqlcount .= '(select rownumber() over() as rownum,A.* from (select DISTINCT  ' . $distinct . $this->get_distinctGrouping() . ' from ';
        //           $sqlcount .=$this->database.'.'.$tablename_studio;
        $sqlcount .= $this->get_innerUpSql();
        $sqlcount .= ' ' . (($sch !== false) ? $this->get_wheresearch() : '') . '' . $distinctgroup . ' ) as A ' . $groupingChk . ') as B )as D';
        $param   = $this->get_whereparam();
        //error_log('LOGMMM: end_count SQL1:'.$sqlcount.'parameter:'.print_r($param,true));
        $sqcount = $this->dbFetchAll($sqlcount, $param);
        return $sqcount;
    }
    public function get_colists($sch, $distinct, $tablename_studio, $distinctgroup, $distinctorder, $rowstart, $rowend) {
        $groupingChk = ($this->get_groupingChk() !== '') ? ' WHERE ' . $this->get_groupingChk() : '';
        $sql         = '';
        $firCol      = array();
        $col         = '';
        $colArr      = $this->get_columnX();
        foreach ($this->ychecked as $key => $value) {
            if ($this->ychecked[$key]['HIDE'] !== '1') {
                $firCol = $this->ychecked[$key];
                $col    = $key;
                break;
            }
        }
        $total = '';
   /*     if (count($firCol) !== 0) {
            if ($firCol['SUMG'] === '') {
                $total = $col . ' IS NOT NULL AND  ' . $col . 'GP <> 1 AND ';
            }
        }*/
        if (count($firCol) !== 0) {
            if ($firCol['SUMG'] === '') {
                $total = $col . 'GP <> 1 AND ';
            }
        }

        if ($sch !== false) {
            $sql = 'select ' . $this->getselect . ' from (select rownumber() over(' . $this->sortColUpData . ') as rownum,A.* from (select DISTINCT  ' . $distinct . $this->get_distinctGrouping() . $this->shukeiGetColist . ' from ';
            $sql .= $this->get_innerUpSql();
            $sql .= ' ' . $this->get_wheresearch() . ' ' . $distinctgroup . ' ' . $distinctorder . ') as A ' . $groupingChk . ') as B where ';
            $sql .= $total;
            $sql .= 'B.rownum between ' . $rowstart . ' and ' . $rowend;
        } else {
            $sql = 'select ' . $this->getselect . ' from (select rownumber() over(' . $this->sortColUpData . ') as rownum,A.* from (select DISTINCT  ' . $distinct . $this->get_distinctGrouping() . $this->shukeiGetColist . ' from ';
            $sql .= $this->get_innerUpSql();
            $sql .= ' ' . $distinctgroup . ' ' . $distinctorder . ') as A ' . $groupingChk . ') as B where ';
            $sql .= $total;
            $sql .= ' B.rownum between ' . $rowstart . ' and ' . $rowend;
        }
        $param   = array();
        //error_log('LOGMMM: get_colists SQL1:'.$sql.'parameter:'.print_r( $this->get_whereparam(),true));
        $colists = $this->dbFetchAll($sql, $this->get_whereparam());
        /*          $colistsEdit = umEx($colists);
        $lastRow = $colistsEdit[count($colistsEdit)-1];
        $count = 0;
        error_log($sql);
        foreach($lastRow as $key => $value){
        if($value === '' ){
        $count += 1;
        }
        }
        if(count((array)$lastRow) == $count){
        unset($colistsEdit[count($colistsEdit)-1]);
        }
        $colists = $colistsEdit;*/
        return $colists;
    }
    
    public function get_result($flg, $orderdata, $sumsql, $selectcol, $tablename_studio, $groupdata, $sumgroupby, $pgstart, $pgend, $sortColData,$lastFlg) {
        
        $gpChk       = ($this->get_gpChk() !== '') ? ' WHERE ' . $this->get_gpChk() : '';
        $gc          = 0;
        $sqldata     = '';
        $sqlcount    = '';
        $getcol_list = '';
        if ($sumsql !== '') {
            if ($flg !== '3') {
                $getcol_list = (($selectcol !== '') ? $selectcol . ',' : '') . $this->get_resucol();
            } else {
                $getcol_list = $this->get_resucol();
                $getcol_list = trim($getcol_list);
                $getcol_list = ltrim($getcol_list, ',');
            }
        } else {
            if ($flg !== '3') {
                $getcol_list = $selectcol . $this->get_casewhen();
            } else {
                $getcol_list = $this->get_casewhen();
                $getcol_list = trim($getcol_list);
                $getcol_list = ltrim($getcol_list, ',');

            }
        }
     /*   $sqldata .= 'select ' . $this->mainselect . ' from ';
        $sqldata .= '(select rownumber() over(' . $this->sortColData . ') as rownum,' . $getcol_list . ' from ';
        $sqldata .= '(' . $sumsql . 'select ' . $selectcol . (($selectcol !== '' && $this->get_dataq() !== '') ? $this->get_dataq() : $this->get_dataq()) . $this->get_Grouping() . ' from ';
        $sqldata .= $this->get_innerSql();
        $sqldata .= $this->get_wheresearch() . ' ' . $groupdata . ' ' . $sumgroupby . ')as X ' . $gpChk . ') ';
        $sqldata .= 'as Y ';*/
        $sqldata .= "select " . $this->mainselect . " from ";
        $sqldata .= "(select rownumber() over(" . $this->sortColData . ") as rownum," . $getcol_list . " from ";
        $sqldata .= "(" . $sumsql . "select " . $selectcol . (($selectcol !== "" && $this->get_dataq() !== "") ? $this->get_dataq() : $this->get_dataq()) . $this->get_Grouping() . " from ";
        //error_log('LOGMMM: getResult SQL1 $selectcol:'.$selectcol);
        //error_log('LOGMMM: getResult SQL1 $this->get_dataq():'.$this->get_dataq());
        //error_log('LOGMMM: getResult SQL1 $this->get_Grouping():'.$this->get_Grouping());
        $sqldata .= $this->get_innerSql();
        $sqldata .= $this->get_wheresearch() . " " . $groupdata . " " . $sumgroupby . ")as X " . $gpChk . ") ";
        $sqldata .= "as Y ";
        //if ($flg !== '5' && $flg !== '2' && $flg !== '3') {
        //if ($flg !== '2' && $flg !== '3') {
        //if ($lastFlg === true) {
            $firCol = array();
            $col    = '';
            $colArr = $this->get_columnY();
            foreach ($this->xchecked as $key => $value) {
                if ($this->xchecked[$key]['HIDE'] !== '1') {
                    $firCol = $this->xchecked[$key];
                    $col    = $key;
                    break;
                }
            }
            $total = '';
            if ($lastFlg === true) {
                if (count($firCol) !== 0) {
                    if ($firCol['SUMG'] === '') {
                        $total = $col . "GP = 1 OR ";
                    }
                }
            }
            $sqldata .= "where " . $total . "Y.rownum between " . $pgstart . " and " . $pgend;
    //    }
        if ($selectcol !== '') {
            $sqlcount = 'select count(*) as totalcount from(select ' . $selectcol . $this->get_Grouping() . ' from ';
            $sqlcount .= $this->get_innerSql();
            $sqlcount .= $this->get_wheresearch() . ' ' . $groupdata . ' )as X' . $gpChk;
            /*総合計を引く*/
            if (count($firCol) !== 0) {
                if ($firCol['SUMG'] === '') {
                    $sqlcount .= ($this->get_gpChk() !== '') ? ' AND ' : ' WHERE ';
                    $sqlcount .= $col . 'GP <> 1 ';
                }
            }
        }
        //error_log('LOGMMM: getResult SQL1:'.$sqldata.print_r($this->get_whereparam(),true));
        $data_studio = $this->dbFetchAll($sqldata, $this->get_whereparam());
      /*      error_log("get_result".$sqldata);
            error_log(print_R($this->get_whereparam(),true));*/
        $total = 0;
        if ($data_studio !== false) {
            $data_studio = $this->umEx($data_studio, true);
            if ($selectcol !== '') {
                //error_log('LOGMMM: getResult SQL2:'.$sqlcount.print_r($this->get_whereOnlyParam(),true));
                $total = $this->dbFetchAll($sqlcount, $this->get_whereOnlyParam());
            } else {
                //selectcolがない時（縦軸を設定していない場合）はtotalcountが1になるようにリターン
                $total = array(
                    array(
                        'totalcount' => '1'
                    )
                );
            }
        }
        return array(
            $data_studio,
            $total
        );
    }
    
    public function get_result2($flg, $orderdata, $selectcol, $ali, $sum_numfun, $tablename_studio, $groupdata, $pgstart, $pgend,$lastFlg) {
        $gpChk    = ($this->get_gpChk() !== '') ? ' WHERE ' . $this->get_gpChk() : '';
        $sqldata  = '';
        $sqlcount = '';
        $sqldata .= 'select ' . $this->mainselect . ' from(';
        $sqldata .= 'select rownumber() over(' . $this->sortColData . ') as rownum ,' . $this->get_casewhen() . ' from ';
        $sqldata .= $sum_numfun . '(select ' . $selectcol . $this->get_Grouping();
        //            $sqldata .= ' from '.$this->database.'.'.$tablename_studio;
        $sqldata .= ' from ' . $this->get_innerSql();
        $sqldata .= ' ' . $this->get_wheresearch() . ' ' . $groupdata . ' ' . $ali . ')as X ' . $gpChk . ') as Y ';
            $firCol = array();
//        if ($flg !== '5' && $flg !== '2' && $flg !== '3') {
//        if ($flg !== '2' && $flg !== '3') {
    //    if ($lastFlg === true) {
            $col    = '';
            $colArr = $this->get_columnY();
            foreach ($this->xchecked as $key => $value) {
                if ($this->xchecked[$key]['HIDE'] !== '1') {
                    $firCol = $this->xchecked[$key];
                    $col    = $key;
                    break;
                }
            }
            $total = '';
            if ($lastFlg === true) {
                if (count($firCol) !== 0) {
                    if ($firCol['SUMG'] === '') {
                        $total = $col . 'GP = 1 OR ';
                    }
                }
            }
            //                $sqldata .= 'where Y.URNENG_0 IS NULL  OR Y.rownum between '.$pgstart.' and '.$pgend;
            $sqldata .= 'where ' . $total . 'rownum between ' . $pgstart . ' and ' . $pgend;
//        }
        
        $sqlcount = 'select count(*) as totalcount from(select ' . $selectcol . $this->get_Grouping();
        $sqlcount .= ' from ' . $this->get_innerSql();
        $sqlcount .= ' ' . $this->get_wheresearch() . ' ' . $groupdata . ')as X' . $gpChk;
        /*総合計を引く*/
        if (count($firCol) !== 0) {
            if ($firCol['SUMG'] === '') {
                $sqlcount .= ($this->get_gpChk() !== '') ? ' AND ' : ' WHERE ';
                $sqlcount .= $col . 'GP <> 1 ';
            }
        }
        //error_log('LOGMMM: getResult2: SQL1.$sqldata,print_r($this->get_whereparam(),true)');
        $data_studio = $this->dbFetchAll($sqldata, $this->get_whereparam());
        $data_studio = $this->umEx($data_studio, true);
        //error_log('LOGMMM: getResult2: SQL3.$sqlcount,print_r($this->get_whereOnlyParam(),true)');
        $total = $this->dbFetchAll($sqlcount, $this->get_whereOnlyParam());
        return array(
            $data_studio,
            $total
        );
    }
    public function get_resultCount($selectcol,$groupdata){
            if($selectcol !== ''){
                $gpChk    = ($this->get_gpChk() !== '') ? ' WHERE ' . $this->get_gpChk() : '';
                $sqlcount = 'select count(*) as totalcount from(select ' . $selectcol . $this->get_Grouping();
        //        error_log('LOGMMM selectCOL:'.$selectcol.'GP'.$this->get_Grouping());
                $sqlcount .= ' from ' . $this->get_innerSql();
                $sqlcount .= ' ' . $this->get_wheresearch() . ' ' . $groupdata . ')as X' . $gpChk;
                //error_log('LOGMMM: get_resultCount SQL:'.$sqlcount.'parameter'.$this->get_whereOnlyParam());
                $total = $this->dbFetchAll($sqlcount, $this->get_whereOnlyParam());
            }else{
                $total = array(
                    array(
                        'TOTALCOUNT' => '1'
                    )
                );
            }
             return $total;
    }
    public function DataCount($tablename_studio, $shukeiXdisplayFlg) {
        $sqldata    = '';
        $sqlcount   = '';
        $totalC     = 0;
        $sqlcount   = 'SELECT COUNT(*) AS TOTALCOUNT FROM ' . $this->database . '/' . $tablename_studio; //COUNTING QUERY DATA 
        $param      = array();
        //error_log('LOGMMM: DataCount SQL1:'.$sqlcount.'parameter:'.$this->get_whereOnlyParam());
        $total      = $this->dbFetchAll($sqlcount, $param);
        $totalCount = $total[0]['TOTALCOUNT'];
        if ((int) $totalCount > 0) {
            //COUNTING QUERY PIVOT DATA 
            //$sqlcount = 'SELECT COUNT(*) AS TOTALCOUNT FROM(SELECT * FROM ' . $this->database . '/' . $tablename_studio . ' ' . $this->get_wheresearch() . ')AS X';
            $sqlcount = 'SELECT COUNT(*) AS TOTALCOUNT FROM ' . $this->database . '/' . $tablename_studio . ' ' . $this->get_wheresearch() ;
            //error_log('LOGMMM: DataCount SQL2:'.$sqlcount.'parameter:'.$this->get_whereOnlyParam());
            $totalC   = $this->dbFetchAll($sqlcount, $this->get_whereOnlyParam());
            $totalC   = $totalC[0]['TOTALCOUNT'];
        }
        return $totalC;
    }
    
    public function set_totalcount($totalcount) {
        $this->totalcount = $totalcount;
    }
    public function get_totalcount() {
        return $this->totalcount;
    }
    public function set_num_count($num_count) {
        $this->num_count = $num_count;
    }
    public function get_num_count() {
        return $this->num_count;
    }
    public function set_columndata($columndata) {
        $this->columndata = $columndata;
    }
    public function get_columndata() {
        return $this->columndata;
    }
    public function set_calcdata($calcdata) {
        $this->calcdata = $calcdata;
    }
    public function get_calcdata() {
        return $this->calcdata;
    }
    public function set_data_studio($data_studio) {
        $this->data_studio = $data_studio;
    }
    public function get_data_studio() {
        return $this->data_studio;
    }
    public function set_columnupdata($columnupdata) {
        $this->columnupdata = $columnupdata;
    }
    public function get_columnupdata() {
        return $this->columnupdata;
    }
    public function set_getcheadingcol($get_cheadingcol) {
        $this->get_cheadingcol = $get_cheadingcol;
    }
    public function get_getcheadingcol() {
        return $this->get_cheadingcol;
    }
    public function set_colslists($colslists) {
        $this->colslists = $colslists;
    }
    public function get_colslists() {
        return $this->colslists;
    }
    public function set_countcalc($countcalc) {
        $this->countcalc = $countcalc;
    }
    public function get_countcalc() {
        return $this->countcalc;
    }
    public function set_headingcoluline($headingcoluline) {
        $this->headingcoluline = $headingcoluline;
    }
    public function get_headingcoluline() {
        return $this->headingcoluline;
    }
    public function set_searchcolumns($searchcolumns) {
        $this->searchcolumns = $searchcolumns;
    }
    public function get_searchcolumns() {
        return $this->searchcolumns;
    }
    public function set_pgstart($pgstart) {
        $this->pgstart = $pgstart;
    }
    public function get_pgstart() {
        return $this->pgstart;
    }
    public function set_pgend($pgend) {
        $this->pgend = $pgend;
    }
    public function get_pgend() {
        return $this->pgend;
    }
    public function set_endcount($endcount) {
        $this->endcount = $endcount;
    }
    public function get_endcount() {
        return $this->endcount;
    }
    public function set_endpointnum($endpointnum) {
        $this->endpointnum = $endpointnum;
    }
    public function get_endpointnum() {
        return $this->endpointnum;
    }
    public function set_shukeiselectColist($flg, $tablename_studio) {
        $SELECT      = '';
        $sortColData = '';
        $sortCol     = '';
        $orderDir    = '';
        $SEL         = ' ROWNUM ';
        $MINMAX      = '';
        foreach ($this->xchecked as $KEY => $value) {
            
            if ($SELECT !== '') {
                $SELECT .= ',';
            }
            if ($value['SORT'] === '1') {
                $orderDir = ' ASC ';
                $MINMAX   = 'MAX';
            } else if ($value['SORT'] === '0') {
                $orderDir = ' DESC ';
                $MINMAX   = 'MIN';
            }
            $SELECT .= $KEY . $orderDir;
            if ($this->xchecked[$KEY]['HIDE'] !== '1') {
                if ($sortColData !== '') {
                    $sortColData .= ',';
                }
                $colGP = ' ,CASE WHEN ';
                foreach ($this->xchecked as $k => $v) {
                    if ($this->xchecked[$k]['HIDE'] !== '1') {
                        if ($KEY !== $k) {
                            $colGP .= $k . 'GP = 0 AND ';
                        } else {
                            break;
                        }
                    }
                }
                if ($flg !== '3') {
                    $col = ' , ' . $KEY;
                    $SEL .= $col;
                }
                $colGP .= $KEY . 'GP = 1 THEN ' . $KEY . 'GP ELSE 0 END AS ' . $KEY . 'GP';
                $SEL .= $colGP;
                $sortColData .= $KEY . 'GP,' . $KEY . $orderDir;
            }
        }
        $orderSel = '';
        if ($SELECT !== '') {
            $orderSel = ' ORDER BY ' . $SELECT;
        }
        //mmm修正
        //$SELECT = ' (SELECT ROWNUMBER() OVER(' . $orderSel . ' ) AS RNORDER ,XY.* FROM ' . $this->database . '/' . $tablename_studio . ' AS XY) AS YZ ';
        //$SELECT = ' (SELECT XY.* FROM ' . $this->database . '/' . $tablename_studio . ' AS XY) AS YZ ';
        $SELECT =  $this->database . '/' . $tablename_studio . '  AS YZ ';
        $this->set_innerSql($SELECT);
        $this->set_xcolSelect($SEL);
        $this->sortColData = ($sortColData !== '') ? ' ORDER BY RNORDER ,' . $sortColData : ' ORDER BY RNORDER ';
        $this->sortCol     = ' , RNORDER' . $sortCol;
        //            $this->sortCol = ' , RNORDER';//.$sortCol;
    }
    public function set_shukeiGetColist($tablename_studio) {
        $SELECT             = '';
        $shukeiGetColistRes = '';
        $sortColUpData      = '';
        $orderDir           = '';
        $getselect          = '';
        $MINMAX             = '';
        foreach ($this->ychecked as $KEY => $value) {
            if ($SELECT !== '') {
                $SELECT .= ' , ';
            }
            if ($value['SORT'] === '1') {
                $orderDir = ' ASC ';
                $MINMAX   = 'MAX';
            } else if ($value['SORT'] === '0') {
                $orderDir = ' DESC ';
                $MINMAX   = 'MIN';
            }
            $SELECT .= $KEY . $orderDir;
            if ($this->ychecked[$KEY]['HIDE'] !== '1') {
                if ($sortColUpData !== '') {
                    $sortColUpData .= ',';
                }
                if ($getselect !== '') {
                    $getselect .= ',';
                }
                $getselect .= $KEY;
                $colGP = ' ,CASE WHEN ';
                foreach ($this->ychecked as $k => $v) {
                    if ($this->ychecked[$k]['HIDE'] !== '1') {
                        if ($KEY !== $k) {
                            $colGP .= $k . 'GP = 0 AND ';
                        } else {
                            break;
                        }
                    }
                }
                $getselect .= $colGP . $KEY . 'GP = 1 THEN ' . $KEY . 'GP ELSE 0 END AS ' . $KEY . '_GP';
                //   $getselect .= ' , '.$KEY.'GP';
                if ($this->shukeiYdisplayFlg === true) {
                    $shukeiGetColistRes .= ',';
                    $shukeiGetColistRes .= ' CASE  WHEN GROUPING(' . $KEY . ') = 1 THEN  ' . $MINMAX . '(' . $KEY . ') ELSE ' . $KEY . ' END AS ' . $KEY . 'S';
                    $sortColUpData .= $KEY . 'S ' . $orderDir;
                } else {
                    $sortColUpData .= $KEY . $orderDir;
                }
            }
        }
        //$SELECT = ' (SELECT ROWNUMBER() OVER(ORDER BY ' . $SELECT . ' ) AS RNORDER ,XY.* FROM ' . $this->database . '/' . $tablename_studio . ' AS XY) AS YZ ';
        //mmm修正
        //$SELECT = ' (SELECT XY.* FROM ' . $this->database . '/' . $tablename_studio . ' AS XY) AS YZ ';
        $SELECT = $this->database . '/' . $tablename_studio . ' AS YZ ';
        $this->set_innerUpSql($SELECT);
        $this->getselect .= $getselect;
        $this->shukeiGetColist = $shukeiGetColistRes;
        $this->sortColUpData   = ' ORDER BY RNORDER ,' . $sortColUpData;
    }
    
    
    //$flg 1:フィルター検索 8:columnPrev 9:columnNext 11:pager 0:表示 5:行全取得,5:Excelダウンロード,2:メール配信（１回目）,3:メール配信（マージ）
     //last : true ダウンロードの修正の時true
    //$searchdata フィルターの値　配列 default blank
    //$chkcoldata フィルター表示非表示 value="1" 配列　default blank
    //search_col_arr　フィルターのカラム名　配列　default blank
    //operator フィルターの条件　配列　default blank
    //columndata 縦軸　配列
    //columnupdata 横軸　配列
    //calcdata 集計　配列
    //num_formula 計算式　配列
    //rowstart 横スクロール　開始位置のposition default blank
    //rowend 横スクロール　終了位置のposition default blank
    //pgstart 縦スクロール　開始位置 default 1
    //pgstart 縦スクロール　終了位置　default 100
    public function crossData($flg, $searchdata, $chkcoldata, $search_col_arr, $operator, $columndata, $columnupdata, $calcdata, $num_formula, $rowstart, $rowend, $pgstart, $pgend, $shukeiXdisplayFlg = false, $shukeiYdisplayFlg = false, $xchecked = array(), $ychecked = array(),$zentaiCountFlg = false , $lastFlg = true) {
        //変数初期化
        /*$varlog = '/www/zendsvr6/htdocs/devquery2/logpivot.log';
        $varlogfile  = 'logpivot_'.date("YmdHis").'.log';
        $varlogDir = '/www/zendsvr6/htdocs/devquery2/'. $varlogfile;
        if (file_exists($varlog)){
            if(copy($varlog,$varlogDir)){
                 @unlink($varlog);
            }
        }
        ini_set('error_log', '/www/zendsvr6/htdocs/devquery2/logpivot.log');*/
        $res              = true;
        $rtncolumn        = array(); //pivotヘッダーカラム
        $sumsql           = ''; //sumcase部分のSQL
        $sumgroupby       = ''; //group by 部分のSQL
        $totalcount       = ''; //全件カウント
        $data_studio      = ''; //全件データ
        $colslists        = ''; //SQLで取得するヘッダーカラム
        $headingcoluline  = ''; //ヘッダーカラムのテキスト配列
        $endcount         = '';
        $endpointnum      = '';
        $tablename_studio = $this->table;
        $this->set_searchdata($searchdata);
        $this->shukeiYdisplayFlg = $shukeiYdisplayFlg;
        $this->shukeiXdisplayFlg = $shukeiXdisplayFlg;
        $this->xchecked          = $xchecked;
        $this->ychecked          = $ychecked;
        $this->set_shukeiselectColist($flg, $tablename_studio);
        $this->mainselect .= $this->get_xcolSelect();
        //Where句作成(フィルター機能)
        $wheresearch = '';
        $this->get_wheresearch_execute($chkcoldata, $wheresearch, $search_col_arr, $operator, $searchdata);
        if ($flg !== '3') {
            //対象テーブルのカラムを全取得
            $searchcolumns = $this->get_Column();
        }
        //縦軸配列をセット
        $this->set_columnY($columndata);
        
        //横軸配列をセット
        $this->set_columnX($columnupdata);
        
        //集計配列をセット
        $this->set_columnC($calcdata);
        
        //セットした縦横集計配列のそれぞれの重複データを削除
        $this->unique_trimArrAll();
        
        /*  //縦軸配列のカウントを取得
        $countcolumn = count($this->get_columnY());
        
        //横軸配列のカウントを取得
        $countcolumnup = count($this->get_columnX());
        
        //集計配列のカウントを取得
        $countcalc = count($this->get_columnC());*/
        
        //縦軸配列をセット
        $columndata = array();
        foreach ($this->xchecked as $key => $value) {
            if ($this->xchecked[$key]['HIDE'] !== '1') {
                array_push($columndata, $key);
            }
        }
        //横軸配列をセット
        $columnupdata = array();
        
        foreach ($this->ychecked as $key => $value) {
            if ($this->ychecked[$key]['HIDE'] !== '1') {
                array_push($columnupdata, $key);
            }
        }
        $this->set_columndata($columndata);
        $this->set_columnupdata($columnupdata);
        
        //縦軸配列のカウントを取得
        $countcolumn = count($columndata);
        
        //横軸配列のカウントを取得
        $countcolumnup = count($columnupdata);
        
        //集計配列のカウントを取得
        $countcalc = count($this->get_columnC());
        
        
        $selectcol     = ''; //SQLのセレクト句
        $incre         = 0;
        //SELECT句作成
        $selectcol     = $this->get_selectcol($countcolumn, $countcalc, $countcolumnup, $flg);
        /*  $selectcol = $get_selectcol[0];
        $gpcol = $get_selectcol[1];*/
        $incre         = 0;
        //$orderdata = $this->get_Orderby($this->get_columnC(),$orderdata); //保留　パラメータいらないはず&上で取得済み
        //追加SQL句作成
        $resultselect2 = $this->get_selectcol2($selectcol, $countcolumnup, $countcalc);
        $getcheading   = $resultselect2[1]; //カラムヘディング
        
        //計算式配列をセット
        $this->set_num_formula($num_formula);
        
        
        $postcalc = $calcdata;
        $col_cat  = '';
        if ($countcolumnup === 0) {
            $col_cat = 'C1';
        }
        
        $t = array(); //集計配列のキーを$key$にして格納
        foreach ($postcalc as $key => $value) {
            $t['$' . $key . '$'] = $col_cat . $value;
        }
        
        //$num_formula = $this->get_num_formula();
        
        $numstr_arr   = array(); //$1$をカラム名に変換した式配列
        //$1$などをカラム名に変換
        $numstr_arr   = $this->numstr_array($this->get_num_formula(), $t);
        $num_function = array();
        //計算式配列の重複データを削除
        $num_function = array_values(array_filter(array_map('trim', $numstr_arr), 'strlen'));
        $this->set_num_formula($num_function);
        //計算式配列のカウントを取得
        $num_count = count($num_function);
        
        //横軸配列があるか確認、あれば縦横集計または横集計、なかった場合は縦軸集計のみのSQLを作成
        if ($countcolumnup > 0) {
            //集計配列があるか確認
            if ($countcalc > 0) {
                $orderdata = $this->get_Orderby();
                $sql       = '';
                $count     = 0;
                $incre     = 0;
                
                
                //※保留　GROUPBY ORDER 句は上で作成済み
                $groupdata = '';
                $orderdata = '';
                
                
                //                      $this->set_Groupby($this->get_columnY(),$groupdata);
                $this->set_Groupby($this->xchecked, $groupdata);
                $this->set_Orderby($this->xchecked, $orderdata);
                
                $groupdata = $this->get_Groupby();
                
                $qData = $this->DataCount($tablename_studio, $shukeiXdisplayFlg);
                if ((int) $qData > 0) {
                    $distinctgroup = ' GROUP BY ';
                    //    $this->set_Groupby($this->get_columnX(),$distinctgroup);
                    $distinctorder = '';
                    $distinct      = '';
                    //                $this->set_Orderby($this->get_columnX(),$distinctorder);
                    $this->set_Orderby($this->ychecked, $distinctorder);
                    $dGroup = '';
                    foreach ($this->get_columnX() as $ke => $value) {
                        if ($this->ychecked[$value]['HIDE'] !== '1') {
                            if ($dGroup !== '') {
                                $dGroup .= ',';
                            }
                            $dGroup .= $value;
                        }
                    }
                    if ($shukeiYdisplayFlg === true) {
                        $distinctgroup .= " ROLLUP(" . $dGroup . ')';
                        ;
                    } else {
                        $distinctgroup .= $dGroup;
                    }
                    $distinctorder  = $this->get_Orderby();
                    $result2        = $this->distinct_columns();
                    $distinct       = $result2[0];
                    $getcheadingcol = $result2[1];
                    //検索項目が横軸配列にあるかどうか確認。あったら$schをtrue
                    $sch            = false;
                    foreach ($chkcoldata as $ky => $vl) {
                        $value = $search_col_arr[$vl];
                        if (in_array($value, $this->get_columnX())) {
                            $sch = true;
                        }
                    }
                    $this->set_shukeiGetColist($tablename_studio);
                    $sqcount = $this->end_count($sch, $distinct, $tablename_studio, $distinctgroup);
                    if ($sqcount === false) {
                        $res = false;
                    } else {
                        $endcount = $sqcount[0]['COUNT']; //total count number of query column heading
                        if ((int) $endcount > 0) {
                            //              $endcount = (int)$endcount-1;
                            $endcount    = (int) $endcount;
                            $endpointnum = $this->end_pointnum($countcolumn, $countcalc, $num_count);
                            
                            $rstart = $rowstart;
                            $this->set_endcount($endcount);
                            $rowstart  = $this->set_rowstart($flg, $rowstart, $rowend, $endpointnum);
                            $rowend    = $this->set_rowend($flg, $endcount, $rstart, $rowend, $endpointnum);
                            $chk_null  = '';
                            $rowstart  = $this->get_rowstart();
                            $rowend    = $this->get_rowend();
                            $colslists = $this->get_colists($sch, $distinct, $tablename_studio, $distinctgroup, $distinctorder, $rowstart, $rowend);
                            if ($colslists === false) {
                                $res = false;
                            } else {
                                $sumquery = '';
                                $dataq    = '';
                                $count    = 0;
                                /*sumqueryfunction*/
                                if (count($num_function) > 0) {
                                    $sumsql .= "select " . $selectcol;
                                    $sumgroupby .= " ) as A ";
                                    if ($groupdata !== '') {
                                        $sumgroupby .= " GROUP BY " . $groupdata;
                                    }
                                }
                                /****************************************************************/
                                /* for number function sum*/
                                /****************************************************************/
                                $SHIKI                = '';
                                //$col_sum = '';
                                $resu                 = '';
                                $this->whereparam     = array();
                                $this->whereOnlyParam = array();
                                $this->sumcase_query($rtncolumn, $colslists, $this->get_columnC(), $num_function, $getcheading, $flg); //get dataq and rtncolumn
                                if (count($num_function) > 0) {
                                    $shiki_colsRs = $this->shiki_cols($colslists, $num_function, $selectcol, $flg);
                                    $sumsql .= $shiki_colsRs[0];
                                    $sumgroupby .= (($groupdata === '') ? ' group by ' : ',') . $shiki_colsRs[1];
                                    $sumgroupby .= $this->sortCol;
                                    $sumsql .= $this->sortCol . ' from (';
                                    $sumgroupby .= ' ' . $orderdata;
                                }
                                $rtncolumn   = $this->get_rtncolumn();
                                $wheresearch = '';
                                $this->get_wheresearch_execute($chkcoldata, $wheresearch, $search_col_arr, $operator, $searchdata); //get wheresearch and whereparam
                                /**縦軸の集計**/
                                if ($groupdata !== '') {
                                    if ($shukeiXdisplayFlg === true) {
                                        $groupdata = "GROUP BY ROLLUP(" . $groupdata . ")";
                                    } else {
                                        $groupdata = "GROUP BY " . $groupdata;
                                    }
                                }
                            if($zentaiCountFlg === true){
                                //e_log('LOGMMM:get_resultCount:'.$selectcol.'gp'.$groupdata);
                                $zentaicount = $this->get_resultCount($selectcol,$groupdata);
                                $zentaiCount = $zentaicount[0]['TOTALCOUNT'];
                                $this->set_zentaiCount($zentaiCount);
                            }else{
                                    $resultdata  = $this->get_result($flg, $orderdata, $sumsql, $selectcol, $tablename_studio, $groupdata, $sumgroupby, $pgstart, $pgend, $sortColData,$lastFlg);
                                    $data_studio = $resultdata[0];
                                    if ($data_studio === false) {
                                        $res = false;
                                    } else {
                                        $total = $resultdata[1];
                                        if ($total === false) {
                                            $res = false;
                                        } else {
                                            $total      = $this->umEx($total);
                                            $totalcount = $total[0]['TOTALCOUNT'];
                                        }
                                    }
                                 }
                            }
                        }
                    }
                } else {
                    $data_studio = array();
                    $totalcount  = 0;
                }
            } else {
                $res = false;
            }
        } else {
            $orderdata = ''; //SQLのORDER句
            $groupdata = ''; //SQLのGROUP BY 句
            //GROUP BY句作成
            //            $groupdata = $this->set_Groupby($this->get_columnY(),$groupdata);
            $groupdata = $this->set_Groupby($this->xchecked, $groupdata);
            //ORDER BY句作成
            $orderdata = $this->set_Orderby($this->xchecked, $orderdata);
            $qData     = $this->DataCount($tablename_studio, $shukeiXdisplayFlg);
            if ((int) $qData > 0) {
                /**縦軸の集計フラグ**/
                if ($shukeiXdisplayFlg == true) {
                    $groupdata = 'GROUP BY ROLLUP( ' . $this->get_Groupby() . ' ) ';
                } else {
                    $groupdata = 'GROUP BY ' . $this->get_Groupby();
                }
                $sum_numfun = '';
                if (count($num_function) > 0) {
                    $sum_numfun .= ' (select A.* ' . $this->shiki_cols2($num_function) . ' from ';
                    $ali .= ') as A';
                }
                $selectcol .= $resultselect2[0]; //追加SELECT句
                // MMM修正
                //$selectcol .= " , MAX(RNORDER) AS RNORDER ";
                $selectcol .= " , MAX(XROWNUM) AS RNORDER ";
                if($zentaiCountFlg === true){
                    $zentaicount = $this->get_resultCount($selectcol,$groupdata);
                    $zentaiCount = $zentaicount[0]['TOTALCOUNT'];
                    $this->set_zentaiCount($zentaiCount);
                }else{
                        $orderdata            = $this->get_Orderby();
                        $rtncolumn            = $this->get_rtncolumn();
                        $this->whereparam     = array();
                        $this->whereOnlyParam = array();
                        $wheresearch          = '';
                        $this->get_wheresearch_execute($chkcoldata, $wheresearch, $search_col_arr, $operator, $searchdata); //get wheresearch and whereparam
                        $resultdata  = $this->get_result2($flg, $orderdata, $selectcol, $ali, $sum_numfun, $tablename_studio, $groupdata, $pgstart, $pgend,$lastFlg); //get resultdata
                        $data_studio = $resultdata[0];
                        $total       = $this->umEx($resultdata[1]);
                        $totalcount  = $total[0]['TOTALCOUNT'];
                        if ($data_studio === false) {
                            $res = false;
                        }
                      }
                     //error_log("全体のカウント".$zentaiCount);
                    } else {
                        $data_studio = array();
                        $totalcount  = 0;
                    }
        }
        if($zentaiCountFlg  !== true){
            $this->set_totalcount($totalcount);
            $this->set_num_count($num_count);
            $this->set_calcdata($calcdata);
            $this->set_data_studio($data_studio);
            $this->set_getcheadingcol($getcheadingcol);
            $this->set_colslists($colslists);
            $this->set_countcalc($countcalc);
            $this->set_headingcoluline($headingcoluline);
            $this->set_searchcolumns($searchcolumns);
            $this->set_pgstart($pgstart);
            $this->set_pgend($pgend);
            $this->set_endpointnum($endpointnum);
          }
        return $res;
        
    }
    
    /*
     *-------------------------------------------------------* 
     * 右トリム
     *-------------------------------------------------------*
     */
    public function um($str) {
        
        $bfr = '';
        $aft = $str;
        
        mb_regex_encoding('UTF-8');
        while ($bfr !== $aft) {
            $bfr = $aft;
            if ($bfr != null) {
                $aft = mb_ereg_replace(" +$", "", $bfr);
                $aft = mb_ereg_replace("　+$", "", $aft);
            } else {
                $aft = $bfr;
            }
        }
        
        return $aft;
        
    }
/*
 *-------------------------------------------------------* 
 * 全角半角スペース削除
 *-------------------------------------------------------* 
 */

function cmMer($str)
{
    
    $bfr = '';
    $aft = $str;
    
    mb_regex_encoding('UTF-8');
    
    while ($bfr !== $aft) {
        $bfr = $aft;
        $aft = mb_ereg_replace(" +$", "", $bfr);
        //$aft = mb_ereg_replace("　+$", "", $aft);
    }
    
    return $aft;
    
}    
    /*
     *-------------------------------------------------------* 
     * 2次元配列をすべて右トリム & フラグに応じてhtmlspecialcharas
     *-------------------------------------------------------*
     */
    public function umEx($pArray, $hscFlg = false) {
        
        $rtn = array();
        
        if (is_array($pArray)) {
            for ($i = 0; $i < count($pArray); $i++) {
                if (is_array($pArray[$i])) {
                    $rtn[$i] = array();
                    foreach ($pArray[$i] as $key => $val) {
                        if ($hscFlg === true) {
                            $v = htmlspecialchars(um($val));
                        } else {
                            $v = $this->um($val);
                        }
                        $rtn[$i][$key] = $v;
                    }
                }
            }
        }
        
        return $rtn;
    }
    
}

?>