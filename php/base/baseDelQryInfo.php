<?php
/****
 * =============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 
 * PROGRAM ID     : baseDelQryInfo.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/04/11
 * MODIFY DATE    : gggggg
 * 備考           : 通常又はSQLで作成したWEBクエリー情報削除処理
 * ============================================================
 **/
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*
*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("generateBaseTableData.php");
include_once("insQryTableData.php");
include_once("createExecuteSQL.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$QRYNM  = cmHscDe($_POST['QRYNM']);

$data = array();
$dropGphTbl = array();
$rtn = 0;
$msg = '';

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
if($rtn === 0){
    //DB接続
    $db2con = cmDb2Con();
    if($db2con === false){
        $rtn = 1;
        $msg = showMsg('FAIL_DBCON');
    }
    cmSetPHPQUERY($db2con);
}
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}
 //ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
// 管理者【グループ、作成者】権限のログインユーザーの場合管理権限のクエリーチェック
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$QRYNM,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg('ログインユーザーに指定したクエリーに対してもクエリー権限がありません。');
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
// 削除対象のクエリーがあるかチェック
if($rtn === 0){
    $res = getFDB2CSV1($db2con,$QRYNM);
    if($res['result'] === 'NOTEXIST_GET' ){
        $rtn =3;
        $msg = showMsg('NOTEXIST_DEL',array('クエリー'));
    }else if($res['result'] !== true){
        $rtn =1;
        $msg = showMsg($res['result'],array('クエリー'));
    }else{
        $fdb2csv1data = $res['data'][0];
    }
}
// 削除対象のクエリーのFDB2CSV1データ削除
if($rtn === 0){
    $resdel = delFDB2CSV1($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg = $msg.'FDB2CSV1削除完了.';
    }
}
// 定義の参照ファイル情報削除
// 削除対象のクエリーのBREFTBLデータ削除
if($rtn === 0){
    $resdel = delBREFTBL($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg = $msg.'BREFTBL削除完了.';
    }
}
// 削除対象のクエリーのBREFFLDデータ削除
if($rtn === 0){
    $resdel = delBREFFLD($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg = $msg.'BREFFLD削除完了.';
    }
}
// 定義のフィールド情報削除
// 削除対象のクエリーのFDB2CSV2データ削除
if($rtn === 0){
    $resdel = delFDB2CSV2($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg = $msg.'FDB2CSV2削除完了.';
    }
}
// 削除対象のクエリーのFDB2CSV5【結果フィールド】データ削除
if($rtn === 0){
    $resdel = delFDB2CSV5($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg = $msg.'FDB2CSV5削除完了.';
    }
}
// 削除対象のクエリーのBSUMFLD【サマリー情報】データ削除
if($rtn === 0){
    $resdel = delBSUMFLD($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'BSUMFLD削除完了.';
    }
}
// 定義の条件データ情報削除
// 削除対象のクエリーのBQRYCND【条件フィールド、検索候補、カレンダーフォマット】データ削除
if($rtn === 0){
    $resdel = delBQRYCND($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'BQRYCND削除完了.';
    }
}
// 削除対象のクエリーのBCNDDAT【初期条件データ】データ削除
if($rtn === 0){
    $resdel = delBCNDDAT($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'BCNDDAT削除完了.';
    }
}
// 削除対象のクエリーのDB2HCND【検索条件のヘルプ】データ削除
if($rtn === 0){
    $resdel = delDB2HCND($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2HCND削除完了.';
    }
}
// 削除対象のクエリーのDB2WCOL【カラム制限】データ削除
if($rtn === 0){
    $resdel = delDB2WCOL($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2WCOL削除完了.';
    }
}

// 削除対象のクエリーのDB2PCAL【ピボットの計算式】データ削除
if($rtn === 0){
    $resdel = delDB2PCAL($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2PCAL削除完了.';
    }
}
// 削除対象のクエリーのDB2PCOL【ピボットのカラム】データ削除
if($rtn === 0){
    $resdel = delDB2PCOL($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2PCOL削除完了.';
    }
}
// 削除対象のクエリーのDB2PMST【ピボット】データ削除
if($rtn === 0){
    $resdel = delDB2PMST($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2PMST削除完了.';
    }
}

//【クエリー】
// 【詳細】【詳細設定】

if($rtn === 0){
    $resdel = delDB2WDFL($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2WDFL削除完了.';
    }
}

if($rtn === 0){
    $resdel = delDB2WDTL($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2WDTL削除完了.';
    }
}
// 【詳細】【DrillDown】
if($rtn === 0){
    $resdel = delDB2DRGS($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2DRGS削除完了.';
    }
}
//【制御設定】
if($rtn === 0){
    $resdel = delDB2COLM($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2COLM削除完了.';
    }
}

if($rtn === 0){
    $resdel = delDB2COLT($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2COLT削除完了.';
    }
}
// 【スケジュール】
if($rtn === 0){
    $resdel = delDB2WSCD($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2WSCD削除完了.';
    }
}
// 【スケージュル】ゼローメール
if($rtn === 0){
    $resdel = delDB2WSOC($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2WSOC削除完了.';
    }
}
// 【スケージュル】
if($rtn === 0){
    $resdel = delDB2WAUT($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2WAUT削除完了.';
    }
}
// 【スケージュル】
if($rtn === 0){
    $resdel = delDB2WAUL($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2WAUL削除完了.';
    }
}
// 【スケージュル】の履歴
if($rtn === 0){
    $resdel = delDB2WHIS($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2WHIS削除完了.';
    }
}
// 【グループ】
if($rtn === 0){
    $resdel = delDB2WGDF($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2WGDF削除完了.';
    }
}
// 【CSV、EXCEL権限】
if($rtn === 0){
    $resdel = delDB2WDEF($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2WDEF削除完了.';
    }
}

// 【CL実行FDB2CSV1PG】
if($rtn === 0){
    $resdel = delFDB2CSV1PG($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'FDB2CSV1PG削除完了.';
    }
}
// 【CL実行FDB2CSV1PM】
if($rtn === 0){
    $resdel = delFDB2CSV1PM($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'FDB2CSV1PM削除完了.';
    }
}
// 【DB2HTMLT】
if($rtn === 0){
    $resdel = delDB2HTMLT($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2HTMLT削除完了.';
    }
}
//【Excel挿入】テンプレート:条件
if($rtn === 0){
    $resdel = delDB2ECON($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2ECON削除完了.';
    }
}
//【Excel挿入】テンプレート:条件
if($rtn === 0){
    $resdel = delDB2EINS($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2EINS削除完了.';
    }
}

//【書式挿入】テンプレート:条件
if($rtn === 0){
    $resdel = delDB2JKSK($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2JKSK削除完了.';
    }
}

// グラフデータ削除
if($rtn === 0){
    $resdel = delDB2GPC($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2GPC削除完了.';
    }
}

//お気に入り削除
if($rtn === 0){
    $rs = cmDeleteDB2QBMK($db2con,'',$QRYNM,'','','');
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }   
}
//検索条件保存の削除
if($rtn === 0){
    $rs = cmDeleteDB2SHSD($db2con,$QRYNM);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
//クエリーグループに削除

if($rtn === 0){
    $rs = fnDelDB2QRYGS($db2con,$QRYNM);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }   
}


// グラフ用一時テーブル削除ため取得
if($rtn === 0){
    $resGtmpTblLst = fnGetDB2GPKTMPTBL($db2con,$QRYNM);
    if($resGtmpTblLst['result'] !== true){
        $rtn = 1;
        $msg = $resddel['errcd'];
    }else{
        $dropGphTbl = $resGtmpTblLst['data'];
    }
}
// グラフキーデータ削除
if($rtn === 0){
    $resdel = delDB2GPK ($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2GPK削除完了.';
    }
}
// ダッシュボード
if($rtn === 0){
    $resdel = delDB2BMK ($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn =1;
        $msg = $resdel['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'DB2BMK削除完了.';
    }
}
//SQL文で作成したクエリーの場合SQL文情報削除
if($rtn === 0){
    $resdel = delBSQLDAT($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn = 1;
        $msg = $resdel['errcd'];
    }else{
        $rtn = 0;
        $msg = $msg.'BSQLDAT削除完了。';
    }
}
//SQL文で作成したクエリーの場合SQL文のパラメータ情報削除
if($rtn === 0){
    $resdel = delBSQLCND($db2con,$QRYNM);
    if($resdel['result'] !== true){
        $rtn = 1;
        $msg = $resdel['errcd'];
    }else{
        $rtn = 0;
        $msg = $msg.'BSQLCND削除完了。';
    }
}
if($rtn === 1){
    e_log('クエリー削除失敗：'.$msg);
    $msg = showMsg('DEL_FAIL',array('クエリー'));
    db2_rollback($db2con);
}else if($rtn === 0){
    if($fdb2csv1data['D1RDB'] === '' || $fdb2csv1data['D1RDB'] === RDB || $fdb2csv1data['D1RDB'] === RDBNAME){
        $db2RDBcon = $db2con;
    }else{
        $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1data['D1RDB'];
        $db2RDBcon = cmDB2ConRDB(SAVE_DB);
    }
    //グラフのワークテーブル削除
    if(count($dropGphTbl)>0){        
        foreach($dropGphTbl as $gphVal){
            dropGphTmpTbl($db2RDBcon,$gphVal);
        }
    }
    if(($fdb2csv1data['D1RDB'] === '' || $fdb2csv1data['D1RDB'] === RDB || $fdb2csv1data['D1RDB'] === RDBNAME) === false){
        cmDb2Close($db2RDBcon);
    }
    db2_commit($db2con);
    $msg = '';
    $rtn = 0;
}else{
    db2_rollback($db2con);
    $msg = $msg;
    $rtn = $rtn;
}


cmDb2Close($db2con);

$rtn = array(
    'RTN'      => $rtn,
    'MSG'      => $msg
);

echo(json_encode($rtn));

function fnDelDB2QRYGS($db2con,$QRYNM){
    $rs = true;
    $params = array($QRYNM);
    //構文
    $strSQL  = " DELETE FROM DB2QRYGS WHERE QRYGSQRY = ? ";
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}