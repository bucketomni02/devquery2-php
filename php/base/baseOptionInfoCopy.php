<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 
 * PROGRAM ID     : baseOptionInfoCopy.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 
 * MODIFY DATE    : 
 * 備考           : 定義コピーするときコピー元にある設定をコピーする
 * ============================================================
 **/
/*
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("cpyUserOpt.php");
include_once("cpyGroupOpt.php");
include_once("cpyQryExeOpt.php");
include_once("cpyHTMLTempOpt.php");
include_once("cpyXlsTempOpt.php");
include_once("cpyShosaiOpt.php");
include_once("cpyDwnKengenOpt.php");
include_once("cpyDwnSeigenOpt.php");
include_once("cpyUsrHojoOpt.php");
include_once("cpyLevelContOpt.php");
include_once("cpyCsvSetOpt.php");
include_once("cpyCndCanOpt.php");
include_once("cpyPivotOpt.php");
include_once("cpyScheduleOpt.php");
include_once("cpyCLSetOpt.php");
include_once("cpyGraphOpt.php");
include_once("cpyDashboardOpt.php");
include_once("cpyConFormatOpt.php"); //TTA
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ :①コピー先クエリー名
*            :②定義のフラグ：通常：''　SQL：1
*            :③コピー元クエリー名
*            :④選択したオプションデータ
*            :⑤条件データ
*-------------------------------------------------------*
*/
function fnCopyOptionData($db2con,$QRYNM,$D1CFLG,$QRYNEWNM,$OPTIONSELDATA,$BQRYCNDDATA)
{
    $data = array();
    foreach ($OPTIONSELDATA as $key => $option) {
        if($option['value']){
            $rs = checkOption($db2con,$QRYNM,$D1CFLG,$QRYNEWNM,$option['field'],$BQRYCNDDATA);
            if($rs['RTN'] !== 0){
                $data = $rs;
                break;
            }else{
                $data = $rs;
            }
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 処理:オプションチェック
* パラメータ :①コピー先クエリー名
*            :②定義のフラグ：通常：''　SQL：1
*            :③コピー元クエリー名
*            :④選択したオプションデータ
*            :⑤条件データ
*-------------------------------------------------------*
*/
function checkOption($db2con,$QRYNM,$D1CFLG,$NEWQRYNM,$OPTVAL,$BQRYCNDDATA){
    $rtn = 0;
    $msg = '';
    $optName = '';
    switch ($OPTVAL) {
        // ユーザー設定
        case '1':   
            $rs = cpyUserOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'ユーザー設定';
            break;
        // グループと権限設定
        case '2':
            $rs = cpyGroupOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'グループと権限設定';
            break;
        // クエリー実行設定
        case '3':
            $rs = cpyQryExeOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'クエリー実行設定';
            break;
        // HTMLテンプレート設定
        case '4':
            $rs = cpyHTMLTempOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'HTMLテンプレート設定';
            break;
        // EXCELテンプレート設定
        case '5':
            $rs = cpyXlsTempOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'EXCELテンプレート設定';
            break;
        // 詳細情報設定
        case '6':
            $rs = cpyShosaiOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = '詳細情報設定';
            break;
        // ダウンロード権限設定
        case '7':
            $rs = cpyDwnKengenOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'ダウンロード権限設定';
            break;
        // ダウンロード制限設定
        case '8':
            $rs = cpyDwnSeigenOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'ダウンロード制限設定';
            break;
        // ユーザ補助設定
        case '9':
            $rs = cpyUsrHojoOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'ユーザ補助設定';
            break;
        // 検索候補設定
        case 'A':
            $rs = cpyCndCanOpt($db2con,$QRYNM,$D1CFLG,$NEWQRYNM,$BQRYCNDDATA);
            $optName = '検索候補設定';
            break;
        // 制御レベール設定
        case 'B':
            $rs = cpyLevelContOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = '制御レベール設定';
            break;
        // ピボット設定
        case 'C':
            $rs = cpyPivotOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'ピボット設定';
            break;
        // CSV形式設定
        case 'D':
            $rs = cpyCsvSetOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'CSV形式設定';
            break;
        // スケジュール設定
        case 'E':
            $rs = cpyScheduleOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'スケジュール設定';
            break;
        // CL連携設定
        case 'F':
            $rs = cpyCLSetOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'CL連携設定';
            break;
        // グラフ設定
        case 'G':
            $rs = cpyGraphOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'グラフ設定';
            break;
        // ダッシュボード設定
        case 'H':
            $rs = cpyDashboardOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = 'ダッシュボード設定';
            break;
		//Add by TTA
        // 条件付き書式設定
        case 'J':
            $rs = cpyConFormatOpt($db2con,$QRYNM,$NEWQRYNM);
            $optName = '条件付き書式設定';
            break;
        default :
            break;
    }
    if($rs !== ''){
        $rtn = 1;
        $msg = showMsg('OPTCPY_FAIL',array($optName));
    }
    $rtnArr = array(
        'RTN' => $rtn,
        'MSG' => $msg
    );
    return $rtnArr;
}
/*
*-------------------------------------------------------* 
*別のライブラリーで使う
* 処理:選択したオプションのコピー呼び出し
*この機能はcheckOptionと違うのはFDB2CSV1とFDB2CSV2なら権限入れてない。
* パラメータ ：①古いライブラリー名
*            :②新いライブラリー名
*            :③クエリー名
*            :③普通のクエリーとSQLのFLG
*            :③検索データ
*-------------------------------------------------------*
*/
function fnCopyOptionData_OTHER($db2con,$newLib,$D1NAME,$D1CFLG){
    $rtn=0;
    $msg='';
    $dataArr=array();
    $rs = cmGetQryOptionInfo($db2con,$D1NAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = umEx($rs['data']);
        if(count($data)>0){
            // グループと権限設定
            if($rtn===0){
                if($data[0]['A2']==="1"){
                    $rs = cpyGroupOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('グループと権限設定'));
                    }
                }
            }
            // HTMLテンプレート設定
            if($rtn===0){
                if($data[0]['A4']==="1"){
                    $rs = cpyHTMLTempOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('HTMLテンプレート設定'));
                    }
                }
            }
            // EXCELテンプレート設定
            if($rtn===0){
                if($data[0]['A5']==="1"){
                    $rs = cpyXlsTempOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('EXCELテンプレート設定'));
                    }
                }
            }
            // 詳細情報設定
            if($rtn===0){
                if($data[0]['A6']==="1"){
                    $rs = cpyShosaiOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('詳細情報設定'));
                    }
                }
            }
            // ダウンロード権限設定
            if($rtn===0){
                if($data[0]['A7']==="1"){
                    $rs = cpyDwnKengenOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('ダウンロード権限設定'));
                    }
                }
            }
            // 制御レベール設定
            if($rtn===0){
                if($data[0]['B']==="1"){
                    $rs = cpyLevelContOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('制御レベール設定'));
                    }
                }
            }
            // ピボット設定
            if($rtn===0){
                if($data[0]['C']==="1"){
                    $rs = cpyPivotOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('ピボット設定'));
                    }
                }
            }
            // スケジュール設定
            if($rtn===0){
                if($data[0]['E']==="1"){
                    $rs = cpyScheduleOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('スケジュール設定'));
                    }
                }
            }
            // CL連携設定
            if($rtn===0){
                if($data[0]['F']==="1"){
                    $rs = cpyCLSetOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('CL連携設定'));
                    }
                }
            }
            // グラフ設定
            if($rtn===0){
                if($data[0]['G']==="1"){
                    $rs = cpyGraphOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('グラフ設定'));
                    }
                }
            }
            //ダッシュボード設定
            if($rtn===0){
                if($data[0]['H']==="1"){
                    $rs = cpyDashboardOpt_OTHER($db2con,$newLib,$D1NAME);
                    if($rs !== ''){
                        $rtn = 1;
                        $msg = showMsg('OPTCPY_FAIL',array('ダッシュボード設定'));
                    }
                }
            }
        }//end of count>0
    }
    $dataArr= array(
        'RTN' => $rtn,
        'MSG' => $msg
    );
    return $dataArr;
}
