<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 
 * PROGRAM ID     : basePreviewQryData.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 
 * MODIFY DATE    : 
 * 備考           : 定義プレビューデータ取得
 * ============================================================
 **/
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("comGetFDB2CSV2_CCSID.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$TEMPTBLNM = $_POST['TEMPTBLNM'];
$RDBNM = (isset($_POST['RDBNM'])?cmMer($_POST['RDBNM']):'');
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$D1CFLG = (isset($_POST['D1CFLG'])?$_POST['D1CFLG']:'');
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$fldArr = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
if($RDBNM === '' || $RDBNM == $RDB || $RDBNM === RDBNAME){
    $db2conLib = $db2con;
}else{
    $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
    $db2conLib = cmDB2ConRDB(SAVE_DB);
}
if($D1CFLG === '1'){
    $rs = fngetSQLFldCCSID($db2conLib,$TEMPTBLNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
       
        $fldCCSIDARR = $rs['data'];
        $fldccsidCnt = $rs['CCSIDCNT'];
        if($fldccsidCnt>0){
            $fldArr = array();
            foreach($fldCCSIDARR as $fldccsid){
                if($fldccsid['CCSID'] === '65535' && $fldccsid['DDS_TYPE'] !== '5' && $fldccsid['DDS_TYPE'] !== 'H'){

                    if($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW'){
                        $fldArr[] = 'CAST('.$fldccsid['COLUMN_NAME'].'  AS CHAR(4) CCSID 5026) AS '.$fldccsid['COLUMN_NAME'];
                    }else{
                        $fldArr[] = 'CAST('.$fldccsid['COLUMN_NAME'].'  AS CHAR('.$fldccsid['LENGTH'].') CCSID 5026) AS '.$fldccsid['COLUMN_NAME'];
                    }

                }else{
                    $fldArr[] = $fldccsid['COLUMN_NAME'];
                }
            }
        }
    }
}
if($rtn === 0){
    $rs = fnGetAllCount($db2conLib,$TEMPTBLNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $allcount = $rs['data'];
    }
}
if($rtn === 0){
    $rs = fnGetQryData($db2conLib,$TEMPTBLNM,$start,$length,$sort,$sortDir, $fldArr);
  
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    
    }

}
/*}else{
    
    if($rtn === 0){
        $rs = fnGetAllCount($db2conRDB,$TEMPTBLNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetQryData($db2conRDB,$TEMPTBLNM,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
    cmDb2Close($db2conRDB);
}*/
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => $data,
    'BACKZERO_FLG' => BACKZERO_FLG,
    'ZERO_FLG' => ZERO_FLG,
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 定義のプレビューデータ取得
*-------------------------------------------------------*
*/

function fnGetQryData($db2con,$TEMPTBLNM,$start = '',$length = '',$sort = '',$sortDir = '', $fldArr = ''){
   
    $data = array();
    $params = array();
    $systables = cmGetSystables($db2con,SAVE_DB,$TEMPTBLNM);
   
    if(count($systables) > 0){
        $strSQL = ' SELECT A.* ';
        $strSQL .= ' FROM ( SELECT ';
        if($fldArr === ''){
            $strSQL .= '  B.* ';
        }else{
            $fldArrStr = join(",", $fldArr);
            $strSQL .=   $fldArrStr ;
        }
        $strSQL .= ' , ROWNUMBER() OVER( ';
        $strSQL .= ' ) as rownum ';
        $strSQL .= ' FROM '.SAVE_DB.'/'.$TEMPTBLNM.'  B ';    
        $strSQL .= ' ) as A ';

        //抽出範囲指定
        if(($start != '')&&($length != '')){
            $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
            array_push($params,$start + 1);
            array_push($params,$start + $length);
        }
        $stmt = db2_prepare($db2con,$strSQL);
      
        
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
          
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_array($stmt)){
                    unset($row[count($row)-1]);
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => umEx($data,true));
            }
        }
    }else{
        //$data = array('result' => '実行中のデータが削除されました。</br>選択条件を直したい場合、「戻る（選択条件の入力）」を押してください。<br>今の定義を保存したい場合、「保存」を押して下さい。');
        $data = array('result' => 'FAIL_PREV_TEMPTBL');
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$TEMPTBLNM){
    $data = array();
    $params = array();
    $systables = cmGetSystables($db2con,SAVE_DB,$TEMPTBLNM);
    if(count($systables) > 0){
        $strSQL  = ' SELECT COUNT(*) AS COUNT';
        $strSQL .= ' FROM (';
        $strSQL .= ' SELECT ';
        $strSQL .= '        A.* ';
        $strSQL .= ' FROM '.SAVE_DB.'/'.$TEMPTBLNM.'  A)  B' ;
     
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row['COUNT'];
                }
                $data = array('result' => true,'data' => $data[0]);
            }
        }
    }else{
        //$data = array('result' => '実行中のデータが削除されました。</br>選択条件を直したい場合、選択条件の入力画面に戻してください。<br>今の定義を保存したい場合、保存して下してください。');
        $data = array('result' => 'FAIL_PREV_TEMPTBL');
    }
    return $data;
}