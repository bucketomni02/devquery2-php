<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 
 * PROGRAM ID     : baseSaveQryInfo.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/04/11
 * MODIFY DATE    : 
 * 備考           : 通常で作成したWEBクエリー情報保存処理
 * ============================================================
 **/
/*
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("generateBaseTableData.php");
include_once("insQryTableData.php");
include_once("createExecuteSQL.php");
include_once("getPrevSaveFrmData.php");
include_once("baseOptionInfoCopy.php");
include_once("fnDB2QHIS.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$PROC             = $_POST['PROC'];
$PREV             = $_POST['PREV'];
$CPYFLG           = $_POST['CPYFLG'];
$BASEFILEFRMDATA  = json_decode($_POST['BASEFILEDATA'],true);
$BASEFIELDFRMDATA = json_decode($_POST['BASEFIELDDATA'],true);
$BASECONDFRMDATA  = json_decode($_POST['BASEC0NDDATA'],true);//ppn test
$BASELIBFRMDATA   = json_decode($_POST['BASELIBDATA'],true);
$SUMMARYFRMDATA   = json_decode($_POST['SUMMARYDATA'],true);
$OPTIONSELDATA    = json_decode($_POST['OPTIONSELDATA'],true);
$COLMDATA   = json_decode($_POST['COLMDATA'],true);
$COLTDATA    = json_decode($_POST['COLTDATA'],true);
$OUTPUTFDATA  = json_decode($_POST['OUTPUTFDATA'],true);//出力ファイルのデータ


error_log("PyaePhyoNaing++++".print_r($BASECONDFRMDATA,true));
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$BASEFILEDATA  = array();
$BASEFIELDDATA = array();
$BASECONDDATA  = array();
$BASELIBDATA   = '';
$SUMMARYDATA   = array();
$qryColData    = array();

$data          = array();
$rtn           = 0;
$msg           = '';
$tmpTblNm      = '';

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
if($CPYFLG === '1'){
    $QRYNM = $BASEFILEFRMDATA['D1NAMENEW'];
    $BASEFILEDATA['D1NAME']   = $BASEFILEFRMDATA['D1NAMENEW'];
    $BASEFIELDDATA['D2NAME']  = $BASEFILEFRMDATA['D1NAMENEW'];
}else{
    $QRYNM = $BASEFILEFRMDATA['D1NAME'];
    $BASEFILEDATA['D1NAME']   = $BASEFILEFRMDATA['D1NAME'];
    $BASEFIELDDATA['D2NAME']  = $BASEFILEFRMDATA['D1NAME'];
}

// ファイルデータ
$BASEFILEDATA['D1FILE']   = $BASEFILEFRMDATA['D1FILE'];
$BASEFILEDATA['RDBNM']    = $BASEFILEFRMDATA['RDBNM'];
$BASEFILEDATA['D1FILLIB'] = $BASEFILEFRMDATA['D1FILLIB'];
$BASEFILEDATA['D1FILMBR'] = $BASEFILEFRMDATA['D1FILMBR'];
$BASEFILEDATA['D1TEXT']   = $BASEFILEFRMDATA['D1TEXT'];
$BASEFILEDATA['REFFILE']  = $BASEFILEFRMDATA['REFFILE'];


$BASEFIELDDATA['BASEFIELDDATA']= $BASEFIELDFRMDATA;


// フィールドデータ
if($CPYFLG === '1'){
    if(count($BASECONDFRMDATA) > 0){
        for($cndcnt = 0; $cndcnt<count($BASECONDFRMDATA);$cndcnt++){
            $BASECONDFRMDATA[$cndcnt]['QRYN'] = $BASEFILEFRMDATA['D1NAMENEW'];
        }
    }
}
// 条件データ
$BASECONDDATA = $BASECONDFRMDATA;

/*$BASECONDDATA['QRYNM']        = $BASECONDFRMDATA['D1NAME'];
$BASECONDDATA['CNDCNT']       = $BASECONDFRMDATA['CNDCNT'];
$BASECONDDATA['BASECONDDATA'] = $BASECONDFRMDATA['CNDDATAINFO'];*/


// サマリーフィールドデータ

if($SUMMARYFRMDATA !== ''){
    foreach($SUMMARYFRMDATA['SUMKEYDATA'] AS $sumKeyData){
        $sumkey = array();
        $sumkey['QRYNM']  = $QRYNM;
        $sumkey['FILID']  = $sumKeyData['FILID'];
        $sumkey['FILTYP'] = $sumKeyData['FILTYP'];
        $sumkey['FLDNM']  = $sumKeyData['FLDNM'];
        $sumkey['ORPOS']  = $sumKeyData['ORPOS'];
        $sumkey['KEYFLG'] = 'S';
        $sumkey['SEQ']    = $sumKeyData['GSEQ'];
        $sumkey['GMES']   = '';
        $SUMMARYDATA[]    = $sumkey;
    }
    foreach($SUMMARYFRMDATA['GROUPINGDATA'] AS $groupingData){
        $gpkey = array();
        $gpkey['QRYNM']  = $QRYNM;
        $gpkey['FILID']  = $groupingData['FILID'];
        $gpkey['FILTYP'] = $groupingData['FILTYP'];
        $gpkey['FLDNM']  = $groupingData['FLDNM'];
        $gpkey['ORPOS']  = $groupingData['ORPOS'];
        $gpkey['KEYFLG'] = 'G';
        $gpkey['SEQ']    = $groupingData['GSEQ'];
        $gpkey['GMES']   = $groupingData['GMES'];
        $SUMMARYDATA[]   = $gpkey;
    }
}

// 画面のデータからテーブルデータに変換
//ファイルデータ
$resultFile = generateDBFileTabData($BASEFILEDATA,$BASELIBFRMDATA);
$fdb2csv1   = $resultFile['FDB2CSV1'];


$breftblfld = $resultFile['BREFTBLFLD'];
e_log("baseSaveQry.php 画面のデータからテーブルデータに変換:".print_r($breftblfld,true));

//フィールドデータ
$resultField = generateDBFieldTabData($BASEFIELDDATA,$SUMMARYDATA);

$fdb2csv2    = $resultField['FDB2CSV2'];
$fdb2csv5    = $resultField['FDB2CSV5'];
$bsumfld     = $resultField['BSUMFLD'];
$updFdb2csv2 = $resultField['UPDFDB2CSV2'];
$updFdb2csv5 = $resultField['UPDFDB2CSV5'];
//e_log("fdb2csv5 baseSaveQryInfo => ".print_r($fdb2csv5,true));
//条件データ
$resultCond  = generateDBCondTabData($BASECONDDATA);
$bqrycndData = $resultCond['BQRYCNDDATA'];
error_log('20181115***');
error_log(print_r($bqrycndData,true));
if($rtn === 0){
    //DB接続
    $db2con = cmDb2Con();
    if($db2con === false){
        $rtn = 1;
        $msg = showMsg('FAIL_DBCON');
    }

    cmSetPHPQUERY($db2con);
}
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}
 //ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
if($rtn === 0 ){
    // 定義のデータを取得、追加の場合　定義有り　⇒エラー
    $res = getFDB2CSV1($db2con,$QRYNM);
    if($res['result'] === 'NOTEXIST_GET' ){
        if($PROC !== 'ADD'){
            //$rtn =1;
            //$msg = showMsg('NOTEXIST_UPD',array('クエリー'));
            $PROC = 'ADD';
        }
    }else if($res['result'] !== true){
        $rtn =1;
        $msg = showMsg($res['result'],array('クエリー'));
    }else{
        if($PROC === 'ADD'){
            $rtn =1;
            $msg = showMsg('ISEXIST',array('クエリー'));
        }
    }
}

if($rtn === 0){
    $cnt = 0;
    foreach($fdb2csv2 as $fdb2csv2data){
        if($fdb2csv2data['D2CSEQ'] > 0){
            $cnt = $cnt +1;
        }
    }
    if(count($fdb2csv5)>0){
        foreach($fdb2csv5 as $fdb2csv5data){
            if($fdb2csv5data['D5CSEQ'] > 0){
                $cnt = $cnt +1;
            }
        }
    }
    $cnt = $cnt + count($bsumfld);
    if($cnt > FLDCNTMAX){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLIMIT',array('フィールド',FLDCNTMAX,'項目'));
    }
}
if($rtn === 0){
    if(count($bqrycndData) > CNDCNTMAX){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLIMIT',array('条件項目',CNDCNTMAX,'個'));
    }
}
/*
/*クエリー作成時にダウンロード権限を付与(イー)
**/
if($rtn === 0){
    if($CPYFLG === '1'){
        $QRYNM = $BASEFILEFRMDATA['D1NAMENEW'];
    }
    if($PROC === 'ADD' && $PREV === 'false'){
		if(CND_QRYKG === '1'){
			$rs = cmGetAllUser($db2con,$QRYNM);
		    if($rs['result'] !== true){
		        $rtn = 1;
		        $msg = $rs['result'];
		    }
			else{
	            foreach($rs['data'] as $userData){
                    $name = cmGetQryNameFromDB2WDEF($db2con,$userData['WUUID'],$QRYNM);
                    if ($name['result'] === 'NOTEXIST_GET') {
				        $rs = cmInsQryKengen($db2con,$userData['WUUID'],$QRYNM,'1','1');
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = $rs['result'];
                            db2_rollback($db2con);
                            break;
                        }
                    }else{
                        $rs = cmUpdQryKengen($db2con,$userData['WUUID'],$QRYNM,'1','1');
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = $rs['result'];
                            db2_rollback($db2con);
                            break;
                        }
                    }
				}
			}
		}
	}
}

if($rtn === 0){
    if($PROC === 'ADD'){
        $resIns = insFDB2CSV1($db2con,$fdb2csv1);
        if($resIns['result'] !== true){
            $rtn =1;
            $msg = $resIns['errcd'];
        }else{
            $rtn =0;
            $msg = 'FDB2CSV1登録完了.';
        }
    }else{
        $resupd = updFDB2CSV1($db2con,$fdb2csv1);
        if($resupd['result'] !== true){
            $rtn =1;
            $msg = $resupd['errcd'];
        }else{
            $rtn =0;
            $msg = 'FDB2CSV1更新完了.';
        }
    }
}
//出力ファイル更新データFDB2CSV1　updOUTFILEFDB2CSV1
if($rtn === 0){
    if(count($OUTPUTFDATA)>0){
        $ddata=$OUTPUTFDATA[0];
        $resIns = updOUTFILEFDB2CSV1($db2con,$ddata);
        if($resIns['result'] !== true){
            $rtn =1;
            $msg = $resIns['errcd'];
        }else{
            $rtn =0;
            $msg .= 'FDB2CSV1で出力ファイル更新完了.';
        }
    }
}

if($rtn === 0){
    if($PROC === 'ADD'){
        if(count($breftblfld) > 0){
            $resIns = insBREFTBLFLD($db2con,$breftblfld);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg = $msg.'参照ファイルデータ登録完了.';
            }
        }
    }else{
        $resdel = delBREFTBL($db2con,$QRYNM);
        if($resdel['result'] !== true){
            $rtn =1;
            $msg = $resdel['errcd'];
        }else{
            $rtn =0;
            $msg = 'BREFTBL削除完了.';
            $resdel = delBREFFLD($db2con,$QRYNM);
            if($resdel['result'] !== true){
                $rtn =1;
                $msg = $resdel['errcd'];
            }else{
                $rtn =0;
                $msg = 'BREFFLD削除完了.';
                if(count($breftblfld) > 0){
                    $resIns = insBREFTBLFLD($db2con,$breftblfld);
                    if($resIns['result'] !== true){
                        $rtn =1;
                        $msg = $resIns['errcd'];
                    }else{
                        $rtn =0;
                        $msg = $msg.'参照ファイルデータ登録完了.';
                    }
                }
            }
        }
    }
}
if($rtn === 0){
    if($PROC === 'ADD'){
        $resIns = insFDB2CSV2($db2con,$fdb2csv2);
        if($resIns['result'] !== true){
            $rtn =1;
            $msg = $resIns['errcd'];
        }else{
            $rtn =0;
            $msg =$msg. 'FDB2CSV2登録完了.';
        }
    }else{
        $resdel = delFDB2CSV2($db2con,$QRYNM);
        if($resdel['result'] !== true){
            $rtn =1;
            $msg = $resdel['errcd'];
        }else{
            $rtn =0;
            $msg =$msg. 'FDB2CSV2削除完了.';
            $resIns = insFDB2CSV2($db2con,$fdb2csv2);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'FDB2CSV2登録完了.';
            }
        }
    }
}
if($rtn === 0){
    if($PROC === 'ADD'){
        if(count($fdb2csv5)>0){
            $resIns = insFDB2CSV5($db2con,$fdb2csv5);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'FDB2CSV5登録完了.';
            }
        }
    }else{
        $resdel = delFDB2CSV5($db2con,$QRYNM);
        if($resdel['result'] !== true){
            //e_log('RESULT MSM delcsv5=> '.$resdel['result'].' MSG => '.$resdel['errcd']);
            $rtn =1;
            $msg = $resdel['errcd'];
        }else{
            //e_log('RESULT MSM else cc=> '.count($fdb2csv5));
            $rtn =0;
            $msg =$msg. 'FDB2CSV5削除完了.';
            if(count($fdb2csv5)>0){
                $resIns = insFDB2CSV5($db2con,$fdb2csv5);
                //e_log('RESULT MSM resIns=> '.print_r($resIns,true));
                if($resIns['result'] !== true){
                    $rtn =1;
                    $msg = $resIns['errcd'];
                }else{
                    //**e_log('RESULT MSM TTTT=> ');
                    $rtn =0;
                    $msg =$msg. 'FDB2CSV5登録完了.';
                }
            }
        }
    }
}
if($rtn === 0){
    if(count($updFdb2csv2)>0){
        $resUpd = updFDB2CSV2($db2con,$updFdb2csv2);
        if($resUpd['result'] !== true){
            $rtn =1;
            $msg = $resUpd['errcd'];
        }else{
            $rtn =0;
            $msg =$msg. 'FDB2CSV2更新完了.';
        }
    }
}
if($rtn === 0){
    if(count($updFdb2csv5)>0){
        $resUpd = updFDB2CSV5($db2con,$updFdb2csv5);
        if($resUpd['result'] !== true){
            $rtn =1;
            $msg = $resUpd['errcd'];
        }else{
            $rtn =0;
            $msg =$msg. 'FDB2CSV5更新完了.';
        }
    }
}
if($rtn === 0){
    if($PROC === 'ADD'){
        if(count($bsumfld)>0){
            $resIns = insBSUMFLD($db2con,$bsumfld);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'BSUMFLD登録完了.';
            }
        }
    }else{
        $resdel = delBSUMFLD($db2con,$QRYNM);
        if($resdel['result'] !== true){
            $rtn =1;
            $msg = $resdel['errcd'];
        }else{
            $rtn =0;
            $msg =$msg. 'BSUMFLD削除完了.';
            if(count($bsumfld)>0){
                $resIns = insBSUMFLD($db2con,$bsumfld);
                if($resIns['result'] !== true){
                    $rtn =1;
                    $msg = $resIns['errcd'];
                }else{
                    $rtn =0;
                    $msg =$msg. 'BSUMFLD登録完了.';
                }
            }
        }
    }
}
if($rtn === 0){
    if($PROC === 'ADD'){
        if(count($bqrycndData) > 0){
            $resIns = insBQRYCNDDAT($db2con,$bqrycndData);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg = $msg.'条件データ登録完了.';
                $msg = '';
            }
        }
    }else if($CPYFLG === '1'){
        if(count($bqrycndData) > 0){
            $resIns = insBQRYCNDDAT($db2con,$bqrycndData);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                //更新条件SEQ
                $resUpd = updBQRYCNDSEQ($db2con,$bqrycndData);
                if($resUpd['result'] !== true){
                    $rtn =1;
                    $msg = $resUpd['errcd'];
                }else{
                    $rtn =0;
                    $msg = $msg.'条件データ登録完了.';
                    $msg = '';
                }
            }
        }
    }else{
        $rs = getCanData($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = $rs['result'];
        }else{
            $candata = $rs['data'];
        }
        if($rtn === 0){
            $rs = getDB2HCND($db2con,$QRYNM);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = $rs['result'];
            }else{
                $hcnddata = $rs['data'];
            }
        }
        if($rtn === 0){
            $resdel = delBQRYCND($db2con,$QRYNM);
            if($resdel['result'] !== true){
                $rtn =1;
                $msg = $resdel['errcd'];
            }else{
                $rtn =0;
                $msg = $msg.'BQRYCND削除完了.';
                $resdel = delBCNDDAT($db2con,$QRYNM);
                if($resdel['result'] !== true){
                    $rtn =1;
                    $msg = $resdel['errcd'];
                }else{
                    $rtn =0;
                    $msg = $msg.'BCNDDAT削除完了.';
                    $resdel = delDB2HCND($db2con,$QRYNM);
                    if($resdel['result'] !== true){
                        $rtn =1;
                        $msg = $resdel['errcd'];
                    }else{
                        $rtn =0;
                        $msg = $msg.'DB2HCND削除完了.';
                        if(count($bqrycndData) > 0){
                            $resIns = insBQRYCNDDAT($db2con,$bqrycndData);
                            if($resIns['result'] !== true){
                                $rtn =1;
                                $msg = $resIns['errcd'];
                            }else{
                                $rtn =0;
                                $msg = $msg.'条件データ登録完了.';
                                if(count($candata)>0){
                                    error_log("PPN20181105 candata for updBQRYCND => ".print_r($candata,true));
                                    $resupd = updBQRYCND($db2con,$candata);
                                    if($resupd['result'] !== true){
                                        $rtn =1;
                                        $msg = $resupd['errcd'];
                                    }
                                }
                                if($rtn === 0){
                                    if(count($hcnddata)>0){
                                        $resupd = updDB2HCND($db2con,$hcnddata);
                                        if($resupd['result'] !== true){
                                            $rtn =1;
                                            $msg = $resupd['errcd'];
                                        }else{
                                            $rtn =0;
                                            $msg = '';
                                        }
                                    }
                                }
                                if($rtn === 0){
                                    //更新条件SEQ
                                    $resUpd = updBQRYCNDSEQ($db2con,$bqrycndData);
                                    if($resUpd['result'] !== true){
                                        $rtn =1;
                                        $msg = $resUpd['errcd'];
                                    }
                                }
                            }
                        }
                    }
                }
            }   
        }  
    }
}
if($PROC !== 'ADD'){
    if($rtn === 0){
        $rs = cmDeleteDB2SHSD($db2con,$QRYNM);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }else{
            $rtn =0;
            $msg =$msg. 'DB2SHSD削除完了.';
        }
    }
}
//詳細キーチェック
if($rtn === 0){
    $rs = getNotShosaiFld($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = $rs['result'];
    }else{
        $shosaidata = $rs['data'];
    }
    if($rtn === 0){
        if(count($shosaidata)>0){
            foreach ($shosaidata as $shosai){
                $delDTL = delDB2WDTLBYSEQ($db2con,$QRYNM,$shosai['DTFILID'],$shosai['DTFLD']);
                if($delDTL['result'] !== true){
                    $rtn = 1;
                    $msg = $delDTL['result'];
                }else{
                    $delDFL = delDB2WDFLBYSEQ($db2con,$QRYNM,$shosai['DTFILID'],$shosai['DTFLD']);
                    if($delDFL['result'] !== true){
                        $rtn = 1;
                        $msg = $delDFL['result'];
                    }
                }
            }
        }
    }    
}
if($rtn === 0){
   //DrillDownチェック
    $rs = getNotDrilldownFld($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = $rs['result'];
    }else{
        $drillDown = $rs['data'];
        if(count($drillDown)>0){
            foreach ($drillDown as $drilldowndata){
                $delDrgs = delDB2DRGSByFld ($db2con,$QRYNM,$drilldowndata);
                if($delDrgs['result'] !== true){
                    $rtn = 1;
                    $msg = $delDrgs['result'];
                }
            }
        }
    } 
}
if($rtn === 0){
    $rs = delDB2WCOL($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = $rs['result'];
    }
}
if($rtn === 1){
    db2_rollback($db2con);
}else if($rtn !== 2){
    // 作成定義のSQL文生成
    $resExeSql = runExecuteSQL($db2con,$QRYNM);
    if($resExeSql['RTN'] !== 0){
        $rtn = 1;
        db2_rollback($db2con);
        $msg = $resExeSql['MSG'];
    }else{
        $qryData = $resExeSql;
        //e_log("qryData *** MSM => ".print_r($qryData,true));
        if($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME){
            $db2Execon = cmDb2ConLib($qryData['LIBLIST']);
            //$resExec = execQry($db2Execon,$qryData['STREXECTESTSQL'],$qryData['EXECPARAM'],$qryData['LIBLIST'],$qryData['MBRDATALST']);
        }else{
            $_SESSION['PHPQUERY']['RDBNM'] = $qryData['SELRDB'];
            $db2Execon = cmDB2ConRDB($qryData['LIBLIST']);
            //$resExec = execQry($db2Execon,$qryData['STREXECTESTSQL'],$qryData['EXECPARAM'],$qryData['LIBLIST'],$qryData['MBRDATALST']);
        }
        if(count($qryData['MBRDATALST']) >0){
            foreach($qryData['MBRDATALST'] as $mbrdata){
                $rs = setFileMBR($db2Execon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                if($rs !== true){
                    //e_log('テーブル設定失敗');
                    $rtn = 1;
                    $msg = showMsg($rs);
                    break;
                }
            }
        }
        if($rtn === 0){
            // 作成定義のSQL文実行
            $resExec = execQry($db2Execon,$qryData['STREXECTESTSQL'],$qryData['EXECPARAM'],$qryData['LIBLIST'],$qryData['MBRDATALST']);
            if($resExec['RTN'] !== 0){
                cmDb2Close($db2Execon);
                $rtn = 1;
                db2_rollback($db2con);
                $msg = showMsg($resExec['MSG']);
            }else{
                cmDb2Close($db2Execon);
                if($PREV === 'true'){
                    $rsQryCol = cmGetFDB2CSV2($db2con,$QRYNM,false,false,false);
                    if($rsQryCol['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rsQryCol['result']);
                    }else{
                        $qryColData = $rsQryCol['data'];
                    }
                    $saveData = getPrevSaveData($db2con,$QRYNM);
                    if($saveData['RTN'] === 0){
                        $SAVEDBASEFILEDATA  = $saveData['BASEFILEDATA'];
                        $SAVEDBASEFIELDDATA = $saveData['BASEFIELDDATA'];
                        $SAVEBASECONDDATA   = $saveData['BASECONDDATA'];
                        $SAVESUMMARYDATA    = $saveData['SUMMARYDATA'];
                    }

                    //db2_rollback($db2con);
                
                    $tmpTblNm = makeRandStr(10,true);
                    //if($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNM){
                    if($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB){
                        $db2tblcon = cmDb2ConLib($qryData['LIBLIST']);
                        //$resCreateTbl = createTmpTable($db2tblcon,$qryData['LIBLIST'],$qryData['STREXECSQL'],$qryData['EXECPARAM'],$tmpTblNm,$qryData['MBRDATALST']);
                    }else{
                        $_SESSION['PHPQUERY']['RDBNM'] = $qryData['SELRDB'];
                        $db2tblcon = cmDB2ConRDB($qryData['LIBLIST']);
                        //$resCreateTbl = createTmpTableRDB($db2con,$db2tblcon,$qryData['SELRDB'],$qryData['LIBLIST'],$qryData['STREXECSQL'],$qryData['EXECPARAM'],$tmpTblNm,$qryData['MBRDATALST']);
                    }
					$strSQLdata='';
                    if(count($qryData['MBRDATALST']) >0){
                        foreach($qryData['MBRDATALST'] as $mbrdata){
			                $strSQL = '';
                            $rs = setFileMBR($db2tblcon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM'],$strSQL);
                            if($rs !== true){
                                e_log('テーブル設定失敗');
                                $rtn = 1;
                                $msg = showMsg($rs);
                                break;
                            }else{
			                    if($strSQLdata !== ''){
			                        $strSQLdata .= '<br/>';
			                    }
			                    $strSQLdata .= $strSQL;
			                }
                        }
                    }
                    $estart = microtime(true);
			        $sql_wlsqlb = $strSQLdata.'<br/><br/>'.$qryData['STREXECSQL'];
                    if($rtn === 0){
                        $resCreateTbl = createTmpTableRDB($db2con,$db2tblcon,$qryData['SELRDB'],$qryData['LIBLIST'],$qryData['STREXECSQL'],$qryData['EXECPARAM'],$tmpTblNm,$qryData['MBRDATALST']);
                        if($resCreateTbl['RTN'] !== 0){
                            cmDb2Close($db2tblcon);
                            $rtn = 1;
                            $msg = showMsg($resCreateTbl['MSG']);
                        }else{
                            cmDb2Close($db2tblcon);
                            // 作成されたワークテーブル
                            $tmpTblNm = $resCreateTbl['TMPTBLNM'];
                        }
                    }
                    $eend = microtime(true);

                    //始：ロックマスター保存
                    $db2Logcon = cmDb2Con();
                    cmSetPHPQUERY($db2Logcon);
                    $logrs = '';
					$LOGCONDDATA=$SAVEBASECONDDATA;
					foreach($LOGCONDDATA as $key1=> $allcnd){
						$cndsData=$allcnd["CNDSDATA"];
						foreach($cndsData as $key2=>$cnd){
							$CNDFLD=$cnd['CNDFLD'];
							if(preg_match('/^P[0-9]*./i',$CNDFLD,$match1)){
								$sRep="0"."_".str_replace($match1[0],'',$CNDFLD);
								$LOGCONDDATA[$key1]["CNDSDATA"][$key2]['CNDFLD']=fnReplaceData($fdb2csv2,$sRep,"D2FILID","D2FLD","D2HED");							
							}else if(preg_match('/^S[0-9]+./i',$CNDFLD,$match2)){
								$indexKey=preg_match('/[0-9]+/i',$match2[0],$match3);
								$sRep=$match3[0]."_".str_replace($match2[0],'',$CNDFLD);
								$LOGCONDDATA[$key1]["CNDSDATA"][$key2]['CNDFLD']=fnReplaceData($fdb2csv2,$sRep,"D2FILID","D2FLD","D2HED");
							}else if(preg_match('/^K[0-9]*./i',$CNDFLD,$match3)){
								$sRep=$QRYNM."_".str_replace($match3[0],'',$CNDFLD);
								$LOGCONDDATA[$key1]["CNDSDATA"][$key2]['CNDFLD']=fnReplaceData($fdb2csv5,$sRep,"D5NAME","D5FLD","D5HED");
							}
							$LOGCONDDATA[$key1]["CNDSDATA"][$key2]["CNDDATA"]=$cnd["CNDDATAREP"];
						}
					}
                    if(strlen($sql_wlsqlb) > 32730){
                        $sql_wlsqlb = showMsg('INVALID_SQLLENGTH');
                    }
                    $jktime = $eend - $estart;
                    $rs = cmInsertDB2WLOG($db2Logcon, $logrs, $userData[0]['WUUID'], 'P', $QRYNM,$LOGCONDDATA, '1',$sql_wlsqlb);
                    cmInsertDB2WSTL($db2Logcon, $rs['DATE'],$rs['TIME'],$userData[0]['WUUID'],$jktime,$resCreateTbl['QRYCNT']);
                    fnDB2QHISExe($db2Logcon,  $QRYNM, $userData[0]['WUUID']);
                    //クエリー実行時間の最大、最小、平均アップデート
                    $dataHis = fnGetDB2QHIS($db2Logcon,$QRYNM);
                    $max = $dataHis['data'][0]['DQTMAX'];
                    $min = $dataHis['data'][0]['DQTMIN'];
                    if($max !== '.00' && $min !== '.00'){
                        $max = max($max,$jktime);
                        $min = min($min,$jktime);
                    }else{
                        $max = $jktime;
                        $min = $jktime;
                    }
                    cmUpdDB2QHIS($db2Logcon, $QRYNM, $max, $min, ($max + $min)/2); 
                    db2_commit($db2Logcon);
                    //終：ロックマスター保存
                    db2_rollback($db2con);
                }else{
                    //コピーした定義のOPTION選択があればのチェック
                   if($CPYFLG === '1'){
                        if($OPTIONSELDATA !== ''){
                            if(is_array($OPTIONSELDATA) && count($OPTIONSELDATA)>0){
                                $QRYNM = $BASEFILEFRMDATA['D1NAME'];
                                $D1CFLG = '';
                                $QRYNEWNM = $BASEFILEFRMDATA['D1NAMENEW'];
                                $rs = fnCopyOptionData($db2con,$QRYNM,$D1CFLG,$QRYNEWNM,$OPTIONSELDATA,$bqrycndData);
                                if($rs['RTN'] !== 0){
                                    $rtn = 1;
                                    $msg = $rs['MSG'];
                                    db2_rollback($db2con);
                                }else{
                                    $_SESSION['PHPQUERY']['SELLIBLIST'] = $qryData['LIBLIST'];
                                    $_SESSION['PHPQUERY']['SAVRDB']     = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
                                    unset($_SESSION['PHPQUERY']['RDBNM']);
                                    db2_commit($db2con);
                                }
                            }else{
                                $_SESSION['PHPQUERY']['SELLIBLIST'] = $qryData['LIBLIST'];
                                $_SESSION['PHPQUERY']['SAVRDB']     = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
                                unset($_SESSION['PHPQUERY']['RDBNM']);
                                db2_commit($db2con);
                            }
                        }else{
                            $_SESSION['PHPQUERY']['SELLIBLIST'] = $qryData['LIBLIST'];
                            $_SESSION['PHPQUERY']['SAVRDB']     = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
                            unset($_SESSION['PHPQUERY']['RDBNM']);
                            db2_commit($db2con);
                        }
                    }else{
                        $_SESSION['PHPQUERY']['SELLIBLIST'] = $qryData['LIBLIST'];
                        $_SESSION['PHPQUERY']['SAVRDB']     = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
                        unset($_SESSION['PHPQUERY']['RDBNM']);
                        db2_commit($db2con);
                    }
                }
            }
        }
    }
}
//db2_rollback($db2con);

cmDb2Close($db2con);

$data['BASEFILEFRMDATA']    = $BASEFILEDATA;
$data['BASEFIELDFRMDATA']   = $BASEFIELDDATA;
$data['BASECONDFRMDATA']    = $BASECONDDATA;
$data['BASELIBFRMDATA']     = $BASELIBFRMDATA;
$data['SUMMARYFRMDATA']     = $SUMMARYDATA;

$SAVEDDATA = array(
        'BASEFILEDATA'      => $SAVEDBASEFILEDATA,
        'BASEFIELDDATA'     => $SAVEDBASEFIELDDATA,
        'BASECONDDATA'      => $SAVEBASECONDDATA,
        'BASESUMMARYDATA'   => $SAVESUMMARYDATA
);

$tabData =  array(
    'FDB2CSV1'    => $fdb2csv1,
    'BREFTBLFLD'  => $breftblfld,
    'FDB2CSV2'    => $fdb2csv2,
    'FDB2CSV5'    => $fdb2csv5,
    'BSUMFLD'     => $bsumfld,
    'UPDFDB2CSV2' => $updFdb2csv2,
    'UPDFDB2CSV5' => $updFdb2csv5,
    'BQRYCNDDAT'  => $bqrycndData
);


$rtn = array(
    'DATA'     => $data,
    'TABLEDATA'=> $tabData,
    'RTN'      => $rtn,
    'MSG'      => $msg,
    'QRYDATA'  => $qryData,
    'TEMPTBLNM'=> $tmpTblNm,
    'QRYCOLDATA' => umEx($qryColData,true),
    'SAVEDDATA'  => $SAVEDDATA,
    'qryData' => $qryData
);

echo(json_encode($rtn));

/*
*@dataというのは検索したいアレイ
*@sResというのは検索TBからのカラム
*@k1Colというのはアレイの検索キー
*@k2Colというのはアレイの検索キー
*@vColというのはアレイの検索キーの検索結果
*/
function fnReplaceData($data,$sRep,$k1Col,$k2Col,$vCol){
	$result='';
	foreach($data as $value1){
		$splitRes=$value1[$k1Col]."_".$value1[$k2Col];
		if($sRep===$splitRes){
			$result=$value1[$vCol];
			break;
		}
	}
	return $result;
}
