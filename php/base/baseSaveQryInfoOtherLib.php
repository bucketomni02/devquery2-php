<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("generateBaseTableData.php");
include_once("insQryTableData.php");
include_once("createExecuteSQL.php");
include_once("getPrevSaveFrmData.php");
include_once("baseOptionInfoCopy.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$FDB2CSV1=json_decode($_POST['FDB2CSV1'],true);
$D1NAME=  (isset($_POST['D1NAME']))?$_POST['D1NAME']:'';
$D1LIBITMLIBNM= (isset($_POST['D1LIBITMLIBNM']))?$_POST['D1LIBITMLIBNM']:'';
$PROC             = (isset($_POST['PROC']))?$_POST['PROC']:'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$BASEFILEDATA  = array();
$BASEFIELDDATA = array();
$BASECONDDATA  = array();
$BASELIBDATA   = '';
$SUMMARYDATA   = array();
$qryColData    = array();

$data          = array();
$rtn           = 0;
$msg           = '';
$tmpTblNm      = '';

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
if($rtn === 0){
    //DB接続（no exist lib data)
    $db2con = cmDb2Con();
    if($db2con === false){
        $rtn = 1;
        $msg = showMsg('FAIL_DBCON');
    }
   cmSetPHPQUERY($db2con);
}

//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}

 //ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
/**クエリーID＆ライブラリーチェック*/
if($rtn===0){
    if($PROC==='CPY'){
        if($rtn===0){
            if(trim($D1NAME) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('定義名'));
            }
        }
        if($rtn===0){
            if(trim($D1LIBITMLIBNM) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('移行先'));
            }
        }
    }
}

/*クエリーがあるかどうかをチェック**/
if($rtn === 0 ){
    $res = getFDB2CSV1($db2con,$D1NAME,$D1LIBITMLIBNM);
    e_log(print_r($res,true));
    if($res['result'] === 'NOTEXIST_GET' ){
        if($PROC !== 'CPY'){
            $PROC = 'CPY';
        }
    }else if($res['result'] !== true){
        $rtn =1;
        $msg = $res['errcd'];
    }else{
        if($PROC === 'CPY'){
            $rtn =1;
            $msg = showMsg('ISEXIST',array('クエリー'));
        }
    }
}

/*クエリーTB保存**/
if($rtn === 0){
    $resIns = insFDB2CSV1_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
    if($resIns['result'] !== true){
        $rtn =1;
        $msg = $resIns['errcd'];
    }else{
        $rtn =0;
        $msg = 'FDB2CSV1登録完了.';
    }
}
/*JOIN TB保存**/
if($rtn === 0){
    if($FDB2CSV1['D1WEBF']==='1'){
        if($FDB2CSV1['D1CFLG']!=='1'){//普通のクエリー
            $resIns = insBREFTBLFLD_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg = $msg.'参照ファイルデータ登録完了.';
            }
        }else{//SQL
            $resIns= insBSQLDAT_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg = $msg.'BSQLDAT登録完了.';
            }
        }//end of D1CFLG
    }else{//黒い画面
        $resIns=  insFDB2CSV4_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
        if($resIns['result'] !== true){
            $rtn =1;
            $msg = $resIns['errcd'];
        }else{
            $rtn =0;
            $msg =$msg. 'FDB2CSV4登録完了.';
        }
    }//end of D1WEBF
}
if($rtn === 0){
    $resIns = insFDB2CSV2_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
    if($resIns['result'] !== true){
        $rtn =1;
        $msg = $resIns['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'FDB2CSV2登録完了.';
    }
}
if($rtn === 0){
    if($FDB2CSV1['D1WEBF']==='1'){
        if($FDB2CSV1['D1CFLG']!=='1'){//普通のクエリー
            $resIns = insFDB2CSV5_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'FDB2CSV5登録完了.';
            }
        }//end of D1CFLG
    }else{//黒い画面
        $resIns = insFDB2CSV5_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
        if($resIns['result'] !== true){
            $rtn =1;
            $msg = $resIns['errcd'];
        }else{
            $rtn =0;
            $msg =$msg. 'FDB2CSV5登録完了.';
        }
    }//end of D1WEBF
}
if($rtn === 0){
    $resIns = insBSUMFLD_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
    if($resIns['result'] !== true){
        $rtn =1;
        $msg = $resIns['errcd'];
    }else{
        $rtn =0;
        $msg =$msg. 'BSUMFLD登録完了.';
    }
}
if($rtn === 0){
    if($FDB2CSV1['D1WEBF']==='1'){
        if($FDB2CSV1['D1CFLG']!=='1'){//普通のクエリー
            $resIns = insBQRYCNDDAT_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'BQRYCND登録完了.BCNDDAT登録完了.';
            }
        }else{
            $resIns=insBSQLCND_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'BSQLCND登録完了.';
            }
        }//end of D1CFLG
    }else{//黒い画面
        $resIns = insFDB2CSV3_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME);
        if($resIns['result'] !== true){
            $rtn =1;
            $msg = $resIns['errcd'];
        }else{
            $rtn =0;
            $msg =$msg. 'FDB2CSV3登録完了.';
        }
    }//end of D1WEBF
}
//権限のデータ
if($rtn===0){
    $rs = fnCopyOptionData_OTHER($db2con,$D1LIBITMLIBNM,$D1NAME,$FDB2CSV1['D1CFLG']);
    if($rs['RTN'] !== 0){
        $rtn = 1;
        $msg = $rs['MSG'];
    }
}
if($rtn === 0){
    e_log('commit');
    db2_commit($db2con);
}else{
    db2_rollback($db2con);
}

cmDb2Close($db2con);
$rtn = array(
    'RTN'      => $rtn,
    'MSG'      => $msg,
);

echo(json_encode($rtn));
