<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("generateSQLTblData.php");
include_once("insSQLTableData.php");
include_once("createTblExeSql.php");
include_once("baseOptionInfoCopy.php");
include_once("fnDB2QHIS.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$PROC             = $_POST['PROC'];
$PREV             = $_POST['PREV'];
$CPYFLG           = $_POST['CPYFLG'];
$BASESQLFRMDATA   = json_decode($_POST['BASESQLDATA'],true);
$BASECNDFRMDATA   = json_decode($_POST['BASECNDDATA'],true);
$BASEFLDFRMDATA   = json_decode($_POST['BASEFLDDATA'],true);
$OPTIONSELDATA    = json_decode($_POST['OPTIONSELDATA'],true);
$OUTPUTFDATA  = json_decode($_POST['OUTPUTFDATA'],true);//出力ファイルのデータ
$wrktblnm         = '';

$fdb2csv1 = array();
$fdb2csv2 = array();
$bsqlcnd  = array();
$bsqldat  = array();

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$BASESQLDATA  = array();
$BASEFLDDATA  = array();
$BASECNDDATA  = array();

$data          = array();
$rtn           = 0;
$msg           = '';
$tmpTblNm      = '';

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

if($CPYFLG === '1'){
    $QRYNM    = $BASESQLFRMDATA['NEWD1NAME'];
    $PROC     = 'ADD';
}else{
    $QRYNM    = $BASESQLFRMDATA['D1NAME'];
}

$rtnSqlTbl = generateSQLTblData($BASESQLFRMDATA);
$fdb2csv1  = $rtnSqlTbl['FDB2CSV1'];
$bsqldat   = $rtnSqlTbl['BSQLDAT'];
$cntWithParam = count($BASESQLFRMDATA['WITHPARAM']);
$rtnCndTbl = generateCNDTblData($QRYNM,$BASECNDFRMDATA,$cntWithParam);

$bsqlcnd   = $rtnCndTbl['BSQLCND'];
$fdb2csv2  = generateFLDTblData($QRYNM,$BASEFLDFRMDATA);

if($rtn === 0){
    //DB接続
    $db2con = cmDb2Con();
    if($db2con === false){
        $rtn = 1;
        $msg = showMsg('FAIL_DBCON');
    }

    cmSetPHPQUERY($db2con);
}
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}
 //ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
/*
/*クエリー作成時にダウンロード権限を付与(イー)
**/
if($rtn === 0){
    if($CPYFLG === '1'){
        $QRYNM = $BASESQLFRMDATA['NEWD1NAME'];
    }
    if($PROC === 'ADD' && $PREV !== '1'){
		if(CND_QRYKG === '1'){
			$rs = cmGetAllUser($db2con,$QRYNM);
		    if($rs['result'] !== true){
		        $rtn = 1;
		        $msg = $rs['result'];
		    }
			else{
	            foreach($rs['data'] as $userData){
                    $name = cmGetQryNameFromDB2WDEF($db2con,$userData['WUUID'],$QRYNM);
                    if ($name['result'] === 'NOTEXIST_GET') {
                        $rs = cmInsQryKengen($db2con,$userData['WUUID'],$QRYNM,'1','1');
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = $rs['result'];
                            //db2_rollback($db2con);
                            break;
                        }
                    }else{
                        $rs = cmUpdQryKengen($db2con,$userData['WUUID'],$QRYNM,'1','1');
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = $rs['result'];
                            //db2_rollback($db2con);
                            break;
                        }
                    }
				}
                //db2_commit($db2con);
			}
		}
	}
}
if($rtn === 0){
    if($PROC === 'ADD'){
        $resIns = insFDB2CSV1($db2con,$fdb2csv1);
        if($resIns['result'] !== true){
            $rtn =1;
            $msg = $resIns['errcd'];
        }else{
            $rtn =0;
            $msg = 'FDB2CSV1登録完了.';
        }
    }else{
        $resupd = updFDB2CSV1($db2con,$fdb2csv1);
        if($resupd['result'] !== true){
            $rtn =1;
            $msg = $resupd['errcd'];
        }else{
            $rtn =0;
            $msg = $msg.'   FDB2CSV1更新完了.';
        }
    }
}
//出力ファイル更新データFDB2CSV1　updOUTFILEFDB2CSV1
if($rtn === 0){
    if(count($OUTPUTFDATA)>0){
        $ddata=$OUTPUTFDATA[0];
        $resIns = updOUTFILEFDB2CSV1($db2con,$OUTPUTFDATA[0]);
        if($resIns['result'] !== true){
            $rtn =1;
            $msg = $resIns['errcd'];
        }else{
            $rtn =0;
            $msg .= 'FDB2CSV1で出力ファイル更新完了.';
        }
    }
}

if($rtn === 0){
    if($PROC === 'ADD'){
        $resIns = insBSQLDAT($db2con,$bsqldat);
        if($resIns['result'] !== true){
            $rtn =1;
            $msg = $resIns['errcd'];
        }else{
            $rtn =0;
            $msg = $msg.'   BSQLDAT登録完了.';
        }
    }else{
        $resdel = delBSQLDAT($db2con,$QRYNM);
        if($resdel['result'] !== true){
            $rtn =1;
            $msg = $resdel['errcd'];
        }else{
            $rtn =0;
            $msg =$msg. 'BSQLDAT削除完了.';
            //error_log('strsql 11111'.print_r($bsqldat,true));

            $resIns = insBSQLDAT($db2con,$bsqldat);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'BSQLDAT登録完了.';
            }
        }
    }
}
if($rtn === 0){
    if($PROC === 'ADD'){
        $resIns = insFDB2CSV2($db2con,$fdb2csv2);
        if($resIns['result'] !== true){
            $rtn =1;
            $msg = $resIns['errcd'];
        }else{
            $rtn =0;
            $msg = $msg.'   FDB2CSV2登録完了.';
        }
    }else{
        $resdel = delFDB2CSV2($db2con,$QRYNM);
        if($resdel['result'] !== true){
            $rtn =1;
            $msg = $resdel['errcd'];
        }else{
            $rtn =0;
            $msg =$msg. 'FDB2CSV2削除完了.';
            $resIns = insFDB2CSV2($db2con,$fdb2csv2);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'FDB2CSV2登録完了.';
            }
        }
    }
}
if($rtn === 0){
    if($PROC === 'ADD'){
        $resIns = insBSQLCND($db2con,$bsqlcnd);
        if($resIns['result'] !== true){
            $rtn = 1;
            $msg = $resIns['errcd'];
        }else{
            $rtn = 0;
            $msg = $msg.'   BSQLCND登録完了';
        }
    }else{
        $rs = getCanData($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = $rs['result'];
        }else{
            $candata = $rs['data'];
        }
        if($rtn === 0){
            $rs = getDB2HCND($db2con,$QRYNM);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = $rs['result'];
            }else{
                $hcnddata = $rs['data'];
            }
        }
        if($rtn === 0){
            $resdel = delBSQLCND($db2con,$QRYNM);
            if($resdel['result'] !== true){
                $rtn =1;
                $msg = $resdel['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'BSQLCND削除完了.';
            }
        }
        if($rtn === 0){
            $resdel = delDB2HCND($db2con,$QRYNM);
            if($resdel['result'] !== true){
                $rtn =1;
                $msg = $resdel['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'DB2HCND削除完了.';
            }
        }
        if($rtn === 0){
            $rs = cmDeleteDB2SHSD($db2con,$QRYNM);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }else{
                $rtn =0;
                $msg =$msg. 'DB2SHSD削除完了.';
            }
        }
        if($rtn === 0){
            $resIns = insBSQLCND($db2con,$bsqlcnd);
            if($resIns['result'] !== true){
                $rtn =1;
                $msg = $resIns['errcd'];
            }else{
                $rtn =0;
                $msg =$msg. 'BSQLCND登録完了.';
            }
        }
        if($rtn === 0){
            if(count($candata)>0){
                $resupd = updBSQLCND($db2con,$candata);
                if($resupd['result'] !== true){
                    $rtn =1;
                    $msg = $resupd['errcd'];
                }
            }
        }
        if($rtn === 0){
            if(count($hcnddata)>0){
                $resupd = updDB2HCND($db2con,$hcnddata);
                if($resupd['result'] !== true){
                    $rtn =1;
                    $msg = $resupd['errcd'];
                }else{
                    $rtn =0;
                    $msg = '';
                }
            }
        }
    }
}
if($rtn === 0){
    $resSQLInfo = getQrySQLData($db2con,$QRYNM);
    if($resSQLInfo['RTN'] !== 0){
        $rtn = 1;
        $msg = $resSQLInfo['MSG'];
    }else{
        $qryData = $resSQLInfo['QRYDATA'];
        error_log('qrydata = '.print_r($qryData,true));
    }
}
if($rtn === 0){
    //$paramArr = '';
    $paramArr = array();
    $fdb2csv1Info = $qryData['FDB2CSV1'][0];
    $libArr = preg_split ("/\s+/", $fdb2csv1Info['D1LIBL'] );
    $LIBLIST = join(' ',$libArr);
    if($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME){
         $db2LibCon = cmDb2ConLib($LIBLIST);
    } else {
        $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1Info['D1RDB'];
        $db2LibCon = cmDB2ConRDB($LIBLIST);
    }

   // if(count($qryData['BSQLCND']) > 0){
        $paramArr = bindParamArr($qryData['BSQLCND']);
   // }
    $strSQL = $qryData['BSQLEXEDAT']['EXESQL'];
    if(count($paramArr['WITHPARAMDATA'])>0){
        
        $strSQL = bindWithParamSQLQry($strSQL,$paramArr['WITHPARAMDATA']);
    }
	/****DBのフィードがブランクの場合【1=1】に代る*/
	$repArr=array();
    //error_log('IMP PramArr**'.print_r($paramArr,true));
	$repArr=cmReplaceSQLParam($strSQL,$paramArr['PARAMDATA']);

	$strExeSQLParam = bindParamSQLQry($repArr['strSQL'],$repArr['params']);

	$repBindArr = array(
	    'PARAMDATA' => $repArr['params'],
	    'WITHPARAMDATA' => $paramArr['WITHPARAMDATA']
	);
	$rsExeSQL = execSQLQry($db2LibCon,$strExeSQLParam,$repBindArr);
//old	$strExeSQLParam = bindParamSQLQry($strSQL,$paramArr['PARAMDATA']);
//old    $rsExeSQL = execSQLQry($db2LibCon,$strExeSQLParam,$paramArr);
    if($rsExeSQL['RTN'] !== 0){
        $rtn = 1;
        $msg = $rsExeSQL['MSG'];
    }else{
        if($PREV  === '1'){
            $tmpTblName = makeRandStr(10, true);
            $estart = microtime(true);//190405
            $rsCreate = createSQLQryTbl($db2LibCon,$strExeSQLParam,$repArr["params"],$tmpTblName);
            if($rsCreate['RTN'] !== 0){
                $rtn = 1;
                $msg = $rsCreate['MSG'];
            }else{
                $eend = microtime(true);//190405
                $wrktblnm = $rsCreate['TMPTBLNM']; 
                //始：ロックマスター保存
                $db2Logcon = cmDb2Con();
                cmSetPHPQUERY($db2Logcon);
                $logrs = '';
                $sql_wlsqlb = $strExeSQLParam;
                $jktime = $eend - $estart;
                $rs = cmInsertDB2WLOG($db2Logcon, $logrs, $userData[0]['WUUID'], 'P', $qryData['BSQLEXEDAT']['BSQLNM'],$qryData['BSQLCND'], '1',$sql_wlsqlb);
                cmInsertDB2WSTL($db2Logcon, $rs['DATE'],$rs['TIME'],$userData[0]['WUUID'],$jktime ,$rsCreate['QRYCNT']);
                fnDB2QHISExe($db2Logcon,  $qryData['BSQLEXEDAT']['BSQLNM'], $userData[0]['WUUID']);
                //クエリー実行時間の最大、最小、平均アップデート
                $dataHis = fnGetDB2QHIS($db2Logcon,$qryData['BSQLEXEDAT']['BSQLNM']);
                $max = $dataHis['data'][0]['DQTMAX'];
                $min = $dataHis['data'][0]['DQTMIN'];
                if($max !== '.00' && $min !== '.00'){
                    $max = max($max,$jktime);
                    $min = min($min,$jktime);
                }else{
                    $max = $jktime;
                    $min = $jktime;
                }
                cmUpdDB2QHIS($db2Logcon, $qryData['BSQLEXEDAT']['BSQLNM'], $max, $min, ($max + $min)/2); 

                db2_commit($db2Logcon);
                //終：ロックマスター保存
            }
        }
    }
}
cmDb2Close($db2LibCon);
if($rtn === 1){
    e_log('登録失敗:'.$msg);
    db2_rollback($db2con);
}else{
    e_log('登録完了しました。');
    if($PREV  === '1'){
        db2_rollback($db2con);
    }else{
        db2_commit($db2con);
    }
}
if($rtn === 0){
    //コピーした定義のOPTION選択があればのチェック
    if($CPYFLG === '1'){
        if($OPTIONSELDATA !== ''){
            if(is_array($OPTIONSELDATA) && count($OPTIONSELDATA)>0){
                $QRYNM = $BASESQLFRMDATA['D1NAME'];
                $QRYNEWNM = $BASESQLFRMDATA['NEWD1NAME'];
                $D1CFLG = '1';
                $rs = fnCopyOptionData($db2con,$QRYNM,$D1CFLG,$QRYNEWNM,$OPTIONSELDATA,$bsqlcnd);
                if($rs['RTN'] !== 0){
                    $rtn = 1;
                    $msg = $rs['MSG'];
                    db2_rollback($db2con);
                }else{
                    $_SESSION['PHPQUERY']['SELLIBLIST'] = $LIBLIST;
                    $_SESSION['PHPQUERY']['SAVRDB']     = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
                    unset($_SESSION['PHPQUERY']['RDBNM']);
                    db2_commit($db2con);
                }
            }else{
                $_SESSION['PHPQUERY']['SELLIBLIST'] = $LIBLIST;
                $_SESSION['PHPQUERY']['SAVRDB']     = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
                unset($_SESSION['PHPQUERY']['RDBNM']);
                db2_commit($db2con);
            }
        }else{
            $_SESSION['PHPQUERY']['SELLIBLIST'] = $LIBLIST;
            $_SESSION['PHPQUERY']['SAVRDB']     = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
            unset($_SESSION['PHPQUERY']['RDBNM']);
            db2_commit($db2con);
        }
    }else{
        $_SESSION['PHPQUERY']['SELLIBLIST'] = $LIBLIST;
        $_SESSION['PHPQUERY']['SAVRDB']     = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        unset($_SESSION['PHPQUERY']['RDBNM']);
        db2_commit($db2con);
    }
}

cmDb2Close($db2con);
$tabData =  array(
    'FDB2CSV1'  => $fdb2csv1,
    'FDB2CSV2'  => $fdb2csv2,
    'BSQLDAT'   => $bsqldat,
    'BSQLCND'   => $bsqlcnd
);
$formData = array(
    'SQLFRMDATA' => $BASESQLFRMDATA,
    'CNDFRMDATA' => $BASECNDFRMDATA,
    'FLDFRMDATA' => $BASEFLDFRMDATA
);
$rtn = array(
    'FORMDATA'  => $formData,
    'TABLEDATA' => $tabData,
    'RTN'       => $rtn,
    'MSG'       => $msg,
    'WRKTBLNM'  => $wrktblnm,
    'strExeSQLParam' => $strExeSQLParam,
    'strSQL' => $strSQL,
    's' => $qryData
);

echo(json_encode($rtn));

