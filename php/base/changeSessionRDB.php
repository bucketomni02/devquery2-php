<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("chkRDB.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$RDBNM = (isset($_POST['RDBNM'])?$_POST['RDBNM']:'');

$rtn = chkRDB($RDBNM);

echo(json_encode($rtn));
