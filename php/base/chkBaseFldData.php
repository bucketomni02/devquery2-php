<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 
 * PROGRAM ID     : chkBaseFldData.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/04/11
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$basefldData = json_decode($_POST['BASEFIELDDATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/

$rtn = 0;
$msg = '';
$row = '';
$column = '';
$fileid = '';

if($rtn === 0){
    for($fldCnt = 0; $fldCnt<count($basefldData); $fldCnt++){
        $fldVal = $basefldData[$fldCnt];
        if($rtn === 0){
            $rexp = '/^[A-Z#@]{1}[0-9A-Z#@_$]*$/';
            if (preg_match($rexp, $fldVal['COLUMN_NAME']) == false) {
                $rtn = 1;
                $msg = showMsg('FAIL_VLDD',array('フィールド',array('一文字目','：','(A-Z, #,','または','@)','<br>','二文字目から','：','(A-Z,0-9, _ , #,','または','@)')));
                $row = $fldCnt;
                $column = 0;
                break;
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($fldVal['LENGTH'],5)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('長さ'));
                $row = $fldCnt;
                $column = 1;
                break;
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($fldVal['COLUMN_HEADING'],60)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('カラムヘディング'));
                $row = $fldCnt;
                $column = 4;
                break;
            }
        }
    }
}

$rtn = array(
    'BASEFIELDDATA' => $basefldData,
    'RTN'           => $rtn,
    'MSG'           => $msg,
    'ROW'           => $row,
    'COL'           => $column
);
echo(json_encode($rtn));
