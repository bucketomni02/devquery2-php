<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("createTblExeSql.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$baseSQLData    = json_decode($_POST['BASESQLDATA'],true);
$baseParamData  = json_decode($_POST['BASEPARAMDATA'],true);
$carr  = json_decode($_POST['CARR'],true);//DBのフィードがコメントあるかどうかのアレイ
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount   = 0;
$data       = array();
$rtn        = 0;
$msg        = '';
$focus      = '';
$tmpTblName = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
$paramDataArr   = array();
$wParamDataArr  = array();
$cParamDataArr  = array();
$wcParamDataArr = array();
$seqArr         = array();
$seqLst         = array();
$seqIdxArr      = array();
e_log("PPPPPPPPPP : ".print_r($baseParamData,true));
e_log("QQQQQQQQQQ : ".print_r($baseSQLData,true));
if($rtn === 0){
    foreach($baseParamData as $key => $paramData){
        $idx = $key+1;
        if($rtn === 0){
            if($paramData['TXTPRMSEQ'.$idx] === ''){
                $rtn    = 1;
                $msg    = showMsg('FAIL_REQ',array('SEQ'));
                $focus  = 'TXTPRMSEQ'.$idx;
                break;
            }
        }
        if($rtn === 0){
            if($paramData['TXTPRMSEQ'.$idx] == 0 ){
                $rtn    = 1;
                $msg    = 'SEQの値を「1」以上入力してください';
                $focus  = 'TXTPRMSEQ'.$idx;
                break;
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($paramData['TXTPRMSEQ'.$idx],3)){
                $rtn    = 1;
                $msg    = showMsg('FAIL_MAXLEN',array('SEQ'));
                $focus  = 'TXTPRMSEQ'.$idx;
                break;
            }
        }
        if($rtn === 0){
            if(checkNuturalNum($paramData['TXTPRMSEQ'.$idx]) == false){
                $rtn    = 1;
                $msg    = showMsg('CHK_FMT',array('SEQ','数値'));
                $focus  = 'TXTPRMSEQ'.$idx;
                break;
            }
        }
        if($rtn === 0){
            $seqArr[]= ($paramData['TXTPRMSEQ'.$idx]);
        }
        if($rtn === 0){
            if($paramData['TXTPARMNM'.$idx] === ''){
                $rtn    = 1;
                $msg    = showMsg('FAIL_REQ',array('パラメータ名'));
                $focus  = 'TXTPARMNM'.$idx;
                break;
            }

        }
        if($rtn === 0){
            if(!checkMaxLen($paramData['TXTPARMNM'.$idx],60)){
                $rtn    = 1;
                $msg    = showMsg('FAIL_MAXLEN',array('パラメータ名'));
                $focus  = 'TXTPARMNM'.$idx;
                break;
            }
        }
        if($rtn === 0){
            if($paramData['CMBDATATYPE'.$idx] === ''){
                $rtn    = 1;
                $msg    = showMsg('FAIL_REQ',array('データ型'));
                $focus  = 'CMBDATATYPE'.$idx;
                break;
            }
        }
        
        if($rtn === 0){
			//任意の場合はパランクでもOk
			/*$isAny=($paramData['TXTPARAMDATA'.$idx] === '' && $paramData['CMBSELTYPE'.$idx] == '2')?true:false;*/
            if($paramData['TXTPARAMDATA'.$idx] === '' && ($paramData['CMBSELTYPE'.$idx] !== '2' && $paramData['CMBSELTYPE'.$idx] !== '1')){
                $rtn    = 1;
                $msg    = showMsg('FAIL_REQ',array('パラメータデータ'));
                $focus  = 'TXTPARAMDATA'.$idx;
                break;
            }else{                
                if($idx > count($baseSQLData['WITHPARAM'])){
                    //array_push($paramDataArr,$paramData['TXTPARAMDATA'.$idx]);
                    $paramDataArr[$paramData['TXTPRMSEQ'.$idx]] = $paramData['TXTPARAMDATA'.$idx];
                }else{
                    array_push($wParamDataArr,array($baseSQLData['WITHPARAM'][$idx-1]=>$paramData['TXTPARAMDATA'.$idx]));
                }
                if ($paramData['CMBDATATYPE'.$idx] === '6' && $paramData['TXTPARAMDATA'.$idx] === ''){
                    $rtn    = 1;
                    $msg    = showMsg('FAIL_REQ',array('パラメータデータ'));
                    $focus  = 'TXTPARAMDATA'.$idx;
                    break;
                }
                switch($paramData['CMBDATATYPE'.$idx]){
                    case '1':
                        if($idx > count($baseSQLData['WITHPARAM'])){
                            array_push($seqIdxArr,$paramData['TXTPRMSEQ'.$idx]);
                            if ($paramData['TXTPARAMDATA'.$idx] == '') {
                                if($paramData['CMBSELTYPE'.$idx] === '1'){
                                    $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] =  '\'\''; 
                                }else{
                                    $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = "";                                
                                }
                            }else{
                                $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = '\''.$paramData['TXTPARAMDATA'.$idx].'\'';
                            }
                        }else{
                            if ($paramData['TXTPARAMDATA'.$idx] == '') {
                                if($paramData['CMBSELTYPE'.$idx] === '1'){
                                    $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = '\'\'';
                                }else{
                                    $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = "";
                                }
                            }else{
                                $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = '\''.$paramData['TXTPARAMDATA'.$idx].'\'';
                            }
                        }
                        break;
                    case '2':
                        if(checkNum($paramData['TXTPARAMDATA'.$idx]) === false/* && !$isAny */ ){
                            $msg = showMsg('CHK_FMT',array('【数値】データ型のパラメータデータ','数値'));
                            $focus = 'TXTPARAMDATA'.$idx;
                            $rtn = 1;
                            break;
                        }else{
                            if($idx > count($baseSQLData['WITHPARAM'])){
                                array_push($seqIdxArr,$paramData['TXTPRMSEQ'.$idx]);
                                if ($paramData['TXTPARAMDATA'.$idx] == '' && $paramData['CMBSELTYPE'.$idx] === '1') {
                                    $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = 0;
                                }else{
                                    $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = $paramData['TXTPARAMDATA'.$idx];
                                }
                            }else{
                                if ($paramData['TXTPARAMDATA'.$idx] == '' && $paramData['CMBSELTYPE'.$idx] === '1') {
                                    $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = 0;
                                }else{
                                    $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = $paramData['TXTPARAMDATA'.$idx];
                                }
                            }
                        }
                        break;
                    case '3':
                        $chkRes = chkVldDate($paramData['TXTPARAMDATA'.$idx]);
                        if($chkRes == 1 /*&& !$isAny*/){
                            $msg = '日付フォマットが正しくないです。<br>「yyyy/mm/dd」、「yyyy-mm-dd」、又「yyyymmdd」のフォッマとで入力してください。';
                            $focus = 'TXTPARAMDATA'.$idx;
                            $rtn = 1;
                            break;
                        }else{
                            if($idx > count($baseSQLData['WITHPARAM'])){
                                array_push($seqIdxArr,$paramData['TXTPRMSEQ'.$idx]);
                                if ($paramData['TXTPARAMDATA'.$idx] == '') {
                                if($paramData['CMBSELTYPE'.$idx] === '1'){
                                        $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = '\''."0000/00/00".'\'';
                                    }else{
                                        $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = "";                                
                                    }
                                }else{
                                    $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = '\''.$paramData['TXTPARAMDATA'.$idx].'\'';
                                }
                            }else{
                                if ($paramData['TXTPARAMDATA'.$idx] == '') {
                                    if($paramData['CMBSELTYPE'.$idx] === '1'){
                                        $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = '\''.'0000/00/00'.'\'';
                                    }else{
                                        $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = "";
                                    }
                                }else{
                                    $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = '\''.$paramData['TXTPARAMDATA'.$idx].'\'';
                                }
                            }
                        }
                        break;
                    case '4':
                        $chkRes = chkVldTime($paramData['TXTPARAMDATA'.$idx]);
                        if($chkRes == 1 /*&& !$isAny*/){
                            $msg = '時刻フォマットが正しくないです。<br>「HH.MM.SS」又「HH:MM:SS」のフォッマとで入力してください。';
                            $focus = 'TXTPARAMDATA'.$idx;
                            $rtn = 1;
                            break;
                        }else{
                            if($idx > count($baseSQLData['WITHPARAM'])){
                                array_push($seqIdxArr,$paramData['TXTPRMSEQ'.$idx]);
                                if ($paramData['TXTPARAMDATA'.$idx] == '') {
                                    if($paramData['CMBSELTYPE'.$idx] === '1'){
                                        $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = '\''.'00.00.00'.'\'';
                                    }else{
                                        $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = "";                                
                                    }
                                }else{
                                    $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = '\''.$paramData['TXTPARAMDATA'.$idx].'\'';
                                }
                            }else{
                                if ($paramData['TXTPARAMDATA'.$idx] == '') {
                                    if($paramData['CMBSELTYPE'.$idx] === '1'){
                                        $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = '\''.'00.00.00'.'\'';
                                    }else{
                                        $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = "";
                                    }
                                }else{
                                    $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = '\''.$paramData['TXTPARAMDATA'.$idx].'\'';
                                }
                            }
                        }
                        break;
                    case '5':
                        $chkRes = chkVldTimeStamp($paramData['TXTPARAMDATA'.$idx]);
                        if($chkRes == 1 /*&& !$isAny*/){
                            $msg = 'タイムスタンプフォマットが正しくないです。<br>「yyyy-mm-dd-HH.MM.SS.NNNNNN」又「yyyy-mm-dd HH.MM.SS」のフォッマとで入力してください。';
                            $focus = 'TXTPARAMDATA'.$idx;
                            $rtn = 1;
                            break;
                        }else{
                            if($idx > count($baseSQLData['WITHPARAM'])){
                                array_push($seqIdxArr,$paramData['TXTPRMSEQ'.$idx]);
                                if ($paramData['TXTPARAMDATA'.$idx] == '') {
                                    if($paramData['CMBSELTYPE'.$idx] === '1'){
                                        $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = '\''.'0000-00-00-00.00.00.000000'.'\'';
                                    }else{
                                        $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = "";                                
                                    }
                                }else{
                                    $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = '\''.$paramData['TXTPARAMDATA'.$idx].'\'';
                                }
                            }else{
                                if ($paramData['TXTPARAMDATA'.$idx] == '') {
                                    if($paramData['CMBSELTYPE'.$idx] === '1'){
                                        $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = '\''.'0000-00-00-00.00.00.000000'.'\'';
                                    }else{
                                        $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = "";
                                    }
                                }else{
                                    $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = '\''.$paramData['TXTPARAMDATA'.$idx].'\'';
                                }
                            }
                        }
                        break;
                    case '6':
                        if($idx > count($baseSQLData['WITHPARAM'])){
                            //array_push($cParamDataArr,'\''.$paramData['TXTPARAMDATA'.$idx].'\'');
                            array_push($seqIdxArr,$paramData['TXTPRMSEQ'.$idx]);
                            $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = $paramData['TXTPARAMDATA'.$idx];
                        }else{
                            $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = $paramData['TXTPARAMDATA'.$idx];
                        }
                        break;
                    default :
                        if($idx > count($baseSQLData['WITHPARAM'])){
                            //array_push($cParamDataArr,'\''.$paramData['TXTPARAMDATA'.$idx].'\'');
                            array_push($seqIdxArr,$paramData['TXTPRMSEQ'.$idx]);
                            $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = $paramData['TXTPARAMDATA'.$idx];
                        }else{
                            $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = $paramData['TXTPARAMDATA'.$idx];
                        }
                        break;
                }
            }
            if($rtn === 0){
                if(!checkMaxLen($paramData['TXTPARAMDATA'.$idx],128)){
                    $rtn    = 1;
                    $msg    = showMsg('FAIL_MAXLEN',array('パラメータデータ'));
                    $focus  = 'TXTPARAMDATA'.$idx;
                    break;
                }
            }
        }
        // ADD
        if($rtn === 0){
            if($paramData['CMBLIKETYPE'.$idx] !== ''){
                if($paramData['CMBDATATYPE'.$idx] !== '1'){
                    $rtn    = 1;
                    //$msg = showMsg('FAIL_REQ',array('LIKE'));
                    $focus  = 'CMBDATATYPE'.$idx;
                    $msg = "データ型の選択は正しくないです。<br />条件を選ぶ場合はデータ型に文字を選択してください。";
                    break;
                }else{
                    switch($paramData['CMBLIKETYPE'.$idx]){
                    case 7:
                        if(count($cParamDataArr) > 0){
                             if($idx === count($cParamDataArr)){
                                 $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = "'%".$paramData['TXTPARAMDATA'.$idx]."%'";
                             }
                        }
                        if(count($wcParamDataArr) > 0){
                             if($idx < count($baseSQLData['WITHPARAM'])){
                                 $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = "'%".$paramData['TXTPARAMDATA'.$idx]."%'";
                             }
                        }
                        break;
                    case 8:
                        if(count($cParamDataArr) > 0){
                             if($idx === count($cParamDataArr)){
                                 $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = "'".$paramData['TXTPARAMDATA'.$idx]."%'";
                             }
                        }
                        if(count($wcParamDataArr) > 0){
                             if($idx < count($baseSQLData['WITHPARAM'])){
                                 $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = "'".$paramData['TXTPARAMDATA'.$idx]."%'";
                             }
                        }
                        break;
                    case 9:
                        if(count($cParamDataArr) > 0){
                             if($idx === count($cParamDataArr)){
                                 $cParamDataArr[$paramData['TXTPRMSEQ'.$idx]] = "'%".$paramData['TXTPARAMDATA'.$idx]."'";
                             }
                        }
                        if(count($wcParamDataArr) > 0){
                             if($idx < count($baseSQLData['WITHPARAM'])){
                                 $wcParamDataArr[$baseSQLData['WITHPARAM'][$idx-1]] = "'%".$paramData['TXTPARAMDATA'.$idx]."'";
                             }
                        }
                        break;
                    }
                }
            }
        }
    }
}
if($rtn === 0){
    if(count(array_unique($seqArr)) < count($seqArr)){
        $rtn = 1;
        $msg = showMsg('CHK_DUP',array('SEQ'));
    }
}
if($rtn === 0 ){
    $AryData = array();
    $AryBaseData = array();
    foreach($baseParamData as $key => $paramData){
        $idx = $key+1;
        $AryData = $paramData['CMBDATATYPE'.$idx];
        array_push($AryBaseData,$AryData);
        
    }

    $withParam = '';
    $strSQL = $baseSQLData['EXESQL'];
    if(count($baseSQLData['WITHPARAM']) > 0){
        $withParam = $wcParamDataArr;
        $strSQL = bindWithParamSQLQry($strSQL,$withParam);
    }else{
        $withParam = array();
    }

    $cparams = array();
    $params  = array();
    sort($seqIdxArr,SORT_NUMERIC );
    foreach($seqIdxArr as $seq){
        array_push($cparams,$cParamDataArr[$seq]);
        array_push($params,$paramDataArr[$seq]);
    }
	/*DBのフィードがブランクの場合【1=1】に代る*/
	e_log("BEFORE REPLACE=>".$strSQL,print_r($cparams,true));
	$repArr = array();
	$repArr = cmReplaceSQLParam($strSQL,$cparams);

	$strSQL=$repArr["strSQL"];
	e_log("AFTER REPLACE=>".$strSQL,print_r($cparams,true));

    $strSQL = bindParamSQLQry($strSQL,$repArr["params"],$AryBaseData);
	error_log("strSQLにparameterを代る事".$strSQL);
    // パラメータ無しにする
   	$params = array();

    if($baseSQLData['RDBNM'] === '' || $baseSQLData['RDBNM'] === RDB || $baseSQLData['RDBNM'] === RDBNAME ){
        $db2con = cmDb2ConLib(join(' ',$baseSQLData['SELLIBLIST']));
    }else{
        $_SESSION['PHPQUERY']['RDBNM'] = $baseSQLData['RDBNM'];
        $db2con = cmDb2ConRDB(join(' ',$baseSQLData['SELLIBLIST']));
    }
    e_log('実行SQLbind後【log】：'.$strSQL.'パラメータ：'.print_r($cparams,true));
    $rsExeSQL = execSQLQry($db2con,$strSQL,$params);
    if($rsExeSQL['RTN'] !== 0){
        $rtn = 1;
        $msg = $rsExeSQL['MSG'];
    }else{
        $tmpTblName = makeRandStr(10, true);
        if(count($baseSQLData['WITHPARAM'])>0){
            $strExeSQLWithparam = bindWithParamSQLQry($baseSQLData['EXESQL'],$withParam);
           e_log('WITH 実行SQLbind前【log】：'.$strExeSQLWithparam.'パラメータ：'.print_r($cparams,true));
			/*DBのフィードがブランクの場合【1=1】に代る*/
			$repArr=array();
			$repArr=cmReplaceSQLParam($strExeSQLWithparam,$cparams);
            $strExeSQLParam = bindParamSQLQry($repArr["strSQL"],$repArr["params"],$AryBaseData);

         //$strExeSQLParam = bindParamSQLQry($strExeSQLWithparam,$cparams);
          e_log('WITH 実行SQLbind後【log】：'.$strExeSQLParam.'パラメータ：'.print_r($cparams,true));

        }else{
           e_log('実行SQLbind前【log】：'.$strExeSQLParam.'パラメータ：'.print_r($cparams,true));
			/*DBのフィードがブランクの場合【1=1】に代る*/
			$repArr=array();
			$repArr=cmReplaceSQLParam($baseSQLData['EXESQL'],$cparams);
            $strExeSQLParam = bindParamSQLQry($repArr["strSQL"],$repArr["params"],$AryBaseData);
            e_log('実行SQLbind後【log】：'.$strExeSQLParam.'パラメータ：'.print_r($cparams,true));
        }
       error_log('実行SQLbind before create：'.$strExeSQLParam);
        $rsCreate = createSQLQryTbl($db2con,$strExeSQLParam,$strExeSQLParam,$tmpTblName,'1');
        if($rsCreate['RTN'] !== 0){
            $rtn = 1;
            $msg = $rsCreate['MSG'];
        }else{
            $bsqlcnd = createBaseParamData($baseParamData);
        }
    }
    cmDb2Close($db2con);
}
/**return**/
$rtn = array(
    'BASESQLDATA'   => $baseSQLData,
    'BASEPARAMDATA' => $baseParamData,
    'BSQLCND'       => $bsqlcnd,
    'PARAMDATA'     => $cParamDataArr,
    'CPARAM'        => $cparams,
    'WITHPARAMDATA' => $wcParamDataArr,
    'TMPTBLNM'      => $tmpTblName,
    'RTN'           => $rtn,
    'MSG'           => $msg,
    'FOCUS'         => $focus,
    'SQL'           => $strExeSQLParam,
	'testReplace'   => $repArr
);

echo(json_encode($rtn));
/*
 *-------------------------------------------------------* 
 * データベースに保存するためにパラメータ配列
 *-------------------------------------------------------*
 */
function createBaseParamData($baseParamData){
    $bsqlcnd = array();
    if(count($baseParamData)>0){
        foreach($baseParamData as $key => $paramData){
            $pData = array();
            $idx = $key+1;
            $pData['CNDSEQ']    = $paramData['TXTPRMSEQ'.$idx];
            $pData['CNDNM']     = $paramData['TXTPARMNM'.$idx];
            $pData['CNDWNM']    = $paramData['TXTWPARMNM'.$idx];
            $pData['CNDDTYP']   = $paramData['CMBDATATYPE'.$idx];
            $pData['CNDSTKB']   = $paramData['CMBSELTYPE'.$idx];
            $pData['CNCKBN']    = $paramData['CMBLIKETYPE'.$idx];
            $pData['CNDDAT']    = $paramData['TXTPARAMDATA'.$idx];
            if(cmMer($pData['CNDDAT']) === ''){
                $pData['CNDBCNT'] = strlen($pData['CNDDAT']);
            }else{
                $pData['CNDBCNT'] = 0;
            }
            $bsqlcnd[] = $pData;
        }
    }
    return $bsqlcnd;
}
