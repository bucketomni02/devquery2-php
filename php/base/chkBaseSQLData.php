<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$BASESQLDATA    = json_decode($_POST['BASESQLDATA'],true);
$PROC           = $_POST['PROC'];
$CPYFLG         = (isset($_POST['CPYFLG']))?$_POST['CPYFLG']: '';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$focus = '';
//コメントあるかどうかのチェック
$CARR=array();
$keyCount=0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$SELLIBLIST = $BASESQLDATA['SELLIBLIST'];
$NEWD1NAME  = (isset($BASESQLDATA['NEWD1NAME']))?$BASESQLDATA['NEWD1NAME']:'';
$RDBNM      = $BASESQLDATA['RDBNM'] ? $BASESQLDATA['RDBNM'] : RDB;
$D1NAME     = cmHscDe($BASESQLDATA['D1NAME']);
$D1TEXT     = cmHscDe($BASESQLDATA['D1TEXT']);
$D1SQL      = cmHscDe($BASESQLDATA['D1SQL']);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
 //ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
// 定義名チェック
if($rtn === 0){
    if($PROC === 'ADD'){
        $rs = chkD1NAME($db2con,$D1NAME);
        if($rs !== 0){
            $rtn = 1;
            $msg = showMsg($rs,array('定義名'));
            $focus = 'D1NAME';
        }
    }else{
        $rs = fnChkExistD1NAME($db2con,$D1NAME);
        if($rs !== 'ISEXIST'){
            if($rs === true){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('クエリー'));
            }else{
                $rtn = 1;
                $msg = showMsg($rs,array('定義名'));
                $focus = 'D1NAME';
            }
        }
    }
}

if($rtn === 0){
	if($CPYFLG === '1'){
	    $rs = chkD1NAME($db2con,$NEWD1NAME);
	    if($rs !== 0){
	        $rtn = 1;
	        $msg = showMsg($rs,array('定義名'));
	        $focus = 'D1NAME';
	    }
	}
}
// 記述チェック
if($rtn === 0){
    if($D1TEXT === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('記述'));
        $focus = 'D1TEXT';
    }
}
if($rtn === 0){
    if(cmMer($D1TEXT) === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_SPACECHK',array('記述'));
        $focus = 'D1TEXT';
    }
}
if($rtn === 0){
    if(!checkMaxLen($D1TEXT,50)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('記述'));
        $focus = 'D1TEXT';
    }
}
// SQL文チェック
if($rtn === 0){
    if($D1SQL === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('SQL文'));
        $focus = 'D1SQL';
    }
}
if($rtn === 0){
    if(cmMer($D1SQL) === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_SPACECHK',array('SQL文'));
        $focus = 'D1SQL';
    }
}
if($rtn === 0){
    $chkD1SQL = cmMer($D1SQL);
    $chkD1SQL = preg_replace( "/\r|\n|\rn|\t/", " ", trim($chkD1SQL));
    $chkD1SQLArr = explode(' ',$chkD1SQL);
    //e_log('【削除】SQL文ARR'.print_r($chkD1SQLArr,true));
    $SQLKBN = strtoupper($chkD1SQLArr[0]);
    if(($SQLKBN === 'SELECT' || $SQLKBN === 'WITH') === false){
        $rtn = 1;
        $msg = showMsg('SQL文に【SELECT】SQLだけ入力してください。');
        $focus = 'D1SQL';
    }
    unset($chkD1SQLArr);
}
if($rtn === 0){
    error_log('str length == '.strlen($D1SQL));
    if(!checkMaxLen($D1SQL,32730)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('SQL文'));
        $focus = 'D1SQL';
    }
}

if($rtn === 0){
    $with_param = array();
    if($SQLKBN === 'WITH'){
        // WITH句にパラメータ変数あるかのチェック
        $res_AS = '';
        $res_VAL = '';
        preg_match('/^WITH [a-zA-Z0-9_(),]+\s*AS|as$/i', trim($D1SQL), $with_str);
        e_log('【削除】pregmatchresult'.print_r($with_str,true).'</br>');
        $withStr = $with_str[0];
        e_log('【削除】str:'.$withStr.strlen($withStr).'AAAA');
        $isParam = strpos( cmMer($D1SQL) , '{',strlen($withStr));
        e_log('<br>【削除】paramcheck:'.$isParam === false ? 'false':$isParam);
        if($isParam !== false){
            $isValues = stripos ( cmMer($D1SQL) ,'VALUES',strlen($str1));
            if($isValues !== false){
                $start_AS = stripos ( $withStr ,'(') === false ? -1 : (stripos ( $withStr ,'(')+1);
                $end_AS = stripos ( $withStr ,')') === false ? -1:stripos ( $withStr ,')');
                if($start_AS >= 0 && $end_AS >= 0){
                    $res_AS = subStr($withStr,$start_AS,$end_AS-($start_AS));
                    e_log('【削除】AS RESULT:'.$res_AS);
                }
                $strValues = substr(cmMer($D1SQL),$isValues);
                $start_Values = stripos($strValues,'(') === false ? -1: (stripos($strValues,'(')+1);
                $end_Values = stripos($strValues,')',$start_Values);
                if($start_Values >= 0 && $end_VALUES >= 0){
                	 e_log('【削除】AS RESULT1:'.$strValues.'START'.$start_Values.'END'.$end_VALUES);
                    //$res_VAL = subStr($strValues,$start_Values+1,$end_Values-($start_Values+1));
                    $res_VAL = subStr($strValues,$start_Values,$end_Values-($start_Values));
                    e_log('【削除】VALUES RESULT:'.$res_VAL);
                }
            }
            if($res_AS !== '' && $res_VAL !== ''){
                $arr_AS = explode( ',', $res_AS );
                $arr_VAL = explode( ',', $res_VAL );
                if(count($arr_AS) === count($arr_VAL)){
                    foreach($arr_AS as $key => $val){
                        if(substr(trim($arr_VAL[$key]),0,1) === '{' && substr(trim($arr_VAL[$key]), -1) === '}'){
                            if('{'.$val.'}' === trim($arr_VAL[$key])){
                                $with_param[] = trim($arr_VAL[$key]);
								$keyCount++;
								$CARR[$keyCount]=0;
                            }
                        }
                    }
                    e_log('</br>【削除】WITH PARM:'.print_r($with_param,true));
                }else{
                    e_log('</br>【削除】エラー'.'WITH句のフィールドと値のカウントは合わない');
                }
            }
        }
    }
}

/*コメントがあったらpositionをもらう事**/
/*$commentTest=array();
$commentArr=array();
if($rtn===0){
	if(preg_match_all("/\/[*]{2}\//i",$D1SQL,$matches,PREG_OFFSET_CAPTURE)){
		$commentTest=$matches[0];
		for($c=0;$c<count($commentTest);$c++){
			if($c%2===0){
				$commentPosition=$commentTest[$c][1];
				$commentArr[]=$commentPosition;
			}
		}
	}
}*/
if($rtn === 0){    
    $paramCnt   = substr_count($D1SQL, '?');
    $needle     = "'";
    $lastPos    = 0;
    $positions  = array();
    $strCnt     = 0;
    $dataPosArr = array();
    $qpos       = 0;
    while (($qpos = strpos($D1SQL, '?', $qpos))!== false) {
        $dataPosArr[] = $qpos;
        $qpos = $qpos + strlen('?');
    }
    while (($lastPos = strpos($D1SQL, $needle, $lastPos))!== false) {
        $positions[] = $lastPos;
        $lastPos = $lastPos + strlen($needle);
        if(count($positions)%2 === 0){
            $startpos = $positions[count($positions)-2];
            $endpos = $positions[count($positions)-1];
            $pos = $startpos+1;
            $charcnt = $positions[count($positions)-1]-$pos;
            $strData = subStr($D1SQL,$pos,$charcnt);
            if(strpos($strData,"?")!== false){
                foreach($dataPosArr as $k => $val){
                    if($val >= $startpos && $val <= $endpos){
                        unset($dataPosArr[$k]);
                        $dataPosArr = array_values($dataPosArr);
                    }
                }
                $strCnt ++;
            }
        }
    }
    $sPos = 0;
    $sqlArr = array();
	$bFlg=false;
	$commentCount=0;
    foreach($dataPosArr as $key => $posVal){
        $sqlArr[] = subStr($D1SQL,$sPos,$posVal-$sPos).' {'.($key+1).'} ';
        $sPos = $posVal+1;
    }
    if($sPos < strlen($D1SQL)){
        $sqlArr[] =  subStr($D1SQL,$sPos);
    }
    $exeSQL = join (' ',$sqlArr);
    $paramCnt = $paramCnt-$strCnt;
}
e_log("任意があるかどうかのFLGをもらう=>".$exeSQL);
/**任意があるかどうかのFLGをもらう*/
if($rtn===0){
	//get Field Column Array in query
	$testQryColArr=cmQueryFieldArr($exeSQL);
	$getQryColArr=$testQryColArr["getQryColArr"];
	//get Field Column Condition Flg in query
	$testArr=chkQueryFieldCondition($getQryColArr,$CARR);
	$CARR=$testArr["CARR"];
	for($i=count($with_param);$i<(count($with_param)+count($with_param));$i++){
		unset($CARR[$i]);
	}
	$CARR =array_combine(range(1, count($CARR)),array_values($CARR)); 
}


if($rtn === 0){
    $data['RDBNM']      = $RDBNM;
    $data['SELLIBLIST'] = $SELLIBLIST;
    $data['D1NAME']     = $D1NAME;
	$data['NEWD1NAME']  = $NEWD1NAME;
    $data['D1TEXT']     = $D1TEXT;
    $data['D1SQL']      = $D1SQL;
    $data['EXESQL']     = $exeSQL;
    $data['WITHPARAM']  = $with_param;
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'BASESQLDATA' => $data,
    'PARAMCNT' => $paramCnt,
    'WITHPARAM' => $with_param,
    'RTN' => $rtn,
    'MSG' => $msg,
    'FOCUS' => $focus,
    'POSITION'=> $positions,
    'dataposition' => $dataPosArr,
    'SQLARR'=> $sqlArr,
    'EXESQL' => $exeSQL,
	'CARR'=>$CARR

);

echo(json_encode($rtn));


function chkD1NAME($db2con,$D1NAME){
    $rtn = 0;
    if($D1NAME === ''){
        $rtn = 'FAIL_REQ';
    }
    if($rtn === 0){
        if(!checkMaxLen($D1NAME,10)){
            $rtn = 'FAIL_MAXLEN';
        }
    }
    if($rtn === 0){
        if (preg_match('/[^0-9A-Z_#@]+/', $D1NAME)) {
            $rtn = 'FAIL_VLDC';
        }
    }
    if($rtn === 0){
        $rs = fnChkExistD1NAME($db2con,$D1NAME);
        if($rs !== true){
            $rtn = $rs;
        }
    }
    
    return $rtn;
}

function fnChkExistD1NAME($db2con,$D1NAME){

    $data = array();
    $rs = true;
    
    $strSQL  = ' SELECT A.D1NAME ';
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;
    $strSQL .= ' WHERE A.D1NAME = ? ' ;

    $params = array(
        $D1NAME
    );
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}
