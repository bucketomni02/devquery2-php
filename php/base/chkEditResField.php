<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("chkResField.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$RESFLDDATA     = json_decode($_POST['RESFLDDATA'],true);
$BASEFILEINFO     = json_decode($_POST['BASEFILEINFO'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
if(count($RESFLDDATA)>0){
    $data = chkValidResFld($RESFLDDATA[0],$BASEFILEINFO,'');
}    

cmDb2Close($db2con);


echo(json_encode($data));

