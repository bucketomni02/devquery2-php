<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$DCNAME = $_POST['DCNAME'];
$COLMDATA = json_decode($_POST['COLMDATA'],true);
$COLTDATA = json_decode($_POST['COLTDATA'],true);


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$DCNAME = cmHscDe($DCNAME);


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'14',$userData[0]['WUSAUT']);//'14' =>制御レベル設定
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('制御レベル設定の権限'));
            }
        }
    }
}

/*if($rtn === 0){
    if($COLMDATA['LABEL1'] === '' &&
       $COLMDATA['LABEL2'] === '' &&
       $COLMDATA['LABEL3'] === '' &&
       $COLMDATA['LABEL4'] === '' &&
       $COLMDATA['LABEL5'] === '' &&
       $COLMDATA['LABEL6'] === '' ){
           $rtn = 1;
           $COLMBflg = '1';
           $msg = showMsg('FAIL_SET',array('制御レベル'));
    }
}*/

if($rtn === 0){
    //集計コンボがブランクになってチェックボックスがチェックされている場合
    $comboFlg = false;
    for ($x = 0; $x < count($COLTDATA); $x += 7) {
       if($COLTDATA[$x+1] !== '' && $COLTDATA[$x+1] !== null){
            $comboFlg = true;
       }
    }        
    if($rtn === 0){
        if($comboFlg === false){
            $rtn = 0;
            $COLTBflg = '1';
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'COLMBFLG' => $COLMBflg,
    'COLTBFLG' => $COLTBflg,
    'COLMDATA' => $COLMDATA,
    'COLTDATA' => $COLTDATA
);

echo(json_encode($rtnAry));

