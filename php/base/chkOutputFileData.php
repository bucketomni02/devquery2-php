<?php

/*
*-------------------------------------------------------* 
* 出力ファイル
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("getChkOutputFileData.php");//出力ファイル
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$OUTPUTFDATA     = json_decode($_POST['OUTPUTFDATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
$flg = 0;
$LIBSNM='';
$LIBSDB='';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
 //ログインユーザが削除されたかどうかチェック
/*if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}*/
//ログインユーザが削除されたかどうかチェック $userData[0]['WUUID']
if($rtn === 0){
    $rs = cmCheckUser($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
    }
}
//始ライブラリー条件
if($rtn === 0){
    $LIBSNM = cmMer($userData[0]['WULIBL']);
}
$RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:RDB);
if($RDBNM !== ''){
    $LIBSDB = $RDBNM;
}else{
    $LIBSDB = RDB;
}
if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
    $db2RDBOutputFileCon = cmDB2ConRDB(SAVE_DB);
}
//終ライブラリー条件
if($rtn===0){
    if(count($OUTPUTFDATA)>0){
        $D1OUTLIB=strtoupper(cmMer($OUTPUTFDATA[0]['D1OUTLIB']));
        $D1OUTFIL=strtoupper(cmMer($OUTPUTFDATA[0]['D1OUTFIL']));
        $D1OUTR=cmMer($OUTPUTFDATA[0]['D1OUTR']);
        $D1OUTJ=cmMer($OUTPUTFDATA[0]['D1OUTJ']);
        $D1NAME=cmMer($OUTPUTFDATA[0]['D1NAME']);
        $CPYFLG=cmMer($OUTPUTFDATA[0]['CPYFLG']);
        //ライブラリー
        if($rtn===0){
            if($D1OUTLIB===''){
                $msg = showMsg('FAIL_REQ',array('ライブラリー'));
                $rtn = 1;
            }
        }
        if($rtn===0){
            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                $rs = chkLIBOutput($db2con,$D1OUTLIB,$LIBSNM,$LIBSDB);
            }else{
                $rs = chkLIBOutput($db2RDBOutputFileCon,$D1OUTLIB,$LIBSNM,$LIBSDB);
            }
            if($rs !== 0){
                $rtn = 1;
                $msg = showMsg($rs,array('ライブラリー'));
            }
        }
        //ファイル
        if($rtn===0){
            if($D1OUTFIL===''){
                $msg = showMsg('FAIL_REQ',array('ファイル'));
                $rtn = 1;
            }else if(preg_match('/^[0-9]/', $D1OUTFIL)){
                $msg = showMsg('FAIL_NAME',array('ファイル'));
                $rtn = 1;
            }
        }
        if($rtn===0){
            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                $rs = chkFileOutput($db2con,$D1OUTFIL);
            }else{
                $rs = chkFileOutput($db2RDBOutputFileCon,$D1OUTFIL);
            }
            if($rs['result'] !== 0){
                $rtn = 1;
                $msg = showMsg($rs['result'],array('ファイル'));
            }
        }
        if($rtn===0){
            //ライブラリーとファイルをSYSからチェック
            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                $r=fnChkExistFileOutput($db2con,$D1OUTLIB,$D1OUTFIL);
            }else{
                $r=fnChkExistFileOutput($db2RDBOutputFileCon,$D1OUTLIB,$D1OUTFIL);
            }
            if($r['result']!==true){
                $rtn=$r['rtn'];
                $msg= ($rtn===4)?showMsg($r['result'],array($D1OUTLIB,$D1OUTFIL)):
                            showMsg($r['result']);
            }
        }
        if($rtn===0){
            //出力ファイルのライブラリーとファイルがFDB2CSV1であるかどうか
            $r=fnExistFileFDB2CSV1($db2con,$D1NAME,$D1OUTLIB,$D1OUTFIL);
            if($r['result'] !==true){
                $rtn=1;
                $msg=showMsg($r['result'],array('ファイル'));
            }else{
                $isAdd=($r['TOTCOUNT']>0)?false:true;
                $r=fnChkD1OUTFIL($db2con,$D1OUTLIB,$D1OUTFIL);
                if($r['result']!==true){
                    $rtn=1;
                    $msg= showMsg($r,array('ファイル'));
                }else{
                    $isFlg=false;
                    if($isAdd){
                        $isFlg=($r['TOTCOUNT']>0)?true:false;
                    }else{
                        $isFlg=($r['TOTCOUNT']>1)?true:false;
                    }
                    if($isFlg){
                        $rtn=4;//メッセージです
                        $msg= showMsg('ISEXIST_OUTC',array($D1OUTLIB,$D1OUTFIL));
                    }
                }
            }
        }
    }
}
if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
    cmDb2Close($db2RDBOutputFileCon);
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN'         => $rtn,
    'MSG'         => $msg
);
echo(json_encode($rtn));
