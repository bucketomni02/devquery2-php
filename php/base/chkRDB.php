<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
function chkRDB($RDBNM){
    $rtn = 0;
    $msg = '';

    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);

    //if($RDBNM === RDB || $RDBNM === RDBNAME){
    if($RDBNM === RDB){
        unset($_SESSION['PHPQUERY']['RDBNM']);
        //e_log('RDBdata'.print_r($_SESSION['PHPQUERY'],true));
    }else{
        if($rtn === 0){
            $rs = fnGetDB2RDB($db2con,$RDBNM);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result'],array('区画'));
            }else{            
                $RDBDATA = $rs['data'];
                $rs = chkDBCon($RDBDATA);
                if($rs !== true){
                    $rtn = 2;
                    $msg = $rs;
                }else{
                    // セッション区画
                    $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
                    //e_log('AfterRDBdata'.print_r($_SESSION['PHPQUERY'],true));
                }
            }
        }
    }

    cmDb2Close($db2con);
    /**return**/
    $rtn = array(
        'RTN'       => $rtn,
        'MSG'       => $msg
    );

    return($rtn);
}
/*
*-------------------------------------------------------* 
* 区画マスター取得
*-------------------------------------------------------*
*/

function fnGetDB2RDB($db2con,$RDBNM){

    $data = array();

    $params = array();

    $rs = true;
    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SQL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SQL';
        }
    }
    if($rs === true)
    {
        $strSQL .= ' SELECT ';
        $strSQL .= '     RDBNM, ';
        $strSQL .= '     RDBUSR, ';
        $strSQL .= '     DECRYPT_CHAR(RDBPASS) AS RDBPASS ';
        $strSQL .= ' FROM ';
        $strSQL .= '     DB2RDB ';
        $strSQL .= ' WHERE RDBNM = ? ';
        $params = array($RDBNM);
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                if(count($data) === 0){
                    $data = array('result' => 'NOTEXIST_GET');
                }else{
                    $data = array('result' => true,'data' => $data[0]);
                }
            }
        }
    }
    return $data;
}

function chkDBCon($RDBDATA){
    $rs = true;
    $con = db2_connect(cmMer($RDBDATA['RDBNM']), cmMer($RDBDATA['RDBUSR']), cmMer($RDBDATA['RDBPASS']));
    if($con){
        db2_close($con);
    }else{
        $rs = showMsg('FAIL_FUNC',array('区画の接続'));//'区画の接続は失敗しました';
    }
    return $rs;
}
