<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../common/inc/common.validation.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$RESFLDDATA     = json_decode($_POST['RESFLDDATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/

$data = array();
$rtn = 0;
$msg = '';
$col = '';
$focus = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
 * 結果フィールドのバリデーションチェックファションを呼び出し
 */


function callchkValidResFld($RESFLDDATA,$BASEFILEINFO,$PROC){
    foreach ($RESFLDDATA as $key => $RESFLD) {
        $res = chkValidResFld($RESFLD,$BASEFILEINFO,$PROC,false);
        if($res['RTN'] !== 0){
            $res['ROW'] = $key;
            break;
        }
    }
    return $res;
}

/*
 * 結果フィールドのバリデーションチェック
 */
function chkValidResFld($RESFLD,$BASEFILEINFO,$PROC,$RFLD=true){

    $FORMULA        = cmHscDe($RESFLD['FORMULA']);
    $FORMULA_D      = cmHscDe($RESFLD['FORMULA_D']);
    $LENGTH         = cmHscDe($RESFLD['LENGTH']); 
    $NUMERIC_SCALE  = cmHscDe($RESFLD['NUMERIC_SCALE']);
    $DDS_TYPE       = cmHscDe($RESFLD['DDS_TYPE']);
    $COLUMN_NAME    = cmHscDe($RESFLD['COLUMN_NAME']);
    $COLUMN_HEADING = cmHscDe($RESFLD['COLUMN_HEADING']);
    //e_log("RESFLD MSMoe => ".print_r($RESFLD,true));
    $rtn = 0;
    $col = '';
    //フィールド名チェック
    if($rtn === 0){
        $exp = array();
        $exp['NAME']    = 'フィールド';
        $exp['MAXLEN']  = 10;
        $exp['REXP']    = '/^[A-Z#@]{1}[0-9A-Z#@_]*$/';
        $exp['PMSG'] = array('一文字目','：','(A-Z, #,','または','@)','<br>','二文字目から','：','(A-Z,0-9, _ , #,','または','@)');
        $rs = comValidate($COLUMN_NAME,$exp,true,true,true,false,$RFLD);
        if($rs['RTN'] !== 0){
            $rtn = $rs['RTN'];
            $msg = $rs['MSG'];
            $focus = 'COLUMN_NAME';
            $rtn = 1;
        }
    }
    if($PROC === ''){
        // 式チェック
        if($rtn === 0){
            $exp = array();
            $exp['NAME']    = '式';
            $exp['MAXLEN']  = 1024;
            $FORMULA_D = str_replace(array("\r\n", "\r", "\n"), '', $FORMULA_D);//MSM change to FORMULA_D
            $rs = comValidate($FORMULA_D,$exp,true,false,true,false,false);
            //e_log("comValidate FORMULA MOOE =>".$FORMULA.print_r($rs,true));
            if($rs['RTN'] !== 0){
                $rtn = $rs['RTN'];
                $msg = $rs['MSG'];
                $focus = 'FORMULA';
               
            }else{
                if($DDS_TYPE === 'P' || $DDS_TYPE === 'B' || $DDS_TYPE == 'S'){
                    $FORMULA_D = 'TRUNC('.$FORMULA_D.','.$NUMERIC_SCALE.')';//MSM change to FORMULA_D
                }

                $res = chkValidFormula($FORMULA_D,$BASEFILEINFO);//

                if($res !== true){
                    //e_log("BASEFILEINFO['D1NAME'] => ".$BASEFILEINFO['D1NAME']);
                    /* MSM check for new added field name validation  */
                   /*if($BASEFILEINFO['D1NAME'] !='' ){
                        $rs_csv5 = chkValidFormula_new($FORMULA_D,$BASEFILEINFO['D1NAME']);//MSM add
                        if(count($rs_csv5['data']) === 0){
                            $rtn = 1;
                            $msg = $res;
                            $focus = 'FORMULA';
                        }else{
                            $csv5data =$rs_csv5['data'];
                            $D5EXPD = $csv5data[0]['D5EXPD'];
                            $str= strpbrk($FORMULA_D, '+|-|*|/');
                            if($str != ""){
                                $chkFORMULA = $D5EXPD.$str;
                            }else{
                                $chkFORMULA = $D5EXPD;
                            }
                            $chkD5EXP = chkValidFormula($chkFORMULA,$BASEFILEINFO);
                            //e_log("csv5data TRUE MSMoe=> ".$chkFORMULA.print_r($chkD5EXP,true));
                            if($chkD5EXP !== true){
                                $rtn = 1;
                                $msg = $res;
                                $focus = 'FORMULA';
                            }
                        }
                    }else{*/
                        $rtn = 1;
                        $msg = $res;
                        $focus = 'FORMULA';
                    //}   //MSM
                }
            }
        }
    }
    //カラムヘディングチェック
    if($rtn === 0){
        $exp = array();
        $exp['NAME']    = 'カラムヘディング';
        $exp['MAXLEN']  = 60;
        $rs = comValidate($COLUMN_HEADING,$exp,false,false,true,false,false);
        if($rs['RTN'] !== 0){
            $rtn = $rs['RTN'];
            $msg = $rs['MSG'];
            $col = '5';
            $focus = 'COLUMN_HEADING';
        }
    }
    //長さチェック
    if($rtn === 0){
        $exp = array();
        $exp['NAME']    = '長さ';
        $exp['MAXLEN']  = 5;
        $exp['PMSG']    = '数値';
		error_log('gettype = '.gettype($LENGTH));
        $rs = comValidate($LENGTH,$exp,true,true,true,true,false);
        if($rs['RTN'] !== 0){
            $rtn = $rs['RTN'];
            $msg = $rs['MSG'];
            $focus = 'LENGTH';
            $col = '2';
        }
    }
    //タイプチェック
    if($rtn === 0){
        $exp = array();
        $exp['NAME']    = 'タイプ';
        $exp['MAXLEN']  = 1;
        $rs = comValidate($DDS_TYPE,$exp,true,true,true,false,false);
        if($rs['RTN'] !== 0){
            $rtn = $rs['RTN'];
            $msg = $rs['MSG'];
            $focus = 'DDS_TYPE';
            $col = '3';
        }
    }
    //スケールチェック
    if($rtn === 0){
        if($DDS_TYPE === 'S' || $DDS_TYPE === 'P' || $DDS_TYPE === 'B'){
            $exp['NAME']    = 'スケール';
            $exp['MAXLEN']  = 2;
            $exp['PMSG']  = '数値';
            $rs = comValidate($NUMERIC_SCALE,$exp,false,true,true,true,false);
            if($rs['RTN'] !== 0){
                $rtn = $rs['RTN'];
                $msg = $rs['MSG'];
                $focus = 'NUMERIC_SCALE';
            }else{
                if( $NUMERIC_SCALE > $LENGTH ){
                    $rtn = 1;
                    $msg = showMsg('FAIL_SCLLENCHK');
                    $focus = 'NUMERIC_SCALE';
                }
            }
        }
    }
    
    if($PROC !== ''){
        $SEQ    = cmHscDe($RESFLD['SEQ']);
        $SORTNO = cmHscDe($RESFLD['SORTNO']);
        //SEQチェック
        if($SEQ !== ''){
            if($rtn === 0){
                $exp['NAME']    = 'SEQ';
                $exp['MAXLEN']  = 4;
                $exp['PMSG']  = '数値';
                $rs = comValidate($SEQ,$exp,false,true,true,true,false);
                if($rs['RTN'] !== 0){
                    $rtn = $rs['RTN'];
                    $msg = $rs['MSG'];
                    $col = '0';
                }
            }
        }
        if($SORTNO !== ''){
            //ソート順チェック
            if($rtn === 0){
                $exp['NAME']    = 'SORT順';
                $exp['MAXLEN']  = 3;
                $exp['PMSG']  = '数値';
                $rs = comValidate($SORTNO,$exp,false,true,true,true,false);
                if($rs['RTN'] !== 0){
                    $rtn = $rs['RTN'];
                    $msg = $rs['MSG'];
                    $col = '6';
                }
            }
        }
    }
    
    $rtnArr = array(
        'RTN' => $rtn,
        'MSG' => $msg,
        'COL' => $col,
        'FOCUS' => $focus
        
    );
    return  $rtnArr;
    
}
/*
 * ファイル選択画面のプライマリと参照ファイル情報取得
 */
function getsqlFile($BASEFILEINFO){
    //e_log('BASEFILEINFO DATA MSM=> '.print_r($BASEFILEINFO,true));
    $filedata = array();
    $filedata['LIBLST'] = join(' ',$BASEFILEINFO['SELLIBLIST']);
    $filedata['PLBL'] = $BASEFILEINFO['D1FILLIB'];
    $filedata['PTAB'] = $BASEFILEINFO['D1FILE'];
    $filedata['RCNT'] = count($BASEFILEINFO['REFFILE']);
    for($rFilCnt = 1; $rFilCnt<=$filedata['RCNT'];$rFilCnt++){
        $refFil = $BASEFILEINFO['REFFILE'][$rFilCnt-1];
        $filedata['RIDX'.$rFilCnt] = $refFil['REFIDX'];
        $filedata['SLIB'.$rFilCnt] = $refFil['RTRLIB'];
        $filedata['STAB'.$rFilCnt] = $refFil['RTRFIL'];
        $filedata['JTYP'.$rFilCnt] = $refFil['JOINTYP'];
        $jkeycnt = 0;
        foreach ($refFil['REFFIELDARR'] as $key => $value){
            if($value !== ''){
                if(substr($key,0,5) === 'JCOND'){
                    $filedata['JCOND'.$rFilCnt._.substr($key, 5)] = $value;
                }else{
                    $jkeycnt = substr($key, 4);
                    if($key[0] === 'P'){
                        if(substr($key,1,3) === 'KEY'){
                            $filedata['PFLD'.$rFilCnt._.$jkeycnt] = $value;
                        }else if(substr($key,1,3) === 'LEN'){
                            $filedata['PLEN'.$rFilCnt._.$jkeycnt] = $value;
                        }else if(substr($key,1,3) === 'TYP'){
                            $filedata['PTYP'.$rFilCnt._.$jkeycnt] = $value;
                        }
                        
                    }else if($key[0] === 'R'){
                        if(substr($key,1,3) === 'KEY'){
                            $filedata['RFLD'.$rFilCnt._.$jkeycnt] = $value;
                        }else if(substr($key,1,3) === 'LEN'){
                            $filedata['RLEN'.$rFilCnt._.$jkeycnt] = $value;
                        }else if(substr($key,1,3) === 'TYP'){
                            $filedata['RTYP'.$rFilCnt._.$jkeycnt] = $value;
                        }
                    }
                }
            }
        }
        $filedata['KCNT'.$rFilCnt] = $jkeycnt;
    }
    return $filedata;
}
/*
 *  式のバリデーションチェック
 */
function chkValidFormula($FORMULA,$BASEFILEINFO){
    global $BASEFILEINFO;
    $RDBNM = $BASEFILEINFO['RDBNM'];
    $strSQL = '';
    $data = array();
    $fileInfo = getsqlFile($BASEFILEINFO);
    $rs = true;
    $strSQL .= ' SELECT  ('.$FORMULA.') AS RESFLD';
    $strSQL .= '    FROM  ';
    if($fileInfo['PLBL'] === '*USRLIBL' || $fileInfo['PLBL'] === '*LIBL'){
        $strSQL .= $fileInfo['PTAB'] . ' AS P ';
    }else{
        $strSQL .= $fileInfo['PLBL'].'/'.$fileInfo['PTAB'] . ' AS P ';
    }
    if($fileInfo['RCNT']>0){
        for($rcnt = 1;$rcnt <= $fileInfo['RCNT']; $rcnt++){
            if($fileInfo['JTYP'.$rcnt] == 1){
                $strSQL .= '   JOIN   ';
            }else if($fileInfo['JTYP'.$rcnt] == 2){
                $strSQL .= '   LEFT JOIN   ';
            }else if($fileInfo['JTYP'.$rcnt] == 3){
                $strSQL .= '   RIGHT JOIN   ';
            }else if($fileInfo['JTYP'.$rcnt] == 4){
                $strSQL .= '   LEFT JOIN   ';
            }else if($fileInfo['JTYP'.$rcnt] == 5){
                $strSQL .= '   RIGHT JOIN   ';
            }else if($fileInfo['JTYP'.$rcnt] == 6){
                $strSQL .= '   JOIN   ';
            }else if($fileInfo['JTYP'.$rcnt] == 7){
                $strSQL .= '   FULL JOIN   ';
            }else if($fileInfo['JTYP'.$rcnt] == 8){
                $strSQL .= '   FULL JOIN   ';
            }else if($fileInfo['JTYP'.$rcnt] == 9){
                $strSQL .= '   CROSS JOIN   ';
            }
            if($fileInfo['SLIB'.$rcnt] === '*USRLIBL' || $fileInfo['SLIB'.$rcnt] === '*LIBL'){
                $strSQL .= '    '.$fileInfo['STAB'.$rcnt];
            }else{
                $strSQL .= '    '.$fileInfo['SLIB'.$rcnt].'/'.$fileInfo['STAB'.$rcnt];
            }
            $strSQL .= '    AS S'.$fileInfo['RIDX'.$rcnt] ;
            for($kcnt=1;$kcnt<=$fileInfo['KCNT'.$rcnt]; $kcnt++){
                if($kcnt === 1){
                    $strSQL .= '    ON    ';
                }else{
                    $strSQL .= '    AND   ';
                }
                //check for datatype and cast for length
                if($fileInfo['PTYP'.$rcnt._.$kcnt] === $fileInfo['RTYP'.$rcnt._.$kcnt]){
                    $reffnm = $fileInfo['RFLD'.$rcnt._.$kcnt];
                }else{
                    if($fileInfo['PTYP'.$rcnt._.$kcnt] === 'P' || $fileInfo['PTYP'.$rcnt._.$kcnt] === 'S' || $fileInfo['PTYP'.$rcnt._.$kcnt] === 'B'){
                        if($fileInfo['RTYP'.$rcnt._.$kcnt] === 'P' || $fileInfo['RTYP'.$rcnt._.$kcnt] === 'S' || $fileInfo['RTYP'.$rcnt._.$kcnt] === 'B'){
                            $reffnm = $fileInfo['RFLD'.$rcnt._.$kcnt];
                        }else{
                            $reffnm = 'CAST('.$fileInfo['RFLD'.$rcnt._.$kcnt].' AS DECIMAL('.$fileInfo['RLEN'.$rcnt._.$kcnt].'))';
                        }
                    }else{
                        if($fileInfo['RTYP'.$rcnt._.$kcnt] === 'P' || $fileInfo['RTYP'.$rcnt._.$kcnt] === 'S' || $fileInfo['RTYP'.$rcnt._.$kcnt] === 'B'){
                            $reffnm = 'CAST('.$fileInfo['RFLD'.$rcnt._.$kcnt].' AS CHAR('.$fileInfo['RLEN'.$rcnt._.$kcnt].'))';
                        }else{
                            $reffnm = $fileInfo['RFLD'.$rcnt._.$kcnt];
                        }
                    }
                }
                switch($fileInfo['JCOND'.$rcnt._.$kcnt]){
                    case 'EQ':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    =    '.$reffnm;
                        break;
                    case 'NE':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    <>    '.$reffnm;
                        break;
                    case 'GE':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    >=    '.$reffnm;
                        break;
                    case 'GT':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    <    '.$reffnm;
                        break;
                    case 'LE':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    <=    '.$reffnm;
                        break;
                    case 'LT':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    <    '.$reffnm;
                        break;
                    case 'LIKE':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    LIKE '.'concat(concat(\'%\','.$reffnm.'),\'%\')';
                        break;
                    case 'LLIKE':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    LIKE '.'concat(\'%\','.$reffnm.')';
                        break;
                    case 'RLIKE':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    LIKE '.'concat('.$reffnm.',\'%\')';
                        break;
                    case 'NLIKE':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    NOT LIKE '.'concat(concat(\'%\','.$reffnm.'),\'%\')';
                        break;
                    case 'NLLIKE':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    NOT LIKE '.'concat(\'%\','.$reffnm.')';
                        break;
                    case 'NRLIKE':
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    NOT LIKE '.'concat('.$reffnm.',\'%\')';
                        break;
                    default:
                        $strSQL .= $fileInfo['PFLD'.$rcnt._.$kcnt] .'    =    '.$reffnm;
                        break;
                }
            }
        }
    }

    if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME ){
        $db2con = cmDb2ConLib($fileInfo['LIBLST']);
    }else{
        $db2con = cmDb2ConRDB($fileInfo['LIBLST']);
    }
    e_log('結果SQL KEKKA MSMoe'.$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = showMsg('FAIL_CHK',array('式'));
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = showMsg('FAIL_CHK',array('式'));
        }else{
            $row =  db2_fetch_assoc($stmt);
            if($row === false && db2_stmt_errormsg() !== '' ){
                $rs = showMsg('FAIL_CHK',array('式'));
            }
            $type = db2_field_type($stmt,'RESFLD');
            switch ($type) {
                case 'date':
                    $rs = 'フィールドのデータタイプ【日付】は結果フィールドに使用できません。';
                    break;
                case 'time':
                    $rs = 'フィールドのデータタイプ【時刻】は結果フィールドに使用できません。';
                    break;
                case 'timestamp':
                    $rs = 'フィールドのデータタイプ【タイムスタンプ】は結果フィールドに使用できません。';
                    break;
            }
        }
    }
    cmDb2Close($db2con);
    return $rs;
}

function chkValidFormula_new($FORMULA,$D1NAME){//MSM add for new field name 2018 jun12
    if (strpos($FORMULA, 'P.') !== false || strpos($FORMULA, 'K.') !== false ) {
        $FORMULA = substr($FORMULA,2);
    }else if(preg_match('/S[0-9.]{2}/', $FORMULA, $matches)){
        //print_r($matches[0]);
        $FORMULA = substr($FORMULA,3);
    }else{
        $FORMULA = $FORMULA;
    }
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $data = array();
    $params = array();
    $rs = true;

    $strSQL  = '';
    $strSQL .= ' SELECT * ';
    $strSQL .= '    FROM FDB2CSV5 ';
    $strSQL .= '      WHERE D5FLD = ?';
    $strSQL .= '      AND D5NAME = ?';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = showMsg('FAIL_CHK',array('式'));
    }else{
        $str= strpbrk($FORMULA, '+|-|*|/');
        if($str != ""){
            $D5FLD = strstr($FORMULA, $str, true);
            array_push($params, $D5FLD);
        }else{
            $D5FLD = $FORMULA;
            array_push($params, $D5FLD);
        }
        array_push($params, $D1NAME);
        
        //e_log("chkValidFormula_new MSMoe => ".$strSQL.print_r($params,true));
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = showMsg('FAIL_CHK',array('式'));

        }else{
            $rs = true;
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data); 
           $csv5 = array('result' => $rs,'data' => $data);
        }
    }

    cmDb2Close($db2con);
    return $csv5; 
}
