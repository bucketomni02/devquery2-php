<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$SUMMARYDATA     = json_decode($_POST['SUMMARYDATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
$flg = 0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$sumKeyInfo = $SUMMARYDATA['SUMKEYDATA'];
$groupingInfo = $SUMMARYDATA['GROUPINGDATA'];

$sCnt = 0;
$gCnt = 0;
$sumKeydata = array();
$sumKeyVal = array();
$groupingKeyData = array();
$groupingKeyVal = array();
if($rtn === 0){
    foreach($sumKeyInfo as $sumKey){
        if($sumKey['FLDVAL'] !== ''){
            $sumKeydata[$sCnt]=array();
            $sumKeyData[$sCnt]['FLDVAL'] = $sumKey['FLDVAL'];
            $sumKeyData[$sCnt]['FILID'] = $sumKey['FILID'];
            $sumKeyData[$sCnt]['FILTYP'] = $sumKey['FILTYP'];
            $sumKeyData[$sCnt]['FLDNM'] = $sumKey['FLDNM'];
            $sumKeyData[$sCnt]['ORPOS'] = $sumKey['ORPOS'];
            $sumKeyData[$sCnt]['GSEQ'] = $sCnt+1;
            $sumKeyVal[$sCnt] = $sumKey['FLDVAL'];
            $sCnt = $sumKeyData[$sCnt]['GSEQ'];
        }
    }
    if(count(array_unique($sumKeyVal)) < count($sumKeyVal)){
        $rtn = 1;
        $msg = showMsg('CHK_DUP',array('サマリーキー'));
    }
}
if($rtn === 0){
    foreach($groupingInfo as $grouping){
        if($grouping['FLDVAL'] !== ''){
            if($grouping['GMES'] !== ''){
                $groupKeyData[$gCnt]=array();
                $groupKeyData[$gCnt]['FLDVAL'] = $grouping['FLDVAL'];
                $groupKeyData[$gCnt]['FILID']  = $grouping['FILID'];
                $groupKeyData[$gCnt]['FILTYP'] = $grouping['FILTYP'];
                $groupKeyData[$gCnt]['FLDNM']  = $grouping['FLDNM'];
                $groupKeyData[$gCnt]['ORPOS']  = $grouping['ORPOS'];
                $groupKeyData[$gCnt]['GMES']   = $grouping['GMES'];
                $groupKeyData[$gCnt]['GSEQ']   = $gCnt+1;
                $groupingKeyVal[$gCnt]         = $grouping['FLDVAL'];
                $gCnt = $groupKeyData[$gCnt]['GSEQ'];
            }else{
                $msg = showMsg('FAIL_SLEC',array('集計方法'));
                $rtn = 1;
                break;
            }
        }
    }
    if(count(array_unique($groupingKeyVal)) < count($groupingKeyVal)){
        $rtn = 1;
        $msg = showMsg('CHK_DUP',array('集計フィールド'));
    }
}
if($rtn === 0){
    if($sCnt === 0 && $gCnt !== 0){
        $msg = showMsg('FAIL_RELY_REQ',array('集計フィールド','サマリーキー'));
        $rtn = 1;
    }
}
if($rtn === 0){
    if($sCnt !== 0 && $gCnt === 0){
        $msg = showMsg('FAIL_RELY_REQ',array('サマリーキー','集計フィールド'));
        $rtn = 1;
    }
}
if($rtn === 0){
    if($sCnt !== 0 && $gCnt === 0){
        $msg = showMsg('FAIL_RELY_REQ',array('サマリーキー','集計フィールド'));
        $rtn = 1;
    }
}
if($rtn === 0){
    $keyArr = array_merge($sumKeyVal,$groupingKeyVal);
    if(count(array_unique($keyArr))< ($sCnt+$gCnt)){
        $msg = showMsg('CHK_DUP',array(array('サマリーキー','集計フィールド')));//showMsg('サマリーキーと集計フィールドが重複しています');
        $rtn = 1;
    }
}
cmDb2Close($db2con);
if($sumKeyData === null || $groupKeyData === null){
    $data = '';
}else{
    $data['SUMKEYDATA'] = $sumKeyData;
    $data['GROUPINGDATA'] = $groupKeyData;
}

/**return**/
$rtn = array(
    'NODATAFLG'   => $flg,
    'SUMMARYDATA' => $data,
    'RTN'         => $rtn,
    'MSG'         => $msg
);
echo(json_encode($rtn));
