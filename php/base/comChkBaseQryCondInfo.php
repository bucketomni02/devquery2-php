<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 
 * PROGRAM ID     : comChkBaseQryCondInfo.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/04/11
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$BASECONDDATA = json_decode($_POST['BASECONDDATA'],true);
error_log("BASECONDDATA++++".print_r($BASECONDDATA,true));
$PROC         =  $_POST['PROC'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/

$rtn = 0;
$data = array();
$msg = '';
$focus = '';
$idx   = '';
$CONDDATA = array();
if($rtn === 0){
    $cndDataInfo = $BASECONDDATA['CNDDATAINFO'];
    e_log("BASECONDDATACNDDATAINFO++++".print_r($BASECONDDATA['CNDDATAINFO'],true));
    $mQryID = 0;
    foreach($cndDataInfo as $mkey => $cndData){
        e_log("cndDatacndDatacndData++++".print_r($cndData,true));
        $cndFldData = $cndData['CNDFLDDATA'];
        $sQryID = 0;
        foreach($cndFldData as $cndfldKey => $cndFld){
            //フィールドチェック
            if($rtn===0){
                    if(cmMer($cndFld['CNDFLD']) !== '' && cmMer($cndFld['CNDFLD']) !== null){
                            // 処理なし
                            if(cmMer($cndFld['CNDFLDTYP']) === '' && cmMer($cndFld['CNDFLDNM']) === ''){
                                $msg = showMsg('FAIL_BASE_FUNCFLG',array('指定したフィールド情報'));//'指定したフィールド情報が取得できません';
                                $focus ='CMBFIELD'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                $idx = $cndData['CNDIDX'];
                                $rtn = 1;
                                break;
                            }
                    }
            }
            if($rtn===0){
                if (cmMer($cndFld['CNDFLD']) !== ''){
                    if($sQryID === 0 ){
                        $mQryID = $mQryID +1;
                        $sQryID = 1;
                        if(cmMer($cndFld['CNDANDOR']) !== ""){
                            $msg = showMsg('FAIL_BASE_NOADDTYPE',array('最初の副条件項目'));//'最初の副条件項目にはAND/ORを入力できません。';
                            $focus = 'CMBANDOR'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                            $idx = $cndData['CNDIDX'];
                            $rtn = 1;
                        }
                        if($rtn === 0){
                            if($mQryID === 1){
                                if($cndData['SQRYAO'] !== ''){
                                    $msg = showMsg('FAIL_BASE_NOADDTYPE',array('最初の主条件'));//'最初の主条件にはAND/ORを入力できません。';
                                    $focus = 'SQRYKBN'.$cndData['CNDIDX'];
                                    $idx = $cndData['CNDIDX'];
                                    $rtn = 1;
                                }
                            }else{
                                if($cndData['SQRYAO'] === ''){
                                    $msg = showMsg('FAIL_BASE_ADDTYPE',array('主条件'));//'主条件にAND/ORを入力してください。';
                                    $focus = 'SQRYKBN'.$cndData['CNDIDX'];
                                    $idx = $cndData['CNDIDX'];
                                    $rtn = 1;
                                }
                            }
                        }
                    }else{
                        $sQryID = $sQryID+1;
                        if(cmMer($cndFld['CNDANDOR']) === ""){
                            $msg = showMsg('FAIL_BASE_ADDTYPE',array('副条件'));//'副条件にAND/OR を入力してください。';
                            $focus = 'CMBANDOR'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                            $idx = $cndData['CNDIDX'];
                            $rtn = 1;
                        }
                    }
                    if($rtn === 0){
                        if($cndFld['CNDKBN'] === ''){
                            $msg = showMsg('FAIL_REQ',array('条件'));//'条件を入力してください';
                            $focus = 'CMBCOND'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                            $idx = $cndData['CNDIDX'];
                            $rtn = 1;
                        }
                    }
                    if($rtn === 0){
                        $cndDataVal = $cndFld['CNDDATA'];
                        //error_log("cndDataValcndDataVal****".print_r($cndDataVal,true));
                        //error_log("cndFldcndFldcndFld????".print_r($cndFld['CNDDATA'],true));
                        if($cndFld['CNDTYP'] === '2' || $cndFld['CNDTYP'] === '1'){
                            foreach ($cndDataVal as $cdata){
                                if(count($cdata) > 0){
                                    if($cdata['FFLG']){
                                        error_log("FFLGFFLGFFLG####".print_r($cdata['FFLG'],true));
                                        $msg = showMsg('FAIL_BASE_FUNC');//'検索タイプ「必須」また「任意」ならフィールドを条件データにすることはできません。';
                                        $idx = $cndData['CNDIDX'];
                                        $focus = 'CMBSELTYPE'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                        $rtn = 1;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if($rtn === 0){
                        $cndDataVal = $cndFld['CNDDATA'];
                        if($cndFld['CNDTYP'] === '2' || $cndFld['CNDTYP'] === '1'){
                            foreach ($cndDataVal as $cdata){
                                if(count($cdata) > 0){
                                    if($rtn===0){
                                        if($cdata['FFLG']){
                                            $msg = showMsg('FAIL_BASE_FUNC');//'検索タイプ「必須」また「任意」ならフィールドを条件データにすることはできません。';
                                            $idx = $cndData['CNDIDX'];
                                            $focus = 'CMBSELTYPE'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                            $rtn = 1;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if($rtn === 0){
                        // 検索タイプは「必須」「非表示」「固定」の場合
                        if($cndFld['CNDTYP'] === '3' || $cndFld['CNDTYP'] === ''){
                            // 条件は「RANGE」の場合
                            if($cndFld['CNDKBN'] === 'RANGE'){
                                // 【FROM】データのため必須チェック
                                switch($cndFld['CNDFLDTYP']){
                                    case 'P':
                                    case 'S':
                                    case 'B':
                                    case 'L':
                                    case 'T':
                                    case 'Z':
                                        if($cndDataVal[0]['FFLG']){
                                            if($cndDataVal[0]['DATA'] === null || $cndDataVal[0]['DATA'] === ''){
                                                $msg = showMsg('FAIL_REQ',array(array('「FROM」','データ')));//'「FROM」データを入力してください。';
                                                $focus = 'TXTCONDDATAFROM'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                                $idx = $cndData['CNDIDX'];
                                                $rtn = 1;
                                            }
                                        }else{
                                            if(trim($cndDataVal[0]['DATA']) === ''){
                                                $msg = showMsg('FAIL_REQ',array(array('「FROM」','データ')));//'「FROM」データを入力してください。';
                                                $focus = 'TXTCONDDATAFROM'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                                $idx = $cndData['CNDIDX'];
                                                $rtn = 1;
                                            }
                                        }
                                        break;
                                    default:
                                        if($cndDataVal[0]['DATA'] === ''){
                                            $msg = showMsg('FAIL_REQ',array(array('「FROM」','データ')));//'「FROM」データを入力してください。';
                                            $focus = 'TXTCONDDATAFROM'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                            $idx = $cndData['CNDIDX'];
                                            $rtn = 1;
                                        }
                                        break;
                                }
                                if($rtn === 0){
                                    //【TO】データのため必須チェック
                                    switch($cndFld['CNDFLDTYP']){
                                        case 'P':
                                        case 'S':
                                        case 'B':
                                        case 'L':
                                        case 'T':
                                        case 'Z':
                                            if($cndDataVal[1]['FFLG']){
                                                if($cndDataVal[1]['DATA'] === '' || $cndDataVal[1]['DATA'] === null ){
                                                    $msg = showMsg('FAIL_REQ',array(array('「TO」','データ')));//'「TO」データを入力してください。';
                                                    $focus = 'TXTCONDDATATO'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                                    $idx = $cndData['CNDIDX'];
                                                    $rtn = 1;
                                                }
                                            }else{
                                                if(trim($cndDataVal[1]['DATA']) === ''){
                                                    $msg = showMsg('FAIL_REQ',array(array('「TO」','データ')));//'「TO」データを入力してください。';
                                                    $focus = 'TXTCONDDATATO'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                                    $idx = $cndData['CNDIDX'];
                                                    $rtn = 1;
                                                }
                                            }
                                            break;
                                        default:
                                            if($cndDataVal[1]['DATA'] === ''){
                                                $msg = showMsg('FAIL_REQ',array(array('「TO」','データ')));//'「TO」データを入力してください.';
                                                $focus = 'TXTCONDDATATO'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                                $idx = $cndData['CNDIDX'];
                                                $rtn = 1;
                                            }
                                            break;
                                    }
                                }
                            }
                            // 条件は「LIST」の場合
                            else if($cndFld['CNDKBN'] === 'LIST' || $cndFld['CNDKBN'] === 'NLIST'){
                                $chkBlk = true;
                                $listRow = 1;
                                foreach($cndDataVal as $key => $value){
                                    $listRow = $key+1;
                                    if($chkBlk === true){
                                        switch($cndFld['CNDFLDTYP']){
                                            case 'P':
                                            case 'S':
                                            case 'B':
                                            case 'L':
                                            case 'T':
                                            case 'Z':
                                                if($value['FFLG']){
                                                    if($value['DATA'] !== ''){
                                                        $chkBlk = false;
                                                    }
                                                }else{
                                                    if($value['DATA'] !== ''){
                                                        $chkBlk = false;
                                                    }
                                                }
                                                break;
                                            default :
                                                if($value['DATA'] !== ''){
                                                    $chkBlk = false;
                                                }
                                                break;
                                        }
                                    }
                                }
                                if($chkBlk === true){
                                    $msg = showMsg('FAIL_REQ',array('条件データ'));//'条件データを入力してください。';
                                    $focus = 'TXTCONDLISTDATA'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'].'_'.'1';
                                    $idx = $cndData['CNDIDX'];
                                    $rtn = 1;
                                }
                            }else{
                                $value = $cndDataVal[0];
                                if($cndFld['CNDKBN'] === 'IS'){
                                    $itm = 'CMBISDATA';
                                }else{
                                    $itm = 'TXTCONDDATA';
                                }
                                switch($cndFld['CNDFLDTYP']){
                                    case 'P':
                                    case 'S':
                                    case 'B':
                                    case 'L':
                                    case 'T':
                                    case 'Z':
                                        if($value['FFLG']){
                                            if($value['DATA'] === null || $value['DATA'] === ''){
                                                $msg = showMsg('FAIL_REQ',array('条件データ'));//'条件データを入力してください。';
                                                $focus = $itm.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                                $idx = $cndData['CNDIDX'];
                                                $rtn = 1;
                                            }
                                        }else{
                                            if($value['DATA'] === ''){
                                                $msg = showMsg('FAIL_REQ',array('条件データ'));//'条件データを入力してください。';
                                                $focus = $itm.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                                $idx = $cndData['CNDIDX'];
                                                $rtn = 1;
                                            }
                                        }
                                        break;
                                    default :
                                        if($value['FFLG']){
                                            if($value['DATA'] === null ||  $value['DATA'] === ''){
                                                $msg = showMsg('FAIL_REQ',array('条件データ'));//'条件データを入力してください。';
                                                $focus = $itm.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                                $idx = $cndData['CNDIDX'];
                                                $rtn = 1;
                                            }
                                        }else{
                                            if($value['DATA'] === ''){
                                                $msg = showMsg('FAIL_REQ',array('条件データ'));//'条件データを入力してください。';
                                                $focus = $itm.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                                $idx = $cndData['CNDIDX'];
                                                $rtn = 1;
                                            }
                                        }
                                        break;
                                }
                            }
                        }else{
                            // 条件タイプは「任意」の場合
                            if($cndFld['CNDKBN'] === 'RANGE'){
                                if((($cndDataVal[0]['DATA'] === '') && ($cndDataVal[1]['DATA'] === ''))=== false){
                                    if($cndDataVal[0]['DATA'] === ''){
                                        $msg = showMsg('FAIL_REQ',array( array('「FROM」','データ')));//'「FROM」データを入力してください.';
                                        $focus = 'TXTCONDDATAFROM'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                        $idx = $cndData['CNDIDX'];
                                        $rtn = 1;    
                                    }else if($cndDataVal[1]['DATA'] === ''){
                                        $msg = showMsg('FAIL_REQ',array(array('「TO」','データ')));//'「TO」データを入力してください.';
                                        $focus = 'TXTCONDDATATO'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                        $idx = $cndData['CNDIDX'];
                                        $rtn = 1;
                                    }
                                }
                            }
                        }
                        if($rtn === 0){
                            foreach($cndDataVal as $key => $value){
                                $itm = '';
                                if($value['DATA'] !== ''){
                                    if($cndFld['CNDKBN'] === 'RANGE'){
                                        if($key === 0){
                                            $itm = 'TXTCONDDATAFROM'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                        }else if($key === 1){
                                            $itm = 'TXTCONDDATATO'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                        }
                                    }else if ($cndFld['CNDKBN'] === 'LIST' || $cndFld['CNDKBN'] === 'NLIST') {
                                        $itm = 'TXTCONDLISTDATA'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'].'_'.($key+1);
                                    }else if($cndFld['CNDKBN'] === 'IS'){
                                        $itm = 'CMBISDATA'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                    }else{
                                        $itm = 'TXTCONDDATA'.$cndData['CNDIDX'].'_'.$cndFld['CDATAIDX'];
                                    }

                                    if($cndFld['CNDFLDTYP'] === 'P' || $cndFld['CNDFLDTYP'] === 'B' || $cndFld['CNDFLDTYP'] === 'S'){
                                        if($cndFld['CNDKBN'] !== 'IS'){
                                            if($value['FFLG']){
                                                if(is_array($value['DATA'])){
                                                    if($value['DATA']['DDS_TYPE'] === 'P' || $value['DATA']['DDS_TYPE'] === 'B' || $value['DATA']['DDS_TYPE'] === 'S'){
                                                        if($value['DATA']['LENGTH'] > $cndFld['CNDFLDLEN']){
                                                            $msg = showMsg('FAIL_CHK',array('条件データ'));//'条件データが正しくありません。'; 
                                                            $focus = $itm;
                                                            $idx = $cndData['CNDIDX'];
                                                            $rtn = 1;
                                                            break;
                                                        }else{
                                                            $value['DATA'] = $value['DATA']['COLUMN_NAME'].'_'.$value['DATA']['FILID'];
                                                        }
                                                    }else{
                                                        $msg = showMsg('FAIL_BASE_FUNCCHK',array(array('数値','フィールド'),array('文字列','フィールド')));//'数値フィールドに文字列フィールドを条件データにすることはできません。';
                                                        $focus = $itm;
                                                        $idx = $cndData['CNDIDX'];
                                                        $rtn = 1;
                                                        break;
                                                    }
                                                }else{
                                                    $msg = showMsg('FAIL_BASE_FUNCFLG',array('指定したフィールド情報'));//'指定したフィールド情報が取得できません';
                                                    $focus = $itm;
                                                    $idx = $cndData['CNDIDX'];
                                                    $rtn = 1;
                                                    break;
                                                }
                                            }else{
                                                if(checkNum($value['DATA']) === false){
                                                    $msg = showMsg('FAIL_BASE_FUNCCHK',array(array('数値','フィールド'),'文字値を条件データ'));//'数値フィールドに文字値を条件データにすることはできません。';
                                                    $focus = $itm;
                                                    $idx = $cndData['CNDIDX'];
                                                    $rtn = 1;
                                                    break;
                                                }
                                                if($rtn === 0){
                                                    if(!checkMaxLen($value['DATA'],$cndFld['CNDFLDLEN'])){
                                                        $msg = showMsg('FAIL_CHK',array('条件データ'));//'条件データが正しくありません。';
                                                        $focus = $itm;
                                                        $idx = $cndData['CNDIDX'];
                                                        $rtn = 1;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }else if($cndFld['CNDFLDTYP'] === 'L'){
                                        if($cndFld['CNDKBN'] !== 'IS'){
                                            //e_log('条件データ日付：'.print_r($value,true));
                                            if($value['FFLG']){
                                                if(is_array($value['DATA'])){
                                                    if($value['DATA']['DDS_TYPE'] === 'L'){
                                                        $value['DATA'] = $value['DATA']['COLUMN_NAME'].'_'.$value['DATA']['FILID'];
                                                    }else{
                                                        $msg = showMsg('FAIL_BASE_FUNCCHK',array(array('日付','フィールド'),array('文字列','又','数値','フィールド')));//'数値フィールドに文字列フィールドを条件データにすることはできません。';
                                                        $focus = $itm;
                                                        $idx = $cndData['CNDIDX'];
                                                        $rtn = 1;
                                                        break;
                                                    }
                                                }else{
                                                    $msg = showMsg('FAIL_BASE_FUNCFLG',array('指定したフィールド情報'));
                                                    $focus = $itm;
                                                    $idx = $cndData['CNDIDX'];
                                                    $rtn = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }else if($cndFld['CNDFLDTYP'] === 'T'){
                                        if($cndFld['CNDKBN'] !== 'IS'){
                                            if($value['FFLG']){
                                                if(is_array($value['DATA'])){
                                                    if($value['DATA']['DDS_TYPE'] === 'T'){
                                                        $value['DATA'] = $value['DATA']['COLUMN_NAME'].'_'.$value['DATA']['FILID'];
                                                    }else{
                                                        $msg = showMsg('FAIL_BASE_FUNCCHK',array(array('時刻','フィールド'),array('文字列','又','数値','フィールド')));//'数値フィールドに文字列フィールドを条件データにすることはできません。';
                                                        $focus = $itm;
                                                        $idx = $cndData['CNDIDX'];
                                                        $rtn = 1;
                                                        break;
                                                    }
                                                }else{
                                                    $msg = showMsg('FAIL_BASE_FUNCFLG',array('指定したフィールド情報'));
                                                    $focus = $itm;
                                                    $idx = $cndData['CNDIDX'];
                                                    $rtn = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }else if($cndFld['CNDFLDTYP'] === 'Z'){
                                        if($cndFld['CNDKBN'] !== 'IS'){
                                            if($value['FFLG']){
                                                if(is_array($value['DATA'])){
                                                    if($value['DATA']['DDS_TYPE'] === 'Z'){
                                                        $value['DATA'] = $value['DATA']['COLUMN_NAME'].'_'.$value['DATA']['FILID'];
                                                    }else{
                                                        $msg = showMsg('FAIL_BASE_FUNCCHK',array(array('タイムスタンプ','フィールド'),array('文字列','又','数値','フィールド')));//'数値フィールドに文字列フィールドを条件データにすることはできません。';
                                                        $focus = $itm;
                                                        $idx = $cndData['CNDIDX'];
                                                        $rtn = 1;
                                                        break;
                                                    }
                                                }else{
                                                    $msg = showMsg('FAIL_BASE_FUNCFLG',array('指定したフィールド情報'));
                                                    $focus = $itm;
                                                    $idx = $cndData['CNDIDX'];
                                                    $rtn = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }else{
                                         if($cndFld['CNDKBN'] !== 'IS'){
                                            if($value['FFLG']){
                                                if(is_array($value['DATA'])){
                                                    if($value['DATA']['DDS_TYPE'] === 'P' || $value['DATA']['DDS_TYPE'] === 'B' || $value['DATA']['DDS_TYPE'] === 'S'){
                                                        $msg = showMsg('FAIL_BASE_FUNCCHK',array(array('文字列','フィールド'),array('数値','フィールド')));//'文字列フィールドに数値フィールドを条件データにすることはできません。';
                                                        $focus = $itm;
                                                        $idx = $cndData['CNDIDX'];
                                                        $rtn = 1;
                                                        break;
                                                    }else{                                                   
                                                        if($value['DATA']['LENGTH'] > $cndFld['CNDFLDLEN']){
                                                            $msg = showMsg('FAIL_CHK',array('条件データ'));//'条件データが正しくありません。';
                                                            $focus = $itm;
                                                            $idx = $cndData['CNDIDX'];
                                                            $rtn = 1;
                                                            break;
                                                        }else{
                                                            $value['DATA'] = $value['DATA']['COLUMN_NAME'].'_'.$value['DATA']['FILID'];
                                                        }
                                                    }
                                                }else{
                                                    $msg = showMsg('FAIL_BASE_FUNCFLG',array('指定したフィールド情報'));//'指定したフィールド情報が取得できません';
                                                    $focus = $itm;
                                                    $idx = $cndData['CNDIDX'];
                                                    $rtn = 1;
                                                    break;
                                                }
                                            }
                                         }
                                    }
                                    if($value['FFLG'] === false){
                                        if(!checkMaxLen($value['DATA'],128)){
                                            $msg = showMsg('FAIL_MAXLEN',array('条件データ'));//'条件データが入力桁数を超えています。';
                                            $rtn = 1;
                                            break;
                                        }
                                        if($rtn === 0){
                                            switch($cndFld['CNDFLDTYP']){
                                                case 'L':
                                                    if($value['DATA'] !== ''){
                                                        switch($cndFld['CNDKBN']){
                                                            case 'LIKE':
                                                            case 'NLIKE':
                                                            case 'LLIKE':
                                                            case 'LNLIKE':
                                                            case 'RLIKE':
                                                            case 'RNLIKE':
                                                                break;
                                                            default:
                                                                $chkRes = chkVldDate($value['DATA']);
                                                                if($chkRes == 1){
                                                                    //$msg = showMsg('FAIL_CHK',array('条件データ'));//'条件データが正しくありません。';
                                                                    $msg = showMsg('FAIL_CHK',array(array('日付','フォーマット'))) .'<br>'
                                                                            . showMsg('FAIL_FIL_EXT',array('日付','フォーマット',array('「yyyy/mm/dd」、「yyyy-mm-dd」','又','「yyyymmdd」')));
                                                                    $focus = $itm;
                                                                    $idx = $cndData['CNDIDX'];
                                                                    $rtn = 1;
                                                                    break;
                                                                }
                                                                break;
                                                        }
                                                    }
                                                    break;
                                                case 'T':
                                                    if($value['DATA'] !== ''){
                                                        switch($cndFld['CNDKBN']){
                                                            case 'LIKE':
                                                            case 'NLIKE':
                                                            case 'LLIKE':
                                                            case 'LNLIKE':
                                                            case 'RLIKE':
                                                            case 'RNLIKE':
                                                                break;
                                                            default:
                                                                $chkRes = chkVldTime($value['DATA']);
                                                                if($chkRes == 1){
                                                                    //$msg = showMsg('FAIL_CHK',array('条件データ'));//'条件データが正しくありません。';
                                                                    //$msg = '時刻フォーマットが正しくないです。<br>「HH.MM.SS」又「HH:MM:SS」のフォッマとで入力してください。';
                                                                    $msg = showMsg('FAIL_CHK',array(array('時刻','フォーマット'))) .'<br>'
                                                                            . showMsg('FAIL_FIL_EXT',array('時刻','フォーマット',array('「HH.MM.SS」','又','「HH:MM:SS」')));
                                                                    $focus = $itm;
                                                                    $idx = $cndData['CNDIDX'];
                                                                    $rtn = 1;
                                                                    break;
                                                                }
                                                                break;
                                                        }
                                                    }
                                                    break;
                                                case 'Z':
                                                    if($value['DATA'] !== ''){
                                                        switch($cndFld['CNDKBN']){
                                                            case 'LIKE':
                                                            case 'NLIKE':
                                                            case 'LLIKE':
                                                            case 'LNLIKE':
                                                            case 'RLIKE':
                                                            case 'RNLIKE':
                                                                break;
                                                            default:
                                                                $chkRes = chkVldTimeStamp($value['DATA']);
                                                                if($chkRes == 1){
                                                                    //$msg = showMsg('FAIL_CHK',array('条件データ'));//'条件データが正しくありません。';
                                                                    //$msg = 'タイムスタンプフォーマットが正しくないです。<br>「yyyy-mm-dd-HH.MM.SS.NNNNNN」又「yyyy-mm-dd HH.MM.SS」のフォッマとで入力してください。';
                                                                    $msg = showMsg('FAIL_CHK',array(array('タイムスタンプ','フォーマット'))) .'<br>'
                                                                            . showMsg('FAIL_FIL_EXT',array('タイムスタンプ','フォーマット',array('「yyyy-mm-dd-HH.MM.SS.NNNNNN」','又','「yyyy-mm-dd HH.MM.SS」')));
                                                                    $focus = $itm;
                                                                    $idx = $cndData['CNDIDX'];
                                                                    $rtn = 1;
                                                                    break;
                                                                }
                                                                break;
                                                        }
                                                    }
                                                    break;
                                                default:
                                                    if(!checkMaxLen($value['DATA'],$cndFld['CNDFLDLEN'])){
                                                        $msg = showMsg('FAIL_CHK',array('条件データ'));//'条件データが正しくありません。';
                                                        $focus = $itm;
                                                        $idx = $cndData['CNDIDX'];
                                                        $rtn = 1;
                                                        //e_log("条件データが正しくありません。 MSM 20180813 444");
                                                        break;
                                                    }
                                                    break;
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if($rtn === 0){
                        $cndifo = array();
                        $cndifo['QRYN'] = $BASECONDDATA['D1NAME'];
                        error_log("PPN20181105 cndFldDATA =>  ".print_r($cndFld,true));
                        error_log("PPN20181105 BASECONDDATA =>  ".print_r($BASECONDDATA,true));
                        if($cndFld['CNDFILTYP'] === 'P'){
                            $cndifo['CNFILID'] = 0;
                        }else if($cndFld['CNDFILTYP'] === 'K'){
                            $cndifo['CNFILID'] = 9999;
                        }else{
                            $cndifo['CNFILID'] = (int)substr($cndFld['CNDFILTYP'],1);
                        }
                        if($PROC === 'ADD'){
                            $cndifo['FMSEQ']  = $mQryID;
                            $cndifo['FSSEQ']  = $sQryID;
                        }else{
                            $cndifo['FMSEQ']  = $mkey+1;
                            $cndifo['FSSEQ']  = $cndFld['CDATAIDX'];
                        }
                        $cndifo['CNMSEQ'] = $mQryID;
                        $cndifo['CNSSEQ'] = $sQryID;
                        if($sQryID === 1){
                            $cndifo['CNAOKB'] = $cndData['SQRYAO'] ;
                        }else{
                            $cndifo['CNAOKB'] = cmMer($cndFld['CNDANDOR']);
                        }
                        $cndifo['CNFLDN'] = $cndFld['CNDFLDNM'];
                        $cndifo['CNCKBN'] = $cndFld['CNDKBN'];
                        $cndifo['CNSTKB'] = $cndFld['CNDTYP'];
                        $cndDVal = array();
                        foreach($cndFld['CNDDATA'] as $value){
                            if($value['FFLG'] === false){
                                if($cndFld['CNDFLDTYP'] === 'P' || $cndFld['CNDFLDTYP'] === 'B' || $cndFld['CNDFLDTYP'] === 'S'){
                                    $value['DATA'] = trim($value['DATA']," ");//MSM change
                                }
                            }
                            if($value['DATA'] !== ''){
                                $valObj = array();
                                $valObj['FFLG'] = $value['FFLG'];
                                if($value['FFLG']){
                                    $valObj['DAT'] = $value['DATA']['FIELDVAL'];
                                    $valObj['BCNT'] = 0;
                                }else{
                                    $valObj['DAT'] = $value['DATA'];
                                    if(cmMer($value['DATA']) === ''){
                                        $valObj['BCNT'] = strlen($value['DATA']);
                                    }else{
                                        $valObj['BCNT'] = 0;
                                    }
                                }
                                $cndDVal[]= $valObj;
                            }
                        }
                        $cndifo['CNDDAT'] = $cndDVal;
                        $CONDDATA[] = $cndifo;
                    }
                }
            }
        }
    }
}
$data['BASECONDFRMDATA'] = $BASECONDDATA;//no need
error_log("PyaePhyoNaing123++++".print_r($BASECONDDATA,true));

////
$data['BASEDCNDDATA']    = $CONDDATA;//ppn no to yes
error_log("PyaePhyoNaing123 CONDDATA ++++ ".print_r($CONDDATA,true));

$rtn = array(
    'DATA' => $data, 
    'RTN' => $rtn,
    'MSG' => $msg,
    'FOCUS' => $focus,
    'IDX'   => $idx
);
echo(json_encode($rtn));
