<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 
 * PROGRAM ID     : comChkBaseQryFieldInfo.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/04/11
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("chkResField.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$SELLIBLIST = json_decode($_POST['SELLIBLIST'],true);
$FIELDINFO  = json_decode($_POST['FIELDINFO'],true);
$BASEFILEINFO = json_decode($_POST['BASEFILEINFO'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/

$data = array();
$rtn = 0;
$msg = '';
$row = '';
$column = '';
$fileid = '';

if($rtn === 0){
    $rs = 0;
    $seqArr = array();
    $sortNoArr = array();
    $sTypChk = 0;

    for($filcnt = 0; $filcnt<count($FIELDINFO);$filcnt++){
        $filVal = $FIELDINFO[$filcnt];
        $fldInfo = $filVal['STRDATA'];
        if(count($fldInfo)>0){
            $result = callchkValidResFld($fldInfo,$BASEFILEINFO,'CHK');
            if($result['RTN'] !== 0){
                $rtn = 1;
                $msg = $result['MSG'];
                $row = $result['ROW'] ;
                $column = $result['COL'];
                $fileid = $filVal['D2FILID'];
                break;
            }else{
                for($fldCnt = 0; $fldCnt<count($fldInfo);$fldCnt++){
                    $fldVal = $fldInfo[$fldCnt];
                    if($fldVal['SEQ'] == 0){
                        $fldVal['SEQ'] = '';
                    }
                    if($fldVal['SORTNO'] == 0){
                        $fldVal['SORTNO']  = '';
                        $fldVal['SORTTYP'] = '';
                    }
                    if($fldVal['SEQ'] !== ''){
                        $seqArr[] = $fldVal['SEQ'];
                        if($fldVal['SORTNO'] !== ''){
                            $sortNoArr[] = $fldVal['SORTNO'];
                            if($fldVal['SORTTYP'] === '' && $sTypChk === 0){
                                $sTypChk === 1;
                                $rtn = 1;
                                $msg = showMsg('FAIL_REQ',array('昇／降順'));
                                $row = $fldCnt;
                                $column = '7';
                                $fileid = $filVal['D2FILID'];
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    if($rtn === 0){
        if(count($seqArr) === 0){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('SEQ'));
            $column = '0';
            $row = 0;
            $fileid = $FIELDINFO[0]['D2FILID'];
        }
    }
    if($rtn === 0){
        if(count($seqArr) > FLDCNTMAX){
            $rtn = 1;
            $msg = showMsg('フィールドの最大数は'.FLDCNTMAX.'項目です。');
        }
    }
    if($rtn === 0){
        if(count(array_unique($seqArr)) < count($seqArr)){
            $rtn = 1;
            $msg = showMsg('CHK_DUP',array('SEQ'));
        }
    }

    if($rtn === 0){
        if(count(array_unique($sortNoArr)) < count($sortNoArr)){
            $rtn = 1;
            $msg = showMsg('CHK_DUP',array('SORT順'));
        }
    }
    if($rtn === 0){
        if($rs !== 0){
            $rtn = 1;
            $msg = $rs;
        }
    }
}

$data['SELLIBLIST'] = $SELLIBLIST;
$data['FIELDINFO']  = $FIELDINFO;

$rtn = array(
    'DATA' => $data,
    'RTN' => $rtn,
    'MSG' => $msg,
    'ROW' => $row,
    'COL' => $column,
    'FILEID' => $fileid
);
echo(json_encode($rtn));
