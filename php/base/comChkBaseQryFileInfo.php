<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
$SELLIBLIST = json_decode($_POST['SELLIBLIST'], true);
$REFFILE = json_decode($_POST['REFFILE'], true);
$D1NAME = $_POST['D1NAME'];
$D1FILLIB = $_POST['D1FILLIB'];
$D1FILE = $_POST['D1FILE'];
$D1TEXT = $_POST['D1TEXT'];
$D1FILMBR = $_POST['D1FILMBR'];
$PROC = $_POST['PROC'];
$NEWD1NAME = (isset($_POST['NEWD1NAME'])) ? $_POST['NEWD1NAME'] : '';
$CPYFLG = $_POST['CPYFLG'];
$RDBNM = (isset($_POST['RDBNM'])) ? $_POST['RDBNM'] : RDB;
/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$PRYFILINFO = array();
$FILELIST = array();
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$focus = '';
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
$D1NAME = cmHscDe($D1NAME);
$D1FILLIB = cmHscDe($D1FILLIB);
$D1FILE = cmHscDe($D1FILE);
$D1TEXT = cmHscDe($D1TEXT);
$D1FILMBR = cmHscDe($D1FILMBR);
$NEWD1NAME = cmHscDe($NEWD1NAME);
$RTNREFFIL = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    } else {
        $userData = umEx($rs['data']);
        if ($userData[0]['WUAUTH'] === '2') {
            $rs = cmChkKenGen($db2con, '3', $userData[0]['WUSAUT']); //'3' =>  queryMaster
            if ($rs['result'] !== true) {
                $rtn = 2;
                $msg = showMsg($rs['result'], array('クエリー作成の権限'));
            }
        }
    }
}
/**20170525 akz**/
if ($rtn === 0) {
    $LIBSNM = cmMer($userData[0]['WULIBL']);
}
$RDBNM = (isset($_SESSION['PHPQUERY']['RDBNM']) ? $_SESSION['PHPQUERY']['RDBNM'] : '');
/**20170525 akz**/
if ($RDBNM !== '') {
    $LIBSDB = $RDBNM;
} else {
    $LIBSDB = RDB;
}
/**20170525 akz**/
if ($RDBNM === '') {
    if ($PROC === 'ADD') {
        //定義名チェック
        if ($rtn === 0) {
            $rs = chkD1NAME($db2con, $D1NAME);
			if($rs==='ISEXIST_GROUPID'){
                $rtn = 1;
                $msg = showMsg('ISEXIST', array('DB2QRYGで定義名'));
                $focus = 'D1NAME';
			} else if ($rs !== 0) {
                $rtn = 1;
                $msg = showMsg($rs, array('定義名'));
                $focus = 'D1NAME';
            }
        }
    }
    if ($CPYFLG === '1') {
        //定義名チェック
        if ($rtn === 0) {
            $rs = chkD1NAME($db2con, $NEWD1NAME);
			if($rs==='ISEXIST_GROUPID'){
                $rtn = 1;
                $msg = showMsg('ISEXIST', array('DB2QRYGで定義名'));
                $focus = 'D1NAME';
			}else if ($rs !== 0) {
                $rtn = 1;
                $msg = showMsg($rs, array('定義名'));
                $focus = 'D1NAME';
            }
        }
    }
  
    //プライマリ.ライブラリーチェック
    if ($rtn === 0) {
        if ($D1FILLIB === '' && $D1FILE === '') {
            $rtn = 1;
            $msg = showMsg('FAIL_REQ', array(array('プライマリ', '．', 'ライブラリー', '名')));
            $focus = 'D1LIBITMLIBNM';
        } else if ($D1FILLIB === '') {
            $D1FILLIB = '*USRLIBL';
        }
    }
    if ($rtn === 0) {
        if ($D1FILLIB === '' || strtoupper($D1FILLIB) === '*LIBL' || strtoupper($D1FILLIB) === '*USRLIBL') {
            $D1FILLIB = strtoupper($D1FILLIB);
        } else {
            $rs = chkLIB($db2con, $D1FILLIB, $SELLIBLIST, $LIBSNM, $LIBSDB);
            if ($rs !== 0) {
                $rtn = 1;
                $msg = showMsg($rs, array(array('プライマリ', '．', 'ライブラリー', '名')));
                $focus = 'D1LIBITMLIBNM';
            }
        }
    }
    //プライマリ.ファイルチェック
    if ($rtn === 0) {
        $rs = chkFile($db2con, $D1FILLIB, $D1FILE, $SELLIBLIST);
		//e_log('Library check result***'.print_r($rs['result']));
        if ($rs['result'] !== 0) {
            $rtn = 1;
            $msg = showMsg($rs['result'], array(array('プライマリ', '．', 'ファイル', '名')));
            $focus = 'D1FILEITMFILENM';
        } else {
            $PRYFILINFO = $rs['data'];
            $fileinfo = array('SYSTEM_TABLE_SCHEMA' => $PRYFILINFO['SYSTEM_TABLE_SCHEMA'], 'SYSTEM_TABLE_NAME' => $PRYFILINFO['SYSTEM_TABLE_NAME'], 'TABLEID' => 'P');
            $FILELIST[$fileinfo['TABLEID']] = $fileinfo;
        }
    }
    // プライマリ．メンバーチェック
    if ($rtn === 0) {
        if ($D1FILMBR === '' || strtoupper($D1FILMBR) === '*FIRST' || strtoupper($D1FILMBR) === '*LAST') {
            $D1FILMBR = strtoupper($D1FILMBR);
        } else {
            $rs = chkMBR($db2con, $fileinfo['SYSTEM_TABLE_SCHEMA'], $fileinfo['SYSTEM_TABLE_NAME'], $D1FILMBR);
            if ($rs['result'] !== 0) {
                $rtn = 1;
                $msg = showMsg($rs['result'], array(array('プライマリ', '．', 'メンバー', '名')));
                $focus = 'D1FILMBRFILMBR';
            }
        }
    }
    //プライマリ.記述チェック
    if ($rtn === 0) {
        if ($D1TEXT === '') {
            $D1TEXT = $PRYFILINFO['TABLE_TEXT'];
        } else {
            if (!checkMaxLen($D1TEXT, 50)) {
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN', array('記述'));
                $focus = 'D1TEXT';
            }
        }
    }
    //参照ファイルチェック
    if ($rtn === 0) {
        $rs = chkRefFileData($db2con, $PRYFILINFO, $FILELIST, $SELLIBLIST, $REFFILE, $LIBSNM, $LIBSDB);
        if ($rs[RTN] !== 0) {
            $rtn = 1;
            $msg = showMsg($rs[RTN]);
            $focus = $rs['FOCUS'];
        } else {
            $REFFILE = $rs['REFFILE'];
        }
    }
} else {
    $db2conRDB = cmDB2ConRDB();
    if ($PROC === 'ADD') {
        //定義名チェック
        if ($rtn === 0) {
            $rs = chkD1NAME($db2con, $D1NAME);
			if($rs==='ISEXIST_GROUPID'){
                $rtn = 1;
                $msg = showMsg('ISEXIST', array('DB2QRYGで定義名'));
                $focus = 'D1NAME';
			}else if ($rs !== 0) {
                $rtn = 1;
                $msg = showMsg($rs, array('定義名'));
                $focus = 'D1NAME';
            }
        }
    }
    if ($CPYFLG === '1') {
        //定義名チェック
        if ($rtn === 0) {
            $rs = chkD1NAME($db2con, $NEWD1NAME);
            if ($rs !== 0) {
				if($rs==='ISEXIST_GROUPID'){
	                $rtn = 1;
	                $msg = showMsg('ISEXIST', array('DB2QRYGで定義名'));
	                $focus = 'D1NAME';
				}else{
	                $rtn = 1;
	                $msg = showMsg($rs, array('定義名'));
	                $focus = 'D1NAME';
				}

            }
        }
    }
    //プライマリ.ライブラリーチェック
    if ($rtn === 0) {
        if ($D1FILLIB === '' && $D1FILE === '') {
            $rtn = 1;
            $msg = showMsg('FAIL_REQ', array(array('プライマリ', '．', 'ライブラリー', '名')));
            $focus = 'D1LIBITMLIBNM';
        } else if ($D1FILLIB === '') {
            $D1FILLIB = '*USRLIBL';
        }
    }
    if ($rtn === 0) {
        if ($D1FILLIB === '' || strtoupper($D1FILLIB) === '*LIBL' || strtoupper($D1FILLIB) === '*USRLIBL') {
            $D1FILLIB = strtoupper($D1FILLIB);
        } else {
            $rs = chkLIB($db2conRDB, $D1FILLIB, $SELLIBLIST, $LIBSNM, $LIBSDB);
            if ($rs !== 0) {
                $rtn = 1;
                $msg = showMsg($rs, array(array('プライマリ', '．', 'ライブラリー', '名')));
                $focus = 'D1LIBITMLIBNM';
            }
        }
    }
    //プライマリ.ファイルチェック
    if ($rtn === 0) {
        $rs = chkFile($db2conRDB, $D1FILLIB, $D1FILE, $SELLIBLIST);
        if ($rs['result'] !== 0) {
            $rtn = 1;
            $msg = showMsg($rs['result'], array(array('プライマリ', '．', 'ファイル', '名')));
            $focus = 'D1FILEITMFILENM';
        } else {
            $PRYFILINFO = $rs['data'];
            $fileinfo = array('SYSTEM_TABLE_SCHEMA' => $PRYFILINFO['SYSTEM_TABLE_SCHEMA'], 'SYSTEM_TABLE_NAME' => $PRYFILINFO['SYSTEM_TABLE_NAME'], 'TABLEID' => 'P');
            $FILELIST[$fileinfo['TABLEID']] = $fileinfo;
        }
    }
    // プライマリ．メンバーチェック
    if ($rtn === 0) {
        if ($D1FILMBR === '' || strtoupper($D1FILMBR) === '*FIRST' || strtoupper($D1FILMBR) === '*LAST') {
            // 処理なし
            
        } else {
            $rs = chkMBR($db2conRDB, $fileinfo['SYSTEM_TABLE_SCHEMA'], $fileinfo['SYSTEM_TABLE_NAME'], $D1FILMBR);
            if ($rs['result'] !== 0) {
                $rtn = 1;
                $msg = showMsg($rs['result'], array(array('プライマリ', '．', 'メンバー', '名')));
                $focus = 'D1FILMBRFILMBR';
            }
        }
    }
    //プライマリ.記述チェック
    if ($rtn === 0) {
        if ($D1TEXT === '') {
            $D1TEXT = $PRYFILINFO['TABLE_TEXT'];
        } else {
            if (!checkMaxLen($D1TEXT, 50)) {
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN', array('記述'));
                $focus = 'D1TEXT';
            }
        }
    }
    //参照ファイルチェック
    if ($rtn === 0) {
        $rs = chkRefFileData($db2conRDB, $PRYFILINFO, $FILELIST, $SELLIBLIST, $REFFILE, $LIBSNM, $LIBSDB);
        if ($rs[RTN] !== 0) {
            $rtn = 1;
            $msg = showMsg($rs[RTN]);
            $focus = $rs['FOCUS'];
        } else {
            $REFFILE = $rs['REFFILE'];
        }
    }
    cmDb2Close($db2conRDB);
}
if ($rtn === 0) {
    if (count($REFFILE) > 0) {
        foreach ($REFFILE as $RFILVAL) {
            if ($RFILVAL['RTRFIL'] !== '') {
                $RFILE = array();
                $RFILE['REFIDX'] = $RFILVAL['REFIDX'];
                $RFILE['RTRFIL'] = $RFILVAL['RTRFIL'];
                $RFILE['RTRMBR'] = $RFILVAL['RTRMBR'];
                $RFILE['RTRLIB'] = $RFILVAL['RTRLIB'];
                $RFILE['JOINTYP'] = $RFILVAL['JOINTYP'];
                $RFILE['RTEXT'] = $RFILVAL['RTEXT'];
                $RFILE['REFFIELDARR'] = $RFILVAL['REFFIELDARR'];
                $RTNREFFIL[] = $RFILE;
            }
            //array_push($RTNREFFIL,$RFILE);
            
        }
    }
    $data['RDBNM'] = $RDBNM;
    $data['SELLIBLIST'] = $SELLIBLIST;
    $data['D1NAME'] = $D1NAME;
    $data['D1NAMENEW'] = $NEWD1NAME;
    $data['D1FILLIB'] = $D1FILLIB;
    $data['D1FILE'] = $D1FILE;
    $data['D1FILMBR'] = $D1FILMBR;
    $data['D1TEXT'] = $D1TEXT;
    $data['REFFILE'] = $RTNREFFIL;
}
//チェック参照ファイルのValid？
if ($rtn === 0) {
    $resValid = chkValidRefFil($data);
    /*if ($resValid === false) {
        $rtn = 1;
        $msg = showMsg('FAIL_CHK', array('参照情報'));
    }*/
	$warn_msg = ' </br>【エラーコード：';

	if($resValid['RTN'] === false){
		 $rtn = 1;
		 //$msg = $resValid['MSG'];
         $msg1 = showMsg('FAIL_CHK', array('参照情報'));
	     $msg = $msg1.$warn_msg.$resValid['MSG'].'】';
	}
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
   'BASEFILEDATA' => $data, 
   'RTN' => $rtn, 
   'MSG' => $msg, 
   'FOCUS' => $focus, 
   'LIBRARY' => $D1FILLIB, 
   'LIBFILE' => $SELLIBLIST, 
   'LIBNAME' => $LIBSNM, 
   'LIBDB' => $LIBSDB
);
echo (json_encode($rtn));
function chkD1NAME($db2con, $D1NAME) {
    $rtn = 0;
    if ($D1NAME === '') {
        $rtn = 'FAIL_REQ';
    }
    if ($rtn === 0) {
        if (!checkMaxLen($D1NAME, 10)) {
            $rtn = 'FAIL_MAXLEN';
        }
    }
    if ($rtn === 0) {
        if (preg_match('/[^0-9A-Z_#@]+/', $D1NAME)) {
            $rtn = 'FAIL_VLDC';
        }
    }
    if ($rtn === 0) {
        $rs = fnChkExistD1NAME($db2con, $D1NAME);
        if ($rs !== true) {
            $rtn = $rs;
        }
    }
    if ($rtn === 0) {
        $rs = fnCheckDB2QRYG($db2con, $D1NAME);
		if ($rs !== true) {
            $rtn = $rs;
        }
    }
    return $rtn;
}
function fnChkExistD1NAME($db2con, $D1NAME) {
    $data = array();
    $rs = true;
    $strSQL = ' SELECT A.D1NAME ';
    $strSQL.= ' FROM FDB2CSV1 AS A ';
    $strSQL.= ' WHERE A.D1NAME = ? ';
    $params = array($D1NAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) > 0) {
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}
function chkLIB($db2con, $LIB, $LIBLIST, $LIBSNM, $LIBSDB) {
    $rtn = 0;
    if ($rtn === 0) {
        if (!checkMaxLen($LIB, 10)) {
            $rtn = 'FAIL_MAXLEN';
        }
    }
    if ($rtn === 0) {
        $byte = checkByte($LIB);
        if ($byte[1] > 0) {
            $rtn = 'FAIL_BYTE';
        }
    }
    if ($rtn === 0) {
        $rs = fnChkExistLib($db2con, $LIB, $LIBLIST, $LIBSNM, $LIBSDB);
        if ($rs === true) {
            $rtn = 'NOTEXIST';
        }
    }
    return $rtn;
}
function fnChkExistLib($db2con, $LIB, $LIBLIST, $LIBSNM, $LIBSDB) {
    $data = array();
    $params = array();
    $rs = true;
    if (($key = array_search('QTEMP', $LIBLIST)) !== false) {
        unset($LIBLIST[$key]);
    }
    if ($LIB === 'QTEMP') {
        $rs = 'ISEXIST';
    } else {
        $strSQL = ' SELECT A.SCHEMA_NAME ';
        $strSQL.= ' FROM  ';
        $strSQL.= '    ( ';
        $strSQL.= '        SELECT ';
        $strSQL.= '        SYSTEM_SCHEMA_NAME SCHEMA_NAME ';
        $strSQL.= '        , SCHEMA_TEXT ';
        $strSQL.= '        FROM ';
        $strSQL.= '        ' . SYSSCHEMASLIB . '/SYSSCHEMAS ';
        $strSQL.= '        WHERE SYSTEM_SCHEMA_NAME <> \'QTEMP\'';
        if ($LIBSNM !== '') {
            if (SYSLIBCHK === '1') {
                $strSQL.= ' AND SYSTEM_SCHEMA_NAME IN (SELECT LIBSID FROM DB2LBLS WHERE LIBSNM = ? AND LIBSDB = ?)';
                array_push($params, $LIBSNM, $LIBSDB);
            } else {
                $strSQL.= ' AND SYSTEM_SCHEMA_NAME NOT IN (SELECT LIBSID FROM DB2LBLS WHERE LIBSNM = ? AND LIBSDB = ?)';
                array_push($params, $LIBSNM, $LIBSDB);
            }
        }
        $strSQL.= '    ) A ';
        if ($LIB === '*USRLIBL' || $LIB === '*LIBL') {
            $strSQL.= ' WHERE A.SCHEMA_NAME IN  (';
            for ($i = 0;$i < count($LIBLIST);$i++) {
                $strSQL.= ' ? ,';
                array_push($params, $LIBLIST[$i]);
            }
            $strSQL = substr($strSQL, 0, -1);
            $strSQL.= ' )';
        } else {
            $strSQL.= ' WHERE A.SCHEMA_NAME = ? ';
            array_push($params, $LIB);
        }
        e_log('comChkBaseQryFileInfo.php⇒' . $strSQL . print_r($params, true));
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $rs = 'FAIL_SEL';
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $rs = 'FAIL_SEL';
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                if ($LIB === '*USRLIBL' || $LIB === '*LIBL') {
                    if (count($data) === count($LIBLIST)) {
                        $rs = 'ISEXIST';
                    }
                } else {
                    if (count($data) > 0) {
                        $rs = 'ISEXIST';
                    }
                }
            }
        }
    }
    return $rs;
}
function chkFile($db2con, $LIB, $FILE, $LIBLIST) {
    $rtn = array('result' => 0);
    if ($rtn['result'] === 0) {
        if ($FILE === '') {
            $rtn = array('result' => 'FAIL_REQ');
        }
    }
    if ($rtn['result'] === 0) {
        if (!checkMaxLen($FILE, 10)) {
            $rtn = array('result' => 'FAIL_MAXLEN');
        }
    }
    if ($rtn['result'] === 0) {
        $byte = checkByte($FILE);
        if ($byte[1] > 0) {
            $rtn = array('result' => 'FAIL_BYTE');
        }
    }
    if ($rtn['result'] === 0) {
        $rs = fnChkExistFile($db2con, $LIB, $FILE, $LIBLIST);
        if ($rs['result'] === true) {
            $rtn = array('result' => 'NOTEXIST');
        } else {
            $rtn = array('result' => 0, 'data' => $rs['data'][0]);
        }
    }
    return $rtn;
}
function chkMBR($db2con, $LIB, $FILE, $MBR) {
    $rtn = array('result' => 0);
    if ($rtn['result'] === 0) {
        if (!checkMaxLen($MBR, 10)) {
            $rtn = array('result' => 'FAIL_MAXLEN');
        }
    }
    if ($rtn['result'] === 0) {
        $byte = checkByte($MBR);
        if ($byte[1] > 0) {
            $rtn = array('result' => 'FAIL_BYTE');
        }
    }
    if ($rtn['result'] === 0) {
        $rs = fnChkExistMBR($db2con, $LIB, $FILE, $MBR);
        if ($rs['result'] === true) {
            $rtn = array('result' => 'NOTEXIST');
        } else {
            $rtn = array('result' => 0, 'data' => $rs['data'][0]);
        }
    }
    return $rtn;
}
function fnChkExistMBR($db2con, $LIB, $FILE, $MBR) {
    $data = array();
    $params = array();
    $strSQL.= '    SELECT ';
    $strSQL.= '        A.SYS_DNAME, ';
    $strSQL.= '        A.SYS_TNAME, ';
    $strSQL.= '        A.SYS_MNAME, ';
    $strSQL.= '        A.PARTNBR, ';
    $strSQL.= '        A.LABEL ';
    $strSQL.= '    FROM ';
    $strSQL.= '        QSYS2/SYSPSTAT A';
    $strSQL.= '    WHERE ';
    $strSQL.= '        A.TABLE_SCHEMA = ? ';
    $strSQL.= '    AND A.TABLE_NAME = ? ';
    $strSQL.= '    AND A.SYS_MNAME = ?';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
        e_log(db2_stmt_errormsg() . 'メンバーデータ取得：' . $strSQL);
    } else {
        $params = array($LIB, $FILE, $MBR);
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
            e_log(db2_stmt_errormsg() . 'メンバーデータ取得：' . $strSQL . print_r($params, true));
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
        if (count($data) === 0) {
            $data = array('result' => true);
        } else {
            $data = array('result' => 'ISEXIST', 'data' => $data);
        }
    }
    return $data;
}
function fnChkExistFile($db2con, $LIB, $FILE, $LIBLIST) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT A.SYSTEM_TABLE_NAME ';
    $strSQL.= '      , A.SYSTEM_TABLE_SCHEMA ';
    $strSQL.= '      , A.TABLE_TEXT';
    $strSQL.= ' FROM  ';
    $strSQL.= '    QSYS2/SYSTABLES A ';
    $strSQL.= ' WHERE A.SYSTEM_TABLE_SCHEMA = ? ';
    $strSQL.= ' AND A.SYSTEM_TABLE_NAME = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        if ($LIB === '*USRLIBL' || $LIB === '*LIBL') {
            foreach ($LIBLIST as $value) {
                $params = array($value, $FILE);
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $data = array('result' => 'FAIL_SEL');
                } else {
                    while ($row = db2_fetch_assoc($stmt)) {
                        $data[] = $row;
                    }
                    if (count($data) > 0) {
                        break;
                    }
                }
            }
        } else {
            $params = array($LIB, $FILE);
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array('result' => 'FAIL_SEL');
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
        }
        if (count($data) === 0) {
            $data = array('result' => true);
        } else {
            $data = array('result' => 'ISEXIST', 'data' => $data);
        }
    }
    return $data;
}
//参照ファイルチェック
function chkRefFileData($db2con, $PFILEINFO, $FILELIST, $LIBLIST, $REFFILE, $LIBSNM, $LIBSDB) {
	e_log('PRYFILINFO data**'.print_r($PFILEINFO,true));
    $rtn = 0;
    $focus = '';
    $refFileData = array();
    if (count($REFFILE) > 0) {
        foreach ($REFFILE as $RFILVAL) {
            $reffileinfo = array();
            if ($RFILVAL['RTRFIL'] !== '') {
                $RLIB = $RFILVAL['RTRLIB'];
                $RFIL = $RFILVAL['RTRFIL'];
                $RMBR = $RFILVAL['RTRMBR'];
                $RJTYP = $RFILVAL['JOINTYP'];
                $RTEXT = $RFILVAL['RTEXT'];
                if ($RLIB === '') {
                    $RLIB = '*USRLIBL';
                }
                if ($rtn === 0) {
                    if ($RLIB === '' || strtoupper($RLIB) === '*LIBL' || strtoupper($RLIB) === '*USRLIBL') {
                        $RLIB = strtoupper($RLIB);
                    } else {
                        $rs = chkLIB($db2con, $RLIB, $LIBLIST, $LIBSNM, $LIBSDB);
                         
                        if ($rs !== 0) {
                            
                            $rtn = showMsg($rs, array('参照ライブラリー'));
                            $focus = 'RTRLIB' . $RFILVAL['REFIDX'] . 'LIBNM';
                            break;
                        }
                    }
                }
                if ($rtn === 0) {
                    $rs = chkFile($db2con, $RLIB, $RFIL, $LIBLIST);
                    if ($rs['result'] !== 0) {
                        $rtn = showMsg($rs['result'], array('参照ファイル'));
                        $focus = 'RTRFIL' . $RFILVAL['REFIDX'] . 'FILENM';
                        break;
                    } else {
                        $REFFILINFO = $rs['data'];
                        // ファイルリスト作成
                        $fileinfo = array('SYSTEM_TABLE_SCHEMA' => $REFFILINFO['SYSTEM_TABLE_SCHEMA'], 'SYSTEM_TABLE_NAME' => $REFFILINFO['SYSTEM_TABLE_NAME'], 'TABLEID' => 'S' . $RFILVAL['REFIDX']);
                        $FILELIST[$fileinfo['TABLEID']] = $fileinfo;
                    }
                }
                // プライマリ．メンバーチェック
                if ($rtn === 0) {
                    if ($RMBR === '' || strtoupper($RMBR) === '*FIRST' || strtoupper($RMBR) === '*LAST') {
                        $RMBR = strtoupper($RMBR);
                    } else {
                        $rs = chkMBR($db2con, $fileinfo['SYSTEM_TABLE_SCHEMA'], $fileinfo['SYSTEM_TABLE_NAME'], $RMBR);
                        if ($rs['result'] !== 0) {
                            $rtn = showMsg($rs['result'], array('参照メンバー'));
                            $focus = 'RTRMBR' . $RFILVAL['REFIDX'] . 'FILMBR';
                            break;
                        }
                    }
                }
                if ($rtn === 0) {
                    if ($RJTYP === null || $RJTYP === '') {
                        $rtn = showMsg('FAIL_SLEC', array('結合タイプ'));
                        $focus = 'JOINTYPE' . $RFILVAL['REFIDX'];
                        break;
                    }
                }
                if ($rtn === 0) {
                    if ($RTEXT === '') {
                        $RTEXT = $REFFILINFO['TABLE_TEXT'];
                    } else {
                        if (!checkMaxLen($RTEXT, 50)) {
                            $rtn = showMsg('FAIL_MAXLEN', array('記述'));
                            $focus = 'RTEXT' . $RFILVAL['REFIDX'];
                        }
                    }
                }
                if ($rtn === 0) {
                    $reffileinfo['REFIDX'] = $RFILVAL['REFIDX'];
                    $reffileinfo['RTRLIB'] = $RLIB;
                    $reffileinfo['RTRFIL'] = $RFIL;
                    $reffileinfo['RTRMBR'] = $RMBR;
                    $reffileinfo['JOINTYP'] = $RJTYP;
                    $reffileinfo['RTEXT'] = $RTEXT;
                }
                if ($rtn === 0) {
                    $REFFLD = $RFILVAL[REFFIELDARR];
                    $row = 1;
                    $kChk = 0;
                    foreach ($REFFLD as $key => $value) {
                        if (substr($key, 0, 1) === 'P' && $value <> '') {
                            $id = substr($key, 4);
                            if ($id > $row) {
                                $rtn = showMsg('FAIL_ASGN', array('キー'));
                                $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KP' . substr($key, 4);
                                break;
                            } else if ($id == $row) {
                                $row = $id + 1;
                            }
                            $kChk = 1;
                        }
                    }
                    if ($rtn === 0) {
                        $row = $row - 1;
                        if ($row > REFFLDMAX) {
                            $rtn = showMsg('結合フィールドの最大数は' . REFFLDMAX . '項目です。');
                        }
                    }
                }
                if ($rtn === 0) {
                    if ($RJTYP !== '9') {
                        if ($kChk === 0) {
                            $rtn = showMsg('FAIL_REQ', array(array('結合元', 'フィールド')));
                            $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KP1';
                        }
                    }
                }
                if ($rtn === 0) {
                    $PKEY = array();
                    $RKEY = array();
                    foreach ($REFFLD as $key => $value) {
                        if ($value <> '' || $value <> null) {
                            if (substr($key, 0, 1) === 'P') {
                                $valueArr = explode(".", $value);
                                if (count($valueArr) === 2) {
                                    if (substr($valueArr[0], 0, 1) === 'S') {
                                        if ($RFILVAL['REFIDX'] == substr($valueArr[0], 1)) {
                                            //$rtn = showMsg('結合先ファイルのフィールドを結合元ファイルのフィールドとして設定できません。');
                                            $rtn = showMsg('FAIL_CONFI', array(array('結合先', 'ファイル', 'フィールド', '結合元', 'ファイル', 'フィールド。')));
                                            $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KP' . substr($key, 4);
                                            break;
                                        }
                                    }
                                } else if (count($valueArr) === 1) {
                                    $valueArr[0] = 'P';
                                    $valueArr[1] = $value;
                                    $REFFLD[$key] = join('.', $valueArr);
                                } else {
                                    $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KP' . substr($key, 4);
                                    //$rtn = showMsg('結合元フィールドのファイル指定の記述に誤りがあります。');
                                    break;
                                }
                                if ($rtn === 0) {
                                    $isKey = array_key_exists($valueArr[0], $FILELIST);
                                    if ($isKey !== false) {
                                        $value = $valueArr[1];
                                        $prs = fnChkExistFld($db2con, $value, $FILELIST[$valueArr[0]], $LIBLIST);
                                        if ($prs['result'] === true) {
                                            $rtn = showMsg('NOTEXIST', array(array('結合元', 'フィールド')));
                                            $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KP' . substr($key, 4);
                                            break;
                                        } else {
                                            $PKEY[$key] = $prs['data'][0];
                                        }
                                    } else {
                                        //$rtn = showMsg('結合元フィールドのファイル指定が正しくありません。');
                                        $rtn = showMsg('FAIL_ASGN', array(array('結合元', 'フィールド', 'ファイル')));
                                        $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KP' . substr($key, 4);
                                        break;
                                    }
                                }
                            } else if (substr($key, 0, 1) === 'R') {
								if($REFFLD['CFLG' . substr($key, 4) ] <> '1'){
			                         $valueArr = explode(".", $value);
			                         if (count($valueArr) === 2) {
			                             if (substr($valueArr[0], 0, 1) === 'S') {     
			                                 if ($RFILVAL['REFIDX'] != substr($valueArr[0], 1)) {
			                                     //$rtn = showMsg('結合先フィールドのファイル指定は自分のファイルのみ可能です。');
			                                     $rtn = showMsg('FAIL_ONESELF', array(array('結合先', 'フィールド', 'ファイル')));
			                                     $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KR' . substr($key, 4);
			                                     break;
			                                 } else {
			                                     $value = $valueArr[1];  
			                                 }
			                             } else {
			                                 //$rtn = showMsg('結合先フィールドのファイル指定は自分のファイルのみ可能です。');
			                                 $rtn = showMsg('FAIL_ONESELF', array(array('結合先', 'フィールド', 'ファイル')));
			                                 $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KR' . substr($key, 4);
			                                 break;
			                             }
			                         } else if (count($valueArr) === 1) {
			                             $value = $valueArr[0];
			                             $valueArr[0] = 'S' . $RFILVAL['REFIDX'];
			                             $valueArr[1] = $value;
			                             $REFFLD[$key] = join('.', $valueArr);
			                         } else {
			                             //$rtn = showMsg('結合先フィールドのファイル指定の記述に誤りがあります。');
			                             $rtn = showMsg('FAIL_DESC', array(array('結合先', 'フィールド', 'ファイル')));
			                             $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KR' . substr($key, 4);
			                             break;    
			                         }
									 if ($rtn === 0) {
	                                    $rrs = fnChkExistFld($db2con, $value, $REFFILINFO, $LIBLIST);
	                                    if ($rrs['result'] === true) {
	                                        $rtn = showMsg('NOTEXIST', array(array('結合先', 'フィールド')));
	                                        $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KR' . substr($key, 4);
	                                        break;
	                                    } else {
	                                        $RKEY[$key] = $rrs['data'][0];
	                                    }
                                	}
								}else{ //Check Condition when checkbox is checked(START)

				                         $valueArr = explode(".", $value);
				                         if (count($valueArr) === 2) {
										 	 $value = $valueArr[1];
				                             if (substr($valueArr[0], 0, 1) === 'S') {     
				                                 if ($RFILVAL['REFIDX'] == substr($valueArr[0], 1)) {
						                            $rtn = showMsg('FAIL_BASE_FUNCFLG',array('指定したフィールド情報'));//'指定したフィールド情報が取得できません';
						                            $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KR' . substr($key, 4);
						                            break;
											 	 }
												 else{
											 			$RKEY[$key] = $REFFLD['RKEY' . substr($key, 4) ];
												 	
												 } 
				                             } 
										}else{
										 	$rrs = fnChkExistFld($db2con, strtoupper($value), $PFILEINFO, $LIBLIST);
		                                    if ($rrs['result'] !== true) {
                                				$rtn = showMsg('FAIL_BASE_FUNCFLG',array('指定したフィールド情報'));//'指定したフィールド情報が取得できません';
		                                        $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KR' . substr($key, 4);
		                                        break;
		                                    } else {
										 		$RKEY[$key] = $REFFLD['RKEY' . substr($key, 4) ];
		                                    }
										}
									  	
								}//Check Condition when checkbox is checked(END)                                
                            }     
                        } else {
                            if (subStr($key, 0, 5) !== 'JCOND') {
                                if (substr($key, 0, 1) === 'P' && $REFFLD['RKEY' . substr($key, 4) ] <> '') {
                                    $rtn = showMsg('FAIL_REQ', array(array('結合元', 'フィールド')));
                                    $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KP' . substr($key, 4);
                                    break;
                                }
                                if (substr($key, 0, 1) === 'R' && $REFFLD['PKEY' . substr($key, 4) ] <> '') {
                                    $rtn = showMsg('FAIL_REQ', array(array('結合先', 'フィールド')));
                                    $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KR' . substr($key, 4);
                                    break;
                                }
                            } else {
                                if ($REFFLD['RKEY' . substr($key, 5) ] <> '' && $REFFLD['PKEY' . substr($key, 5) ] <> '') {
                                    $rtn = showMsg('FAIL_REQ', array('結合条件'));
                                    $focus = 'CMBJCOND' . $RFILVAL['REFIDX'] . '_' . substr($key, 5);
                                    break;
                                }
                            }
                        }
                    }
                }
                if ($rtn === 0) {
					$isInt = array();
                    if (count($PKEY) > 0 && count($RKEY)) {

                        /*foreach($PKEY as $key => $value){
                            if($value['DDS_TYPE'] === 'P' || $value['DDS_TYPE'] === 'S' || $value['DDS_TYPE'] === 'B'){
                                if(($RKEY['RKEY'.substr($key,4)]['DDS_TYPE'] === 'P') || ($RKEY['RKEY'.substr($key,4)]['DDS_TYPE'] === 'S') || ($RKEY['RKEY'.substr($key,4)]['DDS_TYPE'] === 'B')){
                                }else{
                                    //$rtn = showMsg('FAIL_CMP',array('結合フィールドのデータタイプ'));
                                    $rtn = showMsg('FAIL_CMP',array(array('結合フィールド','データタイプ')));
                                    $focus = 'RFIL'.$RFILVAL['REFIDX'].'KP'.substr($key,4);
                                    break;
                                }
                            }
                            if($RKEY['RKEY'.substr($key,4)]['DDS_TYPE'] === 'P' || $RKEY['RKEY'.substr($key,4)]['DDS_TYPE'] === 'S' || $RKEY['RKEY'.substr($key,4)]['DDS_TYPE'] === 'B' ){
                                if($value['DDS_TYPE'] === 'P' || $value['DDS_TYPE'] === 'S' || $value['DDS_TYPE'] === 'B'){
                                }else{
                                    //$rtn = showMsg('FAIL_CMP',array('結合フィールドのデータタイプ'));
                                    $rtn = showMsg('FAIL_CMP',array(array('結合フィールド','データタイプ')));
                                    $focus = 'RFIL'.$RFILVAL['REFIDX'].'KP'.substr($key,4);
                                    break;
                                }
                            }
                        }*/
                        foreach ($PKEY as $key => $value) {
                            $REFFLD['PLEN' . substr($key, 4) ] = $value['LENGTH'];
                            $REFFLD['PTYP' . substr($key, 4) ] = $value['DDS_TYPE'];
							if($REFFLD['CFLG' . substr($key, 4) ] <> '1'){//when checkbox is not checked
	                            $REFFLD['RLEN' . substr($key, 4) ] = $RKEY['RKEY' . substr($key, 4) ]['LENGTH'];
	                            $REFFLD['RTYP' . substr($key, 4) ] = $RKEY['RKEY' . substr($key, 4) ]['DDS_TYPE'];
							}
							else{//when checkbox is checked
								if($value['DDS_TYPE'] === 'P' || $value['DDS_TYPE'] === 'S' || $value['DDS_TYPE'] === 'B'){
									if(!is_numeric($RKEY['RKEY' . substr($key, 4) ])){
                                    	$rtn = showMsg('FAIL_CMP',array(array('結合フィールド','データタイプ')));
		                                $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KR' . substr($key, 4);
										break;
									}
								}
								if(strlen($RKEY['RKEY' . substr($key, 4) ]) > $value['LENGTH'] ){
									$rtn = showMsg('FAIL_CMP', array(array('結合先', 'フィールド長さ')));
	                                $focus = 'RFIL' . $RFILVAL['REFIDX'] . 'KR' . substr($key, 4);
	                                break;
								}
								$REFFLD['RLEN' . substr($key, 4) ] = $value['LENGTH'];
	                            $REFFLD['RTYP' . substr($key, 4) ] = $value['DDS_TYPE'];
							}
                        }
                    }
                }
                if ($rtn === 0) {
                    $reffileinfo['REFFIELDARR'] = $REFFLD;
                }
                if ($rtn === 0) {
                    $refFileData[] = $reffileinfo;
                }
            }
        }
        if (count($refFileData) > REFFILEMAX) {
            $rtn = showMsg('参照できるファイルの最大数は' . REFFILEMAX . 'ファイルです。');
        }
    }
    $rtnarr = array('RTN' => $rtn, 'FOCUS' => $focus, 'REFFILE' => $refFileData);
    return $rtnarr;
}
function fnChkExistFld($db2con, $Field, $FILEINFO, $LIBLIST) {
    $TABLE_SCHEMA = $FILEINFO['SYSTEM_TABLE_SCHEMA'];
    $TABLE_NAME = $FILEINFO['SYSTEM_TABLE_NAME'];
    $COLNAME = $Field;
    $data = array();
    $params = array();
    $strSQL = '';
    $strSQL = ' SELECT A.SYSTEM_COLUMN_NAME  AS COLUMN_NAME ';
    $strSQL.= '       ,A.LENGTH ';
    $strSQL.= '       ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG==='1'){
        $strSQL.= '       ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL.= '       ,A.COLUMN_HEADING ';
    }
    $strSQL.= '       ,CASE A.DDS_TYPE';
    $strSQL.= '         WHEN \'H\' THEN \'A\'';
    $strSQL.= '         WHEN \'J\' THEN \'0\'';
    $strSQL.= '         WHEN \'E\' THEN \'0\'';
    $strSQL.= '         ELSE A.DDS_TYPE ';
    $strSQL.= '         END AS DDS_TYPE';
    $strSQL.= '       ,A.ORDINAL_POSITION ';
    $strSQL.= ' FROM ' . SYSCOLUMN2 . ' A ';
    $strSQL.= ' WHERE A.TABLE_SCHEMA = ? ';
    //$strSQL .=' AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\')';
    //    $strSQL .=' AND A.DDS_TYPE NOT IN(\'5\')';
    $strSQL.= ' AND A.TABLE_NAME = ? ';
    $strSQL.= ' AND UPPER(A.SYSTEM_COLUMN_NAME) = ? ';
    $params = array($TABLE_SCHEMA, $TABLE_NAME, $COLNAME);

    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) > 0) {
                $data = array('result' => 'ISEXIST', 'data' => $data);
            } else {
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
/*
 * ファイル選択画面のプライマリと参照ファイル情報取得
*/
function getsqlFile($BASEFILEINFO) {
    $filedata = array();
    $filedata['LIBLST'] = join(' ', $BASEFILEINFO['SELLIBLIST']);
    $filedata['PLBL'] = $BASEFILEINFO['D1FILLIB'];
    $filedata['PTAB'] = $BASEFILEINFO['D1FILE'];
    $filedata['RCNT'] = count($BASEFILEINFO['REFFILE']);
    for ($rFilCnt = 1;$rFilCnt <= $filedata['RCNT'];$rFilCnt++) {
        $refFil = $BASEFILEINFO['REFFILE'][$rFilCnt - 1];
        $filedata['RIDX' . $rFilCnt] = $refFil['REFIDX'];
        $filedata['SLIB' . $rFilCnt] = $refFil['RTRLIB'];
        $filedata['STAB' . $rFilCnt] = $refFil['RTRFIL'];
        $filedata['JTYP' . $rFilCnt] = $refFil['JOINTYP'];
        $jkeycnt = 0;
        foreach ($refFil['REFFIELDARR'] as $key => $value) {
            if ($value !== '') {
                if (substr($key, 0, 5) === 'JCOND') {
                    $filedata['JCOND' . $rFilCnt . _ . substr($key, 5) ] = $value;
                } else if (substr($key, 0, 4) === 'CFLG') {//add checkbox value for data condition
                    $filedata['CFLG' . $rFilCnt . _ . substr($key, 4) ] = $value;
				}					
				else {
                    $jkeycnt = substr($key, 4);
                    if ($key[0] === 'P') {
                        if (substr($key, 1, 3) === 'KEY') {
                            $filedata['PFLD' . $rFilCnt . _ . $jkeycnt] = $value;
                        } else if (substr($key, 1, 3) === 'LEN') {
                            $filedata['PLEN' . $rFilCnt . _ . $jkeycnt] = $value;
                        } else if (substr($key, 1, 3) === 'TYP') {
                            $filedata['PTYP' . $rFilCnt . _ . $jkeycnt] = $value;
                        }
                    } else if ($key[0] === 'R') {
                        if (substr($key, 1, 3) === 'KEY') {
                            $filedata['RFLD' . $rFilCnt . _ . $jkeycnt] = $value;
                        } else if (substr($key, 1, 3) === 'LEN') {
                            $filedata['RLEN' . $rFilCnt . _ . $jkeycnt] = $value;
                        } else if (substr($key, 1, 3) === 'TYP') {
                            $filedata['RTYP' . $rFilCnt . _ . $jkeycnt] = $value;
                        }
                    }
                }
            }
        }
        $filedata['KCNT' . $rFilCnt] = $jkeycnt;
    }
    return $filedata;
}
function chkValidRefFil($BASEFILEINFO) {
	$paramJSQL = array();
    $RDBNM = $BASEFILEINFO['RDBNM'];
    $strSQL = '';
    $data = array();
    $fileInfo = getsqlFile($BASEFILEINFO);
    $rs = true;
    $strSQL.= ' SELECT  (1) ';
    $strSQL.= '    FROM  ';
    if ($fileInfo['PLBL'] === '*USRLIBL' || $fileInfo['PLBL'] === '*LIBL') {
        $strSQL.= $fileInfo['PTAB'] . ' AS P ';
    } else {
        $strSQL.= $fileInfo['PLBL'] . '/' . $fileInfo['PTAB'] . ' AS P ';
    }
    if ($fileInfo['RCNT'] > 0) {
        for ($rcnt = 1;$rcnt <= $fileInfo['RCNT'];$rcnt++) {
            if ($fileInfo['JTYP' . $rcnt] == 1) {
                $strSQL.= '   JOIN   ';
            } else if ($fileInfo['JTYP' . $rcnt] == 2) {
                $strSQL.= '   LEFT JOIN   ';
            } else if ($fileInfo['JTYP' . $rcnt] == 3) {
                $strSQL.= '   RIGHT JOIN   ';
            } else if ($fileInfo['JTYP' . $rcnt] == 4) {
                $strSQL.= '   LEFT JOIN   ';
            } else if ($fileInfo['JTYP' . $rcnt] == 5) {
                $strSQL.= '   RIGHT JOIN   ';
            } else if ($fileInfo['JTYP' . $rcnt] == 6) {
                $strSQL.= '   JOIN   ';
            } else if ($fileInfo['JTYP' . $rcnt] == 7) {
                $strSQL.= '   FULL JOIN   ';
            } else if ($fileInfo['JTYP' . $rcnt] == 8) {
                $strSQL.= '   FULL JOIN   ';
            } else if ($fileInfo['JTYP' . $rcnt] == 9) {
                $strSQL.= '   CROSS JOIN   ';
            }
            if ($fileInfo['SLIB' . $rcnt] === '*USRLIBL' || $fileInfo['SLIB' . $rcnt] === '*LIBL') {
                $strSQL.= '    ' . $fileInfo['STAB' . $rcnt];
            } else {
                $strSQL.= '    ' . $fileInfo['SLIB' . $rcnt] . '/' . $fileInfo['STAB' . $rcnt];
            }
            $strSQL.= '    AS S' . $fileInfo['RIDX' . $rcnt];
            for ($kcnt = 1;$kcnt <= $fileInfo['KCNT' . $rcnt];$kcnt++) {
                if ($kcnt === 1) {
                    $strSQL.= '    ON    ';
                } else {
                    $strSQL.= '    AND   ';
                }
                //check for datatype and cast for length
                $refPfnm = $fileInfo['PFLD' . $rcnt . _ . $kcnt];
                $refRfnm = $fileInfo['RFLD' . $rcnt . _ . $kcnt];
                if ($fileInfo['PTYP' . $rcnt . _ . $kcnt] === $fileInfo['RTYP' . $rcnt . _ . $kcnt]) {
                    switch ($fileInfo['PTYP' . $rcnt . _ . $kcnt]) {
                        case 'L':
                            switch ($fileInfo['JCOND' . $rcnt . _ . $kcnt]) {
                                case 'LIKE':
                                case 'NLIKE':
                                case 'LLIKE':
                                case 'LNLIKE':
                                case 'RLIKE':
                                case 'RNLIKE':
                                    $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(10))';
                                    $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(10))';
                                break;
                                default:
                                break;
                            }
                        break;
                        case 'T':
                            switch ($fileInfo['JCOND' . $rcnt . _ . $kcnt]) {
                                case 'LIKE':
                                case 'NLIKE':
                                case 'LLIKE':
                                case 'LNLIKE':
                                case 'RLIKE':
                                case 'RNLIKE':
                                    $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(8))';
                                    $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(8))';
                                break;
                                default:
                                break;
                            }
                        break;
                        case 'Z':
                            switch ($fileInfo['JCOND' . $rcnt . _ . $kcnt]) {
                                case 'LIKE':
                                case 'NLIKE':
                                case 'LLIKE':
                                case 'LNLIKE':
                                case 'RLIKE':
                                case 'RNLIKE':
                                    $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(26))';
                                    $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(26))';
                                break;
                                default:
                                break;
                            }
                        break;
                        default://check type when checkbox is checked
							switch ($fileInfo['CFLG' . $rcnt . _ . $kcnt]) {
	                            case '1':
									switch ($fileInfo['RTYP' . $rcnt . _ . $kcnt]) {
			                            case 'P':
			                            case 'S':
			                            case 'B':
										break;
			                            default:
					                    	$refRfnm = '\''. $refRfnm .'\'';
			                            break;
	                        		}
	                            break;
	                            default:
	                            break;
                            }
                        break;

                    }
                } else {
                    switch ($fileInfo['PTYP' . $rcnt . _ . $kcnt]) {

                        case 'L':
                            $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(10))';
                            $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(' . $fileInfo['RLEN' . $rcnt . _ . $kcnt] . '))';
                        break;
                        case 'T':
                            $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(8))';
                            $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(' . $fileInfo['RLEN' . $rcnt . _ . $kcnt] . '))';
                        break;
                        case 'Z':
                            $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(26))';
                            $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(' . $fileInfo['RLEN' . $rcnt . _ . $kcnt] . '))';
                        break;
                        case 'P':
                        case 'S':
                        case 'B':
                            switch ($fileInfo['RTYP' . $rcnt . _ . $kcnt]) {
                                case 'L':
                                    $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $fileInfo['PLEN' . $rcnt . _ . $kcnt] . '))';
                                    $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(10))';
                                break;
                                case 'T':
                                    $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $fileInfo['PLEN' . $rcnt . _ . $kcnt] . '))';
                                    $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(8))';
                                break;
                                case 'Z':
                                    $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $fileInfo['PLEN' . $rcnt . _ . $kcnt] . '))';
                                    $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(26))';
                                break;
                                case 'P':
                                case 'S':
                                case 'B':
                                break;
                                default:
                                    $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $fileInfo['PLEN' . $rcnt . _ . $kcnt] . '))';
                                break;
                            }
                        break;
                        default:
                            switch ($fileInfo['RTYP' . $rcnt . _ . $kcnt]) {

                                case 'L':
                                    $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $fileInfo['PLEN' . $rcnt . _ . $kcnt] . '))';
                                    $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(10))';
                                break;
                                case 'T':
                                    $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $fileInfo['PLEN' . $rcnt . _ . $kcnt] . '))';
                                    $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(8))';
                                break;
                                case 'Z':
                                    $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $fileInfo['PLEN' . $rcnt . _ . $kcnt] . '))';
                                    $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(26))';
                                break;
                                case 'P':
                                case 'S':
                                case 'B':
                                    $refRfnm = 'CAST(' . $refRfnm . ' AS  CHAR(' . $fileInfo['RLEN' . $rcnt . _ . $kcnt] . '))';
                                break;
                                default:
                                break;
                            }
                        break;
                    }
                    /* if($fileInfo['PTYP'.$rcnt._.$kcnt] === 'P' || $fileInfo['PTYP'.$rcnt._.$kcnt] === 'S' || $fileInfo['PTYP'.$rcnt._.$kcnt] === 'B'){
                        if($fileInfo['RTYP'.$rcnt._.$kcnt] === 'P' || $fileInfo['RTYP'.$rcnt._.$kcnt] === 'S' || $fileInfo['RTYP'.$rcnt._.$kcnt] === 'B'){
                            $reffnm = $fileInfo['RFLD'.$rcnt._.$kcnt];
                        }else{
                            $reffnm = 'CAST('.$fileInfo['RFLD'.$rcnt._.$kcnt].' AS DECIMAL('.$fileInfo['RLEN'.$rcnt._.$kcnt].'))';
                        }
                    }else{
                        if($fileInfo['RTYP'.$rcnt._.$kcnt] === 'P' || $fileInfo['RTYP'.$rcnt._.$kcnt] === 'S' || $fileInfo['RTYP'.$rcnt._.$kcnt] === 'B'){
                            $reffnm = 'CAST('.$fileInfo['RFLD'.$rcnt._.$kcnt].' AS CHAR('.$fileInfo['RLEN'.$rcnt._.$kcnt].'))';
                        }else{
                            $reffnm = $fileInfo['RFLD'.$rcnt._.$kcnt];
                        }
                    } */
                }
                /*switch ($fileInfo['JCOND' . $rcnt . _ . $kcnt]) {
                    case 'EQ':
						e_log('JCOND flag'.$fileInfo['JCOND' . $rcnt . _ . $kcnt]);
                        $strSQL.= $refPfnm . ' = ' . ' ? ';
						array_push($paramJSQL, $refRfnm);
                    	break;
                    case 'NE':
                        $strSQL.= $refPfnm . ' <> ' . ' ? ';
						array_push($paramJSQL, $refRfnm);
                    	break;
                    case 'GE':
                        $strSQL.= $refPfnm . ' >= ' . ' ? ';
						array_push($paramJSQL, $refRfnm);
                    	break;
                    case 'GT':
                        $strSQL.= $refPfnm . ' < ' . ' ? ';
						array_push($paramJSQL, $refRfnm);
                   		break;
                    case 'LE':
                        $strSQL.= $refPfnm . ' <= ' . ' ? ';
						array_push($paramJSQL, $refRfnm);
                    	break;
                    case 'LT':
                        $strSQL.= $refPfnm . ' < ' . ' ? ';
						array_push($paramJSQL, $refRfnm);
                    	break;
                    case 'LIKE':
                        $strSQL.= $refPfnm . ' LIKE ' . 'concat(concat(\'%\',' . ' ? ' . '),\'%\')';
						array_push($paramJSQL, $refRfnm);
                    	break;
                    case 'LLIKE':
                        $strSQL.= $refPfnm . ' LIKE ' . 'concat(\'%\',' . ' ? ' . ')';
						array_push($paramJSQL, $refRfnm);
						break;
                    case 'RLIKE':
                        $strSQL.= $refPfnm . ' LIKE ' . 'concat(' . ' ? ' . ',\'%\')';
						array_push($paramJSQL, $refRfnm);
                  		break;
                    case 'NLIKE':
                        $strSQL.= $refPfnm . ' NOT LIKE ' . 'concat(concat(\'%\',' . ' ? ' . '),\'%\')';
						array_push($paramJSQL, $refRfnm);
                    	break;
                    case 'NLLIKE':
                        $strSQL.= $refPfnm . ' NOT LIKE ' . 'concat(\'%\',' . ' ? ' . ')';
						array_push($paramJSQL, $refRfnm);
                    	break;
                    case 'NRLIKE':
                        $strSQL.= $refPfnm . ' NOT LIKE ' . 'concat(' . ' ? ' . ',\'%\')';
						array_push($paramJSQL, $refRfnm);
                    	break;
                    default:
                        $strSQL.= $refPfnm . ' =  ? ';
						array_push($paramJSQL, $refRfnm);
                    	break;
                }*/
				switch ($fileInfo['JCOND' . $rcnt . _ . $kcnt]) {
                    case 'EQ':
                        $strSQL.= $refPfnm . ' = ' . $refRfnm . ' ';;
                    break;
                    case 'NE':
                        $strSQL.= $refPfnm . ' <> ' . $refRfnm . ' ';;
                    break;
                    case 'GE':
                        $strSQL.= $refPfnm . ' >= ' . $refRfnm . ' ';;
                    break;
                    case 'GT':
                        $strSQL.= $refPfnm . ' < ' . $refRfnm . ' ';;
                    break;
                    case 'LE':
                        $strSQL.= $refPfnm . ' <= ' . $refRfnm . ' ';;
                    break;
                    case 'LT':
                        $strSQL.= $refPfnm . ' < ' . $refRfnm . ' ';;
                    break;
                    case 'LIKE':
                        $strSQL.= $refPfnm . ' LIKE ' . 'concat(concat(\'%\',' . $refRfnm . '),\'%\')';
                    break;
                    case 'LLIKE':
                        $strSQL.= $refPfnm . ' LIKE ' . 'concat(\'%\',' . $refRfnm . ')';
                    break;
                    case 'RLIKE':
                        $strSQL.= $refPfnm . ' LIKE ' . 'concat(' . $refRfnm . ',\'%\')';
                    break;
                    case 'NLIKE':
                        $strSQL.= $refPfnm . ' NOT LIKE ' . 'concat(concat(\'%\',' . $refRfnm . '),\'%\')';
                    break;
                    case 'NLLIKE':
                        $strSQL.= $refPfnm . ' NOT LIKE ' . 'concat(\'%\',' . $refRfnm . ')';
                    break;
                    case 'NRLIKE':
                        $strSQL.= $refPfnm . ' NOT LIKE ' . 'concat(' . $refRfnm . ',\'%\')';
                    break;
                    default:
                        $strSQL.= $refPfnm . ' = ' . $refRfnm . ' ';;
                    break;
                }
            }
        }
    }
    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
        $db2con = cmDb2ConLib($fileInfo['LIBLST']);
    } else {
        $db2con = cmDb2ConRDB($fileInfo['LIBLST']);
    }
    error_log('参照ファイルテスト'.$strSQL);  
    //e_log('Params array'.print_r($paramJSQL,true));

    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = false;
		$msg = db2_stmt_errormsg();
    } else {
        $r = db2_execute($stmt);
        if ($r === false) {
            $rs = false;
			$msg = db2_stmt_errormsg();
        } else {
            $row = db2_fetch_assoc($stmt);
            if ($row === false && db2_stmt_errormsg() !== '') {
                $rs = false;
				$msg = db2_stmt_errormsg();
            } else {
                $total_rows = db2_num_rows($stmt);
                for ($i = 0;$i < $total_rows;$i++) {
                    $row = db2_fetch_assoc($stmt);
                    if ($row === false && db2_stmt_errormsg() !== '') {
                        $rs = false;
						$msg = db2_stmt_errormsg();
                        break;
                    }
                }
            }
        }
    }
    cmDb2Close($db2con);
   // return $rs;
	$rtnArr = array(
             'RTN' => $rs ,
             'MSG' => $msg 
             );
    return $rtnArr;
}
/**
*-------------------------------------------------------* 
* クエリーグループが存在するかどうかをチェックすること
* 
* RESULT
*    01：存在する場合   true
*    02：存在ない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $QRYGID クエリーグループID
* 
*-------------------------------------------------------*
*/
function fnCheckDB2QRYG($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' SELECT COUNT(A.QRYGID) AS COUNT ' ;
    $strSQL .= ' FROM DB2QRYG AS A ' ;
    $strSQL .= ' WHERE A.QRYGID = ? ' ;
    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
          while($row = db2_fetch_assoc($stmt)){
                $totcount = $row['COUNT'];
            }
            if($totcount > 0){
                $rs = 'ISEXIST_GROUPID';
            }
        }
    }
    return $rs;
}


