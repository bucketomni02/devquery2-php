<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("chkRDB.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$RDBNM = (isset($_POST['RDBNM'])?$_POST['RDBNM']:'');
$SELLIBLIST = json_decode($_POST['SELLIBLIST'],true);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rs = chkRDB($RDBNM);
if($rs['RTN'] === 2){
    $rtn = 1;
    $msg = showMsg('設定した区画は接続できません。<br>区画の設定を確認してください。');
}else if($rs['RTN'] !== 0){
    $rtn = 1;
    $msg = $rs['MSG'];
}else{
    if(count($SELLIBLIST) === 0){
       $SELLIBLIST[] = 'QTEMP';
    }else{
        if(in_array('QTEMP', $SELLIBLIST) === false){
            $SELLIBLIST[] = 'QTEMP';
        }
    }
    if(count($SELLIBLIST) > LIBLMAX){
        
        $rtn = 1;
        $msg = showMsg('ライブラリーは'.LIBLMAX.'個まで選択可能です');
    }
}
cmDb2Close($db2con);


/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'SELLIBLIST' => $SELLIBLIST,
    'RDBNM'      => $RDBNM
);

echo(json_encode($rtn));


