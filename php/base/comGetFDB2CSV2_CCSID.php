<?php

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
$fdb2csv2 = array();
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
function fnGetFDB2CSV2CCSID($db2con,$QRYNM){
    $rtn = 0;
    $msg = '';
    $fdb2csv2 = array();
    $fdb2csv2FldNm = array();
    $rsfdb2csv1 = fnQryFDB2CSV1($db2con,$QRYNM);
    if($rsfdb2csv1['result'] !== true){
        $rtn = 1;
        $msg = $rsfdb2csv1['result'];
    }else{
        $fdb2csv1 = $rsfdb2csv1['data'];
        // ライブラリーリスト作成
        $LIBLIST = preg_split ("/\s+/", $fdb2csv1['D1LIBL'] );
        $libl = join(' ',$LIBLIST);
        $RDBNM   = $fdb2csv1['D1RDB'];
    }
    if($rtn === 0){
        $rsQryFile = fnGetQryFile($db2con,$QRYNM);
        if($rsQryFile['result'] !== true){
            $rtn = 1;
            $msg = $rsQryFile['result'];
        }else{
            $qryFileLst = $rsQryFile['data'];
        }
    }
    if($rtn === 0){
        if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME ){
            foreach($qryFileLst as $qryFile){
                $rs = fnGetCCSID($db2con,$QRYNM,$qryFile['RERSEQ'],$qryFile['RTRFIL'],$qryFile['RTRLIB'],$LIBLIST);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = $rs['result'];
                    break;
                }else{
                    foreach($rs['data'] as $rsVal){
                        array_push($fdb2csv2,$rsVal);
                    }
                    foreach($rs['FLDARR'] as $fldVal){
                        array_push($fdb2csv2FldNm,$fldVal);
                    }
                }
            }
        }else{
            foreach($qryFileLst as $qryFile){
                $rs = fnGetCCSIDRDB($db2con,$QRYNM,$RDBNM,$qryFile['RERSEQ'],$qryFile['RTRFIL'],$qryFile['RTRLIB'],$LIBLIST);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = $rs['result'];
                    break;
                }else{
                    foreach($rs['data'] as $rsVal){
                        array_push($fdb2csv2,$rsVal);
                    }
                    foreach($rs['FLDARR'] as $fldVal){
                        array_push($fdb2csv2FldNm,$fldVal);
                    }
                }
            }
        }
    }
    $rtnArr = array(
            'RTN' => $rtn,
            'MSG' => $msg,
            'DATA' => $fdb2csv2,
            'FLDDATA' => $fdb2csv2FldNm
        );
    return $rtnArr;
}

function fnGetSYSCOLCCSID($db2con,$LIBL,$FILNM,$FLDNM,$LIBLLIST){
    e_log('パラメータ LIBL：'.$LIBL.' FIL:'.$FILNM.'FLD:'.$FLDNM.'LIBLLSIT:'.print_r($LIBLLIST,true));
    $rtn = 0;
    $msg = '';
    $rsFile = fnChkExistFile($db2con,$LIBL,$FILNM,$LIBLLIST);
    if($rsFile['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rsFile['result'],array('ファイル'));
    }else {
        $FILEINFO = $rsFile['data'];
    }
    if($rtn === 0){
        $rs = fnChkExistFld ($db2con,$FLDNM,$FILEINFO,$LIBLLIST);
        if($rs['result'] === true){
             $rtn = 1;
             $msg = showMsg($rs['data'],array('フィールド'));
        }else{
            $FLDDATA = $rs['data'][0];
        }
    }
    $rtnArr = array(
            'RTN' => $rtn,
            'MSG' => $msg,
            'FILDATA' => $FILDATA,
            'FLDDATA' =>  $FLDDATA
        );
    return $rtnArr;
}

// FDB2CSV1から取得
function fnQryFDB2CSV1($db2con,$QRYNM){
    $strSQL  = '';
    $param   = array();
    $data    = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '     D1NAME, ';
    $strSQL .= '     D1FILE, ';
    $strSQL .= '     D1FILE, ';
    $strSQL .= '     D1LIBL, ';
    $strSQL .= '     D1RDB ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 ';
    $strSQL .= ' WHERE ';
    $strSQL .= '    D1NAME = ? ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
               $data = array('result' => 'NOTEXIST_GET'); 
            }else{
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data[0]);
            }
        }
    }
    return $data;
}
// FDB2CSV1+BQRYTBLのデータ取得
function fnGetQryFile($db2con,$QRYNM){
    $strSQL  = '';
    $params   = array();
    $data    = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '     D1NAME AS RTQRYN, ';
    $strSQL .= '     0 AS RERSEQ, ';
    $strSQL .= '     D1FILE AS RTRFIL, ';
    $strSQL .= '     D1FILLIB AS RTRLIB ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     D1NAME = ? ';
    $strSQL .= ' UNION ALL ';
    $strSQL .= ' SELECT ';
    $strSQL .= '     RTQRYN, ';
    $strSQL .= '     RTRSEQ, ';
    $strSQL .= '     RTRFIL, ';
    $strSQL .= '     RTRLIB ';
    $strSQL .= ' FROM ';
    $strSQL .= '     BREFTBL ';
    $strSQL .= ' WHERE ';
    $strSQL .= '    RTQRYN = ? ';

    $params = array($QRYNM,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
               $data = array('result' => 'NOTEXIST_GET'); 
            }else{
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data);
            }
        }
    }
    return $data;
}
// FDB2CSV2のCCSIDのデータ取得
function fnGetCCSID($db2con,$QRYNM,$FILID,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST){
    $data = array();
    $fieldArr = array();
    $params = array();
    $strSQL = '';
    //e_log(print_r($LIBLIST,true));
    $strSQL .=  '   SELECT ';
    $strSQL .=  '         FDB2CSV2DATA.SEQ            AS SEQ ';
    $strSQL .=  '       , SYSTABDATA.COLUMN_NAME      AS COLUMN_NAME ';
    $strSQL .=  '       , SYSTABDATA.LENGTH           AS LENGTH ';
    $strSQL .=  '       , SYSTABDATA.DDS_TYPE         AS DDS_TYPE ';
    $strSQL .=  '       , SYSTABDATA.NUMERIC_SCALE    AS NUMERIC_SCALE ';
    $strSQL .=  '       ,(CASE ';
    $strSQL .=  '           WHEN FDB2CSV2DATA.COLUMN_HEADING IS NULL ';
    $strSQL .=  '           AND FDB2CSV2DATA.COLUMN_NAME  IS NULL ';
    $strSQL .=  '           THEN SYSTABDATA.COLUMN_HEADING ';
    $strSQL .=  '           ELSE FDB2CSV2DATA.COLUMN_HEADING ';
    $strSQL .=  '         END) AS COLUMN_HEADING ';
    $strSQL .=  '       , FDB2CSV2DATA.FORMULA        AS FORMULA ';
    $strSQL .=  '       , FDB2CSV2DATA.SORTNO         AS SORTNO ';
    $strSQL .=  '       , FDB2CSV2DATA.SORTTYP        AS SORTTYP ';
    $strSQL .=  '       , FDB2CSV2DATA.EDTCD          AS EDTCD ';
    $strSQL .=  '       , SYSTABDATA.ORDINAL_POSITION AS ORDINAL_POSITION ';
    $strSQL .=  '       , SYSTABDATA.CCSID AS CCSID ';
    $strSQL .=  '   FROM  '; 
    $strSQL .=  '       (SELECT  ';
    $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
    $strSQL .=  '          ,A.LENGTH '; 
    $strSQL .=  '          ,CASE A.DDS_TYPE ';
    $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
    $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
    $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
    $strSQL .=  '            ELSE A.DDS_TYPE '; 
    $strSQL .=  '            END AS DDS_TYPE ';
    $strSQL .=  '          ,A.NUMERIC_SCALE '; 
    if(SYSCOLCFLG==='1'){
        $strSQL .=  '          ,A.COLUMN_TEXT AS COLUMN_HEADING '; 
    }else{
        $strSQL .=  '          ,A.COLUMN_HEADING '; 
    }
    $strSQL .=  '          ,A.ORDINAL_POSITION '; 
    $strSQL .=  '          ,A.CCSID AS CCSID ';
    $strSQL .=  '       FROM '. SYSCOLUMN2 .' A '; 
    $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
    $strSQL .=  '       AND A.TABLE_NAME = ? '; 
//    $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
    $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ) SYSTABDATA ';
    $strSQL .=  '   LEFT JOIN ';
    $strSQL .=  '       (SELECT  ';
    $strSQL .=  '            (CASE A.D2CSEQ ';
    $strSQL .=  '            WHEN 0 THEN 99999 '; 
    $strSQL .=  '            ELSE A.D2CSEQ ';
    $strSQL .=  '            END) '; 
    $strSQL .=  '            AS SEQ '; 
    $strSQL .=  '           ,A.D2FLD  AS COLUMN_NAME ';
    $strSQL .=  '           ,A.D2HED  AS COLUMN_HEADING '; 
    $strSQL .=  '           ,\'\'     AS FORMULA ';
    $strSQL .=  '           ,CASE A.D2RSEQ ';
    $strSQL .=  '             WHEN 0 THEN 99999 ';
    $strSQL .=  '             ELSE A.D2RSEQ '; 
    $strSQL .=  '             END AS SORTNO ';
    $strSQL .=  '           ,A.D2RSTP AS SORTTYP '; 
    $strSQL .=  '           ,A.D2WEDT AS EDTCD ';
    $strSQL .=  '       FROM FDB2CSV2 A ';
    $strSQL .=  '       WHERE A.D2NAME = ?';
    $strSQL .=  '       AND   A.D2FILID = ? ';
    $strSQL .=  '       ORDER BY A.D2CSEQ,A.D2RSEQ ) FDB2CSV2DATA ';
    $strSQL .=  '   ON SYSTABDATA.COLUMN_NAME = FDB2CSV2DATA.COLUMN_NAME ';
    $strSQL .=  '   ORDER BY SEQ ,SORTNO, ORDINAL_POSITION ';
    //e_log($strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log(db2_stmt_errormsg());
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL' || $TABLE_SCHEMA === ''){
            foreach($LIBLIST as $value){
                $params = array($value,$TABLE_NAME,$QRYNM,$FILID);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    e_log(db2_stmt_errormsg());
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        if($row['SORTNO'] == 99999){
                            $row['SORTNO'] = '';
                        }
                        if($row['SEQ'] == 99999){
                            $row['SEQ'] = '';
                        }
                        $fieldArr[] = cmMer($row['COLUMN_NAME']).'_'.$FILID;
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
            //$data = umEX($data,true);
            $data = umEx($data);
            $data = array('result' => true,'data' => $data,'FLDARR'=> $fieldArr);
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME,$QRYNM,$FILID);
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row['SORTNO'] == 99999){
                        $row['SORTNO'] = '';
                    }
                    if($row['SEQ'] == 99999){
                        $row['SEQ'] = '';
                    }
                    $fieldArr[] = cmMer($row['COLUMN_NAME']).'_'.$FILID;
                    $data[] = $row;
                }
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data,'FLDARR'=> $fieldArr);
            }
        }
    }
    return $data;
}
function fnGetCCSIDRDB($db2con,$QRYNM,$RDBNM,$FILID,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST){
    $data = array();
    $fieldArr = array();
    $syscolumn2 = array();
    $params = array();
    $LIBLNM = '';
    $rtn = 0;
    $createFlg = false;
    $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:$RDBNM);
    $db2RDB = cmDB2ConRDB();

    $strSQL  =  '   SELECT  ';
    $strSQL .=  '       A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
    $strSQL .=  '      ,A.LENGTH '; 
    $strSQL .=  '      ,CASE A.DDS_TYPE ';
    $strSQL .=  '        WHEN \'H\' THEN \'A\' ';
    $strSQL .=  '        WHEN \'J\' THEN \'0\' ';
    $strSQL .=  '        WHEN \'E\' THEN \'0\' ';
    $strSQL .=  '        ELSE A.DDS_TYPE '; 
    $strSQL .=  '        END AS DDS_TYPE ';
    $strSQL .=  '      ,A.NUMERIC_SCALE '; 
    if(SYSCOLCFLG==='1'){
        $strSQL .=  '      ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .=  '      ,A.COLUMN_HEADING ';
    }
    $strSQL .=  '      ,A.ORDINAL_POSITION '; 
    $strSQL .=  '      ,A.CCSID AS CCSID ';
    $strSQL .=  '   FROM '. SYSCOLUMN2 .' A '; 
    $strSQL .=  '   WHERE A.TABLE_SCHEMA = ? '; 
    $strSQL .=  '   AND A.TABLE_NAME = ? '; 
//    $strSQL .=  '   AND A.DDS_TYPE NOT IN(\'5\') ';
    $strSQL .=  '   ORDER BY A.ORDINAL_POSITION ';

    $stmt = db2_prepare($db2RDB,$strSQL);
    if($stmt === false){
        $syscolumn2 = array('result' => 'FAIL_SEL');
        $rtn = 1;
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$TABLE_NAME);
                e_log('フィールド取得：'.$strSQL.print_r($params,true));
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $syscolumn2 = array('result' => 'FAIL_SEL');
                    $rtn = 1;
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $syscolumn2[] = $row;
                    }
                    if(count($syscolumn2) > 0){
                        $LIBLNM = $value;
                        break;
                    }
                }
            }
            //$data = umEX($data,true);
            $syscolumn2 = umEx($syscolumn2);
            $syscolumn2 = array('result' => true,'data' => $syscolumn2);
            //e_log('取得したデータ：'.print_r($syscolumn2,true));
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME);
            e_log('フィールド取得：'.$strSQL.print_r($params,true));
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $syscolumn2 = array('result' => 'FAIL_SEL');
                $rtn = 1;
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $syscolumn2[] = $row;
                }
                $LIBLNM = $TABLE_SCHEMA;
                $syscolumn2 = umEx($syscolumn2,false);
                $syscolumn2 = array('result' => true,'data' => $syscolumn2);
                //e_log('取得したデータ：'.print_r($syscolumn2,true));
            }
        }
    }

    if($rtn === 0){
        e_log('一時テーブル作成開始①');
        
        // 一時テーブル作成
        $createSQL  = 'DECLARE GLOBAL TEMPORARY TABLE SYSCOLUMN2 ';
        $createSQL .= ' ( ';
        $createSQL .= ' SYSTEM_COLUMN_NAME CHAR(10) , ';
        $createSQL .= ' LENGTH INTEGER , ';
        $createSQL .= ' DDS_TYPE CHAR(1), ';
        $createSQL .= ' NUMERIC_SCALE INTEGER DEFAULT NULL , ';
        $createSQL .= ' COLUMN_HEADING VARGRAPHIC(60) CCSID 1200 , ';
        $createSQL .= ' ORDINAL_POSITION INTEGER, ';
        $createSQL .= ' TABLE_SCHEMA VARCHAR(128), ';
        $createSQL .= ' TABLE_NAME VARCHAR(128), ';
        $createSQL .= ' CCSID CHAR(5) ';
        $createSQL .= ' ) WITH REPLACE ';
        //e_log($createSQL);
        $result = db2_exec($db2con, $createSQL);
        if ($result) {
            e_log( 'テーブル作成成功'.$createSQL.print_r($result,true).'adsfds'.print_r($syscolumn2,true));
            $createFlg = true;
            if(count($syscolumn2['data'])>0){
                $syscolumn2 = umEx($syscolumn2['data']);
                
                $strInsSQL  = ' INSERT INTO QTEMP/SYSCOLUMN2 ';
                $strInsSQL .= ' ( ';
                $strInsSQL .= '     SYSTEM_COLUMN_NAME, ';
                $strInsSQL .= '     LENGTH, ';
                $strInsSQL .= '     DDS_TYPE, ';
                $strInsSQL .= '     NUMERIC_SCALE, ';
                $strInsSQL .= '     COLUMN_HEADING, ';
                $strInsSQL .= '     ORDINAL_POSITION, ';
                $strInsSQL .= '     TABLE_SCHEMA, ';
                $strInsSQL .= '     TABLE_NAME, ';
                $strInsSQL .= '     CCSID ';
                $strInsSQL .= ' ) ';
                $strInsSQL .= ' VALUES ( ?,?,?,?,?,?,?,?,?)';

                $stmt = db2_prepare($db2con,$strInsSQL);
                if($stmt === false ){
                    $data = array(
                                'result' => 'FAIL_INS',
                                'errcd'  => 'SYSCOLUMN2:'.db2_stmt_errormsg()
                            );
                    $result = 1;
                }else{
                    foreach($syscolumn2 as $syscolumn2data){
                        $params =array(
                                    $syscolumn2data['COLUMN_NAME'],
                                    $syscolumn2data['LENGTH'],
                                    $syscolumn2data['DDS_TYPE'],
                                    ($syscolumn2data['NUMERIC_SCALE'] == '') ? NULL : $syscolumn2data['NUMERIC_SCALE'],
                                    $syscolumn2data['COLUMN_HEADING'],
                                    $syscolumn2data['ORDINAL_POSITION'],
                                    $syscolumn2data['TABLE_SCHEMA'],
                                    $syscolumn2data['TABLE_NAME'],
                                    $syscolumn2data['CCSID']
                                );
                        e_log($strInsSQL.print_r($params,true));
                        $res = db2_execute($stmt,$params);
                        if($res === false){
                            $data = array(
                                    'result' => 'FAIL_INS',
                                    'errcd'  => 'SYSCOLUMN2:'.db2_stmt_errormsg()
                                );
                            e_log($strInsSQL.print_r($params,true).print_r($data,true));
                            $result = 1;
                            break;
                        }else{
                            $result = 0;
                            $data = array('result' => true);
                        }
                    }
                }
            }
            if($result !== 0){
                $rtn = 1;
            }
        }else{
            e_log('テーブル作成失敗');
            e_log('テーブルerror'.db2_stmt_errormsg());
            $rtn = 1;
            $data = array(
                'result' => 'FAIL_SEL',
                'errcd'  => 'テーブル作成失敗'
                );
        }
    }

    /*if($rtn === 0){
            $strSQL = '';

        $strSQL  =' SELECT \'\' AS SEQ ';
        $strSQL .='       ,A.SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
        $strSQL .='       ,A.LENGTH ';
        $strSQL .='       ,CASE A.DDS_TYPE';
        $strSQL .='         WHEN \'H\' THEN \'A\'';
        $strSQL .='         WHEN \'J\' THEN \'0\'';
        $strSQL .='         WHEN \'E\' THEN \'0\'';
        $strSQL .='         ELSE A.DDS_TYPE ';
        $strSQL .='         END AS DDS_TYPE';
        $strSQL .='       ,A.NUMERIC_SCALE ';
        $strSQL .='       ,A.COLUMN_HEADING ';
        $strSQL .='       ,\'\' AS FORMULA ';
        $strSQL .='       ,\'\' AS SORTNO ';
        $strSQL .='       ,\'\' AS SORTTYP ';
        $strSQL .='       ,\'\' AS EDTCD ';
        $strSQL .='       ,A.ORDINAL_POSITION ';
        $strSQL .=' FROM QTEMP/SYSCOLUMN2 A ';
        $strSQL .=' WHERE A.TABLE_SCHEMA = ? ';
        $strSQL .=' AND A.TABLE_NAME = ? ';
        //$strSQL .=' AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\')';
        $strSQL .=' AND A.DDS_TYPE NOT IN(\'5\')';
        $strSQL .=' ORDER BY A.ORDINAL_POSITION ';
        
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
            //e_log('error'.db2_stmt_errormsg());
            $rtn = 1;
        }else{
            $params = array($LIBLNM,$TABLE_NAME);
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
                $rtn = 1;
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data);
            }
            //e_log('temptable'.print_r($data,true));
        }
    }*/
    cmDb2Close($db2RDB);
    if($rtn === 0){
        $strSQL = '';
        //e_log(print_r($LIBLIST,true));
        $strSQL .=  '   SELECT ';
        $strSQL .=  '         FDB2CSV2DATA.SEQ            AS SEQ ';
        $strSQL .=  '       , SYSTABDATA.COLUMN_NAME      AS COLUMN_NAME ';
        $strSQL .=  '       , SYSTABDATA.LENGTH           AS LENGTH ';
        $strSQL .=  '       , SYSTABDATA.DDS_TYPE         AS DDS_TYPE ';
        $strSQL .=  '       , SYSTABDATA.NUMERIC_SCALE    AS NUMERIC_SCALE ';
        $strSQL .=  '       ,(CASE ';
        $strSQL .=  '           WHEN FDB2CSV2DATA.COLUMN_HEADING IS NULL ';
        $strSQL .=  '           AND FDB2CSV2DATA.COLUMN_NAME  IS NULL ';
        $strSQL .=  '           THEN SYSTABDATA.COLUMN_HEADING ';
        $strSQL .=  '           ELSE FDB2CSV2DATA.COLUMN_HEADING ';
        $strSQL .=  '         END) AS COLUMN_HEADING ';
        $strSQL .=  '       , FDB2CSV2DATA.FORMULA        AS FORMULA ';
        $strSQL .=  '       , FDB2CSV2DATA.SORTNO         AS SORTNO ';
        $strSQL .=  '       , FDB2CSV2DATA.SORTTYP        AS SORTTYP ';
        $strSQL .=  '       , FDB2CSV2DATA.EDTCD          AS EDTCD ';
        $strSQL .=  '       , SYSTABDATA.ORDINAL_POSITION AS ORDINAL_POSITION ';
        $strSQL .=  '       , SYSTABDATA.CCSID AS CCSID ';
        $strSQL .=  '   FROM  '; 
        $strSQL .=  '       (SELECT  ';
        $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
        $strSQL .=  '          ,A.LENGTH '; 
        $strSQL .=  '          ,CASE A.DDS_TYPE ';
        $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
        $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
        $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
        $strSQL .=  '            ELSE A.DDS_TYPE '; 
        $strSQL .=  '            END AS DDS_TYPE ';
        $strSQL .=  '          ,A.NUMERIC_SCALE '; 
        $strSQL .=  '          ,A.COLUMN_HEADING '; 
        $strSQL .=  '          ,A.ORDINAL_POSITION '; 
        $strSQL .=  '          ,A.CCSID AS CCSID ';
        $strSQL .=  '       FROM QTEMP/SYSCOLUMN2 A '; 
        $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
        $strSQL .=  '       AND A.TABLE_NAME = ? '; 
//        $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
        $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ) SYSTABDATA ';
        $strSQL .=  '   LEFT JOIN ';
        $strSQL .=  '       (SELECT  ';
        $strSQL .=  '            (CASE A.D2CSEQ ';
        $strSQL .=  '            WHEN 0 THEN 99999 '; 
        $strSQL .=  '            ELSE A.D2CSEQ ';
        $strSQL .=  '            END) '; 
        $strSQL .=  '            AS SEQ '; 
        $strSQL .=  '           ,A.D2FLD  AS COLUMN_NAME ';
        $strSQL .=  '           ,A.D2HED  AS COLUMN_HEADING '; 
        $strSQL .=  '           ,\'\'     AS FORMULA ';
        $strSQL .=  '           ,CASE A.D2RSEQ ';
        $strSQL .=  '             WHEN 0 THEN 99999 ';
        $strSQL .=  '             ELSE A.D2RSEQ '; 
        $strSQL .=  '             END AS SORTNO ';
        $strSQL .=  '           ,A.D2RSTP AS SORTTYP '; 
        $strSQL .=  '           ,A.D2WEDT AS EDTCD ';
        $strSQL .=  '       FROM FDB2CSV2 A ';
        $strSQL .=  '       WHERE A.D2NAME = ?';
        $strSQL .=  '       AND   A.D2FILID = ? ';
        $strSQL .=  '       ORDER BY A.D2CSEQ,A.D2RSEQ ) FDB2CSV2DATA ';
        $strSQL .=  '   ON SYSTABDATA.COLUMN_NAME = FDB2CSV2DATA.COLUMN_NAME ';
        $strSQL .=  '   ORDER BY SEQ ,SORTNO, ORDINAL_POSITION ';
        
        //e_log('SQLtemp'.$strSQL);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
            $rtn  = 1;
            //e_log('error'.db2_stmt_errormsg());
        }else{
            if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
               /* foreach($LIBLIST as $value){
                   $params = array($value,$TABLE_NAME,$D1NAME,$FILID);
                    //e_log(print_r($params,true));
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $data = array('result' => 'FAIL_SEL');
                        $rtn  = 1;
                    }else{
                        while($row = db2_fetch_assoc($stmt)){
                            if($row['SORTNO'] == 99999){
                                $row['SORTNO'] = '';
                            }
                            if($row['SEQ'] == 99999){
                                $row['SEQ'] = '';
                            }
                            $data[] = $row;
                        }
                        if(count($data) > 0){
                            break;
                        }
                    }
                }
                //$data = umEX($data,true);
                $data = umEx($data);
                $data = array('result' => true,'data' => $data);*/
                $params = array($LIBLNM,$TABLE_NAME,$D1NAME,$FILID);
            }else{
                $params = array($TABLE_SCHEMA,$TABLE_NAME,$D1NAME,$FILID);
            }
            //e_log(print_r($params,true));
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
                $rtn  = 1;
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row['SORTNO'] == 99999){
                        $row['SORTNO'] = '';
                    }
                    if($row['SEQ'] == 99999){
                        $row['SEQ'] = '';
                    }
                    $data[] = $row;
                    $fieldArr[] = cmMer($row['COLUMN_NAME']).'_'.$FILID;
                }
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data,'FLDARR'=> $fieldArr);
            }
        }
            
       // }
    }
    if($createFlg){
        $strSQL = ' DROP TABLE QTEMP/SYSCOLUMN2' ;
        $stmt = @db2_prepare($db2con,$strSQL);
        if($stmt === false){
            //処理無し
        }else{
            $rs = @db2_execute($stmt);
        }
    }
    //e_log(print_r($data,true));
    return $data;
}
// 指定したファイルがあるかチェック
function fnChkExistFile($db2con,$LIB,$FILE,$LIBLIST){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT A.SYSTEM_TABLE_NAME ';
    $strSQL .= '      , A.SYSTEM_TABLE_SCHEMA ';
    $strSQL .= '      , A.TABLE_TEXT';
    $strSQL .= ' FROM  ' ;
    $strSQL .= '    QSYS2/SYSTABLES A ';
    $strSQL .= ' WHERE A.SYSTEM_TABLE_SCHEMA = ? ' ;
    $strSQL .= ' AND A.SYSTEM_TABLE_NAME = ? ' ;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($LIB === '*USRLIBL' || $LIB === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$FILE);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
        }else{
            $params = array($LIB,$FILE);
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
            }
        }        
        if(count($data) === 0){
            $data = array('result' => 'NOTEXIST');
        }else{
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}
// 指定したファイルに指定フィールドがあるかチェック
function fnChkExistFld ($db2con,$Field,$FILEINFO,$LIBLIST){

    $TABLE_SCHEMA = $FILEINFO['SYSTEM_TABLE_SCHEMA'];
    $TABLE_NAME = $FILEINFO['SYSTEM_TABLE_NAME'];
    $COLNAME = $Field;

    $data = array();
    $params = array();
    $strSQL = '';
    $strSQL  =' SELECT A.SYSTEM_COLUMN_NAME  AS COLUMN_NAME ';
    $strSQL .='       ,A.LENGTH ';
    $strSQL .='       ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG ==='1'){
        $strSQL .='       ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .='       ,A.COLUMN_HEADING ';
    }
    $strSQL .='       ,CASE A.DDS_TYPE';
    $strSQL .='         WHEN \'H\' THEN \'A\'';
    $strSQL .='         WHEN \'J\' THEN \'0\'';
    $strSQL .='         WHEN \'E\' THEN \'0\'';
    $strSQL .='         ELSE A.DDS_TYPE ';
    $strSQL .='         END AS DDS_TYPE';
    $strSQL .='       ,A.ORDINAL_POSITION ';
    $strSQL .='       ,A.CCSID ';
    $strSQL .=' FROM '. SYSCOLUMN2 . ' A ';
    $strSQL .=' WHERE A.TABLE_SCHEMA = ? ';
//    $strSQL .=' AND A.DDS_TYPE NOT IN(\'5\')';
    $strSQL .=' AND A.TABLE_NAME = ? ';
    $strSQL .=' AND A.SYSTEM_COLUMN_NAME = ? ';
    
    $params = array($TABLE_SCHEMA,$TABLE_NAME,$COLNAME);
    $stmt = db2_prepare($db2con,$strSQL);
    
    if($stmt === false){
        $data = array('result' => true,'data'=> 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => true,'data'=>'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $data = array('result' => 'ISEXIST' ,'data' => umEx($data));
            }else{
                $data = array('result' => true,'data'=>'NOTEXIST');
            }
        }
    }
    return $data;
}
function fnGetSQLFDB2CSV2CCSID ($db2con,$QRYNM,$WRKTBL){
    $data = array();
    $rsFDB2CSV1 = fnQryFDB2CSV1($db2con,$QRYNM);
    if($rsfdb2csv1['result'] !== true){
         $data = array('result' => $rsfdb2csv1['result']);
    }else{
        $fdb2csv1 = $rsfdb2csv1['data'];
        // ライブラリーリスト作成
        $libArr = preg_split ("/\s+/", $fdb2csv1['D1LIBL'] );
        $LIBLIST = join(' ',$libArr);
        $RDBNM   = $fdb2csv1['D1RDB'];
    }
    if($rtn === 0){
        if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
             $db2LibCon = cmDb2ConLib($LIBLIST);
        } else {
            $_SESSION['PHPQUERY']['RDBNM'] = $qryData['SELRDB'];
            $db2LibCon = cmDB2ConRDB($LIBLIST);
        }
        $rsFLDCCSID = fngetSQLFldCCSID($db2LibCon,$WRKTBL);
        if($rsFLDCCSID['result'] !== true){
            $data = array('result' => $rsFLDCCSID['result']);
        }else{
            $data = $rsFLDCCSID;
        }
    }
    return $data;
}
// SQLで作成したクエリーの選択フィールドのCCSID情報取得
function fngetSQLFldCCSID($db2LibCon,$wrktbl){

    e_log('fngetSQLFldCCSID');

    $data = array();
    $param = array();
    $syscolumn2 = array();
    $rtn = 0;
    $strSQL  =  ' SELECT  ';
    $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
    $strSQL .=  '          ,A.LENGTH '; 
    $strSQL .=  '          ,CASE A.DDS_TYPE ';
    $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
    $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
    $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
    $strSQL .=  '            ELSE A.DDS_TYPE '; 
    $strSQL .=  '            END AS DDS_TYPE ';
    $strSQL .=  '          ,A.NUMERIC_SCALE '; 
    if(SYSCOLCFLG==='1'){
        $strSQL .=  '          ,A.COLUMN_TEXT AS COLUMN_HEADING '; 
    }else{
        $strSQL .=  '          ,A.COLUMN_HEADING ';
    }
    $strSQL .=  '          ,A.ORDINAL_POSITION '; 
    $strSQL .=  '          ,A.CCSID ';
    $strSQL .=  '       FROM ' . SYSCOLUMN2 . ' A '; 
    $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
    $strSQL .=  '       AND A.TABLE_NAME = ? '; 
//    $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
    $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ';

    $stmt = db2_prepare($db2LibCon,$strSQL);
    if($stmt === false){
        $syscolumn2 = array('result' => 'FAIL_SEL');
        $rtn = 1;
    }else{
        $params = array(SAVE_DB,$wrktbl);
        $r = db2_execute($stmt,$params);
        
        if($r === false){
            $syscolumn2 = array('result' => 'FAIL_SEL');
            $rtn = 1;
        }else{
            $cntCCSID = 0;
            while($row = db2_fetch_assoc($stmt)){
                if(cmMer($row['CCSID']) == '65535'){
                    $cntCCSID++;
                }
                $syscolumn2[] = $row;
            }
            $LIBLNM = $TABLE_SCHEMA;
            $syscolumn2 = umEx($syscolumn2,false);
            e_log('KKKKK');
            e_log(print_r($syscolumn2,true));

            $syscolumn2 = array('result' => true,'data' => $syscolumn2,'CCSIDCNT' => $cntCCSID);
        }
    }
    return $syscolumn2;
}
