<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 共通区分データ取得
 * PROGRAM ID     : comGetKbn.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/04/11
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
 * 変数
 */
$rtn = 0;
$msg = '';
$rs = array();
$kbnSh = $_POST['KBNSH'];
$kbnCd = $_POST['KBNCD'];
$sqlCFlg   = (isset($_POST['SQLCFLG']))?$_POST['SQLCFLG']:'';
$sqlLKFlg   = (isset($_POST['SQLLKFLG']))?$_POST['SQLLKFLG']:'';
/*
 * 処理
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

if($rtn === 0){
    e_log(">>>>>$sqlLKFlg<<<<<<");
    $rs = getDB2MKBN($db2con,$kbnSh,$kbnCd,$sqlCFlg,$sqlLKFlg);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}

$data = umEx($data);
cmDb2Close($db2con);
$rtn = array(
    'DATA' => $data,
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));
/**
  *---------------------------------------------------------------------------
  * 区分データを取得
  * 
  * RESULT
  *    01：区分データリスト
  * 
  * @param String  func       区分種別キー値
  *
  *---------------------------------------------------------------------------
  **/
function getDB2MKBN($db2con,$kbnSh,$kbnCd,$sqlCFlg= '',$sqlLKFlg){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT A.KBNTEXT AS KBNCD';
    $strSQL .=  '       ,A.KBNNM';
    $strSQL .=  '       ,A.KBNCD AS LKEY';
    $strSQL .=  '   FROM DB2MKBN A';
    $strSQL .=  '   WHERE A.KBNKB = (';
    $strSQL .=  '       SELECT KBNCD ';
    $strSQL .=  '       FROM DB2MKBN ';
    $strSQL .=  '       WHERE KBNKB = ? ';
    $strSQL .=  '       AND KBNCD = ?) ';
    if($sqlCFlg === '1'){
        $strSQL .= '    AND KBNCD <> \'2\'';    // SQL実行でクエリ作成するとき検索タイプに任意入力できないため
    }
    if($sqlLKFlg === '1'){
        $strSQL .= '    AND KBNCD >= 7 ';
        $strSQL .= '    AND KBNCD <= 9 ';
    }
    $strSQL .=  ' ORDER BY DECIMAL(A.KBNCD)';
    $params = array($kbnSh,$kbnCd);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
