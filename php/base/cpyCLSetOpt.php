<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyCLSetOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnCLFDB2CSV1PG($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('CL実行設定'.$rs['result'].$rs['errcd']);
    }else{
        $fdb2csv1pg = $rs['data'];
        e_log(print_r($fdb2csv1pg,true));
        if(count($fdb2csv1pg)>0){
            $fdb2csv1pg = $fdb2csv1pg[0];
            $rs = fnInsFDB2CSV1PG($db2con,$NEWQRYNM,$fdb2csv1pg);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('CL実行設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        $rs = fnCLFDB2CSV1PM($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('CL実行設定'.$rs['result'].$rs['errcd']);
        }else{
            $fdb2csv1pm = $rs['data'];
            e_log(print_r($fdb2csv1pm,true));
            if(count($fdb2csv1pm)>0){
                $rs = fnInsFDB2CSV1PM($db2con,$NEWQRYNM,$fdb2csv1pm);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('CL実行設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}

/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyCLSetOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnCLFDB2CSV1PG($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('CL実行設定'.$rs['result'].$rs['errcd']);
    }else{
        $fdb2csv1pg = $rs['data'];
        if(count($fdb2csv1pg)>0){
            $fdb2csv1pg = $fdb2csv1pg[0];
            $rs = fnInsFDB2CSV1PG($db2con,$QRYNM,$fdb2csv1pg,$newLib);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('CL実行設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        $rs = fnCLFDB2CSV1PM($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('CL実行設定'.$rs['result'].$rs['errcd']);
        }else{
            $fdb2csv1pm = $rs['data'];
            if(count($fdb2csv1pm)>0){
                $rs = fnInsFDB2CSV1PM($db2con,$QRYNM,$fdb2csv1pm,$newLib);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('CL実行設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}

/*
 *  FDB2CSV1PG取得
 */
function fnCLFDB2CSV1PG($db2con,$QRYNM){
    $params = array();
    $data   = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     DGNAME, ';
    $strSQL .= '     DGBLIB, ';
    $strSQL .= '     DGBPGM, ';
    $strSQL .= '     DGCLIB, ';
    $strSQL .= '     DGCPGM, ';
    $strSQL .= '     DGCBNM, ';
    $strSQL .= '     DGALIB, ';
    $strSQL .= '     DGAPGM ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1PG ';
    $strSQL .= ' WHERE DGNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsFDB2CSV1PG($db2con,$NEWQRYNM,$fdb2csv1pg,$newLib=''){
    $data   = array();
    $params = array();
    $strSQL = '';
    if($newLib!==''){
        $strSQL .= ' INSERT INTO '.$newLib.'.FDB2CSV1PG';
    }else{
        $strSQL .= ' INSERT INTO FDB2CSV1PG';
    }
    $strSQL .= '        ( ';
    $strSQL .= '     DGNAME, ';
    $strSQL .= '     DGBLIB, ';
    $strSQL .= '     DGBPGM, ';
    $strSQL .= '     DGCLIB, ';
    $strSQL .= '     DGCPGM, ';
    $strSQL .= '     DGCBNM, ';
    $strSQL .= '     DGALIB, ';
    $strSQL .= '     DGAPGM ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    
    $params =array(
        $NEWQRYNM,
        cmMer($fdb2csv1pg['DGBLIB']),
        cmMer($fdb2csv1pg['DGBPGM']),
        cmMer($fdb2csv1pg['DGCLIB']),
        cmMer($fdb2csv1pg['DGCPGM']),
        cmMer($fdb2csv1pg['DGCBNM']),
        cmMer($fdb2csv1pg['DGALIB']),
        cmMer($fdb2csv1pg['DGAPGM'])
    );
    $stmt = db2_prepare($db2con,$strSQL);
    e_log($strSQL.print_r($params,true));
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*
 *  FDB2CSV1PM取得
 */
function fnCLFDB2CSV1PM($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     DMNAME, ';
    $strSQL .= '     DMEVENT, ';
    $strSQL .= '     DMSEQ, ';
    $strSQL .= '     DMUSAGE, ';
    $strSQL .= '     DMPNAME, ';
    $strSQL .= '     DMPTYPE, ';
    $strSQL .= '     DMLEN, ';
    $strSQL .= '     DMSCAL, ';
    $strSQL .= '     DMFRTO, ';
    $strSQL .= '     DMIZDT, ';
    $strSQL .= '     DMSCFG, ';
    $strSQL .= '     DMSCID, ';
    $strSQL .= '     DMSCFD, ';
    $strSQL .= '     DMMSEQ, ';
    $strSQL .= '     DMSSEQ ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1PM ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     DMNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsFDB2CSV1PM($db2con,$NEWQRYNM,$fdb2csv1pmdata,$newLib=''){
    foreach($fdb2csv1pmdata as $fdb2csv1pm){
        $data   = array();
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.FDB2CSV1PM';
        }else{
            $strSQL .= ' INSERT INTO FDB2CSV1PM';
        }
        $strSQL .= '        ( ';
        $strSQL .= '     DMNAME, ';
        $strSQL .= '     DMEVENT, ';
        $strSQL .= '     DMSEQ, ';
        $strSQL .= '     DMUSAGE, ';
        $strSQL .= '     DMPNAME, ';
        $strSQL .= '     DMPTYPE, ';
        $strSQL .= '     DMLEN, ';
        $strSQL .= '     DMSCAL, ';
        $strSQL .= '     DMFRTO, ';
        $strSQL .= '     DMIZDT, ';
        $strSQL .= '     DMSCFG, ';
        $strSQL .= '     DMSCID, ';
        $strSQL .= '     DMSCFD, ';
        $strSQL .= '     DMMSEQ, ';
        $strSQL .= '     DMSSEQ ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
            $NEWQRYNM,
            cmMer($fdb2csv1pm['DMEVENT']),
            cmMer($fdb2csv1pm['DMSEQ']),
            cmMer($fdb2csv1pm['DMUSAGE']),
            cmMer($fdb2csv1pm['DMPNAME']),
            cmMer($fdb2csv1pm['DMPTYPE']),
            cmMer($fdb2csv1pm['DMLEN']),
            cmMer($fdb2csv1pm['DMSCAL']),
            cmMer($fdb2csv1pm['DMFRTO']),
            cmMer($fdb2csv1pm['DMIZDT']),
            cmMer($fdb2csv1pm['DMSCFG']),
            cmMer($fdb2csv1pm['DMSCID']),
            cmMer($fdb2csv1pm['DMSCFD']),
            cmMer($fdb2csv1pm['DMMSEQ']),
            cmMer($fdb2csv1pm['DMSSEQ'])
        );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg()
                    );
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg()
                    );
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
