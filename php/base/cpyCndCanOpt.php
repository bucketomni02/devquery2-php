<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyCndCanOpt($db2con,$QRYNM,$D1CFLG,$NEWQRYNM,$BQRYCNDDATA){
    $rtn = '';
    if($D1CFLG  === ''){
        $rs = fnGetCndCanOpt($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('検索候補設定'.$rs['result'].$rs['errcd']);
        }else{
            $cndCan = $rs['data'];
            if(count($cndCan)>0){
                $rs = fnUpdcndCanOpt($db2con,$NEWQRYNM,$cndCan,$BQRYCNDDATA);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('検索候補設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }else{
        $rs = fnGetSQLCndCanOpt($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('検索候補設定'.$rs['result'].$rs['errcd']);
        }else{
            $cndCan = $rs['data'];
            if(count($cndCan)>0){
                $rs = fnSQLUpdcndCanOpt($db2con,$NEWQRYNM,$cndCan,$BQRYCNDDATA);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('検索候補設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyCndCanOpt_OTHER($db2con,$db2conNew,$QRYNM,$D1CFLG){
    $rtn = '';
    if($D1CFLG  === ''){
        $rs = fnGetCndCanOpt($db2con,$QRYNM);
        e_log('Hello KlYH=>'.print_r($rs,true));
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('検索候補設定'.$rs['result'].$rs['errcd']);
        }else{
            $cndCan = $rs['data'];
            if(count($cndCan)>0){
                $rs = fnUpdcndCanOpt_OTHER($db2conNew,$QRYNM,$cndCan);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('検索候補設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }else{
        $rs = fnGetSQLCndCanOpt($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('検索候補設定'.$rs['result'].$rs['errcd']);
        }else{
            $cndCan = $rs['data'];
            $BQRYCNDDATA=$rs['data'];
            if(count($cndCan)>0){
                $rs = fnSQLUpdcndCanOpt($db2conNew,$QRYNM,$cndCan,$BQRYCNDDATA);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('検索候補設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}

/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetCndCanOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL .= '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     A.CNQRYN, ';
    $strSQL .= '     A.CNFILID, ';
    $strSQL .= '     A.CNMSEQ, ';
    $strSQL .= '     A.CNSSEQ, ';
    $strSQL .= '     A.CNFLDN, ';
    $strSQL .= '     A.CNCKBN, ';
    $strSQL .= '     A.CNSTKB, ';
    $strSQL .= '     A.CNCANL, ';
    $strSQL .= '     A.CNCANF, ';
    $strSQL .= '     A.CNCMBR, ';
    $strSQL .= '     A.CNCANC, ';
    $strSQL .= '     A.CNCANG, ';
    $strSQL .= '     A.CNNAMG, ';
    $strSQL .= '     A.CNNAMC, ';
    $strSQL .= '     A.CNDFMT, ';
    $strSQL .= '     A.CNDSFL, ';
    $strSQL .= '     A.CNDFIN, ';
    $strSQL .= '     A.CNDFPM, ';
    $strSQL .= '     A.CNDFDY, ';
    $strSQL .= '     A.CNDFFL, ';
    $strSQL .= '     A.CNDTIN, ';
    $strSQL .= '     A.CNDTPM, ';
    $strSQL .= '     A.CNDTDY, ';
    $strSQL .= '     A.CNDTFL, ';
    $strSQL .= '     A.CNCAST, ';
    $strSQL .= '     A.CNNAST, ';
    $strSQL .= '     B.DHINFO, ';
    $strSQL .= '     B.DHINFG ';
    $strSQL .= ' FROM ';
    $strSQL .= '     BQRYCND A ';
    $strSQL .= ' LEFT JOIN DB2HCND B ';
    $strSQL .= ' ON A.CNQRYN = B.DHNAME ';
    $strSQL .= ' AND A.CNFILID = B.DHFILID ';
    $strSQL .= ' AND A.CNMSEQ = B.DHMSEQ ';
    $strSQL .= ' AND A.CNSSEQ = B.DHSSEQ ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.CNQRYN = ?';
    $strSQL .= ' AND ((A.CNCANL <> \'\' ';
    $strSQL .= '     AND A.CNCANF <> \'\' '; 
    $strSQL .= '     AND A.CNCANC <> \'\') '; 
    $strSQL .= '     OR A.CNDFMT <> \'\' ';
    $strSQL .= '     OR B.DHINFO <> \'\') ';

    $params = array($QRYNM);
    e_log($strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnUpdcndCanOpt($db2con,$NEWQRYNM,$cndCanData,$BQRYCNDDATA){
    $data   = array();
    
    foreach($BQRYCNDDATA as $bqrycnd){
        $updFlg = false;
        foreach($cndCanData as $cndCan){
            if(($bqrycnd['FMSEQ'] === cmMer($cndCan['CNMSEQ'])) && ($bqrycnd['FSSEQ'] === cmMer($cndCan['CNSSEQ']))){
                $updFlg = true;
                $CNMSEQ = $bqrycnd['FMSEQ'];
                $CNSSEQ = $bqrycnd['FSSEQ'];
                break;
            }
        }
        if($updFlg){
            $params = array();
            $strSQL = '';
            $strSQL .= ' UPDATE BQRYCND ';
            $strSQL .= ' SET ';
            $strSQL .= '    CNCANL = ? , ';
            $strSQL .= '    CNCANF = ? , ';
            $strSQL .= '    CNCMBR = ? , ';
            $strSQL .= '    CNCANC = ? , ';
            $strSQL .= '    CNCANG = ? , ';
            $strSQL .= '    CNNAMG = ? , ';
            $strSQL .= '    CNNAMC = ? , ';
            $strSQL .= '    CNDFMT = ? , ';
            $strSQL .= '    CNDSFL = ? , ';
            $strSQL .= '    CNDFIN = ? , ';
            $strSQL .= '    CNDFPM = ? , ';
            $strSQL .= '    CNDFDY = ? , ';
            $strSQL .= '    CNDFFL = ? , ';
            $strSQL .= '    CNDTIN = ? , ';
            $strSQL .= '    CNDTPM = ? , ';
            $strSQL .= '    CNDTDY = ? , ';
            $strSQL .= '    CNDTFL = ? , ';
            $strSQL .= '    CNCAST = ? , ';
            $strSQL .= '    CNNAST = ? ' ;
            $strSQL .= ' WHERE  ';
            $strSQL .= '    CNQRYN      = ? ';
            $strSQL .= ' AND CNFILID    = ? ';
            $strSQL .= ' AND CNMSEQ     = ? ';
            $strSQL .= ' AND CNSSEQ     = ? ';
            $strSQL .= ' AND CNFLDN     = ? ';
            
            $params =array(
                        cmMer($cndCan['CNCANL']),
                        cmMer($cndCan['CNCANF']),
                        cmMer($cndCan['CNCMBR']),
                        cmMer($cndCan['CNCANC']),
                        cmMer($cndCan['CNCANG']),
                        cmMer($cndCan['CNNAMG']),
                        cmMer($cndCan['CNNAMC']),
                        cmMer($cndCan['CNDFMT']),
                        cmMer($cndCan['CNDSFL']),
                        cmMer($cndCan['CNDFIN']),
                        cmMer($cndCan['CNDFPM']),
                        cmMer($cndCan['CNDFDY']),
                        cmMer($cndCan['CNDFFL']),
                        cmMer($cndCan['CNDTIN']),
                        cmMer($cndCan['CNDTPM']),
                        cmMer($cndCan['CNDTDY']),
                        cmMer($cndCan['CNDTFL']),
                        cmMer($cndCan['CNCAST']),
                        cmMer($cndCan['CNNAST']),
                        $NEWQRYNM,
                        cmMer($cndCan['CNFILID']),
                        /*cmMer($cndCan['CNMSEQ']),
                        cmMer($cndCan['CNSSEQ']),*/
                        $CNMSEQ,
                        $CNSSEQ,
                        cmMer($cndCan['CNFLDN'])
                     );
            e_log('****************'.$strSQL.print_r($params,true));//

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false ){
                $data = array(
                            'result' => 'FAIL_UPD',
                            'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                        );
                break;
            }else{
                
                $res = db2_execute($stmt,$params);
                if($res === false){
                    $data = array(
                            'result' => 'FAIL_UPD',
                            'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                        );
                    break;
                }else{
                    if(db2_num_rows($stmt) === 1){
                        if(cmMer($cndCan['DHINFO']) <> ''){
                            $params = array();
                            $strSQL = '';
                            $strSQL .= ' INSERT INTO DB2HCND';
                            $strSQL .= '        ( ';
                            $strSQL .= '            DHNAME, ';
                            $strSQL .= '            DHFILID, ';
                            $strSQL .= '            DHMSEQ, ';
                            $strSQL .= '            DHSSEQ, ';
                            $strSQL .= '            DHINFO, ';
                            $strSQL .= '            DHINFG ';
                            $strSQL .= '        ) ';
                            $strSQL .= ' VALUES ( ';
                            $strSQL .= '            ?,?,?,?,?,? ';
                            $strSQL .= '        ) ';
                            
                            $params =array(
                                $NEWQRYNM,
                                $cndCan['CNFILID'],
                                /*$cndCan['CNMSEQ'],
                                $cndCan['CNSSEQ'],*/
                                $CNMSEQ,
                                $CNSSEQ,
                                $cndCan['DHINFO'],
                                $cndCan['DHINFG']
                                     );
                            $stmt = db2_prepare($db2con,$strSQL);
                            e_log($strSQL.print_r($params,true));
                            if($stmt === false ){
                                $data = array(
                                            'result' => 'FAIL_INS',
                                            'errcd'  => 'DB2HCND:'.db2_stmt_errormsg()
                                        );
                                break;
                            }else{
                                $res = db2_execute($stmt,$params);
                                if($res === false){
                                    $data = array(
                                            'result' => 'FAIL_INS',
                                            'errcd'  => 'DB2HCND:'.db2_stmt_errormsg()
                                        );
                                    break;
                                }else{
                                    $data = array('result' => true);
                                }
                            }
                        }else{
                            $data = array('result' => true);
                        }
                    }else{
                        $data = array('result' => true);
                    }
                }
            }
        }
    }
    return $data;
}
function fnUpdcndCanOpt_OTHER($db2con,$NEWQRYNM,$cndCanData){
    $data   = array();
    $params = array();
    $strSQL = '';
    $strSQL .= ' UPDATE BQRYCND ';
    $strSQL .= ' SET ';
    $strSQL .= '    CNCANL = ? , ';
    $strSQL .= '    CNCANF = ? , ';
    $strSQL .= '    CNCMBR = ? , ';
    $strSQL .= '    CNCANC = ? , ';
    $strSQL .= '    CNCANG = ? , ';
    $strSQL .= '    CNNAMG = ? , ';
    $strSQL .= '    CNNAMC = ? , ';
    $strSQL .= '    CNDFMT = ? , ';
    $strSQL .= '    CNDSFL = ? , ';
    $strSQL .= '    CNDFIN = ? , ';
    $strSQL .= '    CNDFPM = ? , ';
    $strSQL .= '    CNDFDY = ? , ';
    $strSQL .= '    CNDFFL = ? , ';
    $strSQL .= '    CNDTIN = ? , ';
    $strSQL .= '    CNDTPM = ? , ';
    $strSQL .= '    CNDTDY = ? , ';
    $strSQL .= '    CNDTFL = ? , ';
    $strSQL .= '    CNCAST = ? , ';
    $strSQL .= '    CNNAST = ? ' ;
    $strSQL .= ' WHERE  ';
    $strSQL .= '    CNQRYN      = ? ';
    $strSQL .= ' AND CNFILID    = ? ';
    $strSQL .= ' AND CNMSEQ     = ? ';
    $strSQL .= ' AND CNSSEQ     = ? ';
    $strSQL .= ' AND CNFLDN     = ? ';
    $params =array(
        cmMer($cndCan['CNCANL']),
        cmMer($cndCan['CNCANF']),
        cmMer($cndCan['CNCMBR']),
        cmMer($cndCan['CNCANC']),
        cmMer($cndCan['CNCANG']),
        cmMer($cndCan['CNNAMG']),
        cmMer($cndCan['CNNAMC']),
        cmMer($cndCan['CNDFMT']),
        cmMer($cndCan['CNDSFL']),
        cmMer($cndCan['CNDFIN']),
        cmMer($cndCan['CNDFPM']),
        cmMer($cndCan['CNDFDY']),
        cmMer($cndCan['CNDFFL']),
        cmMer($cndCan['CNDTIN']),
        cmMer($cndCan['CNDTPM']),
        cmMer($cndCan['CNDTDY']),
        cmMer($cndCan['CNDTFL']),
        cmMer($cndCan['CNCAST']),
        cmMer($cndCan['CNNAST']),
        $NEWQRYNM,
        cmMer($cndCan['CNFILID']),
        cmMer($cndCan['CNMSEQ']),
        cmMer($cndCan['CNSSEQ']),
        cmMer($cndCan['CNFLDN'])
    );
    e_log('検索候補設定⇒⇒'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array('result' => 'FAIL_UPD','errcd'  => 'BQRYCND:'.db2_stmt_errormsg());
   }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_UPD','errcd'  => 'BQRYCND:'.db2_stmt_errormsg());
        }else{
            if(db2_num_rows($stmt) === 1){
                if(cmMer($cndCan['DHINFO']) <> ''){
                    $params = array();
                    $strSQL = '';
                    $strSQL .= ' INSERT INTO DB2HCND';
                    $strSQL .= '        ( ';
                    $strSQL .= '            DHNAME, ';
                    $strSQL .= '            DHFILID, ';
                    $strSQL .= '            DHMSEQ, ';
                    $strSQL .= '            DHSSEQ, ';
                    $strSQL .= '            DHINFO, ';
                    $strSQL .= '            DHINFG ';
                    $strSQL .= '        ) ';
                    $strSQL .= ' VALUES ( ';
                    $strSQL .= '            ?,?,?,?,?,? ';
                    $strSQL .= '        ) ';
                    
                    $params =array(
                        $NEWQRYNM,
                        $cndCan['CNFILID'],
                        $cndCan['CNMSEQ'],
                        $cndCan['CNSSEQ'],
                        $cndCan['DHINFO'],
                        $cndCan['DHINFG']
                    );
                    $stmt = db2_prepare($db2con,$strSQL);
                    if($stmt === false ){
                        $data = array('result' => 'FAIL_INS','errcd'  => 'DB2HCND:'.db2_stmt_errormsg());
                    }else{
                        $res = db2_execute($stmt,$params);
                        if($res === false){
                            $data = array('result' => 'FAIL_INS','errcd'  => 'DB2HCND:'.db2_stmt_errormsg());
                        }else{
                            $data = array('result' => true);
                        }
                    }
                }else{
                    $data = array('result' => true);
                }
            }else{
                $data = array('result' => true);
            }
        }
    }//end of statement false
    return $data;
}

/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetSQLCndCanOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL .= '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     A.CNQRYN , ';
    $strSQL .= '     A.CNDSEQ , ';
    $strSQL .= '     A.CNDKBN , ';
    $strSQL .= '     A.CNDNM , ';
    $strSQL .= '     A.CNDWNM , ';
    $strSQL .= '     A.CNDDTYP , ';
    $strSQL .= '     A.CNDSTKB , ';
    $strSQL .= '     A.CNDDAT , ';
    $strSQL .= '     A.CNDBCNT , ';
    $strSQL .= '     A.CNCANL , ';
    $strSQL .= '     A.CNCANF , ';
    $strSQL .= '     A.CNCMBR , ';
    $strSQL .= '     A.CNCANC , ';
    $strSQL .= '     A.CNCANG , ';
    $strSQL .= '     A.CNNAMG , ';
    $strSQL .= '     A.CNNAMC , ';
    $strSQL .= '     A.CNDFMT , ';
    $strSQL .= '     A.CNDSFL , ';
    $strSQL .= '     A.CNDFIN , ';
    $strSQL .= '     A.CNDFPM , ';
    $strSQL .= '     A.CNDFDY , ';
    $strSQL .= '     A.CNDFFL , ';
    $strSQL .= '     A.CNDTIN , ';
    $strSQL .= '     A.CNDTPM , ';
    $strSQL .= '     A.CNDTDY , ';
    $strSQL .= '     A.CNDTFL , ';
    $strSQL .= '     A.CNCAST , ';
    $strSQL .= '     A.CNNAST , ';
    $strSQL .= '     A.CNDSCH , ';
    $strSQL .= '     B.DHINFO , ';
    $strSQL .= '     B.DHINFG  ';
    $strSQL .= ' FROM ';
    $strSQL .= '     BSQLCND A ';
    $strSQL .= ' LEFT JOIN DB2HCND B ';
    $strSQL .= ' ON A.CNQRYN = B.DHNAME ';
    $strSQL .= ' AND A.CNDSEQ = B.DHMSEQ ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.CNQRYN = ?';
    $strSQL .= ' AND ((A.CNCANL <> \'\' ';
    $strSQL .= '     AND A.CNCANF <> \'\' '; 
    $strSQL .= '     AND A.CNCANC <> \'\') '; 
    $strSQL .= '     OR A.CNDFMT <> \'\' ';
    $strSQL .= '     OR B.DHINFO <> \'\') ';

    $params = array($QRYNM);
    e_log($strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => '検索候補データ:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => '検索候補データ:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnSQLUpdcndCanOpt($db2con,$NEWQRYNM,$cndCanData,$BQRYCNDDATA){
    e_log('条件取得データ：'.print_r($cndCanData,true));
    e_log('条件取得FRMデータ：'.print_r($BQRYCNDDATA,true));
    $data   = array();
    foreach($BQRYCNDDATA as $bqrycnd){
        $updFlg = false;
        foreach($cndCanData as $cndCan){
            if($bqrycnd['CNDSEQ'] === cmMer($cndCan['CNDSEQ'])){
                $updFlg = true;
                $CNDSEQ = $bqrycnd['CNDSEQ'];
                break;
            }
        }
        if($updFlg){
            $params = array();
            $strSQL = '';
            $strSQL .= ' UPDATE BSQLCND ';
            $strSQL .= ' SET ';
            $strSQL .= '    CNCANL = ? , ';
            $strSQL .= '    CNCANF = ? , ';
            $strSQL .= '    CNCMBR = ? , ';
            $strSQL .= '    CNCANC = ? , ';
            $strSQL .= '    CNCANG = ? , ';
            $strSQL .= '    CNNAMG = ? , ';
            $strSQL .= '    CNNAMC = ? , ';
            $strSQL .= '    CNDFMT = ? , ';
            $strSQL .= '    CNDSFL = ? , ';
            $strSQL .= '    CNDFIN = ? , ';
            $strSQL .= '    CNDFPM = ? , ';
            $strSQL .= '    CNDFDY = ? , ';
            $strSQL .= '    CNDFFL = ? , ';
            $strSQL .= '    CNDTIN = ? , ';
            $strSQL .= '    CNDTPM = ? , ';
            $strSQL .= '    CNDTDY = ? , ';
            $strSQL .= '    CNDTFL = ? , ';
            $strSQL .= '    CNCAST = ? , ';
            $strSQL .= '    CNNAST = ? ' ;
            $strSQL .= ' WHERE  ';
            $strSQL .= '    CNQRYN      = ? ';
            $strSQL .= ' AND CNDSEQ     = ? ';
            $params =array(
                        cmMer($cndCan['CNCANL']),
                        cmMer($cndCan['CNCANF']),
                        cmMer($cndCan['CNCMBR']),
                        cmMer($cndCan['CNCANC']),
                        cmMer($cndCan['CNCANG']),
                        cmMer($cndCan['CNNAMG']),
                        cmMer($cndCan['CNNAMC']),
                        cmMer($cndCan['CNDFMT']),
                        cmMer($cndCan['CNDSFL']),
                        cmMer($cndCan['CNDFIN']),
                        cmMer($cndCan['CNDFPM']),
                        cmMer($cndCan['CNDFDY']),
                        cmMer($cndCan['CNDFFL']),
                        cmMer($cndCan['CNDTIN']),
                        cmMer($cndCan['CNDTPM']),
                        cmMer($cndCan['CNDTDY']),
                        cmMer($cndCan['CNDTFL']),
                        cmMer($cndCan['CNCAST']),
                        cmMer($cndCan['CNNAST']),
                        $NEWQRYNM,
                        $CNDSEQ,
                     );
            $stmt = db2_prepare($db2con,$strSQL);
            e_log($strSQL.print_r($params,true));
            if($stmt === false ){
                $data = array(
                            'result' => 'FAIL_UPD',
                            'errcd'  => 'BSQLCND:'.db2_stmt_errormsg()
                        );
                break;
            }else{
                
                $res = db2_execute($stmt,$params);
                if($res === false){
                    $data = array(
                            'result' => 'FAIL_UPD',
                            'errcd'  => 'BSQLCND:'.db2_stmt_errormsg()
                        );
                    break;
                }else{
                    if(db2_num_rows($stmt) === 1){
                        if(cmMer($cndCan['DHINFO']) <> ''){
                            $params = array();
                            $strSQL = '';
                            $strSQL .= ' INSERT INTO DB2HCND';
                            $strSQL .= '        ( ';
                            $strSQL .= '            DHNAME, ';
                            $strSQL .= '            DHFILID, ';
                            $strSQL .= '            DHMSEQ, ';
                            $strSQL .= '            DHSSEQ, ';
                            $strSQL .= '            DHINFO, ';
                            $strSQL .= '            DHINFG ';
                            $strSQL .= '        ) ';
                            $strSQL .= ' VALUES ( ';
                            $strSQL .= '            ?,?,?,?,?,? ';
                            $strSQL .= '        ) ';
                            
                            $params =array(
                                $NEWQRYNM,
                                0,
                                $CNDSEQ,
                                0,
                                $cndCan['DHINFO'],
                                $cndCan['DHINFG']
                                     );
                            $stmt = db2_prepare($db2con,$strSQL);
                            e_log($strSQL.print_r($params,true));
                            if($stmt === false ){
                                $data = array(
                                            'result' => 'FAIL_INS',
                                            'errcd'  => 'DB2HCND:'.db2_stmt_errormsg()
                                        );
                                break;
                            }else{
                                $res = db2_execute($stmt,$params);
                                if($res === false){
                                    $data = array(
                                            'result' => 'FAIL_INS',
                                            'errcd'  => 'DB2HCND:'.db2_stmt_errormsg()
                                        );
                                    break;
                                }else{
                                    $data = array('result' => true);
                                }
                            }
                        }else{
                            $data = array('result' => true);
                        }
                    }else{
                        $data = array('result' => true);
                    }
                }
            }
        }
    }
    return $data;
}
