<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：�@コピー先クエリー名
*            :�Aコピー元クエリー名
*-------------------------------------------------------*
*/

function cpyConFormatOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    // コピー先クエリーのユーザーオプション情報取得
    $rs = fnGetDB2JKSK($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('条件付き書式設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2jksk = $rs['data'];
        if(count($db2jksk)>0){
            $rs = fnInsDB2JKSK($db2con,$NEWQRYNM,$db2jksk);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('条件付き書式設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}

/*
 *  DB2JKSK取得
 * 条件付き書式設定
 */
function fnGetDB2JKSK($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL  = ' SELECT ';
    $strSQL .= '     JKNAME, ';
    $strSQL .= '     JKSEQ, ';
    $strSQL .= '     JKFILD, ';
    $strSQL .= '     JKDATA, ';
    $strSQL .= '     JKCOST, ';
    $strSQL .= '     JKBGST, ';
    $strSQL .= '     JKOPTR ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2JKSK ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     JKNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2JKSK:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2JKSK:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2JKSK($db2con,$NEWQRYNM,$db2jkskdata){
    $data   = array();
    foreach($db2jkskdata as $db2jksk){
        $params = array();
        $strSQL = '';
        $strSQL .= ' INSERT INTO DB2JKSK ';
        $strSQL .= '        ( ';
        $strSQL .= '     JKNAME, ';
        $strSQL .= '     JKSEQ, ';
        $strSQL .= '     JKFILD, ';
        $strSQL .= '     JKDATA, ';
        $strSQL .= '     JKCOST, ';
        $strSQL .= '     JKBGST, ';
        $strSQL .= '     JKOPTR ';   
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
                    $NEWQRYNM,
                    $db2jksk['JKSEQ'],
                    $db2jksk['JKFILD'],
                    $db2jksk['JKDATA'],
                    $db2jksk['JKCOST'],
                    $db2jksk['JKBGST'],
                    $db2jksk['JKOPTR']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2JKSK:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2JKSK:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
