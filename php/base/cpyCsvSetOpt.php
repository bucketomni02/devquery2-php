<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyCsvSetOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetCsvSetOpt($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('クエリー実行設定'.$rs['result'].$rs['errcd']);
    }else{
        $csvSet = $rs['data'];
        if(count($csvSet)>0){
            $csvSet = $csvSet[0];
            $rs = fnUpdCsvSetOpt($db2con,$NEWQRYNM,$csvSet);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('クエリー実行設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyCsvSetOpt_OTHER($db2con,$db2conNew,$QRYNM){
    $rtn = '';
    $rs = fnGetCsvSetOpt($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('クエリー実行設定'.$rs['result'].$rs['errcd']);
    }else{
        $csvSet = $rs['data'];
        if(count($csvSet)>0){
            $csvSet = $csvSet[0];
            $rs = fnUpdCsvSetOpt($db2conNew,$QRYNM,$csvSet);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('クエリー実行設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}

/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetCsvSetOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     D1NAME, ';
    $strSQL .= '     D1CENC, ';
    $strSQL .= '     D1CKAI, ';
    $strSQL .= '     D1CHED, ';
    $strSQL .= '     D1CDLM, ';
    $strSQL .= '     D1CECL ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 ';
    $strSQL .= ' WHERE D1NAME = ? ';
    $strSQL .= ' AND ( D1CENC = \'1\' ';
    $strSQL .= ' OR    D1CKAI = \'1\' ';
    $strSQL .= ' OR    D1CHED = \'1\' ';
    $strSQL .= ' OR    D1CDLM = \'1\' ';
    $strSQL .= ' OR    D1CECL = \'1\') ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnUpdCsvSetOpt($db2con,$NEWQRYNM,$csvSet){
    $data   = array();
    $params = array();
    $strSQL = '';
    $strSQL .= ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= '     D1CENC = ?, ';
    $strSQL .= '     D1CKAI = ?, ';
    $strSQL .= '     D1CHED = ?, ';
    $strSQL .= '     D1CDLM = ?, ';
    $strSQL .= '     D1CECL = ? ';
    $strSQL .= ' WHERE  ';
    $strSQL .= '    D1NAME      = ? ';
    
    $params =array(
                $csvSet['D1CENC'],
                $csvSet['D1CKAI'],
                $csvSet['D1CHED'],
                $csvSet['D1CDLM'],
                $csvSet['D1CECL'],
                $NEWQRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    e_log($strSQL.print_r($params,true));
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
