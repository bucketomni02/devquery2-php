<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyDashboardOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetDB2BMK($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ダッシュボード設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2bmk = $rs['data'];
        if(count($db2bmk)>0){
            $rs = fnInsDB2BMK($db2con,$NEWQRYNM,$db2bmk);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('グラフ設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyDashboardOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnGetDB2BMK($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ダッシュボード設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2bmk = $rs['data'];
        if(count($db2bmk)>0){
            $rs = fnInsDB2BMK($db2con,$QRYNM,$db2bmk,$newLib);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('グラフ設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}

/*
 *  DB2BMK取得
 */
function fnGetDB2BMK($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL = '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     BMKDSBID, ';
    $strSQL .= '     BMKSEQ, ';
    $strSQL .= '     BMKQID, ';
    $strSQL .= '     BMKGID, ';
    $strSQL .= '     BMKTYPE, ';
    $strSQL .= '     BMKSIZ ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2BMK ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     BMKQID = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2BMK:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2BMK:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}

function fnInsDB2BMK($db2con,$NEWQRYNM,$db2bmkdata,$newLib=''){
    foreach($db2bmkdata as $db2bmk){
        $data   = array();
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2BMK';
        }else{
            $strSQL .= ' INSERT INTO DB2BMK';
        }
        $strSQL .= '        ( ';
        $strSQL .= '     BMKDSBID, ';
        $strSQL .= '     BMKSEQ, ';
        $strSQL .= '     BMKQID, ';
        $strSQL .= '     BMKGID, ';
        $strSQL .= '     BMKTYPE, ';
        $strSQL .= '     BMKSIZ ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?';
        $strSQL .= '        ) ';
        
        $params =array(
            $db2bmk['BMKDSBID'],
            $db2bmk['BMKSEQ'],
            $NEWQRYNM,
            $db2bmk['BMKGID'],
            $db2bmk['BMKTYPE'],
            $db2bmk['BMKSIZ']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2BMK:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2BMK:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
