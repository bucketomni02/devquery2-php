<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyDwnKengenOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetDwnKengen($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ダウンロード権限設定'.$rs['result'].$rs['errcd']);
    }else{
        $dwnKengen = $rs['data'];
        if(count($dwnKengen)>0){
            $rs = fnInsDwnKengen($db2con,$NEWQRYNM,$dwnKengen);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('ダウンロード権限設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyDwnKengenOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnGetDwnKengen($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ダウンロード権限設定'.$rs['result'].$rs['errcd']);
    }else{
        $dwnKengen = $rs['data'];
        e_log('ダウンロード権限設定'.print_r($dwnKengen,true));
        if(count($dwnKengen)>0){
            $rs = fnInsDwnKengen($db2con,$QRYNM,$dwnKengen,$newLib);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('ダウンロード権限設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}
/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetDwnKengen($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     WDUID, ';
    $strSQL .= '     WDNAME, ';
    $strSQL .= '     WDDWNL, ';
    $strSQL .= '     WDDWN2, ';
    $strSQL .= '     WDSERV ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WDEF A ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.WDNAME = ? ';
    $strSQL .= ' AND (A.WDDWNL = \'1\' ';
    $strSQL .= ' OR   A.WDDWN2 = \'1\' )';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDwnKengen($db2con,$NEWQRYNM,$dwnKengenData,$newLib=''){
    $data   = array();
    foreach($dwnKengenData as $dwnKengen){
        $strSQL =  '';
        $strSQL .= ' SELECT ';
        $strSQL .= '     WDUID, ';
        $strSQL .= '     WDNAME, ';
        $strSQL .= '     WDDWNL, ';
        $strSQL .= '     WDDWN2, ';
        $strSQL .= '     WDSERV ';
        $strSQL .= ' FROM ';
        if($newLib!==''){
            $strSQL .= '     '.$newLib.'.DB2WDEF A ';
        }else{
            $strSQL .= '     DB2WDEF A ';
        }
        $strSQL .= ' WHERE ';
        $strSQL .= '     A.WDUID = ? ';
        $strSQL .= ' AND A.WDNAME = ? ';

        $params = array(cmMer($dwnKengen['WDUID']),$NEWQRYNM);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
            }else{
                $data   = array();
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
                if(count($data['data']) > 0){
                    $params = array();
                    $strSQL  = '';
                    if($newLib!==''){
                        $strSQL .= ' UPDATE '.$newLib.'.DB2WDEF ';
                    }else{
                        $strSQL .= ' UPDATE DB2WDEF ';
                    }
                    $strSQL .= ' SET ';
                    $strSQL .= '    WDDWNL      = ? , ';
                    $strSQL .= '    WDDWN2      = ? , ';
                    $strSQL .= '    WDSERV      = ? ';
                    $strSQL .= ' WHERE  ';
                    $strSQL .= '    WDUID      = ? ';
                    $strSQL .= ' AND WDNAME      = ? ';

                    $params =array(
                                $dwnKengen['WDDWNL'],
                                $dwnKengen['WDDWN2'],
                                $dwnKengen['WDSERV'],
                                cmMer($dwnKengen['WDUID']),
                                $NEWQRYNM
                             );
                    $stmt = db2_prepare($db2con,$strSQL);
                    if($stmt === false ){
                        $data = array(
                                    'result' => 'FAIL_UPD',
                                    'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                                );
                        break;
                    }else{
                        
                        $res = db2_execute($stmt,$params);
                        if($res === false){
                            $data = array(
                                    'result' => 'FAIL_UPD',
                                    'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                                );
                            break;
                        }else{
                            $data = array('result' => true);
                        }
                    }
                }else{
                    $params = array();
                    $strSQL = '';
                    if($newLib!==''){
                        $strSQL .= ' INSERT INTO '.$newLib.'.DB2WDEF ';
                    }else{
                        $strSQL .= ' INSERT INTO DB2WDEF ';
                    }
                    $strSQL .= '        ( ';
                    $strSQL .= '            WDUID, ';
                    $strSQL .= '            WDNAME, ';
                    $strSQL .= '            WDDWNL, ';
                    $strSQL .= '            WDDWN2, ';
                    $strSQL .= '            WDSERV ';
                    $strSQL .= '        ) ';
                    $strSQL .= ' VALUES ( ';
                    $strSQL .= '            ?,?,?,?,? ';
                    $strSQL .= '        ) ';
                    $params =array(
                                cmMer($dwnKengen['WDUID']),
                                $NEWQRYNM,
                                $dwnKengen['WDDWNL'],
                                $dwnKengen['WDDWN2'],
                                $dwnKengen['WDSERV']
                             );
                    $stmt = db2_prepare($db2con,$strSQL);
                    if($stmt === false ){
                        $data = array(
                                    'result' => 'FAIL_INS',
                                    'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                                );
                        break;
                    }else{
                        
                        $res = db2_execute($stmt,$params);
                        if($res === false){
                            $data = array(
                                    'result' => 'FAIL_INS',
                                    'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                                );
                            break;
                        }else{
                            $data = array('result' => true);
                        }
                    }
                }
            }
        }
    }
    return $data;
}
