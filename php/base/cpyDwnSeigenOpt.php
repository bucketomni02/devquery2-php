<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyDwnSeigenOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetDwnSeigenFDB2CSV2($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ダウンロード制限設定'.$rs['result'].$rs['errcd']);
    }else{
        $dwnSeigen2 = $rs['data'];
        if(count($dwnSeigen2)>0){
            $rs = fnUpdDwnSeigenFDB2CSV2($db2con,$NEWQRYNM,$dwnSeigen2);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('ダウンロード制限設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDwnSeigenFDB2CSV5($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('ダウンロード制限設定'.$rs['result'].$rs['errcd']);
        }else{
            $dwnSeigen5 = $rs['data'];
            if(count($dwnSeigen5)>0){
                $rs = fnUpdDwnSeigenFDB2CSV5($db2con,$NEWQRYNM,$dwnSeigen5);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('ダウンロード制限設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyDwnSeigenOpt_OTHER($db2con,$db2conNew,$QRYNM){
    $rtn = '';
    $rs = fnGetDwnSeigenFDB2CSV2($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ダウンロード制限設定'.$rs['result'].$rs['errcd']);
    }else{
        $dwnSeigen2 = $rs['data'];
        if(count($dwnSeigen2)>0){
            $rs = fnUpdDwnSeigenFDB2CSV2($db2conNew,$db2conNew,$dwnSeigen2);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('ダウンロード制限設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDwnSeigenFDB2CSV5($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('ダウンロード制限設定'.$rs['result'].$rs['errcd']);
        }else{
            $dwnSeigen5 = $rs['data'];
            if(count($dwnSeigen5)>0){
                $rs = fnUpdDwnSeigenFDB2CSV5($db2conNew,$QRYNM,$dwnSeigen5);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('ダウンロード制限設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}

/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetDwnSeigenFDB2CSV2($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     D2NAME, ';
    $strSQL .= '     D2FILID, ';
    $strSQL .= '     D2FLD, ';
    $strSQL .= '     D2CSEQ, ';
    $strSQL .= '     D2DNLF ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV2 ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     D2NAME = ? ';
    $strSQL .= 'AND  D2DNLF = \'1\' ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnUpdDwnSeigenFDB2CSV2($db2con,$NEWQRYNM,$dwnSeigenData){
    $data   = array();
    foreach($dwnSeigenData as $dwnSeigen){
        $params = array();
        $strSQL = '';
        $strSQL .= ' UPDATE FDB2CSV2 ';
        $strSQL .= ' SET ';
        $strSQL .= '    D2DNLF      = ?  ';
        $strSQL .= ' WHERE  ';
        $strSQL .= '    D2NAME   = ? ';
        $strSQL .= ' AND D2FILID = ? ';
        $strSQL .= ' AND D2FLD = ? ';
        $strSQL .= ' AND D2CSEQ = ? ';
        
        $params =array(
                    cmMer($dwnSeigen['D2DNLF']),
                    $NEWQRYNM,
                    cmMer($dwnSeigen['D2FILID']),
                    cmMer($dwnSeigen['D2FLD']),
                    cmMer($dwnSeigen['D2CSEQ'])
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetDwnSeigenFDB2CSV5($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     D5NAME, ';
    $strSQL .= '     D5FLD, ';
    $strSQL .= '     D5CSEQ, ';
    $strSQL .= '     D5DNLF ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV5 ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     D5NAME = ? ';
    $strSQL .= 'AND  D5DNLF = \'1\' ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnUpdDwnSeigenFDB2CSV5($db2con,$NEWQRYNM,$dwnSeigenData){
    $data   = array();
    foreach($dwnSeigenData as $dwnSeigen){
        $params = array();
        $strSQL = '';
        $strSQL .= ' UPDATE FDB2CSV5 ';
        $strSQL .= ' SET ';
        $strSQL .= '    D5DNLF      = ?  ';
        $strSQL .= ' WHERE  ';
        $strSQL .= '    D5NAME   = ? ';
        $strSQL .= ' AND D5FLD = ? ';
        $strSQL .= ' AND D5CSEQ = ? ';
        
        $params =array(
                    cmMer($dwnSeigen['D5DNLF']),
                    $NEWQRYNM,
                    cmMer($dwnSeigen['D5FLD']),
                    cmMer($dwnSeigen['D5CSEQ'])
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
