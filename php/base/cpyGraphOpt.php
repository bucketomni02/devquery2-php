<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyGraphOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetDB2GPK($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('グラフ設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2gpk = $rs['data'];
        if(count($db2gpk)>0){
            $rs = fnInsDB2GPK($db2con,$NEWQRYNM,$db2gpk);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('グラフ設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2GPC($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('グラフ設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2gpc = $rs['data'];
            if(count($db2gpc)>0){
                $rs = fnInsDB2GPC($db2con,$NEWQRYNM,$db2gpc);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('グラフ設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/
function cpyGraphOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnGetDB2GPK($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('グラフ設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2gpk = $rs['data'];
        if(count($db2gpk)>0){
            $rs = fnInsDB2GPK($db2con,$QRYNM,$db2gpk,$newLib);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('グラフ設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2GPC($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('グラフ設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2gpc = $rs['data'];
            if(count($db2gpc)>0){
                $rs = fnInsDB2GPC($db2con,$QRYNM,$db2gpc,$newLib);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('グラフ設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}

/*
 *  DB2GPK取得
 */
function fnGetDB2GPK($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL = '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     GPKQRY, ';
    $strSQL .= '     GPKID, ';
    $strSQL .= '     GPKNM, ';
    $strSQL .= '     GPKFM, ';
    $strSQL .= '     GPKNUM, ';
    $strSQL .= '     GPKDAT, ';
    $strSQL .= '     GPKXCO, ';
    $strSQL .= '     GPKXID, ';
    $strSQL .= '     GPKXNM, ';
    $strSQL .= '     GPKCFG, ';
    $strSQL .= '     GPKCUD, ';
    $strSQL .= '     GPKCUT, ';
    $strSQL .= '     GPKBFD, ';
    $strSQL .= '     GPKBFT, ';
    $strSQL .= '     GPKTBL, ';
    $strSQL .= '     GPKEND, ';
    $strSQL .= '     GPKFLG, ';
	$strSQL .= '     GPKDFG, '; //TTA
    $strSQL .= '     GPJKCHK ';
//    $strSQL .= '     GPNTFG ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2GPK ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     GPKQRY = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PMST:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PMST:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}

function fnInsDB2GPK($db2con,$NEWQRYNM,$db2gpkdata,$newLib=''){
    foreach($db2gpkdata as $db2gpk){
        $data   = array();
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2GPK';
        }else{
            $strSQL .= ' INSERT INTO DB2GPK';
        }
        $strSQL .= '        ( ';
        $strSQL .= '     GPKQRY, ';
        $strSQL .= '     GPKID, ';
        $strSQL .= '     GPKNM, ';
        $strSQL .= '     GPKFM, ';
        $strSQL .= '     GPKNUM, ';
        $strSQL .= '     GPKDAT, ';
        $strSQL .= '     GPKXCO, ';
        $strSQL .= '     GPKXID, ';
        $strSQL .= '     GPKXNM, ';
        $strSQL .= '     GPKCFG, ';
		$strSQL .= '     GPKDFG, '; //TTA
        $strSQL .= '     GPJKCHK ';
//        $strSQL .= '     GPNTFG ';
        /*$strSQL .= '     GPKCUD, ';
        $strSQL .= '     GPKCUT, ';
        $strSQL .= '     GPKBFD, ';
        $strSQL .= '     GPKBFT, ';
        $strSQL .= '     GPKTBL, ';
        $strSQL .= '     GPKEND, ';
        $strSQL .= '     GPKFLG ';*/
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?';
        $strSQL .= '        ) ';
        
        $params =array(
            $NEWQRYNM,
            $db2gpk['GPKID'],
            $db2gpk['GPKNM'],
            $db2gpk['GPKFM'],
            $db2gpk['GPKNUM'],
            $db2gpk['GPKDAT'],
            $db2gpk['GPKXCO'],
            $db2gpk['GPKXID'],
            $db2gpk['GPKXNM'],
            $db2gpk['GPKCFG'],
			$db2gpk['GPKDFG'], //TTA
            $db2gpk['GPJKCHK'],/*
            $db2gpk['GPNTFG'],
            $db2gpk['GPKCUD'],
            $db2gpk['GPKCUT'],
            $db2gpk['GPKBFD'],
            $db2gpk['GPKBFT'],
            $db2gpk['GPKTBL'],
            $db2gpk['GPKEND'],
            $db2gpk['GPKFLG']*/
        );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2GPK:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2GPK:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
/*
 *  DB2GPC取得
 */
function fnGetDB2GPC($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL = '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     GPCSEQ, ';
    $strSQL .= '     GPCQRY, ';
    $strSQL .= '     GPCID, ';
    $strSQL .= '     GPCYCO, ';
    $strSQL .= '     GPCYID, ';
    $strSQL .= '     GPCLBL, ';
    $strSQL .= '     GPCFM, ';
    $strSQL .= '     GPCCAL, ';
    $strSQL .= '     GPCCLR ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2GPC ';
    $strSQL .= ' WHERE GPCQRY = ? ';


    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2GPC:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2GPC:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}

function fnInsDB2GPC($db2con,$NEWQRYNM,$db2gpcdata,$newLib=''){
    foreach($db2gpcdata as $db2gpc){
        $data   = array();
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2GPC';
        }else{
            $strSQL .= ' INSERT INTO DB2GPC';
        }
        $strSQL .= '        ( ';
        $strSQL .= '     GPCSEQ, ';
        $strSQL .= '     GPCQRY, ';
        $strSQL .= '     GPCID, ';
        $strSQL .= '     GPCYCO, ';
        $strSQL .= '     GPCYID, ';
        $strSQL .= '     GPCLBL, ';
        $strSQL .= '     GPCFM, ';
        $strSQL .= '     GPCCAL, ';
        $strSQL .= '     GPCCLR ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
            $db2gpc['GPCSEQ'],
            $NEWQRYNM,
            $db2gpc['GPCID'],
            $db2gpc['GPCYCO'],
            $db2gpc['GPCYID'],
            $db2gpc['GPCLBL'],
            $db2gpc['GPCFM'],
            $db2gpc['GPCCAL'],
            $db2gpc['GPCCLR']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2GPC:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2GPC:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}

