<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyGroupOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetDB2WGDF($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ユーザー設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2wgdf = $rs['data'];
        if(count($db2wgdf)>0){
            $rs = fnInsDB2WGDF($db2con,$NEWQRYNM,$db2wgdf);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('グループと権限設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別の処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyGroupOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnGetDB2WGDF($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ユーザー設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2wgdf = $rs['data'];
        if(count($db2wgdf)>0){
            $rs = fnInsDB2WGDF($db2con,$QRYNM,$db2wgdf,$newLib);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('グループと権限設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}

/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetDB2WGDF($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL  = ' SELECT ';
    $strSQL .= '     WGGID, ';
    $strSQL .= '     WGNAME ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WGDF ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     WGNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WGDF:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WGDF:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2WGDF($db2con,$NEWQRYNM,$db2wgdfdata,$newLib=''){
    $data   = array();
    foreach($db2wgdfdata as $db2wgdf){
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2WGDF ';
        }else{
            $strSQL .= ' INSERT INTO DB2WGDF ';
        }
        $strSQL .= '        ( ';
        $strSQL .= '            WGGID, ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
                    $db2wgdf['WGGID'],
                    $NEWQRYNM
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2WGDF:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2WGDF:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
