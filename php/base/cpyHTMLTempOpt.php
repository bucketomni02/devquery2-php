<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyHTMLTempOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetHTMLTemp($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('HTMLTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
    }else{
        $HTMLTemp = $rs['data'];
        e_log(print_r($HTMLTemp,true));
        if(count($HTMLTemp)>0){
            $HTMLTemp = $HTMLTemp[0];
            $rs = fnInsHTMLTemp($db2con,$NEWQRYNM,$HTMLTemp);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('HTMLTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyHTMLTempOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnGetHTMLTemp($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('HTMLTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
    }else{
        $HTMLTemp = $rs['data'];
        e_log('HTMLTEMPLATE実行設定⇒'.print_r($HTMLTemp,true));
        if(count($HTMLTemp)>0){
            $HTMLTemp = $HTMLTemp[0];
            $rs = fnInsHTMLTemp($db2con,$QRYNM,$HTMLTemp,$newLib);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('HTMLTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}

/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetHTMLTemp($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL  = ' SELECT ';
    $strSQL .= '     HTMLTD, ';
    $strSQL .= '     HTMLTN ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2HTMLT ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     HTMLTD = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsHTMLTemp($db2con,$NEWQRYNM,$HTMLTemp,$newLib=''){
    $data   = array();
    $params = array();

    if (cmMer($HTMLTemp['HTMLTN']) !== '') {
        if (file_exists(BASE_DIR."/php/htmltemplate/". cmMer($HTMLTemp['HTMLTD']) . '.html')) {
            $templateFlg = true;
        } else {
            $templateFlg = false;
        }
    }
    if($templateFlg){
        if (!copy(BASE_DIR."/php/htmltemplate/". cmMer($HTMLTemp['HTMLTD']) . '.html', BASE_DIR."/php/htmltemplate/" . cmMer($NEWQRYNM) . '.html')) {
            e_log('ファイルコピー失敗しました。');
        }
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2HTMLT ';
        }else{
            $strSQL .= ' INSERT INTO DB2HTMLT ';
        }
        $strSQL .= '        ( ';
        $strSQL .= '            HTMLTD, ';
        $strSQL .= '            HTMLTN ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
                    $NEWQRYNM,
                    $HTMLTemp['HTMLTN']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2HTMLT:'.db2_stmt_errormsg()
                    );
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2HTMLT:'.db2_stmt_errormsg()
                    );
            }else{
                $data = array('result' => true);
            }
        }
    }else{
        e_log('ファイルこーピー失敗。ファイル見つかりません');
    }
    return $data;
}
