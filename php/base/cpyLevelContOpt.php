<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyLevelContOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetDB2COLM($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('制御レベル設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2colm = $rs['data'];
        e_log(print_r($db2colm,true));
        if(count($db2colm)>0){
            $rs = fnInsDB2COLM($db2con,$NEWQRYNM,$db2colm);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('制御レベル設定'.$rs['result'].$rs['errcd']);
            }else{
                $rs = fnGetDB2COLT($db2con,$QRYNM);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('制御レベル設定'.$rs['result'].$rs['errcd']);
                }else{
                    $db2colt = $rs['data'];
                    e_log(print_r($db2colt,true));
                    if(count($db2colt)>0){
                        $rs = fnInsDB2COLT($db2con,$NEWQRYNM,$db2colt);
                        if($rs['result'] !== true){
                            $rtn = $rs['result'];
                            e_log('制御レベル設定'.$rs['result'].$rs['errcd']);
                        }
                    }
                }
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyLevelContOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnGetDB2COLM($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('制御レベル設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2colm = $rs['data'];
        if(count($db2colm)>0){
            $rs = fnInsDB2COLM($db2con,$QRYNM,$db2colm,$newLib);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('制御レベル設定'.$rs['result'].$rs['errcd']);
            }else{
                $rs = fnGetDB2COLT($db2con,$QRYNM);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('制御レベル設定'.$rs['result'].$rs['errcd']);
                }else{
                    $db2colt = $rs['data'];
                    if(count($db2colt)>0){
                        $rs = fnInsDB2COLT($db2con,$QRYNM,$db2colt,$newLib);
                        if($rs['result'] !== true){
                            $rtn = $rs['result'];
                            e_log('制御レベル設定'.$rs['result'].$rs['errcd']);
                        }
                    }
                }
            }
        }
    }
    return $rtn;
}

/*
 *  DB2COLM取得
 */
function fnGetDB2COLM($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     DCNAME, ';
    $strSQL .= '     DCFILID1, ';
    $strSQL .= '     DCFLD1, ';
    $strSQL .= '     DCFILID2, ';
    $strSQL .= '     DCFLD2, ';
    $strSQL .= '     DCFILID3, ';
    $strSQL .= '     DCFLD3, ';
    $strSQL .= '     DCFILID4, ';
    $strSQL .= '     DCFLD4, ';
    $strSQL .= '     DCFILID5, ';
    $strSQL .= '     DCFLD5, ';
    $strSQL .= '     DCFILID6, ';
    $strSQL .= '     DCFLD6, ';
    $strSQL .= '     DCSUMF, ';
	//Added by TTA
    $strSQL .= '     DCFNAME1, ';
    $strSQL .= '     DCFNAME2, ';
    $strSQL .= '     DCFNAME3, ';
    $strSQL .= '     DCFNAME4, ';
    $strSQL .= '     DCFNAME5, ';
    $strSQL .= '     DCFNAME6 ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2COLM ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     DCNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLM:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLM:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2COLM($db2con,$NEWQRYNM,$db2colmdata,$newLib=''){
    foreach($db2colmdata as $db2colm){
        $data   = array();
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2COLM';
        }else{
            $strSQL .= ' INSERT INTO DB2COLM';
        }
        $strSQL .= '        ( ';
        $strSQL .= '     DCNAME, ';
        $strSQL .= '     DCFILID1, ';
        $strSQL .= '     DCFLD1, ';
        $strSQL .= '     DCFILID2, ';
        $strSQL .= '     DCFLD2, ';
        $strSQL .= '     DCFILID3, ';
        $strSQL .= '     DCFLD3, ';
        $strSQL .= '     DCFILID4, ';
        $strSQL .= '     DCFLD4, ';
        $strSQL .= '     DCFILID5, ';
        $strSQL .= '     DCFLD5, ';
        $strSQL .= '     DCFILID6, ';
        $strSQL .= '     DCFLD6, ';
        $strSQL .= '     DCSUMF, ';
		//Added by TTA
        $strSQL .= '     DCFNAME1, ';
        $strSQL .= '     DCFNAME2, ';
        $strSQL .= '     DCFNAME3, ';
        $strSQL .= '     DCFNAME4, ';
        $strSQL .= '     DCFNAME5, ';
        $strSQL .= '     DCFNAME6 ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
            $NEWQRYNM,
            cmMer($db2colm['DCFILID1']),
            cmMer($db2colm['DCFLD1']),
            cmMer($db2colm['DCFILID2']),
            cmMer($db2colm['DCFLD2']),
            cmMer($db2colm['DCFILID3']),
            cmMer($db2colm['DCFLD3']),
            cmMer($db2colm['DCFILID4']),
            cmMer($db2colm['DCFLD4']),
            cmMer($db2colm['DCFILID5']),
            cmMer($db2colm['DCFLD5']),
            cmMer($db2colm['DCFILID6']),
            cmMer($db2colm['DCFLD6']),
            cmMer($db2colm['DCSUMF']),
			//Added by TTA
            cmMer($db2colm['DCFNAME1']),
            cmMer($db2colm['DCFNAME2']),
            cmMer($db2colm['DCFNAME3']),
            cmMer($db2colm['DCFNAME4']),
            cmMer($db2colm['DCFNAME5']),
            cmMer($db2colm['DCFNAME6'])
        );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2COLM:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2COLM:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
/*
 *  DB2COLT取得
 */
function fnGetDB2COLT($db2con,$QRYNM){
    $params = array();
    $data   = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     DTNAME, ';
    $strSQL .= '     DTSEQ, ';
    $strSQL .= '     DTFILID, ';
    $strSQL .= '     DTFLD, ';
    $strSQL .= '     DTSUM, ';
    $strSQL .= '     DTAVLG, ';
    $strSQL .= '     DTMINV, ';
    $strSQL .= '     DTMAXV, ';
    $strSQL .= '     DTCONT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2COLT ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     DTNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLM:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2COLM:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2COLT($db2con,$NEWQRYNM,$db2coltdata,$newLib=''){
    foreach($db2coltdata as $db2colt){
        $data   = array();
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2COLT';
        }else{
            $strSQL .= ' INSERT INTO DB2COLT';
        }
        $strSQL .= '        ( ';
        $strSQL .= '     DTNAME, ';
        $strSQL .= '     DTSEQ, ';
        $strSQL .= '     DTFILID, ';
        $strSQL .= '     DTFLD, ';
        $strSQL .= '     DTSUM, ';
        $strSQL .= '     DTAVLG, ';
        $strSQL .= '     DTMINV, ';
        $strSQL .= '     DTMAXV, ';
        $strSQL .= '     DTCONT ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
            $NEWQRYNM,
            cmMer($db2colt['DTSEQ']),
            cmMer($db2colt['DTFILID']),
            cmMer($db2colt['DTFLD']),
            cmMer($db2colt['DTSUM']),
            cmMer($db2colt['DTAVLG']),
            cmMer($db2colt['DTMINV']),
            cmMer($db2colt['DTMAXV']),
            cmMer($db2colt['DTCONT'])
            );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2COLT:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2COLT:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
