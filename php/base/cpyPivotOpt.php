<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyPivotOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetDB2PMST($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ピボット設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2pmst = $rs['data'];
        e_log(print_r($db2pmst,true));
        if(count($db2pmst)>0){
            $rs = fnInsDB2PMST($db2con,$NEWQRYNM,$db2pmst);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('ピボット設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2PCOL($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('ピボット設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2pcol = $rs['data'];
            e_log(print_r($db2pcol,true));
            if(count($db2pcol)>0){
                $rs = fnInsDB2PCOL($db2con,$NEWQRYNM,$db2pcol);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('ピボット設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2PCAL($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('ピボット設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2pcal = $rs['data'];
            e_log(print_r($db2pcal,true));
            if(count($db2pcal)>0){
                $rs = fnInsDB2PCAL($db2con,$NEWQRYNM,$db2pcal);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('ピボット設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyPivotOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnGetDB2PMST($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ピボット設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2pmst = $rs['data'];
        if(count($db2pmst)>0){
            $rs = fnInsDB2PMST($db2con,$QRYNM,$db2pmst,$newLib);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('ピボット設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2PCOL($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('ピボット設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2pcol = $rs['data'];
            if(count($db2pcol)>0){
                $rs = fnInsDB2PCOL($db2con,$QRYNM,$db2pcol,$newLib);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('ピボット設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2PCAL($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('ピボット設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2pcal = $rs['data'];
            if(count($db2pcal)>0){
                $rs = fnInsDB2PCAL($db2con,$QRYNM,$db2pcal,$newLib);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('ピボット設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}

/*
 *  DB2PMST取得
 */
function fnGetDB2PMST($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     PMNAME, ';
    $strSQL .= '     PMPKEY, ';
    $strSQL .= '     PMTEXT, ';
	$strSQL .= '     PMDFLG '; //TTA
    //$strSQL .= '     PVNTFLG ';  
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2PMST ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     PMNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PMST:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PMST:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2PMST($db2con,$NEWQRYNM,$db2pmstdata,$newLib=''){
    foreach($db2pmstdata as $db2pmst){
        $data   = array();
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2PMST';
        }else{
            $strSQL .= ' INSERT INTO DB2PMST';
        }
        $strSQL .= '        ( ';
        $strSQL .= '     PMNAME, ';
        $strSQL .= '     PMPKEY, ';
        $strSQL .= '     PMTEXT, ';
		$strSQL .= '     PMDFLG ';  //TTA
        //$strSQL .= '     PVNTFLG ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
            $NEWQRYNM,
            cmMer($db2pmst['PMPKEY']),
            cmMer($db2pmst['PMTEXT']),

			$db2pmst['PMDFLG'], //TTA
            //$db2pmst['PVNTFLG']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2PMST:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2PMST:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
/*
 *  DB2PCOL取得
 */
function fnGetDB2PCOL($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL = ' SELECT ';
    $strSQL .= '     WPNAME, ';
    $strSQL .= '     WPPKEY, ';
    $strSQL .= '     WPPFLG, ';
    $strSQL .= '     WPSEQN, ';
    $strSQL .= '     WPFILID, ';
    $strSQL .= '     WPFLD, ';
    $strSQL .= '     WPFHIDE, ';
    $strSQL .= '     WPSORT, ';
    $strSQL .= '     WPSUMG ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2PCOL ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     WPNAME = ? ';

    $params = array($QRYNM);
   $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PCOL:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PCOL:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2PCOL($db2con,$NEWQRYNM,$db2pcoldata,$newLib=''){
    foreach($db2pcoldata as $db2pcol){
        $data   = array();
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2PCOL';
        }else{
            $strSQL .= ' INSERT INTO DB2PCOL';
        }
        $strSQL .= '        ( ';
        $strSQL .= '     WPNAME, ';
        $strSQL .= '     WPPKEY, ';
        $strSQL .= '     WPPFLG, ';
        $strSQL .= '     WPSEQN, ';
        $strSQL .= '     WPFILID, ';
        $strSQL .= '     WPFLD, ';
        $strSQL .= '     WPFHIDE, ';
        $strSQL .= '     WPSORT, ';
        $strSQL .= '     WPSUMG ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
            $NEWQRYNM,
            cmMer($db2pcol['WPPKEY']),
            cmMer($db2pcol['WPPFLG']),
            cmMer($db2pcol['WPSEQN']),
            cmMer($db2pcol['WPFILID']),
            cmMer($db2pcol['WPFLD']),
            cmMer($db2pcol['WPFHIDE']),
            cmMer($db2pcol['WPSORT']),
            cmMer($db2pcol['WPSUMG'])
        );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2PCOL:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2PCOL:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
/*
 *  DB2PCAL取得
 */
function fnGetDB2PCAL($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL = '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     WCNAME, ';
    $strSQL .= '     WCCKEY, ';
    $strSQL .= '     WCSEQN, ';
    $strSQL .= '     WCCALC ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2PCAL ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     WCNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PCAL:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PCAL:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2PCAL($db2con,$NEWQRYNM,$db2pcaldata,$newLib=''){
    foreach($db2pcaldata as $db2pcal){
        $data   = array();
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2PCAL';
        }else{
            $strSQL .= ' INSERT INTO DB2PCAL';
        }
        $strSQL .= '        ( ';
        $strSQL .= '     WCNAME, ';
        $strSQL .= '     WCCKEY, ';
        $strSQL .= '     WCSEQN, ';
        $strSQL .= '     WCCALC ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
            $NEWQRYNM,
            cmMer($db2pcal['WCCKEY']),
            cmMer($db2pcal['WCSEQN']),
            cmMer($db2pcal['WCCALC'])
        );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2PCAL:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2PCAL:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}

