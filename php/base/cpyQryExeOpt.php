<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyQryExeOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetQryExeOpt($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('クエリー実行設定'.$rs['result'].$rs['errcd']);
    }else{
        $qryexe = $rs['data'];
        if(count($qryexe)>0){
            $qryexe = $qryexe[0];
            $rs = fnUpdQryExeOpt($db2con,$NEWQRYNM,$qryexe);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('クエリー実行設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyQryExeOpt_OTHER($db2con,$db2conNew,$QRYNM){
    $rtn = '';
    $rs = fnGetQryExeOpt($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('クエリー実行設定'.$rs['result'].$rs['errcd']);
    }else{
        $qryexe = $rs['data'];
        if(count($qryexe)>0){
            $qryexe = $qryexe[0];
            $rs = fnUpdQryExeOpt($db2conNew,$NEWQRYNM,$qryexe);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('クエリー実行設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}

/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetQryExeOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL  = ' SELECT ';
    $strSQL .= '     D1DISF, ';
    $strSQL .= '     DISCSN, ';
    $strSQL .= '     D1EFLG ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     D1NAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnUpdQryExeOpt($db2con,$NEWQRYNM,$qryexe){
    $data   = array();
    $params = array();
    $strSQL = '';
    $strSQL .= ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= '    D1DISF      = ? , ';
    $strSQL .= '    DISCSN      = ? , ';
    $strSQL .= '    D1EFLG      = ?  ';
    $strSQL .= ' WHERE  ';
    $strSQL .= '    D1NAME      = ? ';
    
    $params =array(
                $qryexe['D1DISF'],
                $qryexe['DISCSN'],
                $qryexe['D1EFLG'],
                $NEWQRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    e_log($strSQL.print_r($params,true));
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
