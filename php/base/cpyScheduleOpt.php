<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyScheduleOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetDB2WSCD($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2wscd = $rs['data'];
        e_log(print_r($db2wscd,true));
        if(count($db2wscd)>0){
            $rs = fnInsDB2WSCD($db2con,$NEWQRYNM,$db2wscd);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2WAUT($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2waut = $rs['data'];
            e_log(print_r($db2waut,true));
            if(count($db2waut)>0){
                $rs = fnInsDB2WAUT($db2con,$NEWQRYNM,$db2waut);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2WAUL($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2waul = $rs['data'];
            e_log('db2waul'.print_r($db2waul,true));
            if(count($db2waul)>0){
                $rs = fnInsDB2WAUL($db2con,$NEWQRYNM,$db2waul);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetSCHFDB2CSV1($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
        }else{
            $fdb2csv1 = $rs['data'];
            e_log('fdb2csv1'.print_r($fdb2csv1,true));
            if(count($fdb2csv1)>0){
                $fdb2csv1 = $fdb2csv1[0];
                $rs = fnUpdSCHFDB2CSV1($db2con,$NEWQRYNM,$fdb2csv1);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2WSOC($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2wsoc = $rs['data'];
            e_log('db2wsoc'.print_r($db2wsoc,true));
            if(count($db2wsoc)>0){
                $rs = fnInsDB2WSOC($db2con,$NEWQRYNM,$db2wsoc);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}

/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyScheduleOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnGetDB2WSCD($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2wscd = $rs['data'];
        if(count($db2wscd)>0){
            $rs = fnInsDB2WSCD($db2con,$QRYNM,$db2wscd,$newLib);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2WAUT($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2waut = $rs['data'];
            if(count($db2waut)>0){
                $rs = fnInsDB2WAUT($db2con,$QRYNM,$db2waut,$newLib);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2WAUL($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2waul = $rs['data'];
            if(count($db2waul)>0){
                $rs = fnInsDB2WAUL($db2con,$QRYNM,$db2waul,$newLib);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetSCHFDB2CSV1($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
        }else{
            $fdb2csv1 = $rs['data'];
            if(count($fdb2csv1)>0){
                $fdb2csv1 = $fdb2csv1[0];
                $rs = fnUpdSCHFDB2CSV1($db2con,$QRYNM,$fdb2csv1,$newLib);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    if($rtn === ''){
        $rs = fnGetDB2WSOC($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = $rs['result'];
            e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
        }else{
            $db2wsoc = $rs['data'];
            if(count($db2wsoc)>0){
                $rs = fnInsDB2WSOC($db2conNew,$QRYNM,$db2wsoc,$newLib);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('スケジュール設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    return $rtn;
}

/*
 *  DB2WSCD取得
 */
function fnGetDB2WSCD($db2con,$QRYNM){
    $params = array();
    $data   = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     WSNAME, ';
    $strSQL .= '     WSPKEY, ';
    $strSQL .= '     WSFRQ, ';
    $strSQL .= '     WSODAY, ';
    $strSQL .= '     WSWDAY, ';
    $strSQL .= '     WSMDAY, ';
    $strSQL .= '     WSTIME, ';
    $strSQL .= '     WSSPND, ';
    $strSQL .= '     WSBDAY, ';
    $strSQL .= '     WSBTIM, ';
    $strSQL .= '     WSSDAY, ';
    $strSQL .= '     WSSTIM ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WSCD ';
    $strSQL .= ' WHERE WSNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WSCD:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WSCD:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2WSCD($db2con,$NEWQRYNM,$db2wscddata,$newLib=''){
    foreach($db2wscddata as $db2wscd){
        $flg = false;
        if(cmMer($db2wscd['WSPKEY']) === ''){
            $flg = true;
        }else{
            // check pivot
            $chkPivot = fnChkExistPivot($db2con,$NEWQRYNM,cmMer($db2wscd['WSPKEY']));
            if($chkPivot['result'] === true){
                $flg = true;
            }
        }
        if($flg){
            $data   = array();
            $params = array();
            $strSQL = '';
            if($newLib!==''){
                $strSQL .= ' INSERT INTO '.$newLib.'.DB2WSCD';
            }else{
                $strSQL .= ' INSERT INTO DB2WSCD';
            }
            $strSQL .= '        ( ';
            $strSQL .= '     WSNAME, ';
            $strSQL .= '     WSPKEY, ';
            $strSQL .= '     WSFRQ, ';
            $strSQL .= '     WSODAY, ';
            $strSQL .= '     WSWDAY, ';
            $strSQL .= '     WSMDAY, ';
            $strSQL .= '     WSTIME, ';
            $strSQL .= '     WSSPND, ';
            $strSQL .= '     WSBDAY, ';
            $strSQL .= '     WSBTIM, ';
            $strSQL .= '     WSSDAY, ';
            $strSQL .= '     WSSTIM ';
            $strSQL .= '        ) ';
            $strSQL .= ' VALUES ( ';
            $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,? ';
            $strSQL .= '        ) ';
            
            $params =array(
                $NEWQRYNM,
                cmMer($db2wscd['WSPKEY']),
                cmMer($db2wscd['WSFRQ']),
                cmMer($db2wscd['WSODAY']),
                cmMer($db2wscd['WSWDAY']),
                cmMer($db2wscd['WSMDAY']),
                cmMer($db2wscd['WSTIME']),
                cmMer($db2wscd['WSSPND']),
                0,
                0,
                0,
                0
            );
            $stmt = db2_prepare($db2con,$strSQL);
            e_log($strSQL.print_r($params,true));
            if($stmt === false ){
                $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WSCD:'.db2_stmt_errormsg()
                        );
                break;
            }else{
                $res = db2_execute($stmt,$params);
                if($res === false){
                    $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WSCD:'.db2_stmt_errormsg()
                        );
                    break;
                }else{
                    $data = array('result' => true);
                }
            }
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*
 *  DB2WAUT取得
 */
function fnGetDB2WAUT($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     WANAME, ';
    $strSQL .= '     WAPKEY, ';
    $strSQL .= '     WAMAIL, ';
    $strSQL .= '     WAMACC, ';
    $strSQL .= '     WACSV, ';
    $strSQL .= '     WAXLS, ';
    $strSQL .= '     WAHTML, ';
    $strSQL .= '     WASPND, ';
    $strSQL .= '     WASFLG, ';
    $strSQL .= '     WAFRAD, ';
    $strSQL .= '     WAFRNM, ';
    $strSQL .= '     WASUBJ, ';
    $strSQL .= '     WABODY, ';
    $strSQL .= '     WASNKB, ';
    $strSQL .= '     WATYPE, ';
    $strSQL .= '     WABAFID, ';
    $strSQL .= '     WABAFLD, ';
    $strSQL .= '     WABAFR, ';
    $strSQL .= '     WABATO, ';
    $strSQL .= '     WABFLG, ';
    $strSQL .= '     WABINC, ';
    $strSQL .= '     WAQUNM,  ';
    $strSQL .= '     WAFONT ';
    $strSQL .= ' FROM     DB2WAUT ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     WANAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WAUT:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WAUT:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2WAUT($db2con,$NEWQRYNM,$db2wautdata,$newLib=''){
    foreach($db2wautdata as $db2waut){
        $flg = false;
        e_log('Pivot key'.cmMer($db2waut['WAPKEY']));
        if(cmMer($db2waut['WAPKEY']) === ''){
            $flg = true;
        }else{
            // check pivot
            e_log('Pivot key1'.cmMer($db2waut['WAPKEY']));
            $chkPivot = fnChkExistPivot($db2con,$NEWQRYNM,cmMer($db2waut['WAPKEY']));
            if($chkPivot['result'] === true){
                $flg = true;
            }
        }
        if($flg){
            $data   = array();
            $params = array();
            $strSQL = '';
            if($newLib!==''){
                $strSQL .= ' INSERT INTO '.$newLib.'.DB2WAUT';
            }else{
                $strSQL .= ' INSERT INTO DB2WAUT';
            }
            $strSQL .= '        ( ';
            $strSQL .= '     WANAME, ';
            $strSQL .= '     WAPKEY, ';
            $strSQL .= '     WAMAIL, ';
            $strSQL .= '     WAMACC, ';
            $strSQL .= '     WACSV, ';
            $strSQL .= '     WAXLS, ';
            $strSQL .= '     WAHTML, ';
            $strSQL .= '     WASPND, ';
            $strSQL .= '     WASFLG, ';
            $strSQL .= '     WAFRAD, ';
            $strSQL .= '     WAFRNM, ';
            $strSQL .= '     WASUBJ, ';
            $strSQL .= '     WABODY, ';
            $strSQL .= '     WASNKB, ';
            $strSQL .= '     WATYPE, ';
            $strSQL .= '     WABAFID, ';
            $strSQL .= '     WABAFLD, ';
            $strSQL .= '     WABAFR, ';
            $strSQL .= '     WABATO, ';
            $strSQL .= '     WABFLG, ';
            $strSQL .= '     WABINC, ';
            $strSQL .= '     WAQUNM,  ';
            $strSQL .= '     WAFONT ';
            $strSQL .= '        ) ';
            $strSQL .= ' VALUES ( ';
            $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ';
            $strSQL .= '        ) ';
            
            $params =array(
                $NEWQRYNM,
                cmMer($db2waut['WAPKEY']),
                cmMer($db2waut['WAMAIL']),
                cmMer($db2waut['WAMACC']),
                cmMer($db2waut['WACSV']),
                cmMer($db2waut['WAXLS']),
                cmMer($db2waut['WAHTML']),
                cmMer($db2waut['WASPND']),
                cmMer($db2waut['WASFLG']),
                cmMer($db2waut['WAFRAD']),
                cmMer($db2waut['WAFRNM']),
                cmMer($db2waut['WASUBJ']),
                cmMer($db2waut['WABODY']),
                cmMer($db2waut['WASNKB']),
                cmMer($db2waut['WATYPE']),
                cmMer($db2waut['WABAFID']),
                cmMer($db2waut['WABAFLD']),
                cmMer($db2waut['WABAFR']),
                cmMer($db2waut['WABATO']),
                cmMer($db2waut['WABFLG']),
                cmMer($db2waut['WABINC']),
                cmMer($db2waut['WAQUNM']),
                cmMer($db2waut['WAFONT'])
            );
            $stmt = db2_prepare($db2con,$strSQL);
            e_log($strSQL.print_r($params,true));
            if($stmt === false ){
                $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WAUT:'.db2_stmt_errormsg()
                        );
                break;
            }else{
                $res = db2_execute($stmt,$params);
                if($res === false){
                    $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WAUT:'.db2_stmt_errormsg()
                        );
                    break;
                }else{
                    $data = array('result' => true);
                }
            }
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*
 *  DB2WAUL取得
 */
function fnGetDB2WAUL($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     WLNAME, ';
    $strSQL .= '     WLPKEY, ';
    $strSQL .= '     WLMAIL, ';
    $strSQL .= '     WLSEQL, ';
    $strSQL .= '     WLDATA ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WAUL ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     WLNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PCAL:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PCAL:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    e_log('kekka'.print_r($data,true));
    return $data;
}

function fnInsDB2WAUL($db2con,$NEWQRYNM,$db2wauldata,$newLib=''){
    foreach($db2wauldata as $db2waul){
        $flg = false;
        if(cmMer($db2waul['WLPKEY']) === ''){
            $flg = true;
        }else{
            // check pivot
            $chkPivot = fnChkExistPivot($db2con,$NEWQRYNM,cmMer($db2waul['WLPKEY']));
            if($chkPivot['result'] === true){
                $flg = true;
            }
        }
        if($flg){
             $data   = array();
            $params = array();
            $strSQL = '';
            if($newLib!==''){
                $strSQL .= ' INSERT INTO '.$newLib.'.DB2WAUL';
            }else{
                $strSQL .= ' INSERT INTO DB2WAUL';
            }
            $strSQL .= '        ( ';
            $strSQL .= '     WLNAME, ';
            $strSQL .= '     WLPKEY, ';
            $strSQL .= '     WLMAIL, ';
            $strSQL .= '     WLSEQL, ';
            $strSQL .= '     WLDATA ';
            $strSQL .= '        ) ';
            $strSQL .= ' VALUES ( ';
            $strSQL .= '            ?,?,?,?,? ';
            $strSQL .= '        ) ';
            
            $params =array(
                $NEWQRYNM,
                cmMer($db2waul['WLPKEY']),
                cmMer($db2waul['WLMAIL']),
                cmMer($db2waul['WLSEQL']),
                cmMer($db2waul['WLDATA'])
            );
            $stmt = db2_prepare($db2con,$strSQL);
            e_log($strSQL.print_r($params,true));
            if($stmt === false ){
                $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WAUL:'.db2_stmt_errormsg()
                        );
                break;
            }else{
                $res = db2_execute($stmt,$params);
                if($res === false){
                    $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WAUL:'.db2_stmt_errormsg()
                        );
                    break;
                }else{
                    $data = array('result' => true);
                }
            }
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*
 *  DB2WSOC取得
 */
function fnGetDB2WSOC($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     SONAME, ';
    $strSQL .= '     SOPKEY, ';
    $strSQL .= '     SO0MAL ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WSOC ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     SONAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WSOC:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WSOC:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2WSOC($db2con,$NEWQRYNM,$db2wsocdata,$newLib=''){
    foreach($db2wsocdata as $db2wsoc){
        $flg = false;
        if(cmMer($db2wsoc['SOPKEY']) === ''){
            $flg = true;
        }else{
            // check pivot
            $chkPivot = fnChkExistPivot($db2con,$NEWQRYNM,cmMer($db2wsoc['SOPKEY']));
            if($chkPivot['result'] === true){
                $flg = true;
            }
        }
        if($flg){
            $data   = array();
            $params = array();
            $strSQL = '';
            if($newLib!==''){
                $strSQL .= ' INSERT INTO '.$newLib.'.DB2WSOC';
            }else{
                $strSQL .= ' INSERT INTO DB2WSOC';
            }
            $strSQL .= '        ( ';
            $strSQL .= '     SONAME, ';
            $strSQL .= '     SOPKEY, ';
            $strSQL .= '     SO0MAL ';
            $strSQL .= '        ) ';
            $strSQL .= ' VALUES ( ';
            $strSQL .= '            ?,?,? ';
            $strSQL .= '        ) ';
            
            $params =array(
                $NEWQRYNM,
                cmMer($db2wsoc['SOPKEY']),
                cmMer($db2wsoc['SO0MAL'])
            );
            $stmt = db2_prepare($db2con,$strSQL);
            e_log($strSQL.print_r($params,true));
            if($stmt === false ){
                $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WSOC:'.db2_stmt_errormsg()
                        );
                break;
            }else{
                $res = db2_execute($stmt,$params);
                if($res === false){
                    $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WSOC:'.db2_stmt_errormsg()
                        );
                    break;
                }else{
                    $data = array('result' => true);
                }
            }
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*
 *  FDB2CSV1取得
 */
function fnGetSCHFDB2CSV1($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     D1HOKN, ';
    $strSQL .= '     D1DIRE, ';
    $strSQL .= '     D1CTFL, ';
    $strSQL .= '     D1DIEX ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     D1NAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function fnUpdSCHFDB2CSV1($db2con,$NEWQRYNM,$fdb2csv1,$newLib=''){
    $data   = array();
    $params = array();
    $strSQL = '';
    if($newLib!==''){
        $strSQL .= ' UPDATE '.$newLib.'.FDB2CSV1 ';
    }else{
        $strSQL .= ' UPDATE FDB2CSV1 ';
    }
    $strSQL .= ' SET ';
    $strSQL .= '    D1HOKN      = ? , ';
    $strSQL .= '    D1DIRE      = ? , ';
    $strSQL .= '    D1CTFL      = ? ,  ';
    $strSQL .= '    D1DIEX      = ? ';
    $strSQL .= ' WHERE  ';
    $strSQL .= '    D1NAME      = ? ';
    
    $params =array(
                $fdb2csv1['D1HOKN'],
                $fdb2csv1['D1DIRE'],
                $fdb2csv1['D1CTFL'],
                $fdb2csv1['D1DIEX'],
                $NEWQRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    e_log($strSQL.print_r($params,true));
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
function fnChkExistPivot($db2con,$qrynm,$pivotkey){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     PMNAME, ';
    $strSQL .= '     PMPKEY, ';
    $strSQL .= '     PMTEXT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2PMST ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     PMNAME = ? ';
    $strSQL .= ' AND PMPKEY = ? ';

    $params = array($qrynm,$pivotkey);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PMST:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2PMST:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $data = array('result' => 'NOT_EXIST');
            }else{
                $data = array('result' => true,'data' => $data);
            }
        }
    }
    return $data;
}
