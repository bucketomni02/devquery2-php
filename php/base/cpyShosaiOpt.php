<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("insTableData.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyShosaiOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetShosai($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('詳細情報設定'.$rs['result'].$rs['errcd']);
    }else{
        $shosai = $rs['data'];
        e_log(print_r($shosai,true));
        if(count($shosai)>0){
            $rs = fnInsShosai($db2con,$NEWQRYNM,$shosai);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('詳細情報設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        //詳細キーチェック
        $rs = getNotShosaiFld($db2con,$NEWQRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = $rs['result'];
        }else{
            $shosaidata = $rs['data'];
            if(count($shosaidata)>0){
                foreach ($shosaidata as $shosai){
                    $delDTL = delDB2WDTLBYSEQ($db2con,$NEWQRYNM,$shosai['DTFILID'],$shosai['DTFLD']);
                    if($delDTL['result'] !== true){
                        $rtn = 1;
                        $msg = $delDTL['result'];
                    }else{
                        $delDFL = delDB2WDFLBYSEQ($db2con,$NEWQRYNM,$shosai['DTFILID'],$shosai['DTFLD']);
                        if($delDFL['result'] !== true){
                            $rtn = 1;
                            $msg = $delDFL['result'];
                        }
                    }
                }
            }
        }    
    }
    if($rtn === ''){
        $rs = fnGetDrillDown($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = $rs['result'];
        }else{
            $drilldowndata = $rs['data'];
            e_log(print_r($drilldowndata,true));
            if(count($drilldowndata)>0){
                $rs = fnInsDB2DRGS($db2con,$NEWQRYNM,$drilldowndata);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('DrillDown情報設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    if($rtn === ''){
        //DrillDownチェック
        $rs = getNotDrilldownFld($db2con,$NEWQRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = $rs['result'];
        }else{
            $drillDown = $rs['data'];
            if(count($drillDown)>0){
                foreach ($drillDown as $drilldowndata){
                    $delDrgs = delDB2DRGSByFld ($db2con,$NEWQRYNM,$drilldowndata);
                    if($delDrgs['result'] !== true){
                        $rtn = 1;
                        $msg = $delDrgs['result'];
                    }
                }
            }
        }    
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyShosaiOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnGetShosai($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('詳細情報設定'.$rs['result'].$rs['errcd']);
    }else{
        $shosai = $rs['data'];
        e_log('詳細情報設定shosai'.print_r($shosai,true));
        if(count($shosai)>0){
            $rs = fnInsShosai($db2con,$QRYNM,$shosai,$newLib);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('詳細情報設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    if($rtn === ''){
        //詳細キーチェック
        $rs = getNotShosaiFld($db2con,$QRYNM,$newLib);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = $rs['result'];
        }else{
            $shosaidata = $rs['data'];
        e_log('詳細情報設定shosaidata'.print_r($shosaidata,true));
            if(count($shosaidata)>0){
                foreach ($shosaidata as $shosai){
                    $delDTL = delDB2WDTLBYSEQ($db2con,$QRYNM,$shosai['DTFILID'],$shosai['DTFLD'],$newLib);
                    if($delDTL['result'] !== true){
                        $rtn = 1;
                        $msg = $delDTL['result'];
                    }else{
                        $delDFL = delDB2WDFLBYSEQ($db2con,$QRYNM,$shosai['DTFILID'],$shosai['DTFLD'],$newLib);
                        if($delDFL['result'] !== true){
                            $rtn = 1;
                            $msg = $delDFL['result'];
                        }
                    }
                }
            }
        }    
    }
    if($rtn === ''){
        $rs = fnGetDrillDown($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = $rs['result'];
        }else{
            $drilldowndata = $rs['data'];
            e_log('詳細情報設定drilldowndata'.print_r($drilldowndata,true));
            if(count($drilldowndata)>0){
                $rs = fnInsDB2DRGS($db2con,$QRYNM,$drilldowndata,$newLib);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('DrillDown情報設定'.$rs['result'].$rs['errcd']);
                }
            }
        }
    }
    if($rtn === ''){
        //DrillDownチェック
        $rs = getNotDrilldownFld($db2con,$QRYNM,$newLib);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = $rs['result'];
        }else{
            $drillDown = $rs['data'];
            if(count($drillDown)>0){
                foreach ($drillDown as $drilldowndata){
                    $delDrgs = delDB2DRGSByFld ($db2con,$QRYNM,$drilldowndata,$newLib);
                    if($delDrgs['result'] !== true){
                        $rtn = 1;
                        $msg = $delDrgs['result'];
                    }
                }
            }
        }    
    }
    return $rtn;
}


function fnGetShosai($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     DTNAME, ';
    $strSQL .= '     DTFILID, ';
    $strSQL .= '     DTFLD, ';
    $strSQL .= '     DTLIBL, ';
    $strSQL .= '     DTMBR, ';
    $strSQL .= '     DTFILE, ';
    $strSQL .= '     DFFILID, ';
    $strSQL .= '     DFCOLM, ';
    $strSQL .= '     DFFKEY, ';
    $strSQL .= '     DFCHECK ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WDTL A ';
    $strSQL .= ' LEFT JOIN DB2WDFL B ';
    $strSQL .= ' ON A.DTNAME = B.DFNAME ';
    $strSQL .= ' AND A.DTFILID = B.DFFILID ';
    $strSQL .= ' AND A.DTFLD = B.DFFLD ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.DTNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsShosai($db2con,$NEWQRYNM,$shosaiData,$newLib=''){
    $data   = array();
    foreach($shosaiData as $shosai){
        if( cmMer($shosai[DFFKEY]) === '1'){
            $params = array();
            $strSQL = '';
            if($newLib !==''){
                $strSQL .= ' INSERT INTO '.$newLib.'.DB2WDTL';
            }else{
                $strSQL .= ' INSERT INTO DB2WDTL';
            }
            $strSQL .= '        ( ';
            $strSQL .= '            DTNAME, ';
            $strSQL .= '            DTFILID, ';
            $strSQL .= '            DTFLD, ';
            $strSQL .= '            DTLIBL, ';
            $strSQL .= '            DTFILE, ';
            $strSQL .= '            DTMBR ';
            $strSQL .= '        ) ';
            $strSQL .= ' VALUES ( ';
            $strSQL .= '            ?,?,?,?,?,? ';
            $strSQL .= '        ) ';
            
            $params =array(
                        $NEWQRYNM,
                        $shosai['DTFILID'],
                        $shosai['DTFLD'],
                        $shosai['DTLIBL'],
                        $shosai['DTFILE'],
                        $shosai['DTMBR']
                     );
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false ){
                $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WDTL:'.db2_stmt_errormsg()
                        );
                break;
            }else{
                $res = db2_execute($stmt,$params);
                if($res === false){
                    $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WDTL:'.db2_stmt_errormsg()
                        );
                    break;
                }else{
                    $data = array('result' => true);
                }
            }
        }else{
            $data = array('result' => true);
        }
        if($data['result']){
            $params = array();
            $strSQL = '';
            if($newLib!==''){
                $strSQL .= ' INSERT INTO '.$newLib.'.DB2WDFL';
            }else{
                $strSQL .= ' INSERT INTO DB2WDFL';
            }
            $strSQL .= '        ( ';
            $strSQL .= '            DFNAME, ';
            $strSQL .= '            DFFILID, ';
            $strSQL .= '            DFFLD, ';
            $strSQL .= '            DFCOLM, ';
            $strSQL .= '            DFFKEY, ';
            $strSQL .= '            DFCHECK ';
            $strSQL .= '        ) ';
            $strSQL .= ' VALUES ( ';
            $strSQL .= '            ?,?,?,?,?,? ';
            $strSQL .= '        ) ';
            
            $params =array(
                $NEWQRYNM,
                $shosai['DTFILID'],
                $shosai['DTFLD'],
                $shosai['DFCOLM'],
                $shosai['DFFKEY'],
                $shosai['DFCHECK']
                     );
            $stmt = db2_prepare($db2con,$strSQL);
            e_log($strSQL.print_r($params,true));
            if($stmt === false ){
                $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                        );
                break;
            }else{
                $res = db2_execute($stmt,$params);
                if($res === false){
                    $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                        );
                    break;
                }else{
                    $data = array('result' => true);
                }
            }
        }
    }
    return $data;
}


function fnGetDrillDown($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     * ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2DRGS  ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     DRKMTN = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2DRGS:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2DRGS:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function fnInsDB2DRGS($db2con,$NEWQRYNM,$drilldowndata,$newLib=''){
    $data   = array();
    foreach($drilldowndata as $drilldown){
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2DRGS';
        }else{
            $strSQL .= ' INSERT INTO DB2DRGS';
        }
        $strSQL .= '        ( ';
        $strSQL .= '            DRKMTN, ';
        $strSQL .= '            DRKFID, ';
        $strSQL .= '            DRKFLID, ';
        $strSQL .= '            DRDMTN, ';
        $strSQL .= '            DRDFNM, ';
        $strSQL .= '            DRDFID, ';
        $strSQL .= '            DRDJSQ, ';
        $strSQL .= '            DRDCNT, ';
        $strSQL .= '            DRWEBF, ';
        $strSQL .= '            DRWEBK, ';
		$strSQL .= '            DRDFLG, ';//add by TTA
        $strSQL .= '            DRSFLG ';//add by TTA
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
                    $NEWQRYNM,
                    $drilldown['DRKFID'],
                    $drilldown['DRKFLID'],
                    $drilldown['DRDMTN'],
                    $drilldown['DRDFNM'],
                    $drilldown['DRDFID'],
                    $drilldown['DRDJSQ'],
                    $drilldown['DRDCNT'],
                    $drilldown['DRWEBF'],
                    $drilldown['DRWEBK'],
					$drilldown['DRDFLG'],//add by TTA
                    $drilldown['DRSFLG']//add by TTA
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2DRGS:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2DRGS:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}

