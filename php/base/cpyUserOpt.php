<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyUserOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    // コピー先クエリーのユーザーオプション情報取得
    $rs = fnGetDB2WDEF($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ユーザー設定'.$rs['result'].$rs['errcd']);
    }else{
        $db2wdef = $rs['data'];
        if(count($db2wdef)>0){
            $rs = fnInsDB2WDEF($db2con,$NEWQRYNM,$db2wdef);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('ユーザー設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}

/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetDB2WDEF($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL  = ' SELECT ';
    $strSQL .= '     WDUID, ';
    $strSQL .= '     WDDWNL, ';
    $strSQL .= '     WDDWN2 ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WDEF ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     WDNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WDEF:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2WDEF:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnInsDB2WDEF($db2con,$NEWQRYNM,$db2wdefdata){
    $data   = array();
    foreach($db2wdefdata as $db2wdef){
        $params = array();
        $strSQL = '';
        $strSQL .= ' INSERT INTO DB2WDEF ';
        $strSQL .= '        ( ';
        $strSQL .= '            WDUID, ';
        $strSQL .= '            WDNAME, ';
        $strSQL .= '            WDDWNL, ';
        $strSQL .= '            WDDWN2 ';         
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
                    $db2wdef['WDUID'],
                    $NEWQRYNM,
                    $db2wdef['WDDWNL'],
                    $db2wdef['WDDWN2']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
