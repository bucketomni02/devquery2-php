<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyUsrHojoOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetUsrHojoOpt($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ユーザー補助設定'.$rs['result'].$rs['errcd']);
    }else{
        $usrHojo = $rs['data'];
        if(count($usrHojo)>0){
            $usrHojo = $usrHojo[0];
            $rs = fnUpdUsrHojoOpt($db2con,$NEWQRYNM,$usrHojo);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('ユーザー補助設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyUsrHojoOpt_OTHER($db2con,$db2conNew,$QRYNM){
    $rtn = '';
    $rs = fnGetUsrHojoOpt($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('ユーザー補助設定'.$rs['result'].$rs['errcd']);
    }else{
        $usrHojo = $rs['data'];
        if(count($usrHojo)>0){
            $usrHojo = $usrHojo[0];
            $rs = fnUpdUsrHojoOpt($db2conNew,$QRYNM,$usrHojo);
            if($rs['result'] !== true){
                $rtn = $rs['result'];
                e_log('ユーザー補助設定'.$rs['result'].$rs['errcd']);
            }
        }
    }
    return $rtn;
}

/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetUsrHojoOpt($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     D1NAME, ';
    $strSQL .= '     D1INFO, ';
    $strSQL .= '     D1INFG, ';
	$strSQL .= '     D1INFGR ';//add by TTA
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     D1NAME = ? ';
    $strSQL .= ' AND (D1INFO <> \'\' ';
    $strSQL .= ' OR D1INFG <> \'\') ';

    $params = array($QRYNM);
    e_log($strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnUpdUsrHojoOpt($db2con,$NEWQRYNM,$usrHojo){
    $data   = array();
    $params = array();
    $strSQL = '';
    $strSQL .= ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= '    D1INFO      = ? , ';
    $strSQL .= '    D1INFG      = ? , ';
	$strSQL .= '    D1INFGR     = ?  ';//add by TTA
    $strSQL .= ' WHERE  ';
    $strSQL .= '    D1NAME      = ? ';
    
    $params =array(
                $usrHojo['D1INFO'],
                $usrHojo['D1INFG'],
				$usrHojo['D1INFGR'],//add by TTA
                $NEWQRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    e_log($strSQL.print_r($params,true));
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
