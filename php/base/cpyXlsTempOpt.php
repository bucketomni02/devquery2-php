<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
*-------------------------------------------------------* 
* 処理:選択したオプションのコピー呼び出し
* パラメータ ：①コピー先クエリー名
*            :②コピー元クエリー名
*-------------------------------------------------------*
*/

function cpyXlsTempOpt($db2con,$QRYNM,$NEWQRYNM){
    $rtn = '';
    $rs = fnGetXlsTemp($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
    }else{
        $XLSTemp = $rs['data'];
        e_log(print_r($XLSTemp,true));
        if(count($XLSTemp)>0){
            $XLSTemp = $XLSTemp[0];
            if(cmMer($XLSTemp[D1TMPF]) !== '' || cmMer($XLSTemp[D1EDKB] === '2')){
                $rs = fnUpdXlsTemp($db2con,$NEWQRYNM,$XLSTemp);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
                }
            }
            if(cmMer($XLSTemp[D1EDKB] === '2')){
                if($rtn === ''){
                    $rs = fnGetDB2ECON($db2con,$QRYNM);
                    if($rs['result'] !== true){
                        $rtn = $rs['result'];
                         e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
                    }else{
                        $xlsEconTemp = $rs['data'];
                        e_log(print_r($xlsEconTemp,true));
                        if(count($xlsEconTemp)>0){
                            $xlsEconTemp = $xlsEconTemp[0];
                            $rs = fnInsDB2ECON($db2con,$NEWQRYNM,$xlsEconTemp);
                            if($rs['result'] !== true){
                                $rtn = $rs['result'];
                                e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
                            }
                        }
                    }
                }
                if($rtn === ''){
                    $rs = fnGetDB2EINS($db2con,$QRYNM);
                    if($rs['result'] !== true){
                        $rtn = $rs['result'];
                         e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
                    }else{
                        $xlsEinsTemp = $rs['data'];
                        e_log(print_r($xlsEinsTemp,true));
                        if(count($xlsEinsTemp)>0){
                            $rs = fnInsDB2EINS($db2con,$NEWQRYNM,$xlsEinsTemp);
                            if($rs['result'] !== true){
                                $rtn = $rs['result'];
                                e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
                            }
                        }
                    }
                }
            }
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 別のDBのため、処理:選択したオプションのコピー呼び出し
* パラメータ ：①別のDB
*            :②クエリー名
*-------------------------------------------------------*
*/

function cpyXlsTempOpt_OTHER($db2con,$newLib,$QRYNM){
    $rtn = '';
    $rs = fnGetXlsTemp($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = $rs['result'];
        e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
    }else{
        $XLSTemp = $rs['data'];
        e_log('EXCELTEMPLATE実行設定 $XLSTemp'.print_r($XLSTemp,true));
        if(count($XLSTemp)>0){
            $XLSTemp = $XLSTemp[0];
            if(cmMer($XLSTemp[D1TMPF]) !== '' || cmMer($XLSTemp[D1EDKB] === '2')){
                $rs = fnUpdXlsTemp($db2conNew,$QRYNM,$XLSTemp,$newLib);
                if($rs['result'] !== true){
                    $rtn = $rs['result'];
                    e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
                }
            }
            if(cmMer($XLSTemp[D1EDKB] === '2')){
                if($rtn === ''){
                    $rs = fnGetDB2ECON($db2con,$QRYNM);
                    if($rs['result'] !== true){
                        $rtn = $rs['result'];
                         e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
                    }else{
                        $xlsEconTemp = $rs['data'];
                        e_log(print_r($xlsEconTemp,true));
                        if(count($xlsEconTemp)>0){
                            $xlsEconTemp = $xlsEconTemp[0];
                            $rs = fnInsDB2ECON($db2con,$QRYNM,$xlsEconTemp,$newLib);
                            if($rs['result'] !== true){
                                $rtn = $rs['result'];
                                e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
                            }
                        }
                    }
                }
                if($rtn === ''){
                    $rs = fnGetDB2EINS($db2con,$QRYNM);
                    if($rs['result'] !== true){
                        $rtn = $rs['result'];
                         e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
                    }else{
                        $xlsEinsTemp = $rs['data'];
                        e_log(print_r($xlsEinsTemp,true));
                        if(count($xlsEinsTemp)>0){
                            $rs = fnInsDB2EINS($db2con,$QRYNM,$xlsEinsTemp,$newLib);
                            if($rs['result'] !== true){
                                $rtn = $rs['result'];
                                e_log('EXCELTEMPLATE実行設定'.$rs['result'].$rs['errcd']);
                            }
                        }
                    }
                }
            }
        }
    }
    return $rtn;
}

/*
 *  DB2WDEF取得
 * ユーザーにクエリー操作できる権限付与【実行、ダウンロード権限】
 */
function fnGetXlsTemp($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     D1NAME, ';
    $strSQL .= '     D1EDKB, ';
    $strSQL .= '     D1TMPF, ';
    $strSQL .= '     D1SHET, ';
    $strSQL .= '     D1TCOS, ';
    $strSQL .= '     D1TROS, ';
    $strSQL .= '     D1THFG ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 ';
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnUpdXlsTemp($db2con,$NEWQRYNM,$XLSTemp,$newLib=''){
    $data   = array();
    $params = array();

    if (cmMer($XLSTemp['D1TMPF']) !== '') {
        $ext    = explode('.', cmMer($XLSTemp['D1TMPF']));
        $ext    = $ext[count($ext) - 1];
        if (file_exists(BASE_DIR."/php/template/". cmMer($XLSTemp['D1NAME']) .'.'. $ext)) {
            $templateFlg = true;
        } else {
            $templateFlg = false;
        }
    }
    if($templateFlg){
        if (!copy(BASE_DIR."/php/template/". cmMer($XLSTemp['D1NAME']) .'.'. $ext, BASE_DIR."/php/template/" . cmMer($NEWQRYNM) .'.'. $ext)) {
            e_log('ファイルコピー失敗しました。');
        }
    }
    if(cmMer($XLSTemp['D1EDKB'] == '2')){
        $templateFlg = true;
    }
    if($templateFlg){
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' UPDATE '.$newLib.'.FDB2CSV1 ';
        }else{
            $strSQL .= ' UPDATE FDB2CSV1 ';
        }
        $strSQL .= ' SET ';
        $strSQL .= '    D1EDKB      = ? , ';
        $strSQL .= '    D1TMPF      = ? , ';
        $strSQL .= '    D1SHET      = ? , ';
        $strSQL .= '    D1TCOS      = ? , ';
        $strSQL .= '    D1TROS      = ? , ';
        $strSQL .= '    D1THFG      = ?  ';
        $strSQL .= ' WHERE  ';
        $strSQL .= '    D1NAME      = ? ';
        
    $params =array(
                $XLSTemp['D1EDKB'],
                $XLSTemp['D1TMPF'],
                $XLSTemp['D1SHET'],
                $XLSTemp['D1TCOS'],
                $XLSTemp['D1TROS'],
                $XLSTemp['D1THFG'],
                $NEWQRYNM
             );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_UPD',
                        'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                    );
        }else{
            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_UPD',
                        'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                    );
            }else{
                $data = array('result' => true);
            }
        }
    }else{
        e_log('ファイルこーピー失敗。ファイル見つかりません');
    }
    return $data;
}
function fnGetDB2ECON($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     * ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2ECON ';
    $strSQL .= ' WHERE ECNAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2ECON:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2ECON:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function fnGetDB2EINS($db2con,$QRYNM){
    $params = array();
    $data   = array();
    
    $strSQL .= ' SELECT ';
    $strSQL .= '     * ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2EINS ';
    $strSQL .= ' WHERE EINAME = ? ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2EINS:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2EINS:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function fnInsDB2ECON($db2con,$NEWQRYNM,$xlsEconData,$newLib=''){
    $data   = array();
    $params = array();
    $strSQL = '';
    if($newLib!==''){
        $strSQL .= ' INSERT INTO '.$newLib.'.DB2ECON';
    }else{
        $strSQL .= ' INSERT INTO DB2ECON';
    }
    $strSQL .= '        ( ';
    $strSQL .= '            ECNAME, ';
    $strSQL .= '            ECFLG, ';
    $strSQL .= '            ECROWN ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,? ';
    $strSQL .= '        ) ';
    
    $params =array(
                $NEWQRYNM,
                $xlsEconData['ECFLG'],
                $xlsEconData['ECROWN']
             );
    $stmt = db2_prepare($db2con,$strSQL);
    e_log($strSQL.print_r($params,true));
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'DB2ECON:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'DB2ECON:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
function fnInsDB2EINS($db2con,$NEWQRYNM,$xlsEinsData,$newLib=''){
    $data   = array();
    foreach($xlsEinsData as $xlsEins){
        $params = array();
        $strSQL = '';
        if($newLib!==''){
            $strSQL .= ' INSERT INTO '.$newLib.'.DB2EINS';
        }else{
            $strSQL .= ' INSERT INTO DB2EINS';
        }
        $strSQL .= '        ( ';
        $strSQL .= '            EINAME, ';
        $strSQL .= '            EISEQ, ';
        $strSQL .= '            EICOL, ';
        $strSQL .= '            EIROW, ';
        $strSQL .= '            EITEXT, ';
        $strSQL .= '            EISIZE, ';
        $strSQL .= '            EICOLR, ';
        $strSQL .= '            EIBOLD ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
                    $NEWQRYNM,
                    $xlsEins['EISEQ'],
                    $xlsEins['EICOL'],
                    $xlsEins['EIROW'],
                    $xlsEins['EITEXT'],
                    $xlsEins['EISIZE'],
                    $xlsEins['EICOLR'],
                    $xlsEins['EIBOLD']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r($params,true));
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2EINS:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'DB2EINS:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}