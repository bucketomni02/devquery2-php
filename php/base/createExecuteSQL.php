<?php
include_once ("comGetFDB2CSV2_CCSID.php");
include_once ("getQryTblData.php");
//include_once("common/inc/config.php");

//include_once ("chkResField.php");
//include_once ("chkEditResField.php");
/**
 * ===========================================================================================
 * SYSTEM NAME    : PHPQUERY2
 * PROGRAM NAME   : 作成したクエリーの実行SQL作成
 * PROGRAM ID     : createExecuteSQL.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/06/13
 * ===========================================================================================
 *
 */
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
/**
 * 定義情報と実行SQL作成ための呼び出し先機能
 * @param $db2con ：データベース接続
 * @param $QRYNM : 定義名
 * @param $PARAMLIST ：画面上実行の場合、画面上パラメータ
 * @戻り値：定義情報、実行SQL、実行パラメータ
 */
//ini_set('error_log', SAVE_DIR . SISTEM . '/log_Create.log');
function runExecuteSQL($db2con, $QRYNM, $PARAMLIST = NULL) {
    
    $rtn = 0;
    $msg = '';
    $strEXESQL = '';
    $strEXEPARAM = '';
    $LIBLIST = '';
    // クエリのSQL文作成ための定義情報取得呼び出し STREXECTESTSQL
    $result = getQryData($db2con, $QRYNM,false);
    if ($result['RTN'] !== 0) {
        $rtn = $result['RTN'];
        $msg = $result['MSG'];
    } else {
        // 実行SQL作成呼び出し
         //e_log("BASEFILEFRMDATA****".print_r($result['QRYDATA'],true));
        $resExecSql = getExecuteSQL($db2con, $result['QRYDATA'], $PARAMLIST);
       
        if ($resExecSql['RTN'] !== 0) {
            $rtn = $resExecSql['RTN'];
            $msg = $resExecSql['MSG'];
        } else {
            $STREXECTESTSQL = $resExecSql['STREXECTESTSQL'];
            $STREXECSQL = $resExecSql['STREXECSQL'];
            $arrLogSQL = $resExecSql['LOG_SQL'];
            $strEXEPARAM = $resExecSql['EXECPARAM'];
            $LIBLIST = $resExecSql['LIBLIST'];
            $SELRDB = $resExecSql['RDB'];
            $MBRDATALST = $resExecSql['MBRDATAARR'];
        }
    }
    $rtnData = array('RTN' => $rtn, 'MSG' => $msg, 'QRYDATA' => $result['QRYDATA'], 'STREXECTESTSQL' => $STREXECTESTSQL, 'STREXECSQL' => $STREXECSQL, 'LOG_SQL' => $arrLogSQL, 'EXECPARAM' => $strEXEPARAM, 'LIBLIST' => $LIBLIST, 'MBRDATALST' => $MBRDATALST, 'SELRDB' => $SELRDB);
    return $rtnData;
}
/**
 * 制御データ取得のため定義情報と実行SQL作成ための呼び出し先機能
 * @param $db2con ：データベース接続
 * @param $QRYNM : 定義名
 * @param $PARAMLIST ：画面上実行の場合、画面上パラメータ
 * @param $FILTERDATA : 画面上実行の場合,フィルタ情報
 * @param $BURSTITM ：メール実行の場合、バーストアイテム情報
 * @戻り値：定義情報、実行SQL、実行パラメータ
 */
function runExecuteSQLSEIGYORN($db2con, $QRYNM, $PARAMLIST = NULL, $FILTERDATA = '', $BURSTITM = array()) {
    error_log('制御のためデータ取得開始：'.$QRYNM);
    $rtn = 0;
    $msg = '';
    $strEXESQL = '';
    $strEXEPARAM = '';
    $LIBLIST = '';
    // クエリのSQL文作成ための定義情報取得呼び出し
    $result = getQryData($db2con, $QRYNM,true);
    if ($result['RTN'] !== 0) {
        $rtn = $result['RTN'];
        $msg = $result['MSG'];
    } else {
        // 実行SQL作成呼び出し
        $resExecSql = getExecuteSQL($db2con, $result['QRYDATA'], $PARAMLIST, $FILTERDATA, $BURSTITM, 1);
        if ($resExecSql['RTN'] !== 0) {
            $rtn = $resExecSql['RTN'];
            $msg = $resExecSql['MSG'];
        } else {
            $STREXECTESTSQL = $resExecSql['STREXECTESTSQL'];
            $STREXECSQL = $resExecSql['STREXECSQL'];
            $arrLogSQL = $resExecSql['LOG_SQL'];
            $strEXEPARAM = $resExecSql['EXECPARAM'];
            $LIBLIST = $resExecSql['LIBLIST'];
            $SELRDB = $resExecSql['RDB'];
            $MBRDATALST = $resExecSql['MBRDATAARR'];
        }
    }
    $rtnData = array('RTN' => $rtn, 'MSG' => $msg, 'QRYDATA' => $result['QRYDATA'], 'STREXECTESTSQL' => $STREXECTESTSQL, 'STREXECSQL' => $STREXECSQL, 'LOG_SQL' => $arrLogSQL, 'EXECPARAM' => $strEXEPARAM, 'LIBLIST' => $LIBLIST, 'MBRDATALST' => $MBRDATALST, 'SELRDB' => $SELRDB);
    return $rtnData;
}
/**
 * 定義情報と実行SQL作成ための呼び出し先機能
 * 【ピボットの場合】
 * @param $db2con ：データベース接続
 * @param $QRYNM : 定義名
 * @param $PIVID ：ピボットキー
 * @param $PARAMLIST ：画面上実行の場合、画面上パラメータ
 * @param $burstitm ：メールのときの【バーストアイテム情報】、バーストアイテム
 * @戻り値：定義情報、実行SQL、実行パラメータ
 */
function runExecuteSQLPIVOT($db2con, $QRYNM, $PIVID, $PARAMLIST = NULL, $burstitm = array()) {
    e_log('ピボットのためデータ取得開始：'.$QRYNM.'ピボットキー:'.$PIVID);
    $rtn = 0;
    $msg = '';
    $strEXESQL = '';
    $strEXEPARAM = '';
    $LIBLIST = '';
    // クエリのSQL文作成ための定義情報取得呼び出し
    $result = getQryData($db2con, $QRYNM,false);
    if ($result['RTN'] !== 0) {
        $rtn = $result['RTN'];
        $msg = $result['MSG'];
    } else {
        // 実行SQL作成呼び出し
        $resExecSql = getExecuteSQL($db2con, $result['QRYDATA'], $PARAMLIST, $FILTERDATA, $BURSTITM, 2, $PIVID);
        if ($resExecSql['RTN'] !== 0) {
            $rtn = $resExecSql['RTN'];
            $msg = $resExecSql['MSG'];
        } else {
            $STREXECTESTSQL = $resExecSql['STREXECTESTSQL'];
            $STREXECSQL = $resExecSql['STREXECSQL'];
            $arrLogSQL = $resExecSql['LOG_SQL'];
            $strEXEPARAM = $resExecSql['EXECPARAM'];
            $LIBLIST = $resExecSql['LIBLIST'];
            $SELRDB = $resExecSql['RDB'];
            $MBRDATALST = $resExecSql['MBRDATAARR'];
        }
    }
    $rtnData = array('RTN' => $rtn, 'MSG' => $msg, 'QRYDATA' => $result['QRYDATA'], 'STREXECTESTSQL' => $STREXECTESTSQL, 'STREXECSQL' => $STREXECSQL, 'LOG_SQL' => $arrLogSQL, 'EXECPARAM' => $strEXEPARAM, 'LIBLIST' => $LIBLIST, 'MBRDATALST' => $MBRDATALST, 'SELRDB' => $SELRDB);
    return $rtnData;
}
/**
 * 実行して作成したSQLの確認
 * @param $db2con ：データベース接続
 * @param $strSQL ：実行SQL文
 * @param $params : 実行パラメータ
 * @param $libList : ライブラリーリスト
 * @param $mbrLst :　メンバーリスト
 * @戻り値：結果データ
 */
function execQry($db2con, $strSQL, $params, $libList, $mbrLst = array()) {
    $rtn = 0;
    $msg = '';
    $data = array();
    $strSQLtmp = $strSQL;
	error_log('max length '.CND_MAXQRY);
    if ($rtn === 0) {
		// 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
		if (strlen($strSQLtmp) > 32767) {
		    $rtn = 1;
		    $msg = showMsg('FAIL_BASE_LENLIMIT'); // 'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
		    error_log('msg1 = '.$msg);
		}
    }
/*
    //クエリー最大件数を取得
    if ($rtn === 0) {

        error_log('CND_MAXQRY='.CND_MAXQRY);

        //クエリー最大件数が0以上の場合、FETCH FIRSTで件数を制限する
        if(CND_MAXQRY > 0){
            $strSQL .= ' FETCH FIRST '.CND_MAXQRY.' ROWS ONLY ';
        }

    }
*/

    /*if(CRTQTEMPTBL_FLG === 1){
        if(count($mbrLst) > 0){
            foreach($mbrLst as $mbrdata){
                if($mbrdata['FILTYP'] === 'L' || $mbrdata['FILTYP'] === 'V'){
                    if($mbrdata['FILMBR'] !== ''){
                        $rs = dropTmpFile($db2con,'QTEMP',$mbrdata['TBLNM']);
                        $rs = createTmpFil($db2con,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                    }else{
                        $rs = dropTmpFile($db2con,'QTEMP',$mbrdata['TBLNM']);
                        $rs = createTmpFil($db2con,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                    }
                    if($rs !== true){
                        e_log('テーブル設定失敗');
                        $rtn = 1;
                        $msg = showMsg($rs);
                        break;
                    }
                }else{
                    $rs = dropFileMBR($db2con,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                    $rs = setFileMBR($db2con,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                    if($rs !== true){
                        e_log('テーブル設定失敗');
                        $rtn = 1;
                        $msg = showMsg($rs);
                        break;
                    }
                }
            }
        }
    }else{
        if(count($mbrLst) >0){
            foreach($mbrLst as $mbrdata){
                $rs = dropFileMBR($db2con,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                $rs = setFileMBR($db2con,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                if($rs !== true){
                    e_log('テーブル設定失敗');
                    $rtn = 1;
                    $msg = showMsg($rs);
                    break;
                }
            }
        }
    }*/
    
    if ($rtn === 0) {
        error_log('実行SQL createExecuteSQL MYAT START');
        $stmt = db2_prepare($db2con, $strSQLtmp);
        if ($stmt === false) {
            error_log('実行SQL createExecuteSQL MYAT false：' . $strSQLtmp.db2_stmt_errormsg());
            $rtn = 'FAIL_SEL';
            $msg = '設定したクエリーでSQLの実行準備中に失敗しました。</br> 設定をご確認ください。</br>【エラーコード：' . db2_stmt_errormsg() . '】';
        } else {
            error_log('実行SQL createExecuteSQL MYAT true ：' . $strSQLtmp);
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $rtn = 'FAIL_SEL';
                $msg = '設定したクエリーでSQLの実行中に失敗しました。</br> 設定をご確認ください。</br>【エラーコード：' . db2_stmt_errormsg() . '】';
            } else {
                $row = db2_fetch_assoc($stmt);
                if ($row === false && db2_stmt_errormsg() !== '') {
                    $rtn = 'FAIL_SEL';
                    $msg = '設定したクエリーでSQLの実行結果フェッチ中に失敗しました。</br> 設定をご確認ください。</br>【エラーコード：' . db2_stmt_errormsg() . '】';
                } else {
                    $data[] = $row;
                    while ($row = db2_fetch_assoc($stmt)) {
                        $data[] = $row;
                    }
                    $data = umEx($data, false);
                  
                }
            }
        }
    }
/*
    if ($rtn === 0) {
        $dataCnt = count($data);
        $dataRes = array();
        $sqlLMT = " ";
        $sqlLMT .= " SELECT COQRYLMT ";
        $sqlLMT .= " FROM ".MAINLIB."/DB2COF ";
        $sqlLMT .= " WHERE COQRYLMT >= ? ";
        $stmt = db2_prepare($db2con, $sqlLMT);
        if ($stmt === false) {
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
        } else {
            $cntParam = array($dataCnt);
            $r = db2_execute($stmt, $cntParam);
            if ($r === false) {
                $rtn=1;
                $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $dataRes[] = $row;
                }
				if(CND_MAXQRY != 0){
	                if (count($dataRes) == 0) {
	                    $rtn = 2;
	                    $msg = showMsg('FAIL_BASE_LENLIMIT')."<br/>制限を増やす場合、管理者メニューのシステム設定から【クエリー実行制限】を編集してください。";
	                }
				}
            }
        }
    }
*/
    
    //e_log('実行結果：'.print_r($data,true));
    $rtnArr = array('RTN' => $rtn, 'MSG' => $msg, 'STRSQL' => $strSQL, 'RESULTDATA' => $data);
    return $rtnArr;
}
/**
 * 定義の作成したSQL文で一時テーブル作成
 * @param $db2con ：データベース接続
 * @param $libList : ライブラリーリスト
 * @param $strSQL ：実行SQL文
 * @param $params : 実行パラメータ
 * @param $tmpTblNm ：一時テーブル名
 * @戻り値：一時テーブル名
 */
function createTmpTable($db2con, $libList, $strSQL, $params, $tmpTblNm, $mbrLst = array()) {
    $rtn = 0;
    $msg = '';
    //$tmpTblNm = '@MMMMMMMMM';
    $data = array();
    $selTblSQL = '';
    $selTblSQL.= '   SELECT ';
    $selTblSQL.= '          A.SYSTEM_TABLE_NAME TABLE_NAME  ';
    $selTblSQL.= '        , A.TABLE_TEXT ';
    $selTblSQL.= '        , A.SYSTEM_TABLE_SCHEMA AS TAB_SCHEMA  ';
    $selTblSQL.= '        , A.TABLE_TYPE  ';
    $selTblSQL.= '   FROM QSYS2/SYSTABLES A  ';
    //$selTblSQL .=  '      , QSYS2/SYSSCHEMAS B  ';
    $selTblSQL.= '      ,' . SYSSCHEMASLIB . '/SYSSCHEMAS B  ';
    $selTblSQL.= '       WHERE A.SYSTEM_TABLE_SCHEMA = B.SYSTEM_SCHEMA_NAME  ';
    $selTblSQL.= '       AND A.FILE_TYPE <> \'S\'  ';
    $selTblSQL.= '       AND A.TABLE_TYPE = \'T\'  ';
    $selTblSQL.= '       AND A.SYSTEM_TABLE_SCHEMA <> \'\'  ';
    $selTblSQL.= '       AND A.TABLE_SCHEMA = ? ';
    $selTblSQL.= '       AND A.TABLE_NAME = ?  ';
    $stmt = db2_prepare($db2con, $selTblSQL);
    if ($stmt === false) {
        $rtn = 'FAIL_SEL';
        $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
    } else {
        $param = array(SAVE_DB, $tmpTblNm);
        $r = db2_execute($stmt, $param);
        if ($r === false) {
            $rtn = 'FAIL_SEL';
            $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
            
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) > 0) {
                $delTblSQL = ' DROP TABLE ' . SAVE_DB . '/' . $tmpTblNm;
                $stmt = db2_prepare($db2con, $delTblSQL);
                if ($stmt === false) {
                    $rtn = 'FAIL_DEL';
                    $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
                    
                } else {
                    $param = array();
                    $r = db2_execute($stmt, $param);
                    if ($r === false) {
                        $rtn = 'FAIL_DEL';
                        $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
                        
                    } else {
                        e_log('一時テーブルに重複があるので削除完了', '1');
                    }
                }
            }
        }
    }
    if ($rtn === 0) {
        $rtnExeTMchk = comSetQryExecTimeMemory($db2con);
        //e_log('reach it ①SQL実行時間設定');
        
    }

    //クエリーはクエリー最大件数以上の場合　190315
    $msgmax = '';
    $resExec = execQry($db2con, $strSQL, $params, $libList, $mbrLst);

    //クエリー最大件数を取得
    if ($rtn === 0) {
        //クエリー最大件数が0以上の場合、FETCH FIRSTで件数を制限する
        if(CND_MAXQRY > 0){
            if(count($resExec['RESULTDATA']) > CND_MAXQRY){
                $msgmax = showMsg('クエリーの結果がシステム値の最大件数より多いため、処理を中断しました。</br> '.CND_MAXQRY.'件のみ表示されます。');
            }
            $strSQL .= ' FETCH FIRST '.CND_MAXQRY.' ROWS ONLY ';
        }

    }

    if ($rtn === 0) {
        $strCreate = '';
        $strCreate.= ' CREATE TABLE';
        $strCreate.= ' ' . SAVE_DB . '/' . $tmpTblNm . ' AS';
        $strCreate.= ' (' . $strSQL . ' )';
        $strCreate.= ' WITH DATA ';
        if ($rtn === 0) {
            // 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
            if (strlen($strCreate) > 32767) {
                $rtn = 1;
                $msg = showMsg('FAIL_BASE_LENLIMIT'); //'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
                
            }
        }
        if ($rtn === 0) {
            e_log('create table:SQL' . $strCreate . 'params' . print_r($params, true));
            $stmt = db2_prepare($db2con, $strCreate);
            if ($stmt === false) {
                $rtn = 'FAIL_SEL';
                $msg = 'クエリー実行準備失敗しました。' . db2_stmt_errormsg();
                e_log('実行エラー:' . $msg);
            } else {
                // e_log('create table:SQL'.$strCreate.'params'.print_r($params,true));
                $time_start = microtime(true);
                $r = db2_execute($stmt, $params);
                $time = microtime(true) - $time_start;
                error_log('execution time = '.$time);
                if ($r === false) {
                    $rtn = 'FAIL_SEL';
                    $msg = 'クエリー実行中失敗しました。' . db2_stmt_errormsg();
                    //e_log('エラーコード：'.db2_conn_error());
                    if (strpos(db2_stmt_errormsg(), 'SQLSTATE=57005 SQLCODE=-666') !== false) {
                        $msg = $msg . '<br/>制限を増やす場合、管理者メニューのシステム設定から【クエリー実行制限】を編集してください。';
                    }
                } else {
                    $msg = 'ワークテーブル作成完了' . $tmpTblNm;
                }
            }
        }
    }
    if ($rtn === 0) {
        $rtnExeTMchk = comResetQryExecTimeMemory($db2con);
    }
    //cmSetPHPQUERY($db2con);
    $rtnCreateTbl = array();
    $rtnCreateTbl['RTN'] = $rtn;
    $rtnCreateTbl['MSG'] = $msg;
    $rtnCreateTbl['TMPTBLNM'] = $tmpTblNm;
    $rtnCreateTbl['MSGMAX'] = $msgmax;
    $rtnCreateTbl['QRYCNT'] = count($resExec['RESULTDATA']);
    return $rtnCreateTbl;
}
/**
 * 定義の作成したSQL文で出力ファイルのテーブル作成
 * @param $db2con ：データベース接続
 * @param $filename ：実行からの一時テーブル名
 * @param $d1outlib ：出力ライブラリー
 * @param $d1outfil ：出力ファイル
 * @戻り値：一時テーブル名
 */
function createOutputFileTable($db2con, $filename, $d1outlib, $d1outfil,$d1name,$add_flg) {
    $rtn = 0;
    $msg = '';
    $ins = 0;
    $outCName = '';
    $add_cflg = ($add_flg == 1)?"true":"false";
    $data = array();
    if ($rtn === 0) {
        $r = delOutputFileTable($db2con, $d1outlib, $d1outfil,$add_cflg);
        if ($r['RTN'] !== 0) {
            $rtn = 1;
            $msg = $r['MSG'];
        }
        $ins = $r['INS'];
    }
    if ($rtn === 0 && SYSCOLCFLG !=='1' ) {
        $conn = cmDb2Con();
        cmSetPHPQUERY($conn);
        $rs = cmGetFDB2CSV2($conn, $d1name,false,false,false);
        if ($rs['result'] !== true) {
            $rtn = 1;
            $msg = $rs['result'];
        }else{
            $dta2_5 = $rs['data'];
            e_log("dta2_5".print_r($dta2_5,true));
        }
    }

    //d1nameからSQLクエリーor通常を取得
    if($rtn === 0){
        $d1sql = ' SELECT * FROM '.MAINLIB.'/FDB2CSV1 WHERE D1NAME = ? ';
        $stmt = db2_prepare($db2con,$d1sql);
        if($stmt === false){
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
        }else{
            $param = array($d1name);
            $r = db2_execute($stmt,$param);
            if($r === false){
                $rtn = 'FAIL_SEL';
                $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
            }else{
                $d1data = array();
                while ($row = db2_fetch_assoc($stmt)) {
                    $d1data[] = $row;
                }
            }
        }

    }

    if($rtn === 0){
        $d1seigyo = ' SELECT F.DCNAME FROM '.MAINLIB.'/DB2COLM AS F  WHERE F.DCNAME = ? ';
        $stmt = db2_prepare($db2con,$d1seigyo);
        if($stmt === false){
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
        }else{
            $param = array($d1name);
            $r = db2_execute($stmt,$param);
            if($r === false){
                $rtn = 'FAIL_SEL';
                $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
            }else{
                $d1segyo = array();
                while ($row = db2_fetch_assoc($stmt)) {
                    $d1segyo[] = $row;
                }
            }
        }
    }
    error_log('seigyou flg name = '.print_r($d1segyo,true));

   /* if($rtn === 0){
        $d1pivot = ' SELECT * FROM '.MAINLIB.'/DB2PMST  WHERE PMNAME = ?  AND PMPKEY = ?';
        $stmt = db2_prepare($db2con,$d1pivot);
        if($stmt === false){
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
        }else{
            $param = array($d1name,$pmpkey);
            $r = db2_execute($stmt,$param);
            if($r === false){
                $rtn = 'FAIL_SEL';
                $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
            }else{
                $d1pivot = array();
                while ($row = db2_fetch_assoc($stmt)) {
                    $d1pivot[] = $row;
                }
            }
        }
    }*/

    if ($rtn === 0) {
        $rtnExeTMchk = comSetQryExecTimeMemory($db2con);
        //e_log('reach it ①SQL実行時間設定');
        
    }
    //ピボットと制御なら全部のカラムを使わないので、まずSELECT QRY
    if ($rtn === 0) {
        $selTblSQL = '';
        $selTblSQL.= ' SELECT SYSTEM_COLUMN_NAME AS COLUMN_NAME, ';
        if(SYSCOLCFLG==='1'){
            $selTblSQL.='COLUMN_TEXT  AS COLUMN_HEADING ';
        }else{
            $selTblSQL.='COLUMN_HEADING ';
        }
        $selTblSQL.=' FROM ' . SYSCOLUMN2 . ' ';
        $selTblSQL.= 'WHERE TABLE_SCHEMA = ? AND   TABLE_NAME =? AND SYSTEM_COLUMN_NAME  NOT IN(?,?,?,?) ';
        $stmt = db2_prepare($db2con, $selTblSQL);
        if ($stmt === false) {
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
        } else {
            $param = array(SAVE_DB, $filename, 'ROWNUM', 'RN', 'XROWNUM', 'YROWNUM');

            $r = db2_execute($stmt, $param);
            if ($r === false) {
                $rtn = 'FAIL_SEL';
                $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
                
            } else {
                $sysdata = array();
                while ($row = db2_fetch_assoc($stmt)) {
	            //e_log("selTblSQL".print_r($row,true));
                    $sysdata[] = $row;
                    $outCName.= $row['COLUMN_NAME'] . ',';
                }
		e_log("selTblSQL".$selTblSQL.print_r($param,true));
                //一番後ろのコンマを消す
                $outCName = substr($outCName, 0, -1);
		e_log("Mondai : ".print_r($outCName,true));
            }
        }
    }
    
    // 既存テーブルコラムを追加のため取る
    if ($rtn === 0) {
        $selTblSQL = '';
        $selTblSQL.= ' SELECT SYSTEM_COLUMN_NAME AS COLUMN_NAME, ';
        if(SYSCOLCFLG==='1'){
            $selTblSQL.='COLUMN_TEXT  AS COLUMN_HEADING ';
        }else{
            $selTblSQL.='COLUMN_HEADING ';
        }
        $selTblSQL.=' FROM ' . SYSCOLUMN2 . ' ';
        $selTblSQL.= 'WHERE TABLE_SCHEMA = ? AND   TABLE_NAME =?';
        $stmt = db2_prepare($db2con, $selTblSQL);
        if ($stmt === false) {
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
        } else {
            $param = array($d1outlib, $d1outfil);

            $r = db2_execute($stmt, $param);
            if ($r === false) {
                $rtn = 'FAIL_SEL';
                $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
                
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
	            //e_log("selTblSQL".print_r($row,true));
                    $outICName.= $row['COLUMN_NAME'] . ',';
                }
		e_log("selTblSQL".$selTblSQL.print_r($param,true));
                //一番後ろのコンマを消す
                $outICName = substr($outCName, 0, -1);
		e_log("Mondai : ".print_r($outICName,true));		
            }
        }
    }
    
    e_log("INS : ".$ins);
    if($add_cflg === "true"){
        e_log("INS INS : ".$ins);
	    if($ins === 0){
	        e_log("APK:".$add_cflg."APK");
		    $add_cflg = "false"; 
		} else {
		    $oldTblArr = array();
			$insDatArr = array();
			$oldTblArr = getReqCols($db2con,SAVE_DB,$filename);
			$insDatArr = getReqCols($db2con,$d1outlib,$d1outfil);
			if(count($oldTblArr) == count($insDatArr)){
			    //$intersetCnt = array_intersect($oldTblArr, $insDatArr);
			    //e_log("CntArr".count($intersetCnt));
			    $retVal = chekTwoTblFld($oldTblArr,$insDatArr);
			    e_log("retVal:".$retVal);
			    if($retVal == 1){
			        e_log("ADD CASE\n".$outCName);
			        $insSQL = " ";
			        $insSQL.= "    INSERT INTO    ".$d1outlib. "/" .$d1outfil;
			        $insSQL.= "    ( ". $outICName ." ) ";
			        $insSQL.= "        SELECT  ".$outICName."  FROM   ". SAVE_DB ."/". $filename;
			        $insSQL.= " ";
			        $stmt = db2_prepare($db2con, $insSQL);
			        if($stmt === false){
			            $rtn = 1;
			        } else {
			            $r = db2_execute($stmt);
			            if($r === false){
			                $rtn = 1;
			            } else {
			                $msg = $d1outlib . '/' . $d1outfil . 'に追加しました';
			            }
			        }
			    } else {
			        $rtn = 11;
			        $msg = showMsg($d1outlib.'/'.$d1outfil.'に追加失敗しました。');
			    }
			} else {
			    $rtn = 11;
			    $msg = showMsg($d1outlib.'/'.$d1outfil.'に追加失敗しました。');
			}
		}
    }
    if ($rtn === 0) {
	    if($add_cflg === "false"){
		    //tmpを出力ファイルでコピー
			$strCreate = '';
			$strCreate.= ' CREATE TABLE ' . $d1outlib . '/' . $d1outfil . ' AS ';
			$strCreate.= ' ( SELECT ' . $outCName . ' FROM ' . SAVE_DB . '/' . $filename . ') WITH DATA';
			if ($rtn === 0) {
				// 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
				if (strlen($strCreate) > 32767) {
					$rtn = 1;
					$msg = showMsg('FAIL_BASE_LENLIMIT'); //'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
					
				}
			}
			e_log('tmpを出力ファイルでコピー' . $strCreate);
			if ($rtn === 0) {
				$stmt = db2_prepare($db2con, $strCreate);
				if ($stmt === false) {
					$rtn = 'FAIL_SEL';
					$msg = '出力ファイル準備失敗しました。' . db2_stmt_errormsg();
				} else {
					$r = db2_execute($stmt);
					if ($r === false) {
						$rtn = 'FAIL_SEL';
						$msg = '出力ファイル中失敗しました。' . db2_stmt_errormsg();
					} else {
                       error_log('count sysdata = '.count($sysdata).'sys array = '.print_r($sysdata,true));
                       error_log('count dta2_5 = '.count($dta2_5).'dta array = '.print_r($dta2_5,true));

					   $labelSql = '';
        				if(SYSCOLCFLG === '1'){
            				foreach ($sysdata as $key => $value) {
            					$labelSql.= $value['COLUMN_NAME'] . ' IS \'' . $value['COLUMN_HEADING'] . '\'';
            					if (($key + 1) < count($sysdata)) {
            						$labelSql.= ',';
            					}
            				}
        				}else{
                            e_log('D1CFLG='.$d1data[0]['D1CFLG']);
            				foreach ($sysdata as $key => $value) {                               
            					$labelSql .= $value['COLUMN_NAME']. ' IS \'' . $dta2_5[$key]['D2HED'] . '\'';
            					if (($key + 1) < count($sysdata)) {
            						$labelSql.= ',';
            					}
            				}
        				}
						$sysSQL = '';
						$sysSQL.= ' LABEL ON COLUMN ' . $d1outlib . '/' . $d1outfil . ' ( ';
						$sysSQL.= $labelSql;
						$sysSQL.= ' ) ';
				        e_log("sysSQL".$sysSQL);
						$stmt = db2_prepare($db2con, $sysSQL);
						if ($stmt === false) {
							$rtn = 'FAIL_SEL';
							$msg = 'ラベル作成準備失敗しました。' . db2_stmt_errormsg();
							e_log('出力ファイルエラー:' . $msg);
						} else {
							$r = db2_execute($stmt);
							if ($r === false) {
								$rtn = 'FAIL_SEL';
								$msg = 'ラベル作成中失敗しました。' . db2_stmt_errormsg();
							} else {
								$msg = $d1outlib . '/' . $d1outfil . 'に出力しました';
							}
						}
					}
				}
			}   
		}
    }
    
    /*//klyh output file
    if($rtn===0){
        $sqlTbl=' SELECT * FROM '.$d1outlib . '.' . $d1outfil ;
        $stmt = db2_prepare($db2con,$sqlTbl);
        if($stmt === false){
            $dataTmp = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt);
            if($r === false){
                $dataTmp = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $dataTmp[] = $row;
                }
                $dataTmp = umEx($dataTmp,true);
            }
        }
        e_log("tmpを出力ファイルでコピー $d1outlib=>".$d1outlib.'d1outfil=>'.$d1outfil.print_r($dataTmp,true));
    }*/
    if ($rtn === 0) {
        $rtnExeTMchk = comResetQryExecTimeMemory($db2con);
    }
    $rtnCreateTbl = array();
    $rtnCreateTbl['RTN'] = $rtn;
    $rtnCreateTbl['MSG'] = $msg;
    return $rtnCreateTbl;
}

/**
 * 追加のためデータベースに存在したテーブルとTmpテーブルの「コラム、項目、長さ」を比べる
 * @param $arr1 ：Tmpテーブル
 * @param $arr2 ：存在したテーブル
 */
function chekTwoTblFld($arr1,$arr2){
    $matchCnt = 0;
    $matched_arr = array();
    foreach($arr1 as $a){
        $cnt = 0;
        foreach($arr2 as $b){
            $cnt++;
            if(count($matched_arr) > 0){
                if(!in_array($cnt,$matched_arr)){
                    if( in_array($a['SYSTEM_COLUMN_NAME'],$b) && 
                    in_array($a['DATA_TYPE'],$b) && 
                    in_array($a['LENGTH'],$b) ){
                    array_push($matched_arr,$cnt);
                    $matchCnt++;
                    break;
                    }
                }
            } else {
                if( in_array($a['SYSTEM_COLUMN_NAME'],$b) && 
                in_array($a['DATA_TYPE'],$b) && 
                in_array($a['LENGTH'],$b) ){
                array_push($matched_arr,$cnt);
                $matchCnt++;
                break;
                }
            }
        }
    }
    
    if($matchCnt == 0 || $matchCnt < count($arr1)){
        return -1;
    }
    
    return 1;
}

/**
 * 追加のためデータベースに存在したテーブルとTmpテーブルの「コラム、項目、長さ」を比べる
 * @param $arr1 ：存在したテーブル
 * @param $arr2 ：Tmpテーブル
 */
function getReqCols($db2con,$libNm,$filNm){
    $rtnData = array();
    $selTblSQL = '';
    $selTblSQL.= '    SELECT ';
    $selTblSQL.= '        SYSTEM_COLUMN_NAME ';
    $selTblSQL.= '      , DATA_TYPE ';
    $selTblSQL.= '      , LENGTH ';
    $selTblSQL.= '      FROM QSYS2/SYSCOLUMNS ';
    $selTblSQL.= '      WHERE TABLE_SCHEMA = ? ';
    $selTblSQL.= '      AND TABLE_NAME = ? '; 
    $selTblSQL.= '      AND SYSTEM_COLUMN_NAME  NOT IN(?,?,?,?) ';
    $stmt = db2_prepare($db2con, $selTblSQL);
    if ($stmt === false) {
        $rtn = 'FAIL_SEL';
        $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
    } else {
		$param = array($libNm, $filNm, 'ROWNUM', 'RN', 'XROWNUM', 'YROWNUM');
        $r = db2_execute($stmt, $param);
		if($r === false){
		    $rtn=1;
            $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
		} else {
		    while ($row = db2_fetch_assoc($stmt)) {
                $rtnData[] = $row;
            }
		}
    }
    return $rtnData;
}

/**
 * 出力ファイルの古いテーブルを消す
 * @param $db2con ：データベース接続
 * @param $d1outlib ：出力ライブラリー
 * @param $d1outfil ：出力ファイル
 */
function delOutputFileTable($db2con, $d1outlib, $d1outfil, $add_cflg) {
    $rtn = 0;
    $msg = '';
    $data = array();
    $insData = 0;
    $selTblSQL = '';
    $selTblSQL.= '   SELECT ';
    $selTblSQL.= '          A.SYSTEM_TABLE_NAME TABLE_NAME  ';
    $selTblSQL.= '        , A.TABLE_TEXT ';
    $selTblSQL.= '        , A.SYSTEM_TABLE_SCHEMA AS TAB_SCHEMA  ';
    $selTblSQL.= '        , A.TABLE_TYPE  ';
    $selTblSQL.= '   FROM QSYS2/SYSTABLES A  ';
    $selTblSQL.= '      ,' . SYSSCHEMASLIB . '/SYSSCHEMAS B  ';
    $selTblSQL.= '       WHERE A.SYSTEM_TABLE_SCHEMA = B.SYSTEM_SCHEMA_NAME  ';
    $selTblSQL.= '       AND A.FILE_TYPE <> \'S\'  ';
    $selTblSQL.= '       AND A.TABLE_TYPE = \'T\'  ';
    $selTblSQL.= '       AND A.SYSTEM_TABLE_SCHEMA <> \'\'  ';
    $selTblSQL.= '       AND A.TABLE_SCHEMA = ? ';
    $selTblSQL.= '       AND A.TABLE_NAME = ?  ';
    $stmt = db2_prepare($db2con, $selTblSQL);
    if ($stmt === false) {
        $rtn = 'FAIL_SEL';
        $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
    } else {
        $param = array($d1outlib, $d1outfil);
        $r = db2_execute($stmt, $param);
        if ($r === false) {
            //$rtn = 'FAIL_SEL';
			$rtn=1;
            $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
            
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) > 0) {
                if($add_cflg === "false"){
                    e_log("Drop Drop Drop");
					$delTblSQL = ' DROP TABLE ' . $d1outlib . '/' . $d1outfil;
					e_log($delTblSQL);
					$stmt = db2_prepare($db2con, $delTblSQL);
					if ($stmt === false) {
						$rtn =1;
						$msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
					} else {
						$param = array();
						$r = db2_execute($stmt, $param);
						if ($r === false) {
							$rtn =1;
							$msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
							if ($d1outlib !== '' && $d1outfil !== '') {
								$msg = $msg . '<br/>' . showMsg('FAIL_OUTDEL', array($d1outlib, $d1outfil));
							}
						}
					}
                } else {
				    $insData = 1;
				}
            } else {
			    $insData = 0;
			}
        }
    }
    return array('RTN' => $rtn, 'MSG' => $msg, 'INS' => $insData);
}
// 定義の一時テーブル作成
function createTmpTableRDB($db2con, $db2tblcon, $RDBNM, $libList, $strSQL, $params, $tmpTblNm, $mbrLst = array()) {
    $rtn = 0;
    $msg = '';
    $data = array();
    $selTblSQL = '';
    $selTblSQL.= '   SELECT ';
    $selTblSQL.= '          A.SYSTEM_TABLE_NAME TABLE_NAME  ';
    $selTblSQL.= '        , A.TABLE_TEXT ';
    $selTblSQL.= '        , A.SYSTEM_TABLE_SCHEMA AS TAB_SCHEMA  ';
    $selTblSQL.= '        , A.TABLE_TYPE  ';
    $selTblSQL.= '   FROM QSYS2/SYSTABLES A  ';
    //$selTblSQL .=  '      , QSYS2/SYSSCHEMAS B  ';
    $selTblSQL.= '        , ' . SYSSCHEMASLIB . '/SYSSCHEMAS B  ';
    $selTblSQL.= '       WHERE A.SYSTEM_TABLE_SCHEMA = B.SYSTEM_SCHEMA_NAME  ';
    $selTblSQL.= '       AND A.FILE_TYPE <> \'S\'  ';
    $selTblSQL.= '       AND A.TABLE_TYPE = \'T\'  ';
    $selTblSQL.= '       AND A.SYSTEM_TABLE_SCHEMA <> \'\'  ';
    $selTblSQL.= '       AND A.TABLE_SCHEMA = ? ';
    $selTblSQL.= '       AND A.TABLE_NAME = ?  ';
    $stmt = db2_prepare($db2tblcon, $selTblSQL);
    if ($stmt === false) {
        $rtn = 'FAIL_SEL';
        $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
        
    } else {
        $param = array(SAVE_DB, $tmpTblNm);
        $r = db2_execute($stmt, $param);
        if ($r === false) {
            $rtn = 'FAIL_SEL';
            $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
            
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) > 0) {
                $delTblSQL = ' DROP TABLE ' . SAVE_DB . '/' . $tmpTblNm;
                $stmt = db2_prepare($db2tblcon, $delTblSQL);
                if ($stmt === false) {
                    $rtn = 'FAIL_DEL';
                    $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
                    
                } else {
                    $param = array();
                    $r = db2_execute($stmt, $param);
                    if ($r === false) {
                        $rtn = 'FAIL_DEL';
                        $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
                        
                    } else {
                        e_log('一時テーブルに重複があるので削除完了', '1');
                    }
                }
            }
        }
    }
    if (CRTQTEMPTBL_FLG === 1) {
        // CRTQTEMPTBL_FLGは【1】の場合メンバーの設定とテーブルをQTEMPにコピーする
        if (count($mbrLst) > 0) {
            foreach ($mbrLst as $mbrdata) {
                if ($mbrdata['FILTYP'] === 'L' || $mbrdata['FILTYP'] === 'V') {
                    if ($mbrdata['FILMBR'] !== '') {
                        //$rs = dropFileMBR($db2con,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                        $rs = setFileMBR($db2con, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                    } else {
                        $rs = dropTmpFile($db2con, 'QTEMP', $mbrdata['TBLNM']);
                        $rs = createTmpFil($db2con, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                    }
                    if ($rs !== true) {
                        e_log('テーブル設定失敗');
                        $rtn = 1;
                        $msg = showMsg($rs);
                        break;
                    }
                }
            }
        }
    } else {
        if (count($mbrLst) > 0) {
            foreach ($mbrLst as $mbrdata) {
                //$rs = dropFileMBR($db2con,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                $rs = setFileMBR($db2con, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                if ($rs !== true) {
                    e_log('テーブル設定失敗');
                    $rtn = 1;
                    $msg = showMsg($rs);
                    break;
                }
            }
        }
    }
    if ($rtn === 0) {
        //実行時間設定
        $rtnExeTMchk = comSetQryExecTimeMemory($db2tblcon);
        //e_log('reach it 実行時間設定');
        
    }

    //クエリーはクエリー最大件数以上の場合　190315
    $msgmax = '';
    $resExec = execQry($db2tblcon, $strSQL, $params, $libList, $mbrLst);

    //クエリー最大件数を取得
    if ($rtn === 0) {
        //クエリー最大件数が0以上の場合、FETCH FIRSTで件数を制限する
        if(CND_MAXQRY > 0){
            if(count($resExec['RESULTDATA']) > CND_MAXQRY){
                $msgmax = showMsg('クエリーの結果がシステム値の最大件数より多いため、処理を中断しました。</br> '.CND_MAXQRY.'件のみ表示されます。');
            }
            $strSQL .= ' FETCH FIRST '.CND_MAXQRY.' ROWS ONLY ';
        }
    }

    if ($rtn === 0) {
        $strCreate = '';
        $strCreate.= ' CREATE TABLE';
        $strCreate.= ' ' . SAVE_DB . '/' . $tmpTblNm . ' AS';
        $strCreate.= ' (' . $strSQL . ' )';
        $strCreate.= ' WITH DATA ';
        if ($rtn === 0) {
            // 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
            if (strlen($strCreate) > 32767) {
                $rtn = 1;
                $msg = showMsg('FAIL_BASE_LENLIMIT'); //'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
                
            }
        }
        $stmt = db2_prepare($db2tblcon, $strCreate);
    
        if ($stmt === false) {
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行準備失敗しました。' . db2_stmt_errormsg();
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $rtn = 'FAIL_SEL';
                $msg = 'クエリー実行中失敗しました。' . db2_stmt_errormsg();
                //e_log('エラーコード：'.db2_conn_error());
                if (strpos(db2_stmt_errormsg(), 'SQLSTATE=57005 SQLCODE=-666') !== false) {
                    $msg = $msg . '<br/>制限を増やす場合、管理者メニューのシステム設定から【クエリー実行制限】を編集してください。';
                }
            } else {
                $msg = 'ワークテーブル作成完了' . $tmpTblNm;
                 
            }
        }
    }
    if ($rtn === 0) {
        //実行時間をリセットする
        $rtnExeTMchk = comResetQryExecTimeMemory($db2tblcon);
    }
    $rtnCreateTbl = array();
    $rtnCreateTbl['RTN'] = $rtn;
    $rtnCreateTbl['MSG'] = $msg;
    $rtnCreateTbl['TMPTBLNM'] = $tmpTblNm;
    $rtnCreateTbl['MSGMAX'] = $msgmax;
    $rtnCreateTbl['QRYCNT'] = count($resExec['RESULTDATA']);
    return $rtnCreateTbl;
}
/**
 * 定義を実行するためのSQL文作成
 * @param $db2con ：データベース接続
 * @param $qrydata : データベースから取得した定義情報
 * @param $cndparamList ：画面上から実行なら画面上のパラメータデータ
 * @param $filterData :画面上から実行なら、フィルタ情報
 * @param $burstItm  :メール実行の場合、バーストアイテム情報
 * @param $rnFlg     : 実行フラグ：　0 ：標準実行
 *                             1 :制御実行
 *                             2 :ピボット実行
 * @param $pivid     : ピボットID
 * @戻り値：実行SQL文とパラメータ
 */
function getExecuteSQL($db2con, $qrydata, $cndparamList, $filterData = '', $burstItm = array(), $rnFlg = 0, $pivid = '') {
    //e_log('getExecuteSQLの実行クエリーSQL文取得：'.print_r($qrydata,true));// QRYDATA , STREXECTESTSQL
	//e_log('getExecuteSQLの実行クエリーSQL文取得 rnFlg：'.$rnFlg);
    $rtn = 0;
    $msg = '';
    $params = array();
    $strPTable = '';
    $strSTable = '';
    $strEXESQL = '';
    $selRDB = '';
    $mbrTblLst = array();
    if ($rtn === 0) {
        if (count($qrydata['FDB2CSV1'] > 0)) {
            $fdb2csv1 = $qrydata['FDB2CSV1'][0];
            // 区画
            $selRDB = $fdb2csv1['D1RDB'];
            // ライブラリーリスト作成
            $libArr = preg_split("/\s+/", $fdb2csv1['D1LIBL']);
            $LIBLIST = join(' ', $libArr);
            // プラマリーテーブル
            $strPTable.= ' P ';
            $mbrdata = array();
            if ($fdb2csv1['D1FILLIB'] === '*LIBL' || $fdb2csv1['D1FILLIB'] === '*USRLIBL' || cmMer($fdb2csv1['D1FILLIB']) === '') {
                $mbrdata['LIBL'] = '*LIBL';
            } else {
                $mbrdata['LIBL'] = $fdb2csv1['D1FILLIB'];
            }
            $mbrdata['FILE'] = $fdb2csv1['D1FILE'];
            $mbrdata['TBLNM'] = ' P';
            if ($fdb2csv1['D1FILMBR'] === '' || $fdb2csv1['D1FILMBR'] === '*FIRST') {
                $mbrdata['FILMBR'] = '';
            } else if ($fdb2csv1['D1FILMBR'] === '*LAST') {
                // '*LAST'取得
                $mbrdata['FILMBR'] = fnGetFilLast($fdb2csv1['D1RDB'], $fdb2csv1['D1LIBL'], $fdb2csv1['D1FILLIB'], $fdb2csv1['D1FILE']);
            } else {
                $mbrdata['FILMBR'] = $fdb2csv1['D1FILMBR'];
            }
            //e_log('【削除】テーブルタイプ取得にエラー②：'.print_r($LIBLIST,true));
            $mbrdata['FILTYP'] = getTableType($fdb2csv1['D1RDB'], $libArr, $fdb2csv1['D1FILLIB'], $fdb2csv1['D1FILE'], $breftblfld['RTRMBR']);
            $mbrTblLst[] = $mbrdata;
        } else {
            $rtn = 1;
            $msg = 'クエリーの実行SQL文作成失敗【定義の基本情報】。';
        }
    }
    // 参照テーブルと結合フィールドの連結
    if ($rtn === 0) {
        if (count($qrydata['BREFTBLFLD'] > 0)) {
            $breftblflddata = $qrydata['BREFTBLFLD'];
            $rtrseq = '';
            foreach ($breftblflddata as $breftblfld) {
                if ($rtrseq !== $breftblfld['RTRSEQ']) {
                    $rtrseq = $breftblfld['RTRSEQ'];
                    $strSTable.= ' ' . $breftblfld['RTJTYP'];
                    $strSTable.= ' S' . $breftblfld['RTRSEQ'];
                    $mbrdata = array();
                    if ($breftblfld['RTRLIB'] === '' || $breftblfld['RTRLIB'] === '*LIBL' || $breftblfld['RTRLIB'] === '*USRLIBL') {
                        $mbrdata['LIBL'] = '*LIBL';
                    } else {
                        $mbrdata['LIBL'] = $breftblfld['RTRLIB'];
                    }
                    $mbrdata['FILE'] = $breftblfld['RTRFIL'];
                    $mbrdata['TBLNM'] = 'S' . cmMer($breftblfld['RTRSEQ']);
                    if ($breftblfld['RTRMBR'] === '' || $breftblfld['RTRMBR'] === '*FIRST') {
                        $mbrdata['FILMBR'] = '';
                    } else if ($breftblfld['RTRMBR'] === '*LAST') {
                        // '*LAST'取得
                        $mbrdata['FILMBR'] = fnGetFilLast($fdb2csv1['D1RDB'], $fdb2csv1['D1LIBL'], $breftblfld['RTRLIB'], $breftblfld['RTRFIL']);
                    } else {
                        $mbrdata['FILMBR'] = $breftblfld['RTRMBR'];
                    }
                    $mbrdata['FILTYP'] = getTableType($fdb2csv1['D1RDB'], $libArr, $breftblfld['RTRLIB'], $breftblfld['RTRFIL'], $breftblfld['RTRMBR']);
                    $mbrTblLst[] = $mbrdata;
                }
                if ($breftblfld['RTJTYP'] !== 'CROSS JOIN') {
                    if ($rtrseq === $breftblfld['RTRSEQ']) {
                        if ($breftblfld['RFFSEQ'] == 1) {
                            $strSTable.= ' ON ';
                        } else {
                            $strSTable.= ' AND ';
                        }
                    }
                    $refPfnm = $breftblfld['RFPFNM'];
                    $refRfnm = $breftblfld['RFRFNM'];
                    if ($breftblfld['RFPFTYPE'] === $breftblfld['RFRFTYPE']) {
                        switch ($breftblfld['RFPFTYPE']) {
                            case 'L':
                                switch ($breftblfld['RFRKBN']) {
                                    case 'LIKE':
                                    case 'NLIKE':
                                    case 'LLIKE':
                                    case 'LNLIKE':
                                    case 'RLIKE':
                                    case 'RNLIKE':
                                        $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(10))';
                                        $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(10))';
                                    break;
                                    default:
                                    break;
                                }
                            break;
                            case 'T':
                                switch ($breftblfld['RFRKBN']) {
                                    case 'LIKE':
                                    case 'NLIKE':
                                    case 'LLIKE':
                                    case 'LNLIKE':
                                    case 'RLIKE':
                                    case 'RNLIKE':
                                        $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(8))';
                                        $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(8))';
                                    break;
                                    default:
                                    break;
                                }
                            break;
                            case 'Z':
                                switch ($breftblfld['RFRKBN']) {
                                    case 'LIKE':
                                    case 'NLIKE':
                                    case 'LLIKE':
                                    case 'LNLIKE':
                                    case 'RLIKE':
                                    case 'RNLIKE':
                                        $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(26))';
                                        $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(26))';
                                    break;
                                    default:
                                    break;
                                }
                            break;
                            default:
								switch ($breftblfld['RFTEISU']) {
		                            case '1':
										switch ($breftblfld['RFRFTYPE']) {
											//e_log('Entering Flag 1 = '.$fileInfo['RTYP' . $rcnt . _ . $kcnt]);
				                            case 'P':
				                            case 'S':
				                            case 'B':
											break;
				                            default:
						                    	$refRfnm = '\''. $refRfnm .'\'';
				                            break;
		                        		}
		                            break;
		                            default:
		                            break;
                            	}
                            break;
                        }
                    } else {
                        switch ($breftblfld['RFPFTYPE']) {
                            case 'L':
                                $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(10))';
                                $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(' . $breftblfld['RFRFLEN'] . '))';
                            break;
                            case 'T':
                                $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(8))';
                                $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(' . $breftblfld['RFRFLEN'] . '))';
                            break;
                            case 'Z':
                                $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(26))';
                                $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(' . $breftblfld['RFRFLEN'] . '))';
                            break;
                            case 'P':
                            case 'S':
                            case 'B':
                                switch ($breftblfld['RFRFTYPE']) {
                                    case 'L':
                                        $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $breftblfld['RFPFLEN'] . '))';
                                        $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(10))';
                                    break;
                                    case 'T':
                                        $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $breftblfld['RFPFLEN'] . '))';
                                        $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(8))';
                                    break;
                                    case 'Z':
                                        $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $breftblfld['RFPFLEN'] . '))';
                                        $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(26))';
                                    break;
                                    case 'P':
                                    case 'S':
                                    case 'B':
                                    break;
                                    default:
                                        $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $breftblfld['RFPFLEN'] . '))';
                                    break;
                                }
                            break;
                            default:
                                switch ($breftblfld['RFRFTYPE']) {
                                    case 'L':
                                        $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $breftblfld['RFPFLEN'] . '))';
                                        $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(10))';
                                    break;
                                    case 'T':
                                        $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $breftblfld['RFPFLEN'] . '))';
                                        $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(8))';
                                    break;
                                    case 'Z':
                                        $refPfnm = 'CAST(' . $refPfnm . ' AS CHAR(' . $breftblfld['RFPFLEN'] . '))';
                                        $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(26))';
                                    break;
                                    case 'P':
                                    case 'S':
                                    case 'B':
                                        $refRfnm = 'CAST(' . $refRfnm . ' AS CHAR(' . $breftblfld['RFRFLEN'] . '))';
                                    break;
                                    default:
                                    break;
                                }
                            break;
                        }
                    }
				
                    switch ($breftblfld['RFRKBN']) {						
                        case 'EQ':							
                            $strSTable.= $refPfnm . ' = ' . $refRfnm;
                        break;
                        case 'NE':
                            $strSTable.= $refPfnm . ' <> ' . $refRfnm;
                        break;
                        case 'GE':
                            $strSTable.= $refPfnm . ' >= ' . $refRfnm;
                        break;
                        case 'GT':
                            $strSTable.= $refPfnm . ' < ' . $refRfnm;
                        break;
                        case 'LE':
                            $strSTable.= $refPfnm . ' <= ' . $refRfnm;
                        break;
                        case 'LT':
                            $strSTable.= $refPfnm . ' < ' . $refRfnm;
                        break;
                        case 'LIKE':
                            $strSTable.= $refPfnm . ' LIKE ' . 'concat(concat(\'%\',' . $refRfnm . '),\'%\')';
                        break;
                        case 'LLIKE':
                            $strSTable.= $refPfnm . ' LIKE ' . 'concat(\'%\',' . $refRfnm . ')';
                        break;
                        case 'RLIKE':
                            $strSTable.= $refPfnm . ' LIKE ' . 'concat(' . $refRfnm . ',\'%\')';
                        break;
                        case 'NLIKE':
                            $strSTable.= $refPfnm . ' NOT LIKE ' . 'concat(concat(\'%\',' . $refRfnm . '),\'%\')';
                        break;
                        case 'NLLIKE':
                            $strSTable.= $refPfnm . ' NOT LIKE ' . 'concat(\'%\',' . $refRfnm . ')';
                        break;
                        case 'NRLIKE':
                            $strSTable.= $refPfnm . ' NOT LIKE ' . 'concat(' . $refRfnm . ',\'%\')';
                        break;
                        default:
                            $strSTable.= $refPfnm . ' = ' . $refRfnm;
                        break;
                    }
                }
            }
        } else {
            $rtn = 1;
            $msg = 'クエリーの実行SQL文作成失敗【定義の参照ファイル情報】。';
        }
    }
    // 抽出対象全てのテブールのフィールドと結果フィールドの連結
    // 抽出対象全てのテブールのフィールド
    if ($rtn === 0) {
        $strTblFld = '';
        $strTblFldArr = array();
        $strTblFldNmArr = array();
        if (count($qrydata['FDB2CSV2'] > 0)) {
            $ccsidData = $qrydata['FDB2CSV2CCSID'];
            $fdb2csv2FldLst = $qrydata['FDB2CSV2FLDLST'];
            $fdb2csv2data = $qrydata['FDB2CSV2'];
            foreach ($fdb2csv2data as $fdb2csv2) {
                $key = array_search($fdb2csv2['D2FLD'] . '_' . $fdb2csv2['D2FILID'], $fdb2csv2FldLst);
                if ((int)$fdb2csv2['D2FILID'] === 0) {
                    if ($ccsidData[$key]['CCSID'] === '65535' && $ccsidData[$key]['DDS_TYPE'] !== '5' && $ccsidData[$key]['DDS_TYPE'] !== 'H') {

                        if($ccsidData[$key]['COLUMN_NAME'] === 'BREAKLVL' || $ccsidData[$key]['COLUMN_NAME'] === 'OVERFLOW'){
                            $strTblFldArr[] = 'CAST(P.' . $fdb2csv2['D2FLD'] . '  AS CHAR(4) CCSID 5026) AS ' . $fdb2csv2['D2FLD'] . '_' . $fdb2csv2['D2FILID'];
                        }else{
                            $strTblFldArr[] = 'CAST(P.' . $fdb2csv2['D2FLD'] . '  AS CHAR(' . $fdb2csv2['D2LEN'] . ') CCSID 5026) AS ' . $fdb2csv2['D2FLD'] . '_' . $fdb2csv2['D2FILID'];
                        }

                    } else {
                        $strTblFldArr[] = 'P.' . $fdb2csv2['D2FLD'] . ' AS ' . $fdb2csv2['D2FLD'] . '_' . $fdb2csv2['D2FILID'];
                    }
                } else {
                    if ($ccsidData[$key]['CCSID'] === '65535' && $ccsidData[$key]['DDS_TYPE'] !== '5' && $ccsidData[$key]['DDS_TYPE'] !== 'H') {

                        if($ccsidData[$key]['COLUMN_NAME'] === 'BREAKLVL' || $ccsidData[$key]['COLUMN_NAME'] === 'OVERFLOW'){
                            $strTblFldArr[] = 'CAST(S' . $fdb2csv2['D2FILID'] . '.' . $fdb2csv2['D2FLD'] . '  AS CHAR(4) CCSID 5026) AS ' . $fdb2csv2['D2FLD'] . '_' . $fdb2csv2['D2FILID'];
                        }else{
                            $strTblFldArr[] = 'CAST(S' . $fdb2csv2['D2FILID'] . '.' . $fdb2csv2['D2FLD'] . '  AS CHAR(' . $fdb2csv2['D2LEN'] . ') CCSID 5026) AS ' . $fdb2csv2['D2FLD'] . '_' . $fdb2csv2['D2FILID'];
                        }
                    } else {
                        $strTblFldArr[] = 'S' . $fdb2csv2['D2FILID'] . '.' . $fdb2csv2['D2FLD'] . ' AS ' . $fdb2csv2['D2FLD'] . '_' . $fdb2csv2['D2FILID'];
                    }
                }
                $strTblFldNmArr[] = 'SQ1.' . $fdb2csv2['D2FLD'] . '_' . $fdb2csv2['D2FILID'];
            }
            $strTblFld = join(', ', $strTblFldArr);
            $strTblFldNm = join(', ', $strTblFldNmArr);
        } else {
            $rtn = 1;
            $msg = 'クエリーの実行SQL文作成失敗【定義の出力フィールド情報】。';
        }
    }
    // 結果フィールド
    if ($rtn === 0) {
        $strResFld = '';
        $strResFldArr = array();
        $strResFldNmArr = array();
        $strResFldObj = array();
        if (count($qrydata['FDB2CSV5'] > 0)) {
            $fdb2csv5data = $qrydata['FDB2CSV5'];
            foreach ($fdb2csv5data as $fdb2csv5) {

                e_log('fdb2csv5');
                e_log($fdb2csv5['D5TYPE']);
                e_log($fdb2csv5['D5WEDT']);
                
                if ($fdb2csv5['D5TYPE'] === 'P' || $fdb2csv5['D5TYPE'] === 'S' || $fdb2csv5['D5TYPE'] === 'B') {
                      $myCode = $fdb2csv5['D5WEDT'];
                      $myVar = $fdb2csv5['D5EXPD'];


                      $val = $myVar;
                      
                      if($myCode == "M" || $myCode == "N" || $myCode == "O" || $myCode == "P"){

                          $strResFldArr[] = 'ROUND((' . $fdb2csv5['D5EXPD'] . '),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                          $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'ROUND((' . $fdb2csv5['D5EXPD'] . '),' . $fdb2csv5['D5DEC'] . ') ';

                      }else if($myCode == "I" || $myCode == "J" || $myCode == "K" || $myCode == "L"){

                          $j = 0;
                          for($i = 0;$i < $fdb2csv5['D5DEC'];$i++){
                             $k .= $j;
                          }
                          $s = '0.'.$k.'9';

                          $strResFldArr[] = 'TRUNC((' . $fdb2csv5['D5EXPD'] . ')+'.$s.',' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                          $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'TRUNC((' . $fdb2csv5['D5EXPD'] . ')+'.$s.',' . $fdb2csv5['D5DEC'] . ') ';
                          
/*
                          $b = str_replace('.', ',', $val);
                          if(preg_match('/,/',$b)){
                              $j = 0;
                              for($i = 0;$i < $fdb2csv5['D5DEC']-1;$i++){
                                 $k .= $j;
                              }
                              $s = '.'.$k.'1';
                             
                              $strResFldArr[] = '(ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ')+'.$s.') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                              $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'CAST(CAST(' . $val . ' as decimal(18,11)) as float)';
                              $k = '';
                          }else{
                             
                              $strResFldArr[] = 'ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                              $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'CAST(CAST(' . $val . ' as decimal(18,11)) as float)';
                          }
*/
                      }else{
                          $strResFldArr[] = 'TRUNC((' . $fdb2csv5['D5EXPD'] . '),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                          $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'TRUNC((' . $fdb2csv5['D5EXPD'] . '),' . $fdb2csv5['D5DEC'] . ') '; 
                      }



/*                  
                     if($myCode == "A" || $myCode == "F" || $myCode == "G" || $myCode == "H" || $myCode == "I" || $myCode == "J" || $myCode == "K" || $myCode == "L" || $myCode == "M" || $myCode == "N" || $myCode == "O" || $myCode == "P"){
	                        $b = str_replace('/', ',', $myVar);
                         
	                        if(preg_match('/,/',$b)){
                              
	                           if(preg_match("/[a-zA-Z]/i", $b)){
	                                    
	                                    $b  = str_replace('(', '', $b);
					                    $b  = str_replace(')', '', $b);
										$c = explode(",",$b);
										for($i = 0;$i < count($c);$i++){
										  if($i == 0){
	                                          
	                                          if(preg_match("/[a-zA-Z]/i", $c[$i])){
	                                             $c[$i] = "CAST((".$c[$i].") AS float)";

	                                          }
	                                          if(preg_match("/[a-zA-Z]/i", $c[$i+1])){
	                                               
                                                   $c[$i] = "CAST((".$c[$i+1].") AS float)";
	                                          }else{
                                                   $c[$i+1] = "(".$c[$i+1].")";
                                              }
										      $val = $c[$i]."/".$c[$i+1];
	                                          $i++;
										  }else{
	                                          if(preg_match("/[a-zA-Z]/i", $c[$i])){
	                                               $c[$i] = "CAST((".$c[$i].") AS float)";

	                                          }else{
                                                   $c[$i] = "(".$c[$i].")";
                                              }
										      $val = $val."/".$c[$i];
										  }
										}
	                                
	                                  $val = $val;
	                                 


	                           }else{
                                   
		                             $val = str_replace(',', '/', $b);
		                             $val = $val . ".0";
	                            }
	                            
                                  if($myCode == "M" || $myCode == "N" || $myCode == "O" || $myCode == "P"){
		                              $strResFldArr[] = 'ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
		                              $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ') ';
                                  }else if($myCode == "A" || $myCode == "F" || $myCode == "G" || $myCode == "H"){
                                     
                                      $j = 0;
                                      for($i = 0;$i < $fdb2csv5['D5DEC']-1;$i++){
                                         $k .= $j;
                                      }
                                      $s = '.'.$k.'1';
                                    if($fdb2csv5['D5DEC'] == '0' || $fdb2csv5['D5DEC'] == ''){
                                         $strResFldArr[] = 'ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
			                             $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'CAST(CAST(' . $val . ' as decimal(18,11)) as float)';
                                    }else{
                                      $strResFldArr[] = '(ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ')-'.$s.') AS ' . $fdb2csv5['D5FLD'] . '_9999';
		                              $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'CAST(CAST(' . $val . ' as decimal(18,11)) as float)';
                                       $k = '';
                                    }
                                  }else if($myCode == "I" || $myCode == "J" || $myCode == "K" || $myCode == "L"){
                                      $j = 0;
	                                  for($i = 0;$i < $fdb2csv5['D5DEC']-1;$i++){
	                                      $k .= $j;
	                                  }
	                                  $s = '.'.$k.'1';
                                      $strResFldArr[] = '(ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ')+'.$s.') AS ' . $fdb2csv5['D5FLD'] . '_9999';
		                              $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'CAST(CAST(' . $val . ' as decimal(18,11)) as float)';
                                      $k = '';
                                  }else{
                                      $strResFldArr[] = 'TRUNC((' . $fdb2csv5['D5EXPD'] . '),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                                      $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'TRUNC((' . $fdb2csv5['D5EXPD'] . '),' . $fdb2csv5['D5DEC'] . ') '; 
                                  }
	                        }else{
	                              $val = $myVar;
                                 
	                              if($myCode == "M" || $myCode == "N" || $myCode == "O" || $myCode == "P"){
                                     
		                              $strResFldArr[] = 'ROUND((' . $val . '),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                                      $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'ROUND((' . $val . '),' . $fdb2csv5['D5DEC'] . ') '; 
                                  }else if($myCode == "A" || $myCode == "F" || $myCode == "G" || $myCode == "H"){
                                     
                                      $b = str_replace('.', ',', $val);
                                      if(preg_match('/,/',$b)){
	                                      $j = 0;
	                                      for($i = 0;$i < $fdb2csv5['D5DEC']-1;$i++){
	                                         $k .= $j;
	                                      }
	                                      $s = '.'.$k.'1';
	                                    if($fdb2csv5['D5DEC'] == '0' || $fdb2csv5['D5DEC'] == ''){
                                           $strResFldArr[] = 'ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
			                               $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'CAST(CAST(' . $val . ' as decimal(18,11)) as float)';
                                        }else{
	                                      $strResFldArr[] = '(ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ')-'.$s.') AS ' . $fdb2csv5['D5FLD'] . '_9999';
			                              $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'CAST(CAST(' . $val . ' as decimal(18,11)) as float)';
	                                       $k = '';
                                        }
                                       }else{
                                          $strResFldArr[] = 'ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
			                              $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'CAST(CAST(' . $val . ' as decimal(18,11)) as float)';
                                       }
                                  }else if($myCode == "I" || $myCode == "J" || $myCode == "K" || $myCode == "L"){
                                      
                                      $b = str_replace('.', ',', $val);
                                      if(preg_match('/,/',$b)){
                                          $j = 0;
	                                      for($i = 0;$i < $fdb2csv5['D5DEC']-1;$i++){
	                                         $k .= $j;
	                                      }
	                                      $s = '.'.$k.'1';
                                         
                                          $strResFldArr[] = '(ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ')+'.$s.') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                                          $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'CAST(CAST(' . $val . ' as decimal(18,11)) as float)';
                                          $k = '';
                                      }else{
                                         
                                          $strResFldArr[] = 'ROUND(CAST(CAST(' . $val . ' as decimal(18,11)) as float),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
		                                  $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'CAST(CAST(' . $val . ' as decimal(18,11)) as float)';
                                      }
                                  }else{
                                      $strResFldArr[] = 'TRUNC((' . $fdb2csv5['D5EXPD'] . '),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                                      $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'TRUNC((' . $fdb2csv5['D5EXPD'] . '),' . $fdb2csv5['D5DEC'] . ') '; 
                                  }
	                        }
    


                    }else{
                         $strResFldArr[] = 'TRUNC((' . $fdb2csv5['D5EXPD'] . '),' . $fdb2csv5['D5DEC'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                         $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = 'TRUNC((' . $fdb2csv5['D5EXPD'] . '),' . $fdb2csv5['D5DEC'] . ') '; 
                    }
*/                 
                } else {
                    $strResFldArr[] = '(' . $fdb2csv5['D5EXPD'] . ') AS ' . $fdb2csv5['D5FLD'] . '_9999';
                    $strResFldObj['SQ1.' . $fdb2csv5['D5FLD'] . '_9999'] = '(' . $fdb2csv5['D5EXPD'] . ')';// MSM change to D5EXPD
                }
                $strResFldNmArr[] = 'SQ1.' . $fdb2csv5['D5FLD'] . '_9999';
            }
            if (count($strResFldArr) === 0) {
                $strResFld = '';
                $strResFldNm = '';
            } else {
                $strResFld = ' ,' . join(', ', $strResFldArr);
                $strResFldNm = ' ,' . join(', ', $strResFldNmArr);
            }
        } else {
            $rtn = 1;
            $msg = 'クエリーの実行SQL文作成失敗【結果フィールド情報】。';
        }
    }
    if ($rtn === 0) {
        $subQry1SQL = '';
        $subQry1SQL.= ' SELECT';
        $subQry1SQL.= ' ' . $strTblFld;
        $subQry1SQL.= $strResFld;
        $subQry1SQL.= ' FROM ' . $strPTable . $strSTable;

        $strTest1SQL = $subQry1SQL;
        $strEXESQL = $subQry1SQL;
        //ログ用サブクエリー１
        $strLSQL1 = $subQry1SQL;
    }
    if ($rtn === 0) {
        $strJokenSQL = '';
        $cnmseq = '';
        $condAdd = true;
        $addAndOrflg = true;
        $cndExist = false;
        $params = array();
        if (count($qrydata['BQRYCND'] > 0)) {
            $bqrycnddata = $qrydata['BQRYCND'];
            for ($i = 0;$i < count($bqrycnddata);$i++) {
                $bqrycnd = $bqrycnddata[$i];
                // 条件タイプは「任意」の場合
                // 条件タイプは「必須」で画面上からの実行じゃない場合
                if (((int)$bqrycnd['CNSTKB'] === 2) || ((int)$bqrycnd['CNSTKB'] === 1 && $cndparamList === NULL)) {
                    if ($cndparamList !== NULL) {
                        $cdata = getFRMCNDDATbyCND($bqrycnd, $cndparamList);
                    } else {
                        $cdata = getBCNDDATbyCND($db2con, $bqrycnd);
                    }
                    if ($cdata['result'] !== true) {
                        $rtn = 1;
                        $msg = showMsg($cdata['result'], array('クエリー'));
                    } else {
                        $bcnddatlist = $cdata['data'];
                        // 「任意」条件の場合初期値設定しない場合、条件に入れない
                        // 「必須」条件で画面上から実行じゃないとき式値設定しない場合、条件に入れない
                        if (count($bcnddatlist) === 0) {
                            $condAdd = false;
                        } else {
                            $condAdd = true;
                            // プレビューのparameter 作成
                            $paramdatalist = array();
                            foreach ($bcnddatlist as $bcnddatvalue) {
                                if ($bqrycnd['CNCKBN'] === 'IS') {
                                    $isData = $bcnddatvalue['CDDAT'];
                                } else {
                                    if ($bcnddatvalue['CDFFLG'] === '1') {
                                        $dataArr = explode(".", $bcnddatvalue['CDDAT']);
                                        if (count($dataArr) === 2) {
                                            if ($dataArr[0] === 'P') {
                                                //$bcnddatvalue['CDDAT'] = 'SQ1.'.$dataArr[1].'_'.'0';
                                               $bcnddatvalue['CDDAT'] = $dataArr[1] . '_' . '0';
                                            } else {
                                                if ($dataArr[0] === 'K') {
                                                    //$bcnddatvalue['CDDAT'] = 'SQ1.'.$dataArr[1].'_9999';
                                                    $bcnddatvalue['CDDAT'] = $strResFldObj['SQ1.' . $dataArr[1] . '_9999'];
                                                } else {
                                                    // $bcnddatvalue['CDDAT'] = 'SQ1.'.$dataArr[1].'_'.substr($dataArr[0], 1);
                                                    $bcnddatvalue['CDDAT'] = $dataArr[1] . '_' . substr($dataArr[0], 1);
                                                }
                                            }
                                        } else {
                                            $bcnddatvalue['CDDAT'] = '';
                                        }
                                    } else {
                                        if ((int)$bcnddatvalue['CDBCNT'] > 0) {
                                            $bcnddatvalue['CDDAT'] = str_pad($bcnddatvalue['CDDAT'], (int)$bcnddatvalue['CDBCNT']);
                                        }
                                    }
                                    if ($bcnddatvalue['CDFFLG'] === '1') {
                                        $paramdatalist[] = $bcnddatvalue['CDDAT'];
                                    } else {
                                        switch ($bqrycnd['D2TYPE']) {
                                            case 'P':
                                            case 'S':
                                            case 'B':
                                                $paramdatalist[] = $bcnddatvalue['CDDAT'];
                                            break;
                                            case 'L':
                                            case 'T':
                                            case 'Z':
                                                switch ($bqrycnd['CNCKBN']) {
                                                    case 'LIKE':
                                                    case 'NLIKE':
                                                    case 'LLIKE':
                                                    case 'LNLIKE':
                                                    case 'RLIKE':
                                                    case 'RNLIKE':
                                                        $paramdatalist[] = "'" . db2_escape_string($bcnddatvalue['CDDAT']) . "'";
                                                    break;
                                                    default:
                                                        switch ($bqrycnd['D2TYPE']) {
                                                            case 'L':
                                                                $paramdatalist[] = "'" . chkVldDate($bcnddatvalue['CDDAT']) . "'";
                                                            break;
                                                            case 'T':
                                                                $paramdatalist[] = "'" . chkVldTime($bcnddatvalue['CDDAT']) . "'";
                                                            break;
                                                            case 'Z':
                                                                $paramdatalist[] = "'" . chkVldTimeStamp($bcnddatvalue['CDDAT']) . "'";
                                                            break;
                                                            default:
                                                            break;
                                                        }
                                                    break;
                                                }
                                            break;
                                            default:
                                                $paramdatalist[] = "'" . db2_escape_string($bcnddatvalue['CDDAT']) . "'";
                                            break;
                                        }
                                    }
                                    //error_log('条件データのFFLG'.$bcnddatvalue['CDFFLG'].'TYPE'.$bqrycnd['D2TYPE'].'DATA'.$bcnddatvalue['CDDAT'].'DATALST'.print_r($paramdatalist,true));
                                    
                                }
                            }
                            switch ($bqrycnd['CNCKBN']) {
                                case 'RANGE':
                                    array_push($params, $paramdatalist[0]);
                                    $paramcnt = count($params) - 1;
                                    //error_log("paramdatalist333 MSM20181030 => ".print_r($paramdatalist[0],true));
                                    if(empty($paramdatalist[0])){
                                        $rangeFrmIdx = '';
                                    }else{
                                        $rangeFrmIdx = '{' . $paramcnt . '}';
                                    }
                                    //$rangeFrmIdx = '{' . $paramcnt . '}';
                                    array_push($params, $paramdatalist[1]);
                                    $paramcnt = count($params) - 1;
                                    //error_log("paramdatalist444 MSM20181030 => ".print_r($paramdatalist[1],true));
                                    if(empty($paramdatalist[1])){
                                        $rangeToIdx = '';
                                    }else{
                                        $rangeToIdx = '{' . $paramcnt . '}';
                                    }
                                    //$rangeToIdx = '{' . $paramcnt . '}';
                                break;
                                case 'LIST':
                                case 'NLIST':
                                    $listparamArr = array();
                                    $listparam = '';
                                    foreach ($paramdatalist as $value) {
                                        array_push($params, $value);
                                        $paramcnt = count($params) - 1;
                                        $listparamArr[] = '{' . $paramcnt . '}';
                                    }
                                    $listparam = join(',', $listparamArr);
                                break;
                                case 'LIKE':
                                case 'NLIKE':
                                    array_push($params, likeConvert($paramdatalist[0], $bqrycnd['D2LEN'], $bcnddatvalue['CDFFLG']));
                                    $paramcnt = count($params) - 1;
                                    $paramIdx = '{' . $paramcnt . '}';
                                break;
                                case 'LLIKE':
                                case 'LNLIKE':
                                    array_push($params, rightLikeConvert($paramdatalist[0], $bqrycnd['D2LEN'], $bcnddatvalue['CDFFLG']));
                                    $paramcnt = count($params) - 1;
                                    $paramIdx = '{' . $paramcnt . '}';
                                break;
                                case 'RLIKE':
                                case 'RNLIKE':
                                    array_push($params, leftLikeConvert($paramdatalist[0], $bqrycnd['D2LEN'], $bcnddatvalue['CDFFLG']));
                                    $paramcnt = count($params) - 1;
                                    $paramIdx = '{' . $paramcnt . '}';
                                break;
                                default:
                                    array_push($params, $paramdatalist[0]);
                                    $paramcnt = count($params) - 1;
                                    $paramIdx = '{' . $paramcnt . '}';
                                break;
                            }
                        }
                    }
                } else {
                    $condAdd = true;
                    //検索画面からの実行で「必須」の場合
                    if ($cndparamList !== NULL && (int)$bqrycnd['CNSTKB'] === 1) {
                        $cdata = getFRMCNDDATbyCND($bqrycnd, $cndparamList);
                    } else {
                        // 「非表示」または「固定」の場合、
                        $cdata = getBCNDDATbyCND($db2con, $bqrycnd);
                    }
                    if ($cdata['result'] !== true) {
                        $rtn = 1;
                        $msg = showMsg($cdata['result'], array('クエリー'));
                    } else {
                        // プレビューのparameter 作成
                        $bcnddatlist = $cdata['data'];
                        if (count($bcnddatlist) === 0) {
                            $rtn = 1;
                            $msg = showMsg('非表示又は固定のためデータ入力必要です。クエリの設定に確認してください。');
                        } else {
                            // プレビューのparameter 作成
                            $paramdatalist = array();
                            foreach ($bcnddatlist as $bcnddatvalue) {
                                if ($bcnddatvalue['CDFFLG'] === '1') {
                                    $dataArr = explode(".", $bcnddatvalue['CDDAT']);
                                    if (count($dataArr) === 2) {
                                        if ($dataArr[0] === 'P') {
											//klyh test 20180515
                                            //$bcnddatvalue['CDDAT'] = $dataArr[1] . '_' . '0';
											$bcnddatvalue['CDDAT'] = 'P.'.$dataArr[1];
                                        } else {
                                            if ($dataArr[0] === 'K') {
                                                $bcnddatvalue['CDDAT'] = $strResFldObj['SQ1.' . $dataArr[1] . '_9999'];
                                            } else {
                                                //$bcnddatvalue['CDDAT'] = $dataArr[1] . '_' . substr($dataArr[0], 1);
                                                $bcnddatvalue['CDDAT'] = 'S'.substr($dataArr[0], 1).'.'.$dataArr[1];
                                            }
                                        }
                                    } else {
                                        $bcnddatvalue['CDDAT'] = '';
                                    }
                                } else {
                                    if ((int)$bcnddatvalue['CDBCNT'] > 0) {
                                        $bcnddatvalue['CDDAT'] = str_pad($bcnddatvalue['CDDAT'], (int)$bcnddatvalue['CDBCNT']);
                                    }
                                }
                                if ($bqrycnd['CNCKBN'] === 'IS') {
                                    $isData = $bcnddatvalue['CDDAT'];
                                } else if ($bcnddatvalue['CDFFLG'] === '1') {
                                    $paramdatalist[] = $bcnddatvalue['CDDAT'];
                                } else {
                                    switch ($bqrycnd['D2TYPE']) {
                                        case 'P':
                                        case 'S':
                                        case 'B':
                                            $paramdatalist[] = $bcnddatvalue['CDDAT'];
                                        break;
                                        case 'L':
                                        case 'T':
                                        case 'Z':
                                            switch ($bqrycnd['CNCKBN']) {
                                                case 'LIKE':
                                                case 'NLIKE':
                                                case 'LLIKE':
                                                case 'LNLIKE':
                                                case 'RLIKE':
                                                case 'RNLIKE':
                                                    $paramdatalist[] = "'" . db2_escape_string($bcnddatvalue['CDDAT']) . "'";
                                                break;
                                                default:
                                                    switch ($bqrycnd['D2TYPE']) {
                                                        case 'L':
                                                            $paramdatalist[] = "'" . chkVldDate($bcnddatvalue['CDDAT']) . "'";
                                                        break;
                                                        case 'T':
                                                            $paramdatalist[] = "'" . chkVldTime($bcnddatvalue['CDDAT']) . "'";
                                                        break;
                                                        case 'Z':
                                                            $paramdatalist[] = "'" . chkVldTimeStamp($bcnddatvalue['CDDAT']) . "'";
                                                        break;
                                                        default:
                                                        break;
                                                    }
                                                break;
                                            }
                                        break;
                                        default:
                                            $paramdatalist[] = "'" . db2_escape_string($bcnddatvalue['CDDAT']) . "'";
                                        break;
                                    }
                                }
                            }
                            switch ($bqrycnd['CNCKBN']) {
                                case 'RANGE':
                                    //error_log("paramdatalist MSM20181030 => ".print_r($paramdatalist,true));
                                    array_push($params, $paramdatalist[0]);
                                    $paramcnt = count($params) - 1;
                                    //error_log("paramdatalist111 MSM20181030 => ".print_r($paramdatalist[0],true));
                                    if(empty($paramdatalist[0])){
                                        $rangeFrmIdx = '';
                                    }else{
                                        $rangeFrmIdx = '{' . $paramcnt . '}';
                                    }
                                    
                                    array_push($params, $paramdatalist[1]);
                                    $paramcnt = count($params) - 1;
                                    //error_log("paramdatalist222 MSM20181030 => ".print_r($paramdatalist[1],true));
                                    if(empty($paramdatalist[1])){
                                        $rangeToIdx = '';
                                    }else{
                                        $rangeToIdx = '{' . $paramcnt . '}';
                                    }
                                    //$rangeToIdx = '{' . $paramcnt . '}';
                                break;
                                case 'LIST':
                                case 'NLIST':
                                    $listparamArr = array();
                                    $listparam = '';
                                    foreach ($paramdatalist as $value) {
                                        array_push($params, $value);
                                        $paramcnt = count($params) - 1;
                                        $listparamArr[] = '{' . $paramcnt . '}';
                                    }
                                    $listparam = join(',', $listparamArr);
                                break;
                                case 'LIKE':
                                case 'NLIKE':
                                    array_push($params, likeConvert($paramdatalist[0], $bqrycnd['D2LEN'], $bcnddatvalue['CDFFLG']));
                                    $paramcnt = count($params) - 1;
                                    $paramIdx = '{' . $paramcnt . '}';
                                break;
                                case 'LLIKE':
                                case 'LNLIKE':
                                    array_push($params, rightLikeConvert($paramdatalist[0], $bqrycnd['D2LEN'], $bcnddatvalue['CDFFLG']));
                                    $paramcnt = count($params) - 1;
                                    $paramIdx = '{' . $paramcnt . '}';
                                break;
                                case 'RLIKE':
                                case 'RNLIKE':
                                    array_push($params, leftLikeConvert($paramdatalist[0], $bqrycnd['D2LEN'], $bcnddatvalue['CDFFLG']));
                                    $paramcnt = count($params) - 1;
                                    $paramIdx = '{' . $paramcnt . '}';
                                break;
                                default:
                                    array_push($params, $paramdatalist[0]);
                                    $paramcnt = count($params) - 1;
                                    $paramIdx = '{' . $paramcnt . '}';
                                break;
                            }
                        }
                    }
                }
                if ($rtn === 0) {
                    if ($cnmseq !== (int)$bqrycnd['CNMSEQ']) {
                        $isAdded = false;
                        $cnmseq = (int)$bqrycnd['CNMSEQ'];
                        if ($cnmseq > 1 && (int)$bqrycnd['CNSSEQ'] === 1 && $cndExist === true) {
                            $strAndOrSQL = ' ) ' . $bqrycnd['CNAOKB'] . ' ( ';
                            $cndExist = false;
                        }
                    }
                    if ($cnmseq === (int)$bqrycnd['CNMSEQ']) {
                        if ($condAdd === true && $isAdded === false) {
                            $strJokenSQL.= $strAndOrSQL;
                            $isAdded = true;
                        }
                        if ($condAdd === false && (int)$bqrycnd['CNSSEQ'] === 1) {
                            $addAndOrflg = false;
                        }
                    }
                    if ($condAdd === true) {
                        //$strfldJoken = 'SQ1.'.$bqrycnd['CNFLDN'].'_'.$bqrycnd['CNFILID'];
                        if ((int)$bqrycnd['CNFILID'] === 9999) {

                            $strfldJoken = $strResFldObj['SQ1.' . $bqrycnd['CNFLDN'] . '_' . $bqrycnd['CNFILID']];
                            e_log("strfldJoken MYATSU 9999 => ".$strfldJoken);
                        } else {
                            if ((int)$bqrycnd['CNFILID'] === 0) {
                                $strfldJoken = 'P.' . $bqrycnd['CNFLDN'];
                                e_log("strfldJoken MYATSU 0000 => ".$strfldJoken);
                            } else {
                                $strfldJoken = 'S' . (int)$bqrycnd['CNFILID'] . '.' . $bqrycnd['CNFLDN'];
                                e_log("strfldJoken MYATSU else => ".$strfldJoken);
                            }
                        }
                        switch ($bqrycnd['CNCKBN']) {
                            case 'LIKE':
                            case 'NLIKE':
                            case 'LLIKE':
                            case 'LNLIKE':
                            case 'RLIKE':
                            case 'RNLIKE':
                                switch ($bqrycnd['D2TYPE']) {
                                    case 'L':
                                        $strJoken.= 'CAST(' . $strfldJoken . ' AS CHAR(10))';
                                    break;
                                    case 'T':
                                        $strJoken.= 'CAST(' . $strfldJoken . ' AS CHAR(8))';
                                    break;
                                    case 'Z':
                                        $strJoken.= 'CAST(' . $strfldJoken . ' AS CHAR(26))';
                                    break;
                                    default:
                                        $strJoken.= $strfldJoken;
                                    break;
                                }
                            break;
                            default:
                                $strJoken.= $strfldJoken;
                            break;
                        }
                        e_log("Casting....Joken".$strJoken);

 
                        switch ($bqrycnd['CNCKBN']) {
                            case "EQ":
                                $strJoken.= ' = ' . $paramIdx . ' ';
                            break;
                            case "NE":
                                $strJoken.= ' <> ' . $paramIdx . ' ';
                            break;
                            case "GE":
                                $strJoken.= ' >= ' . $paramIdx . ' ';
                            break;
                            case "GT":
                                $strJoken.= ' > ' . $paramIdx . ' ';
                            break;
                            case "LE":
                                $strJoken.= ' <=  ' . $paramIdx . ' ';
                            break;
                            case "LT":
                                $strJoken.= ' < ' . $paramIdx . ' ';
                            break;
                            case "RANGE":
                                //MSM check for calendar range FROM or TO data 20181030
                                if($rangeFrmIdx !== "" &&  $rangeToIdx ===""){
                                    $strJoken.= ' >= ' . $rangeFrmIdx . ' ';
                                }else if( $rangeFrmIdx === "" &&  $rangeToIdx !== ""){
                                    $strJoken.= ' <= '. $rangeToIdx . ' ';
                                }else{
                                    $strJoken.= ' BETWEEN ' . $rangeFrmIdx . ' AND ' . $rangeToIdx . ' ';
                                }
                                
                            break;
                            case "LIST":
                                $strJoken.= ' IN (' . $listparam . ')';
                            break;
                            case "NLIST":
                                $strJoken.= ' NOT IN (' . $listparam . ') ';
                            break;
                            case "LIKE":
                                $strJoken.= ' LIKE ' . $paramIdx . ' ';
                            break;
                            case "LLIKE":
                                $strJoken.= ' LIKE ' . $paramIdx . ' ';
                            break;
                            case "RLIKE":
                                $strJoken = ' RTRIM ( '.$strJoken.' ) ';
                                $strJoken.= ' LIKE ' . $paramIdx . ' ';
                            break;
                            case "NLIKE":
                                $strJoken.= ' NOT LIKE ' . $paramIdx . ' ';
                            break;
                            case "LNLIKE":
                                $strJoken.= ' NOT LIKE ' . $paramIdx . ' ';
                            break;
                            case "RNLIKE":
                                $strJoken = ' RTRIM ( '.$strJoken.' ) ';
                                $strJoken.= ' NOT LIKE ' . $paramIdx . ' ';
                            break;
                            case "IS":
                                $strJoken.= ' IS ' . $isData . ' ';
                            break;
                        }
                        error_log("Casting....JokenI".$strJoken);
                        if ($addAndOrflg === true && (int)$bqrycnd['CNSSEQ'] !== 1) {
                            $strJokenSQL.= $bqrycnd['CNAOKB'] . ' ' . $strJoken;
                            $cndExist = true;
                            $strJoken = '';
                        } else {
                            $strJokenSQL.= ' ' . $strJoken;
                            $strJoken = '';
                            $cndExist = true;
                            $addAndOrflg = true;
                        }
                    }
                }
            }
        }
    }
    if ($rtn === 0) {
        if (strlen(trim($strJokenSQL)) !== 0) {
            //error_log('パラメータ strJokenSQL MYAT：'.print_r($strJokenSQL,true).print_r($params,true));
            $strJokenSQL = getBindParam($strJokenSQL, $params);
            $strJokenSQL = ' WHERE (' . $strJokenSQL . ') ';


        }
        $subQry2SQL = '';
        $subQry2SQL.= $subQry1SQL;
        $subQry2SQL.= ' ' . $strJokenSQL;
        $strTest2SQL = '';
        $strTest2SQL.= $subQry1SQL;
        $strTest2SQL.= ' ' . $strJokenSQL;

        // サブクエリー１
        // すべての抽出対象テーブルのフィールドと結果フィールドを抽出したクエリ
        $subQry2SQL = '(' . $subQry2SQL . ') SQ2';
        $strTest2SQL = ' ((' . $strTest2SQL . ') FETCH FIRST 1 ROWS ONLY ) SQ2';
        $strTestSQL = $strTest2SQL;
        $strEXESQL = $subQry2SQL;
        // ログ用クエリー2
        $strSSQL2 = '';
        $strSSQL2.= ' SELECT';
        $strSSQL2.= ' ' . $strTblFldNm;
        $strSSQL2.= $strResFldNm;
        $strSSQL2.= ' FROM ' . '{JSQL1}';
        $strSSQL2.= ' ' . $strJokenSQL;
    }
    if ($rtn === 0) {
        $sumKeyList = array();
        $sumKeySQL = '';
        if (count($qrydata['FDB2CSV25']) > 0) {
            $fdb2csv25Data = $qrydata['FDB2CSV25'];
            foreach ($fdb2csv25Data as $fdb2csv25) {
                $sumKeyList[] = 'SQ2.' . $fdb2csv25['D2FLD'] . '_' . $fdb2csv25['D2FILID'];
            }
        }
		//e_log("制御qrydata['SELCOLINFO']：".print_r($qrydata['SELCOLINFO'],true));
        if (count($qrydata['SELCOLINFO']) > 0) {
            $selColInfoData = $qrydata['SELCOLINFO'];
            $sfgmesArr = array(); // 集計フィールド配列
            $strTblFldArr = array();
            foreach ($selColInfoData as $selColInfo) {
                if ($selColInfo['SFGMES'] === '') {
                    $strColInfoArr[] = cmMer('SQ2.' . $selColInfo['D2FLD'] . '_' . $selColInfo['D2FILID']);
                    if (count($sumKeyList) > 0) {
                        $sumKeyList[] = 'SQ2.' . $selColInfo['D2FLD'] . '_' . $selColInfo['D2FILID'];
                    }
                } else if ($selColInfo['SFGMES'] === 'AVG') {
                    $sfgmesArr[] = 'SQ2.' . $selColInfo['D2FLD'] . '_' . $selColInfo['D2FILID'];
                    $strColInfoArr[] = cmMer('TRUNC(' . $selColInfo['SFGMES'] . '(SQ2.' . $selColInfo['D2FLD'] . '_' . $selColInfo['D2FILID'] . '),' . $selColInfo['D2DEC'] . ') AS ' . $selColInfo['D2FLD'] . '_' . $selColInfo['D2FILID']);
                } else {
                    $sfgmesArr[] ='SQ2.' . $selColInfo['D2FLD'] . '_' . $selColInfo['D2FILID'];
                    $strColInfoArr[] = cmMer($selColInfo['SFGMES'] . '(SQ2.' . $selColInfo['D2FLD'] . '_' . $selColInfo['D2FILID'] . ') AS ' . $selColInfo['D2FLD'] . '_' . $selColInfo['D2FILID']);
                }
            }
            if (count($sumKeyList) > 0) {
                $sumKeySQL.= ' ' . 'GROUP BY ' . ' ';
                $sumKeySQL.= join(' , ', array_unique($sumKeyList));
            }
            $strColInfo = join(', ', $strColInfoArr);
        }
        if ($rtn === 0) {
            $sortSQL = '';
            if (count($qrydata['SORTCOLINFO']) > 0) {
                $sortColInfoData = $qrydata['SORTCOLINFO'];
                $strSortFldArr = array();
                foreach ($sortColInfoData as $sortColInfo) {
                    if (count($sfgmesArr) > 0) {
                        if (in_array('SQ2.' . $sortColInfo['D2FLD'] . '_' . $sortColInfo['D2FILID'], $sfgmesArr, true)) {
                            // 処理無し
                            $strSortFldArr[] =  $sortColInfo['D2FLD'] . '_' . $sortColInfo['D2FILID'] . ' ' . $sortColInfo['D2RSTP'];                            
                        } else {
                            $strSortFldArr[] = 'SQ2.' . $sortColInfo['D2FLD'] . '_' . $sortColInfo['D2FILID'] . ' ' . $sortColInfo['D2RSTP'];
                        }
                    } else {
                        $strSortFldArr[] = 'SQ2.' . $sortColInfo['D2FLD'] . '_' . $sortColInfo['D2FILID'] . ' ' . $sortColInfo['D2RSTP'];
                    }
                }
                $strSortColInfo = join(', ', $strSortFldArr);
                $sortSQL.= ' ORDER BY ' . ' ' . $strSortColInfo;
            }
        }
        if ($rtn === 0) {
            if ($rnFlg === 0) {
                //e_log('定義のデータ取得 開始');
                $subQry3SQL = '';
                $subQry3SQL.= ' SELECT';
                $subQry3SQL.= ' ' . $strColInfo;
                $subQry3SQL.= ' FROM ' . $subQry2SQL;
                $subQry3SQL.= ' ' . $sumKeySQL;
                $subQry3SQL = $subQry3SQL . $sortSQL;
                $strTest3SQL = '';
                $strTest3SQL.= ' SELECT';
                $strTest3SQL.= ' ' . $strColInfo;
                $strTest3SQL.= ' FROM ' . $strTest2SQL;
                $strTest3SQL.= ' ' . $sumKeySQL;
                $strTest3SQL = $strTest3SQL . $sortSQL;
                $strEXESQL = $subQry3SQL;
                $strTestSQL = $strTest3SQL;
            } else if ($rnFlg === 1) {
                e_log('制御のためのデータ取得 開始');
                $strRowNumCol = '';
                e_log('制御集計キー取得して制御データ取得開始');
                $rsSeigyoData = cmGetSeigyoData($db2con, $fdb2csv1['D1NAME']);
                if ($rsSeigyoData['RESULT'] === 1) {
                    $rtn = 1;
                    $msg = '制御設定情報取得に失敗しました。';
                } else {
                    //e_log('制御情報取得：'.print_r($rsSeigyoData,true));
                    if ($rsSeigyoData['SEIGYOFLG'] === 0) {
                        $subQry3SQL = '';
                        $subQry3SQL.= ' SELECT';
                        $subQry3SQL.= ' ' . $strColInfo;
                        $subQry3SQL.= ' FROM ' . $subQry2SQL;
                        $subQry3SQL.= ' ' . $sumKeySQL;
                        $subQry3SQL = $subQry3SQL . $sortSQL;
                        $strTest3SQL = '';
                        $strTest3SQL.= ' SELECT';
                        $strTest3SQL.= ' ' . $strColInfo;
                        $strTest3SQL.= ' FROM ' . $strTest2SQL;
                        $strTest3SQL.= ' ' . $sumKeySQL;
                        $strTest3SQL = $strTest3SQL . $sortSQL;
                        $strEXESQL = $subQry3SQL;
                        // e_log('サブクエリー２:'.$strTest3SQL);
                        $strTestSQL = $strTest3SQL;
                    } else {
                        // 【制御】のためSQL作成　開始
                        $db2colmData = umEx($rsSeigyoData['RSMASTER']);
                        //e_log('制御キーデータ：'.print_r($db2colmData,true));
                        // DB2COLMキーデータチェック
                        if (count($db2colmData) > 0) {
                            $db2colmData = $db2colmData[0];
                            $seigyoKeyArr = array();
                            $seigyoKArr = array();
                            for ($lvlCnt = 1;$lvlCnt <= 6;$lvlCnt++) {
                            	if ($db2colmData['DCFLD' . $lvlCnt] !== '') {
						            $LGroup = explode(",",$db2colmData['DCFLD' . $lvlCnt]);
						            if (count($LGroup) >= 2) { //グループのレベル
										for($ii = 0;$ii < count($LGroup);$ii++){
											$splitRes=explode("-",$LGroup[$ii]);
		                                    $seigyoKArr[] = $splitRes[1] . '_' .$splitRes[0];
		                                    if ((($sumKeySQL !== '') && ($filterData !== '')) || (($sumKeySQL !== '') && (count($burstItm) > 0))) {
		                                        $seigyoFld = $splitRes[1] . '_' .$splitRes[0];
		                                        $fldnm = $seigyoFld;
		                                        $chkSeigyoFld = 'SQ2.' . $seigyoFld;
		                                    } else {
		                                        $seigyoFld = 'SQ2.' . $splitRes[1] . '_' .$splitRes[0];
		                                        $fldnm = $splitRes[1] . '_' .$splitRes[0];
		                                        $chkSeigyoFld = $seigyoFld;
		                                    }
		                                    // サマリー情報がある場合
		                                    if ($sumKeySQL !== '') {
		                                        foreach ($selColInfoData as $key => $selCol) {
		                                            $selColfld = $selCol['D2FLD'] . '_' . $selCol['D2FILID'];
		                                            //e_log( $lvlCnt.'1111fldshutoku:'.$fldnm .$selColfld);
		                                            if ($selColfld === $fldnm) {
		                                                //e_log( $lvlCnt.'fldshutoku:'.$fldnm .print_r($selCol,true));
		                                                if (count($burstItm) > 0) {
		                                                    $seigyoKeyArr[] = $seigyoFld;
		                                                } else if ($filterData !== '') {
		                                                    $seigyoKeyArr[] = $seigyoFld;
		                                                } else {
		                                                    if ($selCol['SFGMES'] !== '') {
		                                                        $seigyoKeyArr[] = $selCol['SFGMES'] . '(' . $seigyoFld . ')';
		                                                    } else {
		                                                        $seigyoKeyArr[] = $seigyoFld;
		                                                    }
		                                                }
		                                                break;
		                                            }
		                                        }
		                                    } else {
		                                        e_log('制御フィールドチェック：'.$chkSeigyoFld.print_r($strColInfoArr,true));
		                                        if (in_array($chkSeigyoFld, $strColInfoArr, true)) {
		                                            $seigyoKeyArr[] = $seigyoFld;
		                                        } else {
		                                            $seigyoKeyArr = array();
		                                            break;
		                                        }
		                                    }
										}

									}else{
	                                    $seigyoKArr[] = $db2colmData['DCFLD' . $lvlCnt] . '_' . $db2colmData['DCFILID' . $lvlCnt];
	                                    if ((($sumKeySQL !== '') && ($filterData !== '')) || (($sumKeySQL !== '') && (count($burstItm) > 0))) {
	                                        $seigyoFld = $db2colmData['DCFLD' . $lvlCnt] . '_' . $db2colmData['DCFILID' . $lvlCnt];
	                                        $fldnm = $seigyoFld;
	                                        $chkSeigyoFld = cmMer('SQ2.' . $seigyoFld);
	                                    } else {
	                                        $seigyoFld = 'SQ2.' . $db2colmData['DCFLD' . $lvlCnt] . '_' . $db2colmData['DCFILID' . $lvlCnt];
	                                        $fldnm = $db2colmData['DCFLD' . $lvlCnt] . '_' . $db2colmData['DCFILID' . $lvlCnt];
	                                        $chkSeigyoFld = cmMer($seigyoFld);
	                                    }
	                                    // サマリー情報がある場合
	                                    if ($sumKeySQL !== '') {
	                                        foreach ($selColInfoData as $key => $selCol) {
	                                            $selColfld = $selCol['D2FLD'] . '_' . $selCol['D2FILID'];
	                                            //e_log( $lvlCnt.'1111fldshutoku:'.$fldnm .$selColfld);
	                                            if ($selColfld === $fldnm) {
	                                                //e_log( $lvlCnt.'fldshutoku:'.$fldnm .print_r($selCol,true));
	                                                if (count($burstItm) > 0) {
	                                                    $seigyoKeyArr[] = $seigyoFld;
	                                                } else if ($filterData !== '') {
	                                                    $seigyoKeyArr[] = $seigyoFld;
	                                                } else {
	                                                    if ($selCol['SFGMES'] !== '') {
	                                                        $seigyoKeyArr[] = $selCol['SFGMES'] . '(' . $seigyoFld . ')';
	                                                    } else {
	                                                        $seigyoKeyArr[] = $seigyoFld;
	                                                    }
	                                                }
	                                                break;
	                                            }
	                                        }
	                                    } else {
	                                        e_log('制御フィールドチェック：'.$chkSeigyoFld.print_r($strColInfoArr,true));
	                                        if (in_array($chkSeigyoFld,$strColInfoArr, true)) {
	                                            $seigyoKeyArr[] = $seigyoFld;
	                                        } else {
	                                            $seigyoKeyArr = array();
	                                            break;
	                                        }
	                                    }
									}
                                }
                                /*if ($db2colmData['DCFLD' . $lvlCnt] !== '') {
                                    $seigyoKArr[] = $db2colmData['DCFLD' . $lvlCnt] . '_' . $db2colmData['DCFILID' . $lvlCnt];
                                    if ((($sumKeySQL !== '') && ($filterData !== '')) || (($sumKeySQL !== '') && (count($burstItm) > 0))) {
                                        $seigyoFld = $db2colmData['DCFLD' . $lvlCnt] . '_' . $db2colmData['DCFILID' . $lvlCnt];
                                        $fldnm = $seigyoFld;
                                        $chkSeigyoFld = 'SQ2.' . $seigyoFld;
                                    } else {
                                        $seigyoFld = 'SQ2.' . $db2colmData['DCFLD' . $lvlCnt] . '_' . $db2colmData['DCFILID' . $lvlCnt];
                                        $fldnm = $db2colmData['DCFLD' . $lvlCnt] . '_' . $db2colmData['DCFILID' . $lvlCnt];
                                        $chkSeigyoFld = $seigyoFld;
                                    }
                                    // サマリー情報がある場合
                                    if ($sumKeySQL !== '') {
                                        foreach ($selColInfoData as $key => $selCol) {
                                            $selColfld = $selCol['D2FLD'] . '_' . $selCol['D2FILID'];
                                            //e_log( $lvlCnt.'1111fldshutoku:'.$fldnm .$selColfld);
                                            if ($selColfld === $fldnm) {
                                                //e_log( $lvlCnt.'fldshutoku:'.$fldnm .print_r($selCol,true));
                                                if (count($burstItm) > 0) {
                                                    $seigyoKeyArr[] = $seigyoFld;
                                                } else if ($filterData !== '') {
                                                    $seigyoKeyArr[] = $seigyoFld;
                                                } else {
                                                    if ($selCol['SFGMES'] !== '') {
                                                        $seigyoKeyArr[] = $selCol['SFGMES'] . '(' . $seigyoFld . ')';
                                                    } else {
                                                        $seigyoKeyArr[] = $seigyoFld;
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    } else {
                                        //e_log('制御フィールドチェック：'.$chkSeigyoFld.print_r($strColInfoArr,true));
                                        if (in_array($chkSeigyoFld, $strColInfoArr, true)) {
                                            $seigyoKeyArr[] = $seigyoFld;
                                        } else {
                                            $seigyoKeyArr = array();
                                            break;
                                        }
                                    }
                                }*/
                            }
                            //e_log('制御カラム取得：'.print_r($seigyoKeyArr,true));
                           /* if (count($seigyoKeyArr) > 0) {
                                $strRowNumCol = ' ROWNUMBER() OVER ( ';
                                $strSeigyoSortColInfo = join(', ', $seigyoKeyArr);
                                $strRowNumCol.= ' ORDER BY ' . ' ' . $strSeigyoSortColInfo;
                                $strRowNumCol.= ' ) AS ROWNUM ';
                                //e_log('制御カラム情報：'.$strRowNumCol);
                                
                            }*/
							$seigyoSort = "";
							$columnSort = "";
							
			                if (count($seigyoKeyArr) > 0) {
								$sortColInfoDataT = array();
								e_log("sortColInfoData:".print_r($sortColInfoData,true));
								if(count($sortColInfoData)>0){
									foreach($sortColInfoData as $key => $val){
										$sortColInfoDataT['SQ2.'.$val['D2FLD'] . '_' . $val['D2FILID']] = $val['D2RSTP'];
									}
								}
								foreach($seigyoKeyArr as $k => $val){
									if(isset($sortColInfoDataT[$val])){
										$seigyoKeyArr[$k] = $val.' '.$sortColInfoDataT[$val];
										unset($sortColInfoDataT[$val]);
									}
								}
								if(count($sortColInfoData)>0){
									foreach($sortColInfoData as $key => $val){
					                    $sVal = 'SQ2.'.$val['D2FLD'].'_'.$val['D2FILID'];
										if(isset($sortColInfoDataT[$sVal])){
											$columnSort .= " , ". $sVal.' '.$sortColInfoDataT[$sVal];
										}
									}
								}
			                    $strRowNumCol = ' ROWNUMBER() OVER ( ';
			                    $strSeigyoSortColInfo = join(', ', $seigyoKeyArr);
			                    $strRowNumCol.= ' ORDER BY ' . ' ' . $strSeigyoSortColInfo;
								$seigyoSort = " ORDER BY  ". $strSeigyoSortColInfo.$columnSort;
			                    $strRowNumCol.= ' ) AS ROWNUM ';
			                    e_log('制御カラム情報createExecuteSQLseigyoSort：'.$seigyoSort);
			                  }
                        }
                    }
                }
                if ($rtn === 0) {
                    $sortSeigyoSQL = '';
                    if (count($qrydata['SORTCOLINFO']) > 0) {
                        $sortColList = array();
                        foreach ($qrydata['SORTCOLINFO'] as $key => $val) {
                            $sortColList[] = $val['D2FLD'] . '_' . $val['D2FILID'];
                        }
                        $sortArr = array_merge($sortColList, $seigyoKArr);
                        if (count(array_unique($sortArr)) < (count($sortColList) + count($seigyoKArr))) {
                            $dupKeyarr = array_intersect($sortColList, $seigyoKArr);
                            //e_log('重複キーがある'.print_r($dupKeyarr,true));
                            $strSortFldLst = $strSortFldArr;
                            foreach ($dupKeyarr as $key => $val) {
                                $rmKey = array_search($val, $sortColList);
                                if ($rmKey !== NULL) {
                                    unset($strSortFldLst[$rmKey]);
                                }
                            }
                            $sortSeigyoSQL = $seigyoSort;
                        } else {
                            $sortSeigyoSQL = $seigyoSort;
                        }
                    }
                }
                if ($rtn === 0) {
                    //制御実行の【フィルタ情報とバーストアイテム】無しの場合
                    if ($filterData === '' && count($burstItm) === 0) {
                        $subQry3SQL = '';
                        $subQry3SQL.= ' SELECT';
                        $subQry3SQL.= ' ' . $strColInfo;
                        if ($strRowNumCol !== '') {
                            $subQry3SQL.= ' , ' . $strRowNumCol;
                        }
                        $subQry3SQL.= ' FROM ' . $subQry2SQL;
                        $subQry3SQL.= ' ' . $sumKeySQL;
                        $subQry3SQL = $subQry3SQL . $sortSeigyoSQL;
                        $strEXESQL = $subQry3SQL;
                        //e_log('制御実行の【フィルタ情報とバーストアイテム】無しのSQL文：：'.$strEXESQL);
                        $strTest3SQL = '';
                        $strTest3SQL.= ' SELECT';
                        $strTest3SQL.= ' ' . $strColInfo;
                        if ($strRowNumCol !== '') {
                            $strTest3SQL.= ' , ' . $strRowNumCol;
                        }
                        $strTest3SQL.= ' FROM ' . $strTest2SQL;
                        $strTest3SQL.= ' ' . $sumKeySQL;
                        $strTest3SQL = $strTest3SQL . $sortSeigyoSQL;
                        $strTestSQL = $strTest3SQL;
                        //e_log('テスト実行用制御実行の【フィルタ情報とバーストアイテム】無しのSQL文：：'.$strTestSQL);
                        
                    } else {
                        if (count($burstItm) > 0) {
                            e_log('制御実行のバースト情報のため条件設定開始');
                            //制御実行の【バーストアイテム】ありの場合
                            $strburstSQL = '';
                            if (cmMer($burstItm['WABAFLD']) !== '') {
                                $burstFld = cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']);
                                $burstFldTyp = '';
                                foreach ($qrydata['SELCOLINFO'] as $key => $value) {
                                    if ($burstFld === ($value['D2FLD'] . '_' . $value['D2FILID'])) {
                                        $burstFldTyp = $value['D2TYPE'];
                                        break;
                                    }
                                }
                                if (($burstFldType === 'P') || ($burstFldType === 'S') || ($burstFldType === 'B')) {
                                    $typFlg = false;
                                } else {
                                    $typFlg = true;
                                }
                                if ($sumKeySQL === '') {
                                    $burstFld = 'SQ2.' . $burstFld;
                                }
                                if ($burstItm['WABFLG'] == '1') {
                                    $wldataList = array();
                                    foreach ($burstItm['WLDATA'] AS $KEY => $VALUE) {
                                        $wldataList[] = $VALUE;
                                    }
                                    if (count($wldataList) > 0) {
                                        if ($typFlg) {
                                            $burstItmParam = join('\' , \'', $wldataList);
                                            $burstItmParam = '\'' . $burstItmParam . '\'';
                                        } else {
                                            $burstItmParam = join(' , ', $wldataList);
                                        }
                                    }
                                    $strburstSQL.= $burstFld . ' IN ( ';
                                    $strburstSQL.= $burstItmParam;
                                    $strburstSQL.= ')';
                                } else if (cmMer($burstItm['WABAFR']) !== '' && cmMer($burstItm['WABATO']) !== '') {
                                    $strburstSQL.= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' BETWEEN ';
                                    if ($typFlg) {
                                        $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\'' . ' AND ' . '\'' . cmMer($burstItm['WABATO']) . '\' ';
                                    } else {
                                        $strburstSQL.= cmMer($burstItm['WABAFR']) . ' AND ' . cmMer($burstItm['WABATO']);
                                    }
                                } else if ($burstItm['WABAFR'] !== '') {
                                    $strburstSQL.= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' = ';
                                    if ($typFlg) {
                                        $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\' ';
                                    } else {
                                        $strburstSQL.= cmMer($burstItm['WABAFR']);
                                    }
                                }
                            }
                            //e_log('バースト条件データ：'.$strburstSQL);
                            if ($strburstSQL === '') {
                                $subQry3SQL = '';
                                $subQry3SQL.= ' SELECT';
                                $subQry3SQL.= ' ' . $strColInfo;
                                if ($strRowNumCol !== '') {
                                    $subQry3SQL.= ' , ' . $strRowNumCol;
                                }
                                $subQry3SQL.= ' FROM ' . $subQry2SQL;
                                $subQry3SQL.= ' ' . $sumKeySQL;
                                $subQry3SQL = $subQry3SQL . $sortSeigyoSQL;
                                $strEXESQL = $subQry3SQL;
                                //e_log('制御実行の【バーストアイテムありで】無しのSQL文：：'.$strEXESQL);
                                $strTest3SQL = '';
                                $strTest3SQL.= ' SELECT';
                                $strTest3SQL.= ' ' . $strColInfo;
                                if ($strRowNumCol !== '') {
                                    $strTest3SQL.= ' , ' . $strRowNumCol;
                                }
                                $strTest3SQL.= ' FROM ' . $strTest2SQL;
                                $strTest3SQL.= ' ' . $sumKeySQL;
                                $strTest3SQL = $strTest3SQL . $sortSeigyoSQL;
                                $strTestSQL = $strTest3SQL;
                                //e_log('テスト実行用制御実行の【バーストアイテムありで】無しのSQL文：：'.$strTestSQL);
                                
                            } else {
                                if ($sumKeySQL === '') {
                                    $subQry3SQL = '';
                                    $subQry3SQL.= ' SELECT';
                                    $subQry3SQL.= ' ' . $strColInfo;
                                    if ($strRowNumCol !== '') {
                                        $subQry3SQL.= ' , ' . $strRowNumCol;
                                    }
                                    $subQry3SQL.= ' FROM ' . $subQry2SQL;
                                    $subQry3SQL.= ' WHERE ' . $strburstSQL;
                                    $subQry3SQL.= ' ' . $sumKeySQL;
                                    $subQry3SQL = $subQry3SQL . $sortSeigyoSQL;
                                    $strEXESQL = $subQry3SQL;
                                    //e_log('制御実行の【バーストアイテムありで】サマリキー無しのSQL文：：'.$strEXESQL);
                                    $strTest3SQL = '';
                                    $strTest3SQL.= ' SELECT';
                                    $strTest3SQL.= ' ' . $strColInfo;
                                    if ($strRowNumCol !== '') {
                                        $strTest3SQL.= ' , ' . $strRowNumCol;
                                    }
                                    $strTest3SQL.= ' FROM ' . $strTest2SQL;
                                    $strTest3SQL.= ' WHERE ' . $strburstSQL;
                                    $strTest3SQL.= ' ' . $sumKeySQL;
                                    $strTest3SQL = $strTest3SQL . $sortSeigyoSQL;
                                    $strTestSQL = $strTest3SQL;
                                    //e_log('テスト実行用制御実行の【バーストアイテムありで】サマリキー無しのSQL文：：'.$strTestSQL);
                                    
                                } else {
                                    $subQry3SQL = '';
                                    $subQry3SQL.= ' SELECT';
                                    $subQry3SQL.= ' ' . $strColInfo;
                                    if ($filterSQL === '') {
                                        if ($strRowNumCol !== '') {
                                            $subQry3SQL.= ' , ' . $strRowNumCol;
                                        }
                                    }
                                    $subQry3SQL.= ' FROM ' . $subQry2SQL;
                                    $subQry3SQL.= ' ' . $sumKeySQL;
                                    $subQry3SQL = $subQry3SQL . $sortSQL;
                                    if ($strburstSQL !== '') {
                                        $subQryexe = ' SELECT ttbl.* ';
                                        if ($strRowNumCol !== '') {
                                            $subQryexe.= ' , ' . $strRowNumCol;
                                        }
                                        $subQryexe.= ' FROM ( ' . $subQry3SQL . ' ) ttbl ';
                                        $subQryexe.= ' WHERE ' . $strburstSQL;
                                        $strEXESQL = $subQryexe;
                                        //e_log('制御実行の【バーストアイテムありで】サマリキーもありのSQL文：：'.$strEXESQL);
                                        
                                    } else {
                                        $strEXESQL = $subQry3SQL;
                                        //e_log('制御実行の【バーストアイテムありで】なしサマリキーもありのSQL文：：'.$strEXESQL);
                                        
                                    }
                                    $strTest3SQL = '';
                                    $strTest3SQL.= ' SELECT';
                                    $strTest3SQL.= ' ' . $strColInfo;
                                    if ($filterSQL === '') {
                                        if ($strRowNumCol !== '') {
                                            $strTest3SQL.= ' , ' . $strRowNumCol;
                                        }
                                    }
                                    $strTest3SQL.= ' FROM ' . $strTest2SQL;
                                    $strTest3SQL.= ' ' . $sumKeySQL;
                                    $strTest3SQL = $strTest3SQL . $sortSQL;
                                    if ($strburstSQL !== '') {
                                        $subTestQryexe = ' SELECT ttbl.* ';
                                        if ($strRowNumCol !== '') {
                                            $subTestQryexe.= ' , ' . $strRowNumCol;
                                        }
                                        $subTestQryexe.= ' FROM ( ' . $strTest3SQL . ' ) ttbl ';
                                        $subTestQryexe.= ' WHERE ' . $strburstSQL;
                                        $strEXESQL = $subTestQryexe;
                                        //e_log('テスト実行用制御実行の【バーストアイテムありで】サマリキーもありのSQL文：：'.$strTestSQL);
                                        
                                    } else {
                                        $strTestSQL = $strTest3SQL;
                                        //e_log('テスト実行用制御実行の【バーストアイテムありで】サマリキー無しのSQL文：：'.$strTestSQL);
                                        
                                    }
                                }
                            }
                            //e_log('制御実行のフィルタ情報のため条件設定終了');
                            
                        } else if ($filterData !== '') {
                            //e_log('制御実行のフィルタ情報のため条件設定開始');
                            $filterSQLArr = array();
                            if ($sumKeySQL === '') {
                                //e_log('選択フィールド:'.print_r($qrydata['SELCOLINFO'],true));
                                foreach ($qrydata['SELCOLINFO'] as $key => $value) {
                                    $colnm = 'SQ2.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                                    $addFlg = true;
                                    $byte = checkByte($filterData);
                                    if ($byte[1] > 0) {
                                        if ($value['D2TYPE'] !== 'O') {
                                            $addFlg = false;
                                        }
                                    }
                                    if ($addFlg) {
                                        switch ($value['D2TYPE']) {
                                            case 'L':
                                                $filterSQLArr[].= 'CAST(' . $colnm . ' AS CHAR(10))' . ' LIKE \'%' . $filterData . '%\' ';
                                            break;
                                            case 'T':
                                                $filterSQLArr[].= 'CAST(' . $colnm . ' AS CHAR(8))' . ' LIKE \'%' . $filterData . '%\' ';
                                            break;
                                            case 'Z':
                                                $filterSQLArr[].= 'CAST(' . $colnm . ' AS CHAR(26))' . ' LIKE \'%' . $filterData . '%\' ';
                                            break;
                                            default:
                                                $filterSQLArr[].= $colnm . ' LIKE \'%' . $filterData . '%\' ';
                                            break;
                                        }
                                    }
                                    if (count($filterSQLArr) > 0) {
                                        $filterSQL = join('OR ', $filterSQLArr);
                                    } else {
                                        $filterSQL = '';
                                    }
                                }
                                $subQry3SQL = '';
                                $subQry3SQL.= ' SELECT';
                                $subQry3SQL.= ' ' . $strColInfo;
                                if ($strRowNumCol !== '') {
                                    $subQry3SQL.= ' , ' . $strRowNumCol;
                                }
                                $subQry3SQL.= ' FROM ' . $subQry2SQL;
                                if ($filterSQL !== '') {
                                    $subQry3SQL.= ' WHERE ' . $filterSQL;
                                }
                                $subQry3SQL.= ' ' . $sumKeySQL;
                                $subQry3SQL = $subQry3SQL . $sortSQL;
                                $strEXESQL = $subQry3SQL;
                                $strTest3SQL = '';
                                $strTest3SQL.= ' SELECT';
                                $strTest3SQL.= ' ' . $strColInfo;
                                if ($strRowNumCol !== '') {
                                    $strTest3SQL.= ' , ' . $strRowNumCol;
                                }
                                $strTest3SQL.= ' FROM ' . $strTest2SQL;
                                if ($filterSQL !== '') {
                                    $strTest3SQL.= ' WHERE ' . $filterSQL;
                                }
                                $strTest3SQL.= ' ' . $sumKeySQL;
                                $strTest3SQL = $strTest3SQL . $sortSQL;
                                $strTestSQL = $strTest3SQL;
                                //e_log('テスト実行用制御実行の【フィルターありで】サマリキー無しのSQL文：：'.$strTestSQL);
                                //e_log('制御実行の【フィルターありで】サマリキー無しのSQL文：：'.$strEXESQL);
                                
                            } else {
                                //e_log('選択フィールド:'.print_r($qrydata['SELCOLINFO'],true));
                                foreach ($qrydata['SELCOLINFO'] as $key => $value) {
                                    $colnm = $value['D2FLD'] . '_' . $value['D2FILID'];
                                    $addFlg = true;
                                    $byte = checkByte($filterData);
                                    if ($byte[1] > 0) {
                                        if ($value['D2TYPE'] !== 'O') {
                                            $addFlg = false;
                                        }
                                    }
                                    /*if($addFlg){
                                        $filterSQLArr[] .= $colnm . ' LIKE \'%' . $filterData . '%\' ';
                                    }*/
                                    if ($addFlg) {
                                        switch ($value['D2TYPE']) {
                                            case 'L':
                                                $filterSQLArr[].= 'CAST(' . $colnm . ' AS CHAR(10))' . ' LIKE \'%' . $filterData . '%\' ';
                                            break;
                                            case 'T':
                                                $filterSQLArr[].= 'CAST(' . $colnm . ' AS CHAR(8))' . ' LIKE \'%' . $filterData . '%\' ';
                                            break;
                                            case 'Z':
                                                $filterSQLArr[].= 'CAST(' . $colnm . ' AS CHAR(26))' . ' LIKE \'%' . $filterData . '%\' ';
                                            break;
                                            default:
                                                $filterSQLArr[].= $colnm . ' LIKE \'%' . $filterData . '%\' ';
                                            break;
                                        }
                                    }
                                    if (count($filterSQLArr) > 0) {
                                        $filterSQL = join('OR ', $filterSQLArr);
                                    } else {
                                        $filterSQL = '';
                                    }
                                }
                                $subQry3SQL = '';
                                $subQry3SQL.= ' SELECT';
                                $subQry3SQL.= ' ' . $strColInfo;
                                if ($filterSQL === '') {
                                    if ($strRowNumCol !== '') {
                                        $subQry3SQL.= ' , ' . $strRowNumCol;
                                    }
                                }
                                $subQry3SQL.= ' FROM ' . $subQry2SQL;
                                $subQry3SQL.= ' ' . $sumKeySQL;
                                $subQry3SQL = $subQry3SQL . $sortSQL;
                                if ($filterSQL !== '') {
                                    $subQryexe = ' SELECT ttbl.* ';
                                    if ($strRowNumCol !== '') {
                                        $subQryexe.= ' , ' . $strRowNumCol;
                                    }
                                    $subQryexe.= ' FROM ( ' . $subQry3SQL . ' ) ttbl ';
                                    $subQryexe.= ' WHERE ' . $filterSQL;
                                    $strEXESQL = $subQryexe;
                                    //e_log('制御実行の【フィルターありで】サマリキーありでSQL文：：'.$strEXESQL);
                                    
                                } else {
                                    $strEXESQL = $subQry3SQL;
                                    //e_log('制御実行の【フィルター無しで】サマリキーありでSQL文：：'.$strEXESQL);
                                    
                                }
                                $strTest3SQL = '';
                                $strTest3SQL.= ' SELECT';
                                $strTest3SQL.= ' ' . $strColInfo;
                                if ($filterSQL === '') {
                                    if ($strRowNumCol !== '') {
                                        $strTest3SQL.= ' , ' . $strRowNumCol;
                                    }
                                }
                                $strTest3SQL.= ' FROM ' . $strTest2SQL;
                                $strTest3SQL.= ' ' . $sumKeySQL;
                                $strTest3SQL = $strTest3SQL . $sortSQL;
                                if ($filterSQL !== '') {
                                    $subTestQryexe = ' SELECT ttbl.* ';
                                    if ($strRowNumCol !== '') {
                                        $subTestQryexe.= ' , ' . $strRowNumCol;
                                    }
                                    $subTestQryexe.= ' FROM ( ' . $strTest3SQL . ' ) ttbl ';
                                    $subTestQryexe.= ' WHERE ' . $filterSQL;
                                    $strTestSQL = $subTestQryexe;
                                    //e_log('テスト実行用制御実行の【フィルターありで】サマリキーありでSQL文：：'.$strTestSQL);
                                    
                                } else {
                                    $strTestSQL = $strTest3SQL;
                                    //e_log('テスト実行用制御実行の【フィルター無しで】サマリキーありでSQL文：：'.$strTestSQL);
                                    
                                }
                            }
                            e_log('制御実行のフィルタ情報のため条件設定終了');
                        }
                    }
                }
            } else if ($rnFlg === 2) {
                //e_log('ピボットデータ取得:'.$fdb2csv1['D1NAME'].'ピボットキー：'.$pivid);
                $xKeyArr = array();
                $yKeyArr = array();
                if ($rtn === 0) {
                    $db2pcolData = getDB2PCOLData($db2con, $fdb2csv1['D1NAME'], $pivid);
                    if ($db2pcolData[result] === true) {
                        if (count($db2pcolData['data']) > 0) {
                            $db2pcolData = $db2pcolData['data'];
                            //e_log('ピボットキーデータ：'.print_r($db2pcolData,true));
                            foreach ($db2pcolData as $key => $value) {
                                if ($value['D2TYPE'] !== '') {
                                    $pivfldnm = $value['PIVFLD'];
                                    if ($sumKeySQL === '') {
                                        $pivfldnm = 'SQ2.' . $pivfldnm;
                                    } else {
                                        if (count($burstItm) === 0) {
                                            $pivfldnm = 'SQ2.' . $pivfldnm;
                                        }
                                    }
                                    if ($value['PIVFLDSUM'] !== '') {
                                        if (count($burstItm) === 0) {
                                            $pivfldnm = $value['PIVFLDSUM'] . '(' . $pivfldnm . ')' . ' ' . $value['PIVFLDSORT'];
                                        } else {
                                            $pivfldnm = $pivfldnm . ' ' . $value['PIVFLDSORT'];
                                        }
                                    } else {
                                        $pivfldnm = $pivfldnm . ' ' . $value['PIVFLDSORT'];
                                    }
                                    if ($value['WPPFLG'] == 1) {
                                        $xKeyArr[] = $pivfldnm;
                                    } else if ($value['WPPFLG'] == 2) {
                                        $yKeyArr[] = $pivfldnm;
                                    }
                                }
                            }
                            //e_log('ピボットXキー配列：'.print_r($xKeyArr,true));
                            //e_log('ピボットYキー配列：'.print_r($yKeyArr,true));
                            
                        }
                    }
                    //ソートキーなしのデータ
                    $strRowNumCol = ' ROWNUMBER() OVER ( ';
                    $strRowNumCol.= ' ) AS RN ';
                    // 縦軸のデータ
                    $strXRowNumCol = ' ROWNUMBER() OVER ( ';
                    if (count($xKeyArr) > 0) {
                        $xRNSQL = join(' , ', $xKeyArr);
                        $strXRowNumCol.= ' ORDER BY ' . ' ' . $xRNSQL;
                    }
                    $strXRowNumCol.= ' ) AS XROWNUM ';
                    // 横軸のデータ
                    $strYRowNumCol = ' ROWNUMBER() OVER ( ';
                    if (count($yKeyArr) > 0) {
                        $yRNSQL = join(' , ', $yKeyArr);
                        $strYRowNumCol.= ' ORDER BY ' . ' ' . $yRNSQL;
                    }
                    $strYRowNumCol.= ' ) AS YROWNUM ';
                    if (count($burstItm) === 0) {
                        $subQry3SQL = '';
                        $subQry3SQL.= ' SELECT';
                        $subQry3SQL.= ' ' . $strColInfo;
                        $subQry3SQL.= ' , ' . $strRowNumCol;
                        $subQry3SQL.= ' , ' . $strXRowNumCol;
                        $subQry3SQL.= ' , ' . $strYRowNumCol;
                        $subQry3SQL.= ' FROM ' . $subQry2SQL;
                        $subQry3SQL.= ' ' . $sumKeySQL;
                        $subQry3SQL = $subQry3SQL . $sortSQL;
                        $strEXESQL = $subQry3SQL;
                        //                        e_log('ピボット実行BurstItmなしのSQL文：'.$strEXESQL);
                        $strTest3SQL = '';
                        $strTest3SQL.= ' SELECT';
                        $strTest3SQL.= ' ' . $strColInfo;
                        $strTest3SQL.= ' , ' . $strRowNumCol;
                        $strTest3SQL.= ' , ' . $strXRowNumCol;
                        $strTest3SQL.= ' , ' . $strYRowNumCol;
                        $strTest3SQL.= ' FROM ' . $strTest2SQL;
                        $strTest3SQL.= ' ' . $sumKeySQL;
                        $strTest3SQL = $strTest3SQL . $sortSQL;
                        $strTestSQL = $strTest3SQL;
                        //e_log('テスト用ピボット実行BurstItmなしのSQL文：'.$strTestSQL);
                        /*$subQry3SQL  = '';
                        $subQry3SQL .= ' SELECT';
                        $subQry3SQL .= ' '. $strColInfo;
                        $subQry3SQL .= ' , '.$strRowNumCol;
                        $subQry3SQL .= ' , '.$strXRowNumCol;
                        $subQry3SQL .= ' , '.$strYRowNumCol;
                        $subQry3SQL .= ' FROM '.$subQry2SQL;
                        $subQry3SQL .= ' '.$sumKeySQL;
                        $subQry3SQL = $subQry3SQL.$sortSQL;
                        $strEXESQL = $subQry3SQL;
                        //e_log('ピボット実行BurstItmなしのSQL文：'.$strEXESQL);*/
                    } else {
                        $strburstSQL = '';
                        if (cmMer($burstItm['WABAFLD']) !== '') {
                            $burstFld = cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']);
                            $burstFldTyp = '';
                            foreach ($qrydata['SELCOLINFO'] as $key => $value) {
                                if ($burstFld === ($value['D2FLD'] . '_' . $value['D2FILID'])) {
                                    $burstFldTyp = $value['D2TYPE'];
                                    break;
                                }
                            }
                            if (($burstFldType === 'P') || ($burstFldType === 'S') || ($burstFldType === 'B')) {
                                $typFlg = false;
                            } else {
                                $typFlg = true;
                            }
                            if ($sumKeySQL === '') {
                                $burstFld = 'SQ2.' . $burstFld;
                            }
                            if ($burstItm['WABFLG'] == '1') {
                                $wldataList = array();
                                foreach ($burstItm['WLDATA'] AS $KEY => $VALUE) {
                                    $wldataList[] = $VALUE;
                                }
                                if (count($wldataList) > 0) {
                                    if ($typFlg) {
                                        $burstItmParam = join('\' , \'', $wldataList);
                                        $burstItmParam = '\'' . $burstItmParam . '\'';
                                    } else {
                                        $burstItmParam = join(' , ', $wldataList);
                                    }
                                }
                                $strburstSQL.= $burstFld . ' IN ( ';
                                $strburstSQL.= $burstItmParam;
                                $strburstSQL.= ')';
                            } else if (cmMer($burstItm['WABAFR']) !== '' && cmMer($burstItm['WABATO']) !== '') {
                                $strburstSQL.= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' BETWEEN ';
                                if ($typFlg) {
                                    $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\'' . ' AND ' . '\'' . cmMer($burstItm['WABATO']) . '\' ';
                                } else {
                                    $strburstSQL.= cmMer($burstItm['WABAFR']) . ' AND ' . cmMer($burstItm['WABATO']);
                                }
                            } else if ($burstItm['WABAFR'] !== '') {
                                $strburstSQL.= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' = ';
                                if ($typFlg) {
                                    $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\' ';
                                } else {
                                    $strburstSQL.= cmMer($burstItm['WABAFR']);
                                }
                            }
                            //e_log('バーストアイテム条件SQL：'.$strburstSQL);
                            
                        }
                        if ($sumKeySQL === '') {
                            $subQry3SQL = '';
                            $subQry3SQL.= ' SELECT';
                            $subQry3SQL.= ' ' . $strColInfo;
                            $subQry3SQL.= ' , ' . $strRowNumCol;
                            $subQry3SQL.= ' , ' . $strXRowNumCol;
                            $subQry3SQL.= ' , ' . $strYRowNumCol;
                            $subQry3SQL.= ' FROM ' . $subQry2SQL;
                            if ($strburstSQL !== '') {
                                $subQry3SQL.= ' WHERE ' . $strburstSQL;
                            }
                            $subQry3SQL = $subQry3SQL . $sortSQL;
                            $strEXESQL = $subQry3SQL;
                            //e_log('ピボット実行サマリキーなしのSQL文：'.$strEXESQL);
                            
                        } else {
                            $subQry3SQL = '';
                            $subQry3SQL.= ' SELECT';
                            $subQry3SQL.= ' ' . $strColInfo;
                            $subQry3SQL.= ' FROM ' . $subQry2SQL;
                            $subQry3SQL.= ' ' . $sumKeySQL;
                            $subQry3SQL = $subQry3SQL . $sortSQL;
                            if ($strburstSQL !== '') {
                                $subQryexe = ' SELECT ttbl.* ';
                                $subQryexe.= ' , ' . $strRowNumCol;
                                $subQryexe.= ' , ' . $strXRowNumCol;
                                $subQryexe.= ' , ' . $strYRowNumCol;
                                $subQryexe.= ' FROM ( ' . $subQry3SQL . ' ) ttbl ';
                                if ($strburstSQL !== '') {
                                    $subQryexe.= ' WHERE ' . $strburstSQL;
                                }
                                $strEXESQL = $subQryexe;
                                //e_log('ピボット実行の【バーストアイテムありで】サマリキーもありのSQL文：：'.$strEXESQL);
                                
                            } else {
                                $strEXESQL = $subQry3SQL;
                                //e_log('ピボット実行の【バーストアイテムありで】なしサマリキーもありのSQL文：：'.$strEXESQL);
                                
                            }
                            //e_log('ピボット実行サマリキー有りのSQL文：'.$strEXESQL);
                            
                        }
                    }
                }
            }
            //ログ用サブクエリ２
            $strLSQL2 = '';
            $strLSQL2.= ' SELECT';
            $strLSQL2.= ' ' . $strColInfo;
            $strLSQL2.= ' FROM ' . $strSSQL2;
            $strLSQL2.= ' ' . $sumKeySQL;
            $strLSQL2 = $strLSQL2 . $sortSQL;
        }
    }
    error_log("FinalQuery***".print_r($strEXESQL,true));
    $RTNARR = array();
    $RTNARR['RTN'] = $rtn;
    $RTNARR['MSG'] = $msg;
    $RTNARR['QRYDATA'] = $qrydata;
    //e_log('長さspace'.strlen($strEXESQL).'data->'.$strEXESQL);
    /*if($_SESSION['PHPQUERY']['user'][0]['WUUID'] === 'mmm_SQL'){
        $RTNARR['STREXECSQL'] = $testSubQry2SQL;
        $RTNARR['STREXECTESTSQL'] = $testStrTest2SQL;
    }else{*/
    $RTNARR['STREXECSQL'] = $strEXESQL;
    $RTNARR['STREXECTESTSQL'] = $strTestSQL;
    //}
    //e_log('memberdata:'.print_r($mbrTblLst,true));
    $RTNARR['MBRDATAARR'] = $mbrTblLst;
    //$RTNARR['STREXECSQL'] = spaceTrim($strEXESQL);
    //e_log('長さ'.strlen(spaceTrim($strEXESQL)).'data->'.spaceTrim($strEXESQL));
    $RTNARR['EXECPARAM'] = $params;
    $RTNARR['LIBLIST'] = $LIBLIST;
    $RTNARR['RDB'] = $selRDB;
    $RTNARR['LOG_SQL'] = array('LSQL_1' => $strLSQL1, 'LSQL_2' => $strLSQL2);
    return $RTNARR;
}
/**
 * 実行パラメータをSQL文にBindする
 * @param $strSQL:実行SQL文
 * @param $strSQL:実行パラメータリスト
 * 戻り値：パラメータ値Bindされた実行SQL文
 */
function getBindParam($strSQL, $param = '') {
    if ($param !== '') {
        for ($i = 0;$i < count($param);$i++) {
            $bind[] = array('{' . $i . '}' => $param[$i]);
        }
        for ($i = 0;$i < count($bind);$i++) {
            foreach ($bind[$i] as $key => $value) {
                $strSQL = str_replace($key, $value, $strSQL);
            }
        }
    }
    return $strSQL;
}
/**
 * 定義情報取得
 * @param $QRYNM:定義名
 * @seigyoFlg:入れてないSEQのため
 * 戻り値：定義情報
 */
function getQryData($db2con, $QRYNM,$seigyoFlg) {//QRYDATA
    $rtn = 0;
    $msg = '';
    $fdb2csv1 = array();
    $breftblfld = array();
    $fdb2csv2 = array();
    $fdb2csv5 = array();
    $bqrycnddat = array();
    $bqrycnd = array();
    $bcnddat = array();
    $fdb2csv25 = array();
    $bsumfld = array();
    if ($rtn === 0) {
        $res = getFDB2CSV1($db2con, $QRYNM);
        if ($res['result'] === 'NOTEXIST_GET') {
            $rtn = 3;
            $msg = showMsg($res['result'], array('クエリー'));
        } else if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $fdb2csv1 = $res['data'];
        }
    }
    if ($rtn === 0) {
        $res = getBREFTBLFLD($db2con, $QRYNM);
        if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $breftblfld = $res['data'];
        }
    }
    if ($rtn === 0) {
        $res = getFDB2CSV2($db2con, $QRYNM);
        if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $fdb2csv2 = $res['data'];
        }
    }
    if ($rtn === 0) {
        $res = getFDB2CSV5($db2con, $QRYNM);
        if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $fdb2csv5 = $res['data'];
        }
    }
    if ($rtn === 0) {
        $res = getBQRYCNDDAT($db2con, $QRYNM);
        if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $bqrycnddat = $res['data'];

        }
    }
    if ($rtn === 0) {
        $res = getBQRYCND($db2con, $QRYNM);
        if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $bqrycnd = $res['data'];
        }
    }
    if ($rtn === 0) {
        $res = getBCNDDAT($db2con, $QRYNM);
        if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $bcnddat = $res['data'];
        }
    }
    if ($rtn === 0) {
        $res = getFDB2CSV25($db2con, $QRYNM);
        if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $fdb2csv25 = $res['data'];
        }
    }
    if ($rtn === 0) {
        $res = getBSUMFLD($db2con, $QRYNM);
        if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $bsumfld = $res['data'];
        }
    }
    if ($rtn === 0) {
        $res = getSelColumnInfo($db2con, $QRYNM,$seigyoFlg);
        if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $selColInfo = $res['data'];
        }
    }
    if ($rtn === 0) {
        $res = getOrdColumnInfo($db2con, $QRYNM,$seigyoFlg);
        if ($res['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($res['result'], array('クエリー'));
        } else {
            $sortColInfo = $res['data'];
        }
    }
    if ($rtn === 0) {
        $res = fnGetFDB2CSV2CCSID($db2con, $QRYNM);
        if ($res['RTN'] !== 0) {
            $rtn = 1;
            $msg = showMsg($res['MSG'], array('クエリー'));
        } else {
            $fdb2csv2ccsid = $res['DATA'];
            $fdb2csv2Fld = $res['FLDDATA'];
        }
    }//STREXECTESTSQL
    $qryData = array(
		'FDB2CSV1' => $fdb2csv1,
		'BREFTBLFLD' => $breftblfld,
		'FDB2CSV2' => $fdb2csv2,
		'FDB2CSV5' => $fdb2csv5,
		'BQRYCNDDAT' => $bqrycnddat,
		'BQRYCND' => $bqrycnd,
		'BCNDDAT' => $bcnddat,
		'FDB2CSV25' => $fdb2csv25,
		'BSUMFLD' => $bsumfld,
		'SELCOLINFO' => $selColInfo,
		'SORTCOLINFO' => $sortColInfo,
		'FDB2CSV2CCSID' => $fdb2csv2ccsid,
		'FDB2CSV2FLDLST' => $fdb2csv2Fld);
    $rtnArr = array('RTN' => $rtn, 'MSG' => $msg, 'QRYDATA' => $qryData);
    return $rtnArr;
}

