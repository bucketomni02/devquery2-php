<?php
/**
 * ===========================================================================================
 * SYSTEM NAME    : PHPQUERY2
 * PROGRAM NAME   : 作成したクエリーの実行SQL作成
 * PROGRAM ID     : createTblExeSql.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/06/13
 * ===========================================================================================
 **/

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("getQryTblData.php");
include_once("comGetFDB2CSV2_CCSID.php");
include_once("common/inc/config.php");
/**
 * 実行して作成したSQLの確認
 * @param $db2con ：データベース接続
 * @param $strSQL ：実行SQL文
 * @param $params : 実行パラメータ
 * @戻り値：結果データ
 */
function execSQLQry ( $db2con, $strSQL, $params) {
    $rtn  = 0;
    $msg  =  '';
    $data   = array();
            error_log('PREPARE BEFORE SQL：'.$strSQL);

    $strSQLtmp = $strSQL .' FETCH FIRST 1 ROWS ONLY ';//Old
    //$strSQLtmp = $strSQL;
    e_log("strSQLtmp\n".$strSQLtmp."\nstrSQLtmp");
    e_log("strSQLparam\n".print_r($params,true)."\nstrSQLparam");
    if($rtn === 0){
	    // 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
	    if(strlen($strSQLtmp) > 32767 ){
	        error_log('msg2 = '.$msg);
	        $rtn = 1;
	        $msg =showMsg('FAIL_BASE_LENLIMIT');// 'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
	    }       
    }
    if($rtn === 0){

        $stmt = db2_prepare($db2con,$strSQLtmp);
        if($stmt === false){
            $rtn = 'FAIL_SEL';
            $msg = '設定したクエリーでSQLの実行準備中に失敗しました。</br> 設定をご確認ください。</br>【エラーコード：'. db2_stmt_errormsg().'】';
            error_log('エラーSQL：'.$strSQLtmp);
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rtn = 'FAIL_SEL';
                $msg = '設定したクエリーでSQLの実行中に失敗しました。</br> 設定をご確認ください。</br>【エラーコード：' . db2_stmt_errormsg().'】';
                 error_log('エラーSQL：'.$strSQLtmp.print_r($params,true).db2_stmt_error());
            }else{
                $row =  db2_fetch_assoc($stmt);
                if($row === false && db2_stmt_errormsg() !== '' ){
                    $errmsg =  db2_stmt_errormsg();
                    if((strpos($errmsg,'57017') !== false) && (strpos($errmsg,'-332') !== false)){
                        // ccsidエラー無視
                    }else{
                        $rtn = 'FAIL_SEL';
                        $msg .= '設定したクエリーでSQLの実行結果フェッチ中に失敗しました。</br> 設定をご確認ください。</br>【エラーコード：' . db2_stmt_errormsg().'】';
                    }
                }else{
                    $data[] = $row;
                    while ($row = db2_fetch_assoc($stmt)) {
                        $data[] = $row;
                    }
                }
            }
        }
    }
    
/*
    if ($rtn === 0) {
        $dataCnt = count($data);
        //e_log("\ndataCnt\n".$dataCnt."\ndataCnt");
        $dataRes = array();
        $sqlLMT = " ";
        $sqlLMT .= " SELECT COQRYLMT ";
        $sqlLMT .= " FROM ".MAINLIB."/DB2COF ";
        $sqlLMT .= " WHERE COQRYLMT >= ? ";
        //e_log("\nsqlLMT\n".$sqlLMT."\nsqlLMT");
        $stmt = db2_prepare($db2con, $sqlLMT);
        if ($stmt === false) {
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行失敗しました。' . db2_stmt_errormsg();
        } else {
            $cntParam = array($dataCnt);
            $r = db2_execute($stmt, $cntParam);
            if ($r === false) {
                $rtn=1;
                $msg = showMsg('FAIL_FUNC', array('クエリー実行')) . db2_stmt_errormsg(); //'クエリー実行失敗しました。'.db2_stmt_errormsg();
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $dataRes[] = $row;
                }
				if(CND_MAXQRY != 0){
	                if (count($dataRes) == 0) {
	                    $rtn = 2;
	                    $msg = showMsg('FAIL_BASE_LENLIMIT')."<br/>制限を増やす場合、管理者メニューのシステム設定から【クエリー実行制限】を編集してください。";
	                }
				}
            }
        }
    }
*/    
    //e_log("result\n".print_r($data,true)."\nresult");
    $rtnArr = array(
                'RTN' => $rtn ,
                'MSG' => $msg ,
                'STRSQL' => $strSQL,
                'RESULTDATA' => $data
                );
    //e_log(print_r($rtnArr,true));
    return $rtnArr;
}
/**
 * 定義の作成したSQL文で一時テーブル作成
 * @param $db2con ：データベース接続
 * @param $strSQL ：実行SQL文
 * @param $params : 実行パラメータ
 * @param $tmpTblNm ：一時テーブル名
 * @param $exeFlg : 実行フラグ
 * @戻り値：一時テーブル名
 */
function createSQLQryTbl ( $db2con , $strSQL , $params, $tmpTblNm , $exeFlg = '' ){
    $rtn = 0;
    $msg = '';
    // テーブルチェック
    $systables = cmGetSystables($db2con,SAVE_DB,$tmpTblNm);
    if(count($systables)){
        $delTblSQL = ' DROP TABLE '.SAVE_DB.'/'.$tmpTblNm;
        $stmt = db2_prepare($db2con,$delTblSQL); 
        if($stmt === false){
            $rtn  = 'FAIL_DEL';
            $msg  = showMsg('FAIL_FUNC',array('クエリー実行')).db2_stmt_errormsg();//'クエリー実行失敗しました。'.db2_stmt_errormsg();
        }else{
            $param = array();
            $r = db2_execute($stmt,$param);
            if($r === false){
                $rtn = 'FAIL_DEL';
                $msg = showMsg('FAIL_FUNC',array('クエリー実行')).db2_stmt_errormsg();//'クエリー実行失敗しました。'.db2_stmt_errormsg();
            }else{
                e_log('一時テーブルに重複があるので削除完了','1');
            }
        }
    }
    //クエリーはクエリー最大件数以上の場合　190315
    $msgmax = '';
    $resExec = execSQLQry($db2con, $strSQL, $params); 
    if($rtn === 0){
        if($exeFlg !== ''){
            $strSQL = $strSQL .' FETCH FIRST 1 ROWS ONLY ';
        }else{
            if($rtn === 0){
				//クエリー最大件数が0以上の場合、FETCH FIRSTで件数を制限する
                if(CND_MAXQRY > 0){
                    if(count($resExec['RESULTDATA']) > CND_MAXQRY){
                        $msgmax = showMsg('クエリーの結果がシステム値の最大件数より多いため、処理を中断しました。</br> '.CND_MAXQRY.'件のみ表示されます。');
                    }
                    $strSQL .= ' FETCH FIRST '.CND_MAXQRY.' ROWS ONLY ';
                }
                $rtnExeTMchk = comSetQryExecTimeMemory($db2con);
                e_log('reach it SQL実行時間設定');
            }
        }
        $strCreate  = '';
        $strCreate .=   ' CREATE TABLE';
        $strCreate .=   ' '.SAVE_DB.'/'.$tmpTblNm.' AS';
        $strCreate .=   ' ('.$strSQL.' )';
        $strCreate .=   ' WITH DATA ';
        e_log("\nstrCreate\n".$strCreate."\nstrCreate");
        if($rtn === 0){
            // 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
            if(strlen($strCreate) > 32767 ){
                $rtn = 1;
                $msg = showMsg('FAIL_BASE_LENLIMIT');//'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
            }       
        }
        $stmt = db2_prepare($db2con,$strCreate);
        if($stmt === false){
            $rtn  = 'FAIL_SEL';
            $msg = 'クエリー実行準備失敗しました。'.db2_stmt_errormsg();
            e_log('【削除】SQL作成エラー'.$strCreate.'エラー文章：'.db2_stmt_errormsg());
        }else{
            $r = db2_execute($stmt);
            if($r === false){
                $rtn = 'FAIL_SEL';
                $msg = 'クエリー実行中失敗しました。'.db2_stmt_errormsg();
                e_log('エラーコード：'.db2_stmt_errormsg());
                if(strpos(db2_stmt_errormsg(),'SQLSTATE=57005 SQLCODE=-666') !== false){
                    $msg = $msg. '<br/> 制限を増やす場合、管理者メニューのシステム設定から【クエリー実行制限】を編集してください。';
                }
            }else{
                $msg = 'ワークテーブル作成完了'.$tmpTblNm;
            }
        }
    }
    if($exeFlg === ''){
        if($rtn === 0){
            $rtnExeTMchk = comResetQryExecTimeMemory($db2con);
        }
    }
    //cmSetPHPQUERY($db2con);
    $rtnCreateTbl = array();
    $rtnCreateTbl['RTN'] = $rtn;
    $rtnCreateTbl['MSG'] = $msg;
    $rtnCreateTbl['MSGMAX'] = $msgmax;
    $rtnCreateTbl['TMPTBLNM'] = $tmpTblNm;
    $rtnCreateTbl['QRYCNT'] = count($resExec['RESULTDATA']);
    return $rtnCreateTbl;
}
/**
 * 実行して作成したSQLとパラメータバインド
 * @param $strSQL ：実行SQL文
 * @param $params : 実行パラメータ
 * @戻り値：結果データ
 */
function bindParamSQLQry($strSQL,$param = '',$AryBaseData = ''){

    $splitParamthe = array();
    $queryInParamCnt = preg_match_all('/(\{)[0-9]+(\})/',trim($strSQL));
    $bindStrSQL = "";
    if($AryBaseData !== ''){
        $splitParamthe = preg_split("/(\{)[0-9]+(\})/",trim($strSQL));
        if(count($AryBaseData) > 0){
            $pos = 0;
            foreach($param as $key){
                if(strpos($key,'%') !== false){
                    // \' adding
                    if($key[0] !== '\''){
                        $key = "'".$key;
                    }
                    // \' adding
                    if($key[strlen($key)-1] !== '\''){
                        $key = $key."'";
                    }
                    // RLIKE check adding require 'RTRIM' in query
                    if($key[1] === '%' && $key[strlen($key)-2] !== '%'){
                        $splitSpace = array();
                        // split space single or multi
                        $splitSpace = preg_split("/[\s]+/",trim($splitParamthe[$pos]));
                        if(count($splitSpace) > 2){
                            // check the last index of array is 【LIKE】
                            if(strtoupper($splitSpace[count($splitSpace)-1]) === "LIKE"){
                                // check the last before index of array is 【NOT】
                                if(strtoupper($splitSpace[count($splitSpace)-2]) === "NOT"){
                                    $splitSpace[count($splitSpace)-3] = "RTRIM(".$splitSpace[count($splitSpace)-3].")";

                                } else {
                                    $splitSpace[count($splitSpace)-2] = "RTRIM(".$splitSpace[count($splitSpace)-2].")";

                                }
                            }
                        }
                        // array data merge into string with concat space
                        $splitParamthe[$pos] = implode(" ",$splitSpace)."  {".($pos+1)."}";
                    }else{
                        // not RLIKE , nothing will change query
                        $splitParamthe[$pos] = $splitParamthe[$pos]."  {".($pos+1)."}";
                    }
                    // replacing % for LIKE
                    $param[$pos] = $key;
                }else{
                    if(($pos+1) <= $queryInParamCnt){
                        $splitParamthe[$pos] = $splitParamthe[$pos]."  {".($pos+1)."}";
                    }
                }
                $pos++;
            }
        }
        $strSQL = implode(" ",$splitParamthe);
    }
    
    if($param !== ''){
        for($i = 0;$i < count($param);$i++){
            $idx = $i+1;
            $bind[]= array('{'.$idx.'}' => $param[$i]);
        }
        for($i = 0;$i < count($bind);$i++){
            foreach($bind[$i] as $key => $value){
                $strSQL = str_replace($key,$value,$strSQL);
            }
        }
    }
    return $strSQL;
}
/**
 * 実行して作成したWith句SQLとパラメータバインド
 * @param $strSQL ：実行SQL文
 * @param $params : 実行パラメータ
 * @戻り値：結果データ
 */
function bindWithParamSQLQry($strSQL,$param = ''){
    if($param !== ''){
        foreach($param as $key => $pVal){
            $bind[$key]= $pVal;
        }
        foreach($bind as $key => $value){
            //e_log('bindparam:'.$key.'    '.$strSQL.'    '.$key.'    '.$value);
            $strSQL = str_replace($key,$value,$strSQL);
        }
    }
    return $strSQL;
}
/**
 * バインドパラメータ配列作成
 * @param $strSQL ：実行SQL文
 * @param $params : 実行パラメータ
 * @戻り値：結果データ
 */
/*function bindParamArr($paramData){
 e_log('function bindParamArr');
 e_log(print_r($paramData,true));
    $resParamArr = array();
    $withParamArr = array();
    if(count($paramData)>0){
        foreach($paramData as $pkey => $pdata){
            if((int)$pdata['CNDBCNT'] > 0){
                $pdata['CNDDAT'] = str_pad($pdata['CNDDAT'], (int)$pdata['CNDBCNT']);
            }
            if($pdata['CNDDTYP'] === '2'){
                if($pdata['CNDKBN'] === '1'){
                    if ($pdata['CNDDAT'] == '') {
                        $withParamArr[$pdata['CNDWNM']] =  '\''.'0'.'\'';
                    }else{
                        $withParamArr[$pdata['CNDWNM']] =  '\''.$pdata['CNDDAT'].'\'';
                    }
                }else{
                    if ($pdata['CNDDAT'] == '') {
                        $resParamArr[] =  bindWithLikeOptr($pdata['CNCKBN'],'\''.'0'.'\'');
                    }else{
                        $resParamArr[] =  bindWithLikeOptr($pdata['CNCKBN'],'\''.$pdata['CNDDAT'].'\'');
                    }
                }
            }else if($pdata['CNDDTYP'] === '6'){
                if($pdata['CNDKBN'] === '1'){
                    $withParamArr[$pdata['CNDWNM']] =  $pdata['CNDDAT'];
                }else{
                    $resParamArr[] =  bindWithLikeOptr($pdata['CNCKBN'],$pdata['CNDDAT']);
                }
            }else{
                if($pdata['CNDKBN'] === '1'){
                    $withParamArr[$pdata['CNDWNM']] =  '\''.db2_escape_string($pdata['CNDDAT']).'\'';
                }else{
                    $resParamArr[] =  '\''.db2_escape_string(bindWithLikeOptr($pdata['CNCKBN'],$pdata['CNDDAT'])).'\'';
                }
            }
            //error_log("HMM HMM HMM".print_r($resParamArr,true));
        }
    }
    $rtnarr = array(
            'PARAMDATA' => $resParamArr,
            'WITHPARAMDATA' => $withParamArr
        );
    //error_log('RETURN DATA ARR:'.print_r($rtnarr,true));
    return $rtnarr;
}*/
function bindParamArr($paramData){
    $resParamArr = array();
    $withParamArr = array();
    if(count($paramData)>0){
        foreach($paramData as $pkey => $pdata){
            if((int)$pdata['CNDBCNT'] > 0){
                $pdata['CNDDAT'] = str_pad($pdata['CNDDAT'], (int)$pdata['CNDBCNT']);
            }
            switch($pdata['CNDDTYP']){
                case '1': 
                    if($pdata['CNDKBN'] === '1'){
                        if($pdata['CNDDAT'] === ''){
                            if($pdata['CNDSTKB'] === '1'){
                                $withParamArr[$pdata['CNDWNM']] = '\'\'';
                            }else{
                                $withParamArr[$pdata['CNDWNM']] = "";                            
                            }
                        }else{
                            $withParamArr[$pdata['CNDWNM']] =  '\''.db2_escape_string($pdata['CNDDAT']).'\'';
                        }
                    }else{
                        if($pdata['CNDDAT'] === ''){
                            if($pdata['CNDSTKB'] === '1'){
                                $resParamArr[] = '\'\''; 
                            }else{
                                $resParamArr[] = ""; 
                            }
                        }else{
                            $resParamArr[] =  '\''.db2_escape_string(bindWithLikeOptr($pdata['CNCKBN'],$pdata['CNDDAT'])).'\'';
                        }
                    }
                    break;
                case '2';
                    if($pdata['CNDKBN'] === '1'){
                        if($pdata['CNDDAT'] === '' && $pdata['CNDSTKB'] === '1'){
                            $withParamArr[$pdata['CNDWNM']] = 0;
                        }else{
                            $withParamArr[$pdata['CNDWNM']] =  $pdata['CNDDAT'];
                        }
                    }else{
                        if($pdata['CNDDAT'] === '' && $pdata['CNDSTKB'] === '1'){
                            $resParamArr[] = 0; 
                        }else{
                            $resParamArr[] =  bindWithLikeOptr($pdata['CNCKBN'],$pdata['CNDDAT']);
                        }
                    }
                    break;
                case '3':
                    if($pdata['CNDKBN'] === '1'){
                        if($pdata['CNDDAT'] === ''){
                            if($pdata['CNDSTKB'] === '1'){
                                $withParamArr[$pdata['CNDWNM']] = '\''."0000/00/00".'\'';
                            }else{
                                $withParamArr[$pdata['CNDWNM']] = "";                            
                            }
                        }else{
                            $withParamArr[$pdata['CNDWNM']] =  '\''.db2_escape_string($pdata['CNDDAT']).'\'';
                        }
                    }else{
                        if($pdata['CNDDAT'] === ''){
                            if($pdata['CNDSTKB'] === '1'){
                                $resParamArr[] = '\''."0000/00/00".'\'';
                            }else{
                                $resParamArr[] = ""; 
                            }
                        }else{
                            $resParamArr[] =  '\''.db2_escape_string(bindWithLikeOptr($pdata['CNCKBN'],$pdata['CNDDAT'])).'\'';
                        }
                    }
                    break;
                case '4':
                    if($pdata['CNDKBN'] === '1'){
                        if($pdata['CNDDAT'] === ''){
                            if($pdata['CNDSTKB'] === '1'){
                                $withParamArr[$pdata['CNDWNM']] = '\''.'00.00.00'.'\'';
                            }else{
                                $withParamArr[$pdata['CNDWNM']] = "";                            
                            }
                        }else{
                            $withParamArr[$pdata['CNDWNM']] =  '\''.db2_escape_string($pdata['CNDDAT']).'\'';
                        }
                    }else{
                        if($pdata['CNDDAT'] === ''){
                            if($pdata['CNDSTKB'] === '1'){
                                $resParamArr[] =  '\''.'00.00.00'.'\'';
                            }else{
                                $resParamArr[] = ""; 
                            }
                        }else{
                            $resParamArr[] =  '\''.db2_escape_string(bindWithLikeOptr($pdata['CNCKBN'],$pdata['CNDDAT'])).'\'';
                        }
                    }
                    break;
                case '5':
                    if($pdata['CNDKBN'] === '1'){                       
                        if($pdata['CNDDAT'] === ''){
                            if($pdata['CNDSTKB'] === '1'){
                                $withParamArr[$pdata['CNDWNM']] = '\''.'0000-00-00-00.00.00.000000'.'\'';
                            }else{
                                $withParamArr[$pdata['CNDWNM']] = "";
                            }
                        }else{
                            $withParamArr[$pdata['CNDWNM']] =  '\''.db2_escape_string($pdata['CNDDAT']).'\'';
                        }
                    }else{                      
                        if($pdata['CNDDAT'] === ''){
                            if($pdata['CNDSTKB'] === '1'){
                                $resParamArr[] = '\''.'0000-00-00-00.00.00.000000'.'\'';
                            }else{
                                $resParamArr[] = ""; 
                            }
                        }else{
                            $resParamArr[] =  '\''.db2_escape_string(bindWithLikeOptr($pdata['CNCKBN'],$pdata['CNDDAT'])).'\'';
                        }
                    }
                    break;
                case '6':
                    if($pdata['CNDKBN'] === '1'){
                        $withParamArr[$pdata['CNDWNM']] =  $pdata['CNDDAT'];
                    }else{
                        $resParamArr[] =  bindWithLikeOptr($pdata['CNCKBN'],$pdata['CNDDAT']);
                    }
                    break;
            }
            error_log("HMM HMM HMM".print_r($resParamArr,true));
        }
    }
    $rtnarr = array(
            'PARAMDATA' => $resParamArr,
            'WITHPARAMDATA' => $withParamArr
        );
    //error_log('RETURN DATA ARR:'.print_r($rtnarr,true));
    return $rtnarr;
}
/**
*
*/
function bindWithLikeOptr ($pos,$value) {
    switch ($pos) {
        case 7:
            $value = "%".$value."%";
            break;
        case 8:
            $value = $value."%";
            break;
        case 9:
            $value = "%".$value;
            break;
        default:
            $value = $value;
            break;
    }
    return $value;
}
/**
 * 定義の作成したSQL文で一時テーブル作成
 * @param $db2con ：データベース接続
 * @param $db2tblcon : RDBデータベース接続
 * @param $RDBNM : RDBデータベース
 * @param $libList ：ライブラリリスト
 * @param $strSQL : 実行SQL
 * @param $params : 実行パラメータ
 * @param $tmpTblNm ：一時テーブル名
 * @戻り値：一時テーブル名
 */
function createSQLQryTblRDB($db2con,$db2tblcon, $RDBNM,$libList, $strSQL , $params, $tmpTblNm){
    $rtn = 0;
    $msg = '';
    //$tmpTblNm = '@MMMMMMMMM';
    //cmSetLIB($db2con,$libList);
    $data = array();
    $selTblSQL  = '';
    $selTblSQL .=  '   SELECT ';
    $selTblSQL .=  '          A.SYSTEM_TABLE_NAME TABLE_NAME  ';
    $selTblSQL .=  '        , A.TABLE_TEXT ';                       
    $selTblSQL .=  '        , A.SYSTEM_TABLE_SCHEMA AS TAB_SCHEMA  ';
    $selTblSQL .=  '        , A.TABLE_TYPE  ';
    $selTblSQL .=  '   FROM QSYS2/SYSTABLES A  ';
    //$selTblSQL .=  '      , QSYS2/SYSSCHEMAS B  ';
    $selTblSQL .=  '      ,'.SYSSCHEMASLIB.'/SYSSCHEMAS B  ';
    $selTblSQL .=  '       WHERE A.SYSTEM_TABLE_SCHEMA = B.SYSTEM_SCHEMA_NAME  ';
    $selTblSQL .=  '       AND A.FILE_TYPE <> \'S\'  ';
    $selTblSQL .=  '       AND A.TABLE_TYPE = \'T\'  ';
    $selTblSQL .=  '       AND A.SYSTEM_TABLE_SCHEMA <> \'\'  ';
    $selTblSQL .=  '       AND A.TABLE_SCHEMA = ? '; 
    $selTblSQL .=  '       AND A.TABLE_NAME = ?  ';
    $stmt = db2_prepare($db2tblcon,$selTblSQL);
    if($stmt === false){
        $rtn  = 'FAIL_SEL';
        $msg  = showMsg('FAIL_FUNC',array('クエリー実行')).db2_stmt_errormsg();//'クエリー実行失敗しました。'.db2_stmt_errormsg();
    }else{
        $param = array(SAVE_DB,$tmpTblNm);
        $r = db2_execute($stmt,$param);
        if($r === false){
            $rtn = 'FAIL_SEL';
            $msg = showMsg('FAIL_FUNC',array('クエリー実行')).db2_stmt_errormsg();//'クエリー実行失敗しました。'.db2_stmt_errormsg();
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)>0){
                $delTblSQL = ' DROP TABLE '.SAVE_DB.'/'.$tmpTblNm;
                $stmt = db2_prepare($db2tblcon,$delTblSQL);  
                if($stmt === false){
                    $rtn  = 'FAIL_DEL';
                    $msg  = showMsg('FAIL_FUNC',array('クエリー実行')).db2_stmt_errormsg();//'クエリー実行失敗しました。'.db2_stmt_errormsg();
                }else{
                    $param = array();
                    $r = db2_execute($stmt,$param);
                    if($r === false){
                        $rtn = 'FAIL_DEL';
                        $msg = showMsg('FAIL_FUNC',array('クエリー実行')).db2_stmt_errormsg();//'クエリー実行失敗しました。'.db2_stmt_errormsg();
                    }else{
                        e_log('一時テーブルに重複があるので削除完了','1');
                    }
                }

            }
        }
    }
    //クエリーはクエリー最大件数以上の場合　190315
    $msgmax = '';
    $resExec = execSQLQry($db2tblcon, $strSQL, $params); 

    //クエリー最大件数を取得
    if ($rtn === 0) {
        //クエリー最大件数が0以上の場合、FETCH FIRSTで件数を制限する
        if(CND_MAXQRY > 0){
            if(count($resExec['RESULTDATA']) > CND_MAXQRY){
                $msgmax = showMsg('クエリーの結果がシステム値の最大件数より多いため、処理を中断しました。</br> '.CND_MAXQRY.'件のみ表示されます。');
            }
            $strSQL .= ' FETCH FIRST '.CND_MAXQRY.' ROWS ONLY ';
        }

    }
    if($rtn === 0){
        $rtnExeTMchk = comSetQryExecTimeMemory($db2tblcon);
        e_log('reach it SQL実行時間設定');
    }
    if($rtn === 0){
        $strCreate  = '';
        $strCreate .=   ' CREATE TABLE';
        $strCreate .=   ' '.SAVE_DB.'/'.$tmpTblNm.' AS';
        $strCreate .=   ' ('.$strSQL.' )';
        $strCreate .=   ' WITH DATA ';
        if($rtn === 0){
            // 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
            if(strlen($strCreate) > 32767 ){
                $rtn = 1;
                $msg = showMsg('FAIL_BASE_LENLIMIT');//'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
            }       
        }
        $stmt = db2_prepare($db2tblcon,$strCreate);
        if($stmt === false){
            $rtn  = 'FAIL_SEL';
            $msg = showMsg('FAIL_FUNC',array('クエリー実行')).db2_stmt_errormsg();//'クエリー実行失敗しました。'.db2_stmt_errormsg();
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rtn = 'FAIL_SEL';
                $msg = showMsg('FAIL_FUNC',array('クエリー実行')).db2_stmt_errormsg();//'クエリー実行失敗しました。'.db2_stmt_errormsg();
                e_log('エラーコード：'.db2_conn_error());
                if(strpos(db2_stmt_errormsg(),'SQLSTATE=57005 SQLCODE=-666') !== false){
                    $msg = $msg. '<br/> 制限を増やす場合、管理者メニューのシステム設定から【クエリー実行制限】を編集してください。';
                }
            }else{
                $msg = 'ワークテーブル作成完了'.$tmpTblNm;
            }
        }
    }
    if($rtn === 0){
        $rtnExeTMchk = comResetQryExecTimeMemory($db2tblcon);
    }
    $rtnCreateTbl = array();
    $rtnCreateTbl['RTN'] = $rtn;
    $rtnCreateTbl['MSG'] = $msg;
    $rtnCreateTbl['MSGMAX'] = $msgmax;
    $rtnCreateTbl['TMPTBLNM'] = $tmpTblNm;
    $rtnCreateTbl['QRYCNT'] = count($resExec['RESULTDATA']);
    return $rtnCreateTbl;
}
// SQLクエリーデータ取得
function getQrySQLData($db2con,$QRYNM){
    $rtn        = 0;
    $msg        = '';
    $fdb2csv1   = array();
    $fdb2csv2   = array();
    $bsqlcnd    = array();
    $sqldat     = array();
    if($rtn === 0){
        $res = getFDB2CSV1($db2con,$QRYNM);
        if($res['result'] === 'NOTEXIST_GET'){
             $rtn = 3;
             $msg = showMsg($res['result'],array('クエリー'));
        }else if($res['result'] !== true){
             $rtn = 1;
             $msg = showMsg($res['result'],array('クエリー'));
        }else{
            $fdb2csv1 = $res['data'];
        }
    }
    if($rtn === 0){
        $res = getFDB2CSV2($db2con,$QRYNM);
        if($res['result'] !== true){
             $rtn = 1;
             $msg = showMsg($res['result'],array('クエリー'));
        }else{
            $fdb2csv2 = $res['data'];
        }
    }
    if($rtn === 0){
        $res = getBSQLCND($db2con,$QRYNM);
        if($res['result'] !== true){
             $rtn = 1;
             $msg = showMsg($res['result'],array('クエリー'));
        }else{
            $bsqlcnd = $res['data'];
        }
    }
    if($rtn === 0){
        $res = getBSQLDAT($db2con,$QRYNM);
        if($res['result'] !== true){
             $rtn = 1;
             $msg = showMsg($res['result'],array('クエリー'));
        }else{
            $bsqldat = $res['data'];
            $bsqlExeDat = $res['SQLDATA'];
        }
    }
    $qryData = array(
            'FDB2CSV1'       => $fdb2csv1 ,
            'FDB2CSV2'       => $fdb2csv2 ,
            'BSQLCND'        => $bsqlcnd ,
            'BSQLDAT'        => $bsqldat ,
            'BSQLEXEDAT'     => $bsqlExeDat
        );
    $rtnArr = array(
         'RTN'        => $rtn ,
         'MSG'        => $msg ,
         'QRYDATA'    => $qryData
    );
    return $rtnArr;
}
