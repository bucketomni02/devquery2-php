<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : クエリの実行、作成、編集履歴取得、更新、挿入のファンクション
 * PROGRAM ID     : fnDB2QHIS.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/11/08
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
//include_once("../common/inc/config.php");
//include_once("../common/inc/common.inc.php");

function fnDB2QHISExe($db2con,$QRYNM,$DQUUSR){
    $rtn = 0;
    $msg = '';
    if($rtn === 0){
        $rs = fnGetDB2QHIS($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            if(count($rs['data'])===0){
                $rs = fnInsDB2QHIS($db2con,$QRYNM,'');
                if($rs['result']!== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }
            }
        }
    }
    if($rtn === 0){
        $rs = fnUpdDB2QHISExe($db2con,$QRYNM,$DQUUSR);
        if($rs['result']!== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
    $rtnArr = array('RTN' => rtn,'MSG' => $msg);
    return $rtnArr;
}

/**
 *---------------------------------------------------------------------------
 * クエリーごとに履歴情報取得
 *---------------------------------------------------------------------------
 **/
function fnGetDB2QHIS($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT A.* ';
    $strSQL .=  '   FROM DB2QHIS A';
    $strSQL .=  '   WHERE A.DQNAME = ?';

    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,true);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------*
* クエリ編集の場合
* クエリの更新者、更新日と更新時刻情報登録
*-------------------------------------------------------*
*/

function fnUpdDB2QHISEdt($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL  = ' UPDATE DB2QHIS ';
    $strSQL .= ' SET ';
    $strSQL .= ' DQUUSR = ?, ';
    $strSQL .= ' DQUDAY = ?, ';
    $strSQL .= ' DQUTM = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' DQNAME = ? ';
    $params = array(
                $_SESSION['PHPQUERY']['user'][0]['WUUID'],
                date('Ymd'),
                date('His'),
                $QRYNM
            );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_UPD');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_UPD');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------------------------*
* クエリ実行の場合
* クエリの実行回数、最終実行者、最終実行日と最終実行時刻情報登録
*-------------------------------------------------------------------------*
*/

function fnUpdDB2QHISExe($db2con,$QRYNM,$DQUUSR){ 
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL  = ' UPDATE DB2QHIS ';
    $strSQL .= ' SET ';
    $strSQL .= ' DQEKAI = INTEGER(DQEKAI+1), ';
    $strSQL .= ' DQEUSR = ?, ';
    $strSQL .= ' DQEDAY = ?, ';
    $strSQL .= ' DQETM = ?  ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' DQNAME = ? ';
    $params = array(
                $DQUUSR,
                date('Ymd'),
                date('His'),
                $QRYNM                
            );
    $stmt = db2_prepare($db2con,$strSQL);
    
    if($stmt === false){
        $data = array('result' => 'FAIL_UPD');
    }else{
        
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_UPD');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* クエリの作成者、作成日と作成時刻情報登録
*-------------------------------------------------------*
*/

function fnInsDB2QHIS($db2con,$QRYNM,$DQUSR){ 
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL  = ' INSERT INTO DB2QHIS ';
    $strSQL .= ' ( ';
    $strSQL .= ' DQNAME, ';
    $strSQL .= ' DQCUSR, ';
    $strSQL .= ' DQCDAY, ';
    $strSQL .= ' DQCTM  ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?,?,?) ';

    if($DQUSR === ''){
        $params = array(
                    $QRYNM,
                    '',
                    '',
                    ''
                );
    }else{
        $params = array(
                    $QRYNM,
                    $DQUSR,
                    date('Ymd'),
                    date('His')
                    );
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_INS');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_INS');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}

function fnDelDB2QHIS($db2con,$QRYNM){ 
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '   FROM DB2QHIS ';
    $strSQL .=  '   WHERE DQNAME = ? ';

    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_DEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_DEL');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}

function fnUpdDB2QHISReset($db2con,$QRYNM){//MSM add for クエリーの実行情報を初期化 20190405 
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL  = ' UPDATE DB2QHIS ';
    $strSQL .= ' SET ';
    $strSQL .= ' DQEKAI = ?, ';
    $strSQL .= ' DQEUSR = ?, ';
    $strSQL .= ' DQEDAY = ?, ';
    $strSQL .= ' DQETM = ?, ';
    $strSQL .= ' DQTMAX = ?, ';
    $strSQL .= ' DQTMIN = ?, ';
    $strSQL .= ' DQTAVG = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' DQNAME = ? ';
    $params = array(
                0,
                "",
                "",
                "",
                0,0,0,
                $QRYNM
            );
    //error_log("fnUpdDB2QHISReset => ".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_UPD');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_UPD');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*1.DQEKAI => 0
2.DQEUSR => 
3.DQEDAY => 
4.DQETM  => 

5.DQTMAX => 0
6.DQTMIN => 0
7.DQTAVG => 0*/