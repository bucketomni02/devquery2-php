<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/


$data = array();
$rtn = 0;
$msg = '';
/*$fdb2csv1 = array();
$breftbl  = array();
$breffld  = array();
$fdb2csv2 = array();
$fdb2csv5 = array();
$bsumfld  = array();
$bqrycnd  = array();
$bcnddat  = array();*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
// ライブラリー選択画面とファイル選択画面からファイル情報取得
function generateDBFileTabData($BASEFILEDATA,$BASELIBFRMDATA){
    //【FDB2CSV1】テーブルのデータ取得
    $fdb2csv1 = generateFDB2CSV1($BASEFILEDATA,$BASELIBFRMDATA);
    //【BREFTBL、BREFFLD】テーブルのデータ取得

	e_log("before generateDBFileTabData :".print_r($BASEFILEDATA,true));
    $breftblfld = generateBREFTBLFLD($BASEFILEDATA);
	e_log("after generateDBFileTabData :".print_r($BASEFILEDATA,true));
    
    $rtnFile = array(
        'FDB2CSV1'  => $fdb2csv1,
        'BREFTBLFLD'=> $breftblfld
    );
    return $rtnFile;
}
// フィールド選択画面からフィールド情報取得
function generateDBFieldTabData($BASEFIELDDATA,$SUMMARYDATA){
    //【FDB2CSV2とFDB2CSV5】のデータ取得
    $fdb2csv25 = generateFDB2CSV2AND5($BASEFIELDDATA);
    
    //$bsumfld = generateBSUMFLD($SUMMARYDATA);
    //【BSUMFLD】のデータ取得
    $resulSummary = generateBSUMFLD($SUMMARYDATA);

    $rtnField = array(
        'FDB2CSV2'    => $fdb2csv25['FDB2CSV2'],
        'FDB2CSV5'    => $fdb2csv25['FDB2CSV5'],
        'BSUMFLD'     => $resulSummary['BSUMFLD'],
        'UPDFDB2CSV2' => $resulSummary['UPDFDB2CSV2'],
        'UPDFDB2CSV5' => $resulSummary['UPDFDB2CSV5']
    );
    return $rtnField;
    
}
//条件選択画面から条件情報取得
function generateDBCondTabData($BASECONDDATA){
    //【BQRYCNDとBCNDDAT】データ取得
    $bqrycndData = generateBQRYCNDDAT($BASECONDDATA);

    $rtnCond = array(
        'BQRYCNDDATA'    => $bqrycndData
    );
    return $rtnCond;
}
// FDB2CSV1データ取得
function generateFDB2CSV1($BASEFILEDATA,$BASELIBFRMDATA){
    if($BASEFILEDATA['D1FILLIB'] === '*USRLIBL'){
        $BASEFILEDATA['D1FILLIB'] = '';
    }
    $fdb2csv1data = array();
    $fdb2csv1data['D1NAME']   = $BASEFILEDATA['D1NAME'];
    $fdb2csv1data['D1FILE']   = $BASEFILEDATA['D1FILE'];
    $fdb2csv1data['D1FILLIB'] = $BASEFILEDATA['D1FILLIB'];
    $fdb2csv1data['D1FILMBR'] = $BASEFILEDATA['D1FILMBR'];
    $fdb2csv1data['D1TEXT']   = $BASEFILEDATA['D1TEXT'];
    for($libCnt = 0; $libCnt<count($BASELIBFRMDATA); $libCnt++){
        $BASELIBFRMDATA[$libCnt] = str_pad($BASELIBFRMDATA[$libCnt], 10, " ", STR_PAD_RIGHT);
    }
    $fdb2csv1data['D1CSID']   = '5035';
    $fdb2csv1data['D1RDB']    = $BASEFILEDATA['RDBNM'];
    $fdb2csv1data['D1LIBL']   = join(' ',$BASELIBFRMDATA);
    $fdb2csv1data['D1WEBF']   = '1';
    return $fdb2csv1data;
}
//【BREFTBL、BREFFLD】テーブルのデータ取得
function generateBREFTBLFLD($BASEFILEDATA){
    e_log("【BREFTBL、BREFFLD】テーブルのデータ取得:".print_r($BASEFILEDATA,true));
    $BASEREFFILE = $BASEFILEDATA['REFFILE'];
    $breftbldata = array();
    foreach($BASEREFFILE as $REFFILE){
        if($REFFILE['RTRLIB'] === '*USRLIBL'){
            $REFFILE['RTRLIB'] = '';
        }
        $breftblInfo = array();
        $breftblInfo['RTQRYN'] = $BASEFILEDATA['D1NAME'];
        $breftblInfo['RTRSEQ'] = $REFFILE['REFIDX'];
        $breftblInfo['RTRFIL'] = $REFFILE['RTRFIL'];
        $breftblInfo['RTRMBR'] = $REFFILE['RTRMBR'];
        $breftblInfo['RTRLIB'] = $REFFILE['RTRLIB'];
        $breftblInfo['RTDESC'] = $REFFILE['RTEXT'];
        $breftblInfo['RTJTYP'] = $REFFILE['JOINTYP'];
        $breftblInfo['RTJTYP'] = $REFFILE['JOINTYP'];

        $breffldData = array();
        foreach($REFFILE['REFFIELDARR'] as $k => $val){
            if(subStr($k,1,3) === 'LEN'){
                unset($REFFILE['REFFIELDARR'][$k]);
            }else if(subStr($k,1,3) === 'TYP'){
                unset($REFFILE['REFFIELDARR'][$k]);
            }
        }
        for($fldcnt = 0; $fldcnt< (count($REFFILE['REFFIELDARR'])/4);$fldcnt++){
            $breffldInfo = array();
            if(($REFFILE['REFFIELDARR']['PKEY'.($fldcnt+1)] !== '') && 
               ($REFFILE['REFFIELDARR']['RKEY'.($fldcnt+1)] !== '') &&
			   ($REFFILE['REFFIELDARR']['CFLG'.($fldcnt+1)] !== '') ){

                $breffldInfo['RFQRYN'] = $BASEFILEDATA['D1NAME'];
                $breffldInfo['RFRSEQ'] = $REFFILE['REFIDX'];
                $breffldInfo['RFFSEQ'] = $fldcnt+1;
                $PfldDataArr = explode('.', $REFFILE['REFFIELDARR']['PKEY'.($fldcnt+1)]);
                if($PfldDataArr[0] === 'P'){
                    $PfldDataArr[0] = 0;
                }else{
                    $PfldDataArr[0] = substr($PfldDataArr[0], 1);
                }
                $breffldInfo['RFPFID'] = $PfldDataArr[0];
                $breffldInfo['RFPFNM'] = $PfldDataArr[1];
	            $RfldDataArr = explode('.', $REFFILE['REFFIELDARR']['RKEY'.($fldcnt+1)]);

				if($REFFILE['REFFIELDARR']['CFLG'.($fldcnt+1)] == '1'){
					$breffldInfo['RFRFID'] = '0';
                	$breffldInfo['RFRFNM'] = $REFFILE['REFFIELDARR']['RKEY'.($fldcnt+1)];
				}
				else{
	                $RfldDataArr[0] = substr($RfldDataArr[0], 1);
					$breffldInfo['RFRFID'] = $RfldDataArr[0];
	                $breffldInfo['RFRFNM'] = $RfldDataArr[1];
				}
                
                $breffldInfo['RFRKBN'] = $REFFILE['REFFIELDARR']['JCOND'.($fldcnt+1)];
                $breffldInfo['RFTEISU'] = $REFFILE['REFFIELDARR']['CFLG'.($fldcnt+1)];

                $breffldData[] = $breffldInfo;
            }
        }
		e_log(" after 【BREFTBL、BREFFLD】テーブルのデータ取得:".print_r($breffldData,true));
        $breftblInfo['BREFFLD'] = $breffldData;
        $breftbldata[] = $breftblInfo;
    }
    return $breftbldata;
}
//【FDB2CSV2,FDB2CSV5】テーブルのデータ取得
function generateFDB2CSV2AND5($BASEFIELDDATA){
    $fdb2csv2data = array();
    foreach($BASEFIELDDATA['BASEFIELDDATA'] as $FieldData){
        if($FieldData['D2FDESC'] !== '結果フィールド'){
            foreach($FieldData['STRDATA'] as $fieldInfo){
                $fdb2csv2Info = array(); 
                $fdb2csv2Info['D2NAME']  = $BASEFIELDDATA['D2NAME'];
                $fdb2csv2Info['D2FILID'] = $FieldData['D2FILID'];
                $fdb2csv2Info['D2FLD']   = $fieldInfo['COLUMN_NAME'];
                $fdb2csv2Info['D2HED']   = $fieldInfo['COLUMN_HEADING'];
                if($fieldInfo['SEQ'] === ''){
                    $fieldInfo['SEQ'] = 0;
                }
                $fdb2csv2Info['D2CSEQ']  = $fieldInfo['SEQ'];
                if($fieldInfo['SORTNO'] === ''){
                    $fieldInfo['SORTNO'] = 0;
                }
                $fdb2csv2Info['D2RSEQ']  = $fieldInfo['SORTNO'];
                $fdb2csv2Info['D2RSTP']  = $fieldInfo['SORTTYP'];
                $fdb2csv2Info['D2WEDT']  = $fieldInfo['EDTCD'];
                $fdb2csv2Info['D2TYPE']  = $fieldInfo['DDS_TYPE'];
                $fdb2csv2Info['D2LEN']   = $fieldInfo['LENGTH'];
                if($fieldInfo['NUMERIC_SCALE'] === ''){
                    $fieldInfo['NUMERIC_SCALE'] = 0;
                }
                $fdb2csv2Info['D2DEC']   = $fieldInfo['NUMERIC_SCALE'];
                $fdb2csv2data[] = $fdb2csv2Info;
            }
        }else{
            $fdb2csv5data = array();
            if(count($FieldData['STRDATA'])>0){
                foreach($FieldData['STRDATA'] as $fieldInfo){
                    $fdb2csv5info = array();
                    $fdb2csv5info['D5NAME']  = $BASEFIELDDATA['D2NAME'];
                    $fdb2csv5info['D5ASEQ']  = $fieldInfo['ORDINAL_POSITION'];
                    $fdb2csv5info['D5FLD']   = $fieldInfo['COLUMN_NAME'];
                    $fdb2csv5info['D5HED']   = $fieldInfo['COLUMN_HEADING'];
                    if($fieldInfo['SEQ'] === ''){
                        $fieldInfo['SEQ'] = 0;
                    }
                    $fdb2csv5info['D5CSEQ']  = $fieldInfo['SEQ'];
                    if($fieldInfo['SORTNO'] === ''){
                        $fieldInfo['SORTNO'] = 0;
                    }
                    $fdb2csv5info['D5RSEQ']  = $fieldInfo['SORTNO'];
                    $fdb2csv5info['D5RSTP']  = $fieldInfo['SORTTYP'];
                    $fdb2csv5info['D5WEDT']  = $fieldInfo['EDTCD'];
                    $fdb2csv5info['D5TYPE']  = $fieldInfo['DDS_TYPE'];
                    $fdb2csv5info['D5LEN']   = $fieldInfo['LENGTH'];
                    $fdb2csv5info['D5EXP']   = $fieldInfo['FORMULA'];
                    $fdb2csv5info['D5EXPD']  = $fieldInfo['FORMULA_D'];//MSM add
                    if($fieldInfo['NUMERIC_SCALE'] === ''){
                        $fieldInfo['NUMERIC_SCALE'] = 0;
                    }
                    $fdb2csv5info['D5DEC']   = $fieldInfo['NUMERIC_SCALE'];
                    $fdb2csv5data[] = $fdb2csv5info;
                    
                    /*//MSM add for formula change that are used in another new field 
                    $chk_FORMULA = prepareFORMULA($fieldInfo['FORMULA']);//
                    foreach($FieldData['STRDATA'] as $fieldChk){
                        if($chk_FORMULA === $fieldChk['COLUMN_NAME']){
                            //e_log("D5EXP MSM1=> ".$chk_FORMULA.", COLUMN_NAME  => ".$fieldChk['COLUMN_NAME']);
                            $FORMULA_D = $fieldChk['FORMULA_D'];
                            $str= strpbrk($fieldInfo['FORMULA'], '+|-|*|/');
                            if($str != ""){
                               $fdb2csv5info['D5EXPD'] = '('.$FORMULA_D.')'.$str; 
                            }else{
                                $fdb2csv5info['D5EXPD'] = $FORMULA_D;
                            }
                        }
                    }
                
                    $fdb2csv5data[] = $fdb2csv5info;
                    //e_log("fdb2csv5data MSM *** => ".print_r($fdb2csv5data,true));

                    $chk_FORMULA1 = prepareFORMULA($fdb2csv5info['D5EXP']);
                    
                    foreach($fdb2csv5data as $key => $value){
                        
                        if($chk_FORMULA1 === $value['D5FLD']){//

                            $FORMULA_D = $value['D5EXPD'];//value
                            $D5ASEQ = $fdb2csv5info['D5ASEQ']-1;
                            $k = current($fdb2csv5data);
                            
                            $str= strpbrk($fdb2csv5info['D5EXP'], '+|-|*|/');//fdb2csv5info
                            if($str != ""){
                                $fdb2csv5data[$D5ASEQ]['D5EXPD'] = '('.$FORMULA_D.')'.$str; //$key+1
                            }else{
                                $fdb2csv5data[$D5ASEQ]['D5EXPD'] = $FORMULA_D;
                            }
                            
                            //e_log("fdb2csv5data1['D5EXPD'] MSM2 => ".$fdb2csv5data1['D5EXPD']);
                        }
                        else{
                    
                            $fdb2csv5data[$key]['D5EXPD'] = $fdb2csv5data[$key]['D5EXPD'];
                        }
                    }//MSM add end */
                    
                }
            }
        }
    }
    $rtnArr= array(
        'FDB2CSV2' => $fdb2csv2data,
        'FDB2CSV5' => $fdb2csv5data
    );
    return $rtnArr;
}
//【BSUMFLD】テーブルのデータ取得
function generateBSUMFLD($SUMMARYDATA){
    $bsumfldData  = array();
    $updFdb2csv2  = array();
    $updFdb2csv5  = array();
    foreach($SUMMARYDATA as $summaryData){
        if($summaryData['KEYFLG'] === 'S'){
            //更新FDB2CSV2Or 5
            if($summaryData['FILTYP'] === 'K'){
                $fdb2csv5data = array();
                $fdb2csv5data['D5NAME'] = $summaryData['QRYNM'];
                $fdb2csv5data['D5FLD']  = $summaryData['FLDNM'];
                $fdb2csv5data['D5ASEQ'] = $summaryData['ORPOS'];
                $fdb2csv5data['D5GSEQ'] = $summaryData['SEQ'];
                $fdb2csv5data['D5GMES'] = $summaryData['GMES'];
                $updFdb2csv5[] = $fdb2csv5data;
            }else{
                $fdb2csv2data = array();
                $fdb2csv2data['D2NAME']   = $summaryData['QRYNM'];
                $fdb2csv2data['D2FILID']  = $summaryData['FILID'];
                $fdb2csv2data['D2FLD']    = $summaryData['FLDNM'];
                $fdb2csv2data['D2GSEQ']   = $summaryData['SEQ'];
                $fdb2csv2data['D2GMES']   = $summaryData['GMES'];
                $updFdb2csv2[] = $fdb2csv2data;

            } 
        }else if($summaryData['KEYFLG'] === 'G'){
            $bsumfldInfo = array();
            $bsumfldInfo['SFQRYN']  = $summaryData['QRYNM'];
            if($summaryData['FILTYP'] === 'K'){
                $bsumfldInfo['SFFILID']  = 9999;
            }else{
                $bsumfldInfo['SFFILID']  = $summaryData['FILID'];
            }
            $bsumfldInfo['SFFLDNM'] = $summaryData['FLDNM'];
            $bsumfldInfo['SFSEQ']   = $summaryData['SEQ'];
            $bsumfldInfo['SFGMES']  = $summaryData['GMES'];
            $bsumfldData[] = $bsumfldInfo;
        }
    }
    $rtnarr = array(
            'UPDFDB2CSV2'=> $updFdb2csv2,
            'UPDFDB2CSV5'=> $updFdb2csv5,
            'BSUMFLD'    => $bsumfldData
        );
    return $rtnarr;
}
//【BQRYCND,BCNDDAT】テーブルのデータ取得
function generateBQRYCNDDAT($BASECONDDATA){
    error_log("SuperManData++++".print_r($BASECONDDATA,true));
    $bqrycndData = array();
    foreach($BASECONDDATA as $baseCond){
        $bqrycndInfo = array();
        $bqrycndInfo['CNQRYN']  = $baseCond['QRYN'];
        $bqrycndInfo['FMSEQ']   = $baseCond['FMSEQ'];
        $bqrycndInfo['FSSEQ']   = $baseCond['FSSEQ'];
        $bqrycndInfo['CNFILID'] = $baseCond['CNFILID'];
        $bqrycndInfo['CNMSEQ']  = $baseCond['CNMSEQ'];
        $bqrycndInfo['CNSSEQ']  = $baseCond['CNSSEQ'];
        $bqrycndInfo['CNAOKB']  = $baseCond['CNAOKB'];
        $bqrycndInfo['CNFLDN']  = $baseCond['CNFLDN'];
        $bqrycndInfo['CNCKBN']  = $baseCond['CNCKBN'];
        $bqrycndInfo['CNSTKB']  = $baseCond['CNSTKB'];
        $bqrycndInfo['CNDUPLOW']  = $baseCond['CNDUPLOW'];//PPN add for new field data 20181105
        $bcnddatData = array();
        $dataCnt = 0;
        foreach($baseCond['CNDDAT'] as $value){
            $bcnddatinfo = array();
            $dataCnt = $dataCnt+1;
            $bcnddatinfo['CDQRYN']  = $baseCond['QRYN'];
            $bcnddatinfo['CDFILID'] = $baseCond['CNFILID'];
            $bcnddatinfo['CDMSEQ']  = $baseCond['CNMSEQ'];
            $bcnddatinfo['CDSSEQ']  = $baseCond['CNSSEQ'];
            $bcnddatinfo['CDCDCD']  = $dataCnt;
            $bcnddatinfo['CDDAT']   = $value['DAT'];
            $bcnddatinfo['CDBCNT']  = $value['BCNT'];
            $bcnddatinfo['CDFFLG']  = (int)$value['FFLG'];
            $bcnddatData[] = $bcnddatinfo;
        }
        $bqrycndInfo['BCNDDAT'] = $bcnddatData;
        $bqrycndData[] = $bqrycndInfo;
    }
    return $bqrycndData;
}
// グラフ用一時テーブル削除ため取得
function fnGetDB2GPKTMPTBL($db2con,$QRYNM){
    $params = array();
    $data   = array();
    $strSQL = '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     GPKQRY, ';
    $strSQL .= '     GPKID, ';
    $strSQL .= '     GPKTBL ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2GPK ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     GPKQRY = ? ';
    $strSQL .= ' AND GPKTBL <> \'\' ';

    $params = array($QRYNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2GPK:'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'DB2GPK:'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
// グラフ用一時テーブル削除
function dropGphTmpTbl($db2con,$gphVal){
    $rs = cmDropQueryTable($db2con,$gphVal['GPKTBL']);
    if($rs['result'] !== true){
        e_log('テーブル削除失敗。'.$gphVal['GPKTBL']);
     }else{
        $textFileName =  BASE_DIR . "/php/graphfile/".$gphVal['GPKQRY'].'-'.$gphVal['GPKID'].'.txt';
        if (file_exists($textFileName)) {
            @unlink($textFileName);
        }
    }
}

function prepareFORMULA($chk_FORMULA){//$fieldInfo['FORMULA']
    //$chk_FORMULA = $fieldInfo['FORMULA'];
    if (strpos($chk_FORMULA, 'P.') !== false || strpos($chk_FORMULA, 'K.') !== false ) {
        $chk_FORMULA = substr($chk_FORMULA,2);
    }else if(preg_match('/S[0-9.]{2}/', $chk_FORMULA, $matches)){
        $chk_FORMULA = substr($chk_FORMULA,3);
    }
    $str= strpbrk($chk_FORMULA, '+|-|*|/');
    if($str != ""){
        $chk_FORMULA = strstr($chk_FORMULA, $str, true);
    }else{
        $chk_FORMULA = $chk_FORMULA;
    }
    return $chk_FORMULA;
}

