<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/


$data = array();
$rtn = 0;
$msg = '';
/*$fdb2csv1 = array();
$breftbl  = array();
$breffld  = array();
$fdb2csv2 = array();
$fdb2csv5 = array();
$bsumfld  = array();
$bqrycnd  = array();
$bcnddat  = array();*/
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

function generateSQLTblData($BASESQLFRMDATA){
    $BASESQLFRMDATA['NEWD1NAME'] = (isset($BASESQLFRMDATA['NEWD1NAME'])?$BASESQLFRMDATA['NEWD1NAME']:'');
    $fdb2csv1 = createFDB2CSV1Data($BASESQLFRMDATA);
    $bsqldat = createBSQLDATData($BASESQLFRMDATA);
    $rtnSql = array(
        'FDB2CSV1'  => $fdb2csv1,
        'BSQLDAT'=> $bsqldat
    );
    return $rtnSql;
}
function generateCNDTblData($QRYNM,$BASECNDFRMDATA,$withpCnt = 0){
    $bsqlcnd = createBSQLCNDData($QRYNM,$BASECNDFRMDATA,$withpCnt);
    $rtnCnd = array(
        'BSQLCND'  => $bsqlcnd
    );
    return $rtnCnd;
}
function generateFLDTblData($QRYNM,$BASEFLDFRMDATA){
    $fdb2csv2 = createFDB2CSV2Data($QRYNM,$BASEFLDFRMDATA);
    return $fdb2csv2;
}
// FDB2CSV1のデータの生成
function createFDB2CSV1Data($basesqldata){
    $fdb2csv1 = array();
    if($basesqldata['NEWD1NAME'] === ''){
        $fdb2csv1['D1NAME'] = $basesqldata['D1NAME'];
    }else{
        $fdb2csv1['D1NAME'] = $basesqldata['NEWD1NAME'];
    }
    $fdb2csv1['D1TEXT'] = $basesqldata['D1TEXT'];
    $fdb2csv1['D1WEBF'] = '1';
    $fdb2csv1['D1CFLG'] = '1';
    $fdb2csv1['D1RDB']  = $basesqldata['RDBNM'];
    for($libCnt = 0; $libCnt<count($basesqldata['SELLIBLIST']); $libCnt++){
        $basesqldata['SELLIBLIST'][$libCnt] = str_pad($basesqldata['SELLIBLIST'][$libCnt], 10, " ", STR_PAD_RIGHT);
    }
    $fdb2csv1['D1LIBL']   = join(' ',$basesqldata['SELLIBLIST']);
    $fdb2csv1['D1CSID']   = '5035';
    return $fdb2csv1;
}

// BSQLDATのデータを生成
function createBSQLDATData($basesqldata){
    $bsqldat = array();
    $sqldata= array();
    $QRYNM = '';
    if($basesqldata['NEWD1NAME'] === ''){
        $QRYNM  = $basesqldata['D1NAME'];
    }else{
        $QRYNM = $basesqldata['NEWD1NAME'];
    }
    $sqldata['BSQLNM'] = $QRYNM;
    $sqldata['BEXESQL'] = $basesqldata['D1SQL'];
    $sqldata['BSQLFLG'] = '';
    $bsqldat[] = $sqldata;
    $exesql = array();
    $exesql['BSQLNM']  = $QRYNM;
    $exesql['BEXESQL'] = $basesqldata['EXESQL'];
    $exesql['BSQLFLG'] = '1';
    $bsqldat[] = $exesql;
    e_log('SQLDATA:'.print_r($bsqldat,true));
    return $bsqldat;
}

// BSQLCNDのデータを生成
function createBSQLCNDData($QRYNM,$basecnddata,$withpcnt){
    $bsqlcnd = array();
    if(count($basecnddata)>0){
        foreach($basecnddata as $key => $cnddata){
            $sqlcnd = array();
            $sqlcnd['CNQRYN'] = $QRYNM;
            $sqlcnd['CNDSEQ'] = $cnddata['CNDSEQ'];
            $sqlcnd['CNDNM']  = $cnddata['CNDNM'];
            $sqlcnd['CNDWNM']  = $cnddata['CNDWNM'];
            $sqlcnd['CNDDTYP']= $cnddata['CNDDTYP'];
            $sqlcnd['CNDSTKB']= $cnddata['CNDSTKB'];
            $sqlcnd['CNCKBN']= $cnddata['CNCKBN'];
            $sqlcnd['CNDDAT'] = $cnddata['CNDDAT'];
            $sqlcnd['CNDBCNT']= $cnddata['CNDBCNT'];
            if($key<$withpcnt){
                $sqlcnd['CNDKBN']= '1';
            }else{
                $sqlcnd['CNDKBN']= '';
            }
            $bsqlcnd[] = $sqlcnd;
        }
    }
    return $bsqlcnd;
}

// FDB2CSV2のデータを生成
function createFDB2CSV2Data($QRYNM,$baseflddata){
    $fdb2csv2 = array();
    //e_log('gamen FLD Data:'.print_r($baseflddata,true).'cntdata:'.count($baseflddata));
    if(count($baseflddata)>0){

        foreach($baseflddata as $key => $flddata){
            $fdb2csv2data = array();
            $fdb2csv2data['D2NAME']     = $QRYNM;
            $fdb2csv2data['D2FILID']    = 0;
            $fdb2csv2data['D2FLD']      = $flddata['COLUMN_NAME'];
            $fdb2csv2data['D2HED']      = $flddata['COLUMN_HEADING'];
            $fdb2csv2data['D2CSEQ']     = $key+1;
            $fdb2csv2data['D2WEDT']     = $flddata['EDTCD'];
            $fdb2csv2data['D2TYPE']     = $flddata['DDS_TYPE'];
            $fdb2csv2data['D2LEN']      = $flddata['LENGTH'];
            $fdb2csv2data['D2DEC']      = $flddata['NUMERIC_SCALE'] === '' ? 0: $flddata['NUMERIC_SCALE'];
            $fdb2csv2[] = $fdb2csv2data;
        }
    }
    return  $fdb2csv2;
}
