<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$SEARCHDATA = (isset($_POST['SEARCHDATA']))?cmMer($_POST['SEARCHDATA']):'';
$LIBNAME    = (isset($_POST['LIBNAME']))?cmMer($_POST['LIBNAME']):'';
$start      = $_POST['start'];
$length     = $_POST['length'];
$sort       = $_POST['sort'];
$sortDir    = $_POST['sortDir'];


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$LIBSNM = '';
$LIBSDB = '';
$LIBData = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
/**20170525 akz**/
if($rtn === 0){
    $LIBSNM = cmMer($userData[0]['WULIBL']);
}
/**20170525 akz**/

$RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
$filterLibList = array();
if($rtn === 0){
    if($RDBNM !== ''){
        $LIBSDB = $RDBNM;
    }else{
        $LIBSDB = RDB;
    }
}

$qtempArr = array(
                    SCHEMA_NAME => 'QTEMP',
                    SCHEMA_TEXT => '' ,
                    ROWNUM      => 1
            );

if($RDBNM === '' || $RDBNM === RDBNAME || $RDBNM === RDB){
    $db2conRDB = $db2con;
}else{
    $db2conRDB = cmDB2ConRDB();
}
if($rtn === 0){
    if($LIBSNM !== '' ){
        if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
            $resCreate = createQTEMPDB2LBLS($db2conRDB,$LIBSNM);
            if($resCreate !== true){
                $rtn = 1;
                $msg = showMsg($resCreate);
            }
        }
    }
}
if($rtn === 0){
    $rs = fnGetAllCount($db2conRDB,$SEARCHDATA,$LIBNAME,$LIBSNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $allcount = $rs['data'];
        if($SEARCHDATA === '' && $LIBNAME === ''){
            $allcount = $allcount+1;
        }else if($LIBNAME === ''){
            $allcount = $allcount+1;
        }else if((strpos('QTEMP',$SEARCHDATA) === false && strpos('QTEMP',$LIBNAME) === false) === false){
            $allcount = $allcount+1;
        }
    }
}
/*
if($rtn === 0){
    $rs = fnGetSysData($db2conRDB);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $LIBData = $rs['data'][0]['COLBFLG'];
        
    }
}*/
$LIBData = SYSLIBCHK;

if($rtn === 0){
    $rs = fnGetLibData($db2conRDB,$SEARCHDATA,$LIBNAME,$LIBSNM,$LIBData,$start,$length,$sort,$sortDir);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
        if($SEARCHDATA === '' && $LIBNAME === ''){
            array_unshift($data, $qtempArr);
        }else if($LIBNAME === ''){
            //array_unshift($data, $qtempArr);
        }else if((strpos('QTEMP',$SEARCHDATA) === false && strpos('QTEMP',$LIBNAME) === false) === false){
            array_unshift($data, $qtempArr);
        }
    }
}
if(($RDBNM === '' || $RDBNM === RDBNAME || $RDBNM === RDB) === false){
    cmDb2Close($db2conRDB);
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData'        => umEx($data,true),
    'LIBLIST'       => $LIBLIST,
    'RTN'           => $rtn,
    'MSG'           => $msg,
    'wulibl' => $wulibl,
    'libdata' => $LIBData
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ラブラリーマスター取得
*-------------------------------------------------------*
*/

function fnGetLibData($db2con,$SearchData,$LibName,$libsnm = '',$LIBData,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();
    $params = array();
    if(CRTQTEMPTBL_FLG === 1){
        createTmpFil($db2con,SYSSCHEMASLIB,'SYSSCHEMAS','','SYSSCHEMAS');
    }
    $strSQL = ' SELECT A.SCHEMA_NAME ';
    $strSQL .= ' FROM ( SELECT B.SCHEMA_NAME';
    $strSQL .= ' , B.SCHEMA_TEXT';
    $strSQL .= ' , ROWNUMBER() OVER( ';

    if($sort != ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.SCHEMA_NAME ASC ';
    }

    $strSQL .= ' ) as ROWNUM ';
    $strSQL .= ' FROM  ';
    $strSQL .= '    (SELECT ';
    $strSQL .= '        SYSTEM_SCHEMA_NAME SCHEMA_NAME ';
    $strSQL .= '        , SCHEMA_TEXT ';
    $strSQL .= '     FROM ';
    if(CRTQTEMPTBL_FLG === 1){
        e_log('一時テーブル作成フラグ②：'.CRTQTEMPTBL_FLG);
        $strSQL .= '        QTEMP/SYSSCHEMAS ';
    }else{
        $strSQL .= '        '.SYSSCHEMASLIB.'/SYSSCHEMAS ';
    }
    $strSQL .= '     WHERE ';
    $strSQL .= '        SYSTEM_SCHEMA_NAME <> \'QTEMP\' ) B ';
    $strSQL .= ' WHERE B.SCHEMA_NAME <> \'\'';
    
    if($SearchData != ''){
        $strSQL .= ' AND ';
        $strSQL .= ' B.SCHEMA_NAME like ? ';
        array_push($params,'%'.$SearchData.'%');
    }
    $strSQL .= ' ) as A ';
    $strSQL .= ' WHERE A.ROWNUM <> 0';
    if($LibName != ''){
        $strSQL .= ' AND A.SCHEMA_NAME IN ( ? )';
        array_push($params,$LibName);
    }
    /***ライブラリー除外**/
    //$strSQL .= ' AND A.SCHEMA_NAME NOT IN (SELECT LIBSID FROM QTEMP/DB2LBLS)';
    if($libsnm !== '' ){
      if(SYSLIBCHK === '1'){
        $strSQL .= ' AND A.SCHEMA_NAME IN (';
        $strSQL .= '    SELECT ';
        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
        $strSQL .= '    FROM ';
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
            $strSQL .= MAINLIB.'/DB2LBLS ';
            $strSQL .= '        WHERE LIBSNM = ? ';
            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
            $strSQL .= '        AND LIBSFG = \'1\' ' ;
            array_push($params,$libsnm);
           
        }else{
            $strSQL .= ' QTEMP/DB2LBLS ';
           
        }
        $strSQL .= ' )';
	  }else{
		$strSQL .= ' AND A.SCHEMA_NAME NOT IN (';
        $strSQL .= '    SELECT ';
        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
        $strSQL .= '    FROM ';
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
            $strSQL .= MAINLIB.'/DB2LBLS ';
            $strSQL .= '        WHERE LIBSNM = ? ';
            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
            $strSQL .= '        AND LIBSFG = \'1\' ' ;
            array_push($params,$libsnm);
            
        }else{
            $strSQL .= ' QTEMP/DB2LBLS ';
            
        }
        $strSQL .= ' )';
	  }
    }
    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' AND A.ROWNUM BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    e_log('ライブラリー データ取得：'.$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                if($SearchData === '' && $LibName === ''){
                    // QTEMPのデータ追加
                        
                    $row['ROWNUM'] = $row['ROWNUM']+1;
                }else if($LibName === ''){
                    // QTEMPのデータ追加
                        
                    $row['ROWNUM'] = $row['ROWNUM']+1;
                }else if(strpos('QTEMP',$SearchData) || strpos('QTEMP',$LibName)){
                    // QTEMPのデータ追加
                    $row['ROWNUM'] = $row['ROWNUM']+1;
                }
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$SearchData,$LibName,$Libsnm = ''){
    $data = array();
    $params = array();
    $strSQL  = ' SELECT count(B.SCHEMA_NAME) as COUNT from ( ';
    $strSQL .= ' SELECT ';
    $strSQL .= ' A.SCHEMA_NAME ';
    $strSQL .= ' , A.SCHEMA_TEXT ';
    $strSQL .= ' FROM '  ;
    $strSQL .= '    (SELECT ';
    $strSQL .= '        SYSTEM_SCHEMA_NAME SCHEMA_NAME ';
    $strSQL .= '        , SCHEMA_TEXT ';
    $strSQL .= '    FROM ';
    $strSQL .= '        '.SYSSCHEMASLIB.'/SYSSCHEMAS ';
    $strSQL .= '    WHERE SYSTEM_SCHEMA_NAME <> \'QTEMP\' ';
    $strSQL .= '    ) A';
    $strSQL .= ' WHERE A.SCHEMA_NAME <> \'\'';
    if($SearchData != '' ){
        $strSQL .= ' AND ';
        $strSQL .= '(A.SCHEMA_NAME LIKE ? ';
        $strSQL .= 'OR A.SCHEMA_TEXT LIKE ?)';
        
        array_push($params,'%'.$SearchData.'%');
        array_push($params,'%'.$SearchData.'%');
    }
    if($LibName != ''){
        $strSQL .= ' AND A.SCHEMA_NAME IN ( ? )';
        array_push($params,$LibName);
    }
    if($Libsnm !== ''){
        $strSQL .= ' AND A.SCHEMA_NAME NOT IN (';
        $strSQL .= '    SELECT ';
        $strSQL .= '        CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
        $strSQL .= '    FROM ';
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
            $strSQL .= MAINLIB.'/DB2LBLS ';
            $strSQL .= '        WHERE LIBSNM = ? ';
            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
            $strSQL .= '        AND LIBSFG = \'1\' ' ;
            array_push($params,$Libsnm);
        }else{
            $strSQL .= ' QTEMP/DB2LBLS ';
        }
        $strSQL .= ' )';
    }
    $strSQL .= ' ) as B ';
    e_log('ライブラリー カウント取得：'.$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        e_log('ライブラリー取得：'.$strSQL.db2_stmt_errormsg());
        $data = array('result' => false);
    }else{

        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => false);
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}

function fnGetSysData($db2con)
{
    $data = array();
    
    $params = array();
    $strSQL = ' SELECT COLBFLG FROM DB2COF';
    $params = array(
        $HTMLTD
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    return $data;
}