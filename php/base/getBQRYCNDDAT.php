<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 条件情報取得
 * PROGRAM ID     : getBQRYCNDDAT.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/04/11
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");

/*
 * 変数
 */
$rtn   = 0;
$msg   = '';
$data  = array();
$QRYNM  = $_POST['QRYNM'];
$resultarr = array();
$visflg = 0;

/*
 * 処理
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}

if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    
        $auth = $rs['data'][0];
        $WUAUTH = cmMer($auth['WUAUTH']);
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'14',$userData[0]['WUSAUT']);//'14' =>制御レベル設定
            if($rs['result'] !== true){
                $visflg = 1;
                //$msg =  showMsg($rs['result'],array('制御レベル設定の権限'));
          }
    }
    
}
//クエリー存在チェック
if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        if(count($rs['data']) === 0){
            $rtn = 3;
            $msg = showMsg('NOTEXIST_GET',array('クエリー'));
        }
    }
}

if($rtn === 0){
    $rs = getBQRYCNDDAT($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}
$data = umEx($data);
if(count($data) > 0){
    $resultarr = createCndData($data);
}


cmDb2Close($db2con);
$rtn = array(
    'DATA' => $data,
    'BASECNDDAT' => $resultarr,
    'RTN' => $rtn,
    'MSG' => $msg,
    'VISFLG' => $visflg,
    'licensePivot' =>$licensePivot,
    //'licenseSeigyoSpecialFlg'=>$licenseSeigyoSpecialFlg
    'licenseSeigyo' => $licenseSeigyo
);
echo(json_encode($rtn));
/**
  *---------------------------------------------------------------------------
  * 条件情報を取得
  *---------------------------------------------------------------------------
  **/
function getBQRYCNDDAT($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=   '   SELECT QRYCND.CNQRYN ';
    $strSQL .=   '        , QRYCND.CNFILID ';
    $strSQL .=   '        , QRYCND.FILSEQ ';
    $strSQL .=   '        , QRYCND.CNFIL ';
    $strSQL .=   '        , QRYCND.CNMSEQ ';
    $strSQL .=   '        , GRPCND.CNDCNT ';
    $strSQL .=   '        , GRPCND.CNSCNT ';
    $strSQL .=   '        , GRPCND.CDCNT ';
    $strSQL .=   '        , QRYCND.CNSSEQ ';
    $strSQL .=   '        , QRYCND.CNFLDN ';
    $strSQL .=   '        , QRYCND.CNAOKB ';
    $strSQL .=   '        , QRYCND.CNCKBN ';
    $strSQL .=   '        , QRYCND.CNSTKB ';
    $strSQL .=   '        , QRYCND.CDCDCD ';
    $strSQL .=   '        , QRYCND.CDDAT ';
    $strSQL .=   '        , QRYCND.CDBCNT ';
    $strSQL .=   '        , QRYCND.CDCDCD ';
    $strSQL .=   '        , QRYCND.CDFFLG ';
    $strSQL .=   '   FROM ';        
    $strSQL .=   '       ( ';
    $strSQL .=   '           SELECT A.CNQRYN ';
    $strSQL .=   '                , A.CNFILID ';
    $strSQL .=   '                , FILCD.FILSEQ ';
    $strSQL .=   '                , ( CASE ';
    $strSQL .=   '                       WHEN A.CNFILID = 0 THEN \'P\' ';
    $strSQL .=   '                       WHEN A.CNFILID <= FILCD.FILSEQ ';
    $strSQL .=   '                       THEN CONCAT(\'S\', A.CNFILID ) ';
    $strSQL .=   '                       ELSE \'K\' ';
    $strSQL .=   '                    END ';
    $strSQL .=   '                   ) AS CNFIL ';
    $strSQL .=   '               , A.CNMSEQ ';
    $strSQL .=   '               , A.CNSSEQ ';
    $strSQL .=   '               , A.CNFLDN ';
    $strSQL .=   '               , A.CNAOKB ';
    $strSQL .=   '               , A.CNCKBN ';
    $strSQL .=   '               , A.CNSTKB ';  
    $strSQL .=   '               , B.CDCDCD ';
    $strSQL .=   '               , B.CDDAT ';
    $strSQL .=   '               , B.CDBCNT ';
    $strSQL .=   '               , (CASE B.CDFFLG ';
    $strSQL .=   '                      WHEN \'\' THEN \'0\' ';
    $strSQL .=   '                      ELSE B.CDFFLG ';
    $strSQL .=   '                  END ';
    $strSQL .=   '                  ) AS CDFFLG ';
    $strSQL .=   '           FROM BQRYCND A ';
    $strSQL .=   '           LEFT JOIN  BCNDDAT B ';
    $strSQL .=   '           ON    A.CNQRYN  = B.CDQRYN ';
    $strSQL .=   '           AND   A.CNFILID = B.CDFILID ';   
    $strSQL .=   '           AND   A.CNMSEQ  = B.CDMSEQ ';
    $strSQL .=   '           AND   A.CNSSEQ  = B.CDSSEQ ';
    $strSQL .=   '           LEFT JOIN  ( ';
    $strSQL .=   '               SELECT RTQRYN,MAX(RTRSEQ) AS FILSEQ  '; 
    $strSQL .=   '               FROM BREFTBL ';
    $strSQL .=   '               WHERE RTQRYN = ? ';
    $strSQL .=   '               GROUP BY  RTQRYN ) FILCD ';
    $strSQL .=   '           ON FILCD.RTQRYN = A.CNQRYN ';   
    $strSQL .=   '           WHERE   A.CNQRYN  = ? ';
    $strSQL .=   '           ORDER BY A.CNMSEQ ,A.CNSSEQ  ,B.CDCDCD ';
    $strSQL .=   '       ) QRYCND , ';
    $strSQL .=   '       ( ';
    $strSQL .=   '           SELECT A.CNQRYN ';
    $strSQL .=   '                , A.CNMSEQ ';
    $strSQL .=   '                , B.CNDCNT ';
    $strSQL .=   '                , A.CNSCNT ';             
    $strSQL .=   '                , C.CDSSEQ ';
    $strSQL .=   '                , C.CDCNT ';
    $strSQL .=   '           FROM ( '; 
    $strSQL .=   '                   SELECT CNQRYN ';
    $strSQL .=   '                        , CNMSEQ ';           
    $strSQL .=   '                        , COUNT(CNSSEQ) CNSCNT ';
    $strSQL .=   '                     FROM BQRYCND ';
    $strSQL .=   '                    WHERE CNQRYN  = ? ';
    $strSQL .=   '                 GROUP BY CNQRYN ';              
    $strSQL .=   '                        , CNMSEQ ';
    $strSQL .=   '               ) A ';
    $strSQL .=   '               ,( ';
    $strSQL .=   '                   SELECT CNQRYN ';
    $strSQL .=   '                        , MAX(CNMSEQ) AS CNDCNT ';
    $strSQL .=   '                     FROM BQRYCND ';      
    $strSQL .=   '                    WHERE CNQRYN  = ? ';
    $strSQL .=   '                 GROUP BY CNQRYN ';
    $strSQL .=   '               ) B ';
    $strSQL .=   '               ,( ';
    $strSQL .=   '                   SELECT QRYC.CNQRYN AS CDQRYN ';
    $strSQL .=   '                        , QRYC.CNMSEQ AS CDMSEQ ';
    $strSQL .=   '                        , QRYC.CNSSEQ AS CDSSEQ ';
    $strSQL .=   '                        , COUNT(CDAT.CDCDCD) CDCNT '; 
    $strSQL .=   '                     FROM BQRYCND QRYC '; 
    $strSQL .=   '                LEFT JOIN BCNDDAT CDAT ';
    $strSQL .=   '                       ON QRYC.CNQRYN  = CDAT.CDQRYN ';
    $strSQL .=   '                      AND QRYC.CNMSEQ = CDAT.CDMSEQ '; 
    $strSQL .=   '                      AND QRYC.CNSSEQ = CDAT.CDSSEQ ';
    $strSQL .=   '                    WHERE  QRYC.CNQRYN  = ? ';
    $strSQL .=   '                 GROUP BY QRYC.CNQRYN ';
    $strSQL .=   '                        , QRYC.CNMSEQ ';
    $strSQL .=   '                        , QRYC.CNSSEQ ';
    $strSQL .=   '                        , CDAT.CDQRYN ';
    $strSQL .=   '                        , CDAT.CDMSEQ ';
    $strSQL .=   '                        , CDAT.CDSSEQ '; 
    $strSQL .=   '               )C ';
    $strSQL .=   '           WHERE A.CNQRYN = B.CNQRYN ';   
    $strSQL .=   '             AND A.CNQRYN = C.CDQRYN ';
    $strSQL .=   '             AND B.CNQRYN = C.CDQRYN ';
    $strSQL .=   '             AND A.CNMSEQ = C.CDMSEQ ';
    $strSQL .=   '       ) GRPCND ';   
    $strSQL .=   '   WHERE QRYCND.CNQRYN =  GRPCND.CNQRYN ';     
    $strSQL .=   '     AND QRYCND.CNMSEQ =  GRPCND.CNMSEQ ';
    $strSQL .=   '     AND QRYCND.CNSSEQ =  GRPCND.CDSSEQ ';
    $strSQL .=   '   ORDER BY QRYCND.CNMSEQ ';
    $strSQL .=   '          , QRYCND.CNSSEQ ';

    $params = array($QRYNM,$QRYNM,$QRYNM,$QRYNM,$QRYNM);
    error_log("PPN20181105 getBQRYCNDDAT => ".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function createCndData($bqrycnddat){
    $cdcnt = '';
    $cnmseq = '';
    $cnsseq = '';
    $bqrycnddatRes = array();
    foreach($bqrycnddat as $bqrycnddatdata){

        if($cnmseq != $bqrycnddatdata['CNMSEQ']){
            $bqrycnd = array();
            $cnmseq = $bqrycnddatdata['CNMSEQ'];
            if($bqrycnddatdata['CNSSEQ'] == 1){
                $cnsseq = '';
                $bqrycnd['CNDIDX'] = $bqrycnddatdata['CNMSEQ'];
                $bqrycnd['SQRYAO'] = $bqrycnddatdata['CNAOKB'];
                $bqrycnd['CDATACNT'] = $bqrycnddatdata['CNSCNT'];
                $bqrycnd['CNDSDATA'] = array();
            }
        }
        if($cnmseq == $bqrycnddatdata['CNMSEQ']){
            if($cnsseq != $bqrycnddatdata['CNSSEQ']){
                $cnsseq = $bqrycnddatdata['CNSSEQ'];
                $bqrycndinfo = array();
                
                $bqrycndinfo['CNDDATA'] = array();
                $cdcnt = '';
                $cdcnt = $bqrycnddatdata['CDCNT'];
                $bqrycndinfo['CDATAIDX'] = $cnsseq;
                if($bqrycnddatdata['CNSSEQ'] > 1){
                    $bqrycndinfo['CNDANDOR'] = $bqrycnddatdata['CNAOKB'];
                }else{
                    $bqrycndinfo['CNDANDOR'] = '';
                }
                if($bqrycnddatdata['CNFIL'] === ''){
                    $bqrycndinfo['CNDFLD']   = $bqrycnddatdata['CNFLDN'];
                }else{
                    $bqrycndinfo['CNDFLD']   = $bqrycnddatdata['CNFIL'].'.'.$bqrycnddatdata['CNFLDN'];
                }
                $bqrycndinfo['CNDKBN']   = $bqrycnddatdata['CNCKBN'];
                $bqrycndinfo['CNDTYP']   = $bqrycnddatdata['CNSTKB'];
                $bqrycndinfo['CDCNT']    = $bqrycnddatdata['CDCNT'];
            }
            if($bqrycndinfo['CDATAIDX'] == $bqrycnddatdata['CNSSEQ']){
                if($bqrycndinfo['CDCNT'] == 0){
					$cnddat = array();
					$cnddat['DATA'] = '';
					$cnddat['FFLG'] = '0';
					if($bqrycndinfo['CNDKBN'] === 'RANGE'){
						$bqrycndinfo['CNDDATA'][] = $cnddat;
					}
					$bqrycndinfo['CNDDATA'][] = $cnddat;
                    $bqrycnd['CNDSDATA'][]  = $bqrycndinfo;
                }else{
                    //$CDDAT = array();
                    //$CDDAT['DAT'] = $bqrycnddatdata['CDDAT'];
                    //$CDDAT['BCNT'] = $bqrycnddatdata['CDBCNT'];
                    if((int)$bqrycnddatdata['CDBCNT'] > 0){
                        $bqrycnddatdata['CDDAT'] = str_pad($bqrycnddatdata['CDDAT'], (int)$bqrycnddatdata['CDBCNT']);
                    }
                    $cnddat = array();
                    $cnddat['DATA'] = $bqrycnddatdata['CDDAT'];
                    $cnddat['FFLG'] = $bqrycnddatdata['CDFFLG'];
                    $bqrycndinfo['CNDDATA'][] = $cnddat;
                    if ($bqrycndinfo['CDCNT'] == count($bqrycndinfo['CNDDATA'])){
                        $bqrycnd['CNDSDATA'][]  = $bqrycndinfo;
                    }
                }
                
            }
            if($bqrycnd['CDATACNT'] == count($bqrycnd['CNDSDATA'])){
                $bqrycnddatRes[] = $bqrycnd;
            }
        }
    }
    return $bqrycnddatRes;
}
function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
