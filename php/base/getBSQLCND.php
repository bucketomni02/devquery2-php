<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : SQL文で作成された定義の条件パラメータデータ取得
 * PROGRAM ID     : getBSQLCND.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2017/03/23
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

$QRYNM  = $_POST['QRYNM'];
/*
 * 変数
 */
$rtn            = 0;
$msg            = '';
$data           = array();
$baseparamdata  = array();

/*
 * 処理
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
//クエリー存在チェック
if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        if(count($rs['data']) === 0){
            $rtn = 3;
            $msg = showMsg('NOTEXIST_GET',array('クエリー'));
        }
    }
}
if($rtn === 0){
    $rs = fnGetBSQLCND($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
        $baseparamdata = createCndParamData($data);
    }
}
cmDb2Close($db2con);
$rtn = array(
    'BSQLCND' => $data,
    'BASEPARAMDATA' => $baseparamdata,
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));
/**
  *---------------------------------------------------------------------------
  * SQL文で作成された定義の条件パラメータデータ取得
  *---------------------------------------------------------------------------
  **/
function fnGetBSQLCND($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';
    $strSQL .= '    SELECT ';
    $strSQL .= '        CNQRYN, ';
    $strSQL .= '        CNDSEQ, ';
    $strSQL .= '        CNDKBN, ';
    $strSQL .= '        CNDNM, ';
    $strSQL .= '        CNDWNM, ';
    $strSQL .= '        CNDDTYP, ';
    $strSQL .= '        CNDSTKB, ';
    $strSQL .= '        CNDDAT, ';
    $strSQL .= '        CNDBCNT, ';
    $strSQL .= '        CNCKBN ';
    $strSQL .= '    FROM ';
    $strSQL .= '        BSQLCND ';
    $strSQL .= '    WHERE ';
    $strSQL .= '        CNQRYN = ? ';
    $strSQL .= '    ORDER BY CAST(CNDSEQ AS INTEGER) ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
function createCndParamData($bsqlcnd){
    $baseparamData = array();
    if(count($bsqlcnd)>0){
        foreach($bsqlcnd as $key => $bsqlcnddata){
            $pData = array();
            $idx = $key+1;
            $pData['TXTPRMSEQ'.$idx] = $bsqlcnddata['CNDSEQ'];
            $pData['CMBDATATYPE'.$idx] = $bsqlcnddata['CNDDTYP'];
            $pData['CMBSELTYPE'.$idx] = $bsqlcnddata['CNDSTKB'];//CMBLIKETYPE
            $pData['CMBLIKETYPE'.$idx] = $bsqlcnddata['CNCKBN'];
            if((int)$bsqlcnddata['CNDBCNT'] > 0){
                $pData['TXTPARAMDATA'.$idx] = str_pad($bsqlcnddata['CNDDAT'], (int)$bsqlcnddata['CNDBCNT']);
            }else{
                $pData['TXTPARAMDATA'.$idx] = $bsqlcnddata['CNDDAT'];
            }
            $pData['TXTWPARMNM'.$idx] = $bsqlcnddata['CNDWNM'];
            $pData['TXTPARMNM'.$idx] = $bsqlcnddata['CNDNM'];
            $baseparamData[] = $pData;
        }
    }
    return $baseparamData;
}
/**
  *---------------------------------------------------------------------------
  * FDB2CSV1情報取得
  *---------------------------------------------------------------------------
  **/
function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
