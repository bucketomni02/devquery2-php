<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : サマリー情報取得
 * PROGRAM ID     : getBSUMFLD.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/04/11
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
 * 変数
 */
$rtn   = 0;
$msg   = '';
$sumdata  = array();
$gpdata  = array();
$QRYNM  = $_POST['QRYNM'];
$sumkeydata = array();
$groupingdata = array();
$summarydata = array();

/*
 * 処理
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
//クエリー存在チェック
if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        if(count($rs['data']) === 0){
            $rtn = 3;
            $msg = showMsg('NOTEXIST_GET',array('クエリー'));
        }
    }
}

if($rtn === 0){
    $rs = getFDB2CSV25($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $sumdata = $rs['data'];
    }
}
if($rtn === 0){
    $sumdata = umEx($sumdata,false);
    if(count($sumdata) > 0){

        $sumkeydata = createSUMKEYDATA($sumdata);

        $rs = getBSUMFLD($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $gpdata = $rs['data'];
        }
        $gpdata = umEx($gpdata);
        if(count($gpdata) > 0){
            $groupingdata = createGROUPINGDATA($gpdata);
        }
    }
}
$summarydata['SUMKEYDATA']   = $sumkeydata;
$summarydata['GROUPINGDATA'] = $groupingdata;
cmDb2Close($db2con);
$rtn = array(
    'SUMMARYDATA' => $summarydata,
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));
/**
  *---------------------------------------------------------------------------
  * 条件情報を取得
  *---------------------------------------------------------------------------
  **/
function getFDB2CSV25($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT A.* ';
    $strSQL .=  '   FROM ( ';        
    $strSQL .=  '           (SELECT D2NAME ';
    $strSQL .=  '                 , D2FILID ';
    $strSQL .=  '                 , D2FLD ';
    $strSQL .=  '                 , D2GSEQ ';
    $strSQL .=  '                 , (CASE '; 
    $strSQL .=  '                       WHEN D2FILID = 0 THEN \'P\' ';
    $strSQL .=  '                       WHEN D2FILID > 0 THEN concat(\'S\',D2FILID) ';
    $strSQL .=  '                  END) AS FILTYP ';            
    $strSQL .=  '           FROM  FDB2CSV2 '; 
    $strSQL .=  '           WHERE D2NAME = ? ';
    $strSQL .=  '           AND   D2GSEQ > 0) ';
    $strSQL .=  '           UNION ALL '; 
    $strSQL .=  '           (SELECT  D5NAME AS D2NAME ';
    $strSQL .=  '               , 9999 AS D2FILID ';
    $strSQL .=  '               , D5FLD AS D2FLD ';
    $strSQL .=  '               , D5GSEQ AS D2GSEQ ';
    $strSQL .=  '               , \'K\' AS FILTYP ';
    $strSQL .=  '           FROM  FDB2CSV5 '; 
    $strSQL .=  '           WHERE D5NAME = ? ';
    $strSQL .=  '           AND   D5GSEQ > 0 ) ';
    $strSQL .=  '       )  A ';
    $strSQL .=  '   ORDER BY A.D2GSEQ ';

    $params = array($QRYNM,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function getBSUMFLD($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '    SELECT A.SFQRYN ';
    $strSQL .=  '         , A.SFFILID ';
    $strSQL .=  '         , A.SFFLDNM ';
    $strSQL .=  '         , A.SFSEQ ';
    $strSQL .=  '         , A.SFGMES ';
    $strSQL .=  '         , FILCD.FILSEQ ';
    $strSQL .=  '         , CASE '; 
    $strSQL .=  '                WHEN A.SFFILID = 0 THEN \'P\' ';
    $strSQL .=  '                WHEN A.SFFILID <= FILCD.FILSEQ THEN concat(\'S\',A.SFFILID ) ';
    $strSQL .=  '                ELSE \'K\' ';
    $strSQL .=  '              END AS FILTYP ';
    $strSQL .=  '    FROM BSUMFLD A ';   
    $strSQL .=  '    LEFT JOIN ';
    $strSQL .=  '        ( ';
    $strSQL .=  '            SELECT RTQRYN ';
    $strSQL .=  '                 , MAX(RTRSEQ) AS FILSEQ ';          
    $strSQL .=  '            FROM   BREFTBL ';
    $strSQL .=  '            WHERE  RTQRYN = ? ';
    $strSQL .=  '            GROUP BY  RTQRYN '; 
    $strSQL .=  '        ) FILCD ';
    $strSQL .=  '    ON    FILCD.RTQRYN = A.SFQRYN ';
    $strSQL .=  '    WHERE A.SFQRYN     = ? ';
    $strSQL .=  '    ORDER BY A.SFSEQ ';

    $params = array($QRYNM,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function createSUMKEYDATA($sumdata){
    $sumkeydata = array();
    foreach($sumdata as $sumkey){
        $sumkeyInfo = array();
        $sumkeyInfo['GSEQ']     = $sumkey['D2GSEQ'];
        $sumkeyInfo['FLDVAL']   = $sumkey['FILTYP'].'.'.$sumkey['D2FLD'];
        $sumkeydata[]           = $sumkeyInfo;
    }
    return $sumkeydata;
}
function createGROUPINGDATA($gpdata){
    $groupingdata = array();
    foreach($gpdata as $gpkey){
        $gpInfo = array();
        $gpInfo['GSEQ']     = $gpkey['SFSEQ'];
        $gpInfo['FLDVAL']   = $gpkey['FILTYP'].'.'.$gpkey['SFFLDNM'];
        $gpInfo['GMES']     = $gpkey['SFGMES'];
        $groupingdata[]     = $gpInfo;
    }
    return $groupingdata;
}
function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
