<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../licenseInfo.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
$GETLVLDATA = json_decode($_POST['GETLVLDATA'], true);
$GETCOLMDATA = json_decode($_POST['GETCOLMDATA'], true);
$GETCOLTDATA = json_decode($_POST['GETCOLTDATA'], true);
/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
$DCFLD1 = false;
$DCFLD2 = false;
$DCFLD3 = false;
$DCFLD4 = false;
$DCFLD5 = false;
$DCFLD6 = false;
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    } else {
        $auth = $rs['data'][0];
        $WUAUTH = cmMer($auth['WUAUTH']);
        $userData = umEx($rs['data']);
        if ($userData[0]['WUAUTH'] === '2') {
            $rs = cmChkKenGen($db2con, '14', $userData[0]['WUSAUT']); //'14' =>制御レベル設定
            if ($rs['result'] !== true) {
                $rtn = 3;
                $msg = showMsg($rs['result'], array('制御レベル設定の権限'));
            }
        }
    }
}

if ($rtn === 0) {
    foreach ($GETLVLDATA as $lvldata) {
        if ($DCFLD1 !== true) {
            $LGroup = explode(",", $GETCOLMDATA['DCFLD1']);
            if (count($LGroup) >= 2) { //グループのレベル
                for ($i = 0;$i < count($LGroup);$i++) {
                    $spiltRes = explode("-", $LGroup[$i]);
		            if ($spiltRes[1] === '') {
		                $DCFLD1 = true;
						break;
		            } else if ($lvldata['D2FLD'] === $spiltRes[1]) {
		                $DCFLD1 = true;
						break;
		            }
                }
            } else {
	            if ($GETCOLMDATA['DCFLD1'] === '') {
	                $DCFLD1 = true;
	            } else if ($lvldata['D2FLD'] === $GETCOLMDATA['DCFLD1']) {
	                $DCFLD1 = true;
	            }
            }
        }
        if ($DCFLD2 !== true) {
            $LGroup = explode(",", $GETCOLMDATA['DCFLD2']);
            if (count($LGroup) >= 2) { //グループのレベル
                for ($i = 0;$i < count($LGroup);$i++) {
                    $spiltRes = explode("-", $LGroup[$i]);
		            if ($spiltRes[1] === '') {
		                $DCFLD2 = true;
						break;
		            } else if ($lvldata['D2FLD'] === $spiltRes[1]) {
		                $DCFLD2 = true;
						break;
		            }
                }
            } else {
	            if ($GETCOLMDATA['DCFLD2'] === '') {
	                $DCFLD2 = true;
	            } else if ($lvldata['D2FLD'] === $GETCOLMDATA['DCFLD2']) {
	                $DCFLD2 = true;
	            }
            }
	    }
        if ($DCFLD3 !== true) {
            $LGroup = explode(",", $GETCOLMDATA['DCFLD3']);
            if (count($LGroup) >= 2) { //グループのレベル
                for ($i = 0;$i < count($LGroup);$i++) {
                    $spiltRes = explode("-", $LGroup[$i]);
		            if ($spiltRes[1] === '') {
		                $DCFLD3 = true;
						break;
		            } else if ($lvldata['D2FLD'] === $spiltRes[1]) {
		                $DCFLD3 = true;
						break;
		            }
                }
            } else {
	            if ($GETCOLMDATA['DCFLD3'] === '') {
	                $DCFLD3 = true;
	            } else if ($lvldata['D2FLD'] === $GETCOLMDATA['DCFLD3']) {
	                $DCFLD3 = true;
	            }
            }
        }
        if ($DCFLD4 !== true) {
            $LGroup = explode(",",$GETCOLMDATA['DCFLD4']);
            if (count($LGroup) >= 2) { //グループのレベル
                for ($i = 0;$i < count($LGroup);$i++) {
                    $spiltRes = explode("-", $LGroup[$i]);
		            if ($spiltRes[1] === '') {
		                $DCFLD4 = true;
						break;
		            } else if ($lvldata['D2FLD'] === $spiltRes[1]) {
		                $DCFLD4 = true;
						break;
		            }
                }
            } else {
	            if ($GETCOLMDATA['DCFLD4'] === '') {
	                $DCFLD4 = true;
	            } else if ($lvldata['D2FLD'] === $GETCOLMDATA['DCFLD4']) {
	                $DCFLD4 = true;
	            }
            }
        }
        if ($DCFLD5 !== true) {
            $LGroup = explode(",",$GETCOLMDATA['DCFLD5']);
            if (count($LGroup) >= 2) { //グループのレベル
                for ($i = 0;$i < count($LGroup);$i++) {
                    $spiltRes = explode("-", $LGroup[$i]);
		            if ($spiltRes[1] === '') {
		                $DCFLD5 = true;
						break;
		            } else if ($lvldata['D2FLD'] === $spiltRes[1]) {
		                $DCFLD5 = true;
						break;
		            }
                }
            } else {
	            if ($GETCOLMDATA['DCFLD5'] === '') {
	                $DCFLD5 = true;
	            } else if ($lvldata['D2FLD'] === $GETCOLMDATA['DCFLD5']) {
	                $DCFLD5 = true;
	            }
            }
        }
        if ($DCFLD6 !== true) {
            $LGroup = explode(",",$GETCOLMDATA['DCFLD6']);
            if (count($LGroup) >= 2) { //グループのレベル
                for ($i = 0;$i < count($LGroup);$i++) {
                    $spiltRes = explode("-", $LGroup[$i]);
		            if ($spiltRes[1] === '') {
		                $DCFLD6 = true;
						break;
		            } else if ($lvldata['D2FLD'] === $spiltRes[1]) {
		                $DCFLD6 = true;
						break;
		            }
                }
            } else {
	            if ($GETCOLMDATA['DCFLD6'] === '') {
	                $DCFLD6 = true;
	            } else if ($lvldata['D2FLD'] === $GETCOLMDATA['DCFLD6']) {
	                $DCFLD6 = true;
	            }
            }
        }
    }
}
if ($DCFLD1 === false) {
    $rtn = 1;
    $msg = showMsg('SEIGYO_NO_COLS');
}
if ($DCFLD2 === false) {
    $rtn = 1;
    $msg = showMsg('SEIGYO_NO_COLS');
}
if ($DCFLD3 === false) {
    $rtn = 1;
    $msg = showMsg('SEIGYO_NO_COLS');
}
if ($DCFLD4 === false) {
    $rtn = 1;
    $msg = showMsg('SEIGYO_NO_COLS');
}
if ($DCFLD5 === false) {
    $rtn = 1;
    $msg = showMsg('SEIGYO_NO_COLS');
}
if ($DCFLD6 === false) {
    $rtn = 1;
    $msg = showMsg('SEIGYO_NO_COLS');
}
if ($rtn === 0) {
    $chkflg = '0';
    for ($i = 0;$i <= count($GETCOLTDATA);$i++) {
        // e_log('GETLVLDATA***'.print_r($GETCOLTDATA[$i]['DTFLD'],true));
        if ($GETCOLTDATA[$i]['DTFLD'] === null) {
            $chkflg = '1';
            $rtn = 0;
            break;
        } else if ($GETCOLTDATA[$i]['DTFLD'] !== '') {
            foreach ($GETLVLDATA as $lvldata) {
                //  e_log('LVLDATA***'.print_r($lvldata['D2FLD'],true));
                if ($lvldata['D2FLD'] === $GETCOLTDATA[$i]['DTFLD']) {
                    $chkflg = '1';
                    break;
                } else {
                    $chkflg = '0';
                }
            }
            if ($chkflg === '0') {
                $rtn = 1;
                $msg = showMsg('SEIGYO_NO_COLS');
                break;
            }
            // e_log('CHECKFLG***'.print_r($chkflg,true));
            
        }
    }
}
cmDb2Close($db2con);
/**return**/
$rtn = array('RTN' => $rtn, 'MSG' => $msg, 'DATA' => umEx($data), 'licensePivot' => $licensePivot, 'licenseSeigyoSpecialFlg' => $licenseSeigyoSpecialFlg, 'WUAUTH' => $WUAUTH);
echo (json_encode($rtn));
