<?php

/*
*-------------------------------------------------------* 
* 出力ファイル
*-------------------------------------------------------*
*/
/*include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");*/

$rtn=0;
$msg='';
//【コピー】FDB2CSV1で出力ファイルがすでにかどうかチェック
function fnChkD1OUTFIL($db2con,$D1OUTLIB,$D1OUTFIL){
    $data = array();
    $params=array();
    $totcount=0;
    $rs = true;    
    $strSQL  = ' SELECT COUNT(*) AS TOTCOUNT ';
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;
    $strSQL .= ' WHERE  A.D1OUTLIB=? AND A.D1OUTFIL = ? ' ;
    $params = array(
        $D1OUTLIB,
        $D1OUTFIL
    );   
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $totcount= $row['TOTCOUNT'];
            }
        }
    }
    $data=array('result'=>$rs,'TOTCOUNT'=>$totcount);
    return $data;
}
//FDB2CSV1で自分かどうかをチェック
function fnExistFileFDB2CSV1($db2con,$D1NAME,$D1OUTLIB,$D1OUTFIL){
    $data = array();
    $params=array();
    $totcount=0;
    $rs = true;    
    $strSQL  = ' SELECT COUNT(*) AS TOTCOUNT ';
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;
    $strSQL .= ' WHERE  A.D1NAME=? AND A.D1OUTLIB=? AND A.D1OUTFIL = ? ' ;
    $params = array(
        $D1NAME,
        $D1OUTLIB,
        $D1OUTFIL
    );   
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $totcount= $row['TOTCOUNT'];
            }
        }
    }
    $data=array('result'=>$rs,'TOTCOUNT'=>$totcount);
    return $data;
}

//【追加】管理者ライブラリの同じファイルをチェック
function fnChkExistLibOutput($db2con,$LIB,$LIBSNM='',$LIBSDB=''){
    $data = array();
    $params = array();
    $rs = true;
    if($LIB === 'QTEMP'){
        $rs = 'ISEXIST';
    }else{
        $strSQL  = ' SELECT A.SCHEMA_NAME ';
        $strSQL .= ' FROM  ' ;
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '        SYSTEM_SCHEMA_NAME SCHEMA_NAME ';
        $strSQL .= '        , SCHEMA_TEXT ';
        $strSQL .= '        FROM ';
        $strSQL .= '        '.SYSSCHEMASLIB.'/SYSSCHEMAS ';
        $strSQL .= '        WHERE SYSTEM_SCHEMA_NAME <> \'QTEMP\'';
        //ライブラリー条件
        if($LIBSNM !==''){
            if(SYSLIBCHK === '1'){
                $strSQL .= ' AND SYSTEM_SCHEMA_NAME  IN (SELECT LIBSID FROM DB2LBLS WHERE LIBSNM = ? AND LIBSDB = ?)';
                array_push($params,$LIBSNM,$LIBSDB);
            }else{
                $strSQL .= ' AND SYSTEM_SCHEMA_NAME NOT IN (SELECT LIBSID FROM DB2LBLS WHERE LIBSNM = ? AND LIBSDB = ?)';
                array_push($params,$LIBSNM,$LIBSDB);
            }
        }
        $strSQL .= '    ) A ';
        $strSQL .= ' WHERE A.SCHEMA_NAME = ? ' ;
        array_push($params,$LIB);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_SEL';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_SEL';
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                 if(count($data) > 0){
                    $rs = 'ISEXIST';
                }
            }
        }
    }
    return $rs;
}
//チェックファイル
function chkFileOutput($db2con,$FILE){
    $rtn = array('result' => 0);
    if($rtn['result'] === 0){
        if($FILE === ''){
            $rtn = array('result' => 'FAIL_REQ');
        }
    }
    if($rtn['result'] === 0){
        if(!checkMaxLen($FILE,10)){
            $rtn = array('result' => 'FAIL_MAXLEN');
        }
    }
    if($rtn['result'] === 0){
        $byte = checkByte($FILE);
        if($byte[1] > 0){
            $rtn = array('result' => 'FAIL_BYTE');
        }
    }
    return $rtn;
}
//チェックライブラリー
function chkLIBOutput($db2con,$LIB,$LIBSNM='',$LIBSDB=''){
    $rtn = 0;
    if($rtn === 0){
        if(!checkMaxLen($LIB,10)){
            $rtn = 'FAIL_MAXLEN';
        }
    }
    if($rtn === 0){
        $byte = checkByte($LIB);
        if($byte[1] > 0){
            $rtn = 'FAIL_BYTE';
        }
    }
    if($rtn === 0){
        $rs = fnChkExistLibOutput($db2con,$LIB,$LIBSNM,$LIBSDB);
        if($rs === true){
            $rtn = 'NOTEXIST';
        }
    }
    return $rtn;
}
//【追加】管理者ライブラリとファイルの同じファイルをチェック
function fnChkExistFileOutput($db2con,$LIB,$FILE){
    $data = array();
    $rs=true;
    $rtn=0;
    $params = array();
    $strSQL  = ' SELECT A.SYSTEM_TABLE_NAME ';
    $strSQL .= '      , A.SYSTEM_TABLE_SCHEMA ';
    $strSQL .= '      , A.TABLE_TEXT';
    $strSQL .= ' FROM  ' ;
    $strSQL .= '    QSYS2/SYSTABLES A ';
    $strSQL .= ' WHERE A.SYSTEM_TABLE_SCHEMA = ? ' ;
    $strSQL .= ' AND A.SYSTEM_TABLE_NAME = ? ' ;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs='FAIL_SEL';
        $rtn=1;//普通のエラー
    }else{
        $params = array($LIB,$FILE);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs='FAIL_SEL';
            $rtn=1;//普通のエラー
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
        if(count($data)>0){
            $rs='ISEXIST_OUT';
            $rtn=4;//というのは普通のエラーじゃなくて、メッセージをですのため
        }
    }
    return array('result'=>$rs,'rtn'=>$rtn);
}


