<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
// セッションライブラリ

$COLDATALST = json_decode($_POST['COLDATALST'],true);
$COLSELDATA = json_decode($_POST['COLSELDATA'],true);

$resColLst      = array();
$resColDataLst  = array();
$resStrData     = array();
$SEARCHKEY      = '';
$SEARCHCOL      = '';
/**return**/
$SEARCHCOLDATALST = $COLDATALST;
foreach($COLSELDATA as $key => $data){
    $SEARCHCOL = $key;
    $SEARCHKEY = $data;
    if($SEARCHKEY === ''){
        // 処理無し
        $resColDataLst  = $SEARCHCOLDATALST;
    }else{
        if($SEARCHCOL === 'SORTTYP'){
            $seldata = array();
            if(strlen($SEARCHKEY) === 1 && $SEARCHKEY === ' '){
                $seldata[] = ' ';
            }
            if(stripos('昇順',$SEARCHKEY) !== false){
                $seldata[]   = 'A';
            }
            if(stripos('降順',$SEARCHKEY) !== false){
                $seldata[]  = 'D';
            }
            if(count($seldata) === 2){
                $resColDataLst  = array();
                foreach($SEARCHCOLDATALST as $key => $colData){
                    if( $colData[$SEARCHCOL] !== ''){
                        $resColLst[] = $colData[$SEARCHCOL];
                        $resStrData[] = array('CHK_VAL'=> false, 'COL_DAT'=>$colData[$SEARCHCOL]);
                        $resColDataLst[] = $colData;
                    }
                }
            }else if(count($seldata) === 1){
                $resColDataLst  = array();
                foreach($SEARCHCOLDATALST as $key => $colData){
                    if($seldata[0] === ' '){
                        if($colData[$SEARCHCOL] === ''){
                            $resColLst[] = $colData[$SEARCHCOL];
                            $resStrData[] = array('CHK_VAL'=> false, 'COL_DAT'=>$colData[$SEARCHCOL]);
                            $resColDataLst[] = $colData;
                        }
                    }else if( stripos($colData[$SEARCHCOL],$seldata[0]) !== false){
                        $resColLst[] = $colData[$SEARCHCOL];
                        $resStrData[] = array('CHK_VAL'=> false, 'COL_DAT'=>$colData[$SEARCHCOL]);
                        $resColDataLst[] = $colData;
                    }
                }
            }else{
                $resColDataLst  = array();
                foreach($SEARCHCOLDATALST as $key => $colData){
                    if( stripos($colData[$SEARCHCOL],$SEARCHKEY) !== false){
                        $resColLst[] = $colData[$SEARCHCOL];
                        $resStrData[] = array('CHK_VAL'=> false, 'COL_DAT'=>$colData[$SEARCHCOL]);
                        $resColDataLst[] = $colData;
                    }
                }
            }
        }else if($SEARCHCOL === 'EDTCD'){
            $seldata = array();
            if(strlen($SEARCHKEY) === 1 && $SEARCHKEY === ' '){
                $seldata[] = ' ';
            }else{
                $db2con = cmDb2Con();
                cmSetPHPQUERY($db2con);

                $data = array();
                $rtn = 0;

                $strSQL = '';
                $strSQL .= ' SELECT ';
                $strSQL .= '    KBNTEXT ';
                $strSQL .= ' FROM ';
                $strSQL .= '    DB2MKBN ';
                $strSQL .= ' WHERE ';
                $strSQL .= '     KBNKB = \'001\' ';
                $strSQL .= ' AND KBNNM LIKE ? ';
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rtn = 'FAIL_SQL';
                }else{
                    $params = array('%'.$SEARCHKEY.'%');
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $rtn = 'FAIL_SQL';
                    }else{
                        while($row = db2_fetch_assoc($stmt)){
                            $data[] = cmMer($row['KBNTEXT']);
                        }
                    }
                }
                cmDb2Close($db2con);
                if($rtn !== 0){
                    // 処理無し
                }else{
                    if(count($data) === 0){
                        // 処理無し
                    }else{
                        $seldata = $data;
                    }
                }
            }
            if(count($seldata) === 0){
                $resColDataLst  = array();
                foreach($SEARCHCOLDATALST as $key => $colData){
                    if( stripos($colData[$SEARCHCOL],$SEARCHKEY) !== false){
                        $resColLst[] = $colData[$SEARCHCOL];
                        $resStrData[] = array('CHK_VAL'=> false, 'COL_DAT'=>$colData[$SEARCHCOL]);
                        $resColDataLst[] = $colData;
                    }
                }
            }else if(count($seldata) === 1 && $seldata[0] === ' '){
                $resColDataLst  = array();
                foreach($SEARCHCOLDATALST as $key => $colData){
                    if( $colData[$SEARCHCOL] === ''){
                        $resColLst[] = $colData[$SEARCHCOL];
                        $resStrData[] = array('CHK_VAL'=> false, 'COL_DAT'=>$colData[$SEARCHCOL]);
                        $resColDataLst[] = $colData;
                    }
                }
            }else{
                $resColDataLst  = array();
                foreach($SEARCHCOLDATALST as $key => $colData){
                    if (in_array($colData[$SEARCHCOL], $seldata)) {
                        $resColLst[] = $colData[$SEARCHCOL];
                        $resStrData[] = array('CHK_VAL'=> false, 'COL_DAT'=>$colData[$SEARCHCOL]);
                        $resColDataLst[] = $colData;
                    }
                }
            }
        }else{
            $resColDataLst  = array();
            foreach($SEARCHCOLDATALST as $key => $colData){
                if( stripos($colData[$SEARCHCOL],$SEARCHKEY) !== false){
                    $resColLst[] = $colData[$SEARCHCOL];
                    $resStrData[] = array('CHK_VAL'=> false, 'COL_DAT'=>$colData[$SEARCHCOL]);
                    $resColDataLst[] = $colData;
                }
            }
        }
    }
    $SEARCHCOLDATALST = $resColDataLst;
}

$resColDataLst = $SEARCHCOLDATALST;//COLDATALST
$rtn = array(
    'COLDATALST'       => $COLDATALST,
    'COLSELDATA'       => $COLSELDATA,
    'RESCOL'           => $resColLst,
    'RESCOLDATALST'    => $resColDataLst,
    'RESSTRDATA'       => $resStrData,
    'csv5_formula'     => $csv5_formula,
    'FILE'             => $FILE,
    'FORMULA'          => $FORMULA
);
echo(json_encode($rtn));

