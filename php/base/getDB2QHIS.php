<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : クエリの実行、作成、編集履歴取得、更新、挿入
 * PROGRAM ID     : getDB2QHIS.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/11/08
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("fnDB2QHIS.php");

/*
 * 変数
 */
$rtn   = 0;
$msg   = '';
$PROC  = (isset($_POST['PROC'])?$_POST['PROC']:'');
$QRYNM = (isset($_POST['QRYNM'])?$_POST['QRYNM']:'');

/*
 * 処理
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}

//新規作成するときの場合、クエリの作成者、作成日、作成時刻を挿入
if($PROC === 'ADD'){
    if($rtn === 0){
        $rs = fnGetDB2QHIS($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            if(count($rs['data'])>0){
                $rs = fnDelDB2QHIS($db2con,$QRYNM);
                if($rs['result']!== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }
            }
        }
    }
    if($rtn === 0){
        $rs = fnInsDB2QHIS($db2con,$QRYNM,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if($rs['result']!== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
}
//編集するときの場合、クエリの更新者、更新日、更新時刻を挿入
else if($PROC === 'EDT'){
    if($rtn === 0){
        $rs = fnGetDB2QHIS($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            if(count($rs['data'])===0){
                $rs = fnInsDB2QHIS($db2con,$QRYNM,'');
                if($rs['result']!== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }
            }
        }
    }
    if($rtn === 0){
        $rs = fnUpdDB2QHISEdt($db2con,$QRYNM);
        if($rs['result']!== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
}
//実行するときの場合、クエリの最終者、最終実行日、最終実行時刻を更新
else if($PROC === 'EXE'){
    if($rtn === 0){
        $rs = fnGetDB2QHIS($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            if(count($rs['data'])===0){
                $rs = fnInsDB2QHIS($db2con,$QRYNM,'');
                if($rs['result']!== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }
            }
        }
    }
    if($rtn === 0){
        $rs = fnUpdDB2QHISExe($db2con,$QRYNM,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if($rs['result']!== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
}
//削除するときの場合、クエリの履歴情報削除
else if($PROC === 'DEL'){
    if($rtn === 0){
        $rs = fnDelDB2QHIS($db2con,$QRYNM);
        if($rs['result']!== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
}
//MSM add for クエリーの実行情報を初期化 20190405
//実行するときの場合、クエリの 実行回数：DQEKAI,最終実行者：DQEUSR,最終実行日：DQEDAY,最終実行時刻：DQETM,実行時間最小 : DQTMAX,実行時間最大 : DQTMIN,実行時間平均」:DQTAVG を更新
else if($PROC === 'RESET'){
    if($rtn === 0){
        $rs = fnGetDB2QHIS($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            if(count($rs['data'])===0){
                $rs = fnInsDB2QHIS($db2con,$QRYNM,'');
                if($rs['result']!== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }
            }
        }
    }
    if($rtn === 0){
        $rs = fnUpdDB2QHISReset($db2con,$QRYNM,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if($rs['result']!== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
}
cmDb2Close($db2con);
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));
