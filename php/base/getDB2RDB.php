<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$RDBNM = (isset($_POST['RDBNM'])?$_POST['RDBNM']:'');
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}

if($rtn === 0){
    $rs = fnGetDB2RDB($db2con,$RDBNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }

}
cmDb2Close($db2con);


/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'DATA'        => umEx($data),
    'RTN'           => $rtn,
    'MSG'           => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 区画マスター取得
*-------------------------------------------------------*
*/

function fnGetDB2RDB($db2con,$RDBNM){

    $data = array();

    $params = array();

    $rs = true;
    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SQL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SQL';
        }
    }
    if($rs === true)
    {
        $strSQL .= ' SELECT ';
        $strSQL .= '\''.RDB.'\' AS RDBNM ,';
        $strSQL .= '\''.RDB.'\' AS RDBHNM ,';
        $strSQL .= '\''.RDB_USER.'\' AS RDBUSR ,';
        $strSQL .= '\''.RDB_PASSWORD.'\' AS RDBPASS';
        $strSQL .= ' FROM ';
        $strSQL .= '     sysibm/sysdummy1 ';
        $strSQL .= ' UNION ';
        $strSQL .= ' SELECT ';
        $strSQL .= '     RDBNM, ';
        $strSQL .= '     RDBHNM, ';
        $strSQL .= '     RDBUSR, ';
        $strSQL .= '     DECRYPT_CHAR(RDBPASS) AS RDBPASS ';
        $strSQL .= ' FROM ';
        $strSQL .= '     DB2RDB ';
        if($RDBNM !== ''){
            $strSQL .= ' WHERE RDBNM = ? ';
            array_push($params,$RDBNM);
        }
        //e_log("data akz".$strSQL.print_r($params,true));
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    }
    return $data;
}
