<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : 共通
* PROGRAM NAME   : フィールド情報取得【テーブル：SYSCOLUMN2】
* PROGRAM ID     : getFieldInfo.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/04/11
* MODIFY DATE    : 
* ============================================================
**/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
 * 変数
 */
$rtn = 0;
$msg = '';
$rs = array();
$SEARCHDATA     = (isset($_POST['SEARCHDATA'])?$_POST['SEARCHDATA']:'');
$TABLE_NAME     = (isset($_POST['TABLE_NAME'])?$_POST['TABLE_NAME']:'');
$TABLE_SCHEMA   = (isset($_POST['TABLE_SCHEMA'])?$_POST['TABLE_SCHEMA']:'');
$LIBLIST        = json_decode($_POST['LIBLIST'],true);
$D1NAME         = json_decode($_POST['D1NAME'],true);//MSM add 
$start          = (isset($_POST['start'])?$_POST['start']:'');
$length         = (isset($_POST['length'])?$_POST['length']:'');
$sort           = (isset($_POST['sort'])?$_POST['sort']:'');
$sortDir        = (isset($_POST['sortDir'])?$_POST['sortDir']:'');
$newField       = json_decode($_POST['newField'],true);//MSM add 
$allcount       = 0;
$ALLSHOW = array();
/*
 * 処理
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

$RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
    $db2conRDB = $db2con;
}else{
    $db2conRDB = cmDB2ConRDB();
}

function combineWithKeys($array1, $array2){
    foreach($array1 as $key=>$value) $array2[$key] = $value;
    asort($array2);
    return $array2;
} 
if($TABLE_NAME == "FDB2CSV5"){
    //MSM add for new add data search field
    if($rtn === 0){
        //$paramFILTYP=($filCnt+1).'-0';
        //$FILE = 'K';
        $rs = getFDB2CSV5($db2conRDB,$D1NAME,$SEARCHDATA,$LIBLIST,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            if($SEARCHDATA == ''){
                $CSV5 = umEx($rs['data']);
                /*if(count($CSV5) === 0){
                    $data = $newField;
                }else{
                    $data = $CSV5 + $newField;
                }*/
                 $data = $newField;
                 e_log("getField1***".print_r($data,true));
                $allcount = count($data);
            }else{
                $CSV5 = umEx($rs['data']);
                $S_data =array();
                    foreach($newField as $value){
                        if (strpos($value['COLUMN_NAME'],$SEARCHDATA) !== false || strpos($value['DDS_TYPE'],$SEARCHDATA) !== false || strpos($value['COLUMN_HEADING'],$SEARCHDATA) !== false ) {
                            array_push($S_data,$value);
                            
                        }
                    }
                    if(count($CSV5) === 0){
                        $data = $S_data;
                        e_log("getField2***".print_r($data,true));
                    }else{
                        //$data = combineWithKeys($CSV5, $S_data);
                        $data = $S_data;
                        e_log("getField3***".print_r($data,true));
                    }
                //$data = combineWithKeys($CSV5, $newField);
                $allcount = count($data); 
            }

        }

    }
}else{
    if($rtn === 0){
        $rs = getSYSCOLUMN2($db2conRDB,$SEARCHDATA,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = umEx($rs['data']);
            e_log("getField4***".print_r($data,true));
        }
    }
    if($rtn === 0){
        $rs = fnGetAllCount($db2conRDB,$SEARCHDATA,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
}




if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
    cmDb2Close($db2conRDB);
}
cmDb2Close($db2con);
$rtn = array(
    'iTotalRecords' => $allcount,
    'DATA' => $data,
    'CSV5' => $CSV5,   
    'RTN' => $rtn,
    'MSG' => $msg,
    'newField' => $newField
);
echo(json_encode($rtn));
/**
  *---------------------------------------------------------------------------
  * フィールドデータを取得
  *---------------------------------------------------------------------------
  **/
function getSYSCOLUMN2($db2con,$SEARCHDATA,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST,$start = '',$length = '',$sort = '',$sortDir = ''){
    $data = array();
    $params = array();
    if(CRTQTEMPTBL_FLG === 1){
        $paramSQL  = ' SELECT ';
        $paramSQL .= '     * ';
        $paramSQL .= ' FROM ';
        $paramSQL .= SYSCOLUMN2 ;
        $paramSQL .= ' WHERE ';
        if(is_array($LIBLIST) && count($LIBLIST)>0){
            $paramSQL .='  TABLE_SCHEMA IN ( ';
            //e_log('【削除】ライブラリーリスト：'. print_r($LIBLIST,true));
            //e_log('【削除】ライブラリーリストデータ：'. implode(',\' ',$LIBLIST));
            $paramSQL .= ' \''. join('\', \'',$LIBLIST).' \'';
            if(($TABLE_SCHEMA == '' || strtoupper($TABLE_SCHEMA) === '*LIBL' || strtoupper($TABLE_SCHEMA) === '*USRLIBL') === false){
                $paramSQL .= ', \''.$TABLE_SCHEMA . '\' ';
            }
            $paramSQL .= ' ) ';
            if($TABLE_NAME !== ''){
                $paramSQL .= ' AND ';
            }
        }else{
            if(($TABLE_SCHEMA == '' || strtoupper($TABLE_SCHEMA) === '*LIBL' || strtoupper($TABLE_SCHEMA) === '*USRLIBL') === false){
                $paramSQL .='  TABLE_SCHEMA = \''.$TABLE_SCHEMA.'\'';
            }
            if($TABLE_NAME !== ''){
                $paramSQL .= ' AND ';
            }
        }
        if($TABLE_NAME !== ''){
            $paramSQL .='  TABLE_NAME =  \''.$TABLE_NAME . '\'';
        }
        createTmpFil($db2con,'',SYSCOLUMN2,'','SYSCOLUMN2',$paramSQL);
    }
    $strSQL = '';
    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM( ';
    $strSQL .= '    SELECT \'\' AS SEQ ';
    $strSQL .= '       ,B.SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
    $strSQL .= '       ,B.LENGTH ';
    $strSQL .= '       ,CASE B.DDS_TYPE';
    $strSQL .= '         WHEN \'H\' THEN \'A\'';
    $strSQL .= '         WHEN \'J\' THEN \'0\'';
    $strSQL .= '         WHEN \'E\' THEN \'0\'';
    $strSQL .= '         ELSE B.DDS_TYPE ';
    $strSQL .= '         END AS DDS_TYPE';
    $strSQL .= '       ,B.NUMERIC_SCALE ';
    if(SYSCOLCFLG==='1'){
        $strSQL .= '       ,B.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .= '       ,B.COLUMN_HEADING ';
    }
    $strSQL .= '       ,\'\' AS FORMULA ';
    $strSQL .= '       ,\'\' AS SORTNO ';
    $strSQL .= '       ,\'\' AS SORTTYP ';
    $strSQL .= '       ,\'\' AS EDTCD ';
    $strSQL .= '       ,B.ORDINAL_POSITION ';
    $strSQL .= '      , ROWNUMBER() OVER( ';
    if($sort != ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.ORDINAL_POSITION ASC ';
    }
    $strSQL .= '            ) AS ROWNUM ';
    if(CRTQTEMPTBL_FLG === 1){
        $strSQL .=' FROM QTEMP/SYSCOLUMN2 B ';
    }else{
        $strSQL .=' FROM '. SYSCOLUMN2 .' B ';
    }
    $strSQL .=' WHERE B.TABLE_SCHEMA = ? ';
    $strSQL .=' AND B.TABLE_NAME = ? ';
//    $strSQL .=' AND B.DDS_TYPE NOT IN(\'5\')';
    if($SEARCHDATA != ''){
        $strSQL .= ' AND ';
        $strSQL .= '  (B.SYSTEM_COLUMN_NAME like ? ';
        $strSQL .= ' OR B.DDS_TYPE like ? ';
        $strSQL .= ' OR ';
        if(SYSCOLCFLG==='1'){
            $strSQL .=' B.COLUMN_TEXT';
        }else{
            $strSQL .=' B.COLUMN_HEADING';
        }
        $strSQL .=' like ?) ';
    }
    $strSQL .= ' )AS A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.ROWNUM BETWEEN ? AND ? ';
    }
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
                $params = array($value,$TABLE_NAME);
                if($SEARCHDATA !== ''){
                    array_push($params,'%'.$SEARCHDATA.'%');
                    array_push($params,'%'.$SEARCHDATA.'%');
                    array_push($params,'%'.$SEARCHDATA.'%');
                }
                 if(($start != '')&&($length != '')){
                    array_push($params,$start + 1);
                    array_push($params,$start + $length);
                }
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        unset($row[count($row)-1]);
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
            $data = umEX($data,true);
            $data = array('result' => true,'data' => $data);
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME);
            if($SEARCHDATA !== ''){
                array_push($params,'%'.$SEARCHDATA.'%');
                array_push($params,'%'.$SEARCHDATA.'%');
                array_push($params,'%'.$SEARCHDATA.'%');
            }
             if(($start != '')&&($length != '')){
                array_push($params,$start + 1);
                array_push($params,$start + $length);
            }
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    unset($row[count($row)-1]);
                    $data[] = $row;
                }
                $data = umEX($data,true);
                $data = array('result' => true,'data' => $data);
            }
        }
     }
    return $data;
}
/**
  *---------------------------------------------------------------------------
  * フィールドデータカウント取得
  * 
  * RESULT
  *    フィールドデータカウント
  *
  *---------------------------------------------------------------------------
  **/
function fnGetAllCount($db2con,$SEARCHDATA,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .= ' SELECT COUNT(A.COLUMN_NAME) AS COUNT';
    $strSQL .= ' FROM( ';
    $strSQL .= '    SELECT \'\' AS SEQ ';
    $strSQL .= '       ,B.SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
    $strSQL .= '       ,B.LENGTH ';
    $strSQL .= '       ,CASE B.DDS_TYPE';
    $strSQL .= '         WHEN \'H\' THEN \'A\'';
    $strSQL .= '         WHEN \'J\' THEN \'0\'';
    $strSQL .= '         WHEN \'E\' THEN \'0\'';
    $strSQL .= '         ELSE B.DDS_TYPE ';
    $strSQL .= '         END AS DDS_TYPE';
    $strSQL .= '       ,B.NUMERIC_SCALE ';
    $strSQL .= '       ,B.COLUMN_HEADING ';
    $strSQL .= '       ,\'\' AS FORMULA ';
    $strSQL .= '       ,\'\' AS SORTNO ';
    $strSQL .= '       ,\'\' AS SORTTYP ';
    $strSQL .= '       ,\'\' AS EDTCD ';
    $strSQL .= '       ,B.ORDINAL_POSITION ';
    $strSQL .=' FROM ' . SYSCOLUMN2 . ' B ';
    $strSQL .=' WHERE B.TABLE_SCHEMA = ? ';
    $strSQL .=' AND B.TABLE_NAME = ? ';
    //$strSQL .=' AND B.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\')';
//    $strSQL .=' AND B.DDS_TYPE NOT IN(\'5\')';
    if($SEARCHDATA != ''){
        $strSQL .= ' AND ';
        $strSQL .= '  (B.SYSTEM_COLUMN_NAME like ? ';
        $strSQL .= ' OR B.DDS_TYPE like ? ';
        $strSQL .= ' OR ';
        if(SYSCOLCFLG==='1'){
            $strSQL .= ' B.COLUMN_TEXT ';
        }else{
            $strSQL .= ' B.COLUMN_HEADING ';
        }
        $strSQL .= ' like ?) ';
    }
    $strSQL .= ' )AS A ';
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
                $params = array($value,$TABLE_NAME);
                if($SEARCHDATA !== ''){
                    array_push($params,'%'.$SEARCHDATA.'%');
                    array_push($params,'%'.$SEARCHDATA.'%');
                    array_push($params,'%'.$SEARCHDATA.'%');
                }
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row['COUNT'];
                    }
                    if($data[0] > 0){
                        break;
                    }else{
                        $data = array();
                    }
                }
            }
            $data = array('result' => true,'data' => $data[0]);
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME);
            if($SEARCHDATA !== ''){
                array_push($params,'%'.$SEARCHDATA.'%');
                array_push($params,'%'.$SEARCHDATA.'%');
                array_push($params,'%'.$SEARCHDATA.'%');
            }
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row['COUNT'];
                }
                $data = array('result' => true,'data' => $data[0]);
            }
        }
     }
    return $data;
}
// 【編集】結果フィールドデータ取得クエリー
function getFDB2CSV5($db2con,$D1NAME,$SEARCHDATA,$LIBLIST,$start,$length,$sort,$sortDir){//$FILTYP,
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT   ';
    $strSQL .=  '   (CASE   A.D5CSEQ'; 
    $strSQL .=  '       WHEN 0 THEN 99999 ';
    $strSQL .=  '       ELSE A.D5CSEQ END) AS SEQ '; 
    $strSQL .=  '           ,A.D5FLD AS COLUMN_NAME '; 
    $strSQL .=  '           ,A.D5LEN  AS LENGTH '; 
    $strSQL .=  '           ,A.D5TYPE AS DDS_TYPE ';
    $strSQL .=  '           ,(CASE ';
    $strSQL .=  '               WHEN A.D5TYPE = \'P\' '; 
    $strSQL .=  '                   OR A.D5TYPE = \'B\' '; 
    $strSQL .=  '                   OR A.D5TYPE = \'S\' ';
    $strSQL .=  '               THEN A.D5DEC ';
    $strSQL .=  '               ELSE \'\' ';
    $strSQL .=  '           END) AS NUMERIC_SCALE '; 
    $strSQL .=  '           ,A.D5HED  AS COLUMN_HEADING '; 
    $strSQL .=  '           ,A.D5EXP  AS FORMULA ';
    $strSQL .=  '           ,A.D5EXPD  AS FORMULA_D ';//MSM add 
    $strSQL .=  '           ,A.D5RSEQ AS SORTNO  ';
    $strSQL .=  '           ,A.D5RSTP AS SORTTYP '; 
    $strSQL .=  '           ,A.D5WEDT AS EDTCD '; 
    $strSQL .=  '           ,A.D5ASEQ AS ORDINAL_POSITION '; 
    //$strSQL .=  '           , \''.$FILTYP.'\' AS FILTYP  ';
    //$strSQL .=  '           , \''.$FILE.'\' AS FILE  ';

    $strSQL .=  '   FROM ';    
    $strSQL .=  '           FDB2CSV5 A ';
    $strSQL .=  '   WHERE ';   
    $strSQL .=  '           A.D5NAME = ? ';
    //e_log(" getFDB2CSV5 SEARCHDATA MYAT".$SEARCHDATA);
    if($SEARCHDATA != ''){
        $strSQL .= ' AND ';
        $strSQL .= '  (A.D5FLD like ? ';//COLUMN_NAME
        $strSQL .= ' OR A.D5TYPE like ? ';//DDS_TYPE
        $strSQL .= ' OR A.D5HED like ?)';//COLUMN_HEADING

    }
    //抽出範囲指定
    // if(($start != '')&&($length != '')){
    //     $strSQL .= ' WHERE ROWNUM BETWEEN ? AND ? ';
    // }
    if($sort != ''){
        $strSQL .= ' ORDER BY '.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .=  '   ORDER BY '; 
        $strSQL .=  '           SEQ '; 
        $strSQL .=  '          ,ORDINAL_POSITION ';
    }
        
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($D1NAME);

        if($SEARCHDATA !== ''){
            array_push($params,'%'.$SEARCHDATA.'%');
            array_push($params,'%'.$SEARCHDATA.'%');
            array_push($params,'%'.$SEARCHDATA.'%');
        }
        // if(($start != '')&&($length != '')){
        //     array_push($params,$start + 1);
        //     array_push($params,$start + $length);
        // }
        e_log("getFDB2CSV5 MYAT => ".$strSQL.print_r($params,true));
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                if(($row['DDS_TYPE'] === 'P' || $row['DDS_TYPE'] === 'B' || $row['DDS_TYPE'] === 'S') === false){
                    $row['NUMERIC_SCALE'] = '';
                }
                if($row['SORTNO'] == 0){
                    $row['SORTNO'] = '';
                }
                if($row['SEQ'] == 99999){
                    $row['SEQ'] = '';
                }
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
