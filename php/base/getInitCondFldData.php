<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$BASEFIELDINFO     = json_decode($_POST['BASEFIELDINFO'],true);
$D1NAME=(isset($_POST['D1NAME']))?$_POST['D1NAME']:'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$outputfiledata=array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
if(count($BASEFIELDINFO)>0){
    foreach ($BASEFIELDINFO as $FLDINFO) {
        foreach($FLDINFO['STRDATA'] as $FLDDATA){
            $fieldInfo = array();
            if($FLDINFO['D2FILID'] === 0){
                $fieldInfo['FILTYP']   = 'P';
                $fieldInfo['FILTYPDESC']= $FLDINFO['D2FDESC'];
            }else if($FLDINFO['D2FDESC'] === '結果フィールド'){
                $fieldInfo['FILTYP']   = 'K';
                $fieldInfo['FILTYPDESC']= $FLDINFO['D2FDESC']; 
            }else{
                $fieldInfo['FILTYP']   = 'S'.$FLDINFO['D2FILID'];
                $fieldInfo['FILTYPDESC']= $FLDINFO['D2FDESC'];
            }
            $fieldInfo['FILID']            = $FLDINFO['D2FILID'];
            $fieldInfo['COLUMN_NAME']      = $FLDDATA['COLUMN_NAME'];
            $fieldInfo['COLUMN_HEADING']   = $FLDDATA['COLUMN_HEADING'];
            $fieldInfo['DDS_TYPE']         = $FLDDATA['DDS_TYPE'];
            $fieldInfo['LENGTH']           = $FLDDATA['LENGTH'];
            $fieldInfo['NUMERIC_SCALE']    = $FLDDATA['NUMERIC_SCALE'];
            $fieldInfo['FIELDVAL']         = $fieldInfo['FILTYP'].'.'.$FLDDATA['COLUMN_NAME'];
            $fieldInfo['FIELDDESC']        = $FLDDATA['COLUMN_NAME'];//MSM add COLUMN_HEADING ."【".$FLDDATA['COLUMN_HEADING']."】"
            $fieldInfo['ORDINAL_POSITION'] = $FLDDATA['ORDINAL_POSITION'];
            //$fieldInfo['MAX_WIDTH']        = strlen($fieldInfo['FIELDDESC']);
            
            $data[] = $fieldInfo;
        }
    }
}

/*$max_arr = array();
$max_arr = array_reduce($data, function ($a, $b) {
    return @$a['MAX_WIDTH'] > $b['MAX_WIDTH'] ? $a : $b ;
});
//error_log("getInitCondFldData data max_arr => ".print_r($max_arr,true));
error_log('getInitCondFldData maxwidth: ' . $max_arr["MAX_WIDTH"]);
foreach ($data as &$value) {
    $value["MAX_WIDTH"] = $max_arr["MAX_WIDTH"];
}

error_log("getInitCondFldData data after => ".print_r($data,true));*/
//error_log('Current PHP version: ' . phpversion());

//出力ファイルのデータを入れる
 if($rtn===0){
    $r=fnGetFDB2CSV1($db2con,$D1NAME);
    if($r['result']!==true){
        $rtn=1;
        $msg=$r['result'];
    }else{
        $outputfiledata=$r['data'];
    }
}

cmDb2Close($db2con);
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG'=>$msg,
    'DATA' => umEx($data),
    'licensePivot' =>$licensePivot,
    // 'licenseSeigyoSpecialFlg'=>$licenseSeigyoSpecialFlg,
    'licenseSeigyo'=>$licenseSeigyo,
    'licenseFileOut'=>$licenseFileOut,
    'WUAUTH' =>$WUAUTH,
    'OUTPUTFDATA'=>$outputfiledata
);
echo(json_encode($rtn));

//出力ファイルのためデータをもらう
function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.D1OUTJ,A.D1OUTA,A.D1OUTLIB,A.D1OUTFIL,A.D1OUTR,A.D1NAME ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ?';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}

