<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : 共通
* PROGRAM NAME   : フィールド情報取得【テーブル：SYSCOLUMN2】
* PROGRAM ID     : getInitFieldFrmData.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/04/11
* MODIFY DATE    : 
* ============================================================
**/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
 * 変数
 */
$rtn = 0;
$msg = '';
$data = array();
$rs = array();
$BASEFILEINFO   = json_decode($_POST['BASEFILEINFO'],true);
$RTNINITBASEFLD = array(); 
$TABLE_SCHEMA   = $_POST['TABLE_SCHEMA'];
$FILECNT        = (isset($_POST['FILECNT'])?$_POST['FILECNT']:'');
$PROC           = (isset($_POST['PROC'])?$_POST['PROC']:'');


$STRDATA1 =  array();
$STRDATA2 =  array();
$STRDATA3 =  array();

//全てチェック
$allShow=array();
/*
 * 処理
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
if($PROC !== 'ADD'){
    //クエリー存在チェック
    if($rtn === 0){
        $rs = fnGetFDB2CSV1($db2con,$BASEFILEINFO['D1NAME']);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            if(count($rs['data']) === 0){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('クエリー'));
            }else{
                $ori_FDB2CSV1 = $rs['data'][0];
            }
        }
    }
}
//  チェックライブラリー又ファイルを変更するか？
if($rtn === 0){
    $compLibl = $BASEFILEINFO['D1FILLIB'];
    $compFile = $BASEFILEINFO['D1FILE'] ;
    $oriLibl  = $ori_FDB2CSV1['D1FILLIB'];
    $oriFile  = $ori_FDB2CSV1['D1FILE'];
    $orilibList = preg_replace('/\s(?=\s)/', '', $ori_FDB2CSV1['D1LIBL']);
    $complibList = join(' ',$BASEFILEINFO['SELLIBLIST']);
    $chkChgFileInfo = chkChangeFile($oriLibl,$oriFile,$compLibl,$compFile,$orilibList,$complibList);
}
$RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
if($RDBNM === ''){
    if($rtn === 0){
        $paramFILTYP='0-P';
		$FILE = 'P';
        if($PROC === 'EDIT'){
            //e_log("getFDB2CSV2");
            $rs = getFDB2CSV2($db2con,$BASEFILEINFO['D1NAME'],0,$BASEFILEINFO['D1FILE'] ,$BASEFILEINFO['D1FILLIB'],$BASEFILEINFO['SELLIBLIST'],$chkChgFileInfo,$paramFILTYP,$FILE);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
                e_log("Init1***".print_r($data,true));
            }
        }else{
            $rs = getSYSCOLUMN2($db2con,$BASEFILEINFO['D1FILE'] ,$BASEFILEINFO['D1FILLIB'],$BASEFILEINFO['SELLIBLIST'],$paramFILTYP,$FILE);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
            }
        }
    }
}else{
    if($rtn === 0){
        $paramFILTYP='0-P';
		$FILE = 'P';
        if($PROC === 'EDIT'){
            $rs = getFDB2CSV2RDB($db2con,$BASEFILEINFO['D1NAME'],0,$BASEFILEINFO['D1FILE'] ,$BASEFILEINFO['D1FILLIB'],$BASEFILEINFO['SELLIBLIST'],$chkChgFileInfo,$paramFILTYP,$FILE);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
                e_log("Init2***".print_r($data,true));
            }
            cmDb2Close($db2con);
            $db2con = cmDb2Con();
            cmSetPHPQUERY($db2con);
        }else{
            $db2conRDB = cmDB2ConRDB();
            $rs = getSYSCOLUMN2($db2conRDB,$BASEFILEINFO['D1FILE'] ,$BASEFILEINFO['D1FILLIB'],$BASEFILEINFO['SELLIBLIST'],$paramFILTYP,$FILE);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
            }
            cmDb2Close($db2conRDB);
        }
    }
}
$PRIMARYD = $data;
if($rtn === 0){
    $fileInfoData['D2FILID'] = 0;
    $fileInfoData['D2FDESC'] = 'プライマリ';
    $fileInfoData['FILENM'] = $BASEFILEINFO['D1FILE'];
    $fileInfoData['LIBNM'] = $BASEFILEINFO['D1FILLIB'];
    $fileInfoData['FILTYP'] = 'P';
    $fileInfoData['STRDATA'] = $data;
    $STRDATA1 =  $data;
    $RTNINITBASEFLD[] = $fileInfoData;
    $allShow[]=$data;//全てチェック
    //e_log("allShow❶ =>".print_r($allShow,true));
}
$filCnt = 0;
if($rtn === 0){
    $REFFILE = $BASEFILEINFO['REFFILE'];
    if(count($REFFILE) > 0){
        $rs = fnGetBREFTBL($db2con,$BASEFILEINFO['D1NAME']);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $oriBREFFIL = $rs['data'];
            if(count($oriBREFFIL) === 0){
                $chgFileCnt = true;
            }else{
                $chgFileCnt = false;
            }
        }
        if($rtn === 0){
            foreach ($REFFILE as $RFILVAL) {
                //  チェックライブラリー又ファイルを変更するか？
                $chgFileFlg = $chgFileCnt;
                if($chgFileFlg === false){
                    if($rtn === 0){
                        foreach($oriBREFFIL as $oriBREFFILVAL){
                            if($RFILVAL['REFIDX'] == $oriBREFFILVAL['RTRSEQ']){
                                $compLibl = $RFILVAL['RTRLIB'];
                                $compFile = $RFILVAL['RTRFIL'];
                                $oriLibl  = $oriBREFFILVAL['RTRLIB'];
                                $oriFile  = $oriBREFFILVAL['RTRFIL'] ;
                                $chgFileFlg = chkChangeFile($oriLibl,$oriFile,$compLibl,$compFile,$orilibList,$complibList);
                                break;
                            }else{
                                $chgFileFlg = true;
                            }
                        }
                    }    
                }
                if($RDBNM === ''){
                    $paramFILTYP=$RFILVAL['REFIDX'].'-R';
					$FILE = 'S'.$RFILVAL['REFIDX'];
                    if($PROC === 'EDIT'){
                        $rs = getFDB2CSV2($db2con,$BASEFILEINFO['D1NAME'],$RFILVAL['REFIDX'],$RFILVAL['RTRFIL'] ,$RFILVAL['RTRLIB'],$BASEFILEINFO['SELLIBLIST'],$chgFileFlg,$paramFILTYP,$FILE);
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = showMsg($rs['result']);
                            $break;
                        }else{
                            $data = $rs['data'];
                        }
                    }else{
                        $rs = getSYSCOLUMN2($db2con,$RFILVAL['RTRFIL'] ,$RFILVAL['RTRLIB'],$BASEFILEINFO['SELLIBLIST'],$paramFILTYP,$FILE);
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = showMsg($rs['result']);
                            break;
                        }else{
                            $data = $rs['data'];
                        }
                    }
                }else{
                    $paramFILTYP=$RFILVAL['REFIDX'].'-R';
					$FILE = 'S'.$RFILVAL['REFIDX'];
                    if($PROC === 'EDIT'){
                        $rs = getFDB2CSV2RDB($db2con,$BASEFILEINFO['D1NAME'],$RFILVAL['REFIDX'],$RFILVAL['RTRFIL'] ,$RFILVAL['RTRLIB'],$BASEFILEINFO['SELLIBLIST'],$chgFileFlg,$paramFILTYP,$FILE);
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = showMsg($rs['result']);
                            $break;
                        }else{
                            $data = $rs['data'];
                        }
                        cmDb2Close($db2con);
                        $db2con = cmDb2Con();
                        cmSetPHPQUERY($db2con);
                    }else{
                        $db2conRDB = cmDB2ConRDB();
                        $rs = getSYSCOLUMN2($db2conRDB,$RFILVAL['RTRFIL'] ,$RFILVAL['RTRLIB'],$BASEFILEINFO['SELLIBLIST'],$paramFILTYP,$FILE);
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = showMsg($rs['result']);
                            break;
                        }else{
                            $data = $rs['data'];
                        }
                        cmDb2Close($db2conRDB);
                    }
                }
                if($rtn === 0){
                    $filCnt = $RFILVAL['REFIDX'];
                    $fileInfoData['D2FILID'] = $RFILVAL['REFIDX'];
                    $fileInfoData['D2FDESC'] = '参照'.$RFILVAL['REFIDX'];
                    $fileInfoData['FILENM']  = $RFILVAL['RTRFIL'];
                    $fileInfoData['LIBNM']   = $RFILVAL['RTRLIB'];
                    $fileInfoData['FILTYP']  = 'R';
                    $fileInfoData['STRDATA'] = $data;
                    $RTNINITBASEFLD[] = $fileInfoData;
                    $allShow[]=$data;//全てチェック
                    //e_log("allShow➋ =>".print_r($allShow,true));
                }
            }
        }
    }
}
if($rtn === 0){
    $paramFILTYP=($filCnt+1).'-0';
    e_log("paramFILTYP => ".$paramFILTYP);
	$FILE = 'K';
    if($PROC == 'EDIT'){    
        $rs = getFDB2CSV5($db2con,$BASEFILEINFO['D1NAME'],$paramFILTYP,$FILE);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }else{
        $rs = getSYSCOLUMN2($db2con,'' ,'','',$paramFILTYP,$FILE);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}
if($rtn === 0){
    $fileInfoData['D2FILID'] = $filCnt+1;
    $fileInfoData['D2FDESC'] = '結果フィールド';
    $fileInfoData['FILENM']  = '';
    $fileInfoData['LIBNM']   = '';
    $fileInfoData['FILTYP']  = '0';
    $fileInfoData['STRDATA'] = $data;
    $RTNINITBASEFLD[] = $fileInfoData;
    $allShow[]=$data;//全てチェック
    //e_log("allShow❸ =>".print_r($allShow,true));
}

cmDb2Close($db2con);
$rtn = array(
    'DATA' => $RTNINITBASEFLD,
    'ALLSHOW'=>$allShow,//全てチェック
    'RTN' => $rtn,
    'MSG' => $msg,
    'FILECNT' => $FILECNT,
	'SYSQRYCOL' => SYSQRYCOL,
	'PRIMARYD' => $PRIMARYD,
    'BASEFILEINFO' => $BASEFILEINFO
);
echo(json_encode($rtn));
/**
  *---------------------------------------------------------------------------
  * 区分データを取得
  * 
  * RESULT
  *    01：区分データリスト
  * 
  * @param String  func       区分種別キー値
  *
  *---------------------------------------------------------------------------
  **/
function getSYSCOLUMN2($db2con,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST,$FILTYP,$FILE){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL =' SELECT \'\' AS SEQ ';
    $strSQL .='       ,A.SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
    $strSQL .='       ,A.LENGTH ';
    $strSQL .='       ,CASE A.DDS_TYPE';
    $strSQL .='         WHEN \'H\' THEN \'A\'';
    $strSQL .='         WHEN \'J\' THEN \'0\'';
    $strSQL .='         WHEN \'E\' THEN \'0\'';
    $strSQL .='         ELSE A.DDS_TYPE ';
    $strSQL .='         END AS DDS_TYPE';
    $strSQL .='       ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG==='1'){
        $strSQL .='       ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .='       ,A.COLUMN_HEADING ';
    }
    $strSQL .='       ,\'\' AS FORMULA ';
    $strSQL .='       ,\'\' AS SORTNO ';
    $strSQL .='       ,\'\' AS SORTTYP ';
    $strSQL .='       ,\'\' AS EDTCD ';
    $strSQL .='       ,A.ORDINAL_POSITION ';
    $strSQL .='       , \''.$FILTYP.'\' AS FILTYP  ';
    $strSQL .='       , \''.$FILE.'\' AS FILE  ';

    $strSQL .=' FROM ' . SYSCOLUMN2 .' A ';
    $strSQL .=' WHERE A.TABLE_SCHEMA = ? ';
    $strSQL .=' AND A.TABLE_NAME = ? ';
    //$strSQL .=' AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\')';
//    $strSQL .=' AND A.DDS_TYPE NOT IN(\'5\')';
    $strSQL .=' ORDER BY A.ORDINAL_POSITION ';
    
    e_log("getSYSCOLUMN2 akz ".$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$TABLE_NAME);
               //e_log("getSYSCOLUMN2 MSM1 ".$strSQL.print_r($params,true));
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
            //$data = umEx($data,true);
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME);
            //e_log("getSYSCOLUMN2 MSM2 ".$strSQL.print_r($params,true));
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                //$data = umEx($data,true);
                $data = umEx($data);
                $data = array('result' => true,'data' => $data);
            }
        }
        
    }
    return $data;
}
// 【編集】結果フィールドデータ取得クエリー
function getFDB2CSV5($db2con,$D1NAME,$FILTYP,$FILE){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT   ';
    $strSQL .=  '   (CASE   A.D5CSEQ'; 
    $strSQL .=  '       WHEN 0 THEN 99999 ';
    $strSQL .=  '       ELSE A.D5CSEQ END) AS SEQ '; 
    $strSQL .=  '           ,A.D5FLD AS COLUMN_NAME '; 
    $strSQL .=  '           ,A.D5LEN  AS LENGTH '; 
    $strSQL .=  '           ,A.D5TYPE AS DDS_TYPE ';
    $strSQL .=  '           ,(CASE ';
    $strSQL .=  '               WHEN A.D5TYPE = \'P\' '; 
    $strSQL .=  '                   OR A.D5TYPE = \'B\' '; 
    $strSQL .=  '                   OR A.D5TYPE = \'S\' ';
    $strSQL .=  '               THEN A.D5DEC ';
    $strSQL .=  '               ELSE \'\' ';
    $strSQL .=  '           END) AS NUMERIC_SCALE '; 
    $strSQL .=  '           ,A.D5HED  AS COLUMN_HEADING '; 
    $strSQL .=  '           ,A.D5EXP  AS FORMULA ';
    $strSQL .=  '           ,A.D5EXPD  AS FORMULA_D ';//MSM add
    $strSQL .=  '           ,A.D5RSEQ AS SORTNO  ';
    $strSQL .=  '           ,A.D5RSTP AS SORTTYP '; 
    $strSQL .=  '           ,A.D5WEDT AS EDTCD '; 
    $strSQL .=  '           ,A.D5ASEQ AS ORDINAL_POSITION '; 
    $strSQL .=  '           , \''.$FILTYP.'\' AS FILTYP  ';
    $strSQL .=  '           , \''.$FILE.'\' AS FILE  ';

    $strSQL .=  '   FROM ';    
    $strSQL .=  '           FDB2CSV5 A ';
    $strSQL .=  '   WHERE ';   
    $strSQL .=  '           A.D5NAME = ? ';
    $strSQL .=  '   ORDER BY '; 
    $strSQL .=  '           SEQ '; 
    $strSQL .=  '          ,ORDINAL_POSITION ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($D1NAME);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                if(($row['DDS_TYPE'] === 'P' || $row['DDS_TYPE'] === 'B' || $row['DDS_TYPE'] === 'S') === false){
                    $row['NUMERIC_SCALE'] = '';
                }
                if($row['SORTNO'] == 0){
                    $row['SORTNO'] = '';
                }
                if($row['SEQ'] == 99999){
                    $row['SEQ'] = '';
                }
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function getFDB2CSV2($db2con,$D1NAME,$FILID,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST,$chkFileFlg,$FILTYP,$FILE){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT ';
    $strSQL .=  '         FDB2CSV2DATA.SEQ            AS SEQ ';
    $strSQL .=  '       , SYSTABDATA.COLUMN_NAME      AS COLUMN_NAME ';
    $strSQL .=  '       , SYSTABDATA.LENGTH           AS LENGTH ';
    $strSQL .=  '       , SYSTABDATA.DDS_TYPE         AS DDS_TYPE ';
    $strSQL .=  '       , SYSTABDATA.NUMERIC_SCALE    AS NUMERIC_SCALE ';
    if($chkFileFlg === true){
        $strSQL .=  '     ,SYSTABDATA.COLUMN_HEADING AS COLUMN_HEADING ';
    }else{
        $strSQL .=  '       ,(CASE ';
        $strSQL .=  '           WHEN FDB2CSV2DATA.COLUMN_HEADING IS NULL ';
        $strSQL .=  '           AND FDB2CSV2DATA.COLUMN_NAME  IS NULL ';
        $strSQL .=  '           THEN SYSTABDATA.COLUMN_HEADING ';
        $strSQL .=  '           ELSE FDB2CSV2DATA.COLUMN_HEADING ';
        $strSQL .=  '         END) AS COLUMN_HEADING ';
    }
    $strSQL .=  '       , FDB2CSV2DATA.FORMULA        AS FORMULA ';
    $strSQL .=  '       , FDB2CSV2DATA.SORTNO         AS SORTNO ';
    $strSQL .=  '       , FDB2CSV2DATA.SORTTYP        AS SORTTYP ';
    $strSQL .=  '       , FDB2CSV2DATA.EDTCD          AS EDTCD ';
    $strSQL .=  '       , SYSTABDATA.ORDINAL_POSITION AS ORDINAL_POSITION ';
    $strSQL .=  '       , \''.$FILTYP.'\' AS FILTYP  ';
    $strSQL .='       , \''.$FILE.'\' AS FILE  ';
    $strSQL .=  '   FROM  '; 
    $strSQL .=  '       (SELECT  ';
    $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
    $strSQL .=  '          ,A.LENGTH '; 
    $strSQL .=  '          ,CASE A.DDS_TYPE ';
    $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
    $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
    $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
    $strSQL .=  '            ELSE A.DDS_TYPE '; 
    $strSQL .=  '            END AS DDS_TYPE ';
    $strSQL .=  '          ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG==='1'){
        $strSQL .=  '          ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .=  '          ,A.COLUMN_HEADING ';
    }
    $strSQL .=  '          ,A.ORDINAL_POSITION '; 
    $strSQL .=  '       FROM ' . SYSCOLUMN2 .' A '; 
    $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
    $strSQL .=  '       AND A.TABLE_NAME = ? '; 
    //$strSQL .=  '       AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\') ';
//    $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
    $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ) SYSTABDATA ';
    $strSQL .=  '   LEFT JOIN ';
    $strSQL .=  '       (SELECT  ';
    $strSQL .=  '            (CASE A.D2CSEQ ';
    $strSQL .=  '            WHEN 0 THEN 99999 '; 
    $strSQL .=  '            ELSE A.D2CSEQ ';
    $strSQL .=  '            END) '; 
    $strSQL .=  '            AS SEQ '; 
    $strSQL .=  '           ,A.D2FLD  AS COLUMN_NAME ';
    $strSQL .=  '           ,A.D2HED  AS COLUMN_HEADING '; 
    $strSQL .=  '           ,\'\'     AS FORMULA ';
    $strSQL .=  '           ,CASE A.D2RSEQ ';
    $strSQL .=  '             WHEN 0 THEN 99999 ';
    $strSQL .=  '             ELSE A.D2RSEQ '; 
    $strSQL .=  '             END AS SORTNO ';
    $strSQL .=  '           ,A.D2RSTP AS SORTTYP '; 
    $strSQL .=  '           ,A.D2WEDT AS EDTCD ';
    $strSQL .=  '       FROM FDB2CSV2 A ';
    $strSQL .=  '       WHERE A.D2NAME = ?';
    $strSQL .=  '       AND   A.D2FILID = ? ';
    $strSQL .=  '       ORDER BY A.D2CSEQ,A.D2RSEQ ) FDB2CSV2DATA ';
    $strSQL .=  '   ON SYSTABDATA.COLUMN_NAME = FDB2CSV2DATA.COLUMN_NAME ';
    $strSQL .=  '   ORDER BY SEQ ,SORTNO, ORDINAL_POSITION ';
    
    //e_log('実行SQL　 '.$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$TABLE_NAME,$D1NAME,$FILID);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        if($row['SORTNO'] == 99999){
                            $row['SORTNO'] = '';
                        }
                        if($row['SEQ'] == 99999){
                            $row['SEQ'] = '';
                        }
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
            //$data = umEX($data,true);
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME,$D1NAME,$FILID);
            //e_log("MSM getFDB2CSV2 param".print_r($params,true));
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row['SORTNO'] == 99999){
                        $row['SORTNO'] = '';
                    }
                    if($row['SEQ'] == 99999){
                        $row['SEQ'] = '';
                    }
                    $data[] = $row;
                }
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data);
            }
        }
        
    }
    return $data;
}
function getFDB2CSV2RDB ($db2con,$D1NAME,$FILID,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST,$chkFileFlg,$FILTYP,$FILE){
    $data = array();
    $syscolumn2 = array();
    $params = array();
    $LIBLNM = '';
    $rtn = 0;
    $db2RDB = cmDB2ConRDB();

    $strSQL  =  ' SELECT  ';
    $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
    $strSQL .=  '          ,A.LENGTH '; 
    $strSQL .=  '          ,CASE A.DDS_TYPE ';
    $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
    $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
    $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
    $strSQL .=  '            ELSE A.DDS_TYPE '; 
    $strSQL .=  '            END AS DDS_TYPE ';
    $strSQL .=  '          ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG==='1'){
        $strSQL .=  '          ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .=  '          ,A.COLUMN_HEADING ';
    }
    $strSQL .=  '          ,A.ORDINAL_POSITION '; 
    $strSQL .=  '          ,A.TABLE_SCHEMA '; 
    $strSQL .=  '          ,A.TABLE_NAME '; 
    $strSQL .=  '       FROM ' . SYSCOLUMN2 . ' A '; 
    $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
    $strSQL .=  '       AND A.TABLE_NAME = ? '; 
    //$strSQL .=  '       AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\') ';
//    $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
    $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ';

    $stmt = db2_prepare($db2RDB,$strSQL);
    if($stmt === false){
        $syscolumn2 = array('result' => 'FAIL_SEL');
        $rtn = 1;
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$TABLE_NAME);
                //e_log('フィールド取得：'.$strSQL.print_r($params,true));
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $syscolumn2 = array('result' => 'FAIL_SEL');
                    $rtn = 1;
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $syscolumn2[] = $row;
                    }
                    if(count($syscolumn2) > 0){
                        $LIBLNM = $value;
                        break;
                    }
                }
            }
            //$data = umEX($data,true);
            $syscolumn2 = umEx($syscolumn2);
            $syscolumn2 = array('result' => true,'data' => $syscolumn2);
            //e_log('取得したデータ：'.print_r($syscolumn2,true));
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME);
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $syscolumn2 = array('result' => 'FAIL_SEL');
                $rtn = 1;
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $syscolumn2[] = $row;
                }
                $LIBLNM = $TABLE_SCHEMA;
                $syscolumn2 = umEx($syscolumn2,false);
                $syscolumn2 = array('result' => true,'data' => $syscolumn2);
                //e_log('取得したデータ：'.print_r($syscolumn2,true));
            }
        }
    }

    if($rtn === 0){
        e_log('一時テーブル作成開始');
        
        // 一時テーブル作成
        $createSQL  = 'DECLARE GLOBAL TEMPORARY TABLE SYSCOLUMN2 ';
        $createSQL .= ' ( ';
        $createSQL .= ' SYSTEM_COLUMN_NAME CHAR(10) , ';
        $createSQL .= ' LENGTH INTEGER , ';
        $createSQL .= ' DDS_TYPE CHAR(1), ';
        $createSQL .= ' NUMERIC_SCALE INTEGER DEFAULT NULL , ';
        $createSQL .= ' COLUMN_HEADING VARGRAPHIC(60) CCSID 1200 , ';
        $createSQL .= ' ORDINAL_POSITION INTEGER, ';
        $createSQL .= ' TABLE_SCHEMA VARCHAR(128), ';
        $createSQL .= ' TABLE_NAME VARCHAR(128)'; 
        $createSQL .= ' )  WITH REPLACE ';
        //e_log($createSQL);
        $result = db2_exec($db2con, $createSQL);
        if ($result) {
            e_log( 'テーブル作成成功');
            if(count($syscolumn2['data'])>0){
                $syscolumn2 = umEx($syscolumn2['data']);
                
                $strInsSQL  = ' INSERT INTO QTEMP/SYSCOLUMN2 ';
                $strInsSQL .= ' ( ';
                $strInsSQL .= '     SYSTEM_COLUMN_NAME, ';
                $strInsSQL .= '     LENGTH, ';
                $strInsSQL .= '     DDS_TYPE, ';
                $strInsSQL .= '     NUMERIC_SCALE, ';
                $strInsSQL .= '     COLUMN_HEADING, ';
                $strInsSQL .= '     ORDINAL_POSITION, ';
                $strInsSQL .= '     TABLE_SCHEMA, ';
                $strInsSQL .= '     TABLE_NAME ';
                $strInsSQL .= ' ) ';
                $strInsSQL .= ' VALUES ( ?,?,?,?,?,?,?,?)';

                $stmt = db2_prepare($db2con,$strInsSQL);
                if($stmt === false ){
                    $data = array(
                                'result' => 'FAIL_INS',
                                'errcd'  => 'SYSCOLUMN2:'.db2_stmt_errormsg()
                            );
                    $result = 1;
                }else{
                    foreach($syscolumn2 as $syscolumn2data){
                        $params =array(
                                    $syscolumn2data['COLUMN_NAME'],
                                    $syscolumn2data['LENGTH'],
                                    $syscolumn2data['DDS_TYPE'],
                                    ($syscolumn2data['NUMERIC_SCALE'] == '') ? NULL : $syscolumn2data['NUMERIC_SCALE'],
                                    $syscolumn2data['COLUMN_HEADING'],
                                    $syscolumn2data['ORDINAL_POSITION'],
                                    $syscolumn2data['TABLE_SCHEMA'],
                                    $syscolumn2data['TABLE_NAME']
                                );
                        //e_log($strInsSQL.print_r($params,true));
                        $res = db2_execute($stmt,$params);
                        if($res === false){
                            $data = array(
                                    'result' => 'FAIL_INS',
                                    'errcd'  => 'SYSCOLUMN2:'.db2_stmt_errormsg()
                                );
                            $result = 1;
                            break;
                        }else{
                            $result = 0;
                            $data = array('result' => true);
                        }
                    }
                }
            }
            if($result !== 0){
                $rtn = 1;
            }
        }else{
            e_log('テーブル作成失敗');
            e_log('テーブルerror'.db2_stmt_errormsg());
            $rtn = 1;
            $data = array(
                'result' => 'FAIL_SEL',
                'errcd'  => 'テーブル作成失敗'
                );
        }
    }
     cmDb2Close($db2RDB);
    if($rtn === 0){
        $strSQL = '';
        $strSQL .=  '   SELECT ';
        $strSQL .=  '         FDB2CSV2DATA.SEQ            AS SEQ ';
        $strSQL .=  '       , SYSTABDATA.COLUMN_NAME      AS COLUMN_NAME ';
        $strSQL .=  '       , SYSTABDATA.LENGTH           AS LENGTH ';
        $strSQL .=  '       , SYSTABDATA.DDS_TYPE         AS DDS_TYPE ';
        $strSQL .=  '       , SYSTABDATA.NUMERIC_SCALE    AS NUMERIC_SCALE ';
        if($chkFileFlg === true){
            $strSQL .=  '   , SYSTABDATA.COLUMN_HEADING AS COLUMN_HEADING ';
        }else{
            $strSQL .=  '       ,(CASE ';
            $strSQL .=  '           WHEN FDB2CSV2DATA.COLUMN_HEADING IS NULL ';
            $strSQL .=  '           AND FDB2CSV2DATA.COLUMN_NAME  IS NULL ';
            $strSQL .=  '           THEN SYSTABDATA.COLUMN_HEADING ';
            $strSQL .=  '           ELSE FDB2CSV2DATA.COLUMN_HEADING ';
            $strSQL .=  '         END) AS COLUMN_HEADING ';
        }
        $strSQL .=  '       , FDB2CSV2DATA.FORMULA        AS FORMULA ';
        $strSQL .=  '       , FDB2CSV2DATA.SORTNO         AS SORTNO ';
        $strSQL .=  '       , FDB2CSV2DATA.SORTTYP        AS SORTTYP ';
        $strSQL .=  '       , FDB2CSV2DATA.EDTCD          AS EDTCD ';
        $strSQL .=  '       , SYSTABDATA.ORDINAL_POSITION AS ORDINAL_POSITION ';
        $strSQL .=  '       , \''.$FILTYP.'\' AS FILTYP  ';
	    $strSQL .='       , \''.$FILE.'\' AS FILE  ';
        $strSQL .=  '   FROM  '; 
        $strSQL .=  '       (SELECT  ';
        $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
        $strSQL .=  '          ,A.LENGTH '; 
        $strSQL .=  '          ,CASE A.DDS_TYPE ';
        $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
        $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
        $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
        $strSQL .=  '            ELSE A.DDS_TYPE '; 
        $strSQL .=  '            END AS DDS_TYPE ';
        $strSQL .=  '          ,A.NUMERIC_SCALE '; 
        $strSQL .=  '          ,A.COLUMN_HEADING '; 
        $strSQL .=  '          ,A.ORDINAL_POSITION '; 
        $strSQL .=  '       FROM QTEMP/SYSCOLUMN2 A '; 
        $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
        $strSQL .=  '       AND A.TABLE_NAME = ? '; 
        //$strSQL .=  '       AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\') ';
//        $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
        $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ) SYSTABDATA ';
        $strSQL .=  '   LEFT JOIN ';
        $strSQL .=  '       (SELECT  ';
        $strSQL .=  '            (CASE A.D2CSEQ ';
        $strSQL .=  '            WHEN 0 THEN 99999 '; 
        $strSQL .=  '            ELSE A.D2CSEQ ';
        $strSQL .=  '            END) '; 
        $strSQL .=  '            AS SEQ '; 
        $strSQL .=  '           ,A.D2FLD  AS COLUMN_NAME ';
        $strSQL .=  '           ,A.D2HED  AS COLUMN_HEADING '; 
        $strSQL .=  '           ,\'\'     AS FORMULA ';
        $strSQL .=  '           ,CASE A.D2RSEQ ';
        $strSQL .=  '             WHEN 0 THEN 99999 ';
        $strSQL .=  '             ELSE A.D2RSEQ '; 
        $strSQL .=  '             END AS SORTNO ';
        $strSQL .=  '           ,A.D2RSTP AS SORTTYP '; 
        $strSQL .=  '           ,A.D2WEDT AS EDTCD ';
        $strSQL .=  '       FROM FDB2CSV2 A ';
        $strSQL .=  '       WHERE A.D2NAME = ?';
        $strSQL .=  '       AND   A.D2FILID = ? ';
        $strSQL .=  '       ORDER BY A.D2CSEQ,A.D2RSEQ ) FDB2CSV2DATA ';
        $strSQL .=  '   ON SYSTABDATA.COLUMN_NAME = FDB2CSV2DATA.COLUMN_NAME ';
        $strSQL .=  '   ORDER BY SEQ ,SORTNO, ORDINAL_POSITION ';
        
        //e_log('SQLtemp'.$strSQL);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
            $rtn  = 1;
            //e_log('error'.db2_stmt_errormsg());
        }else{
            if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
                $params = array($LIBLNM,$TABLE_NAME,$D1NAME,$FILID);
            }else{
                $params = array($TABLE_SCHEMA,$TABLE_NAME,$D1NAME,$FILID);
            }
            //e_log(print_r($params,true));
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
                $rtn  = 1;
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row['SORTNO'] == 99999){
                        $row['SORTNO'] = '';
                    }
                    if($row['SEQ'] == 99999){
                        $row['SEQ'] = '';
                    }
                    $data[] = $row;
                }
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data);
            }
        }
            
       // }
    }
    //e_log("MSM getInitFieldFrmData".print_r($data,true));
    return $data;
}
function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}

function chkChangeFile($oriLibl,$oriFile,$compLibl,$compFile,$orilibList,$complibList){
    if(cmMer($compLibl) === '' || cmMer($compLibl) === '*USRLIBL'){
        $compLibl = '*LIBL';
    }else{
        $compLibl = cmMer($compLibl);
    }
    $compFile = cmMer($compFile) ;
    if(cmMer($oriLibl) === '' || cmMer($oriLibl) === '*USRLIBL'){
        $oriLibl = '*LIBL';
    }else{
        $oriLibl = cmMer($oriLibl);
    }
    $oriFile  = cmMer($oriFile);
    $chgFileFlg = false;
    if($compLibl !== $oriLibl){
        $chgFileFlg = true;
    }else{
        if($oriLibl === '*LIBL'){
            if($orilibList !== $complibList){
                $chgFileFlg = true;
            }else{
                if($compFile !== $oriFile){
                    //e_log($compFile.'dfd'.$oriFile);
                    $chgFileFlg = true;
                }
            }
        }else{
            if($compFile !== $oriFile){
                $chgFileFlg = true;
            }
        }
    }
    return $chgFileFlg;
}
function fnGetBREFTBL($db2con,$D1NAME){
    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM BREFTBL as A ';
    $strSQL .= ' WHERE RTQRYN <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND RTQRYN = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}