<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
$D1NAME = $_POST['D1NAME'];
$BASEFIELDINFO = json_decode($_POST['BASEFIELDINFO'], true);
$BASEGRDFLDINFO = json_decode($_POST['BASEGRDFLDINFO'], true);
$SQLFLG = $_POST['SQLFLG'];
$D2FLD = $_POST['D2FLD'];
$D2FILID = $_POST['D2FILID'];
/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
e_log('LevelControlD2Type D1NAME***' . print_r($D1NAME, true));
e_log('LevelControlD2Type BASEFIELDINFO***' . print_r($BASEFIELDINFO, true));
e_log('LevelControlBASEGRDFLDINFO***' . print_r($BASEGRDFLDINFO, true));
if ($SQLFLG !== "1") {
    if (count($BASEFIELDINFO) > 0) {
        foreach ($BASEFIELDINFO as $FLDINFO) {
            foreach ($FLDINFO['STRDATA'] as $FLDDATA) {
                /*if ($FLDDATA['SEQ'] == 0) {
                    $FLDDATA['SEQ'] = '';
                }
                if ($FLDDATA['SEQ'] !== '') {*/
                    $fieldInfo = array();
                    $fieldInfo['D2FILID'] = $FLDINFO['D2FILID'];
                    if ($FLDINFO['D2FILID'] === 0) {
                        $fieldInfo['D2TYPE'] = 'P';
                    } else if ($FLDINFO['D2FDESC'] === '結果フィールド') {
                        $fieldInfo['D2TYPE'] = 'K';
                    } else {
                        $fieldInfo['D2TYPE'] = 'S' . $FLDINFO['D2FILID'];
                    }
                    $fieldInfo['D2CSEQ'] = $FLDDATA['SEQ'];
                    $fieldInfo['D2DEC'] = '0';
                    $fieldInfo['D2DNLF'] = '';
                    $fieldInfo['D2FLD'] = $FLDDATA['COLUMN_NAME'];
                    $fieldInfo['D2HED'] = $FLDDATA['COLUMN_HEADING'];
                    $fieldInfo['D2LEN'] = $FLDDATA['LENGTH'];
                    $fieldInfo['D2NAME'] = $D1NAME;
                    $fieldInfo['D2TYPE'] = $FLDDATA['DDS_TYPE'];
                    $fieldInfo['D2WEDT'] = $FLDDATA['EDTCD'];
                    if ($fieldInfo['D2FLD'] === $D2FLD) {
                        $data[] = $fieldInfo;
                    }
            }
        }
    }
} else {
    if (count($BASEGRDFLDINFO) > 0) {
        foreach ($BASEGRDFLDINFO as $FLDINFO) {
            $fieldInfo['D2CSEQ'] = $i; //どうして$iを入れるのが分からない
            $fieldInfo['D2DEC'] = '0';
            $fieldInfo['D2DNLF'] = '';
            $fieldInfo['D2FILID'] = '0';
            $fieldInfo['D2FLD'] = $FLDINFO['COLUMN_NAME'];
            $fieldInfo['D2HED'] = $FLDINFO['COLUMN_HEADING'];
            $fieldInfo['D2LEN'] = $FLDINFO['LENGTH'];
            $fieldInfo['D2NAME'] = $D1NAME;
            $fieldInfo['D2TYPE'] = $FLDINFO['DDS_TYPE'];
            $fieldInfo['D2WEDT'] = $FLDINFO['EDTCD'];
            if ($fieldInfo['D2FLD'] === $D2FLD) {
                $data[] = $fieldInfo;
            }  
        }
    }
}
cmDb2Close($db2con);
/**return**/
$rtn = array('DATA' => umEx($data),);
echo (json_encode($rtn));
