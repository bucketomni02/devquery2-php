<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$D1NAME = $_POST['D1NAME'];
$BASEFIELDINFO     = json_decode($_POST['BASEFIELDINFO'],true);
$BASEGRDFLDINFO     = json_decode($_POST['BASEGRDFLDINFO'],true);
$SQLFLG = $_POST['SQLFLG'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);


if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $auth = $rs['data'][0];
        $WUAUTH = cmMer($auth['WUAUTH']);
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'14',$userData[0]['WUSAUT']);//'14' =>制御レベル設定
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('制御レベル設定の権限'));
            }
        }
    }
}
//e_log('LevelControl D1NAME***'.print_r($D1NAME,true));
//e_log('LevelControlBASEGRDFLDINFO***'.print_r($BASEGRDFLDINFO ,true));
//e_log('LevelControlBASEFIELDINFO***'.print_r($BASEFIELDINFO ,true));
if($SQLFLG !== "1"){
 if(count($BASEFIELDINFO)>0){
    foreach ($BASEFIELDINFO as $FLDINFO) {
         foreach($FLDINFO['STRDATA'] as $FLDDATA){
            /*if($FLDDATA['SEQ'] == 0){
                $FLDDATA['SEQ'] = '';
            }
            if($FLDDATA['SEQ'] !== '' ){*/
                $fieldInfo = array();
                $fieldInfo['D2FILID']            = $FLDINFO['D2FILID'];
                if($FLDINFO['D2FILID'] === 0){
                    $fieldInfo['D2TYPE']   = 'P';
                }else if($FLDINFO['D2FDESC'] === '結果フィールド'){
                    $fieldInfo['D2TYPE']    = 'K';
                    $fieldInfo['D2FILID']= '9999';
                }else{
                    $fieldInfo['D2TYPE']    = 'S'.$FLDINFO['D2FILID'];
                } 
                $fieldInfo['D2CSEQ']   = $FLDDATA['SEQ'];
                $fieldInfo['D2DEC']    = '0';
                $fieldInfo['D2DNLF']   = '';
                $fieldInfo['D2FLD']    = $FLDDATA['COLUMN_NAME'];
                $fieldInfo['D2HED']    = $FLDDATA['COLUMN_HEADING'];
                $fieldInfo['D2LEN']    = $FLDDATA['LENGTH'];
                $fieldInfo['D2NAME']   = $D1NAME;
                $fieldInfo['D2TYPE']   = $FLDDATA['DDS_TYPE'];
                $fieldInfo['D2WEDT']    = $FLDDATA['EDTCD'];
                $data[] = $fieldInfo;
             // }
           }
     }
  }
}else{
   $i = 1;
   if(count($BASEGRDFLDINFO)>0){
    foreach ($BASEGRDFLDINFO as $FLDINFO) {
        $fieldInfo['D2CSEQ']   = $i;
        $fieldInfo['D2DEC']    = '0';
        $fieldInfo['D2DNLF']   = '';
        $fieldInfo['D2FILID']   = '0';
        $fieldInfo['D2FLD']    = $FLDINFO['COLUMN_NAME'];
        $fieldInfo['D2HED']    = $FLDINFO['COLUMN_HEADING'];
        $fieldInfo['D2LEN']    = $FLDINFO['LENGTH'];
        $fieldInfo['D2NAME']   = $D1NAME;
        $fieldInfo['D2TYPE']   = $FLDINFO['DDS_TYPE'];
        $fieldInfo['D2WEDT']    = $FLDINFO['EDTCD'];
        $data[] = $fieldInfo;
        $i++;
     }
  }
}    

cmDb2Close($db2con);
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => umEx($data),
    'licensePivot' =>$licensePivot,
    'licenseSeigyoSpecialFlg'=>$licenseSeigyoSpecialFlg,
    'WUAUTH' =>$WUAUTH
);
echo(json_encode($rtn));
