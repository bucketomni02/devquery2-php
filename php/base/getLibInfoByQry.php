<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount       = 0;
$data           = array();
$basefiledata   = array();
$basesqldata    = array();
$rtn            = 0;
$msg            = '';
$SQLCFLG        = '';
$htmlFlg        = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}

if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg('ログインユーザーに指定したクエリーに対してもクエリー作成の権限がありません。');
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}

if($rtn === 0){
    //クエリーがない場合
    $rs = fnGetFDB2CSV1($db2con,$D1NAME);
    if($rs['RTN'] !== 0){
        $rtn = $rs['RTN'];
        $msg = showMsg($rs['MSG'],array('クエリー'));
    }else{
        $fdb2csv1 = $rs['DATA'];
        $SQLCFLG  = $fdb2csv1['D1CFLG'];
        $D1LIBL   = $fdb2csv1['D1LIBL'];
        $D1RDB    = $fdb2csv1['D1RDB'] ==='' ? RDB : $fdb2csv1['D1RDB']; 
        if($SQLCFLG === '1'){
            $rs = getBaseSQLData($db2con,$fdb2csv1);
            if($rs['RTN'] !== 0){
                $rtn = $rs['RTN'];
                $msg = $rs['MSG'];
            }else{
                $basesqldata = $rs['BASESQLDATA'];
            }
        }else{
            $rs = generateBaseFileData($db2con,$fdb2csv1);
            if($rs['RTN'] !== 0){
                $rtn = $rs['RTN'] ;
                $msg = $rs['MSG'];
            }else{
                $basefiledata = $rs['BASEFILEDATA'];
            }
        }
    }
}
cmDb2Close($db2con);
if($D1RDB === '' || $D1RDB === RDB || $D1RDB === RDBNM){
    $_SESSION['PHPQUERY']['RDBNM'] = '';
}else{
    $_SESSION['PHPQUERY']['RDBNM'] = $D1RDB;
}
/**return**/
$rtn = array(
    'RTN'           => $rtn,
    'MSG'           => $msg,
    'D1NAME'        => $D1NAME,
    'BASEFILEDATA'  => $basefiledata,
    'BASESQLDATA'   => $basesqldata,
    'SQLCFLG'       => $SQLCFLG,
    'D1LIBL'        => $D1LIBL,
    'D1RDB'         => $D1RDB
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();
    $rtn = 0;
    $msg = '';
    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rtn = 1;
        $msg = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rtn = 1;
            $msg = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rtn = 3;
                $msg = 'NOTEXIST_GET';
            }else{
                $data = umEx($data);
                $data = $data[0];
                if($data['D1WEBF'] !== '1'){
                    $rtn = 3;
                    $msg = 'MSGQRCRE';
                }
            }
        }
    }
    $rtnArr = array(
                'RTN' => $rtn,
                'MSG' => $msg,
                'DATA'=> $data
            );
    return $rtnArr;
}
function generateBaseFileData($db2con,$fdb2csv1){
    $rtn = 0;
    $msg = '';
    if($rtn === 0){
        $basefileData = array();
        $basefileData['D1NAME'] = $fdb2csv1['D1NAME'];
        $basefileData['D1FILE'] = $fdb2csv1['D1FILE'];
        $basefileData['D1FILMBR'] = $fdb2csv1['D1FILMBR'];
        if($fdb2csv1data['D1FILLIB'] === ''){
            $fdb2csv1data['D1FILLIB'] = '*USRLIBL';
        }
        $basefileData['D1FILLIB'] = $fdb2csv1['D1FILLIB'];
        $basefileData['D1TEXT'] = $fdb2csv1['D1TEXT'];
        $basefileData['D1LIBL'] = $fdb2csv1['D1LIBL'];
        $basefileData['D1RDB'] = (cmMer($fdb2csv1['D1RDB'])=='' ? RDB : cmMer($fdb2csv1['D1RDB'])); 
        $res = getBREFTBLData($db2con,$basefileData['D1NAME']);
        if($res['result'] !== true ){
            $rtn = 1;
            $msg = showMsg($res['result']);
        }else{
            $breftbldata = array();
            if(count($res['data'])>0){
                foreach($res['data'] as $breftblitm){
                    $res1 = getBREFFLDData($db2con,$breftblitm['RTQRYN'],$breftblitm['RTRSEQ']);
                    if($res1['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                        break;
                    }
                    else{
                        $brefflddata = array();
                        foreach($res1['data'] as $resBreffld){
                            $breffldinfo = array();
                            $breffldinfo['RFFSEQ'] = $resBreffld['RFFSEQ'];
                            $breffldinfo['RFRKBN'] = $resBreffld['RFRKBN'];
                            $breffldinfo['RFPFNM'] = $resBreffld['RFPFNM'];
                            $breffldinfo['RFRFNM'] = $resBreffld['RFRFNM'];
                            $breffldinfo['RFTEISU'] = $resBreffld['RFTEISU'];

                            $brefflddata[] = $breffldinfo;
                        }
                        $breftblinfo = array();
                        $breftblinfo['RTQRYN']  = $breftblitm['RTQRYN'];
                        $breftblinfo['RTRSEQ']  = $breftblitm['RTRSEQ'];
                        $breftblinfo['RTRFIL']  = $breftblitm['RTRFIL'];
                        $breftblinfo['RTRMBR']  = $breftblitm['RTRMBR'];
                        if( $breftblitm['RTRLIB'] === ''){
                             $breftblitm['RTRLIB'] = '*USRLIBL';
                        }
                        $breftblinfo['RTRLIB']  = $breftblitm['RTRLIB'];
                        $breftblinfo['RTDESC']  = $breftblitm['RTDESC'];
                        $breftblinfo['RTJTYP']  = $breftblitm['RTJTYP'];
                        $breftblinfo['BREFFLD'] = $brefflddata;
                        
                        $breftbldata[] = $breftblinfo;
                    }
                }
            }
            $basefileData['REFFILE'] = $breftbldata;
        }
    }
    $rtnArr = array( 
                    'RTN' => $rtn ,
                    'MSG' => $msg ,
                    'BASEFILEDATA' => $basefileData
                );
                
    return $rtnArr;
}
// BASESQLDATA
function getBaseSQLData($db2con,$fdb2csv1){
    $rtn = 0;
    $msg = '';
    if($rtn === 0){
        $basesqldata = array();
        $basesqldata['D1NAME'] = $fdb2csv1['D1NAME'];
        $basesqldata['D1TEXT'] = $fdb2csv1['D1TEXT'];
        $basesqldata['D1LIBL'] = $fdb2csv1['D1LIBL'];
        $basesqldata['D1RDB'] = $fdb2csv1['D1RDB'] === '' ? RDB : $fdb2csv1['D1RDB']; 
        $res = fnGetBSQLDAT($db2con,$basesqldata['D1NAME']);
        if($res['result'] !== true ){
            $rtn = 1;
            $msg = showMsg($res['result']);
        }else{
            $data = $res['data'];
            $basesqldata['D1SQL'] = $res['data']['BEXESQL'];
            //e_log('BASESQLDATA:'.print_r($basesqldata,true));
        }
    }
    $rtnArr = array( 
                    'RTN' => $rtn ,
                    'MSG' => $msg ,
                    'BASESQLDATA' => $basesqldata
                );
                
    return $rtnArr;
}
function fnGetBSQLDAT($db2con,$QRYNM){
    $data = array();
    $params = array();
    
    /*$strSQL .= '    SELECT ';
    $strSQL .= '        BSQLNM, ';
    $strSQL .= '        BSQLFLG, ';
    $strSQL .= '        BEXESQL ';
    $strSQL .= '    FROM ';
    $strSQL .= '        BSQLDAT ';
    $strSQL .= '    WHERE ';
    $strSQL .= '        BSQLNM = ? AND ';
    $strSQL .= '        BSQLFLG = \'\' ';*/
    $strSQL .= ' SELECT ';
    $strSQL .= '    BSQLNM, ';
    $strSQL .= '    BSQLFLG, ';
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,1,5000)) > 0 THEN SUBSTR(BEXESQL,1,5000)  ELSE '' END AS BEXESQL1,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,5001,5000)) > 0 THEN  SUBSTR(BEXESQL,5001,5000)  ELSE '' END AS BEXESQL2,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,10001,5000)) > 0 THEN SUBSTR(BEXESQL,10001,5000)  ELSE '' END AS BEXESQL3,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,15001,5000)) > 0 THEN SUBSTR(BEXESQL,15001,5000) ELSE '' END AS BEXESQL4,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,20001,5000)) > 0 THEN SUBSTR(BEXESQL,20001,5000) ELSE '' END AS BEXESQL5,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,25001,5000)) > 0 THEN SUBSTR(BEXESQL,25001,5000) ELSE '' END AS BEXESQL6,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,30001,2730)) > 0 THEN SUBSTR(BEXESQL,30001,2730) ELSE '' END AS BEXESQL7";
    $strSQL .= " FROM( ";
    $strSQL .= "    SELECT BSQLNM, BSQLFLG, BEXESQL FROM BSQLDAT WHERE BSQLNM = ? ";
    $strSQL .= ") AS A";
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        array_push($params,$QRYNM);
        error_log('bsqldat query = '.$strSQL.print_r($params,true));
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $tmp = array();
                $tmp['BSQLNM'] = $row['BSQLNM'];
                $tmp['BSQLFLG'] = $row['BSQLFLG'];
                $tmp['BEXESQL'] = $row['BEXESQL1'].$row['BEXESQL2'].$row['BEXESQL3'].$row['BEXESQL4'].$row['BEXESQL5'].$row['BEXESQL6'].$row['BEXESQL7'];
                $data[] = $tmp;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}
function getBREFTBLData($db2con,$QRYNM){
    $data = array();
    $params = array();
    
    $strSQL  =  '   SELECT A.RTQRYN ';
    $strSQL .=  '        , A.RTRSEQ ';
    $strSQL .=  '        , A.RTRFIL ';
    $strSQL .=  '        , A.RTRMBR ';
    $strSQL .=  '        , A.RTRLIB ';
    $strSQL .=  '        , A.RTDESC ';
    $strSQL .=  '        , CASE A.RTJTYP ';  
    $strSQL .=  '          WHEN \'2\' THEN \'4\' '; 
    $strSQL .=  '          WHEN \'3\' THEN \'5\' '; 
    $strSQL .=  '          WHEN \'1\' THEN \'6\' '; 
    $strSQL .=  '          WHEN \'7\' THEN \'8\' ';
    $strSQL .=  '          ELSE A.RTJTYP ';
    $strSQL .=  '        END AS RTJTYP ';
    $strSQL .=  '   FROM BREFTBL A ';
    $strSQL .=  '   WHERE A.RTQRYN = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        array_push($params,$QRYNM);
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
function getBREFFLDData($db2con,$QRYNM,$RTRSEQ){
    $data = array();
    
    $strSQL .= '    SELECT A.RFQRYN ';
    $strSQL .= '        , A.RFRSEQ ';
    $strSQL .= '        , A.RFFSEQ ';
    $strSQL .= '        , A.RFPFID ';
    $strSQL .= '        , A.RFPFNM  AS RFPF ';
    $strSQL .= '        , CASE A.RFPFID '; 
    $strSQL .= '            WHEN 0 THEN concat(\'P.\',A.RFPFNM) ';
    $strSQL .= '            ELSE  concat(concat(concat(\'S\', A.RFPFID),\'.\'),A.RFPFNM) ';
    $strSQL .= '        END AS RFPFNM '; 
    $strSQL .= '        , A.RFTEISU ';
    $strSQL .= '        , A.RFRFID ';
    $strSQL .= '        , A.RFRFNM  AS RFRF ';
    //$strSQL .= '        , concat(concat(concat(\'S\', A.RFRFID),\'.\'),A.RFRFNM) AS RFRFNM ';

	$strSQL .= '         , CASE A.RFTEISU ';//check checkbox value
    $strSQL .= '           	WHEN 0 THEN concat(concat(concat(\'S\', A.RFRFID),\'.\'),A.RFRFNM)';
	$strSQL .= '            ELSE  A.RFRFNM ';
    $strSQL .= '           END AS RFRFNM ';//end

    $strSQL .= '        , CASE A.RFRKBN ';
    $strSQL .= '            WHEN \'\' THEN \'EQ\' ';
    $strSQL .= '            ELSE  A.RFRKBN ';
    $strSQL .= '        END AS RFRKBN '; 
    $strSQL .= '    FROM   BREFFLD A ';
    $strSQL .= '    WHERE  A.RFQRYN = ? ';
    $strSQL .= '    AND  A.RFRSEQ = ? ';
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($QRYNM,$RTRSEQ);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
