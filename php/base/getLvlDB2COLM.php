<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$DCNAME = $_POST['DCNAME'];
$DATA = json_decode($_POST['DATA'],true);
$SUMFLG = $_POST['SUMFLG'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$DCNAME = cmHscDe($DCNAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

if($rtn === 0){
    if($DATA['LABEL1'] === '' &&
       $DATA['LABEL2'] === '' &&
       $DATA['LABEL3'] === '' &&
       $DATA['LABEL4'] === '' &&
       $DATA['LABEL5'] === '' &&
       $DATA['LABEL6'] === '' ){
           $rtn = 1;
           $msg = showMsg('FAIL_SET',array('制御レベル'));
    }
}
//同じ制御レベルが選ばれた場合
if($rtn === 0){
    for( $x = 1 ; $x <= 6 ; $x +=1){
        $cData = $DATA['LABEL'.$x];
        if($cData !== '' && $DATA['LABEL'.$x] !== null){
            for($y = 1 ; $y <= 6 ; $y +=1){
                if($y !== $x){
                    if( $cData ===  $DATA['LABEL'.$y] ){
                      
                    }
                }
            }
        }
        
    }
}

  if($rtn === 0){
    for ($y = 1;$y <= 6;$y++) {
            $ {"nVar" . $y} = array();//$DATA
          
            $ {"nVar" . $y} = $DATA['TEXTBOX'.$y];
             if(!checkMaxLen($ {"nVar" . $y},90)){
		         $rtn = 1;
		         $msg = showMsg('FAIL_MAXLEN',array(array('ラベル手入力',$y)));
		       
		     }
        }
  }
 
//データ配列のnullと’’の対応
if($LVLFLG !== '1' ){
 for ($x = 1; $x <= 6; $x++) {
    $LGroup = explode(",", $DATA['LABEL' . $x]);
    if (count($LGroup) >= 2) { //グループのレベル
        $ {"exp" . $x} = explode("-", $LGroup[0]);
        $ {"tmp" . $x} = array(
				'FILID' => ($ {"exp" . $x}[0] !== '') ? $ {"exp" . $x}[0] : 0,
				'FLD' => $DATA['LABEL' . $x]);
    }else{
	    ${"exp" . $x} = explode("-",$DATA['LABEL'. $x]);
	    ${"tmp" . $x} = array(
	            'FILID' => (${"exp" . $x}[0] !== '')? ${"exp" . $x}[0]:0,
	            'FLD' => (${"exp" . $x}[1] !== null)? ${"exp" . $x}[1]:''
	    );
	}
}
if($rtn === 0){
    for ($x = 1; $x <= 6; $x++) {
        if(${"tmp" . $x}['FLD'] !== ''){
            //FDB2CSV2に存在するかチェック(FILID=9999は無条件でtrue)
           
       }
    }

    for ($y = 1;$y <= 6;$y++) {
           $ {"nVar" . $y} = array();//$DATA
           // if($DATA['TEXTBOX1'])
           $ {"nVar" . $y} = $DATA['TEXTBOX'.$y];
  
    }
  }
}

        

$fieldInfo = array();
$fieldInfo['DCNAME'] = $DCNAME;
$fieldInfo['DCFILID1'] = $tmp1['FILID'];
$fieldInfo['DCFLD1'] = $tmp1['FLD'];
$fieldInfo['DCFNAME1'] = $nVar1;
$fieldInfo['DCFILID2'] = $tmp2['FILID'];
$fieldInfo['DCFLD2'] = $tmp2['FLD'];
$fieldInfo['DCFNAME2'] = $nVar2;
$fieldInfo['DCFILID3'] = $tmp3['FILID'];
$fieldInfo['DCFLD3'] = $tmp3['FLD'];
$fieldInfo['DCFNAME3'] = $nVar3;
$fieldInfo['DCFILID4'] = $tmp4['FILID'];
$fieldInfo['DCFLD4'] = $tmp4['FLD'];
$fieldInfo['DCFNAME4'] = $nVar4;
$fieldInfo['DCFILID5'] = $tmp5['FILID'];
$fieldInfo['DCFLD5'] = $tmp5['FLD'];
$fieldInfo['DCFNAME5'] = $nVar5;
$fieldInfo['DCFILID6'] = $tmp6['FILID'];
$fieldInfo['DCFLD6'] = $tmp6['FLD'];
$fieldInfo['DCFNAME6'] = $nVar6;
$fieldInfo['DCSUMF'] = $SUMFLG;

$data[] = $fieldInfo;


cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData' => umEx($data)
);

echo(json_encode($rtnAry));

