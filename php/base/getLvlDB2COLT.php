<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$DTNAME = $_POST['DTNAME'];
$DATA = json_decode($_POST['DATA'],true);
$BASEFIELDINFO = json_decode($_POST['BASEFIELDINFO'],true);
$BASEGRDFLDINFO = json_decode($_POST['BASEGRDFLDINFO'],true);
$SQLFLG = $_POST['SQLFLG'];

/*
*-------------------------------------------------------*
* 変数
*-------------------------------------------------------*
*/
$data = array();
$D2type = array();
$rtn = 0;
$msg = '';
$datacount = 0;
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$DTNAME = cmHscDe($DTNAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
e_log('LevelControlDATA***'.print_r($DATA ,true));
//e_log('LevelControlBASEFIELDINFO***'.print_r($BASEFIELDINFO ,true));

if($rtn === 0){
    //集計コンボがブランクになってチェックボックスがチェックされている場合
    $comboFlg = false;
    for ($x = 0; $x < count($DATA); $x += 7) {
       if($DATA[$x+1] !== '' && $DATA[$x+1] !== null){
            $comboFlg = true;
       }else{
            if($DATA[$x+2]  === '1' 
                || $DATA[$x+3]  === '1' 
                || $DATA[$x+4]  === '1' 
                || $DATA[$x+5]  === '1' 
                || $DATA[$x+6]  === '1' ){
               
            }

        }
    }        
    if($rtn === 0){
        if($comboFlg === false){
           
        }
    }
}
if($rtn === 0){
    //同じコンボデータが選ばれた場合
    for ($i = 1; $i < count($DATA) ; $i += 7) { 
        for($j = ($i + 7) ;$j < count($DATA) ; $j += 7){
            if( $DATA[$i] !== null && $DATA[$i] !== '' && $DATA[$j] !== null && $DATA[$j] !== ''){
                if($DATA[$i] === $DATA[$j]){
                   
                }
            }
        }
        if($rtn === 1){
            break;
        }
    }
}

if($rtn === 0){
    //チェックボックスがチェックされてないで集計コンボにデータがある場合
    for ($x = 0; $x < count($DATA); $x += 7) {
        if($DATA[$x+1] !== '' && $DATA[$x+1] !== null){
            if(($DATA[$x+2]  === '0' ||  $DATA[$x+2]  === '')
                && ($DATA[$x+3]  === '0' || $DATA[$x+3]  === '') 
                && $DATA[$x+4]  === '0' 
                && $DATA[$x+5]  === '0' 
                && $DATA[$x+6]  === '0' ){
                   
            }
        }
    }
}

if($rtn === 0){
    for ($x = 0; $x < count($DATA); $x += 7) {
        $exp1 =  explode("-",$DATA[$x + 1]);
        $tmp1 = array(
            'FILID' => ($exp1[0] !== '')?$exp1[0]:0,
            'FLD' => ($exp1[1] !== null)?$exp1[1]:''
        );
       
        if($tmp1['FILID'] !== 0 && $tmp1['FLD'] !== ''){

            //FDB2CSV2に存在するかチェック(FILID=9999は無条件でtrue)
           
        }
    }
  }

if($rtn === 0){
    for ($x = 0; $x < count($DATA); $x+=7) {
     $exp1 =  explode("-",$DATA[$x+1]);
        $tmp1 = array(
            'FILID' => ($exp1[0] !== '')?$exp1[0]:0,
            'FLD' => ($exp1[1] !== null)?$exp1[1]:''
        );
     

if($SQLFLG !== '1'){

 if(count($BASEFIELDINFO)>0){
    foreach ($BASEFIELDINFO as $FLDINFO) {
      foreach($FLDINFO['STRDATA'] as $FLDDATA){
       if($tmp1['FLD'] === $FLDDATA['COLUMN_NAME']){
           $d2type = $FLDDATA['DDS_TYPE'];
		  // e_log('D2FLDNAME***'.print_r($D2type ,true));
	    }
     }
   }
 }
      
    $fieldInfo = array();
    
    $fieldInfo['D2TYPE']   = $d2type;
   
    $fieldInfo['DTNAME'] = $DTNAME;
    $fieldInfo['DTSEQ'] = $DATA[$x];
    $fieldInfo['DTFILID'] = $tmp1['FILID'];
    $fieldInfo['DTFLD'] = $tmp1['FLD'];
    $fieldInfo['DTSUM'] = $DATA[$x+2];
    $fieldInfo['DTAVLG'] = $DATA[$x+3];
    $fieldInfo['DTMINV'] = $DATA[$x+4];
    $fieldInfo['DTMAXV'] = $DATA[$x+5];
    $fieldInfo['DTCONT'] = $DATA[$x+6];


     $data[] = $fieldInfo;
      // e_log('LevelControlDATA***'.print_r($data ,true));
       
 }else{
      if(count($BASEGRDFLDINFO)>0){
        foreach ($BASEGRDFLDINFO as $FLDINFO) {
         
           if($tmp1['FLD'] === $FLDINFO['COLUMN_NAME']){
               $d2type = $FLDINFO['DDS_TYPE'];
               $D2type = $d2type;
             //  e_log('D2FLDNAME***'.print_r($D2type ,true));
               
        }
     }
  }
          
        
        $fieldInfo = array();
          
       
        $fieldInfo['D2TYPE']   = $D2type;
       
        $fieldInfo['DTNAME'] = $DTNAME;
        $fieldInfo['DTSEQ'] = $DATA[$x];
        $fieldInfo['DTFILID'] = $tmp1['FILID'];
        $fieldInfo['DTFLD'] = $tmp1['FLD'];
        $fieldInfo['DTSUM'] = $DATA[$x+2];
        $fieldInfo['DTAVLG'] = $DATA[$x+3];
        $fieldInfo['DTMINV'] = $DATA[$x+4];
        $fieldInfo['DTMAXV'] = $DATA[$x+5];
        $fieldInfo['DTCONT'] = $DATA[$x+6];


           $data[] = $fieldInfo;
        //   e_log('LevelControlDATASQL***'.print_r($data ,true));
          }
       }
     }
  


cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData' => umEx($data)
);

echo(json_encode($rtnAry));

