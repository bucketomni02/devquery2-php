<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("getChkOutputFileData.php");//出力ファイル

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$QRYNM = $_POST['QRYNM'];
$outputfdata     = json_decode($_POST['OUTPUTFDATA'],true);


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
$resultArr = array();
$LIBSNM='';
$LIBSDB='';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
//始ライブラリー条件//
if($rtn === 0){
    $LIBSNM = cmMer($userData[0]['WULIBL']);
}
$RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:RDB);
if($RDBNM !== ''){
    $LIBSDB = cmMer($RDBNM);
}else{
    $LIBSDB = RDB;
}
//終ライブラリー条件
//クエリー存在チェック
if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        if(count($rs['data']) === 0){
            $rtn = 3;
            $msg = showMsg('NOTEXIST_GET',array('クエリー'));
        }
    }
}
if($rtn === 0){
    $rs = cmGetQryOptionInfo($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = umEx($rs['data']);
        if(count($data)>0){
            $resultArr['2'] = $data[0]['A2'];
            $resultArr['3'] = $data[0]['A3'];
            $resultArr['4'] = $data[0]['A4'];
            $resultArr['5'] = $data[0]['A5'];
            $resultArr['6'] = $data[0]['A6'];
            $resultArr['7'] = $data[0]['A7'];
            $resultArr['8'] = $data[0]['A8'];
            $resultArr['9'] = $data[0]['A9'];
            $resultArr['A'] = $data[0]['A'];
           // $resultArr['B'] = $data[0]['B'];
            $resultArr['C'] = $data[0]['C'];
            $resultArr['D'] = $data[0]['D'];
            $resultArr['E'] = $data[0]['E'];
            $resultArr['F'] = $data[0]['F'];
            $resultArr['G'] = $data[0]['G'];
            $resultArr['H'] = $data[0]['H'];
			$resultArr['I'] = $data[0]['I'];//add by TTA
            $resultArr['J'] = $data[0]['J'];//add by TTA
        }
    }
}
//コピーだけのため出力ファイルチェック
if($rtn===0){
    if(count($outputfdata)>0){
        $D1OUTLIB=cmMer($outputfdata[0]['D1OUTLIB']);
        $D1OUTFIL=cmMer($outputfdata[0]['D1OUTFIL']);
        $D1NAME=cmMer($outputfdata[0]['D1NAME']);
        if($D1OUTLIB !=='' && $D1OUTFIL !=='' ){
            //ライブラリー
            if($rtn===0){
                if($D1OUTLIB===''){
                    $msg = showMsg('FAIL_REQ',array('ライブラリー'));
                    $rtn = 1;
                }
            }
            if($rtn===0){
                $rs = chkLIBOutput($db2con,$D1OUTLIB,$LIBSNM,$LIBSDB);
                if($rs !== 0){
                    $rtn = 1;
                    $msg = showMsg($rs,array('ライブラリー'));
                }
            }
            //ファイル
            if($rtn===0){
                if($D1OUTFIL===''){
                    $msg = showMsg('FAIL_REQ',array('ファイル'));
                    $rtn = 1;
                }
            }
            if($rtn===0){
                $rs = chkFileOutput($db2con,$D1OUTFIL);
                if($rs['result'] !== 0){
                    $rtn = 1;
                    $msg = showMsg($rs['result'],array('ファイル'));
                }
            }
            if($rtn===0){
                //ライブラリーとファイルをSYSからチェック
                $r=fnChkExistFileOutput($db2con,$D1OUTLIB,$D1OUTFIL);
                if($r['result']!==true){
                    $rtn=$r['rtn'];
                    $msg= ($rtn===4)?showMsg($r['result'],array($D1OUTLIB,$D1OUTFIL)):
                                showMsg($r['result'],array($D1OUTLIB.'/'.$D1OUTFIL));
                }
            }
            if($rtn===0){
                //出力ファイルのライブラリーとファイルがFDB2CSV1であるかどうか
                $r=fnExistFileFDB2CSV1($db2con,$D1NAME,$D1OUTLIB,$D1OUTFIL);
                if($r['result'] !==true){
                    $rtn=1;
                    $msg=showMsg($r['result'],array('ファイル'));
                }else{
                    if($r['TOTCOUNT']<=0){
                        //ライブラリーとファイルをFDB2CSV1からチェック
                        $r=fnChkD1OUTFIL($db2con,$D1OUTLIB,$D1OUTFIL);
                        if($r['result']!==true){
                            $rtn=1;
                            $msg= showMsg($r['result'],array('ファイル'));
                        }else{
                            if($r['TOTCOUNT']>0){
                                $rtn=4;//メッセージです
                                $msg= showMsg('ISEXIST_OUTC',array($D1OUTLIB,$D1OUTFIL));
                            }
                        }
                    }
                }
            }
        }
    }
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'OPTIONDATA'    => $resultArr,
    'DATA'          => $data,
    'RTN'           => $rtn,
    'MSG'           => $msg,
    'data'           => $data
);

echo(json_encode($rtn));


function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}