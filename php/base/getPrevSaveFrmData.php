<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : WEBBASE
 * PROGRAM NAME   : プレビューときの画面情報データ取得
 * PROGRAM ID     : getPrevSaveFrmData.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/08/10
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */

/*
 * 変数
 */

function getPrevSaveData($db2con,$QRYNM){
    // getBaseFileData
    // getBaseFieldData
    // getBaseCondData
    // getBaseSummaryData
    $rtn = 0;
    $msg = '';
    if($rtn === 0){
        $rs= getBaseFileData($db2con,$QRYNM);
        if($rs['RTN'] !== 0){
            $rtn = $rs['RTN'];
            $msg = $rs['MSG'];
        }else{
            $BASEFILEDATA = $rs['BASEFILEDATA'];
        }
    }
    if($rtn === 0){
        $rs = getBaseFieldData($db2con,$BASEFILEDATA);
        if($rs['RTN'] !== 0){
            $rtn = $rs['RTN'];
            $msg = $rs['MSG'];
        }else{
            $BASEFIELDDATA = $rs['DATA'];
        }
    }
    if($rtn === 0){
        $rs = getBaseCondData($db2con,$QRYNM);
        if($rs['RTN'] !== 0){
            $rtn = $rs['RTN'];
            $msg = $rs['MSG'];
        }else{
            $BASECONDDATA = $rs['BASECNDDAT'];
        }
    }
    if($rtn === 0){
        $rs = getBaseSummaryData($db2con,$QRYNM);
        if($rs['RTN'] !== 0){
            $rtn = $rs['RTN'];
            $msg = $rs['MSG'];
        }else{
            $SUMMARYDATA = $rs['SUMMARYDATA'];
        }
    }
    $rtnSaveData = array(
        'RTN' => $rtn,
        'MSG' => $msg,
        'BASEFILEDATA' => $BASEFILEDATA,
        'BASEFIELDDATA'=> $BASEFIELDDATA,
        'BASECONDDATA' => $BASECONDDATA,
        'SUMMARYDATA'  => $SUMMARYDATA
    );
    return $rtnSaveData;
}
function getBaseFileData($db2con,$QRYNM){
    $rtn = 0;
    $msg = '';

    if($rtn === 0){
        //クエリーがない場合
        $rs = fnGetFDB2CSV1($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $fdb2csv1 = $rs['data'];
            $rs = generateBaseFileData($db2con,$fdb2csv1);
            if($rs['RTN'] !== 0){
                $rtn = $rs['RTN'] ;
                $msg = $rs['MSG'];
            }else{
                $basefiledata = $rs['BASEFILEDATA'];
            }
        }
    }
    $rtnFile = array(
        'RTN' => $rtn,
        'MSG' => $msg,
        'BASEFILEDATA' => $basefiledata
    );
    return $rtnFile;
}
function getBaseFieldData($db2con,$BASEFILEINFO){
    $rtn = 0;
    $msg = '';
    $data = array();
    $fileInfoData = array();
    if($rtn === 0){
        $rs = fnGetFDB2CSV1($db2con,$BASEFILEINFO['D1NAME']);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $fdb2csv1 = $rs['data'][0];
            //$SELLIBLIST = preg_split ("/[\s]+/", $fdb2csv1['D1LIBL'] );
            $SELLIBLIST = preg_split('/ +/', $fdb2csv1['D1LIBL']);
            $BASEFILEINFO['SELLIBLIST'] = $SELLIBLIST;
            $rs = fnGetFDB2CSV2($db2con,$BASEFILEINFO['D1NAME'],0,$BASEFILEINFO['D1FILE'] ,$BASEFILEINFO['D1FILLIB'],$BASEFILEINFO['SELLIBLIST']);
            if($rs['result'] !== true){
                $rtn = 1 ;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
            }
        }
    }
    if($rtn === 0){
        $fileInfoData['D2FILID'] = 0;
        $fileInfoData['D2FDESC'] = 'プライマリ';
        $fileInfoData['FILENM'] = $BASEFILEINFO['D1FILE'];
        $fileInfoData['LIBNM'] = $BASEFILEINFO['D1FILLIB'];
        $fileInfoData['FILTYP'] = 'P';
        $fileInfoData['STRDATA'] = $data;
        $RTNINITBASEFLD[] = $fileInfoData;
    }
    $filCnt = 0;
    if($rtn === 0){
        $REFFILE = $BASEFILEINFO['REFFILE'];
        if(count($REFFILE) > 0){
            foreach ($REFFILE as $RFILVAL) {
                $rs = fnGetFDB2CSV2($db2con,$BASEFILEINFO['D1NAME'],$RFILVAL['RTRSEQ'],$RFILVAL['RTRFIL'] ,$RFILVAL['RTRLIB'],$BASEFILEINFO['SELLIBLIST']);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }else{
                    $data = $rs['data'];
                }
                
                if($rtn === 0){
                    $filCnt = $RFILVAL['RTRSEQ'];
                    $fileInfoData['D2FILID'] = $RFILVAL['RTRSEQ'];
                    $fileInfoData['D2FDESC'] = '参照'.$RFILVAL['RTRSEQ'];
                    $fileInfoData['FILENM'] = $RFILVAL['RTRFIL'];
                    $fileInfoData['LIBNM'] = $RFILVAL['RTRLIB'];
                    $fileInfoData['FILTYP'] = 'R';
                    $fileInfoData['STRDATA'] = $data;
                    $RTNINITBASEFLD[] = $fileInfoData;
                }
            }
        }
    }
    if($rtn === 0){
        $rs = fnGetFDB2CSV5($db2con,$BASEFILEINFO['D1NAME']);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
    if($rtn === 0){
        $fileInfoData['D2FILID'] = $filCnt+1;
        $fileInfoData['D2FDESC'] = '結果フィールド';
        $fileInfoData['FILENM'] = '';
        $fileInfoData['LIBNM'] = '';
        $fileInfoData['FILTYP'] = '0';
        $fileInfoData['STRDATA'] = $data;
        $RTNINITBASEFLD[] = $fileInfoData;
    }
    $rtn = array(
        'DATA' => $RTNINITBASEFLD,
        'RTN' => $rtn,
        'MSG' => $msg,
        'FILECNT' => $filCnt
    );
    return $rtn;
}
function getBaseCondData($db2con,$QRYNM){
    $rtn = 0;
    $msg = '';
    $resultarr = array();
    if($rtn === 0){
        $rs = fnGetBQRYCNDDAT($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
            $data = umEx($data);
            if(count($data) > 0){
                $resultarr = fnCreateCndData($data);
            }
        }
    }
    $rtnArr = array(
            'BASECNDDAT' => $resultarr,
            'RTN' => $rtn,
            'MSG' => $msg
        );
    return $rtnArr;
}
function getBaseSummaryData($db2con,$QRYNM){
    $rtn   = 0;
    $msg   = '';
    $sumdata  = array();
    $gpdata  = array();
    $sumkeydata = array();
    $groupingdata = array();
    $summarydata = array();

    /*
     * 処理
     */

    if($rtn === 0){
        $rs = sumGetFDB2CSV25($db2con,$QRYNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $sumdata = $rs['data'];
        }
    }
    if($rtn === 0){
        $sumdata = umEx($sumdata,false);
        if(count($sumdata) > 0){

            $sumkeydata = fnCreateSUMKEYDATA($sumdata);

            $rs = fnGetBSUMFLD($db2con,$QRYNM);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $gpdata = $rs['data'];
            }
            $gpdata = umEx($gpdata);
            if(count($gpdata) > 0){
                $groupingdata = fnCreateGROUPINGDATA($gpdata);
            }
        }
    }
    $summarydata['SUMKEYDATA']   = $sumkeydata;
    $summarydata['GROUPINGDATA'] = $groupingdata;
    $rtn = array(
        'SUMMARYDATA' => $summarydata,
        'RTN' => $rtn,
        'MSG' => $msg
    );
    return $rtn;
}
function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
function generateBaseFileData($db2con,$fdb2csv1){
    $rtn = 0;
    $msg = '';
    if(count($fdb2csv1) === 0){
        $rtn = 3;
        $msg = showMsg('NOTEXIST_GET',array('クエリー'));
    }
    if($rtn === 0){
        $fdb2csv1data = $fdb2csv1[0];
        $basefileData = array();
        $basefileData['D1NAME'] = $fdb2csv1data['D1NAME'];
        $basefileData['D1FILE'] = $fdb2csv1data['D1FILE'];
        $basefileData['D1FILMBR'] = $fdb2csv1data['D1FILMBR'];
        if($fdb2csv1data['D1FILLIB'] === ''){
            $fdb2csv1data['D1FILLIB'] = '*USRLIBL';
        }
        $basefileData['D1FILLIB'] = $fdb2csv1data['D1FILLIB'];
        $basefileData['D1TEXT'] = $fdb2csv1data['D1TEXT'];
        $basefileData['D1LIBL'] = $fdb2csv1data['D1LIBL'];
        $basefileData['D1RDB'] = $fdb2csv1data['D1RDB'];
        $res = getBREFTBLData($db2con,$basefileData['D1NAME']);
        if($res['result'] !== true ){
            $rtn = 1;
            $msg = showMsg($res['result']);
        }else{
            $breftbldata = array();
            if(count($res['data'])>0){
                foreach($res['data'] as $breftblitm){
                    $res1 = getBREFFLDData($db2con,$breftblitm['RTQRYN'],$breftblitm['RTRSEQ']);
                    if($res1['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                        break;
                    }
                    else{
                        $brefflddata = array();
                        foreach($res1['data'] as $resBreffld){
                            $breffldinfo = array();
                            $breffldinfo['RFFSEQ'] = $resBreffld['RFFSEQ'];
                            $breffldinfo['RFPFNM'] = $resBreffld['RFPFNM'];
                            $breffldinfo['RFRFNM'] = $resBreffld['RFRFNM'];
                            $brefflddata[] = $breffldinfo;
                        }
                        $breftblinfo = array();
                        $breftblinfo['RTQRYN']  = $breftblitm['RTQRYN'];
                        $breftblinfo['RTRSEQ']  = $breftblitm['RTRSEQ'];
                        $breftblinfo['RTRFIL']  = $breftblitm['RTRFIL'];
                        $breftblinfo['RTRMBR']  = $breftblitm['RTRMBR'];
                        if( $breftblitm['RTRLIB'] === ''){
                             $breftblitm['RTRLIB'] = '*USRLIBL';
                        }
                        $breftblinfo['RTRLIB']  = $breftblitm['RTRLIB'];
                        $breftblinfo['RTDESC']  = $breftblitm['RTDESC'];
                        $breftblinfo['RTJTYP']  = $breftblitm['RTJTYP'];
                        $breftblinfo['BREFFLD'] = $brefflddata;
                        
                        $breftbldata[] = $breftblinfo;
                    }
                }
            }
            $basefileData['REFFILE'] = $breftbldata;
        }
    }
    $rtnArr = array( 
                    'RTN' => $rtn ,
                    'MSG' => $msg ,
                    'BASEFILEDATA' => $basefileData
                );
                
    return $rtnArr;
}
function getBREFTBLData($db2con,$QRYNM){
    $data = array();
    $params = array();
    
    $strSQL  =  '   SELECT A.RTQRYN ';
    $strSQL .=  '        , A.RTRSEQ ';
    $strSQL .=  '        , A.RTRFIL ';
    $strSQL .=  '        , A.RTRMBR ';
    $strSQL .=  '        , A.RTRLIB ';
    $strSQL .=  '        , A.RTDESC ';
    $strSQL .=  '        , CASE A.RTJTYP ';  
    $strSQL .=  '          WHEN \'2\' THEN \'4\' '; 
    $strSQL .=  '          WHEN \'3\' THEN \'5\' '; 
    $strSQL .=  '          WHEN \'1\' THEN \'6\' '; 
    $strSQL .=  '          WHEN \'7\' THEN \'8\' ';
    $strSQL .=  '          ELSE A.RTJTYP ';
    $strSQL .=  '        END AS RTJTYP ';
    $strSQL .=  '   FROM BREFTBL A ';
    $strSQL .=  '   WHERE A.RTQRYN = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        array_push($params,$QRYNM);
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
function getBREFFLDData($db2con,$QRYNM,$RTRSEQ){
    $data = array();
    
    $strSQL .= '    SELECT A.RFQRYN ';
    $strSQL .= '        , A.RFRSEQ ';
    $strSQL .= '        , A.RFFSEQ ';
    $strSQL .= '        , A.RFPFID ';
    $strSQL .= '        , A.RFPFNM  AS RFPF ';
    $strSQL .= '        , CASE A.RFPFID '; 
    $strSQL .= '            WHEN 0 THEN concat(\'P.\',A.RFPFNM) ';
    $strSQL .= '            ELSE  concat(concat(concat(\'S\', A.RFPFID),\'.\'),A.RFPFNM) ';
    $strSQL .= '        END AS RFPFNM '; 
    $strSQL .= '        , A.RFRFID '; 
    $strSQL .= '        , A.RFRFNM  AS RFRF ';
    $strSQL .= '        , concat(concat(concat(\'S\', A.RFRFID),\'.\'),A.RFRFNM) AS RFRFNM '; 
    $strSQL .= '    FROM   BREFFLD A ';
    $strSQL .= '    WHERE  A.RFQRYN = ? ';
    $strSQL .= '    AND  A.RFRSEQ = ? ';
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($QRYNM,$RTRSEQ);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
// 【編集】結果フィールドデータ取得クエリー
function fnGetFDB2CSV5($db2con,$D1NAME){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT   ';
    $strSQL .=  '   (CASE   A.D5CSEQ'; 
    $strSQL .=  '       WHEN 0 THEN 99999 ';
    $strSQL .=  '       ELSE A.D5CSEQ END) AS SEQ '; 
    $strSQL .=  '           ,A.D5FLD AS COLUMN_NAME '; 
    $strSQL .=  '           ,A.D5LEN  AS LENGTH '; 
    $strSQL .=  '           ,A.D5TYPE AS DDS_TYPE ';
    $strSQL .=  '           ,(CASE ';
    $strSQL .=  '               WHEN A.D5TYPE = \'P\' '; 
    $strSQL .=  '                   OR A.D5TYPE = \'B\' '; 
    $strSQL .=  '                   OR A.D5TYPE = \'S\' ';
    $strSQL .=  '               THEN A.D5DEC ';
    $strSQL .=  '               ELSE \'\' ';
    $strSQL .=  '           END) AS NUMERIC_SCALE '; 
    $strSQL .=  '           ,A.D5HED  AS COLUMN_HEADING '; 
    $strSQL .=  '           ,A.D5EXP  AS FORMULA ';
    $strSQL .=  '           ,A.D5RSEQ AS SORTNO  ';
    $strSQL .=  '           ,A.D5RSTP AS SORTTYP '; 
    $strSQL .=  '           ,A.D5WEDT AS EDTCD '; 
    $strSQL .=  '           ,A.D5ASEQ AS ORDINAL_POSITION '; 
    $strSQL .=  '   FROM ';    
    $strSQL .=  '           FDB2CSV5 A ';
    $strSQL .=  '   WHERE ';   
    $strSQL .=  '           A.D5NAME = ? ';
    $strSQL .=  '   ORDER BY '; 
    $strSQL .=  '           SEQ '; 
    $strSQL .=  '          ,ORDINAL_POSITION ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($D1NAME);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                if(($row['DDS_TYPE'] === 'P' || $row['DDS_TYPE'] === 'B' || $row['DDS_TYPE'] === 'S') === false){
                    $row['NUMERIC_SCALE'] = '';
                }
                if($row['SORTNO'] == 0){
                    $row['SORTNO'] = '';
                }
                if($row['SEQ'] == 99999){
                    $row['SEQ'] = '';
                }
                $data[] = $row;
            }
            $data = umEX($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function fnGetFDB2CSV2($db2con,$D1NAME,$FILID,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT ';
    $strSQL .=  '         FDB2CSV2DATA.SEQ            AS SEQ ';
    $strSQL .=  '       , SYSTABDATA.COLUMN_NAME      AS COLUMN_NAME ';
    $strSQL .=  '       , SYSTABDATA.LENGTH           AS LENGTH ';
    $strSQL .=  '       , SYSTABDATA.DDS_TYPE         AS DDS_TYPE ';
    $strSQL .=  '       , SYSTABDATA.NUMERIC_SCALE    AS NUMERIC_SCALE ';
    $strSQL .=  '       ,(CASE ';
    $strSQL .=  '           WHEN FDB2CSV2DATA.COLUMN_HEADING IS NULL ';
    $strSQL .=  '           AND FDB2CSV2DATA.COLUMN_NAME  IS NULL ';
    $strSQL .=  '           THEN SYSTABDATA.COLUMN_HEADING ';
    $strSQL .=  '           ELSE FDB2CSV2DATA.COLUMN_HEADING ';
    $strSQL .=  '         END) AS COLUMN_HEADING '; 
    $strSQL .=  '       , FDB2CSV2DATA.FORMULA        AS FORMULA ';
    $strSQL .=  '       , FDB2CSV2DATA.SORTNO         AS SORTNO ';
    $strSQL .=  '       , FDB2CSV2DATA.SORTTYP        AS SORTTYP ';
    $strSQL .=  '       , FDB2CSV2DATA.EDTCD          AS EDTCD ';
    $strSQL .=  '       , SYSTABDATA.ORDINAL_POSITION AS ORDINAL_POSITION ';
    $strSQL .=  '   FROM  '; 
    $strSQL .=  '       (SELECT  ';
    $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
    $strSQL .=  '          ,A.LENGTH '; 
    $strSQL .=  '          ,CASE A.DDS_TYPE ';
    $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
    $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
    $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
    $strSQL .=  '            ELSE A.DDS_TYPE '; 
    $strSQL .=  '            END AS DDS_TYPE ';
    $strSQL .=  '          ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG==='1'){
        $strSQL .=  '          ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .=  '          ,A.COLUMN_HEADING ';
    }
    $strSQL .=  '          ,A.ORDINAL_POSITION '; 
    $strSQL .=  '       FROM '. SYSCOLUMN2 . ' A '; 
    $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
    $strSQL .=  '       AND A.TABLE_NAME = ? '; 
    //$strSQL .=  '       AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\') ';
//    $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
    $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ) SYSTABDATA ';
    $strSQL .=  '   LEFT JOIN ';
    $strSQL .=  '       (SELECT  ';
    $strSQL .=  '            (CASE A.D2CSEQ ';
    $strSQL .=  '            WHEN 0 THEN 99999 '; 
    $strSQL .=  '            ELSE A.D2CSEQ ';
    $strSQL .=  '            END) '; 
    $strSQL .=  '            AS SEQ '; 
    $strSQL .=  '           ,A.D2FLD  AS COLUMN_NAME ';
    $strSQL .=  '           ,A.D2HED  AS COLUMN_HEADING '; 
    $strSQL .=  '           ,\'\'     AS FORMULA ';
    $strSQL .=  '           ,CASE A.D2RSEQ ';
    $strSQL .=  '             WHEN 0 THEN 99999 ';
    $strSQL .=  '             ELSE A.D2RSEQ '; 
    $strSQL .=  '             END AS SORTNO ';
    $strSQL .=  '           ,A.D2RSTP AS SORTTYP '; 
    $strSQL .=  '           ,A.D2WEDT AS EDTCD ';
    $strSQL .=  '       FROM FDB2CSV2 A ';
    $strSQL .=  '       WHERE A.D2NAME = ?';
    $strSQL .=  '       AND   A.D2FILID = ? ';
    $strSQL .=  '       ORDER BY A.D2CSEQ,A.D2RSEQ ) FDB2CSV2DATA ';
    $strSQL .=  '   ON SYSTABDATA.COLUMN_NAME = FDB2CSV2DATA.COLUMN_NAME ';
    $strSQL .=  '   ORDER BY SEQ ,SORTNO, ORDINAL_POSITION ';
    
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$TABLE_NAME,$D1NAME,$FILID);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        if($row['SORTNO'] == 99999){
                            $row['SORTNO'] = '';
                        }
                        if($row['SEQ'] == 99999){
                            $row['SEQ'] = '';
                        }
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
            $data = umEX($data);
            $data = array('result' => true,'data' => $data);
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME,$D1NAME,$FILID);
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row['SORTNO'] == 99999){
                        $row['SORTNO'] = '';
                    }
                    if($row['SEQ'] == 99999){
                        $row['SEQ'] = '';
                    }
                    $data[] = $row;
                }
                $data = umEX($data,false);
                $data = array('result' => true,'data' => $data);
            }
        }
    }
    return $data;
}
/**
  *---------------------------------------------------------------------------
  * 条件情報を取得
  *---------------------------------------------------------------------------
  **/
function fnGetBQRYCNDDAT($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=   '   SELECT QRYCND.CNQRYN ';
    $strSQL .=   '        , QRYCND.CNFILID ';
    $strSQL .=   '        , QRYCND.FILSEQ ';
    $strSQL .=   '        , QRYCND.CNFIL ';
    $strSQL .=   '        , QRYCND.CNMSEQ ';
    $strSQL .=   '        , GRPCND.CNDCNT ';
    $strSQL .=   '        , GRPCND.CNSCNT ';
    $strSQL .=   '        , GRPCND.CDCNT ';
    $strSQL .=   '        , QRYCND.CNSSEQ ';
    $strSQL .=   '        , QRYCND.CNFLDN ';
    $strSQL .=   '        , QRYCND.CNAOKB ';         
    $strSQL .=   '        , QRYCND.CNCKBN ';
    $strSQL .=   '        , QRYCND.CNSTKB ';
    $strSQL .=   '        , QRYCND.CDCDCD ';
    $strSQL .=   '        , QRYCND.CDDAT ';
    $strSQL .=   '        , QRYCND.CDBCNT ';
    $strSQL .=   '        , QRYCND.CDFFLG ';
    $strSQL .=   '   FROM ';        
    $strSQL .=   '       ( ';
    $strSQL .=   '           SELECT A.CNQRYN ';
    $strSQL .=   '                , A.CNFILID ';
    $strSQL .=   '                , FILCD.FILSEQ ';
    $strSQL .=   '                , ( CASE ';
    $strSQL .=   '                       WHEN A.CNFILID = 0 THEN \'P\' ';          
    $strSQL .=   '                       WHEN A.CNFILID <= FILCD.FILSEQ ';
    $strSQL .=   '                       THEN CONCAT(\'S\', A.CNFILID ) ';
    $strSQL .=   '                       ELSE \'K\' ';              
    $strSQL .=   '                    END ';             
    $strSQL .=   '                   ) AS CNFIL ';       
    $strSQL .=   '               , A.CNMSEQ ';
    $strSQL .=   '               , A.CNSSEQ ';
    $strSQL .=   '               , A.CNFLDN ';
    $strSQL .=   '               , A.CNAOKB ';
    $strSQL .=   '               , A.CNCKBN ';
    $strSQL .=   '               , A.CNSTKB ';      
    $strSQL .=   '               , B.CDCDCD ';
    $strSQL .=   '               , B.CDDAT ';
    $strSQL .=   '               , B.CDBCNT ';
    $strSQL .=   '               , B.CDFFLG ';
    $strSQL .=   '           FROM BQRYCND A ';
    $strSQL .=   '           LEFT JOIN  BCNDDAT B ';
    $strSQL .=   '           ON    A.CNQRYN  = B.CDQRYN ';
    $strSQL .=   '           AND   A.CNFILID = B.CDFILID ';   
    $strSQL .=   '           AND   A.CNMSEQ  = B.CDMSEQ ';
    $strSQL .=   '           AND   A.CNSSEQ  = B.CDSSEQ ';
    $strSQL .=   '           LEFT JOIN  ( ';
    $strSQL .=   '               SELECT RTQRYN,MAX(RTRSEQ) AS FILSEQ  ';          
    $strSQL .=   '               FROM BREFTBL ';
    $strSQL .=   '               WHERE RTQRYN = ? ';
    $strSQL .=   '               GROUP BY  RTQRYN ) FILCD ';
    $strSQL .=   '           ON FILCD.RTQRYN = A.CNQRYN ';   
    $strSQL .=   '           WHERE   A.CNQRYN  = ? ';
    $strSQL .=   '           ORDER BY A.CNMSEQ ,A.CNSSEQ  ,B.CDCDCD ';
    $strSQL .=   '       ) QRYCND , ';
    $strSQL .=   '       ( ';
    $strSQL .=   '           SELECT A.CNQRYN ';
    $strSQL .=   '                , A.CNMSEQ ';
    $strSQL .=   '                , B.CNDCNT ';
    $strSQL .=   '                , A.CNSCNT ';             
    $strSQL .=   '                , C.CDSSEQ ';
    $strSQL .=   '                , C.CDCNT ';
    $strSQL .=   '           FROM ( '; 
    $strSQL .=   '                   SELECT CNQRYN ';
    $strSQL .=   '                        , CNMSEQ ';           
    $strSQL .=   '                        , COUNT(CNSSEQ) CNSCNT ';
    $strSQL .=   '                     FROM BQRYCND ';
    $strSQL .=   '                    WHERE CNQRYN  = ? ';
    $strSQL .=   '                 GROUP BY CNQRYN ';              
    $strSQL .=   '                        , CNMSEQ ';
    $strSQL .=   '               ) A ';
    $strSQL .=   '               ,( ';
    $strSQL .=   '                   SELECT CNQRYN ';
    $strSQL .=   '                        , MAX(CNMSEQ) AS CNDCNT ';
    $strSQL .=   '                     FROM BQRYCND ';      
    $strSQL .=   '                    WHERE CNQRYN  = ? ';
    $strSQL .=   '                 GROUP BY CNQRYN ';
    $strSQL .=   '               ) B ';
    $strSQL .=   '               ,( ';
    $strSQL .=   '                   SELECT QRYC.CNQRYN AS CDQRYN ';
    $strSQL .=   '                        , QRYC.CNMSEQ AS CDMSEQ ';
    $strSQL .=   '                        , QRYC.CNSSEQ AS CDSSEQ ';
    $strSQL .=   '                        , COUNT(CDAT.CDCDCD) CDCNT ';        
    $strSQL .=   '                     FROM BQRYCND QRYC '; 
    $strSQL .=   '                LEFT JOIN BCNDDAT CDAT ';
    $strSQL .=   '                       ON QRYC.CNQRYN  = CDAT.CDQRYN ';
    $strSQL .=   '                      AND QRYC.CNMSEQ = CDAT.CDMSEQ '; 
    $strSQL .=   '                      AND QRYC.CNSSEQ = CDAT.CDSSEQ ';
    $strSQL .=   '                    WHERE  QRYC.CNQRYN  = ? ';
    $strSQL .=   '                 GROUP BY QRYC.CNQRYN ';
    $strSQL .=   '                        , QRYC.CNMSEQ ';
    $strSQL .=   '                        , QRYC.CNSSEQ ';
    $strSQL .=   '                        , CDAT.CDQRYN ';
    $strSQL .=   '                        , CDAT.CDMSEQ ';
    $strSQL .=   '                        , CDAT.CDSSEQ '; 
    $strSQL .=   '               )C ';
    $strSQL .=   '           WHERE A.CNQRYN = B.CNQRYN ';   
    $strSQL .=   '             AND A.CNQRYN = C.CDQRYN ';
    $strSQL .=   '             AND B.CNQRYN = C.CDQRYN ';
    $strSQL .=   '             AND A.CNMSEQ = C.CDMSEQ ';
    $strSQL .=   '       ) GRPCND ';   
    $strSQL .=   '   WHERE QRYCND.CNQRYN =  GRPCND.CNQRYN ';     
    $strSQL .=   '     AND QRYCND.CNMSEQ =  GRPCND.CNMSEQ ';
    $strSQL .=   '     AND QRYCND.CNSSEQ =  GRPCND.CDSSEQ ';
    $strSQL .=   '   ORDER BY QRYCND.CNMSEQ ';
    $strSQL .=   '          , QRYCND.CNSSEQ ';

    $params = array($QRYNM,$QRYNM,$QRYNM,$QRYNM,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnCreateCndData($bqrycnddat){
    $cdcnt = '';
    $cnmseq = '';
    $cnsseq = '';
    $bqrycnddatRes = array();
    foreach($bqrycnddat as $bqrycnddatdata){
        if($cnmseq != $bqrycnddatdata['CNMSEQ']){
            $bqrycnd = array();
            $cnmseq = $bqrycnddatdata['CNMSEQ'];
            if($bqrycnddatdata['CNSSEQ'] == 1){
                $cnsseq = '';
                $bqrycnd['CNDIDX'] = $bqrycnddatdata['CNMSEQ'];
                $bqrycnd['SQRYAO'] = $bqrycnddatdata['CNAOKB'];
                $bqrycnd['CDATACNT'] = $bqrycnddatdata['CNSCNT'];
                $bqrycnd['CNDSDATA'] = array();
            }
        }
        if($cnmseq == $bqrycnddatdata['CNMSEQ']){
            if($cnsseq != $bqrycnddatdata['CNSSEQ']){
                $cnsseq = $bqrycnddatdata['CNSSEQ'];
                $bqrycndinfo = array();
                $bqrycndinfo['CNDDATA'] = array();
				$bqrycndinfo['CNDDATAREP']=array();
                $cdcnt = '';
                $cdcnt = $bqrycnddatdata['CDCNT'];
                $bqrycndinfo['CDATAIDX'] = $cnsseq;
                if($bqrycnddatdata['CNSSEQ'] > 1){
                    $bqrycndinfo['CNDANDOR'] = $bqrycnddatdata['CNAOKB'];
                }else{
                    $bqrycndinfo['CNDANDOR'] = '';
                }
                if($bqrycnddatdata['CNFIL'] === ''){
                    $bqrycndinfo['CNDFLD']   = $bqrycnddatdata['CNFLDN'];
                }else{
                    $bqrycndinfo['CNDFLD']   = $bqrycnddatdata['CNFIL'].'.'.$bqrycnddatdata['CNFLDN'];
                }
                $bqrycndinfo['CNDKBN']   = $bqrycnddatdata['CNCKBN'];
                $bqrycndinfo['CNDTYP']   = $bqrycnddatdata['CNSTKB'];
                $bqrycndinfo['CDCNT']    = $bqrycnddatdata['CDCNT'];
            }
            if($bqrycndinfo['CDATAIDX'] == $bqrycnddatdata['CNSSEQ']){
                if($bqrycndinfo['CDCNT'] == 0){
                    $cnddat = array();
                    $cnddat['DATA'] = '';
                    $cnddat['FFLG'] = '0';
                    if($bqrycndinfo['CNDKBN'] === 'RANGE'){
                        $bqrycndinfo['CNDDATA'][] = $cnddat;
						$bqrycndinfo['CNDDATAREP'][] =$cnddat['DATA'];//logmasterのため変わるアレイ形
                    }
                    $bqrycndinfo['CNDDATA'][] = $cnddat;
					$bqrycndinfo['CNDDATAREP'][] =$cnddat['DATA'];//logmasterのため変わるアレイ形
                    $bqrycnd['CNDSDATA'][]  = $bqrycndinfo;
                }else{
                    if((int)$bqrycnddatdata['CDBCNT'] > 0){
                        $bqrycnddatdata['CDDAT'] = str_pad($bqrycnddatdata['CDDAT'], (int)$bqrycnddatdata['CDBCNT']);
                    }
                    $cnddat = array();
                    $cnddat['DATA'] = $bqrycnddatdata['CDDAT'];
                    $cnddat['FFLG'] = $bqrycnddatdata['CDFFLG'];
                    $bqrycndinfo['CNDDATA'][] = $cnddat;
					$bqrycndinfo['CNDDATAREP'][] = $cnddat['DATA'];//logmasterのため変わるアレイ形
                    if ($bqrycndinfo['CDCNT'] == count($bqrycndinfo['CNDDATA'])){
                        $bqrycnd['CNDSDATA'][]  = $bqrycndinfo;
                    }
                }
                
            }
            if($bqrycnd['CDATACNT'] == count($bqrycnd['CNDSDATA'])){
                $bqrycnddatRes[] = $bqrycnd;
            }
        }
    }
    return $bqrycnddatRes;
}
/**
  *---------------------------------------------------------------------------
  * フィールド情報を取得
  *---------------------------------------------------------------------------
  **/
function sumGetFDB2CSV25($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT A.* ';
    $strSQL .=  '   FROM ( ';        
    $strSQL .=  '           (SELECT D2NAME ';
    $strSQL .=  '                 , D2FILID ';
    $strSQL .=  '                 , D2FLD ';
    $strSQL .=  '                 , D2GSEQ ';
    $strSQL .=  '                 , (CASE '; 
    $strSQL .=  '                       WHEN D2FILID = 0 THEN \'P\' ';
    $strSQL .=  '                       WHEN D2FILID > 0 THEN concat(\'S\',D2FILID) ';
    $strSQL .=  '                  END) AS FILTYP ';            
    $strSQL .=  '           FROM  FDB2CSV2 '; 
    $strSQL .=  '           WHERE D2NAME = ? ';
    $strSQL .=  '           AND   D2GSEQ > 0) ';
    $strSQL .=  '           UNION ALL '; 
    $strSQL .=  '           (SELECT  D5NAME AS D2NAME ';
    $strSQL .=  '               , 9999 AS D2FILID ';
    $strSQL .=  '               , D5FLD AS D2FLD ';
    $strSQL .=  '               , D5GSEQ AS D2GSEQ ';
    $strSQL .=  '               , \'K\' AS FILTYP ';
    $strSQL .=  '           FROM  FDB2CSV5 '; 
    $strSQL .=  '           WHERE D5NAME = ? ';
    $strSQL .=  '           AND   D5GSEQ > 0 ) ';
    $strSQL .=  '       )  A ';
    $strSQL .=  '   ORDER BY A.D2GSEQ ';

    $params = array($QRYNM,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnGetBSUMFLD($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '    SELECT A.SFQRYN ';
    $strSQL .=  '         , A.SFFILID ';
    $strSQL .=  '         , A.SFFLDNM ';
    $strSQL .=  '         , A.SFSEQ ';
    $strSQL .=  '         , A.SFGMES ';
    $strSQL .=  '         , FILCD.FILSEQ ';
    $strSQL .=  '         , CASE '; 
    $strSQL .=  '                WHEN A.SFFILID = 0 THEN \'P\' ';
    $strSQL .=  '                WHEN A.SFFILID <= FILCD.FILSEQ THEN concat(\'S\',A.SFFILID ) ';
    $strSQL .=  '                ELSE \'K\' ';
    $strSQL .=  '              END AS FILTYP ';
    $strSQL .=  '    FROM BSUMFLD A ';   
    $strSQL .=  '    LEFT JOIN ';
    $strSQL .=  '        ( ';
    $strSQL .=  '            SELECT RTQRYN ';
    $strSQL .=  '                 , MAX(RTRSEQ) AS FILSEQ ';          
    $strSQL .=  '            FROM   BREFTBL ';
    $strSQL .=  '            WHERE  RTQRYN = ? ';
    $strSQL .=  '            GROUP BY  RTQRYN '; 
    $strSQL .=  '        ) FILCD ';
    $strSQL .=  '    ON    FILCD.RTQRYN = A.SFQRYN ';
    $strSQL .=  '    WHERE A.SFQRYN     = ? ';
    $strSQL .=  '    ORDER BY A.SFSEQ ';

    $params = array($QRYNM,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnCreateSUMKEYDATA($sumdata){
    $sumkeydata = array();
    foreach($sumdata as $sumkey){
        $sumkeyInfo = array();
        $sumkeyInfo['GSEQ']     = $sumkey['D2GSEQ'];
        $sumkeyInfo['FLDVAL']   = $sumkey['FILTYP'].'.'.$sumkey['D2FLD'];
        $sumkeydata[]           = $sumkeyInfo;
    }
    return $sumkeydata;
}
function fnCreateGROUPINGDATA($gpdata){
    $groupingdata = array();
    foreach($gpdata as $gpkey){
        $gpInfo = array();
        $gpInfo['GSEQ']     = $gpkey['SFSEQ'];
        $gpInfo['FLDVAL']   = $gpkey['FILTYP'].'.'.$gpkey['SFFLDNM'];
        $gpInfo['GMES']     = $gpkey['SFGMES'];
        $groupingdata[]     = $gpInfo;
    }
    return $groupingdata;
}
