<?php
// FDB2CSV1取得
function getFDB2CSV1($db2con,$QRYNM,$newLib=''){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    if($newLib!==''){
        $strSQL .= '      FROM '.$newLib.'.FDB2CSV1 ';
    }else{
        $strSQL .= '      FROM FDB2CSV1 ';
    }
    $strSQL .= '     WHERE D1NAME = ? ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());//
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());//
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
               $data = array('result' => 'NOTEXIST_GET'); 
            }else{
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data);
            }
        }
    }
    return $data;
}
//参照テーブル情報取得
function getBREFTBLFLD($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    
    $strSQL .= '    SELECT A.RTQRYN ';
    $strSQL .= '         , A.RTRSEQ ';
    $strSQL .= '         , A.RTRFIL ';
    $strSQL .= '         , A.RTRMBR ';
    $strSQL .= '         , A.RTRLIB ';
    $strSQL .= '         , CASE A.RTJTYP ';
    $strSQL .= '            WHEN 1 THEN \'JOIN\' ';
    $strSQL .= '            WHEN 2 THEN \'LEFT JOIN\' ';
    $strSQL .= '            WHEN 3 THEN \'RIGHT JOIN\' ';
    $strSQL .= '            WHEN 4 THEN \'LEFT JOIN\' ';
    $strSQL .= '            WHEN 5 THEN \'RIGHT JOIN\' ';
    $strSQL .= '            WHEN 6 THEN \'JOIN\' ';
    $strSQL .= '            WHEN 7 THEN \'FULL JOIN\' ';
    $strSQL .= '            WHEN 8 THEN \'FULL JOIN\' ';
    $strSQL .= '            WHEN 9 THEN \'CROSS JOIN\' ';
    $strSQL .= '           END AS RTJTYP ';
    $strSQL .= '         , B.RFFSEQ ';
    $strSQL .= '         , B.RFPFID ';
    $strSQL .= '         , B.RFPFNM  AS RFPF';
    $strSQL .= '         , CASE B.RFPFID ';
    $strSQL .= '            WHEN 0 THEN concat(\'P.\',B.RFPFNM)';
    $strSQL .= '            ELSE  concat(concat(concat(\'S\',B.RFPFID),\'.\'),B.RFPFNM) ';
    $strSQL .= '           END AS RFPFNM ';
    $strSQL .= '         , B.RFTEISU ';//extract checkbox value
    $strSQL .= '         , B.RFRFID ';
    $strSQL .= '         , B.RFRFNM  AS RFRF';	
    $strSQL .= '         , CASE B.RFTEISU ';//check checkbox value
    $strSQL .= '           	WHEN \'0\' THEN concat(concat(concat(\'S\',B.RFRFID),\'.\'),B.RFRFNM)';
    $strSQL .= '           	WHEN \'\' THEN concat(concat(concat(\'S\',B.RFRFID),\'.\'),B.RFRFNM)';
	$strSQL .= '            ELSE  B.RFRFNM ';
    $strSQL .= '           END AS RFRFNM ';//end
    //$strSQL .= '         , concat(concat(concat(\'S\',B.RFRFID),\'.\'),B.RFRFNM) AS RFRFNM';
    $strSQL .= '         , B.RFRKBN ';
    $strSQL .= '         , B.RFPFLEN ';
    $strSQL .= '         , B.RFPFTYPE ';
    $strSQL .= '         , B.RFRFLEN  ';
    $strSQL .= '         , B.RFRFTYPE' ;
    $strSQL .= '    FROM BREFTBL A ';
    $strSQL .= '    LEFT JOIN '; 
    //$strSQL .= '         BREFFLD B ';
    $strSQL .= '        (SELECT ';
    $strSQL .= '            AA.*, ';
    $strSQL .= '            BB.D2LEN AS RFPFLEN , ';
    $strSQL .= '            BB.D2TYPE AS RFPFTYPE , ';
    //$strSQL .= '            CC.D2LEN AS RFRFLEN , ';
    //$strSQL .= '            CC.D2TYPE AS RFRFTYPE ';
	//Start add second condition 
	$strSQL .= '		CASE AA.RFTEISU';
	$strSQL .= '		  WHEN 1 THEN BB.D2LEN';
	$strSQL .= '		  ELSE CC.D2LEN';
	$strSQL .= '		END      AS RFRFLEN,';
	$strSQL .= '		CASE AA.RFTEISU';
	$strSQL .= '		  WHEN 1 THEN BB.D2TYPE';
	$strSQL .= '		  ELSE CC.D2TYPE';
	$strSQL .= '		END AS RFRFTYPE';
	//End

    $strSQL .= '        FROM ';
    $strSQL .= '            BREFFLD AS AA ';
    $strSQL .= '        LEFT JOIN FDB2CSV2 AS BB ';
    $strSQL .= '        ON  AA.RFQRYN = BB.D2NAME ';
    $strSQL .= '        AND AA.RFPFID = BB.D2FILID ';
    $strSQL .= '        AND AA.RFPFNM = BB.D2FLD ';
    $strSQL .= '        LEFT JOIN  FDB2CSV2 AS CC ';
    $strSQL .= '        ON AA.RFQRYN = CC.D2NAME ';
    $strSQL .= '        AND AA.RFRFID = CC.D2FILID ';
    $strSQL .= '        AND AA.RFRFNM = CC.D2FLD ';
    $strSQL .= '        WHERE AA.RFQRYN = ?) B ';
    $strSQL .= '    ON   A.RTQRYN = B.RFQRYN ';
    $strSQL .= '    AND  A.RTRSEQ = B.RFRSEQ ';
    $strSQL .= '    WHERE A.RTQRYN  = ? ';
    $strSQL .= '    ORDER BY ';
    $strSQL .= '          A.RTRSEQ ';
    $strSQL .= '        , B.RFFSEQ ';

    
    $params = array($QRYNM,$QRYNM);
    e_log('STRSQL for left join**'.$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }            
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
//FDB2CSV2取得
function getFDB2CSV2($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM FDB2CSV2 ';
    $strSQL .= '     WHERE D2NAME = ? ';
    $strSQL .= '     ORDER BY D2FILID ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
//FDB2CSV5取得
function getFDB2CSV5($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT ';
    $strSQL .= '    D5NAME, ';
    $strSQL .= '    D5ASEQ, ';
    $strSQL .= '    D5FLD, ';
    $strSQL .= '    D5HED, ';
    $strSQL .= '    D5CSEQ, ';
    $strSQL .= '    D5GSEQ, ';
    $strSQL .= '    D5GMES, ';
    $strSQL .= '    D5IPDC, ';
    $strSQL .= '    D5WEDT, ';
    $strSQL .= '    D5TYPE, ';
    $strSQL .= '    D5LEN, ';
    $strSQL .= '    D5DEC, ';
    $strSQL .= '    D5EXP, ';
    $strSQL .= '    CASE WHEN D5EXPD = \'\' THEN D5EXP ELSE D5EXPD END AS D5EXPD, ';
    $strSQL .= '    D5DNLF, ';
    $strSQL .= '    D5RSEQ, ';
    $strSQL .= '    D5RSTP  ';
    $strSQL .= '      FROM FDB2CSV5 ';
    $strSQL .= '     WHERE D5NAME = ? ';
    $strSQL .= '     ORDER BY D5ASEQ ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
//検索条件情報取得
function getBQRYCNDDAT($db2con,$QRYNM){

    $strSQL = '';
    $data   = array();

    $strSQL .= '    SELECT A.CNQRYN ';
    $strSQL .= '        , A.CNFILID ';
    $strSQL .= '        , A.CNMSEQ ';
    $strSQL .= '        , A.CNSSEQ ';
    $strSQL .= '        , CASE A.CNAOKB ';
    $strSQL .= '           WHEN \'AN\' THEN \'AND\' ';
    $strSQL .= '           WHEN \'OR\' THEN \'OR\' ';
    $strSQL .= '         END AS CNAOKB ';
    $strSQL .= '        , A.CNFLDN ';
    $strSQL .= '        , A.CNCKBN ';
    $strSQL .= '        , A.CNSTKB ';
    $strSQL .= '        , B.CDCDCD ';
    $strSQL .= '        , B.CDDAT '; 
    $strSQL .= '    FROM BQRYCND A ';
    $strSQL .= '    LEFT JOIN ';
    $strSQL .= '        BCNDDAT B ';
    $strSQL .= '    ON  A.CNQRYN   = B.CDQRYN ';
    $strSQL .= '    AND A.CNFILID  = B.CDFILID ';
    $strSQL .= '    AND A.CNMSEQ   = B.CDMSEQ ';
    $strSQL .= '    AND A.CNSSEQ   = B.CDSSEQ ';
    $strSQL .= '    WHERE A.CNQRYN = ? ';
    $strSQL .= '    ORDER BY ';
    $strSQL .= '          A.CNMSEQ ';
    $strSQL .= '        , A.CNSSEQ ';
    $strSQL .= '        , B.CDCDCD ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
//検索条件取得
function getBQRYCND($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL      .=  '   SELECT A.CNQRYN ';
    $strSQL      .=  '       , A.CNFILID '; 
    $strSQL      .=  '       , A.CNMSEQ '; 
    $strSQL      .=  '       , A.CNSSEQ '; 
    $strSQL      .=  '       , CASE A.CNAOKB '; 
    $strSQL      .=  '          WHEN \'AN\' THEN \'AND\' '; 
    $strSQL      .=  '          WHEN \'OR\' THEN \'OR\' '; 
    $strSQL      .=  '        END AS CNAOKB '; 
    $strSQL      .=  '       , A.CNFLDN '; 
    $strSQL      .=  '       , A.CNCKBN '; 
    $strSQL      .=  '       , A.CNSTKB ';
    $strSQL      .=  '       , FLDTBL.D2TYPE ';
    $strSQL      .=  '       , FLDTBL.D2LEN ';
    $strSQL      .=  '       , FLDTBL.D2DEC ';
    $strSQL      .=  '   FROM BQRYCND A '; 
    $strSQL      .=  '   LEFT JOIN ( ';
    $strSQL      .=  '       SELECT D2NAME ';
    $strSQL      .=  '            , D2FILID ';
    $strSQL      .=  '            , D2FLD ';
    $strSQL      .=  '            , D2CSEQ ';
    $strSQL      .=  '            , D2TYPE ';
    $strSQL      .=  '            , D2LEN ';
    $strSQL      .=  '            , D2DEC ';
    $strSQL      .=  '       FROM   FDB2CSV2 ';
    $strSQL      .=  '       WHERE  D2NAME = ? ';
    $strSQL      .=  '       UNION ALL ';
    $strSQL      .=  '       SELECT D5NAME AS D2NAME ';
    $strSQL      .=  '            , 9999   AS D2FILID ';
    $strSQL      .=  '            , D5FLD  AS D2FLD ';
    $strSQL      .=  '            , D5CSEQ AS D2CSEQ ';
    $strSQL      .=  '            , D5TYPE AS D2TYPE ';
    $strSQL      .=  '            , D5LEN  AS D2LEN ';
    $strSQL      .=  '            , D5DEC  AS D2DEC ';
    $strSQL      .=  '       FROM   FDB2CSV5 ';
    $strSQL      .=  '       WHERE  D5NAME = ? ';
    $strSQL      .=  '       ) FLDTBL ';
    $strSQL      .=  '   ON A.CNFILID = FLDTBL.D2FILID ';
    $strSQL      .=  '   AND A.CNFLDN = D2FLD ';
    $strSQL      .=  '       WHERE A.CNQRYN = ?  '; 
    $strSQL      .=  '       ORDER BY '; 
    $strSQL      .=  '             A.CNMSEQ '; 
    $strSQL      .=  '           , A.CNSSEQ ';
    $params = array($QRYNM,$QRYNM,$QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
//検索条件データ取得
function getBCNDDAT($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM BCNDDAT ';
    $strSQL .= '     WHERE CDQRYN = ? ';
    $strSQL .= '     ORDER BY CDMSEQ ';
    $strSQL .= '            , CDSSEQ ';
    $strSQL .= '            , CDCDCD ';   
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
//検索条件ごとにデータ取得
function getBCNDDATbyCND($db2con,$bqrycnd){
    $strSQL = '';
    $data   = array();

    $strSQL .= '    SELECT * ';
    $strSQL .= '      FROM BCNDDAT '; 
    $strSQL .= '     WHERE CDQRYN = ? ';
    $strSQL .= '       AND CDFILID = ? '; 
    $strSQL .= '       AND CDMSEQ = ? '; 
    $strSQL .= '       AND CDSSEQ = ? '; 
    $strSQL .= '    ORDER BY CDMSEQ '; 
    $strSQL .= '           , CDSSEQ '; 
    $strSQL .= '           , CDCDCD '; 
    $params = array( 
                    $bqrycnd['CNQRYN']
                  , $bqrycnd['CNFILID']
                  , $bqrycnd['CNMSEQ']
                  , $bqrycnd['CNSSEQ']
                   );    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
//画面上パラメータを検索条件ごとにデータ取得
function getFRMCNDDATbyCND($bqrycnd,$frmCndParamLst){
    $strSQL = '';
    $data   = array();
    foreach ($frmCndParamLst AS $frmcndparam){
        if($frmcndparam['CDFILID'] === $bqrycnd['CNFILID'] 
           && $frmcndparam['CDMSEQ'] === $bqrycnd['CNMSEQ']
           && $frmcndparam['CDSSEQ'] === $bqrycnd['CNSSEQ']
           ){
               if($frmcndparam['CDDAT'] !== ''){
                    $data[] = $frmcndparam;
                }
           }
    }
    $data = array('result' => true,'data' => $data);
    return $data;
}
//抽出フィールド情報取得（FDB2CSV2+FDB2CSV5）
function getFDB2CSV25($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();

    $strSQL .= '    SELECT A.* ';
    $strSQL .= '    FROM ( ';       
    $strSQL .= '            (SELECT D2NAME ';
    $strSQL .= '                  , D2FILID ';
    $strSQL .= '                  , D2FLD ';
    $strSQL .= '                  , D2GSEQ ';
    $strSQL .= '            FROM  FDB2CSV2 '; 
    $strSQL .= '            WHERE D2NAME = ? ';
    $strSQL .= '            AND   D2GSEQ > 0) ';
    $strSQL .= '            UNION ALL '; 
    $strSQL .= '            (SELECT  D5NAME AS D2NAME ';
    $strSQL .= '                , 9999 AS D2FILID ';
    $strSQL .= '                , D5FLD AS D2FLD ';
    $strSQL .= '                , D5GSEQ AS D2GSEQ ';
    $strSQL .= '            FROM  FDB2CSV5 '; 
    $strSQL .= '            WHERE D5NAME = ? ';
    $strSQL .= '            AND   D5GSEQ > 0 ) ';
    $strSQL .= '        )  A '; 
    $strSQL .= '    ORDER BY A.D2GSEQ ';
    
    $params = array($QRYNM,$QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
//サマリフィールド情報取得（BSUMFLD）
function getBSUMFLD($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();

    $strSQL .= '    SELECT A.SFQRYN ';
    $strSQL .= '         , A.SFFILID ';
    $strSQL .= '         , A.SFFLDNM ';
    $strSQL .= '         , A.SFSEQ ';
    $strSQL .= '         , A.SFGMES ';
    $strSQL .= '    FROM BSUMFLD A ';   
    $strSQL .= '    WHERE A.SFQRYN     = ? ';
    $strSQL .= '    ORDER BY A.SFSEQ ';
    
    $params = array($QRYNM);
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/**
*SEQを入力していない項目を制御キーに指定
*
**/
function getSeiGyoColumnBySEQZERO($db2con,$D1NAME) {
	$rs=true;
    $data = array();
	$dataTest=array();
    $params = array();
    $strSQL = ' SELECT DCFILID1,DCFLD1,DCFILID2,DCFLD2,DCFILID3,DCFLD3, ';
	$strSQL.= ' DCFILID4,DCFLD4,DCFILID5,DCFLD5,DCFILID6,DCFLD6 ';
	$strSQL.= ' FROM DB2COLM WHERE DCNAME=? ';
	$params=array($D1NAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
		$rs='FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
			$rs='FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
				$dataTest[]=$row;
            }
			$dataResult=$dataTest[0];
			for($i=1;$i<=6;$i++){
				if(cmMer($dataResult["DCFLD".$i])!==''){
					$resExp1=explode(",",cmMer($dataResult["DCFLD".$i]));
					if(count($resExp1)>=2){
						for($j=0;$j<count($resExp1);$j++){
							$resExp2=explode("-",$resExp1[$j]);
							$data[]='\''.cmMer($resExp2[0]).cmMer($resExp2[1]).'\'';
						}
					}else{
						$data[] ='\''.cmMer($dataResult["DCFILID".$i]).cmMer($dataResult["DCFLD".$i]).'\'';
					}
				}
			}
		    $strSQL = ' SELECT CONCAT(DTFILID,DTFLD) AS DTCOL FROM DB2COLT WHERE DTNAME=? AND DTFLD IS NOT NULL ';
			$params=array($D1NAME);
		    $stmt = db2_prepare($db2con, $strSQL);
		    if ($stmt === false) {
				$rs='FAIL_SEL';
		    } else {
		        $r = db2_execute($stmt, $params);
		        if ($r === false) {
					$rs='FAIL_SEL';
		        } else {
		            while ($row = db2_fetch_assoc($stmt)) {
						if(cmMer($row["DTCOL"])!==""){
			                $data[]='\''.cmMer($row["DTCOL"]).'\'';
						}
		            }
		        }
		    }
        }
    }
	//e_log("cmGetSeiGyoColumnBySEQZERO:".print_r($data,true));
    return array("result"=>$rs,"data"=>$data);
}
/*
*抽出対象フィールド情報取得
*@seigyoFlg:入れてないSEQのため使う
*/
function getSelColumnInfo($db2con,$QRYNM,$seigyoFlg=false){
    $strSQL = '';
    $data   = array();
    $params=array();
    $strSQL .= '    SELECT      C.D2NAME '; 
    $strSQL .= '               ,C.D2FILID ';  
    $strSQL .= '               ,C.D2FLD ';  
    $strSQL .= '               ,C.D2HED ';  
    $strSQL .= '               ,C.D2CSEQ ';  
    $strSQL .= '               ,C.D2TYPE ';  
    $strSQL .= '               ,C.D2LEN ';  
    $strSQL .= '               ,C.D2DEC '; 
    $strSQL .= '               ,D.SFGMES '; 
    $strSQL .= '    FROM ';   
    $strSQL .= '          ( ';  
    $strSQL .= '              SELECT A.D2NAME '; 
    $strSQL .= '                   , A.D2FILID ';  
    $strSQL .= '                   , A.D2FLD ';  
    $strSQL .= '                   , A.D2HED ';  
    $strSQL .= '                   , A.D2CSEQ ';  
    $strSQL .= '                   , A.D2TYPE ';  
    $strSQL .= '                   , A.D2LEN ';  
    $strSQL .= '                   , A.D2DEC ';  
    $strSQL .= '               FROM FDB2CSV2 A ';  
    $strSQL .= '               WHERE (A.D2NAME = ? AND   A.D2CSEQ > 0 )  '; 
    $params[] = $QRYNM;
	//制御：入れてないSEQのため使う
	$seigyoCOL=array();
	if($seigyoFlg===true){
		$r=getSeiGyoColumnBySEQZERO($db2con,$QRYNM);
		if($r["result"]===true){
			$seigyoCOL=$r["data"];
		}
	}
    if(count($seigyoCOL)>0 && $seigyoFlg===true){
	    $strSQL.= ' OR ( A.D2NAME = ? AND A.D2CSEQ=0  ';
		if(count($seigyoCOL)>0){
			$strSQL.='AND CONCAT(A.D2FILID,A.D2FLD) IN( '.join(',',$seigyoCOL).') ';
		}
		$strSQL.= ' ) ';
		$params[]=$QRYNM;
    }
    $strSQL .= '              UNION ALL ';  
    $strSQL .= '              SELECT B.D5NAME AS D2NAME  '; 
    $strSQL .= '                   , 9999     AS D2FILID ';  
    $strSQL .= '                   , B.D5FLD  AS D2FLD ';  
    $strSQL .= '                   , B.D5HED  AS D2HED ';  
    $strSQL .= '                   , B.D5CSEQ AS D2CSEQ ';  
    $strSQL .= '                   , B.D5TYPE AS D2TYPE ';  
    $strSQL .= '                   , B.D5LEN  AS D2LEN ';  
    $strSQL .= '                   , B.D5DEC  AS D2DEC ';  
    $strSQL .= '               FROM FDB2CSV5 B ';  
    $strSQL .= '              WHERE ( B.D5NAME = ? AND   B.D5CSEQ > 0 ) '; 
    $params[] = $QRYNM;
    if(count($seigyoCOL)>0 && $seigyoFlg===true){
	    $strSQL.= ' OR ( B.D5NAME = ? AND B. D5CSEQ=0  ';
		if(count($seigyoCOL)>0 && $seigyoFlg===true){
			$strSQL.='AND CONCAT(\'9999\',B.D5FLD) IN( '.join(',',$seigyoCOL).') ';
		}
		$strSQL.= ' ) ';
		$params[]=$QRYNM;
    }
    $strSQL .= '          ) C '; 
    $strSQL .= '        LEFT JOIN BSUMFLD D '; 
    $strSQL .= '        ON   C.D2NAME   = D.SFQRYN ';
    $strSQL .= '        AND  C.D2FILID  = D.SFFILID '; 
    $strSQL .= '        AND  C.D2FLD    = D.SFFLDNM '; 
    $strSQL .= '    ORDER BY C.D2CSEQ '; 
       
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/*ソート対象フィールド情報取得
*@seigyoFlg:入れてないSEQのため使う
*/
function getOrdColumnInfo($db2con,$QRYNM,$seigyoFlg=false){
	$strSQL = '';
	$data   = array();
	$params=array();
	$strSQL .= '    SELECT C.* FROM ';
	$strSQL .= '        ( ';
	$strSQL .= '        SELECT A.D2NAME ';
	$strSQL .= '             , A.D2FILID ';
	$strSQL .= '             , A.D2FLD ';
	$strSQL .= '             , A.D2HED ';
	$strSQL .= '             , A.D2CSEQ ';
	$strSQL .= '             , A.D2RSEQ ';
	$strSQL .= '             , CASE A.D2RSTP ';
	$strSQL .= '               WHEN \'A\' THEN \'ASC\' ';
	$strSQL .= '               WHEN \'D\' THEN \'DESC\' ';
	$strSQL .= '               END AS D2RSTP ';
	$strSQL .= '         FROM FDB2CSV2 A ';
	$strSQL .= '         WHERE ( A.D2NAME = ? AND   A.D2RSEQ  > 0 ) ';
	$params[] = $QRYNM;
	//制御：入れてないSEQのため使う
	$seigyoCOL=array();
	if($seigyoFlg===true){
		$r=getSeiGyoColumnBySEQZERO($db2con,$QRYNM);
		if($r["result"]===true){
			$seigyoCOL=$r["data"];
		}
	}
	if(count($seigyoCOL)>0 && $seigyoFlg===true){
	    $strSQL.= ' OR ( A.D2NAME = ? AND A.D2CSEQ=0  ';
		if(count($seigyoCOL)>0){
			$strSQL.='AND CONCAT(A.D2FILID,A.D2FLD) IN( '.join(',',$seigyoCOL).') ';
		}
		$strSQL.= ' ) ';
		$params[]=$QRYNM;
	}
	$strSQL .= '        UNION ALL ';
	$strSQL .= '        SELECT B.D5NAME AS D2NAME ';
	$strSQL .= '             , 9999     AS D2FILID ';
	$strSQL .= '             , B.D5FLD  AS D2FLD ';
	$strSQL .= '             , B.D5HED  AS D2HED ';
	$strSQL .= '             , B.D5CSEQ AS D2CSEQ ';
	$strSQL .= '             , B.D5RSEQ AS D2RSEQ ';
	$strSQL .= '             , CASE B.D5RSTP ';
	$strSQL .= '               WHEN \'A\' THEN \'ASC\' ';
	$strSQL .= '               WHEN \'D\' THEN \'DESC\' ';
	$strSQL .= '               END AS D2RSTP ';
	$strSQL .= '         FROM FDB2CSV5 B ';
	$strSQL .= '        WHERE (B.D5NAME = ? AND B.D5RSEQ > 0 ) ';
	$params[] = $QRYNM;
	if(count($seigyoCOL)>0 && $seigyoFlg===true){
	    $strSQL.= ' OR ( B.D5NAME = ? AND B. D5CSEQ=0  ';
		if(count($seigyoCOL)>0 && $seigyoFlg===true){
			$strSQL.='AND CONCAT(\'9999\',B.D5FLD) IN( '.join(',',$seigyoCOL).') ';
		}
		$strSQL.= ' ) ';
		$params[]=$QRYNM;
	}
    $strSQL .= '     ) C ';
    $strSQL .= '    ORDER BY C.D2RSEQ ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function getBSQLCND($db2con,$QRYNM,$allFlg = false){
    //error_log('GetBSQLCND***'.print_r($QRYNM,true));
    $strSQL = '';
    $data   = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '     CNQRYN, ';
    $strSQL .= '     CNDSEQ, ';
    $strSQL .= '     CNDKBN, ';
    $strSQL .= '     CNDNM, ';
    $strSQL .= '     CNDWNM, ';
    $strSQL .= '     CNDDTYP, ';
    $strSQL .= '     CNDSTKB, ';
    $strSQL .= '     CNDDAT, ';
    $strSQL .= '     CNDBCNT, ';
    $strSQL .= '     CNCANL, ';
    $strSQL .= '     CNCANF, ';
    $strSQL .= '     CNCANC, ';
    $strSQL .= '     CNCANG, ';
    $strSQL .= '     CNNAMG, ';
    $strSQL .= '     CNNAMC, ';
    $strSQL .= '     CNDFMT, ';
    $strSQL .= '     CNDSFL, ';
    $strSQL .= '     CNDFIN, ';
    $strSQL .= '     CNDFPM, ';
    $strSQL .= '     CNDFDY, ';
    $strSQL .= '     CNDFFL, ';
    $strSQL .= '     CNDTIN, ';
    $strSQL .= '     CNDTPM, ';
    $strSQL .= '     CNDTDY, ';
    $strSQL .= '     CNDTFL, ';
    $strSQL .= '     CNCAST, ';
    $strSQL .= '     CNNAST, ';
    $strSQL .= '     CNDSCH,  ';
    $strSQL .= '     CNCKBN  ';
    $strSQL .= ' FROM ';
    $strSQL .= '     BSQLCND ';
    $strSQL .= ' WHERE CNQRYN = ? ';
    if($allFlg === false){
       // $strSQL .= " AND (CNDSTKB <> 2 AND (CNDDAT <>'' OR CNDBCNT > 0))";//CNDSTKB => 2であってブラクのデーターを取得しない
    }
    $strSQL .= ' ORDER BY CNDSEQ ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $niniFlg = false;
            while($row = db2_fetch_assoc($stmt)){
                $add = true;
			//if((int)$row['CNDSTKB'] === 2 && cmMer($row['CNDDAT']) === '' && (int)$row['CNDBCNT'] === 0)
			if((int)$row['CNDSTKB'] !== 2){
	              /*  if(cmMer($row['CNDDAT']) === '' && (int)$row['CNDBCNT'] === 0){
	                    if($allFlg === false){
	                        $add = false;
	                    }
	                    e_log("true2");
	                    $niniFlg = true;
	                }   */
			}
                if($add === true){
                    $data[] = $row;
                }
            }		
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data,'NINIFLG' => $niniFlg);
        }
    }
    if($niniFlg === true){
        e_log("true");
    }else{
        e_log("False");
    }
    return $data;
}
function getBSQLDAT($db2con,$QRYNM){
    $strSQL = '';
    $data   = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '    BSQLNM, ';
    $strSQL .= '    BSQLFLG, ';
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,1,5000)) > 0 THEN SUBSTR(BEXESQL,1,5000)  ELSE '' END AS BEXESQL1,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,5001,5000)) > 0 THEN  SUBSTR(BEXESQL,5001,5000)  ELSE '' END AS BEXESQL2,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,10001,5000)) > 0 THEN SUBSTR(BEXESQL,10001,5000)  ELSE '' END AS BEXESQL3,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,15001,5000)) > 0 THEN SUBSTR(BEXESQL,15001,5000) ELSE '' END AS BEXESQL4,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,20001,5000)) > 0 THEN SUBSTR(BEXESQL,20001,5000) ELSE '' END AS BEXESQL5,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,25001,5000)) > 0 THEN SUBSTR(BEXESQL,25001,5000) ELSE '' END AS BEXESQL6,";
    $strSQL .= "    CASE WHEN LENGTH(SUBSTR(BEXESQL,30001,2730)) > 0 THEN SUBSTR(BEXESQL,30001,2730) ELSE '' END AS BEXESQL7";
    $strSQL .= " FROM( ";
    $strSQL .= "    SELECT BSQLNM, BSQLFLG, BEXESQL FROM BSQLDAT WHERE BSQLNM = ? ";
    $strSQL .= ") AS A";

    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        error_log(db2_stmt_errormsg());
        $data = array('result' => 'FAIL_SEL');
    }else{
        error_log('sql bunn = '.$strSQL.print_r($params,true));

        $r = db2_execute($stmt,$params);
        if($r === false){
        error_log(db2_stmt_errormsg());

            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $tmp = array();
                $tmp['BSQLNM'] = $row['BSQLNM'];
                $tmp['BSQLFLG'] = $row['BSQLFLG'];
                $tmp['BEXESQL'] = $row['BEXESQL1'].$row['BEXESQL2'].$row['BEXESQL3'].$row['BEXESQL4'].$row['BEXESQL5'].$row['BEXESQL6'].$row['BEXESQL7'];
                $data[] = $tmp;
            }
            $data = umEx($data,false);
            $sqlData = array();$strbind = '';
            if(count($data)>0){
                $sqlData['BSQLNM'] = $QRYNM;
                foreach($data as $datVal){
                    if($datVal['BSQLFLG'] === '1'){
                        $sqlData['EXESQL'] = $datVal['BEXESQL'];
                    }else{
                        $sqlData['DATSQL'] = $datVal['BEXESQL'];
                    }
                }
                $rs = getBSQLCND($db2con,$QRYNM,true);
                /*if($rs['NINIFLG'] === true){
                    $cnddata = $rs['data'];
                    $srs = reformSQLStatment($sqlData['DATSQL'],$cnddata);
                    $sqlData['EXESQL'] = $srs['EXESQL'];
                    $sqlData['DATSQL'] = $srs['DATSQL'];
                }*/
            }
            error_log('sqldata = '.print_r($sqlData,true));
            $data = array('result' => true,'data' => $data,'SQLDATA'=>$sqlData);

        }
    }
    return $data;
}


//任意であるブラク項目解除
function reformSQLStatment($strSQL,$data){
e_log("任意であるブラク項目解除".$strSQL.print_r($data,true));
    $sqlData = array();
    $strArr = explode('?',$strSQL);
    $strArr = array_filter($strArr);
    $newStr = '';
    $newStr2 = '';
    $wflg = false;
    $index = 0;
    foreach($strArr as $key => $value){
        $CNDSTKB = $data[$key]['CNDSTKB'];
        $CNDDAT = $data[$key]['CNDDAT'];
        $pos = 0;
        $str = '';
        $strValue = '';
        $strValue2 = '';
        $strValue = strtoupper($value);
        if((int)$CNDSTKB === 2 && $CNDDAT === ''){ 
            if (strpos($strValue, ' WHERE ') !== false) {
                 if(strripos($value, 'WHERE') > $pos){
                    $wpos = strripos($value, 'WHERE');
                    $pos = $wpos;
                 }
            }
            if (strpos($strValue, ' AND ') !== false) {
                 if(strripos($value, 'AND') > $pos){
                    $apos = strripos($value, 'AND');
                    $pos = $apos;
                 }
            }
            if (strpos($strValue, ' OR ') !== false) {
                 if(strripos($strValue, 'OR') > $pos){
                    $opos = strripos($value, 'OR');
                    $pos = $opos;
                 }
            }
        }else{
            $strValue = rtrim($strValue);
            if(substr($strValue, -1) === '=' || substr($strValue, -4) === 'LIKE'){
                $strValue = $value.'?';
                $strValue2 = $value.'{'.++$index.'}';
            }else{
                $strValue = $value;
                $strValue2 = $value;
            }
        }

        if($pos !== 0){
            $newS = substr($strValue, 0,($pos-1));
            if($wflg === true && strlen($newS) > 0){
                $newS = ltrim($newS);
                if($pos === $apos){
                  $newS = substr($newS, 3);
               }else if($pos === $opos){
                  $newS = substr($newS, 2);
                }
                $wflg = false;
                if($newS !== '' && $newS !== NULL){
                    $newStr .= ' WHERE ';
                    $newStr2 .= ' WHERE ';
                }
            }
            $newStr .= $newS;
            $newStr2 .= $newS;
        }else{
            if($wflg === true && strlen($strValue) > 0){
                $strValue = ltrim($strValue);
                $strValue2 = ltrim($strValue2);
               if (strpos($strValue, 'AND') !== false){
                  $strValue = substr($strValue, 3);
                  $strValue2 = substr($strValue2, 3);
               }else if (strpos($strValue, 'OR') !== false){
                  $strValue = substr($strValue, 2);
                  $strValue2 = substr($strValue2, 3);
               }
                $wflg = false;
                $newStr .= ' WHERE ';
                $newStr2 .= ' WHERE ';
            }
            $newStr .= $strValue;
            $newStr2 .= $strValue2;
        }
        if($pos === $wpos){
            $wflg = true;
        }
    }
    $sqlData['EXESQL'] = $newStr2;
    $sqlData['DATSQL'] = $newStr;

    return $sqlData;
}