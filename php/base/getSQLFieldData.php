<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : 共通
* PROGRAM NAME   : フィールド情報取得【テーブル：SYSCOLUMN2】
* PROGRAM ID     : getSQLFieldData.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/04/11
* MODIFY DATE    : 
* ============================================================
**/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/********************************************************************
 * 変数
 ********************************************************************/
$rtn = 0;
$msg = '';
$data = array();
$outputfiledata = array();
$baseSQLData    = json_decode($_POST['BASESQLDATA'],true);
$baseFldData    = json_decode($_POST['BASEFLDDATA'],true);
$tmptblnm       = (isset($_POST['TMPTBLNM'])?$_POST['TMPTBLNM']:'');
$PROC           = (isset($_POST['PROC'])?$_POST['PROC']:'');
$D1NAME = (isset($_POST['D1NAME']))?$_POST['D1NAME']:'';
$originalSQL = '';
$currentSQL = '';
/********************************************************************
 * 処理
 ********************************************************************/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
$changeFlg = true;//元のデータを取得
if($rtn === 0){
    $rs = fnGetSQLQUERY($db2con,$D1NAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $originalSQL = $rs['originalSQL'];
        $currentSQL = cmMer($baseSQLData['D1SQL']);
        if($originalSQL === $currentSQL){
            $changeFlg = false; //クエリ作成したときの保存されたデータを出す
        }
    }
}
//e_log('SQLクエリーデータ：'.print_r($baseSQLData,true));
$RDBNM = $baseSQLData['RDBNM'];
if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
    $db2RDBCon = $db2con;
}else{
    $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
    $db2RDBCon = cmDB2ConRDB(SAVEDB);
}
// 新規の場合
if($rtn === 0){
    $rs = getSYSCOLUMN2($db2RDBCon,$tmptblnm,$changeFlg,$D1NAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
        $colNmArr = array();
        if($baseFldData !== ''){
            if(count($baseFldData) >0){
                foreach($baseFldData as $colVal){
                    $colNmArr[] = $colVal['COLUMN_NAME'];
                }
                foreach($data as $key => $dataVal){
                    $fKey = array_search($dataVal['COLUMN_NAME'], $colNmArr);
                    if(isset($fKey)){
                        $data[$key]['COLUMN_HEADING'] = $baseFldData[$fKey]['COLUMN_HEADING'];
                        $data[$key]['EDTCD'] = $baseFldData[$fKey]['EDTCD'];
                    }
                }
            }
        }
    }
}
//出力ファイルのデータを入れる
 if($rtn===0){
    $r=fnGetFDB2CSV1($db2con,$D1NAME);
    if($r['result']!==true){
        $rtn=1;
        $msg=$r['result'];
    }else{
        $outputfiledata=$r['data'];
    }
}

if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
    cmDb2Close($db2RDBCon);
}
cmDb2Close($db2con);
$rtn = array(
    'DATA' => $data,
    'RTN' => $rtn,
    'MSG' => $msg,
    'changeFlg' => $changeFlg,
    'originalSQL' => $originalSQL,
    'currentSQL' => $currentSQL,
    'BASESQLDATA' => $baseSQLData,
    'OUTPUTFDATA'=>$outputfiledata//出力ファイルデータ
);
echo(json_encode($rtn));
//出力ファイルのためデータをもらう
function fnGetFDB2CSV1($db2con,$D1NAME){
    $data = array();
    $params = array();
    // Adding => 'D1OUTA'
    $strSQL .= ' SELECT A.D1OUTJ,A.D1OUTLIB,A.D1OUTFIL,A.D1OUTR,A.D1NAME,A.D1OUTA ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}

//元のSQLを取得
function fnGetSQLQUERY($db2con,$D1NAME){
    $data = array();
    $originalSQL = '';
    $params = array($D1NAME);

    $strSQL = 'select * from bsqldat where bsqlnm = ? AND BSQLFLG = \'\'';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $originalSQL = cmMer($data[0]['BEXESQL']);
            }
            $data = array('result' => true,'originalSQL' => $originalSQL);
        }
    }
    return $data;
}

/**
  *---------------------------------------------------------------------------
  * 定義の結果カラム情報取得
  *
  *---------------------------------------------------------------------------
  **/
function getSYSCOLUMN2($db2con,$tblname,$changeFlg,$D1NAME){
    $data = array();
    $params = array();
    $strSQL = '';
    $strSQL .= ' SELECT SYS.SEQ,SYS.COLUMN_NAME,SYS.LENGTH,SYS.DDS_TYPE,SYS.NUMERIC_SCALE ';
//    $strSQL .= ' ,SYS.COLUMN_HEADING ';
    if($changeFlg === true){
        $strSQL .=  '     ,SYS.COLUMN_HEADING AS COLUMN_HEADING ';
    }else{
        $strSQL .=  '       ,(CASE ';
        $strSQL .=  '           WHEN FDB2CSV2DATA.COLUMN_HEADING IS NULL ';
        $strSQL .=  '           AND FDB2CSV2DATA.COLUMN_NAME  IS NULL ';
        $strSQL .=  '           THEN SYS.COLUMN_HEADING ';
        $strSQL .=  '           ELSE FDB2CSV2DATA.COLUMN_HEADING ';
        $strSQL .=  '         END) AS COLUMN_HEADING ';
    }
    $strSQL .= ' ,SYS.CCSID,SYS.ORDINAL_POSITION ';
    if($changeFlg === true){
        $strSQL .= ' ,SYS.EDTCD ';
    }else{
        $strSQL .= ' ,FDB2CSV2DATA.EDTCD ';
    }
    $strSQL .= ' FROM(';
    $strSQL .=' SELECT \'\' AS SEQ ';
    $strSQL .='       ,A.SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
    $strSQL .='       ,A.LENGTH ';
    $strSQL .='       ,CASE A.DDS_TYPE';
    $strSQL .='         WHEN \'H\' THEN \'A\'';
    $strSQL .='         WHEN \'J\' THEN \'0\'';
    $strSQL .='         WHEN \'E\' THEN \'0\'';
    $strSQL .='         ELSE A.DDS_TYPE ';
    $strSQL .='         END AS DDS_TYPE';
    $strSQL .='       ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG ==='1'){
        $strSQL .='       ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .='       ,A.COLUMN_HEADING ';
    }
    $strSQL .='       ,A.CCSID ';
    $strSQL .='       ,A.ORDINAL_POSITION ';
    $strSQL .='       ,\'\' AS EDTCD ';
    $strSQL .=' FROM '.SYSCOLUMN2 .' A ';
    $strSQL .=' WHERE A.TABLE_SCHEMA = ? ';
    $strSQL .=' AND A.TABLE_NAME = ? ';
    $strSQL .=' ORDER BY A.ORDINAL_POSITION ) AS SYS ';
    $strSQL .=  '   LEFT JOIN ';
    $strSQL .=  '       (SELECT  ';
    $strSQL .=  '            (CASE A.D2CSEQ ';
    $strSQL .=  '            WHEN 0 THEN 99999 '; 
    $strSQL .=  '            ELSE A.D2CSEQ ';
    $strSQL .=  '            END) '; 
    $strSQL .=  '            AS SEQ '; 
    $strSQL .=  '           ,A.D2FLD  AS COLUMN_NAME ';
    $strSQL .=  '           ,A.D2HED  AS COLUMN_HEADING '; 
    $strSQL .=  '           ,\'\'     AS FORMULA ';
    $strSQL .=  '           ,CASE A.D2RSEQ ';
    $strSQL .=  '             WHEN 0 THEN 99999 ';
    $strSQL .=  '             ELSE A.D2RSEQ '; 
    $strSQL .=  '             END AS SORTNO ';
    $strSQL .=  '           ,A.D2RSTP AS SORTTYP '; 
    $strSQL .=  '           ,A.D2WEDT AS EDTCD ';
    $strSQL .=  '       FROM FDB2CSV2 A ';
    $strSQL .=  '       WHERE A.D2NAME = ?';
    $strSQL .=  '       AND   A.D2FILID = 0 ';
    $strSQL .=  '       ORDER BY A.D2CSEQ,A.D2RSEQ ) FDB2CSV2DATA ';
    $strSQL .=  '   ON SYS.COLUMN_NAME = FDB2CSV2DATA.COLUMN_NAME ';
	$params = array(SAVE_DB,$tblname,$D1NAME);
    e_log("strSQL akz GETSQLFILEDDATA ".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
// 【編集】結果フィールドデータ取得クエリー
function getFDB2CSV5($db2con,$D1NAME){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT   ';
    $strSQL .=  '   (CASE   A.D5CSEQ'; 
    $strSQL .=  '       WHEN 0 THEN 99999 ';
    $strSQL .=  '       ELSE A.D5CSEQ END) AS SEQ '; 
    $strSQL .=  '           ,A.D5FLD AS COLUMN_NAME '; 
    $strSQL .=  '           ,A.D5LEN  AS LENGTH '; 
    $strSQL .=  '           ,A.D5TYPE AS DDS_TYPE ';
    $strSQL .=  '           ,(CASE ';
    $strSQL .=  '               WHEN A.D5TYPE = \'P\' '; 
    $strSQL .=  '                   OR A.D5TYPE = \'B\' '; 
    $strSQL .=  '                   OR A.D5TYPE = \'S\' ';
    $strSQL .=  '               THEN A.D5DEC ';
    $strSQL .=  '               ELSE \'\' ';
    $strSQL .=  '           END) AS NUMERIC_SCALE '; 
    $strSQL .=  '           ,A.D5HED  AS COLUMN_HEADING '; 
    $strSQL .=  '           ,A.D5EXP  AS FORMULA ';
    $strSQL .=  '           ,A.D5RSEQ AS SORTNO  ';
    $strSQL .=  '           ,A.D5RSTP AS SORTTYP '; 
    $strSQL .=  '           ,A.D5WEDT AS EDTCD '; 
    $strSQL .=  '           ,A.D5ASEQ AS ORDINAL_POSITION '; 
    $strSQL .=  '   FROM ';    
    $strSQL .=  '           FDB2CSV5 A ';
    $strSQL .=  '   WHERE ';   
    $strSQL .=  '           A.D5NAME = ? ';
    $strSQL .=  '   ORDER BY '; 
    $strSQL .=  '           SEQ '; 
    $strSQL .=  '          ,ORDINAL_POSITION ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($D1NAME);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                if(($row['DDS_TYPE'] === 'P' || $row['DDS_TYPE'] === 'B' || $row['DDS_TYPE'] === 'S') === false){
                    $row['NUMERIC_SCALE'] = '';
                }
                if($row['SORTNO'] == 0){
                    $row['SORTNO'] = '';
                }
                if($row['SEQ'] == 99999){
                    $row['SEQ'] = '';
                }
                $data[] = $row;
            }
            $data = umEx($data,false);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function getFDB2CSV2($db2con,$D1NAME,$FILID,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST,$chkFileFlg){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT ';
    $strSQL .=  '         FDB2CSV2DATA.SEQ            AS SEQ ';
    $strSQL .=  '       , SYSTABDATA.COLUMN_NAME      AS COLUMN_NAME ';
    $strSQL .=  '       , SYSTABDATA.LENGTH           AS LENGTH ';
    $strSQL .=  '       , SYSTABDATA.DDS_TYPE         AS DDS_TYPE ';
    $strSQL .=  '       , SYSTABDATA.NUMERIC_SCALE    AS NUMERIC_SCALE ';
    if($chkFileFlg === true){
        $strSQL .=  '     ,SYSTABDATA.COLUMN_HEADING AS COLUMN_HEADING ';
    }else{
        $strSQL .=  '       ,(CASE ';
        $strSQL .=  '           WHEN FDB2CSV2DATA.COLUMN_HEADING IS NULL ';
        $strSQL .=  '           AND FDB2CSV2DATA.COLUMN_NAME  IS NULL ';
        $strSQL .=  '           THEN SYSTABDATA.COLUMN_HEADING ';
        $strSQL .=  '           ELSE FDB2CSV2DATA.COLUMN_HEADING ';
        $strSQL .=  '         END) AS COLUMN_HEADING ';
    }
    $strSQL .=  '       , FDB2CSV2DATA.FORMULA        AS FORMULA ';
    $strSQL .=  '       , FDB2CSV2DATA.SORTNO         AS SORTNO ';
    $strSQL .=  '       , FDB2CSV2DATA.SORTTYP        AS SORTTYP ';
    $strSQL .=  '       , FDB2CSV2DATA.EDTCD          AS EDTCD ';
    $strSQL .=  '       , SYSTABDATA.ORDINAL_POSITION AS ORDINAL_POSITION ';
    $strSQL .=  '   FROM  '; 
    $strSQL .=  '       (SELECT  ';
    $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
    $strSQL .=  '          ,A.LENGTH '; 
    $strSQL .=  '          ,CASE A.DDS_TYPE ';
    $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
    $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
    $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
    $strSQL .=  '            ELSE A.DDS_TYPE '; 
    $strSQL .=  '            END AS DDS_TYPE ';
    $strSQL .=  '          ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG ==='1'){
        $strSQL .=  '          ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .=  '          ,A.COLUMN_HEADING ';
    }
    $strSQL .=  '          ,A.ORDINAL_POSITION '; 
    //$strSQL .=  '       FROM QSYS2/SYSCOLUMN2 A ';
    $strSQL .=  '       FROM '. SYSCOLUMN2 .' A ' ;
    $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
    $strSQL .=  '       AND A.TABLE_NAME = ? '; 
    //$strSQL .=  '       AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\') ';
//    $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
    $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ) SYSTABDATA ';
    $strSQL .=  '   LEFT JOIN ';
    $strSQL .=  '       (SELECT  ';
    $strSQL .=  '            (CASE A.D2CSEQ ';
    $strSQL .=  '            WHEN 0 THEN 99999 '; 
    $strSQL .=  '            ELSE A.D2CSEQ ';
    $strSQL .=  '            END) '; 
    $strSQL .=  '            AS SEQ '; 
    $strSQL .=  '           ,A.D2FLD  AS COLUMN_NAME ';
    $strSQL .=  '           ,A.D2HED  AS COLUMN_HEADING '; 
    $strSQL .=  '           ,\'\'     AS FORMULA ';
    $strSQL .=  '           ,CASE A.D2RSEQ ';
    $strSQL .=  '             WHEN 0 THEN 99999 ';
    $strSQL .=  '             ELSE A.D2RSEQ '; 
    $strSQL .=  '             END AS SORTNO ';
    $strSQL .=  '           ,A.D2RSTP AS SORTTYP '; 
    $strSQL .=  '           ,A.D2WEDT AS EDTCD ';
    $strSQL .=  '       FROM FDB2CSV2 A ';
    $strSQL .=  '       WHERE A.D2NAME = ?';
    $strSQL .=  '       AND   A.D2FILID = ? ';
    $strSQL .=  '       ORDER BY A.D2CSEQ,A.D2RSEQ ) FDB2CSV2DATA ';
    $strSQL .=  '   ON SYSTABDATA.COLUMN_NAME = FDB2CSV2DATA.COLUMN_NAME ';
    $strSQL .=  '   ORDER BY SEQ ,SORTNO, ORDINAL_POSITION ';
    
    //e_log('実行SQL '.$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$TABLE_NAME,$D1NAME,$FILID);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        if($row['SORTNO'] == 99999){
                            $row['SORTNO'] = '';
                        }
                        if($row['SEQ'] == 99999){
                            $row['SEQ'] = '';
                        }
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
            //$data = umEX($data,true);
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME,$D1NAME,$FILID);
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row['SORTNO'] == 99999){
                        $row['SORTNO'] = '';
                    }
                    if($row['SEQ'] == 99999){
                        $row['SEQ'] = '';
                    }
                    $data[] = $row;
                }
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data);
            }
        }
        
    }
    return $data;
}
function getFDB2CSV2RDB ($db2con,$D1NAME,$FILID,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST,$chkFileFlg){
    $data = array();
    $syscolumn2 = array();
    $params = array();
    $LIBLNM = '';
    $rtn = 0;
    $db2RDB = cmDB2ConRDB();

    $strSQL  =  ' SELECT  ';
    $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
    $strSQL .=  '          ,A.LENGTH '; 
    $strSQL .=  '          ,CASE A.DDS_TYPE ';
    $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
    $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
    $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
    $strSQL .=  '            ELSE A.DDS_TYPE '; 
    $strSQL .=  '            END AS DDS_TYPE ';
    $strSQL .=  '          ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG==='1'){
        $strSQL .=  '          ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .=  '          ,A.COLUMN_HEADING ';
    }
    $strSQL .=  '          ,A.ORDINAL_POSITION '; 
    $strSQL .=  '          ,A.TABLE_SCHEMA '; 
    $strSQL .=  '          ,A.TABLE_NAME '; 
    //$strSQL .=  '       FROM QSYS2/SYSCOLUMN2 A '; 
    $strSQL .=  '       FROM '. SYSCOLUMN2 .' A ';
    $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
    $strSQL .=  '       AND A.TABLE_NAME = ? '; 
    //$strSQL .=  '       AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\') ';
//    $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
    $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ';

    $stmt = db2_prepare($db2RDB,$strSQL);
    if($stmt === false){
        $syscolumn2 = array('result' => 'FAIL_SEL');
        $rtn = 1;
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$TABLE_NAME);
                //e_log('フィールド取得：'.$strSQL.print_r($params,true));
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $syscolumn2 = array('result' => 'FAIL_SEL');
                    $rtn = 1;
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $syscolumn2[] = $row;
                    }
                    if(count($syscolumn2) > 0){
                        $LIBLNM = $value;
                        break;
                    }
                }
            }
            //$data = umEX($data,true);
            $syscolumn2 = umEx($syscolumn2);
            $syscolumn2 = array('result' => true,'data' => $syscolumn2);
            //e_log('取得したデータ：'.print_r($syscolumn2,true));
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME);
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $syscolumn2 = array('result' => 'FAIL_SEL');
                $rtn = 1;
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $syscolumn2[] = $row;
                }
                $LIBLNM = $TABLE_SCHEMA;
                $syscolumn2 = umEx($syscolumn2,false);
                $syscolumn2 = array('result' => true,'data' => $syscolumn2);
                //e_log('取得したデータ：'.print_r($syscolumn2,true));
            }
        }
    }

    if($rtn === 0){
        e_log('一時テーブル作成開始');
        
        // 一時テーブル作成
        $createSQL  = 'DECLARE GLOBAL TEMPORARY TABLE SYSCOLUMN2 ';
        $createSQL .= ' ( ';
        $createSQL .= ' SYSTEM_COLUMN_NAME CHAR(10) , ';
        $createSQL .= ' LENGTH INTEGER , ';
        $createSQL .= ' DDS_TYPE CHAR(1), ';
        $createSQL .= ' NUMERIC_SCALE INTEGER DEFAULT NULL , ';
        $createSQL .= ' COLUMN_HEADING VARGRAPHIC(60) CCSID 1200 , ';
        $createSQL .= ' ORDINAL_POSITION INTEGER, ';
        $createSQL .= ' TABLE_SCHEMA VARCHAR(128), ';
        $createSQL .= ' TABLE_NAME VARCHAR(128)'; 
        $createSQL .= ' )  WITH REPLACE ';
        //e_log($createSQL);
        $result = db2_exec($db2con, $createSQL);
        if ($result) {
            e_log( 'テーブル作成成功');
            if(count($syscolumn2['data'])>0){
                $syscolumn2 = umEx($syscolumn2['data']);
                
                $strInsSQL  = ' INSERT INTO QTEMP/SYSCOLUMN2 ';
                $strInsSQL .= ' ( ';
                $strInsSQL .= '     SYSTEM_COLUMN_NAME, ';
                $strInsSQL .= '     LENGTH, ';
                $strInsSQL .= '     DDS_TYPE, ';
                $strInsSQL .= '     NUMERIC_SCALE, ';
                $strInsSQL .= '     COLUMN_HEADING, ';
                $strInsSQL .= '     ORDINAL_POSITION, ';
                $strInsSQL .= '     TABLE_SCHEMA, ';
                $strInsSQL .= '     TABLE_NAME ';
                $strInsSQL .= ' ) ';
                $strInsSQL .= ' VALUES ( ?,?,?,?,?,?,?,?)';

                $stmt = db2_prepare($db2con,$strInsSQL);
                if($stmt === false ){
                    $data = array(
                                'result' => 'FAIL_INS',
                                'errcd'  => 'SYSCOLUMN2:'.db2_stmt_errormsg()
                            );
                    $result = 1;
                }else{
                    foreach($syscolumn2 as $syscolumn2data){
                        $params =array(
                                    $syscolumn2data['COLUMN_NAME'],
                                    $syscolumn2data['LENGTH'],
                                    $syscolumn2data['DDS_TYPE'],
                                    ($syscolumn2data['NUMERIC_SCALE'] == '') ? NULL : $syscolumn2data['NUMERIC_SCALE'],
                                    $syscolumn2data['COLUMN_HEADING'],
                                    $syscolumn2data['ORDINAL_POSITION'],
                                    $syscolumn2data['TABLE_SCHEMA'],
                                    $syscolumn2data['TABLE_NAME']
                                );
                        //e_log($strInsSQL.print_r($params,true));
                        $res = db2_execute($stmt,$params);
                        if($res === false){
                            $data = array(
                                    'result' => 'FAIL_INS',
                                    'errcd'  => 'SYSCOLUMN2:'.db2_stmt_errormsg()
                                );
                            $result = 1;
                            break;
                        }else{
                            $result = 0;
                            $data = array('result' => true);
                        }
                    }
                }
            }
            if($result !== 0){
                $rtn = 1;
            }
        }else{
            e_log('テーブル作成失敗');
            e_log('テーブルerror'.db2_stmt_errormsg());
            $rtn = 1;
            $data = array(
                'result' => 'FAIL_SEL',
                'errcd'  => 'テーブル作成失敗'
                );
        }
    }

    /*if($rtn === 0){
            $strSQL = '';

        $strSQL  =' SELECT \'\' AS SEQ ';
        $strSQL .='       ,A.SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
        $strSQL .='       ,A.LENGTH ';
        $strSQL .='       ,CASE A.DDS_TYPE';
        $strSQL .='         WHEN \'H\' THEN \'A\'';
        $strSQL .='         WHEN \'J\' THEN \'0\'';
        $strSQL .='         WHEN \'E\' THEN \'0\'';
        $strSQL .='         ELSE A.DDS_TYPE ';
        $strSQL .='         END AS DDS_TYPE';
        $strSQL .='       ,A.NUMERIC_SCALE ';
        $strSQL .='       ,A.COLUMN_HEADING ';
        $strSQL .='       ,\'\' AS FORMULA ';
        $strSQL .='       ,\'\' AS SORTNO ';
        $strSQL .='       ,\'\' AS SORTTYP ';
        $strSQL .='       ,\'\' AS EDTCD ';
        $strSQL .='       ,A.ORDINAL_POSITION ';
        $strSQL .=' FROM QTEMP/SYSCOLUMN2 A ';
        $strSQL .=' WHERE A.TABLE_SCHEMA = ? ';
        $strSQL .=' AND A.TABLE_NAME = ? ';
        //$strSQL .=' AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\')';
        $strSQL .=' AND A.DDS_TYPE NOT IN(\'5\')';
        $strSQL .=' ORDER BY A.ORDINAL_POSITION ';
        
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
            //e_log('error'.db2_stmt_errormsg());
            $rtn = 1;
        }else{
            $params = array($LIBLNM,$TABLE_NAME);
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
                $rtn = 1;
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data);
            }
            //e_log('temptable'.print_r($data,true));
        }
    }*/
     cmDb2Close($db2RDB);
    if($rtn === 0){
        $strSQL = '';
        $strSQL .=  '   SELECT ';
        $strSQL .=  '         FDB2CSV2DATA.SEQ            AS SEQ ';
        $strSQL .=  '       , SYSTABDATA.COLUMN_NAME      AS COLUMN_NAME ';
        $strSQL .=  '       , SYSTABDATA.LENGTH           AS LENGTH ';
        $strSQL .=  '       , SYSTABDATA.DDS_TYPE         AS DDS_TYPE ';
        $strSQL .=  '       , SYSTABDATA.NUMERIC_SCALE    AS NUMERIC_SCALE ';
        if($chkFileFlg === true){
            $strSQL .=  '   , SYSTABDATA.COLUMN_HEADING AS COLUMN_HEADING ';
        }else{
            $strSQL .=  '       ,(CASE ';
            $strSQL .=  '           WHEN FDB2CSV2DATA.COLUMN_HEADING IS NULL ';
            $strSQL .=  '           AND FDB2CSV2DATA.COLUMN_NAME  IS NULL ';
            $strSQL .=  '           THEN SYSTABDATA.COLUMN_HEADING ';
            $strSQL .=  '           ELSE FDB2CSV2DATA.COLUMN_HEADING ';
            $strSQL .=  '         END) AS COLUMN_HEADING ';
        }
        $strSQL .=  '       , FDB2CSV2DATA.FORMULA        AS FORMULA ';
        $strSQL .=  '       , FDB2CSV2DATA.SORTNO         AS SORTNO ';
        $strSQL .=  '       , FDB2CSV2DATA.SORTTYP        AS SORTTYP ';
        $strSQL .=  '       , FDB2CSV2DATA.EDTCD          AS EDTCD ';
        $strSQL .=  '       , SYSTABDATA.ORDINAL_POSITION AS ORDINAL_POSITION ';
        $strSQL .=  '   FROM  '; 
        $strSQL .=  '       (SELECT  ';
        $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
        $strSQL .=  '          ,A.LENGTH '; 
        $strSQL .=  '          ,CASE A.DDS_TYPE ';
        $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
        $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
        $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
        $strSQL .=  '            ELSE A.DDS_TYPE '; 
        $strSQL .=  '            END AS DDS_TYPE ';
        $strSQL .=  '          ,A.NUMERIC_SCALE '; 
        $strSQL .=  '          ,A.COLUMN_HEADING '; 
        $strSQL .=  '          ,A.ORDINAL_POSITION '; 
        $strSQL .=  '       FROM '. SYSCOLUMN2 .' A ';
        //$strSQL .=  '       FROM QTEMP/SYSCOLUMN2 A '; 
        $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
        $strSQL .=  '       AND A.TABLE_NAME = ? '; 
        //$strSQL .=  '       AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\') ';
//        $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
        $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ) SYSTABDATA ';
        $strSQL .=  '   LEFT JOIN ';
        $strSQL .=  '       (SELECT  ';
        $strSQL .=  '            (CASE A.D2CSEQ ';
        $strSQL .=  '            WHEN 0 THEN 99999 '; 
        $strSQL .=  '            ELSE A.D2CSEQ ';
        $strSQL .=  '            END) '; 
        $strSQL .=  '            AS SEQ '; 
        $strSQL .=  '           ,A.D2FLD  AS COLUMN_NAME ';
        $strSQL .=  '           ,A.D2HED  AS COLUMN_HEADING '; 
        $strSQL .=  '           ,\'\'     AS FORMULA ';
        $strSQL .=  '           ,CASE A.D2RSEQ ';
        $strSQL .=  '             WHEN 0 THEN 99999 ';
        $strSQL .=  '             ELSE A.D2RSEQ '; 
        $strSQL .=  '             END AS SORTNO ';
        $strSQL .=  '           ,A.D2RSTP AS SORTTYP '; 
        $strSQL .=  '           ,A.D2WEDT AS EDTCD ';
        $strSQL .=  '       FROM FDB2CSV2 A ';
        $strSQL .=  '       WHERE A.D2NAME = ?';
        $strSQL .=  '       AND   A.D2FILID = ? ';
        $strSQL .=  '       ORDER BY A.D2CSEQ,A.D2RSEQ ) FDB2CSV2DATA ';
        $strSQL .=  '   ON SYSTABDATA.COLUMN_NAME = FDB2CSV2DATA.COLUMN_NAME ';
        $strSQL .=  '   ORDER BY SEQ ,SORTNO, ORDINAL_POSITION ';
        
        //e_log('SQLtemp'.$strSQL);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
            $rtn  = 1;
            //e_log('error'.db2_stmt_errormsg());
        }else{
            if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
               /* foreach($LIBLIST as $value){
                   $params = array($value,$TABLE_NAME,$D1NAME,$FILID);
                    e_log(print_r($params,true));
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $data = array('result' => 'FAIL_SEL');
                        $rtn  = 1;
                    }else{
                        while($row = db2_fetch_assoc($stmt)){
                            if($row['SORTNO'] == 99999){
                                $row['SORTNO'] = '';
                            }
                            if($row['SEQ'] == 99999){
                                $row['SEQ'] = '';
                            }
                            $data[] = $row;
                        }
                        if(count($data) > 0){
                            break;
                        }
                    }
                }
                //$data = umEX($data,true);
                $data = umEx($data);
                $data = array('result' => true,'data' => $data);*/
                $params = array($LIBLNM,$TABLE_NAME,$D1NAME,$FILID);
            }else{
                $params = array($TABLE_SCHEMA,$TABLE_NAME,$D1NAME,$FILID);
            }
            //e_log(print_r($params,true));
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
                $rtn  = 1;
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row['SORTNO'] == 99999){
                        $row['SORTNO'] = '';
                    }
                    if($row['SEQ'] == 99999){
                        $row['SEQ'] = '';
                    }
                    $data[] = $row;
                }
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data);
            }
        }
            
       // }
    }
    //e_log(print_r($data,true));
    return $data;
}
/*function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}*/

function chkChangeFile($oriLibl,$oriFile,$compLibl,$compFile,$orilibList,$complibList){
    if(cmMer($compLibl) === '' || cmMer($compLibl) === '*USRLIBL'){
        $compLibl = '*LIBL';
    }else{
        $compLibl = cmMer($compLibl);
    }
    $compFile = cmMer($compFile) ;
    if(cmMer($oriLibl) === '' || cmMer($oriLibl) === '*USRLIBL'){
        $oriLibl = '*LIBL';
    }else{
        $oriLibl = cmMer($oriLibl);
    }
    $oriFile  = cmMer($oriFile);
    $chgFileFlg = false;
    if($compLibl !== $oriLibl){
        $chgFileFlg = true;
    }else{
        if($oriLibl === '*LIBL'){
            if($orilibList !== $complibList){
                $chgFileFlg = true;
            }else{
                if($compFile !== $oriFile){
                    //e_log($compFile.'dfd'.$oriFile);
                    $chgFileFlg = true;
                }
            }
        }else{
            if($compFile !== $oriFile){
                $chgFileFlg = true;
            }
        }
    }
    return $chgFileFlg;
}
function fnGetBREFTBL($db2con,$D1NAME){
    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM BREFTBL as A ';
    $strSQL .= ' WHERE RTQRYN <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND RTQRYN = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}
