<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 条件情報取得
 * PROGRAM ID     : getBQRYCNDDAT.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/04/11
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");

/*
 * 変数
 */
$rtn   = 0;
$msg   = '';
$QRYNM  = $_POST['QRYNM'];
$data  = array();
$visflg = 0;

/*
 * 処理
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}

if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    
        $auth = $rs['data'][0];
        $WUAUTH = cmMer($auth['WUAUTH']);
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'14',$userData[0]['WUSAUT']);//'14' =>制御レベル設定
            if($rs['result'] !== true){
                $visflg = 1;
                //$msg =  showMsg($rs['result'],array('制御レベル設定の権限'));
          }
    }
    
}



cmDb2Close($db2con);
$rtn = array(
    'DATA' => $data,
    'RTN' => $rtn,
    'MSG' => $msg,
    'VISFLG' => $visflg,
    'licensePivot' =>$licensePivot,
    'licenseSeigyo'=>$licenseSeigyo,
    'licenseFileOut'=>$licenseFileOut
);
echo(json_encode($rtn));

