<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$SEARCHDATA     = (isset($_POST['SEARCHDATA']))?cmMer($_POST['SEARCHDATA']):'';
$LIBNAME        = (isset($_POST['LIBNAME']))?cmMer($_POST['LIBNAME']):'';
$start          = (isset($_POST['start']))?cmMer($_POST['start']):0;
$length         = (isset($_POST['length']))?cmMer($_POST['length']):100;
$sort           = (isset($_POST['sort']))?cmMer($_POST['sort']):'';
$sortDir        = (isset($_POST['sortDir']))?cmMer($_POST['sortDir']):'';
//$libjyogaiFlg   = (isset($_POST['libjyogaiFlg']))?cmMer($_POST['libjyogaiFlg']):'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$LIBSNM = '';
$LIBSDB = '';


/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
/**20170525 akz**/
if($rtn === 0){
    $LIBSNM = cmMer($userData[0]['WULIBL']);
}
$filterLibList = array();
if($rtn === 0){
    $RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
    if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
        $db2conRDB = $db2con;
    }else{
        $db2conRDB = cmDB2ConRDB();
    }
    if($RDBNM !== ''){
        $LIBSDB = $RDBNM;
    }else{
        $LIBSDB = RDB;
    }
}
if($rtn === 0){
    if($LIBSNM !== '' ){
        if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
            $resCreate = createQTEMPDB2LBLS($db2conRDB,$LIBSNM);
            if($resCreate !== true){
                $rtn = 1;
                $msg = showMsg($resCreate);
            }
        }
    }
}

if($rtn === 0){
    $rs = fnGetAllCount($db2conRDB,$SEARCHDATA,$LIBNAME,$LIBSNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $allcount = $rs['data'];
    }
}



if($rtn === 0){
    $rs = fnGetLibData($db2conRDB,$SEARCHDATA,$LIBNAME,$LIBSNM,$start,$length,$sort,$sortDir);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}
if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
    cmDb2Close($db2conRDB);
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data),
    'LIBData' => SYSLIBCHK,
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetLibData($db2con,$SearchData,$LibName,$libsnm = '',$start = '',$length = '',$sort = '',$sortDir = ''){
    $data = array();

    $params = array();

    $qtempArr = array(
                    SCHEMA_NAME => 'QTEMP',
                    SCHEMA_TEXT => '' ,
                    ROWNUM      => 1
            );
    $addQtempFlg = false;
    $qtempFlg    = false;
    if(is_array($LibName)){
        if (($key = array_search('QTEMP', $LibName)) !== false) {
            unset($LibName[$key]);
        }
    }else{
        if($LibName === 'QTEMP'){
            $qtempFlg = true;
        }
    }
    if($qtempFlg){
        $data[] = $qtempArr;
        $data = array('result' => true,'data' => $data);
    }else{
        if($LibName !== ''){
            $addQtempFlg = false;
        }else{
            if($SearchData === ''){
                $addQtempFlg = true;
            }else{
                if(strpos('QTEMP',$SearchData) !== false){
                    $addQtempFlg = true;
                }
            }
        }
        if(CRTQTEMPTBL_FLG === 1){
            createTmpFil($db2con,SYSSCHEMASLIB,'SYSSCHEMAS','','SYSSCHEMAS');   
        }
        $strSQL = ' SELECT A.* ';
        $strSQL .= ' FROM ( SELECT B.SCHEMA_NAME';
        $strSQL .= ' , B.SCHEMA_TEXT';
        $strSQL .= ' , ROWNUMBER() OVER( ';

        if($sort != ''){
            $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= ' ORDER BY B.SCHEMA_NAME ASC ';
        }

        $strSQL .= ' ) as ROWNUM ';
        $strSQL .= ' FROM  ';
        $strSQL .= '    (SELECT ';
        $strSQL .= '        SYSTEM_SCHEMA_NAME SCHEMA_NAME ';
        $strSQL .= '        , SCHEMA_TEXT ';
        $strSQL .= '     FROM ';
        if(CRTQTEMPTBL_FLG === 1){
            $strSQL .= '        QTEMP/SYSSCHEMAS ';
        }else{
            $strSQL .= '        '.SYSSCHEMASLIB.'/SYSSCHEMAS ';
        }
        $strSQL .= '     WHERE ';
        $strSQL .= '        SYSTEM_SCHEMA_NAME <> \'QTEMP\' ) B ';
        $strSQL .= ' WHERE B.SCHEMA_NAME <> \'\'';
        
	        /***ライブラリー除外**/
	   if($libsnm !== '' ){
	      if(SYSLIBCHK === '1'){
	        $strSQL .= ' AND B.SCHEMA_NAME IN (';
	        $strSQL .= '    SELECT ';
	        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
	        $strSQL .= '    FROM ';
	        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
	        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
	            $strSQL .= MAINLIB.'/DB2LBLS ';
	            $strSQL .= '        WHERE LIBSNM = ? ';
	            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
	            $strSQL .= '        AND LIBSFG = \'1\' ' ;
	            array_push($params,$libsnm);
	           
	        }else{
	            $strSQL .= ' QTEMP/DB2LBLS ';
	           
	        }
	        $strSQL .= ' )';
		  }else{
			$strSQL .= ' AND B.SCHEMA_NAME NOT IN (';
	        $strSQL .= '    SELECT ';
	        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
	        $strSQL .= '    FROM ';
	        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
	        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
	            $strSQL .= MAINLIB.'/DB2LBLS ';
	            $strSQL .= '        WHERE LIBSNM = ? ';
	            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
	            $strSQL .= '        AND LIBSFG = \'1\' ' ;
	            array_push($params,$libsnm);
	            
	        }else{
	            $strSQL .= ' QTEMP/DB2LBLS ';
	            
	        }
	        $strSQL .= ' )';
		  }
	    }

        if($SearchData != ''){
            $strSQL .= ' AND ';
            $strSQL .= '  (B.SCHEMA_NAME like ? ';
            $strSQL .= ' OR B.SCHEMA_TEXT like ?) ';
            array_push($params,'%'.$SearchData.'%');
            array_push($params,'%'.$SearchData.'%');
        }
        $strSQL .= ' ) as A ';
        $strSQL .= ' WHERE A.ROWNUM <> 0';
        if($LibName != ''){
            $strSQL .= ' AND A.SCHEMA_NAME IN ( ? )';
            array_push($params,$LibName);
        }

        //抽出範囲指定
        if(($start != '')&&($length != '')){
            $strSQL .= ' AND A.ROWNUM BETWEEN ? AND ? ';
            if($addQtempFlg){
                if($start == 0){
                    array_push($params,$start + 1);
                    array_push($params,$start + ($length-1));
                }else{
                    array_push($params,$start );
                    array_push($params,$start + $length);
                }
            }else{
                array_push($params,$start + 1);
                array_push($params,$start + $length);
            }
        }
        e_log('ライブラリー取得SQL'.$strSQL.print_r($params,true));
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
             e_log('ライブラリーカウント取得SQL'.$strSQL.print_r($params,true).db2_stmt_errormsg());
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                if(($addQtempFlg === true) && ($start == 0)){
                    $data[] = $qtempArr;
                }
                while($row = db2_fetch_assoc($stmt)){
                    if($addQtempFlg){
                        $row['ROWNUM'] = $row['ROWNUM']+1;
                    }
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => umEx($data,true));
            }
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$SearchData,$LibName,$libsnm = ''){
    e_log('除外ライブラリー名：'.$libsnm);
    $data = array();
    $params = array();
    $qtempArr = array(
                    SCHEMA_NAME => 'QTEMP',
                    SCHEMA_TEXT => '' ,
                    ROWNUM      => 1
            );
    $addQtempFlg = false;
    $qtempFlg    = false;
    if(is_array($LibName)){
        if (($key = array_search('QTEMP', $LibName)) !== false) {
            unset($LibName[$key]);
        }
    }else{
        if($LibName === 'QTEMP'){
            $qtempFlg = true;
        }
    }
    if($qtempFlg){
        $data = array('result' => true,'data' => 1);
    }else{
        if($SearchData === ''){
            $addQtempFlg = true;
        }else{
            if(strpos('QTEMP',$SearchData) !== false){
                $addQtempFlg = true;
                $qtempFlg = true;
            }
        }
        $strSQL  = ' SELECT count(B.SCHEMA_NAME) as COUNT from ( ';
        $strSQL .= ' SELECT ';
        $strSQL .= ' A.SCHEMA_NAME ';
        $strSQL .= ' , A.SCHEMA_TEXT ';
        $strSQL .= ' FROM '  ;
        $strSQL .= '    (SELECT ';
        $strSQL .= '        SYSTEM_SCHEMA_NAME SCHEMA_NAME ';
        $strSQL .= '        , SCHEMA_TEXT ';
        $strSQL .= '     FROM ';
        $strSQL .= '        '.SYSSCHEMASLIB.'/SYSSCHEMAS ';
        $strSQL .= '     WHERE ';
        $strSQL .= '        SYSTEM_SCHEMA_NAME <> \'QTEMP\' ) A ';
        $strSQL .= ' WHERE A.SCHEMA_NAME <> \'\'';
        if($SearchData != '' ){
            $strSQL .= ' AND ';
            $strSQL .= '(A.SCHEMA_NAME LIKE ? ';
            $strSQL .= 'OR A.SCHEMA_TEXT LIKE ?)';
            
            array_push($params,'%'.$SearchData.'%');
            array_push($params,'%'.$SearchData.'%');
        }
        if($LibName != ''){
            $strSQL .= ' AND A.SCHEMA_NAME IN ( ? )';
            array_push($params,$LibName);
        }
        if($libsnm !== ''){
           e_log('CHECK LIBDATA'.print_r(SYSLIBCHK,true));
          if(SYSLIBCHK === '1'){
             $strSQL .= ' AND A.SCHEMA_NAME IN (';
          }else{
             $strSQL .= ' AND A.SCHEMA_NAME NOT IN (';
          }
            $strSQL .= '    SELECT ';
            $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
            $strSQL .= '    FROM ';
            $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
            if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
                $strSQL .= MAINLIB.'/DB2LBLS ';
                $strSQL .= '        WHERE LIBSNM = ? ';
                $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
                $strSQL .= '        AND LIBSFG = \'1\' ' ;
                array_push($params,$libsnm);
            }else{
                $strSQL .= ' QTEMP/DB2LBLS ';
            }
            $strSQL .= ' )';
        }
        $strSQL .= ' ) as B ';

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
            e_log('ライブラリーカウント取得SQL'.$strSQL.print_r($params,true).db2_stmt_errormsg());
        }else{
            e_log('ライブラリーカウント取得SQL'.$strSQL.print_r($params,true));
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($qtempFlg){
                        $data[] = $row['COUNT']+1;
                    }else{
                        $data[] = $row['COUNT'];
                    }
                }
                $data = array('result' => true,'data' => $data[0]);
            }
        }
    }
    return $data;
}

