<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$SEARCHDATA     = (isset($_POST['SEARCHDATA'])?$_POST['SEARCHDATA']:'');
$LIBNAME        = (isset($_POST['LIBNAME'])?$_POST['LIBNAME']:'');
$FILNAME        = (isset($_POST['FILNAME'])?$_POST['FILNAME']:'');
$LIBLIST        = json_decode($_POST['LIBLIST'],true);
$LIBNM          = (isset($_POST['LIBNM']))?$_POST['LIBNM']:'';
$start          = (isset($_POST['start'])?$_POST['start']:'');
$length         = (isset($_POST['length'])?$_POST['length']:'');
$sort           = (isset($_POST['sort'])?$_POST['sort']:'');
$sortDir        = (isset($_POST['sortDir'])?$_POST['sortDir']:'');

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$LIBSNM = '';
$LIBSDB = '';

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
if($rtn === 0){
    $LIBSNM = cmMer($userData[0]['WULIBL']);
}

if($rtn === 0){
    $RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
    if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
        $db2conRDB = $db2con;
    }else{
        $db2conRDB = cmDB2ConRDB();
    }
}
if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
    $LIBSDB = $RDBNM;
}else{
    $LIBSDB = RDB ;
}

if($rtn === 0){
    if($LIBSNM !== '' ){
        if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
            $resCreate = createQTEMPDB2LBLS($db2conRDB,$LIBSNM);
            if($resCreate !== true){
                $rtn = 1;
                $msg = showMsg($resCreate);
            }
        }
    }
}



if($rtn === 0){
    if($FILNAME === ''){
        $rs = fnGetAllCount($db2conRDB,$LIBNAME,$LIBLIST,$FILNAME,$SEARCHDATA,$LIBSNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }

        if($rtn === 0){
            $rs = fnGetFilData($db2conRDB,$LIBNAME,$LIBLIST,$LIBNM,$FILNAME,$SEARCHDATA,$LIBSNM,$start,$length,$sort,$sortDir);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
            }

        }
    }else{
        $rs = fnGetFilDataByFile($db2conRDB,$LIBNAME,$LIBNM,$LIBLIST,$FILNAME,$LIBSNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
            $allcount = count($data);
        }
    }
}
if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
    cmDb2Close($db2conRDB);
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetFilData($db2con,$LIBNAME,$LIBLIST,$LIBNM,$FILNAME,$SEARCHDATA,$LIBSNM = '',$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();
    $params = array();
    if(CRTQTEMPTBL_FLG === 1){
        createTmpFil($db2con,SYSSCHEMASLIB,'SYSSCHEMAS','','SYSSCHEMAS');
        createTmpFil($db2con,'QSYS2','SYSTABLES','','SYSTABLES');
    }
    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM( ';
    $strSQL .= '     SELECT B.SYSTEM_TABLE_NAME TABLE_NAME';
    $strSQL .= '          , B.TABLE_TEXT ';
    if($LIBNAME === ''){
        $strSQL .= '     , \''. $LIBNM . '\' AS TABLE_SCHEMA ';
        if(CRTQTEMPTBL_FLG === 1){
            $strSQL .= '     ,  \'\' AS SCHEMA_TEXT ';
        }else{
            $strSQL .= '          , C.SCHEMA_TEXT ';
        }
    }else{
        $strSQL .= '          , B.SYSTEM_TABLE_SCHEMA TABLE_SCHEMA ';
        if(CRTQTEMPTBL_FLG === 1){
            $strSQL .= '     ,  \'\' AS SCHEMA_TEXT ';
        }else{
            $strSQL .= '          , C.SCHEMA_TEXT ';
        }
    }
    $strSQL .= '          , B.SYSTEM_TABLE_SCHEMA AS TAB_SCHEMA ';
    $strSQL .= '          , B.TABLE_TYPE ';
    $strSQL .= '          , ROWNUMBER() OVER( ';

    if($sort != ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.SYSTEM_TABLE_SCHEMA ASC ';
        $strSQL .= ' , B.SYSTEM_TABLE_NAME ASC ';
    }

    $strSQL .= '            ) AS ROWNUM ';
    if(CRTQTEMPTBL_FLG === 1){
        $strSQL .= '     FROM QTEMP/SYSTABLES B ';
        //$strSQL .=  '     , QTEMP/SYSSCHEMAS C  ';
        $strSQL .= '     WHERE B.FILE_TYPE <> \'S\' ';
    }else{
        $strSQL .= '     FROM QSYS2/SYSTABLES B ';
        $strSQL .=  '      ,'.SYSSCHEMASLIB.'/SYSSCHEMAS C  ';
        $strSQL .= '     WHERE B.SYSTEM_TABLE_SCHEMA = C.SYSTEM_SCHEMA_NAME ';
        $strSQL .= '     AND B.FILE_TYPE <> \'S\' ';
    }
    $strSQL .= '     AND B.SYSTEM_TABLE_SCHEMA <> \'\' ';
    //$strSQL .= ' AND B.SYSTEM_TABLE_SCHEMA NOT IN (SELECT LIBSID FROM QTEMP/DB2LBLS)';
    if($LIBSNM !== '' ){
      if(SYSLIBCHK === '1'){
        $strSQL .= ' AND B.SYSTEM_TABLE_SCHEMA IN (';
        $strSQL .= '    SELECT ';
        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
        $strSQL .= '    FROM ';
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
            $strSQL .= MAINLIB.'/DB2LBLS ';
            $strSQL .= '        WHERE LIBSNM = ? ';
            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
            $strSQL .= '        AND LIBSFG = \'1\' ' ;
            array_push($params,$LIBSNM);
        }else{
            $strSQL .= ' QTEMP/DB2LBLS ';
        }
        $strSQL .= ' )';
      }else{
           $strSQL .= ' AND B.SYSTEM_TABLE_SCHEMA NOT IN (';
        $strSQL .= '    SELECT ';
        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
        $strSQL .= '    FROM ';
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
            $strSQL .= MAINLIB.'/DB2LBLS ';
            $strSQL .= '        WHERE LIBSNM = ? ';
            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
            $strSQL .= '        AND LIBSFG = \'1\' ' ;
            array_push($params,$LIBSNM);
        }else{
            $strSQL .= ' QTEMP/DB2LBLS ';
        }
        $strSQL .= ' )';
      }
    }
    if($LIBNAME != ''){
        $strSQL .= '     AND B.SYSTEM_TABLE_SCHEMA = ?';
        array_push($params,$LIBNAME);
    }

    if($LIBLIST != ''){
        $strSQL .= '     AND B.SYSTEM_TABLE_SCHEMA IN  (';
        for($i = 0;$i<count($LIBLIST); $i++){
            $strSQL .= ' ? ,';
            array_push($params,$LIBLIST[$i]);
        }
        $strSQL = substr($strSQL, 0, -1);
        $strSQL .= ' )';
    }

    if($FILNAME != ''){
        $strSQL .= '     AND B.SYSTEM_TABLE_NAME = ? ';
        array_push($params,$FILNAME);
    }
    
    if($SEARCHDATA != ''){
        $strSQL .= '     AND (B.SYSTEM_TABLE_NAME LIKE ? ';
        $strSQL .= '     OR  B.TABLE_TEXT LIKE ?  ';
        $strSQL .= '     OR  B.TABLE_TYPE LIKE ? ) ';
        array_push($params,'%'.$SEARCHDATA.'%');
        array_push($params,'%'.$SEARCHDATA.'%');
        array_push($params,'%'.$SEARCHDATA.'%');
    }    
    $strSQL .= ' )AS A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.ROWNUM BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log('【削除】テーブルデータ取得エラー①：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
            e_log('【削除】テーブルデータ取得エラー②：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data,true);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$LIBNAME,$LIBLIST,$FILNAME,$SEARCHDATA,$LIBSNM = ''){
    $data = array();
    $params = array();
    $strSQL  = ' SELECT COUNT(B.TABLE_NAME) AS COUNT';
    $strSQL .= ' FROM (';
    $strSQL .= ' SELECT ';
    $strSQL .= '        A.TABLE_NAME ';
    $strSQL .= '      , A.TABLE_TEXT ';
    $strSQL .= '      , A.TABLE_SCHEMA ';
    $strSQL .= '      , A.TABLE_TYPE ';
    $strSQL .= ' FROM QSYS2/SYSTABLES A'  ;
    $strSQL .= '     WHERE A.FILE_TYPE <> \'S\' ';
    $strSQL .= '     AND A.SYSTEM_TABLE_SCHEMA <> \'\' ';
    //$strSQL .= ' AND A.SYSTEM_TABLE_SCHEMA NOT IN (SELECT LIBSID FROM QTEMP/DB2LBLS)';
    if($LIBSNM !== ''){
     if(SYSLIBCHK === '1'){
        $strSQL .= ' AND A.SYSTEM_TABLE_SCHEMA IN (';
        $strSQL .= '    SELECT ';
        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
        $strSQL .= '    FROM ';
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
            $strSQL .= MAINLIB.'/DB2LBLS ';
            $strSQL .= '        WHERE LIBSNM = ? ';
            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
            $strSQL .= '        AND LIBSFG = \'1\' ' ;
            array_push($params,$LIBSNM);
        }else{
            $strSQL .= ' QTEMP/DB2LBLS ';
        }
        $strSQL .= ' )';
      }else{
        $strSQL .= ' AND A.SYSTEM_TABLE_SCHEMA NOT IN (';
        $strSQL .= '    SELECT ';
        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
        $strSQL .= '    FROM ';
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
            $strSQL .= MAINLIB.'/DB2LBLS ';
            $strSQL .= '        WHERE LIBSNM = ? ';
            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
            $strSQL .= '        AND LIBSFG = \'1\' ' ;
            array_push($params,$LIBSNM);
        }else{
            $strSQL .= ' QTEMP/DB2LBLS ';
        }
        $strSQL .= ' )';
      }
    }
    if($LIBNAME != '' ){
        $strSQL .= 'AND A.TABLE_SCHEMA = ? ';
        array_push($params,$LIBNAME);
    }
    if($LIBLIST != ''){
        $strSQL .= '     AND A.TABLE_SCHEMA IN  (';
        for($i = 0;$i<count($LIBLIST); $i++){
            $strSQL .= ' ? ,';
            array_push($params,$LIBLIST[$i]);
        }
        $strSQL = substr($strSQL, 0, -1);
        $strSQL .= ' )';
    }

    if($FILNAME !=''){
        $strSQL .= 'AND A.TABLE_NAME = ? ';
        array_push($params,$FILNAME);
    }
    //e_log("strSQL".$strSQL.print_r($params,true));
    if($SEARCHDATA != ''){
        $strSQL .= 'AND (A.TABLE_NAME = ? ';
        $strSQL .= 'OR A.TABLE_TEXT = ? ';
        $strSQL .= 'OR A.TABLE_TYPE = ?) ';
        array_push($params,'%'.$SEARCHDATA.'%');
        array_push($params,'%'.$SEARCHDATA.'%');
        array_push($params,'%'.$SEARCHDATA.'%');
    }
    $strSQL .= ' ) as B ';
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log('【削除】テーブルデータ取得エラー①：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
            e_log('【削除】テーブルデータ取得エラー②：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* データ取得ByID
*-------------------------------------------------------*
*/

function fnGetFilDataByFile($db2con,$LIBNAME,$LIBNM,$LIBLIST,$FILNAME,$LIBSNM = ''){
    $data = array();
    $params = array();
    if(CRTQTEMPTBL_FLG === 1){
        createTmpFil($db2con,SYSSCHEMASLIB,'SYSSCHEMAS','','SYSSCHEMAS');
        createTmpFil($db2con,'QSYS2','SYSTABLES','','SYSTABLES');
    }
    $strSQL  = ' SELECT ';
    $strSQL .= '        A.SYSTEM_TABLE_NAME TABLE_NAME ';
    $strSQL .= '      , A.TABLE_TEXT ';
    if($LIBNAME <> ''){
        $strSQL .= '      , A.SYSTEM_TABLE_SCHEMA TABLE_SCHEMA ';
        $strSQL .= '      , B.SCHEMA_TEXT ';
    }else{
        $strSQL .= '     , \''. $LIBNM . '\' AS TABLE_SCHEMA ';
        $strSQL .= '     ,  \'\' AS SCHEMA_TEXT ';
    }                        
    $strSQL .= '      , A.SYSTEM_TABLE_SCHEMA AS TAB_SCHEMA ';
    $strSQL .= '      , A.TABLE_TYPE ';
    if(CRTQTEMPTBL_FLG === 1){
        $strSQL .= '     FROM QTEMP/SYSTABLES A ';
        $strSQL .=  '     , QTEMP/SYSSCHEMAS B  ';
    }else{
        $strSQL .= ' FROM QSYS2/SYSTABLES A'  ;
        $strSQL .=  '      ,'.SYSSCHEMASLIB.'/SYSSCHEMAS B  ';
    }
    $strSQL .= '     WHERE A.SYSTEM_TABLE_SCHEMA = B.SYSTEM_SCHEMA_NAME ';
    $strSQL .= '     AND A.FILE_TYPE <> \'S\' ';
    $strSQL .= '     AND A.SYSTEM_TABLE_SCHEMA <> \'\' ';
    //$strSQL .= ' AND A.SYSTEM_TABLE_SCHEMA NOT IN (SELECT LIBSID FROM QTEMP/DB2LBLS)';
    if($LIBSNM !== ''){
      if(SYSLIBCHK === '1'){
        $strSQL .= ' AND A.SYSTEM_TABLE_SCHEMA IN (';
        $strSQL .= '    SELECT ';
        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
        $strSQL .= '    FROM ';
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
            $strSQL .= MAINLIB.'/DB2LBLS ';
            $strSQL .= '        WHERE LIBSNM = ? ';
            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
            $strSQL .= '        AND LIBSFG = \'1\' ' ;
        }else{
            $strSQL .= ' QTEMP/DB2LBLS ';
        }
        $strSQL .= ' )';
      }else{
        $strSQL .= ' AND A.SYSTEM_TABLE_SCHEMA NOT IN (';
        $strSQL .= '    SELECT ';
        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
        $strSQL .= '    FROM ';
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
            $strSQL .= MAINLIB.'/DB2LBLS ';
            $strSQL .= '        WHERE LIBSNM = ? ';
            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
            $strSQL .= '        AND LIBSFG = \'1\' ' ;
        }else{
            $strSQL .= ' QTEMP/DB2LBLS ';
        }
        $strSQL .= ' )';
      }
    }
    $strSQL .= '     AND A.TABLE_SCHEMA = ? ';
    $strSQL .= '     AND A.TABLE_NAME = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($LIBNAME <> ''){
            //$params = array($LIBNAME,$FILNAME);
            array_push($params,$LIBNAME);
            array_push($params,$FILNAME);
            if($LIBSNM === ''){
                $params = array($LIBNAME,$FILNAME);
            }else{
                $params = array($LIBSNM,$LIBNAME,$FILNAME);
            }

            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log('【削除】テーブルデータ取得エラー①：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }else{
            foreach($LIBLIST as $value){
    //           $params = array($value,$FILNAME);
                if($LIBSNM === ''){
                    $params = array($value,$FILNAME);
                }else{
                    $params = array($LIBSNM,$value,$FILNAME);
                }
                $r = db2_execute($stmt,$params);
                if($r === false){
                    e_log('【削除】テーブルデータ取得エラー②：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
 }



