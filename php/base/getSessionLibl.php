<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
// セッションライブラリ

$LIBLIST = (isset($_SESSION['PHPQUERY']['SELLIBLIST'])?$_SESSION['PHPQUERY']['SELLIBLIST']:'');
e_log('SAVDB'.isset($_SESSION['PHPQUERY']['SAVRDB']));
$RDBNM   = (isset($_SESSION['PHPQUERY']['SAVRDB'])?$_SESSION['PHPQUERY']['SAVRDB']:RDB);
$RDBNM = cmMer($RDBNM);
if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
    $RDBNM = RDB;
}
/**return**/
$rtn = array(
    'LIBLIST'       => $LIBLIST,
    'RDBNM'        => $RDBNM
);
$_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
echo(json_encode($rtn));

