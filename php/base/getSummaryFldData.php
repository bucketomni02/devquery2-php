<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$BASEFIELDINFO     = json_decode($_POST['BASEFIELDINFO'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
e_log('SummaryBASEFIELDINFO***'.print_r($BASEFIELDINFO ,true));
if(count($BASEFIELDINFO)>0){
    foreach ($BASEFIELDINFO as $FLDINFO) {
        foreach($FLDINFO['STRDATA'] as $FLDDATA){
            if($FLDDATA['SEQ'] == 0){
                $FLDDATA['SEQ'] = '';
            }
            if($FLDDATA['SEQ'] !== '' ){
                $fieldInfo = array();
                $fieldInfo['FILID']            = $FLDINFO['D2FILID'];
                if($FLDINFO['D2FILID'] === 0){
                    $fieldInfo['FILTYP']   = 'P';
                    $fieldInfo['FILTYPDESC']= $FLDINFO['D2FDESC'];
                }else if($FLDINFO['D2FDESC'] === '結果フィールド'){
                    $fieldInfo['FILTYP']    = 'K';
                    $fieldInfo['FILTYPDESC']= $FLDINFO['D2FDESC'];
                }else{
                    $fieldInfo['FILTYP']    = 'S'.$FLDINFO['D2FILID'];
                    $fieldInfo['FILTYPDESC']= '参照'.$FLDINFO['D2FILID'];
                }
                $fieldInfo['COLUMN_NAME']      = $FLDDATA['COLUMN_NAME'];
                $fieldInfo['COLUMN_HEADING']   = $FLDDATA['COLUMN_HEADING'];
                $fieldInfo['DDS_TYPE']         = $FLDDATA['DDS_TYPE'];
                $fieldInfo['LENGTH']           = $FLDDATA['LENGTH'];
                $fieldInfo['NUMERIC_SCALE']    = $FLDDATA['NUMERIC_SCALE'];
                $fieldInfo['FIELDVAL']         = $fieldInfo['FILTYP'].'.'.$FLDDATA['COLUMN_NAME'];
                $fieldInfo['FIELDDESC']        = $FLDDATA['COLUMN_NAME'];
                $fieldInfo['SEQ']              = $FLDDATA['SEQ'];
                $data[] = $fieldInfo;
            }
        }
    }
}    

cmDb2Close($db2con);
/**return**/
$rtn = array(
    'DATA' => umEx($data),
);
echo(json_encode($rtn));
