<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : 共通
* PROGRAM NAME   : テーブルのメンバーデータ取得【テーブル：SYSPSTAT】
* PROGRAM ID     : getTableMBRData.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/03/30
* MODIFY DATE    : 
* ============================================================
**/

/*
 * 外部ファイル読み込み
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("./getTblMBR.php");
/*
 *データリスト
 */
$SEARCHDATA     = (isset($_POST['SEARCHDATA']) ? $_POST['SEARCHDATA'] : '');
$SEARCHKEY      = (isset($_POST['SEARCHKEY']) ? $_POST['SEARCHKEY'] : '');
$TABLE_NAME     = $_POST['FILNAME'];
$TABLE_SCHEMA   = $_POST['LIBNAME'];
$LIBLIST        = json_decode($_POST['LIBLIST'],true);
$start          = (isset($_POST['start']) ? $_POST['start'] : '');
$length         = (isset($_POST['length']) ? $_POST['length'] : '');
$sort           = (isset($_POST['sort']) ? $_POST['sort'] : '');
$sortDir        = (isset($_POST['sortDir']) ? $_POST['sortDir'] : '');
$allcount       = 0;

/*
 * 変数
 */
$rtn        = 0;
$msg        = '';
$libFilInfo = array();
$data       = array();
/*
 * 処理
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
if($rtn === 0){
    $RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
    //e_log('RDB名：'.$RDBNM);
    $lbil = join(' ',$LIBLIST);
    if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
        $db2conlib = cmDb2ConLib($lbil);
    }else{
        $db2conlib = cmDB2ConRDB($lbil);
    }
}
if($rtn === 0){
    $rs = fnGetLibFil($db2conlib,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        if(count($rs['data']) === 0){
            $rtn = 2;
            $msg = showMsg('指定したファイルが有りません。');
        }else{
            $libFilInfo = $rs['data'][0];
            //e_log('liblFilInfo:'.print_r($libFilInfo,true));
        }
    }
}
if($rtn === 0){
    $rs = fnChkTblCol($db2conlib);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $lblflg = false;
        foreach($rs['data'] as $val){
            if($val === 'LABEL'){
                $lblflg = true;
                break;
            }
        }
    }
}
if($SEARCHKEY === ''){
    if($rtn === 0){
        $rs = fnGetAllCount($db2conlib,$SEARCHDATA,$libFilInfo['TABLE_NAME'],$libFilInfo['TABLE_SCHEMA'],$lblflg);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetTBLMBR($db2conlib,$SEARCHDATA,$libFilInfo['TABLE_NAME'],$libFilInfo['TABLE_SCHEMA'],$start,$length,$sort,$sortDir,$lblflg);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}else{
    if($rtn === 0){
        $rs = fnGetTBLMBRBYID($db2conlib,$SEARCHKEY,$libFilInfo['TABLE_NAME'],$libFilInfo['TABLE_SCHEMA'],$lblflg);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}
cmDb2Close($db2conlib);
cmDb2Close($db2con);
$rtnArr = array(
    'iTotalRecords' => $allcount,
    'LIBLFILINFO'   => $libFilInfo,
    'DATA'          => $data,
    'RTN'           => $rtn,
    'MSG'           => $msg,
);
echo(json_encode($rtnArr));
