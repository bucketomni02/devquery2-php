<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : 共通
* PROGRAM NAME   : テーブルのメンバーデータ取得【テーブル：SYSPSTAT】
* PROGRAM ID     : getTableMBRData.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/03/30
* MODIFY DATE    : 
* ============================================================
**/
function fnChkTblCol($db2con){
    $data = array();
    $strSQL = '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     SYSTEM_COLUMN_NAME ';
    $strSQL .= ' FROM ';
    $strSQL .=      SYSCOLUMN2 ;
    $strSQL .= ' WHERE ';
    $strSQL .= '     TABLE_SCHEMA = ? AND ';
    $strSQL .= '     TABLE_NAME = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log('【削除】テーブルメンバーデータ取得エラー①：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
    }else{
        $params = array('QSYS2','SYSPARTITIONSTAT');
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
            e_log('【削除】テーブルメンバーデータ取得エラー②：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = cmMer($row['SYSTEM_COLUMN_NAME']);
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
// ライブラリーとファイル取得
function fnGetLibFil($db2con,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST){
    $data   = array();
    $dcnt   = 0;

    $strSQL  = '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     TBLDATA.TABLE_NAME, ';
    $strSQL .= '     TBLDATA.TABLE_TEXT, ';
    $strSQL .= '     TBLDATA.TABLE_SCHEMA, ';
    $strSQL .= '     LIBDATA.SCHEMA_TEXT ';
    $strSQL .= ' FROM ';
    $strSQL .= ' ( ';
    $strSQL .= '     SELECT ';
    $strSQL .= '         TABLE_NAME        , ';
    $strSQL .= '         TABLE_TEXT        , ';
    $strSQL .= '         TABLE_SCHEMA ';
    $strSQL .= '     FROM ';
    $strSQL .= '         QSYS2/SYSTABLES ';
    $strSQL .= '     WHERE ';
    $strSQL .= '         TABLE_NAME = ? ';
    $strSQL .= '     AND TABLE_SCHEMA = ? ';
    $strSQL .= ' ) TBLDATA ';
    $strSQL .= ' LEFT JOIN  ( ';
    $strSQL .= '     SELECT ';
    $strSQL .= '         SYSTEM_SCHEMA_NAME, ';
    $strSQL .= '         SCHEMA_TEXT ';
    $strSQL .= '     FROM ';
    //$strSQL .= '         QSYS2/SYSSCHEMAS ';
    $strSQL .=  '      '.SYSSCHEMASLIB.'/SYSSCHEMAS  ';
    $strSQL .= '     WHERE ';
    $strSQL .= '         SYSTEM_SCHEMA_NAME = ? ';
    $strSQL .= ' )LIBDATA ';
    $strSQL .= ' ON TBLDATA.TABLE_SCHEMA = LIBDATA.SYSTEM_SCHEMA_NAME ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log('【削除】テーブルメンバーデータ取得エラー①：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
    }else{
        if(cmMer($TABLE_SCHEMA) === '' || strtoupper($TABLE_SCHEMA) === '*LIBL' || strtoupper($TABLE_SCHEMA) === '*USRLIBL'){
            foreach($LIBLIST as $lbiVal){
                $params = array($TABLE_NAME,$lbiVal,$lbiVal);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                    e_log('【削除】テーブルメンバーデータ取得エラー②：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
                }else{
                    $data = array();
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        $data = array('result' => true,'data' => umEx($data));
                        break;
                    }else{
                        $data = array('result' => true,'data'=> array());
                    }
                }
            }
        }else{
            $params = array($TABLE_NAME,$TABLE_SCHEMA,$TABLE_SCHEMA);
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => umEx($data));
            }
        }
    }
    return $data;
}
/**
  *---------------------------------------------------------------------------
  * テーブルのメンバーデータ取得
  * 
  * RESULT
  *    メンバーカウント
  * 
  * @param   db2con       データベース接続
  * @param String  SEARCHDATA       検索データ
  * @param String  TABLE_NAME       ファイル名
  * @param String  TABLE_SCHEMA     ライブラリー名
  *
  *---------------------------------------------------------------------------
  **/
function fnGetTBLMBR($db2con,$SEARCHDATA,$TABLE_NAME,$TABLE_SCHEMA,$start = '',$length = '',$sort = '',$sortDir = '',$lblflg = true){
    $data = array();
    $params = array();
    if(CRTQTEMPTBL_FLG === 1){
        $paramSQL = ' SELECT * FROM QSYS2/SYSPSTAT WHERE TABLE_SCHEMA = \''. $TABLE_SCHEMA .'\' AND TABLE_NAME = \''.$TABLE_NAME .'\'' ;
        createTmpFil($db2con,'QSYS2','SYSPSTAT','','SYSPSTAT',$paramSQL);
    }

    $strSQL = '';

    $strSQL .= ' SELECT  A.* ';
    $strSQL .= ' FROM( ';
    $strSQL .= '    SELECT ';
    $strSQL .= '        B.SYS_DNAME, ';
    $strSQL .= '        B.SYS_TNAME, ';
    $strSQL .= '        B.SYS_MNAME, ';
    $strSQL .= '        B.PARTNBR, ';
    if($lblflg){
        $strSQL .= '        B.LABEL, ';
    }else{
        $strSQL .= '        B.SYS_MNAME AS LABEL, ';
    }
    $strSQL .= '        ROWNUMBER() OVER( ';
    if($sort != ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.PARTNBR ASC ';
    }
    $strSQL .= '            ) AS ROWNUM ';
    $strSQL .= '    FROM ';
    if(CRTQTEMPTBL_FLG === 0){
        $strSQL .= '        QSYS2/SYSPSTAT B';
    }else{
        $strSQL .= '        QTEMP/SYSPSTAT B';
    }
    $strSQL .= '    WHERE ';
    $strSQL .= '        B.TABLE_SCHEMA = ? ';
    $strSQL .= '    AND B.TABLE_NAME = ? ';
    
    $params = array($TABLE_SCHEMA,$TABLE_NAME);
    if($SEARCHDATA != ''){
        if($lblflg){
            $strSQL .= '    AND ';
            $strSQL .= '     (SYS_MNAME like ? ';
            $strSQL .= '    OR LABEL like ? ) ';
            array_push($params,'%'.$SEARCHDATA.'%');
            array_push($params,'%'.$SEARCHDATA.'%');
        }else{
            $strSQL .= '    AND ';
            $strSQL .= '     SYS_MNAME like ? ';
            array_push($params,'%'.$SEARCHDATA.'%');
        }
    }
    $strSQL .= '    ORDER BY PARTNBR ';
    $strSQL .= ' ) A ';
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.ROWNUM BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    //e_log('データ取得:'.$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log('【削除】テーブルメンバーデータ取得エラー①：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        e_log($strSQL.db2_stmt_errormsg());
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
            e_log('【削除】テーブルメンバーデータ取得エラー②：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                unset($row[count($row)-1]);
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }

     }
    return $data;
}
/**
  *---------------------------------------------------------------------------
  * テーブルのメンバーデータ取得
  * 
  * RESULT
  *    メンバーカウント
  * 
  * @param   db2con       データベース接続
  * @param String  SEARCHDATA       検索データ
  * @param String  TABLE_NAME       ファイル名
  * @param String  TABLE_SCHEMA     ライブラリー名
  *
  *---------------------------------------------------------------------------
  **/
function fnGetTBLMBRBYID($db2con,$SEARCHKEY,$TABLE_NAME,$TABLE_SCHEMA,$lblflg = true){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .= '    SELECT ';
    $strSQL .= '        A.SYS_DNAME, ';
    $strSQL .= '        A.SYS_TNAME, ';
    $strSQL .= '        A.SYS_MNAME, ';
    $strSQL .= '        A.PARTNBR, ';
    if($lblflg){
        $strSQL .= '        A.LABEL ';
    }else{
        $strSQL .= '        A.SYS_MNAME AS LABEL ';
    }
    $strSQL .= '    FROM ';
    $strSQL .= '        QSYS2/SYSPSTAT A';
    $strSQL .= '    WHERE ';
    $strSQL .= '        A.TABLE_SCHEMA = ? ';
    $strSQL .= '    AND A.TABLE_NAME = ? ';
    $strSQL .= '    AND A.SYS_MNAME = ?';
    
    $params = array($TABLE_SCHEMA,$TABLE_NAME,$SEARCHKEY);
    //e_log('データ取得:'.$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log($strSQL.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        e_log($strSQL.db2_stmt_errormsg());
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                unset($row[count($row)-1]);
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }

     }
    return $data;
}
/**
  *---------------------------------------------------------------------------
  * テーブルのメンバーデータカウント
  * 
  * RESULT
  *    メンバーカウント
  * 
  * @param   db2con       データベース接続
  * @param String  SEARCHDATA       検索データ
  * @param String  TABLE_NAME       ファイル名
  * @param String  TABLE_SCHEMA     ライブラリー名
  *
  *---------------------------------------------------------------------------
  **/
function fnGetAllCount($db2con,$SEARCHDATA,$TABLE_NAME,$TABLE_SCHEMA,$lblflg){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .= ' SELECT  COUNT(A.SYS_MNAME) AS COUNT';
    $strSQL .= ' FROM( ';
    $strSQL .= '    SELECT ';
    $strSQL .= '        SYS_DNAME, ';
    $strSQL .= '        SYS_TNAME, ';
    $strSQL .= '        SYS_MNAME, ';
    $strSQL .= '        PARTNBR, ';
    if($lblflg){
        $strSQL .= '        LABEL ';
    }else{
        $strSQL .= '        SYS_MNAME AS LABEL ';
    }
    $strSQL .= '    FROM ';
    $strSQL .= '        QSYS2/SYSPSTAT ';
    $strSQL .= '    WHERE ';
    $strSQL .= '        TABLE_SCHEMA = ? ';
    $strSQL .= '    AND TABLE_NAME = ? ';
    
    $params = array($TABLE_SCHEMA,$TABLE_NAME);
    if($SEARCHDATA != ''){
        if($lblflg){
            $strSQL .= '    AND ';
            $strSQL .= '     (SYS_MNAME like ? ';
            $strSQL .= '    OR LABEL like ? ) ';
            array_push($params,'%'.$SEARCHDATA.'%');
            array_push($params,'%'.$SEARCHDATA.'%');
        }else{
            $strSQL .= '    AND ';
            $strSQL .= '     SYS_MNAME like ? ';
            array_push($params,'%'.$SEARCHDATA.'%');
        }
    }
    $strSQL .= '    ORDER BY PARTNBR ';
    $strSQL .= ' ) A ';
    e_log($strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log($strSQL.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
            e_log($strSQL.db2_stmt_errormsg(),print_r($params,true));
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
     }
    return $data;
}
