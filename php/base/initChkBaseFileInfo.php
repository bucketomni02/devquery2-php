<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$SELLIBLIST     = (isset($_POST['SELLIBLIST']))?json_decode($_POST['SELLIBLIST'],true):'';
$D1RDB          = cmHscDe($_POST['D1RDB']);
$REFFILE        = json_decode($_POST['REFFILE'],true);
$D1NAME         = cmHscDe($_POST['D1NAME']);
$D1FILLIB       = cmHscDe($_POST['D1FILLIB']);
$D1FILE         = cmHscDe($_POST['D1FILE']);
$D1FILMBR       = cmHscDe($_POST['D1FILMBR']);
$D1TEXT         = cmHscDe($_POST['D1TEXT']);
$PROC           = cmHscDe($_POST['PROC']);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$PRYFILINFO = array();
$allcount   = 0;
$data       = array();
$rtn        = 0;
$msg        = '';
$chkPLIB    = '';
$RTNDATA    = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'3',$userData[0]['WUSAUT']);//'3' =>  queryMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('クエリー作成の権限'));
            }
        }
    }
}
//クエリー存在チェック
if($PROC !== 'ADD'){
    if($rtn === 0){
        $rs = fnGetFDB2CSV1($db2con,$D1NAME);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            if(count($rs['data']) === 0){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('クエリー'));
            }
        }
    }
}

$RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
    $db2conRDB = $db2con;
}else{
    $db2conRDB = cmDB2ConRDB();
}

//プライマリ.ライブラリーチェック
if($rtn === 0){
    $D1FILLIBTXT = fnChkExistLib($db2conRDB,$D1FILLIB,$SELLIBLIST);
    if($D1FILLIBTXT === 'FAIL_SEL'){
        $rtn = 1;
        $msg = $D1FILLIBTXT;
    }
}
//プライマリ.ファイルチェック
if($rtn === 0){
    $D1FILETXT = fnChkExistFile($db2conRDB,$D1FILLIB,'',$SELLIBLIST,$D1FILE);
    if($D1FILETXT === 'FAIL_SEL'){
        $rtn = 1;
        $msg = $D1FILETXT;
    }else{
        e_log('fileRes'.print_r($D1FILETXT,true));
        if(is_array($D1FILETXT)){
            $lib        = $D1FILETXT['TAB_SCHEMA'];
            $fil        = $D1FILETXT['TABLE_NAME'];
            $D1FILETXT  = $D1FILETXT['TABLE_TEXT'];
        }else{
            $lib = '';
            $fil = '';
            $D1FILETXT  = $D1FILETXT;
        }
    }
}
//プライマリ.ファイルメンバーチェック
if($rtn === 0){
    $D1FILMBR = cmMer(strtoupper($D1FILMBR));
    if(cmMer($D1FILMBR === '') || $D1FILMBR === '*FIRST' || $D1FILMBR === '*LAST' ){
        $D1FILMBRTXT = '';
    }else{
        if($lib === '' || $fil === ''){
            $D1FILMBRTXT =  showMsg('NOTEXIST',array('ファイル'));
        }else{
            $D1FILMBRTXT = fnChkExistMBR($db2conRDB,$lib,$fil,$D1FILMBR);
            if($D1FILMBRTXT === 'FAIL_SEL'){
                $rtn = 1;
                $msg = $D1FILMBRTXT;
            }
        }
    }
}
if($rtn === 0){
    $RTNREFFIL = array();
    $refCnt = 0;
    if(count($REFFILE) > 0){
        foreach ($REFFILE as $RFILVAL) {
            $RLIBTXT = fnChkExistLib($db2conRDB,$RFILVAL['RTRLIB'],$SELLIBLIST);
            if($RLIBTXT === 'FAIL_SEL'){
                $rtn = 1;
                $msg = $RLIBTXT;
                break;
            }
            $RFILTXT = fnChkExistFile($db2conRDB,$RFILVAL['RTRLIB'],'',$SELLIBLIST,$RFILVAL['RTRFIL']);
            if($RFILTXT === 'FAIL_SEL'){
                $rtn = 1;
                $msg = $RFILTXT;
                break;
            }else{
                $lib        = $RFILTXT['TAB_SCHEMA'];
                $fil        = $RFILTXT['TABLE_NAME'];
                $RFILTXT    = $RFILTXT['TABLE_TEXT'];
            }
            // 参照ファイルメンバー取得
            $RTRMBR = cmMer(strtoupper($RFILVAL['RTRMBR']));
            if(cmMer($RTRMBR === '') || $RTRMBR === '*FIRST' || $RTRMBR === '*LAST' ){
                $RTRMBRTXT = '';
            }else{
                e_log('メンバーのライブラリーとファイル情報：'.$lib.'file'.$fil);
                if($lib === '' || $fil === ''){
                    $RTRMBRTXT =  showMsg('NOTEXIST',array('ファイル'));
                }else{
                    $RTRMBRTXT = fnChkExistMBR($db2conRDB,$lib,$fil,$RTRMBR);
                    if($RTRMBRTXT === 'FAIL_SEL'){
                        $rtn = 1;
                        $msg = $RTRMBRTXT;
                    }
                }
            }
            $RFILVAL['RLIBTXT'] = $RLIBTXT;
            $RFILVAL['RFILTXT'] = $RFILTXT;
            $RFILVAL['RTRMBRTXT'] = $RTRMBRTXT;
            $RTNREFFIL[] = $RFILVAL;
        }
    }
}
if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
    cmDb2Close($db2conRDB);
}
if($rtn === 0){
    $data['SELLIBLIST']     = $SELLIBLIST;
    $data['D1NAME']         = $D1NAME;
    $data['D1FILLIB']       = $D1FILLIB;
    $data['D1FILE']         = $D1FILE;
    $data['D1FILMBR']       = $D1FILMBR;
    $data['D1TEXT']         = $D1TEXT;
    $data['D1FILLIBTXT']    = $D1FILLIBTXT;
    $data['D1FILETXT']      = $D1FILETXT;
    $data['D1FILMBRTXT']    = $D1FILMBRTXT;
    $data['REFFILE']        = $RTNREFFIL;
}
cmDb2Close($db2con);
/**return**/
$RTNDATA = array(
    'BASEFILEDATA' => $data,
    'SELLIST' => $SELLIBLIST,
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($RTNDATA));

/*
 *  クエリー情報取得
 */
function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
/*
 *  ライブラリー情報存在チェック、ある場合ライブラリーテキスト取得
 */
function fnChkExistLib($db2con,$LIB,$LIBLIST){
    $libtxt = '';
    if(($LIB === '') || ($LIB === '*USRLIBL') || ($LIB === '*LIBL') ){

    }else{
        $rs = array();
        $rs = fnGetLibInfo($db2con,$LIB,$LIBLIST);
        if($rs['result'] === 'FAIL_SEL'){
            $libtxt = $rs['result'];
        }else if($rs['result'] === 'NOTEXIST'){
            $libtxt = showMsg($rs['result'],array('ライブラリー'));
        }else{
            $libtxt = $rs['data']['SCHEMA_TEXT'];
        }
    }
    return $libtxt;
}
/*
 *  ファイルメンバー存在チェック、ある場合ファイルメンバーテキスト取得
 */
function fnChkExistMBR($db2con,$LIBNM,$FILNM,$MBRNM){
    $mbrTxt = '';
    e_log('メンバー取得：'.$LIBNM,$FILNM,$MBRNM);
    if($rtn === 0){
        $rs = fnChkTblCol($db2con);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $lblflg = false;
            foreach($rs['data'] as $val){
                if($val === 'LABEL'){
                    $lblflg = true;
                    break;
                }
            }
        }
    }
    $rs = fnGetMBRInfo($db2con,$LIBNM,$FILNM,$MBRNM,$lblflg);
    if($rs['result' === 'FAIL_SEL']){
        $mbrTxt = $rs['result'];
    }else if($rs['result'] === 'NOTEXIST'){
        $mbrTxt = showMsg($rs['result'],array('ファイル'));
    }else{
        //$filetxt = $rs['data']['TABLE_TEXT'];
        $mbrTxt = $rs['data']['LABEL'];
    }
    return $mbrTxt;
}
/*
 *  ファイル情報存在チェック、ある場合ファイル情報
 */
function fnChkExistFile($db2con,$LIBNAME,$LIBNM,$LIBLIST,$FILNAME){
    $filetxt = '';
    if($FILNAME <> '' ){
        $rs = array();
        if(($LIBNAME === '') || ($LIBNAME  === '*USRLIBL') || ($LIBNAME === '*LIBL')){
            $LIBNM = $LIBNAME;
            $rs = fnGetFilInfo($db2con,'',$LIBNM,$LIBLIST,$FILNAME);
        }else{
            $rs = fnGetFilInfo($db2con,$LIBNAME,'',$LIBLIST,$FILNAME);
        }
        if($rs['result'] === 'FAIL_SEL'){
            $filetxt = $rs['result'];
        }else if($rs['result'] === 'NOTEXIST'){
            $filetxt = showMsg($rs['result'],array('ファイル'));
        }else{
            //$filetxt = $rs['data']['TABLE_TEXT'];
            $filetxt = $rs['data'];
        }
    }
    return $filetxt;
}
/*
 *  ライブラリー情報取得
 */
function fnGetLibInfo($db2con,$LIB,$LIBLIST){
    $data = array();
    $params = array();
    $qtempData = array(
                    SCHEMA_NAME => 'QTEMP',
                    SCHEMA_TEXT => '' 
            );
    if($LIB === 'QTEMP'){
        $data = array('result' => true,'data' => $qtempData);
    }else{
        if($LIB === '*USRLIBL' || $LIB === '*LIBL'){
            if (($key = array_search('QTEMP', $LIBLIST)) !== false) {
                unset($LIBLIST[$key]);
            }
        }
        $rs = 0;
        $strSQL  = ' SELECT A.SCHEMA_NAME ';
        $strSQL .= ' , A.SCHEMA_TEXT ';
        $strSQL .= ' FROM  ' ;
        $strSQL .= '    (SELECT ';
        $strSQL .= '        SYSTEM_SCHEMA_NAME SCHEMA_NAME ';
        $strSQL .= '        , SCHEMA_TEXT ';
        $strSQL .= '    FROM ';
        $strSQL .= '        '.SYSSCHEMASLIB.'/SYSSCHEMAS ';
        $strSQL .= '    WHERE SYSTEM_SCHEMA_NAME <> \'QTEMP\' ';
        $strSQL .= '    ) A';
        if($LIB === '*USRLIBL' || $LIB === '*LIBL'){
            $strSQL .= ' WHERE A.SCHEMA_NAME IN  (';
            for($i=0; $i<count($LIBLIST);$i++){
               $strSQL .= ' ? ,';
                array_push($params,$LIBLIST[$i]);
            }
            $strSQL = substr($strSQL, 0, -1);
            $strSQL .= ' )';
        }else{
            $strSQL .= ' WHERE A.SCHEMA_NAME = ? ' ;
            array_push($params,$LIB);
        }
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = umEx($data);
                if($LIB <> '*USRLIBL' || $LIB <> '*LIBL'){
                    if(count($data) < 0){
                        $data = array('result' => 'NOTEXIST');
                    }
                }else{
                    if(count($data) !== count($LIBLIST)){
                        $data = array('result' => 'NOTEXIST');
                    }
                }
                $data = array('result' => true,'data' => $data[0]);
            }
        }
    }
    return $data;
}
/*
 *  ファイル情報取得
 */
function fnGetFilInfo($db2con,$LIBNAME,$LIBNM,$LIBLIST,$FILNAME){
    $data = array();
    $strSQL  = ' SELECT ';
    $strSQL .= '        A.SYSTEM_TABLE_NAME TABLE_NAME ';
    $strSQL .= '      , A.TABLE_TEXT ';
    if($LIBNAME <> ''){
        $strSQL .= '      , A.SYSTEM_TABLE_SCHEMA TABLE_SCHEMA ';
        $strSQL .= '      , B.SCHEMA_TEXT ';
    }else{
        $strSQL .= '     , \''. $LIBNM . '\' AS TABLE_SCHEMA ';
        $strSQL .= '     ,  \'\' AS SCHEMA_TEXT ';
    }                        
    $strSQL .= '      , A.SYSTEM_TABLE_SCHEMA AS TAB_SCHEMA ';
    $strSQL .= '      , A.TABLE_TYPE ';
    $strSQL .= ' FROM QSYS2/SYSTABLES A'  ;
    $strSQL .= '        ,'.SYSSCHEMASLIB.'/SYSSCHEMAS B ';
    //$strSQL .= '    , QSYS2/SYSSCHEMAS B ';
    $strSQL .= '     WHERE A.SYSTEM_TABLE_SCHEMA = B.SYSTEM_SCHEMA_NAME ';
    $strSQL .= '     AND A.FILE_TYPE <> \'S\' ';
    $strSQL .= '     AND A.SYSTEM_TABLE_SCHEMA <> \'\' ';
    $strSQL .= '     AND A.TABLE_SCHEMA = ? ';
    $strSQL .= '     AND A.TABLE_NAME = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        e_log('リブラリー取得：'.$strSQL.db2_stmt_errormsg ());
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($LIBNAME <> ''){
            $params = array($LIBNAME,$FILNAME);
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = umEx($data);
                if(count($data) === 0){
                    $data = array('result' => 'NOTEXIST');
                }else{
                    $data = array('result' => true,'data' => $data[0]); 
                } 
            }
        }else{
            foreach($LIBLIST as $value){
               $params = array($value,$FILNAME);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    $data = umEx($data);
                    if(count($data) > 0){
                        break;
                    }
                }
            }
            if(count($data) === 0){
                    $data = array('result' => 'NOTEXIST');
            }else{
                $data = array('result' => true,'data' => $data[0]); 
            }
        }
    }
    return $data;
}
/*
 *  ファイルメンバー情報取得
 */
function fnGetMBRInfo($db2con,$LIB,$FIL,$MBR,$lblflg){
    $data = array();
    $strSQL = '';
    $strSQL .= '    SELECT ';
    $strSQL .= '        A.SYS_DNAME, ';
    $strSQL .= '        A.SYS_TNAME, ';
    $strSQL .= '        A.SYS_MNAME, ';
    $strSQL .= '        A.PARTNBR, ';
    if($lblflg){
        $strSQL .= '        A.LABEL ';
    }else{
        $strSQL .= '        A.SYS_MNAME AS LABEL ';
    }
    $strSQL .= '    FROM ';
    $strSQL .= '        QSYS2/SYSPSTAT A';
    $strSQL .= '    WHERE ';
    $strSQL .= '        A.TABLE_SCHEMA = ? ';
    $strSQL .= '    AND A.TABLE_NAME = ? ';
    $strSQL .= '    AND A.SYS_MNAME = ?';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($LIB,$FIL,$MBR);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            if(count($data) === 0){
                $data = array('result' => 'NOTEXIST');
            }else{
                $data = array('result' => true,'data' => $data[0]); 
            }
        }
    }
    e_log('メンバー取得：'.$strSQL.'parameter:'.print_r($params,true).print_r($data,true));
    return $data;
}