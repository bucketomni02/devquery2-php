<?php

/**
 * ===========================================================================================
 * SYSTEM NAME    : PHPQUERY2
 * PROGRAM NAME   : BASE 登録・更新処理
 * PROGRAM ID     : insQryTableData.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/06/13
 * ===========================================================================================
 **/

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("./insTableData.php");

/*
 * 変数
 */
$rtn = 0;
$msg = '';
// 【挿入】FDB2CSV1
function insFDB2CSV1($db2con,$fdb2csv1){
    $strSQL = '';
    $data   = array();
   
    $strSQL .= ' INSERT INTO FDB2CSV1 ';
    $strSQL .= '        ( ';
    $strSQL .= '            D1NAME, ';
    $strSQL .= '            D1FILE, ';
    $strSQL .= '            D1FILMBR, ';
    $strSQL .= '            D1FILLIB, ';
    $strSQL .= '            D1TEXT, ';
    $strSQL .= '            D1CSID, ';
    $strSQL .= '            D1LIBL, ';
    $strSQL .= '            D1WEBF, ';
    $strSQL .= '            D1RDB';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    
    $params =array(
                $fdb2csv1['D1NAME'],
                $fdb2csv1['D1FILE'],
                $fdb2csv1['D1FILMBR'],
                $fdb2csv1['D1FILLIB'],
                $fdb2csv1['D1TEXT'],
                $fdb2csv1['D1CSID'],
                $fdb2csv1['D1LIBL'],
                $fdb2csv1['D1WEBF'],
                $fdb2csv1['D1RDB']
             );
    //e_log("insFDB2CSV1 MSM => ".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【更新】FDB2CSV1
function updFDB2CSV1($db2con,$fdb2csv1){
    $strSQL = '';
    $data   = array();
   
    $strSQL .= ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= '    D1FILE      = ? , ';
    $strSQL .= '    D1FILMBR    = ? , ';
    $strSQL .= '    D1FILLIB    = ? , ';
    $strSQL .= '    D1TEXT      = ? , ';
    $strSQL .= '    D1CSID      = ? , ';
    $strSQL .= '    D1LIBL      = ? , ';
    $strSQL .= '    D1WEBF      = ? , ';
    $strSQL .= '    D1RDB      = ? ';
    $strSQL .= ' WHERE  ';
    $strSQL .= '    D1NAME      = ? ';
    
    $params =array(
                $fdb2csv1['D1FILE'],
                $fdb2csv1['D1FILMBR'],
                $fdb2csv1['D1FILLIB'],
                $fdb2csv1['D1TEXT'],
                $fdb2csv1['D1CSID'],
                $fdb2csv1['D1LIBL'],
                $fdb2csv1['D1WEBF'],
                $fdb2csv1['D1RDB'],
                $fdb2csv1['D1NAME']
             );
    //e_log("updFDB2CSV1 MSM => ".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            //e_log("DATA FALSE => ".$data );
            $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【出力ファイルの更新】FDB2CSV1
function updOUTFILEFDB2CSV1($db2con,$fdb2csv1){
    $strSQL = '';
    $data   = array();
   
    $strSQL .= ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= '    D1OUTJ      = ? , ';
    $strSQL .= '    D1OUTA      = ? , ';
    $strSQL .= '    D1OUTLIB    = ? , ';
    $strSQL .= '    D1OUTFIL    = ? , ';
    $strSQL .= '    D1OUTR    = ?  ';
    $strSQL .= ' WHERE  ';
    $strSQL .= '    D1NAME      = ? ';
    
    $params =array(
                $fdb2csv1['D1OUTJ'],
                $fdb2csv1['D1OUTA'],
                strtoupper($fdb2csv1['D1OUTLIB']),
                strtoupper($fdb2csv1['D1OUTFIL']),
                $fdb2csv1['D1OUTR'],
                $fdb2csv1['D1NAME']
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        //e_log($strSQL.print_r($params,true));
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}

/* 【挿入】FDB2CSV1 別のDBのため
*newLibはもらったクエリーを保存するため、使うライブラリー
**/
function insFDB2CSV1_OTHER($db2con,$newLib,$D1NAME){
    $result = 0;
    $param=array();
    $strSQL = '';
    $strSQL .= ' INSERT INTO '.$newLib.'.'.'FDB2CSV1 ';
    $strSQL .= ' (SELECT * FROM FDB2CSV1 ';
    $strSQL .= ' WHERE D1NAME=?)';
    $params=array($D1NAME);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
        $result = 1;
    }else{            
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg());
            $result = 1;
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}

// 【削除】FDB2CSV1
function delFDB2CSV1($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM FDB2CSV1 ';
    $strSQL .=  '    WHERE D1NAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
            $templatename = ETEMP_DIR.$QRYNM.'.*';
            if (file_exists($templatename)) {
                @unlink($templatename);
            }
        }
    }
    return $data;
}
// 【挿入】BREFTBL BREFFLD
function insBREFTBLFLD($db2con,$breftblfld){
    $result = 0;
    foreach($breftblfld as $breftbl){
        $strSQL = '';      
        $strSQL .= ' INSERT INTO BREFTBL ';
        $strSQL .= '        ( ';
        $strSQL .= '            RTQRYN, ';
        $strSQL .= '            RTRSEQ, ';
        $strSQL .= '            RTRFIL, ';
        $strSQL .= '            RTRMBR, ';
        $strSQL .= '            RTRLIB, ';
        $strSQL .= '            RTDESC, ';
        $strSQL .= '            RTJTYP ';       
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?';
        $strSQL .= '        ) ';
        
        $params =array(
                    $breftbl['RTQRYN'],
                    $breftbl['RTRSEQ'],
                    $breftbl['RTRFIL'],
                    $breftbl['RTRMBR'],
                    $breftbl['RTRLIB'],
                    $breftbl['RTDESC'],
                    $breftbl['RTJTYP']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BREFTBL:'.db2_stmt_errormsg()
                    );
            $result = 1;
            break;
        }else{            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BREFTBL:'.db2_stmt_errormsg()
                    );
                $result = 1;
                break;
            }else{
                $result = 0;
                $data = array('result' => true);
                if(count($breftbl['BREFFLD'])>0){
                    foreach($breftbl['BREFFLD'] as $breffld){
                        $strSQL = '';
                        $data   = array();
                        $strSQL .= ' INSERT INTO BREFFLD ';
                        $strSQL .= '        ( ';
                        $strSQL .= '            RFQRYN, ';
                        $strSQL .= '            RFRSEQ, ';
                        $strSQL .= '            RFFSEQ, ';
                        $strSQL .= '            RFPFID, ';
                        $strSQL .= '            RFPFNM, ';
                        $strSQL .= '            RFRFID, ';
                        $strSQL .= '            RFRFNM, ';
                        $strSQL .= '            RFRKBN, ';
                        $strSQL .= '            RFTEISU ';

                        $strSQL .= '        ) ';
                        $strSQL .= ' VALUES ( ';
                        $strSQL .= '            ?,?,?,?,?,?,?,?,?';
                        $strSQL .= '        ) ';

                        $params =array(
                            $breffld['RFQRYN'],
                            $breffld['RFRSEQ'],
                            $breffld['RFFSEQ'],
                            $breffld['RFPFID'],
                            $breffld['RFPFNM'],
                            $breffld['RFRFID'],
                            $breffld['RFRFNM'],
                            $breffld['RFRKBN'],
                            $breffld['RFTEISU']

                        );
						e_log('Checking sql value for breffld'.$strSQL.print_r($params,true));

                        $stmt = db2_prepare($db2con,$strSQL);
                        if($stmt === false ){
                            $data = array(
                                        'result' => 'FAIL_INS',
                                        'errcd'  => 'BREFFLD:'.db2_stmt_errormsg()
                                    );
                            $result = 1;
                            break;
                        }else{
                            $res = db2_execute($stmt,$params);
                            if($res === false){
                                $data = array(
                                        'result' => 'FAIL_INS',
                                        'errcd'  => 'BREFFLD:'.db2_stmt_errormsg()
                                    );
                                $result = 1;
                                break;
                            }else{
                                $result = 0;
                                $data = array('result' => true);
                            }
                        }
                    }
                }
                if($result === 1){
                    break;
                }
            }
        }
    }
    return $data;
}
/* 【挿入】BREFTBL BREFFLD 別のDBのため
*newLibはもらったクエリーを保存するため、使うライブラリー
**/
function insBREFTBLFLD_OTHER($db2con,$newLib,$D1NAME){
    $result = 0;
    $param=array();
    $strSQL = '';
    $strSQL .= ' INSERT INTO '.$newLib.'.'.'BREFTBL ';
    $strSQL .= ' (SELECT * FROM BREFTBL ';
    $strSQL .= ' WHERE RTQRYN=?)';
    $params=array($D1NAME);
    //e_log('insBREFTBLFLD_OTHER SQL=>'.$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array('result' => 'FAIL_INS','errcd'  => 'BREFTBL:'.db2_stmt_errormsg());
        $result = 1;
    }else{            
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BREFTBL:'.db2_stmt_errormsg());
            $result = 1;
        }else{
            $result = 0;
            $data = array('result' => true);
            $strSQL = '';
            $data   = array();
            $strSQL     .=  ' INSERT INTO '.$newLib.'.'.'BREFFLD ';
            $strSQL     .=  ' (SELECT * FROM BREFFLD ';
            $strSQL     .=  ' WHERE RFQRYN=? )';
            $param=array($D1NAME);
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false ){
                $data = array('result' => 'FAIL_INS','errcd'  => 'BREFFLD:'.db2_stmt_errormsg());
                $result = 1;
            }else{
                $res = db2_execute($stmt,$param);
                if($res === false){
                    $data = array('result' => 'FAIL_INS','errcd'  => 'BREFFLD:'.db2_stmt_errormsg());
                    $result = 1;
                }else{
                    $result = 0;
                    $data = array('result' => true);
                }
            }
        }
    }
    return $data;
}
// 【削除】BREFTBL
function delBREFTBL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BREFTBL ';
    $strSQL .=  '    WHERE RTQRYN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BREFTBL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BREFTBL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】BREFFLD
function delBREFFLD($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BREFFLD ';
    $strSQL .=  '    WHERE RFQRYN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
	e_log('Myat su moe_ei'.$strSQL.print_r($params,true));
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BREFFLD:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BREFFLD:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【挿入】FDB2CSV2
function insFDB2CSV2($db2con,$fdb2csv2){
    $data   = array();
    foreach($fdb2csv2 as $fdb2csv2data){
        $strSQL = '';
       
        $strSQL .= ' INSERT INTO FDB2CSV2 ';
        $strSQL .= '        ( ';
        $strSQL .= '            D2NAME, ';
        $strSQL .= '            D2FILID, ';
        $strSQL .= '            D2FLD, ';
        $strSQL .= '            D2HED, ';
        $strSQL .= '            D2CSEQ, ';
        $strSQL .= '            D2RSEQ, ';
        $strSQL .= '            D2RSTP, ';
        $strSQL .= '            D2WEDT, ';
        $strSQL .= '            D2TYPE, ';
        $strSQL .= '            D2LEN, ';
        $strSQL .= '            D2DEC ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
                    $fdb2csv2data['D2NAME'],
                    $fdb2csv2data['D2FILID'],
                    $fdb2csv2data['D2FLD'],
                    $fdb2csv2data['D2HED'],
                    $fdb2csv2data['D2CSEQ'],
                    $fdb2csv2data['D2RSEQ'],
                    $fdb2csv2data['D2RSTP'],
                    $fdb2csv2data['D2WEDT'],
                    $fdb2csv2data['D2TYPE'],
                    $fdb2csv2data['D2LEN'],
                    $fdb2csv2data['D2DEC']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
/* 【挿入】FDB2CSV2
*newLibはもらったクエリーを保存するため、使うライブラリー
**/
function insFDB2CSV2_OTHER($db2con,$newLib,$D1NAME){
    $data   = array();
    $strSQL= '';
    $strSQL     .=  ' INSERT INTO '.$newLib.'.FDB2CSV2 ';
    $strSQL     .=  ' (SELECT * FROM FDB2CSV2 ';
    $strSQL     .=  ' WHERE D2NAME=?) ';
    $params=array($D1NAME);
    $stmt = db2_prepare($db2con,$strSQL);
    //e_log('FDB2CSV2INS:'.$strSQL.print_r($params,true));
    if($stmt === false ){
        $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg());
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg());
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】FDB2CSV2
function delFDB2CSV2($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM FDB2CSV2 ';
    $strSQL .=  '    WHERE D2NAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【挿入】FDB2CSV5
function insFDB2CSV5($db2con,$fdb2csv5){
    $data   = array();
    foreach($fdb2csv5 as $fdb2csv5data){
        $strSQL = '';
       
        $strSQL .= ' INSERT INTO FDB2CSV5 ';
        $strSQL .= '        ( ';
        $strSQL .= '            D5NAME, ';
        $strSQL .= '            D5ASEQ, ';
        $strSQL .= '            D5FLD, ';
        $strSQL .= '            D5HED, ';
        $strSQL .= '            D5CSEQ, ';
        $strSQL .= '            D5WEDT, ';
        $strSQL .= '            D5TYPE, ';
        $strSQL .= '            D5LEN, ';
        $strSQL .= '            D5DEC, ';
        $strSQL .= '            D5EXP, ';
        $strSQL .= '            D5EXPD, ';
        $strSQL .= '            D5RSEQ, ';
        $strSQL .= '            D5RSTP ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,?,?,?,?,?,?';
        $strSQL .= '        ) ';
        
        $params =array(
                    $fdb2csv5data['D5NAME'],
                    $fdb2csv5data['D5ASEQ'],
                    $fdb2csv5data['D5FLD'],
                    $fdb2csv5data['D5HED'],
                    $fdb2csv5data['D5CSEQ'],
                    $fdb2csv5data['D5WEDT'],
                    $fdb2csv5data['D5TYPE'],
                    $fdb2csv5data['D5LEN'],
                    $fdb2csv5data['D5DEC'],
                    $fdb2csv5data['D5EXP'],
                    $fdb2csv5data['D5EXPD'],
                    $fdb2csv5data['D5RSEQ'],
                    $fdb2csv5data['D5RSTP']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            //e_log("insFDB2CSV5 MSM => ".$strSQL.print_r($params,true));
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
/* 【挿入】FDB2CSV5
*newLibはもらったクエリーを保存するため、使うライブラリー
**/
function insFDB2CSV5_OTHER($db2con,$newLib,$D1NAME){
    $data   = array();
    $strSQL = '';       
    $strSQL     .=  ' INSERT INTO '.$newLib.'.'.'FDB2CSV5 ';
    $strSQL     .=  ' ( SELECT * FROM FDB2CSV5 ';
    $strSQL     .=  ' WHERE D5NAME=? )';
    $params=array($D1NAME);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg());
    }else{
        //e_log("insFDB2CSV5_OTHER MSM => ".$strSQL.print_r($params,true));
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg());
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}

// 【更新】FDB2CSV5
function delFDB2CSV5($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM FDB2CSV5 ';
    $strSQL .=  '    WHERE D5NAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    //e_log("delFDB2CSV5 MYTLY => ".$strSQL.print_r($params,true));
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}

// 【更新】FDB2CSV2
function updFDB2CSV2($db2con,$updFdb2csv2){
    $data   = array();
    foreach($updFdb2csv2 as $fdb2csv2data){
        $strSQL = '';
       
        $strSQL .= ' UPDATE FDB2CSV2 ';
        $strSQL .= ' SET D2GSEQ = ?, ';
        $strSQL .= '     D2GMES = ? ';
        $strSQL .= ' WHERE ';
        $strSQL .= ' D2NAME = ? ';
        $strSQL .= ' AND D2FILID = ? ';
        $strSQL .= ' AND D2FLD = ? ';
        $params =array(
                    $fdb2csv2data['D2GSEQ'],
                    $fdb2csv2data['D2GMES'],
                    $fdb2csv2data['D2NAME'],
                    $fdb2csv2data['D2FILID'],
                    $fdb2csv2data['D2FLD']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_UPD',
                        'errcd'  => 'UPD FDB2CSV2:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;


}
// 【更新】FDB2CSV5
function updFDB2CSV5($db2con,$updFdb2csv5){
    $data   = array();
    foreach($updFdb2csv5 as $fdb2csv5data){
        $strSQL = '';
       
        $strSQL .= ' UPDATE FDB2CSV5 ';
        $strSQL .= ' SET D5GSEQ = ?, ';
        $strSQL .= '     D5GMES = ? ';
        $strSQL .= ' WHERE ';
        $strSQL .= ' D5NAME = ? ';
        $strSQL .= ' AND D5ASEQ = ? ';
        $strSQL .= ' AND D5FLD = ? ';
        $params =array(
                    $fdb2csv5data['D5GSEQ'],
                    $fdb2csv5data['D5GMES'],
                    $fdb2csv5data['D5NAME'],
                    $fdb2csv5data['D5ASEQ'],
                    $fdb2csv5data['D5FLD']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_UPD',
                        'errcd'  => 'UPD FDB2CSV5:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV5:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;


}
// 【挿入】BSUMFLD
function insBSUMFLD($db2con,$bsumfld){
    $data   = array();
    foreach($bsumfld as $bsumflddata){
        $strSQL = '';
        $strSQL .= ' INSERT INTO BSUMFLD ';
        $strSQL .= '        ( ';
        $strSQL .= '            SFQRYN, ';
        $strSQL .= '            SFFILID, ';
        $strSQL .= '            SFFLDNM, ';
        $strSQL .= '            SFSEQ, ';
        $strSQL .= '            SFGMES ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,? ';
        $strSQL .= '        ) ';
        
        $params =array(
                    $bsumflddata['SFQRYN'],
                    $bsumflddata['SFFILID'],
                    $bsumflddata['SFFLDNM'],
                    $bsumflddata['SFSEQ'],
                    $bsumflddata['SFGMES']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BSUMFLD:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BSUMFLD:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
/* 【挿入】BSUMFLD
*newLibはもらったクエリーを保存するため、使うライブラリー
**/
function insBSUMFLD_OTHER($db2con,$newLib,$D1NAME){
    $data   = array();
    $strSQL = '';
    $strSQL .= ' INSERT INTO '.$newLib.'.'.' BSUMFLD ';
    $strSQL .= ' ( SELECT * FROM  BSUMFLD ';
    $strSQL .= ' WHERE SFQRYN=? )';
    $params=array($D1NAME);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array('result' => 'FAIL_INS','errcd'  => 'BSUMFLD:'.db2_stmt_errormsg());
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BSUMFLD:'.db2_stmt_errormsg());
        }else{
            $data = array('result' => true);
        }
    }
    return $data;

}
// 【削除】BSUMFLD
function delBSUMFLD($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BSUMFLD ';
    $strSQL .=  '    WHERE SFQRYN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BSUMFLD:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BSUMFLD:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【挿入】BQRYCND BQRYDAT
function insBQRYCNDDAT($db2con,$bqrycndData){
    error_log("PPNData++++".print_r($bqrycndData,true));
    $result = 0;
    foreach($bqrycndData as $bqrycnd){
        $strSQL = '';      
        $strSQL .= ' INSERT INTO BQRYCND ';
        $strSQL .= '        ( ';
        $strSQL .= '            CNQRYN, ';
        $strSQL .= '            CNFILID, ';
        $strSQL .= '            CNMSEQ, ';
        $strSQL .= '            CNSSEQ, ';
        $strSQL .= '            CNAOKB, ';
        $strSQL .= '            CNFLDN, ';
        $strSQL .= '            CNCKBN, ';
        $strSQL .= '            CNSTKB ';
        $strSQL .= '        ) ';
        $strSQL .= ' VALUES ( ';
        $strSQL .= '            ?,?,?,?,?,?,?,?';
        $strSQL .= '        ) ';
        
        $params =array(
                    $bqrycnd['CNQRYN'],
                    $bqrycnd['CNFILID'],
                    $bqrycnd['FMSEQ'],
                    $bqrycnd['FSSEQ'],
                    $bqrycnd['CNAOKB'],
                    $bqrycnd['CNFLDN'],
                    $bqrycnd['CNCKBN'],
                    $bqrycnd['CNSTKB']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                    );
            $result = 1;
            break;
        }else{            
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                    );
                $result = 1;
                break;
            }else{
                $result = 0;
                $data = array('result' => true);
                if(count($bqrycnd['BCNDDAT'])>0){
                    foreach($bqrycnd['BCNDDAT'] as $bcnddat){
                        $strSQL = '';
                        $data   = array();
                        $strSQL .= ' INSERT INTO BCNDDAT ';
                        $strSQL .= '        ( ';
                        $strSQL .= '            CDQRYN, ';
                        $strSQL .= '            CDFILID, ';
                        $strSQL .= '            CDMSEQ, ';
                        $strSQL .= '            CDSSEQ, ';
                        $strSQL .= '            CDCDCD, ';
                        $strSQL .= '            CDDAT, ';
                        $strSQL .= '            CDBCNT, ';
                        $strSQL .= '            CDFFLG ';
                        $strSQL .= '        ) ';
                        $strSQL .= ' VALUES ( ';
                        $strSQL .= '            ?,?,?,?,?,?,?,?';
                        $strSQL .= '        ) ';

                        $params =array(
                            $bcnddat['CDQRYN'],
                            $bcnddat['CDFILID'],
                            $bcnddat['CDMSEQ'],
                            $bcnddat['CDSSEQ'],
                            $bcnddat['CDCDCD'],
                            $bcnddat['CDDAT'],
                            $bcnddat['CDBCNT'],
                            $bcnddat['CDFFLG']
                        );
                        $stmt = db2_prepare($db2con,$strSQL);
                        if($stmt === false ){
                            $data = array(
                                        'result' => 'FAIL_INS',
                                        'errcd'  => 'BCNDDAT:'.db2_stmt_errormsg()
                                    );
                            $result = 1;
                            break;
                        }else{
                            $res = db2_execute($stmt,$params);
                            if($res === false){
                                $data = array(
                                        'result' => 'FAIL_INS',
                                        'errcd'  => 'BCNDDAT:'.db2_stmt_errormsg()
                                    );
                                $result = 1;
                                break;
                            }else{
                                $result = 0;
                                $data = array('result' => true);
                            }
                        }
                    }
                }
                
                if($result === 1){
                    break;
                }
            }
        }
    }
    return $data;
}
/* 【挿入】BQRYCND BQRYDAT
*newLibはもらったクエリーを保存するため、使うライブラリー
**/
function insBQRYCNDDAT_OTHER($db2con,$newLib,$D1NAME){
    $result = 0;
    $strSQL = '';
    $strSQL     .=   ' INSERT INTO '.$newLib.'.'.' BQRYCND ';
    $strSQL     .=   ' (SELECT * FROM  BQRYCND ';
    $strSQL     .=   ' WHERE CNQRYN=? )';
    $params=array($D1NAME);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array('result' => 'FAIL_INS','errcd'  => 'BQRYCND:'.db2_stmt_errormsg());
        $result = 1;
    }else{            
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BQRYCND:'.db2_stmt_errormsg());
            $result = 1;
        }else{
            $result = 0;
            $data = array('result' => true);
            $strSQL = '';
            $data   = array();
            $strSQL     .= ' INSERT INTO '.$newLib.'.'.'BCNDDAT ';
            $strSQL     .= ' (SELECT * FROM BCNDDAT ';
            $strSQL     .=' WHERE CDQRYN=? ) ';
            $params=array($D1NAME);
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false ){
                $data = array('result' => 'FAIL_INS','errcd'  => 'BCNDDAT:'.db2_stmt_errormsg());
                $result = 1;
            }else{
                $res = db2_execute($stmt,$params);
                if($res === false){
                    $data = array('result' => 'FAIL_INS','errcd'  => 'BCNDDAT:'.db2_stmt_errormsg());
                    $result = 1;
                }else{
                    $result = 0;
                    $data = array('result' => true);
                }
            }
       }
    }
    return $data;
}


function getCanData($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .= '   SELECT A.CNQRYN ';
    $strSQL .= '        , A.CNFILID ';
    $strSQL .= '        , A.CNMSEQ ';
    $strSQL .= '        , A.CNSSEQ ';
    $strSQL .= '        , A.CNFLDN ';
    $strSQL .= '        , A.CNCANL ';
    $strSQL .= '        , A.CNCANF ';
    $strSQL .= '        , A.CNCMBR ';
    $strSQL .= '        , A.CNCANC ';
    $strSQL .= '        , A.CNCANG ';
    $strSQL .= '        , A.CNNAMG ';
    $strSQL .= '        , A.CNNAMC ';
    $strSQL .= '        , A.CNDFMT ';
    $strSQL .= '        , A.CNDSFL ';
    $strSQL .= '        , A.CNDFIN ';
    $strSQL .= '        , A.CNDFPM ';
    $strSQL .= '        , A.CNDFDY ';
    $strSQL .= '        , A.CNDFFL ';
    $strSQL .= '        , A.CNDTIN ';
    $strSQL .= '        , A.CNDTPM ';
    $strSQL .= '        , A.CNDTDY ';
    $strSQL .= '        , A.CNDTFL ';
    $strSQL .= '        , A.CNCAST ';
    $strSQL .= '        , A.CNNAST ';
    $strSQL .= '   FROM  BQRYCND A ';
    $strSQL .= '   WHERE A.CNQRYN = ? ';
    $strSQL .= '   AND   ( A.CNCANL <> \'\' ';
    $strSQL .= '   AND     A.CNCANF <> \'\' ) ';
    $strSQL .= '   OR    ( A.CNDFMT <> \'\' ) ';

    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

// 検索条件のヘルプ情報取得
function getDB2HCND($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .= '   SELECT B.DHNAME ';
    $strSQL .= '        , A.CNFLDN ';
    $strSQL .= '        , B.DHFILID ';
    $strSQL .= '        , B.DHMSEQ ';
    $strSQL .= '        , B.DHSSEQ ';
    $strSQL .= '        , B.DHINFO ';
    $strSQL .= '        , B.DHINFG ';
    $strSQL .= '   FROM  BQRYCND A ';
    $strSQL .= '   LEFT JOIN DB2HCND B ';
    $strSQL .=  '    ON A.CNQRYN = B.DHNAME ';
    $strSQL .=  '   AND A.CNFILID  = B.DHFILID ';
    $strSQL .=  '   AND A.CNMSEQ = B.DHMSEQ ';
    $strSQL .=  '   AND A.CNSSEQ = B.DHSSEQ ';
    $strSQL .= '   WHERE B.DHNAME = ? ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/*// 詳細設定対象フィールドチェック
function getNotShosaiFld($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT DTNAME ';
    $strSQL .=  '        , DTFILID ';
    $strSQL .=  '        , DTFLD ';
    $strSQL .=  '   FROM DB2WDTL ';
    $strSQL .=  '   WHERE DTNAME = ? ';
    $strSQL .=  '   EXCEPT ';
    $strSQL .=  '   SELECT A.D2NAME  DTNAME ';
    $strSQL .=  '        ,  A.D2FILID DTFILID ';
    $strSQL .=  '        ,  A.D2FLD   DTFLD ';  
    $strSQL .=  '   FROM ( ';        
    $strSQL .=  '           (SELECT D2NAME '; 
    $strSQL .=  '                 , D2FILID '; 
    $strSQL .=  '                 , D2FLD '; 
    $strSQL .=  '           FROM  FDB2CSV2 ';  
    $strSQL .=  '           WHERE D2NAME = ? '; 
    $strSQL .=  '           AND   D2CSEQ > 0) '; 
    $strSQL .=  '           UNION ALL ';  
    $strSQL .=  '           (SELECT  D5NAME AS D2NAME '; 
    $strSQL .=  '               , 9999 AS D2FILID '; 
    $strSQL .=  '               , D5FLD AS D2FLD '; 
    $strSQL .=  '           FROM  FDB2CSV5 ';  
    $strSQL .=  '           WHERE D5NAME = ? '; 
    $strSQL .=  '           AND   D5CSEQ > 0 ) '; 
    $strSQL .=  '       )  A  ';

    $params = array($QRYNM,$QRYNM,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
// 詳細設定対象フィールドチェック
function getNotDrilldownFld($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT DRKMTN ';
    $strSQL .=  '        , DRKFLID ';
    $strSQL .=  '        , DRKFID ';
    $strSQL .=  '   FROM DB2DRGS ';
    $strSQL .=  '   WHERE DRKMTN = ? ';
    $strSQL .=  '   EXCEPT ';
    $strSQL .=  '   SELECT A.D2NAME  DTNAME ';
    $strSQL .=  '        ,  A.D2FILID DTFILID ';
    $strSQL .=  '        ,  A.D2FLD   DTFLD ';  
    $strSQL .=  '   FROM ( ';        
    $strSQL .=  '           (SELECT D2NAME '; 
    $strSQL .=  '                 , D2FILID '; 
    $strSQL .=  '                 , D2FLD '; 
    $strSQL .=  '           FROM  FDB2CSV2 ';  
    $strSQL .=  '           WHERE D2NAME = ? '; 
    $strSQL .=  '           AND   D2CSEQ > 0) '; 
    $strSQL .=  '           UNION ALL ';  
    $strSQL .=  '           (SELECT  D5NAME AS D2NAME '; 
    $strSQL .=  '               , 9999 AS D2FILID '; 
    $strSQL .=  '               , D5FLD AS D2FLD '; 
    $strSQL .=  '           FROM  FDB2CSV5 ';  
    $strSQL .=  '           WHERE D5NAME = ? '; 
    $strSQL .=  '           AND   D5CSEQ > 0 ) '; 
    $strSQL .=  '       )  A  ';

    $params = array($QRYNM,$QRYNM,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}*/
function delDB2DRGSByFld ($db2con,$NEWQRYNM,$drilldowndata,$newLib=''){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    if($newLib!==''){
        $strSQL .=  '     FROM  '.$newLib.'.DB2DRGS ';
    }else{
        $strSQL .=  '     FROM  DB2DRGS ';
    }
    $strSQL .=  '    WHERE  DRKMTN = ? ';
    $strSQL .=  '      AND  DRKFID = ? ';
    $strSQL .=  '      AND  DRKFLID = ? ';
    //e_log('delDB2DRGSByFld=>'.$newLib.'=>'.$strSQL);
    $params =array(
                 $NEWQRYNM
                ,$drilldowndata['DRKFID']
                ,$drilldowndata['DRKFLID']
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2DRGS:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】BQRYCND
function delBQRYCND($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BQRYCND ';
    $strSQL .=  '    WHERE CNQRYN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】BCNDDAT
function delBCNDDAT($db2con,$QRYNM){

    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BCNDDAT ';
    $strSQL .=  '    WHERE CDQRYN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BCNDDAT:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BCNDDAT:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2HCND
function delDB2HCND($db2con,$QRYNM){

    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2HCND ';
    $strSQL .=  '    WHERE DHNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2HCND:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2HCND:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
function updBQRYCND($db2con,$canInfo){
    error_log("PPN20181105 canInfo for updBQRYCND => ".print_r($canInfo,true));
    $data = array();
    $params = array();
    foreach($canInfo as $candata){
        $strSQL = '';       
        $strSQL .= ' UPDATE BQRYCND ';
        $strSQL .= ' SET CNCANL = ? ';
        $strSQL .= '   , CNCANF = ? ';
        $strSQL .= '   , CNCMBR = ? ';
        $strSQL .= '   , CNCANC = ? ';
        $strSQL .= '   , CNCANG = ? ';
        $strSQL .= '   , CNNAMG = ? ';
        $strSQL .= '   , CNNAMC = ? ';
        $strSQL .= '   , CNDFMT = ? ';
        $strSQL .= '   , CNDSFL = ? ';
        $strSQL .= '   , CNDFIN = ? ';
        $strSQL .= '   , CNDFPM = ? ';
        $strSQL .= '   , CNDFDY = ? ';
        $strSQL .= '   , CNDFFL = ? ';
        $strSQL .= '   , CNDTIN = ? ';
        $strSQL .= '   , CNDTPM = ? ';
        $strSQL .= '   , CNDTDY = ? ';
        $strSQL .= '   , CNDTFL = ? ';
        $strSQL .= '   , CNCAST = ? ';
        $strSQL .= '   , CNNAST = ? ';
        $strSQL .= ' WHERE CNQRYN = ? ';
        $strSQL .= ' AND CNFILID = ? ';
        $strSQL .= ' AND CNMSEQ = ? ';
        $strSQL .= ' AND CNSSEQ = ? ';
        $strSQL .= ' AND CNCKBN <> \'IS\' ';
        $strSQL .= ' AND CNFLDN = ? ';
        $params =array(
                    $candata['CNCANL'],
                    $candata['CNCANF'],
                    $candata['CNCMBR'],
                    $candata['CNCANC'],
                    $candata['CNCANG'],
                    $candata['CNNAMG'],
                    $candata['CNNAMC'],
                    $candata['CNDFMT'],
                    $candata['CNDSFL'],
                    $candata['CNDFIN'],
                    $candata['CNDFPM'],
                    $candata['CNDFDY'],
                    $candata['CNDFFL'],
                    $candata['CNDTIN'],
                    $candata['CNDTPM'],
                    $candata['CNDTDY'],
                    $candata['CNDTFL'],
                    $candata['CNCAST'],
                    $candata['CNNAST'],
                    $candata['CNQRYN'],
                    $candata['CNFILID'],
                    $candata['CNMSEQ'],
                    $candata['CNSSEQ'],
                    $candata['CNFLDN']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_UPD',
                        'errcd'  => 'UPD BQRYCND:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// 検索条件のヘルプ情報更新
function updDB2HCND($db2con,$hcndInfo){
    $params = array();
    foreach($hcndInfo as $hcnddata){
        $strSQL = '';       
        $strSQL .= ' SELECT ';
        $strSQL .= '     * ';
        $strSQL .= ' FROM BQRYCND ';
        $strSQL .= ' WHERE CNQRYN = ? ';
        $strSQL .= ' AND CNFILID = ? ';
        $strSQL .= ' AND CNMSEQ = ? ';
        $strSQL .= ' AND CNSSEQ = ? ';
        $strSQL .= ' AND CNFLDN = ? ';
        $params = array (
                $hcnddata['DHNAME'],
                $hcnddata['DHFILID'],
                $hcnddata['DHMSEQ'],
                $hcnddata['DHSSEQ'],
                $hcnddata['CNFLDN']
            );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                    'result' => 'FAIL_SEL',
                    'errcd'  => 'updDB2HCND:'.db2_stmt_errormsg()
                );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                    'result' => 'FAIL_SEL',
                    'errcd'  => 'updDB2HCND:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array();
                while($row = db2_fetch_assoc($stmt)){
                    $data[]= $row;
                }
                 e_log('SQL文1：'.$strSQL.print_r($params,true).print_r($data,true));
                if( count($data) > 0 ){
                    $strSQL = ' INSERT INTO  ';
                    $strSQL .= '     DB2HCND ( ';
                    $strSQL .= '     DHNAME , ' ;
                    $strSQL .= '     DHFILID , ' ;
                    $strSQL .= '     DHMSEQ , ' ;
                    $strSQL .= '     DHSSEQ , ' ;
                    $strSQL .= '     DHINFO , ' ;
                    $strSQL .= '     DHINFG ' ;
                    $strSQL .= '    ) ';
                    $strSQL .= ' VALUES   ';
                    $strSQL .= ' ( ?,?,?,?,?,? ) ';

                    $params = array(
                        $hcnddata['DHNAME'],
                        $hcnddata['DHFILID'],
                        $hcnddata['DHMSEQ'],
                        $hcnddata['DHSSEQ'],
                        $hcnddata['DHINFO'],
                        $hcnddata['DHINFG']
                    );
                    $stmt = db2_prepare($db2con,$strSQL);
                    e_log('SQL文：'.$strSQL.print_r($params,true));
                    if($stmt === false ){
                        $data = array(
                                    'result' => 'FAIL_UPD',
                                    'errcd'  => 'UPD DB2HCND:'.db2_stmt_errormsg()
                                );
                        break;
                    }else{
                        $res = db2_execute($stmt,$params);
                        if($res === false){
                            $data = array(
                                    'result' => 'FAIL_INS',
                                    'errcd'  => 'DB2HCND:'.db2_stmt_errormsg()
                                );
                            break;
                        }else{
                            $data = array('result' => true);
                        }
                    }
                }else{
                    $data = array('result' => true);
                }
            }
        }
    }
    return $data;
}
// 検索条件のSEQを更新
function updBQRYCNDSEQ($db2con,$bqrycndData){
    error_log("PPN20181105 bqrycndData for updBQRYCNDSEQ".print_r($bqrycndData,true));
    $data = array();
    $params = array();
    foreach($bqrycndData as $bqrycnd){
        $strSQL = '';       
        $strSQL .= ' UPDATE BQRYCND ';
        $strSQL .= ' SET CNMSEQ = ? ';
        $strSQL .= '   , CNSSEQ = ? ';
        $strSQL .= ' WHERE CNQRYN = ? ';
        $strSQL .= ' AND CNFILID = ? ';
        $strSQL .= ' AND CNMSEQ = ? ';
        $strSQL .= ' AND CNSSEQ = ? ';
        $params =array(
                    $bqrycnd['CNMSEQ'],
                    $bqrycnd['CNSSEQ'],
                    $bqrycnd['CNQRYN'],
                    $bqrycnd['CNFILID'],
                    $bqrycnd['FMSEQ'],
                    $bqrycnd['FSSEQ']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_UPD',
                        'errcd'  => 'UPD BQRYCND:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// 【削除】DB2WCOL
function delDB2WCOL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WCOL ';
    $strSQL .=  '    WHERE WCNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WCOL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WCOL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2PCAL
function delDB2PCAL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2PCAL ';
    $strSQL .=  '    WHERE WCNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PCAL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PCAL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2PCOL
function delDB2PCOL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2PCOL ';
    $strSQL .=  '    WHERE WPNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PCOL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PCOL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2PMST
function delDB2PMST($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2PMST ';
    $strSQL .=  '    WHERE PMNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PMST:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PMST:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WDFL
function delDB2WDFL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WDFL ';
    $strSQL .=  '    WHERE DFNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WDTL
function delDB2WDTL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WDTL ';
    $strSQL .=  '    WHERE DTNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDTL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDTL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*function delDB2WDFLBYSEQ($db2con,$QRYNM,$FILID,$FLDNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM  DB2WDFL ';
    $strSQL .=  '    WHERE  DFNAME = ? ';
    $strSQL .=  '      AND  DFFILID = ? ';
    $strSQL .=  '      AND  DFFLD = ? ';

    $params =array(
                 $QRYNM
                ,$FILID
                ,$FLDNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
function delDB2WDTLBYSEQ($db2con,$QRYNM,$FILID,$FLDNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WDTL ';
    $strSQL .=  '    WHERE DTNAME = ? ';
    $strSQL .=  '      AND DTFILID = ? ';
    $strSQL .=  '      AND DTFLD   = ? ';

    $params =array(
                $QRYNM
               ,$FILID
               ,$FLDNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDTL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDTL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}*/
// 【削除】DB2COLM
function delDB2COLM($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2COLM ';
    $strSQL .=  '    WHERE DCNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2COLM:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2COLM:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2COLT
function delDB2COLT($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2COLT ';
    $strSQL .=  '    WHERE DTNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2COLT:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2COLT:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WSCD
function delDB2WSCD($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WSCD ';
    $strSQL .=  '    WHERE WSNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WSCD:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WSCD:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WSOC
function delDB2WSOC($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WSOC ';
    $strSQL .=  '    WHERE SONAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WSOC:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WSOC:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WAUT
function delDB2WAUT($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WAUT ';
    $strSQL .=  '    WHERE WANAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WAUT:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WAUT:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WAUL
function delDB2WAUL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WAUL ';
    $strSQL .=  '    WHERE WLNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WAUL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WAUL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WHIS
function delDB2WHIS($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WHIS ';
    $strSQL .=  '    WHERE WHNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WHIS:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WHIS:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WGDF
function delDB2WGDF($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WGDF ';
    $strSQL .=  '    WHERE WGNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WGDF:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WGDF:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WDEF
function delDB2WDEF($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WDEF ';
    $strSQL .=  '    WHERE WDNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】FDB2CSV1PG
function delFDB2CSV1PG($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM FDB2CSV1PG ';
    $strSQL .=  '    WHERE DGNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】FDB2CSV1PM
function delFDB2CSV1PM($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM FDB2CSV1PM ';
    $strSQL .=  '    WHERE DMNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2HTMLT
function delDB2HTMLT($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2HTMLT ';
    $strSQL .=  '    WHERE HTMLTD = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2HTMLT:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2HTMLT:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
            $templatename = PHP_DIR.'htmltemplate/'.$QRYNM.'.html';
            if (file_exists($templatename)) {
                @unlink($templatename);
            }
        }
    }
    return $data;
}

// 【削除】DE2ECON
function delDB2ECON($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2ECON ';
    $strSQL .=  '   WHERE ECNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2ECON:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2ECON:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2EINS
function delDB2EINS($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2EINS ';
    $strSQL .=  '   WHERE EINAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2EINS:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2EINS:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2JKSK
function delDB2JKSK($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2JKSK ';
    $strSQL .=  '   WHERE JKNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2JKSK:'.db2_stmt_errormsg()
                );
    }else{
        //e_log($strSQL.print_r($params,true));
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2JKSK:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2DRGS
function delDB2DRGS($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2DRGS ';
    $strSQL .=  '   WHERE DRKMTN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2DRGS:'.db2_stmt_errormsg()
                );
    }else{
        //e_log($strSQL.print_r($params,true));
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2DRGS:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*
 * グラフデータ削除
 */
// 【削除】DB2GPC
function delDB2GPC($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2GPC ';
    $strSQL .=  '   WHERE GPCQRY = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'GPCQRY:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'GPCQRY:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2GPK 
function delDB2GPK($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2GPK ';
    $strSQL .=  '   WHERE GPKQRY = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2GPK:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2GPK:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*
 * お気に入りデータ削除
 */
// 【削除】DB2BMK 
function delDB2BMK($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2BMK ';
    $strSQL .=  '   WHERE BMKQID = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2BMK:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2BMK:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/* 【挿入】BSQLDAT
*newLibはもらったクエリーを保存するため、使うライブラリー
**/
function insBSQLDAT_OTHER($db2con,$newLib,$D1NAME){
    $data   = array();
    $strSQL = '';
    $strSQL .= ' INSERT INTO '.$newLib.'.BSQLDAT ';
    $strSQL .= ' SELECT * FROM BSQLDAT WHERE ';
    $strSQL .= ' BSQLNM =?';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array('result' => 'FAIL_INS','errcd'  => 'BSQLDAT:'.db2_stmt_errormsg());
    }else{
        $data = array('result' => true);
        $params =array($D1NAME);
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BSQLDAT:'.db2_stmt_errormsg());
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/* 【挿入】BSQLCND
*newLibはもらったクエリーを保存するため、使うライブラリー
**/
function insBSQLCND_OTHER($db2con,$newLib,$D1NAME){
    $result = 0;
    $strSQL = '';      
    $strSQL .= ' INSERT INTO '.$newLib.'.BSQLCND ';
    $strSQL .= ' SELECT * FROM BSQLCND WHERE CNQRYN=? ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array('result' => 'FAIL_INS','errcd'  => 'BSQLCND:'.db2_stmt_errormsg());
        $result = 1;
    }else{
        $params=array($D1NAME);
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_INS','errcd'  => 'BSQLCND:'.db2_stmt_errormsg());
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}

// 【削除】BSQLDAT
function delBSQLDAT($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BSQLDAT ';
    $strSQL .=  '    WHERE BSQLNM = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BSQLDAT:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BSQLDAT:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】BSQLCND
function delBSQLCND($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BSQLCND ';
    $strSQL .=  '    WHERE CNQRYN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BSQLCND:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BSQLCND:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*【挿入】FDB2CSV4
*newLibはもらったクエリーを保存するため、使うライブラリー
**/
function insFDB2CSV4_OTHER($db2con,$newLib,$D1NAME){
    $result = 0;
    $strSQL = '';      
    $strSQL .= ' INSERT INTO '.$newLib.'.FDB2CSV4 ';
    $strSQL .= ' SELECT * FROM FDB2CSV4 WHERE D4NAME=? ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV4:'.db2_stmt_errormsg());
        $result = 1;
    }else{
        $params=array($D1NAME);
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV4:'.db2_stmt_errormsg());
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*【挿入】FDB2CSV3
*newLibはもらったクエリーを保存するため、使うライブラリー
**/
function insFDB2CSV3_OTHER($db2con,$newLib,$D1NAME){
    $result = 0;
    $strSQL = '';      
    $strSQL .= ' INSERT INTO '.$newLib.'.FDB2CSV3 ';
    $strSQL .= ' SELECT * FROM FDB2CSV3 WHERE D3NAME=? ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV3:'.db2_stmt_errormsg());
        $result = 1;
    }else{
        $params=array($D1NAME);
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array('result' => 'FAIL_INS','errcd'  => 'FDB2CSV3:'.db2_stmt_errormsg());
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
