<?php

/**
 * ===========================================================================================
 * SYSTEM NAME    : PHPQUERY2
 * PROGRAM NAME   : BASE 登録・更新処理
 * PROGRAM ID     : insSQLTableData.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/06/13
 * ===========================================================================================
 **/

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("./insTableData.php");
/*
 * 変数
 */
$rtn = 0;
$msg = '';
// 【挿入】FDB2CSV1
function insFDB2CSV1($db2con,$fdb2csv1){
    $strSQL = '';
    $data   = array();
   
    $strSQL .= ' INSERT INTO FDB2CSV1 ';
    $strSQL .= '        ( ';
    $strSQL .= '            D1NAME, ';
    $strSQL .= '            D1TEXT, ';
    $strSQL .= '            D1CSID, ';
    $strSQL .= '            D1LIBL, ';
    $strSQL .= '            D1WEBF, ';
    $strSQL .= '            D1RDB, ';
    $strSQL .= '            D1CFLG ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?';
    $strSQL .= '        ) ';
    
    $params =array(
                $fdb2csv1['D1NAME'],
                $fdb2csv1['D1TEXT'],
                $fdb2csv1['D1CSID'],
                $fdb2csv1['D1LIBL'],
                $fdb2csv1['D1WEBF'],
                $fdb2csv1['D1RDB'],
                $fdb2csv1['D1CFLG']
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【更新】FDB2CSV1
function updFDB2CSV1($db2con,$fdb2csv1){
    $strSQL = '';
    $data   = array();
   
    $strSQL .= ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= '    D1TEXT      = ? , ';
    $strSQL .= '    D1CSID      = ? , ';
    $strSQL .= '    D1LIBL      = ? , ';
    $strSQL .= '    D1WEBF      = ? , ';
    $strSQL .= '    D1RDB       = ? , ';
    $strSQL .= '    D1CFLG      = ?  ';
    $strSQL .= ' WHERE  ';
    $strSQL .= '    D1NAME      = ? ';
    
    $params =array(
                $fdb2csv1['D1TEXT'],
                $fdb2csv1['D1CSID'],
                $fdb2csv1['D1LIBL'],
                $fdb2csv1['D1WEBF'],
                $fdb2csv1['D1RDB'],
                $fdb2csv1['D1CFLG'],
                $fdb2csv1['D1NAME']
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        e_log('準備エラー:'.$strSQL.print_r($params,true).print_r($data,true));
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【出力ファイルの更新】FDB2CSV1
function updOUTFILEFDB2CSV1($db2con,$fdb2csv1){
    $strSQL = '';
    $data   = array();
   
    $strSQL .= ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= '    D1OUTJ      = ? , ';
    $strSQL .= '    D1OUTA      = ? , ';
    $strSQL .= '    D1OUTLIB    = ? , ';
    $strSQL .= '    D1OUTFIL    = ? , ';
    $strSQL .= '    D1OUTR      = ?   ';
    $strSQL .= ' WHERE  ';
    $strSQL .= '    D1NAME      = ? ';
    
    $params =array(
                $fdb2csv1['D1OUTJ'],
                $fdb2csv1['D1OUTA'],
                strtoupper($fdb2csv1['D1OUTLIB']),
                strtoupper($fdb2csv1['D1OUTFIL']),
                $fdb2csv1['D1OUTR'],
                $fdb2csv1['D1NAME']
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        e_log($strSQL.print_r($params,true));
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】FDB2CSV1
function delFDB2CSV1($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM FDB2CSV1 ';
    $strSQL .=  '    WHERE D1NAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
            $templatename = ETEMP_DIR.$QRYNM.'.*';
            if (file_exists($templatename)) {
                @unlink($templatename);
            }
        }
    }
    return $data;
}
// 【挿入】BSQLDAT
function insBSQLDAT($db2con,$bsqldat){
    $data   = array();
    $strSQL = '';
    $strSQL .= ' INSERT INTO BSQLDAT ';
    $strSQL .= '        ( ';
    $strSQL .= '            BSQLNM, ';
    $strSQL .= '            BSQLFLG, ';
    $strSQL .= '            BEXESQL  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?';
    $strSQL .= '        ) ';
    $stmt = db2_prepare($db2con,$strSQL);
   
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'BSQLDAT:'.db2_stmt_errormsg()
                );
    }else{

        $data = array('result' => true);
        foreach($bsqldat as $bsqldatdata){
            $params =array(
                $bsqldatdata['BSQLNM'],
                $bsqldatdata['BSQLFLG'],
                $bsqldatdata['BEXESQL']
            );
           //e_log('SQL文：'.$strSQL.'parameter:'.print_r($params,true));

            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BSQLDAT:'.db2_stmt_errormsg()
                    );
                e_log('error:'.print_r($data,true).'SQL文：'.$strSQL.'parameter:'.print_r($params,true));
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}

// 【挿入】FDB2CSV2
function insFDB2CSV2($db2con,$fdb2csv2){
    $data   = array();
    $strSQL = '';
    $strSQL .= ' INSERT INTO FDB2CSV2 ';
    $strSQL .= '        ( ';
    $strSQL .= '            D2NAME, ';
    $strSQL .= '            D2FILID, ';
    $strSQL .= '            D2FLD, ';
    $strSQL .= '            D2HED, ';
    $strSQL .= '            D2CSEQ, ';
    $strSQL .= '            D2WEDT, ';
    $strSQL .= '            D2TYPE, ';
    $strSQL .= '            D2LEN, ';
    $strSQL .= '            D2DEC ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,? ';
    $strSQL .= '        ) ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                );
    }else{
        $data = array('result' => true);
        foreach($fdb2csv2 as $fdb2csv2data){
            $params =array(
                $fdb2csv2data['D2NAME'],
                $fdb2csv2data['D2FILID'],
                $fdb2csv2data['D2FLD'],
                $fdb2csv2data['D2HED'],
                $fdb2csv2data['D2CSEQ'],
                $fdb2csv2data['D2WEDT'],
                $fdb2csv2data['D2TYPE'],
                $fdb2csv2data['D2LEN'],
                $fdb2csv2data['D2DEC']
            );
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                    );
                e_log('error:'.print_r($data,true).'SQL文：'.$strSQL.'parameter:'.print_r($params,true));
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// 【削除】FDB2CSV2
function delFDB2CSV2($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM FDB2CSV2 ';
    $strSQL .=  '    WHERE D2NAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV2:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}

// 【削除】BSQLDAT
function delBSQLDAT($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BSQLDAT ';
    $strSQL .=  '    WHERE BSQLNM = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BSQLDAT:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BSQLDAT:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}

// 【挿入】BSQLCND
function insBSQLCND($db2con,$bsqlcnd){
    $result = 0;
    $strSQL = '';      
    $strSQL .= ' INSERT INTO BSQLCND ';
    $strSQL .= '        ( ';
    $strSQL .= '            CNQRYN, ';
    $strSQL .= '            CNDSEQ, ';
    $strSQL .= '            CNDKBN, ';
    $strSQL .= '            CNDNM, ';
    $strSQL .= '            CNDWNM, ';
    $strSQL .= '            CNDDTYP, ';
    $strSQL .= '            CNDSTKB, ';
    $strSQL .= '            CNDDAT, ';
    $strSQL .= '            CNDBCNT, ';
    $strSQL .= '            CNCKBN ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?,?,?,?,?,?,?';
    $strSQL .= '        ) ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_INS',
                    'errcd'  => 'BSQLCND:'.db2_stmt_errormsg()
                );
        $result = 1;
    }else{
        $data = array('result' => true);
        foreach($bsqlcnd as $bsqlcnddata){
            $params =array(
                $bsqlcnddata['CNQRYN'],
                $bsqlcnddata['CNDSEQ'],
                $bsqlcnddata['CNDKBN'],
                $bsqlcnddata['CNDNM'],
                $bsqlcnddata['CNDWNM'],
                $bsqlcnddata['CNDDTYP'],
                $bsqlcnddata['CNDSTKB'],
                $bsqlcnddata['CNDDAT'],
                $bsqlcnddata['CNDBCNT'],
                $bsqlcnddata['CNCKBN']
            );
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BSQLCND:'.db2_stmt_errormsg()
                    );
                e_log('error:'.print_r($data,true).'SQL文：'.$strSQL.'parameter:'.print_r($params,true));
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// 【削除】BSQLCND
function delBSQLCND($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BSQLCND ';
    $strSQL .=  '    WHERE CNQRYN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BSQLCND:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BSQLCND:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
function getCanData($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .= '   SELECT A.CNQRYN , ';
    $strSQL .= '         A.CNDSEQ , ';
    $strSQL .= '         A.CNDNM , ';
    $strSQL .= '         A.CNDDTYP , ';
    $strSQL .= '         A.CNDSTKB , ';
    $strSQL .= '         A.CNDDAT , ';
    $strSQL .= '         A.CNDBCNT , ';
    $strSQL .= '         A.CNCANL , ';
    $strSQL .= '         A.CNCMBR , ';
    $strSQL .= '         A.CNCANF , ';
    $strSQL .= '         A.CNCANC , ';
    $strSQL .= '         A.CNCANG , ';
    $strSQL .= '         A.CNNAMG , ';
    $strSQL .= '         A.CNNAMC , ';
    $strSQL .= '         A.CNDFMT , ';
    $strSQL .= '         A.CNDSFL , ';
    $strSQL .= '         A.CNDFIN , ';
    $strSQL .= '         A.CNDFPM , ';
    $strSQL .= '         A.CNDFDY , ';
    $strSQL .= '         A.CNDFFL , ';
    $strSQL .= '         A.CNDTIN , ';
    $strSQL .= '         A.CNDTPM , ';
    $strSQL .= '         A.CNDTDY , ';
    $strSQL .= '         A.CNDTFL , ';
    $strSQL .= '         A.CNCAST , ';
    $strSQL .= '         A.CNNAST , ';
    $strSQL .= '         A.CNDSCH ';
    $strSQL .= '   FROM  BSQLCND A ';
    $strSQL .= '   WHERE A.CNQRYN = ? ';
    $strSQL .= '   AND   (( A.CNCANL <> \'\' ';
    $strSQL .= '   AND     A.CNCANF <> \'\' ) ';
    $strSQL .= '   OR    ( A.CNDFMT <> \'\' )) ';

    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log($strSQL.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

// 検索条件のヘルプ情報取得
function getDB2HCND($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .= '   SELECT B.DHNAME ';
    $strSQL .= '        , A.CNDSEQ ';
    $strSQL .= '        , B.DHFILID ';
    $strSQL .= '        , B.DHMSEQ ';
    $strSQL .= '        , B.DHSSEQ ';
    $strSQL .= '        , B.DHINFO ';
    $strSQL .= '        , B.DHINFG ';
    $strSQL .= '   FROM  BSQLCND A ';
    $strSQL .= '   LEFT JOIN DB2HCND B ';
    $strSQL .=  '    ON A.CNQRYN = B.DHNAME ';
    $strSQL .=  '   AND A.CNDSEQ  = B.DHMSEQ ';
    $strSQL .= '   WHERE B.DHNAME = ? ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function getDB2HCND_BK($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .= '   SELECT B.DHNAME ';
    $strSQL .= '        , A.CNFLDN ';
    $strSQL .= '        , B.DHFILID ';
    $strSQL .= '        , B.DHMSEQ ';
    $strSQL .= '        , B.DHSSEQ ';
    $strSQL .= '        , B.DHINFO ';
    $strSQL .= '        , B.DHINFG ';
//    $strSQL .= '   FROM  BQRYCND A ';
    $strSQL .= '   FROM  BSQLCND A ';
    $strSQL .= '   LEFT JOIN DB2HCND B ';
    $strSQL .=  '    ON A.CNQRYN = B.DHNAME ';
    $strSQL .=  '   AND A.CNFILID  = B.DHFILID ';
    $strSQL .=  '   AND A.CNMSEQ = B.DHMSEQ ';
    $strSQL .=  '   AND A.CNSSEQ = B.DHSSEQ ';
    $strSQL .= '   WHERE B.DHNAME = ? ';
    $params = array($QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function delDB2DRGSByFld ($db2con,$NEWQRYNM,$drilldowndata){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM  DB2DRGS ';
    $strSQL .=  '    WHERE  DRKMTN = ? ';
    $strSQL .=  '      AND  DRKFID = ? ';
    $strSQL .=  '      AND  DRKFLID = ? ';

    $params =array(
                 $NEWQRYNM
                ,$drilldowndata['DRKFID']
                ,$drilldowndata['DRKFLID']
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2DRGS:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】BQRYCND
function delBQRYCND($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BQRYCND ';
    $strSQL .=  '    WHERE CNQRYN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】BCNDDAT
function delBCNDDAT($db2con,$QRYNM){

    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM BCNDDAT ';
    $strSQL .=  '    WHERE CDQRYN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BCNDDAT:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'BCNDDAT:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2HCND
function delDB2HCND($db2con,$QRYNM){

    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2HCND ';
    $strSQL .=  '    WHERE DHNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2HCND:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2HCND:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
function updBSQLCND($db2con,$canInfo){
    $data = array();
    $params = array();
    foreach($canInfo as $candata){
        $strSQL = '';       
        $strSQL .= ' UPDATE BSQLCND ';
        $strSQL .= ' SET CNCANL = ? ';
        $strSQL .= '   , CNCANF = ? ';
        $strSQL .= '   , CNCMBR = ? ';
        $strSQL .= '   , CNCANC = ? ';
        $strSQL .= '   , CNCANG = ? ';
        $strSQL .= '   , CNNAMG = ? ';
        $strSQL .= '   , CNNAMC = ? ';
        $strSQL .= '   , CNDFMT = ? ';
        $strSQL .= '   , CNDSFL = ? ';
        $strSQL .= '   , CNDFIN = ? ';
        $strSQL .= '   , CNDFPM = ? ';
        $strSQL .= '   , CNDFDY = ? ';
        $strSQL .= '   , CNDFFL = ? ';
        $strSQL .= '   , CNDTIN = ? ';
        $strSQL .= '   , CNDTPM = ? ';
        $strSQL .= '   , CNDTDY = ? ';
        $strSQL .= '   , CNDTFL = ? ';
        $strSQL .= '   , CNCAST = ? ';
        $strSQL .= '   , CNNAST = ? ';
        $strSQL .= ' WHERE CNQRYN = ? ';
        $strSQL .= ' AND CNDSEQ = ? ';
        $params =array(
                    $candata['CNCANL'],
                    $candata['CNCANF'],
                    $candata['CNCMBR'],
                    $candata['CNCANC'],
                    $candata['CNCANG'],
                    $candata['CNNAMG'],
                    $candata['CNNAMC'],
                    $candata['CNDFMT'],
                    $candata['CNDSFL'],
                    $candata['CNDFIN'],
                    $candata['CNDFPM'],
                    $candata['CNDFDY'],
                    $candata['CNDFFL'],
                    $candata['CNDTIN'],
                    $candata['CNDTPM'],
                    $candata['CNDTDY'],
                    $candata['CNDTFL'],
                    $candata['CNCAST'],
                    $candata['CNNAST'],
                    $candata['CNQRYN'],
                    $candata['CNDSEQ']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_UPD',
                        'errcd'  => 'UPD BSQLCND:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BSQLCND:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// 検索条件のヘルプ情報更新
function updDB2HCND($db2con,$hcndInfo){
    $params = array();
    foreach($hcndInfo as $hcnddata){
        $strSQL = '';       
        $strSQL .= ' SELECT ';
        $strSQL .= '     * ';
        $strSQL .= ' FROM BSQLCND ';
        $strSQL .= ' WHERE CNQRYN = ? ';
        $strSQL .= ' AND CNDSEQ = ? ';
        $params = array (
                $hcnddata['DHNAME'],
                $hcnddata['DHMSEQ'],
            );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                    'result' => 'FAIL_SEL',
                    'errcd'  => 'updDB2HCND:'.db2_stmt_errormsg()
                );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                    'result' => 'FAIL_SEL',
                    'errcd'  => 'updDB2HCND:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array();
                while($row = db2_fetch_assoc($stmt)){
                    $data[]= $row;
                }
                 e_log('SQL文1：'.$strSQL.print_r($params,true).print_r($data,true));
                if( count($data) > 0 ){
                    $strSQL = ' INSERT INTO  ';
                    $strSQL .= '     DB2HCND ( ';
                    $strSQL .= '     DHNAME , ' ;
                    $strSQL .= '     DHFILID , ' ;
                    $strSQL .= '     DHMSEQ , ' ;
                    $strSQL .= '     DHSSEQ , ' ;
                    $strSQL .= '     DHINFO , ' ;
                    $strSQL .= '     DHINFG ' ;
                    $strSQL .= '    ) ';
                    $strSQL .= ' VALUES   ';
                    $strSQL .= ' ( ?,?,?,?,?,? ) ';

                    $params = array(
                        $hcnddata['DHNAME'],
                        $hcnddata['DHFILID'],
                        $hcnddata['DHMSEQ'],
                        $hcnddata['DHSSEQ'],
                        $hcnddata['DHINFO'],
                        $hcnddata['DHINFG']
                    );
                    $stmt = db2_prepare($db2con,$strSQL);
                    e_log('SQL文：'.$strSQL.print_r($params,true));
                    if($stmt === false ){
                        $data = array(
                                    'result' => 'FAIL_UPD',
                                    'errcd'  => 'UPD DB2HCND:'.db2_stmt_errormsg()
                                );
                        break;
                    }else{
                        $res = db2_execute($stmt,$params);
                        if($res === false){
                            $data = array(
                                    'result' => 'FAIL_INS',
                                    'errcd'  => 'DB2HCND:'.db2_stmt_errormsg()
                                );
                            break;
                        }else{
                            $data = array('result' => true);
                        }
                    }
                }else{
                    $data = array('result' => true);
                }
            }
        }
    }
    return $data;
}
// 検索条件のSEQを更新
function updBQRYCNDSEQ($db2con,$bqrycndData){
    //e_log(print_r($bqrycndData,true));
    $data = array();
    $params = array();
    foreach($bqrycndData as $bqrycnd){
        $strSQL = '';       
        $strSQL .= ' UPDATE BQRYCND ';
        $strSQL .= ' SET CNMSEQ = ? ';
        $strSQL .= '   , CNSSEQ = ? ';
        $strSQL .= ' WHERE CNQRYN = ? ';
        $strSQL .= ' AND CNFILID = ? ';
        $strSQL .= ' AND CNMSEQ = ? ';
        $strSQL .= ' AND CNSSEQ = ? ';
        $params =array(
                    $bqrycnd['CNMSEQ'],
                    $bqrycnd['CNSSEQ'],
                    $bqrycnd['CNQRYN'],
                    $bqrycnd['CNFILID'],
                    $bqrycnd['FMSEQ'],
                    $bqrycnd['FSSEQ']
                 );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false ){
            $data = array(
                        'result' => 'FAIL_UPD',
                        'errcd'  => 'UPD BQRYCND:'.db2_stmt_errormsg()
                    );
            break;
        }else{
            $res = db2_execute($stmt,$params);
            if($res === false){
                $data = array(
                        'result' => 'FAIL_INS',
                        'errcd'  => 'BQRYCND:'.db2_stmt_errormsg()
                    );
                break;
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
// 【削除】DB2WCOL
function delDB2WCOL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WCOL ';
    $strSQL .=  '    WHERE WCNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WCOL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WCOL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2PCAL
function delDB2PCAL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2PCAL ';
    $strSQL .=  '    WHERE WCNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PCAL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PCAL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2PCOL
function delDB2PCOL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2PCOL ';
    $strSQL .=  '    WHERE WPNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PCOL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PCOL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2PMST
function delDB2PMST($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2PMST ';
    $strSQL .=  '    WHERE PMNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PMST:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2PMST:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WDFL
function delDB2WDFL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WDFL ';
    $strSQL .=  '    WHERE DFNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WDTL
function delDB2WDTL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WDTL ';
    $strSQL .=  '    WHERE DTNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDTL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDTL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2COLM
function delDB2COLM($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2COLM ';
    $strSQL .=  '    WHERE DCNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2COLM:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2COLM:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2COLT
function delDB2COLT($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2COLT ';
    $strSQL .=  '    WHERE DTNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2COLT:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2COLT:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WSCD
function delDB2WSCD($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WSCD ';
    $strSQL .=  '    WHERE WSNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WSCD:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WSCD:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WSOC
function delDB2WSOC($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WSOC ';
    $strSQL .=  '    WHERE SONAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WSOC:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WSOC:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WAUT
function delDB2WAUT($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WAUT ';
    $strSQL .=  '    WHERE WANAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WAUT:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WAUT:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WAUL
function delDB2WAUL($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WAUL ';
    $strSQL .=  '    WHERE WLNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WAUL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WAUL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WHIS
function delDB2WHIS($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WHIS ';
    $strSQL .=  '    WHERE WHNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WHIS:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WHIS:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WGDF
function delDB2WGDF($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WGDF ';
    $strSQL .=  '    WHERE WGNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WGDF:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WGDF:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2WDEF
function delDB2WDEF($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2WDEF ';
    $strSQL .=  '    WHERE WDNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】FDB2CSV1PG
function delFDB2CSV1PG($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM FDB2CSV1PG ';
    $strSQL .=  '    WHERE DGNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1PG:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】FDB2CSV1PM
function delFDB2CSV1PM($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM FDB2CSV1PM ';
    $strSQL .=  '    WHERE DMNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'FDB2CSV1PM:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2HTMLT
function delDB2HTMLT($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '     FROM DB2HTMLT ';
    $strSQL .=  '    WHERE HTMLTD = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2HTMLT:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2HTMLT:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
            $templatename = PHP_DIR.'htmltemplate/'.$QRYNM.'.html';
            if (file_exists($templatename)) {
                @unlink($templatename);
            }
        }
    }
    return $data;
}

// 【削除】DE2ECON
function delDB2ECON($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2ECON ';
    $strSQL .=  '   WHERE ECNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2ECON:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2ECON:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2EINS
function delDB2EINS($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2EINS ';
    $strSQL .=  '   WHERE EINAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2EINS:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2EINS:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2JKSK
function delDB2JKSK($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2JKSK ';
    $strSQL .=  '   WHERE JKNAME = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2JKSK:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2JKSK:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 【削除】DB2DRGS
function delDB2DRGS($db2con,$QRYNM){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    $strSQL .=  '       FROM DB2DRGS ';
    $strSQL .=  '   WHERE DRKMTN = ? ';

    $params =array(
                $QRYNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2DRGS:'.db2_stmt_errormsg()
                );
    }else{
        e_log($strSQL.print_r($params,true));
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2DRGS:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
