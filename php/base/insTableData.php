<?php

/**
 * ===========================================================================================
 * SYSTEM NAME    : PHPQUERY2
 * PROGRAM NAME   : BASE 登録・更新処理
 * PROGRAM ID     : insTableData.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/06/13
 * ===========================================================================================
 **/

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/

// 詳細設定対象フィールドチェック
function getNotShosaiFld($db2con,$QRYNM,$newLib=''){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT DTNAME ';
    $strSQL .=  '        , DTFILID ';
    $strSQL .=  '        , DTFLD ';
    if($newLib!==''){
        $strSQL .=  '   FROM '.$newLib.'.DB2WDTL ';
    }else{
        $strSQL .=  '   FROM DB2WDTL ';
    }
    $strSQL .=  '   WHERE DTNAME = ? ';
    $strSQL .=  '   EXCEPT ';
    $strSQL .=  '   SELECT A.D2NAME  DTNAME ';
    $strSQL .=  '        ,  A.D2FILID DTFILID ';
    $strSQL .=  '        ,  A.D2FLD   DTFLD ';  
    $strSQL .=  '   FROM ( ';        
    $strSQL .=  '           (SELECT D2NAME '; 
    $strSQL .=  '                 , D2FILID '; 
    $strSQL .=  '                 , D2FLD '; 
    if($newLib!==''){
        $strSQL .=  '           FROM  '.$newLib.'.FDB2CSV2 ';
    }else{
        $strSQL .=  '           FROM  FDB2CSV2 ';
    }
    $strSQL .=  '           WHERE D2NAME = ? '; 
    $strSQL .=  '           AND   D2CSEQ > 0) '; 
    $strSQL .=  '           UNION ALL ';  
    $strSQL .=  '           (SELECT  D5NAME AS D2NAME '; 
    $strSQL .=  '               , 9999 AS D2FILID '; 
    $strSQL .=  '               , D5FLD AS D2FLD '; 
    if($newLib!==''){
        $strSQL .=  '           FROM  '.$newLib.'.FDB2CSV5 ';
    }else{
        $strSQL .=  '           FROM  FDB2CSV5 ';
    }
    $strSQL .=  '           WHERE D5NAME = ? '; 
    $strSQL .=  '           AND   D5CSEQ > 0 ) '; 
    $strSQL .=  '       )  A  ';

    $params = array($QRYNM,$QRYNM,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
// 詳細設定対象フィールドチェック
function getNotDrilldownFld($db2con,$QRYNM,$newLib=''){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL .=  '   SELECT DRKMTN ';
    $strSQL .=  '        , DRKFLID ';
    $strSQL .=  '        , DRKFID ';
    if($newLib!==''){
        $strSQL .=  '   FROM '.$newLib.'.DB2DRGS ';
    }else{
        $strSQL .=  '   FROM DB2DRGS ';
    }
    $strSQL .=  '   WHERE DRKMTN = ? ';
    $strSQL .=  '   EXCEPT ';
    $strSQL .=  '   SELECT A.D2NAME  DTNAME ';
    $strSQL .=  '        ,  A.D2FILID DTFILID ';
    $strSQL .=  '        ,  A.D2FLD   DTFLD ';  
    $strSQL .=  '   FROM ( ';        
    $strSQL .=  '           (SELECT D2NAME '; 
    $strSQL .=  '                 , D2FILID '; 
    $strSQL .=  '                 , D2FLD ';
    if($newLib!==''){
        $strSQL .=  '           FROM  '.$newLib.'.FDB2CSV2 ';
    }else{
        $strSQL .=  '           FROM  FDB2CSV2 ';
    }
    $strSQL .=  '           WHERE D2NAME = ? '; 
    $strSQL .=  '           AND   D2CSEQ > 0) '; 
    $strSQL .=  '           UNION ALL ';  
    $strSQL .=  '           (SELECT  D5NAME AS D2NAME '; 
    $strSQL .=  '               , 9999 AS D2FILID '; 
    $strSQL .=  '               , D5FLD AS D2FLD ';
    if($newLib!==''){
        $strSQL .=  '           FROM  '.$newLib.'.FDB2CSV5 ';
    }else{
        $strSQL .=  '           FROM  FDB2CSV5 ';
    }
    $strSQL .=  '           WHERE D5NAME = ? '; 
    $strSQL .=  '           AND   D5CSEQ > 0 ) '; 
    $strSQL .=  '       )  A  ';

    $params = array($QRYNM,$QRYNM,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
// 詳細のデータ削除【SEQより】
function delDB2WDFLBYSEQ($db2con,$QRYNM,$FILID,$FLDNM,$newLib=''){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    if($newLib!==''){
        $strSQL .=  '     FROM  '.$newLib.'.DB2WDFL ';
    }else{
        $strSQL .=  '     FROM  DB2WDFL ';
    }
    $strSQL .=  '    WHERE  DFNAME = ? ';
    $strSQL .=  '      AND  DFFILID = ? ';
    $strSQL .=  '      AND  DFFLD = ? ';

    $params =array(
                 $QRYNM
                ,$FILID
                ,$FLDNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDFL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// 詳細のデータ削除【SEQより】
function delDB2WDTLBYSEQ($db2con,$QRYNM,$FILID,$FLDNM,$newLib=''){
    $data   = array();
    $strSQL = '';

    $strSQL .=  '   DELETE ';
    if($newLib!==''){
        $strSQL .=  '     FROM '.$newLib.'.DB2WDTL ';
    }else{
        $strSQL .=  '     FROM DB2WDTL ';
    }
    $strSQL .=  '    WHERE DTNAME = ? ';
    $strSQL .=  '      AND DTFILID = ? ';
    $strSQL .=  '      AND DTFLD   = ? ';

    $params =array(
                $QRYNM
               ,$FILID
               ,$FLDNM
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDTL:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_DEL',
                    'errcd'  => 'DB2WDTL:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}