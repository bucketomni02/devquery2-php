<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$_SESSION['PHPQUERYTOUCH']['user'] = $_SESSION['PHPQUERY']['user'];
$_SESSION['PHPQUERYTOUCH']['LOGIN'] = $_SESSION['PHPQUERY']['LOGIN'];
$_SESSION['PHPQUERYTOUCH']['ClFlg'] = $_SESSION['PHPQUERY']['ClFlg'];


$rtn = array(
	'IP' => IP,
	'PROTOCOL' => PROTOCOL,
	'SISTEMTOUCH' => SISTEMTOUCH
);

echo(json_encode($rtn));