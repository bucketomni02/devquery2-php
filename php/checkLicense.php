<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/

include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* ライセンス管理　
*-------------------------------------------------------*
*/
include_once("licenseInfo.php");
/*
*-------------------------------------------------------* 
* 共通変数
*-------------------------------------------------------*
*/
$FileFlg = true;        //ファイルの存在チェック
$DateFlg = true;        //ライセンス期限チェックフラグ
$SerialFlg = true;      //シリアルNoチェックフラグ
$UserFlg = true;        //ユーザー数チェックフラグ
$GroupFlg = true;        //ユーザー数チェックフラグ
$QueryFlg = true;       //クエリー登録数チェックフラグ
$ClFlg = true;          //事前処理権限チェックフラグ
$ScheduleFlg = true;    //スケジュール自動実行権限チェックフラグ
$PivotFlg = true;       //ピボット実行権限チェックフラグ
$GraphFlg = true;       //ピボット実行権限チェックフラグ
$errFlg = true;
$msg = '';                //エラーメッセージ
$beforErr = false;      //ユーザー、グループの登録前チェック
$flag = (isset($_POST['flag'])?$_POST['flag']:'');
$rtn = 0;
/*
*-------------------------------------------------------* 
* ライセンス管理　
*-------------------------------------------------------*
*/
//ログインユーザが削除されたかどうかチェック
if($flag === "1"){
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['"WUAUTH'] === '4'){
            $rtn = 2;
            $msg = showMsg('NOTEXIST',array('ライセンス情報の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'29',$userData[0]['WUSAUT']);//'6' =>  ダウンロード権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg = showMsg($rs['result'],array('ライセンス情報の権限'));
            }
        }
    }
/*    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'29',$userData[0]['WUSAUT']);//'1' => LicenseMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライセンス情報の権限'));
            }
        }
   }*/
     cmDb2Close($db2con);
 }


 


if($rtn === 0){
        if(file_exists('licenseInfo.php')){
            include_once("licenseInfo.php");
        }else{
            $FileFlg = false;
            $errFlg = false;
            if(isset($_SESSION['PHPQUERY'])){
                $_SESSION['PHPQUERY'] = array();
            }
            if(isset($_SESSION['PHPQUERYTOUCH'])){
                $_SESSION['PHPQUERYTOUCH'] = array();
            }
        }

        //ライセンス期限チェック
        /*
        if($licenseDate != ''){
            $nowDate = date('Ymd');

            if($nowDate > $licenseDate){
                $DateFlg = false;
                $errFlg = false;
                $msg = showMsg('FAIL_CHKLICENSE');//'体験デモ版の試用期間が終了しました。継続利用いただく場合は、正規版ライセンスをご購入ください。';
            }
        }
        */
        //シリアルNoチェック
        if($errFlg === true){
            if($licenseSerial != ''){
                $db2con = cmDb2con();
                cmSetPHPQUERY($db2con);
                $getserial = cmGETSRLNBR($db2con);
                if(trim($getserial) != $licenseSerial){
                    $SerialFlg = false;
                    $errFlg = false;
                    $msg = showMsg('FAIL_CHK',array('シリアル番号'));//'シリアル番号が間違っています。';
                    if(isset($_SESSION['PHPQUERY'])){
                        $_SESSION['PHPQUERY'] = array();
                    }
                    if(isset($_SESSION['PHPQUERYTOUCH'])){
                        $_SESSION['PHPQUERYTOUCH'] = array();
                    }
                }
                cmDb2Close($db2con);
            }
        }
        $errChk = '';
        //ユーザー数チェック
        if($errFlg === true){
            if($licenseUser != ''){

                $db2con = cmDb2con();
                cmSetPHPQUERY($db2con);
                $count = fnGetUserCount($db2con,$licenseUser);
                if(isset($_POST['BeforAddUser'])){
                    if($count >= $licenseUser){
                        $UserFlg = false;
                   //     $errFlg = false;
                        $beforErr = true;
                        $msg = showMsg('NO_ADD_BUT',array('ユーザー'));//'ユーザーはこれ以上追加できません。';
                    }
                }else{
                    if($count > $licenseUser){
                        $UserFlg = false;
                         $errChk = 'ユーザー';
                        if($licenseType === 'FREE'){
                            $licenseUser_F = $licenseUser_F.'(現在：'.$count.')';
                        }
                        //$errFlg = false;
                        //$msg = 'ユーザー登録数が制限を超えています。<br/>使えるユーザーは'.$licenseUser.'人までです。';
                    }
                }

                cmDb2Close($db2con);    
            }
        }

        //グループ数チェック
        if($errFlg === true){
            if($licenseGroup != ''){

                $db2con = cmDb2con();
                cmSetPHPQUERY($db2con);
                $count = fnGetGroupCount($db2con);
                if(isset($_POST['BeforAddGroup'])){
                    if($count >= $licenseGroup){
                        $GroupFlg = false;
                     //   $errFlg = false;
                        $beforErr = true;
                        $msg = showMsg('NO_ADD_BUT',array('グループ'));//'グループはこれ以上追加できません。';
                    }
                }else{
                    if($count > $licenseGroup){
                        if($errChk !== ''){
                             $errChk .= '/';
                         }
                         $errChk .= 'グループ';
                        $GroupFlg = false;
                       //$errFlg = false;
                      //$msg = 'グループ登録数が制限を超えています。<br/>使えるグループは'.$licenseGroup.'グループまでです。';
                    }
                }
                cmDb2Close($db2con);
            }
        }

        //定義数チェック
        if($errFlg === true){
            if($licenseQuery != ''){

                $db2con = cmDb2con();
                cmSetPHPQUERY($db2con);
                $count = fnGetQueryCount($db2con);
                if($count > $licenseQuery){
                    $QueryFlg = false;
                    //$errFlg = false;
                        if($errChk !== ''){
                                $errChk .= '/';
                        }
                        $errChk .= 'クエリー';
                        if($licenseType === 'FREE'){
                            $licenseQuery_F = $licenseQuery_F.'(現在：'.$count.')';
                        }
                    //$msg = 'クエリー登録数が制限を超えています。<br/>使えるクエリーは'.$licenseQuery.'個までです。';
                }
                cmDb2Close($db2con);
            }
        }
        if($errFlg === true){
            if($licenseKukaku != ''){

                $db2con = cmDb2con();
                cmSetPHPQUERY($db2con);
                $count = fnGetKukakuCount($db2con);
                if($count > (int)$licenseKukaku){
                    $KukakuFlg = false;
                    //$errFlg = false;
                        if($errChk !== ''){
                                $errChk .= '/';
                        }
                        $errChk .= '区画';
                        if($licenseType === 'FREE'){
                            $licenseKukaku_F = $licenseKukaku_F.'(現在：'.$count.')';
                        }
                    //$msg = 'クエリー登録数が制限を超えています。<br/>使えるクエリーは'.$licenseQuery.'個までです。';
                }
                cmDb2Close($db2con);
            }
        }

        if($errChk !==''){
        //        $errFlg = false;
               $licenseText = showMsg('FAIL_MAXLEN',array($errChk));//$errChk.' が制限を超えています。';
        }

        //事前処理登録権限チェック
        if($licenseCl == false){
            $ClFlg = false;
        }

        //スケジュール自動実行権限チェック
        if($licenseSchedule == false){
            $ScheduleFlg = false;
        }
        //グラフ実行権限チェック
        if($licenseGraph == false){
            $GraphFlg = false;
        }
        //ピボット実行権限チェック
        if($licensePivot == false){
            $PivotFlg = false;
        }
        $flg = 0;
        $rtn = 0;
        $FLG = (isset($_POST['FLG'])) ? $_POST['FLG'] : '';
        if($errFlg !== false){
                    //ピッボト作成権限
                    if($FLG === 'PIVOTCREATE'){
                        if($licensePivotCreate === false){
                            $rtn = 1;
                        }
                    }
                    //クエリーダウロードできるかの権限
                    if($FLG === 'DOWNLOAD'){
                        if($licenseExcelcsvDownload === false){
                            $rtn = 1;
                        }
                    }
                       //スケジュールボタンの使用権限
                    if($FLG === 'SCHBTN'){
                        if($licenseScheduleBtn === false){
                            $rtn = 1;
                        }
                    }

                    if($rtn === 1){
                            $msg = showMsg('FAIL_FREE_LICENSE');//"フリー版ではご利用いただけません。";
                    }
        }else{
                    if($FLG === 'QRYJIKO'){
                            $msg = showMsg('FAIL_FREE_LICENSEOVER');//"フリー版の上限を超えています。</br>ライセンス情報を確認してください。";
                    }else if($FLG === 'SCHBTN' || $FLG === 'DOWNLOAD'){
                            $msg = showMsg('FAIL_FREE_LICENSE');//"フリー版ではご利用いただけません。";
                    }
        }

        //ライセンスエラーがある場合、セッションをクリア
        if($errFlg === false){

            //登録前エラーチェックがtrue(エラーがあった)の場合はスルー
            if($beforErr !== true){
                if(!isset($_POST['BeforAdd'])){
                    if(isset($_SESSION['PHPQUERY'])){
                        $_SESSION['PHPQUERY'] = array();
                    }
                    if(isset($_SESSION['PHPQUERYTOUCH'])){
                        $_SESSION['PHPQUERYTOUCH'] = array();
                    }
                }
            }
        }
        /**
        *フリー日付の修正（２０１６１０１３）
        **/
        if($licenseDate_SF !== '~'){
           $licenseDate_SF = date("Ymd", strtotime("+1 day", strtotime($licenseDate_SF)));
        }
		//$KukakuFlg = $licenseKukaku;
}
$ary = array(
    'licenseTitle' => $licenseTitle,
    'FileFlg' => $FileFlg,
    'DateFlg' => $DateFlg,
    'SerialFlg' => $SerialFlg, 
    'UserFlg' => $UserFlg,
    'GroupFlg'=> $GroupFlg,
	'KukakuFlg' => $KukakuFlg,
    'QueryFlg' => $QueryFlg,
    'ClFlg' => $ClFlg,
    'PivotFlg' => $PivotFlg,
    'GraphFlg' => $GraphFlg,
    'ScheduleFlg'=>$ScheduleFlg,
    'ErrFlg' => $errFlg,
    'MSG' => $msg,
    'licenseText' => $licenseText,
    'licenseDate_S' => $licenseDate_S,
    'licenseDate_E' => $licenseDate_E,
    "licenseType" =>$licenseType,
    "licenseTitle"=> $licenseTitle,
    'licenseUser' =>$licenseUser,//プラン
    'licenseQuery' => $licenseQuery,
    'licenseExcelcsvDownload' => $licenseExcelcsvDownload,
    'licenseSchedule' => $licenseSchedule,
    'licensePivot' => $licensePivot,
    'licenseCl' =>$licenseCl,
    'licenseLog' => $licenseLog,
    'licenseTemplate' => $licenseTemplate,
    'licenseIFS' => $licenseIFS,
    'licenseDrilldown'  => $licenseDrilldown,
    'licenseGraph'  => $licenseGraph,
    'licenseDate_SP' => $licenseDate_SP,
    "licenseDate_SF" => $licenseDate_SF,
    'licenseDate_EP' =>$licenseDate_EP,
    "licenseTitle_F"=> $licenseTitle_F,
    'licenseUser_F' =>$licenseUser_F,//フリー
    'licenseQuery_F' => $licenseQuery_F,
    'licenseKukaku_F' => $licenseKukaku_F,
    'licenseExcelcsvDownload_F' => $licenseExcelcsvDownload_F,
    'licenseSchedule_F' => $licenseSchedule_F,
    'licenseScheduleBtn_F' => $licenseScheduleBtn_F,
    'licensePivot_F' => $licensePivot_F,
    'licenseCl_F' =>$licenseCl_F, 
    'licenseShow_F' => $licenseShow_F,
    'licenseLog_F' => $licenseLog_F,
    'licenseTemplate_F' => $licenseTemplate_F,
    'licenseIFS_F' => $licenseIFS_F,
    'licenseDrilldown_F'  => $licenseDrilldown_F,
    'licenseGraph_F'  => $licenseGraph_F,
    "licenseTitle_T"=> $licenseTitle_T,
    'licenseUser_T' =>$licenseUser_T,//トライアル
    'licenseQuery_T' => $licenseQuery_T,
    'licenseKukaku_T' => $licenseKukaku_T,
    'licenseExcelcsvDownload_T' => $licenseExcelcsvDownload_T,
    'licenseSchedule_T' => $licenseSchedule_T,
    'licenseScheduleBtn_T' => $licenseScheduleBtn_T,
    'licensePivot_T' => $licensePivot_T,
    'licenseCl_T' =>$licenseCl_T, 
    'licenseShow_T' => $licenseShow_T,
    'licenseDate_ST' => $licenseDate_ST,
    'licenseDate_ET' => $licenseDate_ET,
    'licenseLog_T' => $licenseLog_T,
    'licenseTemplate_T' => $licenseTemplate_T,
    'licenseIFS_T' => $licenseIFS_T,
    'licenseDrilldown_T'  => $licenseDrilldown_T,
    'licenseGraph_T'  => $licenseGraph_T,
    "licenseTitle_P"=> $licenseTitle_P,
    'licenseUser_P' =>$licenseUser_P,//プラン
    'licenseQuery_P' => $licenseQuery_P,
    'licenseKukaku_P' => $licenseKukaku_P,
    'licenseExcelcsvDownload_P' => $licenseExcelcsvDownload_P,
    'licenseSchedule_P' => $licenseSchedule_P,
    'licenseScheduleBtn_P' => $licenseScheduleBtn_P,
    'licensePivot_P' => $licensePivot_P,
    'licenseCl_P' =>$licenseCl_P,
    'licenseDate_SP' => $licenseDate_SP,
    'licenseDate_EP' =>$licenseDate_EP,
    'licenseShow_P' => $licenseShow_P,
    'licenseLog_P' => $licenseLog_P,
    'licenseTemplate_P' => $licenseTemplate_P,
    'licenseIFS_P' => $licenseIFS_P,
    'licenseDrilldown_P'  => $licenseDrilldown_P,
    'licenseGraph_P'  => $licenseGraph_P,
    'flag' => $flag,
    'RTN' => $rtn
);

//BASE側ライセンスチェック用
$db2con = cmDb2con();
cmSetPHPQUERY($db2con);
fnPUTLICINF($db2con,$ary);
cmDb2Close($db2con);
echo(json_encode($ary));




/*
*-------------------------------------------------------* 
* ライセンス情報書き込み
*-------------------------------------------------------*
*/
function fnPUTLICINF($db2con, $ary){

    $flg = array();
    $ret = '0';

    $strSQL  = ' UPDATE DB2WLIC SET ';
    $strSQL .= ' WLDATE = ? ' ;
    $strSQL .= ',WLSRLN = ? ' ;
    $strSQL .= ',WLUSER = ? ' ;
    $strSQL .= ',WLQRY  = ? ' ;
    $strSQL .= ',WLCL   = ? ' ;
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false){
        $ret = '1';
    }else{
        if($ary['DateFlg']){$flg[]='1';}else{$flg[]='0';}
        if($ary['SerialFlg']){$flg[]='1';}else{$flg[]='0';}
        if($ary['UserFlg']){$flg[]='1';}else{$flg[]='0';}
        if($ary['QueryFlg']){$flg[]='1';}else{$flg[]='0';}
        if($ary['ClFlg']){$flg[]='1';}else{$flg[]='0';}
        $r = db2_execute($stmt,$flg);
        if($r === false){
            $ret = '1';
        }
    }
    return $ret;

}

/*
*-------------------------------------------------------* 
* ユーザー数取得
*-------------------------------------------------------*
*/
function fnGetUserCount($db2con,$licenseUser){

    $data = array();

    $strSQL  = ' SELECT COUNT(A.WUUID) AS COUNT ';
    $strSQL .= ' FROM DB2WUSR as A ' ;
//    $strSQL .= ' FROM '.cmLicenseDB2WUSR($licenseUser).' as A ' ;

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if ($stmt === false ){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = $data[0]['COUNT'];
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* グループ数取得
*-------------------------------------------------------*
*/
function fnGetGroupCount($db2con){

    $data = array();

    $strSQL  = ' SELECT COUNT(A.WQGID) AS COUNT ';
    $strSQL .= ' FROM DB2WQGR as A ' ;

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = $data[0]['COUNT'];
        }
    }

    return $data;

}

/*
*-------------------------------------------------------* 
* クエリー登録数取得
*-------------------------------------------------------*
*/
function fnGetQueryCount($db2con){

    $data = array();

    $strSQL  = ' SELECT COUNT(A.D1NAME) AS COUNT ';
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = $data[0]['COUNT'];
        }
    }
    return $data;

}
/*
*-------------------------------------------------------* 
* 区画登録数取得
*-------------------------------------------------------*
*/
function fnGetKukakuCount($db2con){

    $data = array();
    $strSQL  = ' SELECT count(A.RDBNM) as COUNT ';
    $strSQL .= ' FROM DB2RDB as A ' ;
    $strSQL .= ' WHERE RDBNM <> \'\' ';

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = $data[0]['COUNT'];
        }
    }
    return $data;

}