<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$filename = $_POST['filename'];


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
//テーブルある無しフラグ
$tableFlg = true;

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

$systables = cmGetSystables($db2con,SAVE_DB,$filename);

if(count($systables) == 0){
    $tableFlg = false;
}

cmDb2Close($db2con);


/**return**/
$rtn = array(
    'tableFlg' => $tableFlg
);

echo(json_encode($rtn));
