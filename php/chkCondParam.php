<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 
 * PROGRAM ID     : chkCondParam.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2016/04/11
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

function chkCondParamData($db2con,$QRYNM,$cndDataInfo){
    $rtn = 0;
    $data = array();
    $msg = '';
    $CONDDATA = array();
    if($rtn === 0){
        if($cndDataInfo !== ''){
            foreach($cndDataInfo as $cndData){
                $cndSDataInfo = $cndData['CNDSDATA'];
                if($cndSDataInfo !== ''){
                    foreach($cndSDataInfo as $cndSData){
                        //検索条件は【必須】の場合
                        if ($cndSData['CNDTYP'] === '1'){
                            //検索条件データは【RANGE】の場合
                            if($cndSData['CNDKBN'] === 'RANGE'){
                                if(count($cndSData['CNDDATA']) > 0){
                                    foreach ($cndSData['CNDDATA'] as $key => $value){
                                        //【必須】の場合空白チェック
                                        switch ($cndSData['CNDFTYP']) {
                                            case 'P':
                                            case 'S':
                                            case 'B':
                                            case 'L':
                                            case 'T':
                                            case 'Z':
                                                $value = trim($value);
                                                break;
                                            default:
                                                break;
                                        }
                                        if($rtn === 0){
                                            if($value === ''){
                                                //$msg = $cndSData['CNDFHED'].'は入力必須です。';
                                                $msg =showMsg('FAIL_REQ',array($cndSData['CNDFHED']));                                            
                                                $rtn = 1;
                                                break;
                                            }
                                        }
                                        //【P、S、B】の場合数値チェック
                                        //【L、T、Z】の場合【日付、時刻、タイムスタンプ】チェック
                                        if($rtn === 0){
                                            switch ($cndSData['CNDFTYP']) {
                                                case 'L':
                                                    $chkRes = chkVldDate($value);
                                                    if( $chkRes === 1){
                                                        //$msg = '日付フォーマットが正しくないです。<br>「yyyy/mm/dd」、「yyyy-mm-dd」、又「yyyymmdd」のフォッマとで入力してください。';
                                                        $msg = showMsg('FAIL_CHK',array(array('日付','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('日付','フォーマット',array('「yyyy/mm/dd」、「yyyy-mm-dd」','又','「yyyymmdd」')));
                                                        $rtn = 1;
                                                        break;
                                                    }else{
                                                        $cndSData['CNDDATA'][$key] = $chkRes;
                                                    }
                                                    break;
                                                case 'T':
                                                    $chkRes = chkVldTime($value);
                                                    if( $chkRes === 1){
                                                        //$msg = '時刻フォーマットが正しくないです。<br>「HH.MM.SS」又「HH:MM:SS」のフォッマとで入力してください。';
                                                        $msg = showMsg('FAIL_CHK',array(array('時刻','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('時刻','フォーマット',array('「HH.MM.SS」','又','「HH:MM:SS」')));
                                                        $rtn = 1;
                                                        break;
                                                    }else{
                                                        $cndSData['CNDDATA'][$key] = $chkRes;
                                                    }
                                                    break;
                                                case 'Z':
                                                    $chkRes = chkVldTimeStamp($value);
                                                    if( $chkRes === 1){
                                                        //$msg = 'タイムスタンプフォーマットが正しくないです。<br>「yyyy-mm-dd-HH.MM.SS.NNNNNN」又「yyyy-mm-dd HH.MM.SS」のフォッマとで入力してください。';
                                                        $msg = showMsg('FAIL_CHK',array(array('タイムスタンプ','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('タイムスタンプ','フォーマット',array('「yyyy-mm-dd-HH.MM.SS.NNNNNN」','又','「yyyy-mm-dd HH.MM.SS」')));
                                                        $rtn = 1;
                                                        break;
                                                    }else{
                                                        $cndSData['CNDDATA'][$key] = $chkRes;
                                                    }
                                                break;
                                                case 'P':
                                                case 'S':
                                                case 'B':
                                                    if(checkNum($value) === false){
                                                        $msg =showMsg('FAIL_CMP',array($cndSData['CNDFHED']));// $cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                        $rtn = 1;
                                                        break;
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        // 【L、T、Z】以外のデータ長さチェック
                                        if($rtn === 0){
                                            switch ($cndSData['CNDFTYP']) {
                                                case 'L':
                                                case 'T':
                                                case 'Z':
                                                    break;
                                                default:
                                                    if(!checkMaxLen($value,$cndSData['CNDFLEN'])){
                                                        $rtn = 1;
                                                        $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));//$cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                        break;
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    if($rtn !== 0){
                                        break;
                                    }
                                }else {
                                    $rtn = 1;
                                    //$msg = $cndSData['CNDFHED'].'は入力必須です。';
                                    $msg =showMsg('FAIL_REQ',array($cndSData['CNDFHED']));    
                                    break;
                                }
                            }else if($cndSData['CNDKBN'] === 'LIST' || $cndSData['CNDKBN'] === 'NLIST'){
                                //検索条件データは【LIST又はNLIST】の場合
                                if(count($cndSData['CNDDATA']) > 0){
                                    $blkFlg = 0;
                                    foreach ($cndSData['CNDDATA'] as $key => $value){
                                        switch ($cndSData['CNDFTYP']) {
                                            case 'P':
                                            case 'S':
                                            case 'B':
                                            case 'L':
                                            case 'T':
                                            case 'Z':
                                                $value = trim($value);
                                                break;
                                            default:
                                                break;
                                        }
                                        if($rtn === 0){
                                            if($value === ''){
                                                if($blkFlg === 0){
                                                    $blkFlg = 1;
                                                    //$msg = $cndSData['CNDFHED'].'は入力必須です。';
                                                    $msg =showMsg('FAIL_REQ',array($cndSData['CNDFHED']));    
                                                    $rtn = 0;
                                                }
                                            }else{
                                                if($blkFlg === 1){
                                                    $blkFlg = 2;
                                                    $msg = '';
                                                    $rtn = 0;
                                                }
                                                if($rtn === 0){
                                                    switch ($cndSData['CNDFTYP']) {
                                                        case 'L':
                                                            $chkRes = chkVldDate($value);
                                                            if( $chkRes === 1){
                                                                //$msg = '日付フォーマットが正しくないです。<br>「yyyy/mm/dd」、「yyyy-mm-dd」、又「yyyymmdd」のフォッマとで入力してください。';
                                                                $msg = showMsg('FAIL_CHK',array(array('日付','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('日付','フォーマット',array('「yyyy/mm/dd」、「yyyy-mm-dd」','又','「yyyymmdd」')));
                                                                $rtn = 1;
                                                                break;
                                                            }else{
                                                                $cndSData['CNDDATA'][$key] = $chkRes;
                                                            }
                                                            break;
                                                        case 'T':
                                                            $chkRes = chkVldTime($value);
                                                            if( $chkRes === 1){
                                                                //$msg = '時刻フォーマットが正しくないです。<br>「HH.MM.SS」又「HH:MM:SS」のフォッマとで入力してください。';
                                                                $msg = showMsg('FAIL_CHK',array(array('時刻','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('時刻','フォーマット',array('「HH.MM.SS」','又','「HH:MM:SS」')));
                                                                $rtn = 1;
                                                                break;
                                                            }else{
                                                                $cndSData['CNDDATA'][$key] = $chkRes;
                                                            }
                                                            break;
                                                        case 'Z':
                                                            $chkRes = chkVldTimeStamp($value);
                                                            if( $chkRes === 1){
                                                                //$msg = 'タイムスタンプフォーマットが正しくないです。<br>「yyyy-mm-dd-HH.MM.SS.NNNNNN」又「yyyy-mm-dd HH.MM.SS」のフォッマとで入力してください。';
                                                                $msg = showMsg('FAIL_CHK',array(array('タイムスタンプ','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('タイムスタンプ','フォーマット',array('「yyyy-mm-dd-HH.MM.SS.NNNNNN」','又','「yyyy-mm-dd HH.MM.SS」')));
                                                                $rtn = 1;
                                                                break;
                                                            }else{
                                                                $cndSData['CNDDATA'][$key] = $chkRes;
                                                            }
                                                        break;
                                                        case 'P':
                                                        case 'S':
                                                        case 'B':
                                                            if(checkNum($value) === false){
                                                                $msg =showMsg('FAIL_CMP',array($cndSData['CNDFHED']));// $cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                                $rtn = 1;
                                                                break;
                                                            }
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                }
                                                if($rtn === 0){
                                                    //  【L、T、Z】以外のデータ長さチェック
                                                    switch ($cndSData['CNDFTYP']) {
                                                        case 'L':
                                                        case 'T':
                                                        case 'Z':
                                                            break;
                                                        default:
                                                        if(!checkMaxLen($value,$cndSData['CNDFLEN'])){
                                                            $rtn = 1;
                                                            $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));//$cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if($blkFlg === 1){
                                        $rtn = 1;
                                    }
                                }else {
                                    $rtn = 1;
                                    //$msg = $cndSData['CNDFHED'].'は入力必須です。';
                                    $msg =showMsg('FAIL_REQ',array($cndSData['CNDFHED']));                                
                                    break;
                                }
                            }else if($cndSData['CNDKBN'] === 'IS'){
                                //検索条件データは【IS】の場合
                                if(count($cndSData['CNDDATA']) === 0){
                                    $rtn = 1;
                                    //$msg = $cndSData['CNDFHED'].'は入力必須です。';
                                    $msg =showMsg('FAIL_REQ',array($cndSData['CNDFHED']));    
                                    break;
                                }else{
                                    if($cndSData['CNDDATA'][0] === ''){
                                        //$msg = $cndSData['CNDFHED'].'は入力必須です。';
                                        $msg =showMsg('FAIL_REQ',array($cndSData['CNDFHED']));    
                                        $rtn = 1;
                                        break;
                                    }
                                }
                            }else{
                                //検索条件データは【RANGE、LIST、NLIST、IS以外】の場合
                                if(count($cndSData['CNDDATA']) > 0){
                                    switch ($cndSData['CNDFTYP']) {
                                        case 'P':
                                        case 'S':
                                        case 'B':
                                        case 'L':
                                        case 'T':
                                        case 'Z':
                                            $cndSData['CNDDATA'][0] = trim($cndSData['CNDDATA'][0]);
                                            break;
                                        default:
                                            break;
                                    }
                                    if($rtn === 0){
                                        if($cndSData['CNDDATA'][0] === ''){
                                            //$msg = $cndSData['CNDFHED'].'は入力必須です。';
                                            $msg =showMsg('FAIL_REQ',array($cndSData['CNDFHED']));    
                                            $rtn = 1;
                                            break;
                                        }
                                    }
                                    if($rtn === 0){
                                        switch ($cndSData['CNDFTYP']) {
                                            case 'L':
                                                switch($cndSData['CNDKBN']){
                                                    case 'LIKE':
                                                    case 'NLIKE':
                                                    case 'LLIKE':
                                                    case 'LNLIKE':
                                                    case 'RLIKE':
                                                    case 'RNLIKE':
                                                        break;
                                                    default :
                                                        $chkRes = chkVldDate($cndSData['CNDDATA'][0]);
                                                        if( $chkRes === 1){
                                                            //$msg = '日付フォーマットが正しくないです。<br>「yyyy/mm/dd」、「yyyy-mm-dd」、又「yyyymmdd」のフォッマとで入力してください。';
                                                            $msg = showMsg('FAIL_CHK',array(array('日付','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('日付','フォーマット',array('「yyyy/mm/dd」、「yyyy-mm-dd」','又','「yyyymmdd」')));
                                                            $rtn = 1;
                                                            break;
                                                        }else{
                                                            $cndSData['CNDDATA'][0] = $chkRes;
                                                        }
                                                        break;
                                                }
                                                break;
                                            case 'T':
                                                switch($cndSData['CNDKBN']){
                                                    case 'LIKE':
                                                    case 'NLIKE':
                                                    case 'LLIKE':
                                                    case 'LNLIKE':
                                                    case 'RLIKE':
                                                    case 'RNLIKE':
                                                        break;
                                                    default :
                                                        $chkRes = chkVldTime($cndSData['CNDDATA'][0]);
                                                        if( $chkRes === 1){
                                                            //$msg = '時刻フォーマットが正しくないです。<br>「HH.MM.SS」又「HH:MM:SS」のフォッマとで入力してください。';
                                                            $msg = showMsg('FAIL_CHK',array(array('時刻','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('時刻','フォーマット',array('「HH.MM.SS」','又','「HH:MM:SS」')));
                                                            $rtn = 1;
                                                            break;
                                                        }else{
                                                            $cndSData['CNDDATA'][0] = $chkRes;
                                                        }
                                                        break;
                                                }
                                                break;
                                            case 'Z':
                                                switch($cndSData['CNDKBN']){
                                                    case 'LIKE':
                                                    case 'NLIKE':
                                                    case 'LLIKE':
                                                    case 'LNLIKE':
                                                    case 'RLIKE':
                                                    case 'RNLIKE':
                                                        break;
                                                    default :
                                                        $chkRes = chkVldTimeStamp($cndSData['CNDDATA'][0]);
                                                        if( $chkRes === 1){
                                                            //$msg = 'タイムスタンプフォーマットが正しくないです。<br>「yyyy-mm-dd-HH.MM.SS.NNNNNN」又「yyyy-mm-dd HH.MM.SS」のフォッマとで入力してください。';
                                                            $msg = showMsg('FAIL_CHK',array(array('タイムスタンプ','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('タイムスタンプ','フォーマット',array('「yyyy-mm-dd-HH.MM.SS.NNNNNN」','又','「yyyy-mm-dd HH.MM.SS」')));
                                                            $rtn = 1;
                                                            break;
                                                        }else{
                                                            $cndSData['CNDDATA'][0] = $chkRes;
                                                        }
                                                        break;
                                                }
                                                break;
                                            case 'P':
                                            case 'S':
                                            case 'B':
                                                if(checkNum($cndSData['CNDDATA'][0]) === false){
                                                    $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));//$cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                    $rtn = 1;
                                                    break;
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    if($rtn === 0){
                                        switch ($cndSData['CNDFTYP']) {
                                            case 'L':
                                            case 'T':
                                            case 'Z':
                                                break;
                                            default:
                                                if(!checkMaxLen($cndSData['CNDDATA'][0],$cndSData['CNDFLEN'])){
                                                    $rtn = 1;
                                                    $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));// $cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                    break;
                                                }
                                                break;
                                        }
                                    }
                                }else {
                                    $rtn = 1;
                                    //$msg = $cndSData['CNDFHED'].'は入力必須です。';
                                    $msg =showMsg('FAIL_REQ',array($cndSData['CNDFHED']));    
                                    break;
                                }  
                            }
                        }else if($cndSData['CNDTYP'] === '2'){
                            //検索条件は【任意】の場合
                            if($cndSData['CNDKBN'] === 'RANGE'){
                                //検索条件データは【RANGE】の場合
                                if(count($cndSData['CNDDATA']) > 0){
                                    foreach ($cndSData['CNDDATA'] as $key => $value){
                                        switch ($cndSData['CNDFTYP']) {
                                            case 'P':
                                            case 'S':
                                            case 'B':
                                            case 'L':
                                            case 'T':
                                            case 'Z':
                                                $value = trim($value);
                                                break;
                                            default:
                                                break;
                                        }
                                        if($rtn === 0){
                                            /*if($cndSData['CNDFTYP'] === 'P' || $cndSData['CNDFTYP'] === 'S' || $cndSData['CNDFTYP'] === 'B'){
                                                if(checkNum($value) === false){
                                                    $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));// $cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                    $rtn = 1;
                                                    break;
                                                }
                                            }*/
                                            switch ($cndSData['CNDFTYP']) {
                                                case 'L':
                                                    $chkRes = chkVldDate($value);
                                                    if( $chkRes === 1){
                                                        //$msg = '日付フォーマットが正しくないです。<br>「yyyy/mm/dd」、「yyyy-mm-dd」、又「yyyymmdd」のフォッマとで入力してください。';
                                                        $msg = showMsg('FAIL_CHK',array(array('日付','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('日付','フォーマット',array('「yyyy/mm/dd」、「yyyy-mm-dd」','又','「yyyymmdd」')));
                                                        $rtn = 1;
                                                        break;
                                                    }else{
                                                        $cndSData['CNDDATA'][$key] = $chkRes;
                                                    }
                                                    break;
                                                case 'T':
                                                    $chkRes = chkVldTime($value);
                                                    if( $chkRes === 1){
                                                        //$msg = '時刻フォーマットが正しくないです。<br>「HH.MM.SS」又「HH:MM:SS」のフォッマとで入力してください。';
                                                        $msg = showMsg('FAIL_CHK',array(array('時刻','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('時刻','フォーマット',array('「HH.MM.SS」','又','「HH:MM:SS」')));
                                                        $rtn = 1;
                                                        break;
                                                    }else{
                                                        $cndSData['CNDDATA'][$key] = $chkRes;
                                                    }
                                                    break;
                                                case 'Z':
                                                    $chkRes = chkVldTimeStamp($value);
                                                    if( $chkRes === 1){
                                                        //$msg = 'タイムスタンプフォーマットが正しくないです。<br>「yyyy-mm-dd-HH.MM.SS.NNNNNN」又「yyyy-mm-dd HH.MM.SS」のフォッマとで入力してください。';
                                                        $msg = showMsg('FAIL_CHK',array(array('タイムスタンプ','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('タイムスタンプ','フォーマット',array('「yyyy-mm-dd-HH.MM.SS.NNNNNN」','又','「yyyy-mm-dd HH.MM.SS」')));
                                                        $rtn = 1;
                                                        break;
                                                    }else{
                                                        $cndSData['CNDDATA'][$key] = $chkRes;
                                                    }
                                                break;
                                                case 'P':
                                                case 'S':
                                                case 'B':
                                                    if(checkNum($value) === false){
                                                        $msg =showMsg('FAIL_CMP',array($cndSData['CNDFHED']));// $cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                        $rtn = 1;
                                                        break;
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        if($rtn === 0){
                                            switch ($cndSData['CNDFTYP']) {
                                                case 'L':
                                                case 'T':
                                                case 'Z':
                                                    break;
                                                default:
                                                    if(!checkMaxLen($value,$cndSData['CNDFLEN'])){
                                                        $rtn = 1;
                                                        $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));//$cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                        break;
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    if($rtn === 0){
                                        if(count($cndSData['CNDDATA']) === 2){
                                            if($cndSData['CNDFTYP'] === 'P' || $cndSData['CNDFTYP'] === 'S' || $cndSData['CNDFTYP'] === 'B'){
                                                $cndSData['CNDDATA'][0] = trim($cndSData['CNDDATA'][0]);
                                                $cndSData['CNDDATA'][1] = trim($cndSData['CNDDATA'][1]);
                                            }
                                            /* MSM block this check for range calendar one of value not insert 20181030
                                            if($cndSData['CNDDATA'][0] === '' && $cndSData['CNDDATA'][1] !== ''){
                                                $rtn = 1;
                                                $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));//$cndSData['CNDFHED'].'の入力に誤りがあります。';
                                            }else if($cndSData['CNDDATA'][0] !== '' && $cndSData['CNDDATA'][1] === ''){
                                                $rtn = 1;
                                                $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));//$cndSData['CNDFHED'].'の入力に誤りがあります。';
                                            }*/ 
                                        }
                                    }
                                    if($rtn !== 0){
                                        break;
                                    }
                                }
                            }else if($cndSData['CNDKBN'] === 'LIST' || $cndSData['CNDKBN'] === 'NLIST'){
                                //検索条件データは【LIST、NLIST】の場合
                                if(count($cndSData['CNDDATA']) > 0){
                                    foreach ($cndSData['CNDDATA'] as $key => $value){
                                        switch ($cndSData['CNDFTYP']) {
                                            case 'P':
                                            case 'S':
                                            case 'B':
                                            case 'L':
                                            case 'T':
                                            case 'Z':
                                                $value = trim($value);
                                                break;
                                            default:
                                                break;
                                        }
                                        if($rtn === 0){
                                            switch ($cndSData['CNDFTYP']) {
                                                case 'L':
                                                    $chkRes = chkVldDate($value);
                                                    if( $chkRes === 1){
                                                        //$msg = '日付フォーマットが正しくないです。<br>「yyyy/mm/dd」、「yyyy-mm-dd」、又「yyyymmdd」のフォッマとで入力してください。';
                                                        $msg = showMsg('FAIL_CHK',array(array('日付','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('日付','フォーマット',array('「yyyy/mm/dd」、「yyyy-mm-dd」','又','「yyyymmdd」')));
                                                        $rtn = 1;
                                                        break;
                                                    }else{
                                                        $cndSData['CNDDATA'][$key] = $chkRes;
                                                    }
                                                    break;
                                                case 'T':
                                                    $chkRes = chkVldTime($value);
                                                    if( $chkRes === 1){
                                                        //$msg = '時刻フォーマットが正しくないです。<br>「HH.MM.SS」又「HH:MM:SS」のフォッマとで入力してください。';
                                                        $msg = showMsg('FAIL_CHK',array(array('時刻','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('時刻','フォーマット',array('「HH.MM.SS」','又','「HH:MM:SS」')));
                                                        $rtn = 1;
                                                        break;
                                                    }else{
                                                        $cndSData['CNDDATA'][$key] = $chkRes;
                                                    }
                                                    break;
                                                case 'Z':
                                                    $chkRes = chkVldTimeStamp($value);
                                                    if( $chkRes === 1){
                                                        //$msg = 'タイムスタンプフォーマットが正しくないです。<br>「yyyy-mm-dd-HH.MM.SS.NNNNNN」又「yyyy-mm-dd HH.MM.SS」のフォッマとで入力してください。';
                                                        $msg = showMsg('FAIL_CHK',array(array('タイムスタンプ','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('タイムスタンプ','フォーマット',array('「yyyy-mm-dd-HH.MM.SS.NNNNNN」','又','「yyyy-mm-dd HH.MM.SS」')));
                                                        $rtn = 1;
                                                        break;
                                                    }else{
                                                        $cndSData['CNDDATA'][$key] = $chkRes;
                                                    }
                                                break;
                                                case 'P':
                                                case 'S':
                                                case 'B':
                                                    if(checkNum($value) === false){
                                                        $msg =showMsg('FAIL_CMP',array($cndSData['CNDFHED']));// $cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                        $rtn = 1;
                                                        break;
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        if($rtn === 0){
                                            switch ($cndSData['CNDFTYP']) {
                                                case 'L':
                                                case 'T':
                                                case 'Z':
                                                    break;
                                                default:
                                                    if(!checkMaxLen($value,$cndSData['CNDFLEN'])){
                                                        $rtn = 1;
                                                        $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));//$cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                        break;
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    if($rtn !== 0){
                                        break;
                                    }
                                }                   
                            }else if($cndSData['CNDKBN'] === 'IS'){
                                // チェック無し；
                            }else{
                                //検索条件データは【RANGE、LIST、NLIST、IS以外】の場合
                                if(count($cndSData['CNDDATA']) > 0){
                                    switch ($cndSData['CNDFTYP']) {
                                        case 'P':
                                        case 'S':
                                        case 'B':
                                        case 'L':
                                        case 'T':
                                        case 'Z':
                                            $cndSData['CNDDATA'][0] = trim($cndSData['CNDDATA'][0]);
                                            break;
                                        default:
                                            break;
                                    }
                                    if($rtn === 0){
                                        switch ($cndSData['CNDFTYP']) {
                                            case 'L':
                                                switch($cndSData['CNDKBN']){
                                                    case 'LIKE':
                                                    case 'NLIKE':
                                                    case 'LLIKE':
                                                    case 'LNLIKE':
                                                    case 'RLIKE':
                                                    case 'RNLIKE':
                                                        break;
                                                    default :
                                                        $chkRes = chkVldDate($cndSData['CNDDATA'][0]);
                                                        if( $chkRes === 1){
                                                            //$msg = '日付フォーマットが正しくないです。<br>「yyyy/mm/dd」、「yyyy-mm-dd」、又「yyyymmdd」のフォッマとで入力してください。';
                                                            $msg = showMsg('FAIL_CHK',array(array('日付','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('日付','フォーマット',array('「yyyy/mm/dd」、「yyyy-mm-dd」','又','「yyyymmdd」')));
                                                            $rtn = 1;
                                                            break;
                                                        }else{
                                                            $cndSData['CNDDATA'][0] = $chkRes;
                                                        }
                                                        break;
                                                }
                                                break;
                                            case 'T':
                                                switch($cndSData['CNDKBN']){
                                                    case 'LIKE':
                                                    case 'NLIKE':
                                                    case 'LLIKE':
                                                    case 'LNLIKE':
                                                    case 'RLIKE':
                                                    case 'RNLIKE':
                                                        break;
                                                    default :
                                                        $chkRes = chkVldTime($cndSData['CNDDATA'][0]);
                                                        if( $chkRes === 1){
                                                            //$msg = '時刻フォーマットが正しくないです。<br>「HH.MM.SS」又「HH:MM:SS」のフォッマとで入力してください。';
                                                            $msg = showMsg('FAIL_CHK',array(array('時刻','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('時刻','フォーマット',array('「HH.MM.SS」','又','「HH:MM:SS」')));
                                                            $rtn = 1;
                                                            break;
                                                        }else{
                                                            $cndSData['CNDDATA'][0] = $chkRes;
                                                        }
                                                        break;
                                                }
                                                break;
                                            case 'Z':
                                                switch($cndSData['CNDKBN']){
                                                    case 'LIKE':
                                                    case 'NLIKE':
                                                    case 'LLIKE':
                                                    case 'LNLIKE':
                                                    case 'RLIKE':
                                                    case 'RNLIKE':
                                                        break;
                                                    default :
                                                        $chkRes = chkVldTimeStamp($cndSData['CNDDATA'][0]);
                                                        if( $chkRes === 1){
                                                            //$msg = 'タイムスタンプフォーマットが正しくないです。<br>「yyyy-mm-dd-HH.MM.SS.NNNNNN」又「yyyy-mm-dd HH.MM.SS」のフォッマとで入力してください。';
                                                            $msg = showMsg('FAIL_CHK',array(array('タイムスタンプ','フォーマット'))) .'<br>'
                                                                        . showMsg('FAIL_FIL_EXT',array('タイムスタンプ','フォーマット',array('「yyyy-mm-dd-HH.MM.SS.NNNNNN」','又','「yyyy-mm-dd HH.MM.SS」')));
                                                            $rtn = 1;
                                                            break;
                                                        }else{
                                                            $cndSData['CNDDATA'][0] = $chkRes;
                                                        }
                                                        break;
                                                }
                                                break;
                                            case 'P':
                                            case 'S':
                                            case 'B':
                                                if(checkNum($cndSData['CNDDATA'][0]) === false){
                                                    $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));//$cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                    $rtn = 1;
                                                    break;
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    if($rtn === 0){
                                        switch ($cndSData['CNDFTYP']) {
                                            case 'L':
                                            case 'T':
                                            case 'Z':
                                                break;
                                            default:
                                                if(!checkMaxLen($cndSData['CNDDATA'][0],$cndSData['CNDFLEN'])){
                                                    $rtn = 1;
                                                    $msg = showMsg('FAIL_CMP',array($cndSData['CNDFHED']));// $cndSData['CNDFHED'].'の入力に誤りがあります。';
                                                    break;
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                        if($rtn !== 0){
                            break;
                        }else{
                            for($i = 0; $i<count($cndSData['CNDDATA']); $i++){
                                $cnddatainfo = array();
                                $cnddatainfo['CDQRYN']  =  $QRYNM;
                                $cnddatainfo['CDFILID'] =  $cndSData['CNFILID'];
                                $cnddatainfo['CDMSEQ']  =  $cndData['CNDIDX'];
                                $cnddatainfo['CDSSEQ']  =  $cndSData['CDATAIDX'];
                                $cnddatainfo['CDCDCD']  =  $i+1;
                                $cnddatainfo['CDDAT']   =  $cndSData['CNDDATA'][$i];
                                $CONDDATA[] = $cnddatainfo; 
                            }
                        }
                    }
                }
                if($rtn !== 0){
                    break;
                }
            }
        }
    }
    $data['BASECONDFRMDATA'] = $chkcondInfo;
    $data['BASEDCNDDATA']    = $CONDDATA;
    error_log("BASEDCNDDATA MSM20181030=> ".print_r($data['BASEDCNDDATA'],true));
    $rtnArr = array(
        'DATA' => $data, 
        'RTN' => $rtn,
        'MSG' => $msg
    );
    //e_log('戻り値'.print_r($rtnArr,true));
    return $rtnArr;
}