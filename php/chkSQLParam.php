<?php
/**
 * ============================================================
 * SYSTEM NAME    : PHPQUEYR2
 * SUBSYSTEM NAME : 共通
 * PROGRAM NAME   : 
 * PROGRAM ID     : chkSQLParam.php
 * DEVELOPED BY   : OSC
 * CREATE DATE    : 2017/04/19
 * MODIFY DATE    : 
 * ============================================================
 **/

/*
 * 外部ファイル読み込み
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

function chkSQLParamData($db2con,$QRYNM,$cndDataInfo){
    //e_log('条件データ：'.print_r($cndDataInfo,true));
    $rtn = 0;
    $data = array();
    $msg = '';
    $CONDDATA = array();
    if($rtn === 0){
        if($cndDataInfo !== ''){
            foreach($cndDataInfo as $cndData){
                if($cndData['CNDSTKB'] === '1'){
                    if($cndData['CNDDTYP'] !== '1'){
                        $cndData['CNDDAT'] = trim($cndData['CNDDAT']);
                    }
                    // 必須チェック
                    if($rtn === 0){
                        if($cndData['CNDDAT'] === ''){
                            $msg =showMsg('FAIL_REQ',array($cndData['CNDNM']));
                            $rtn = 1;
                            break;
                        }
                    }
                    // 【数値、日付、時刻、タイムスタンプ】の場合データ型のパラメータデータのスペースだけのチェック
                    if($rtn === 0){
                        if($cndData['CNDDTYP'] !== '1'){
                            if(cmMer($cndData['CNDDAT']) === ''){
                                $rtn    = 1;
                                $msg    = showMsg('FAIL_SPACECHK',array('【数値又日付又時刻又タイムスタンプ】データ型のパラメータデータ'));
                                break;
                            }
                        }
                    }
                    //データ型とデータの誤りチェック
                    switch($cndData['CNDDTYP']){
                        case '2':
                            if(checkNum($cndData['CNDDAT']) === false ){
                                $msg = showMsg('CHK_FMT',array('【数値】データ型のパラメータデータ','数値'));
                                $rtn = 1;
                                break;
                            }
                            break;
                        case '3':
                            $chkRes = chkVldDate($cndData['CNDDAT']);
                            if($chkRes == 1){
                                $msg = showMsg('FAIL_CHK',array(array('日付','フォーマット'))) .'<br>'
                                            . showMsg('FAIL_FIL_EXT',array('日付','フォーマット',array('「yyyy/mm/dd」、「yyyy-mm-dd」','又','「yyyymmdd」')));
                                $rtn = 1;
                                break;
                            }
                            break;
                        case '4':
                            $chkRes = chkVldTime($cndData['CNDDAT']);
                            if($chkRes == 1){
                                $msg = showMsg('FAIL_CHK',array(array('時刻','フォーマット'))) .'<br>'
                                                . showMsg('FAIL_FIL_EXT',array('時刻','フォーマット',array('「HH.MM.SS」','又','「HH:MM:SS」')));
                                $rtn = 1;
                                break;
                            }
                            break;
                        case '5':
                            $chkRes = chkVldTimeStamp($cndData['CNDDAT']);
                            if($chkRes == 1){
                                $msg = showMsg('FAIL_CHK',array(array('タイムスタンプ','フォーマット'))) .'<br>'
                                        . showMsg('FAIL_FIL_EXT',array('タイムスタンプ','フォーマット',array('「yyyy-mm-dd-HH.MM.SS.NNNNNN」','又','「yyyy-mm-dd HH.MM.SS」')));
                                $rtn = 1;
                                break;
                            }
                            break;
                        default :
                            break;
                    }
                    if($rtn === 0){
                        // 【L、T、Z】以外のデータ長さチェック
                        switch ($cndData['CNDDTYP']) {
                            case '3':
                            case '4':
                            case '5':
                                break;
                            default:
                                if(!checkMaxLen($cndData['CNDDAT'],128)){
                                    $rtn = 1;
                                    $msg = showMsg('FAIL_CMP',array($cndData['CNDNM']));
                                    break;
                                }
                            break;
                        }
                    }
                }
                if($rtn !== 0){
                    break;
                }
            }
        }
    }
    $data['BASEFRMPARAMDATA']   = $cndDataInfo;
    $rtnArr = array(
        'DATA' => $data, 
        'RTN' => $rtn,
        'MSG' => $msg
    );
    //e_log('条件データ：'.print_r($rtnArr,true));
    return $rtnArr;
}