<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");


//BASE側ライセンスチェック用
$db2con = cmDb2con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

fnCLRLICINF($db2con,$ary);
cmDb2Close($db2con);    

/*
*-------------------------------------------------------* 
* ライセンス情報クリア
*-------------------------------------------------------*
*/
function fnCLRLICINF($db2con){

    $flg = array();
    $ret = '0';

    $strSQL  = ' UPDATE DB2WLIC SET ';
    $strSQL .= ' WLDATE = ? ' ;
    $strSQL .= ',WLSRLN = ? ' ;
    $strSQL .= ',WLUSER = ? ' ;
    $strSQL .= ',WLQRY  = ? ' ;
    $strSQL .= ',WLCL   = ? ' ;
    $stmt = db2_prepare($db2con, $strSQL);
    if($stmt === false){
        $ret = '1';
    }else{
        $flg[]='0';
        $flg[]='0';
        $flg[]='0';
        $flg[]='0';
        $flg[]='0';
        $r = db2_execute($stmt,$flg);
        if($r === false){
            $ret = '1';
        }
    }
    return $ret;
}

