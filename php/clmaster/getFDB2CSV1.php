<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$D1NAME = $_POST['D1NAME'];
$D1TEXT = $_POST['D1TEXT'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);
$D1TEXT = cmHscDe($D1TEXT);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){          
            $rs = cmChkKenGen($db2con,'19',$userData[0]['WUSAUT']);
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('CL連携の権限'));
            }
        }
    }
}

if($proc === 'EDIT'){
    if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
        if($rtn === 0){
            $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
            if($chkQryUsr === 'NOTEXIST_GET'){
                $rtn = 3;
                $msg = showMsg('FAIL_QRY_USR',array('CL連携の権限'));
            }else if($chkQryUsr !== true){
                $rtn = 1;
                $msg = showMsg($chkQryUsr['result'],array('クエリー'));
            }
        }
    }
    if($rtn === 0){
        $rs = fnSelFDB2CSV1($db2con,$userData[0]['WUAUTH'],$D1NAME,$licenseSql);
        if($rs['result'] === true){
            if(count($rs['data']) === 0){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET_QRY',array('ヘルプ','クエリー'));
            }else{
                $data = $rs['data'];
            }
        }else{
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $rs = fnGetAllCount($db2con,$userData[0]['WUAUTH'],$D1NAME,$D1TEXT,$licenseSql);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetFDB2CSV1PG($db2con,$userData[0]['WUAUTH'],$D1NAME,$D1TEXT,$start,$length,$sort,$sortDir,$licenseSql);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg)
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/


/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$WUAUTH,$D1NAME = '',$D1TEXT = '',$licenseSql){
    $data = array();
    $params = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '     count(D1NAME) as COUNT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 AS B ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     D1NAME <> \'\' ';
    if($WUAUTH === '3'){
        $strSQL .= '    AND D1NAME IN ( ';
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if( $WUAUTH === '4'){
        $strSQL .= '    AND D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '            )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params,'%'.$D1NAME.'%');
    }

    if($D1TEXT != ''){
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params,'%'.$D1TEXT.'%');
    }
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND D1CFLG = \'\' ';
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}
function fnGetFDB2CSV1PG($db2con,$WUAUTH, $D1NAME, $D1TEXT, $start = '', $length = '', $sort = '', $sortDir = '',$licenseSql)
{
    
    $data = array();
    
    $params = array();
    
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.*, C.*, ROWNUMBER() OVER( ';
    
    if ($sort !== '') {
        $strSQL .= ' ORDER BY B.' . $sort . ' ' . $sortDir . ' ';
    } else {
        $strSQL .= ' ORDER BY B.D1NAME ASC ';
    }
    
    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT  * ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                 SELECT DISTINCT ';
        $strSQL .= '                     WGNAME ';
        $strSQL .= '                 FROM ';
        $strSQL .= '                     DB2WGDF ';
        $strSQL .= '                 WHERE ';
        $strSQL .= '                     WGGID IN (SELECT ';
        $strSQL .= '                                     WUGID ';
        $strSQL .= '                                 FROM ';
        $strSQL .= '                                     DB2WUGR ';
        $strSQL .= '                                 WHERE ';
        $strSQL .= '                                     WUUID = ?  ';
        $strSQL .= '                                 ) ';
        $strSQL .= '             )';
        $strSQL .= '             UNION  ( ';
        $strSQL .= '                 SELECT ';
        $strSQL .= '                     DQNAME ';
        $strSQL .= '                 FROM ';
        $strSQL .= '                     DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '                 WHERE ';
        $strSQL .= '                     DQNAME <> \'\' ';
        $strSQL .= '                  AND DQCUSR = ? ';
        $strSQL .= '                ) ';         
        $strSQL .= '            )';
        $strSQL .= '        )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH ===     '4'){
        $strSQL .= '        ( ';
        $strSQL .= '        SELECT  * ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE D1NAME IN ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                DQNAME ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                DQNAME <> \'\' ';
        $strSQL .= '             AND DQCUSR = ? ';
        $strSQL .= '            )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '   FDB2CSV1 AS B';
    }
    $strSQL .= ' LEFT JOIN FDB2CSV1PG AS C ON B.D1NAME = C.DGNAME ';    
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if ($D1NAME != '') {
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params, '%' . $D1NAME . '%');
    }
    
    if ($D1TEXT != '') {
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params, '%' . $D1TEXT . '%');
    }
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND D1CFLG = \'\' ';
    }
    $strSQL .= ' ) as A ';
    
    //抽出範囲指定
    //    if (($start != '') && ($length != '')) {
    if (($start !== '') && ($length !== '')) {
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params, $start + 1);
        array_push($params, $start + $length);
    }
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* FDB2CSV1取得(1行)
*-------------------------------------------------------*
*/

function fnSelFDB2CSV1($db2con,$WUAUTH,$D1NAME,$licenseSql){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     B.*, ';
    $strSQL .= '     A.* ';
    $strSQL .= ' FROM ';
    //$strSQL .= '     FDB2CSV1 AS B ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT  * ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                 SELECT DISTINCT ';
        $strSQL .= '                     WGNAME ';
        $strSQL .= '                 FROM ';
        $strSQL .= '                     DB2WGDF ';
        $strSQL .= '                 WHERE ';
        $strSQL .= '                     WGGID IN (SELECT ';
        $strSQL .= '                                     WUGID ';
        $strSQL .= '                                 FROM ';
        $strSQL .= '                                     DB2WUGR ';
        $strSQL .= '                                 WHERE ';
        $strSQL .= '                                     WUUID = ?  ';
        $strSQL .= '                                 ) ';
        $strSQL .= '             )';
        $strSQL .= '             UNION  ( ';
        $strSQL .= '                 SELECT ';
        $strSQL .= '                     DQNAME ';
        $strSQL .= '                 FROM ';
        $strSQL .= '                     DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '                 WHERE ';
        $strSQL .= '                     DQNAME <> \'\' ';
        $strSQL .= '                  AND DQCUSR = ? ';
        $strSQL .= '                ) ';         
        $strSQL .= '            )';
        $strSQL .= '        )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH ===     '4'){
        $strSQL .= '        ( ';
        $strSQL .= '        SELECT  * ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE D1NAME IN ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                DQNAME ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                DQNAME <> \'\' ';
        $strSQL .= '             AND DQCUSR = ? ';
        $strSQL .= '            )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '   FDB2CSV1 AS B';
    }
    $strSQL .= '  LEFT JOIN FDB2CSV1PG AS A ';
    $strSQL .= '  ON B.D1NAME = A.DGNAME ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     D1NAME = ? ';
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND D1CFLG = \'\' ';
    }
    array_push($params,$D1NAME);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}