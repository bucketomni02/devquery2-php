<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$d1name = $_POST['D1NAME'];
$d1event = $_POST['D1EVENT'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = false;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
          
            $rs = cmChkKenGen($db2con,'19',$userData[0]['WUSAUT']);//'1' => mailMaster
       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('CL連携の権限'));
            }
        }
    }
}
if($rtn === 0){
    $rs = fnGetAllCount($db2con, $d1name, $d1event);
    if($rs['result'] !== true){
        $rtn =1;
        $msg = showMsg($rs['result']);
    }else{
        $allcount = $rs['data'];
    }
    $htmlFlg = true;
}
if($rtn === 0){
    $rs = _fnGetFDB2CSV1PM($db2con, $rs, $d1name, $d1event);
    if($rs['result'] !== true){
        $rtn =1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg)
);
echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function _fnGetFDB2CSV1PM($db2con,&$rs = '0', $d1name , $d1event){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT A.DMNAME, A.DMPNAME, A.DMPTYPE, A.DMLEN, A.DMSCAL, A.DMFRTO, A.DMIZDT, A.DMSCFG, A.DMSCFD,A.DMMSEQ,A.DMSSEQ ';
    $strSQL .= ' FROM FDB2CSV1PM AS A ' ;
    $strSQL .= ' WHERE A.DMNAME <> \'\' ';
    $strSQL .= ' AND DMNAME = ? AND DMEVENT = ? ';
    $strSQL .= ' ORDER BY A.DMSEQ ASC ' ;
    $stmt = db2_prepare($db2con,$strSQL);
    $params = array(
        $d1name,
        $d1event
    );
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                    if(rtrim($row['DMSCFD']) !== ''){
                         $row['BD3SEQ'] = rtrim($row['DMSCFD']).'_'.rtrim($row['DMMSEQ']).'_'.rtrim($row['DMSSEQ']);
                    }else{
                         $row['BD3SEQ'] = '';
                    }
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con, $d1name , $d1event){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT count(A.DMNAME) as COUNT ';
    $strSQL .= ' FROM FDB2CSV1PM as A ' ;
    $strSQL .= ' WHERE A.DMNAME <> \'\' ';
    $strSQL .= ' AND DMNAME = ? AND DMEVENT = ?  ';
    $stmt = db2_prepare($db2con,$strSQL);
    $params = array(
        $d1name,
        $d1event
    );
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}