<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$d3name = $_POST['D3NAME'];
$D1WEBF = $_POST['D1WEBF'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
          
            $rs = cmChkKenGen($db2con,'19',$userData[0]['WUSAUT']);//'1' => mailMaster
       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('CL連携の権限'));
            }
        }
    }
}
if($rtn === 0){
    $chkQry = cmChkQuery($db2con,'', $d3name, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($rtn === 0){
    $fdb2csv1data = FDB2CSV1_DATA($d3name); 
    if($fdb2csv1data['result'] !== true){
        $rtn = 1;
        $msg = showMsg($fdb2csv1data['result'],array('クエリー'));
    }else{
        $fdb2csv1data = umEx($fdb2csv1data['data']);
        $fdb2csv1data = $fdb2csv1data[0];
        $D1CFLG       = $fdb2csv1data['D1CFLG'];
    }
}
if($rtn === 0){
    $web = '';
    if($D1WEBF !== '1'){
        $web = 'NOT WEB';
        if($rtn === 0){
            $rs = fnGetAllCount($db2con, $d3name);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $allcount = $rs['data'];
            }
        }
        if($rtn === 0){
            $rs = fnGetFDB2CSV3($db2con, $rs, $d3name);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
            }
        }
    }else{
        $web = 'WEB';
        if($D1CFLG == '1'){
            if($rtn === 0){
                $rs = fnGetCountBSQLCND($db2con, $d3name);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }else{
                    $allcount = $rs['data'];
                }
            }
            if($rtn === 0){
                $rs = fnGetBSQLCND($db2con, $rs, $d3name);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }else{
                    $data = $rs['data'];
                }
            }
        }else{
            if($rtn === 0){
                $rs = fnGetCountBQRYCND($db2con, $d3name);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }else{
                    $allcount = $rs['data'];
                }
            }
            if($rtn === 0){
                $rs = fnGetBQRYCND($db2con, $rs, $d3name);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }else{
                    $data = $rs['data'];
                }
            }
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data),
    'web' =>$web
);
echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV3($db2con,&$rs = '0', $name = ''){
    $data = array();
    $data[] = array('D3FLD'=>' ', 'TEXT'=>'<未選択>','BD3SEQ'=>'');
    $params = array();

    $strSQL  = ' SELECT A.D3FLD, A.D3FLD AS TEXT,B.D2HED AS TIP ';
    $strSQL .= ' FROM FDB2CSV3 AS A ' ;
    $strSQL .=' LEFT JOIN FDB2CSV2 AS B ';
    $strSQL .=' ON A.D3NAME = B.D2NAME  ';
    $strSQL .=' AND A.D3FILID = B.D2FILID ';
    $strSQL .=' AND A.D3FLD = B.D2FLD ';

    $strSQL .= ' WHERE A.D3NAME <> \'\' ';
    if($name != ''){
        $strSQL .= ' AND D3NAME = ?  AND D3CND IN (\'EQ\', \'NE\', \'GE\', \'GT\', \'LE\', \'LT\', \'RANGE\' ) ';
        array_push($params,$name);
    }
    $strSQL .= ' ORDER BY A.D3JSEQ ASC ' ;

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $row['BD3SEQ'] = rtrim($row['D3FLD']).'_0_0';
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/



function fnGetAllCount($db2con, $name = ''){
    $data = array();

    $strSQL  = ' SELECT count(A.D3NAME) as COUNT ';
    $strSQL .= ' FROM FDB2CSV3 as A ' ;
    $strSQL .= ' WHERE A.D3NAME <> \'\' ';

    $params = array();
    if($name != ''){
        $strSQL .= ' AND D3NAME = ? ';
        array_push($params,$name);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    } 
    return $data;

}
/*
*-------------------------------------------------------* 
* WEB通常クエリーの条件データ取得
*-------------------------------------------------------*
*/

function fnGetBQRYCND($db2con,&$rs = '0', $name = ''){
    $data = array();
    $data[] = array('D3FLD'=>' ', 'TEXT'=>'<未選択>','CNMSEQ'=>0,'CNSSEQ'=>0,'BD3SEQ'=>'');
    $params = array();

    $strSQL  = ' SELECT A.CNFLDN AS D3FLD, A.CNFLDN AS TEXT,A.CNMSEQ AS CNMSEQ,A.CNSSEQ AS CNSSEQ,B.D2HED AS TIP ';
    $strSQL .= ' FROM BQRYCND AS A ' ;
    $strSQL .=' LEFT JOIN FDB2CSV2 AS B ';
    $strSQL .=' ON A.CNQRYN = B.D2NAME  ';
    $strSQL .=' AND A.CNFILID = B.D2FILID ';
    $strSQL .=' AND A.CNFLDN = B.D2FLD ';

    $strSQL .= ' WHERE A.CNQRYN <> \'\' ';
    if($name != ''){
        $strSQL .= ' AND CNQRYN = ?  AND CNCKBN IN (\'EQ\', \'NE\', \'GE\', \'GT\', \'LE\', \'LT\', \'RANGE\' ) ';
        array_push($params,$name);
    }
    $strSQL .= ' ORDER BY A.CNMSEQ ASC ' ;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                 $row['BD3SEQ'] = rtrim($row['D3FLD']).'_'.rtrim($row['CNMSEQ']).'_'.rtrim($row['CNSSEQ']);
                 $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* WEB通常クエリーの条件カウント取得
*-------------------------------------------------------*
*/

function fnGetCountBQRYCND($db2con, $name = ''){
    $data = array();

    $strSQL  = ' SELECT count(A.CNQRYN) as COUNT ';
    $strSQL .= ' FROM BQRYCND as A ' ;
    $strSQL .= ' WHERE A.CNQRYN <> \'\' ';

    $params = array();
    if($name != ''){
        $strSQL .= ' AND CNQRYN = ?  AND CNCKBN IN (\'EQ\', \'NE\', \'GE\', \'GT\', \'LE\', \'LT\', \'RANGE\' ) ';
        array_push($params,$name);
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT']+1;//COUNT FOR EXTRA
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    } 
    return $data;
}
/*
*-------------------------------------------------------* 
* WEBSQLクエリーの条件データ取得
*-------------------------------------------------------*
*/

function fnGetBSQLCND($db2con,&$rs = '0', $name = ''){
    $data = array();
    $data[] = array('D3FLD'=>' ', 'TEXT'=>'<未選択>','CNMSEQ'=>0,'CNSSEQ'=>0,'BD3SEQ'=>'');
    $params = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     A.CNDSEQ AS D3FLD, ';
    $strSQL .= '     A.CNDNM AS TEXT, ';
    $strSQL .= '     A.CNDNM AS TIP ';
    $strSQL .= ' FROM ';
    $strSQL .= '     BSQLCND AS A ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.CNQRYN <> \'\' AND ';
    if($name != ''){
        $strSQL .= '     CNQRYN = ?  ';
        array_push($params,$name);
    }
    $strSQL .= ' ORDER BY ';
    $strSQL .= '     A.CNDSEQ ASC ';
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                 $row['BD3SEQ'] = rtrim($row['D3FLD']).'_0_0';
                 $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* WEBSQLクエリーの条件カウント取得
*-------------------------------------------------------*
*/

function fnGetCountBSQLCND($db2con, $name = ''){
    $data = array();

    $strSQL  = ' SELECT count(A.CNQRYN) as COUNT ';
    $strSQL .= ' FROM BSQLCND as A ' ;
    $strSQL .= ' WHERE A.CNQRYN <> \'\' ';

    $params = array();
    if($name != ''){
        $strSQL .= ' AND CNQRYN = ?  ';
        array_push($params,$name);
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT']+1;//COUNT FOR EXTRA
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    } 
    return $data;
}
