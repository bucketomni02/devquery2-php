<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$d1name = mb_convert_kana($_POST['D1NAME'], 's', 'UTF-8');
$d1webf = mb_convert_kana($_POST['D1WEBF'], 's', 'UTF-8');
$d1event = mb_convert_kana($_POST['D1EVENT'], 's', 'UTF-8');
$odgpgm = mb_convert_kana($_POST['ODGPGM'], 's', 'UTF-8');
$odglib = mb_convert_kana($_POST['ODGLIB'], 's', 'UTF-8');
$ndgpgm = mb_convert_kana($_POST['NDGPGM'], 's', 'UTF-8');
$ndglib = mb_convert_kana($_POST['NDGLIB'], 's', 'UTF-8');
$fdb2csv1mr = json_decode($_POST['FDB2CSV1MR'],true);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtnArray = array();        //リターン配列
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
          
            $rs = cmChkKenGen($db2con,'19',$userData[0]['WUSAUT']);//'1' => mailMaster
       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('CL連携の権限'));
            }
        }
    }
}
//クエリー存在チェック
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$d1name,'');
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 1;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$d1name,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのCL連携の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('CL連携の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($d1event !== '3'){
    $w1evname = '';
    switch(trim($d1event)){
        case '2':
            $w1evname = '実行後';
            break;
        case '1':
        default:
            $w1evname = '実行前';
            break;
    }
    if($rtn === 0){
        if(cmMer($ndglib) !== ''){
            if (cmMer($ndgpgm) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array(array($w1evname,'ﾌﾟﾛｸﾞﾗﾑ')));
            }
        }
    }
    if($rtn === 0){
        if (!checkMaxLen(trim($ndgpgm),10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('ﾌﾟﾛｸﾞﾗﾑ'));
        }
    }

    if ($rtn === 0){
        $byte = checkByte(trim($ndgpgm));
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('ﾌﾟﾛｸﾞﾗﾑ'));
        }
    }
    if($rtn === 0){
        if(cmMer($ndgpgm) !== ''){
            if (cmMer($ndglib) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array(array($w1evname,'ライブラリー')));
            }
        }
    }
    if($rtn === 0){
        if ((!checkMaxLen(trim($ndglib),10))){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('ライブラリー'));
        }
    }
    if ($rtn === 0){
        $byte = checkByte(trim($ndglib));
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('ライブラリー'));
        }
    }

    //チェック(Non DB)
    //$fdb2csv1mrに値があった場合のみ、プログラムとライブラリのブランクチェック
    if(count($fdb2csv1mr) > 0){
        if($rtn === 0){
            if (trim($ndgpgm) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array(array($w1evname,'ﾌﾟﾛｸﾞﾗﾑ')));
            }
        }
        if($rtn === 0){
            if (trim($ndglib) == ''){
                $rtn = 1;
                $msg =showMsg('FAIL_REQ',array(array($w1evname,'ライブラリー')));
            }
        }
    }

    if ($rtn === 0){
        $w1fdb2csv1mr = $fdb2csv1mr;
        foreach ($fdb2csv1mr as $key => $col){
            if (trim(mb_convert_kana($col['DMPNAME'], 's', 'UTF-8')) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('ﾊﾟﾗﾒｰﾀ名'));
                break;
            }

            if($rtn === 0){
                if(!checkMaxLen(trim(mb_convert_kana($col['DMPNAME'], 's', 'UTF-8')),10)){
                    $rtn = 1;
                    $msg = showMsg('FAIL_MAXLEN',array('ﾊﾟﾗﾒｰﾀ名'));
                    break;
                }
            }

            if($rtn === 0){
                $byte = checkByte(trim(mb_convert_kana($col['DMPNAME'], 's', 'UTF-8')));
                if($byte[1] > 0){
                    $rtn = 1;
                    $msg = showMsg('FAIL_BYTE',array('ﾊﾟﾗﾒｰﾀ名'));
                }
            }
            //長さチェック
            if($rtn === 0){
                if($col['DMLEN'] === null){
                    $rtn = 1;
                    $msg = showMsg('FAIL_REQ',array('長さ'));
                    break;
                }
            }
            if($rtn === 0){
                if(checkNuturalNum($col['DMLEN']) === false){
                    $rtn = 1;
                    $msg = showMsg('CHK_FMT',array('長さ','数値'));
                    break;
                }
            }
            //スケール
            if($rtn === 0){
                if($col['DMSCAL'] === null){
                    $rtn = 1;
                    $msg = showMsg('FAIL_REQ',array('スケール'));
                    break;
                }
            }
            if($rtn === 0){
                if(checkNuturalNum($col['DMSCAL']) === false){
                    $rtn = 1;
                    $msg = showMsg('CHK_FMT',array('スケール','数値'));
                    break;
                }
            }

            if (trim($col['BD3SEQ']) === '' && trim($col['DMIZDT']) === ''){
            //if (trim($col['DMSCFD']) === '' && trim($col['DMIZDT']) === ''){
                $rtn = 1;
                $msg = showMsg('COND_REQ',array('対応条件フィールドが未選択','初期値'));
                break;
            }

            if($rtn === 0){
                if(!checkMaxLen(trim($col['DMIZDT']),100)){
                    $rtn = 1;
                    $msg = showMsg('FAIL_MAXLEN',array('初期値'));
                    break;
                }
            }
            if($col['BD3SEQ'] !== ''){
                $dta = explode('_',$col['BD3SEQ']);
            }else{
                    $dta[0] = '';
                    $dta[1] = 0; 
                    $dta[2] = 0;
            }
            $DMSCFD = $dta[0];
            $DMMSEQ = $dta[1];
            $DMSSEQ =  $dta[2];
            //RANGEパラメータかチェック
             if($d1webf !== '1'){
                //$range = checkRange($db2con,$d1name,trim($col['DMSCFD']));
                $range = checkRange($db2con,$d1name,trim($DMSCFD));
            }else{
                $range = checkRangeWeb($db2con,$d1name,trim($DMSCFD),trim($DMMSEQ),trim($DMSSEQ));
            }
            if($range['result'] !== true){
                $rtn = 1;
                $msg = showMsg($range['result'],array(array('対応フィールドが','RANGE')));
                break;

            }else{
                $range = $range{'data'};
                if($range > 0){
                    //RANGEの場合はFromToが選択されているかチェック
                    if(trim($col['DMFRTO']) === '0'){
                        $rtn = 1;
                        $msg = showMsg('COND_REQ',array('対応フィールドが','RANGE',array('FromTo')));
                        break;
                    }
                }
            }

            $rangeDupCount = 0;
            foreach ($w1fdb2csv1mr as $w1key => $w1col){
                if ($key !== $w1key && trim($col['DMPNAME']) == trim($w1col['DMPNAME'])) {
                    $rtn = 1;
                    $msg = showMsg('CHK_DUP',array('ﾊﾟﾗﾒｰﾀ名'));
                    break;
                }
                //フィールド重複チェック　RANGEの場合は２個までOK
                if ($key !== $w1key && trim($col['BD3SEQ']) !== '' && trim($col['BD3SEQ']) == trim($w1col['BD3SEQ'])) {
                    if($range === 0){
                        $rtn = 1;
                        $msg = showMsg('CHK_DUP',array('対応条件フィールド'));
                        break;
                    }else{
                        $rangeDupCount++;
                        if($rangeDupCount > 1){
                            $rtn = 1;
                            //$msg = showMsg('RANGE条件の対応条件フィールドが２つ以上選択されています。');
                              $msg = showMsg('FAIL_ABOTWO',array(array('RANGE','条件の対応条件フィールドが')));
                            break;
                        }
                    }
                }
            }

            if ($rtn === 1){
                break;
            }
        }
    }
}
if ($rtn === 0){
    $rs = fnUpdFDB2CSV1PG($db2con, $d1name, $d1event, $odgpgm, $odglib, $ndgpgm, $ndglib, $fdb2csv1mr);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'fdb2csv1mr' =>$fdb2csv1mr
);
echo(json_encode($rtnArray));

//与えられた項目がRANGEか否かをチェック カウントをリターン
function checkRange($db2con,$d1name,$d3fld){

    $data = true;

    $strSQL  = '';
    $strSQL .= ' SELECT COUNT(D3FLD) AS COUNT ';
    $strSQL .= ' FROM FDB2CSV3 ';
    $strSQL .= ' WHERE D3NAME = ? AND D3FLD = ? AND D3CND = ? ';

    $params = array(
        $d1name,
        $d3fld,
        'RANGE'
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $exeRs = db2_execute($stmt,$params);
        if($exeRs === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}
function checkRangeWeb($db2con,$d1name,$cnfldn,$cnmseq,$cnsseq){
    $data = true;

    $strSQL  = '';
    $strSQL .= ' SELECT COUNT(CNFLDN) AS COUNT ';
    $strSQL .= ' FROM BQRYCND ';
    $strSQL .= ' WHERE CNQRYN = ? AND CNFLDN = ? AND CNCKBN = ? ';
    $strSQL .= ' AND CNMSEQ = ? AND CNSSEQ = ? ';

    $params = array(
        $d1name,
        $cnfldn,
        'RANGE',
        $cnmseq,
        $cnsseq
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $exeRs = db2_execute($stmt,$params);
        if($exeRs === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function fnUpdFDB2CSV1PG($db2con, $d1name, $d1event, $odgpgm, $odglib, $ndgpgm, $ndglib, $fdb2csv1mr){
    $rs = true;
    // FDB2CSV1PG
    $wcount = 0;
    $strSQL  = ' SELECT COUNT(A.DGNAME) AS COUNT ';
    $strSQL .= ' FROM FDB2CSV1PG AS A ' ;
    $strSQL .= ' WHERE A.DGNAME = ? ';
    $stmt1 = db2_prepare($db2con, $strSQL);
    $params = array(
        $d1name
    );
    if($stmt1 === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt1,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt1)){
                $wcount = $row['COUNT'];
            }
            $w1dgblib = '';
            $w1dgbpgm = '';
            $w1dgclib = '';
            $w1dgcpgm = '';
            $w1dgalib = '';
            $w1dgapgm = '';
            switch(trim($d1event)){
                case '2':
                    $w1dgblib = '';
                    $w1dgbpgm = '';
                    $w1dgclib = '';
                    $w1dgcpgm = '';
                    $w1dgalib = $ndglib;
                    $w1dgapgm = $ndgpgm;
                    break;
                case '1':
                default:
                    $w1dgblib = $ndglib;
                    $w1dgbpgm = $ndgpgm;
                    $w1dgclib = '';
                    $w1dgcpgm = '';
                    $w1dgalib = '';
                    $w1dgapgm = '';
                    break;
            }
            if($wcount == 0){
                $strSQL  = ' INSERT INTO FDB2CSV1PG ';
                $strSQL .= ' (';
                $strSQL .= ' DGNAME,';
                $strSQL .= ' DGBLIB,';
                $strSQL .= ' DGBPGM,';
                $strSQL .= ' DGCLIB,';
                $strSQL .= ' DGCPGM,';
                $strSQL .= ' DGCBNM,';
                $strSQL .= ' DGALIB,';
                $strSQL .= ' DGAPGM';
                $strSQL .= ' )';
                $strSQL .= ' VALUES (';
                $strSQL .= ' ?, ?, ?, ?, ? , ?, ?, ? ';
                $strSQL .= ' ) ' ;
                $stmt2 = db2_prepare($db2con,$strSQL);
                $params = array(
                    $d1name,
                    $w1dgblib,
                    $w1dgbpgm,
                    $w1dgclib,
                    $w1dgcpgm,
                    '',
                    $w1dgalib,
                    $w1dgapgm
                );
                if($stmt2 === false){
                    $rs = 'FAIL_INS';
                }else{
                    $r = db2_execute($stmt2,$params);
                    if($r === false){
                        $rs = 'FAIL_INS';
                    }
                }                    
            }else{
                $strSQL  = ' UPDATE FDB2CSV1PG ';
                $strSQL .= ' SET ';
                switch(trim($d1event)){
                    case '2':
                        $strSQL .= ' DGALIB = ?, DGAPGM = ? ';
                        $params = array(
                            $w1dgalib,
                            $w1dgapgm,
                            $d1name
                        );
                        break;
                    case '1':
                    default:
                        $strSQL .= ' DGBLIB = ?, DGBPGM = ? ';
                        $params = array(
                            $w1dgblib,
                            $w1dgbpgm,
                            $d1name
                        );
                        break;
                }

                $strSQL .= ' WHERE ';
                $strSQL .= ' DGNAME = ? ';
                $stmt2 = db2_prepare($db2con,$strSQL);
                if($stmt2 === false){
                    $rs = 'FAIL_UPD';
                }else{
                    $r = db2_execute($stmt2,$params);
                    if($r === false){
                        $rs = 'FAIL_UPD';
                    }
                }
            }
            if($rs !== 1){
                // FDB2CSV1PM
                $strSQL  = ' DELETE FROM FDB2CSV1PM ';
                $strSQL .= ' WHERE DMNAME = ? AND DMEVENT = ?';
                $params = array(
                    $d1name,
                    $d1event
                );
                $stmt3 = db2_prepare($db2con, $strSQL);
                if($stmt3 === false){
                    $rs = 'FAIL_DEL';
                }else{
                    $r =db2_execute($stmt3,$params);
                    if($r === false){
                        $rs = 'FAIL_DEL';
                    }else{
                        $strSQL  = ' INSERT INTO FDB2CSV1PM ';
                        $strSQL .= ' (';
                        $strSQL .= ' DMNAME,';
                        $strSQL .= ' DMEVENT,';
                        $strSQL .= ' DMSEQ,';
                        $strSQL .= ' DMUSAGE,';
                        $strSQL .= ' DMPNAME,';
                        $strSQL .= ' DMPTYPE,';
                        $strSQL .= ' DMLEN,';
                        $strSQL .= ' DMSCAL,';
                        $strSQL .= ' DMFRTO,';
                        $strSQL .= ' DMIZDT,';
                        $strSQL .= ' DMSCFG,';
                        $strSQL .= ' DMSCID,';
                        $strSQL .= ' DMSCFD,';
                        $strSQL .= ' DMMSEQ,';
                        $strSQL .= ' DMSSEQ';
                        $strSQL .= ' )';
                        $strSQL .= ' VALUES (';
                        $strSQL .= ' ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?,? ';
                        $strSQL .= ' ) ' ;
                        $stmt4 = db2_prepare($db2con,$strSQL);
                        if($stmt4 === false){
                            $rs = 'FAIL_INS';
                        }else{
                            $seqnumber = 0;
                            foreach ($fdb2csv1mr as $col){
                                if($col['BD3SEQ'] !== ''){
                                    $dta = explode('_',$col['BD3SEQ']);
                                }else{
                                        $dta[0] = '';
                                        $dta[1] = 0; 
                                        $dta[2] = 0;
                                }
                                $DMSCFD = $dta[0];
                                $DMMSEQ = $dta[1];
                                $DMSSEQ =  $dta[2];
                                $params = array(
                                    $d1name,
                                    $d1event,
                                    ++$seqnumber,
                                    '3',
                                    $col['DMPNAME'],
                                    $col['DMPTYPE'],
                                    $col['DMLEN'],
                                    $col['DMSCAL'],
                                    $col['DMFRTO'],
                                    $col['DMIZDT'],
                                    $col['DMSCFG'],
                                    0,
                                    $DMSCFD,
                                    $DMMSEQ,
                                    $DMSSEQ
                                );
                                $r = db2_execute($stmt4,$params);
                                if($r === false){
                                    $rs = 'FAIL_INS';
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $rs;
}
