<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */

$d1name      = mb_convert_kana($_POST['D1NAME'], 's', 'UTF-8');
$d1event     = mb_convert_kana($_POST['D1EVENT'], 's', 'UTF-8');
$odgpgm      = mb_convert_kana($_POST['ODGPGM'], 's', 'UTF-8');
$odglib      = mb_convert_kana($_POST['ODGLIB'], 's', 'UTF-8');
$ndgpgm      = mb_convert_kana($_POST['NDGPGM'], 's', 'UTF-8');
$ndglib      = mb_convert_kana($_POST['NDGLIB'], 's', 'UTF-8');
$dgcbnm      = mb_convert_kana($_POST['DGCBNM'], 's', 'UTF-8');
$fdb2csv1mr  = umEx(json_decode($_POST['FDB2CSV1MR'], true));
$fdb2csv1_Ar = array();

/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$rtnArray = array(); //リターン配列
$dups = array();
$rtn      = 0;
$msg      = '';
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
          
            $rs = cmChkKenGen($db2con,'19',$userData[0]['WUSAUT']);//'1' => mailMaster
       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('CL連携の権限'));
            }
        }
    }
}
//クエリー存在チェック
if ($rtn === 0) {
    $chkQry = array();
    $chkQry = cmChkQuery($db2con, '', $d1name, '');
    if ($chkQry['result'] !== true) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'], array(
            'クエリー'
        ));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$d1name,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのCL連携の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('CL連携の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if ($rtn === 0) {
    if(cmMer($ndglib) !== ''){
        if ($rtn === 0 && (cmMer($ndgpgm) === '')) {
            $rtn = 1;
            $msg = showMsg('FAIL_REQ', array(
                array('実行中','ﾌﾟﾛｸﾞﾗﾑ')
            ));
        }
    }
    if ($rtn === 0 && (!checkMaxLen(trim($ndgpgm), 10))) {
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN', array(
            array('実行中','ﾌﾟﾛｸﾞﾗﾑ')
        ));
    }
    
    if ($rtn === 0) {
        $byte = checkByte(trim($ndgpgm));
        if ($byte[1] > 0) {
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE', array(
                array('実行中','ﾌﾟﾛｸﾞﾗﾑ')
            ));
        }
    }
    if(cmMer($ndgpgm) !== ''){
        if ($rtn === 0 && (cmMer($ndglib) === '')) {
            $rtn = 1;
            $msg = showMsg('FAIL_REQ', array(array(
                '実行中','ライブラリー'
            )));
        }
    }
    if ($rtn === 0 && (!checkMaxLen(trim($ndglib), 10))) {
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN', array(array(
            '実行中','ライブラリー'
        )));
    }
    
    if ($rtn === 0) {
        $byte = checkByte(trim($ndglib));
        if ($byte[1] > 0) {
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE', array(array(
                '実行中','ライブラリー'
            )));
        }
    }
}

if($rtn === 0){
foreach ($fdb2csv1mr as $key => $value) {
    if ($value['TXTFLG'] !== NULL && $value['TXTFLG'] !== '') {
        if (in_array($value['TXTFLG'], $dups) && (int) $value['TXTFLG'] > 0) {
            $rtn = 1;
            $msg = showMsg('CHK_DUP', array(
                'SEQ'
            ));
        } else {
            if (!checkNum($value['TXTFLG'])) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT', array(
                    'SEQ',
                    '数値'
                ));
                break;
            }
            if ((int) $value['TXTFLG'] < 0) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT', array(
                    'SEQ',
                    '数値'
                ));
                break;
            }
            if (strlen($value['TXTFLG']) > 3) {
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN', array(
                    'SEQ'
                ));
                break;
            }
            if ($rtn === 0) {
                if (strpos($value['TXTFLG'], '.') !== false) {
                    $rtn = 1;
                    $msg = showMsg('CHK_FMT', array(
                        'TOの移動数',
                        '数値'
                    ));
                }
            }
            if ($rtn === 0) {
                array_push($dups, $value['TXTFLG']);
                if ((int) $value['TXTFLG'] > 0) {
                    foreach ($value as $k => $v) {
                        $fdb2csv1_Ar[$key][$k] = $v;
                    }
                }
            }
        }
        
      }
   }
}

if($rtn === 0){
if (count($fdb2csv1_Ar) > 0) {
        $w1evname = '実行中';
        if ($rtn === 0 && trim($ndgpgm) === '') {
            $rtn = 1;
            $msg = showMsg('FAIL_REQ', array(
                array($w1evname,'ﾌﾟﾛｸﾞﾗﾑ')
            ));
        }
        if ($rtn === 0 && trim($ndglib) == '') {
            $rtn = 1;
            $msg = $w1evname . showMsg('FAIL_REQ', array(
                array($w1evname,'ライブラリー')
            ));
        }
    if ($rtn === 0 && trim($dgcbnm) == '') {
        $rtn = 1;
        $msg = $w1evname . showMsg('FAIL_REQ', array(
            '実行ボタン名称'
        ));
    }
    
    if ($rtn === 0 && (!checkMaxLen(trim($dgcbnm), 50))) {
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN', array(
            '実行ボタン名称'
        ));
    }
  }
}
if ($rtn === 0) {
    $rs = fnUpdFDB2CSV1PG($db2con, $d1name, $d1event, $odgpgm, $odglib, $ndgpgm, $ndglib, $fdb2csv1_Ar, $dgcbnm);
    if ($rs !== true) {
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'fdb2csv1_Ar' => $fdb2csv1_Ar,
    'test' => $test
);
echo (json_encode($rtnArray));

//与えられた項目がRANGEか否かをチェック カウントをリターン
function checkRange($db2con, $d1name, $d3fld)
{
    
    $data = true;
    
    $strSQL = '';
    $strSQL .= ' SELECT COUNT(D3FLD) AS COUNT ';
    $strSQL .= ' FROM FDB2CSV3 ';
    $strSQL .= ' WHERE D3NAME = ? AND D3FLD = ? AND D3CND = ? ';
    
    $params = array(
        $d1name,
        $d3fld,
        'RANGE'
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $exeRs = db2_execute($stmt, $params);
        if ($exeRs === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data = $row['COUNT'];
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    return $data;
    
}

function fnUpdFDB2CSV1PG($db2con, $d1name, $d1event, $odgpgm, $odglib, $ndgpgm, $ndglib, $fdb2csv1_Ar, $dgcbnm)
{
    $rs     = true;
    // FDB2CSV1PG
    $wcount = 0;
    $strSQL = ' SELECT COUNT(A.DGNAME) AS COUNT ';
    $strSQL .= ' FROM FDB2CSV1PG AS A ';
    $strSQL .= ' WHERE A.DGNAME = ? ';
    $stmt1  = db2_prepare($db2con, $strSQL);
    $params = array(
        $d1name
    );
    if ($stmt1 === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt1, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt1)) {
                $wcount = $row['COUNT'];
            }
            $w1dgblib = '';
            $w1dgbpgm = '';
            $w1dgclib = $ndglib;
            $w1dgcpgm = $ndgpgm;
            $w1dgalib = '';
            $w1dgapgm = '';
            if ($wcount == 0) {
                $strSQL  = ' INSERT INTO FDB2CSV1PG ';
                $strSQL .= ' (';
                $strSQL .= ' DGNAME,';
                $strSQL .= ' DGBLIB,';
                $strSQL .= ' DGBPGM,';
                $strSQL .= ' DGCLIB,';
                $strSQL .= ' DGCPGM,';
                $strSQL .= ' DGCBNM,';
                $strSQL .= ' DGALIB,';
                $strSQL .= ' DGAPGM';
                $strSQL .= ' )';
                $strSQL .= ' VALUES (';
                $strSQL .= ' ?, ?, ?, ?, ? , ?, ?, ? ';
                $strSQL .= ' ) ' ;
                $stmt2  = db2_prepare($db2con, $strSQL);
                $params = array(
                    $d1name,
                    $w1dgblib,
                    $w1dgbpgm,
                    $w1dgclib,
                    $w1dgcpgm,
                    $dgcbnm,
                    $w1dgalib,
                    $w1dgapgm
                );
                if ($stmt2 === false) {
                    $rs = 'FAIL_INS';
                } else {
                    $r = db2_execute($stmt2, $params);
                    if ($r === false) {
                        $rs = 'FAIL_INS';
                    }
                }
            } else {
                $strSQL = ' UPDATE FDB2CSV1PG ';
                $strSQL .= ' SET ';
                $strSQL .= ' DGCLIB = ?, DGCPGM = ?, DGCBNM = ? ';
                $params = array(
                    $w1dgclib,
                    $w1dgcpgm,
                    $dgcbnm,
                    $d1name
                );
                
                $strSQL .= ' WHERE ';
                $strSQL .= ' DGNAME = ? ';
                $stmt2 = db2_prepare($db2con, $strSQL);
                if ($stmt2 === false) {
                    $rs = 'FAIL_UPD';
                } else {
                    $r = db2_execute($stmt2, $params);
                    if ($r === false) {
                        $rs = 'FAIL_UPD';
                    }
                }
            }
            if ($rs !== 1) {
                // FDB2CSV1PM
                $strSQL = ' DELETE FROM FDB2CSV1PM ';
                $strSQL .= ' WHERE DMNAME = ? AND DMEVENT = ?';
                $params = array(
                    $d1name,
                    $d1event
                );
                $stmt3  = db2_prepare($db2con, $strSQL);
                if ($stmt3 === false) {
                    $rs = 'FAIL_DEL';
                } else {
                    $r = db2_execute($stmt3, $params);
                    if ($r === false) {
                        $rs = 'FAIL_DEL';
                    } else {
                        $strSQL  = ' INSERT INTO FDB2CSV1PM ';
                        $strSQL .= ' (';
                        $strSQL .= ' DMNAME,';
                        $strSQL .= ' DMEVENT,';
                        $strSQL .= ' DMSEQ,';
                        $strSQL .= ' DMUSAGE,';
                        $strSQL .= ' DMPNAME,';
                        $strSQL .= ' DMPTYPE,';
                        $strSQL .= ' DMLEN,';
                        $strSQL .= ' DMSCAL,';
                        $strSQL .= ' DMFRTO,';
                        $strSQL .= ' DMIZDT,';
                        $strSQL .= ' DMSCFG,';
                        $strSQL .= ' DMSCID,';
                        $strSQL .= ' DMSCFD,';
                        $strSQL .= ' DMMSEQ,';
                        $strSQL .= ' DMSSEQ';
                        $strSQL .= ' )';
                        $strSQL .= ' VALUES (';
                        $strSQL .= ' ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?,? ';
                        $strSQL .= ' ) ' ;
                        $stmt4 = db2_prepare($db2con, $strSQL);
                        if ($stmt4 === false) {
                            $rs = 'FAIL_INS';
                        } else {
                            $seqnumber = 0;
                            foreach ($fdb2csv1_Ar as $col) {
                                $dmptype = '';
                                if ($col['D2TYPE'] === 'S' || $col['D2TYPE'] === 'P' || $col['D2TYPE'] === 'B') {
                                    $dmptype = '02';
                                } else {
                                    $dmptype = '01';
                                }
                                
                                $params = array(
                                    $d1name,
                                    $d1event,
                                    $col['TXTFLG'],
                                    '',
                                    '',
                                    $dmptype,
                                    $col['D2LEN'],
                                    $col['D2DEC'],
                                    '',
                                    '',
                                    '',
                                    $col['D2FILID'],
                                    $col['D2FLD'],
                                    0,
                                    0
                                );
                                $r      = db2_execute($stmt4, $params);
                                if ($r === false) {
                                    $rs = 'FAIL_INS';
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $rs;
}
