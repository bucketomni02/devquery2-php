<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
require_once('ToolkitService.php');


/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/**
*-------------------------------------------------------* 
* CL実行のため呼び出し
* 
* RESULT
*    01：存在する場合     true
*    02：存在しない場合   false
* 
* @param Object  $cldbcon      CL実行対象DBコネクション
* @param String  $qrynm        定義名
* @param Object  $updParamArr  CL実行対象の最新パラメータ
* @param String  $clFlg        CL実行の区分
*                              BEF:実行前
*                              AFT:実行後
* 
*-------------------------------------------------------*
*/

    $cldbcon = cmDb2ConLib2();

    if($cldbcon === false){
        echo('接続失敗');
    }else{

db2_autocommit($cldbcon,DB2_AUTOCOMMIT_OFF);
db2_exec($cldbcon, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');

        //CL実行のため
        //IBMi接続（データベース接続と共有）
        $conn = ToolkitService::getInstance($cldbcon, DB2_I5_NAMING_ON);
        $conn->setToolkitServiceParams(array('stateless' => true,'XMLServiceLib'=>'QXMLSERV'));
        //パラメータループの前にリターン配列をセット。エラーが出た場合はresultにtrue以外を入れる

        $parm[] = $conn->AddParameterChar('Both', 2, 'RTCD1', 'RTCD1', '');
        $parm[] = $conn->AddParameterChar('Both', 50, 'P1', 'P1', 'ABC');

        $dgpgm = 'CLTEST2';
        $dglib = 'KABE';

        $rs = array('result' => true);
        //プログラム実行
        $result = $conn->PgmCall($dgpgm, $dglib, $parm, null, null);
        //コールに失敗した場合はbreak;
        if($result === false){
            echo('CL失敗');
        }else{
            echo('CL成功');
        }

    }

//すべての処理が正常の場合、COMMIT
db2_commit($cldbcon);

//IBM i 切断
$conn->disconnect();

function cmDb2ConLib2()
{
    
    $user     = '';
    $password = '';
    $database = '*LOCAL';
    
    $option = array(
        'i5_naming' => DB2_I5_NAMING_ON,
        'i5_libl' => 'QTEMP QGPL KABE'
    );
    
    $con = db2_connect($database, $user, $password, $option);
    
    return $con;
}