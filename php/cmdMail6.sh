#!/bin/sh
if [ -f /usr/local/zendsvr6/etc/zce.rc ];then
    . /usr/local/zendsvr6/etc/zce.rc
else
    echo "/usr/local/zendsvr6/etc/zce.rc doesn't exist!"
    exit 1;
fi
LIBPATH=$ZCE_PREFIX/lib

export LDR_CNTRL=MAXDATA=0x80000000
$ZCE_PREFIX/bin/php "/www/zendsvr6/htdocs/devquery2/php/exeMailSend.php" $1 $2
unset LDR_CNTRL

