#!/bin/sh
if [ -f /usr/local/zendphp7/etc/zce.rc ];then
    . /usr/local/zendphp7/etc/zce.rc
else
    echo "/usr/local/zendphp7/etc/zce.rc doesn't exist!"
    exit 1;
fi
LIBPATH=$ZCE_PREFIX/lib

export LDR_CNTRL=MAXDATA=0x80000000
$ZCE_PREFIX/bin/php "/www/zendphp7/htdocs/devquery2/php/exeMailSend.php" $1 $2
unset LDR_CNTRL

