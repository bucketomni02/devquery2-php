<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();

cmSetPHPQUERY($db2con);

fnCodeUserPassword($db2con);

fnCodeMailPassword($db2con);

/*
*-------------------------------------------------------* 
* ユーザーマスター　WUPSWDを暗号化してWUPSWEに更新
*-------------------------------------------------------*
*/

function fnCodeUserPassword($db2con){

    $ret = '0';
    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $ret = '1';
    }else{
        $r = db2_execute($stmt);

        if($r === false){
            echo(db2_stmt_errormsg());
            $ret = '1';
        }else{
            $strSQL  = '';
            $strSQL .= ' UPDATE DB2WUSR ';
            $strSQL .= ' SET ';
            $strSQL .= ' WUPSWE = ENCRYPT(WUPSWD) ';
            
            $params = array();

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                 $ret = '1';
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $ret = '1';
                }
            }
        }
    }
    return $ret;


    /**必要ならパスワード暗号化の後に元のパスワードを削除**/
/*
    $strSQL  = '';
    $strSQL .= ' UPDATE DB2WUSR ';
    $strSQL .= ' SET ';
    $strSQL .= ' WUPSWD = \'\' ';
    
    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);

    db2_execute($stmt,$params);
*/
}

/*
*-------------------------------------------------------* 
* メール設定　WMPASSを暗号化してWMPASEに更新
*-------------------------------------------------------*
*/

function fnCodeMailPassword($db2con){
    $ret = '0';
    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if( $stmt=== false){
        $ret = '1';
    }else{
        $r = db2_exec($db2con,$sql);

        if($r === false){
            echo(db2_stmt_errormsg());
            $ret ='1';
        }else{
            $strSQL  = '';
            $strSQL .= ' UPDATE DB2WMAL ';
            $strSQL .= ' SET ';
            $strSQL .= ' WMPASE = ENCRYPT(WMPASS) ';
            
            $params = array();

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $ret ='1';
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $ret ='1';
                }
            }
        }
    }
    return $ret;

    /**必要ならパスワード暗号化の後に元のパスワードを削除**/
/*
    $strSQL  = '';
    $strSQL .= ' UPDATE DB2WMAL ';
    $strSQL .= ' SET ';
    $strSQL .= ' WMPASS = \'\' ';
    
    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);

    db2_execute($stmt,$params);
*/
}