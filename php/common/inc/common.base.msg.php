<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : 共通
* PROGRAM NAME   : 共通メッセージ
* PROGRAM ID     : common.msg.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/02/29
* MODIFY DATE    : 
* ============================================================
**/

/**
  *---------------------------------------------------------------------------
  * メッセージ文章取得
  * 
  * RESULT
  *    01：メッセージ文章
  * 
  * @param String  func       メッセージキー値
  *
  *---------------------------------------------------------------------------
  **/
function getMsgB($func){
    switch ($func) {
    case "FAIL_INS":
        $msg =  '登録処理に失敗しました。管理者にお問い合わせください。';
        break;
    case "FAIL_UPD":
        $msg = '更新処理に失敗しました。管理者にお問い合わせください。';
        break;
    case "FAIL_DEL":
        $msg = '削除処理に失敗しました。管理者にお問い合わせください。';
        break;
    case "FAIL_SEL":
        $msg = 'データベースの処理に失敗しました。管理者にお問い合わせください。';
        break;
    case "FAIL_SYS":
        $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
        break;
    case "FAIL_DBCON":
        $msg = 'データベースに接続できませんでした。環境を確認して再度実行してください。';
        break;
    case "FAIL_SQL":
        $msg = 'データベースの処理に失敗しました。管理者にお問い合わせください。\r\nファイル名={0}\r\n関数名={1} ';
        break;
    case "FAIL_BYTE":
        $msg = '{0}は半角文字で入力してください。';
        break;
    case "FAIL_REQ":
        $msg = '{0}を入力してください。';
        break;
    case "FAIL_SLEC":
        $msg = '{0}を選択してください。';
        break;
    case "FAIL_RELY_REQ":
        $msg = '{0}を指定した場合は{1}を最低一つ選択してください。';
        break;
    case "FAIL_ASGN":
        $msg = '{0}の指定が正しくありません。';
        break;
    case "FAIL_VLDC":
        $msg = '{0}に使用できない文字が含まれています。</br>{0}には次の文字しか使えません。<br>(A-Z,1-9, _ , #,または@)';
        break;
    case "FAIL_VLDD":
        $msg = '{0}に使用できない文字が含まれています。</br>{0}には次の文字しか使えません。<br>{1}';
        break;
    case "FAIL_MAXLEN":
        $msg = '{0}が入力桁数を超えています。';
        break;
    case "FAIL_FMT":
        $msg = '{0}には{1}形式で登録できません。';
        break;
    case "CHK_FMT":
        $msg = '{0}には{1}で入力してください。';
        break;
    case "ISEXIST":
        $msg = '{0}はすでに登録されています。';
        break;
    case "NOTEXIST":
        $msg = '{0}がありません。';
        break;
    case "NOTEXIST_GET":
        $msg = '{0}情報が取得できませんでした。</br>指定の{0}は削除されている可能性があります。';
        break;
    case "NOTEXIST_UPD":
        $msg = '更新対象{0}は存在しません。</br>指定の{0}は削除されている可能性があります。';
        break;
    case "NOTEXIST_DEL":
        $msg = '削除対象{0}は存在しません。</br>指定の{0}は削除されている可能性があります。';
        break;
    case "NOEXIST_":
        $msg = '該当データは存在しません。';
        break;
    case "ACC_LCK":
        $msg = 'アカウントがロックされました。管理者にお問い合わせください。';
        break;
    case "FAIL_EXP":
        $msg = '{0}の有効期限がきれました。</br>{0}を変更してください';
        break;
    case "FAIL_LGN":
        $msg = 'IDまたはパスワードが正しくありません。';
        break;
    case "CHK_AUT":
        $msg ='{0}は最低一人必要です。削除する場合は他に{1}を作成してください。';
        break;
    case "CHK_AUTUPD":
        $msg ='{0}は最低一人必要です。権限を変える場合は管理者ユーザーを追加するか、他のユーザーに{1}を付けてください。';
        break;
    case "FAIL_CMP":
        $msg ='{0}入力に誤りがあります。';
        break;
    case "FAIL_COL":
        $msg ='{0}に登録されていない{1}が選択されています。{0}の設定をご確認ください。';
        break;
    case "FAIL_SET":
        $msg = '{0}を設定してください。';
        break;
    case "CHK_SEL":
        $msg = '{0}が選択されていません。一つ以上の{0}を選択してください。';
        break;
    case "NOTEXIST_GET_QRY":
        $msg ='{0}が取得できませんでした。</br>指定の{1}が削除された可能性があります。';
        break;
    case "COND_REQ":
        $msg ='{0}の場合、</br>{1}は入力必須です。';
        break;
    case "CHK_DUP":
        $msg ='{0}が重複しています。';
        break;
    case "FAIL_USR_AUT":
        $msg = '一般ユーザーはマスター機能を使用できません。';
        break;
    case "CHECK_LOGOUT":
        $msg = '{0}は既にログアウトされました。';
        break;
    case "FAIL_USR":
        $msg = '指定の{0}が存在しないか、実行する権限がありません。';
        break;
    case "OPTCPY_FAIL":
        $msg = '【{0}】のコピーに失敗しました。';
        break;
    case "ACTIVE_FAIL":
        $msg = 'ActiveDirectory を入力してください。';
        break;
    /*
    case "2":
        $msg = '{0}該当データ{1}は存在しません。';
        break;
    case "3":
        $msg = '{0}該当{1}データ{2}は存在しません。';
        break;
    */
    default:
         $msg = $func;
        break;
    }
    return $msg;
}
/**
  *---------------------------------------------------------------------------
  * メッセージ情報取得
  * 
  * RESULT
  *    01：メッセージ情報
  * 
  * @param String  func       メッセージキー値
  * @param Array   param      メッセージパラメタ値
  *
  *---------------------------------------------------------------------------
  **/
function showMsg($func,$param = '')
{
    $msg = getMsgB($func);
    if($param !== ''){
        for($i = 0;$i < count($param);$i++){
            $bind[]= array('{'.$i.'}' => $param[$i]);
        }
        for($i = 0;$i < count($bind);$i++){
            foreach($bind[$i] as $key => $value){
                $msg = str_replace($key,$value,$msg);
            }
        }
    }
    return $msg;
}

