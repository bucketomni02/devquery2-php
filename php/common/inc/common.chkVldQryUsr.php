<?php

/*
 * 指定したユーザーは指定したクエリーにダウンロード権限があるかをチェックする
 */
function chkVldQryUsr($db2con,$QRYNM,$WUAUTH){
    $rs = true;
    $data = array();
    $params = array();
    $params1 = array();
    if($WUAUTH === '3'){
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        ( ';
        $strSQL .= '            SELECT DISTINCT ';
        $strSQL .= '                WGNAME ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2WGDF ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                WGGID IN ( ';
        $strSQL .= '                    SELECT ';
        $strSQL .= '                        WUGID ';
        $strSQL .= '                    FROM ';
        $strSQL .= '                        DB2WUGR ';
        $strSQL .= '                    WHERE ';
        $strSQL .= '                        WUUID = ?  ';
        $strSQL .= '                ) ';
        $strSQL .= '        ) UNION  ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                DQNAME ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                DQNAME <> \'\' ';
        $strSQL .= '            AND DQCUSR = ? ';
        $strSQL .= '        ) ';         
        $strSQL .= '    )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= '    AND D1NAME = ? ';
    array_push($params,$QRYNM);
    //e_log("chkVldQryUsr MSM 20190404 2 => ".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){

                //$rs = 'NOTEXIST_GET'; 
                //MSM add for クエリーが存在しないのか、権限がないのか、区別できるようにする
                $strSQL1 = ' SELECT  * FROM DB2QHIS WHERE DQNAME = ? ';
                array_push($params1,$QRYNM);
                $stmt1 = db2_prepare($db2con,$strSQL1);
                if($stmt1 === false){
                    $rs1 = 'FAIL_SEL';
                }else{
                    $r1 = db2_execute($stmt1,$params1);
                    if($r1 === false){
                        $rs = 'FAIL_SEL';
                    }else{
                        while($row = db2_fetch_assoc($stmt1)){
                            $data[] = $row;
                        }
                        if(count($data) === 0){
                            $rs = 'NO_QRY';
                        }else{
                            $rs = 'NO_PERMIT';
                        }
                    }
                }
            }//
        }
    }
    return $rs;
}
