<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : 共通
* PROGRAM NAME   : 共通ファイルマンバーセット
* PROGRAM ID     : common.createTmpTbl.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/02/29
* MODIFY DATE    : 
* ============================================================
**/
function createTmpFil($db2con,$lib,$fil,$mbr,$tmpFilnm,$paramSQL = ''){
    $res = true;
    if($mbr !== ''){
        if($lib === '*LIBL' || $lib === ''){
            $tmpFil = $fil.'_'.$mbr.'_'.$tmpFilnm;
        }else{
            $tmpFil = $lib._.$fil.'_'.$mbr.'_'.$tmpFilnm;
        }
        $rs = setFileMBR($db2con,$lbl,$fil,$mbr,$tmpFil);
    }
    $strCreate  = ' CREATE TABLE QTEMP/'.$tmpFilnm.'  AS ';
    $strCreate .= ' ( ';
    if($paramSQL === ''){
        $strCreate .= ' SELECT * FROM ';
        if($mbr === ''){
            if($lib === '*LIBL' || $lib === ''){
                $strCreate .= $fil;
            }else{
                 $strCreate .= $lib .'/'.$fil;
            }
        }else{
            $strCreate .= $QTEMP .'/'.$tmpFil;
        }
    }else{
        $strCreate .= $paramSQL;
    }
    $strCreate .=  ') ';
    $strCreate .=   ' WITH DATA ';
    e_log('1回目データ取得SQL:'.$strCreate);

    $stmt = db2_prepare($db2con,$strCreate);
    if($stmt === false){
        $res = false;
        e_log('テーブルエラー：'.db2_stmt_errormsg().'テーブル：'.$strCreate);
    }else{
        $rs = db2_execute($stmt);

        if($rs === false){
            $res = false;
            e_log('テーブルエラー実行：'.db2_stmt_errormsg().'テーブル：'.$strCreate);
        }/*else{
            //e_log('1回目データ取得'.$tmpFilnm);
            //getTmpData($db2con,$lib,$tmpFilnm);
            if($mbr !== ''){
                $rs = dropFileMBR($db2con,$lbl,$fil,$mbr,$tmpFil);
            }
        }*/
    }
    return $res;
}
function dropTmpFile($db2con,$libl,$tmpFilnm){
    $res = true;
    $strSQL = ' DROP TABLE '.$libl.'/'.$tmpFilnm ;
    $stmt = @db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $res = false;
        e_log(db2_stmt_errormsg());
    }else{
        $rs = @db2_execute($stmt);
        if($rs === false){
            $res = false;
        }
    }

    return $res;
}

function getTmpData($db2con,$LIBL,$TBLNM){
    $data = array();
    $strSQL = '';
    $strSQL .= ' SELECT ';
    $strSQL .= ' * ';
    $strSQL .= ' FROM ';
    if($LIBL === ''|| $LIBL === '*LIBL' || $LIBL === '*USRLIBL'){
        $strSQL .= 'QTEMP/'.$TBLNM;
    }else{
        $strSQL .= 'QTEMP/'.$TBLNM;
    }

    $stmt = db2_prepare($db2con, $strSQL);
    if($stmt === false){
        e_log('データ取得エラー'.db2_stmt_errormsg().$strSQL);
    }else{

        $ret = db2_execute($stmt);

        if($ret === false){
            var_dump(db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            e_log('データ取得結果：'.$strSQL.$TBLNM.print_r($data,true));
        }
    }
}

// テーブルのタイプ取得
function getTableType($rdbnm,$libList,$lib,$fil,$mbr){
    $rtn = '';
    if($rdbnm === '' || $rdbnm === RDB || $rdbnm === RDBNAME){
        $db2con = cmDb2Con();
    }else{
        $db2con = cmDB2ConRDB();
    }
    $strSQL  = '';
    $strSQL .=  '   SELECT ';
    $strSQL .=  '       TABLE_TYPE ';
    $strSQL .=  '   FROM ';
    $strSQL .=  '       QSYS2/SYSTABLES ';
    $strSQL .=  '   WHERE ';
    $strSQL .=  '       TABLE_SCHEMA = ? ';
    $strSQL .=  '   AND TABLE_NAME = ? ';
    //$strSQL .=  '   AND TABLE_TYPE IN (\'L\',\'V\') ';
    $stmt = db2_prepare($db2con, $strSQL);
    if($stmt === false){
        $rtn = '';
        e_log('【削除】テーブルデータ取得エラー①：'.db2_stmt_errormsg().$strSQL);
    }else{
        if($lib === '' || $lib === '*LIBL' || $lib == '*USRLIBL'){
            foreach($libList as $libnm){
                $params = array($libnm,$fil);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    e_log('【削除】テーブルデータ取得エラー②：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
                    $rtn = '';
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        $rtn = cmMer($data[0]['TABLE_TYPE']);
                    }
                }
            }
        }else{
            $params = array($lib,$fil);
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log('【削除】テーブルデータ取得エラー③：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
                $rtn = '';
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                if(count($data) > 0){
                    $rtn = cmMer($data[0]['TABLE_TYPE']);
                }
            }
        }
    }
    cmDb2Close($db2con);
    return $rtn;
}
function comSetQryExecTimeMemory($db2con){
    $rtn = true;
    if(QRYLMTTIME == -1 && QRYLMTMRYS == -1){
        // 処理無し
    }else{
        $stmt = db2_prepare($db2con, "CALL " . MAINLIB ."/PHPQRYA(?,?)");
        if($stmt === false){
            e_log('prepare失敗');
            e_log("①" . db2_stmt_errormsg());
            $rtn = false;
        }else{
            $in_param1 = QRYLMTTIME == -1? '*NOMAX':QRYLMTTIME;
            $in_param2 = QRYLMTMRYS == -1? '*NOMAX':QRYLMTMRYS;

            e_log('時間：'.$in_param1.'    memory:'.$in_param2);
            // ストアドへのパラメータバインド
            $retBool = db2_bind_param($stmt, 1, "in_param1", DB2_PARAM_IN);
            $retBoo4 = db2_bind_param($stmt, 2, "in_param2", DB2_PARAM_IN);

            $ret = db2_execute($stmt);

            if($ret === false){
                e_log('execute失敗');
                e_log('②'.db2_stmt_errormsg());
                $rtn = false;
            }else{
                e_log('リターンコード='.$RTCD);
            }
        }
    }
    return $rtn;
}
function comResetQryExecTimeMemory($db2con){
    $rtn = true;
    if(QRYLMTTIME == -1 && QRYLMTMRYS == -1){
        // 処理無し
    }else{
        $stmt = db2_prepare($db2con, "CALL " . MAINLIB ."/PHPQRYA(?,?)");
        if($stmt === false){
            e_log('prepare失敗');
            e_log("①" . db2_stmt_errormsg());
            $rtn = false;
        }else{
            $in_param1 = '*NOMAX';
            $in_param2 = '*NOMAX';

            // ストアドへのパラメータバインド
            $retBool = db2_bind_param($stmt, 1, "in_param1", DB2_PARAM_IN);
            $retBoo4 = db2_bind_param($stmt, 2, "in_param2", DB2_PARAM_IN);

            $ret = db2_execute($stmt);

            if($ret === false){
                e_log('execute失敗');
                e_log('②'.db2_stmt_errormsg());
                $rtn = false;
            }else{
                e_log('リターンコード='.$RTCD);
            }
        }
    }
    return $rtn;
}
