<?php

/**
 * 日付スラッシュ
 */
function format1($value){
    if ($value!= null) {
        $isNum = checkNum($value);
        if ($isNum === true) {

            $value = (string)$value;

            if (strlen($value) === 8) {
                $value = substr($value, 0, 4) . '/' . substr($value, 4, 2) . '/' . substr($value, 6, 2);
            } else if (strlen($value) === 6) {
                $value = substr($value, 0, 4) . '/' . substr($value, 4, 2);
            } 
        }
    }
    return $value;
}

/**
 * 日付スラッシュ(ゼロの場合はブランク)
 */

function format2($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value != null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                $value = $value . '';
                $value = cmHscDe($value);
                if (strlen($value) == 8) {
                    $value = substr($value, 0, 4) . '/' . substr($value, 4, 2) . '/' . substr($value, 6, 2);
                } else if (strlen($value) == 6) {
                    $value = substr($value, 0, 4) . '/' . substr($value, 4, 2);
                }
            }
        }
        
        return $value;
    }
}

// 確認

/**
 * カンマ編集
 */

function format3($value)
{
    
    //    msoフォーマットでやる
    
    return $value;
}

/**
 * カンマ編集(ゼロの場合はブランク)
 */

function format4($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        return $value;
        
        //   MSOフォーマットでやる
        
    }
}

/**
 * ゼロの場合はブランク
 */

function format5($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        return $value;
    }
}

/**
 * 日付_YY/MM/DD
 */

function format6($value)
{
    if ($value != null) {
        $isNum = checkNum($value);
        if ($isNum === true) {
            $value = $value . '';
            $value = cmHscDe($value);
            //$value = sprintf('%06d',$value);
            $value = str_pad($value,6,'0',STR_PAD_LEFT);
            if (strlen($value) == 6) {
                $value = substr($value, 0, 2) . '/' . substr($value, 2, 2) . '/' . substr($value, 4, 2);
            }
        }
    }
    
    return $value;
}

/**
 * 日付_YYYY/MM
 */

function format7($value)
{
    if ($value != null) {
        $isNum = checkNum($value);
        if ($isNum === true) {
            $value = $value . '';
            $value = cmHscDe($value);
            if (strlen($value) == 6) {
                $value = substr($value, 0, 4) . '/' . substr($value, 4, 2);
            }
        }
    }
    
    return $value;
}

/**
 * 日付_YY/MM/DD(ゼロの場合はブランク)
 */

function format9($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value != null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                $value = $value . '';
                $value = cmHscDe($value);
                //$value = sprintf('%06d',$value);
                $value = str_pad($value,6,'0',STR_PAD_LEFT);
                if (strlen($value) == 6) {
                    $value = substr($value, 0, 2) . '/' . substr($value, 2, 2) . '/' . substr($value, 4, 2);
                }
            }
        }
        
        return $value;
    }
}

/**
 * 日付_YYYY/MM(ゼロの場合はブランク)
 */

function format0($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value != null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                $value = $value . '';
                $value = cmHscDe($value);
                if (strlen($value) == 6) {
                    $value = substr($value, 0, 4) . '/' . substr($value, 4, 2);
                }
            }
        }
        
        return $value;
    }
}

/**
 * 少数切捨て
 */

function formatA($value)
{
    $isNum = checkNum($value);
    if ($isNum === true) {
        $value = floatFormat($value, 0, 2);
    }
    
    return $value;
}

/*function convertFloor($value){
   $precision=0;
   $num = 10;
   $pow_num = pow($num,$precision);
   $value = $value * $pow_num;
   $value = floor($value);
   $value = $value / $pow_num;
   return $value;
}*/

/**
 * 時間_00:00:00
 */

function formatB($value)
{
    if ($value != null) {
        $isNum = checkNum($value);
        if ($isNum === true) {
            $value = $value . '';
            $value = cmHscDe($value);
            //$value = sprintf('%06d',$value);
            $value = str_pad($value,6,'0',STR_PAD_LEFT);
            if (strlen($value) == 6) {
                $value = substr($value, 0, 2) . ':' . substr($value, 2, 2) . ':' . substr($value, 4, 2);
            } else if (strlen($value) == 4) {
                $value = substr($value, 0, 2) . ':' . substr($value, 2, 2);
            }
        }
    }
    
    return $value;
}

/**
 * 時間_00:00
 */

function formatC($value)
{
    if ($value != null) {
        $isNum = checkNum($value);
        if ($isNum === true) {
            $value = $value . '';
            $value = cmHscDe($value);
            //$value = sprintf('%04d',$value);
            $value = str_pad($value,4,'0',STR_PAD_LEFT);
            if (strlen($value) == 4) {
                $value = substr($value, 0, 2) . ':' . substr($value, 2, 2);
            }
        }
    }
    
    return $value;
}

/**
 * 時間_00:00:00(ゼロ非表示)
 */

function formatD($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value != null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                $value = $value . '';
                $value = cmHscDe($value);
                //$value = sprintf('%06d',$value);
                $value = str_pad($value,6,'0',STR_PAD_LEFT);
                if (strlen($value) == 6) {
                    $value = substr($value, 0, 2) . ':' . substr($value, 2, 2) . ':' . substr($value, 4, 2);
                } else if (strlen($value) == 4) {
                    $value = substr($value, 0, 2) . ':' . substr($value, 2, 2);
                }
            }
        }
        
        return $value;
    }
}

/**
 * 時間_00:00(ゼロ非表示)
 */

function formatE($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value != null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                $value = $value . '';
                $value = cmHscDe($value);
                //$value = sprintf('%04d',$value);
                $value = str_pad($value,4,'0',STR_PAD_LEFT);
                if (strlen($value) == 4) {
                    $value = substr($value, 0, 2) . ':' . substr($value, 2, 2);
                }
            }
        }
        
        return $value;
    }
}

// F)小数切り捨て(ゼロ非表示)(111.1⇒111) & 0⇒''

function formatF($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value != null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                
                //                value = this.floatFormat(value,0,2,'1'); phpのfloatFormatで合わせ
                
                $value   = floatFormat($value, 0, 2);
                $zeroFlg = hiddenZero($value);
                if ($zeroFlg == '0') {
                    $value = '';
                }
            }
        }
        
        return $value;
    }
}

// G)小数切り捨て（カンマ）(1111.1⇒1,111)

function formatG($value)
{
    if ($value != null) {
        $isNum = checkNum($value);
        if ($isNum === true) {
            
            //                value = this.floatFormat(value,0,2,'1'); phpのfloatFormatで合わせ
            
            $value = floatFormat($value, 0, 2);
            
            //               value =  this.numformat(value); mso で作る
            
        }
    }
    
    return $value;
}

// H)小数切り捨て（カンマ_ゼロ非表示）(1111.1⇒1,111) & 0⇒''

function formatH($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value != null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                
                //                value = this.floatFormat(value,0,2,'1'); phpのfloatFormatで合わせ
                
                $value = floatFormat($value, 0, 2); //少数切捨て
                
                //               value =  this.numformat(value); mso で作る
                
                $zeroFlg = hiddenZero($value);
                if ($zeroFlg == '0') {
                    $value = '';
                }
            }
        }
        
        return $value;
    }
}

// I)小数切り上げ(111.1⇒112)

function formatI($value)
{
    if ($value != null) {
        $isNum = checkNum($value);
        if ($isNum === true) {
            $value = MathCeil($value);
        }
    }
    
    return $value;
}

// J)小数切り上げ(ゼロ非表示)(111.1⇒112) & 0⇒''

function formatJ($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value != null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                $value   = MathCeil($value);
                $zeroFlg = hiddenZero($value);
                if ($zeroFlg == '0') {
                    $value = '';
                }
            }
        }
        
        return $value;
    }
}

// K)小数切り上げ（カンマ）(1111.1⇒1,112)

function formatK($value)
{
    if ($value != null) {
        $isNum = checkNum($value);
        if ($isNum === true) {
            $value = MathCeil($value);
            
            //  value =  this.numformat(value); commonに作る
            
        }
    }
    
    return $value;
}

// L)小数切り上げ（カンマ_ゼロ非表示）(1111.1⇒1,112) & 0⇒''

function formatL($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value != null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                $value = MathCeil($value);
                
                //  value =  this.numformat(value); //common に作る
                
                $zeroFlg = hiddenZero($value);
                if ($zeroFlg == '0') {
                    $value = '';
                }
            }
        }
        
        return $value;
    }
}

// M)四捨五入(111.1⇒111、111.5⇒112)

function formatM($value)
{
    if ($value != null) {
        $isNum = checkNum($value);
        if ($isNum === true) {
            $value = MathRound($value);
        }
    }
    
    return $value;
}

// N)四捨五入（ゼロ非表示）(111.1⇒111、111.5⇒112) & 0⇒''

function formatN($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value != null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                $value   = MathRound($value);
                $zeroFlg = hiddenZero($value);
                if ($zeroFlg == '0') {
                    $value = '';
                }
            }
        }
        
        return $value;
    }
}

// O)四捨五入（カンマ）(1111.1⇒1,111、1111.5⇒1,112)

function formatO($value)
{
    if ($value != null) {
        $isNum = checkNum($value);
        if ($isNum === true) {
            $value = MathRound($value);
            
            //  value =  this.numformat(value); commonに作る
            
        }
    }
    
    return $value;
}

// P)四捨五入（カンマ_ゼロ非表）(111.1⇒1,111、111.5⇒1,112) & 0⇒''

function formatP($value)
{
    $zeroFlg = hiddenZero($value);
    if ($zeroFlg == '0') {
        return '';
    } else {
        if ($value !== null) {
            $isNum = checkNum($value);
            if ($isNum === true) {
                $value = MathRound($value);
                
                //  value =  this.numformat(value); commonに作る
                
                $zeroFlg = hiddenZero($value);
                e_log('zero data'.$zeroFlg);
                if ($zeroFlg == '0') {
                    $value = '';
                }
            }
        }
        
        return $value;
    }
}

/***
 **小数切り上げ
 **/

function MathCeil($number)
{
    /*      $number = (String)$number;
    if (strpos($number, '.') !== false) {
    $numArr = explode(".", (string)$number);
    if ((int)$numArr[1] > 0) {
    $number = (int)$numArr[0] + 1;
    }
    else {
    $number = (int)$numArr[0];
    }
    }
    */
    $number = ceil($number);
    return $number;
}

/***
 **四捨五入
 **/

function MathRound($number)
{
    /*      $number = (String)$number;
    if (strpos($number, '.') !== false) {
    $numArr = explode(".", (string)$number);
    if ((int)$numArr[1] > 4) {
    $number = (int)$numArr[0] + 1;
    }
    else {
    $number = (int)$numArr[0];
    }
    }
    */
    // SYPS 追加
    if (strpos($number, '-') !== false) {
       $number = round($number,0,PHP_ROUND_HALF_DOWN);
    }
    else{
        $number = round($number,0,PHP_ROUND_HALF_UP);
    }
    return $number;
}

/*
 *
 *フォマッとのゼロ非表示
 *
 */
function hiddenZero($number)
{
    $rtn = '1';
    $num = explode(".", $number);
    if (count($num) > 1) {
        if ((int) $num[0] === 0 || (int) $num[0] === -0 || (int) $num[0] === '-' || (int) $num[0] === '') {
            if ((int) $num[1] === 0) {
                $rtn = '0';
            }
        }
    } else {
        //20170113 SYPS 追加 (int)でtypecastした
        if ((int)$number === '0' || (int)$number === 0 || (int)$number === '-0' || (int)$number === -0) {
            $rtn = '0';
        }
    }
    return $rtn;
}