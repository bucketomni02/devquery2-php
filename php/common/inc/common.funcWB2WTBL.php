<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : 共通
* PROGRAM NAME   : 共通日付、時間、タイムスタンプ
* PROGRAM ID     : common.funcWB2WTBL.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/02/29
* MODIFY DATE    : 
* ============================================================
**/
//利用しているシステムテーブルからワークテーブル情報取得
function cmGetWTBLInfo($db2con,$wtblnm){
    $data = array();
    // last altered timestamp取得
    $strSQL  =  ' SELECT ';
    $strSQL .=  '     TABLE_NAME,VARCHAR_FORMAT(LAST_ALTERED_TIMESTAMP, \'YYYY-MM-DD HH24:MI:SS\') AS CREATE_TIME ';
    $strSQL .=  ' FROM ';
    $strSQL .=  '     QSYS2/SYSTABLES ';
    $strSQL .=  ' WHERE ';
    $strSQL .=  '     TABLE_SCHEMA = \''.SAVE_DB.'\' AND ';
    $strSQL .=  '     TABLE_NAME = ? ';
    $params = array($wtblnm);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('RESULT' => 'FAIL_SEL');
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
         $r = db2_execute($stmt,$params);
        if($r === false){
             $data = array('RESULT' => 'FAIL_SEL');
            e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                 $data = array('RESULT' => 'NO_DATA');
            }else{
                $data = umEx($data);
                $data = $data[0];
                $data = array('RESULT' => true,'DATA'=> $data);
            }
        }
    }
//    e_log('cmGetWTBLInfo:'.print_r($data,true));
    return $data;
}
//DB2WTBLに利用しているテーブル挿入
function cmInsWTBL($db2con,$wtblnm,$db2RDBCon = ''){
    $rtn = 0;
    // ワークテーブル情報取得
    // テーブル情報が既にあるかチェック
    // ある場合更新
    if($db2RDBCon === ''){
        $resWtbl = cmGetWTBLInfo($db2con,$wtblnm);
    }else{
        $resWtbl = cmGetWTBLInfo($db2RDBCon,$wtblnm);
    }
    if($resWtbl['RESULT'] !== true){
        $rtn = $resWtbl['RESULT'];
    }else{
        $resWtbl = $resWtbl['DATA'];
    }
    if($rtn === 0){
        $chkwtbl = cmGetDB2WTBL($db2con,$wtblnm);
        if($chkwtbl['RESULT'] !== true){
            $rtn = $chkwtbl['RESULT'];
        }else{
            $chkwtbl = $chkwtbl['DATA'];
            if(count($chkwtbl)>0){
                $rsdb2wtbl = cmUpdDB2WTBL($db2con,$resWtbl);
            }else{
                $rsdb2wtbl = cmInsDB2WTBL($db2con,$resWtbl);
            }
            if($rsdb2wtbl !== true){
                $rtn = $rsdb2wtbl;
            }
        }
    }
    return $rtn;
}
//DB2WTBLに利用しているテーブル削除
function cmDelWTBL($db2con,$wtblnm,$db2RDBCon = ''){
    $rtn = 0;
    // ワークテーブル情報取得
    // テーブル情報が既にあるかチェック
    // ある場合更新
    /*if($db2RDBCon === ''){
        $resWtbl = cmGetWTBLInfo($db2con,$wtblnm);
    }else{
        $resWtbl = cmGetWTBLInfo($db2RDBCon,$wtblnm);
    }
    if($resWtbl['RESULT'] !== true){
        $rtn = $resWtbl['RESULT'];
    }else{
        $resWtbl = $resWtbl['DATA'];
    }*/
    if($rtn === 0){
        $chkwtbl = cmGetDB2WTBL($db2con,$wtblnm);
        if($chkwtbl['RESULT'] !== true){
            $rtn = $chkwtbl['RESULT'];
        }else{
            if(count($chkwtbl['DATA'])>0){
                $rsdb2wtbl = cmDelDB2WTBL($db2con,$wtblnm);
                if($rsdb2wtbl !== true){
                    $rtn = $chkwtbl['RESULT'];
                }
            }
        }
    }
    return $rtn;
}
// DB2WTBL取得　チェックため
function cmGetDB2WTBL($db2con,$wtblnm){
    $data = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     WTBLNM, ';
    $strSQL .= '     WTBLCT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WTBL ';
    $strSQL .= ' WHERE WTBLNM = ? ';
    $params = array($wtblnm);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('RESULT' => 'FAIL_SEL');
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
         $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('RESULT' => 'FAIL_SEL');
            e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('RESULT' => true,'DATA'=>umEx($data));
        }
    }
    return $data;
}
// DB2WTBL更新
function cmUpdDB2WTBL($db2con,$db2wtbl){
    $result = true;

    $strSQL .= ' UPDATE ';
    $strSQL .= '     DB2WTBL ';
    $strSQL .= ' SET ';
    $strSQL .= '    WTBLCT = CURRENT TIMESTAMP ';
    $strSQL .= ' WHERE WTBLNM = ? ';
    $params = array(
                $db2wtbl['TABLE_NAME']
            );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $result =  'FAIL_UPD';
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
         $r = db2_execute($stmt,$params);
        if($r === false){
            $result =  'FAIL_UPD';
            e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }
    }
    return $result;
}
// DB2WTBL挿入
function cmInsDB2WTBL($db2con,$db2wtbl){
    $result = true;

    $strSQL .= ' INSERT INTO ';
    $strSQL .= '     DB2WTBL(WTBLNM,WTBLCT) ';
    $strSQL .= '    VALUES( ?,CURRENT TIMESTAMP) ';
    $params = array(
                 $db2wtbl['TABLE_NAME']
            );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $result = 'FAIL_INS';
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
         $r = db2_execute($stmt,$params);
        if($r === false){
            $result = 'FAIL_INS';
            e_log('失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }
    }
    return $result;
}
// DB2WTBL削除
function cmDelDB2WTBL($db2con,$wtblnm){
    $result = true;

    $strSQL .= ' DELETE  ';
    $strSQL .= ' FROM DB2WTBL ';
    $strSQL .= ' WHERE WTBLNM = ? ';
    $params = array(
                $wtblnm
            );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $result = 'FAIL_DEL';
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
         $r = db2_execute($stmt,$params);
        if($r === false){
            $result = 'FAIL_DEL';
            e_log('失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }
    }
    return $result;
}
