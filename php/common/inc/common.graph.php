<?php
/*
*-------------------------------------------------------* 
* ブックマークグラフが存在チェック いたらtrue
*-------------------------------------------------------*
*/

function cmCheckDB2BMK($db2con,$BMKDSBID,$BMKQID,$BMKGID){

    $data = array();
    $rtn = array();
    $rs = true;
    $strSQL  = ' SELECT * FROM DB2BMK AS A WHERE A.BMKDSBID = ? AND A.BMKQID = ? AND A.BMKGID = ?  ';
   $params = array($BMKDSBID,$BMKQID,$BMKGID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    $rtn = array('rs' => $rs, 'data' =>$data);
    return $rtn;
}
/*
*-------------------------------------------------------* 
* グラフが存在チェック いたらtrue
*-------------------------------------------------------*
*/

function cmCheckDB2GPK($db2con,$GPKQRY,$GPKID){

    $data = array();
    $rtn = array();
    $rs = true;
    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM  DB2GPK AS A ' ;
    $strSQL .= ' WHERE A.GPKQRY = ? ' ;
    $strSQL .= ' AND A.GPKID = ? ' ;
   $params = array(
        $GPKQRY,
        $GPKID
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    $rtn = array('rs' => $rs, 'data' =>$data);
    return $rtn;
}
function cmSelDB2GPK($db2con,$GPKQRY,$GPKID){
    $data = array();
        $params = array($GPKQRY,$GPKQRY,$GPKQRY,$GPKID);
        $strSQL = ' SELECT  B.GPKQRY, B.GPKID,B.GPKNM, B.GPKFM, B.GPKNUM, B.GPKDAT, B.GPKXCO,B.GPKXID,B.GPKXNM,B.GPKYNM,B.GPKCFG,A.D2HED AS GPKHED,B.GPKTBL,B.GPKDFG,B.GPJKCHK FROM DB2GPK AS B LEFT JOIN ';//MSM add GPKDFG 20180925
        $strSQL .= ' (SELECT D2NAME,D2FILID,D2FLD,D2HED,D2CSEQ,D2WEDT, ';
        $strSQL .= ' D2TYPE,D2LEN,D2DEC,D2DNLF ';
        $strSQL .= ' FROM FDB2CSV2 ';
        $strSQL .= ' WHERE D2NAME = ? ';
        $strSQL .= ' AND D2CSEQ > 0 ';
        $strSQL .= ' UNION ALL ';
        $strSQL .= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID, ';
        $strSQL .= ' D5FLD AS D2FLD,D5HED AS D2HED,D5CSEQ AS D2CSEQ, ';
        $strSQL .= ' D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DEC AS D2DEC,D5DNLF AS D2DNLF ';
        $strSQL .= ' FROM FDB2CSV5 ';
        $strSQL .= ' WHERE D5NAME = ? AND D5CSEQ > 0 ';
        $strSQL .= ' ) AS A ';
        $strSQL .= ' ON B.GPKQRY = A.D2NAME ';
        $strSQL .= ' AND B.GPKXCO = A.D2FLD ';
        $strSQL .= ' AND B.GPKXID = A.D2FILID ';
        $strSQL .= ' WHERE B.GPKQRY <> \'\' ';
        $strSQL .= ' AND B.GPKID <> \'\' ';
       $strSQL .= ' AND B.GPKQRY = ? ';
       $strSQL .= ' AND B.GPKID = ? ';
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            e_log('error:'.db2_stmt_errormsg());
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;
}

function cmSelDB2GPC($db2con,$GPCQRY,$GPCID){
    $data = array();
        $params = array($GPCQRY,$GPCQRY,$GPCQRY,$GPCID);
        $strSQL = ' SELECT B.GPCSEQ,B.GPCQRY,B.GPCID,B.GPCYCO,B.GPCYID,B.GPCLBL,B.GPCFM,B.GPCCAL,B.GPCCLR,B.GPCSOS,B.GPCSFG,A.D2HED AS GPCHED FROM DB2GPC AS B LEFT JOIN ';
        $strSQL .= '(SELECT D2NAME,D2FILID,D2FLD,D2HED,D2CSEQ,D2WEDT, ';
        $strSQL .= ' D2TYPE,D2LEN,D2DEC,D2DNLF ';
        $strSQL .= ' FROM FDB2CSV2 ';
        $strSQL .= ' WHERE D2NAME = ? ';
        $strSQL .= ' AND D2CSEQ > 0 ';
        $strSQL .= ' UNION ALL ';
        $strSQL .= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID, ';
        $strSQL .= ' D5FLD AS D2FLD,D5HED AS D2HED,D5CSEQ AS D2CSEQ, ';
        $strSQL .= ' D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DEC AS D2DEC,D5DNLF AS D2DNLF ';
        $strSQL .= ' FROM FDB2CSV5 ';
        $strSQL .= ' WHERE D5NAME = ? AND D5CSEQ > 0 ';
        $strSQL .= ' ) AS A ';
        $strSQL .= ' ON B.GPCQRY = A.D2NAME ';
        $strSQL .= ' AND B.GPCYCO = A.D2FLD ';
        $strSQL .= 'AND B.GPCYID = A.D2FILID  ';
        $strSQL .= ' WHERE B.GPCQRY <> \'\' ';
        $strSQL .= ' AND B.GPCID <> \'\' ';
        $strSQL .= ' AND B.GPCQRY = ? ';
        $strSQL .= ' AND B.GPCID = ? ';
//        $strSQL .= ' ORDER BY B.GPCSEQ ';
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
            e_log('error:'.db2_stmt_errormsg());
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;
}
function cmpivot_graphMerge($pivotFlg, $graphFlg){
    $strSQL = '';
    if($pivotFlg === true && $graphFlg === true) {//ピボットもグラフも両方あり
       $strSQL .= ' (';
       $strSQL .= ' SELECT PMNAME AS SUBQRY,PMPKEY AS SUBKEY,PMTEXT AS SUBNME,0 as SUBFLG from DB2PMST';
       $strSQL .= ' UNION';
       $strSQL .= ' SELECT GPKQRY  AS SUBQRY,GPKID AS SUBKEY,GPKNM AS SUBNME,1 as SUBFLG from DB2GPK ORDER BY SUBFLG';
       $strSQL .= ' ) AS SUB ';
    }else if($pivotFlg === true){//ピボットがあり、グラフがなし
       $strSQL .= ' (';
       $strSQL .= ' SELECT PMNAME AS SUBQRY,PMPKEY AS SUBKEY,PMTEXT AS SUBNME,0 as SUBFLG from DB2PMST';
       $strSQL .= ' ) AS SUB ';
    }else if($graphFlg === true){//ピボットがなり、グラフがあり
       $strSQL .= ' (';
       $strSQL .= ' SELECT GPKQRY  AS SUBQRY,GPKID AS SUBKEY,GPKNM AS SUBNME,1 as SUBFLG from DB2GPK ';
       $strSQL .= ' ) AS SUB ';
    }
    return $strSQL;
}
/**グラフのクエリーデータカウントを取得**/
function cmGetCountGphData($db2con,$filename,$d1name,$D1WEBF,$FLG,$licenseCl){
    $rtn = 0;
        $systables = cmGetSystables($db2con, SAVE_DB, $filename);
        if(count($systables) === 0){
            if($FLG === 'REFRESH'){
              $rs = cmCreateQryTable($db2con, $D1WEBF, $d1name ,$filename,$licenseCl);
                if($rs['RTN'] == 1){
                    $rtn = 1;
                    $data = array('result' => 'FAIL_SEL');
                }
            }else{
                $rtn = 1;
                $data = array('result' => 'FAIL_SEL');
            }
        }
    if($rtn === 0){
        $data = array();
        $count = 0;
        $strSQL ='';
        $strSQL .= ' SELECT COUNT(*) AS COUNT FROM ' . SAVE_DB . '/' . $filename ;
        $stmt = db2_prepare($db2con,$strSQL);
        $params = array();
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $count = $row['COUNT'];
                }
                $data = array('result' => true,'COUNT' => $count);
            }
        }
    }
    return $data;
}
/**グラフのクエリーデータを取得**/
function cmGetGphQueryData($db2con,$filename,$rowstart ,$rowend,$d1name,$D1WEBF,$FLG){
    $rtn = 0;
        $systables = cmGetSystables($db2con, SAVE_DB, $filename);
        if(count($systables) === 0){
            if($FLG === 'REFRESH'){
              $rs = cmCreateQryTable($db2con, $D1WEBF, $d1name ,$filename,$licenseCl);
                if($rs['RTN'] == 1){
                    $rtn = 1;
                     $data = array('result' => 'FAIL_SEL');
                }
            }else{
                    $rtn = 1;
                     $data = array('result' => 'FAIL_DATA');
            }
        }
    if($rtn === 0){
        $data = array();
        $strSQL ='';
        $strSQL .='SELECT B.* FROM';
        $strSQL .= ' (SELECT  A.*,ROWNUMBER() OVER() AS ROWNUM FROM ' . SAVE_DB . '/' . $filename .' AS A)' ;
        $strSQL .= ' AS B WHERE B.ROWNUM BETWEEN ? AND ? ';
        $stmt = db2_prepare($db2con,$strSQL);
        $params = array($rowstart,$rowend);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_array($stmt)){
                    unset($row[count($row) - 1]); //最後の列を削除
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    }
    return $data;
}

function cmGetGraphData($db2con,$filename,$gpk,$gpc,$filtersearchData=array()){
       $data = array();
       $params = array();
       $orderArr = array();
       $orderBy = '';
        $str = '';
        $groupBy = '';
        foreach($gpk as $key => $value){
              $str = $value['GPKXCO'].'_'.$value['GPKXID'].' AS '.$value['GPKXCO'].'_'.$value['GPKXID'];
               if($value['GPKCFG'] !== '1'){
                    $xcol = $value['GPKXCO'].'_'.$value['GPKXID'];
                    $groupBy = 'GROUP BY B.'.$xcol;
		    $orderBy = ' ORDER BY B.'.$xcol;
               }
        }
        //error_log("GPC 20181009 cmGetGraphData => ".print_r($gpc,true));
        foreach($gpc as $key => $value){
              if($value['GPCCAL'] === '1'){
                  $str .= ',SUM('.$value['GPCYCO'].'_'.$value['GPCYID'].') AS '.$value['GPCYCO'].'_'.$value['GPCYID'];
               }else if($value['GPCCAL'] === '2'){
                  $str .= ',MAX('.$value['GPCYCO'].'_'.$value['GPCYID'].') AS '.$value['GPCYCO'].'_'.$value['GPCYID'];
               }else if($value['GPCCAL'] === '3'){
                  $str .= ',MIN('.$value['GPCYCO'].'_'.$value['GPCYID'].') AS '.$value['GPCYCO'].'_'.$value['GPCYID'];
               }else  if($value['GPCCAL'] === '4'){
                  $str .= ',AVG('.$value['GPCYCO'].'_'.$value['GPCYID'].') AS '.$value['GPCYCO'].'_'.$value['GPCYID'];
               }else{
                  $str .= ','.$value['GPCYCO'].'_'.$value['GPCYID'].' AS '.$value['GPCYCO'].'_'.$value['GPCYID'];
               }
	       $GPCSOS = $value['GPCSOS'];
	       if($GPCSOS !== '' && $GPCSOS !== null){
			$ycol = $value['GPCYCO'].'_'.$value['GPCYID'];
			$s = $value['GPCSFG'];
			$sort = ' asc';
			if($s === '1'){
				$sort = ' desc ';
			}
			$orderArr[$GPCSOS]['YCOL'] = $ycol;
			$orderArr[$GPCSOS]['SORT'] = $sort;
	       }
        }
	ksort($orderArr);

	if(count($orderArr) > 0){
		$count = 0;
		$orderBy = ' ORDER BY ';
		foreach($orderArr as $key => $value){
			if($count !== 0){
				$orderBy .= ',';
			}
			$count += 1;
			$orderBy .= $value['YCOL'].' '.$value['SORT'];
		}
	}
        $rs = fngetSQLFldCCSID($db2con,$filename);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            //e_log('fldCCSIDresult'.print_r($rs,true));
            $fldCCSIDARR = $rs['data'];
            $fldccsidCnt = $rs['CCSIDCNT'];
            if($fldccsidCnt>0){
                $fldArr = array();
                foreach($fldCCSIDARR as $fldccsid){
                    if($fldccsid['CCSID'] === '65535'){

                        if($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW'){
                            $fldArr[] = 'CAST('.$fldccsid['COLUMN_NAME'].'  AS CHAR('.$fldccsid['LENGTH'].') CCSID 5026) AS '.$fldccsid['COLUMN_NAME'].'_0 ';
                        }else{
                            $fldArr[] = 'CAST('.$fldccsid['COLUMN_NAME'].'  AS CHAR('.$fldccsid['LENGTH'].') CCSID 5026) AS '.$fldccsid['COLUMN_NAME'].'_0 ';
                        }

                    }else{
                        $fldArr[] = $fldccsid['COLUMN_NAME']. ' AS '.$fldccsid['COLUMN_NAME']. '_0 ';
                    }
                }
            }else{
                foreach($fldCCSIDARR as $fldccsid){
                    $fldArr[] =  $fldccsid['COLUMN_NAME']. ' AS '.$fldccsid['COLUMN_NAME']. '_0 ';
                }
            }
        }
        if($str === ''){
            $str = join(',',$fldArr);
        }
        $strSQL = ' SELECT '.$str.'  from ' . SAVE_DB . '/' . $filename . ' as B ';
 		//for filter search
        if(count($filtersearchData)>0){
            
                $paramDSQL = array();
                $strSQL .= ' WHERE ';
                foreach ($filtersearchData as $key => $value) {
                      list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                      switch ($value['dtype']) {
                          case 'L':
                              $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . $value['D1OPERATOR'] . ' ? ';
                              break;
                          case 'T':
                              $paramDSQL[] = 'CAST(' .cmMer($D1COL2) . ' AS CHAR(8))' . $value['D1OPERATOR'] . ' ? ';
                              $paramDSQL[] = 'CAST(' .cmMer($D1COL2) . ' AS CHAR(8))' . $value['D1OPERATOR'] . ' ? ';
                              break;
                          case 'Z':
                              $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . $value['D1OPERATOR'] . ' ? ';
                              break;
                          default:
                              $paramDSQL[] = $D1COL2 . '_' .cmMer($D1COL1).  $value['D1OPERATOR'] . ' ? ';
                              break;
                }
                $params[] =cmMer($value['D1VAL']);
              }
                $strSQL .= join(' AND ',$paramDSQL);
            } 
        $strSQL .= $groupBy.$orderBy; 
        $stmt = db2_prepare($db2con,$strSQL);
         error_log('cmGetGraphData 実行SQL：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;
}
function cmGetGraphSQLData($db2con,$filename,$gpk,$gpc,$filtersearchData=array()){
       $data = array();
        $params = array();
       $orderArr = array();
        $str = '';
        $groupBy = '';
       $orderBy = '';
        foreach($gpk as $key => $value){
//              $str = $value['GPKXCO'] . ' AS '.$value['GPKXCO'];
              $str = $value['GPKXCO'].' AS '.$value['GPKXCO'].'_'.$value['GPKXID'];
               if($value['GPKCFG'] !== '1'){
                    $xcol = $value['GPKXCO'];
                    $groupBy = 'GROUP BY B.'.$xcol;
		    $orderBy = ' ORDER BY B.'.$xcol;
               }
        }
        foreach($gpc as $key => $value){
              if($value['GPCCAL'] === '1'){
                  $str .= ',SUM('.$value['GPCYCO'].') AS '.$value['GPCYCO'].'_'.$value['GPCYID'];
               }else if($value['GPCCAL'] === '2'){
                  $str .= ',MAX('.$value['GPCYCO'].') AS '.$value['GPCYCO'].'_'.$value['GPCYID'];
               }else if($value['GPCCAL'] === '3'){
                  $str .= ',MIN('.$value['GPCYCO'].') AS '.$value['GPCYCO'].'_'.$value['GPCYID'];
               }else  if($value['GPCCAL'] === '4'){
                  $str .= ',AVG('.$value['GPCYCO'].') AS '.$value['GPCYCO'].'_'.$value['GPCYID'];
               }else{
                  $str .= ','.$value['GPCYCO'].' AS '.$value['GPCYCO'].'_'.$value['GPCYID'];
                }
	       $GPCSOS = $value['GPCSOS'];
	       if($GPCSOS !== '' && $GPCSOS !== null){
			$ycol = $value['GPCYCO'].'_'.$value['GPCYID'];
			$s = $value['GPCSFG'];
			$sort = ' asc';
			if($s === '1'){
				$sort = ' desc ';
			}
			$orderArr[$GPCSOS]['YCOL'] = $ycol;
			$orderArr[$GPCSOS]['SORT'] = $sort;
	       }
        }
	ksort($orderArr);
	if(count($orderArr) > 0){
		$count = 0;
		$orderBy = ' ORDER BY ';
		foreach($orderArr as $key => $value){
			if($count !== 0){
				$orderBy .= ',';
			}
			$count += 1;
			$orderBy .= $value['YCOL'].' '.$value['SORT'];
		}
	}
        if($str === ''){
            $str = '*';
        }
        $strSQL = ' SELECT '.$str.'  from ' . SAVE_DB . '/' . $filename . ' as B ';
        //for filter search
        if(count($filtersearchData)>0){
            
                $paramDSQL = array();
                $strSQL .= ' WHERE ';
                foreach ($filtersearchData as $key => $value) {
                      list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                      switch ($value['dtype']) {
                          case 'L':
                              $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . $value['D1OPERATOR'] . ' ? ';
                              break;
                          case 'T':
                              $paramDSQL[] = 'CAST(' .cmMer($D1COL2) . ' AS CHAR(8))' . $value['D1OPERATOR'] . ' ? ';
                              $paramDSQL[] = 'CAST(' .cmMer($D1COL2) . ' AS CHAR(8))' . $value['D1OPERATOR'] . ' ? ';
                              break;
                          case 'Z':
                              $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . $value['D1OPERATOR'] . ' ? ';
                              break;
                          default:
                              $paramDSQL[] = $D1COL2 . $value['D1OPERATOR'] . ' ? ';
                              break;
                }
                $params[] =cmMer($value['D1VAL']);
              }
                $strSQL .= join(' AND ',$paramDSQL);
            } 
         $strSQL .= $groupBy.$orderBy; 

        $stmt = db2_prepare($db2con,$strSQL);
         e_log('実行SQL：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;
}
function cmCreateGphArray($data,$gpk,$gpc,$LINE,$AREA,$BAR,$GHEADER = array()){
        $PIE = array();
        $LINEARR = array();
        $BARARR = array();
        $AREAARR = array();
        $PIEARR = array();
       foreach($data as $key => $value){
            foreach($value as $k => $val){
                   $xcol = $gpk[0]['GPKXCO'].'_'.$gpk[0]['GPKXID'];
                    if($k !== $xcol){
                        $val = cmMer($val);
                        if(strpos($val, '.') !== false) {
                            $val = (float)$val;
                        }else{
                            $val = (int)$val;
                        }
                       $ytype = $gpc[$k]['GTYPE'];
                        if($ytype === '1'){
                            $LINEARR[$key][$k] = $val;
                        }else if($ytype === '2'){
                            $BARARR[$key][$k] = $val;
                        }else if($ytype === '3'){
                            $AREAARR[$key][$k] = $val;
                        }else if($ytype === '4'){
                            $PIEARR[$key][$k] = $val;
                        }
                    }else{
                       $val = cmMer($val);
                       $LINEARR[$key][$k] = $val;
                       $BARARR[$key][$k] = $val;
                       $AREAARR[$key][$k] = $val;
                       $PIEARR[$key][$k] = $val;
                    }
            }
        }
        //error_log("LINEARR 20181009 => ".print_r($LINEARR,true));
        $gpctemp = $gpc;

        if(count($BARARR) > 0){
            foreach($BARARR as $key => $value){
                foreach($value as $k => $val){
                        $xcol = $gpk[0]['GPKXCO'].'_'.$gpk[0]['GPKXID'];
                        if($k !== $xcol){
                           $seq = $gpctemp[$k]['SEQ'];
                           $newkey = $seq.'_'.$k;
                            if(array_key_exists($k,$gpc)){
                               $gpc[$newkey] = $gpc[$k];
                                unset($gpc[$k]);
                            }
                        }else{
                           $newkey = $k;
                        }
                       array_push($BAR[$newkey],$val);
                }
            }
        }

        if(count($LINEARR) > 0){
            foreach($LINEARR as $key => $value){
                foreach($value as $k => $val){
                        $xcol = $gpk[0]['GPKXCO'].'_'.$gpk[0]['GPKXID'];
                        if($k !== $xcol){
                           $seq = $gpctemp[$k]['SEQ'];
                           $newkey = $seq.'_'.$k;
                            if(array_key_exists($k,$gpc)){
                               $gpc[$newkey] = $gpc[$k];
                                unset($gpc[$k]);
                            }
                        }else{
                           $newkey = $k;
                        }
                       array_push($LINE[$newkey],$val);
                }
            }
        }
        if(count($AREA) > 0){
            foreach($AREAARR as $key => $value){
                foreach($value as $k => $val){
                        $xcol = $gpk[0]['GPKXCO'].'_'.$gpk[0]['GPKXID'];
                        if($k !== $xcol){
                           $seq = $gpctemp[$k]['SEQ'];
                           $newkey = $seq.'_'.$k;
                            if(array_key_exists($k,$gpc)){
                               $gpc[$newkey] = $gpc[$k];
                                unset($gpc[$k]);
                            }
                        }else{
                           $newkey = $k;
                        }
                       array_push($AREA[$newkey],$val);
                }
            }
        }
         if(count($PIEARR) > 0){
               foreach($PIEARR as $key => $value){
                    foreach($value as $k => $val){
                           $xcol = $gpk[0]['GPKXCO'].'_'.$gpk[0]['GPKXID'];
                            if($xcol !== $k){
                                $PIE[$key]['YCOL'] = $val;
                            }else{
                                $PIE[$key]['XCOL'] = $val;
                            }
                    }
                }
        }
    $gphArray = array(
        'DATA' => $data,
        'GPK' => $gpk,
        'GPC' => $gpc,
        'LINE' => $LINE,
        'LINEARR' => $LINEARR,
        'BAR' => $BAR,
        'BARARR' => $BARARR,
        'AREA' => $AREA,
        'AREAARR' => $AREAARR,
        'PIE' => $PIE,
        'PIEARR' => $PIEARR,
         'GHEADER' => $GHEADER
    );
    return $gphArray;
}

function cmCreateGraphInfoFile($db2con,$GPHINFO){
    $data = $GPHINFO['DATA'];
    $GPK = $GPHINFO['GPK'];
    $GHEADER = $GPHINFO['GHEADER'];
//    $graphTitle = $GPK[0]['GPKNM'];
    $graphTitle = '';
   $chartInfo = array();
    $xAxisTitle = '';
    $yAxisTitle = '';
    if(count($GPK) > 0){
        $xAxisTitle = $GPK[0]['GPKXNM'];
        $yAxisTitle = $GPK[0]['GPKYNM'];
    }
      $GPC = $GPHINFO['GPC'];
      $chartInfo['lang']['noData'] = showMsg('FAIL_GPH_DATA');
      $chartInfo['exporting']['enabled'] = false;
      $chartInfo['exporting']['filename'] = $GPK[0]['GPKNM'];
      $chartInfo['exporting']['fallbackToExportServer'] = false;
      $chartInfo['credits']['enabled'] = false;
      if($GPK[0]['GPKFM'] !== '4'){ //piechartじゃない場合通る
                $LINE = $GPHINFO['LINE'];
                $LINEARR = $GPHINFO['LINEARR'];
                $BAR = $GPHINFO['BAR'];
                $BARARR = $GPHINFO['BARARR'];
                $AREA = $GPHINFO['AREA'];
                $AREAARR = $GPHINFO['AREAARR'];
                $dataList = array();
                $seriesData = array();
                $xData = array();
                $Xcolumn = $GPK[0]['GPKXCO'].'_'.$GPK[0]['GPKXID'];
                if(count($BARARR)>0){
                    $xData = $BAR[$Xcolumn];
                }else if(count($LINEARR)>0){
                    $xData = $LINE[$Xcolumn];
                }else if(count($AREAARR)>0){
                    $xData = $AREA[$Xcolumn];
                }
                if(count($BARARR) > 0){
                        foreach($BAR as $key => $value){
                            if($key !== $Xcolumn && count($BAR[$key]) > 0){
                                $bar_arr = array();
                                $bar_arr['type'] = 'column';
                                $bar_arr['name'] = $GPC[$key]['LABEL'];
                                $bar_arr['data'] = $BAR[$key];
                                $bar_arr['color'] = $GPC[$key]['COLOR'];
                                array_push($seriesData,$bar_arr);
                            }
                        }
                }
                if(count($LINEARR) > 0){
                        foreach($LINE as $key => $value){
                            if($key !== $Xcolumn && count($LINE[$key]) > 0){
                                $line_arr = array();
                                $line_arr['type'] = 'line';
                                $line_arr['name'] = $GPC[$key]['LABEL'];
                                $line_arr['data'] = $LINE[$key];
                                $line_arr['color'] = $GPC[$key]['COLOR'];
                                $line_arr['marker']['lineWidth'] = 2;
                                $line_arr['marker']['lineColor'] = $GPC[$key]['COLOR'];
                                $line_arr['marker']['fillColor'] = 'white';
                                array_push($seriesData,$line_arr);
                            }
                        }
                }
                if(count($AREAARR) > 0){
                        foreach($AREA as $key => $value){
                            if($key !== $Xcolumn && count($AREA[$key]) > 0){
                                $area_arr = array();
                                $area_arr['type'] = 'area';
                                $area_arr['name'] = $GPC[$key]['LABEL'];
                                $area_arr['data'] = $AREA[$key];
                                $area_arr['color'] = $GPC[$key]['COLOR'];
                                array_push($seriesData,$area_arr);
                            }
                        }
                }
                /**plotOptions**/
                $chartInfo['plotOptions']['series']['animation'] = false;
                /**plotOptions**/

                /**label**/
                $chartInfo['labels']['items'][0]['html']  = '';
                $chartInfo['labels']['items'][0]['style']['color'] = 'black';
                $chartInfo['labels']['items'][0]['style']['left'] = '50px';
                $chartInfo['labels']['items'][0]['style']['top'] = '18px';
                /**label**/
                /**legend**/
                $chartInfo['legend']['align'] = 'right';
                $chartInfo['legend']['layout'] = 'veritical';
                $chartInfo['legend']['verticalAlign'] = 'top';
                $chartInfo['legend']['x'] = 0;
                $chartInfo['legend']['y'] = 25;
                /**legend**/
                /**series**/
                $chartInfo['series'] = $seriesData;
                /**series**/
                /**title**/
                $chartInfo['title']['text'] = $graphTitle;
                /**title**/
                /**xAxis**/
                $chartInfo['xAxis']['title']['text'] = $xAxisTitle;
                $chartInfo['xAxis']['categories'] = $xData;
                /**xAxis**/
                /**yAxis**/
                $chartInfo['yAxis']['title']['text'] = $yAxisTitle;
                 /**yAxis**/
        }else{
                $PIE = $GPHINFO['PIE'];
                $PIEARR = $GPHINFO['PIEARR'];
                 $dataList = array();
                for($i = 0 ; $i< count($PIE) ; $i++){
                      $pieArr = array();
                      $pieArr['name'] =  $PIE[$i]['XCOL'];
                      $pieArr['y'] = $PIE[$i]['YCOL'];
                      array_push($dataList,$pieArr);
                }
                /**chart**/
                $chartInfo['chart']['plotBackgroundColor'] = null;
                $chartInfo['chart']['plotBorderWidth'] = null;
                $chartInfo['chart']['plotShadow'] = false;
                $chartInfo['chart']['type'] = 'pie';
                /**chart**/

                /**legend**/
                $chartInfo['legend']['align'] = 'right';
                $chartInfo['legend']['layout'] = 'vertical';
                $chartInfo['legend']['verticalAlign'] = 'top';
                $chartInfo['legend']['x'] = 0;
                $chartInfo['legend']['y'] = 25;
                /**legend**/
                /**plotOptions**/
                $chartInfo['plotOptions']['series']['animation'] = false;
                $chartInfo['plotOptions']['pie']['allowPointSelect'] = true;
                $chartInfo['plotOptions']['pie']['cursor'] = "pointer";
                $chartInfo['plotOptions']['pie']['dataLabels']['enabled'] = true;
                $chartInfo['plotOptions']['pie']['dataLabels']['format'] = "<b>{point.name}</b>: {point.percentage:.1f} %";
                $chartInfo['plotOptions']['pie']['dataLabels']['style']['color'] = "black";
                $chartInfo['plotOptions']['pie']['showInLegend'] = true;
                /**plotOptions**/
                /**series**/
                $chartInfo['series']['0']['colorByPoint'] = true;
                $chartInfo['series']['0']['data'] = $dataList;
                $chartInfo['series']['0']['name'] = $yAxisTitle;
                /**series**/
                /**subtitle**/
                $chartInfo['subtitle']['text'] = ''; ;
                /**subtitle**/
                /**title**/
                $chartInfo['title']['text'] = $graphTitle;
                /**title**/
                /**tooltip**/
                $chartInfo['tooltip']['pointFormat'] = "{series.name}: <b>{point.percentage:.1f}%</b>";
                /**tooltip**/
        }
        $CHARTDATA = array();
        $CHARTDATA['CHARTINFO'] = $chartInfo;
        $HEADER = cmGetFDB2CSV2($db2con,$GPK[0]['GPKQRY'],false,false,false);
        $HEADER = umEx($HEADER['data'],true);
        $CHARTDATA['HEADER'] = $HEADER;
        $errmsg = array();
        if(count($data) === 0){
            $msg = showMsg('NO_DATA');
            array_push($errmsg,$msg);
        }
        $CHARTDATA['ERRMSG'] = $errmsg;
        //結果String
        $str_json_format = json_encode($CHARTDATA);
        $textFileName =  BASE_DIR . "/php/graphfile/".$GPK[0]['GPKQRY'].'-'.$GPK[0]['GPKID'].'.txt';
        if (file_exists($textFileName)) {
            unlink($textFileName);
        }
        $txt = $str_json_format;
        $myfile = fopen($textFileName, "w") or die("Unable to open file!");
        fwrite($myfile, $txt);
        fclose($myfile);
        return $CHARTDATA;
}
/**クエリーテーブル削除**/
function cmDropQueryTable($db2con,$filename){
    $rtn = array();
    $rs  = '0';
    //構文
    $strSQL  = ' DROP TABLE ';
    $strSQL .= SAVE_DB.'/'.$filename;
    $params = array();
    $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            e_log("prepare fail " . db2_stmt_errormsg());
            $rtn = array(
                'result' => 'FAIL_SEL'
            );
        }else{
            $r = db2_execute($stmt,$params);
            if ($r === false) {
            e_log("execute fail " . db2_stmt_errormsg());
                $rtn = array(
                    'result' => 'FAIL_SEL'
                );
            } else {
                $rtn = array(
                    'result' => true
                );
            }
    }
    return $rtn;
}
function cmCreateGraphInfoData($db2con,$db2connet,$d1name,$filename,$GPHKEY,$D1CFLG = '',$currentDate = '',$currentTime = ''){
    $LINE = array();
    $BAR = array();
    $AREA = array();
    $PIE = array();
    $GHEADER = array();
    $rtn = 0;
    //グラフ存在チェック
        $rs = cmCheckDB2GPK($db2con,$d1name,$GPHKEY);
        if($rs['rs'] === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_UPD',array('グラフ'));
        }else if($rs['rs'] !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs,array('グラフ'));
        }
            if($rtn === 0){
                $rs = cmSelDB2GPK($db2con,$d1name,$GPHKEY);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result'],array('グラフ'));
                }else{
                    $gpk = umEx($rs['data'],true);
                    $xcol = $gpk[0]['GPKXCO'].'_'.$gpk[0]['GPKXID'];
                    foreach($gpk as $key => $val){
                        $gpk[$key][$xcol] = $val['GPKHED'];
                        $GHEADER[0][$xcol] = $val['GPKHED'];
                        //FILD=9999の場合は無条件でarray()
                        if ($gpk[0]['GPKXID'] === '9999') {
                            $checkRs = array();
                        } else {
                            $checkRs = cmCheckFDB2CSV2($db2con, $d1name, $val['GPKXID'], $val['GPKXCO']);
                        }
                        if ($checkRs === false) {
                            $rtn = 2;
                            //      $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                            $msg = showMsg('FAIL_SYS');
                        } else {
                            if (count($checkRs) === 0) {
                                $checkRs = cmCheckFDB2CSV5($db2con, $d1name, $val['GPKXCO']);
                                if ($checkRs === false) {
                                    $rtn = 2;
                                    //    $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                                    $msg = showMsg('FAIL_SYS');
                                    break;
                                } else {
                                    if (count($checkRs) === 0) {
                                        $rtn = 2;
                                        $msg = showMsg('PIVOT_NO_COLS');//'クエリーに登録されていないカラムが選択されています。クエリーの設定をご確認ください。';
                                    }
                                }
                            }
                        }
                    }
                    $BAR[$xcol] = array();
                    $LINE[$xcol] = array();
                    $AREA[$xcol] = array();
                    $rs = cmSelDB2GPC($db2con,$d1name,$GPHKEY);
                    if($rs['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rs['result'],array('グラフ'));
                    }else{
                        $gpc = array();
                        $gpcArr = umEx($rs['data'],true);
                        //error_log("gpcArr 20181009 cmCreateGraphInfoData => ".print_r($gpcArr,true));
                            foreach($gpcArr as $key => $val){
                                    if ($val['GPCYID'] === '9999') {
                                        $checkRs = array();
                                    } else {
                                        $checkRs = cmCheckFDB2CSV2($db2con, $d1name, $val['GPCYID'], $val['GPCYCO']); //クエリーカラム
                                    }
                                    if ($checkRs === false) {
                                        $rtn = 2;
                                        //      $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                                        $msg = showMsg('FAIL_SYS');
                                    } else {
                                        if (count($checkRs) === 0) {
                                            $checkRs = cmCheckFDB2CSV5($db2con, $d1name, $val['GPCYCO']);//結果フィールドカラム
                                            if ($checkRs === false) {
                                                $rtn = 2;
                                                //    $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                                                $msg = showMsg('FAIL_SYS');
                                                break;
                                            } else {
                                                if (count($checkRs) === 0) {
                                                    $rtn = 2;
                                                    $msg = showMsg('PIVOT_NO_COLS');//'クエリーに登録されていないカラムが選択されています。クエリーの設定をご確認ください。';
                                                }
                                            }
                                        }//count
                                    }//checkRs === false
                                 if($rtn === 0){
                                        $gpc_inArr = array();
                                        $gpcLbl =  ($val['GPCLBL'] !== '' ) ? $val['GPCLBL'] : $val['GPCHED'];
                                        $gpc_inArr['LABEL'] =  $gpcLbl;
                                        $gpc_inArr['COLOR'] = $val['GPCCLR'];
                                        $gpc_inArr['GTYPE'] = $val['GPCFM'];
                                        $gpc_inArr['SEQ'] = $val['GPCSEQ'];
                                        $ycol = $val['GPCYCO'].'_'.$val['GPCYID'];
                                        $gpc[$ycol] =$gpc_inArr;
                                        $GHEADER[$key+1][$ycol] = $gpcLbl;
                                        $ycol =$val['GPCSEQ'].'_' .$val['GPCYCO'].'_'.$val['GPCYID'];
                                        $BAR[$ycol] = array();
                                        $LINE[$ycol] = array();
                                        $AREA[$ycol] = array();
                                 }
                            }
                    }
                }
            }
        if($rtn === 0){
            if($D1CFLG === ''){
                $rs = cmGetGraphData($db2connet,$filename,$gpk,$gpcArr);
            }else{
                $rs = cmGetGraphSQLData($db2connet,$filename,$gpk,$gpcArr,$filtersearchData);
            }
            
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
                $filename = '';
            }//else{
                
              /*do{
                    $dbname = makeRandStr(10);
                    $systables = cmGetSystables($db2connet, SAVE_DB, $filename);
               }while(count($systables) > 0);
                $rs = cmCreateGphTable($db2connet,$dbname,$filename);
                e_log("filename".$dbname);
                if($rs['result'] === true){*/
                   /*     if($filename !== '' || $currentDate !== '' || $currentTime !== ''){
                                $r = cmUpdDBGPK($db2con,$d1name,$GPHKEY,$filename,$currentDate,$currentTime);
                                if($r['result'] !== true){
                                    $rtn = 1;
                                    $msg = showMsg($r['result']);
                                }else{
                                    $data = $rs['data'];
                                    $gphInfo = cmCreateGphArray($data,$gpk,$gpc,$LINE,$AREA,$BAR,$GHEADER);
                                }
                         }*/
               /* }else{
                        $rtn = 1;
                        $msg = showMsg($rs['result']);
                 }*/
              //}
        }
        if($filename !== '' || $currentDate !== '' || $currentTime !== ''){
                $r = cmUpdDBGPK($db2con,$d1name,$GPHKEY,$filename,$currentDate,$currentTime);
                if($r['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($r['result']);
                }else{
                    $data = $rs['data'];
                    $gphInfo = cmCreateGphArray($data,$gpk,$gpc,$LINE,$AREA,$BAR,$GHEADER);
                }
         }
        $data = array('MSG' => $msg ,'RTN' => $rtn , 'GPHINFO' => $gphInfo);
    return $data;
}
function cmCreateGphTable($db2con,$dbname,$filename,$D1CFLG = ''){
        $rtn = array();
        if($D1CFLG === '1'){
            $rs = fngetSQLFldCCSID($db2con,$filename);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                //e_log('fldCCSIDresult'.print_r($rs,true));
                $fldCCSIDARR = $rs['data'];
                $fldccsidCnt = $rs['CCSIDCNT'];
                if($fldccsidCnt>0){
                    $fldArr = array();
                    foreach($fldCCSIDARR as $fldccsid){
                        if($fldccsid['CCSID'] === '65535'){

                            if($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW'){
                                $fldArr[] = 'CAST('.$fldccsid['COLUMN_NAME'].'  AS CHAR('.$fldccsid['LENGTH'].') CCSID 5026) AS '.$fldccsid['COLUMN_NAME'].'_0 ';
                            }else{
                                $fldArr[] = 'CAST('.$fldccsid['COLUMN_NAME'].'  AS CHAR('.$fldccsid['LENGTH'].') CCSID 5026) AS '.$fldccsid['COLUMN_NAME'].'_0 ';
                            }

                        }else{
                            $fldArr[] = $fldccsid['COLUMN_NAME']. ' AS '.$fldccsid['COLUMN_NAME']. '_0 ';
                        }
                    }
                }else{
                    foreach ($fldCCSIDARR as $fldccsid) {
                        $fldArr[] =  $fldccsid['COLUMN_NAME']. ' AS '.cmMer($fldccsid['COLUMN_NAME']).'_0 ';
                    }
                }
            }
            $str = join(',',$fldArr);
        }else{
            $str = '*';
        }
        $strSQL  = '';
        $strSQL .=   ' CREATE TABLE';
        $strSQL .=   ' '.SAVE_DB.'/'.$dbname.' AS';
        
        $strSQL .=   ' (SELECT ';
        $strSQL .=   $str ;
        $strSQL .=   '  FROM '.SAVE_DB.'/'.$filename.' )';
        $strSQL .=   ' WITH DATA ';
        $stmt = db2_prepare($db2con,$strSQL);
        e_log('error:'.$strSQL.db2_stmt_errormsg ());
        $params = array();
        if($stmt === false){
              $rtn = array(
                    'result' => 'FAIL_SEL'
                );
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
              $rtn = array(
                    'result' => 'FAIL_SEL'
                );
            }else{
              $rtn = array(
                    'result' => true
                );
            }
        }
    return $rtn;
}
function cmUpdDBGPK($db2con,$GPKQRY,$GPKID,$GPKTBL,$currentDate,$currentTime){
    $rtn = array();
    //構文
    $strSQL = ' UPDATE DB2GPK ';
    $strSQL .= ' SET ';
    if($GPKTBL !== ''){
        $strSQL .= ' GPKTBL = ? ';
    }
    if($currentDate !== '' && $currentTime !== ''){
        if($GPKTBL !== ''){
                $strSQL .= ' ,';
        }
        $strSQL .= ' GPKJKD = ? ';
        $strSQL .= ' ,GPKJKT = ? ';
   }
    $strSQL .= ' WHERE ';
    $strSQL .= ' GPKQRY = ? ';
    $strSQL .= ' AND GPKID = ? ';
    $params = array($GPKTBL);
    if($currentDate !== '' && $currentTime !== ''){
        $currentDate = (int)str_replace('/','',$currentDate);
        $currentTime = (int)str_replace(':','',$currentTime);
        array_push($params,$currentDate,$currentTime);
   }
    array_push($params,$GPKQRY,$GPKID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            $rtn = array(
                'result' => true
            );
        }
    }
    return $rtn;
}

//creating table for graph
function cmCreateQryTable($db2con, $WEBF, $d1name,$dbname,$licenseCl,$D1CFLG = '')
{
    $currentDate = date('Ymd');
    $errMsg = '';
    $rtn = 0;
    $RTCD        = ' ';
    /******************************************************/
    if ($WEBF === '1') {
        if($D1CFLG === ''){
            $rs = fnBQRYCNDDAT($db2con, $d1name);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $errMsg = showMsg($rs['result']);
            } else {
                $data = $rs['data'];
            }
            $data = umEx($data);
            if (count($data) > 0) {
                $cnddatArr = fnCreateCndData($data);
                $cnddatArr  = cmCalendarDataParamWeb($currentDate, $cnddatArr);
                $resChkCond = chkCondParamData($db2con, $d1name, $cnddatArr);
                if ($resChkCond['RTN'] > 0) {
                    $rtn = 1;
                }else {
                    $cndParamList = $resChkCond['DATA']['BASEDCNDDATA'];
                }
            }
            if ($rtn === 0) {
                //$resExeSql = runExecuteSQL($db2con, $d1name);
                if (count($cndParamList) === 0) {
                    $resExeSql = runExecuteSQL($db2con, $d1name);
                } else {
                    $resExeSql = runExecuteSQL($db2con, $d1name, $cndParamList);
                }                
                if ($resExeSql['RTN'] !== 0) {
                    $rtn = 1;
                    $errMsg = $resExeSql['MSG'];
                } else {
                    $qryData = $resExeSql;
                    $exeSQL  = array(
                        'EXESQL' => $qryData['STREXECSQL'],
                        'LOGSQL' => $qryData['LOG_SQL']
                    );
                }
            }
        }else{
            if ($rtn === 0) {
                $res = fnGetBSQLCND($db2con, $d1name);
                if ($res['result'] !== true) {
                    $rtn = 1;
                    $msg = showMsg($res['result']);
                } else {
                    $data = $res['data'];
                }
            }
            $data = umEx($data);
            $cndParamList = array();
            //e_log('条件取得データ1111'.print_r($data,true));
            if (count($data) > 0) {
                $cnddatArr = $data;
                $cnddatArr  = cmCalendarDataParamSQL($currentDate, $cnddatArr);
                $resChkCond = chkSQLParamData($db2con, $d1name, $cnddatArr);
                if ($resChkCond['RTN'] > 0) {
                    $rtn = 1;
                }else{
                    $cndParamList = $resChkCond['DATA']['BASEFRMPARAMDATA'];
                    //e_log('条件取得データ122222'.print_r($cndParamList,true));
                }
            }
            if ($rtn === 0) {
                $resExeSql = getQrySQLData($db2con, $d1name);
                if ($resExeSql['RTN'] !== 0) {
                    $rtn = 1;
                    $errMsg = $resExeSql['MSG'];
                } else {
                    $qryData = $resExeSql['QRYDATA'];
                    /*$exeSQL  = array(
                        'EXESQL' => $qryData['STREXECSQL'],
                        'LOGSQL' => $qryData['LOG_SQL']
                    );*/
                }
            }
        }
    }
    
    if ($rtn === 0) {
        $rsCLChk = cmChkFDB2CSV1PG($db2con, $d1name);
        if ($rsCLChk['result'] !== true) {
            $rtn = 1;
            $msg = $rsCLChk['result'];
        } else {
            if (count($rsCLChk['data']) > 0) {
                $fdb2csv1pg = $rsCLChk['data'][0];
                if ($fdb2csv1pg['DGBLIB'] !== '' || $fdb2csv1pg['DGBPGM'] !== '') {
                    $DGBFLG = true;
                }
                if ($fdb2csv1pg['DGALIB'] !== '' || $fdb2csv1pg['DGAPGM'] !== '') {
                    $DGAFLG = true;
                }
            }
        }
    } else {
       $rtn = 1;
        //クエリー　＊クエリーの実行準備に失敗しました
        $errMsg = showMsg('FAIL_FUNC', array(
            'クエリーの実行準備'
        )); //"クエリーの実行準備に失敗しました。";
    }
    if ($rtn === 0) {
        if ($WEBF === '1') {
            if($D1CFLG === ''){
                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                    $db2LibCon = cmDb2ConLib($qryData['LIBLIST']);
                } else {
                    $_SESSION['PHPQUERY']['RDBNM'] = $qryData['SELRDB'];
                    $db2LibCon                     = cmDB2ConRDB($qryData['LIBLIST']);
                }
            }else{
                $fdb2csv1Info = $qryData['FDB2CSV1'][0];
                $libArr = preg_split ("/\s+/", $fdb2csv1Info['D1LIBL'] );
                $LIBLIST = join(' ',$libArr);
                if($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME){
                     $db2LibCon = cmDb2ConLib($LIBLIST);
                } else {
                    $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1Info['D1RDB'];
                    $db2LibCon = cmDB2ConRDB($LIBLIST);
                }
            }
        }
        /******************************************************/
        //【Int】
        if ($WEBF !== '1') {
            cmInt($db2con, $RTCD, $JSEQ, $d1name);
            $rs      = true;
            $dataArr = cmGetFDB2CSV3($db2con, $d1name);
            $dataArr = cmCalendarDataParam($currentDate, $dataArr);
            cmSetQTEMP($db2con);
            //初期画面の検索条件でFDB2CSV3を更新
            $r  = updSearchCond($db2con, $dataArr, $d1name);
            $rs = $r[0];
            if ($rs === 1) {
                //クエリー　＊クエリーの実行準備に失敗しました
                $errMsg = showMsg('FAIL_FUNC', array(
                    'クエリーの実行準備'
                )); //"クエリーの実行準備に失敗しました。";
                $rtn    = 1;
            }else{
                    cmSetPHPQUERY($db2con);
            }
        } else {
            if($D1CFLG === ''){
                if ($licenseCl === true) {
                    if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                        if ($DGBFLG === true || $DGAFLG === true) {
                            cmIntWC($db2LibCon, $RTCD, 'INT', $d1name);
                        }
                    } else {
                        $RTCD = ' ';
                    }
                }
            }else{
                if($licenseCl === true){
                    if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME){
                        if ($DGBFLG === true || $DGAFLG === true) {
                            cmIntWC($db2LibCon, $RTCD, 'INT', $d1name);
                        }
                    } else {
                        $RTCD = ' ';
                    }
                }
            }
        }
        if ($RTCD === ' ' || is_null($RTCD)) {
            // 【setCmd】
            if ($WEBF !== '1') {
                if ($licenseCl === true) {
                    if ($DGBFLG === true || $DGAFLG === true) {
                        setCmd($db2con, $d1name, $WEBF);
                    }
                }
            } else {
                if($D1CFLG === ''){
                    if ($licenseCl === true) {
                        if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                            if ($DGBFLG === true || $DGAFLG === true) {
                                setCmd($db2LibCon, $d1name, $WEBF, $cnddatArr);
                            }
                        }
                        if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                            if ($DGBFLG === true) {
                                cmIntWC($db2LibCon, $RTCD, 'BEF', $d1name);
                            }
                        } else {
                            //RDBのCL実行呼び出し;
                            if ($DGBFLG === true) {
                                $rsClExe = callClExecute($db2LibCon, $d1name, $data, 'BEF');
                                if ($rsClExe === 0) {
                                    $RTCD = ' ';
                                } else {
                                    $RTCD == '9';
                                }
                            }
                        }
                    }
                }else{
                    if($licenseCl === true){
                        if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME){
                            if ($DGBFLG === true || $DGAFLG === true) {
                                 setSQLCmd($db2LibCon, $d1name, $D1WEBF, $setCmdData);
                            }
                        }
                        if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME){
                            if ($DGBFLG === true) {
                                cmIntWC($db2LibCon, $RTCD, 'BEF', $d1name);
                            }
                        } else {
                            //RDBのCL実行呼び出し;
                            if ($DGBFLG === true) {
                                $rsClExe = callClExecute($db2LibCon, $d1name, $data, 'BEF');
                                if ($rsClExe === 0) {
                                    $RTCD = ' ';
                                } else {
                                    $RTCD == '9';
                                }
                            }
                        }
                    }
                }
            }
        }
        if (($RTCD === ' ' || is_null($RTCD)) === false) {
            $rtn    = 1;
            $errMsg = 'CL実行の失敗しました。';//CLJIKO
        }
        if ($rtn === 0) {
            // 【cmDoのRTCDが' 'の場合、WEBの場合は実行後CL】
            //$dbname = makeRandStr(10);//filename
            if ($WEBF !== '1') {
                cmDo($db2con, $RTCD, $JSEQ, $dbname, $d1name);
            } else {
                if($D1CFLG === ''){
                    if(count($qryData['MBRDATALST']) >0){
                        foreach($qryData['MBRDATALST'] as $mbrdata){
                            //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                            $rs = setFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                            if($rs !== true){
                                e_log('テーブル設定失敗');
                                $rtn = 1;
                                $msg = showMsg($rs);
                                break;
                            }
                        }
                    }
                    if($rtn === 0){
                        $resExec = execQry($db2LibCon, $qryData['STREXECSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'],$qryData['MBRDATALST']);
                        if ($resExec['RTN'] !== 0) {
                            $rtn    = 1;
                            $errMsg = showMsg($resExec['MSG']);
                        } else {
                            if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                $resCreateTbl = createTmpTable($db2LibCon, $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $dbname,$qryData['MBRDATALST']);
                            } else {
                                $resCreateTbl = createTmpTableRDB($db2con, $db2LibCon, $qryData['SELRDB'], $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $dbname,$qryData['MBRDATALST']);
                            }
                            if ($resCreateTbl['RTN'] !== 0) {
                                $rtn    = 1;
                                $errMsg = showMsg($resCreateTbl['MSG']);
                            } else {
                                if ($licenseCl === true) {
                                    if ($DGAFLG === true) {
                                        if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                            cmIntWC($db2LibCon, $RTCD, 'AFT', $d1name);
                                        } else {
                                            //RDBのCL実行呼び出し;
                                            $rsClExe = callClExecute($db2LibCon, $d1name, $data, 'AFT');
                                            if ($rsClExe === 0) {
                                                $RTCD = ' ';
                                            } else {
                                                $RTCD == '9';
                                            }
                                        }
                                    }
                                }
                                if ($RTCD !== ' ' && !is_null($RTCD)) {
                                    $rtn    = 1;
                                    //クエリー　＊CLの事後処理に失敗しました。
                                    $errMsg = "CLの事後処理に失敗しました。";//CLJIKOAFT
                                    e_log($errMsg);
                                }
                            }
                        }
                    }
                }else{
                    e_log('parame-ta:'.print_r($cndParamList,true));
                    if ($rtn === 0) {
                        if(count($qryData['BSQLCND']) > 0){
                            $paramArr = bindParamArr($cndParamList);
                        }else{
                            $paramArr = array();
                        }
                        $strSQL = $qryData['BSQLEXEDAT']['EXESQL'];
                        if(count($paramArr['WITHPARAMDATA'])>0){
                            $strSQL = bindWithParamSQLQry($strSQL,$paramArr['WITHPARAMDATA']);
                        }
                        $strExeSQLParam = bindParamSQLQry($strSQL,$paramArr['PARAMDATA']);
                        $rsExeSQL = execSQLQry($db2LibCon,$strExeSQLParam,$paramArr);
                        if($rsExeSQL['RTN'] !== 0){
                            $rtn = 1;
                            $msg = $rsExeSQL['MSG'];
                        }else{
                            $rsCreate = createSQLQryTbl($db2LibCon,$strExeSQLParam,$paramArr, $dbname);
                            if($rsCreate['RTN'] !== 0){
                                $rtn = 1;
                                $msg = $rsCreate['MSG'];
                            }else{
                                $dbname = $rsCreate['TMPTBLNM'];
                                if ($licenseCl === true) {
                                    if ($DGAFLG === true) {
                                        if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME){
                                            cmIntWC($db2LibCon, $RTCD, 'AFT', $d1name);
                                        } else {
                                            //RDBのCL実行呼び出し;
                                            $rsClExe = callClExecute($db2LibCon, $d1name, $data, 'AFT');
                                            if ($rsClExe === 0) {
                                                $RTCD = ' ';
                                            } else {
                                                $RTCD == '9';
                                            }
                                        }
                                    }
                                }
                                if ($RTCD !== ' ' && !is_null($RTCD)) {
                                    $rtn    = 1;
                                    //クエリー　＊CLの事後処理に失敗しました。
                                    $errMsg = "CLの事後処理に失敗しました。";//CLJIKOAFT
                                    e_log($errMsg);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if($WEBF === '1' && $D1CFLG === ''){
        if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
            cmDb2Close($db2LibCon);
        }
    }else if($WEBF === '1' && $D1CFLG <> ''){
        if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME){
            cmDb2Close($db2LibCon);
        }
    }
    $data = array('RTN' => $rtn , 'MSG' => $errMsg);
    return $data;
}
function cmUpdTime($GPKBFD,$GPKBFT,$CURRENTDATE,$CURRENTTIME){
        $updMsg = '';
        if((int)$GPKBFD !== 0 && (int)$GPKBFT !== 0){
                while(strlen($GPKBFT) < 4){
                        $GPKBFT = '0'.$GPKBFT;
                }
                $GPKBFT = formatC($GPKBFT);
                $GPKBFD = format1($GPKBFD);
                $diff = abs(strtotime($CURRENTDATE) - strtotime($GPKBFD));
                $years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
             if((int)$years > 0 || (int)$months > 0 || (int)$days > 0){
                    $msg = array();
                    if((int)$years > 0){
                            array_push($msg,$years,'年');
//                          $msg .= $years." 年";
                    }
                    if((int)$months > 0){
//                          $msg .= $months."月";
                            array_push($msg,$months,'月');
                    }
                    if((int)$days > 0){
//                          $msg .= $days."日";
                          array_push($msg,$days,'日');
                    }
                     $updMsg = showMsg("UPDJIKO", array($msg));
            }else{
                    $datetime1 = strtotime($GPKBFT);
                    $datetime2 = strtotime($CURRENTTIME);
                    $interval  = abs($datetime2 - $datetime1);
                    $minutes   = round($interval / 60);
                    if($minutes > 59){
                        $hours = floor($minutes / 60);
                        $minutes = ($minutes % 60);
                        $updMsg = showMsg("UPDJIKO", array(array($hours,"時間")));
                    }else{
                        $updMsg = showMsg("UPDJIKO", array(array($minutes,"分")));
                    }
            }
         }else{
                     $updMsg = showMsg("UPDJIKO", array(array(0,"分")));
        }
        return $updMsg;
}