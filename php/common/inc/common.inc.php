<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
clearstatcache();
include_once ('common.message.php');
include_once ('common.format.php');
include_once ('common.validationLTZ.php');
include_once ('common.funcWB2WTBL.php');
include_once ('common.graph.php');
include_once ('common.setFileMbr.php');
include_once ('common.createTmpTbl.php');
include_once ('common.chkVldQryUsr.php');
/*
include_once(PHP_DIR.'common/lib/spout/src/Spout/Autoloader/autoload.php');
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color; 
*/
clearstatcache();
/**common**/
session_start();
ini_set('error_log', SAVE_DIR . SISTEM . '/log.log');
//e_log('GET_IP common ###'.print_r($_SERVER,true));
$scptname = explode('/', $_SERVER['SCRIPT_NAME']);
if ($scptname[count($scptname) - 1] === 'exeMail.php' || $scptname[count($scptname) - 1] === 'monitor.php' || $scptname[count($scptname) - 1] === 'exeMailSend.php') {
    $rs = cmUtilTimeCheck();
    if ($rs['result'] !== true) {
        if ($rs['result'] === 'NO_UTIL_TIME') {
            if ($rs['teishiFlg'] !== '1') {
                exit();
            }
        }
    }
}
$chkUtil = false;
if ($_SESSION['PHPQUERY']['user'][0]['WUAUTH'] !== '1') {
    $chkUtil = true;
    $WUAUTH = $_SESSION['PHPQUERY']['user'][0]['WUAUTH'];
    if ($WUAUTH === '2') {
        $WUSAUT = $_SESSION['PHPQUERY']['user'][0]['WUSAUT'];
        $rsChk = cmChkKenGen('', '22', $WUSAUT);
        if ($rsChk['result'] !== true) {
            $chkUtil = true;
        } else {
            $chkUtil = false;
        }
    }
}
if ($_SESSION['PHPQUERY']['LOGIN'] === '1' && $chkUtil === true) {
    $rs = cmUtilTimeCheck();
    if ($rs['result'] !== true) {
        if ($rs['result'] === 'NO_UTIL_TIME') {
            $_SESSION['PHPQUERYTOUCH'] = array();
            $_SESSION['PHPQUERY'] = array();
            if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $rtn = array('COM_RTN' => '7777', 'COM_MSG' => showMsg('NO_UTIL_TIME', array($rs['fromTime'], $rs['toTime'])), 'COM_URL' => PROTOCOL . '://' . IP . '/' . SISTEM);
                echo (json_encode($rtn));
                exit();
            }
            /*
            else{
            header('Location:'.PROTOCOL.'://'.IP.'/'.SISTEM.'/php/utimpage.php');
            exit();
            }
            */
        }
    }
}
//e_log('***common.inc.php****3');
/*
 *-------------------------------------------------------*
 * ライセンス取得
 *-------------------------------------------------------*
*/
function cmGETSRLNBR($db2con) {
    $stmt = db2_prepare($db2con, "CALL " . MAINLIB . "/GETSRLNBR(?)");
    $return_value = "";
    $retBool = db2_bind_param($stmt, 1, "return_value", DB2_PARAM_OUT);
    $ret = db2_execute($stmt);
    $RTNVAL = 'return_value';
    $RTNVAL = $$RTNVAL;
    return $RTNVAL;
}
/**
 *ライブラリーリストを変更
 *PHP
 *
 */
function cmPHPCHGLIB($db2con, $libl) {
    $stmt = db2_prepare($db2con, "CALL " . MAINLIB . "/PHPCHGLIB(?,?)");
    $in_param1 = $libl;
    $return_value1 = "";
    // ストアドへのパラメータバインド
    $retBool = db2_bind_param($stmt, 1, "in_param1", DB2_PARAM_IN);
    $retBool = db2_bind_param($stmt, 2, "return_value1", DB2_PARAM_OUT);
    $ret = db2_execute($stmt);
    $RTCD = 'return_value1';
    $RTCD = $$RTCD;
    if ($RTCD == '9') {
        e_log('Change Library List Fail');
    } else {
        e_log('Change Library List Success');
    }
    return $RTCD;
}
/**
 *ライブラリーリストを変更
 *RDB
 *
 */
function cmRDBCHGLIBL($db2con, $libl) {
    $rs = true;
    //IBMi接続（データベース接続と共有）
    $conn = ToolkitService::getInstance($db2con, DB2_I5_NAMING_ON);
    $conn->setToolkitServiceParams(array('stateless' => true));
    //CCSIDをセット
    $conn->CLCommand('CHGLIBL LIBL(' . $libl . ')');
    //IBM i 切断
    //$conn->disconnect();
    return $rs;
}
/**
 *
 * SQLのSELECT結果の要素を全て右トリム
 *
 * @param  array 変換前
 * @return array 変換後
 */
function umEx($pArray, $htmlFlg = false) {
    $rtn = array();
    if (is_array($pArray)) {
        for ($i = 0;$i < count($pArray);$i++) {
            if (is_array($pArray[$i])) {
                $rtn[$i] = array();
                foreach ($pArray[$i] as $key => $val) {
                    if ($htmlFlg === true) {
                        $val = cmHsc($val);
                    }
                    $rtn[$i][$key] = um($val);
                }
            }
        }
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * FDB2CSV2からヘッダー配列を作成
 * (dnlFlg===true) ＝＞ＥＸＣＥＬ・ＣＳＶ出力の時すべての見出しを出すようにする
 *-------------------------------------------------------*
*/
function cmCreateHeaderArray($fdb2csv2, $db2wcol = array()) {
    //e_log('【削除】Headerarr:'.print_r($fdb2csv2,true));

    $rtn = array();
    //フィールドIDとフィールド名をキーにしてダウンロード可否フラグを持たせる配列
    $compFDB2CSV2 = comparsionFDB2CSV2($fdb2csv2);
  
    //カラム設定があればそれに対応したヘッダを作成
    if (count($db2wcol) > 0) {
        $wcol = $db2wcol;
        foreach ($wcol as $key => $value) {
            //       reset($wcol);
            // while (list($key, $value) = each($wcol)) {
            if ($value['WCHIDE'] === '0' && isset($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']])) {
                //array_push($rtn, $value['WCTEXT']);
                $rtn[] = $value['WCTEXT'];
            }
        }
    } else {
        foreach ($fdb2csv2 as $key => $value) {
            /*        reset($fdb2csv2);
             while (list($key, $value) = each($fdb2csv2)) {*/
            if ($value['D2DNLF'] !== '1') {
                //array_push($rtn, cmMer($value['D2HED']));
                $rtn[] = cmMer($value['D2HED']);
            }
        }
   }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * CLで作成されたDBを読み込み
 *@searchDrillListはピポットのトレイダウンの検索のテータ
 *-------------------------------------------------------*
*/
function cmGetDbFile($db2con, $fdb2csv2, $filename, $start, $length, $sortcol, $sortdir, $sortingcols = 0, $search, $db2wcol = array(), $dnlFlg = false, $D1CFLG = '', $EXCELVER = '', $searchDrillList = array(), $filtersearchData = array()) {
    $rtn = 0;
    $data = array();
    $params = array();
    $selecttype = array();
    $cntfmt = array();//サマリーカウントのアライメント

    error_log("このへcmGetDbFileんか********" . print_r($db2wcol, true) . 'FLG' . $D1CFLG);
    if ($rtn === 0) {
        $rsChk = chkFDB2CSV2WKTblCol($db2con, $filename, $fdb2csv2, $D1CFLG);
        if ($rsChk['result'] !== true) {
            $rtn = 1;
            $data = array('result' => $rsChk['result'] // recreate table error
            );
            error_log("ここ入ったね＜＞");
            
        }
    }
    if ($rtn === 0) {
        //フィールドIDとフィールド名をキーにしてダウンロード可否フラグを持たせる配列
        //e_log('【削除】FDB2SCV2データcmGetDbFile'.print_r($fdb2csv2,true));
        $compFDB2CSV2 = comparsionFDB2CSV2($fdb2csv2);
        // e_log('【削除】compFDB2CSV2データcmGetDbFile'.print_r($compFDB2CSV2,true));
        $strSQL = '';
        if ($D1CFLG === '') {
            $strSQL.= ' SELECT ';
            if (count($db2wcol) > 0) {
                $wcol = $db2wcol;
                foreach ($wcol as $key => $value) {
                    //array_push($selecttype, $value['WCTYPE']);
                    //ダウンロードフラグがtrueの場合、FDB2CSV2のダウンロードフラグを見る
                    if ($dnlFlg === true) {
                        if ($value['WCHIDE'] === '0' && isset($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']])) {
                            if ($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']] !== '1') {
                                $selecttype[] = $value['WCTYPE'];
                                $strSQL.= ' A.' . $value['WCINDX'];
                                $strSQL.= ' , ';
                            }
                        }
                    } else {
                        $strSQL.= ' A.' . $value['WCINDX'];
                        $strSQL.= ' , ';
                    }
                }
                $strSQL.= ' A.ROWNO ';
                //array_push($selecttype, "wcol");
                $selecttype[] = 'wcol';
            } else {
                if ($dnlFlg === true) {
                    foreach ($fdb2csv2 as $key => $value) {
                        //array_push($selecttype, $value['D2TYPE']);
                        if ($value['D2DNLF'] !== '1') {
                            $selecttype[] = $value['D2TYPE'];
                            $cntfmt[] = $value['SFGMES'];
                            $strSQL.= ' A.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                            $strSQL.= ' , ';
                        }
                    }
                    $strSQL.= ' A.ROWNO ';
                } else {
                    foreach ($fdb2csv2 as $key => $value) {
                        $strSQL.= ' A.' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']);
                        if (($key + 1) < count($fdb2csv2)) {
                            $strSQL.= ' , ';
                        }
                        //array_push($selecttype, $value['D2TYPE']);
                        $selecttype[] = $value['D2TYPE'];
                        $cntfmt[] = $value['SFGMES'];
                    }
                    $strSQL.= ' , A.ROWNO ';
                }
                //array_push($selecttype, "fdb2csv");
                $selecttype[] = 'fdb2csv';
            }
            $strSQL.= ' FROM( ';
            $strSQL.= ' SELECT B.* , ';
            $strSQL.= ' ROWNUMBER() OVER ( ';
            //ソート
            error_log("PPN 20180806 sortcol1".$sortcol);
            if ($sortcol !== "") {
                $sortColumn = $sortcol;
                $strSQL.= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
            }
            $strSQL.= ') ';
            $strSQL.= ' AS ROWNO ';
            $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as B ';
            $isWhereFlg = false;
            if ($search != '') {
                if (count($fdb2csv2) > 0) {
                    $isWhereFlg = true;
                    $strSQL.= ' WHERE  ( ';
                    $paramSQL = array();
                    foreach ($fdb2csv2 as $key => $value) {
                        switch ($value['D2TYPE']) {
                            case 'L':
                                $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . ' LIKE ? ';
                            break;
                            case 'T':
                                $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(8))' . ' LIKE ? ';
                            break;
                            case 'Z':
                                $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(26))' . ' LIKE ? ';
                            break;
                            default:
                                $paramSQL[] = cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
                            break;
                        }
                        //array_push($params, '%' . $search . '%');
                        $params[] = '%' . $search . '%';
                        /*if ($key < (count($fdb2csv2) - 1)) {
                        $strSQL .= ' OR ';
                        }*/
                    }
                    $strSQL.= join(' OR ', $paramSQL);
                    $strSQL.= ' ) ';
                }
            }
            if (count($searchDrillList) > 0) {
                $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
                if(!$isWhereFlg) $isWhereFlg = true;
                $paramDSQL = array();
                foreach ($searchDrillList as $key => $value) {
                    switch ($value['dtype']) {
                        case 'L':
                            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
                        break;
                        case 'T':
                            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
                        break;
                        case 'Z':
                            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
                        break;
                        default:
                            $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
                        break;
                    }
                    $params[] = cmMer($value["val"]);
                }
                $strSQL.= join(' AND ', $paramDSQL);
                $strSQL.= ' ) ';
            }
            if (count($filtersearchData) > 0) {
                $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
                $paramDSQL = array();
                foreach ($filtersearchData as $key => $value) {
                    list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                    switch ($value['dtype']) {
                        case 'L':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . $value['D1OPERATOR'] . ' ? ';
                        break;
                        case 'T':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . $value['D1OPERATOR'] . ' ? ';
                        break;
                        case 'Z':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . $value['D1OPERATOR'] . ' ? ';
                        break;
                        default:
                            $paramDSQL[] = $D1COL2 . '_' .cmMer($D1COL1).  $value['D1OPERATOR'] . ' ? ';
                        break;
                    }
                    $params[] = cmMer($value['D1VAL']);
                }
                $strSQL.= join(' AND ', $paramDSQL);
                $strSQL.= ' ) ';
            }
            $strSQL.= ' ) AS A ';
            //抽出範囲指定
            if (($start !== '') && ($length !== '')) {
                $strSQL.= ' WHERE A.ROWNO BETWEEN ? AND ? ';
                /*array_push($params, $start + 1);
                 array_push($params, $start + $length);*/
                $params[] = $start + 1;
                $params[] = $start + $length;
            }
            $strSQL.= ' ORDER BY A.ROWNO ';
        } else {
            if ($sortcol !== '') {
                $sortColArr = explode('_', $sortcol);
                if ($sortColArr[count($sortColArr) - 1] === '0') {
                    array_pop($sortColArr);
                    $sortcol = join($sortColArr);
                }
            }
            $strSQL.= ' SELECT ';
            if (count($db2wcol) > 0) {
                $wcol = umEx($db2wcol);
                foreach ($wcol as $key => $value) {
                    //array_push($selecttype, $value['WCTYPE']);
                    //ダウンロードフラグがtrueの場合、FDB2CSV2のダウンロードフラグを見る
                    if ($dnlFlg === true) {
                        if ($value['WCHIDE'] === '0' && isset($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']])) {
                            if ($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']] !== '1') {
                                $selecttype[] = $value['WCTYPE'];
                                $strSQL.= ' A.' . $value['WCFLD'] . '_' . $value['WCFILID'];
                                $strSQL.= ' , ';
                            }
                        }
                    } else {
                        $strSQL.= ' A.' . $value['WCFLD'] . '_' . $value['WCFILID'];
                        $strSQL.= ' , ';
                    }
                }
                $strSQL.= ' A.ROWNO ';
                //array_push($selecttype, "wcol");
                $selecttype[] = 'wcol';
            } else {
                //$fdb2csv2 = umEx($fdb2csv2);
                if ($dnlFlg === true) {
                    foreach ($fdb2csv2 as $key => $value) {
                        //array_push($selecttype, $value['D2TYPE']);
                        if ($value['D2DNLF'] !== '1') {
                            $selecttype[] = $value['D2TYPE'];
                            $cntfmt[] = $value['SFGMES'];
                            $strSQL.= ' A.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                            $strSQL.= ' , ';
                        }
                    }
                    $strSQL.= ' A.ROWNO ';
                } else {
                    $paramSQL = array();
                    foreach ($fdb2csv2 as $key => $value) {
                        $paramSQL[] = ' A.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                        /*if (($key + 1) < count($fdb2csv2)) {
                        $strSQL .= ' , ';
                        }*/
                        //array_push($selecttype, $value['D2TYPE']);
                        $selecttype[] = $value['D2TYPE'];
                        $cntfmt[] = $value['SFGMES'];
                    }
                    $strSQL.= join(' , ', $paramSQL);
                    $strSQL.= ' , A.ROWNO ';
                }
                array_push($selecttype, "fdb2csv");
            }
            $strSQL.= ' FROM( ';
            $strSQL.= ' SELECT  ';
            $rs = fngetSQLFldCCSID($db2con, $filename);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result']);
            } else {
                //e_log('fldCCSIDresult'.print_r($rs,true));
                $fldCCSIDARR = $rs['data'];
                $fldccsidCnt = $rs['CCSIDCNT'];
                if ($fldccsidCnt > 0) {
                    $fldArr = array();
                    foreach ($fldCCSIDARR as $fldccsid) {
                        if ($fldccsid['CCSID'] === '65535' && $fldccsid['DDS_TYPE'] !== '5' && $fldccsid['DDS_TYPE'] !== 'H') {
                            if ($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW') {
                                $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(4) CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                            } else {
                                $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                            }
                        } else {
                            $fldArr[] = $fldccsid['COLUMN_NAME'] . ' AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                        }
                    }
                } else {
                    foreach ($fdb2csv2 as $key => $value) {
                        $fldArr[] = $value['D2FLD'] . ' AS ' . cmMer($value['D2FLD']) . '_0 ';
                    }
                }
            }
            $strSQL.= join(',', $fldArr);
            $strSQL.= ' , ROWNUMBER() OVER ( ';
            //ソート
            if ($sortcol !== "") {
                $sortColumn = $sortcol;
                $strSQL.= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
            }
            $strSQL.= ') ';
            $strSQL.= ' AS ROWNO ';
            $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as B ';
            $isWhereFlg = false;
            if ($search != '') {
                if (count($fldCCSIDARR) > 0) {
                    $isWhereFlg = true;
                    $strSQL.= ' WHERE (  ';
                    $paramSQL = array();
                    foreach ($fldCCSIDARR as $key => $value) {
                        switch ($value['DDS_TYPE']) {
                            case 'L':
                                if ($value['CCSID'] == '65535') {
                                    $paramSQL[] = 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(10)  CCSID 5026)' . ' LIKE ? ';
                                } else {
                                    $paramSQL[] = 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(10) )' . ' LIKE ? ';
                                }
                            break;
                            case 'T':
                                if ($value['CCSID'] == '65535') {
                                    $paramSQL[] = 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(8) CCSID 5026)' . ' LIKE ? ';
                                } else {
                                    $paramSQL[] = 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(8))' . ' LIKE ? ';
                                }
                            break;
                            case 'Z':
                                if ($value['CCSID'] === '65535') {
                                    $paramSQL[] = ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(26) CCSID 5026)' . ' LIKE ? ';
                                } else {
                                    $paramSQL[] = ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(26))' . ' LIKE ? ';
                                }
                            break;
                            default:
                                if ($value['CCSID'] === '65535' && $value['DDS_TYPE'] !== '5' && $value['DDS_TYPE'] !== 'H') {
                                    if ($value['COLUMN_NAME'] === 'BREAKLVL' || $value['COLUMN_NAME'] === 'OVERFLOW') {
                                        $paramSQL[] = ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(4) CCSID 5026)' . ' LIKE ? ';
                                    } else {
                                        $paramSQL[] = ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(' . $value['LENGTH'] . ') CCSID 5026)' . ' LIKE ? ';
                                    }
                                } else {
                                    $paramSQL[] = $value['COLUMN_NAME'] . ' LIKE ? ';
                                }
                            break;
                        }
                        //array_push($params, '%' . $search . '%');
                        $params[] = '%' . $search . '%';
                    }
                    $strSQL.= join(' OR ', $paramSQL);
                    $strSQL.= ' ) ';
                }
            }
            if (count($searchDrillList) > 0) {
                $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
                if(!$isWhereFlg) $isWhereFlg = true;

                $paramDSQL = array();
                foreach ($searchDrillList as $key => $value) {
                    switch ($value['dtype']) {
                        case 'L':
                            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
                        break;
                        case 'T':
                            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
                        break;
                        case 'Z':
                            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
                        break;
                        default:
                            $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
                        break;
                    }
                    $params[] = cmMer($value["val"]);
                }
                $strSQL.= join(' AND ', $paramDSQL);
                $strSQL.= ' ) ';
            }
            if (count($filtersearchData) > 0) {
                $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
                $paramDSQL = array();
                foreach ($filtersearchData as $key => $value) {
                    list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                    switch ($value['dtype']) {
                        case 'L':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . $value['D1OPERATOR'] . ' ? ';
                        break;
                        case 'T':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . $value['D1OPERATOR'] . ' ? ';
                        break;
                        case 'Z':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . $value['D1OPERATOR'] . ' ? ';
                        break;
                        default:
                            $paramDSQL[] = $D1COL2 . $value['D1OPERATOR'] . ' ? ';
                        break;
                    }
                    $params[] = cmMer($value['D1VAL']);
                }
                $strSQL.= join(' AND ', $paramDSQL);
                $strSQL.= ' ) ';
            }
            $strSQL.= ' ) AS A ';
            //抽出範囲指定
            if (($start !== '') && ($length !== '')) {
                $strSQL.= ' WHERE A.ROWNO BETWEEN ? AND ? ';
                //array_push($params, $start + 1);
                //array_push($params, $start + $length);
                $params[] = $start + 1;
                $params[] = $start + $length;
            }
            $strSQL.= ' ORDER BY A.ROWNO ';
        }
        error_log("common.inc.phpのcmGetDbFile:" . $strSQL. print_r($params, true));
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $rtn = 1;
            $data = array('result' => 'FAIL_SEL'
            //normal sql error
            );
            error_log("statement fail " . db2_stmt_errormsg());

        } else {
            $r = db2_execute($stmt, $params);
            //e_log('実行SQL：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
            if ($r === false) {
                $rtn = 1;
                $data = array('result' => 'FAIL_SEL' 
                // recreate table error
                );
            } else {
                while ($row = db2_fetch_array($stmt)) {
                    unset($row[count($row) - 1]); //最後の列を削除
                    if ($dnlFlg === true && $EXCELVER === 'XLSX') {
                        foreach ($row as $key => $val) {
                            if ($selecttype[$key] === 'S' || $selecttype[$key] === 'P' || $selecttype[$key] === 'B' || $cntfmt[$key] === 'COUNT') {
                                $row[$key] = (float)$val;
                            } else {
                                $row[$key] = (string)$val;
                            }
                        }
                    }
                    $data[] = $row;
                }
                $data = array('result' => true, 'data' => $data, 'selecttype' => $selecttype);
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * CLでテスト作成されたDBを読み込み
 *-------------------------------------------------------*
*/
function cmGetDbFile_CL($db2con, $fdb2csv2, $filename, $start, $length, $sortcol, $sortdir, $sortingcols = 0, $search, $db2wcol = array(), $D1CFLG = '') {
   
    $data = array();
    $params = array();
    $selecttype = array();
    $rtn = 0;
    if ($rtn === 0) {
        $rsChk = chkFDB2CSV2WKTblCol($db2con, $filename, $fdb2csv2, $D1CFLG);
        if ($rsChk['result'] !== true) {
            $rtn = 1;
            $data = array('result' => $rsChk['result'] // recreate table error
            );
        }
    }
    if ($rtn === 0) {
        //フィールドIDとフィールド名をキーにしてダウンロード可否フラグを持たせる配列
        //e_log('【削除】FDB2SCV2データcmGetDbFile_CL'.print_r($fdb2csv2,true));
        $compFDB2CSV2 = comparsionFDB2CSV2($fdb2csv2);
        $strSQL = '';
        if ($D1CFLG === '') {
            $strSQL.= ' SELECT ';
            if (count($db2wcol) > 0) {
                $wcol = $db2wcol;
                $strSQL.= ' A.rownum ';
                foreach ($wcol as $key => $value) {
                    array_push($selecttype, $value['WCTYPE']);
                    $strSQL.= ' , ';
                    $strSQL.= ' A.' . $value['WCINDX'];
                }
                array_push($selecttype, "wcol");
            } else {
                $strSQL.= ' A.* ';
                foreach ($fdb2csv2 as $key => $value) {
                    array_push($selecttype, $value['D2TYPE']);
                }
                array_push($selecttype, "fdb2csv");
            }
            $strSQL.= ' FROM( ';
            $strSQL.= ' SELECT  ';
            $strSQL.= ' ROWNUMBER() OVER ( ';
            //ソート
            if ($sortcol !== "") {
                $sortColumn = $sortcol;
                $strSQL.= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
            }
            $strSQL.= ') ';
            $strSQL.= ' AS rownum ';
            $strSQL.= ', B.* ';
            /**ADDED**/
            /*$sort_ing = '';
            if ($sortdir !== '') {
            $sort_ing = ' ORDER BY Y.REC ' . $sortdir;
            }
            $strSQL .= ' FROM ( ';
            $strSQL .= ' SELECT ROWNUMBER() OVER (' . $sort_ing . ')  AS RS,Y.* FROM ';
            $strSQL .= ' (SELECT ROWNUMBER() OVER () AS REC,X.* FROM ' . SAVE_DB . '/' . $filename . '  AS X)  ';
            $strSQL .= ' AS Y  ';
            $strSQL .= ' )   as B ';*/
            $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . '  AS B ';
            //フィルター条件セット
            if ($search != '') {
                if (count($fdb2csv2) > 0) {
                    $strSQL.= ' WHERE ';
                    foreach ($fdb2csv2 as $key => $value) {
                        switch ($value['D2TYPE']) {
                            case 'L':
                                $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . ' LIKE ? ';
                            break;
                            case 'T':
                                $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(8))' . ' LIKE ? ';
                            break;
                            case 'Z':
                                $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(26))' . ' LIKE ? ';
                            break;
                            default:
                                $strSQL.= cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
                            break;
                        }
                        array_push($params, '%' . $search . '%');
                        if ($key < (count($fdb2csv2) - 1)) {
                            $strSQL.= ' OR ';
                        }
                    }
                }
            }
            $strSQL.= ' ) AS A ';
            //抽出範囲指定
            //    if (($start != '') && ($length != '')) {
            if (($start !== '') && ($length !== '')) {
                $strSQL.= ' WHERE A.rownum BETWEEN ? AND ? ';
                array_push($params, $start + 1);
                array_push($params, $start + $length);
            }
            $strSQL.= ' ORDER BY A.rownum ';
        } else {
            if ($sortcol !== '') {
                $sortColArr = explode('_', $sortcol);
                error_log('SortColArr:' . $sortcol . 'AAA' . print_r($sortColArr, true) . $sortColArr[count($sortColArr) - 1]);
                if ($sortColArr[count($sortColArr) - 1] === '0') {
                    array_pop($sortColArr);
                    $sortcol = join($sortColArr);
                }
            }
            $strSQL.= ' SELECT ';
            if (count($db2wcol) > 0) {
                $wcol = $db2wcol;
                foreach ($wcol as $key => $value) {
                    array_push($selecttype, $value['WCTYPE']);
                    $strSQL.= ' A.' . $value['WCINDX'];
                    $strSQL.= ' , ';
                }
                $strSQL.= ' A.rownum ';
                array_push($selecttype, "wcol");
            } else {
                $strSQL.= ' A.* ';
                foreach ($fdb2csv2 as $key => $value) {
                    array_push($selecttype, $value['D2TYPE']);
                }
                array_push($selecttype, "fdb2csv");
            }
            $strSQL.= ' FROM( ';
            $strSQL.= ' SELECT  ';
            $rs = fngetSQLFldCCSID($db2con, $filename);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result']);
            } else {
                //e_log('fldCCSIDresult'.print_r($rs,true));
                $fldCCSIDARR = $rs['data'];
                $fldccsidCnt = $rs['CCSIDCNT'];
                if ($fldccsidCnt > 0) {
                    $fldArr = array();
                    foreach ($fldCCSIDARR as $fldccsid) {
                        if ($fldccsid['CCSID'] === '65535') {
                            if (($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW')) {
                                $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(4) CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                            } else {
                                $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                            }
                        } else {
                            $fldArr[] = $fldccsid['COLUMN_NAME'] . ' AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                        }
                    }
                } else {
                    foreach ($fdb2csv2 as $key => $value) {
                        $fldArr[] = $value['D2FLD'] . ' AS ' . cmMer($value['D2FLD']) . '_0 ';
                    }
                }
            }
            $strSQL.= join(',', $fldArr);
            $strSQL.= ' , ROWNUMBER() OVER ( ';
            //ソート
            if ($sortcol !== "") {
                $sortColumn = $sortcol;
                $strSQL.= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
            }
            $strSQL.= ') ';
            $strSQL.= ' AS rownum ';
            /**ADDED**/
            /*$sort_ing = '';
            if ($sortdir !== '') {
            $sort_ing = ' ORDER BY Y.REC ' . $sortdir;
            }
            $strSQL .= ' FROM ( ';
            $strSQL .= ' SELECT ROWNUMBER() OVER (' . $sort_ing . ')  AS RS,Y.* FROM ';
            $strSQL .= ' (SELECT ROWNUMBER() OVER () AS REC,X.* FROM ' . SAVE_DB . '/' . $filename . '  AS X)  ';
            $strSQL .= ' AS Y  ';
            $strSQL .= ' )   as B ';*/
            $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . '  AS B ';
            //フィルター条件セット
            if ($search != '') {
                if (count($fldCCSIDARR) > 0) {
                    $strSQL.= ' WHERE ';
                    foreach ($fldCCSIDARR as $key => $value) {
                        switch ($value['DDS_TYPE']) {
                            case 'L':
                                if ($value['CCSID'] == '65535') {
                                    $strSQL.= 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(10)  CCSID 5026)' . ' LIKE ? ';
                                } else {
                                    $strSQL.= 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(10) )' . ' LIKE ? ';
                                }
                            break;
                            case 'T':
                                if ($value['CCSID'] == '65535') {
                                    $strSQL.= 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(8) CCSID 5026)' . ' LIKE ? ';
                                } else {
                                    $strSQL.= 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(8))' . ' LIKE ? ';
                                }
                            break;
                            case 'Z':
                                if ($value['CCSID'] === '65535') {
                                    $strSQL.= ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(26) CCSID 5026)' . ' LIKE ? ';
                                } else {
                                    $strSQL.= ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(26))' . ' LIKE ? ';
                                }
                            break;
                            default:
                                if ($value['CCSID'] === '65535') {
                                    if ($value['COLUMN_NAME'] === 'BREAKLVL' || $value['COLUMN_NAME'] === 'OVERFLOW') {
                                        $strSQL.= ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(4) CCSID 5026)' . ' LIKE ? ';
                                    } else {
                                        $strSQL.= ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(' . $value['LENGTH'] . ') CCSID 5026)' . ' LIKE ? ';
                                    }
                                } else {
                                    $strSQL.= $value['COLUMN_NAME'] . ' LIKE ? ';
                                }
                            break;
                        }
                        array_push($params, '%' . $search . '%');
                        if ($key < (count($fldCCSIDARR) - 1)) {
                            $strSQL.= ' OR ';
                        }
                    }
                }
            }
            $strSQL.= ' ) AS A ';
            //抽出範囲指定
            //    if (($start != '') && ($length != '')) {
            if (($start !== '') && ($length !== '')) {
                $strSQL.= ' WHERE A.rownum BETWEEN ? AND ? ';
                array_push($params, $start + 1);
                array_push($params, $start + $length);
            }
            $strSQL.= ' ORDER BY A.rownum ';
        }
        $stmt = db2_prepare($db2con, $strSQL);
        e_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        if ($stmt === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array('result' => 'FAIL_SEL');
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    //unset($row['ROWNUM']); //最後の列を削除
                    //unset($row['RS']); //最後の列を削除
                    $format_row = array();
                    //reset($row);
                    //while (list($key, $value) = each($row)) {
                    foreach ($row as $key => $value) {
                        $format_row[] = $value;
                    }
                    $data[] = $format_row;
                }
                $data = array('result' => true, 'data' => $data);
            }
        }
    }
    return $data;
}
//concat 0.0 in front of number
function str_join($num) {
    if (ZERO_FLG === '0') {
        if (strpos($num, '.') !== false) {
            $nu = explode('.', $num);
            if ($nu[0] === '') {
                return "0" . $num;
            } else {
                return $num;
            }
        } else {
            return $num;
        }
    } else {
        return $num;
    }
}
/*
 *-------------------------------------------------------*
 * CLで作成されたDBを読み込み
 *-------------------------------------------------------*
*/
function cmGetDbFileBurstItm($db2con, $fdb2csv2, $burstItmLst, $filename, $D1CFLG, $start, $length, $sortcol, $sortdir, $sortingcols = 0, $search, $db2wcol = array(), $dnlFlg = false) {
    //e_log('D1CFLG :'.$D1CFLG);
    //e_log('BURSTITM :'.print_r($burstItmLst,true));
    //e_log('filename :'.$filename);
    $DB2WAUL = array();
    $selecttype = array();
    $cntfmt = array();//サマリーカウントのアライメント
    $WABFLG = '';
    if ($burstItmLst['WABFLG'] !== '') {
        $WABFLG = $burstItmLst['WABFLG'];
        if ($WABFLG === '1') {
            $WLDATA = $burstItmLst['WLDATA'];
        }
    }
    $data = array();
    $params = array();
    //フィールドIDとフィールド名をキーにしてダウンロード可否フラグを持たせる配列
    //e_log('【削除】FDB2CSV2データ cmGetDbFileBurstItm:'.print_r($fdb2csv2,true));
    $compFDB2CSV2 = comparsionFDB2CSV2($fdb2csv2);
    $strSQL = '';
    e_log('SQL作成フラグ：' . $D1CFLG);
    if ($D1CFLG === '1') {
        $strSQL.= ' SELECT ';
        if (count($db2wcol) > 0) {
            $wcol = $db2wcol;
            foreach ($wcol as $key => $value) {
                //ダウンロードフラグがtrueの場合、FDB2CSV2のダウンロードフラグを見る
                if ($dnlFlg === true) {
                    if ($value['WCHIDE'] === '0' && isset($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']])) {
                        if ($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']] !== '1') {
                            $strSQL.= ' A.' . $value['WCINDX'];
                            $strSQL.= ' , ';
                        }
                    }
                } else {
                    //if($value['WCHIDE'] === '0'){
                    $strSQL.= ' A.' . $value['WCINDX'];
                    $strSQL.= ' , ';
                    //}
                    
                }
            }
            $strSQL.= ' A.ROWNUM ';
        } else {
            if ($dnlFlg === true) {
                foreach ($fdb2csv2 as $key => $value) {
                    if ($value['D2DNLF'] !== '1') {
                        //$strSQL .= ' A.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                        $strSQL.= ' A.' . $value['D2FLD'];
                        $strSQL.= ' , ';
                    }
                }
                $strSQL.= ' A.ROWNUM ';
            } else {
                $strSQL.= ' A.* ';
            }
        }
        $strSQL.= ' FROM( ';
        $strSQL.= ' SELECT ';
        $rs = fngetSQLFldCCSID($db2con, $filename);
        if ($rs['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($rs['result']);
        } else {
            //e_log('fldCCSIDresult'.print_r($rs,true));
            $fldCCSIDARR = $rs['data'];
            $fldccsidCnt = $rs['CCSIDCNT'];
            if ($fldccsidCnt > 0) {
                $fldArr = array();
                foreach ($fldCCSIDARR as $fldccsid) {
                    if ($fldccsid['CCSID'] === '65535') {
                        if ($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW') {
                            $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(4) CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'];
                        } else {
                            $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'];
                        }
                        // $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                        
                    } else {
                        //$fldArr[] = $fldccsid['COLUMN_NAME'] . ' AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                        $fldArr[] = $fldccsid['COLUMN_NAME'] . ' AS ' . $fldccsid['COLUMN_NAME'];
                    }
                }
            } else {
                foreach ($fdb2csv2 as $key => $value) {
                    //$fldArr[] = $value['D2FLD'] . ' AS ' . cmMer($value['D2FLD']) . '_0 ';
                    $fldArr[] = $value['D2FLD'] . ' AS ' . cmMer($value['D2FLD']);
                    $selecttype[] = $value['D2TYPE'];
                    $cntfmt[] = $value['SFGMES'];
                }
            }
        }
        $strSQL.= join(',', $fldArr);
        $strSQL.= ' , ROWNUMBER() OVER ( ';
        //ソート
        if ($sortcol !== "") {
            $sortColumn = $sortcol;
            $strSQL.= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
        }
        $strSQL.= ') ';
        $strSQL.= ' AS rownum ';
        $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as B ';
        if ($burstItmLst != '') {
            if ($D1CFLG === '1') {
                if (cmMer($burstItmLst['WABAFLD']) !== '') {
                    $strSQL.= ' WHERE ';
                    if ($WABFLG === '1') {
                        $str = '';
                        foreach ($WLDATA AS $KEY => $VALUE) {
                            $WLDATA = $VALUE;
                            $str.= ' B.' . cmMer($burstItmLst['WABAFLD']) . ' = ? OR';
                            array_push($params, cmMer($VALUE));
                        }
                        $strSQL.= rtrim($str, "OR");
                    } else if (cmMer($burstItmLst['WABAFR']) !== '' && cmMer($burstItmLst['WABATO']) !== '') {
                        $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . ' BETWEEN ? AND ?';
                        array_push($params, cmMer($burstItmLst['WABAFR']));
                        array_push($params, cmMer($burstItmLst['WABATO']));
                    } else if ($burstItmLst['WABAFR'] !== '') {
                        $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . ' = ?';
                        array_push($params, cmMer($burstItmLst['WABAFR']));
                    }
                }
            } else {
                if (cmMer($burstItmLst['WABAFLD']) !== '') {
                    $strSQL.= ' WHERE ';
                    if ($WABFLG === '1') {
                        $str = '';
                        foreach ($WLDATA AS $KEY => $VALUE) {
                            $WLDATA = $VALUE;
                            $str.= ' B.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' = ? OR';
                            array_push($params, cmMer($VALUE));
                        }
                        $strSQL.= rtrim($str, "OR");
                    } else if (cmMer($burstItmLst['WABAFR']) !== '' && cmMer($burstItmLst['WABATO']) !== '') {
                        $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' BETWEEN ? AND ?';
                        array_push($params, cmMer($burstItmLst['WABAFR']));
                        array_push($params, cmMer($burstItmLst['WABATO']));
                    } else if ($burstItmLst['WABAFR'] !== '') {
                        $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' = ?';
                        array_push($params, cmMer($burstItmLst['WABAFR']));
                    }
                }
            }
        }
        $strSQL.= ' ) AS A ';
        //抽出範囲指定
        if (($start !== '') && ($length !== '')) {
            $strSQL.= ' WHERE A.rownum BETWEEN ? AND ? ';
            array_push($params, $start + 1);
            array_push($params, $start + $length);
        }
        $strSQL.= ' ORDER BY A.rownum ';
        e_log('SQLのburstItm:' . $strSQL);
    } else {
        $strSQL.= ' SELECT ';
        if (count($db2wcol) > 0) {
            $wcol = $db2wcol;
            foreach ($wcol as $key => $value) {
                //ダウンロードフラグがtrueの場合、FDB2CSV2のダウンロードフラグを見る
                if ($dnlFlg === true) {
                    if ($value['WCHIDE'] === '0' && isset($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']])) {
                        if ($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']] !== '1') {
                            $strSQL.= ' A.' . $value['WCINDX'];
                            $strSQL.= ' , ';
                        }
                    }
                } else {
                    //if($value['WCHIDE'] === '0'){
                    $strSQL.= ' A.' . $value['WCINDX'];
                    $strSQL.= ' , ';
                    //}
                    
                }
            }
            $strSQL.= ' A.rownum ';
        } else {
            if ($dnlFlg === true) {
                foreach ($fdb2csv2 as $key => $value) {
                    if ($value['D2DNLF'] !== '1') {
                        $strSQL.= ' A.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                        $selecttype[] = $value['D2TYPE'];
                        $cntfmt[] = $value['SFGMES'];
                        $strSQL.= ' , ';
                    }
                }
                $strSQL.= ' A.rownum ';
            } else {
                $strSQL.= ' A.* ';
            }
        }
        $strSQL.= ' FROM( ';
        $strSQL.= ' SELECT B.* , ';
        $strSQL.= ' ROWNUMBER() OVER ( ';
        //ソート
        if ($sortcol !== "") {
            $sortColumn = $sortcol;
            $strSQL.= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
        }
        $strSQL.= ') ';
        $strSQL.= ' AS rownum ';
        $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as B ';
        if ($burstItmLst != '') {
            if ($D1CFLG === '1') {
                if (cmMer($burstItmLst['WABAFLD']) !== '') {
                    $strSQL.= ' WHERE ';
                    if ($WABFLG === '1') {
                        $str = '';
                        foreach ($WLDATA AS $KEY => $VALUE) {
                            $WLDATA = $VALUE;
                            $str.= ' B.' . cmMer($burstItmLst['WABAFLD']) . ' = ? OR';
                            array_push($params, cmMer($VALUE));
                        }
                        $strSQL.= rtrim($str, "OR");
                    } else if (cmMer($burstItmLst['WABAFR']) !== '' && cmMer($burstItmLst['WABATO']) !== '') {
                        $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . ' BETWEEN ? AND ?';
                        array_push($params, cmMer($burstItmLst['WABAFR']));
                        array_push($params, cmMer($burstItmLst['WABATO']));
                    } else if ($burstItmLst['WABAFR'] !== '') {
                        $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . ' = ?';
                        array_push($params, cmMer($burstItmLst['WABAFR']));
                    }
                }
            } else {
                if (cmMer($burstItmLst['WABAFLD']) !== '') {
                    $strSQL.= ' WHERE ';
                    if ($WABFLG === '1') {
                        $str = '';
                        foreach ($WLDATA AS $KEY => $VALUE) {
                            $WLDATA = $VALUE;
                            $str.= ' B.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' = ? OR';
                            array_push($params, cmMer($VALUE));
                        }
                        $strSQL.= rtrim($str, "OR");
                    } else if (cmMer($burstItmLst['WABAFR']) !== '' && cmMer($burstItmLst['WABATO']) !== '') {
                        $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' BETWEEN ? AND ?';
                        array_push($params, cmMer($burstItmLst['WABAFR']));
                        array_push($params, cmMer($burstItmLst['WABATO']));
                    } else if ($burstItmLst['WABAFR'] !== '') {
                        $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' = ?';
                        array_push($params, cmMer($burstItmLst['WABAFR']));
                    }
                }
            }
        }
        $strSQL.= ' ) AS A ';
        //抽出範囲指定
        if (($start !== '') && ($length !== '')) {
            $strSQL.= ' WHERE A.rownum BETWEEN ? AND ? ';
            array_push($params, $start + 1);
            array_push($params, $start + $length);
        }
        $strSQL.= ' ORDER BY A.rownum ';
    }
    e_log("burst item SQLこのへん1024" . $strSQL);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($stmt === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_array($stmt)) {
                unset($row[count($row) - 1]); //最後の列を削除
                if ($dnlFlg === true && EXCELVERSION === 'XLSX') {
                    foreach ($row as $key => $val) {
                        if ($selecttype[$key] === 'S' || $selecttype[$key] === 'P' || $selecttype[$key] === 'B' || $cntfmt[$key] === 'COUNT') {
                            $row[$key] = (float)$val;
                        } else {
                            $row[$key] = $val;
                        }
                    }
                }
                $data[] = $row;
            }
        }
    }
    //e_log('mail SQL :'.print_r($strSQL,true));
    //e_log('mail result :'.count($data));
    return $data;
}
/**
 *burstItem total count
 *
 *
 */
function cmGetAllCountBurstItm($db2con, $burstItmLst, $filename, $D1CFLG = '') {
    e_log('バーストカウント取得:' . $D1CFLG . ' ファイル名：' . $filename . ' バーストアイテム：' . print_r($burstItmLst, true));
    $DB2WAUL = array();
    $WABFLG = '';
    if ($burstItmLst !== '') {
        if ($burstItmLst['WABFLG'] !== '') {
            $WABFLG = $burstItmLst['WABFLG'];
            if ($WABFLG === '1') {
                $WLDATA = $burstItmLst['WLDATA'];
            }
        }
    }
    $data = array();
    $params = array();
    $strSQL = '';
    $strSQL.= ' SELECT COUNT(*) AS COUNT  ';
    $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as B ';
    if ($burstItmLst !== '') {
        if ($D1CFLG === '1') {
            if (cmMer($burstItmLst['WABAFLD']) !== '') {
                $strSQL.= ' WHERE ';
                if ($WABFLG === '1') {
                    $str = '';
                    foreach ($WLDATA AS $KEY => $VALUE) {
                        $WLDATA = $VALUE;
                        $str.= ' B.' . cmMer($burstItmLst['WABAFLD']) . ' = ? OR';
                        array_push($params, cmMer($VALUE));
                    }
                    $strSQL.= rtrim($str, "OR");
                } else if (cmMer($burstItmLst['WABAFR']) !== '' && cmMer($burstItmLst['WABATO']) !== '') {
                    $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . ' BETWEEN ? AND ?';
                    array_push($params, cmMer($burstItmLst['WABAFR']));
                    array_push($params, cmMer($burstItmLst['WABATO']));
                } else if ($burstItmLst['WABAFR'] !== '') {
                    $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . ' = ?';
                    array_push($params, cmMer($burstItmLst['WABAFR']));
                }
            }
        } else {
            if (cmMer($burstItmLst['WABAFLD']) !== '') {
                $strSQL.= ' WHERE ';
                if ($WABFLG === '1') {
                    $str = '';
                    foreach ($WLDATA AS $KEY => $VALUE) {
                        $WLDATA = $VALUE;
                        $str.= ' B.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' = ? OR';
                        array_push($params, cmMer($VALUE));
                    }
                    $strSQL.= rtrim($str, "OR");
                } else if (cmMer($burstItmLst['WABAFR']) !== '' && cmMer($burstItmLst['WABATO']) !== '') {
                    $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' BETWEEN ? AND ?';
                    array_push($params, cmMer($burstItmLst['WABAFR']));
                    array_push($params, cmMer($burstItmLst['WABATO']));
                } else if ($burstItmLst['WABAFR'] !== '') {
                    $strSQL.= 'B.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' = ?';
                    array_push($params, cmMer($burstItmLst['WABAFR']));
                }
            }
        }
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        e_log('burst Itm countERR' . db2_stmt_errormsg());
        e_log('burst Itm count' . $strSQL . print_r($params, true));
        $data = false;
    } else {
        e_log('KLYH strSQL⇒' . $strSQL . print_r($params, true));
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
            e_log('burst Itm countERR' . db2_stmt_errormsg());
            e_log('burst Itm count' . $strSQL . print_r($params, true));
        } else {
            while ($row = db2_fetch_array($stmt)) {
                $data = $row;
            }
        }
    }
    //e_log('burst Itm 結果'.print_r($data,true));
    return $data;
}
/*
 *-------------------------------------------------------*
 * ログインチェック
 *-------------------------------------------------------*
*/
function GetUsrinfo($db2con, $id, $password) {
    $data = array();
    $strSQL = ' SELECT A.* FROM DB2WUSR AS A ';
    $strSQL.= ' WHERE WUUID = ? ';
    $strSQL.= ' AND WUPSWD = ? ';
    $params = array($id, $password);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * ユーザー情報取得
 *-------------------------------------------------------*
*/
function GetDB2WUSR($db2con, $id, $password) {
    $data = array();
    $strSQL = ' SELECT A.*,B.* ';
    $strSQL.= ' FROM DB2WUSR AS A JOIN (SELECT WDUID,WDNAME FROM DB2WDEF WHERE WDNAME IN (SELECT D1NAME FROM FDB2CSV1)) AS B ';
    $strSQL.= ' ON A.WUUID = B.WDUID ';
    $strSQL.= ' WHERE WUUID = ? ';
    $strSQL.= ' AND WUPSWD = ? ';
    $params = array($id, $password);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * ログ書き込み
 *-------------------------------------------------------*
*/
//function cmInsertDB2WLOG($db2con, &$rs, $wluid, $wlkbn, $wlname,$jsql1,$jsql2)
function cmInsertDB2WLOG($db2con, &$rs, $wluid, $wlkbn, $wlname, $data, $kbnFlg, $sqlb = '') {
   
    $rs = '0';
    $wltime = date('His');
    $wlday = date('Ymd');
    //構文
    $strSQL = ' INSERT INTO DB2WLOG ';
    $strSQL.= ' ( ';
    $strSQL.= ' WLDAY, ';
    $strSQL.= ' WLTIME, ';
    $strSQL.= ' WLUID, ';
    $strSQL.= ' WLKBN, ';
    $strSQL.= ' WLNAME, ';
    $strSQL.= ' WLSQLB ';
    $strSQL.= ' ) ';
    $strSQL.= ' VALUES ';
    $strSQL.= ' (?,?,?,?,?,?) ';
    $params = array($wlday, $wltime, $wluid, $wlkbn, $wlname, $sqlb);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = '1';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = '1';
        } else {
            if ($kbnFlg === '1') {
                $insArrFArr = array();
                if (is_array($data) === true && count($data) !== 0) {
                    foreach ($data as $value) {
                        $insArr = array();
                        $insArr['SDDAY'] = $wlday;
                        $insArr['SDTIME'] = $wltime;
                        $insArr['SDUID'] = $wluid;
                        $insArr['SDMSEQ'] = $value['CNDIDX'];
                        $insArr['SDSTKB'] = '';
                        $insArr['SDANDOR'] = '';
                        $insArr['SDFLD'] = '';
                        $insArr['SDKBN'] = '';
                        if (count($value['CNDSDATA']) !== 0) {
                            foreach ($value['CNDSDATA'] as $value1) {
                                $insArr['SDSSEQ'] = $value1['CDATAIDX'];
                                if (count($value1['CNDDATA']) !== 0) {
                                    $firstKey = 0;
                                    foreach ($value1['CNDDATA'] as $value2) {
                                        if ($value1['CNDKBN'] === 'LIST' || $value1['CNDKBN'] === 'NLIST') {
                                            $insArr['SDCDCD'] = count($value1['CNDDATA']);
                                        } else {
                                            $insArr['SDCDCD'] = 1;
                                        }
                                        $insArr['SDDAT'] = $value2;
                                        //新しいのフィールド
                                        if ($firstKey === 0 && $wlkbn === "P") {
                                            //if($wlkbn==="P"){
                                            $insArr['SDSTKB'] = $value1['CNDTYP'];
                                            $insArr['SDANDOR'] = ($value1['CNDANDOR'] === "") ? $value['SQRYAO'] : $value1['CNDANDOR'];
                                            $insArr['SDFLD'] = $value1['CNDFLD'];
                                            $insArr['SDKBN'] = $value1['CNDKBN'];
                                        } else {
                                            $insArr['SDSTKB'] = '';
                                            $insArr['SDANDOR'] = '';
                                            $insArr['SDFLD'] = '';
                                            $insArr['SDKBN'] = '';
                                        }
                                        array_push($insArrFArr, $insArr);
                                        $firstKey++;
                                    }
                                }
                            }
                        }
                    }
                }
                foreach ($insArrFArr as $value) {
                    //構文
                    if ($rs === '0') {
                        $strSQL = ' INSERT INTO DB2WSDA ';
                        $strSQL.= ' ( ';
                        $strSQL.= ' SDDAY, ';
                        $strSQL.= ' SDTIME, ';
                        $strSQL.= ' SDUID, ';
                        $strSQL.= ' SDMSEQ, ';
                        $strSQL.= ' SDSSEQ,';
                        $strSQL.= ' SDCDCD, ';
                        $strSQL.= ' SDDAT, ';
                        $strSQL.= ' SDSTKB, ';
                        $strSQL.= ' SDANDOR, ';
                        $strSQL.= ' SDFLD, ';
                        $strSQL.= ' SDKBN ';
                        $strSQL.= ' ) ';
                        $strSQL.= ' VALUES ';
                        $strSQL.= ' (?,?,?,?,?,?,?,?,?,?,?) ';
                        $params = array($value['SDDAY'], $value['SDTIME'], $value['SDUID'], $value['SDMSEQ'], $value['SDSSEQ'], $value['SDCDCD'], $value['SDDAT'], $value['SDSTKB'], $value['SDANDOR'], $value['SDFLD'], $value['SDKBN']);
                        $stmt = db2_prepare($db2con, $strSQL);
                        if ($stmt === false) {
                            $rs = '1';
                        } else {
                            $r = db2_execute($stmt, $params);
                            if ($r === false) {
                                $rs = '1';
                            }
                        }
                    }
                }
            } else if ($kbnFlg === '0') {
                $insArrFKur = array();
                if (count($data) === 0) {
                    if ($rs === '0') {
                        $rsCND = fnGetFDB2CSV3HCND($db2con, $wlname);
                        if ($rsCND['result'] !== true) {
                            $rs = '1';
                            $msg = showMsg($rsCND['result']);
                        } else {
                            $data = $rsCND['data'];
                        }
                    }
                }
                if (count($data) !== 0) {
                    foreach ($data as $value) {
                        $insArrKur = array();
                        $insArrKur['SKDAY'] = $wlday;
                        $insArrKur['SKTIME'] = $wltime;
                        $insArrKur['SKUID'] = $wluid;
                        $insArrKur['SKSEQ'] = $value['D3JSEQ'];
                        $insArrKur['SKDAT'] = $value['D3DAT'];
                        $insArrKur['SDSTKB'] = '';
                        $insArrKur['SDANDOR'] = '';
                        $insArrKur['SDFLD'] = '';
                        $insArrKur['SDKBN'] = '';
                        array_push($insArrFKur, $insArrKur);
                    }
                }
                foreach ($insArrFKur as $value) {
                    //構文
                    if ($rs === '0') {
                        $strSQL = ' INSERT INTO DB2WSDK ';
                        $strSQL.= ' ( ';
                        $strSQL.= ' SKDAY, ';
                        $strSQL.= ' SKTIME, ';
                        $strSQL.= ' SKUID, ';
                        $strSQL.= ' SKSEQ, ';
                        $strSQL.= ' SKDAT ';
                        $strSQL.= ' ) ';
                        $strSQL.= ' VALUES ';
                        $strSQL.= ' (?,?,?,?,?) ';
                        $params = array($value['SKDAY'], $value['SKTIME'], $value['SKUID'], $value['SKSEQ'], $value['SKDAT']);
                        $stmt = db2_prepare($db2con, $strSQL);
                        if ($stmt === false) {
                            $rs = '1';
                        } else {
                            $r = db2_execute($stmt, $params);
                            if ($r === false) {
                                $rs = '1';
                            }
                        }
                    }
                }
            }
        }
    }
    /*if($rs === '0'){
    if($jsql1 !== ''){
    //構文
    $strSQL = ' INSERT INTO DB2SQL1 ';
    $strSQL .= ' ( ';
    $strSQL .= ' S1DAY, ';
    $strSQL .= ' S1TIME, ';
    $strSQL .= ' S1UID, ';
    $strSQL .= ' S1JSQL ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?,?,?) ';
    
    $params = array(
    $wlday,
    $wltime,
    $wluid,
    $jsql1
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
    $rs = '1';
    } else {
    $r = db2_execute($stmt, $params);
    if ($r === false) {
    $rs = '1';
    }
    }
    }
    }*/
    /*if($rs === '0'){
    if($jsql2 !== ''){
    //構文
    $strSQL = ' INSERT INTO DB2SQL2 ';
    $strSQL .= ' ( ';
    $strSQL .= ' S2DAY, ';
    $strSQL .= ' S2TIME, ';
    $strSQL .= ' S2UID, ';
    $strSQL .= ' S2JSQL ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?,?,?) ';
    
    $params = array(
    $wlday,
    $wltime,
    $wluid,
    $jsql2
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
    $rs = '1';
    } else {
    $r = db2_execute($stmt, $params);
    if ($r === false) {
    $rs = '1';
    }
    }
    }
    }*/
    $rtnArr = array(
                'RTN' => $rs ,
                'DATE' => $wlday,
                'TIME' => $wltime
                );
    //e_log(print_r($rtnArr,true));
    return $rtnArr;
}
/*
 *-------------------------------------------------------*
 * 指定小数点で切り上げ、切り捨て、四捨五入
 * $flg：1=切り上げ、2=切り捨て、3=四捨五入
 * ※四捨五入は未テスト。使用する時テスト
 *-------------------------------------------------------*
*/
function floatFormat($number, $n, $flg) {
  
    $rtn = '';
    $res = '';
    $numChk = 0;
    if ($number === '' || $number === null) {
        $numChk = 1;
    }
    if (checkNum($number) == false) {
        $numChk = 1;
    }
    if ($numChk === 0) {
        //小数の桁数を確認
        //nが元の小数より大きい場合は不足分の0を追加
        $sa = explode('.', (string)$number);
        if (count($sa) > 1) {
            /*   if(strlen($sa[0]) === 0){
            $number = '0'.$number;
            }else if(strlen($sa[0]) === 1 &&  strlen($sa[0]) === '-'){
            $number = $sa[0]. '0.'.$sa[1];
            }*/
            $deccount = strlen($sa[1]);
        } else {
            $deccount = 0;
        }
        if ($n > $deccount) {
            $zero = '';
            for ($i = $deccount;$i < $n;$i++) {
                $zero.= '0';
            }
            if ($deccount === 0) {
                $zero = '.' . $zero;
            }
            $number = (string)$number . $zero;
            $flg = 99;
        }
        $p = pow(10, $n);
        switch ($flg) {
            case 1:
                $res = ceil($number * $p) / $p;
            break;
            case 2:
                $number = (string)$number;
                $saa = explode('.', (string)$number);
                if (count($saa) === 2) {
                    $saa[1] = substr($saa[1], 0, $n);
                    if ($n === 0) {
                        // $res = $saa[0];
                        if (strpos($saa[0], '-') !== false) {
                            $seisuVa = explode('-', (string)$number);
                            if (!empty($seisuVa[1])) {
                                $res = '-' . ceil($seisuVa[1]);
                            } else {
                                $res = $saa[0];
                            }
                        } else {
                            if (!empty($saa[0])) {
                                $res = ceil($saa[0]);
                            } else {
                                $res = $saa[0];
                            }
                        }
                        if ($res === '' || $res === '-' || $res === '-0') {
                            $res = 0;
                        }
                    } else {
                        $res = $saa[0] . '.' . $saa[1];
                    }
                } else {
                    if (!empty($number)) {
                        $res = ceil($number);
                    } else {
                        $res = $number;
                    }
                }
            break;
            case 3:
                $res = round($number, $n);
            break;
            case 99:
                $res = $number;
            break;
        }
        //        $res = number_format($res,$n,'.','');
        $rtn = (string)$res;
        $resarr = explode('.', (string)$res);
        if (count($resarr) > 1) {
            if (strlen($resarr[1]) < $n) {
                for ($i = strlen($resarr[1]);$i <= ($n - strlen($resarr[1]));$i++) {
                    $rtn = $rtn . '0';
                }
            }
        } else {
            if ($n > 0) {
                $rtn = $rtn . '.';
                for ($i = 0;$i < $n;$i++) {
                    $rtn = $rtn . '0';
                }
            }
        }
      
        return $rtn;
         
    } else {
       
        return $number;
    }
}
//zeroFlg とBACKZEROFLGを見る
function back_front_numFlg($number) {
    $numarr = explode('.', $number);

    //含まれている場合、小数の桁数を確認。
    //nが元の小数より大きい場合は不足分の0を追加
    //zero_flg ０の場合にゼロ表示、１の場合はゼロ非表示
    if (count($numarr) > 1) {
        if (ZERO_FLG === '0') {
            if (strlen($numarr[0]) === 0) {
                $number = '0' . $number;
            } else if (strlen($numarr[0]) === 1 && $numarr[0] === '-') {
                $number = $numarr[0] . "0." . $numarr[1];
            }
        } else {
            if ($numarr[1] !== '') {
                if ($numarr[0] === '-0' || $numarr[0] === '-') {
                    $number = "-." . $numarr[1];
                } else if (count($numarr) > 1 && $numarr[0] === '0') {
                    $number = "." . $numarr[1];
                }
            }
        }
    }
    $numarr = explode('.', $number);

    if (BACKZERO_FLG === '0') {
        if (count($numarr) === 2) {
            if ($numarr[1] !== '') {
                $numarr[1] = preg_replace("/\.?0+$/", "", $numarr[1]);
            }
            if (strlen($numarr[1]) > 0) {
                $number = $numarr[0] . '.' . $numarr[1];
            } else {
                if ($numarr[0] === '') {
                    $number = 0;
                } else {
                    $number = $numarr[0];
                }
            }
        } else {
            $number = $number;
        }
    }
    
    return $number;
}
/***
 *create php excel setting
 *
 ***/
function php_excelCreateHeading($d1name, $NOTEMPLATE = 'TEMPLATE') {
    $D1SHET = 1;
    $D1TROS = 1;
    $D1TCOS = 0;
    $D1TMPF = '';
    $D1THFG = '';
    $D1EFLG = '';
    $D1ETYP = '';
    //フィールドIDとフィールド名をキーにしてダウンロード可否フラグを持たせる配列
    $fdb2csv1 = FDB2CSV1_DATA($d1name);
    if ($fdb2csv1['result'] === true) {
        $webf = um($fdb2csv1['data'][0]['D1WEBF']);
        $d1edkb = um($fdb2csv1['data'][0]['D1EDKB']);
        $D1SHET = um($fdb2csv1['data'][0]['D1SHET']);
        $D1TMPF = cmHsc($fdb2csv1['data'][0]['D1TMPF']);
        $D1TMPF = um($D1TMPF);
        $D1ETYP = cmHsc($fdb2csv1['data'][0]['D1ETYP']);
        $D1ETYP = um($D1ETYP);
        $D1EFLG = cmHsc($fdb2csv1['data'][0]['D1EFLG']);
        $D1EFLG = um($D1EFLG);
        //e_log("NOTEMPLATE1".$NOTEMPLATE.print_r($fdb2csv1,true));
        //      if(($D1EFLG === '2' && $D1ETYP !== '') || ($D1EFLG === '2' && $D1ETYP === '' && $NOTEMPLATE === 'NOTEMPLATE')){
        if ($NOTEMPLATE === 'NOTEMPLATE') {
            $D1TMPF = ''; //クエリー実行設定に選択されてるタイプがあったら、テンプレートの設定を無効にする
            
        }
        if ($D1TMPF !== '') {
            $D1TMPF = $D1TMPF;
        }
    }
    e_log("php_excelCreateHeading d1tmpf" . $D1TMPF);
    if ($D1TMPF !== '') {
        $ext = explode('.', $D1TMPF);
        $ext = $ext[count($ext) - 1];
        if ($ext === 'xls') {
            $reader = PHPExcel_IOFactory::createReader('Excel5');
        } else if ($ext === 'xlsx' || $ext === 'xlsm') {
            $reader = PHPExcel_IOFactory::createReader('Excel2007');
        }
        $reader->setIncludeCharts(TRUE);
        $load_url = BASE_DIR . '/php/template/' . $d1name . '.' . $ext;
        /*Ramon.. check excel type*/
        $inputFileType = PHPExcel_IOFactory::identify($load_url);
        //
        if ($inputFileType !== 'HTML') {
            $book = $reader->load($load_url);
            $sIndex = $D1SHET - 1;
            //シート数取得
            $sheetsCount = $book->getSheetCount();
            if ($sIndex < 0 || $sIndex >= $sheetsCount) {
                $sIndex = 0;
            }
            $book->setActiveSheetIndex($sIndex);
        }
    } else {
        $book = new PHPExcel();
        $book->setActiveSheetIndex(0);
    }
    /**/
    if ($inputFileType !== 'HTML') {
        $sheet = $book->getActiveSheet();
        $msg = '';
    } else {
        $msg = showMsg('FAIL_EXCELCHK');
    }
    $res = array("SHEET" => $sheet, "BOOK" => $book, "MSG" => $msg);
    return $res;
}
/*
 *-------------------------------------------------------*
 * エクセル作成
 *-------------------------------------------------------*
*/
function cmCreateXLS($sheet, $hdata, $data, $fdb2csv2, $dctext, $db2wcol = array(), $d1name, $DB2ECON, $DB2EINS, $CNDSDATA, $headerFlg = false, $row, $htmlFlg, $NOTEMPLATE = 'TEMPLATE') {
    e_log('cmCreateXLS');
    $dctext = replaceFilename($dctext);
    $D1SHET = 1;
    $D1TROS = 1;
    $D1TCOS = 0;
    $D1TMPF = '';
    $D1THFG = '';
    $D1EFLG = '';
    $D1ETYP = '';
    //フィールドIDとフィールド名をキーにしてダウンロード可否フラグを持たせる配列
    //e_log('【削除】FDB2CSV2データ cmCreateXLS:'.print_r($fdb2csv2,true));
    $compFDB2CSV2 = comparsionFDB2CSV2($fdb2csv2);
   
    $fdb2csv1 = FDB2CSV1_DATA($d1name);
    if ($fdb2csv1['result'] === true) {
        $webf = um($fdb2csv1['data'][0]['D1WEBF']);
        $d1edkb = um($fdb2csv1['data'][0]['D1EDKB']);
        $D1TMPF = cmHsc($fdb2csv1['data'][0]['D1TMPF']);
        $D1TMPF = um($D1TMPF);
        $D1ETYP = cmHsc($fdb2csv1['data'][0]['D1ETYP']);
        $D1ETYP = um($D1ETYP);
        $D1EFLG = cmHsc($fdb2csv1['data'][0]['D1EFLG']);
        $D1EFLG = um($D1EFLG);
        //e_log("NOTEMPLATE2".$NOTEMPLATE.print_r($fdb2csv1,true));
        //      if(($D1EFLG === '2' && $D1ETYP !== '') || ($D1EFLG === '2' && $D1ETYP === '' && $NOTEMPLATE === 'NOTEMPLATE')){
        if ($NOTEMPLATE === 'NOTEMPLATE') {
            $D1TMPF = ''; //クエリー実行設定に選択されてるタイプがあったら、テンプレートの設定を無効にする
            
        }
        if ($D1TMPF !== '') {
            $D1TMPF = $D1TMPF;
            $D1TCOS = um($fdb2csv1['data'][0]['D1TCOS']);
            $D1TCOS = letters_to_num($D1TCOS);
            $D1SHET = um($fdb2csv1['data'][0]['D1SHET']);
            $D1TROS = um($fdb2csv1['data'][0]['D1TROS']);
            $D1THFG = um($fdb2csv1['data'][0]['D1THFG']);
            if ($D1THFG !== '') {
                $hdata = array(); // clear header data
                
            }
        }
    }
    $colconf = array();
    if (count($db2wcol) > 0) {
        foreach ($db2wcol as $key => $value) {
            if ($value['WCHIDE'] === '0' && isset($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']])) {
                $colconf[] = $value;
            }
        }
    }
    //    $book = new PHPExcel();
    /***/
    //    $book = new PHPExcel();
    /***/
    if ($headerFlg === true) {
        //タイトルが31バイト以上の場合、切る
        if (strlen($dctext) > 31) {
            $dctext = mb_strcut($dctext, 0, 31, 'UTF-8');
        }
        if ($D1TMPF === '') {
            $sheet->setTitle($dctext);
        }
        $col = $D1TCOS;
        $row = $D1TROS;
        if ($d1edkb === '2') {
            //insert table DB2EINS
            if (count($DB2EINS) > 0) {
                //reset($DB2EINS);
                //while (list($key, $value) = each($DB2EINS)) {
                foreach ($DB2EINS as $key => $value) {
                    $EICOL = 0; //(int) letters_to_num($value['EICOL']);
                    $EIROW = (int)$value['EIROW'];
                    $EITEXT = $value['EITEXT'];
                    $EISIZE = (trim($value['EISIZE']) !== '' && trim($value['EISIZE']) !== '0') ? (int)$value['EISIZE'] : 11; //(int)$value['EISIZE'];
                    $EICOLR = str_replace("#", "", trim($value['EICOLR']));
                    $EIBOLD = trim($value['EIBOLD']);
                    $isbold = ($EIBOLD !== '1') ? false : true;
                    $sheet->getCellByColumnAndRow($EICOL, $EIROW)->setValueExplicit($EITEXT, PHPExcel_Cell_DataType::TYPE_STRING);
                    $styleArray = array('font' => array('bold' => $isbold, 'color' => array('rgb' => $EICOLR), 'size' => $EISIZE));
                    $sheet->getStyleByColumnAndRow($EICOL, $EIROW)->applyFromArray($styleArray);
                    $row = ($row < $EIROW) ? $EIROW : $row;
                    //      unset($DB2EINS[$key]);
                    
                }
            }
            //insert table DB2ECON
            $ecflg = '';
            $ecrown = 0;
            if (count($DB2ECON) > 0) {
                $ecrown = (int)$DB2ECON[0]['ECROWN'];
                $ecflg = $DB2ECON[0]['ECFLG'];
            }
            //insert table CNDSDATA
            if (count($DB2EINS) > 0) $row = $row + 1;
            if ($ecflg === '1' && count($CNDSDATA) > 0) {
                if (count($DB2EINS) > 0) $row = $row + 1;
                //  reset($CNDSDATA);
                //while (list($key, $value) = each($CNDSDATA)) {
                foreach ($CNDSDATA as $key => $value) {
                    $col = $D1TCOS;
                    if ($value['isSQL'] === '1') {
                        $sheet->getCellByColumnAndRow($col++, $row)->setValueExplicit(cmHscDe($value['cndlabel']), PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->getStyleByColumnAndRow($col, $row)->getAlignment()->setWrapText(true);
                        $sheet->getCellByColumnAndRow($col++, $row)->setValueExplicit(cmHscDe($value['cnddata']), PHPExcel_Cell_DataType::TYPE_STRING);
                    } else {
                        if ($value['gandor'] !== 'seq') $sheet->getCellByColumnAndRow($col++, $row)->setValueExplicit(cmHscDe($value['gandor']), PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->getCellByColumnAndRow($col++, $row)->setValueExplicit(cmHscDe($value['andor']), PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->getCellByColumnAndRow($col++, $row)->setValueExplicit(cmHscDe($value['cndlabel']), PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->getStyleByColumnAndRow($col, $row)->getAlignment()->setWrapText(true);
                        $sheet->getCellByColumnAndRow($col++, $row)->setValueExplicit(cmHscDe($value['cnddata']), PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->getCellByColumnAndRow($col++, $row)->setValueExplicit(cmHscDe($value['cndtype']), PHPExcel_Cell_DataType::TYPE_STRING);
                    }
                    unset($cndsvalue[$key]);
                    $row++;
                }
            }
            //insert row for DB2ECON
            $row = $row + $ecrown;
        }
        $col = $D1TCOS;
        //           reset($hdata);
        //          while (list($key, $value) = each($hdata)) {
        /*if (count($colconf) > 0) {
            e_log("colconf akz" . print_r($colconf, true));
        } else {
            e_log("fdb2csv2 akz" . print_r($fdb2csv2, true));
        }*/
        foreach ($hdata as $key => $value) {
            $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
            unset($hdata[$key]);
            $col++;
        }
        $row = $row + 1;
    } else {
        //  reset($data);
        //  while (list($key1, $value1) = each($data)) {

e_log('fdb2csv2='.print_r($fdb2csv2,true));
     
        foreach ($data as $key1 => $value1) {
            
            $col = $D1TCOS;
//e_log('col='.$col);
            foreach ($value1 as $key2 => $value2) {
                $value2 = cmMer($value2);
                $type = '';
/*
                if (count($colconf) > 0) {
                    $filid = $colconf[$col]['WCFILID'];
                    $type = $colconf[$col]['WCTYPE'];
                    $dec = $colconf[$col]['D2DEC'];
                    $wedt = $colconf[$col]['D2WEDT'];
                } else {
                    $filid = $fdb2csv2[$col]['D2FILID'];
                    $type = $fdb2csv2[$col]['D2TYPE'];
                    $dec = $fdb2csv2[$col]['D2DEC'];
                    $wedt = $fdb2csv2[$col]['D2WEDT'];
                }
*/

                if (count($colconf) > 0) {
                    $filid = $colconf[$key2]['WCFILID'];
                    $type = $colconf[$key2]['WCTYPE'];
                    $dec = $colconf[$key2]['D2DEC'];
                    $wedt = $colconf[$key2]['D2WEDT'];
                } else {
                    $filid = $fdb2csv2[$key2]['D2FILID'];
                    $type = $fdb2csv2[$key2]['D2TYPE'];
                    $dec = $fdb2csv2[$key2]['D2DEC'];
                    $wedt = $fdb2csv2[$key2]['D2WEDT'];
                    $cntfmt = $fdb2csv2[$key2]['SFGMES'];

                }

                $c = getNameFromNumber($col + 1);
                $cl = $c . $row;
                $isNum = checkNum($value2);
                if ($isNum === true) {
                    $value2 = str_replace(",", "", $value2);
                    $value2 = str_replace("/", "", $value2);
                     //e_log('insert data***'.print_r($value2,true));
                    //結果フィールドの場合は指定少数桁数で切り捨て
                    //if ($filid === '9999' || $webf === '1') {
                    //20170113 SYPS修正でコンメントアウトする
                 // if($wedt != "A" && $wedt != "F" && $wedt != "G" && $wedt != "H" && $wedt != "I" && $wedt != "J" && $wedt != "K" && $wedt != "L" && $wedt != "M" && $wedt != "N" && $wedt != "O" && $wedt != "P"){
                    if ($type === 'S' || $type === 'P' || $type === 'B') {
                       if($wedt == "A" || $wedt == "F" || $wedt == "G" || $wedt == "H"){
                             $value2 = convertFloor($value2, $dec);
                       }else if($wedt == "I" || $wedt == "J" || $wedt == "K" || $wedt == "L"){
                             $value2 = convertCeil($value2, $dec);
                       }else{
                             $value2 = floatFormat($value2, $dec, 2);
                       }
                        
                    }
               //   }
                    // }
                    // 編集コード
                   // e_log('value2_bef='.$value2);

                    $value2 = back_front_numFlg($value2);
                    if ($htmlFlg == '1') {
                        
                        if ($value1[1] !== 'カウント:') {
                           
                            $value2 = henshuFormatSetting($value2, $wedt, $type, false, EXCELHENSHUU);
                        }
                    } else {
                      
                        $value2 = henshuFormatSetting($value2, $wedt, $type, false, EXCELHENSHUU);
                    }
                    //小数の数だけ0を文字連結
                    $deCount = 0;
                    if ($value2 !== '') {
                        $tmpData = (string)$value2;
                        if (strpos($tmpData, '.') !== false) {
                            $tmpDec = explode('.', $tmpData);
                            $l_idx = count($tmpDec) - 1; //最後のarrayのidexを取る
                            $deCount = strlen($tmpDec[$l_idx]);
                        }
                    }
                    if ((int)$deCount > 0) {
                        //小数の数だけ0を文字連結
                        $zero = '';
                        for ($di = 0;$di < $deCount;$di++) {
                            $zero.= '0';
                        }
                    }
                    //小数CSSを作成
                    $decStyle = '';
                    $zeroflg = '';
                    if (ZERO_FLG === '0') {
                        $zeroflg = '0';
                    }
                    if (($wedt === 'G' || $wedt === 'H' || $wedt === 'K' || $wedt === 'L' || $wedt === 'O' || $wedt === 'P') && EXCELHENSHUU === '0') {
                        $decStyle = '#,##0';
                    } else if (($wedt === '3' || $wedt === '4') && EXCELHENSHUU === '0') {
                        if ($type === 'S' || $type === 'P' || $type === 'B') {
                            if ((int)$deCount > 0) {
                                $decStyle = '#,##' . $zeroflg . '.' . $zero;
                            } else {
                                $decStyle = '#,##0';
                            }
                        }
                    } else {
                        if ((int)$deCount > 0) {
                            //    $decStyle = '0.' . $zero ;
                            $decStyle = $zeroflg . '.' . $zero;
                        }
                    }
                    if ($value2 !== '' && $value2 !== null) {
                        $strFlg = false; //日付と時間のフォーマットをPHPExcelに正しく表示するようの対応フラグ
                        //  $value2 = $value2 . " ";
                        if (strpos($value2, '/') !== false || strpos($value2, ':') !== false) {
                            $strFlg = true;
                        }
                        //                        $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($value2, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                        //$sheet->getStyle($c)->getNumberFormat()->setFormatCode($decStyle);
                        if ($key1 === 0) {
                            $sheet->getStyle($c)->getNumberFormat()->setFormatCode($decStyle); //MSO FORMAT SET
                            
                        }
                        if ($type === 'S' || $type === 'P' || $type === 'B' || $cntfmt === 'COUNT') {
                            if ($strFlg === true) {
                                //$sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($value2, PHPExcel_Cell_DataType::TYPE_STRING);
                                //                                  $sheet->getStyle($c)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                                if ($key1 === 0) {
                                    //error_log('HORIZONTAL_RIGHT $c='.$c);
                                    $sheet->getStyle($c)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                                }
                                $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($value2, PHPExcel_Cell_DataType::TYPE_STRING);
                            } else {
                                $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($value2, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                            }
                        } else {
                            //$sheet -> writeString($row ,$col,$value2);
                            $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($value2, PHPExcel_Cell_DataType::TYPE_STRING);
                        }
                        //$sheet->setCellValue($cl, $value2);
                        
                    }
                } else {
                    $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($value2, PHPExcel_Cell_DataType::TYPE_STRING);
                    //$value2 = $value2 . " "; 1002
                    //$sheet->setCellValue($cl, $value2);
                    
                }
                /*     foreach (range($c, $sheet->getHighestDataColumn()) as $c) {
                $sheet->getColumnDimension($c)->setAutoSize(true);
                } 
                */
                $col++;
            }
            unset($data[$key1]);
            $row++;
        }
    }
   
    return $row;
    //    return $book;
    
}
 
function convertFloor($value,$precision){
 
   $num = 10;
   $pow_num = pow($num,$precision);
   $value = $value * $pow_num;
   $value = floor($value);
   $value = $value / $pow_num;
   return $value;
}

function convertCeil($value,$precision){
 
   $num = 10;
   $pow_num = pow($num,$precision);
   $value = $value * $pow_num;
   $value = ceil($value);
   $value = $value / $pow_num;
   return $value;
}
/*excel col number to alphabet*/
function getNameFromNumber($num) {
    $numeric = ($num - 1) % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval(($num - 1) / 26);
    if ($num2 > 0) {
        return getNameFromNumber($num2) . $letter;
    } else {
        return $letter;
    }
}
/**CONVERT EXCEL COLUMN ALPHABHET TO NUMBER**/
function letters_to_num($letters) {
    $num = - 1;
    $arr = array_reverse(str_split($letters));
    for ($i = 0;$i < count($arr);$i++) {
        $num+= (ord(strtolower($arr[$i])) - 96) * (pow(26, $i));
    }
    return $num;
}
function FDB2CSV1_DATA($d1name) {
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $data = array();
    $params = array();
    $strSQL = ' SELECT * FROM FDB2CSV1';
    $strSQL.= ' WHERE D1NAME = ? ';
    $params = array($d1name);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
function DB2WDEF_DATA($WUUID, $d1name) {
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $data = array();
    $params = array();
    $strSQL = ' SELECT * FROM DB2WDEF';
    $strSQL.= ' WHERE WDUID = ? ';
    $strSQL.= ' AND WDNAME = ? ';
    $params = array($WUUID, $d1name);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * CSVダウンロード
 *-------------------------------------------------------*
*/
//第一引数・・・ダウンロードするときのファイル名
//第二引数・・・CSVにする配列（array(array(data1,data2),array(data1,data2))の2次元配列形式）
//第三引数・・・ヘッダ
//返り値・・・なし
function cmCsvDownload($stream, $file_name, $data, $header, $time, $fdb2csv2, $headingFlg, $db2wcol = array()) {
   
    //フィールドIDとフィールド名をキーにしてダウンロード可否フラグを持たせる配列
    $compFDB2CSV2 = comparsionFDB2CSV2($fdb2csv2);
    $headerCnt = count($header);
    $colconf = array();
    if (count($db2wcol) > 0) {
        foreach ($db2wcol as $key => $value) {
            if ($value['WCHIDE'] === '0' && isset($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']])) {
                $colconf[] = $value;
            }
        }
    }
    $fdb2csv1 = FDB2CSV1_DATA($file_name);
    $fdb2csv1 = $fdb2csv1['data'][0];
    if ($headingFlg === true) {
        if ($headerCnt > 0) {
            $data = array($header);
        } else {
            $data = array();
        }
    }
    foreach ($data as $key => $value) {
        _fputcsv($stream, $value, $key, $fdb2csv1, $fdb2csv2, $headerCnt, $headingFlg, $colconf);
    }
    e_log('ダウンロード中データ取得SQLメモリ=' . memory_get_usage());
}
function _fputcsv($fp, $data, $rownum, $fdb2csv1, $fdb2csv2, $headerCnt, $headingFlg, $colconf = array(), $toEncoding = 'sjis-win', $srcEncoding = 'UTF-8') {
    $csv = '';
    $webf = cmMer($fdb2csv1['D1WEBF']);
    $D1CENC = cmHsc($fdb2csv1['D1CENC']);
    $D1CENC = um($D1CENC);
    if ($D1CENC == '') {
        $D1CENC = $toEncoding;
    } else {
        $D1CENC = $srcEncoding;
    }
    $D1CKAI = cmHsc($fdb2csv1['D1CKAI']);
    $D1CKAI = um($D1CKAI);
    if ($D1CKAI == '') {
        $D1CKAI = "\r\n";
    } else {
        $D1CKAI = "\n";
    }
    $D1CDLM = cmHsc($fdb2csv1['D1CDLM']);
    $D1CDLM = um($D1CDLM);
    if ($D1CDLM == '') {
        $D1CDLM = ',';
    } else {
        $D1CDLM = "\t";
    }
    $D1CECL = cmHsc($fdb2csv1['D1CECL']);
    $D1CECL = um($D1CECL);
    $D1CHED = cmHsc($fdb2csv1['D1CHED']);
    $D1CHED = um($D1CHED);
    foreach ($data as $key => $col) {
        if ($key < $headerCnt) {
            $type = '';
            if (count($colconf) > 0) {
                $filid = $colconf[$key]['WCFILID'];
                $type = $colconf[$key]['WCTYPE'];
                $dec = $colconf[$key]['D2DEC'];
                $wedt = $colconf[$key]['D2WEDT'];
            } else {
                $filid = $fdb2csv2[$key]['D2FILID'];
                $type = $fdb2csv2[$key]['D2TYPE'];
                $dec = $fdb2csv2[$key]['D2DEC'];
                $wedt = $fdb2csv2[$key]['D2WEDT'];
                $cntfmt = $fdb2csv2[$key]['SFGMES'];
            }
            $col = cmMer($col);
            $isNum = checkNum($col);
            if ($isNum === true && $headingFlg !== true) {
                //結果フィールドの場合は指定少数桁数で切り捨て
                if ($type === 'S' || $type === 'P' || $type === 'B') {
                    $col = floatFormat($col, $dec, 2);
                }
                $col = back_front_numFlg($col); //backflagzero
                 //e_log('CSvData****'.print_r($col,true));
                // 編集コードは「少数切捨て」の場合フォーマットセット
                switch ($wedt) {
                    case 'A':
                    case 'I':
                    case 'M':
                    case '5':
                        $col = henshuFormatSetting($col, $wedt, $type, false, CSVHENSHUU);
                    break;
                    default:
                        $col = $col;
                    break;
                }
                if ($type !== 'S' && $type !== 'P' && $type !== 'B' && $cntfmt !== 'COUNT') {
                    $col = '"' . $col . '"';
                }
                $csv.= $col;
            } else {
                $col = mb_convert_encoding($col, $D1CENC, $srcEncoding);
                if ($D1CECL == '') {
                    $col = mb_str_replace('"', '""', $col, $D1CENC);
                    $csv.= '"' . $col . '"';
                } else {
                    $col = mb_str_replace('"', '', $col, $D1CENC);
                    $col = mb_str_replace($D1CDLM, '', $col, $D1CENC);
                    $col = mb_str_replace($D1CKAI, '', $col, $D1CENC);
                    $csv.= $col;
                }
            }
            if (($key + 1) < count($data)) {
                $csv.= $D1CDLM;
            }
        }
    }
    if ($headingFlg === true) {
        if ($D1CHED == '') {
            fwrite($fp, $csv);
            fwrite($fp, $D1CKAI);
        }
    } else {
        fwrite($fp, $csv);
        fwrite($fp, $D1CKAI);
    }
}
/*
 *-------------------------------------------------------*
 * ExcelHtmlダウンロード
 *-------------------------------------------------------*
*/
//第一引数・・・ダウンロードするときのファイル名
//第二引数・・・CSVにする配列（array(array(data1,data2),array(data1,data2))の2次元配列形式）
//第三引数・・・ヘッダ
//返り値・・・なし
function cmExcelDownload($fp, $file_name, $data, $header, $time, $fdb2csv2, $db2wcol = array(), $DB2ECON, $DB2EINS, $CNDSDATA, $headerFlg) {
    //e_log('【削除】FDB2CSV2データ cmExcelDownload:'.print_r($fdb2csv2,true));
    //フィールドIDとフィールド名をキーにしてダウンロード可否フラグを持たせる配列
    $compFDB2CSV2 = comparsionFDB2CSV2($fdb2csv2);
    $colconf = array();
    if (count($db2wcol) > 0) {
        foreach ($db2wcol as $key => $value) {
            if ($value['WCHIDE'] === '0' && isset($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']])) {
                $colconf[] = $value;
            }
        }
    }
    $data = umEx($data, true);
  
    //    $fp = fopen(TEMP_DIR . cmMer($file_name) . $time . '.xls', 'w');
    _fputexcel($fp, $header, $data, $fdb2csv2, $colconf, false, $DB2ECON, $DB2EINS, $CNDSDATA, $headerFlg);
    //    fclose($fp);
    //exit();
    
}
function _fputexcel($fp, $header, $data, $fdb2csv2, $colconf = array(), $htmlFlg, $DB2ECON, $DB2EINS, $CNDSDATA, $headerFlg = true) {
    error_log('excel hellloooooooooooo');
    if ($headerFlg === true) {
        fwrite($fp, '<thead><tr>');
        foreach ($header as $key => $value) {
            //$value=preg_replace('/\s/', '&nbsp;', cmMer($value));
            $value = str_replace(" ", "&#32;", cmMer($value));
            $value = mb_convert_encoding($value, 'HTML-ENTITIES', 'UTF-8');
            fwrite($fp, '<th nowrap class="xls-header txt">' . $value . '</th>');
        }
        fwrite($fp, '</tr></thead>');
    } else {
        $henshuFormat = true;
        if ($htmlFlg === true) {
            //if(HTMFHENSHUU === '1'){
            $henshuFormat = HTMFHENSHUU;
            // }
            
        } else {
            $henshuFormat = EXCELHENSHUU;
        }
        fwrite($fp, '<tbody>');
        foreach ($data as $key => $row) {
            $bgcolor = '';
            if ($key % 2 !== 0) {
                $bgcolor = 'bgcolor="#dddddd"';
            }
            fwrite($fp, '<tr ' . $bgcolor . '>');
            foreach ($row as $k => $col) {
                //    $col = '2011/11/11';//testing
                $decStyle = '';
                if ($k < count($header)) {
                    $type = '';
                    if (count($colconf) > 0) {
                        $filid = $colconf[$k]['WCFILID'];
                        $type = $colconf[$k]['WCTYPE'];
                        $dec = $colconf[$k]['D2DEC'];
                        $wedt = $colconf[$k]['D2WEDT'];
                    } else {
                        $filid = $fdb2csv2[$k]['D2FILID'];
                        $type = $fdb2csv2[$k]['D2TYPE'];
                        $dec = $fdb2csv2[$k]['D2DEC'];
                        $wedt = $fdb2csv2[$k]['D2WEDT'];
                        $cntfmt = $fdb2csv2[$k]['SFGMES'];
                    }
                    $alignType = 'left';
                    if ($type === 'S' || $type === 'P' || $type === 'B' || $cntfmt === 'COUNT') {
                        $alignType = 'right';
                        // $col       = floatFormat($col, $dec, 2);
                        
                    }
                    $isNum = checkNum($col);
                    if ($isNum === true) {
                        //                if ($type === 'S' || $type === 'P' || $type === 'B') {
                        //結果フィールドの場合は指定少数桁数で切り捨て
                        //      if ($filid === '9999' || $webf === '1') {
                        // 編集コードは「少数切捨て」の場合フォーマットセット
                        $txt = '';
                        if ($type === 'S' || $type === 'P' || $type === 'B') {
                            $col = floatFormat($col, $dec, 2);
                            /*  if($wedt === '0' || $wedt === '1' || $wedt === '2' || $wedt === '6' || $wedt === '7' || $wedt === '9' || $wedt === 'B' || $wedt === 'C' || $wedt === 'D' || $wedt === 'E'){
                            $txt = 'txt';
                            }*/
                        } else {
                            $txt = 'txt';
                        }
                        $col = back_front_numFlg($col);
                        $col = henshuFormatSetting($col, $wedt, $type, false, $henshuFormat);
                        //小数の数だけ0を文字連結
                        $decStyle = queryDecStyle($col, $wedt, $type);
                        //}
                        if ($wedt === 'A') {
                            //$col=preg_replace('/\s/', '&nbsp;', cmMer($col));
                            $col = str_replace(" ", "&#32;", cmMer($col));
                            $col = mb_convert_encoding($col, 'HTML-ENTITIES', 'UTF-8');
                            fwrite($fp, '<td nowrap align="' . $alignType . '" class ="' . $txt . '">' . $col . '</td>');
                        } else {
                            $col = str_replace(" ", "&#32;", cmMer($col));
                            $col = mb_convert_encoding($col, 'HTML-ENTITIES', 'UTF-8');
                            if (strpos(((String)$col), '/') !== false || strpos(((String)$col), ':') !== false) {
                                $txt = 'txt';
                            }
                            fwrite($fp, '<td nowrap align="' . $alignType . '" style="' . $decStyle . '" class ="' . $txt . '">' . $col . '</td>');
                        }
                    } else {
                        //fwrite($fp, '<td nowrap align="left" class="txt">' . $col . '</td>');
                        //fwrite($fp, '<td align="left" style="color:red;">' . $col . '</td>');
                        //fwrite($fp, '<td style="white-space:pre;">' . $col . '</td>');
                        //$col=preg_replace('/\s/', '&nbsp;', cmMer($col));
                        $col = str_replace(" ", "&#32;", cmMer($col));
                        $col = mb_convert_encoding($col, 'HTML-ENTITIES', 'UTF-8');
                        fwrite($fp, '<td nowrap align="' . $alignType . '" class="txt">' . $col . '</td>');
                    }
                }
            }
        }
    }
    //fwrite($fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
    //文字化け対策用全角ブランク
    //fwrite($fp, '　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　');
    
}
function excelCreateHeading($fp, $DB2ECON, $DB2EINS, $CNDSDATA) {
    $tableClass = '';
    if (isset($_SESSION['PHPQUERY'])) {
        if ($_SESSION['PHPQUERY']['LOGIN'] === '1') {
            $db2con = cmDb2Con();
            cmSetPHPQUERY($db2con);
            $DB2WUSR = cmGetDB2WUSR($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
            $DB2WUSR = umEx($DB2WUSR);
            cmDb2Close($db2con);
            $wuclr3 = $DB2WUSR[0]['WUCLR3'];
            $wuclr3 = (($wuclr3 === '') ? COLOR3 : $wuclr3);
            switch ($wuclr3) {
                case 'red':
                    $tableClass = 'table th{background-color:#b60f0b;color:#ffffff;}';
                break;
                case 'blue':
                    $tableClass = 'table th{background-color:#005879;color:#ffffff;}';
                break;
                case 'orange':
                    $tableClass = 'table th{background-color:#a66505;color:#ffffff;}';
                break;
                case 'purple':
                    $tableClass = 'table th{background-color:#541fa7;color:#ffffff;}';
                break;
                case 'green':
                    $tableClass = 'table th{background-color:#246534;color:#ffffff;}';
                break;
                case 'fb':
                    $tableClass = 'table th{background-color:#192441;color:#ffffff;}';
                break;
                case 'muted':
                    $tableClass = 'table th{background-color:#595959;color:#ffffff;}';
                break;
                case 'dark':
                    $tableClass = 'table th{background-color:black;color:#ffffff;}';
                break;
                case 'pink':
                    $tableClass = 'table th{background-color:#6b2345;color:#ffffff;}';
                break;
                case 'brown':
                    $tableClass = 'table th{background-color:#4f2a1b;color:#ffffff;}';
                break;
                case 'sea-blue':
                    $tableClass = 'table th{background-color:#013760;color:#ffffff;}';
                break;
                case 'banana':
                    $tableClass = 'table th{background-color:#cb9704;color:#ffffff;}';
                break;
                default:
                    $tableClass = 'table th{background-color:#f5f5f5;color:#666;}';
                break;
            }
        }
    }
    if ($tableClass === '') {
        $tableClass = 'table th{background-color:rgb(79,129,189);color:#ffffff;}';
    }
    fwrite($fp, '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">');
    fwrite($fp, '<html><head>');
    if ($htmlFlg === true) {
        fwrite($fp, '<meta charset="UTF-8">');
    }
    fwrite($fp, '<meta http-equiv="Content-Type" content="application/vnd.ms-excel; " />');
    fwrite($fp, '<meta http-equiv="Content-Disposition" content="attachment; filename=output.xls" />');
    fwrite($fp, '<style type="text/css">');
    fwrite($fp, '<!-- .txt{mso-number-format:"\@";}-->');
    fwrite($fp, 'table{font-size:13px;}');
    fwrite($fp, $tableClass);
    fwrite($fp, '</style>');
    fwrite($fp, '</head><body>');
    //insert table DB2EINS,DB2ECON,CNDDATA
    insertDB2EINSDB2ECONCNDSDATAHTML($fp, $DB2ECON, $DB2EINS, $CNDSDATA);
    fwrite($fp, '<table border="1">');
}
/*
 *
*/
function num_MaxHTML($db2eins, $paramval) {
    $farray = array();
    $mvalue = '';
    if (count($db2eins) > 0) {
        //reset($db2eins);
        //while (list($keys, $values) = each($db2eins)) {
        foreach ($db2eins as $keys => $values) {
            array_push($farray, $values[$paramval]);
        }
    }
    $mvalue = max($farray);
    return $mvalue;
}
/*
 *UPPER CASE LETTER TO CHANGE NUMBER
*/
/*function letters_to_numEXCEL($letters) {
$num = 0;
$arr = array_reverse(str_split($letters));
for ($i = 0; $i < count($arr); $i++) {
$num += (ord(strtolower($arr[$i])) - 96) * (pow(26,$i));
}
return $num;
}*/
/*
 *insertDB2EINSDB2ECONCNDDATA
*/
function insertDB2EINSDB2ECONCNDSDATAHTML($fp, $DB2ECON, $DB2EINS, $CNDSDATA) //
{
    //insert table DB2EINS
    if (count($DB2EINS) > 0) {
        $row = (int)num_MaxHTML($DB2EINS, 'EIROW');
        //$col = (int) (letters_to_num(num_MaxHTML($DB2EINS, 'EICOL')) + 1);
        $trdata = '<div>';
        for ($i = 1;$i <= $row;$i++) {
            foreach ($DB2EINS as $key => $val) {
                $erow = $val['EIROW'];
                $textstyle = '';
                $tddata = '';
                $searchvalue = array_search($i, (array)$erow);
                if (false !== $searchvalue) {
                    //$EICOL     = (int) (letters_to_num($val['EICOL']) + 1);
                    $EIROW = (int)$val['EIROW'];
                    $EITEXT = $val['EITEXT'];
                    $EISIZE = (trim($val['EISIZE']) !== '' && trim($val['EISIZE']) !== '0') ? (int)$val['EISIZE'] : 10; //excel default font size 11px
                    $EICOLR = $val['EICOLR'];
                    $EIBOLD = ($val['EIBOLD'] !== '1') ? 'normal' : 'bold';
                    $textstyle = "font-size:" . $EISIZE . "pt;color:" . $EICOLR . ";font-weight:" . $EIBOLD . ";border:none; "; //
                    $tddata.= '<span nowrap  align="left" class="txt" style="' . $textstyle . '">' . mb_convert_encoding(cmHsc($EITEXT), 'HTML-ENTITIES', 'UTF-8') . '</span>';
                    break;
                } else {
                    $tddata.= '';
                }
            }
            $trdata.= $tddata . '<br/>';
        }
        fwrite($fp, $trdata . '</div>');
    }
    //get data DB2ECON
    $ecflg = '';
    $ecrown = 0;
    if (count($DB2ECON) > 0) {
        $ecrown = $DB2ECON[0]['ECROWN'];
        $ecflg = $DB2ECON[0]['ECFLG'];
    }
    //insert tabel CNDSDATA
    if ($ecflg === '1' && count($CNDSDATA) > 0) {
        if (count($DB2EINS) > 0) fwrite($fp, '<table ><tbody><tr></tr></tbody></table>');
        fwrite($fp, '<table border="1" bordercolor="#C0C0C0"><tbody>');
        for ($i = 0;$i < count($CNDSDATA);$i++) {
            if ($CNDSDATA[$i]['isSQL'] === '1') {
                $trow = '<tr>';
                $trow.= '<td nowrap align="left" class="txt">' . mb_convert_encoding(cmHsc(cmHscDe($CNDSDATA[$i]['cndlabel'])), 'HTML-ENTITIES', 'UTF-8') . '</td>';
                $cnddatadecode = cmHscDe($CNDSDATA[$i]['cnddata']);
                $trow.= '<td nowrap align="left" class="txt">' . str_replace("\n", "<br/>", mb_convert_encoding(cmHsc($cnddatadecode), 'HTML-ENTITIES', 'UTF-8')) . '</td></tr>';
            } else {
                $trow = '<tr>';
                if ($CNDSDATA[$i]['gandor'] !== 'seq') $trow.= '<td nowrap align="left" class="txt">' . mb_convert_encoding(cmHsc(cmHscDe($CNDSDATA[$i]['gandor'])), 'HTML-ENTITIES', 'UTF-8') . '</td>';
                $trow.= '<td nowrap align="left" class="txt">' . mb_convert_encoding(cmHsc(cmHscDe($CNDSDATA[$i]['andor'])), 'HTML-ENTITIES', 'UTF-8') . '</td>';
                $trow.= '<td nowrap align="left" class="txt">' . mb_convert_encoding(cmHsc(cmHscDe($CNDSDATA[$i]['cndlabel'])), 'HTML-ENTITIES', 'UTF-8') . '</td>';
                $cnddatadecode = cmHscDe($CNDSDATA[$i]['cnddata']);
                $trow.= '<td nowrap align="left" class="txt">' . str_replace("\n", "<br/>", mb_convert_encoding(cmHsc($cnddatadecode), 'HTML-ENTITIES', 'UTF-8')) . '</td>';
                $trow.= '<td nowrap align="left" class="txt">' . mb_convert_encoding(cmHsc(cmHscDe($CNDSDATA[$i]['cndtype'])), 'HTML-ENTITIES', 'UTF-8') . '</td></tr>';
            }
            fwrite($fp, $trow);
        }
        fwrite($fp, '</tbody></table>');
    }
    //insert table DB2ECON
    if ($ecrown > 0) {
        fwrite($fp, '<table><tbody>');
        for ($j = 0;$j < $ecrown;$j++) {
            fwrite($fp, '<tr><td></td></tr>');
        }
        fwrite($fp, '</tbody></table>');
    }
}
/////////
function queryDecStyle($value, $wedt, $type) {
    //小数の数だけ0を文字連結
    $deCount = 0;
    if ($value !== '') {
        $tmpData = (string)$value;
        if (strpos($tmpData, '.') !== false) {
            $tmpDec = explode('.', $tmpData);
            $l_idx = count($tmpDec) - 1; //最後のarrayのidexを取る
            $deCount = strlen($tmpDec[$l_idx]);
        }
    }
    if ((int)$deCount > 0) {
        //小数の数だけ0を文字連結
        $zero = '';
        for ($di = 0;$di < $deCount;$di++) {
            $zero.= '0';
        }
    }
    //小数CSSを作成
    $decStyle = '';
    $zeroflg = '';
    if (ZERO_FLG === '0') {
        $zeroflg = '0';
    }
    if (($wedt === 'G' || $wedt === 'H' || $wedt === 'K' || $wedt === 'L' || $wedt === 'O' || $wedt === 'P') && EXCELHENSHUU === '0') {
        //   $decStyle = 'mso-number-format:\#\,\#\#0.' . $zero . ';';
        $decStyle = 'mso-number-format:\#\,\#\#0 ;';
    } else if (($wedt === '3' || $wedt === '4') && EXCELHENSHUU === '0') {
        if ($type === 'S' || $type === 'P' || $type === 'B') {
            if ((int)$deCount > 0) {
                $decStyle = 'mso-number-format:\#\,\#\#' . $zeroflg . '.' . $zero . ';';
            } else {
                $decStyle = 'mso-number-format:\#\,\#\#0 ;';
            }
        }
    } else {
        if ((int)$deCount > 0) {
            $decStyle = 'mso-number-format:' . $zeroflg . '\.' . $zero . ';';
        }
    }
    return $decStyle;
}
//メールから来るときformatFlg trueになる
function henshuFormatSetting($value, $wedt, $type, $formatFlg, $henshuFlg) {
  
    if ($henshuFlg === '0') {
        switch ($wedt) {
            case '1':
                $value = format1($value);
            break;
            case '2':
                $value = format2($value);
            break;
                /* case '3':
                $value = $value;
                break;*/
            case '4':
                $value = format4($value);
            break;
            case '5':
                $value = format5($value);
            break;
            case '6':
                $value = format6($value);
            break;
            case '7':
                $value = format7($value);
            break;
            case '9':
                $value = format9($value);
            break;
            case '0':
                $value = format0($value);
            break;
            case 'A':
                //$value = formatA($value);
                $value = $value;
            break;
            case 'B':
                $value = formatB($value);
            break;
            case 'C':
                $value = formatC($value);
            break;
            case 'D':
                $value = formatD($value);
            break;
            case 'E':
                $value = formatE($value);
            break;
            case 'F':
               // $value = formatF($value);
                if(is_numeric($value)){
					  $value = $value;
				}else{
	                $zeroFlg = hiddenZero($value);
				    if ($zeroFlg == '0') {
				        $value = '';
				    } else {
				        if ($value != null) {
				            $isNum = checkNum($value);
				            if ($isNum === true) {
				                $value   = $value;
				                $zeroFlg = hiddenZero($value);
				                if ($zeroFlg == '0') {
				                    $value = '';
				                }
				            }
				        }
				     }
			    }
            break;
            case 'G':
                //$value = formatG($value);
                if(is_numeric($value)){
					  $value = $value;
				}else{
	               if ($value != null) {
				     $isNum = checkNum($value);
				     if ($isNum === true) {
				         $value = $value;
				     }
				  }
               }
            break;
            case 'H':
               // $value = formatH($value);
                    if(is_numeric($value)){
					  $value = $value;
				}else{
	                  $zeroFlg = hiddenZero($value);
					  if ($zeroFlg == '0') {
					      $value = '';
					  } else {
					      if ($value != null) {
					          $isNum = checkNum($value);
					          if ($isNum === true) {
					              $value = $value; //少数切捨て
					              $zeroFlg = hiddenZero($value);
					              if ($zeroFlg == '0') {
					                  $value = '';
					              }
					          }
					      }
					 }
                 }
            break;
            case 'I':
                //$value = formatI($value);
                $value = $value;
            break;
            case 'J':
                //$value = formatJ($value);
                if(is_numeric($value)){
					  $value = $value;
				}else{
	                  $zeroFlg = hiddenZero($value);
					  if ($zeroFlg == '0') {
					      $value = '';
					  } else {
					      if ($value != null) {
					          $isNum = checkNum($value);
					          if ($isNum === true) {
					              $value = $value;
					              $zeroFlg = hiddenZero($value);
					              if ($zeroFlg == '0') {
					                  $value = '';
					              }
					          }
					      }
					 }
                 }
            break;
            case 'K':
                //$value = formatK($value);
               if(is_numeric($value)){
					  $value = $value;
			   }else{
	               if ($value != null) {
				      $isNum = checkNum($value);
				      if ($isNum === true) {
				           $value = $value;
				       }
				    }
               }
            break;
            case 'L':
                //$value = formatL($value);
               if(is_numeric($value)){
					$value = $value;
			   }else{
	                $zeroFlg = hiddenZero($value);
				    if ($zeroFlg == '0') {
				        $value = '';
				    } else {
				        if ($value != null) {
				            $isNum = checkNum($value);
				            if ($isNum === true) {
				                $value = $value;
				                $zeroFlg = hiddenZero($value);
				                if ($zeroFlg == '0') {
				                    $value = '';
				                }
				            }
				        }
				    
				    }
                }
            break;
            case 'M':
                //$value = formatM($value);
                  $value = $value;
            break;
            case 'N':
                //$value = formatN($value);
                if(is_numeric($value)){
					$value = $value;
			    }else{
                   $zeroFlg = hiddenZero($value);
				    if ($zeroFlg == '0') {
				        $value = '';
				    } else {
				        if ($value != null) {
				            $isNum = checkNum($value);
				            if ($isNum === true) {
				                $value   = $value;
				                $zeroFlg = hiddenZero($value);
				                if ($zeroFlg == '0') {
				                    $value = '';
				                }
				            }
				        }
                   }
                }
            break;
            case 'O':
                //$value = formatO($value);
                if(is_numeric($value)){
					$value = $value;
			    }else{
                  if ($value != null) {
			        $isNum = checkNum($value);
			        if ($isNum === true) {
			            $value = $value;
			         }
			       }
                }
            break;
            case 'P':
                //$value = formatP($value);
                if(is_numeric($value)){
					$value = $value;
			    }else{
	                $zeroFlg = hiddenZero($value);
					if ($zeroFlg == '0') {
					    $value = '';
					} else {
					    if ($value !== null) {
					        $isNum = checkNum($value);
					        if ($isNum === true) {
					           $value = $value;
					           $zeroFlg = hiddenZero($value);
					         
					           if ($zeroFlg == '0') {
					               $value = '';
					           }
					        }
					    }
				    }
                }
            break;
            default:
                $value = $value;
            break;
        }
    }
    if ($formatFlg == true || ($type !== 'S' && $type !== 'P' && $type !== 'B')) {
        if (($wedt == '3' || $wedt == '4' || $wedt == 'G' || $wedt == 'H' || $wedt == 'K' || $wedt == 'L' || $wedt == 'O' || $wedt == 'P') && $henshuFlg == '0') {
            if (strpos($value, '.') !== false) {
                $tmp = explode('.', $value);
                $tmpvalue = '';
                for ($tidx = 0;$tidx < count($tmp) - 1;$tidx++) {
                    if ($tmpvalue != '') {
                        $tmpvalue.= '.';
                    }
                    $tmpvalue.= $tmp[$tidx];
                }
                $tmpvalue = num_format($tmpvalue);
                $value = $tmpvalue . '.' . $tmp[count($tmp) - 1];
            } else {
                $value = num_format($value);
            }
        }
    }
    return $value;
}


/*
 *-------------------------------------------------------*
 * Pivotダウンロード
 *-------------------------------------------------------*
*/
/*
 *-------------------------------------------------------*
 * Pivotダウンロード
 *-------------------------------------------------------*
*/
function cmPivotDownload($fp, $file_name, $columnupdata, $getcheadingcol, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, $time, $xchecked, $ychecked, $cchecked, $xcheckedcount, $d1webf, $DB2ECON, $DB2EINS, $CNDSDATA, $headerFlg, $rtnResult, $xlsCount, $zentaiCount, $lastrowxlsf) {
 
    if ($data_studio <> '' && count($data_studio) > 0) {
        
        $head_color = _fputpivot($fp, $getcheadingcol, $columnupdata, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, false, $xchecked, $ychecked, $cchecked, $xcheckedcount, $d1webf, $DB2ECON, $DB2EINS, $CNDSDATA, $headerFlg, $rtnResult, $xlsCount, $zentaiCount, $lastrowxlsf);
        return $head_color;
    } else {
      
        _fputPivotExcelFirstData($fp, $d1webf, $DB2ECON, $DB2EINS, $CNDSDATA);
    }
}
/*
 *klyh 挿入文字
*/
function _fputPivotExcelFirstData($fp, $d1webf, $DB2ECON, $DB2EINS, $CNDSDATA) {
    fwrite($fp, '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">');
    fwrite($fp, '<html><head>');
    fwrite($fp, '<meta charset="UTF-8">');
    fwrite($fp, '<meta http-equiv="Content-Type" content="application/vnd.ms-excel; " />');
    fwrite($fp, '<meta http-equiv="Content-Disposition" content="attachment; filename=output.xls" />');
    fwrite($fp, '<style type="text/css">');
    fwrite($fp, '<!-- .txt{mso-number-format:"\@";}-->');
    fwrite($fp, '<!-- .dec1{mso-number-format:"\#\,\#\#0\.0";}-->');
    fwrite($fp, 'table{font-size:13px;border-collapse:collapse;}');
    fwrite($fp, '.downloadTable td,.downloadTable th{border:1px solid #ccc;border-bottom:none;white-space:nowrap;padding:5px 10px 4px 10px;}');
    fwrite($fp, $tableClass);
    fwrite($fp, '</style>');
    fwrite($fp, '</head><body>');
    //insert table DB2EINS,DB2ECON,CNDSDATA
    insertDB2EINSDB2ECONCNDSDATAHTML($fp, $DB2ECON, $DB2EINS, $CNDSDATA);
    fwrite($fp, '</body></html>');
}
/*
 *-------------------------------------------------------*
 * _fputpivot
 *-------------------------------------------------------*
*/
//function _fputpivot($fp, $getcheadingcol, $columnupdata, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, $htmlFlg, $xchecked, $ychecked, $cchecked, $xcheckedcount, $d1webf, $DB2ECON, $DB2EINS, $CNDSDATA, $headerFlg = false, $rtnResult = array())
function _fputpivot($fp, $getcheadingcol, $columnupdata, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, $htmlFlg, $xchecked, $ychecked, $cchecked, $xcheckedcount, $d1webf, $DB2ECON, $DB2EINS, $CNDSDATA, $headerFlg = false, $rtnResult = array(), $count = 0, $zentaiCount = 0, $lastrow = array()) {
    $colslists = umEx($colslists, true);
    $head_color = '';
    if (isset($_SESSION['PHPQUERY'])) {
        if ($_SESSION['PHPQUERY']['LOGIN'] === '1') {
            $db2con = cmDb2Con();
            cmSetPHPQUERY($db2con);
            $DB2WUSR = cmGetDB2WUSR($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
            $DB2WUSR = umEx($DB2WUSR);
            cmDb2Close($db2con);
            $wuclr3 = $DB2WUSR[0]['WUCLR3'];
            $wuclr3 = (($wuclr3 === '') ? COLOR3 : $wuclr3);
        }
    }else{
        $wuclr3 = CND_COFCLR;//配信時のクロス集計の見出し色:
    }
    switch ($wuclr3) {
        case 'red':
            $head_color = 'background-color:#b60f0b;color:#ffffff;';
            $result_color = 'background-color:#d9100d;color:#ffffff;';
            $result_shukei = 'background-color:#f7cfce;';
        break;
        case 'blue':
            $head_color = 'background-color:#005879;color:#ffffff;';
            $result_color = 'background-color:#007099;color:#ffffff;';
            $result_shukei = 'background-color:#cce2ea;';
        break;
        case 'orange':
            $head_color = 'background-color:#a66505;color:#ffffff;';
            $result_color = 'background-color:#df8807;color:#ffffff;';
            $result_shukei = 'background-color:#f8e7cd;';
        break;
        case 'purple':
            $head_color = 'background-color:#541fa7;color:#ffffff;';
            $result_color = 'background-color:#7a3ddb;color:#ffffff;';
            $result_shukei = 'background-color:#e4d8f7;';
        break;
        case 'green':
            $head_color = 'background-color:#246534;color:#ffffff;';
            $result_color = 'background-color:#2f8344;color:#ffffff;';
            $result_shukei = 'background-color:#d5e6d9;';
        break;
        case 'fb':
            $head_color = 'background-color:#192441;color:#ffffff;';
            $result_color = 'background-color:#2b3d6e;color:#ffffff;';
            $result_shukei = 'background-color:#d4d8e2;';
        break;
        case 'muted':
            $head_color = 'background-color:#595959;color:#ffffff;';
            $result_color = 'background-color:#737373;color:#ffffff;';
            $result_shukei = 'background-color:#e3e3e3;';
        break;
        case 'dark':
            $head_color = 'background-color:black;color:#ffffff;';
            $result_color = 'background-color:#494c50;color:#ffffff;';
            $result_shukei = 'background-color:#dadbdc;';
        break;
        case 'pink':
            $head_color = 'background-color:#6b2345;color:#ffffff;';
            $result_color = 'background-color:#9a3263;color:#ffffff;';
            $result_shukei = 'background-color:#ead6df;';
        break;
        case 'brown':
            $head_color = 'background-color:#4f2a1b;color:#ffffff;';
            $result_color = 'background-color:#5f3221;color:#ffffff;';
            $result_shukei = 'background-color:#dfd6d2;';
        break;
        case 'sea-blue':
            $head_color = 'background-color:#013760;color:#ffffff;';
            $result_color = 'background-color:#01487e;color:#ffffff;';
            $result_shukei = 'background-color:#ccdae5;';
        break;
        case 'banana':
            $head_color = 'background-color:#cb9704;color:#ffffff;';
            $result_color = 'background-color:#fab905;color:#ffffff;';
            $result_shukei = 'background-color:#fef1cd;';
        break;
        default:
            $head_color = 'background-color:#f3f3f3;color:#666;';
            $result_color = 'background-color:#f9f9f9;color:#666;';
            $result_shukei = 'background-color:#fdfdfd;';
        break;
    }

    if ($head_color === '') {
        $head_color = 'background-color:rgb(79,129,189);color:#ffffff;';
        $result_color = 'background-color:#608dc3;color:#ffffff;';
        $result_shukei = 'background-color:#dbe5f1;';
    }
    if ($headerFlg === true) {
        fwrite($fp, '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">');
        fwrite($fp, '<html><head>');
        if ($htmlFlg === true) {
            fwrite($fp, '<meta charset="UTF-8">');
        }
        fwrite($fp, '<meta http-equiv="Content-Type" content="application/vnd.ms-excel; " />');
        fwrite($fp, '<meta http-equiv="Content-Disposition" content="attachment; filename=output.xls" />');
        fwrite($fp, '<style type="text/css">');
        fwrite($fp, '<!-- .txt{mso-number-format:"\@";}-->');
        fwrite($fp, '<!-- .dec1{mso-number-format:"\#\,\#\#0\.0";}-->');
        fwrite($fp, 'table{font-size:13px;border-collapse:collapse;}');
        fwrite($fp, '.downloadTable td,.downloadTable th{border:1px solid #ccc;border-bottom:none;white-space:nowrap;padding:5px 10px 4px 10px;}');
        fwrite($fp, $tableClass);
        fwrite($fp, '</style>');
        fwrite($fp, '</head><body>');
        $h_color = $head_color;
        //klyh 挿入文字
       
        _fputPivotExcelFirstData($fp, $d1webf, $DB2ECON, $DB2EINS, $CNDSDATA);
        fwrite($fp, '<table class="downloadTable">');
        fwrite($fp, '<thead>');
        $colFlg = true;
        //sFlg : 0=> head_color ; 1 => result_color
        $sFlg = '0';
        $colyHedColor = array_keys($ychecked);
        $colyHed = array();
        $shukeiArr = array();
        $shukeiArrColor = array();
        for ($col = 0;$col < count($columnupdata);$col++) {
            if (PIVSCOLR === 1) {
                if ($sFlg === '0') {
                    $sFlg = '1';
                    $h_color = $head_color;
                } else {
                    $sFlg = '0';
                    $h_color = $result_color;
                }
            }
            $colyHed[$columnupdata[$col]] = $h_color;
            $rowVal = '';
            $colNm = $columnupdata[$col];
            $fwrite_tmp = '';
            //fwrite($fp, '<tr style="'.$head_color.'">');
            $fwrite_tmp.= '<tr>';
            //fwrite($fp,'<th colspan="'.count($columndata).'" style="text-align:right;">'.$getcheadingcol[$col].'</th>');
            //$getcheadingcol[$col]=preg_replace('/\s/', '&nbsp;', cmMer($getcheadingcol[$col]));
            $hedCol = str_replace(" ", "&#32;", cmMer($getcheadingcol[$col]));
            $hedCol = mb_convert_encoding($hedCol, 'HTML-ENTITIES', 'UTF-8');
            $fwrite_tmp.= '<th colspan="' . $xcheckedcount . '" style="text-align:right;' . $h_color . '">' . $hedCol . '</th>';
            $colspancount = 1;
            for ($i = 0;$i < count($colslists);$i++) {
                $headColData = $colslists[$i][$columnupdata[$col]];
                if ($i + 1 !== count($colslists)) {
                    //2行目以降の場合、親行が変更されたかどうかをチェック
                    $parentChange = false;
                    if ($col > 0) {
                        if ($colslists[$i]['colspanEndFlg'] === true) {
                            $parentChange = true;
                        }
                    }
                    $heflg = true;
                    if ($headColData === '' && $colslists[$i + 1][$columnupdata[$col] . '_GP'] === '1') {
                        $heflg = false;
                    }
                    if ($headColData !== $colslists[$i + 1][$columnupdata[$col]] || ($parentChange === true) || $heflg === false) {
                        // 編集コードは「少数切捨て」の場合データのフォーマットセット
                        $headColData = pivotFloatFormat($d1webf, $ychecked[$colNm]['FILID'], $ychecked[$colNm]['TYPE'], $headColData, $ychecked[$colNm]['DEC'], 2, $ychecked[$colNm]['WEDT']);
                        $countddd = $colspancount * ($countcalc + $num_count);
                        //fwrite($fp,'<th colspan="'.$countddd.'" style="text-align:left;" class="txt">'.$colslists[$i][$columnupdata[$col]].'</th>');
                        $rowsp = '';
                        $headColNm = '';
                        if ($colslists[$i][$columnupdata[$col] . '_GP'] === '1') {
                            $headColData = '計';
                            $rowsp = ' rowspan = "' . (count($columnupdata) - $col) . '"';
                            $headColNm = pivotFloatFormat($d1webf, $ychecked[$columnupdata[$col - 1]]['FILID'], $ychecked[$columnupdata[$col - 1]]['TYPE'], $colslists[$i][$columnupdata[$col - 1]], $ychecked[$columnupdata[$col - 1]]['DEC'], 2, $ychecked[$columnupdata[$col - 1]]['WEDT']);
                        } else {
                            if (in_array($i, $shukeiArr)) {
                                $rowsp = false;
                            }
                        }
                        if ($rowsp !== false) {
                            $shukeiColColor = $h_color;
                            if ($headColData === '計') {
                                array_push($shukeiArr, $i);
                                $shukeiArrColor[$i] = $columnupdata[$col - 1];
                                $shukeiColColor = $colyHed[$columnupdata[$col - 1]];
                            }
                            //$headColData=preg_replace('/\s/', '&nbsp;', cmMer($headColData));
                            $headColData = str_replace(" ", "&#32;", cmMer($headColData));
                            $pData = mb_convert_encoding(($headColNm . $headColData), 'HTML-ENTITIES', 'UTF-8');
                            $fwrite_tmp.= '<th colspan="' . $countddd . '"' . $rowsp . ' style="text-align:left;' . $shukeiColColor . '" class="txt">' . $pData . '</th>';
                        }
                        $colspancount = 0;
                        $colslists[$i]['colspanEndFlg'] = true;
                    }
                } else {
                    // 編集コードは「少数切捨て」の場合データのフォーマットセット
                    $headColData = pivotFloatFormat($d1webf, $ychecked[$colNm]['FILID'], $ychecked[$colNm]['TYPE'], $headColData, $ychecked[$colNm]['DEC'], 2, $ychecked[$colNm]['WEDT']);
                    $countddd = $colspancount * ($countcalc + $num_count);
                    //fwrite($fp,'<th colspan="'.$countddd.'" style="text-align:left;" class="txt">'.$colslists[$i][$columnupdata[$col]].'</th>');
                    $rowsp = '';
                    $headColNm = '';
                    if ($colslists[$i][$columnupdata[$col] . '_GP'] === '1') {
                        $headColData = '計';
                        $rowsp = ' rowspan = "' . (count($columnupdata) - $col) . '"';
                        $headColNm = pivotFloatFormat($d1webf, $ychecked[$columnupdata[$col - 1]]['FILID'], $ychecked[$columnupdata[$col - 1]]['TYPE'], $colslists[$i][$columnupdata[$col - 1]], $ychecked[$columnupdata[$col - 1]]['DEC'], 2, $ychecked[$columnupdata[$col - 1]]['WEDT']);
                    } else {
                        if (in_array($i, $shukeiArr)) {
                            $rowsp = false;
                        }
                    }
                    if ($rowsp !== false) {
                        $shukeiColColor = $h_color;
                        if ($headColData === '計') {
                            array_push($shukeiArr, $i);
                            $shukeiArrColor[$i] = $columnupdata[$col - 1];
                            $shukeiColColor = $colyHed[$columnupdata[$col - 1]];
                        }
                        //$headColData=preg_replace('/\s/', '&nbsp;', cmMer($headColData));
                        $headColData = str_replace(" ", "&#32;", cmMer($headColData));
                        $pData = mb_convert_encoding(($headColNm . $headColData), 'HTML-ENTITIES', 'UTF-8');
                        $fwrite_tmp.= '<th colspan="' . $countddd . '" ' . $rowsp . ' style="text-align:left;' . $shukeiColColor . '" class="txt">' . $pData . '</th>';
                    }
                    $colslists[$i]['colspanEndFlg'] = true;
                }
                $rowVal = $headColData;
                $colspancount++;
            }
            //fwrite($fp, '</tr>');
            $rowspan = 0;
            if ($col === 0 && count((array)$ychecked) > 0) {
                if ($ychecked[$colNm]['SUMG'] !== '1') {
                    $rowspan = 1;
                    $colFlg = true;
                } else {
                    $colFlg = false;
                }
                if ($rowspan !== 0) {
                    if ($colFlg !== false) {
                        $cTotal = $countcalc + $num_count;
                        $pData = '総合計';
                        $pData = mb_convert_encoding(($pData), 'HTML-ENTITIES', 'UTF-8');
                        $shukeiArrColor[$i] = $columnupdata[$col];
                        $fwrite_tmp.= '<td style = "' . $head_color . '" rowspan ="' . count((array)$ychecked) . '" colspan ="' . $cTotal . '">' . $pData . '</td>';
                    }
                }
                //}
                
            }
            $fwrite_tmp.= '</tr>';
            if ($ychecked[$colNm]['HIDE'] !== '1') {
                fwrite($fp, $fwrite_tmp);
            }
        }
        fwrite($fp, '<tr>');
        if (count($columndata) === 0) {
            fwrite($fp, '<td style="' . $bottomstyle . $head_color . '"></td>');
        }
        if (PIVSCOLR === 1) {
            $shukeiColorArr = array();
            $resColcount = $countcalc + $num_count;
            foreach ($shukeiArrColor as $key => $val) {
                $sCol = $key * $resColcount;
                $sColRes = $shukeiArrColor[$key];
                $cColRes = $colyHed[$sColRes];
                for ($cidx = 0;$cidx < $resColcount;$cidx++) {
                    $cCol = $sCol + $cidx;
                    $shukeiColorArr[$cCol] = $cColRes;
                }
            }
        }
        $colHedColor = array_keys($xchecked);
        $colHed = array();
        $countIdx = 0;
        $hd_sFlg = $sFlg;
        //sFlg : 0=> head_color ; 1 => result_color
        $sFlg = '0';
        $countScolor = 0;
        foreach ($rtncolumn as $key => $val) {
            $k = $columndata[$key];
            $xColHD = str_replace(" ", "&#32;", cmMer($val[0]['COLUMN_HEADING']));
            $xColHD = mb_convert_encoding($xColHD, 'HTML-ENTITIES', 'UTF-8');
            if (PIVSCOLR === 1) {
                if ($xcheckedcount > $countIdx) {
                    $hedKey = $colHedColor[$key];
                    if ($sFlg === '0') {
                        $sFlg = '1';
                        $h_color = $head_color;
                    } else {
                        $sFlg = '0';
                        $h_color = $result_color;
                    }
                    $colHed[$hedKey] = $h_color;
                } else {
                    $sFlg = $head_color;
                    if (count($columnupdata) > 0) {
                        if ($hd_sFlg === '0') {
                            $h_color = $head_color;
                        } else {
                            $h_color = $result_color;
                        }
                    }
                    if ($shukeiColorArr[$countScolor] !== null) {
                        $h_color = $shukeiColorArr[$countScolor];
                    }
                    $countScolor++;
                }
                $countIdx++;
            }
            if (isset($xchecked[$k]) === false) {
                fwrite($fp, '<td style="' . $h_color . '" class="txt">' . $xColHD . '</td>');
            } else {
                fwrite($fp, '<td style="' . $h_color . '" class="txt">' . $xColHD . '</td>');
            }
        }
        fwrite($fp, '</tr>');
        fwrite($fp, '</thead>');
        fwrite($fp, '<tbody>');
        //sFlg : 0=> head_color ; 1 => result_color
        
    } else {
        $colHed = $rtnResult['colHed'];
        $shukeiColorArr = $rtnResult['shukeiColorArr'];
    }
    /***
    colHed
    shukeiColorArr
    ***/
    if ($htmlFlg === true) {
        e_log("count" . $count . "zentaiCount" . $zentaiCount . "html file last row " . print_R($lastrow, true));
    }
    $dta = $lastrow;
    $h_color = $head_color;
    for ($x = 0;$x < count($data_studio);$x++) {
        $count++;
        $trcolor = '#ffffff';
        if ($x % 2 !== 0) {
            $trcolor = 'rgb(250,250,250)';
        }
        $r = $data_studio[$x];
       
        fwrite($fp, '<tr style="background-color:' . $trcolor . '">');
        $bottomstyle = '';
        //        if ($x + 1 === count($data_studio)) {
        if ($count === $zentaiCount) {
            $bottomstyle = 'border-bottom:1px solid #ccc;';
        }
        if (count($columndata) === 0) {
            fwrite($fp, '<td style="' . $bottomstyle . $h_color . '"></td>');
        }
        $befChangeFlg = ''; //ループ中、一つ前の列が変更されたかのフラグ
        $sumFlg = - 1;
        $colVal = '';
        $tFlg = false;
        $total = 0;
        $shukeiFlg = '';
        $countIdx = 0;
        $countScolor = 0;
        $sFlg = '0';
        foreach ($r as $i => $val) {
        
            $decStyle = ''; //小数フォーマットCSS
            if ($i !== 'ROWNUM' && strpos($i, 'GP') === false) {
                if (PIVSCOLR === 1) {
                    if ($sFlg === '0') {
                        $sFlg = '1';
                        $h_color = $head_color;
                    } else {
                        $sFlg = '0';
                        $h_color = $result_color;
                    }
                    $countIdx++; // 色変更フラグ
                    
                }
                $sumFlg+= 1;
                $fg = true;
                for ($p = 0;$p < count($columndata);$p++) {
                    if ($i === $columndata[$p]) {
                        $fg = false;
                    }
                }
                $flg = false;
                if ($val === '' && $r[$i . 'GP'] === '1') {
                    $flg = true;
                }
                if (JOINPIV === '1' && $htmlFlg !== true) {
                    $befChangeFlg = true;
                }
                if ((isset($dta[$i]) && $dta[$i] !== $val) || $befChangeFlg === true || ($x === 0 && $count === 1) || $i === $columndata[count($columndata) - 1] || $flg === true) {
                    if ($fg !== false) {
                        //このif文はデータ部のみ通る
                        if ($val === '') {
                            $val = '<a style="color:red">Over</a>';
                            //縦軸集計カラー
                            $tdCssColor = '';
                            if ($shukeiFlg === '1') {
                                $tdCssColor = $result_shukei;
                            }
                            //横軸集計カラー
                            if (PIVSCOLR === 1) {
                                if ($shukeiColorArr[$countScolor] !== null) {
                                    $tdCssColor = $result_shukei;
                                }
                                $countScolor++;
                            }
                            fwrite($fp, '<td style="border-top:1px solid #ccc;text-align:right;' . $tdCssColor . $bottomstyle . '">' . $val . '</td>');
                        } else {
                            //アンダーバーはそのまま表示、それ以外は編集
                            if ($val !== '_') {
                                if (substr($i, 0, 3) !== 'RES') {
                                    $pattern = '/^C[0-9]+/i';
                                    $calcKey = preg_replace($pattern, '', $i);
                                    
                                    $val = pivotFloatFormat($d1webf, $cchecked[$calcKey]['FILID'], $cchecked[$calcKey]['TYPE'], $val, $cchecked[$calcKey]['DEC'], 2, $cchecked[$calcKey]['WEDT']);
                                  
                                    //TYPEがSorPorBで小数桁数が1以上の場合、小数CSSを作成（Excelの数値自動変換を防ぐため）
                                    //TODO:結果フィールド以外は未対応。FDB2CSV2に小数項目がない為。ver3.0以降のDBで拡張予定
                                    $decStyle = getDecStyle($cchecked[$calcKey]['TYPE'], $val);
                                } else {
                                    $val = back_front_numFlg($val);
                                 
                                }
                                if ($val !== '') {
                                    if (strpos($val, '.') !== false) {
                                        $tmp = explode('.', $val);
                                        $tmpValue = '';
                                        for ($tidx = 0;$tidx < count($tmp) - 1;$tidx++) {
                                            if ($tmpValue != '') {
                                                $tmpValue.= '.';
                                            }
                                            $tmpValue.= $tmp[$tidx];
                                        }
                                        $tmpValue = num_format($tmpValue);
                                        $val = $tmpValue . '.' . $tmp[count($tmp) - 1];
                                      
                                    } else {
                                        $val = num_format($val);
                                      
                                    }
                                }
                            }
                            $val = str_replace(" ", "&#32;", cmMer($val));
                            $val = mb_convert_encoding($val, 'HTML-ENTITIES', 'UTF-8');
                          
                            //縦軸集計カラー
                            $tdCssColor = '';
                            if ($shukeiFlg === '1') {
                                $tdCssColor = $result_shukei;
                            }
                            //横軸集計カラー
                            if (PIVSCOLR === 1) {
                                if ($shukeiColorArr[$countScolor] !== null) {
                                    $tdCssColor = $result_shukei;
                                }
                                $countScolor++;
                            }
                            //                            fwrite($fp, '<td style="border-top:1px solid #ccc;text-align:right;'.$tdCssColor. $bottomstyle . $decStyle . '" class="txt" >' . $val . '</td>');
                            fwrite($fp, '<td style="border-top:1px solid #ccc;text-align:right;' . $tdCssColor . $bottomstyle . $decStyle . '" >' . $val . '</td>');
                        }
                    } else {
                        //ここのelseは見出し項目の場合のみ通る
                        if ($val !== '') {
                            $val = pivotFloatFormat($d1webf, $xchecked[$i]['FILID'], $xchecked[$i]['TYPE'], $val, $xchecked[$i]['DEC'], 2, $xchecked[$i]['WEDT']);
                        
                            
                        }
                   
                        if ($xchecked[$i]['HIDE'] !== '1') {
                            $colSp = '';
                            $colSu = '';
                            if ($r[$i . 'GP'] === '1') {
                                //                                    $colSu =$r[array_keys($xchecked)[$sumFlg-1]];
                                $prevCol = array_keys($xchecked);
                                $prevColum = $prevCol[$sumFlg - 1];
                                $colSu = $r[$prevColum];
                                if ($colSu === NULL) {
                                    $colSu = '総合';
                                } else {
                                    //                                    $colSu =  pivotFloatFormat($d1webf,$xchecked[array_keys($xchecked)[$sumFlg-1]]['FILID'],$xchecked[array_keys($xchecked)[$sumFlg-1]]['TYPE'],$colSu,$xchecked[array_keys($xchecked)[$sumFlg-1]]['DEC'],2,$xchecked[array_keys($xchecked)[$sumFlg-1]]['WEDT']);
                                    $colSu = pivotFloatFormat($d1webf, $xchecked[$prevColum]['FILID'], $xchecked[$prevColum]['TYPE'], $colSu, $xchecked[$prevColum]['DEC'], 2, $xchecked[$prevColum]['WEDT']);
                                }
                                $val = 'TOTAL';
                                $tFlg = true;
                                $total = $xcheckedcount - $sumFlg;
                                $colSp = ' colspan = "' . $total . '"';
                            }
                            if ($tFlg === true) {
                                if ($total !== ($xcheckedcount - $sumFlg)) {
                                    $colSp = false;
                                }
                            }
                            if ($colSp !== false) {
                                /*  if ($colSucolSu === '総合') {
                                $colSp = ' colspan = \"' + $xcheckedcount + '\"';
                                $h_color = $head_color;
                                }*/
                                $val = ($val !== 'TOTAL') ? $val : '計';
                                if ($val === "計" && PIVSCOLR === 1) {
                                    $shukeiFlg = '1';
                                    if ($colSu !== '総合') {
                                        $h_color = $colHed[$prevColum]; //column heading color array
                                        
                                    } else {
                                        $h_color = $head_color;
                                    }
                                }
                                $pData = $colSu . $val;
                            
                                $pData = str_replace(" ", "&#32;", cmMer($pData));
                                $pData = mb_convert_encoding($pData, 'HTML-ENTITIES', 'UTF-8');
                              
                                fwrite($fp, '<td' . $colSp . ' style="border-top:1px solid #ccc;text-align:left;' . $bottomstyle . $h_color . '" class="txt">' . $pData . '</td>');
                            }
                        }
                        $colVal = $val;
                      
                        $val = '';
                    }
                    $befChangeFlg = true;
                } else {
                    if ($fg !== false) {
                        if ($val !== '') {
                            //アンダーバーはそのまま表示、それ以外は編集
                            if ($val !== '_') {
                                if (substr($i, 0, 3) !== 'RES') {
                                    $pattern = '/^C[0-9]+/i';
                                    $calcKey = preg_replace($pattern, '', $i);
                                    $val = pivotFloatFormat($d1webf, $cchecked[$calcKey]['FILID'], $cchecked[$calcKey]['TYPE'], $val, $cchecked[$calcKey]['DEC'], 2, $cchecked[$calcKey]['WEDT']);
                                  
                                    //TYPEがSorPorBで小数桁数が1以上の場合、小数CSSを作成（Excelの数値自動変換を防ぐため）
                                    //TODO:結果フィールド以外は未対応。FDB2CSV2に小数項目がない為。ver3.0以降のDBで拡張予定
                                    $decStyle = getDecStyle($cchecked[$calcKey]['TYPE'], $val);
                                } else {
                                    $val = back_front_numFlg($val);
                                  
                                }
                                if (strpos($val, '.') !== false) {
                                    $tmp = explode('.', $val);
                                    $tmpValue = '';
                                    for ($tidx = 0;$tidx < count($tmp) - 1;$tidx++) {
                                        if ($tmpValue != '') {
                                            $tmpValue.= '.';
                                        }
                                        $tmpValue.= '' . $tmp[$tidx];
                                    }
                                    $tmpValue = num_format($tmpValue);
                                    $val = $tmpValue . '.' . $tmp[count($tmp) - 1]; 
                                  
                                } else {
                                    $val = (int)$val;
                                    $val = num_format($val);
                                    
                                }
                            }
                        }
                        $val = str_replace(" ", "&#32;", cmMer($val));
                        $val = mb_convert_encoding($val, 'HTML-ENTITIES', 'UTF-8');
                       
                        //縦軸集計カラー
                        $tdCssColor = '';
                        if ($shukeiFlg === '1') {
                            $tdCssColor = $result_shukei;
                        }
                        //横軸集計カラー
                        if (PIVSCOLR === 1) {
                            if ($shukeiColorArr[$countScolor] !== null) {
                                $tdCssColor = $result_shukei;
                            }
                            $countScolor++;
                        }
                     
                        fwrite($fp, '<td style="border-top:1px solid #ccc;text-align:right;' . $tdCssColor . $bottomstyle . $decStyle . '" class="txt">' . $val . '</td>');
                    } else {
                        //ここのelseは見出し項目の場合のみ通る
                        $val = pivotFloatFormat($d1webf, $xchecked[$i]['FILID'], $xchecked[$i]['TYPE'], $val, $xchecked[$i]['DEC'], 2, $xchecked[$i]['WEDT']);
                   
                        if ($xchecked[$i]['HIDE'] !== '1') {
                            fwrite($fp, '<td style="border-top:none;text-align:right;' . $bottomstyle . $h_color . '" class="txt"></td>');
                        }
                    }
                    $befChangeFlg = false;
                }
            }
        }
        $dta = $r;
        
        fwrite($fp, '</tr>');
    }
    //    fwrite($fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
    //文字化け対策用全角ブランク
    //fwrite($fp, zenSpace100());
    if ($headerFlg === true) {
        $rtnResult = array();
        $rtnResult['colHed'] = $colHed;
        $rtnResult['shukeiColorArr'] = $shukeiColorArr;
    }
    $fileRtn = array();
    $fileRtn['RTNRESULT'] = $rtnResult;
    $fileRtn['ROWCOUNT'] = $count;
    $fileRtn['LASTROW'] = array();
    $datacount = count($data_studio);
    if ($datacount > 0) {
        $lastrow = $data_studio[$datacount - 1];
        $fileRtn['LASTROW'] = $lastrow;
    }
    return $fileRtn;
}
/*
 *
 *ピボットの数値値のフォーマット(２０１６１０１３）
 *
*/
function pivotFloatFormat($d1webf, $FILID, $TYPE, $val, $DEC, $FLG, $WEDT) {
    if ($d1webf === '1') {
        if ($TYPE === 'S' || $TYPE === 'P' || $TYPE === 'B') {
            $val = floatFormat($val, $DEC, 2);
            $val = back_front_numFlg($val);
        }
    } else {
        if ($FILID === '9999') {
            if ($TYPE === 'S' || $TYPE === 'P' || $TYPE === 'B') {
                $val = floatFormat($val, $DEC, 2);
                $val = back_front_numFlg($val);
            }
        }
    }
    // 編集コードは「少数切捨て」の場合データのフォーマットセット
    switch ($WEDT) {
        case 'A':
            $val = floatFormat($val, 0, 2);
            $val = back_front_numFlg($val);
        break;
        default:
            $val = $val;
        break;
    }
    return $val;
}
/*
 *
 *decStyle 作り
 *
*/
function getDecStyle($TYPE, $val) {
    $decStyle = '';
    if ($TYPE === 'S' || $TYPE === 'P' || $TYPE === 'B') {
        $deCount = 0;
        if ($val !== '') {
            $tmpData = (string)$val;
            if (strpos($tmpData, '.') !== false) {
                $tmpDec = explode('.', $tmpData);
                $l_idx = count($tmpDec) - 1; //最後のarrayのidexを取る
                $deCount = strlen($tmpDec[$l_idx]);
            }
        }
        if ((int)$deCount > 0) {
            //小数の数だけ0を文字連結
            $zero = '';
            for ($di = 0;$di < $deCount;$di++) {
                $zero.= '0';
            }
            if (ZERO_FLG === '0') {
                $decStyle = 'mso-number-format:\#\,\#\#0\.' . $zero . ';';
            } else {
                $decStyle = 'mso-number-format:\#\,\#\#\.' . $zero . ';';
            }
        }
    }
    return $decStyle;
}
/*
 *-------------------------------------------------------*
 * CSV取得
 *-------------------------------------------------------*
*/
function cmGetCsv(&$csv_h, &$csv_d, &$define, $filename) {
    $file = '../csv/' . $filename;
    $csv_h = array();
    $csv_d = array();
    if (file_exists($file)) {
        $data = file_get_contents($file);
        $data = mb_convert_encoding($data, 'UTF-8', 'SJIS-WIN');
        $temp = tmpfile();
        fwrite($temp, $data);
        rewind($temp);
        /**定義取得**/
        $i = 0;
        while (($data = fgetcsv($temp, 0, ",", "'")) !== FALSE && $i < 2) {
            if ($i == 0) {
            } else if ($i > 0) {
                foreach ($data as $key => $value) {
                    if (substr($value, 0, 1) == '"') {
                        $define[] = 'left';
                    } else {
                        $define[] = 'right';
                    }
                }
            }
            $i++;
        }
        /*****/
        //fwrite($temp, $data);
        rewind($temp);
        $i = 0;
        while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
            if ($i == 0) {
                $csv_h[] = $data;
            } else if ($i > 0) {
                $csv_d[] = $data;
            }
            $i++;
        }
        fclose($temp);
    }
}
/*
 *-------------------------------------------------------*
 * DB接続
 *-------------------------------------------------------*
*/
function cmDb2Con() {
    try {
        $user = RDB_USER;
        $password = RDB_PASSWORD;
        $database = RDB;
        $option = array('autocommit' => DB2_AUTOCOMMIT_ON, 'i5_naming' => DB2_I5_NAMING_ON, 'i5_dbcs_alloc' => DB2_I5_DBCS_ALLOC_ON);
        $db2con = db2_connect($database, $user, $password, $option);
    }
    catch(Exception $e) {
        $this->pconn = 'die';
        die();
    }
    return $db2con;
}
/*
 *-------------------------------------------------------*
 * DB接続
 * ライブラリ
 *-------------------------------------------------------*
*/
function cmDb2ConLib($liblist) {
    $user = RDB_USER;
    $password = RDB_PASSWORD;
    $database = RDB;

    if(QCMDEXC === 1){

        $option = array(
            'i5_naming' => DB2_I5_NAMING_ON
        );

        $con    = db2_connect($database, $user, $password, $option);
        if ($con) {
            e_log("Connection succeeded.");

            $strCMD = "CHGLIBL LIBL(".$liblist.")";
            $lenCMD = strlen($strCMD);
            $lenCMD = str_pad($lenCMD,10,'0',STR_PAD_LEFT).'.00000';
            $strSQL = " call QSYS2/QCMDEXC('$strCMD',$lenCMD) ";
            db2_exec($con,$strSQL);

        } else {
            e_log("Connection Failed.");
        }

    }else{

        $option = array('i5_naming' => DB2_I5_NAMING_ON, 'i5_libl' => $liblist);

        $con = db2_connect($database, $user, $password, $option);

        if ($con) {
            e_log("Connection succeeded.");
        } else {
            e_log("Connection Failed.");
        }

    }

    return $con;
}
/*
 *-------------------------------------------------------*
 * DB接続
 * ライブラリ
 *-------------------------------------------------------*
*/
function cmDb2ConOption($option) {
    $user = RDB_USER;
    $password = RDB_PASSWORD;
    $database = RDB;
    $con = db2_connect($database, $user, $password, $option);
    if ($con) {
        e_log("Connection succeeded.");
    } else {
        e_log("Connection Failed.");
    }
    return $con;
}
/*
 *-------------------------------------------------------*
 * DB接続
 * 区画に対しての接続
 *-------------------------------------------------------*
*/
function cmDb2ConOptionRDB($option) {
    $RDBNM = (isset($_SESSION['PHPQUERY']['RDBNM']) ? $_SESSION['PHPQUERY']['RDBNM'] : '');
    if ($RDBNM === '' || $RDBNM === RDB) {
        $user = RDB_USER;
        $password = RDB_PASSWORD;
        $database = RDB;
    } else {
        $RDBDATA = cmGetDB2RDB($RDBNM);
        if ($RDBDATA['result'] === true) {
            $RDBDATA = $RDBDATA['data'];
            $user = cmMer($RDBDATA['RDBUSR']);
            $password = cmMer($RDBDATA['RDBPASS']);
            $database = cmMer($RDBDATA['RDBNM']);
        } else {
            $con = false;
        }
    }
    $con = db2_connect($database, $user, $password, $option);
    if ($con) {
        e_log("Connection succeeded.");
    } else {
        e_log("Connection Failed.");
    }
    return $con;
}
/*
 *-------------------------------------------------------*
 * DB接続
 * 区画に対しての接続
 *-------------------------------------------------------*
*/
function cmDb2ConRDB($liblist = SAVE_DB) {
    $RDBNM = (isset($_SESSION['PHPQUERY']['RDBNM']) ? $_SESSION['PHPQUERY']['RDBNM'] : '');
    if ($RDBNM === '' || $RDBNM === RDB) {
        $user = RDB_USER;
        $password = RDB_PASSWORD;
        $database = RDB;
    } else {
        $RDBDATA = cmGetDB2RDB($RDBNM);
        if ($RDBDATA['result'] === true) {
            $RDBDATA = $RDBDATA['data'];
            $user = cmMer($RDBDATA['RDBUSR']);
            $password = cmMer($RDBDATA['RDBPASS']);
            $database = cmMer($RDBDATA['RDBNM']);
        } else {
            $con = false;
        }
    }

    if(QCMDEXC === 1){

        $option = array(
            'i5_naming' => DB2_I5_NAMING_ON
        );

        $con    = db2_connect($database, $user, $password, $option);
        if ($con) {

            e_log("Connection succeeded.");
            $strCMD = "CHGLIBL LIBL(".$liblist.")";
            $lenCMD = strlen($strCMD);
            $lenCMD = str_pad($lenCMD,10,'0',STR_PAD_LEFT).'.00000';
            $strSQL = " call QSYS2/QCMDEXC('$strCMD',$lenCMD) ";
            db2_exec($con,$strSQL);

        } else {
            e_log("Connection Failed.");
        }

    }else{

        $option = array('i5_naming' => DB2_I5_NAMING_ON, 'i5_libl' => $liblist);
        $con = db2_connect($database, $user, $password, $option);
        if ($con) {
            e_log("Connection succeeded.");
        } else {
            e_log("Connection Failed.");
        }

    }

    return $con;
}
/*
 *-------------------------------------------------------*
 * 区画マスター取得
 *-------------------------------------------------------*
*/
function cmGetDB2RDB($RDBNM) {
    $data = array();
    $params = array();
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $rs = true;
    $sql = 'SET ENCRYPTION PASSWORD = \'' . RDB_KEY . '\'';
    $stmt = db2_prepare($db2con, $sql);
    if ($stmt === false) {
        $rs = 'FAIL_SQL';
    } else {
        $r = db2_execute($stmt);
        if ($r === false) {
            $rs = 'FAIL_SQL';
        }
    }
    if ($rs === true) {
        $strSQL.= ' SELECT ';
        $strSQL.= '     RDBNM, ';
        $strSQL.= '     RDBUSR, ';
        $strSQL.= '     DECRYPT_CHAR(RDBPASS) AS RDBPASS ';
        $strSQL.= ' FROM ';
        $strSQL.= '     DB2RDB ';
        $strSQL.= ' WHERE RDBNM = ? ';
        $params = array($RDBNM);
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array('result' => 'FAIL_SEL');
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                if (count($data) === 0) {
                    $data = array('result' => 'NOTEXIST_GET');
                } else {
                    $data = array('result' => true, 'data' => $data[0]);
                }
            }
        }
    }
    cmDb2Close($db2con);
    return $data;
}
/*
 *-------------------------------------------------------*
 * DB切断
 *-------------------------------------------------------*
*/
function cmDb2Close($con) {
    db2_close($con);
}
/*
 *-------------------------------------------------------*
 * SET PHPQUERY
 *-------------------------------------------------------*
*/
function cmSetPHPQUERY($db2con) {
    db2_exec($db2con, 'SET SCHEMA ' . MAINLIB);
}
/*
 *-------------------------------------------------------*
 * SET QTEMP
 *-------------------------------------------------------*
*/
function cmSetQTEMP($db2con) {
    db2_exec($db2con, 'SET SCHEMA QTEMP');
}

/*
 *-------------------------------------------------------*
 * SET LibList
 *-------------------------------------------------------*
*/
function cmSetLIB($db2con, $libList) {
    $rtn = 0;
    // db2_exec($db2con, 'SET SCHEMA '.$LIBLIST);
    $options = array('i5_naming' => DB2_I5_NAMING_ON, 'i5_libl' => $liblist);
    $result = db2_set_option($db2con, $options, 1);
    if ($result === false) {
        $rtn = 1;
        e_log('接続失敗', '1');
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * CALL PHPQUERYW
 *-------------------------------------------------------*
*/
function cmCallPHPQUERYW($db2con, $DCNAME, $PROC, $OUTSTMF, &$RTCD, &$JSEQ) {
    $stmt = db2_prepare($db2con, "CALL " . MAINLIB . "/PHPQUERYW(?,?,?,?,?)");
    $in_param1 = $DCNAME;
    $in_param2 = $PROC;
    $in_param3 = $OUTSTMF;
    $return_value1 = "";
    $return_value2 = 0;
    // ストアドへのパラメータバインド
    $retBool = db2_bind_param($stmt, 1, "in_param1", DB2_PARAM_IN);
    $retBool = db2_bind_param($stmt, 2, "in_param2", DB2_PARAM_IN);
    $retBool = db2_bind_param($stmt, 3, "in_param3", DB2_PARAM_IN);
    $retBool = db2_bind_param($stmt, 4, "return_value1", DB2_PARAM_OUT);
    $retBool = db2_bind_param($stmt, 5, "return_value2", DB2_PARAM_OUT);
    $ret = db2_execute($stmt);
    $RTCD = 'return_value1';
    $JSEQ = 'return_value2';
    $RTCD = $$RTCD;
    $JSEQ = $$JSEQ;
}
/*
 *-------------------------------------------------------*
 * CALL PHPQUERYWS
 *-------------------------------------------------------*
*/
function cmCallPHPQUERYWS($db2con, $DCNAME, $DKEY, $DATE, $TIME, $FLAG) {
    $stmt = db2_prepare($db2con, "CALL " . CLLIB . "/PHPQUERYWS(?,?,?,?,?)");
  
    $in_param1 = $DCNAME;
    $in_param2 = $DKEY;
    $in_param3 = $DATE;
    $in_param4 = $TIME;
    $in_param5 = $FLAG;
    // ストアドへのパラメータバインド
    $retBool = db2_bind_param($stmt, 1, "in_param1", DB2_PARAM_IN);
    $retBool = db2_bind_param($stmt, 2, "in_param2", DB2_PARAM_IN);
    $retBool = db2_bind_param($stmt, 3, "in_param3", DB2_PARAM_IN);
    $retBool = db2_bind_param($stmt, 4, "in_param4", DB2_PARAM_IN);
    $retBool = db2_bind_param($stmt, 5, "in_param5", DB2_PARAM_IN);
    $ret = db2_execute($stmt);
}
/*
 *-------------------------------------------------------*
 * CALL PHPQUERYWC
 *-------------------------------------------------------*
*/
function cmCallPHPQUERYWC($db2con, $DCNAME, $PROC, &$RTCD) {
    $stmt = db2_prepare($db2con, "CALL " . MAINLIB . "/PHPQUERYWC(?,?,?)");
    if ($stmt === false) {
        e_log("err" . db2_stmt_errormsg(), '1');
    }
    $in_param1 = $DCNAME; //クエリー名
    $in_param2 = $PROC; //'INT'
    $return_value1 = "";
    /*    $in_param3 = $OUTSTMF;//ブランク
    
    $return_value2 = 0;
    */
    // ストアドへのパラメータバインド
    $retBool = db2_bind_param($stmt, 1, "in_param1", DB2_PARAM_IN);
    $retBool = db2_bind_param($stmt, 2, "in_param2", DB2_PARAM_IN);
    //  $retBool = db2_bind_param($stmt, 3, "in_param3", DB2_PARAM_IN);
    $retBool = db2_bind_param($stmt, 3, "return_value1", DB2_PARAM_OUT);
    //  $retBool = db2_bind_param($stmt, 5, "return_value2", DB2_PARAM_OUT);*/
    $ret = db2_execute($stmt);
    $RTCD = 'return_value1';
    $RTCD = $$RTCD;
}
function cmCallCNVQRYCS($db2con, $LIBNAME, $ODOBNM, $ODOBTX) {
    $rtn = true;
    $RTCD = '';

    $stmt = db2_prepare($db2con, "CALL " . MAINLIB . "/CNVQRYCS(?,?,?,?)");
  
    if ($stmt === false) {
        $rtn = false;
        e_log("①" . db2_stmt_errormsg(), print_r($stmt, true));
    } else {
        $in_param1 = $LIBNAME;
        $in_param2 = $ODOBNM;
        $in_param3 = $ODOBTX;
        $return_value1 = "";
        e_log('ライブラリー名:' . $LIBNAME);
        // ストアドへのパラメータバインド
        $retBool = db2_bind_param($stmt, 1, "in_param1", DB2_PARAM_IN);
        $retBool = db2_bind_param($stmt, 2, "in_param2", DB2_PARAM_IN);
        $retBool = db2_bind_param($stmt, 3, "in_param3", DB2_PARAM_IN);
        $retBool = db2_bind_param($stmt, 4, "return_value1", DB2_PARAM_OUT);
        $ret = db2_execute($stmt);
        $RTCD = 'return_value1';
        $RTCD = $$RTCD;
      
        if ($ret === false) {
            $rtn = false;
            e_log('②' . db2_stmt_errormsg(), print_r($ret, true));
        }
    }
    return $rtn;
}
function cmCallQRYOUTF($db2con, $LIBNAME) {
    $rtn = true;
    $LIBDEV = 'QTEMP';
    $RTCD = '';
    $tmpTblNm = makeRandStr(10, true);
    $stmt = db2_prepare($db2con, "CALL " . MAINLIB . "/QRYOUTF(?,?,?,?)");
  
    if ($stmt === false) {
        $rtn = false;
        e_log("①" . db2_stmt_errormsg(), print_r($stmt, true));
    } else {
        $in_param1 = $LIBNAME;
        $in_param2 = $LIBDEV;
        $in_param3 = $tmpTblNm;
        $return_value1 = "";
        e_log('ライブラリー名:' . $LIBNAME);
        // ストアドへのパラメータバインド
        $retBool = db2_bind_param($stmt, 1, "in_param1", DB2_PARAM_IN);
        $retBool = db2_bind_param($stmt, 2, "in_param2", DB2_PARAM_IN);
        $retBool = db2_bind_param($stmt, 3, "in_param3", DB2_PARAM_IN);
        $retBool = db2_bind_param($stmt, 4, "return_value1", DB2_PARAM_OUT);
        $ret = db2_execute($stmt);
        $RTCD = 'return_value1';
        $RTCD = $$RTCD;
      
        if ($ret === false) {
            $rtn = false;
            e_log('②' . db2_stmt_errormsg(), print_r($ret, true));
        }
    }
    return array('result' => $rtn, 'LibDEV' => $LIBDEV, 'TmpTBl' => $tmpTblNm, 'RTCD' => $RTCD);
}
/*function cmCallCNVQRY($db2con, $LIBNAME)
{
$rtn = true;
$stmt = db2_prepare($db2con, "CALL " . "DEVQUERY2/CNVQRYC(?)");
e_log("STMT***" . $stmt);
if($stmt === false){
$rtn = false;
e_log("①" . db2_stmt_errormsg(),print_r($stmt,true));
}else{

$in_param1 = $LIBNAME;
e_log('ライブラリー名:'.$LIBNAME);

// ストアドへのパラメータバインド
$retBool = db2_bind_param($stmt, 1, "in_param1", DB2_PARAM_IN);
$ret = db2_execute($stmt);
e_log("RET***" . $ret);
if($ret === false){
$rtn = false;
e_log('②'.db2_stmt_errormsg(),print_r($ret,true));
}
}
return $rtn;
}*/
/*
 *-------------------------------------------------------*
 * 全角半角スペース削除
 *-------------------------------------------------------*
*/
function cmMer($str) {
    $bfr = '';
    $aft = $str;
    mb_regex_encoding('UTF-8');
    while ($bfr !== $aft) {
        $bfr = $aft;
        $aft = mb_ereg_replace(" +$", "", $bfr);
        //$aft = mb_ereg_replace("　+$", "", $aft);
        
    }
    return $aft;
}
/*
 *-------------------------------------------------------*
 * htmlspecialchars()
 *-------------------------------------------------------*
*/
function cmHsc($str) {
    if (is_array($str)) {
        return array_map("cmHsc", $str);
    } else {
        return htmlspecialchars($str, ENT_QUOTES);
    }
}
/*
 *-------------------------------------------------------*
 * htmlspecialchars_decode()
 *-------------------------------------------------------*
*/
function cmHscDe($str) {
    if (is_array($str)) {
        return array_map("cmHscDe", $str);
    } else {
        return htmlspecialchars_decode($str, ENT_QUOTES);
    }
}
/*
 *-------------------------------------------------------*
 * クォーテーションのエスケープ
 *
 * cmHscで変換されたコーテーションをエスケープする
 * javascript関数に渡す引数などに使用
 *-------------------------------------------------------*
*/
function escQuot($str) {
    $str = str_replace("'", "\'", $str);
    $str = str_replace('"', '\"', $str);
    $str = str_replace('&#039;', '\&#039;', $str);
    $str = str_replace('&quot;', '\&quot;', $str);
    return $str;
}
/*
 *-------------------------------------------------------*
 * 条件文
 *-------------------------------------------------------*
*/
/*
function cmCND($cnd){

$rs = '';
$cnd = cmMer($cnd);

switch($cnd){
case 'EQ':
$rs = '=';
break;
case 'NE':
$rs = '≠';
break;
case 'GT':
$rs = '>';
break;
case 'LT':
$rs = '<';
break;
case 'GE':
$rs = '>=';
break;
case 'LE':
$rs = '<=';
break;
case 'LIKE':
$rs = 'LIKE';
break;
case 'NLIKE':
$rs = 'NLIKE';
break;
case 'RANGE':
$rs = 'RANGE';
break;
case 'LIST':
$rs = 'LIST';
break;
case 'NLIST':
$rs = 'NLIST';
break;
}

return $rs;

}
*/
/*
 *-------------------------------------------------------*
 * 条件文
 *-------------------------------------------------------*
*/
function cmCND($cnd) {
    $rs = '';
    $cnd = cmMer($cnd);
    switch ($cnd) {
        case 'EQ':
            $rs = CND_EQ;
        break;
        case 'NE':
            $rs = CND_NE;
        break;
        case 'GT':
            $rs = CND_GT;
        break;
        case 'LT':
            $rs = CND_LT;
        break;
        case 'GE':
            $rs = CND_GE;
        break;
        case 'LE':
            $rs = CND_LE;
        break;
        case 'LIKE':
            $rs = CND_LIKE;
        break;
        case 'NLIKE':
            $rs = CND_NLIKE;
        break;
        case 'RANGE':
            $rs = CND_RANGE;
        break;
        case 'LIST':
            $rs = CND_LIST;
        break;
        case 'NLIST':
            $rs = CND_NLIST;
        break;
    }
    return $rs;
}
/*
 *-------------------------------------------------------*
 * ANDOR
 *-------------------------------------------------------*
*/
function cmANDOR($andor) {
    $rs = '';
    $andor = cmMer($andor);
    switch ($andor) {
        case 'AN':
            $rs = 'かつ、';
        break;
        case 'OR':
            $rs = 'もしくは、';
        break;
        default:
            $rs = $andor;
    }
    return $rs;
}
/*
 *-------------------------------------------------------*
 * クォーテーションチェック
 (文字列の場合、シングルコーテーションを外す 
 *-------------------------------------------------------*
*/
function cmQuotCheck($value, $type) {
    $rs = '';
    if (($type != 'S') && ($type != 'P') && ($type != 'B')) {
        //$rs = trim($value, "\x22 \x27");
        $value = mb_ereg_replace(" +$", "", $value);
        $rs = substr($value, 1, -1);
    } else {
        $rs = $value;
    }
    return $rs;
}
/*
 *-------------------------------------------------------*
 * LIKEチェック
 (LIKEの場合、%を外す 
 *-------------------------------------------------------*
*/
function cmLikeCheck($value, $cnd) {
    $rs = '';
    if ($cnd == 'LIKE') {
        if (mb_substr($value, 0, 1) == '%' && mb_substr($value, -1) != '%') {
            $value = str_replace("%", "", $value);
            $value = "%" . $value;
        } else if (mb_substr($value, 0, 1) != '%' && mb_substr($value, -1) == '%') {
            $value = str_replace("%", "", $value);
            $value = $value . "%";
        } else {
            $value = str_replace("%", "", $value);
        }
        $rs = $value;
    } else {
        $rs = $value;
    }
    return $rs;
}
/*
 *-------------------------------------------------------*
 * 数値or文字？
 *-------------------------------------------------------*
*/
function cmType($str) {
    $rs = '';
    $substr1 = substr($str, 0, 1);
    $substr2 = substr($str, -1, 1);
    if ($substr1 == "'" && $substr2 == "'") {
        $rs = 'O';
    } else {
        $rs = 'A';
    }
    return $rs;
}
/*
 *-------------------------------------------------------*
 * エクセル
 *-------------------------------------------------------*
*/
function cmXlsDownload($file_name, $book, $time, $WUUID, $d1name, $EXCELVER, $NOTEMPLATE = 'TEMPLATE') {
    /*
    $ua = $_SERVER['HTTP_USER_AGENT'];
    
    if(!(preg_match("/Chrome/i",$ua)) && !(preg_match("/Safari/i",$ua))){
    $file_name = mb_convert_encoding($file_name, "SJIS-win","UTF-8");
    }
    */
   
    $fdb2csv1 = FDB2CSV1_DATA($d1name);
    $fdb2csv2 = DB2WDEF_DATA($WUUID, $d1name);
    $ext = '';
    $D1TMPF = '';
    $WDSERV = '';
    $D1TEXT = '';
    $D1EFLG = '';
    $D1ETYP = '';
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $DB2WUSR = cmGetDB2WUSR($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    $DB2WUSR = umEx($DB2WUSR);
    cmDb2Close($db2con);
    $WUSERV = $DB2WUSR[0]['WUSERV'];
    /**AKZ フォルダスペース除去20170511**/
    if ($WUSERV !== '') {
        $WUSERVARR = explode('/', $WUSERV);
        $WUSERV = '';
        $WUSERVCOUNT = count($WUSERVARR) - 1;
        /*foreach($WUSERVARR as $key => $value){
        $WUSERV .= '/'.trim($value);
        }*/
        for ($i = 0;$i < $WUSERVCOUNT;$i++) {
            if (cmMer($WUSERVARR[$i]) != '') {
                $WUSERV.= '/' . replaceFilename(trim($WUSERVARR[$i]));
            }
        }
        $WUSERV.= '/' . replaceFilename(ltrim($WUSERVARR[$WUSERVCOUNT]));
    }
    /**AKZ フォルダスペース除去20170511**/
    $WUCTFL = $DB2WUSR[0]['WUCTFL'];
    $WUCNFL = $DB2WUSR[0]['WUCNFL'];
    if ($fdb2csv1['result'] === true) {
        $D1TMPF = cmHsc($fdb2csv1['data'][0]['D1TMPF']);
        $D1TMPF = um($D1TMPF);
        //        $D1TEXT = cmHsc($fdb2csv1['data'][0]['D1TEXT']);
        $D1TEXT = $fdb2csv1['data'][0]['D1TEXT'];
       
        $D1TEXT = replaceFilename(um($D1TEXT));
       
        $D1ETYP = cmHsc($fdb2csv1['data'][0]['D1ETYP']);
        $D1ETYP = um($D1ETYP);
        $D1EFLG = cmHsc($fdb2csv1['data'][0]['D1EFLG']);
        $D1EFLG = um($D1EFLG);
        //e_log("NOTEMPLATE3".$NOTEMPLATE.print_r($fdb2csv1,true));
        //      if(($D1EFLG === '2' && $D1ETYP !== '') || ($D1EFLG === '2' && $D1ETYP === '' && $NOTEMPLATE === 'NOTEMPLATE')){
        if ($NOTEMPLATE === 'NOTEMPLATE') {
            $D1TMPF = ''; //クエリー実行設定に選択されてるタイプがあったら、テンプレートの設定を無効にする
            
        }
        if ($D1TMPF !== '') {
            $D1TMPF = $D1TMPF;
            $ext = explode('.', $D1TMPF);
            $ext = $ext[count($ext) - 1];
        }
    }
    if ($fdb2csv2['result'] === true) {
        $WDSERV = cmHsc($fdb2csv2['data'][0]['WDSERV']);
        $WDSERV = um($WDSERV);
        /**AKZ フォルダスペース除去20170511**/
        if ($WDSERV !== '') {
            $WDSERVARR = explode('/', $WDSERV);
            $WDSERV = '';
            $WDSERVCOUNT = count($WDSERVARR) - 1;
            /*foreach($WDSERVARR as $key => $value){
            $WDSERV .= '/'.trim($value);
            }*/
            for ($i = 0;$i < $WDSERVCOUNT;$i++) {
                if (cmMer($WDSERVARR[$i]) != '') {
                    $WDSERV.= '/' . replaceFilename(trim($WDSERVARR[$i]));
                }
            }
            $WDSERV.= '/' . replaceFilename(ltrim($WDSERVARR[$WDSERVCOUNT]));
        }
        /**AKZ フォルダスペース除去20170511**/
        $WDCTFL = cmHsc($fdb2csv2['data'][0]['WDCTFL']);
        $WDCTFL = um($WDCTFL);
        $WDCNFL = cmHsc($fdb2csv2['data'][0]['WDCNFL']);
        $WDCNFL = um($WDCNFL);
    }
    /**20161014**/
    if (($EXCELVER == '2003' && $ext === '') || $ext === 'xls') {
        /*
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="'.cmMer($file_name).'.xls"');
        header('Cache-Control: max-age=0');
        */
        $writer = PHPExcel_IOFactory::createWriter($book, "Excel5");
        $writer->setIncludeCharts(TRUE);
        if ($WDSERV !== '') {
            if (substr($WDSERV, 0, 1) !== '/') {
                $WDSERV = '/' . $WDSERV . '/';
            } else if (substr($WDSERV, 0, 1) === '/') {
                $WDSERV = $WDSERV . '/';
            }
            $dfilename = $D1TEXT;
            if ($WDCNFL === '1') {
                $dfilename = $d1name;
            }
            if ($WDCTFL !== '1') {
                $xlsfiledownload = $WDSERV . cmMer($dfilename) . '.xls';
            } else {
                $xlsfiledownload = $WDSERV . cmMer($dfilename) . $time . '.xls';
            }
            $writer->save($xlsfiledownload);
        } else {
            if ($WUSERV !== '') {
                if (substr($WUSERV, 0, 1) !== '/') {
                    $WUSERV = '/' . $WUSERV . '/';
                } else if (substr($WUSERV, 0, 1) === '/') {
                    $WUSERV = $WUSERV . '/';
                }
                $dfilename = $D1TEXT;
                if (WUCNFL === '1') {
                    $dfilename = $d1name;
                }
                if ($WUCTFL !== '1') {
                    $xlsfiledownload = $WUSERV . cmMer($dfilename) . '.xls';
                } else {
                    $xlsfiledownload = $WUSERV . cmMer($dfilename) . $time . '.xls';
                }
                $writer->save($xlsfiledownload);
            } else if ($WUSERV === '') {
                $writer->save(TEMP_DIR . cmMer($file_name) . $time . '.xls');
            }
        }
        //        exit();
        
    } else if ($EXCELVER == '2007' || $ext === 'xlsx' || $ext === 'xlsm') {
        if ($EXCELVER == '2007') {
            $ext = 'xlsx';
        }
        /*
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="'.cmMer($file_name).'.xlsx"');
        header('Cache-Control: max-age=0');
        */
    
        $writer = PHPExcel_IOFactory::createWriter($book, "Excel2007");
        $writer->setIncludeCharts(TRUE);
        if ($WDSERV !== '') {
            if (substr($WDSERV, 0, 1) !== '/') {
                $WDSERV = '/' . $WDSERV . '/';
            } else if (substr($WDSERV, 0, 1) === '/') {
                $WDSERV = $WDSERV . '/';
            }
            $dfilename = $D1TEXT;
            if ($WDCNFL === '1') {
                $dfilename = $d1name;
            }
            if ($WDCTFL !== '1') {
                $xlsfiledownload = $WDSERV . cmMer($dfilename) . '.' . $ext;
            } else {
                $xlsfiledownload = $WDSERV . cmMer($dfilename) . $time . '.' . $ext;
            }
            $writer->save($xlsfiledownload);
        } else {
            if ($WUSERV !== '') {
                if (substr($WUSERV, 0, 1) !== '/') {
                    $WUSERV = '/' . $WUSERV . '/';
                } else if (substr($WUSERV, 0, 1) === '/') {
                    $WUSERV = $WUSERV . '/';
                }
                $dfilename = $D1TEXT;
                if (WUCNFL === '1') {
                    $dfilename = $d1name;
                }
                if ($WUCTFL !== '1') {
                    $xlsfiledownload = $WUSERV . cmMer($dfilename) . '.' . $ext;
                } else {
                    $xlsfiledownload = $WUSERV . cmMer($dfilename) . $time . '.' . $ext;
                }
                $writer->save($xlsfiledownload);
            } else if ($WUSERV === '') {
                $writer->save(TEMP_DIR . cmMer($file_name) . $time . '.' . $ext);
            }
        }
        //        exit();
        
    }
}
/*
 *-------------------------------------------------------*
 * 定義ファイル（基本取得）
 *-------------------------------------------------------*
*/
function cmGetGroup($db2con, &$rs) {
    $rs = '0';
    $d = array();
    $data = array();
    $strSQL = ' SELECT A.WQGID,A.WQUNAM,A.WQTILH,C.D1NAME,C.D1TEXT ';
    $strSQL.= ' FROM DB2WQGR AS A JOIN DB2WGDF AS B ';
    $strSQL.= ' ON A.WQGID = B.WGGID ';
    $strSQL.= ' JOIN FDB2CSV1 as C ';
    $strSQL.= ' ON B.WGNAME = C.D1NAME ';
    $strSQL.= ' ORDER BY A.WQGID ASC ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2execute($stmt);
        if ($r === false) {
            $data = false;
        } else {
            $tmp = '';
            while ($row = db2_fetch_assoc($stmt)) {
                if ($tmp == $row['WQGID'] || $tmp == '') {
                    $d[] = $row;
                } else {
                    $data[] = $d;
                    $d = array();
                    $d[] = $row;
                }
                $tmp = $row['WQGID'];
            }
            if (count($d) > 0) {
                $data[] = $d;
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * 定義ファイル（基本取得）
 *-------------------------------------------------------*
*/
function cmGetNoGroup($db2con, &$rs) {
    $rs = '0';
    $data = array();
    $strSQL = ' SELECT A.WQGID,A.WQUNAM,A.WQTILH,C.D1NAME,C.D1TEXT ';
    $strSQL.= ' FROM DB2WQGR AS A JOIN DB2WGDF AS B ';
    $strSQL.= ' ON A.WQGID = B.WGGID ';
    $strSQL.= ' RIGHT JOIN FDB2CSV1 as C ';
    $strSQL.= ' ON B.WGNAME = C.D1NAME ';
    $strSQL.= ' WHERE A.WQGID IS NULL ';
    $strSQL.= ' ORDER BY A.WQGID ASC ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * スラッシュ削除
 *-------------------------------------------------------*
*/
function cmDelSlash($str) {
    return str_replace('/', '', $str);
}
/*
 *-------------------------------------------------------*
 * ｺﾛﾝ削除
 *-------------------------------------------------------*
*/
function cmDelColon($str) {
    return str_replace(':', '', $str);
}
/*
 *-------------------------------------------------------*
 * 日付フォーマット yyyy/mm/dd
 *-------------------------------------------------------*
*/
function cmYyyyMmDd($str) {
    if ($str == null || $str == '' || $str == '0') {
        return false;
    }
    $str = strval($str);
    $str = sprintf('%08d', $str);
    return substr($str, 0, 4) . "/" . substr($str, 4, 2) . "/" . substr($str, 6, 2);
}
/*
 *-------------------------------------------------------*
 * 日付フォーマット hh:ii
 *-------------------------------------------------------*
*/
function cmHhIi($str) {
    if ($str == '') {
        return false;
    }
    $str = strval($str);
    $str = sprintf('%04d', $str);
    return substr($str, 0, 2) . ":" . substr($str, 2, 2);
}
/*
 *-------------------------------------------------------*
 * 日付フォーマット hh:ii:ss
 *-------------------------------------------------------*
*/
function cmHhIiSs($str) {
    if ($str == '') {
        return false;
    }
    $str = strval($str);
    $str = sprintf('%06d', $str);
    return substr($str, 0, 2) . ":" . substr($str, 2, 2) . ":" . substr($str, 4, 2);
}
/*
 *-------------------------------------------------------*
 * ユーザー情報から閲覧可能定義ファイルを取得
 *-------------------------------------------------------*
*/
function cmDNAME($db2con, $WUUID) {
    $data = array();
    $strSQL = ' SELECT A.WDNAME ';
    $strSQL.= ' FROM DB2WDEF AS A ';
    $strSQL.= ' WHERE A.WDUID = ? ';
    $strSQL.= ' ORDER BY A.WDNAME ASC ';
    $params = array($WUUID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row['WDNAME'];
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * 定義ファイル（基本取得）
 *-------------------------------------------------------*
*/
function cmGetFDB2CSV1($db2con, &$rs = '0') {
    $data = array();
    $strSQL = ' SELECT B.D1NAME,B.D1FILE,B.D1FILLIB,B.D1TEXT ';
    $strSQL.= ' FROM FDB2CSV1 AS B';
    $strSQL.= ' ORDER BY B.D1NAME ASC ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}
/**
 * 指定されたデータのAS/400でのByte数を返す
 *
 * 引数   :1.チェックしたい値
 * 戻り値 :1.AS/400でのByte数
 *         2.全角文字数
 *
 */
function checkByte($data) {
    $zCnt = 0;
    $sCnt = 0;
    $zn0 = 0;
    $zn = 0;
    $strlen = mb_strlen($data, "UTF-8");
    //１文字ずつ比較
    for ($i = 0;$i < $strlen;$i++) {
        $evalstr = mb_substr($data, $i, 1, "UTF-8");
        //1byte??
        if ((strlen(bin2hex($evalstr)) / 2) == 1) {
            $zn = 0;
            $sCnt = $sCnt + 1;
            //半角ｶﾅ??
            
        } else if (preg_match('/^(?:\xEF\xBD[\xA1-\xBF]|\xEF\xBE[\x80-\x9F]){1,100}$/', $evalstr)) {
            $zn = 0;
            $sCnt = $sCnt + 1;
        } else {
            $zn = 1;
            $sCnt = $sCnt + 2;
        }
        if ($zn0 == 0 && $zn == 1) {
            $zCnt = $zCnt + 1;
        }
        $zn0 = $zn;
    }
    $LenAS = $sCnt + $zCnt * 2;
    $res[0] = $LenAS;
    $res[1] = $zCnt;
    return $res;
}
/**
 * 指定された文字列の桁数のチェックを行う
 *
 * 引数   :1.チェックしたい文字列
 *         2.桁数
 * 戻り値 :1.true :チェックOK
 *           false:チェックNG
 *
 */
function checkMaxLen($data, $maxLen) {
    $len = array();
    $len = checkByte($data);
    if ($maxLen < $len[0]) {
        return false;
    }
    return true;
}
/**
 *
 * 右トリム
 * 半角空白、全角空白
 *
 * @param  string 変換前
 * @return string 変換後
 */
function um($str) {
    $bfr = '';
    $aft = $str;
    mb_regex_encoding('UTF-8');
    while ($bfr !== $aft) {
        $bfr = $aft;
        $aft = mb_ereg_replace(" +$", "", $bfr);
        //$aft = mb_ereg_replace("　+$", "", $aft);
        
    }
    return $aft;
}
function cmEdit($val, $wedt, $type) {
    /*
    '1'：日付スラッシュ　※修正前の'D'
    '2'：日付スラッシュ、但し”数値”の場合でゼロはブランクに置き換える
    '3'：カンマ編集　※修正前の'D'
    '4'：カンマ編集　但し”数値”の場合でゼロはブランクに置き換える
    '5'：”数値”の場合でゼロはブランクに置き換える
    
    上記"数値"の判定が必要なのでFDB2CSV2にもD2TYPEを追加しました
    'S' , 'P' , 'B' は数値とみなしてください
    */
    switch ($wedt) {
        case '1':
            $val = cmYyyyMmDd($val);
        break;
        case '2':
            if ($type == 'S' || $type == 'P' || $type == 'B') {
                if ($val == 0) {
                    $val = '';
                }
            }
            $val = cmYyyyMmDd($val);
        break;
        case '3':
            if (strstr($val, '.')) {
                $tmp = explode('.', $val);
                $tmp[0] = num_format($tmp[0]);
                $val = $tmp[0] . '.' . $tmp[1];
            } else {
                if (is_numeric($val)) {
                    $val = num_format($val);
                }
            }
        break;
        case '4':
            if ($type == 'S' || $type == 'P' || $type == 'B') {
                if ($val == 0) {
                    $val = '';
                }
            }
            if (strstr($val, '.')) {
                $tmp = explode('.', $val);
                $tmp[0] = num_format($tmp[0]);
                $val = $tmp[0] . '.' . $tmp[1];
            } else {
                if (is_numeric($val)) {
                    $val = num_format($val);
                }
            }
        break;
        case '5':
            if ($type == 'S' || $type == 'P' || $type == 'B') {
                if ($val == 0) {
                    $val = '';
                }
            }
        break;
    }
    return $val;
}
//大文字英数字乱数作成

/**
 $length = 乱数の文字列の長さ
 $query = クエリ実行(true)、それ以外(false)
 $query = trueの場合、頭人桁目は@にする
 *
 */
function makeRandStr($length, $query = false) {
    $f_str = ''; //最初の1桁
    $r_str = '';
    if ($query == true) {
        $fstr = '@';
    } else {
        $fstr = array_merge(range('A', 'Z'));
    }
    $f_str = $fstr[rand(0, count($fstr) - 1) ];
    $str = array_merge(range('A', 'Z'), range('0', '9'), range('A', 'Z"'));
    for ($i = 0;$i < ($length - 1);$i++) {
        $r_str.= $str[rand(0, count($str) - 1) ];
    }
    return $f_str . $r_str;
    e_log('create tablename:' . $f_str . $r_str);
}
/*
 *-------------------------------------------------------*
 * FDB2CSV2取得
 *$seigyoFlg:trueなら全部のカラムをもらう
 *-------------------------------------------------------*
*/
function cmGetFDB2CSV2($db2con, $D1NAME, $dnlFlg = false, $gphFlg = false,$seigyoFlg=false) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT A.* ,D.SFGMES FROM ( ';
    $strSQL.= ' SELECT D2NAME,D2FILID,D2FLD,D2HED,D2CSEQ,D2WEDT, ';
    $strSQL.= ' D2TYPE,D2LEN,D2DEC,D2DNLF ';
    $strSQL.= ' FROM FDB2CSV2 ';
    $strSQL.= ' WHERE (D2NAME = ? AND D2CSEQ > 0) ';
    $params[]= $D1NAME;
	//制御：入れてないSEQのため使う
	$seigyoCOL=array();
    if($seigyoFlg===true){
		$r=cmGetSeiGyoColumnBySEQZERO($db2con,$D1NAME);
		if($r["result"]===true){
			$seigyoCOL=$r["data"];
		}
	}
    if(count($seigyoCOL)>0 && $seigyoFlg===true){
	    $strSQL.= ' OR ( D2NAME = ? AND D2CSEQ=0  ';
		if(count($seigyoCOL)>0){
			$strSQL.='AND CONCAT(D2FILID,D2FLD) IN( '.join(',',$seigyoCOL).') ';
		}
		$strSQL.= ' ) ';
		$params[]=$D1NAME;
    }
    $strSQL.= ' UNION ALL ';
    $strSQL.= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID, ';
    $strSQL.= ' D5FLD AS D2FLD,D5HED AS D2HED,D5CSEQ AS D2CSEQ, ';
    $strSQL.= ' D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DEC AS D2DEC,D5DNLF AS D2DNLF ';
    $strSQL.= ' FROM FDB2CSV5 ';
    $strSQL.= ' WHERE (D5NAME = ? AND D5CSEQ > 0 )';
	$params[]=$D1NAME;
    if(count($seigyoCOL)>0 && $seigyoFlg===true){
	    $strSQL.= ' OR ( D5NAME = ? AND D5CSEQ=0  ';
		if(count($seigyoCOL)>0){
			$strSQL.='AND CONCAT(\'9999\',D5FLD) IN( '.join(',',$seigyoCOL).') ';
		}
		$strSQL.= ' ) ';
		$params[]=$D1NAME;
    }
    $strSQL.= ' ) AS A ';
    $strSQL .= '        LEFT JOIN BSUMFLD D '; 
    $strSQL .= '        ON   A.D2NAME   = D.SFQRYN ';
    $strSQL .= '        AND  A.D2FILID  = D.SFFILID '; 
    $strSQL .= '        AND  A.D2FLD    = D.SFFLDNM '; 
    if ($dnlFlg === true || $gphFlg === true) {
        $strSQL.= ' WHERE ';
    }
    if ($dnlFlg === true) {
        $strSQL.= ' A.D2DNLF = \'\' ';
    }
    if ($gphFlg === true) {
        if ($dnlFlg === true) {
            $strSQL.= ' AND (';
        }
        $strSQL.= ' A.D2TYPE = \'S\' OR A.D2TYPE = \'P\' OR A.D2TYPE = \'B\' ';
        if ($dnlFlg === true) {
            $strSQL.= ' ) ';
        }
    }
    if(count($seigyoCOL)>0 && $seigyoFlg===true){
	    $strSQL.= 'ORDER BY CASE WHEN A.D2CSEQ=0 THEN NULL ELSE A.D2CSEQ END ASC ';
	}else{
	    $strSQL.= ' ORDER BY A.D2CSEQ ASC ';
	}
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
            if (count($data) === 0 && $gphFlg !== true) {
                $data = array('result' => 'NOTEXIST_GET');
            } else {
                $data = array('result' => true, 'data' => $data);
            }
        }
    }
    return $data;
}
/**
*SEQを入力していない項目を制御キーに指定
*
**/
function cmGetSeiGyoColumnBySEQZERO($db2con,$D1NAME) {
	$rs=true;
    $data = array();
	$dataTest=array();
    $params = array();
    $strSQL = ' SELECT DCFILID1,DCFLD1,DCFILID2,DCFLD2,DCFILID3,DCFLD3, ';
	$strSQL.= ' DCFILID4,DCFLD4,DCFILID5,DCFLD5,DCFILID6,DCFLD6 ';
	$strSQL.= ' FROM DB2COLM WHERE DCNAME=? ';
	$params=array($D1NAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
		$rs='FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
			$rs='FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
				$dataTest[]=$row;
            }
			$dataResult=$dataTest[0];
			for($i=1;$i<=6;$i++){
				if(cmMer($dataResult["DCFLD".$i])!==''){
					$resExp1=explode(",",cmMer($dataResult["DCFLD".$i]));
					if(count($resExp1)>=2){
						for($j=0;$j<count($resExp1);$j++){
							$resExp2=explode("-",$resExp1[$j]);
							$data[]='\''.cmMer($resExp2[0]).cmMer($resExp2[1]).'\'';
						}
					}else{
						$data[] ='\''.cmMer($dataResult["DCFILID".$i]).cmMer($dataResult["DCFLD".$i]).'\'';
					}
				}
			}
		    $strSQL = ' SELECT DTFILID,DTFLD FROM DB2COLT WHERE DTNAME=? AND DTFLD IS NOT NULL ';
			$params=array($D1NAME);
		    $stmt = db2_prepare($db2con, $strSQL);
		    if ($stmt === false) {
				$rs='FAIL_SEL';
		    } else {
		        $r = db2_execute($stmt, $params);
		        if ($r === false) {
					$rs='FAIL_SEL';
		        } else {
		            while ($row = db2_fetch_assoc($stmt)) {
						$dataTest[]=$row;
		            }
					for($n=0;$n<count($dataTest);$n++){
						$dataResult=$dataTest[$n];
						if(cmMer($dataResult['DTFLD'])!==""){
							$data[]='\''.cmMer($dataResult["DTFILID"]).cmMer($dataResult["DTFLD"]).'\'';
						}
					}
		        }
		    }
        }
    }

    return array("result"=>$rs,"data"=>$data);
}
/*
 *-------------------------------------------------------*
 * 比較用FDB2CSV2
 *-------------------------------------------------------*
*/
function comparsionFDB2CSV2($fdb2csv2) {
    //e_log("【削除】fdb2csv2データ".count($fdb2csv2));
    $rtn = array();
    foreach ($fdb2csv2 as $key => $value) {
        $filid = cmMer($value['D2FILID']);
        $fld = cmMer($value['D2FLD']);
        $dnlf = cmMer($value['D2DNLF']);
        if (!isset($rtn['filid' . $filid])) {
            $rtn['filid' . $filid] = array();
        }
        $rtn['filid' . $filid][$fld . '_' . $filid] = $dnlf;
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * 定義ファイル（基本取得）
 *-------------------------------------------------------*
*/
function cmGetFDB2CSV3($db2con, $d1name) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT A.*,B.* ';
    $strSQL.= ' FROM FDB2CSV3 AS A JOIN FDB2CSV2 as B ';
    $strSQL.= ' ON A.D3NAME = B.D2NAME AND A.D3FILID = B.D2FILID ';
    $strSQL.= ' AND A.D3FLD = B.D2FLD ';
    $strSQL.= ' WHERE A.D3NAME = ? ';
    $strSQL.= ' AND A.D3USEL <> ? ';
    $strSQL.= ' ORDER BY A.D3JSEQ ASC ';
    $params = array($d1name, '3');
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        e_log("db2_prepare" . db2_stmt_errormsg());
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            e_log("db2_execute" . db2_stmt_errormsg());
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 *
 *-------------------------------------------------------*
*/
function cmSearchUpdate($db2con, $sch_data) {
    foreach ($sch_data as $k1 => $v1) {
        foreach ($v1 as $k2 => $v2) {
            foreach ($v2 as $k3 => $v3) {
                //固定or非表示の場合、更新処理なし
                if ($v3['usel'] == ' ' || $v3['usel'] == '3') {
                    continue;
                }
                $updvalue = '';
                if ($v3['cnd'] == 'RANGE') {
                    $tmpval1 = $v3['rangevalue'][0];
                    $tmpval2 = $v3['rangevalue'][1];
                    if ($v3['type'] != 'S' && $v3['type'] != 'P' && $v3['type'] != 'B') {
                        if ($tmpval1 != '') {
                            $tmpval1 = "'" . $tmpval1 . "'";
                        }
                        if ($tmpval2 != '') {
                            $tmpval2 = "'" . $tmpval2 . "'";
                        }
                    }
                    $updvalue = $tmpval1 . ' ' . $tmpval2;
                } else if ($v3['cnd'] == 'LIST' || $v3['cnd'] == 'NLIST') {
                    //リストが一つだけだった場合、同じ内容で２つのリストを作成
                    if (count($v3['listvalue']) === 1) {
                        $listvalue = $v3['listvalue'][0];
                        if ($listvalue != '') {
                            if ($v3['type'] != 'S' && $v3['type'] != 'P' && $v3['type'] != 'B') {
                                $updvalue = "'" . $listvalue . "' '" . $listvalue . "'";
                            } else {
                                $updvalue = $listvalue . " " . $listvalue;
                            }
                        }
                    } else {
                        //リストが複数ある場合
                        foreach ($v3['listvalue'] as $key => $listvalue) {
                            if ($key != 0) {
                                $updvalue.= ' ';
                            }
                            if ($v3['type'] != 'S' && $v3['type'] != 'P' && $v3['type'] != 'B') {
                                if ($listvalue != '') {
                                    $listvalue = "'" . $listvalue . "'";
                                }
                            }
                            $updvalue.= $listvalue;
                        }
                    }
                } else if ($v3['cnd'] == 'LIKE') {
                    $updvalue = $v3['value'];
                    //前方％の場合
                    if (mb_substr($updvalue, 0, 1) == '%' && mb_substr($updvalue, -1) != '%') {
                        //%を全て削除
                        $updvalue = str_replace("%", "", $updvalue);
                        if ($updvalue != '') {
                            //Byte数取得
                            $byteAry = checkByte($updvalue);
                            $updlen = $byteAry[0];
                            //1Byte以上空きがある場合、%を付与
                            if (((int)$v3['len'] - $updlen) >= 1) {
                                $updvalue = "'%" . $updvalue . "'";
                            }
                        }
                    } else if (mb_substr($updvalue, 0, 1) != '%' && mb_substr($updvalue, -1) == '%') {
                        //後方％の場合
                        //%を全て削除
                        $updvalue = str_replace("%", "", $updvalue);
                        if ($updvalue != '') {
                            //Byte数取得
                            $byteAry = checkByte($updvalue);
                            $updlen = $byteAry[0];
                            //1Byte以上空きがある場合、%を付与
                            if (((int)$v3['len'] - $updlen) >= 1) {
                                $updvalue = "'" . $updvalue . "%'";
                            }
                        }
                    } else {
                        //%を全て削除
                        $updvalue = str_replace("%", "", $updvalue);
                        if ($updvalue != '') {
                            //Byte数取得
                            $byteAry = checkByte($updvalue);
                            $updlen = $byteAry[0];
                            //2Byte以上空きがある場合、両サイドに%を付与
                            if (((int)$v3['len'] - $updlen) >= 2) {
                                $updvalue = "'%" . $updvalue . "%'";
                            } else if (((int)$v3['len'] - $updlen) >= 1) {
                                //elseで1Byte以上の空きがある場合は、前方一致
                                $updvalue = "'" . $updvalue . "%'";
                            }
                        }
                    }
                } else {
                    $updvalue = $v3['value'];
                    if ($v3['type'] != 'S' && $v3['type'] != 'P' && $v3['type'] != 'B') {
                        if ($updvalue != '') {
                            $updvalue = "'" . $v3['value'] . "'";
                        }
                    }
                }
                $rs = cmUpdFDB2CSV3($db2con, $k1, $k2, $k3, $updvalue, $v3['type']);
            }
        }
    }
    /*
    $strSQL  = ' SELECT A.D3NAME,A.D3DAT ';
    $strSQL .= ' FROM FDB2CSV3 AS A ' ;
    
    $params = array();
    $stmt = db2_prepare($db2con,$strSQL);
    db2_execute($stmt,$params);
    
    if($stmt){
    while($row = db2_fetch_assoc($stmt)){
    $data[] = $row;
    }
    }
    
    var_dump($data);
    */
}
/*
 *-------------------------------------------------------*
 * カラム情報取得
 *-------------------------------------------------------*
*/
function cmGetDB2WCOL($db2con, $WCUID, $WCNAME) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT A.*,B.D2DEC,B.D2WEDT ';
    //    $strSQL = ' SELECT A.* ';
    $strSQL.= ' FROM DB2WCOL AS A ';
    $strSQL.= ' LEFT JOIN ';
    $strSQL.= ' ( ';
    $strSQL.= ' SELECT D2NAME,(RTRIM(D2FLD) || \'_\' || RTRIM(D2FILID)) as D2INDX,D2FILID,D2FLD,D2HED,D2CSEQ,D2WEDT,D2TYPE,D2LEN,D2DNLF,D2DEC ';
    $strSQL.= ' FROM FDB2CSV2 WHERE D2NAME = ? ';
    $strSQL.= ' UNION ALL ';
    $strSQL.= ' SELECT D5NAME AS D2NAME,(RTRIM(D5FLD) || \'_9999\') AS D2INDX,9999 AS D2FILID,D5FLD AS D2FLD,D5HED AS D2HED, ';
    $strSQL.= ' D5CSEQ AS D2CSEQ,D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN, ';
    $strSQL.= ' D5DNLF AS D2DNLF,D5DEC AS D2DEC ';
    $strSQL.= ' FROM FDB2CSV5 WHERE D5NAME = ? ';
    $strSQL.= ' ) AS B ';
    $strSQL.= ' ON A.WCNAME = B.D2NAME ';
    $strSQL.= ' AND A.WCFILID = B.D2FILID ';
    $strSQL.= ' AND A.WCFLD = B.D2FLD ';
    $strSQL.= ' WHERE A.WCUID = ? ';
    $strSQL.= ' AND A.WCNAME = ? ';
    $strSQL.= ' ORDER BY A.WCSEQN ';
    $params = array($WCNAME, $WCNAME, $WCUID, $WCNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        e_log("db2_prepare" . db2_stmt_errormsg());
        $data === false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            e_log("db2_prepare" . db2_stmt_errormsg());
            $data === false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * カラム情報追加
 *-------------------------------------------------------*
*/
function cmAddDB2WCOL($db2con, $WCUID, $WCNAME, $WCSEQN, $WCFILID, $WCFLD, $WCINDX, $WCTEXT, $WCTYPE, $WCLOCK, $WCHIDE) {
    $rs = 0;
    //構文
    $strSQL = ' INSERT INTO DB2WCOL ';
    $strSQL.= ' (WCUID,WCNAME,WCSEQN,WCFILID,WCFLD,WCINDX,WCTEXT,WCTYPE,WCLOCK,WCHIDE) ';
    $strSQL.= ' VALUES ';
    $strSQL.= ' (?,?,?,?,?,?,?,?,?,?) ';
    $params = array($WCUID, $WCNAME, $WCSEQN, $WCFILID, $WCFLD, $WCINDX, $WCTEXT, $WCTYPE, $WCLOCK, $WCHIDE);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 1;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 1;
        }
    }
    return $rs;
}
/*
 *-------------------------------------------------------*
 * カラム情報更新
 *-------------------------------------------------------*
*/
/*
function cmUpdDB2WCOL($db2con,$WCUID,$WCNAME,$WCCOL){

$rs = 0;

//構文
try {

$strSQL  = ' UPDATE DB2WCOL AS A SET ';
$strSQL .= ' A.WCCOL = ? ';
$strSQL .= ' WHERE ' ;
$strSQL .= ' A.WCUID = ? ';
$strSQL .= ' AND A.WCNAME = ? ';

$params = array(
$WCCOL,
$WCUID,
$WCNAME
);

$stmt = db2_prepare($db2con,$strSQL);
db2_execute($stmt,$params);

} catch(Exception $e) {
$rs = 1;
}

return $rs;

}
*/
/*
 *-------------------------------------------------------*
 * カラム情報削除
 *-------------------------------------------------------*
*/
function cmDeleteDB2WCOL($db2con, $WCUID, $WCNAME) {
    $rs = 0;
    //構文
    $strSQL = ' DELETE FROM DB2WCOL AS A ';
    $strSQL.= ' WHERE ';
    $strSQL.= ' A.WCUID = ? ';
    $strSQL.= ' AND A.WCNAME = ? ';
    $params = array($WCUID, $WCNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 1;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 1;
        }
    }
    return $rs;
}
/*
 *-------------------------------------------------------*
 *
 *-------------------------------------------------------*
*/
function cmUpdFDB2CSV3($db2con, $D3NAME, $D3JSEQ, $value) {
    $rs = 0;
    $strSQL = ' UPDATE FDB2CSV3 AS A SET ';
    $strSQL.= ' A.D3DAT = ? ';
    $strSQL.= ' WHERE ';
    $strSQL.= ' A.D3NAME = ? ';
    $strSQL.= ' AND A.D3JSEQ = ? ';
    $params = array($value, $D3NAME, $D3JSEQ);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        e_log("err" . db2_stmt_errormsg(), '1');
        $rs = 1;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 1;
        }
    }
    return $rs;
}
/*
function cmUpdFDB2CSV3($db2con,$D3NAME,$D3FILID,$D3FLD,$D3DAT,$type){

$rs = '0';

//構文
try {

$strSQL  = ' UPDATE FDB2CSV3 AS A SET ';
$strSQL .= ' A.D3DAT = ? ';
$strSQL .= ' WHERE ' ;
$strSQL .= ' A.D3NAME = ? ';
$strSQL .= ' AND A.D3FILID = ? ';
$strSQL .= ' AND A.D3FLD = ? ';

$params = array(
$D3DAT,
$D3NAME,
$D3FILID,
$D3FLD
);

$stmt = db2_prepare($db2con,$strSQL);
db2_execute($stmt,$params);

} catch(Exception $e) {
$rs = '1';
}

return $rs;

}
*/
/*
 *-------------------------------------------------------*
 * D2HED取得
 *-------------------------------------------------------*
*/
function cmGetD2HED($db2con, $d1name, $JSEQ) {
    $data = array();
    $strSQL = ' SELECT ';
    $strSQL.= ' B.D2HED ';
    $strSQL.= ' FROM FDB2CSV3 AS A JOIN FDB2CSV2 as B ';
    $strSQL.= ' ON A.D3NAME = B.D2NAME AND A.D3FILID = B.D2FILID ';
    $strSQL.= ' AND A.D3FLD = B.D2FLD ';
    $strSQL.= ' WHERE A.D3NAME = ? ';
    $strSQL.= ' AND A.D3JSEQ = ? ';
    $strSQL.= ' ORDER BY A.D3JSEQ ASC ';
    $params = array($d1name, $JSEQ);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = $data[0]['D2HED'];
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * INT処理 ウエブ
 *-------------------------------------------------------*
*/
function cmIntWC($db2con, &$RTCD, $STAT, $d1name) {
    cmCallPHPQUERYWC($db2con, $d1name, $STAT, $RTCD);
}
/*
 *-------------------------------------------------------*
 * INT処理
 *-------------------------------------------------------*
*/
function cmInt($db2con, &$RTCD, &$JSEQ, $d1name) {
    cmCallPHPQUERYW($db2con, $d1name, 'INT', '', $RTCD, $JSEQ);
}
/*
 *-------------------------------------------------------*
 * DO処理
 *-------------------------------------------------------*
*/
function cmDo($db2con, &$RTCD, &$JSEQ, $filename, $d1name) {
    cmCallPHPQUERYW($db2con, $d1name, 'DO', $filename, $RTCD, $JSEQ);
}
/*
 *-------------------------------------------------------*
 * END処理
 *-------------------------------------------------------*
*/
function cmEnd($db2con, &$RTCD, &$JSEQ, $filename, $d1name) {
    cmCallPHPQUERYW($db2con, $d1name, 'END', $filename, $RTCD, $JSEQ);
}
/*
 *-------------------------------------------------------*
 * SYSTABLES
 *-------------------------------------------------------*
*/
function cmCNV($db2con, $libname) {
    cmCallCNVQRY($db2con, $libname);
}
function cmGetSystables($db2con, $library, $filename) {
    $data = array();
    $strSQL = ' SELECT A.* ';
    $strSQL.= ' FROM QSYS2/SYSTABLES AS A ';
    $strSQL.= ' WHERE A.TABLE_SCHEMA = ? ';
    $strSQL.= ' AND A.TABLE_NAME = ? ';
    $params = array($library, $filename);
    $stmt = db2_prepare($db2con, $strSQL);
    //error_log("cmGetSystables\n" . $strSQL."\n".print_r($params,true));
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}
/**
 * 指定された値が数値かチェックする（負数可）
 * （0～9,".","-"）で構成されているか？
 *
 * 引数   :1.チェックしたい値
 * 戻り値 :1.true :チェックOK
 *           false:チェックNG
 *
 */
function checkNum($data) {
    if ($data === "") {
        return true;
    }
    if (preg_match('/^[-]?[0-9]*\.?[0-9]+$/', $data)) {
        return true;
    }
    return false;
}
/**
 * 指定された日付が妥当かチェックする
 *
 * 引数   :1.チェックしたい日付
 * 戻り値 :1.true :チェックOK
 *           false:チェックNG
 *
 */
function checkALP($NEWPWD) {
    //  if($NEWPWD === ""){return true;}
    if (preg_match("/^[A-Z][a-zA-Z -]+$/", $NEWPWD)) {
        return true;
    }
}
/**
 * 指定された日付が妥当かチェックする
 *
 * 引数   :1.チェックしたい日付
 * 戻り値 :1.true :チェックOK
 *           false:チェックNG
 *
 */
function checkDateVal($date) {
    $date = (string)$date;
    $date = str_replace("/", "", $date);
    if ($date == '') {
        return true;
    }
    $date = getStrYmd($date);
    if ($date !== "") {
        if (strlen($date) === 10) {
            list($year, $momth, $day) = preg_split("/\//", $date);
            if (checkdate($momth, $day, $year)) {
                return true;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
    return false;
}
/**
 * 指定された時刻が妥当かチェックする
 *
 * 引数   :1.チェックしたい日付
 * 戻り値 :1.true :チェックOK
 *           false:チェックNG
 *
 */
function checkTimeVal($time) {
    $rtn = true;
    $time = (string)$time;
    $timeArray = preg_split('/:/', $time);
    if (count($timeArray) === 2) {
        $hour = (int)$timeArray[0];
        $min = (int)$timeArray[1];
        if ($hour < 0 || $hour > 23 || !is_numeric($hour)) {
            $rtn = false;
        }
        if ($min < 0 || $min > 59 || !is_numeric($min)) {
            $rtn = false;
        }
    } else {
        $rtn = false;
    }
    return $rtn;
}
/**
 * 日付形式（YYYYMMDD）をスラッシュあり（YYYY/MM/DD）にする
 *
 *
 * 引数   :1.編集する日付
 * 戻り値 :1.編集した日付
 *
 */
function getStrYmd($date) {
    $res = '';
    if (($date !== 0) && (strlen($date) === 8)) {
        $res = substr($date, 0, 4) . '/' . substr($date, 4, 2) . '/' . substr($date, 6, 2);
    }
    return $res;
}
function cmStr0($value) {
    if ($value === "") {
        $value = 0;
    }
    return $value;
}
/*
 *-------------------------------------------------------*
 * 全件カウント取得
 *@searchDrillListはピポットのトレイダウンの検索のテータ
 *-------------------------------------------------------*
*/
function cmGetAllCount($db2con, $fdb2csv2, $filename, $search, $D1CFLG = '', $searchDrillList = array(), $filtersearchData = array()) {
    $data = array();
    $params = array();
    $isWhereFlg = false;
    if ($D1CFLG === '') {
        // $strSQL = ' SELECT count(A.' . cmMer($fdb2csv2[0]['D2FLD']) . '_' . cmMer($fdb2csv2[0]['D2FILID']) . ') as COUNT ';
        $strSQL = ' SELECT count(*) as COUNT ';
        $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
        if ($search != '') {
            if (count($fdb2csv2) > 0) {
                $isWhereFlg = true;
                $strSQL.= ' WHERE ( ';
                foreach ($fdb2csv2 as $key => $value) {
                    switch ($value['D2TYPE']) {
                        case 'L':
                            $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . ' LIKE ? ';
                        break;
                        case 'T':
                            $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(8))' . ' LIKE ? ';
                        break;
                        case 'Z':
                            $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(26))' . ' LIKE ? ';
                        break;
                        default:
                            $strSQL.= cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
                        break;
                    }
                    array_push($params, '%' . $search . '%');
                    if ($key < (count($fdb2csv2) - 1)) {
                        $strSQL.= ' OR ';
                    }
                }
                $strSQL.= ' ) ';
            }
        }
        if (count($searchDrillList) > 0) {		
            $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
            if(!$isWhereFlg) $isWhereFlg = true;
            $paramDSQL = array();
            foreach ($searchDrillList as $key => $value) {
                switch ($value['dtype']) {
                    case 'L':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
                    break;
                    case 'T':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
                    break;
                    case 'Z':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
                    break;
                    default:
                        $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
                    break;
                }
                $params[] = cmMer($value["val"]);
            }
            $strSQL.= join(' AND ', $paramDSQL);
            $strSQL.= ' ) ';
        }
        if (count($filtersearchData) > 0) {
            $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
            $paramDSQL = array();
            foreach ($filtersearchData as $key => $value) {
                list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                switch ($value['dtype']) {
                    case 'L':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . $value['D1OPERATOR'] . ' ? ';
                    break;
                    case 'T':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . $value['D1OPERATOR'] . ' ? ';
                    break;
                    case 'Z':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . $value['D1OPERATOR'] . ' ? ';
                    break;
                    default:
                        $paramDSQL[] = $D1COL2.'_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                    break;
                }
                $params[] = cmMer($value['D1VAL']);
            }
            $strSQL.= join(' AND ', $paramDSQL);
            $strSQL.= ' ) ';
        }
    } else {
        $isWhereFlg = false;
        //$strSQL = ' SELECT count(A.' . cmMer($fdb2csv2[0]['D2FLD']) . ') as COUNT ';
        $strSQL = ' SELECT count(*) as COUNT ';
        $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
        if ($search != '') {
            $rs = fngetSQLFldCCSID($db2con, $filename);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result']);
            } else {
                //e_log('fldCCSIDresult'.print_r($rs,true));
                $fldCCSIDARR = $rs['data'];
                $fldccsidCnt = $rs['CCSIDCNT'];
                if ($fldccsidCnt > 0) {
                    $fldArr = array();
                    $isWhereFlg = true;
                    foreach ($fldCCSIDARR as $fldccsid) {
                        switch ($fldccsid['DDS_TYPE']) {
                            case 'L':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(10) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL.= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(10))' . ' LIKE ? ';
                                }
                            break;
                            case 'T':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(8) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL.= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(8))' . ' LIKE ? ';
                                }
                            break;
                            case 'Z':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(26) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL.= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(26))' . ' LIKE ? ';
                                }
                            break;
                            default:
                                if ($fldccsid['CCSID'] === '65535') {
                                    if ($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW') {
                                        $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(4) CCSID 5026) ' . ' LIKE ? ';
                                    } else {
                                        $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) ' . ' LIKE ? ';
                                    }
                                } else {
                                    $fldArr[] = $fldccsid['COLUMN_NAME'] . ' LIKE ? ';
                                }
                            break;
                        }
                    }
                } else {
                    if (count($fdb2csv2) > 0) {
                        $isWhereFlg = true;
                        $strSQL.= ' WHERE ( ';
                        foreach ($fdb2csv2 as $key => $value) {
                            switch ($value['D2TYPE']) {
                                case 'L':
                                    $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(10))' . ' LIKE ? ';
                                break;
                                case 'T':
                                    $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(8))' . ' LIKE ? ';
                                break;
                                case 'Z':
                                    $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(26))' . ' LIKE ? ';
                                break;
                                default:
                                    $strSQL.= cmMer($value['D2FLD']) . ' LIKE ? ';
                                break;
                            }
                            array_push($params, '%' . $search . '%');
                            if ($key < (count($fdb2csv2) - 1)) {
                                $strSQL.= ' OR ';
                            }
                        }
                        $strSQL.= ' ) ';
                    }
                }
            }
        }
        if (count($searchDrillList) > 0) {
            $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
            if(!$isWhereFlg) $isWhereFlg = true;

            $paramDSQL = array();
            foreach ($searchDrillList as $key => $value) {
                switch ($value['dtype']) {
                    case 'L':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
                    break;
                    case 'T':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
                    break;
                    case 'Z':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
                    break;
                    default:
                        $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
                    break;
                }
                $params[] = cmMer($value["val"]);
            }
            $strSQL.= join(' AND ', $paramDSQL);
            $strSQL.= ' ) ';
        }
        if (count($filtersearchData) > 0) {
            $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
            $paramDSQL = array();
            foreach ($filtersearchData as $key => $value) {
                list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                switch ($value['dtype']) {
                    case 'L':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . $value['D1OPERATOR'] . ' ? ';
                    break;
                    case 'T':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . $value['D1OPERATOR'] . ' ? ';
                    break;
                    case 'Z':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . $value['D1OPERATOR'] . ' ? ';
                    break;
                    default:
                        $paramDSQL[] = $D1COL2 . $value['D1OPERATOR'] . ' ? ';
                    break;
                }
                $params[] = cmMer($value['D1VAL']);
            }
            $strSQL.= join(' AND ', $paramDSQL);
            $strSQL.= ' ) ';
        }
    } 
    error_log('getallcount 実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());

    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        error_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        error_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true, 'data' => $data[0]);
        }
    }
    return $data;
}
/**
 * クエリデータに集計されたデータの全件カウント取得
 *
 * RESULT
 *    01：データ件数
 *    02：エラーメッセージ
 *
 * @param Object  $db2con    DBコネクション
 * @param String  $fdb2csv2  クエリデータ
 * @param String  $d1name    クエリ名
 * @param String  $filename  ファイル名
 * @param String  $search    検索キー
 *
 */
function cmGetAllCountShukei($db2con, $fdb2csv2, $d1name, $filename, $search) {
    $data = array();
    $params = array();
    $levelFlg = '';
    $normalFlg = '';
    $lastColumn = '';
    $joukenFlg = '';
    $groupingVar = 'rownum';
    $fldvar2 = '';
    $key2 = 0;
    //集計するフィールド情報取得
    $rsmaster = cmGetDB2COLM($db2con, $d1name);
    if ($rsmaster['result'] !== true) {
        $msg = showMsg($rsmaster['result']);
        $rtn = 1;
    } else {
        $rsMasterArr = $rsmaster['data'];
    }
    if ($rsmaster !== null) {
        //クエリの作成
        $strSQL = 'SELECT ';
        $strSQL.= ' COUNT(*) as COUNT ';
        $strSQL.= ' from ( ';
        $strSQL.= ' SELECT ';
        //reset($fdb2csv2);
        //while (list($key, $value) = each($fdb2csv2)) {
        foreach ($fdb2csv2 as $key => $row) {
            $fldvar2 = cmMer($row['D2FLD']) . '_' . cmMer($row['D2FILID']);
            for ($i = 1;$i <= 6;$i++) {
                if ($fldvar2 === (cmMer($rsMasterArr[0]['DCFLD' . ($i) ]) . '_' . cmMer($rsMasterArr[0]['DCFILID' . ($i) ]))) {
                    // 集計結果行も含めてカウントする
                    $strSQL.= ' case when grouping(' . $groupingVar . ') = 1 then \'合計' . '\' else ' . $fldvar2 . ' end as ' . $fldvar2 . ',';
                    $groupingVar = $fldvar2;
                    if ($gpFirstFlg === 1) {
                        $gpFirstVar = $fldvar2;
                        $gpFirstFlg = 2;
                    }
                    $flg = 1;
                    $levelFlg.= $fldvar2 . ',';
                    $joukenFlg.= ' OR ' . 'char(' . $fldvar2 . ')' . '=' . '\'合計' . '\'';
                }
            }
            if ($flg !== 1) {
                $strSQL.= $fldvar2 . ',';
                $normalFlg.= ($fldvar2) . ',';
            }
            $flg = 0;
        }
        $cnt = count($fdb2csv2) - 1;
        $lastColumn = cmMer($fdb2csv2[$cnt]['D2FLD']) . '_' . cmMer($fdb2csv2[$cnt]['D2FILID']);
        $strSQL = rtrim($strSQL, ',');
        $strSQL.= ' from ( ';
        $strSQL.= ' select A.*,rownumber() over() rownum from ' . SAVE_DB . '/' . $filename;
        $strSQL.= ' as A ';
        //検索条件
        if ($search != '') {
            if (count($fdb2csv2) > 0) {
                $strSQL.= ' WHERE ';
                //reset($fdb2csv2);
                //while (list($key, $value) = each($fdb2csv2)) {
                foreach ($fdb2csv2 as $key => $value) {
                    $strSQL.= cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
                    array_push($params, '%' . $search . '%');
                    if ($key < (count($fdb2csv2) - 1)) {
                        $strSQL.= ' OR ';
                    }
                }
            }
        }
        $strSQL.= ' ) as B ';
        $strSQL.= ' group by rollup(';
        $normalFlg = rtrim($normalFlg, ',');
        $strSQL.= $levelFlg . 'rownum,' . $normalFlg;
        $strSQL.= ') ';
        $strSQL.= ' ) as C ';
        $strSQL.= ' where ' . $lastColumn . ' is not null ';
        $strSQL.= $joukenFlg;
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            // クエリの実行
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array('result' => 'FAIL_SEL');
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row['COUNT'];
                }
                $data = array('result' => true, 'data' => $data[0]);
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * CL文字列生成
 *-------------------------------------------------------*
*/
function setCmd($db2con, $d1name, $D1WEBF, $cndData = array()) {
    e_log("setCmd" . $D1WEBF);
    //cmSetPHPQUERY($db2con);
    $cnddata = array();
    if ($D1WEBF === '1') {
        $cnddata = cndDataArrChange($cndData);
    }
    $rs = '0';
    // FDB2CSV1PG
    $strSQL = ' SELECT A.DGBLIB, A.DGBPGM, A.DGALIB, A.DGAPGM ';
    $strSQL.= ' FROM ' . MAINLIB . '/FDB2CSV1PG AS A ';
    $strSQL.= ' WHERE A.DGNAME = ? ';
    $stmt1 = db2_prepare($db2con, $strSQL);
    if ($stmt1 === false) {
        $rs = '1';
    } else {
        $params = array($d1name);
        $r = db2_execute($stmt1, $params);
        if ($r === false) {
            $rs = '1';
        } else {
            // FDB2CSV1PGのデータ数だけループ
            while ($row = db2_fetch_assoc($stmt1)) {
                $strCmdB = '';
                $strCmdA = '';
                //実行前コマンド生成(ライブラリとプログラムのみ)
                if (cmMer($row['DGBLIB']) !== '' && cmMer($row['DGBPGM']) !== '') {
                    $strCmdB = 'CALL PGM(' . cmMer($row['DGBLIB']) . '/' . cmMer($row['DGBPGM']) . ')';
                }
                //実行後コマンド生成(ライブラリとプログラムのみ)
                if (cmMer($row['DGALIB']) !== '' && cmMer($row['DGAPGM']) !== '') {
                    $strCmdA = 'CALL PGM(' . cmMer($row['DGALIB']) . '/' . cmMer($row['DGAPGM']) . ')';
                }
                //cmSetPHPQUERY($db2con);
                //FDB2CSV1PMを取得
                $strSQL = ' SELECT A.DMPTYPE, A.DMLEN, A.DMSCAL, A.DMIZDT, A.DMSCFG, A.DMSCFD, A.DMFRTO,A.DMMSEQ,A.DMSSEQ ';
                $strSQL.= ' FROM ' . MAINLIB . '/FDB2CSV1PM AS A ';
                $strSQL.= ' WHERE A.DMNAME = ? AND A.DMEVENT = ?';
                $strSQL.= ' ORDER BY A.DMSEQ ,A.DMFRTO ';
                $stmt2 = db2_prepare($db2con, $strSQL);
                if ($stmt2 === false) {
                    $rs = '1';
                } else {
                    // コマンド作成 STR --------------------------------
                    $d1event = ''; //フラグ　1:実行前 2:実行後
                    //必ず２回ループ
                    for ($count = 0;$count < 2;$count++) {
                        $d1event = strval($count + 1);
                        $params = array($d1name, $d1event);
                        $r = db2_execute($stmt2, $params);
                        if ($r === false) {
                            $rs = '1';
                        } else {
                            // パラメータ作成 STR --------------------------------
                            $strPrm = '';
                            while ($row = db2_fetch_assoc($stmt2)) {
                                $tmpPrm = '';
                                $wtype = cmMer($row['DMPTYPE']); //型 01:文字 02:数値
                                $wlen = cmMer($row['DMLEN']); //長さ
                                $wscal = cmMer($row['DMSCAL']); //スケール
                                $wscfg = cmMer($row['DMSCFG']); //検索対象 1:検索 ブランク:非検索
                                $wscfd = cmMer($row['DMSCFD']); //選択フィールド
                                $wfrto = cmMer($row['DMFRTO']); //FROM TO
                                $wmseq = cmMer($row['DMMSEQ']); //MAIN SEQUENCE NUMBER
                                $wsseq = cmMer($row['DMSSEQ']); //INNER SEQUENCE NUMBER
                                //初期値がある場合は初期値を値として保持
                                if ($wscfd === '') {
                                    $wizdt = cmMer($row['DMIZDT']); //初期値
                                    
                                } else {
                                    if ($D1WEBF !== '1') {
                                        //初期値がない場合、QTEMP/FDB2CSV3から値を取得
                                        //cmSetQTEMP($db2con);
                                        e_log("setcmd " . $d1name . ' ' . $wscfd);
                                        $getParam = getParmData($db2con, $d1name, $wscfd);
                                        $wizdt = $getParam['D3DAT']; //'01' '02'
                                        $type = cmMer($getParam['D3CND']);
                                        if ($type === 'RANGE') {
                                            if ($wtype === '01') {
                                                //$wizdt = explode("' '", trim($wizdt));
                                                $wizdt = makeParamArr($wizdt);
                                            } else if ($wtype === '02') {
                                                $wizdt = explode(" ", $wizdt);
                                                $wizdt = array_values(array_filter($wizdt, 'strlen'));
                                            }
                                        }
                                    } else {
                                        $wizdt = $cnddata[$wmseq][$wsseq]['CNDDATA'];
                                        $getParam = $cnddata[$wmseq][$wsseq];
                                        $type = cmMer($getParam['CNDKBN']);
                                    }
                                    if ($type === 'RANGE') {
                                        //RANGEの場合、文字か数値を判断して入力文字を分割
                                        if ($wtype === '01') {
                                            list($wizdt1, $wizdt2) = $wizdt;
                                            //$wizdt1 = trim($wizdt1, "'");
                                            //$wizdt2 = trim($wizdt2, "'");
                                            
                                        } else if ($wtype === '02') {
                                            list($wizdt1, $wizdt2) = $wizdt;
                                        }
                                        //FRTOを見て、wizdt1にするかwizdt2にするかを決める
                                        if ($wfrto === '1') {
                                            $wizdt = $wizdt1;
                                        } else if ($wfrto === '2') {
                                            $wizdt = $wizdt2;
                                        }
                                    } else {
                                        //RANGE以外の場合は単純に両サイドコーテーションを削除
                                        if ($D1WEBF !== '1') {
                                            //e_log('param'.$wizdt.'aaa'.cmQuotCheck($wizdt, 'A'));
                                            $wizdt = cmQuotCheck($wizdt, 'A');
                                        } else {
                                            $w = '';
                                            foreach ($wizdt as $key => $value) {
                                                $w.= $value . ' ';
                                            }
                                            $wizdt = cmMer($w);
                                        }
                                    }
                                }
                                //文字の場合はブランク埋めして両サイドにコーテーション
                                //数値の場合はヘキサ文字に変換
                                switch ($wtype) {
                                        // 文字
                                        
                                    case '01':
                                        //UFT8バイト計算で文字列バイトを取得
                                        $u8byte = strlen(($wizdt));
                                        //ASのバイト数を取得
                                        $asbyteAry = checkByte($wizdt);
                                        $asbyte = $asbyteAry[0];
                                        //ASバイト数がUTFバイト計算のバイト数を超えている場合、誤差を算出し$wlenに足す
                                        if ($asbyte < $u8byte) {
                                            $wlen = $wlen + ($u8byte - $asbyte);
                                        } else {
                                            $wlen = $wlen - ($asbyte - $u8byte);
                                        }
                                        $tmpPrm = '\'' . str_pad($wizdt, $wlen) . '\'';
                                        //$tmpPrm = '\'' . str_pad(trim($wizdt), $wlen) . '\'';
                                        
                                    break;
                                        // 数値
                                        
                                    case '02':
                                        $tmpPrm = getHexaInt($wlen, $wizdt, $wscal); //convert int data to hexa byte
                                        
                                    break;
                                    default:
                                }
                                $strPrm.= ' ' . $tmpPrm;
                        }
                    }
                    //cmSetQTEMP($db2con);
                    e_log('cmdCmdA.=' . $strCmdA . ' PARM(' . $strPrm . ')');
                    // パラメータ作成 END --------------------------------
                    // コマンド登録 STR --------------------------------
                    if (trim($strPrm) != '') {
                        $strCmd = '';
                        $strSQL3 = ' UPDATE QTEMP/FDB2CSV1 ';
                        $strSQL3.= ' SET ';
                        switch ($d1event) {
                            case '2':
                                $strSQL3.= ' D1CMDE = ? ';
                                $strCmd = $strCmdA . ' PARM(' . $strPrm . ')';
                            break;
                            case '1':
                            default:
                                $strSQL3.= ' D1CMD = ? ';
                                $strCmd = $strCmdB . ' PARM(' . $strPrm . ')';
                            break;
                        }
                        $strSQL3.= ' WHERE ';
                        $strSQL3.= ' D1NAME = ? ';
                        $stmt3 = db2_prepare($db2con, $strSQL3);
                        $params = array($strCmd, $d1name);
                        db2_execute($stmt3, $params);
                    } else {
                        $strCmd = '';
                        $strSQL3 = ' UPDATE QTEMP/FDB2CSV1 ';
                        $strSQL3.= ' SET ';
                        switch ($d1event) {
                            case '2':
                                $strSQL3.= ' D1CMDE = ? ';
                                $strCmd = $strCmdA;
                            break;
                            case '1':
                            default:
                                $strSQL3.= ' D1CMD = ? ';
                                $strCmd = $strCmdB;
                            break;
                        }
                        $strSQL3.= ' WHERE ';
                        $strSQL3.= ' D1NAME = ? ';
                        $stmt3 = db2_prepare($db2con, $strSQL3);
                        if ($stmt3 === false) {
                            e_log("err" . db2_stmt_errormsg(), '1');
                            $rs = '1';
                        } else {
                            $params = array($strCmd, $d1name);
                            $r = db2_execute($stmt3, $params);
                            if ($r === false) {
                                e_log("err" . db2_stmt_errormsg(), '1');
                                $rs = '1';
                            }
                        }
                    }
                    // コマンド登録 END --------------------------------
                    
                }
                // コマンド作成 END --------------------------------
                
            }
        }
    }
}
return $rs;
}
//reform array of cndData of web
function cndDataArrChange($cndData) {
    $data = array();
    if ($cndData !== '') {
        foreach ($cndData as $key => $value) {
            foreach ($value['CNDSDATA'] as $k => $v) {
                $d3jseq = $v['CDATAIDX'];
                $d3dat = $v['CNDDATA'];
                $data[$value['CNDIDX']][$v['CDATAIDX']] = array('CNDDATA' => $v['CNDDATA'], 'CNDKBN' => $v['CNDKBN']);
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * CL文字列生成
 *-------------------------------------------------------*
*/
function setSQLCmd($db2con, $d1name, $D1WEBF, $cndData = array()) {
    e_log("setSQLCmd");
    //cmSetPHPQUERY($db2con);
    $cnddata = array();
    $cnddata = sqlCndDataArrChange($cndData);
    $rs = '0';
    // FDB2CSV1PG
    $strSQL = ' SELECT ';
    $strSQL.= '     A.DGBLIB, ';
    $strSQL.= '     A.DGBPGM, ';
    $strSQL.= '     A.DGALIB, ';
    $strSQL.= '     A.DGAPGM ';
    $strSQL.= ' FROM ';
    $strSQL.= MAINLIB . '/FDB2CSV1PG AS A ';
    $strSQL.= ' WHERE ';
    $strSQL.= '     A.DGNAME =? ';
    $stmt1 = db2_prepare($db2con, $strSQL);
    if ($stmt1 === false) {
        $rs = '1';
    } else {
        $params = array($d1name);
        $r = db2_execute($stmt1, $params);
        if ($r === false) {
            $rs = '1';
        } else {
            // FDB2CSV1PGのデータ数だけループ
            while ($row = db2_fetch_assoc($stmt1)) {
                $strCmdB = '';
                $strCmdA = '';
                //実行前コマンド生成(ライブラリとプログラムのみ)
                if (trim($row['DGBLIB']) !== '' && trim($row['DGBPGM']) !== '') {
                    $strCmdB = 'CALL PGM(' . trim($row['DGBLIB']) . '/' . trim($row['DGBPGM']) . ')';
                }
                //実行後コマンド生成(ライブラリとプログラムのみ)
                if (trim($row['DGALIB']) !== '' && trim($row['DGAPGM']) !== '') {
                    $strCmdA = 'CALL PGM(' . trim($row['DGALIB']) . '/' . trim($row['DGAPGM']) . ')';
                }
                //cmSetPHPQUERY($db2con);
                //FDB2CSV1PMを取得
                $strSQL = ' SELECT ';
                $strSQL.= '     A.DMPTYPE, ';
                $strSQL.= '     A.DMLEN, ';
                $strSQL.= '     A.DMSCAL, ';
                $strSQL.= '     A.DMIZDT, ';
                $strSQL.= '     A.DMSCFG, ';
                $strSQL.= '     A.DMSCFD, ';
                $strSQL.= '     A.DMFRTO, ';
                $strSQL.= '     A.DMMSEQ, ';
                $strSQL.= '     A.DMSSEQ ';
                $strSQL.= ' FROM ';
                $strSQL.= MAINLIB . '/FDB2CSV1PM AS A ';
                $strSQL.= ' WHERE ';
                $strSQL.= '     A.DMNAME = ? AND ';
                $strSQL.= '     A.DMEVENT = ? ';
                $strSQL.= ' ORDER BY ';
                $strSQL.= '     A.DMSEQ , ';
                $strSQL.= '     A.DMFRTO ';
                $stmt2 = db2_prepare($db2con, $strSQL);
                if ($stmt2 === false) {
                    $rs = '1';
                } else {
                    // コマンド作成 STR --------------------------------
                    $d1event = ''; //フラグ　1:実行前 2:実行後
                    //必ず２回ループ
                    for ($count = 0;$count < 2;$count++) {
                        $d1event = strval($count + 1);
                        $params = array($d1name, $d1event);
                        $r = db2_execute($stmt2, $params);
                        if ($r === false) {
                            $rs = '1';
                        } else {
                            // パラメータ作成 STR --------------------------------
                            $strPrm = '';
                            while ($row = db2_fetch_assoc($stmt2)) {
                                e_log('parameterdata:' . print_r($row, true));
                                $tmpPrm = '';
                                $wtype = cmMer($row['DMPTYPE']); //型 01:文字 02:数値
                                $wlen = cmMer($row['DMLEN']); //長さ
                                $wscal = cmMer($row['DMSCAL']); //スケール
                                $wscfg = cmMer($row['DMSCFG']); //検索対象 1:検索 ブランク:非検索
                                $wscfd = cmMer($row['DMSCFD']); //選択フィールド
                                $wfrto = cmMer($row['DMFRTO']); //FROM TO
                                $wmseq = cmMer($row['DMMSEQ']); //MAIN SEQUENCE NUMBER
                                $wsseq = cmMer($row['DMSSEQ']); //INNER SEQUENCE NUMBER
                                //初期値がある場合は初期値を値として保持
                                if (trim($wscfd) === '') {
                                    $wizdt = trim($row['DMIZDT']); //初期値
                                    
                                } else {
                                    $wizdt = $cnddata[$wscfd]['CNDDATA'];
                                    $getParam = $cnddata[$wscfd];
                                    $wizdt = rtrim($wizdt);
                                }
                                //文字の場合はブランク埋めして両サイドにコーテーション
                                //数値の場合はヘキサ文字に変換
                                switch ($wtype) {
                                        // 文字
                                        
                                    case '01':
                                        //UFT8バイト計算で文字列バイトを取得
                                        $u8byte = strlen((trim($wizdt)));
                                        //ASのバイト数を取得
                                        $asbyteAry = checkByte(trim($wizdt));
                                        $asbyte = $asbyteAry[0];
                                        //ASバイト数がUTFバイト計算のバイト数を超えている場合、誤差を算出し$wlenに足す
                                        if ($asbyte < $u8byte) {
                                            $wlen = $wlen + ($u8byte - $asbyte);
                                        } else {
                                            $wlen = $wlen - ($asbyte - $u8byte);
                                        }
                                        $tmpPrm = '\'' . str_pad(trim($wizdt), $wlen) . '\'';
                                        //$tmpPrm = '\'' . str_pad(trim($wizdt), $wlen) . '\'';
                                        
                                    break;
                                        // 数値
                                        
                                    case '02':
                                        $tmpPrm = getHexaInt($wlen, $wizdt, $wscal); //convert int data to hexa byte
                                        
                                    break;
                                    default:
                                }
                                $strPrm.= ' ' . $tmpPrm;
                        }
                    }
                    //cmSetQTEMP($db2con);
                    e_log('cmdCmdA.=' . $strCmdA . ' PARM(' . $strPrm . ')');
                    // パラメータ作成 END --------------------------------
                    // コマンド登録 STR --------------------------------
                    if (trim($strPrm) != '') {
                        $strCmd = '';
                        $strSQL3 = ' UPDATE QTEMP/FDB2CSV1 ';
                        $strSQL3.= ' SET ';
                        switch ($d1event) {
                            case '2':
                                $strSQL3.= ' D1CMDE = ? ';
                                $strCmd = $strCmdA . ' PARM(' . $strPrm . ')';
                            break;
                            case '1':
                            default:
                                $strSQL3.= ' D1CMD = ? ';
                                $strCmd = $strCmdB . ' PARM(' . $strPrm . ')';
                            break;
                        }
                        $strSQL3.= ' WHERE ';
                        $strSQL3.= ' D1NAME = ? ';
                        $stmt3 = db2_prepare($db2con, $strSQL3);
                        $params = array($strCmd, $d1name);
                        db2_execute($stmt3, $params);
                    } else {
                        $strCmd = '';
                        $strSQL3 = ' UPDATE QTEMP/FDB2CSV1 ';
                        $strSQL3.= ' SET ';
                        switch ($d1event) {
                            case '2':
                                $strSQL3.= ' D1CMDE = ? ';
                                $strCmd = $strCmdA;
                            break;
                            case '1':
                            default:
                                $strSQL3.= ' D1CMD = ? ';
                                $strCmd = $strCmdB;
                            break;
                        }
                        $strSQL3.= ' WHERE ';
                        $strSQL3.= ' D1NAME = ? ';
                        $stmt3 = db2_prepare($db2con, $strSQL3);
                        if ($stmt3 === false) {
                            e_log("err" . db2_stmt_errormsg(), '1');
                            $rs = '1';
                        } else {
                            $params = array($strCmd, $d1name);
                            $r = db2_execute($stmt3, $params);
                            if ($r === false) {
                                e_log("err" . db2_stmt_errormsg(), '1');
                                $rs = '1';
                            }
                        }
                    }
                    // コマンド登録 END --------------------------------
                    
                }
                // コマンド作成 END --------------------------------
                
            }
        }
    }
}
return $rs;
}
//reform array of cndData of web
function sqlCndDataArrChange($cndData) {
    $data = array();
    foreach ($cndData as $k => $v) {
        $d3jseq = $v['CNDSEQ'];
        $d3dat = $v['CNDDAT'];
        $data[$v['CNDSEQ']] = array('CNDDATA' => $v['CNDDAT'], 'CNDKBN' => $v['CNDSTKB']);
    }
    return $data;
}
//convert int data to hexa byte
function getHexaInt($wlen, $wizdt, $wscal) {
    $tmpPrm = 'X\'';
    // 偶数
    if ($wlen % 2 == 0) {
        $tmpPrm.= '0';
    }
    // 符号
    $signval = 'F';
    $signpos = strpos($wizdt, '-');
    if ($signpos !== false) {
        $signval = 'D';
        $wizdt = substr($wizdt, $signpos + 1);
    }
    // 小数点あり
    if ($wscal != 0) {
        $intlen = $wlen - $wscal;
        $scalpos = strpos($wizdt, '.');
        if ($scalpos !== false) {
            $intval = substr($wizdt, 0, $scalpos);
            $scalval = substr($wizdt, $scalpos + 1);
            $tmpPrm.= str_pad($intval, $intlen, '0', STR_PAD_LEFT);
            $tmpPrm.= str_pad($scalval, $wscal, '0');
        } else {
            $intval = $wizdt;
            $scalval = 0;
            $tmpPrm.= str_pad($intval, $intlen, '0', STR_PAD_LEFT);
            $tmpPrm.= str_pad($scalval, $wscal, '0');
        }
    } else {
        $tmpPrm.= str_pad($wizdt, $wlen, '0', STR_PAD_LEFT);
    }
    $tmpPrm.= $signval . '\'';
    return $tmpPrm;
}
/*
 *-------------------------------------------------------*
 * CL文字列生成(値の取得)
 *-------------------------------------------------------*
*/
function getParmData($db2con, $d1name, $d3fld) {
    $data = array();
    // FDB2CSV1PG
    $strSQL = ' SELECT A.D3DAT,A.D3CND ';
    $strSQL.= ' FROM QTEMP/FDB2CSV3 AS A ';
    $strSQL.= ' WHERE A.D3NAME = ? AND A.D3FLD = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $params = array($d1name, $d3fld);
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data = $row;
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * CL文字列生成(値の取得)ウエブ
 *-------------------------------------------------------*
*/
function getWebParmData($db2con, $d1name, $d3fld) {
    $data = array();
    // FDB2CSV1PG
    $strSQL = ' SELECT A.D3DAT,A.D3CND ';
    $strSQL.= ' FROM FDB2CSV3 AS A ';
    $strSQL.= ' WHERE A.D3NAME = ? AND A.D3FLD = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $params = array($d1name, $d3fld);
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data = $row;
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * 検索条件更新
 *-------------------------------------------------------*
*/
function updSearchCond($db2con, $data, $d1name) {
    $rs = 0;
    foreach ($data as $key => $value) {
        $d3jseq = $value['D3JSEQ'];
        $d3dat = $value['D3DAT'];
        $d3cnd = $value['D3CND'];
        $d3len = $value['D3LEN'];
        if ($d3cnd === 'LIKE' || $d3cnd === 'NLIKE') {
            $d3dat = likeConvert($d3dat, $d3len);
        } else if ($d3cnd === 'EQ' && (strtoupper($d3dat) === "'NULL'")) {
            $d3dat = 'NULL';
        }
        //デバッグ用配列　更新内容をリターン
        $d3datArray['seq' . $d3jseq] = $d3dat;
        //バイト数チェック
        if (!(checkMaxLen($d3dat, 128))) {
            $rs = 1;
        } else {
            $rs = cmUpdFDB2CSV3($db2con, $d1name, $d3jseq, $d3dat);
        }
        if ($rs === 1) {
            break;
        }
    }
    $currentdata = fnGetCurrentSchema($db2con);
    /****DEMO/COUNT*****************************/
    /*
    $Tsql  = 'select * from FDB2CSV3 where D3NAME = ? ';
    $Tstmt = db2_prepare($db2con, $Tsql);
    $param = array(
    $d1name
    );
    db2_execute($Tstmt, $param);
    
    while ($row = db2_fetch_array($Tstmt)) {
    //error_log(print_r($row, true));
    }
    */
    /*******************************************/
    return array($rs, $d3datArray);
}
/***************test*******************/
function fnGetCurrentSchema($db2con) {
    $data = array();
    try {
        $strSQL = ' VALUES CURRENT SCHEMA ';
        $params = array();
        $stmt = db2_prepare($db2con, $strSQL);
        db2_execute($stmt, $params);
        if ($stmt) {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    catch(Exception $e) {
        $rs = false;
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * LIKE条件に％を付与
 *-------------------------------------------------------*
*/
function likeConvert($d3dat, $d3len, $flg = '0') {
    $typeflg = false;
    //コーテーションを外す(最初と最後の文字を削除)
    if (substr($d3dat, 0, 1) === "'" && substr($d3dat, -1) === "'") {
        $d3dat = ltrim($d3dat, "'");
        $d3dat = rtrim($d3dat, "'");
        $typeflg = true;
    }
    if ($flg === '1') {
        $d3dat = "concat(concat('%'," . $d3dat . "),'%')";
    } else {
        //前方％の場合
        if (mb_substr($d3dat, 0, 1) == '%' && mb_substr($d3dat, -1) != '%') {
            //%を全て削除
            $d3dat = str_replace("%", "", $d3dat);
            if ($d3dat != '') {
                //Byte数取得
                $byteAry = checkByte($d3dat);
                $updlen = $byteAry[0];
                //1Byte以上空きがある場合、%を付与
                if (((int)$d3len - $updlen) >= 1) {
                    $d3dat = "'%" . $d3dat . "'";
                } else {
                    if ($typeflg) {
                        $d3dat = "'" . $d3dat . "'";
                    }
                }
            }
        } else if (mb_substr($d3dat, 0, 1) != '%' && mb_substr($d3dat, -1) == '%') {
            //後方％の場合
            //%を全て削除
            $d3dat = str_replace("%", "", $d3dat);
            if ($d3dat != '') {
                //Byte数取得
                $byteAry = checkByte($d3dat);
                $updlen = $byteAry[0];
                //1Byte以上空きがある場合、%を付与
                if (((int)$d3len - $updlen) >= 1) {
                    $d3dat = "'" . $d3dat . "%'";
                } else {
                    if ($typeflg) {
                        $d3dat = "'" . $d3dat . "'";
                    }
                }
            }
        } else if (mb_substr($d3dat, 0, 1) == '%' && mb_substr($d3dat, -1) == '%') {
            //%を全て削除
            $d3dat = str_replace("%", "", $d3dat);
            if ($d3dat != '') {
                //Byte数取得
                $byteAry = checkByte($d3dat);
                $updlen = $byteAry[0];
                //2Byte以上空きがある場合、両サイドに%を付与
                if (((int)$d3len - $updlen) >= 2) {
                    $d3dat = "'%" . $d3dat . "%'";
                } else if (((int)$d3len - $updlen) >= 1) {
                    //elseで1Byte以上の空きがある場合は、前方一致
                    $d3dat = "'" . $d3dat . "%'";
                } else {
                    if ($typeflg) {
                        $d3dat = "'" . $d3dat . "'";
                    }
                }
            }
        } else {
            //%を全て削除
            $d3dat = str_replace("%", "", $d3dat);
            if ($d3dat != '') {
                //Byte数取得
                $byteAry = checkByte($d3dat);
                $updlen = $byteAry[0];
                //2Byte以上空きがある場合、両サイドに%を付与
                if (((int)$d3len - $updlen) >= 2) {
                    $d3dat = "'%" . $d3dat . "%'";
                } else if (((int)$d3len - $updlen) >= 1) {
                    //elseで1Byte以上の空きがある場合は、前方一致
                    $d3dat = "'" . $d3dat . "%'";
                } else {
                    if ($typeflg) {
                        $d3dat = "'" . $d3dat . "'";
                    }
                }
            }
        }
    }
    return $d3dat;
}
/*
 *-------------------------------------------------------*
 * LIKE条件に前方一致％を付与
 *-------------------------------------------------------*
*/
function leftLikeConvert($d3dat, $d3len, $flg = '0') {
    $typeflg = false;
    if (substr($d3dat, 0, 1) === "'" && substr($d3dat, -1) === "'") {
        //コーテーションを外す(最初の文字を削除)
        $d3dat = ltrim($d3dat, "'");
        $d3dat = rtrim($d3dat, "'");
        $typeflg = true;
    }
    //%を全て削除
    $d3dat = str_replace("%", "", $d3dat);
    if ($d3dat != '') {
        if ($flg === '1') {
            $d3dat = "concat('%'," . $d3dat . ")";
        } else {
            //Byte数取得
            $byteAry = checkByte($d3dat);
            $updlen = $byteAry[0];
            //1Byte以上空きがある場合、%を付与
            if (((int)$d3len - $updlen) >= 1) {
                $d3dat = "'%" . $d3dat . "'";
            } else {
                if ($typeflg) {
                    $d3dat = "'" . $d3dat . "'";
                }
            }
        }
    }
    return $d3dat;
}
/*
 *-------------------------------------------------------*
 * LIKE条件に後方一致％を付与
 *-------------------------------------------------------*
*/
function rightLikeConvert($d3dat, $d3len, $flg = '0') {
    $typeflg = false;
    if (substr($d3dat, 0, 1) === "'" && substr($d3dat, -1) === "'") {
        //コーテーションを外す(最後の文字を削除)
        $d3dat = ltrim($d3dat, "'");
        $d3dat = rtrim($d3dat, "'");
        $typeflg = true;
    }
    //%を全て削除
    $d3dat = str_replace("%", "", $d3dat);
    if ($d3dat != '') {
        if ($flg === '1') {
            $d3dat = "concat(" . $d3dat . ",'%')";
        } else {
            //Byte数取得
            $byteAry = checkByte($d3dat);
            $updlen = $byteAry[0];
            //1Byte以上空きがある場合、%を付与
            if (((int)$d3len - $updlen) >= 1) {
                $d3dat = "'" . $d3dat . "%'";
            } else {
                if ($typeflg) {
                    $d3dat = "'" . $d3dat . "'";
                }
            }
        }
    }
    return $d3dat;
}
/*
 *-------------------------------------------------------*
 * カラー情報取得
 *-------------------------------------------------------*
*/
function cmGetDB2WUSR($db2con, $id) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT A.* ';
    $strSQL.= ' FROM DB2WUSR as A ';
    $strSQL.= ' WHERE WUUID <> \'\' ';
    if ($id != '') {
        $strSQL.= ' AND WUUID = ? ';
        array_push($params, $id);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}
function cmGetJsonDB2WCOL($DB2WCOL) {
    $rtnArray = array();
    foreach ($DB2WCOL as $key => $value) {
        if ($value['WCLOCK'] === '1') {
            $value['WCLOCK'] = true;
        } else {
            $value['WCLOCK'] = false;
        }
        if ($value['WCHIDE'] === '1') {
            $value['WCHIDE'] = true;
        } else {
            $value['WCHIDE'] = false;
        }
        $rtnArray[] = array('index' => $value['WCINDX'], 'text' => $value['WCTEXT'], 'type' => $value['WCTYPE'], 'locked' => $value['WCLOCK'], 'hidden' => $value['WCHIDE']);
    }
    return json_encode($rtnArray);
}
function zenSpace100() {
    $space = '';
    for ($i = 0;$i < 100;$i++) {
        $space.= '　';
    }
    return $space;
}
/*
 *-------------------------------------------------------*
 * FDB2CSV2チェック
 *@SEIGYOFLG=true：制御なら入れないSEQをチェックしない
 *-------------------------------------------------------*
*/
function cmCheckFDB2CSV2($db2con, $D2NAME, $D2FILID, $D2FLD,$SEIGYOFLG=false) {
    $rtn = data;
    $strSQL = ' SELECT * ';
    $strSQL.= ' FROM FDB2CSV2 ';
    $strSQL.= ' WHERE D2NAME = ? AND D2FILID = ? AND D2FLD = ?  ';
	$params[] =$D2NAME;
    $params[] = (int)$D2FILID;
	$params[] =$D2FLD;
	if($SEIGYOFLG!==true){
	    $strSQL.= ' AND D2CSEQ > 0 ';
	}
    $stmt = db2_prepare($db2con, $strSQL);   
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * ログインユーザが削除されるかどうかチェック
 *
 * RESULT
 *    01：データ件数
 *    02：false
 *
 * @param Object  $db2con DBコネクション
 * @param String  $id     ユーザーID
 *
 *-------------------------------------------------------*
*/
function cmGetWUAUTH($db2con, $id) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT A.* ';
    //    $strSQL .= ' FROM DB2WUSR as A ';
    $strSQL.= ' FROM DB2WUSR as A ';
    $strSQL.= ' WHERE WUUID <> \'\' ';
    if ($id != '') {
        $strSQL.= ' AND WUUID = ? ';
        array_push($params, $id);
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array('result' => 'FAIL_SEL');
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                if (count($data) === 0) {
                    $data = array('result' => 'NOTEXIST_GET');
                } else if ($data[0]['WUAUTH'] === '0') {
                    $data = array('result' => 'FAIL_USR_AUT', 'data' => $data);
                } else {
                    $data = array('result' => true, 'data' => $data);
                }
            }
        }
    } else {
        $data = array('result' => 'CHECK_LOGOUT');
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * FDB2CSV5チェック
 *@SEIGYOFLG=true：制御なら入れないSEQをチェックしない
 *-------------------------------------------------------*
*/
function cmCheckFDB2CSV5($db2con, $D5NAME, $D5FLD,$SEIGYOFLG = false) {
    $data = array();
	$params=array();
    $strSQL = ' SELECT * ';
    $strSQL.= ' FROM FDB2CSV5 ';
    $strSQL.= ' WHERE D5NAME = ? AND D5FLD = ? ';
	$params[]=$D5NAME;
    $params[]=$D5FLD;
	if($SEIGYOFLG!==true){
	    $strSQL.= ' AND D5CSEQ > 0 ';
	}
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}
function num_format($Integer) {
    $chk = checkNum($Integer);
    if ($chk === true) {
        $Integer = cmHscDe($Integer);
        $Blanks = strlen($Integer) % 3;
        if ($Blanks !== 0) {
            for ($i = 0;3 - $Blanks > $i;$i++) {
                $Integer = ' ' . $Integer;
            }
        }
        $FigureInteger = substr($Integer, 0, 3);
        $j = 2;
        if ($Integer{2} == '-') {
            $FigureInteger = $FigureInteger . substr($Integer, 3, 3);
            $j = 4;
        }
        $len = strlen($Integer);
        for ($i = $j;strlen($Integer) > $i;$i++) {
            if ($i % 3 === 0) {
                $FigureInteger = $FigureInteger . ',' . substr($Integer, $i, 3);
            }
        }
        // $FigureInterger = cmHsc($FigureInteger);
        //$FigureInterger = htmlspecialchars($FigureInteger, ENT_QUOTES);
        $FigureInteger = cmHsc($FigureInteger);
        $Integer = $FigureInteger;
        $Integer = trim($Integer);
    }
    return $Integer;
}
/*
 *-------------------------------------------------------*
 * 定義チェック
 *
 *-------------------------------------------------------*
*/
function cmChkQuery($db2con, $USRNAME, $QRYNAME, $PIVOTNAME, $GPHNAME = '') {
   error_log('USRNAME***'.print_r($USRNAME,true));
   error_log('QRYNAME***'.print_r($QRYNAME,true));
 
    $data = array();
    $rs = 0;
    $strSQL = ' SELECT A.*';
    $strSQL.= ' FROM FDB2CSV1 AS A';
    $strSQL.= ' WHERE A.D1NAME = ? ';
    $params = array($QRYNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
        error_log('Error Fdb2csv1*');
        $rs = 1;
    } else {
        $r = db2_execute($stmt, $params);
    
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
            error_log('Error Fdb2csv1*');
            $rs = 1;
        } else {
            $res = array();
            while ($row = db2_fetch_assoc($stmt)) {
                //e_log('cmChkQuerystmtRow***'.print_r($row,true));
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $res[] = $row;
            }
            //e_log('cmChkQueryDataError***'.print_r($res,true));
            if (isset($res[0]) === false) {
                $data = array('result' => 'NOTEXIST_GET');
                error_log('Error Fdb2csv1AAA*');
                $rs = 1;
              
            } else {
                $data = array('result' => true, 'FDB2CSV1' => cmHsc($res[0]));//MSM add cmHsc
            }
        }
    }
    if ($rs === 0) {
        if ($GPHNAME !== '') {
            $strSQL = ' SELECT C.* ';
            $strSQL.= ' FROM FDB2CSV1 AS B';
            $strSQL.= ' RIGHT JOIN DB2GPK AS C ';
            $strSQL.= ' ON D1NAME = GPKQRY AND GPKID = ? ';
            $strSQL.= ' WHERE D1NAME = ? ';
            $params = array($GPHNAME, $QRYNAME);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $data = array('result' => 'FAIL_SEL');
                $rs = 1;
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $data = array('result' => 'FAIL_SEL');
                    $rs = 1;
                } else {
                    $res = array();
                    while ($row = db2_fetch_assoc($stmt)) {
                        foreach ($row as $k => $v) {
                            $row[$k] = cmMer($v);
                        }
                        $res[] = $row;
                    }
                    if (isset($res[0]) === false) {
                        $data = array('result' => 'NOTEXIST_GPH');
                        $rs = 1;
                    } else {
                        $data['DB2GPK'] = $res[0];
                    }
                }
            }
        } else if ($PIVOTNAME !== '') {
            $strSQL = ' SELECT C.* ';
            $strSQL.= ' FROM FDB2CSV1 AS B';
            $strSQL.= ' RIGHT JOIN DB2PMST AS C ';
            $strSQL.= ' ON D1NAME = PMNAME AND PMPKEY = ? ';
            $strSQL.= ' WHERE D1NAME = ? ';
            $params = array($PIVOTNAME, $QRYNAME);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $data = array('result' => 'FAIL_SEL');
                $rs = 1;
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $data = array('result' => 'FAIL_SEL');
                    $rs = 1;
                } else {
                    $res = array();
                    while ($row = db2_fetch_assoc($stmt)) {
                        foreach ($row as $k => $v) {
                            $row[$k] = cmMer($v);
                        }
                        $res[] = $row;
                    }
                    if (isset($res[0]) === false) {
                        $data = array('result' => 'NOTEXIST_PIV');
                        $rs = 1;
                    } else {
                        $data['DB2PMST'] = $res[0];
                    }
                }
            }
        } //PIVOT END
        
    }
    if ($USRNAME !== '') {
        if ($rs === 0) {
            /*$strSQL = ' SELECT COUNT(*) AS COUNT ';
            $strSQL .= ' FROM DB2WGDF AS A ';
            $strSQL .= ' JOIN DB2WUGR AS B ON  ';
            $strSQL .= ' A.WGGID = B.WUGID ';
            $strSQL .= ' WHERE A.WGNAME = ?';
            $strSQL .= ' AND B.WUUID = ? ';*/
            $strSQL = ' SELECT COUNT(*) AS COUNT FROM( ';
            $strSQL.= '     SELECT';
            $strSQL.= '         A.WGNAME';
            $strSQL.= '     FROM';
            $strSQL.= '         DB2WGDF AS A   JOIN DB2WUGR AS B ON    A.WGGID = B.WUGID';
            $strSQL.= '     WHERE';
            $strSQL.= '         A.WGNAME = ?  AND';
            $strSQL.= '         B.WUUID = ? ';
            $strSQL.= '     UNION ALL ';
            $strSQL.= '     SELECT ';
            $strSQL.= '         DQNAME AS WGNAME ';
            $strSQL.= '     FROM ';
            $strSQL.= '         DB2WUSR ';
            $strSQL.= '     LEFT JOIN ';
            $strSQL.= '        DB2QHIS';
            $strSQL.= '    ON WUUID = DQCUSR ';
            $strSQL.= '    AND WUUQFG = \'1\'';
            $strSQL.= '    WHERE ';
            $strSQL.= '         DQNAME = ? AND ';
            $strSQL.= '         DQCUSR = ? ';
            $strSQL.= ' ) AS C ';
            $params = array($QRYNAME, $USRNAME, $QRYNAME, $USRNAME);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $data = array('result' => 'FAIL_SEL');
                $rs = 1;
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $data = array('result' => 'FAIL_SEL');
                    $rs = 1;
                } else {
                    $result = 0;
                    while ($row = db2_fetch_assoc($stmt)) {
                        $result = $row['COUNT'];
                    }
                    $data['COUNT'] = $result;
                    /*
                    if ($result === 0) {
                        $data = array(
                            'result' => 'FAIL_USR'
                        );
                        $rs   = 1;
                    }
                    */
                }
            }
        }
        if ($rs === 0) {
            $strSQL = ' SELECT *';
            $strSQL.= ' FROM DB2WDEF AS A';
            $strSQL.= ' WHERE A.WDUID = ?';
            $strSQL.= ' AND A.WDNAME = ?';
            $params = array($USRNAME, $QRYNAME);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $data = array('result' => 'FAIL_SEL');
                $rs = 1;
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $data = array('result' => 'FAIL_SEL');
                    $rs = 1;
                } else {
                    $res = array();
                    while ($row = db2_fetch_assoc($stmt)) {
                        foreach ($row as $k => $v) {
                            $row[$k] = $v;
                        }
                        $res[] = $row;
                    }
                    if (isset($res[0]) === false) {
                        $res[] = array('WDUID' => $USRNAME, 'WDNAME' => $QRYNAME, 'WDDWNL' => '0', 'WDDWN2' => '0');
                    }
                    $data['DB2WDEF'] = $res[0];
                }
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * クエリーグループチェック
 *@USRNAMEロックインのユーザ
 *@QRYGIDクエリーグループID
 *@D1NAMEクエリーID
 *@LASTFLGクエリーグループの最後のクエリー
 *@PIVOTNAMEピボットキー
 *@GPHNAMEグラフキー
 *-------------------------------------------------------*
*/
function cmChkQueryGroup($db2con, $USRNAME, $QRYGID, $D1NAME, $LASTFLG, $PIVOTNAME = '', $GPHNAME = '') {
    $data = array();
    $params = array();
    $FDB2CSV1 = array();
    $DB2PMST = array();
    $DB2GPK = array();
    $DB2WDEF = array();
    $userData = array();
    $LASTDB2QRYG = array();
    $rs = true;
    $WUAUTH = "";
    $qryGpTot = 0;
    if ($USRNAME === "") {
        $strSQL = ' SELECT COUNT(*) AS COUNT';
        $strSQL.= ' FROM DB2QRYG AS A ';
        $strSQL.= ' JOIN DB2QRYGS AS B ';
        $strSQL.= ' ON A.QRYGID=B.QRYGSID ';
        $strSQL.= ' WHERE A.QRYGID = ? ';
        $params = array($QRYGID);
    } else {
        $r = cmGetWUAUTH($db2con, $USRNAME);
        if ($r['result'] === 'FAIL_SEL' || $r['result'] === 'NOTEXIST_GET') {
            $rs = $r['result'];
        } else {
            $userData = umEx($r['data']);
            $WUAUTH = $userData[0]['WUAUTH'];
        }
        $strSQL = ' SELECT count(A.QRYGID) AS COUNT ';
        $strSQL.= ' FROM ';
        if ($WUAUTH === "3") {
            $strSQL.= ' ( ';
            $strSQL.= '    SELECT DISTINCT QRYGID FROM ';
            $strSQL.= '    ( ';
            $strSQL.= '    SELECT  QRYGID ';
            $strSQL.= '    FROM ';
            $strSQL.= '        DB2QRYG ';
            $strSQL.= '        WHERE QRYGWUUID= ? ';
            $strSQL.= '    UNION ALL ';
            $strSQL.= '    SELECT QRYGID ';
            $strSQL.= '        FROM DB2QRYG ';
            $strSQL.= '        WHERE QRYGID IN ( ';
            $strSQL.= '            SELECT DISTINCT ';
            $strSQL.= '                WGNAME ';
            $strSQL.= '            FROM ';
            $strSQL.= '            DB2WGDF ';
            $strSQL.= '            WHERE ';
            $strSQL.= '                WGGID IN (SELECT ';
            $strSQL.= '                            WUGID ';
            $strSQL.= '                        FROM ';
            $strSQL.= '                            DB2WUGR ';
            $strSQL.= '                        WHERE ';
            $strSQL.= '                            WUUID = ?  ';
            $strSQL.= '                        ) ';
            $strSQL.= '            AND WGQGFLG=\'1\' ';
            $strSQL.= '        )';
            $strSQL.= '    )  AA ';
            $strSQL.= ' ) A ';
            $params = array($USRNAME, $USRNAME);
        } else if ($WUAUTH === "4") {
            $strSQL.= " (SELECT QRYGID FROM DB2QRYG";
            $strSQL.= " WHERE QRYGWUUID=? ) A";
            $params = array($USRNAME);
        } else {
            $strSQL.= ' (SELECT QRYGID FROM  DB2QRYG ) A ';
        }
        $strSQL.= ' WHERE QRYGID <> \'\' ';
        $strSQL.= ' AND QRYGID=? ';
        array_push($params, $QRYGID);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $qryGpTot = $row['COUNT'];
            }
            if ($qryGpTot === 0) {
                $rs = 'NOTEXIST_GET';
                e_log('groupcheck1');
            }
        }
    }
    if ($rs === true) {
        if ($D1NAME !== "") {
            $strSQL = ' SELECT A.*';
            $strSQL.= ' FROM FDB2CSV1 AS A';
            $strSQL.= ' WHERE A.D1NAME = ? ';
            $params = array($D1NAME);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rs = 'FAIL_SEL';
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $rs = 'FAIL_SEL';
                } else {
                    $res = array();
                    while ($row = db2_fetch_assoc($stmt)) {
                        foreach ($row as $k => $v) {
                            $row[$k] = cmHsc(cmMer($v));//MSM add cmHsc
                        }
                        $res[] = $row;
                    }
                    if (isset($res[0]) === false) {
                        $rs = 'NOTEXIST_GET';
                        e_log('groupcheck2');
                    } else {
						$FDB2CSV1 = $res[0];
                    }
                }
            }
        }
    }
    //チェックピボットとグラフ
    if ($rs === true) {
        if ($PIVOTNAME !== '') {
            $strSQL = ' SELECT C.* ';
            $strSQL.= ' FROM FDB2CSV1 AS B';
            $strSQL.= ' RIGHT JOIN DB2PMST AS C ';
            $strSQL.= ' ON D1NAME = PMNAME AND PMPKEY = ? ';
            $strSQL.= ' WHERE D1NAME = ? ';
            $params = array($PIVOTNAME, $D1NAME);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rs = 'FAIL_SEL';
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $rs = 'FAIL_SEL';
                } else {
                    $res = array();
                    while ($row = db2_fetch_assoc($stmt)) {
                        foreach ($row as $k => $v) {
                            $row[$k] = cmMer($v);
                        }
                        $res[] = $row;
                    }
                    if (isset($res[0]) === false) {
                        $rs = 'NOTEXIST_PIV';
                        e_log('groupcheck3');
                    } else {
                        $DB2PMST = $res[0];
                    }
                }
            }
        } else if ($GPHNAME !== '') {
            $strSQL = ' SELECT C.* ';
            $strSQL.= ' FROM FDB2CSV1 AS B';
            $strSQL.= ' RIGHT JOIN DB2GPK AS C ';
            $strSQL.= ' ON D1NAME = GPKQRY AND GPKID = ? ';
            $strSQL.= ' WHERE D1NAME = ? ';
            $params = array($GPHNAME, $D1NAME);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rs = 'FAIL_SEL';
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $rs = 'FAIL_SEL';
                } else {
                    $res = array();
                    while ($row = db2_fetch_assoc($stmt)) {
                        foreach ($row as $k => $v) {
                            $row[$k] = cmMer($v);
                        }
                        $res[] = $row;
                    }
                    if (isset($res[0]) === false) {
                        $rs = 'NOTEXIST_GPH';
                        e_log('groupcheck4');
                    } else {
                        $DB2GPK = $res[0];
                    }
                }
            }
        }
    }
    /**実行するクエリーの権限チェック*/
    if ($USRNAME !== '' && $D1NAME != "") {
        if ($rs === true) {
            $strSQL = ' SELECT COUNT(*) AS COUNT FROM( ';
            $strSQL.= '    SELECT D.QRYGSQRY FROM ( ';
            $strSQL.= '        SELECT A.WGNAME ';
            $strSQL.= '        FROM';
            $strSQL.= '            DB2WGDF AS A JOIN ';
            $strSQL.= '            DB2WUGR AS B ON A.WGGID=B.WUGID';
            $strSQL.= '        WHERE A.WGNAME=? AND B.WUUID=? ';
            $strSQL.= '    ) C ';
            $strSQL.= '    JOIN DB2QRYGS AS D ';
            $strSQL.= '    ON C.WGNAME=D.QRYGSID';
            $strSQL.= '    WHERE D.QRYGSQRY=? ';
            $strSQL.= ' ) E ';
            $params = array($QRYGID, $USRNAME, $D1NAME);
            e_log("実行するクエリーの権限チェック" . $strSQL . print_r($params, true));
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rs = 'FAIL_SEL';
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $rs = 'FAIL_SEL';
                } else {
                    $result = 0;
                    while ($row = db2_fetch_assoc($stmt)) {
                        $result = $row['COUNT'];
                    }
                    if ($result === 0) {
                        $rs = 'FAIL_USR';
                        e_log('groupcheck5');
                    }
                }
            }
        }
    }
    if ($rs === true) {
        if ($LASTFLG === '1') {
            $strSQL = ' SELECT *';
            $strSQL.= ' FROM DB2WDEF AS A';
            $strSQL.= ' WHERE A.WDUID = ?';
            $strSQL.= ' AND A.WDNAME = ?';
            $params = array($USRNAME, $D1NAME);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rs = 'FAIL_SEL';
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $rs = 'FAIL_SEL';
                } else {
                    $res = array();
                    while ($row = db2_fetch_assoc($stmt)) {
                        foreach ($row as $k => $v) {
                            $row[$k] = $v;
                        }
                        $res[] = $row;
                    }
                    if (isset($res[0]) === false) {
                        $res[] = array('WDUID' => $USRNAME, 'WDNAME' => $D1NAME, 'WDDWNL' => '0', 'WDDWN2' => '0');
                    }
                    $DB2WDEF = $res[0];
                }
            }
        }
    }
    if ($rs === true) {
        if ($D1NAME !== "") {
            $strSQL = ' SELECT QRYGID,QRYGNAME,EMP.QRYGSQRY AS D1NAME ';
            $strSQL.= '     FROM DB2QRYG ';
            $strSQL.= '     JOIN (SELECT EMP1.QRYGSQRY,EMP1.QRYGSID ';
            $strSQL.= '                     FROM DB2QRYGS AS EMP1 ';
            $strSQL.= '                     JOIN ';
            $strSQL.= '                     (SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
            $strSQL.= '                           FROM DB2QRYGS GROUP BY QRYGSID ';
            $strSQL.= '                     ) EMP2 ';
            $strSQL.= '                     ON EMP1.QRYGSID=EMP2.QRYGSID AND EMP1.QRYGSSEQ=EMP2.QRYGSSEQ  ';
            $strSQL.= '    ) EMP ON QRYGID=EMP.QRYGSID  ';
            $strSQL.= ' WHERE QRYGID=?';
            $params = array($QRYGID);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rs = 'FAIL_SEL';
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $rs = 'FAIL_SEL';
                } else {
                    $res = array();
                    while ($row = db2_fetch_assoc($stmt)) {
                        $res[] = $row;
                    }
                    if (isset($res[0]) === false) {
                        $rs = 'NOTEXIST_GET';
                        e_log('groupcheck6');
                    } else {
                        $LASTDB2QRYG = $res[0];
                    }
                }
            }
        }
    }
    $data = array('result' => $rs, 'FDB2CSV1' => $FDB2CSV1, 'DB2PMST' => $DB2PMST, 'DB2GPK' => $DB2GPK, 'DB2WDEF' => $DB2WDEF, 'LASTDB2QRYG' => $LASTDB2QRYG);
    return $data;
}
/*
 *-------------------------------------------------------*
 *クエリーグループのデータ
 *＠QRYGIDクエリーグループＩＤ
 *-------------------------------------------------------*
*/
function cmGetQueryGroup($db2con, $QRYGID, $PMPKEY = '', $GPHKEY = '') {
    $data = array();
    $params = array();
    $rs = true;
    $rtn = 0;
    $strSQL = ' SELECT GP.*,F.D1CFLG FROM ( ';
    $strSQL.= '  SELECT A.QRYGID,B.QRYGSSEQ,B.QRYGSQRY,\'0\' AS LASTFLG, ';
    $strSQL.= '  \'\' AS PMPKEY,\'\' AS GPHKEY  ';
    $strSQL.= '  FROM DB2QRYG AS A ';
    $strSQL.= '  JOIN ';
    $strSQL.= '    ( ';
    $strSQL.= '        SELECT * FROM DB2QRYGS ';
    $strSQL.= '        WHERE QRYGSSEQ NOT IN ';
    $strSQL.= '        ( ';
    $strSQL.= '            SELECT MAX(QRYGSSEQ) AS QRYGSSEQ FROM DB2QRYGS WHERE QRYGSID=? ';
    $strSQL.= '        ) ';
    $strSQL.= '    ) AS B ';
    $strSQL.= '  ON A.QRYGID=B.QRYGSID ';
    $strSQL.= '  WHERE QRYGID=?  ';
    $strSQL.= '  UNION ALL ';
    $strSQL.= '  SELECT C.QRYGID,QRYGSSEQ,QRYGSQRY,\'1\' AS LASTFLG, ';
    $strSQL.= '  \'' . $PMPKEY . '\' AS PMPKEY,\'' . $GPHKEY . '\' AS GPHKEY ';
    $strSQL.= '  FROM DB2QRYG AS C ';
    $strSQL.= '  JOIN  ';
    $strSQL.= '  ( ';
    $strSQL.= '        SELECT EMP2.* FROM DB2QRYGS AS EMP2  ';
    $strSQL.= '        JOIN ';
    $strSQL.= '        ( ';
    $strSQL.= '            SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
    $strSQL.= '            FROM DB2QRYGS AS D ';
    $strSQL.= '            GROUP BY QRYGSID ';
    $strSQL.= '        ) EMP1 ';
    $strSQL.= '        ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ';
    $strSQL.= '  ) EMP  ';
    $strSQL.= '  ON C.QRYGID=EMP.QRYGSID';
    $strSQL.= '  WHERE QRYGID=? ';
    $strSQL.= '  ORDER BY QRYGSSEQ ';
    $strSQL.= '     ) AS GP ';
    $strSQL.= 'LEFT JOIN FDB2CSV1 AS F ON GP.QRYGSQRY = F.D1NAME  ';
    $params[] = $QRYGID;
    $params[] = $QRYGID;
    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con, $strSQL);
          //  error_log('sql group = '.$strSQL.print_r($params,true));
    if ($stmt === false) {
        error_log('stmt error = '.db2_stmt_errormsg());
        $rs = 'FAIL_SEL';
        $rtn = 1;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
            $rtn = 1;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            //クエリーグループの中でクエリーがない場合チェック
            if (count($data) === 0) {
                $rs = 'NOTEXIST';
                $rtn = 2;
            }
        }
    }
    return array('result' => $rs, 'rtn' => $rtn, 'data' => umEx($data));
}
/*
 *-------------------------------------------------------*
 *一番最後のクエリーグループのデータ
 *＠QRYGIDクエリーグループＩＤ
 *-------------------------------------------------------*
*/
function cmGetLastQueryGroup($db2con, $QRYGID) {
    $data = array();
    $params = array();
    $rs = true;
    $rtn = 0;
    $strSQL = ' SELECT QRYGID,QRYGNAME,EMP.QRYGSQRY AS D1NAME ';
    $strSQL.= '     FROM DB2QRYG ';
    $strSQL.= '     LEFT JOIN (SELECT EMP1.QRYGSQRY,EMP1.QRYGSID ';
    $strSQL.= '                     FROM DB2QRYGS AS EMP1 ';
    $strSQL.= '                     JOIN ';
    $strSQL.= '                     (SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
    $strSQL.= '                           FROM DB2QRYGS GROUP BY QRYGSID ';
    $strSQL.= '                     ) EMP2 ';
    $strSQL.= '                     ON EMP1.QRYGSID=EMP2.QRYGSID AND EMP1.QRYGSSEQ=EMP2.QRYGSSEQ  ';
    $strSQL.= '    ) EMP ON QRYGID=EMP.QRYGSID  ';
    $strSQL.= ' WHERE QRYGID=? ';
    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con, $strSQL);
    error_log("cmGetLastQueryGroup 20180919 => ".$strSQL.print_r($params,true));
    if ($stmt === false){
        $rs = 'FAIL_SEL';
        $rtn = 1;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
            $rtn = 1;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return array('result' => $rs, 'rtn' => $rtn, 'data' => umEx($data));
}
/*
 *-------------------------------------------------------*
 * ログインユーザが削除されるかどうかチェック
 *
 * RESULT
 *    01：データ件数
 *    02：false
 *
 * @param Object  $db2con DBコネクション
 * @param String  $id     ユーザーID
 *
 *-------------------------------------------------------*
*/
function cmCheckUser($db2con, $id) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT A.* ';
    $strSQL.= ' FROM DB2WUSR as A ';
    $strSQL.= ' WHERE WUUID <> \'\' ';
    if ($id != '') {
        $strSQL.= ' AND WUUID = ? ';
        array_push($params, $id);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $data = array('result' => 'NOTEXIST_GET');
            } else {
                $data = array('result' => true, 'data' => $data);
            }
        }
    }
    return $data;
}
/**
 * 集計メッソドデータ取得
 *
 * RESULT
 *    01：データ件数
 *    02：false
 *
 * @param Object  $db2con DBコネクション
 * @param String  $DTNAME クエリ名
 *
 */
function cmGetDB2COLT($db2con, $DTNAME) {
    $data = array();
    $strSQL = ' SELECT A.* ';
    $strSQL.= ' FROM DB2COLT AS A ';
    $strSQL.= ' WHERE A.DTNAME = ? AND A.DTFLD <> \'\' ';
    $params = array($DTNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/**
 * 制御レベルデータ取得
 *
 * RESULT
 *    01：データ件数
 *    02：false
 *
 * @param Object  $db2con DBコネクション
 * @param String  $DCNAME クエリ名
 *
 */
function cmGetDB2COLM($db2con, $DCNAME) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT C.* ';
    $strSQL.= ' FROM DB2COLM AS C ';
    $strSQL.= ' WHERE C.DCNAME = ? ';
    $params = array($DCNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
        e_log('実行エラー：' . db2_stmt_errormsg() . $strSQL . print_r($params, true));
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/**
 * 制御レベルデータ取得
 *
 * DCSUMF
 *    ''：合計がない
 *    1：合計がある
 *
 * @param Object  $db2con DBコネクション
 * @param String  $DCNAME クエリ名
 *
 */
function cmUpdDB2COLM($db2con, $DCNAME, $DCSUMF) {
    $rs = true;
    $data = array();
    $params = array();
    $strSQL = ' UPDATE DB2COLM SET DCSUMF=?  ';
    $strSQL.= ' WHERE DCNAME = ? ';
    $params = array($DCSUMF, $DCNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_UPD';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_UPD';
        }
    }
    $data = array('result' => $rs);
    return $data;
}
//checking html template exist or not
function fnGetDB2HTMLT($db2con, $HTMLTD) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT * FROM DB2HTMLT';
    $strSQL.= ' WHERE HTMLTD = ? ';
    $params = array($HTMLTD);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
//checking html template DATA
function fnGetDB2HTMLK_INSHI($db2con) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT * FROM DB2HTMLK';
    $params = array($HTMLTD);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
function fnGetFDB2CSV1PM($db2con, $d1name, $d1event) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT *';
    $strSQL.= ' FROM FDB2CSV1PM AS A ';
    $strSQL.= ' WHERE A.DMNAME <> \'\' ';
    $strSQL.= ' AND DMNAME = ? AND DMEVENT = ? ';
    $strSQL.= ' ORDER BY A.DMSEQ ASC ';
    $stmt = db2_prepare($db2con, $strSQL);
    $params = array($d1name, $d1event);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/**
 * DB2WDTLテーブルデータ取得
 *
 * RESULT
 *    01：データ件数
 *    02：false
 *
 * @param Object  $db2con  DBコネクション
 * @param String  $QYNAME  クエリ名
 * @param String  $FILID   DTFILID
 * @param String  $FLD     DTFLD
 *
 */
function cmGetDB2WDTL($db2con, $QYNAME, $FILID, $FLD) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT * ';
    $strSQL.= ' FROM DB2WDTL';
    $strSQL.= ' WHERE DTNAME = ? ';
    $params = array($QYNAME);
    if ($FILID !== '') {
        $strSQL.= ' AND DTFILID= ? ';
        array_push($params, $FILID);
    }
    if ($FLD !== '') {
        $strSQL.= ' AND DTFLD= ? ';
        array_push($params, $FLD);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $temp = array();
                foreach ($row as $key => $value) {
                    $temp[$key] = cmMer($value);
                }
                $data[] = $temp;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/****
 *
 *取得DB2DRGS(20161124)
 **/
function cmGetDB2DRGS($db2con, $DRKMTN) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT * ';
    $strSQL.= ' FROM DB2DRGS';
    $strSQL.= ' WHERE DRKMTN = ? ';
    $params = array($DRKMTN);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/**
 * 日付スラッシュ
 *
 * RESULT
 *    変換されたフォーマット
 *
 * @param String  $value  変換する値
 *
 */
/*function cmFormat1($value)
{
$val = '';

if ($value !== '') {
$newformat = strtotime($value);
if (strlen($value) === 8) {
$val = date('Y/m/d', $newformat);
} else if (strlen($value) === 6) {
$val = date('Y/m', $newformat);
}
}
return $val;
}*/
/**
 * 日付スラッシュ(ゼロの場合はブランク)
 *
 * RESULT
 *    変換されたフォーマット
 *
 * @param String  $value  変換する値
 *
 */
function cmFormat2($value) {
    $val = '';
    if ($value !== '') {
        if ($value === 0) {
            $val = '';
        }
        $newformat = strtotime($value);
        if (strlen($value) === 8) {
            $val = date('Y/m/d', $newformat);
        } else if (strlen($value) === 6) {
            $val = date('Y/m', $newformat);
        }
    }
    return $val;
}
/**
 * カンマ編集
 *
 * RESULT
 *    変換されたフォーマット
 *
 * @param String  $value  変換する値
 *
 */
function cmFormat3($value) {
    $val = '';
    if ($value !== '') {
        $val = changeNumberFormat($value);
    }
    return $val;
}
/**
 * カンマ編集(ゼロの場合はブランク)
 *
 * RESULT
 *    変換されたフォーマット
 *
 * @param String  $value  変換する値
 *
 */
function cmFormat4($value) {
    $val = '';
    if ($value !== '') {
        if ($value === 0) {
            return '';
        } else {
            $val = changeNumberFormat($value);
        }
    }
    return $val;
}
/**
 * ゼロの場合はブランク
 *
 * RESULT
 *    変換されたフォーマット
 *
 * @param String  $value  変換する値
 *
 */
function cmFormat5($value) {
    $val = '';
    if ($value !== '') {
        if ($value === 0) {
            return '';
        } else {
            $val = changeNumberFormat($value);
        }
    }
    return $val;
}
/**
 * 文字列カンマつける
 *
 * RESULT
 *    変換されたフォーマット
 *
 * @param String  $value  変換する値
 *
 */
function changeNumberFormat($numeric) {
    $numeric = strval($numeric);
    //うっかり入っていたカンマを消す
    $numeric = str_replace(',', '', $numeric);
    //小数点を探し、小数点以下と整数部を分割して保持する
    $decimalPoint = strpos($numeric, '.');
    if ($decimalPoint === false) {
        $decimals = '';
        $integers = $numeric;
    } else {
        $decimals = substr($numeric, $decimalPoint, (strlen($numeric) - $decimalPoint)) . '';
        $integers = substr($numeric, 0, $decimalPoint) . '';
    }
    //整数部の文字列長を3の倍数にする。足りない分は手前に' 'を埋め込む
    $Blanks = strlen($integers) % 3;
    if ($Blanks !== 0) {
        for ($i = 0;$i < 3 - $Blanks;$i++) {
            $integers = ' ' . $integers;
        }
    }
    //整数文字列先頭から3文字おきにカンマを挿入する
    //先頭がマイナス符号の時は負数として処理する
    $figureInteger = substr($integers, 0, 3);
    $j = 2;
    if ($integers{2} === '-') {
        $figureInteger = $figureInteger . substr($integers, 3, 6);
        $j = 4;
    }
    for ($i = $j;$i < strlen($integers);$i++) {
        if ($i % 3 === 0) {;
            $figureInteger = $figureInteger . ',' . substr($integers, $i, 3);
        }
    }
    //臨時に入れておいた' 'を削除する
    while ($figureInteger{0} == ' ') {
        $figureInteger = substr($figureInteger, 1, strlen($figureInteger));
    }
    //整形済みの整数部と、待避してあった小数部を連結。連結した文字列を返して終了！
    $commaNumber = $figureInteger . $decimals;
    return $commaNumber;
}
/**
 * check Numeric
 * （0～9）で構成されているか？
 *
 * 引数   :1.チェックしたい値
 * 戻り値 :1.true :チェックOK
 *           false:チェックNG
 *
 */
function checkNuturalNum($data) {
    if (preg_match('/^[0-9]+$/', $data)) {
        return true;
    }
    return false;
}
/*
 *-------------------------------------------------------*
 * 制御取得のため作成されたDBを読み込み
 *-------------------------------------------------------*
*/
function cmGetDbFile_1($db2con, $fdb2csv2, $filename, $start, $length, $sortcol, $sortdir, $sortingcols = 0, $search, $db2wcol, $rsMasterArr, $dnlFlg = false, $burstItmLst, $webf = '', $d1name = '', $CNDSDATA = array(), $EXCELVER = '', $filtersearchData = array()) {
    $data = array();
    $params = array();
    $selecttype = array();
    $cntfmt = array();//サマリーカウントのアライメント
    // データ取得SQL作成
    $strSQL = '';
    $strSQL.= ' SELECT ';
    //ダウンロードの場合
    if ($dnlFlg === true) {
       
        foreach ($fdb2csv2 as $key => $value) {
            //ダウンロード制限があるカラムだけ出す
            if ($value['D2DNLF'] !== '1') {
                $strSQL.= ' A.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                $strSQL.= ' , ';
                $selecttype[] = $value['D2TYPE'];
                $cntfmt[] = $value['SFGMES'];
            }
        }
        if ($rsMasterArr[0]['DCFLD1'] !== '' || $rsMasterArr[0]['DCFLD2'] !== '' || $rsMasterArr[0]['DCFLD3'] !== '' || $rsMasterArr[0]['DCFLD4'] !== '' || $rsMasterArr[0]['DCFLD5'] !== '' || $rsMasterArr[0]['DCFLD6'] !== '') {
              $strSQL.= ' A.ROWNO ';
        }else{
               $strSQL.= 1;
        } 
    } else {
       
        //ウェブのデータ表示
        foreach ($fdb2csv2 as $key => $value) {
            $strSQL.= ' A.' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']);
            if (($key + 1) < count($fdb2csv2)) {
                $strSQL.= ' , ';
            }
            $selecttype[] = $value['D2TYPE'];
            $cntfmt[] = $value['SFGMES'];
        }
      if ($rsMasterArr[0]['DCFLD1'] !== '' || $rsMasterArr[0]['DCFLD2'] !== '' || $rsMasterArr[0]['DCFLD3'] !== '' || $rsMasterArr[0]['DCFLD4'] !== '' || $rsMasterArr[0]['DCFLD5'] !== '' || $rsMasterArr[0]['DCFLD6'] !== '') {
        $strSQL.= ' , A.ROWNO ';
      }else{
        $strSQL.= ' ,1 ';
      }
    }
    $selecttype[] = "fdb2csv";
    $strSQL.= ' FROM (';
    $strSQL.= ' SELECT B.* , ';
    $strSQL.= ' ROWNUMBER() OVER ( ';
    $strSQL.= ') ';
    $strSQL.= ' AS ROWNO ';
    $strSQL.= ' FROM ';
    $strSQL.= SAVE_DB . '/' . $filename . ' AS B';
    if (count($filtersearchData) > 0) {
        $paramDSQL = array();
        $strSQL.= ' WHERE ';
        foreach ($filtersearchData as $key => $value) {
            list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
            switch ($value['dtype']) {
                case 'L':
                    $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . '_0' . $value['D1OPERATOR'] . ' ? ';
                break;
                case 'T':
                    $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . '_0' . $value['D1OPERATOR'] . ' ? ';
                    $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . '_0' . $value['D1OPERATOR'] . ' ? ';
                break;
                case 'Z':
                    $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . '_0' . $value['D1OPERATOR'] . ' ? ';
                break;
                default:
                    $paramDSQL[] = $D1COL2 . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                break;
            }
            $params[] = cmMer($value['D1VAL']);
        }
        $strSQL.= join(' AND ', $paramDSQL);
    }
    $strSQL.= ' ) AS A ';
    //抽出範囲指定
    if (($start !== '') && ($length !== '')) {
        if (count($filtersearchData) > 0) {
            
            $strSQL.= ' WHERE A.ROWNO BETWEEN ? AND ? ';
            $params[] = $start + 1;
            $params[] = $start + $length;
            $strSQL.= ' ORDER BY A.ROWNO ';
        } else {
           
         if ($rsMasterArr[0]['DCFLD1'] !== '' || $rsMasterArr[0]['DCFLD2'] !== '' || $rsMasterArr[0]['DCFLD3'] !== '' || $rsMasterArr[0]['DCFLD4'] !== '' || $rsMasterArr[0]['DCFLD5'] !== '' || $rsMasterArr[0]['DCFLD6'] !== '') {
            $strSQL.= ' WHERE A.ROWNO BETWEEN ? AND ? ';
         }
            $params[] = $start + 1;
            $params[] = $start + $length;
            $strSQL.= ' ORDER BY A.ROWNO ';
        }
    }
    $systables = cmGetSystables($db2con, SAVE_DB, $filename);
    error_log('cmGetDbFile_1の制御データ取得クエリー:' . $strSQL . print_r($params, true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL'
        // after recreate table prepare statement error
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL'
            // after recreate table execute statement error
            );
        } else {
            while ($row = db2_fetch_array($stmt)) {
                if ($dnlFlg === true && $EXCELVER === 'XLSX') {
                    foreach ($row as $key => $val) {
                        if ($selecttype[$key] === 'S' || $selecttype[$key] === 'P' || $selecttype[$key] === 'B' || $cntfmt[$key] === 'COUNT') {
                            $row[$key] = (float)$val;
                        } else {
                            $row[$key] = $val;
                        }
                    }
                }
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 *　制御データ取得CLで作成された集計するデータを読み込み
 *-------------------------------------------------------*
*/
//($db2con, $FDB2CSV2['data'], $d1name, $filename, $iDisplayStart, $iDisplayLength, $sSearch, '', $rsMasterArr, $rsshukeiArr, false, '',$filtersearchData)
function cmGetDbFile_2($db2con, $fdb2csv2, $d1name, $filename, $start, $length, $search, $db2wcol, $rsMasterArr, $rsshukeiArr, $dnlFlg = false, $burstItmLst, $webf = '', $d1name2 = '', $CNDSDATA = array(), $filtersearchData = array()) { 
    $rtn = 0;
    $data = array();
    $params = array();
    $levelFlg = '';
    $gpFlg = '';
    $shukeiFlg = '';
    $rsMasterArr = umEx($rsMasterArr);
    $rsshukeiArr = umEx($rsshukeiArr);
    //$fdb2csv2 = umEx($fdb2csv2);
	$testFdb2csv2=cmGetFDB2CSV2($db2con, $d1name,false,false,true);
	$fdb2csv2=umEx($testFdb2csv2["data"]);
    $colres = cmGetOrdColumnInfo($db2con, $d1name);
    $colsorting = $colres['data'];
    $colsortingArr = array();
    foreach ($colsorting as $ckey => $cval) {
        $cid = $cval['D2FLD'] . '_' . $cval['D2FILID'];
        $colsortingArr[$cid] = $cval['D2RSTP'];
    }
    $keyArr = array();
    $i = 1;
    while ($i <= 6) {
        if ($rsMasterArr[0]['DCFLD' . $i] !== '') {
			$splitResArr=array();
	        $LGroup = explode(",",cmMer($rsMasterArr[0]['DCFLD' . $i]));
	        if (count($LGroup) >= 2) { //グループのレベル
				for($innerL = 0;$innerL < count($LGroup);$innerL++){
					$splitRes=explode("-",$LGroup[$innerL]);
					$splitResArr[] = $splitRes[1] . '_' .$splitRes[0];
					$keyArr[] =$splitRes[1] . '_' .$splitRes[0];
				}
				/*$splitResJoin=implode(",",$splitResArr);
				$keyArr[] = $splitResJoin;*/
			}else{
	            $keyArr[] = $rsMasterArr[0]['DCFLD' . $i] . '_' . $rsMasterArr[0]['DCFILID' . $i];
                $keyArr[] = $rsMasterArr[0]['DCFNAME' . $i];
			}
            //$keyArr[] = $rsMasterArr[0]['DCFLD' . $i] . '_' . $rsMasterArr[0]['DCFILID' . $i];
        }
        ++$i;
    }
    $shukeiArr = array();
    $selFld = array();
    $shukeiFld = array();
    foreach ($rsshukeiArr as $key => $row) {
        if ($row['DTSUM'] === '1') {
            $shukeiFld[] = 'A.SUM_' . $row['DTFLD'] . '_' . $row['DTFILID'];
            $shukeiArr[] = 'SUM (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS SUM_' . $row['DTFLD'] . '_' . $row['DTFILID'];
        }
        if ($row['DTAVLG'] === '1') {
            $shukeiFld[] = 'A.AVG_' . $row['DTFLD'] . '_' . $row['DTFILID'];
            $shukeiArr[] = 'AVG (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS AVG_' . $row['DTFLD'] . '_' . $row['DTFILID'];
        }
        if ($row['DTMAXV'] === '1') {
            $shukeiFld[] = 'A.MAX_' . $row['DTFLD'] . '_' . $row['DTFILID'];
            $shukeiArr[] = 'MAX (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS MAX_' . $row['DTFLD'] . '_' . $row['DTFILID'];
        }
        if ($row['DTMINV'] === '1') {
            $shukeiFld[] = 'A.MIN_' . $row['DTFLD'] . '_' . $row['DTFILID'];
            $shukeiArr[] = 'MIN (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS MIN_' . $row['DTFLD'] . '_' . $row['DTFILID'];
        }
        if ($row['DTCONT'] === '1') {
            $shukeiFld[] = 'A.COUNT_' . $row['DTFLD'] . '_' . $row['DTFILID'];
            $shukeiArr[] = 'COUNT (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS COUNT_' . $row['DTFLD'] . '_' . $row['DTFILID'];
        }
    }
    $keyDataArr = $keyArr;
    $keyValArr = $keyArr;
    $keyData = array();
    $ordKeyArr = array();
    $selKeyArr = array();
    foreach ($keyArr as $k => $kVal) {
        if (empty($keyDataArr)) {
            $keyData[] = '';
        } else {
            $keyData[] = join(',', $keyDataArr);
        }
        foreach ($fdb2csv2 as $fdb2csv2data) {
            if ($fdb2csv2data['D2FLD'] . '_' . $fdb2csv2data['D2FILID'] === $kVal) {
                $datatype = $fdb2csv2data['D2TYPE'];
                $sSort = '';
                if (isset($colsortingArr[$kVal])) {
                    $sSort = $colsortingArr[$kVal];
                }
                switch ($datatype) {
                    case 'L':
                        $selFld[] = 'CHAR(A.' . $kVal . ') AS ' . $kVal;
                        $ordStr = str_pad('9', 10, "9");
                        if ($sSort === 'DESC') {
                            $ordStr = '-' . $ordStr;
                        }
                        $ordKeyArr[] = ' CASE  WHEN ' . $kVal . ' IS NULL THEN \'' . $ordStr . '\' ELSE ' . $kVal . ' END ' . $sSort . ', ' . $kVal . ' ' . $sSort;
                    break;
                    case 'T':
                        $selFld[] = 'CHAR(A.' . $kVal . ') AS ' . $kVal;
                        $ordStr = str_pad('9', 8, "9");
                        if ($sSort === 'DESC') {
                            $ordStr = '-' . $ordStr;
                        }
                        $ordKeyArr[] = ' CASE  WHEN ' . $kVal . ' IS NULL THEN \'' . $ordStr . '\' ELSE ' . $kVal . ' END ' . $sSort . ', ' . $kVal . ' ' . $sSort;
                    break;
                    case 'Z':
                        $selFld[] = 'VARCHAR_FORMAT(A.' . $kVal . ', \'YYYY-MM-DD HH24:MI:SS.nnnnnn\') AS ' . $kVal;
                        $ordStr = str_pad('9', 26, "9");
                        if ($sSort === 'DESC') {
                            $ordStr = '-' . $ordStr;
                        }
                        $ordKeyArr[] = ' CASE  WHEN ' . $kVal . ' IS NULL THEN \'' . $ordStr . '\' ELSE ' . $kVal . ' END ' . $sSort . ', ' . $kVal . ' ' . $sSort;
                    break;
                    case 'P':
                    case 'S':
                    case 'B':
                        $selFld[] = 'A.' . $kVal;
                        $ordStr = str_pad('9', $fdb2csv2data['D2LEN'] + 1, "9");
                        if ($sSort === 'DESC') {
                            $ordStr = '-' . $ordStr;
                        }
                        $ordKeyArr[] = ' CASE  WHEN ' . $kVal . ' IS NULL THEN ' . $ordStr . ' ELSE ' . $kVal . ' END ' . $sSort . ', ' . $kVal . ' ' . $sSort;
                    break;
                    case 'A':
                    case 'O':
                        $selFld[] = 'A.' . $kVal;
                        $ordStr = str_pad('9', $fdb2csv2data['D2LEN'] + 1, "9");
                        if ($sSort === 'DESC') {
                            $ordStr = '-' . $ordStr;
                        }
                        $ordKeyArr[] = ' CASE  WHEN ' . $kVal . ' IS NULL THEN \'' . $ordStr . '\' ELSE ' . $kVal . ' END ' . $sSort . ', ' . $kVal . ' ' . $sSort;
                    break;
                    default:
                    break;
                }
                break;
            }
        }
        if ($k === 0) {
            $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyDataArr[count($keyValArr) - 1] . '\'AS CHECK ';
        } else {
            $c = count($keyArr);
            $vc = count($keyValArr);
            foreach ($fdb2csv2 as $fdb2csv2OrdData) {
                if ($fdb2csv2OrdData['D2FLD'] . '_' . $fdb2csv2OrdData['D2FILID'] === $keyArr[$c - $k]) {
                    $ordDatatype = $fdb2csv2OrdData['D2TYPE'];
                    switch ($ordDatatype) {
                        case 'L':
                            $keyArr[$c - $k] = ' CAST(NULL AS DATE) AS ' . $keyArr[$c - $k];
                            $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1) ] . '\'AS CHECK ';
                        break;
                        case 'T':
                            $keyArr[$c - $k] = ' CAST(NULL AS TIME) AS ' . $keyArr[$c - $k];
                            $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1) ] . '\'AS CHECK ';
                        break;
                        case 'Z':
                            $keyArr[$c - $k] = ' CAST(NULL AS TIMESTAMP) AS ' . $keyArr[$c - $k];
                            $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1) ] . '\'AS CHECK ';
                        break;
                        case 'P':
                        case 'S':
                        case 'B':
                            $keyArr[$c - $k] = ' CAST(NULL AS INT) AS ' . $keyArr[$c - $k];
                            $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1) ] . '\'AS CHECK ';
                        break;
                        case 'A':
                        case 'O':
                            $keyArr[$c - $k] = ' CAST(NULL AS CHAR) AS ' . $keyArr[$c - $k];
                            $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1) ] . '\'AS CHECK ';
                        break;
                        default:
                            $keyArr[$c - $k] = ' CAST(NULL AS CHAR) AS ' . $keyArr[$c - $k];
                            $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1) ] . '\'AS CHECK ';
                        break;
                    }
                    break;
                }
            }
        }
        $rmData = array_pop($keyDataArr);
    }
    /**20180213**/
    //データを集計するクエリの作成
    if (CRTQTEMPTBL_FLG === 1) {
        /***20180213の文を消した***/
        foreach ($fdb2csv2 as $fdata) {
            if ($fdata['D2FLD'] . '_' . $fdata['D2FILID'] === $keyArr[0]) {
                $datatype = $fdata['D2TYPE'];
                break;
            }
        }
        switch ($datatype) {
            case 'L':
                $keyArr[0] = ' CAST(NULL AS DATE) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
            break;
            case 'T':
                $keyArr[0] = ' CAST(NULL AS TIME) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
            break;
            case 'Z':
                $keyArr[0] = ' CAST(NULL AS TIMESTAMP) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
            break;
            case 'P':
            case 'S':
            case 'B':
                $keyArr[0] = ' CAST(NULL AS INT) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
            break;
            case 'A':
            case 'O':
                $keyArr[0] = ' CAST(NULL AS CHAR) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
            break;
            default:
                $keyArr[0] = ' CAST(NULL AS CHAR) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
            break;
        }
        $shukeiData = join(',', $shukeiArr);
        $strSQLArr = array();
        $kd = count($keyData);
        $cnt = 0;
        while ($cnt <= $kd) {
            $strSQL = '';
            $strSQL.= ' SELECT ';
            $strSQL.= $selKeyArr[$cnt];
            $strSQL.= ' , ' . $shukeiData;
            $strSQL.= ' , MAX(ROWNUM ) as RN ';
            $strSQL.= ' FROM ';
            $strSQL.= SAVE_DB . '/' . $filename;
            if ($cnt < $kd) {
                $strSQL.= ' GROUP BY ';
                $strSQL.= $keyData[$cnt];
            }
            $strSQLArr[] = $strSQL;
            ++$cnt;
        }
        $strSQL = ' SELECT ';
        $strSQL.= '    * ';
        $strSQL.= ' FROM (';
        $strSQL.= '    SELECT ';
        $strSQL.= join(',', $selFld);
        $strSQL.= ', A.CHECK , ';
        $strSQL.= join(',', $shukeiFld);
        $strSQL.= ', A.RN ';
        $strSQL.= ', CASE WHEN A.RN IS NOT NULL THEN  ROWNUMBER() OVER (   ) END AS ROWNUM ';
        $strSQL.= '    FROM ( ';
        $strSQL.= join(' UNION ALL ', $strSQLArr);
        $strSQL.= '    ) A ';
        $strSQL.= ' ) B';
        if (count($filtersearchData) > 0) {
            $paramDSQL = array();
            $strSQL.= ' WHERE ';
            foreach ($filtersearchData as $key => $value) {
                list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                switch ($value['dtype']) {
                    case 'L':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                    break;
                    case 'T':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                    break;
                    case 'Z':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                    break;
                    default:
                        $paramDSQL[] = $D1COL2 . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                    break;
                }
                $params[] = cmMer($value['D1VAL']);
            }
            $strSQL.= join(' AND ', $paramDSQL);
        }
        if (($start !== '') && ($length !== '')) {
            if (count($filtersearchData) > 0) {
                if ($rsMasterArr[0]['DCSUMF'] === '') {
                    $strSQL.= ' AND B.RN BETWEEN ? AND ? ';
                } else {
                    if ($dnlFlg === false) {
                        $strSQL.= ' AND B.rownum BETWEEN ? AND ? ';
                    } else {
                        $strSQL.= ' AND B.RN BETWEEN ? AND ? ';
                    }
                }
            } else {
                if ($rsMasterArr[0]['DCSUMF'] === '') {
                    $strSQL.= ' WHERE B.RN BETWEEN ? AND ? ';
                } else {
                    if ($dnlFlg === false) {
                        $strSQL.= ' WHERE B.rownum BETWEEN ? AND ? ';
                    } else {
                        $strSQL.= ' WHERE B.RN BETWEEN ? AND ? ';
                    }
                }
            }
            $params[] = $start + 1;
            $params[] = $start + $length;
        }
        $strSQL.= '    ORDER BY  ';
        $strSQL.= join(', ', $ordKeyArr);
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            //   $rtn  = 1;
            $data = array('result' => 'FAIL_SEL');
        } else {
            e_log('【削除】：制御合計データ取得：akz' . $strSQL . print_r($params, true));
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                //  $rtn  = 1;
                $data = array('result' => 'FAIL_SEL');
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                $data = array('result' => true, 'data' => $data);
            }
        }
    } else {
        if (!empty($rsMasterArr) && !empty($rsshukeiArr)) {
         
            $strSQL = ' SELECT F.*  ';
            $strSQL.= ' FROM(  ';
         if ($rsMasterArr[0]['DCFLD1'] !== '' || $rsMasterArr[0]['DCFLD2'] !== '' || $rsMasterArr[0]['DCFLD3'] !== '' || $rsMasterArr[0]['DCFLD4'] !== '' || $rsMasterArr[0]['DCFLD5'] !== '' || $rsMasterArr[0]['DCFLD6'] !== '') {
            $strSQL.= ' SELECT P.*, ';
            $strSQL.= ' CASE WHEN P.RN IS NOT NULL THEN ';
            $strSQL.= ' rownumber() over (  ';
            $strSQL.= ' ) END AS rownum ';
         }else{
            $strSQL.= ' SELECT P.* ';
         }
            $strSQL.= ' FROM ( ';
            $strSQL.= ' SELECT ';
            //DB2COLMテーブルのレベル情報を取得する
            $i = 1;
			$nextNo=$i;
			$isExistNext=false;
            while ($i <= 6) {
                if (cmMer($rsMasterArr[0]['DCFLD' . $i]) !== '') {
					$isExistNext=false;
					$nextNo=$i;
					for($next=$i+1;$next<=6;$next++){
						if(!$isExistNext){
							if(cmMer($rsMasterArr[0]['DCFLD' . $next]) !==''){
								$isExistNext=true;
								$nextNo=$next;
							}
						}
					}
					$splitResArr1=array();
		            $LGroup = explode(",",cmMer($rsMasterArr[0]['DCFLD' . $i]));
                
		            if (count($LGroup) >= 2) { //グループのレベル
						for($flevel = 0;$flevel < count($LGroup);$flevel++){
							$splitRes=explode("-",$LGroup[$flevel]);
							$splitResArr1[] = $splitRes[1] . '_' .$splitRes[0];
						}
						$splitResJoin=implode(",",$splitResArr1);
						$levelFlg.= '( '.$splitResJoin.')'. ',';
                     
						if ($isExistNext===true && $i < 6) {
							for($gIndex=0;$gIndex<count($splitResArr1);$gIndex++){
								if($gIndex < (count($splitResArr1)-1)){
			                        $gpFlg .= ' WHEN GROUPING (' .cmMer($splitResArr1[$gIndex+1]). ' ) = 1 THEN \'' .$rsMasterArr[0]['DCFLD' . $i] . '\'';//$splitResArr1[$gIndex1]
								}else{
									$nextCol=cmMer($rsMasterArr[0]['DCFLD' . $nextNo]);
                                  
									$nextColExp=explode(",",$nextCol);
									$nextColRes='';
									if(count($nextColExp)>=2){
										$nextColExpExp=explode("-",$nextColExp[0]);
										$nextColRes=cmMer($nextColExpExp[1]).'_'.cmMer($nextColExpExp[0]);
									}else{
										$nextColRes=cmMer($rsMasterArr[0]['DCFLD' .$nextNo]) . '_' . cmMer($rsMasterArr[0]['DCFILID' . $nextNo]);
                                    
									}
			                        $gpFlg .= ' WHEN GROUPING (' .$nextColRes. ' ) = 1 THEN \'' .$rsMasterArr[0]['DCFLD' . $i] . '\'';//$splitResArr1[$gIndex1]
								}
							}
						}else{
							for($gIndex1=0;$gIndex1<count($splitResArr1);$gIndex1++){
			                    if (cmMer($splitResArr1[$gIndex1+1]) !== '' && $gIndex1 < (count($splitResArr1)-1)) {
			                        $gpFlg .= ' WHEN GROUPING (' .cmMer($splitResArr1[$gIndex1+1]). ' ) = 1 THEN \'' .$rsMasterArr[0]['DCFLD' . $i] . '\'';
			                    } else {
			                        $gpFlg .= ' ELSE \'' .cmMer($splitResArr1[$gIndex1]). '\'' . ' END ';
			                    }
							}
						}
					}else{
	                    $levelFlg.= $rsMasterArr[0]['DCFLD' . $i] . '_' . $rsMasterArr[0]['DCFILID' . $i] . ',';
                      
	                    if ($isExistNext===true && $i < 6) {
							$nextCol=cmMer($rsMasterArr[0]['DCFLD' . $nextNo]);
							$nextColExp=explode(",",$nextCol);
							$nextColRes='';
							if(count($nextColExp)>=2){
								$nextColExpExp=explode("-",$nextColExp[0]);
								$nextColRes=cmMer($nextColExpExp[1]).'_'.cmMer($nextColExpExp[0]);
							}else{
								$nextColRes=cmMer($rsMasterArr[0]['DCFLD' . $nextNo]) . '_' . cmMer($rsMasterArr[0]['DCFILID' . $nextNo]);
							}
	                        $gpFlg .= ' WHEN GROUPING (' . $nextColRes . ' ) = 1 THEN \'' . $rsMasterArr[0]['DCFLD' . ($i)] . '_' . $rsMasterArr[0]['DCFILID' . ($i)] . '\'';
	                    } else {
	                        $gpFlg .= ' ELSE \'' . cmMer($rsMasterArr[0]['DCFLD' . $i]) . '_' . cmMer($rsMasterArr[0]['DCFILID' . $i]) . '\'' . ' END ';
	                    }
					}
                }
                ++$i;
            }
            //DB2COLTテーブルの集計情報を取得する
            foreach ($rsshukeiArr as $key => $row) {
            //  if ($rsMasterArr[0]['DCFLD1'] !== '' && $rsMasterArr[0]['DCFLD2'] !== '' && $rsMasterArr[0]['DCFLD3'] !== '' && $rsMasterArr[0]['DCFLD4'] !== '' && $rsMasterArr[0]['DCFLD5'] !== '' && $rsMasterArr[0]['DCFLD6'] !== '') {
                 
	                if ($row['DTSUM'] === '1') {
	                    $shukeiFlg.= 'SUM (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS SUM_' . $row['DTFLD'] . '_' . $row['DTFILID'] . ',';
	                }
	                if ($row['DTAVLG'] === '1') {
	                    $shukeiFlg.= 'AVG (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS AVG_' . $row['DTFLD'] . '_' . $row['DTFILID'] . ',';
	                }
	                if ($row['DTMAXV'] === '1') {
	                    $shukeiFlg.= 'MAX (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS MAX_' . $row['DTFLD'] . '_' . $row['DTFILID'] . ',';
	                }
	                if ($row['DTMINV'] === '1') {
	                    $shukeiFlg.= 'MIN (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS MIN_' . $row['DTFLD'] . '_' . $row['DTFILID'] . ',';
	                }
	                if ($row['DTCONT'] === '1') {
	                    $shukeiFlg.= 'COUNT (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS COUNT_' . $row['DTFLD'] . '_' . $row['DTFILID'] . ',';
	                }
            //   }
            }
            // レベルフラッグの連結
			$levelFlgStr1=str_replace('(','',$levelFlg);
			$levelFlgStr2=str_replace(')','',$levelFlgStr1);
            $strSQL.=$levelFlgStr2;
            // groupingストリングの連結
            $levelFlgCnt = explode(',', rtrim($levelFlgStr2, ','));
            if (count($levelFlgCnt) > 1) {
                $strSQL.= ' CASE ';
                $strSQL.= $gpFlg;
            } else {
                $strSQL.= '\'' . $levelFlgCnt[0] . '\'';
            }
            $strSQL.= ' AS CHECK ,';
            // 集計フラッグの連結
            $strSQL.= $shukeiFlg;
         if ($rsMasterArr[0]['DCFLD1'] !== '' || $rsMasterArr[0]['DCFLD2'] !== '' || $rsMasterArr[0]['DCFLD3'] !== '' || $rsMasterArr[0]['DCFLD4'] !== '' || $rsMasterArr[0]['DCFLD5'] !== '' || $rsMasterArr[0]['DCFLD6'] !== '') {
         //   $strSQL.= ' MAX ( ROWNUMBER() OVER() ) as RN';
            $strSQL.= ' MAX (b.rownum) as RN';
         }else{
            $strSQL.= ' 1';
         }
            $strSQL.= ' FROM ';
            $strSQL.= SAVE_DB . '/' . $filename . ' B';
			if (count($filtersearchData) > 0) {
                $paramDSQL = array();
                $strSQL.= ' WHERE ';
                foreach ($filtersearchData as $key => $value) {
                    list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                    switch ($value['dtype']) {
                        case 'L':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                        case 'T':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                        case 'Z':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                        default:
                            $paramDSQL[] = $D1COL2 . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                    }
                    $params[] = cmMer($value['D1VAL']);
                }
                $strSQL.= join(' AND ', $paramDSQL);
            }
	        if ($rsMasterArr[0]['DCFLD1'] !== '' || $rsMasterArr[0]['DCFLD2'] !== '' || $rsMasterArr[0]['DCFLD3'] !== '' || $rsMasterArr[0]['DCFLD4'] !== '' || $rsMasterArr[0]['DCFLD5'] !== '' || $rsMasterArr[0]['DCFLD6'] !== '') {
	            $strSQL.= ' GROUP BY ROLLUP ( ';
	            $strSQL.= rtrim($levelFlg, ',');
	            $strSQL.= ' ) ';
	        }
            $strSQL.= ' ) AS P ) AS F';
            
            //抽出範囲指定
            if (($start !== '') && ($length !== '')) {
              if ($rsMasterArr[0]['DCFLD1'] !== '' || $rsMasterArr[0]['DCFLD2'] !== '' || $rsMasterArr[0]['DCFLD3'] !== '' || $rsMasterArr[0]['DCFLD4'] !== '' || $rsMasterArr[0]['DCFLD5'] !== '' || $rsMasterArr[0]['DCFLD6'] !== '') {
                /*if (count($filtersearchData) > 0) {
                    if ($rsMasterArr[0]['DCSUMF'] === '') {
                        $strSQL.= ' AND F.RN BETWEEN ? AND ? ';
                    } else {
                        if ($dnlFlg === false) {
                            $strSQL.= ' AND F.rownum BETWEEN ? AND ? ';
                        } else {
                            $strSQL.= ' AND F.RN BETWEEN ? AND ? ';
                        }
                    }
                } else {*/
                    if ($rsMasterArr[0]['DCSUMF'] === '') {
                        $strSQL.= ' WHERE F.RN BETWEEN ? AND ? ';
                    } else {
                        if ($dnlFlg === false) {
                          
                            $strSQL.= ' WHERE F.rownum BETWEEN ? AND ? ';
                         
                        } else {
                            $strSQL.= ' WHERE F.RN BETWEEN ? AND ? ';
                        }
                     }
                 // }
                }
          
                if ($rsMasterArr[0]['DCFLD1'] !== '' || $rsMasterArr[0]['DCFLD2'] !== '' || $rsMasterArr[0]['DCFLD3'] !== '' || $rsMasterArr[0]['DCFLD4'] !== '' || $rsMasterArr[0]['DCFLD5'] !== '' || $rsMasterArr[0]['DCFLD6'] !== '') {
	                $strSQL.= '    ORDER BY  ';
	                $strSQL.= join(', ', $ordKeyArr);
	                $params[] = $start + 1;
	                $params[] = $start + $length;
                }
            }
         
            error_log('cmGetDbFile_2の制御データ取得クエリー:' . $strSQL . print_r($params, true));
            $stmt = db2_prepare($db2con, $strSQL);
         
            if ($stmt === false) {
                $data = array('result' => 'FAIL_SEL');
        		error_log('【エラー】cmGetDbFile_2の制御データ取得クエリー:=>' . db2_stmt_errormsg());
            } else {
                //e_log('【削除】：制御合計データ取得：' . $strSQL . print_r($params, true));
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    // $rtn  = 1;
                    $data = array('result' => 'FAIL_SEL');
                } else {
                    while ($row = db2_fetch_assoc($stmt)) {
                        $data[] = $row;
                         error_log('【エラー】cmGetDbFile_2の制御データ取得クエリー:=>' . db2_stmt_errormsg());
                    }
                    $data = array('result' => true, 'data' => $data);
                }
            }
        }
    }
 
    return $data;
}
function cmShukeiDataMerge($db2con, $dFile1, $dFile2, $rsMasterArr, $rsshukeiArr, $fdb2csv2, $fdb2csv2_all, $lenCount) {
  
 
   $fnameS = array();
   $fnameF = array();
    for($f = 1;$f <= 6;$f++){
       $fnameS = explode(",",cmMer($rsMasterArr[0]['DCFLD' . $f]));
   
       if(count($fnameS) > 1){
            $fnameS = $fnameS[count($fnameS)-1];
            $fnameS = explode("-",$fnameS);
            $fnameS = $fnameS[1].'_'.$fnameS[0];
         //   $fnameS = preg_replace("([0-9-]+)", "_", $fnameS);
         //   $fnameS = trim($fnameS, "_");
       }else{
            $fnameS = $fnameS[0].'_'.cmMer($rsMasterArr[0]['DCFILID' . $f]);
       }
       array_push($fnameF,$fnameS);
    
   }


    $selecttype = array();
    $cntfmt = array();
    foreach ($fdb2csv2 as $key => $value) {
        if ($value['D2DNLF'] !== '1') {
            $selecttype[$value['D2FLD'] . '_' . $value['D2FILID']] = $value['D2TYPE'];
            $cntfmt[$value['D2FLD'] . '_' . $value['D2FILID']] = $value['SFGMES'];
        }
    }
    $data = array();
    $c = 0;
    $fdb2csv2 = umEx($fdb2csv2);
    $fdb2csv2_all = umEx($fdb2csv2_all);
    $rsMasterArr = umEx($rsMasterArr);
    $rsshukeiArr = umEx($rsshukeiArr);
    $bef = gettype($dFile1[0][5]);
    $dFile1 = $dFile1;
    $dFile2 = umEx($dFile2);
    if (!empty($rsshukeiArr)) {
        $nameArr = array();
        $rollup = 0;  
        $shukeiVarName = array();
        $calc = array();
        $str = array();
        $shekeiHdArr = array();
        $newarr = 0;
        $count = 0;
        $tmpArr = array();
        $sortArr = array();
        $lstArr = array();
        $lastColVar = '';
        $cnt = 0;
        // 集計メッソード配列を作る
        $shukeiVarName['SUM'] = '合計';
        $shukeiVarName['AVG'] = '平均';
        $shukeiVarName['MAX'] = '最大';
        $shukeiVarName['MIN'] = '最小';
        $shukeiVarName['COUNT'] = 'カウント';
		//始：SEQがないならのため使うカラムのデータ
        $testFdb2csv2 = cmGetFDB2CSV2($db2con,$fdb2csv2[0]['D2NAME'],true,false,true);
        if ($testFdb2csv2['result'] === true) {
            $fdb2csv2_T = umEx($testFdb2csv2['data']);
            $testFdb2csv2 = cmGetFDB2CSV2($db2con,$fdb2csv2[0]['D2NAME'], false, false, true);
            if ($testFdb2csv2['result'] === true) {
                $fdb2csv2_T_ALL = umEx($testFdb2csv2['data']); //データ取得の時使ってる
			}
		}
		//終：SEQがないならのため使うカラムのデータ
        foreach ($fdb2csv2_T as $key => $row) {
            // 小数数配列
            $decArr[$row['D2FLD'] . '_' . $row['D2FILID']] = $row['D2DEC'];
            // ヘッダ名用配列
            //$shekeiHdArr[$row['D2FLD'] . '_' . $row['D2FILID']] = '';
            //カラム配列
            $colArr[$count++] = $row['D2FLD'] . '_' . $row['D2FILID'];
        }
        foreach ($fdb2csv2 as $key2 => $row2) {
            // ヘッダ名用配列
            $shekeiHdArr[$row2['D2FLD'] . '_' . $row2['D2FILID']] = '';
        }
        array_unshift($shekeiHdArr, '', '');
        foreach ($fdb2csv2_T_ALL as $key1 => $row1) {
            // ヘッダ名配列
            $nameArr[$row1['D2FLD'] . '_' . $row1['D2FILID']] = $row1['D2HED'];
        }
		//e_log("shekeiHdArr:".print_r($shekeiHdArr,true));
        // 集計データ用配列を作る
        $i = 0;
        $rk = count($rsshukeiArr);
        while ($i < $rk) {
            $m = 0;
            if (in_array($rsshukeiArr[$i]['DTFLD'] . '_' . $rsshukeiArr[$i]['DTFILID'], $colArr)) {
                if ($rsshukeiArr[$i]['DTSUM'] === '1') {
                    $str[$m++] = 'SUM';
                }
                if ($rsshukeiArr[$i]['DTAVLG'] === '1') {
                    $str[$m++] = 'AVG';
                }
                if ($rsshukeiArr[$i]['DTMAXV'] === '1') {
                    $str[$m++] = 'MAX';
                }
                if ($rsshukeiArr[$i]['DTMINV'] === '1') {
                    $str[$m++] = 'MIN';
                }
                if ($rsshukeiArr[$i]['DTCONT'] === '1') {
                    $str[$m++] = 'COUNT';
                }
                $calc[$rsshukeiArr[$i]['DTFLD'] . '_' . $rsshukeiArr[$i]['DTFILID']] = $str;
                $tmpArr = array_unique(array_merge($tmpArr, $str), SORT_REGULAR);
                $str = array();
            }
            ++$i;
        }
	
        // 配列のデータを「SUM,AVG,MAX,MIN,COUNT」の順番通りソートする
        $k = 0;
        $tpa = count($tmpArr);
        while ($k < $tpa) {
            switch ($tmpArr[$k]) {
                case 'SUM':
                    $sortArr[0] = 'SUM';
                break;
                case 'AVG':
                    $sortArr[1] = 'AVG';
                break;
                case 'MAX':
                    $sortArr[2] = 'MAX';
                break;
                case 'MIN':
                    $sortArr[3] = 'MIN';
                break;
                case 'COUNT':
                    $sortArr[4] = 'COUNT';
                break;
                default:
                break;
            }
            ++$k;
        }
        	
        $l = 0;
        while ($l < 5) {
            if ($sortArr[$l] !== null) {
                //ソートした配列
                $lstArr[$cnt++] = $sortArr[$l];
            }
            ++$l;
        }
        // クエリデータをループして配列に入れる
        $rc = count($dFile1[0]) + 2;
        foreach ($dFile1 as $key => $row1) {
           
            array_unshift($row1, '', '');
            $lastColVar = $row1[$rc - 1];
            unset($row1[$rc - 1]);
            if ($rsMasterArr[0]['DCSUMF'] !== '1') {
                $data[$newarr++] = $row1;
            }
            //e_log('csvdataerror111***'.print_r($data,true));
            //rownumが同じの場合は集計行を入れる
            $dfc = count($dFile2);
          
           while ($rollup < $dfc && $dFile2[$rollup]['RN'] === $lastColVar) {
               
                $level = $dFile2[$rollup]['CHECK'];
                $data[$newarr] = $shekeiHdArr;
               
             
                $fname = '';
                 for($h = 0;$h <= 6;$h++){
	                    if($level == $fnameF[$h]){
                            
		             	  	  if($rsMasterArr[0]['DCFNAME'.($h+1)] != ''){
	                             $fname = $rsMasterArr[0]['DCFNAME'.($h+1)];                                          
		             	  	  }else{
		             	  	  	  $fname = $nameArr[$level];
		             	   }
	             	  }
	             }
                //総合集計の表示
                if ($dFile2[$rollup]['RN'] === cmMer($lenCount) && $rollup === ($dfc - 1)) {
                    $data[$newarr][0] = '総合集計';
                  
                } else if ($rsMasterArr[0]['DCSUMF'] === '') {
                    // 集計結果
                    //$data[$newarr][0] = $nameArr[$level] . '集計';
                      $data[$newarr][0] = $fname  . '集計';
                   
                } else if ($rsMasterArr[0]['DCSUMF'] !== '') {
                   // $data[$newarr][0] = $nameArr[$level] . '集計' . '(' . $dFile2[$rollup][$level] . ')';
                      $data[$newarr][0] = $fname  . '集計' . '(' . $dFile2[$rollup][$level] . ')';
                    
                }
                $i = 0;
                $lac = count($lstArr);
                while ($i < $lac) {
                    $data[$newarr][1] = $shukeiVarName[$lstArr[$i]] . ':';
                
                    foreach ($calc as $key => $row) {
                        $ci = 0;
                        $ck = count($calc[$key]);
                        while ($ci < $ck) {
						
                            if ($shukeiVarName[$lstArr[$i]] === $shukeiVarName[$calc[$key][$ci]]) {
                                $va = $dFile2[$rollup][$calc[$key][$ci] . '_' . $key];
								$testK1=$calc[$key][$ci] . '_' . $key;
								$testK2=$rollup;
								//e_log("calc looping va:".$va."rollup:".$testK2."rolluplast:".$testK1);
								$testK11=explode("_",$testK1);
							//	if (array_key_exists($testK11[$testK11.length-1],$shekeiHdArr)){
	                                //平均の場合はroundする
	                                if ($shukeiVarName[$lstArr[$i]] === '平均') {
	                                    $data[$newarr][$key] = (float)floatFormat($va, $decArr[$key], 3);
                                        //e_log('csvdataerror777***'.print_r($data,true));
	                                } else {
	                                    //データ一つずつの設定
	                                    //spoutエクセルにＳＰＢなら右寄せ
                                      
	                                    if ($selecttype[$key] === 'S' || $selecttype[$key] === 'P' || $selecttype[$key] === 'B' || $cntfmt[$key] === 'COUNT') {
	                                        $data[$newarr][$key] = (float)$va;
	                                    } else {
	                                        $data[$newarr][$key] = $va;
	                                    }
                                     
	                                }
							//	}
                            }
                            ++$ci;
                        }
                    }
                    $data[$newarr] = array_values($data[$newarr]);
                    $newarr++;
                    $data[$newarr] = $shekeiHdArr;
                    ++$i;
                }
                $rollup++;
           }
   
    if($c == $lenCount-1){
       if($dFile2[$rollup]['RN'] == ''){
            while ($rollup < $dfc) {
              
                $level = $dFile2[$rollup]['CHECK'];
                $data[$newarr] = $shekeiHdArr;
               
                //総合集計の表示
            /*    if ($dFile2[$rollup]['RN'] === cmMer($lenCount) && $rollup === ($dfc - 1)) {
                    $data[$newarr][0] = '総合集計';
                    e_log('11111');
                    
                } else if ($rsMasterArr[0]['DCSUMF'] === '') {
                    // 集計結果
                    $data[$newarr][0] = $nameArr[$level] . '集計';
                     e_log('2222');
                    
                }   */
                error_log('DCSUMF = '.$rsMasterArr[0]['DCSUMF']);
                if ($rsMasterArr[0]['DCSUMF'] !== '') {
                     $data[$newarr][0] = '総合集計';
                  
                   
                }
                $i = 0;
                $lac = count($lstArr);
                while ($i < $lac) {
                    $data[$newarr][1] = $shukeiVarName[$lstArr[$i]] . ':';
               
                    foreach ($calc as $key => $row) {
                        $ci = 0;
                        $ck = count($calc[$key]);
                        while ($ci < $ck) {
						
                            if ($shukeiVarName[$lstArr[$i]] === $shukeiVarName[$calc[$key][$ci]]) {
                                $va = $dFile2[$rollup][$calc[$key][$ci] . '_' . $key];
								$testK1=$calc[$key][$ci] . '_' . $key;
								$testK2=$rollup;
								//e_log("calc looping va:".$va."rollup:".$testK2."rolluplast:".$testK1);
								$testK11=explode("_",$testK1);
							//	if (array_key_exists($testK11[$testK11.length-1],$shekeiHdArr)){
	                                //平均の場合はroundする
	                                if ($shukeiVarName[$lstArr[$i]] === '平均') {
	                                    $data[$newarr][$key] = (float)floatFormat($va, $decArr[$key], 3);
                                        //e_log('csvdataerror777***'.print_r($data,true));
	                                } else {
	                                    //データ一つずつの設定
	                                    //spoutエクセルにＳＰＢなら右寄せ
                                      
	                                    if ($selecttype[$key] === 'S' || $selecttype[$key] === 'P' || $selecttype[$key] === 'B') {
	                                        $data[$newarr][$key] = (float)$va;
	                                    } else {
	                                        $data[$newarr][$key] = $va;
	                                    }
                                        //e_log('csvdataerror888***'.print_r($data,true));
	                                }
							//	}
                            }
                            ++$ci;
                        }
                    }
                    $data[$newarr] = array_values($data[$newarr]);
                    $newarr++;
                    $data[$newarr] = $shekeiHdArr;
                    ++$i;
                }
                $rollup++;
            }
          }
        }
          $c++; 
      }
    }
    if (empty($data[count($data) - 1])) {
        unset($data[count($data) - 1]);
    }

    $data = array('result' => true, 'data' => $data);
   // error_log("cmShukeiDataMerge:".print_r($data,true));
    return $data;
}
//ソート対象フィールド情報取得
function cmGetOrdColumnInfo($db2con, $QRYNM) {
    $strSQL = '';
    $data = array();
    $strSQL.= '    SELECT C.* FROM ';
    $strSQL.= '        ( ';
    $strSQL.= '        SELECT A.D2NAME ';
    $strSQL.= '             , A.D2FILID ';
    $strSQL.= '             , A.D2FLD ';
    $strSQL.= '             , A.D2HED ';
    $strSQL.= '             , A.D2CSEQ ';
    $strSQL.= '             , A.D2RSEQ ';
    $strSQL.= '             , CASE A.D2RSTP ';
    $strSQL.= '               WHEN \'A\' THEN \'ASC\' ';
    $strSQL.= '               WHEN \'D\' THEN \'DESC\' ';
    $strSQL.= '               END AS D2RSTP ';
    $strSQL.= '         FROM FDB2CSV2 A ';
    $strSQL.= '         WHERE A.D2NAME = ? ';
    $strSQL.= '         AND   A.D2RSEQ  > 0 ';
    //$strSQL .= '         AND   (A.D2CSEQ > 0 ';
    //$strSQL .= '         OR     A.D2GSEQ > 0) ';
    $strSQL.= '        UNION ALL ';
    $strSQL.= '        SELECT B.D5NAME AS D2NAME ';
    $strSQL.= '             , 9999     AS D2FILID ';
    $strSQL.= '             , B.D5FLD  AS D2FLD ';
    $strSQL.= '             , B.D5HED  AS D2HED ';
    $strSQL.= '             , B.D5CSEQ AS D2CSEQ ';
    $strSQL.= '             , B.D5RSEQ AS D2RSEQ ';
    $strSQL.= '             , CASE B.D5RSTP ';
    $strSQL.= '               WHEN \'A\' THEN \'ASC\' ';
    $strSQL.= '               WHEN \'D\' THEN \'DESC\' ';
    $strSQL.= '               END AS D2RSTP ';
    $strSQL.= '         FROM FDB2CSV5 B ';
    $strSQL.= '        WHERE B.D5NAME = ? ';
    $strSQL.= '         AND   B.D5RSEQ > 0 ';
    // $strSQL .= '         AND   (B.D5CSEQ > 0 ';
    //$strSQL .= '         OR     B.D5GSEQ > 0) ';
    $strSQL.= '     ) C ';
    $strSQL.= '    ORDER BY C.D2RSEQ ';
    $params = array($QRYNM, $QRYNM);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = umEx($data, false);
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/**
 * 配列をシフトする
 *
 * RESULT
 *    01：シフトした配列
 * @param String  $fdb2csv2　ヘッダ情報
 */
function cmArrayUnshift($fdb2csv2) {
    $unshiftArray = array('D2NAME' => '', 'D2FILID' => '', 'D2FLD' => '', 'D2HED' => '', 'D2CSEQ' => '', 'D2WEDT' => '', 'D2TYPE' => '', 'D2LEN' => '', 'D2DEC' => '', 'D2DNLF' => '');
    array_unshift($fdb2csv2, $unshiftArray, $unshiftArray);
    return $fdb2csv2;
}
//free VERSION CHECK
function cmLicenseFDB2CSV1($licenseQuery) {
    $sql = '';
    if ($licenseQuery !== '') {
        $sql.= ' (SELECT * FROM FDB2CSV1  ORDER BY D1NAME ';
        $sql.= ' FETCH FIRST ' . $licenseQuery . ' ROWS ONLY)  ';
    } else {
        $sql = ' FDB2CSV1 ';
    }
    return $sql;
}
//free VERSION CHECK
function cmLicenseDB2WUSR($licenseUser) {
    $sql = '';
    if ($licenseUser === '' || $licenseUser === null) {
        $sql.= 'DB2WUSR';
    } else {
        $sql.= ' (SELECT * FROM DB2WUSR ';
        $sql.= ' WHERE WUUID <> \'\'  ORDER BY WUUID  ';
        $sql.= ' FETCH FIRST ' . $licenseUser . ' ROWS ONLY) ';
    }
    return $sql;
}
//free VERSION CHECK
function cmLicenseDB2WQGR($licenseGroup) {
    $sql = '';
    if ($licenseGroup === '') {
        $sql.= 'DB2WQGR';
    } else {
        $sql.= ' (SELECT * FROM DB2WQGR ';
        $sql.= ' WHERE WQGID <> \'\'  ORDER BY WQGID  ';
        $sql.= ' FETCH FIRST ' . $licenseGroup . ' ROWS ONLY) ';
    }
    return $sql;
}
/*
 *-------------------------------------------------------*
 * カラム名取得
 *-------------------------------------------------------*
*/
function cmGetColumnName($db2con, $tblName) {
    $data = array();
    $strSQL = ' SELECT COLUMN_NAME AS COLUMN_NAME ';
    $strSQL.= ' FROM ' . SYSCOLUMN2 . '  ';
    $strSQL.= ' WHERE TABLE_SCHEMA = ? ';
    $strSQL.= ' AND TABLE_NAME = ? ';
    $params = array(SAVE_DB, $tblName);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
        $data = array('result' => true, 'data' => $data);
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * FDB2CSV2のカラムが'DEVQUERYH'のテーブルにない場合消す
 *-------------------------------------------------------*
*/
function cmColNameDiff($db2con, $fdb2csv2, $tblName, $D1CFLG = '') {
    $colnew_arr = array();
    $tmpFDB2CSV2 = array();
    $column_name = cmGetColumnName($db2con, $tblName);
    if ($column_name['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array('クエリー'));
    } else {
        //        $column_name = umEx($column_name['data']);
        $column_name = $column_name['data'];
    }
    for ($i = 0;$i < count($column_name);$i++) {
        $colnm = cmMer($column_name[$i]['COLUMN_NAME']);
        array_push($colnew_arr, $colnm);
    }
    // id つけているかどうかチェック
   // e_log('column name :' . $colnew_arr[0]);
    $colnmArr = explode('_', $colnew_arr[0]);
    if (count($colnmArr) > 1) {
        if (end($colnmArr) === $fdb2csv2[0]['D2FILID']) {
            $idFlg = true;
        } else {
            $idFlg = false;
        }
    } else {
        $idFlg = false;
    }
    foreach ($fdb2csv2 as $key => $value) {
        if ($idFlg) {
            if (in_array((cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID'])), $colnew_arr)) {
                array_push($tmpFDB2CSV2, $value);
            }
        } else {
            if (in_array(cmMer($value['D2FLD']), $colnew_arr)) {
                array_push($tmpFDB2CSV2, $value);
            }
        }
    }
    return $tmpFDB2CSV2;
}
/**
 * カラムによってD2TYPEの取得
 *
 * RESULT
 *    01：データ件数
 *    02：false
 *
 * @param Object  $db2con  DBコネクション
 * @param String  $D1NAME  クエリ名
 *
 */
function cmGetDB2COLTTYPE($db2con, $DTNAME) {
    $data = array();
    $params = array();
    /*$strSQL = ' select  ';
    $strSQL .= ' DTNAME, ';
    $strSQL .= ' DTSEQ, ';
    $strSQL .= ' DTFILID, ';
    $strSQL .= ' DTFLD, ';
    $strSQL .= ' DTSUM, ';
    $strSQL .= ' DTAVLG, ';
    $strSQL .= ' DTMINV, ';
    $strSQL .= ' DTMAXV, ';
    $strSQL .= ' DTCONT, ';
    $strSQL .= ' D2TYPE from ( ';
    $strSQL .= ' SELECT * FROM  ';
    $strSQL .= ' DB2COLT  ';
    $strSQL .= ' left join  ';
    $strSQL .= '         (select D2TYPE,D2NAME,D2FILID,D2FLD  ';
    $strSQL .= '          from(SELECT D2TYPE,D2NAME,D2FILID,D2FLD,D2CSEQ  ';
    $strSQL .= '             FROM FDB2CSV2  ';
    $strSQL .= '             UNION ALL  ';
    $strSQL .= '             SELECT D5TYPE  ';
    $strSQL .= '             AS D2TYPE,D5NAME AS D2NAME,9999 AS D2FILID, ';
    $strSQL .= '             D5FLD AS D2FLD,D5CSEQ AS D2CSEQ ';
    $strSQL .= '             FROM FDB2CSV5)as A ';
    $strSQL .= '         ) as B ';
    $strSQL .= ' on B.D2NAME = DTNAME ';
    $strSQL .= ' and B.D2FILID = DTFILID  ';
    $strSQL .= ' and B.D2FLD  = DTFLD) as D ';
    $strSQL .= ' where D.DTNAME = ? ';
    $strSQL .= ' and D.DTFLD <> \'\' ';
    $params = array(
    $DTNAME
    );*/
    $strSQL.= '    SELECT ';
    $strSQL.= '        DTNAME, ';
    $strSQL.= '        DTSEQ, ';
    $strSQL.= '        DTFILID, ';
    $strSQL.= '        DTFLD, ';
    $strSQL.= '        DTSUM, ';
    $strSQL.= '        DTAVLG, ';
    $strSQL.= '        DTMINV, ';
    $strSQL.= '        DTMAXV, ';
    $strSQL.= '        DTCONT, ';
    $strSQL.= '        D2TYPE ';
    $strSQL.= '    FROM ';
    $strSQL.= '        ( ';
    $strSQL.= '            SELECT ';
    $strSQL.= '                * ';
    $strSQL.= '            FROM ';
    $strSQL.= '                DB2COLT ';
    $strSQL.= '            WHERE DTNAME = ? ';
    $strSQL.= '            AND DTFLD <>\'\' ';
    $strSQL.= '        ) COLT ';
    $strSQL.= '    LEFT JOIN ';
    $strSQL.= '        ( ';
    $strSQL.= '            ( ';
    $strSQL.= '                SELECT ';
    $strSQL.= '                    D2NAME, ';
    $strSQL.= '                    D2FILID, ';
    $strSQL.= '                    D2FLD, ';
    $strSQL.= '                    D2TYPE, ';
    $strSQL.= '                    D2CSEQ ';
    $strSQL.= '                FROM ';
    $strSQL.= '                    FDB2CSV2 ';
    $strSQL.= '                WHERE D2NAME = ? ';
    $strSQL.= '            ) ';
    $strSQL.= '            UNION ALL ';
    $strSQL.= '            ( ';
    $strSQL.= '                SELECT ';
    $strSQL.= '                    D5NAME AS D2NAME, ';
    $strSQL.= '                    9999 AS D2FILID, ';
    $strSQL.= '                    D5FLD AS D2FLD, ';
    $strSQL.= '                    D5TYPE AS D2TYPE, ';
    $strSQL.= '                    D5CSEQ AS D2CSEQ ';
    $strSQL.= '                FROM ';
    $strSQL.= '                    FDB2CSV5 ';
    $strSQL.= '                WHERE D5NAME = ? ';
    $strSQL.= '            ) ';
    $strSQL.= '        ) FDB2CSV25 ';
    $strSQL.= '    ON  COLT.DTNAME  = FDB2CSV25.D2NAME ';
    $strSQL.= '    AND COLT.DTFILID = FDB2CSV25.D2FILID ';
    $strSQL.= '    AND COLT.DTFLD   = FDB2CSV25.D2FLD ';
    $params = array($DTNAME, $DTNAME, $DTNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/**
 * 表示するカラムが
 *
 * RESULT
 *    01：データ件数
 *    02：false
 *
 * @param Object  $db2con  DBコネクション
 * @param String  $D1NAME  クエリ名
 * @param String  $filid   FILID
 * @param String  $fld     FLD
 *
 */
function cmColCheck($db2con, $d1name, $filid, $fld) {
    $data = false;
    //FILD=9999の場合は無条件でarray()
    if ($filid === '9999') {
        $checkRs = array();
    } else {
        $checkRs = cmCheckFDB2CSV2($db2con, $d1name, $filid, $fld,true);
    }
    if ($checkRs === false) {
        $data = array('result' => 'FAIL_SYS');
    } else {
        if (count($checkRs) === 0) {
            $checkRs = cmCheckFDB2CSV5($db2con, $d1name, $fld,true);
            if ($checkRs === false) {
                $data = array('result' => 'FAIL_SYS');
            } else {
                if (count($checkRs) === 0) {
                    $data = array('result' => 'FAIL_COL');
                } else {
                    $data = array('result' => true);
                }
            }
        } else {
            $data = array('result' => true);
        }
    }
    return $data;
}
/**
 * 制御データチェック
 *
 * RESULT
 *    01：メッセージ　か　OK
 *
 * @param Object  $db2con  DBコネクション
 * @param String  $D1NAME  クエリ名
 * @param String  $rsMasterArr   レベル情報
 * @param String  $rsshukeiArr   集計方法情報
 *
 */
function cmLevelFieldCheck($db2con, $d1name, $rsMasterArr, $rsshukeiArr) {
 
    $rtn = 0;
    if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
      /*  if ($rtn === 0) {
            if (count($rsshukeiArr) !== 0 && $rsMasterArr[0]['DCFLD1'] === '' && $rsMasterArr[0]['DCFLD2'] === '' && $rsMasterArr[0]['DCFLD3'] === '' && $rsMasterArr[0]['DCFLD4'] === '' && $rsMasterArr[0]['DCFLD5'] === '' && $rsMasterArr[0]['DCFLD6'] === '') {
                $rtn = 2;
                $data = array('result' => 'NO_SEIGYO_LEVEL');
            }
        }   */
        if ($rtn === 0) {
            for ($i = 1;$i < 7;$i++) {
                $fld = $rsMasterArr[0]['DCFLD' . $i];
                $filid = $rsMasterArr[0]['DCFILID' . $i];
                if ($fld !== '') {
                    $LGroup = explode(",", $fld);
                    if (count($LGroup) >= 2) { //グループのレベル
                        for ($j = 0;$j < count($LGroup);$j++) {
                            $spiltRes = explode("-", $LGroup[$j]);
                            $rs = cmColCheck($db2con, $d1name, $spiltRes[0], $spiltRes[1]);
                            if ($rs['result'] !== true) {
                                $rtn = 2;
                                $data = array('result' => $rs['result']);
                                break;
                            }
                        }
                    } else {
                        $rs = cmColCheck($db2con, $d1name, $filid, $fld);
                        if ($rs['result'] !== true) {
                            $rtn = 2;
                            $data = array('result' => $rs['result']);
                            break;
                        }
                    }
                }
            }
        }
        if ($rtn === 0) {
            foreach ($rsshukeiArr as $key => $value) {
                $fld1 = $value['DTFLD'];
                $filid1 = $value['DTFILID'];
                if ($fld1 !== '') {
                    $rs = cmColCheck($db2con, $d1name, $filid1, $fld1);
                    if ($rs['result'] !== true) {
                        $rtn = 2;
                        $data = array('result' => $rs['result']);
                        break;
                    }
                }
                if ($rtn === 0) {
                    if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                        if ($value['DTFLD'] === '' && $value['DTFLD'] === null) {
                            $rtn = 2;
                            $data = array('result' => 'NO_SEIGYO_FIELD');
                            break;
                        }
                        if (($value['DTSUM'] === '0' || $value['DTSUM'] === '') && ($value['DTAVLG'] === '0' || $value['DTAVLG'] === '') && ($value['DTMAXV'] === '0' || $value['DTMAXV'] === '') && ($value['DTMINV'] === '0' || $value['DTMINV'] === '') && ($value['DTCONT'] === '0' || $value['DTCONT'] === '')) {
                            $rtn = 2;
                            $data = array('result' => 'NO_SEIGYO_METHOD');
                            break;
                        }
                    }
                }
                if ($rtn === 0) {
                    if ($value['D2TYPE'] !== 'S' && $value['D2TYPE'] !== 'P' && $value['D2TYPE'] !== 'B') {
                        if ($value['DTSUM'] === '1' || $value['DTAVLG'] === '1') {
                            $data = array('result' => 'SEIGYO_STR_CHK');
                            $rtn = 2;
                            break;
                        }
                    }
                }
            }
        }
        if ($rtn === 0) {
            $data = array('result' => true);
        }
    }
    return $data;
}
/**
 * 利用時間が有効かチェック
 *
 * RESULT
 *    01：メッセージ　か　OK
 *
 * @param Object  $db2con  DBコネクション
 *
 */
function cmUtilTimeCheck() {
    $data = array();
    $systime = array();
    $usrsttime = array();
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    //ユーザが設定した時間情報を取る
    $strSQL = ' SELECT *  ';
    $strSQL.= ' FROM DB2UTIM ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => showMsg('FAIL_SEL'));
    } else {
        $r = db2_execute($stmt);
        if ($r === false) {
            $data = array('result' => showMsg('FAIL_SEL'));
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                //ユーザが設定した時間情報を保持する
                $usrsttime[] = $row;
            }
            if (count($usrsttime) > 0) {
                //システム時間情報を取る
                $strSQL = ' SELECT current time as CURRTIME ';
                $strSQL.= ' FROM sysibm/sysdummy1  ';
                $stmt = db2_prepare($db2con, $strSQL);
                if ($stmt === false) {
                    $data = array('result' => showMsg('FAIL_SEL'));
                } else {
                    $r = db2_execute($stmt);
                    if ($r === false) {
                        $data = array('result' => showMsg('FAIL_SEL'));
                    } else {
                        while ($row = db2_fetch_assoc($stmt)) {
                            $data[] = $row;
                        }
                        //システム時間情報を保持する
                        $systime = $data[0]['CURRTIME'];
                        $hrmn = preg_split('/[.]/', $systime);
                        $systimemin = (intval($hrmn[0]) * 60) + intval($hrmn[1]);
                        $starttimemin = ((intval($usrsttime[0]['UTFRHR'])) * 60) + intval($usrsttime[0]['UTFRMN']);
                        $endtimemin = ((intval($usrsttime[0]['UTTOHR'])) * 60) + intval($usrsttime[0]['UTTOMN']);
                        if (intval($usrsttime[0]['UTFRHR']) < 10) {
                            $fhr = '0' . $usrsttime[0]['UTFRHR'];
                        } else {
                            $fhr = $usrsttime[0]['UTFRHR'];
                        }
                        if (intval($usrsttime[0]['UTFRMN']) < 10) {
                            $fmn = '0' . $usrsttime[0]['UTFRMN'];
                        } else {
                            $fmn = $usrsttime[0]['UTFRMN'];
                        }
                        if (intval($usrsttime[0]['UTTOHR']) < 10) {
                            $thr = '0' . $usrsttime[0]['UTTOHR'];
                        } else {
                            $thr = $usrsttime[0]['UTTOHR'];
                        }
                        if (intval($usrsttime[0]['UTTOMN']) < 10) {
                            $tmn = '0' . $usrsttime[0]['UTTOMN'];
                        } else {
                            $tmn = $usrsttime[0]['UTTOMN'];
                        }
                        if ($starttimemin > $endtimemin) {
                            if (($starttimemin <= $systimemin && $systimemin <= 1440) || (0 <= $systimemin && $systimemin <= $endtimemin)) {
                                //ユーザが決めた時間以外はエラー
                                $data = array('result' => 'NO_UTIL_TIME', 'teishiFlg' => $usrsttime[0]['UTSCHF'], 'fromTime' => $fhr . ':' . $fmn, 'toTime' => $thr . ':' . $tmn);
                            } else {
                                $data = array('result' => true);
                            }
                        } else {
                            //利用できる時間を決める
                            if ($starttimemin <= $systimemin && $systimemin <= $endtimemin) {
                                //ユーザが決めた時間以外はエラー
                                $data = array('result' => 'NO_UTIL_TIME', 'teishiFlg' => $usrsttime[0]['UTSCHF'], 'fromTime' => $fhr . ':' . $fmn, 'toTime' => $thr . ':' . $tmn);
                            } else {
                                $data = array('result' => true);
                            }
                        }
                    }
                }
            } else {
                $data = array('result' => true);
            }
        }
    }
    cmDb2Close($db2con);
    return $data;
}
/**exeMail.php(monitor)**/
/*
 *-------------------------------------------------------*
 * DB2WSCD取得カラム
 *-------------------------------------------------------*
*/
function getDb2wscdColumns() {
    $strSQL = ' A.WSNAME, ';
    $strSQL.= ' A.WSPKEY, ';
    $strSQL.= ' A.WSFRQ, ';
    $strSQL.= ' A.WSODAY, ';
    $strSQL.= ' A.WSWDAY, ';
    $strSQL.= ' A.WSMDAY, ';
    $strSQL.= ' A.WSTIME, ';
    $strSQL.= ' A.WSSPND, ';
    $strSQL.= ' A.WSBDAY, ';
    $strSQL.= ' A.WSBTIM, ';
    $strSQL.= ' A.WSSDAY, ';
    $strSQL.= ' A.WSSTIM ';
    return $strSQL;
}
/*
 *-------------------------------------------------------*
 * 実行履歴(空)を作成
 *-------------------------------------------------------*
*/
function fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, $pivkey = '', $errMsg = '', $mailAddLst = array(), $isQGflg = false) {
    e_log("fnCreateHistory mailAddLst ### => ".print_r($mailAddLst,true));
    $DB2WAUT = getDB2WAUT($db2con, $d1name, $pivkey);
    e_log("fnCreateHistory DB2WAUT ### => ".print_r($DB2WAUT,true));
    $WHQGFLG = ($isQGflg) ? '1' : '';
    if ($DB2WAUT['result'] === true) {
        /*if (count($DB2WAUT['data']) == 0) {
            $DB2WHIS = fnInsertDB2WHIS($db2con, $d1name, '', $nowYmd, $nowTime, '', $dbname, '9', '9', '9', $pivkey, $errMsg,$WHQGFLG);
            if ($DB2WHIS['result'] !== true) {
                e_log(showMsg($DB2WHIS['result'], array(
                    'exeMail.php',
                    'fnInsertDB2WHIS'
                )), '1');
            }
        }*/
        if (count($DB2WAUT['data']) === 0) {
            $whcsvf = '0';
            $whxlsf = '0';
            $whhtmf = '0';
            $DB2WHIS = fnDB2WHIS($db2con, $d1name, '', $nowYmd, $nowTime, $dbname, $whcsvf, $whxlsf, $whhtmf, $pivkey, $errMsg, $WHQGFLG);
            if ($DB2WHIS['result'] !== true) {
               // e_log(showMsg($DB2WHIS['result'], array('exeMail.php', 'fnInsertDB2WHIS')), '1');
            }
        }
        $chkHistory = array();
        if (count($mailAddLst) === 0) {
            foreach ($DB2WAUT['data'] as $key => $value) {
                $whcsvf = '0';
                $whxlsf = '0';
                $whhtmf = '0';
                if ($value['WACSV'] == '1') {
                    $whcsvf = '9';
                }
                if ($value['WAXLS'] == '1') {
                    $whxlsf = '9';
                }
                if ($value['WAHTML'] == '1') {
                    $whhtmf = '9';
                }
                fnDB2WHIS($db2con, $d1name, $value['WAMAIL'], $nowYmd, $nowTime, $dbname, $whcsvf, $whxlsf, $whhtmf, $pivkey, $errMsg, $WHQGFLG);
            }
        } else {
            foreach ($mailAddLst as $key => $value) {
                $whcsvf = '0';
                $whxlsf = '0';
                $whhtmf = '0';
                $EXEMAXLEN = '';
                if ($value['WACSV'] == '1') {
                    $whcsvf = '9';
                }
                if ($value['WAXLS'] == '1') {
                    $whxlsf = '9';
                }
                if ($value['WAHTML'] == '1') {
                    $whhtmf = '9';
                }
                fnDB2WHIS($db2con, $d1name, $value['WAMAIL'], $nowYmd, $nowTime, $dbname, $whcsvf, $whxlsf, $whhtmf, $pivkey, $errMsg, $WHQGFLG);
            }
        }
    } else {
        //e_log(showMsg($DB2WAUT['result'], array('exeMail.php', 'getDB2WAUT')), '1');
    }
}
//履歴の登録、エラー編集
function fnDB2WHIS($db2con, $d1name, $WAMAIL, $nowYmd, $nowTime, $dbname, $whcsvf, $whxlsf, $whhtmf, $pivkey = '', $errMsg = '', $WHQGFLG = '') {
    $chkHistory = fnGetDB2WHISByID($db2con, $d1name, $pivkey, $WAMAIL, $nowYmd, $nowTime, $WHQGFLG);
    if ($chkHistory['result'] == true) {
        if (count($chkHistory['data']) > 0) {
            $dhis = umEx($chkHistory['data']);
            if ($dhis[0]['WHERRO'] != "") {
                $errMsg.= "<br/>";
            }
            $errMsg.= $dhis[0]['WHERRO'];
            //e_log('TESTING ERRORMSG=>' . $errMsg);
            $DB2WHIS = fnUpdateDB2WHISByID($db2con, $d1name, $pivkey, $WAMAIL, $nowYmd, $nowTime, $errMsg, $WHQGFLG, $dbname);
            if ($DB2WHIS['result'] !== true) {
               // e_log(showMsg($DB2WHIS['result'], array('exeMail.php', 'fnUpdateDB2WHISByID')), '1');
            }
        } else {
            $DB2WHIS = fnInsertDB2WHIS($db2con, $d1name, $WAMAIL, $nowYmd, $nowTime, '', $dbname, $whcsvf, $whxlsf, $whhtmf, $pivkey, $errMsg, $WHQGFLG);
            if ($DB2WHIS['result'] !== true) {
                //e_log(showMsg($DB2WHIS['result'], array('exeMail.php', 'fnInsertDB2WHIS')), '1');
            }
        }
    } else {
        if ($chkHistory['result'] !== true) {
           e_log(showMsg($DB2WHIS['result'], array('exeMail.php', 'fnGetDB2WHISByID')), '1');
        }
    }
}
/*
 *-------------------------------------------------------*
 * 送信履歴Insert
 *-------------------------------------------------------*
*/
function fnInsertDB2WHIS($db2con, $whname, $whmail, $whbday, $whbtim, $whzpas, $whoutf, $whcsvf, $whxlsf, $whhtmf, $whpkey = '', $errMsg = '', $WHQGFLG = '') {
    $rtn = array();
    $rs = '0';
    //構文
    $strSQL = ' INSERT INTO DB2WHIS ';
    $strSQL.= ' ( ';
    $strSQL.= ' WHNAME, ';
    $strSQL.= ' WHPKEY, ';
    $strSQL.= ' WHMAIL, ';
    $strSQL.= ' WHBDAY, ';
    $strSQL.= ' WHBTIM, ';
    $strSQL.= ' WHCSVF, ';
    $strSQL.= ' WHXLSF, ';
    $strSQL.= ' WHHTMF, ';
    $strSQL.= ' WHZPAS, ';
    $strSQL.= ' WHOUTF, ';
    $strSQL.= ' WHERRO, ';
    $strSQL.= ' WHQGFLG ';
    $strSQL.= ' ) ';
    $strSQL.= ' VALUES ';
    $strSQL.= ' (?,?,?,?,?,?,?,?,?,?,?,?) ';
    $params = array($whname, $whpkey, $whmail, $whbday, $whbtim, $whcsvf, $whxlsf, $whhtmf, $whzpas, $whoutf, $errMsg, $WHQGFLG);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        e_log("prepare fail " . db2_stmt_errormsg());
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        e_log("Hello PP " . $strSQL . print_r($params, true));
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            //e_log("execute fail " . db2_stmt_errormsg());
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            $rtn = array('result' => true);
        }
    }
    return $rtn;
}
//すでに履歴の情報が登録できるかどうかのチェック
function fnGetDB2WHISByID($db2con, $whname, $whpkey, $whmail, $whbday, $whbtim, $WHQGFLG) {
    $data = array();
    $strSQL = ' SELECT * ';
    $strSQL.= ' FROM DB2WHIS  ';
    $strSQL.= ' WHERE WHNAME <> \'\' ';
    $strSQL.= ' AND WHNAME = ? ';
    $strSQL.= ' AND WHMAIL = ? ';
    $strSQL.= ' AND WHBDAY = ? ';
    $strSQL.= ' AND WHBTIM = ? ';
    // $strSQL .= ' AND WHOUTF= ? ';
    $params = array($whname, $whmail, $whbday, $whbtim);
    if ($whpkey != '') {
        $strSQL.= ' AND WHPKEY = ? ';
        array_push($params, $whpkey);
    }
    if ($WHQGFLG === '1') {
        $strSQL.= ' AND WHQGFLG = ? ';
        array_push($params, $WHQGFLG);
    } else {
        $strSQL.= ' AND WHQGFLG <> \'1\'  ';
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
//すでに履歴の情報が登録できてるなら、エラーメッセージを更新する
function fnUpdateDB2WHISByID($db2con, $whname, $whpkey, $whmail, $whbday, $whbtim, $errmsg, $WHQGFLG, $dbname = '') {
    $data = array();
    $strSQL = ' UPDATE DB2WHIS SET WHERRO = ? ';
    //クエリーグループの一番最後のクエリーのTMPのため更新
    if ($WHQGFLG === '1' && cmMer($dbname) !== '') {
        $strSQL.= ' ,WHOUTF = ? ';
    }
    $strSQL.= ' WHERE WHNAME = ? ';
    $strSQL.= ' AND WHMAIL = ? ';
    $strSQL.= ' AND WHBDAY = ? ';
    $strSQL.= ' AND WHBTIM = ? ';
    if ($WHQGFLG === '1' && cmMer($dbname) !== '') {
        $params = array($errmsg, $dbname, $whname, $whmail, $whbday, $whbtim);
    } else {
        $params = array($errmsg, $whname, $whmail, $whbday, $whbtim);
    }
    if ($WHQGFLG === '1') {
        $strSQL.= ' AND WHQGFLG = \'1\' ';
    } else {
        $strSQL.= ' AND WHQGFLG<>\'1\' ';
    }
    if ($whpkey != '') {
        $strSQL.= ' AND WHPKEY = ? ';
        array_push($params, $whpkey);
    }
    //e_log('fnUpdateDB2HISByID⇒' . $strSQL . print_r($params, true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        //e_log($strSQL.'final->'.print_r($params,true));
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            $data = array('result' => true);
        }
    }
    return $data;
}
/**
 * エラーロッグ出力コントロール
 *
 * @param String $msg  出力メッセージ
 * @param String $outputFlg　フラッグ
 */
function e_log($msg, $outputFlg = '0') {
    //configのフラッグを見てエラーロッグ出力の判断
    if (ERRLOG === '1') {
        error_log($msg);
    } else {
        //configの値が'0'の場合本番環境用のロッグだけ出力
        if ($outputFlg === '1') {
            error_log($msg);
        }
    }
}
/**
 *パラメータ文字列を配列に変換
 *@param String $strData  変換したい文字列
 *@戻り値　変換した配列
 *
 */
/*function makeParamArr($strData){
$resarr = array();
$res = strpos($strData,"'");
$res1 = 0;
$tmpStr = '';
while($res<strlen($strData)){
$strData = substr($strData,$res+1);
$res1 = strpos($strData,"'");
if($res1 === (strlen($strData)-1) || $res1 === (strlen($strData)-2)){
$tmpStr .= substr($strData,0,strlen($strData)-1);
$resarr[] = $tmpStr;
$res = strlen($strData);
}else{
if((($strData[$res1+1] === ' ') && ($strData[$res1+2] === '\'')) === false){
$tmpStr .= substr($strData,0,$res1+1);
$res = $res1;
}else{
if($tmpStr !== ''){
$resarr[] = $tmpStr;
$tmpStr = '';
}else{
$resdata = substr($strData,0,$res1);
$resarr[] = $resdata;
}
$res = $res1+2;
}
}
}
return $resarr;
}*/
function makeParamArr($strData) {
    $resarr = array();
    $strData = trim($strData, ' ');
    $res = strpos($strData, "'");
    $res1 = 0;
    $tmpStr = '';
    $resVal = '';
    while ($res < strlen($strData)) {
        $strData = substr($strData, $res + 1);
        $res1 = strpos($strData, "'");
        if ($res1 === (strlen($strData) - 1) || $res1 === (strlen($strData) - 2)) {
            $tmpStr.= substr($strData, 0, strlen($strData) - 1);
            $resarr[] = $tmpStr;
            $res = strlen($strData);
        } else {
            if ((($strData[$res1 + 1] === ' ') && ($strData[$res1 + 2] === '\'')) === false) {
                $tmpStr.= substr($strData, 0, $res1 + 1);
                if ($resVal !== '') {
                    $resVal = $tmpStr;
                }
                $tmpStrData = substr($strData, $res1 + 1);
                $nextIdx = strpos($tmpStrData, "'");
                if ((strlen(substr($tmpStrData, 0, $nextIdx)) > 0) && (trim(substr($tmpStrData, 0, $nextIdx), ' ') === '')) {
                    if ($resVal === '') {
                        $resdata = substr($strData, 0, $res1);
                    } else {
                        $resdata = $resVal;
                        $resdata = substr($resVal, 0, -1);
                        $resVal = '';
                    }
                    $resarr[] = $resdata;
                    $tmpStr = '';
                    $strData = substr($tmpStrData, $nextIdx + 1);
                    $res = - 1;
                } else {
                    $resVal = $tmpStr;
                    $res = $res1;
                }
            } else {
                if ($tmpStr !== '') {
                    $resdata = $tmpStr . substr($strData, 0, $res1);
                    $resarr[] = $resdata;
                    $tmpStr = '';
                } else {
                    $resdata = substr($strData, 0, $res1);
                    $resarr[] = $resdata;
                }
                $res = $res1 + 2;
            }
        }
    }
    return $resarr;
}
/**20161018**/
function cmGetDB2PMST($db2con, $d1name, $pmpkey) {
    $rtn = array();
    $data = array();
    $strSQL = '';
    $strSQL.= ' SELECT * FROM DB2PMST ';
    $strSQL.= ' WHERE PMNAME = ? AND PMPKEY = ? ';
    $params = array($d1name, $pmpkey);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    $rtn = $data;
    return $rtn;
}
function cmGetDB2PCOL($db2con, $d1name, $wppkey) {
    $data = array();
    $strSQL = '';
    $strSQL.= '    SELECT ';
    $strSQL.= '        A.*, ';
    $strSQL.= '        B.D2LEN, ';
    $strSQL.= '        B.D2DEC, ';
    $strSQL.= '        B.D2TYPE, ';
    $strSQL.= '        D2WEDT ';
    $strSQL.= '    FROM ';
    $strSQL.= '    ( ';
    $strSQL.= '        SELECT *  ';
    $strSQL.= '        FROM DB2PCOL ';
    $strSQL.= '        WHERE ';
    $strSQL.= '            WPNAME = ? ';
    $strSQL.= '        AND WPPKEY = ? ';
    $strSQL.= '    ) A ';
    $strSQL.= '    LEFT JOIN ';
    $strSQL.= '    ( ';
    $strSQL.= '        SELECT ';
    $strSQL.= '            D2NAME, ';
    $strSQL.= '            D2FILID, ';
    $strSQL.= '            D2FLD, ';
    $strSQL.= '            D2LEN, ';
    $strSQL.= '            D2DEC, ';
    $strSQL.= '            D2TYPE, ';
    $strSQL.= '            D2WEDT ';
    $strSQL.= '        FROM ';
    $strSQL.= '            FDB2CSV2 ';
    $strSQL.= '        WHERE D2NAME = ? ';
    $strSQL.= '    UNION ';
    $strSQL.= '        SELECT ';
    $strSQL.= '            D5NAME AS D2NAME, ';
    $strSQL.= '            9999 AS D2FILID, ';
    $strSQL.= '            D5FLD AS D2FLD, ';
    $strSQL.= '            D5LEN AS D2LEN, ';
    $strSQL.= '            D5DEC AS D2DEC, ';
    $strSQL.= '            D5TYPE AS D2TYPE, ';
    $strSQL.= '            D5WEDT AS D2WEDT ';
    $strSQL.= '        FROM ';
    $strSQL.= '            FDB2CSV5 ';
    $strSQL.= '        WHERE D5NAME = ? ';
    $strSQL.= '    ) B ';
    $strSQL.= '    ON A.WPNAME = B.D2NAME ';
    $strSQL.= '    AND A.WPFILID = B.D2FILID ';
    $strSQL.= '    AND A.WPFLD = B.D2FLD ';
    $strSQL.= '    ORDER BY ';
    $strSQL.= '        WPPFLG, ';
    $strSQL.= '        WPSEQN ';
    $params = array($d1name, $wppkey, $d1name, $d1name);
	error_log('cmGetdb2 sql = '.$strSQL.'params'.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data === false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data === false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
        }
    }
    return $data;
}
function cmGetDB2PCAL($db2con, $d1name, $wcckey) {
    $data = array();
    $strSQL = '';
    $strSQL.= ' SELECT * FROM DB2PCAL ';
    $strSQL.= ' WHERE WCNAME = ? AND WCCKEY = ? ';
    $strSQL.= ' ORDER BY WCSEQN ';
    $params = array($d1name, $wcckey);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
        }
    }
    return $data;
}
/**20161018**/
/*
 *-------------------------------------------------------*
 * DB2WDFL取得 20161122
 *-------------------------------------------------------*
*/
function cmCountDB2WDFL($db2con, $QYNAME, $FILID, $FLD) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT COUNT(*) AS COUNT ';
    $strSQL.= ' FROM DB2WDFL';
    $strSQL.= ' WHERE DFNAME = ? ';
    $strSQL.= ' AND DFFILID= ? ';
    $strSQL.= ' AND DFFLD= ? ';
    $params = array($QYNAME, $FILID, $FLD);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $COUNT = $row['COUNT'];
            }
            $data = array('result' => true, 'COUNT' => $COUNT);
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 *取得 DB2DRGS
 *-------------------------------------------------------*
*/
function cmCountDB2DRGS($db2con, $DRKMTN, $DRKFID, $DRKFLID) {
    $rs = true;
    //構文
    $strSQL = ' SELECT COUNT(*) AS COUNT FROM DB2DRGS WHERE ';
    $strSQL.= ' DRKMTN = ?  AND DRKFID = ?  AND DRKFLID = ? ';
    $params = array($DRKMTN, $DRKFID, $DRKFLID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $COUNT = $row['COUNT'];
            }
            $data = array('result' => true, 'COUNT' => $COUNT);
        }
    }
    return $data;
}
function replaceFilename($filename) {
    /*  $filename = str_replace(array('\\','/',':','*','?','"','<','>','|'),'_',$filename);
    return  $filename;
    */
    return str_replace(array('\\', '/', ':', '*', '?', '"', '<', '>', '|'), '_', $filename);
}
/***
 *
 *ウエブ画面からの検索必須データチェック
 *
 ***/
function cmChkWebHisu($webData, $SCH) {
    $VALUES = $SCH;
    $jsq = explode("-", $SCH['DRDJSQ']);
    $DRDJSQ = $jsq[0];
    $DRDCNT = $jsq[1];
    if ($SCH['DRDFID'] === '0') {
        $VALUES['DRDFNM'] = 'P.' . $SCH['DRDFNM'];
    } else if ($SCH['DRDFID'] === '9999') {
        $VALUES['DRDFNM'] = 'K.' . $SCH['DRDFNM'];
    } else {
        $VALUES['DRDFNM'] = 'S.' . $SCH['DRDFID'] . '.' + $SCH['DRDFNM'];
    }
    $rs = true;
    for ($i = 0;$i < count($webData);$i++) {
        $datas = $webData[$i]['CNDSDATA'];
        for ($j = 0;$j < count($datas);$j++) {
            $DATA = $datas[$j];
            if ($webData[$i]['CNDIDX'] !== $DRDJSQ || $DATA['CNDFLD'] !== $VALUES['DRDFNM'] || $DATA['CNFILID'] !== $VALUES['DRDFID'] || $DATA['CDATAIDX'] !== $DRDCNT) {
                if ($DATA['CNDDATA'][0] === '' && $DATA['CNDTYP'] === '1') {
                    $rs = 'DRILL_HISSU';
                    break;
                }
            }
        }
        if ($rs === false) {
            break;
        }
    }
    return $rs;
}
/**
 *メール形のチェック
 *
 *
 */
function cmMailFormatChk($mailaddress) {
    $res = true;
    if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $mailaddress)) {
        $res = false;
    }
    return $res;
}
//EXCEL SETTING 挿入文字
function changeWEFBARRFORMAT($CNDSDATA) {
    //change array format
    $formatarray = array();
    $resultarray = array();
    if (count($CNDSDATA) > 0 && is_array($CNDSDATA)) {
        foreach ($CNDSDATA as $keys => $values) {
            $cnds = $values['CNDSDATA'];
            $counted = array_count_values(array_map(function ($value) {
                return $value['CNDTYP'];
            }, $cnds));
            $andorlist = setAndOr(str_replace(" ", "", $CNDSDATA[$keys]['SQRYAO']));
            if ($keys !== 0) {
                $tmp = array('isSQL' => '', 'gandor' => cmMer($andorlist), 'andor' => '', 'cndlabel' => '', 'cnddata' => '', 'cndtype' => '');
                $formatarray[] = $tmp;
            }
            $cnds = $values['CNDSDATA'];
            $innerANORCount = 0; //最初の内のかつ、もくカウント
            foreach ($cnds as $key => $value) {
                //検索タイプ　チェック
                $CNDTYP = $value['CNDTYP'];
                if ($CNDTYP !== '3') {
                    $cndlabel = $value['CNDFHED'];
                    $cndtype = getConfigLabel(str_replace(" ", "", $value['CNDKBN']));
                    $andor = setAndOr(str_replace(" ", "", $value['CNDANDOR']));
                    $cnd = $value['CNDDATA'];
                    $cnddata = setColFListRange($value['CNDKBN'], $cnd);
                    $gandor = (count($CNDSDATA) === 1) ? 'seq' : '';
                    $tmp = array('isSQL' => '', 'gandor' => cmMer($gandor), 'andor' => ($innerANORCount === 0) ? "" : $andor, //かつ、もくチック
                    'cndlabel' => $cndlabel, 'cnddata' => $cnddata, 'cndtype' => $cndtype);
                    $formatarray[] = $tmp;
                    $innerANORCount++;
                }
            }
            //外チェックのかつ、もく
            $fOuterArr = current($formatarray); //first array
            $lOuterArr = end($formatarray); // last array
            if (!empty($fOuterArr['gandor']) && $fOuterArr['gandor'] !== "seq") {
                array_shift($formatarray);
            }
            if (!empty($lOuterArr['gandor']) && $lOuterArr['gandor'] !== "seq") {
                array_pop($formatarray);
            }
        }
    }
    //バーランのデータを削除
    $formatarray = array_values(array_filter($formatarray, function ($v) {
        return array_filter($v) != array();
    }));
    return $formatarray;
}
function changeSCREENARRFORMAT($CNDSDATA) {
    $formatarray = array();
    if (count($CNDSDATA) > 0 && is_array($CNDSDATA)) {
        $innerANORCount = 0; //最初の内のかつ、もくカウント
        foreach ($CNDSDATA as $key => $values) {
            //検索タイプ　チェック
            $D3USEL = $values['D3USEL'];
            if ($D3USEL !== '3') {
                $d3dat = array();
                $andor = setAndOr(str_replace(" ", "", $values['D3ANDOR']));
                $cndlabel = $values['D2HED'];
                $d3type = $values['D3TYPE']; //type
                if ($d3type === 'S' || $d3type === 'B' || $d3type === 'P') {
                    $d3dat = explode(" ", trim($values['D3DAT']));
                    $d3dat = array_values(array_filter($d3dat, 'strlen'));
                } else {
                    $d3dat = makeParamArr($values['D3DAT']);
                }
                $list1Flg = $values['list1Flg']; //true define that LIST or NLIST DATA is only one value
                if ($list1Flg === true) array_pop($d3dat);
                $cnddata = setColFListRange(str_replace(" ", "", $values['D3CND']), $d3dat);
                $cndtype = getConfigLabel(str_replace(" ", "", $values['D3CND']));
                $tmp = array('isSQL' => '', 'gandor' => 'seq', 'andor' => ($innerANORCount === 0) ? "" : $andor, //かつ、もくチック
                'cndlabel' => $cndlabel, 'cnddata' => $cnddata, 'cndtype' => $cndtype);
                $formatarray[] = $tmp;
                $innerANORCount++;
            }
        }
    }
    return $formatarray;
}
function changeSQLARRFORMAT($CNDSDATA) {
    $formatarray = array();
    if (count($CNDSDATA) > 0 && is_array($CNDSDATA)) {
        foreach ($CNDSDATA as $key => $values) {
            //検索タイプ　チェック
            $D3USEL = $values['CNDSTKB'];
            if ($D3USEL !== '3') {
                $d3dat = array();
                $cndlabel = $values['CNDNM'];
                $d3type = $values['CNDDTYP']; //type
                $cnddata = cmMer($values['CNDDAT']);
                $tmp = array('isSQL' => '1', 'gandor' => '', 'andor' => '', 'cndlabel' => $cndlabel, 'cnddata' => $cnddata, 'cndtype' => '');
                $formatarray[] = $tmp;
            }
        }
    }
    return $formatarray;
}
//EXCEL SETTING 挿入文字
/*function cmCNDSDATA($WEBF, $db2con, $d1name, $CNDSDATA,$D1CFLG)
{
$CNDSDATAF = array();
$data      = array();
if (count($CNDSDATA) > 0) { //普通のクエリー,制御レベル,ピボットExcel データー
$CNDSDATAF = ($WEBF !== '1') ? changeSCREENARRFORMAT($CNDSDATA) : changeWEFBARRFORMAT($CNDSDATA);
} else { //メール,#call,スケジュール Excel データー

if ($WEBF !== '1') {
// 黒い画面
$rsCND = fnGetFDB2CSV3HCNDEXCEL($db2con, $d1name);
if ($rsCND['result'] !== true) {
$rtn = 1;
$msg = showMsg($rsCND['result']);
} else {
$res       = umEx($rsCND['data']);
$CNDSDATAF = changeSCREENARRFORMAT($res);
}
} else {
//WEB
if($D1CFLG==='1'){
$rs=fnBSQLHCND($db2con, $d1name);
if ($rs['result'] !== true) {
$rtn = 1;
$msg = showMsg($rs['result']);
} else {
$CNDSDATA = $rs['data'];
}
$CNDSDATAF = changeWEFBARRFORMAT($CNDSDATA);
}else{
$rs = fnGetBQRYCND($db2con, $d1name);
if ($rs['result'] !== true) {
$rtn = 1;
$msg = showMsg($rs['result']);
} else {
$data = $rs['data'];
}
$data = umEx($data);
if (count($data) > 0) {
$CNDSDATA = fnCreateCndData($data);
}
$CNDSDATAF = changeWEFBARRFORMAT($CNDSDATA);
}
}
}
return $CNDSDATAF;
}*/
//EXCEL SETTING 挿入文字
function cmCNDSDATA($WEBF, $db2con, $d1name, $CNDSDATA, $D1CFLG) {
    $CNDSDATAF = array();
    $data = array();
    if ($WEBF !== '1') { // 黒い画面
        if (count($CNDSDATA) <= 0) { //メール,#call,スケジュール Excel データー
            $rsCND = fnGetFDB2CSV3HCNDEXCEL($db2con, $d1name);
            if ($rsCND['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rsCND['result']);
            } else {
                $CNDSDATA = umEx($rsCND['data']);
            }
        }
        $CNDSDATAF = changeSCREENARRFORMAT($CNDSDATA);
    } else {
        if ($D1CFLG === '1') { //SQLクエリーのためFLAG
            if (count($CNDSDATA) <= 0) { //メール,#call,スケジュール Excel データー
                $rs = fnBSQLHCND($db2con, $d1name);
                if ($rs['result'] !== true) {
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                } else {
                    $CNDSDATA = $rs['data'];
                }
            }
            $CNDSDATAF = changeSQLARRFORMAT($CNDSDATA);
        } else {
            if (count($CNDSDATA) <= 0) { //メール,#call,スケジュール Excel データー
                $rs = fnBQRYCNDDATEXCEL($db2con, $d1name);
                if ($rs['result'] !== true) {
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                } else {
                    $data = $rs['data'];
                }
                $data = umEx($data);
                if (count($data) > 0) {
                    $CNDSDATA = fnCreateCndDataEXCEL($data);
                }
            }
            $CNDSDATAF = changeWEFBARRFORMAT($CNDSDATA);
        }
    }
    return $CNDSDATAF;
}
/*
 *-------------------------------------------------------*
 * 検索情報とヘルプ取得
 *-------------------------------------------------------*
*/
function fnGetFDB2CSV3HCNDEXCEL($db2con, $d1name) {
    $data = array();
    $params = array();
    $strSQL.= '    SELECT \'0\' as list1Flg,';
    $strSQL.= '        A.*, ';
    $strSQL.= '        B.* , ';
    $strSQL.= '        C.DHINFO, ';
    $strSQL.= '        C.DHINFG ';
    $strSQL.= '    FROM ';
    $strSQL.= '        FDB2CSV3 AS A ';
    $strSQL.= '    JOIN FDB2CSV2 as B ';
    $strSQL.= '    ON A.D3NAME = B.D2NAME ';
    $strSQL.= '    AND A.D3FILID = B.D2FILID ';
    $strSQL.= '    AND A.D3FLD = B.D2FLD ';
    $strSQL.= '    LEFT JOIN DB2HCND AS C ';
    $strSQL.= '    ON A.D3NAME  = C.DHNAME ';
    $strSQL.= '    AND A.D3FILID  = C.DHFILID ';
    $strSQL.= '    AND A.D3JSEQ  = C.DHSSEQ ';
    $strSQL.= '    AND C.DHMSEQ = \'0\' ';
    $strSQL.= '    WHERE ';
    $strSQL.= '        A.D3NAME = ?  AND ';
    $strSQL.= '        A.D3USEL <> ? ';
    $strSQL.= '    ORDER BY ';
    $strSQL.= '        A.D3JSEQ ASC ';
    $params = array($d1name, '3');
    $stmt = db2_prepare($db2con, $strSQL);
    //e_log('SQL文：'.$strSQL.print_r($params,true));
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
function fnBQRYCNDDATEXCEL($db2con, $QRYNM) {
    $data = array();
    $params = array();
    $strSQL = '';
    $strSQL.= '    SELECT QRYCND.CNQRYN ';
    $strSQL.= '    , QRYCND.CNFILID ';
    $strSQL.= '            , QRYCND.FILSEQ ';
    $strSQL.= '            , QRYCND.CNFIL ';
    $strSQL.= '            , QRYCND.CNMSEQ ';
    $strSQL.= '            , GRPCND.CNDCNT ';
    $strSQL.= '            , GRPCND.CNSCNT ';
    $strSQL.= '            , GRPCND.CDCNT ';
    $strSQL.= '            , QRYCND.CNSSEQ ';
    $strSQL.= '            , QRYCND.CNFLDN ';
    $strSQL.= '            , QRYCND.D2HED ';
    $strSQL.= '            , QRYCND.D2TYPE ';
    $strSQL.= '            , QRYCND.D2LEN ';
    $strSQL.= '            , QRYCND.D2DEC ';
    $strSQL.= '            , QRYCND.CNAOKB ';
    $strSQL.= '            , QRYCND.CNCKBN ';
    $strSQL.= '            , QRYCND.CNSTKB ';
    $strSQL.= '            , QRYCND.CNCANL ';
    $strSQL.= '            , QRYCND.CNCANF ';
    $strSQL.= '            , QRYCND.CNCANC ';
    $strSQL.= '            , QRYCND.CNCANG ';
    $strSQL.= '            , QRYCND.CNNAMG ';
    $strSQL.= '            , QRYCND.CNNAMC ';
    //ソート用カラム導入
    $strSQL.= '            , QRYCND.CNCAST ';
    $strSQL.= '            , QRYCND.CNNAST ';
    //カレンダ用カラム導入
    $strSQL.= '            , QRYCND.CNDFMT ';
    $strSQL.= '            , QRYCND.CNDSFL ';
    //日付のデータ用カラム導入
    $strSQL.= '            , QRYCND.CNDFIN ';
    $strSQL.= '            , QRYCND.CNDFPM ';
    $strSQL.= '            , QRYCND.CNDFDY ';
    $strSQL.= '            , QRYCND.CNDFFL ';
    $strSQL.= '            , QRYCND.CNDTIN ';
    $strSQL.= '            , QRYCND.CNDTPM ';
    $strSQL.= '            , QRYCND.CNDTDY ';
    $strSQL.= '            , QRYCND.CNDTFL ';
    // ヘルプ情報取得
    $strSQL.= '            , QRYCND.DHINFO ';
    $strSQL.= '            , QRYCND.DHINFG ';
    $strSQL.= '            , QRYCND.CDCDCD ';
    $strSQL.= '            , QRYCND.CDDAT ';
    $strSQL.= '            , QRYCND.CDBCNT ';
    $strSQL.= '       FROM ';
    $strSQL.= '           ( SELECT ';
    $strSQL.= '                    A.CNQRYN ';
    $strSQL.= '                  , A.CNFILID ';
    $strSQL.= '                  , FILCD.FILSEQ ';
    $strSQL.= '                  , ( CASE ';
    $strSQL.= '                         WHEN A.CNFILID = 0 THEN \'P\' ';
    $strSQL.= '                         WHEN A.CNFILID <= FILCD.FILSEQ  ';
    $strSQL.= '                         THEN CONCAT(\'S\', A.CNFILID )  ';
    $strSQL.= '                         ELSE \'K\' ';
    $strSQL.= '                      END ';
    $strSQL.= '                     ) AS CNFIL ';
    $strSQL.= '                  , A.CNMSEQ ';
    $strSQL.= '                  , A.CNSSEQ ';
    $strSQL.= '                  , A.CNFLDN ';
    $strSQL.= '                  , A.D2HED ';
    $strSQL.= '                  , A.D2TYPE ';
    $strSQL.= '                  , A.D2LEN ';
    $strSQL.= '                  , A.D2DEC ';
    $strSQL.= '                  , A.CNAOKB ';
    $strSQL.= '                  , A.CNCKBN ';
    $strSQL.= '                  , A.CNSTKB ';
    $strSQL.= '                  , A.CNCANL ';
    $strSQL.= '                  , A.CNCANF ';
    $strSQL.= '                  , A.CNCANC ';
    $strSQL.= '                  , A.CNCANG ';
    $strSQL.= '                  , A.CNNAMG ';
    $strSQL.= '                  , A.CNNAMC ';
    //ソート用カラム導入
    $strSQL.= '                  , A.CNCAST ';
    $strSQL.= '                  , A.CNNAST ';
    //カレンダ用カラム導入
    $strSQL.= '                  , A.CNDFMT ';
    $strSQL.= '                  , A.CNDSFL ';
    //日付のデータ用カラム導入
    $strSQL.= '                  , A.CNDFIN ';
    $strSQL.= '                  , A.CNDFPM ';
    $strSQL.= '                  , A.CNDFDY ';
    $strSQL.= '                  , A.CNDFFL ';
    $strSQL.= '                  , A.CNDTIN ';
    $strSQL.= '                  , A.CNDTPM ';
    $strSQL.= '                  , A.CNDTDY ';
    $strSQL.= '                  , A.CNDTFL ';
    // ヘルプ情報取得
    $strSQL.= '                  , HCND.DHINFO ';
    $strSQL.= '                  , HCND.DHINFG ';
    $strSQL.= '                  , B.CDCDCD ';
    $strSQL.= '                  , B.CDDAT ';
    $strSQL.= '                  , B.CDBCNT ';
    $strSQL.= '             FROM ';
    $strSQL.= '                ( SELECT AA.* ';
    $strSQL.= '                  FROM ';
    $strSQL.= '                   (SELECT QCND.* ';
    $strSQL.= '                        , FLDINFO.D2HED ';
    $strSQL.= '                        , FLDINFO.D2TYPE ';
    $strSQL.= '                        , FLDINFO.D2LEN ';
    $strSQL.= '                        , FLDINFO.D2DEC ';
    $strSQL.= '                    FROM BQRYCND QCND ';
    $strSQL.= '                    LEFT JOIN ( ';
    $strSQL.= '                        SELECT FLDDATA.* ';
    $strSQL.= '                        FROM ( ';
    $strSQL.= '                             SELECT D2NAME ';
    $strSQL.= '                                  , D2FILID ';
    $strSQL.= '                                  , D2FLD ';
    $strSQL.= '                                  , D2HED ';
    $strSQL.= '                                  , D2CSEQ ';
    $strSQL.= '                                  , D2WEDT ';
    $strSQL.= '                                  , D2TYPE ';
    $strSQL.= '                                  , D2LEN ';
    $strSQL.= '                                  , D2DEC ';
    $strSQL.= '                                  , D2DNLF ';
    $strSQL.= '                            FROM FDB2CSV2 ';
    $strSQL.= '                            WHERE D2NAME = ? ';
    $strSQL.= '                        UNION ALL  ';
    $strSQL.= '                            SELECT  D5NAME AS D2NAME ';
    $strSQL.= '                                   , 9999 AS D2FILID  ';
    $strSQL.= '                                   , D5FLD AS D2FLD ';
    $strSQL.= '                                   , D5HED AS D2HED ';
    $strSQL.= '                                   , D5CSEQ AS D2CSEQ ';
    $strSQL.= '                                   , D5WEDT AS D2WEDT ';
    $strSQL.= '                                   , D5TYPE AS D2TYPE ';
    $strSQL.= '                                   , D5LEN AS D2LEN ';
    $strSQL.= '                                   , D5DEC AS D2DEC ';
    $strSQL.= '                                   , D5DNLF AS D2DNLF  ';
    $strSQL.= '                            FROM FDB2CSV5  ';
    $strSQL.= '                            WHERE D5NAME = ? ';
    $strSQL.= '                        )  FLDDATA ';
    $strSQL.= '                    ) FLDINFO ';
    $strSQL.= '                    ON QCND.CNQRYN    = FLDINFO.D2NAME ';
    $strSQL.= '                    AND QCND.CNFILID  = FLDINFO.D2FILID ';
    $strSQL.= '                    AND QCND.CNFLDN   = FLDINFO.D2FLD ';
    $strSQL.= '                    WHERE QCND.CNQRYN = ? ';
    $strSQL.= '                   )   AA  ';
    $strSQL.= '                ) A  ';
    $strSQL.= '               LEFT JOIN  BCNDDAT B  ';
    $strSQL.= '               ON    A.CNQRYN  = B.CDQRYN  ';
    $strSQL.= '               AND   A.CNFILID = B.CDFILID ';
    $strSQL.= '               AND   A.CNMSEQ  = B.CDMSEQ ';
    $strSQL.= '               AND   A.CNSSEQ  = B.CDSSEQ  ';
    // ヘルプ情報取得
    $strSQL.= '               LEFT JOIN  DB2HCND HCND  ';
    $strSQL.= '               ON    A.CNQRYN  = HCND.DHNAME  ';
    $strSQL.= '               AND   A.CNFILID = HCND.DHFILID ';
    $strSQL.= '               AND   A.CNMSEQ  = HCND.DHMSEQ ';
    $strSQL.= '               AND   A.CNSSEQ  = HCND.DHSSEQ  ';
    $strSQL.= '               LEFT JOIN  ( ';
    $strSQL.= '                   SELECT RTQRYN,MAX(RTRSEQ) AS FILSEQ ';
    $strSQL.= '                   FROM BREFTBL ';
    $strSQL.= '                   WHERE RTQRYN = ? ';
    $strSQL.= '                   GROUP BY  RTQRYN ) FILCD  ';
    $strSQL.= '               ON FILCD.RTQRYN = A.CNQRYN  ';
    $strSQL.= '               WHERE   A.CNQRYN  = ? ';
    // $strSQL .= '                 AND   A.CNSTKB <>  \'3\' ';
    $strSQL.= '               ORDER BY A.CNMSEQ ,A.CNSSEQ  ,B.CDCDCD  ';
    $strSQL.= '           ) QRYCND ,  ';
    $strSQL.= '           (  ';
    $strSQL.= '               SELECT A.CNQRYN  ';
    $strSQL.= '                    , A.CNMSEQ  ';
    $strSQL.= '                    , B.CNDCNT  ';
    $strSQL.= '                    , A.CNSCNT  ';
    $strSQL.= '                    , C.CDSSEQ  ';
    $strSQL.= '                    , C.CDCNT  ';
    $strSQL.= '               FROM (  ';
    $strSQL.= '                       SELECT CNQRYN  ';
    $strSQL.= '                            , CNMSEQ  ';
    $strSQL.= '                            , COUNT(CNSSEQ) CNSCNT  ';
    $strSQL.= '                         FROM BQRYCND  ';
    $strSQL.= '                        WHERE CNQRYN  = ?  ';
    //$strSQL .= '                          AND CNSTKB <>  \'3\' ';
    $strSQL.= '                     GROUP BY CNQRYN ';
    $strSQL.= '                            , CNMSEQ  ';
    $strSQL.= '                   ) A  ';
    $strSQL.= '                   ,(  ';
    $strSQL.= '                       SELECT CNQRYN  ';
    $strSQL.= '                            , MAX(CNMSEQ) AS CNDCNT  ';
    $strSQL.= '                         FROM BQRYCND   ';
    $strSQL.= '                        WHERE CNQRYN  = ?  ';
    // $strSQL .= '                          AND CNSTKB <>  \'3\' ';
    $strSQL.= '                     GROUP BY CNQRYN  ';
    $strSQL.= '                   ) B  ';
    $strSQL.= '                   ,( ';
    $strSQL.= '                       SELECT QRYC.CNQRYN AS CDQRYN ';
    $strSQL.= '                            , QRYC.CNMSEQ AS CDMSEQ  ';
    $strSQL.= '                            , QRYC.CNSSEQ AS CDSSEQ ';
    $strSQL.= '                            , COUNT(CDAT.CDCDCD) CDCNT  ';
    $strSQL.= '                         FROM BQRYCND QRYC  ';
    $strSQL.= '                    LEFT JOIN BCNDDAT CDAT  ';
    $strSQL.= '                           ON QRYC.CNQRYN  = CDAT.CDQRYN  ';
    $strSQL.= '                          AND QRYC.CNMSEQ = CDAT.CDMSEQ  ';
    $strSQL.= '                          AND QRYC.CNSSEQ = CDAT.CDSSEQ  ';
    $strSQL.= '                        WHERE QRYC.CNQRYN  = ?  ';
    // $strSQL .= '                          AND QRYC.CNSTKB <>  \'3\' ';
    $strSQL.= '                     GROUP BY QRYC.CNQRYN ';
    $strSQL.= '                            , QRYC.CNMSEQ ';
    $strSQL.= '                            , QRYC.CNSSEQ ';
    $strSQL.= '                            , CDAT.CDQRYN ';
    $strSQL.= '                            , CDAT.CDMSEQ ';
    $strSQL.= '                            , CDAT.CDSSEQ ';
    $strSQL.= '                   )C ';
    $strSQL.= '               WHERE A.CNQRYN = B.CNQRYN ';
    $strSQL.= '                 AND A.CNQRYN = C.CDQRYN ';
    $strSQL.= '                 AND B.CNQRYN = C.CDQRYN ';
    $strSQL.= '                 AND A.CNMSEQ = C.CDMSEQ ';
    $strSQL.= '           ) GRPCND ';
    $strSQL.= '       WHERE QRYCND.CNQRYN =  GRPCND.CNQRYN ';
    $strSQL.= '         AND QRYCND.CNMSEQ =  GRPCND.CNMSEQ ';
    $strSQL.= '         AND QRYCND.CNSSEQ =  GRPCND.CDSSEQ ';
    $strSQL.= '       ORDER BY QRYCND.CNMSEQ ';
    $strSQL.= '              , QRYCND.CNSSEQ ';
    $params = array($QRYNM, $QRYNM, $QRYNM, $QRYNM, $QRYNM, $QRYNM, $QRYNM, $QRYNM);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = umEx($data, true);
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
function fnCreateCndDataEXCEL($bqrycnddat) {
    $cdcnt = '';
    $cnmseq = '';
    $cnsseq = '';
    $bqrycnddatRes = array();
    foreach ($bqrycnddat as $bqrycnddatdata) {
        if ($cnmseq != $bqrycnddatdata['CNMSEQ']) {
            $bqrycnd = array();
            $cnmseq = $bqrycnddatdata['CNMSEQ'];
            if ($bqrycnddatdata['CNSSEQ'] == 1) {
                $cnsseq = '';
                $bqrycnd['CNDIDX'] = $bqrycnddatdata['CNMSEQ'];
                $bqrycnd['SQRYAO'] = $bqrycnddatdata['CNAOKB'];
                $bqrycnd['CDATACNT'] = $bqrycnddatdata['CNSCNT'];
                $bqrycnd['CNDSDATA'] = array();
            }
        }
        if ($cnmseq == $bqrycnddatdata['CNMSEQ']) {
            if ($cnsseq != $bqrycnddatdata['CNSSEQ']) {
                $cnsseq = $bqrycnddatdata['CNSSEQ'];
                $bqrycndinfo = array();
                $bqrycndinfo['CNDDATA'] = array();
                $cdcnt = '';
                $cdcnt = $bqrycnddatdata['CDCNT'];
                $bqrycndinfo['CDATAIDX'] = $cnsseq;
                if ($bqrycnddatdata['CNSSEQ'] > 1) {
                    $bqrycndinfo['CNDANDOR'] = $bqrycnddatdata['CNAOKB'];
                } else {
                    $bqrycndinfo['CNDANDOR'] = '';
                }
                if ($bqrycnddatdata['CNFIL'] === '') {
                    $bqrycndinfo['CNDFLD'] = $bqrycnddatdata['CNFLDN'];
                } else {
                    $bqrycndinfo['CNDFLD'] = $bqrycnddatdata['CNFIL'] . '.' . $bqrycnddatdata['CNFLDN'];
                }
                $bqrycndinfo['CNFILID'] = $bqrycnddatdata['CNFILID'];
                $bqrycndinfo['CNDFHED'] = $bqrycnddatdata['D2HED'];
                $bqrycndinfo['CNDFTYP'] = $bqrycnddatdata['D2TYPE'];
                $bqrycndinfo['CNDFLEN'] = $bqrycnddatdata['D2LEN'];
                $bqrycndinfo['CNDFDEC'] = $bqrycnddatdata['D2DEC'];
                $bqrycndinfo['CNDKBN'] = $bqrycnddatdata['CNCKBN'];
                $bqrycndinfo['CNDTYP'] = $bqrycnddatdata['CNSTKB'];
                $bqrycndinfo['CNCANL'] = $bqrycnddatdata['CNCANL'];
                $bqrycndinfo['CNCANF'] = $bqrycnddatdata['CNCANF'];
                $bqrycndinfo['CNCANC'] = $bqrycnddatdata['CNCANC'];
                $bqrycndinfo['CNCANG'] = $bqrycnddatdata['CNCANG'];
                $bqrycndinfo['CNNAMG'] = $bqrycnddatdata['CNNAMG'];
                $bqrycndinfo['CNNAMC'] = $bqrycnddatdata['CNNAMC'];
                //ソート用カラム導入
                $bqrycndinfo['CNCAST'] = $bqrycnddatdata['CNCAST'];
                $bqrycndinfo['CNNAST'] = $bqrycnddatdata['CNNAST'];
                //カレンダ用カラム導入
                $bqrycndinfo['CNDFMT'] = $bqrycnddatdata['CNDFMT'];
                $bqrycndinfo['CNDSFL'] = $bqrycnddatdata['CNDSFL'];
                //日付のデータ用カラム導入
                $bqrycndinfo['CNDFIN'] = $bqrycnddatdata['CNDFIN'];
                $bqrycndinfo['CNDFPM'] = $bqrycnddatdata['CNDFPM'];
                $bqrycndinfo['CNDFDY'] = $bqrycnddatdata['CNDFDY'];
                $bqrycndinfo['CNDFFL'] = $bqrycnddatdata['CNDFFL'];
                $bqrycndinfo['CNDTIN'] = $bqrycnddatdata['CNDTIN'];
                $bqrycndinfo['CNDTPM'] = $bqrycnddatdata['CNDTPM'];
                $bqrycndinfo['CNDTDY'] = $bqrycnddatdata['CNDTDY'];
                $bqrycndinfo['CNDTFL'] = $bqrycnddatdata['CNDTFL'];
                //日付のデータ用カラム導入
                $bqrycndinfo['DHINFO'] = $bqrycnddatdata['DHINFO'];
                $bqrycndinfo['DHINFG'] = $bqrycnddatdata['DHINFG'];
                $bqrycndinfo['CDCNT'] = $bqrycnddatdata['CDCNT'];
            }
            if ($bqrycndinfo['CDATAIDX'] == $bqrycnddatdata['CNSSEQ']) {
                if ($bqrycndinfo['CDCNT'] == 0) {
                    $bqrycndinfo['CNDDATA'][] = '';
                    $bqrycnd['CNDSDATA'][] = $bqrycndinfo;
                } else {
                    if ((int)$bqrycnddatdata['CDBCNT'] > 0) {
                        $bqrycnddatdata['CDDAT'] = str_pad($bqrycnddatdata['CDDAT'], (int)$bqrycnddatdata['CDBCNT']);
                    }
                    $bqrycndinfo['CNDDATA'][] = $bqrycnddatdata['CDDAT'];
                    if ($bqrycndinfo['CDCNT'] == count($bqrycndinfo['CNDDATA'])) {
                        $bqrycnd['CNDSDATA'][] = $bqrycndinfo;
                    }
                }
            }
            if ($bqrycnd['CDATACNT'] == count($bqrycnd['CNDSDATA'])) {
                $bqrycnddatRes[] = $bqrycnd;
            }
        }
    }
    return $bqrycnddatRes;
}
/*
 *get data for excel setting
 **/
function cmGetDB2EINS($db2con, $D1NAME) {
    $data = array();
    $params = array();
    $params = array($D1NAME);
    $strSQL = ' select A.* from ';
    $strSQL.= ' DB2EINS as A WHERE A.EINAME = ? ';
    $strSQL.= ' ORDER BY A.EIROW ASC ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data, 'MAXEIROW' => getMaxEIROW($data, 'EIROW') //maximum eirow in DB2EINS
            );
        }
    }
    return $data;
}
/**get maximum eirow in DB2EINS table*/
function getMaxEIROW($data, $eirow) {
    $maxno = 0;
    foreach ($data as $key => $value) {
        $maxno = max(array($maxno, $value[$eirow]));
    }
    return $maxno;
}
/*EXCEL SETTING 挿入文字カウント**/
function cmGetCountSearchData($DB2ECON, $DB2EINSROWMAX, $CNDSDATAF) {
    $maxresult = 0;
    $ecflg = '';
    $maxresult+= $DB2EINSROWMAX;
    if (count($DB2ECON) > 0) {
        $maxresult+= $DB2ECON[0]['ECROWN'];
        $ecflg = $DB2ECON[0]['ECFLG'];
    }
    $maxresult+= ($ecflg === '1' && count($CNDSDATAF) > 0) ? count($CNDSDATAF) : 0;
    if ($DB2EINSROWMAX !== 0 && $ecflg === '1' && count($CNDSDATAF) > 0) {
        $maxresult+= 1;
    }
    return $maxresult;
}
/*ピボートヘッダーカウント*/
function cmGetHeaderCountDB2PCOL($db2con, $d1name, $pivotkey) {
    $pivotcount = 0;
    $rs = array();
    $strSQL = ' SELECT COUNT(*) AS TOTALCOUNT  FROM DB2PCOL';
    $strSQL.= ' WHERE WPNAME = ? AND WPPKEY = ? AND WPPFLG = 2 AND WPFHIDE <> \'1\'';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = array('result' => 'FAIL_SEL');
    } else {
        $params = array($d1name, $pivotkey);
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $pivotcount = $row['TOTALCOUNT'];
            }
            $rs = array('result' => true, 'TOTALCOUNT' => $pivotcount);
        }
    }
    return $rs;
}
/*制御CHECKBOXカウント　*/
function cmGetChkboxCountDB2COLT($db2con, $d1name) {
    $chkcount = 0;
    $params = array();
    $rs = array();
    $strSQL = ' SELECT (CAST (MAX(CASE WHEN DTSUM=\'\' THEN 0 WHEN DTSUM IS NULL THEN 0 ELSE DTSUM END) AS INTEGER)+';
    $strSQL.= 'CAST(MAX(CASE WHEN DTAVLG=\'\' THEN 0 WHEN DTAVLG IS NULL THEN 0 ELSE DTAVLG END) AS INTEGER)+';
    $strSQL.= 'CAST(MAX(CASE WHEN DTMINV=\'\' THEN 0 WHEN DTMINV IS NULL THEN 0 ELSE DTMINV END ) AS INTEGER)+';
    $strSQL.= 'CAST(MAX(CASE WHEN DTMAXV=\'\' THEN 0 WHEN DTMAXV IS NULL THEN 0 ELSE DTMAXV END ) AS INTEGER)+';
    $strSQL.= 'CAST(MAX(CASE WHEN DTCONT=\'\' THEN 0 WHEN DTCONT IS NULL THEN 0 ELSE DTCONT END ) AS INTEGER)) AS TOTALCOUNT';
    $strSQL.= ' FROM DB2COLT where DTNAME=?';
    $strSQL.= ' group by DTNAME ';
    //e_log('cmGetChkboxCountDB2COLT**************--------------->'.$strSQL);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = array('result' => 'FAIL_SEL');
    } else {
        $params = array($d1name);
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $chkcount = $row['TOTALCOUNT'];
            }
            $rs = array('result' => true, 'TOTALCOUNT' => $chkcount);
        }
    }
    return $rs;
}
/*制御INSERT TIME COUNT*/
/*function cmGetInsertCountSEIKYO($db2con, $str, $tmpfilename, $fdb2csv2, $search, $burstItmLst)
{
$inserttimecount = 0;
$rs              = array();
$params          = array();
//$strSQL          = 'SELECT  MAX(P.RN) as TOTALCOUNT FROM (SELECT  MAX ( B.rownum ) as RN  FROM  (SELECT  A.*,rownumber() over ( order by ' . $str . ') as rownum';
//$strSQL .= ' FROM  ' . SAVE_DB . '.' . $tmpfilename . ' AS A ';
$strSQL .= ' SELECT ';
$strSQL .= '     MAX(P.RN) as TOTALCOUNT ';
$strSQL .= ' FROM ';
$strSQL .= ' ( ';
$strSQL .= '     SELECT ';
$strSQL .= '          MAX ( A.rownum ) as RN ';
$strSQL .= '     FROM ';
$strSQL .=  SAVE_DB . '/' . $tmpfilename . ' AS A ';
//for mail
$WABFLG = '';
if ($burstItmLst != '') {
if ($burstItmLst['WABFLG'] !== '') {
$WABFLG = $burstItmLst['WABFLG'];
if ($WABFLG === '1') {
$WLDATA = $burstItmLst['WLDATA'];
}
}
}


if ($burstItmLst != '') {
if (cmMer($burstItmLst['WABAFLD']) !== '') {
$strSQL .= ' WHERE ';
if ($WABFLG === '1') {
$strburst = '';
foreach ($WLDATA AS $KEY => $VALUE) {
$WLDATA = $VALUE;
$strburst .= ' A.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' = ? OR';
array_push($params, cmMer($VALUE));
}
//e_log('burstItmlist:'.$str.'result'.rtrim($str, "OR"));
$strSQL .= rtrim($strburst, "OR");
} else if (cmMer($burstItmLst['WABAFR']) !== '' && cmMer($burstItmLst['WABATO']) !== '') {
$strSQL .= 'A.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' BETWEEN ? AND ?';
array_push($params, cmMer($burstItmLst['WABAFR']));
array_push($params, cmMer($burstItmLst['WABATO']));
} else if ($burstItmLst['WABAFR'] !== '') {
$strSQL .= 'A.' . cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']) . ' = ?';
array_push($params, cmMer($burstItmLst['WABAFR']));
}
}
}
//検索キーがある場合検索条件を作る
if ($search != '') {
if (count($fdb2csv2) > 0) {
$strSQL .= ' WHERE ';
foreach ($fdb2csv2 as $key => $value) {
$strSQL .= cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
array_push($params, '%' . $search . '%');

if ($key < (count($fdb2csv2) - 1)) {
$strSQL .= ' OR ';
}
}
}
}
$strSQL .= '     GROUP BY ';
$strSQL .= '          ROLLUP ( ' . $str . ') ';
$strSQL .= ' ) AS P ';
$stmt = db2_prepare($db2con, $strSQL);
if ($stmt === false) {
$rs = array(
'result' => 'FAIL_SEL'
);
} else {
$r = db2_execute($stmt, $params);
if ($r === false) {
$rs = array(
'result' => 'FAIL_SEL'
);
} else {
while ($row = db2_fetch_assoc($stmt)) {
$inserttimecount = $row['TOTALCOUNT'];
}
$rs = array(
'result' => true,
'TOTALCOUNT' => $inserttimecount
);
}
}
return $rs;
}*/
/*制御INSERT TIME COUNT*/
function cmGetInsertCountSEIKYO($db2con, $str, $tmpfilename, $fdb2csv2, $search, $burstItmLst) {
    $inserttimecount = 0;
    $rs = array();
    $params = array();
    $strSQL = ' SELECT ';
    $strSQL.= ' MAX(Q.rownum)-1 as TOTALCOUNT ';
    $strSQL.= ' FROM (SELECT ';
    $strSQL.= ' CASE WHEN P.RN IS NOT NULL THEN  rownumber() over (   ) END AS rownum ';
    $strSQL.= ' FROM (SELECT MAX ( B.ROWNUM ) as RN FROM ';
    $strSQL.= SAVE_DB . '/' . $tmpfilename . ' B ';
    $strSQL.= 'GROUP BY ';
    $strSQL.= 'ROLLUP ( ' . $str . ' )';
    $strSQL.= ') AS P) AS Q';
    error_log('【削除】cmGetInsertCountSEIKYO　SQL=>' . $strSQL);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = array('result' => 'FAIL_SEL');
        e_log('【削除】cmGetInsertCountSEIKYO　SQLｄｂエラー=>' . db2_stmt_errormsg());
    } else {
        $r = db2_execute($stmt);
        if ($r === false) {
            $rs = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $inserttimecount = $row['TOTALCOUNT'];
            }
            $rs = array('result' => true, 'TOTALCOUNT' => $inserttimecount);
        }
    }
    return $rs;
}
/*Formula SEIGYO [(Timecount*((level*chkcount))+chkcount]*/
function cmGetALLCOUNTSEIKYO($db2con, $d1name, $csvfilename, $rsMasterArr, $FDB2CSV2, $sSearch, $burstItmLst, $sumfFlg = false) {
  
    $inserttimecount = 0;
    $chkcount = 0;
    $rs = array();
    /*レベルカウント*/
    $levelFlg = '';
    $levelcount = 0;
    for ($i = 1;$i <= 6;$i++) {
        if (cmMer($rsMasterArr[0]['DCFLD' . $i]) !== '') {
            $LGroup = explode(",",cmMer($rsMasterArr[0]['DCFLD' . $i]));
            if (count($LGroup) >= 2) { //グループのレベル
                for($flevel = 0;$flevel < count($LGroup);$flevel++){
                    $splitRes=explode("-",$LGroup[$flevel]);
                    $splitResArr1[] = $splitRes[1] . '_' .$splitRes[0];
                }
                $splitResJoin=implode(",",$splitResArr1);
                $levelFlg.=$splitResJoin.',';
			}else{//普通
	            $levelFlg.= cmMer($rsMasterArr[0]['DCFLD' . $i]) . '_' . cmMer($rsMasterArr[0]['DCFILID' . $i]) . ',';
			}
            $levelcount++;
        }
    }
    $levelFlg = substr_replace($levelFlg, "", -1);
    /*制御INSERT TIME COUNT*/
  if ($rsMasterArr[0]['DCFLD1'] !== '' || $rsMasterArr[0]['DCFLD2'] !== '' || $rsMasterArr[0]['DCFLD3'] !== '' || $rsMasterArr[0]['DCFLD4'] !== '' || $rsMasterArr[0]['DCFLD5'] !== '' || $rsMasterArr[0]['DCFLD6'] !== '') {
    $rs = cmGetInsertCountSEIKYO($db2con, $levelFlg, $csvfilename, $FDB2CSV2, $sSearch, $burstItmLst);
    if ($rs['result'] === true) {
        $inserttimecount = (cmMer($rs['TOTALCOUNT']) === '') ? 0 : $rs['TOTALCOUNT'];
    }
  }
    if ($sumfFlg === false) {
        /*制御CHECKBOXカウント*/
        $rs = cmGetChkboxCountDB2COLT($db2con, $d1name);
        if ($rs['result'] === true) {
            $chkcount = (cmMer($rs['TOTALCOUNT']) === '') ? 0 : $rs['TOTALCOUNT'];
        }
        //e_log('********************$inserttimecount=>'.$inserttimecount.'$levelcount=>'.$levelcount.'$chkcount=>'.$chkcount);
        return ($inserttimecount === 0) ? 0 : ((($inserttimecount / $levelcount) * ($levelcount * $chkcount)) + $chkcount);
    } else {
        return ($inserttimecount === 0) ? 0 : (($inserttimecount / $levelcount) * ($levelcount));
    }
}
/*DB2ECON*/
function cmGetDB2ECON($db2con, $D1NAME) {
    $data = array();
    $params = array();
    $params = array($D1NAME);
    $strSQL = ' select A.* from ';
    $strSQL.= ' DB2ECON as A WHERE A.ECNAME = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/***
 *data type
*/
function setColFListRange($cndbn, $arrlist) {
    $colF = '';
    if ($cndbn === 'RANGE') {
        $lastval = array_pop($arrlist);
        $colF = ($arrlist) ? join('~', $arrlist) . "~" . $lastval : $lastval;
    } else if ($cndbn === 'LIST' || $cndbn === 'NLIST') {
        $lastval = array_pop($arrlist);
        $colF = ($arrlist) ? join('\n', $arrlist) . "\n" . $lastval : $lastval;
    } else {
        for ($k = 0;$k < count($arrlist);$k++) {
            $colF = $arrlist[$k];
        }
    }
    return $colF;
}
/**
 * ウエブ「かつ、」「もしくは、」
 */
function setAndOr($andor) // {{{
{
    $result = '';
    switch ($andor) {
        case 'AN':
            $result = 'かつ、';
        break;
        case 'OR':
            $result = 'もしくは、';
        break;
        default:
            $result = '';
        break;
    }
    return $result;
}
/**Check Config Constant Label*/
function getConfigLabel($CNDKBN) {
    $result = "";
    switch ($CNDKBN) {
        case 'EQ':
            $result = CND_EQ;
        break;
        case 'NE':
            $result = CND_NE;
        break;
        case 'GT':
            $result = CND_GT;
        break;
        case 'LT':
            $result = CND_LT;
        break;
        case 'GE':
            $result = CND_GE;
        break;
        case 'LE':
            $result = CND_LE;
        break;
        case 'LIKE':
            $result = CND_LIKE;
        break;
        case 'NLIKE':
            $result = CND_NLIKE;
        break;
        case 'LLIKE':
            $result = CND_LLIKE;
        break;
        case 'RLIKE':
            $result = CND_RLIKE;
        break;
        case 'NLIKE':
            $result = CND_NLIKE;
        break;
        case 'LNLIKE':
            $result = CND_LNLIKE;
        break;
        case 'RNLIKE':
            $result = CND_RNLIKE;
        break;
        case 'RANGE':
            $result = CND_RANGE;
        break;
        case 'LIST':
            $result = CND_LIST;
        break;
        case 'NLIST':
            $result = CND_NLIST;
        break;
        case 'IS':
            $result = CND_IS;
        break;
        default:
            $result = '';
        break;
    }
    return $result;
}
//DLOOPC =>ダウンロードのループカウント
function cmTotalLen($allcount) {
    $totalCount = (int)$allcount;
    $lenArr = array();
    $start = 0;
    while ($totalCount > DLOOPC) {
        $tot = array($start, DLOOPC);
        array_push($lenArr, $tot);
        $start = $start + DLOOPC;
        $totalCount = $totalCount - DLOOPC;
    }
    if ($totalCount > 0) {
        $tot = array($start, $totalCount);
        array_push($lenArr, $tot);
    }
    return $lenArr;
}
//DLOOPC =>ダウンロードのループカウント
function cmPivotTotalLen($allcount) {
    $totalCount = (int)$allcount;
    $lenArr = array();
    $len = 0;
    $start = 0;
    while ($totalCount > DLOOPC) {
        $start = $len + 1;
        $len = ($start - 1) + DLOOPC;
        $tot = array($start, $len);
        array_push($lenArr, $tot);
        $totalCount = $totalCount - DLOOPC;
    }
    if ($totalCount > 0) {
        $start = $len + 1;
        $len = ($start - 1) + DLOOPC;
        $tot = array($start, $len);
        array_push($lenArr, $tot);
    }
    return $lenArr;
}
/****
creating fake table when lost table while dowloading
****/
/*
function cmRecreatTable($db2con, $WEBF, $d1name, $data, $csvfilename)
{
$rtn = 0;
if ($WEBF === '1') {
if (count($data) === 0 || $data === '') {
$rs = fnBQRYCNDDAT($db2con, $d1name);
if ($rs['result'] !== true) {
$rtn = 1;
$msg = showMsg($rs['result']);
} else {
$data = $rs['data'];
}
$data = umEx($data);
if (count($data) > 0) {
$cnddatArr = fnCreateCndData($data);
}
} else {
$cnddatArr = $data; //配列がある場合は＝＞ｃｒｅａｔｅｄｏｗｎｌｏａｄ．ｐｈｐ
}
if ($rtn === 0) {
$resExeSql = runExecuteSQL($db2con, $d1name);
if ($resExeSql['RTN'] !== 0) {
$rtn = 1;
} else {
$qryData = $resExeSql;
$exeSQL  = array(
'EXESQL' => $qryData['STREXECSQL'],
'LOGSQL' => $qryData['LOG_SQL']
);
}
}
}
if ($rtn === 0) {
$rsCLChk = cmChkFDB2CSV1PG($db2con, $d1name);
if ($rsCLChk['result'] !== true) {
$rtn = 1;
$msg = $rsCLChk['result'];
} else {
if (count($rsCLChk['data']) > 0) {
$fdb2csv1pg = $rsCLChk['data'][0];
if ($fdb2csv1pg['DGBLIB'] !== '' || $fdb2csv1pg['DGBPGM'] !== '') {
$DGBFLG = true;
}
if ($fdb2csv1pg['DGALIB'] !== '' || $fdb2csv1pg['DGAPGM'] !== '') {
$DGAFLG = true;
}
}
}
} else {
//クエリー　＊クエリーの実行準備に失敗しました
$errMsg = showMsg('FAIL_FUNC', array(
'クエリーの実行準備'
)); //"クエリーの実行準備に失敗しました。";
}
if ($rtn === 0) {
if ($WEBF === '1') {
if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
$db2LibCon = cmDb2ConLib($qryData['LIBLIST']);
} else {
$_SESSION['PHPQUERY']['RDBNM'] = $qryData['SELRDB'];
$db2LibCon                     = cmDB2ConRDB($qryData['LIBLIST']);
}
}
/******************************************************/
//【Int】
/*
if ($WEBF !== '1') {
cmInt($db2con, $RTCD, $JSEQ, $d1name);
} else {
if ($licenseCl === true) {
if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
if ($DGBFLG === true || $DGAFLG === true) {
cmIntWC($db2LibCon, $RTCD, 'INT', $d1name);
}
} else {
$RTCD = ' ';
}
}
}
if ($RTCD === ' ' || is_null($RTCD)) {
// 【setCmd】
if ($WEBF !== '1') {
if ($licenseCl === true) {
if ($DGBFLG === true || $DGAFLG === true) {
setCmd($db2con, $d1name, $WEBF);
}
}
} else {
if ($licenseCl === true) {
if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
if ($DGBFLG === true || $DGAFLG === true) {
setCmd($db2LibCon, $d1name, $WEBF, $cnddatArr);
}
}
if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
if ($DGBFLG === true) {
cmIntWC($db2LibCon, $RTCD, 'BEF', $d1name);
}
} else {
//RDBのCL実行呼び出し;
if ($DGBFLG === true) {
$rsClExe = callClExecute($db2LibCon, $d1name, $data, 'BEF');
if ($rsClExe === 0) {
$RTCD = ' ';
} else {
$RTCD == '9';
}
}
}
}
}
}
if (($RTCD === ' ' || is_null($RTCD)) === false) {
$rtn    = 1;
$errMsg = 'CL実行の失敗';
}
if ($rtn === 0) {
// 【cmDoのRTCDが' 'の場合、WEBの場合は実行後CL】
//$dbname = makeRandStr(10);//filename
$dbname = $csvfilename;
if ($WEBF !== '1') {
cmDo($db2con, $RTCD, $JSEQ, $dbname, $d1name);
} else {
$resExec = execQry($db2LibCon, $qryData['STREXECTESTSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST']);
if ($resExec['RTN'] !== 0) {
$rtn    = 1;
$errMsg = showMsg($resExec['MSG']);
} else {
if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
$resCreateTbl = createTmpTable($db2LibCon, $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $dbname);
} else {
$resCreateTbl = createTmpTableRDB($db2con, $db2LibCon, $qryData['SELRDB'], $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $dbname);
}
if ($resCreateTbl['RTN'] !== 0) {
$rtn    = 1;
$errMsg = showMsg($resCreateTbl['MSG']);
} else {
if ($licenseCl === true) {
if ($DGAFLG === true) {
if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
cmIntWC($db2LibCon, $RTCD, 'AFT', $d1name);
} else {
//RDBのCL実行呼び出し;
$rsClExe = callClExecute($db2LibCon, $d1name, $data, 'AFT');
if ($rsClExe === 0) {
$RTCD = ' ';
} else {
$RTCD == '9';
}
}
}
}
if ($RTCD !== ' ' && !is_null($RTCD)) {
$rtn    = 1;
//クエリー　＊CLの事後処理に失敗しました。
$errMsg = "CLの事後処理に失敗しました。";
//e_log($errMsg);
}
}
}
}
}
}
if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
cmDb2Close($db2LibCon);
}
if ($rtn === 0) {
return $dbname;
} else {
return false;
}
}*/
function cmChkFDB2CSV1PG($db2con, $d1name) {
    $data = array();
    $strSQL = ' SELECT A.* ';
    $strSQL.= ' FROM FDB2CSV1PG A ';
    $strSQL.= ' WHERE A.DGNAME  = ? ';
    $params = array($d1name);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => umEx($data, false));
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * 権限取得(一行)
 *-------------------------------------------------------*
*/
function cmSelDB2AMST($db2con, $AMNAME) {
    $data = array();
    $params = array();
    $rs = true;
    $strSQL = ' SELECT A.* ';
    $strSQL.= ' FROM DB2AMST as A ';
    if ($AMNAME !== '') {
        $strSQL.= ' WHERE AMNAME = ? ';
        array_push($params, $AMNAME);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/*
1  ==> ユーザ作成
2  ==> グループ作成
3  ==> クエリー作成
4  ==> グループクエリー権限
5  ==> グループユーザー権限
6  ==> ダウンロード権限
7  ==> クエリー実行設定
8  ==> HTMLテンプレート
9  ==> EXCELテンプレート
10 ==> 詳細情報設定
11 ==> ダウンロード制限
12 ==> ユーザー補助
13 ==> 検索候補
14 ==> 制御レベル設定
15 ==> ピボット設定
16 ==> CSV形式
17 ==> スケジュール
18 ==> メール配信グループ
19 ==> CL連携
20 ==> メールサーバー
21 ==> 会社情報
22 ==> 利用停止時間
23 ==> 実行ログ
24 ==> HTML禁止タグ
25 ==> 区画
26 ==> 言語
27 ==> 権限
28 ==> システム設定
29 ==> イセンス情報
*/
function cmChkKenGen($db2con, $masterFlg, $AGNAME) {
    e_log('moe cmChkKenGen');
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $AGMNID = '';
    /*if ($masterFlg === '1') {
    $AGMNID = '1';
    } else if ($masterFlg === '2') {
    $AGMNID = '2';
    } else if ($masterFlg === '3') {
    $AGMNID = '3';
    } else if ($masterFlg === '4') {
    $AGMNID = '4';
    } else if ($masterFlg === '5') {
    $AGMNID = '5';
    } else if ($masterFlg === '6') {
    $AGMNID = '6';
    } else if ($masterFlg === '7') {
    $AGMNID = '7';
    } else if ($masterFlg === '8') {
    $AGMNID = '8';
    } else if ($masterFlg === '9') {
    $AGMNID = '9';
    } else if ($masterFlg === '10') {
    $AGMNID = '10';
    } else if ($masterFlg === '11') {
    $AGMNID = '11';
    } else if ($masterFlg === '12') {
    $AGMNID = '12';
    } else if ($masterFlg === '13') {
    $AGMNID = '13';
    } else if ($masterFlg === '14') {
    $AGMNID = '14';
    } else if ($masterFlg === '15') {
    $AGMNID = '15';
    } else if ($masterFlg === '16') {
    $AGMNID = '16';
    } else if ($masterFlg === '17') {
    $AGMNID = '17';
    } else if ($masterFlg === '18') {
    $AGMNID = '18';
    } else if ($masterFlg === '19') {
    $AGMNID = '19';
    } else if ($masterFlg === '20') {
    $AGMNID = '20';
    } else if ($masterFlg === '21') {
    $AGMNID = '21';
    } else if ($masterFlg === '22') {
    $AGMNID = '22';
    } else if ($masterFlg === '23') {
    $AGMNID = '23';
    } else if ($masterFlg === '24') {
    $AGMNID = '24';
    } else if ($masterFlg === '25') {
    $AGMNID = '25';
    } else if ($masterFlg === '26') {
    $AGMNID = '26';
    } else if ($masterFlg === '27') {
    $AGMNID = '27';
    } else if ($masterFlg === '28') {
    $AGMNID = '28';
    } else if ($masterFlg === '29') {
    $AGMNID = '29';
    } else if ($masterFlg === '31') {
    $AGMNID = '31';
    }*/
    if (isset($masterFlg)) {
        $AGMNID = $masterFlg;
    }
    $data = array();
    $params = array($AGNAME, $AGMNID);
    $rs = true;
    $strSQL = ' SELECT A.* ';
    $strSQL.= ' FROM DB2AGRT as A ';
    $strSQL.= ' WHERE AGNAME = ? AND AGMNID = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $data = array('result' => 'NOTEXIST');
            } else {
                $AGFLAG = cmMer($data[0]['AGFLAG']);
                if ($AGFLAG === '') {
                    $data = array('result' => 'NOTEXIST');
                } else {
                    $data = array('result' => true);
                }
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * マスター一覧取得(一行)
 *-------------------------------------------------------*
*/
function cmSelDB2AGRT($db2con, $AGNAME, $AGFLAG = '') {
    $data = array();
    $params = array($AGNAME);
    $rs = true;
    $strSQL = ' SELECT ';
    $strSQL.= ' A.AGNAME,A.AGMNID,A.AGFLAG  ';
    $strSQL.= ' FROM DB2AGRT AS A ';
    $strSQL.= ' WHERE A.AGNAME = ? ';
    if ($AGFLAG !== '') {
        $strSQL.= ' AND A.AGFLAG = ? ';
        array_push($params, $AGFLAG);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * 検索情報とヘルプ取得
 *-------------------------------------------------------*
*/
function fnGetFDB2CSV3HCND($db2con, $d1name) {
    $data = array();
    $params = array();
    $strSQL.= '    SELECT ';
    $strSQL.= '        A.*, ';
    $strSQL.= '        B.* , ';
    $strSQL.= '        C.DHINFO, ';
    $strSQL.= '        C.DHINFG ';
    $strSQL.= '    FROM ';
    $strSQL.= '        FDB2CSV3 AS A ';
    $strSQL.= '    JOIN FDB2CSV2 as B ';
    $strSQL.= '    ON A.D3NAME = B.D2NAME ';
    $strSQL.= '    AND A.D3FILID = B.D2FILID ';
    $strSQL.= '    AND A.D3FLD = B.D2FLD ';
    $strSQL.= '    LEFT JOIN DB2HCND AS C ';
    $strSQL.= '    ON A.D3NAME  = C.DHNAME ';
    $strSQL.= '    AND A.D3FILID  = C.DHFILID ';
    $strSQL.= '    AND A.D3JSEQ  = C.DHSSEQ ';
    $strSQL.= '    AND C.DHMSEQ = \'0\' ';
    $strSQL.= '    WHERE ';
    $strSQL.= '        A.D3NAME = ?  AND ';
    $strSQL.= '        A.D3USEL <> ? ';
    $strSQL.= '    ORDER BY ';
    $strSQL.= '        A.D3JSEQ ASC ';
    $params = array($d1name, '3');
    $stmt = db2_prepare($db2con, $strSQL);
    //e_log('SQL文：'.$strSQL.print_r($params,true));
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                if (cmMer($row['D2HED']) !== '') {
                    $row['D2HED'] = cmHsc($row['D2HED']);
                }
                if (cmMer($row['DHINFO']) !== '') {
                    $row['DHINFO'] = cmHsc($row['DHINFO']);
                }
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
// 5250のクエリー実行に制御データ取得の場合
function cmRecreateSeigyoTbl5250($db2con, $d1name, $filename, $sSearch = '', $burstItm = array()) {
    //
    e_log("Calling.....START");
    //
    $rtn = 0;
    $tmpTblNm = makeRandStr(10, true);
    $db2colmData = array();
    $rsDb2colm = cmGetDB2COLM($db2con, $d1name);
    if ($rsDb2colm['result'] !== true) {
        $rtn = 'FAIL_SEL';
        $msg = 'クエリー実行中失敗しました。' . db2_stmt_errormsg();
    } else {
        $db2colmData = $rsDb2colm['data'];
    }
    $seigyoKeyArr = array();
    if (count($db2colmData) > 0) {
        $db2colmData = $db2colmData[0];
        for ($lvlCnt = 1;$lvlCnt <= 6;$lvlCnt++) {
            if ($db2colmData['DCFLD' . $lvlCnt] !== '') {
                $seigyoFld = 'A.' . $db2colmData['DCFLD' . $lvlCnt] . '_' . $db2colmData['DCFILID' . $lvlCnt];
                $seigyoKeyArr[] = $seigyoFld;
            }
        }
    }
    $strRowNumCol = '';
    if (count($seigyoKeyArr) > 0) {
        $strRowNumCol = ' ROWNUMBER() OVER ( ';
        $strSeigyoSortColInfo = join(', ', $seigyoKeyArr);
        $strRowNumCol.= ' ORDER BY ' . ' ' . $strSeigyoSortColInfo;
        $strRowNumCol.= ' ) AS ROWNUM ';
        //e_log('制御カラム情報：'.$strRowNumCol);
        
    } else {
        $strRowNumCol = ' ROWNUMBER() OVER ( ';
        $strRowNumCol.= ' ) AS ROWNUM ';
    }
    $strSQL = ' SELECT A.* ';
    if ($strRowNumCol !== '') {
        $strSQL.= ' , ' . $strRowNumCol;
    }
    $strSQL.= ' FROM ';
    $strSQL.= SAVE_DB . '/' . $filename . ' A ';
    if ($sSearch !== '') {
        $fdb2csv2 = cmGetFDB2CSV2($db2con, $d1name,false,false,false);
        if ($fdb2csv2['result'] === true) {
            $fdb2csv2 = $fdb2csv2['data'];
            foreach ($fdb2csv2 as $key => $value) {
                $colnm = 'A.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                $addFlg = true;
                $byte = checkByte($sSearch);
                if ($byte[1] > 0) {
                    if ($value['D2TYPE'] !== 'O') {
                        $addFlg = false;
                    }
                }
                if ($addFlg) {
                    $filterSQLArr[].= $colnm . ' LIKE \'%' . $sSearch . '%\' ';
                }
                if (count($filterSQLArr) > 0) {
                    $filterSQL = join('OR ', $filterSQLArr);
                } else {
                    $filterSQL = '';
                }
            }
            //e_log('FILTERSQL：'.$filterSQL);
            if ($filterSQL !== '') {
                $strSQL.= ' WHERE ';
                $strSQL.= $filterSQL;
            }
        }
    }
    if (count($burstItm) > 0) {
        $strburstSQL = '';
        if (cmMer($burstItm['WABAFLD']) !== '') {
            $burstFld = 'A.' . cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']);
            $burstFldTyp = '';
            $fdb2csv2 = cmGetFDB2CSV2($db2con, $d1name,false,false,false);
            if ($fdb2csv2['result'] === true) {
                $fdb2csv2 = $fdb2csv2['data'];
                foreach ($fdb2csv2 as $key => $value) {
                    if ($burstFld === ($value['D2FLD'] . '_' . $value['D2FILID'])) {
                        $burstFldTyp = $value['D2TYPE'];
                        break;
                    }
                }
                if (($burstFldType === 'P') || ($burstFldType === 'S') || ($burstFldType === 'B')) {
                    $typFlg = false;
                } else {
                    $typFlg = true;
                }
                if ($burstItm['WABFLG'] == '1') {
                    $wldataList = array();
                    foreach ($burstItm['WLDATA'] AS $KEY => $VALUE) {
                        $wldataList[] = $VALUE;
                    }
                    if (count($wldataList) > 0) {
                        if ($typFlg) {
                            $burstItmParam = join('\' , \'', $wldataList);
                            $burstItmParam = '\'' . $burstItmParam . '\'';
                        } else {
                            $burstItmParam = join(' , ', $wldataList);
                        }
                    }
                    $strburstSQL.= $burstFld . ' IN ( ';
                    $strburstSQL.= $burstItmParam;
                    $strburstSQL.= ')';
                } else if (cmMer($burstItm['WABAFR']) !== '' && cmMer($burstItm['WABATO']) !== '') {
                    $strburstSQL.= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' BETWEEN ';
                    if ($typFlg) {
                        $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\'' . ' AND ' . '\'' . cmMer($burstItm['WABATO']) . '\' ';
                    } else {
                        $strburstSQL.= cmMer($burstItm['WABAFR']) . ' AND ' . cmMer($burstItm['WABATO']);
                    }
                } else if ($burstItm['WABAFR'] !== '') {
                    $strburstSQL.= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' = ';
                    if ($typFlg) {
                        $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\' ';
                    } else {
                        $strburstSQL.= cmMer($burstItm['WABAFR']);
                    }
                }
            }
            //e_log('バースト条件データ：'.$strburstSQL);
            if ($strburstSQL !== '') {
                $strSQL.= ' WHERE ';
                $strSQL.= $strburstSQL;
            }
        }
    }
    e_log("strSQL >>>" . $strSQL);
    if ($rtn === 0) {
        $strCreate = '';
        $strCreate.= ' CREATE TABLE';
        $strCreate.= ' ' . SAVE_DB . '/' . $tmpTblNm . ' AS';
        $strCreate.= ' (' . $strSQL . ' )';
        $strCreate.= ' WITH DATA ';
        e_log("strCreate >>>" . $strCreate);
        // 複数の空白消す
        //$strCreate = spaceTrim($strCreate);
        if ($rtn === 0) {
            // 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
            if (strlen($strCreate) > 32767) {
                $rtn = 1;
                $msg = showMsg('FAIL_BASE_LENLIMIT'); //'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
                
            }
        }
        $stmt = db2_prepare($db2con, $strCreate);
        if ($stmt === false) {
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行準備失敗しました。' . db2_stmt_errormsg();
            e_log('制御クエリー実行準備失敗しました:' . $msg);
            e_log('制御取得SQL文:' . $strCreate);
        } else {
            $r = db2_execute($stmt, array());
            if ($r === false) {
                $rtn = 'FAIL_SEL';
                $msg = 'クエリー実行中失敗しました。' . db2_stmt_errormsg();
                e_log('制御クエリー実行準備失敗しました:' . $msg);
                e_log('制御取得SQL文:' . $strCreate);
            } else {
                $msg = 'ワークテーブル作成完了' . $tmpTblNm;
            }
        }
    }
    if ($rtn !== 0) {
        $tmpTblNm = '';
    } else {
        e_log('5250ワークテーブル作成成功。' . $msg);
    }
    //
    e_log("Calling.....END");
    //
    return $tmpTblNm;
}
// 5250のクエリー実行にピボットデータ取得の場合
function cmRecreateRnPivotTbl5250($db2con, $d1name, $pivNm, $filename, $burstItm = array()) {
    //e_log('ピボットのためワークテーブル作成：'.$d1name.'2'.$pivNm.'3'.$filename.'4'.print_r($burstItm,true));
    $rtn = 0;
    $tmpTblNm = makeRandStr(10, true);
    $db2pcolData = getDB2PCOLData($db2con, $d1name, $pivNm);
    if ($db2pcolData[result] === true) {
        if (count($db2pcolData['data']) > 0) {
            $db2pcolData = $db2pcolData['data'];
            //e_log('ピボットキーデータ：'.print_r($db2pcolData,true));
            foreach ($db2pcolData as $key => $value) {
                if ($value['D2TYPE'] !== '') {
                    $pivfldnm = $value['PIVFLD'] . ' ' . $value['PIVFLDSORT'];
                    if ($sumKeySQL === '') {
                        $pivfldnm = 'SQ2.' . $pivfldnm;
                    }
                    if ($value['WPPFLG'] == 1) {
                        $xKeyArr[] = $pivfldnm;
                    } else if ($value['WPPFLG'] == 2) {
                        $yKeyArr[] = $pivfldnm;
                    }
                }
            }
            //e_log('ピボットXキー配列：'.print_r($xKeyArr,true));
            //e_log('ピボットYキー配列：'.print_r($yKeyArr,true));
            
        }
    }
    $strRowNumCol = ' ROWNUMBER() OVER ( ';
    $strRowNumCol.= ' ) AS RN ';
    // 縦軸のデータ
    $strXRowNumCol = ' ROWNUMBER() OVER ( ';
    if (count($xKeyArr) > 0) {
        $xRNSQL = join(" , ", $xKeyArr);
        $strXRowNumCol.= ' ORDER BY ' . ' ' . $xRNSQL;
    }
    $strXRowNumCol.= ' ) AS XROWNUM ';
    // 横軸のデータ
    $strYRowNumCol = ' ROWNUMBER() OVER ( ';
    if (count($yKeyArr) > 0) {
        $yRNSQL = join(' , ', $yKeyArr);
        $strYRowNumCol.= ' ORDER BY ' . ' ' . $yRNSQL;
    }
    $strYRowNumCol.= ' ) AS YROWNUM ';
    $strSQL = ' SELECT A.* ';
    $strSQL.= ' , ' . $strRowNumCol;
    $strSQL.= ' , ' . $strXRowNumCol;
    $strSQL.= ' , ' . $strYRowNumCol;
    $strSQL.= ' FROM ';
    $strSQL.= SAVE_DB . '/' . $filename . ' A ';
    if (count($burstItm) > 0) {
        $strburstSQL = '';
        if (cmMer($burstItm['WABAFLD']) !== '') {
            $burstFld = 'A.' . cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']);
            $burstFldTyp = '';
            $fdb2csv2 = cmGetFDB2CSV2($db2con, $d1name,false,false,false);
            if ($fdb2csv2['result'] === true) {
                $fdb2csv2 = $fdb2csv2['data'];
                foreach ($fdb2csv2 as $key => $value) {
                    if ($burstFld === ($value['D2FLD'] . '_' . $value['D2FILID'])) {
                        $burstFldTyp = $value['D2TYPE'];
                        break;
                    }
                }
                if (($burstFldType === 'P') || ($burstFldType === 'S') || ($burstFldType === 'B')) {
                    $typFlg = false;
                } else {
                    $typFlg = true;
                }
                if ($burstItm['WABFLG'] == '1') {
                    $wldataList = array();
                    foreach ($burstItm['WLDATA'] AS $KEY => $VALUE) {
                        $wldataList[] = $VALUE;
                    }
                    if (count($wldataList) > 0) {
                        if ($typFlg) {
                            $burstItmParam = join('\' , \'', $wldataList);
                            $burstItmParam = '\'' . $burstItmParam . '\'';
                        } else {
                            $burstItmParam = join(' , ', $wldataList);
                        }
                    }
                    $strburstSQL.= $burstFld . ' IN ( ';
                    $strburstSQL.= $burstItmParam;
                    $strburstSQL.= ')';
                } else if (cmMer($burstItm['WABAFR']) !== '' && cmMer($burstItm['WABATO']) !== '') {
                    $strburstSQL.= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' BETWEEN ';
                    if ($typFlg) {
                        $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\'' . ' AND ' . '\'' . cmMer($burstItm['WABATO']) . '\' ';
                    } else {
                        $strburstSQL.= cmMer($burstItm['WABAFR']) . ' AND ' . cmMer($burstItm['WABATO']);
                    }
                } else if ($burstItm['WABAFR'] !== '') {
                    $strburstSQL.= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' = ';
                    if ($typFlg) {
                        $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\' ';
                    } else {
                        $strburstSQL.= cmMer($burstItm['WABAFR']);
                    }
                }
            }
            //e_log('バースト条件データ：'.$strburstSQL);
            if ($strburstSQL !== '') {
                $strSQL.= ' WHERE ';
                $strSQL.= $strburstSQL;
            }
        }
    }
    if ($rtn === 0) {
        $strCreate = '';
        $strCreate.= ' CREATE TABLE';
        $strCreate.= ' ' . SAVE_DB . '/' . $tmpTblNm . ' AS';
        $strCreate.= ' (' . $strSQL . ' )';
        $strCreate.= ' WITH DATA ';
        // 複数の空白消す
        //$strCreate = spaceTrim($strCreate);
        if ($rtn === 0) {
            // 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
            if (strlen($strCreate) > 32767) {
                $rtn = 1;
                $msg = showMsg('FAIL_BASE_LENLIMIT'); //'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
                
            }
        }
        //e_log('create SQL:'.$strCreate);
        $stmt = db2_prepare($db2con, $strCreate);
        if ($stmt === false) {
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行準備失敗しました。' . db2_stmt_errormsg();
        } else {
            //e_log('create table:SQL'.$strCreate.'params'.print_r($params,true));
            $r = db2_execute($stmt, array());
            if ($r === false) {
                $rtn = 'FAIL_SEL';
                $msg = 'クエリー実行中失敗しました。' . db2_stmt_errormsg();
            } else {
                $msg = 'ワークテーブル作成完了' . $tmpTblNm;
            }
        }
    }
    if ($rtn !== 0) {
        $tmpTblNm = '';
    } else {
        e_log('5250ワークテーブル作成失敗。' . $msg);
    }
    return $tmpTblNm;
}
//制御チェックと制御データ取得
function cmGetSeigyoData($db2con, $d1name) {
    $rtn = 0;
    $seigyoFlg = 0;
    $rsmaster = array();
    $rsshukei = array();
    // htmlテンプレート
    if ($rtn === 0) {
        $rshtml = cmGetDB2HTMLT($db2con, $d1name);
        if ($rshtml['result'] !== true) {
            $msg = showMsg($rshtml['result']);
            $rtn = 1;
        } else {
            if (count($rshtml['data']) == 0) {
                //制御チェック
                //集計するフィールド情報取得
                $rsmaster = cmGetDB2COLM($db2con, $d1name);
                if ($rsmaster['result'] !== true) {
                    $msg = showMsg($rsmaster['result']);
                    $rtn = 1;
                } else {
                    $rsMasterArr = umEx($rsmaster['data']);
                    //集計するメソッド情報取得
                    $rsshukei = cmGetDB2COLTTYPE($db2con, $d1name);
                    if ($rsshukei['result'] !== true) {
                        $msg = showMsg($rsshukei['result']);
                        $rtn = 1;
                    } else {
                        $rsshukeiArr = umEx($rsshukei['data']);
                    }
                    if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                        $seigyoFlg = 1;
                    }
                }
            }
        }
    }
    $rtnArr = array(RESULT => $rtn, MSG => $msg, SEIGYOFLG => $seigyoFlg, RSMASTER => $rsMasterArr, RSSHUKEI => $rsshukeiArr);
    return $rtnArr;
}
//DB2HTMLT取得
function cmGetDB2HTMLT($db2con, $D1NAME) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT * ';
    $strSQL.= ' FROM DB2HTMLT ';
    $strSQL.= ' WHERE HTMLTD = ?';
    $params = array($D1NAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => umEx($data));
        }
    }
    return $data;
}
/*
 *  ピボットキー取得
*/
function getDB2PCOLData($db2con, $QRYNM, $PIVNM) {
    $params = array();
    $data = array();
    $strSQL = '';
    $strSQL.= '   SELECT ';
    $strSQL.= '       CONCAT(CONCAT(TRIM(PKEYLST.WPFLD),\'_\'),TRIM(PKEYLST.WPFILID)) AS PIVFLD, ';
    $strSQL.= '       PKEYLST.WPFILID, ';
    $strSQL.= '       PKEYLST.WPFLD, ';
    $strSQL.= '       PKEYLST.WPPFLG, ';
    $strSQL.= '       PKEYLST.WPSEQN, ';
    $strSQL.= '       PKEYLST.WPFHIDE, ';
    $strSQL.= '       PKEYLST.WPSORT, ';
    $strSQL.= '       ( ';
    $strSQL.= '           CASE ';
    $strSQL.= '               WHEN PKEYLST.WPSORT = 0  THEN \'DESC\' ';
    $strSQL.= '               ELSE \'ASC\' END ';
    $strSQL.= '       ) AS PIVFLDSORT, ';
    $strSQL.= '       PKEYLST.WPSUMG, ';
    $strSQL.= '       ( ';
    $strSQL.= '           CASE ';
    $strSQL.= '                WHEN COLLST.D2TYPE IS NULL  THEN \'\' ';
    $strSQL.= '                ELSE COLLST.D2TYPE END ';
    $strSQL.= '       ) AS D2TYPE, ';
    $strSQL.= '       COLLST.D2TYPE, ';
    $strSQL.= '       COLLST.D2LEN, ';
    $strSQL.= '       COLLST.D2DEC, ';
    $strSQL.= '       ( ';
    $strSQL.= '           CASE ';
    $strSQL.= '                WHEN SUMFLD.SFGMES  IS NULL  THEN \'\' ';
    $strSQL.= '                ELSE SUMFLD.SFGMES  END ';
    $strSQL.= '       ) AS PIVFLDSUM ';
    $strSQL.= '   FROM ';
    $strSQL.= '       ( ';
    $strSQL.= '           SELECT ';
    $strSQL.= '               WPPFLG, ';
    $strSQL.= '               WPSEQN, ';
    $strSQL.= '               WPFILID, ';
    $strSQL.= '               WPFLD, ';
    $strSQL.= '               WPFHIDE, ';
    $strSQL.= '               WPSORT, ';
    $strSQL.= '               WPSUMG ';
    $strSQL.= '           FROM ';
    $strSQL.= '               DB2PCOL ';
    $strSQL.= '           WHERE ';
    $strSQL.= '               WPNAME = ? AND ';
    $strSQL.= '               WPPKEY = ? AND ';
    $strSQL.= '               WPPFLG <> 3 ';
    $strSQL.= '           ORDER BY ';
    $strSQL.= '               WPPFLG , ';
    $strSQL.= '               WPSEQN ';
    $strSQL.= '       )PKEYLST ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '       ( ';
    $strSQL.= '           SELECT ';
    $strSQL.= '               D2NAME, ';
    $strSQL.= '               D2FILID, ';
    $strSQL.= '               D2FLD, ';
    $strSQL.= '               D2TYPE, ';
    $strSQL.= '               D2LEN, ';
    $strSQL.= '               D2DEC ';
    $strSQL.= '           FROM ';
    $strSQL.= '               FDB2CSV2 ';
    $strSQL.= '           WHERE ';
    $strSQL.= '               D2NAME = ? ';
    $strSQL.= '           UNION ALL ';
    $strSQL.= '           SELECT ';
    $strSQL.= '               D5NAME AS D2NAME, ';
    $strSQL.= '               9999 AS D2FILID, ';
    $strSQL.= '               D5FLD AS D2FLD, ';
    $strSQL.= '               D5TYPE AS D2TYPE, ';
    $strSQL.= '               D5LEN AS D2LEN, ';
    $strSQL.= '               D5DEC AS D2DEC ';
    $strSQL.= '           FROM ';
    $strSQL.= '               FDB2CSV5 ';
    $strSQL.= '           WHERE ';
    $strSQL.= '               D5NAME = ? ';
    $strSQL.= '       ) COLLST ';
    $strSQL.= '   ON PKEYLST.WPFILID = COLLST.D2FILID ';
    $strSQL.= '   AND PKEYLST.WPFLD = COLLST.D2FLD ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '        ( ';
    $strSQL.= '        SELECT ';
    $strSQL.= '            SFFILID, ';
    $strSQL.= '            SFFLDNM, ';
    $strSQL.= '            SFGMES ';
    $strSQL.= '        FROM ';
    $strSQL.= '            BSUMFLD ';
    $strSQL.= '        WHERE ';
    $strSQL.= '            SFQRYN = ? ';
    $strSQL.= '        ) SUMFLD ';
    $strSQL.= '    ON PKEYLST.WPFILID = SUMFLD.SFFILID ';
    $strSQL.= '    AND PKEYLST.WPFLD = SUMFLD.SFFLDNM ';
    $params = array($QRYNM, $PIVNM, $QRYNM, $QRYNM, $QRYNM);
    //e_log('ピボットカラム情報データ取得：'.$strSQL.'parameter:'.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL', 'errcd' => 'getDB2PCOLData:' . db2_stmt_errormsg());
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL', 'errcd' => 'getDB2PCOLData:' . db2_stmt_errormsg());
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => umEx($data));
        }
    }
    return $data;
}
/***
 *グラフを削除した後、ブックマーク削除
 *クエリー削除した後、ブックマーク削除
 *グラフのため$flg=1
 *グループのクエリーのため$flg=2
 **/
function cmDelDB2BMK($db2con, $GID = '', $QRY = '', $flg) {
    $USR_ID = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
    $data = array();
    $params = array();
    $rs = true;
    $strSQL = ' DELETE FROM DB2BMK AS A WHERE ';
    $strSQL.= ' EXISTS( SELECT * FROM DB2DSB AS B WHERE A.BMKDSBID=B.DSBID AND B.DSBUID=?) ';
    $params = array($USR_ID);
    if ($flg === 1) { //グラフ
        $strSQL.= '  AND BMKGID = ? AND BMKQID= ? ';
        array_push($params, $GID, $QRY);
    } else if ($flg === 2) { //グループのクエリー
        $strSQL.= '  AND BMKQID= ? ';
        array_push($params, $QRY);
    }
    e_log('klyhtest******************->' . $strSQL . print_r($params, true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_DEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            e_log('error:' . db2_stmt_errormsg());
            $rs = 'FAIL_DEL';
        }
    }
    $data = array('result' => $rs);
    return $data;
}
/*
 *-------------------------------------------------------*
 * ダッシュボードが存在チェック いたらtrue
 *-------------------------------------------------------*
*/
function cmCheckDB2DSB($db2con, $DSBUID, $DSBID) {
    $data = array();
    $rs = true;
    $strSQL = ' SELECT * ';
    $strSQL.= ' FROM  DB2DSB AS A ';
    $strSQL.= ' WHERE A.DSBUID = ? ';
    $strSQL.= ' AND A.DSBID = ? ';
    $params = array($DSBUID, $DSBID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            e_log('cmCheckDB2DSB:' . db2_stmt_errormsg());
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $rs = 'NOTEXIST_UPD';
            }
        }
    }
    return $rs;
}
/*
 *ダッシュボード一つのため、グラフは20個以上ならエラーチェック
*/
function cmCountDB2BMK($db2con, $DSBID) {
    $TOTALCOUNT = 0;
    $rs = true;
    $strSQL = ' SELECT COUNT(*) AS TOTALCOUNT ';
    $strSQL.= ' FROM  DB2BMK AS A ';
    $strSQL.= ' WHERE A.BMKDSBID = ? ';
    $params = array($DSBID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $TOTALCOUNT = $row['TOTALCOUNT'];
            }
            if ($TOTALCOUNT > 20) {
                $rs = 'FAIL_DMAXLEN';
            }
        }
    }
    return array('result' => $rs, 'TOTALCOUNT' => $TOTALCOUNT);
}
/**
 *
 *CHECK DB2DSB
 */
function cmCheckDSBNAME_DB2DSB($db2con, $NEWDASHNAME, $USR_ID) {
    $rs = true;
    $DSBID = 0;
    $strSQL = '';
    $strSQL = ' SELECT COUNT(*) AS TOTALCOUNT';
    $strSQL.= ' FROM DB2DSB WHERE DSBUID = ? AND DSBNAME =?';
    $params = array($USR_ID, $NEWDASHNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt1 === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $DSBID = $row['TOTALCOUNT'];
            }
            if ($DSBID > 0) {
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}
/**
 *BMKTYPEを考えないのカウント
 *ブックマークカウント
 */
function cmGETDB2BMKCOUNT($db2con, $BMKQID, $BMKGID, $USR_ID) {
    $totalcount = 0;
    $data = array();
    $params = array();
    $rs = true;
    $strSQL = '';
    $strSQL.= 'SELECT COUNT(*) AS TOTALCOUNT FROM DB2DSB AS A ';
    $strSQL.= 'INNER JOIN DB2BMK AS B ';
    $strSQL.= 'ON A.DSBID=B.BMKDSBID AND A.DSBUID=?';
    $strSQL.= 'WHERE B.BMKQID=? AND B.BMKGID=?';
    $params = array($USR_ID, $BMKQID, $BMKGID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $totalcount = $row['TOTALCOUNT'];
            }
        }
    }
    $data = array('result' => $rs, 'TOTALCOUNT' => $totalcount);
    return $data;
}
/*
 *-------------------------------------------------------*
 * クエリーをグループからもらう
 *-------------------------------------------------------*
*/
function cmGETQRY_DB2WGDF($db2con, $WGGID) {
    $data = array();
    $rs = true;
    $msg = '';
    $strSQL = ' SELECT WGNAME FROM DB2WGDF ';
    $strSQL.= ' WHERE ';
    $strSQL.= ' WGGID = ? ';
    $params = array($WGGID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    $data = array('result' => $rs, 'data' => $data);
    return $data;
}
/*
 *グループとユーザーカウント
*/
function cmCountDB2WGDF_DB2WUGR($db2con, $QRY) {
    $USR_ID = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
    $data = array();
    $params = array();
    $rs = true;
    $TOTALCOUNT = 0;
    $msg = '';
    $strSQL = 'SELECT COUNT(*) AS TOTALCOUNT FROM DB2WUGR AS A ';
    $strSQL.= 'INNER JOIN DB2WGDF AS B ON A.WUGID=B.WGGID ';
    $strSQL.= 'WHERE WUUID=? AND WGNAME=?';
    $params = array($USR_ID, $QRY);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            e_log('error:' . db2_stmt_errormsg());
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $TOTALCOUNT = $row['TOTALCOUNT'];
            }
        }
    }
    $data = array('result' => $rs, 'TOTALCOUNT' => $TOTALCOUNT);
    return $data;
}
function cmCalendarDataParam($currentDate, $seq_data) {
    $calFlg = false;
    $data = array();
    for ($idx = 0;$idx < count($seq_data);$idx++) {
        $data = $seq_data[$idx];
        if ($data['D3DFMT'] !== '' && $data['D3DFMT'] !== null && $data['D3DSCH'] === '1') {
            $fmt = getFmt($data['D3DFMT'], $data['D3DSFL']);
            $D3DAT = '';
            $S_data = $data['D3DAT'];
            $type = $data['D3TYPE'];
            if ($type === 'P' || $type === 'S' || $type === 'B') {
                $S_data = explode(" ", trim($S_data));
                $S_data = array_values(array_filter($S_data, 'strlen'));
            } else {
                $S_data = makeParamArr(trim($S_data));
            }
            //  if (count($S_data) > 0) {
            $calFlg = true;
            $data0 = '';
            $data1 = '';
            if (count($S_data) === 1) {
                $S_data[1] = '';
            }
            if ($data['D3CND'] === "RANGE") {
                $data0 = dateFieldFormatCondition($fmt, $currentDate, $data['D3DFIN'], $data['D3DFPM'], $data['D3DFDY'], $data['D3DFFL'], $S_data[0]);
                if ($data0 !== '') {
                    if ($type === 'P' || $type === 'S' || $type === 'B') {
                        $D3DAT = $data0;
                    } else {
                        $D3DAT = "'" . $data0 . "'";
                    }
                } else {
                    $D3DAT = $S_data[0];
                }
                $data1 = dateFieldFormatCondition($fmt, $currentDate, $data['D3DTIN'], $data['D3DTPM'], $data['D3DTDY'], $data['D3DTFL'], $S_data[1]);
                if ($data1 !== '') {
                    if ($type === 'P' || $type === 'S' || $type === 'B') {
                        $D3DAT.= " " . $data1;
                    } else {
                        $D3DAT.= " '" . $data1 . "'";
                    }
                }
                $seq_data[$idx]['D3DAT'] = $D3DAT;
            } else if ($data['D3CND'] === "LIST" || $data['D3CND'] === "NLIST") {
                if ($data['D3DFIN'] === '0') {
                    for ($ix = 0;$ix < count($S_data);$ix++) {
                        $cdata = dateFieldFormatCondition($fmt, $currentDate, $data['D3DFIN'], $data['D3DFPM'], $data['D3DFDY'], $data['D3DFFL'], $S_data[$ix]);
                        if ($type === 'P' || $type === 'S' || $type === 'B') {
                            $D3DAT.= $cdata;
                        } else {
                            $D3DAT.= "'" . $cdata . "'";
                        }
                        if ($ix !== (count($S_data) - 1)) {
                            $D3DAT.= " ";
                        }
                    }
                    $seq_data[$idx]['D3DAT'] = $D3DAT;
                }
            } else {
                if ($data['D3DFIN'] === '0') {
                    $cdata = dateFieldFormatCondition($fmt, $currentDate, $data['D3DFIN'], $data['D3DFPM'], $data['D3DFDY'], $data['D3DFFL'], $S_data[0]);
                    if ($type === 'P' || $type === 'S' || $type === 'B') {
                        $D3DAT = $cdata;
                    } else {
                        $D3DAT = "'" . $cdata . "'";
                    }
                    $seq_data[$idx]['D3DAT'] = $D3DAT;
                }
            }
        }
    }
    return $seq_data;
}
function cmCalendarDataParamWeb($currentDate, $webData) {
    $calFlg = false;
    $data = array();
    for ($idx = 0;$idx < count($webData);$idx++) {
        $cdnsdata = $webData[$idx]['CNDSDATA'];
        for ($idxd = 0;$idxd < count($cdnsdata);$idxd++) {
            $cdata = $cdnsdata[$idxd];
            if ($cdata['CNDFMT'] !== '' && $cdata['CNDFMT'] !== null && $cdata['CNDSCH'] === '1') {
                $calFlg = true;
                $fmt = getFmt($cdata['CNDFMT'], $cdata['CNDSFL']);
                if ($cdata['CNDKBN'] === "RANGE") {
                    if (count($webData[$idx]['CNDSDATA'][$idxd]['CNDDATA']) < 2 && ($cdata['CNDFIN'] === '0' || $cdata['CNDTIN'] === '0')) {
                        $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][0] = '';
                        $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][1] = '';
                    }
                    $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][0] = dateFieldFormatCondition($fmt, $currentDate, $cdata['CNDFIN'], $cdata['CNDFPM'], $cdata['CNDFDY'], $cdata['CNDFFL'], $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][0]);
                    $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][1] = dateFieldFormatCondition($fmt, $currentDate, $cdata['CNDTIN'], $cdata['CNDTPM'], $cdata['CNDTDY'], $cdata['CNDTFL'], $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][1]);
                } else if ($cdata['CNDKBN'] === "LIST" || $cdata['CNDKBN'] === "NLIST") {
                    if ($cdata['CNDFIN'] === '0') {
                        for ($idL = 0;$idL < count($cdata['CNDDATA']);$idL++) {
                            $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][$idL] = dateFieldFormatCondition($fmt, $currentDate, $cdata['CNDFIN'], $cdata['CNDFPM'], $cdata['CNDFDY'], $cdata['CNDFFL'], $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][$idL]);
                        }
                    }
                } else {
                    $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][0] = dateFieldFormatCondition($fmt, $currentDate, $cdata['CNDFIN'], $cdata['CNDFPM'], $cdata['CNDFDY'], $cdata['CNDFFL'], $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][0]);
                }
            }
        }
    } //for idx end
    return $webData;
}
function cmCalendarDataParamSQL($currentDate, $webData) {
    //e_log('パラメータデータ：'.print_r($webData,true));
    $calFlg = false;
    $data = array();
    $cdnsdata = $webData;
    for ($idxd = 0;$idxd < count($cdnsdata);$idxd++) {
        $cdata = $cdnsdata[$idxd];
        if ($cdata['CNDFMT'] !== '' && $cdata['CNDFMT'] !== null && $cdata['CNDSCH'] === '1') {
            $calFlg = true;
            $fmt = getFmt($cdata['CNDFMT'], $cdata['CNDSFL']);
            $CNDAT = dateFieldFormatCondition($fmt, $currentDate, $cdata['CNDFIN'], $cdata['CNDFPM'], $cdata['CNDFDY'], $cdata['CNDFFL'], $cdata['CNDDAT']);
            $webData[$idxd]['CNDDAT'] = $CNDAT;
        }
    }
    //for idx end
    return $webData;
}
function getFmt($FMT, $SFL) {
    $fmt = '';
    switch ($FMT) {
        case '1':
            $fmt = ($SFL === '') ? 'Y/m/d' : 'Ymd';
        break;
        case '2':
            $fmt = ($SFL === '') ? 'Y/m' : 'Ym';
        break;
        case '3':
            $fmt = ($SFL === '') ? 'y/m/d' : 'ymd';
        break;
        default:
            $fmt = 'Y/m/d';
    }
    return $fmt;
}
function dateFieldFormatCondition($fmt, $currentDate, $IN, $PM, $DY, $FL, $DATA) {
    if ($IN === '0') {
        $newdate = $currentDate; //20161020
        $tmpdate = '';
        $DY = (int)$DY;
        if ($PM !== '' && $DY > 0 && $FL !== '') {
            $tmpdate = strtotime($newdate);
            $cur_date = date("d", $tmpdate);
            $cur_month = date("m", $tmpdate);
            $cur_year = date("Y", $tmpdate);
            $g_lastDate = $cur_year . '' . $cur_month . '' . $cur_date;
            $lastdate = substr($g_lastDate, -2);
            $newdate = new DateTime();
            switch ($FL) {
                case "D":
                    if ($PM === '1') {
                        $cur_date+= $DY;
                        $newdate->setDate($cur_year, $cur_month, $cur_date);
                    } else {
                        $cur_date-= $DY;
                        $newdate->setDate($cur_year, $cur_month, $cur_date);
                    }
                break;
                case "M":
                    if ($lastdate === $cur_date) { //最後の日付をチェックしてから次の最後の日付を取る
                        if ($PM === '1') {
                            $cur_month+= $DY;
                            if ($cur_month < 10) {
                                $cur_month = '0' . $cur_month;
                            }
                            $g_lastDate = $cur_year . $cur_month . '01';
                            $g_last = date("Ymt", strtotime($g_lastDate));
                            $nextlastdate = substr($g_last, -2);
                        } else {
                            $cur_month-= $DY;
                            if ($cur_month < 10) {
                                $cur_month = '0' . $cur_month;
                            }
                            $g_lastDate = $cur_year . $cur_month . '01';
                            $g_last = date("Ymt", strtotime($g_lastDate));
                            $nextlastdate = substr($g_last, -2);
                        }
                        if ($nextlastdate <= $cur_date) {
                            $cur_date = $nextlastdate;
                        }
                    } else {
                        if ($PM === '1') {
                            $cur_month+= $DY;
                        } else {
                            $cur_month-= $DY;
                        }
                    }
                    $newdate->setDate($cur_year, $cur_month, $cur_date);
                break;
                case "Y":
                    if ($lastdate === $cur_date) { //最後の日付をチェックしてから次の最後の日付を取る
                        if ($PM === '1') {
                            $g_lastDate = ($cur_year + $DY) . '' . $cur_month . '01';
                            $g_last = date("Ymt", strtotime($g_lastDate));
                            $nextlastdate = substr($g_last, -2);
                        } else {
                            $g_lastDate = ($cur_year - $DY) . '' . $cur_month . '01';
                            $g_last = date("Ymt", strtotime($g_lastDate));
                            $nextlastdate = substr($g_last, -2);
                        }
                        if ($nextlastdate <= $cur_date) {
                            $cur_date = $nextlastdate;
                        }
                    }
                    if ($PM === '1') {
                        $cur_year+= $DY;
                    } else {
                        $cur_year-= $DY;
                    }
                    $newdate->setDate($cur_year, $cur_month, $cur_date);
                break;
            }
            $newdate = $newdate->format('Ymd');
        }
        $DATA = dateStyle($fmt, $newdate);
    }
    return $DATA;
}
/**日付フォーマット**/
function dateStyle($fmt, $newdate) {
    $dd = substr($newdate, 6, 2);
    $mm = substr($newdate, 4, 2);
    $yyyy = substr($newdate, 0, 4);
    switch ($fmt) {
        case "Y/m":
            $newdate = $yyyy . '/' . $mm;
        break;
        case "Ym":
            $newdate = $yyyy . '' . $mm;
        break;
        case "y/m/d":
            $newdate = substr((String)$yyyy, 2, 2) . '/' . $mm . '/' . $dd;
        break;
        case "ymd":
            $newdate = substr((String)$yyyy, 2, 2) . '' . $mm . '' . $dd;
        break;
        case "Y/m/d":
            $newdate = $yyyy . '/' . $mm . '/' . $dd;
        break;
        case "Ymd":
            $newdate = $yyyy . '' . $mm . '' . $dd;
        break;
    }
    return $newdate;
}
// SQLのクエリー実行に制御データ取得の場合
function createSeigyoSQLQryTbl($d1name, $filename, $sSearch = '', $burstItm = array()) {
    e_log('before制御ファイル名：' . $filename);
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $fdb2csv1 = FDB2CSV1_DATA($d1name);
    if ($fdb2csv1['result'] === true) {
        $fdb2csv1 = umEx($fdb2csv1['data']);
        $fdb2csv1 = $fdb2csv1[0];
    }
    if (($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME) === false) {
        $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1['D1RDB'];
        $db2RDBCon = cmDB2ConRDB(SAVE_DB);
    } else {
        $db2RDBCon = $db2con;
    }
    $fdb2csv2 = cmGetFDB2CSV2($db2con, $d1name,false,false,false);
    if ($fdb2csv2['result'] === true) {
        $fdb2csv2 = $fdb2csv2['data'];
    }
    $rtn = 0;
    $tmpTblNm = makeRandStr(10, true);
    $systables = cmGetSystables($db2RDBCon, SAVE_DB, $tmpTblNm);
    e_log('SYSTEMTable' . print_r($systables, true));
    if (count($systables) === 0) {
        $db2colmData = array();
        $rsDb2colm = cmGetDB2COLM($db2con, $d1name);
        if ($rsDb2colm['result'] !== true) {
            $rtn = 'FAIL_SEL';
            $msg = 'クエリー実行中失敗しました。' . db2_stmt_errormsg();
        } else {
            $db2colmData = umEx($rsDb2colm['data']);
        }
        $seigyoKeyArr = array();
        if (count($db2colmData) > 0) {
            $db2colmData = $db2colmData[0];
            for ($lvlCnt = 1;$lvlCnt <= 6;$lvlCnt++) {
                if ($db2colmData['DCFLD' . $lvlCnt] !== '') {
					$splitResArr=array();
			        $LGroup = explode(",",cmMer($db2colmData['DCFLD' . $lvlCnt]));
			        if (count($LGroup) >= 2) { //グループのレベル
						for($innerL = 0;$innerL < count($LGroup);$innerL++){
							$splitRes=explode("-",$LGroup[$innerL]);
							$seigyoFld = 'A.'.$splitRes[1];
							$seigyoKeyArr[] =$seigyoFld;
						}
					}else{
						$seigyoFld = 'A.' . $db2colmData['DCFLD' . $lvlCnt];
						$seigyoKeyArr[] = $seigyoFld;
					}
                }
            }
        }
        $strRowNumCol = '';
        if (count($seigyoKeyArr) > 0) {
            $strRowNumCol = ' ROWNUMBER() OVER ( ';
            $strSeigyoSortColInfo = join(', ', $seigyoKeyArr);
            $strRowNumCol.= ' ORDER BY ' . ' ' . $strSeigyoSortColInfo;
            $strRowNumCol.= ' ) AS ROWNUM ';
            //e_log('制御カラム情報：'.$strRowNumCol);
            
        } else {
            $strRowNumCol = ' ROWNUMBER() OVER ( ';
            $strRowNumCol.= ' ) AS ROWNUM ';
        }
        $strSQL = ' SELECT ';
        $fldArr = array();
        $rs = fngetSQLFldCCSID($db2RDBCon, $filename);
        if ($rs['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($rs['result']);
        } else {
            //e_log('fldCCSIDresult'.print_r($rs,true));
            $fldCCSIDARR = $rs['data'];
            $fldccsidCnt = $rs['CCSIDCNT'];
            if ($fldccsidCnt > 0) {
                $fldArr = array();
                foreach ($fldCCSIDARR as $fldccsid) {
                    if ($fldccsid['CCSID'] === '65535') {
                        if ($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW') {
                            $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                        } else {
                            $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                        }
                    } else {
                        $fldArr[] = $fldccsid['COLUMN_NAME'] . ' AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                    }
                }
            } else {
                foreach ($fdb2csv2 as $fdb2csv2data) {
                    $fldArr[] = $fdb2csv2data['D2FLD'] . ' AS ' . $fdb2csv2data['D2FLD'] . '_' . $fdb2csv2data['D2FILID'];
                }
            }
        }
        $fldArrStr = join(",", $fldArr);
        $strSQL.= $fldArrStr;
        if ($strRowNumCol !== '') {
            $strSQL.= ' , ' . $strRowNumCol;
        }
        $strSQL.= ' FROM ';
        $strSQL.= SAVE_DB . '/' . $filename . ' A ';
        if ($sSearch !== '') {
            $fdb2csv2 = cmGetFDB2CSV2($db2con, $d1name,false,false,false);
            if ($fdb2csv2['result'] === true) {
                $fdb2csv2 = $fdb2csv2['data'];
                foreach ($fdb2csv2 as $key => $value) {
                    $colnm = 'A.' . $value['D2FLD'];
                    $addFlg = true;
                    $byte = checkByte($sSearch);
                    if ($byte[1] > 0) {
                        if ($value['D2TYPE'] !== 'O') {
                            $addFlg = false;
                        }
                    }
                    if ($addFlg) {
                        $filterSQLArr[].= $colnm . ' LIKE \'%' . $sSearch . '%\' ';
                    }
                    if (count($filterSQLArr) > 0) {
                        $filterSQL = join("OR ", $filterSQLArr);
                    } else {
                        $filterSQL = '';
                    }
                }
                //e_log('FILTERSQL：'.$filterSQL);
                if ($filterSQL !== '') {
                    $strSQL.= ' WHERE ';
                    $strSQL.= $filterSQL;
                }
            }
        }
        if (count($burstItm) > 0) {
            $strburstSQL = '';
            if (cmMer($burstItm['WABAFLD']) !== '') {
                $burstFld = 'A.' . cmMer($burstItm['WABAFLD']);
                $burstFldTyp = '';
                $fdb2csv2 = cmGetFDB2CSV2($db2con, $d1name,false,false,false);
                if ($fdb2csv2['result'] === true) {
                    $fdb2csv2 = $fdb2csv2['data'];
                    foreach ($fdb2csv2 as $key => $value) {
                        if ($burstFld === ($value['D2FLD'])) {
                            $burstFldTyp = $value['D2TYPE'];
                            break;
                        }
                    }
                    if (($burstFldType === 'P') || ($burstFldType === 'S') || ($burstFldType === 'B')) {
                        $typFlg = false;
                    } else {
                        $typFlg = true;
                    }
                    if ($burstItm['WABFLG'] == '1') {
                        $wldataList = array();
                        foreach ($burstItm['WLDATA'] AS $KEY => $VALUE) {
                            $wldataList[] = $VALUE;
                        }
                        if (count($wldataList) > 0) {
                            if ($typFlg) {
                                $burstItmParam = join("' , '", $wldataList);
                                $burstItmParam = '\'' . $burstItmParam . '\'';
                            } else {
                                $burstItmParam = join(" , ", $wldataList);
                            }
                        }
                        $strburstSQL.= $burstFld . ' IN ( ';
                        $strburstSQL.= $burstItmParam;
                        $strburstSQL.= ')';
                    } else if (cmMer($burstItm['WABAFR']) !== '' && cmMer($burstItm['WABATO']) !== '') {
                        //$strburstSQL .= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' BETWEEN ';
                        $strburstSQL.= cmMer($burstItm['WABAFLD']) . ' BETWEEN ';
                        if ($typFlg) {
                            $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\'' . ' AND ' . '\'' . cmMer($burstItm['WABATO']) . '\' ';
                        } else {
                            $strburstSQL.= cmMer($burstItm['WABAFR']) . ' AND ' . cmMer($burstItm['WABATO']);
                        }
                    } else if ($burstItm['WABAFR'] !== '') {
                        //$strburstSQL .= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' = ';
                        $strburstSQL.= cmMer($burstItm['WABAFLD']) . ' = ';
                        if ($typFlg) {
                            $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\' ';
                        } else {
                            $strburstSQL.= cmMer($burstItm['WABAFR']);
                        }
                    }
                }
                //e_log('バースト条件データ：'.$strburstSQL);
                if ($strburstSQL !== '') {
                    $strSQL.= ' WHERE ';
                    $strSQL.= $strburstSQL;
                }
            }
        }
        if ($rtn === 0) {
            $strCreate = '';
            $strCreate.= ' CREATE TABLE';
            $strCreate.= ' ' . SAVE_DB . '/' . $tmpTblNm . ' AS';
            $strCreate.= ' (' . $strSQL . ' )';
            $strCreate.= ' WITH DATA ';
            // 複数の空白消す
            //$strCreate = spaceTrim($strCreate);
            if ($rtn === 0) {
                // 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
                if (strlen($strCreate) > 32767) {
                    $rtn = 1;
                    $msg = showMsg('FAIL_BASE_LENLIMIT'); //'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
                    
                }
            }
            e_log('制御取得SQL文:' . $strCreate);
            $stmt = db2_prepare($db2RDBCon, $strCreate);
            if ($stmt === false) {
                $rtn = 'FAIL_SEL';
                $msg = 'クエリー実行準備失敗しました。' . db2_stmt_errormsg();
            } else {
                $r = db2_execute($stmt, array());
                if ($r === false) {
                    $rtn = 'FAIL_SEL';
                    $msg = 'クエリー実行中失敗しました。' . db2_stmt_errormsg();
                } else {
                    $msg = 'ワークテーブル作成完了' . $tmpTblNm;
                }
            }
        }
        if ($rtn !== 0) {
            $tmpTblNm = '';
        } else {
            e_log('SQL制御ワークテーブル作成完了。' . $msg);
        }
    } else {
        $tmpTblNm = '';
        e_log('重複ワークテーブル有りSQL制御ワークテーブル作成失敗。' . $msg);
    }
    cmDb2Close($db2con);
    if (($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME) === false) {
        cmDb2Close($db2RDBCon);
    }
    e_log('after制御ファイル名：' . $tmpTblNm);
    return $tmpTblNm;
}
// SQLのクエリー実行にピボットデータ取得の場合
function createPivotSQLQryTbl($d1name, $pivNm, $filename, $burstItm = array()) {
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $fdb2csv1 = FDB2CSV1_DATA($d1name);
    if ($fdb2csv1['result'] === true) {
        $fdb2csv1 = umEx($fdb2csv1['data']);
        $fdb2csv1 = $fdb2csv1[0];
    }
    if (($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME) === false) {
        $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1['D1RDB'];
        $db2RDBCon = cmDB2ConRDB(SAVE_DB);
    } else {
        $db2RDBCon = $db2con;
    }
    $fdb2csv2 = cmGetFDB2CSV2($db2con, $d1name,false,false,false);
    if ($fdb2csv2['result'] === true) {
        $fdb2csv2 = $fdb2csv2['data'];
    }
    $rtn = 0;
    $tmpTblNm = makeRandStr(10, true);
    $systables = cmGetSystables($db2RDBCon, SAVE_DB, $tmpTblNm);
    //e_log('システムテーブル：'.print_r($systables,true));
    if (count($systables) === 0) {
        $db2pcolData = getDB2PCOLData($db2con, $d1name, $pivNm);
        //e_log('ピボットキーデータ：'.$d1name.'ki-'.$pivNm.print_r($db2pcolData,true));
        if ($db2pcolData[result] === true) {
            if (count($db2pcolData['data']) > 0) {
                $db2pcolData = $db2pcolData['data'];
                //e_log('ピボットキーデータ：'.print_r($db2pcolData,true));
                foreach ($db2pcolData as $key => $value) {
                    if ($value['D2TYPE'] !== '') {
                        $pivfldnm = $value['WPFLD'] . ' ' . $value['PIVFLDSORT'];
                        if ($sumKeySQL === '') {
                            $pivfldnm = 'SQ2.' . $pivfldnm;
                        }
                        if ($value['WPPFLG'] == 1) {
                            $xKeyArr[] = $pivfldnm;
                        } else if ($value['WPPFLG'] == 2) {
                            $yKeyArr[] = $pivfldnm;
                        }
                    }
                }
                //e_log('ピボットXキー配列：'.print_r($xKeyArr,true));
                //e_log('ピボットYキー配列：'.print_r($yKeyArr,true));
                
            }
        }
        $strRowNumCol = ' ROWNUMBER() OVER ( ';
        $strRowNumCol.= ' ) AS RN ';
        // 縦軸のデータ
        $strXRowNumCol = ' ROWNUMBER() OVER ( ';
        if (count($xKeyArr) > 0) {
            $xRNSQL = join(" , ", $xKeyArr);
            $strXRowNumCol.= ' ORDER BY ' . ' ' . $xRNSQL;
        }
        $strXRowNumCol.= ' ) AS XROWNUM ';
        // 横軸のデータ
        $strYRowNumCol = ' ROWNUMBER() OVER ( ';
        if (count($yKeyArr) > 0) {
            $yRNSQL = join(" , ", $yKeyArr);
            $strYRowNumCol.= ' ORDER BY ' . ' ' . $yRNSQL;
        }
        $strYRowNumCol.= ' ) AS YROWNUM ';
        //$strSQL  = ' SELECT A.* ';
        $strSQL = ' SELECT  ';
        $fldArr = array();
        $rs = fngetSQLFldCCSID($db2RDBCon, $filename);
        if ($rs['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($rs['result']);
        } else {
            //e_log('fldCCSIDresult'.print_r($rs,true));
            $fldCCSIDARR = $rs['data'];
            $fldccsidCnt = $rs['CCSIDCNT'];
            if ($fldccsidCnt > 0) {
                $fldArr = array();
                foreach ($fldCCSIDARR as $fldccsid) {
                    if ($fldccsid['CCSID'] === '65535') {
                        if ($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW') {
                            $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(4) CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                        } else {
                            $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                        }
                    } else {
                        $fldArr[] = $fldccsid['COLUMN_NAME'] . '_0 ';
                    }
                }
            } else {
                foreach ($fdb2csv2 as $fdb2csv2data) {
                    $fldArr[] = $fdb2csv2data['D2FLD'] . ' ' . $fdb2csv2data['D2FLD'] . '_' . $fdb2csv2data['D2FILID'];
                }
            }
        }
        $fldArrStr = join(",", $fldArr);
        $strSQL.= $fldArrStr;
        $strSQL.= ' , ' . $strRowNumCol;
        $strSQL.= ' , ' . $strXRowNumCol;
        $strSQL.= ' , ' . $strYRowNumCol;
        $strSQL.= ' FROM ';
        $strSQL.= SAVE_DB . '/' . $filename . ' A ';
        if ($burstItm <> '' && count($burstItm) > 0) {
            $strburstSQL = '';
            if (cmMer($burstItm['WABAFLD']) !== '') {
                $burstFld = 'A.' . cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']);
                $burstFldTyp = '';
                foreach ($fdb2csv2 as $key => $value) {
                    if ($burstFld === ($value['D2FLD'] . '_' . $value['D2FILID'])) {
                        $burstFldTyp = $value['D2TYPE'];
                        break;
                    }
                }
                if (($burstFldType === 'P') || ($burstFldType === 'S') || ($burstFldType === 'B')) {
                    $typFlg = false;
                } else {
                    $typFlg = true;
                }
                if ($burstItm['WABFLG'] == '1') {
                    $wldataList = array();
                    foreach ($burstItm['WLDATA'] AS $KEY => $VALUE) {
                        $wldataList[] = $VALUE;
                    }
                    if (count($wldataList) > 0) {
                        if ($typFlg) {
                            $burstItmParam = join("' , '", $wldataList);
                            $burstItmParam = '\'' . $burstItmParam . '\'';
                        } else {
                            $burstItmParam = join(" , ", $wldataList);
                        }
                    }
                    $strburstSQL.= $burstFld . ' IN ( ';
                    $strburstSQL.= $burstItmParam;
                    $strburstSQL.= ')';
                } else if (cmMer($burstItm['WABAFR']) !== '' && cmMer($burstItm['WABATO']) !== '') {
                    $strburstSQL.= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' BETWEEN ';
                    if ($typFlg) {
                        $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\'' . ' AND ' . '\'' . cmMer($burstItm['WABATO']) . '\' ';
                    } else {
                        $strburstSQL.= cmMer($burstItm['WABAFR']) . ' AND ' . cmMer($burstItm['WABATO']);
                    }
                } else if ($burstItm['WABAFR'] !== '') {
                    $strburstSQL.= cmMer($burstItm['WABAFLD']) . '_' . cmMer($burstItm['WABAFID']) . ' = ';
                    if ($typFlg) {
                        $strburstSQL.= '\'' . cmMer($burstItm['WABAFR']) . '\' ';
                    } else {
                        $strburstSQL.= cmMer($burstItm['WABAFR']);
                    }
                }
                //e_log('バースト条件データ：'.$strburstSQL);
                if ($strburstSQL !== '') {
                    $strSQL.= ' WHERE ';
                    $strSQL.= $strburstSQL;
                }
            }
        }
        if ($rtn === 0) {
            $strCreate = '';
            $strCreate.= ' CREATE TABLE';
            $strCreate.= ' ' . SAVE_DB . '/' . $tmpTblNm . ' AS';
            $strCreate.= ' (' . $strSQL . ' )';
            $strCreate.= ' WITH DATA ';
            // 複数の空白消す
            //$strCreate = spaceTrim($strCreate);
            if ($rtn === 0) {
                // 実行対象のSQL文の桁数チェック、制限【32767】超えればエラーメッセージ表示
                if (strlen($strCreate) > 32767) {
                    $rtn = 1;
                    $msg = showMsg('FAIL_BASE_LENLIMIT'); //'クエリーで作成されたSQLが制限を超えています。</br>SQLを実行できませんでした。</br>※参照ファイルや出力フィールド、条件指定を減らすことで実行できる可能性があります。';
                    
                }
            }
            e_log('create SQL:' . $strCreate);
            $stmt = db2_prepare($db2RDBCon, $strCreate);
            if ($stmt === false) {
                $rtn = 'FAIL_SEL';
                $msg = 'クエリー実行準備失敗しました。' . db2_stmt_errormsg();
            } else {
                //e_log('create table:SQL'.$strCreate.'params'.print_r($params,true));
                $r = db2_execute($stmt, array());
                if ($r === false) {
                    $rtn = 'FAIL_SEL';
                    $msg = 'クエリー実行中失敗しました。' . db2_stmt_errormsg();
                } else {
                    $msg = 'ワークテーブル作成完了' . $tmpTblNm;
                }
            }
        }
        if ($rtn !== 0) {
            $tmpTblNm = '';
            e_log($msg);
        } else {
            e_log('SQLピボットワークテーブル作成完了。' . $msg);
        }
    } else {
        e_log('重複SQLピボットワークテーブル作成失敗。' . $msg);
    }
    return $tmpTblNm;
}
/****filecopy****/
function fileCopy($filedownload) {
    $res = true;
    if (file_exists($filedownload)) {
        $originalFile = $filedownload;
        $newFile = $filedownload . "_temp";
        if (!copy($originalFile, $newFile)) {
            $res = false;
        }
    }
    return $res;
}
/****filecopyrollback****/
function filerollBack($filedownload) {
    $res = true;
    $newFile = $filedownload . "_temp";
    $originalFile = $filedownload;
    if (file_exists($newFile)) {
        if (!rename($newFile, $originalFile)) {
            $res = false;
        }
    }
    return $res;
}
/***file unlink and backup***/
function unlinkFile($filedownload) {
    $res = true;
    if (file_exists($filedownload)) {
        $rs = fileCopy($filedownload);
        if (!unlink($filedownload)) {
            $filedownloadbk = $filedownload . "_temp";
            if (file_exists($filedownloadbk)) {
                unlink($filedownloadbk);
            }
        }
    }
    return $res;
}
/*
 *  ワークテーブルにあるフィールドとFDB2CSV2のフィールド同じチェック
*/
function chkFDB2CSV2WKTblCol($db2con, $tblname, $fdb2csv2, $d1cflg) {
    $fldArr = array();
    $rtn = 0;
    $createFlg = false;
    foreach ($fdb2csv2 as $val) {
        if (cmMer($val['D2CSEQ']) > 0) {
            if ($d1cflg === '1') {
                $fldArr[] = cmMer($val['D2FLD']);
            } else {
                $fldArr[] = cmMer($val['D2FLD']) . '_' . cmMer($val['D2FILID']);
            }
        }
    }
    if (count($fldArr) > 0) {
        $createSQL = '';
        $createSQL = 'DECLARE GLOBAL TEMPORARY TABLE FDB2CSV2 ';
        $createSQL.= ' ( ';
        $createSQL.= ' D2FLD CHAR(20)  ';
        $createSQL.= ' )  WITH REPLACE  ';
        e_log($createSQL);
        $result = db2_exec($db2con, $createSQL);
        if ($result) {
            e_log('テーブル作成成功');
            if (count($fldArr) > 0) {
                $strInsSQL = ' INSERT INTO QTEMP/FDB2CSV2 ';
                $strInsSQL.= ' ( ';
                $strInsSQL.= '     D2FLD ';
                $strInsSQL.= ' ) ';
                $strInsSQL.= ' VALUES ( ?)';
                $stmt = db2_prepare($db2con, $strInsSQL);
                if ($stmt === false) {
                    $data = array('result' => 'FAIL_INS', 'errcd' => 'FDB2CSV2:' . db2_stmt_errormsg());
                    $result = 1;
                } else {
                    $createFlg = true;
                    foreach ($fldArr as $fldVal) {
                        $params = array($fldVal);
                        $res = db2_execute($stmt, $params);
                        //e_log($strInsSQL.print_r($params,true));
                        if ($res === false) {
                            $data = array('result' => 'FAIL_INS', 'errcd' => 'FDB2CSV2:' . db2_stmt_errormsg());
                            e_log($strInsSQL . print_r($params, true) . print_r($data, true));
                            $result = 1;
                            break;
                        } else {
                            $result = 0;
                            $data = array('result' => true);
                        }
                    }
                }
            }
            if ($result !== 0) {
                $rtn = 1;
            }
        } else {
            //e_log('テーブル作成失敗');
            e_log('テーブルerror' . db2_stmt_errormsg());
            $rtn = 1;
            $data = array('result' => 'FAIL_SEL', 'errcd' => 'テーブル作成失敗');
        }
    }
    /*e_log('RTNDATA:'.$rtn.print_r($fdb2csv2,true));
    if($rtn === 0){
        e_log('QTEMP Data取得:');
        $qtempdata = array();
        $strSQL  = ' SELECT  ';
        $strSQL .= '    * ';
        $strSQL .= ' FROM ';
        $strSQL .= '    QTEMP/FDB2CSV2 ';
        $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $qtempdata = array('result' => 'FAIL_SEL');
            }else{
                $parms = array();
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $qtempdata = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $qtempdata[] = $row;
                    }
                }
            }
        e_log('QTEMP DATA FDB2CSV2:'.print_r($qtempdata,true));
    }*/
    /*
    $strSQL .=' SELECT FLG FROM ( ';
    $strSQL .='     SELECT A.COLUMN_NAME ';
    $strSQL .='          , B.D2FLD ';
    $strSQL .='          , CASE WHEN A.COLUMN_NAME IS NULL THEN \'F\' WHEN  B.D2FLD IS NULL THEN \'F\' END AS FLG ';
    $strSQL .='     FROM ( ';
    $strSQL .='         SELECT ';
    $strSQL .='             COLUMN_NAME ';
    $strSQL .='         FROM ';
    //$strSQL .='             QSYS2/SYSCOLUMNS2 ';
    $strSQL .=             SYSCOLUMN2 ;
    $strSQL .='         WHERE ';
    $strSQL .='             TABLE_SCHEMA = ? AND ';
    $strSQL .='             TABLE_NAME = ? AND ';
    $strSQL .='             COLUMN_NAME NOT IN (\'ROWNUM\',\'RN\',\'XROWNUM\',\'YROWNUM\') ';
    $strSQL .='     ) A ';
    $strSQL .='     FULL OUTER JOIN  ';
    $strSQL .='     ( ';
    $strSQL .='         SELECT ';
    $strSQL .='             D2FLD ';
    $strSQL .='         FROM ';
    $strSQL .='             QTEMP/FDB2CSV2 ';
    $strSQL .='     ) B ';
    $strSQL .='     ON A.COLUMN_NAME = B.D2FLD ';
    $strSQL .=' ) TBL ';
    $strSQL .=' WHERE FLG IS NOT NULL ';
    */
    if ($rtn === 0) {
        $param = array();
        $data = array();
        $strSQL = '';
        $strSQL.= '    SELECT ';
        $strSQL.= '        FLG ';
        $strSQL.= '    FROM ';
        $strSQL.= '        (  ';
        $strSQL.= '            ( ';
        $strSQL.= '                SELECT ';
        $strSQL.= '                    A.COLUMN_NAME ';
        $strSQL.= '                    , B.D2FLD ';
        $strSQL.= '                    , CASE WHEN A.COLUMN_NAME IS NULL THEN \'F\' WHEN  B.D2FLD IS NULL THEN \'F\' END AS FLG ';
        $strSQL.= '                FROM  ';
        $strSQL.= '                ( ';
        $strSQL.= '                    ( ';
        $strSQL.= '                        SELECT ';
        if ($d1cflg == 1) {
            $strSQL.= '                            SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
        } else {
            $strSQL.= '                            COLUMN_NAME ';
        }
        $strSQL.= '                        FROM  ';
        $strSQL.= '                            ' . SYSCOLUMN2 . ' ';
        $strSQL.= '                        WHERE ';
        $strSQL.= '                            TABLE_SCHEMA = ? AND ';
        $strSQL.= '                            TABLE_NAME = ? AND ';
        $strSQL.= '                            COLUMN_NAME NOT IN (\'ROWNUM\',\'RN\',\'XROWNUM\',\'YROWNUM\') ';
        $strSQL.= '                    ) A ';
        $strSQL.= '                    LEFT JOIN ';
        $strSQL.= '                    ( ';
        $strSQL.= '                        SELECT ';
        $strSQL.= '                            D2FLD ';
        $strSQL.= '                        FROM ';
        $strSQL.= '                            QTEMP/FDB2CSV2 ';
        $strSQL.= '                    ) B ';
        $strSQL.= '                    ON A.COLUMN_NAME = B.D2FLD ';
        $strSQL.= '                ) ';
        $strSQL.= '            ) ';
        $strSQL.= '            UNION ';
        $strSQL.= '            ( ';
        $strSQL.= '                SELECT ';
        $strSQL.= '                    A.COLUMN_NAME ';
        $strSQL.= '                    , B.D2FLD ';
        $strSQL.= '                    , CASE WHEN A.COLUMN_NAME IS NULL THEN \'F\' WHEN  B.D2FLD IS NULL THEN \'F\' END AS FLG ';
        $strSQL.= '                FROM  ';
        $strSQL.= '                ( ';
        $strSQL.= '                    ( ';
        $strSQL.= '                        SELECT ';
        if ($d1cflg == 1) {
            $strSQL.= '                            SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
        } else {
            $strSQL.= '                            COLUMN_NAME ';
        }
        $strSQL.= '                        FROM ';
        $strSQL.= '                            ' . SYSCOLUMN2 . ' ';
        $strSQL.= '                        WHERE ';
        $strSQL.= '                            TABLE_SCHEMA = ? AND ';
        $strSQL.= '                            TABLE_NAME = ? AND ';
        $strSQL.= '                            COLUMN_NAME NOT IN (\'ROWNUM\',\'RN\',\'XROWNUM\',\'YROWNUM\') ';
        $strSQL.= '                    ) A ';
        $strSQL.= '                    RIGHT JOIN ';
        $strSQL.= '                    ( ';
        $strSQL.= '                        SELECT ';
        $strSQL.= '                            D2FLD ';
        $strSQL.= '                        FROM';
        $strSQL.= '                            QTEMP/FDB2CSV2 ';
        $strSQL.= '                    ) B ';
        $strSQL.= '                    ON A.COLUMN_NAME = B.D2FLD ';
        $strSQL.= '                ) ';
        $strSQL.= '            ) ';
        $strSQL.= '        ) TBL  WHERE FLG IS NOT NULL  ';
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $data = array('result' => 'FAIL_SEL');
            e_log('【削除】フィールド差分取得:' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        } else {
            $params = array(SAVE_DB, $tblname, SAVE_DB, $tblname);
            e_log('【削除】フィールド差分取得①:' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array('result' => 'FAIL_SEL');
                e_log('【削除】dafdsf2:' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                if (count($data) > 0) {
                    e_log('【削除】dafdsf2:' . $strSQL . print_r($params, true) . print_r($data, true));
                    $data = array('result' => 'クエリーの設定が変更されています。再度実行してください。', data => umEx($data));
                } else {
                    $data = array('result' => true, 'data' => umEx($data));
                }
            }
        }
    }
    if ($createFlg) {
        $strSQL = ' DROP TABLE QTEMP/FDB2CSV2';
        $stmt = @db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            // error
            
        } else {
            $rs = @db2_execute($stmt);
        }
    }
    e_log('FDB2CSVData:' . print_r($data, true));
    return $data;
}
/*
 *-------------------------------------------------------*
 * ライブラリーマスター取得(一行)
 *-------------------------------------------------------*
*/
function cmSelDB2LIBL($db2con, $LIBLNM) {
    $data = array();
    $params = array();
    $rs = true;
    $strSQL = ' SELECT ';
    $strSQL.= ' A.LIBLNM,A.LIBLTE  ';
    $strSQL.= ' FROM DB2LIBL as A ';
    if ($LIBLNM !== '') {
        $strSQL.= ' WHERE LIBLNM = ? ';
        //array_push($params,$LIBLNM);
        $params[] = $LIBLNM;
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $data = array('result' => 'NOTEXIST_GET');
            } else {
                $data = array('result' => true, 'data' => $data);
            }
        }
    }
    return $data;
}
function filterLibl($db2con, $LIBSNM, $LIBSDB) {
    $data = array();
    $strSQL = 'SELECT LIBSID,LIBSFG FROM DB2LBLS WHERE LIBSNM = ? AND LIBSDB = ? AND LIBSFG <> \'0\'';
    $params = array($LIBSNM, $LIBSDB);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
function createTmpJlibl($con, $filterArr) {
    $data = array('result' => true);
    $strCreate = '';
    $strCreate = 'DECLARE GLOBAL TEMPORARY TABLE DB2LBLS ';
    $strCreate.= ' ( ';
    $strCreate.= ' LIBSID CHAR(10)  ';
    $strCreate.= ' ,LIBSFG CHAR(1) ';
    $strCreate.= ' )  WITH REPLACE  ';
    $stmt = db2_prepare($con, $strCreate);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $params = array();
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        }
    }
    if ($data['result'] === true) {
        $strSQL = "insert into QTEMP/DB2LBLS values(?,?)";
        $stmt = db2_prepare($con, $strSQL);
        if ($stmt === false) {
            $data = array('result' => 'FAIL_SEL');
            e_log(db2_stmt_errormsg());
        } else {
            foreach ($filterArr as $key => $value) {
                $LIBSID = $value['LIBSID'];
                $LIBSFG = $value['LIBSFG'];
                $params = array($LIBSID, $LIBSFG);
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $data = array('result' => 'FAIL_SEL');
                    break;
                }
            }
            $data = array('result' => true, 'filterArr' => $filterArr, 'c' => $c);
        }
    }
    return $data;
}
function createQTEMPDB2LBLS($db2con, $LIBSNM) {
    $resCreate = true;
    $rsDb2lbls = true;
    $dbconnect = cmDb2Con();
    cmSetPHPQUERY($dbconnect);
    $dataDb2lbls = array();
    // 除外されたライブラリー情報取得
    $strdb2lbls = ' SELECT  ';
    $strdb2lbls.= '     LIBSNM, ';
    $strdb2lbls.= '     LIBSID, ';
    $strdb2lbls.= '     LIBSDB, ';
    $strdb2lbls.= '     LIBSFG ';
    $strdb2lbls.= ' FROM DB2LBLS ';
    $strdb2lbls.= ' WHERE LIBSNM = ? ';
    $strdb2lbls.= ' AND LIBSDB = ? ';
    $strdb2lbls.= ' AND LIBSFG = \'1\' ';
    $stmt = db2_prepare($dbconnect, $strdb2lbls);
    if ($stmt === false) {
        $resCreate = 'FAIL_SEL';
    } else {
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM']) ? $_SESSION['PHPQUERY']['RDBNM'] : '');
        $lblsParam = array($LIBSNM, $_SESSION['PHPQUERY']['RDBNM'] === '' ? RDB : $_SESSION['PHPQUERY']['RDBNM']);
        $r = db2_execute($stmt, $lblsParam);
        if ($r === false) {
            $resCreate = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $dataDb2lbls[] = $row;
            }
        }
    }
    //e_log('result'.print_r($dataDb2lbls,true));
    cmDb2Close($dbconnect);
    if ($resCreate) {
        $createSQL = '';
        $createSQL = 'DECLARE GLOBAL TEMPORARY TABLE DB2LBLS ';
        $createSQL.= ' ( ';
        $createSQL.= ' LIBSID CHAR(10),  ';
        $createSQL.= ' LIBSFG CHAR(1) ';
        $createSQL.= ' )  WITH REPLACE  ';
        $res = db2_exec($db2con, $createSQL);
        e_log('createRes:' . print_r($resCreate, true));
        if ($res) {
            $resCreate = true;
            e_log('テーブル作成成功');
            if (count($dataDb2lbls) > 0) {
                $strInsSQL = ' INSERT INTO QTEMP/DB2LBLS ';
                $strInsSQL.= ' ( ';
                $strInsSQL.= '     LIBSID ';
                $strInsSQL.= '    ,LIBSFG ';
                $strInsSQL.= ' ) ';
                $strInsSQL.= ' VALUES ( ?,?)';
                $stmt = db2_prepare($db2con, $strInsSQL);
                if ($stmt === false) {
                    $data = array('result' => 'FAIL_INS', 'errcd' => 'DB2LBLS:' . db2_stmt_errormsg());
                    $resCreate = 'FAIL_INS';
                } else {
                    e_log('取得したデータ' . print_r($dataDb2lbls, true));
                    foreach ($dataDb2lbls as $db2lblsVal) {
                        $insparams = array($db2lblsVal['LIBSID'], $db2lblsVal['LIBSFG']);
                        $res = db2_execute($stmt, $insparams);
                        if ($res === false) {
                            $data = array('result' => 'FAIL_INS', 'errcd' => 'DB2LBLS:' . db2_stmt_errormsg());
                            $resCreate = 'FAIL_INS';
                            break;
                        } else {
                            $data = array('result' => true);
                            $resCreate = true;
                        }
                    }
                }
            }
        }
    }
    return $resCreate;
}
/**
 *ユーザが作成したクエリーのメニュー
 */
function cmGetDB2COF($db2con) {
    $data = array();
    $rs = true;
    $strSQL = '';
    $strSQL = ' SELECT ';
    $strSQL.= ' * ';
    $strSQL.= ' FROM ';
    $strSQL.= ' DB2COF  ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return array('result' => $rs, 'data' => umEx($data, true));
}
/*
 *-------------------------------------------------------*
 * ラブラリーマスター取得
 *-------------------------------------------------------*
*/
// クエリーマスタ情報取得
function cmGetQryOptionInfo($db2con, $QRYNM) {
    $data = array();
    $params = array();
    $strSQL = '   SELECT ';
    $strSQL.= '      A.D1NAME, ';
    $strSQL.= '      A.D1TEXT, ';
    $strSQL.= '       (CASE ';
    $strSQL.= '           WHEN I.WGNAME <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS A2, ';
    $strSQL.= '      (CASE A.D1WEBF ';
    $strSQL.= '          WHEN \'1\' THEN \'Web\' ';
    $strSQL.= '          ELSE \'5250\' ';
    $strSQL.= '      END) AS D1WEBF, ';
    $strSQL.= '      (CASE ';
    $strSQL.= '           WHEN A.D1DISF = \'1\' THEN \'1\' ';
    $strSQL.= '           WHEN A.DISCSN <> \'\' THEN \'1\' ';
    $strSQL.= '           WHEN A.D1EFLG = \'1\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS A3, '; //実行設定
    $strSQL.= '       (CASE ';
    $strSQL.= '           WHEN B.HTMLTN <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS A4, '; // HTMLTEMPLATE
    $strSQL.= '       (CASE ';
    $strSQL.= '           WHEN A.D1TMPF <> \'\' THEN \'1\' ';
    $strSQL.= '           WHEN G.ECNAME <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS A5, '; // XLSTemplate
    $strSQL.= '       (CASE ';
    $strSQL.= '           WHEN C.DFNAME <> \'\' THEN \'1\' ';
    $strSQL.= '           WHEN H.DRKMTN <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS A6, '; //詳細Template
    $strSQL.= '       (CASE ';
    $strSQL.= '           WHEN M.WDNAME <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS A7, '; //ダウンロード権限
    $strSQL.= '       (CASE ';
    $strSQL.= '           WHEN N.D2NAME <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS A8, '; //ダウンロード権限
    $strSQL.= '      (CASE ';
    $strSQL.= '           WHEN A.D1INFO <> \'\' THEN \'1\' ';
    $strSQL.= '           WHEN A.D1INFG = \'1\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS A9, '; //補助
    $strSQL.= '      (CASE ';
    $strSQL.= '           WHEN D3NAME <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS A, ';
    $strSQL.= '      (CASE ';
    $strSQL.= '           WHEN D.DTNAME <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS B, ';
    $strSQL.= '      (CASE ';
    $strSQL.= '           WHEN J.PMNAME <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS C, ';
    $strSQL.= '      (CASE ';
    $strSQL.= '           WHEN A.D1CENC = \'1\' THEN \'1\' ';
    $strSQL.= '           WHEN A.D1CKAI = \'1\' THEN \'1\' ';
    $strSQL.= '           WHEN A.D1CHED = \'1\' THEN \'1\' ';
    $strSQL.= '           WHEN A.D1CDLM = \'1\' THEN \'1\' ';
    $strSQL.= '           WHEN A.D1CECL = \'1\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '        END) AS D, ';
    $strSQL.= '       (CASE ';
    $strSQL.= '           WHEN K.SNAME <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS E, ';
    $strSQL.= '      (CASE ';
    $strSQL.= '           WHEN L.CLNAME <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS F, ';
    $strSQL.= '      (CASE ';
    $strSQL.= '           WHEN GRP.GPKQRY <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END) AS G , ';
    $strSQL.= '       (CASE ';
    $strSQL.= '           WHEN DSH.BMKQID <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END ) AS H , ';
	//add by TTA case J
    $strSQL.= '       (CASE ';
    $strSQL.= '           WHEN COL.JKNAME <> \'\' THEN \'1\' ';
    $strSQL.= '           ELSE \'\' ';
    $strSQL.= '       END ) AS J ';
    //End
    $strSQL.= '   FROM FDB2CSV1 A ';
    $strSQL.= '   LEFT JOIN DB2HTMLT B ';
    $strSQL.= '   ON A.D1NAME = B.HTMLTD ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '   (SELECT DFNAME ';
    $strSQL.= '    FROM DB2WDTL ';
    $strSQL.= '    JOIN DB2WDFL ';
    $strSQL.= '    ON DTNAME = DFNAME ';
    $strSQL.= '    GROUP BY DFNAME)C ';
    $strSQL.= '    ON A.D1NAME = C.DFNAME ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '   (SELECT ';
    $strSQL.= '       DRKMTN ';
    $strSQL.= '   FROM ';
    $strSQL.= '       DB2DRGS ';
    $strSQL.= '   GROUP BY ';
    $strSQL.= '       DRKMTN ) H';
    $strSQL.= '    ON A.D1NAME = H.DRKMTN ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '    (SELECT ';
    $strSQL.= '        ECNAME ';
    $strSQL.= '    FROM ';
    $strSQL.= '        DB2ECON ';
    $strSQL.= '    LEFT JOIN DB2EINS ';
    $strSQL.= '    ON EINAME = ECNAME';
    $strSQL.= '    GROUP BY ECNAME)G ';
    $strSQL.= '    ON A.D1NAME = G.ECNAME ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '   (SELECT DTNAME ';
    $strSQL.= '   FROM DB2COLM ';
    $strSQL.= '   JOIN DB2COLT ';
    $strSQL.= '   ON DCNAME = DTNAME ';
    $strSQL.= '   GROUP BY DTNAME)D ';
    $strSQL.= '   ON A.D1NAME = D.DTNAME ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '   (SELECT D3NAME FROM FDB2CSV3 ';
    $strSQL.= '   WHERE (D3CANL <> \'\' ';
    $strSQL.= '       AND D3CANF <> \'\') ';
    $strSQL.= '   OR (D3DFMT <> \'\') ';
    $strSQL.= '   GROUP BY D3NAME ';
    $strSQL.= '   UNION ';
    $strSQL.= '   SELECT CNQRYN D3NAME FROM BQRYCND ';
    $strSQL.= '   WHERE (CNCANL <> \'\' ';
    $strSQL.= '       AND CNCANF <> \'\') ';
    $strSQL.= '   OR (CNDFMT <> \'\') ';
    $strSQL.= '   GROUP BY CNQRYN ';
    $strSQL.= '   UNION ';
    $strSQL.= '   SELECT CNQRYN D3NAME FROM BSQLCND ';
    $strSQL.= '   WHERE (CNCANL <> \'\' ';
    $strSQL.= '       AND CNCANF <> \'\') ';
    $strSQL.= '   OR (CNDFMT <> \'\') ';
    $strSQL.= '   GROUP BY CNQRYN ';
    $strSQL.= '   UNION ';
    $strSQL.= '   SELECT DHNAME D3NAME ';
    $strSQL.= '   FROM DB2HCND ';
    $strSQL.= '   WHERE DHINFO <> \'\' ';
    $strSQL.= '   GROUP BY DHNAME ';
    $strSQL.= '   )F ';
    $strSQL.= '   ON A.D1NAME = F.D3NAME ';
    $strSQL.= '   LEFT JOIN (SELECT WGNAME FROM DB2WGDF GROUP BY WGNAME) I ';
    $strSQL.= '   ON A.D1NAME = I.WGNAME ';
    $strSQL.= '   LEFT JOIN (SELECT PMNAME FROM DB2PMST GROUP BY PMNAME) J';
    $strSQL.= '   ON A.D1NAME = J.PMNAME ';
    $strSQL.= '   LEFT JOIN ';
    /*$strSQL .=  '    (SELECT PMNAME AS SNAME FROM DB2PMST GROUP BY PMNAME ';
     $strSQL .=  '    UNION ';*/
    $strSQL.= '    (SELECT WSNAME AS SNAME FROM DB2WSCD GROUP BY WSNAME ';
    $strSQL.= '    UNION ';
    $strSQL.= '    SELECT WANAME AS SNAME  FROM DB2WAUT GROUP BY WANAME ';
    $strSQL.= '    UNION ';
    $strSQL.= '    SELECT WLNAME AS SNAME  FROM DB2WAUL GROUP BY WLNAME ';
    $strSQL.= '    UNION ';
    $strSQL.= '    SELECT SONAME AS SNAME  FROM DB2WSOC GROUP BY SONAME) K ';
    $strSQL.= '   ON A.D1NAME = K.SNAME ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '  (SELECT DGNAME AS CLNAME FROM FDB2CSV1PG GROUP BY DGNAME ';
    $strSQL.= '  UNION ';
    $strSQL.= '  SELECT DMNAME AS CLNAME FROM FDB2CSV1PM GROUP BY DMNAME) L ';
    $strSQL.= '   ON A.D1NAME = L.CLNAME ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '  (SELECT WDNAME FROM DB2WDEF GROUP BY WDNAME) M ';
    $strSQL.= '   ON A.D1NAME = M.WDNAME ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '  (SELECT D2NAME FROM FDB2CSV2 WHERE D2CSEQ > 0  AND D2DNLF = \'1\' GROUP BY D2NAME) N ';
    $strSQL.= '   ON A.D1NAME = N.D2NAME ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '   (SELECT GPKQRY  FROM DB2GPK LEFT JOIN DB2GPC ON GPKQRY = GPCQRY AND GPKID = GPCID GROUP BY GPKQRY) GRP';
    $strSQL.= '  ON A.D1NAME = GRP.GPKQRY ';
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '   (SELECT BMKQID FROM DB2BMK GROUP BY BMKQID )DSH ';
    $strSQL.= '   ON A.D1NAME = DSH.BMKQID ';
	//add by TTA LEFT JOIN with COL
    $strSQL.= '   LEFT JOIN ';
    $strSQL.= '   (SELECT JKNAME FROM DB2JKSK GROUP BY JKNAME )COL ';
    $strSQL.= '   ON A.D1NAME = COL.JKNAME ';
    //End
    $strSQL.= ' WHERE ';
    $strSQL.= ' A.D1NAME <> \'\' ';
    $strSQL.= ' AND A.D1NAME = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
        $err = db2_stmt_errormsg();
        e_log($err);
    } else {
        $params = array($QRYNM);
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $err = db2_stmt_errormsg();
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => umEx($data));
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * ユーザー情報取得
 *-------------------------------------------------------*
*/
function cmChkDB2QRYG($db2con, $QRYGID) {
    $data = array();
    $params = array();
    $rs = true;
    $strSQL = ' SELECT *  ';
    $strSQL.= ' FROM DB2QRYG AS A ';
    $strSQL.= ' WHERE A.QRYGID = ? ';
    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    $data = array('result' => $rs, 'data' => umEx($data));
    return $data;
}
/**
 *お気に入りにから取得
 */
function cmGetDB2QBMK($db2con, $QBMUSR, $QBMQRY, $QBMPIV, $QBMGPH, $QBMFLG) {
    $BMKSEQ = 0;
    $data = array();
    $strSQL = '';
    $strSQL.= ' SELECT * FROM DB2QBMK WHERE QBMUSR = ? AND QBMQRY = ? AND QBMPIV = ? AND QBMGPH = ? AND QBMFLG = ? ';
    $params = array($QBMUSR, $QBMQRY, $QBMPIV, $QBMGPH, $QBMFLG);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt1 === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            $dt = array();
            while ($row = db2_fetch_assoc($stmt)) {
                $dt[] = $row;
            }
            $data = array('result' => true, 'datacount' => count($dt));
        }
    }
    return $data;
}
/*****
 *
 *select from DB2JKSK
 *
 *****/
function cmGetDB2JKSK($db2con, $JKNAME) {
    $data = array();
    $params = array();
    $rs = true;
    $strSQL = ' SELECT *  ';
    $strSQL.= ' FROM DB2JKSK AS A ';
    $strSQL.= ' WHERE A.JKNAME = ? ';
    $params[] = $JKNAME;
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    $data = array('result' => $rs, 'data' => umEx($data));
    return $data;
}
/*****
 *
 *お気に入りのデーター削除
 *
 *****/
function cmDeleteDB2QBMK($db2con, $QBMQRY = '', $QBMKEY = '', $QBMPIV = '', $QBMGPH = '', $QBMUSR = '') {
    $rs = true;
    $params = array();
    //構文
    $strSQL = " DELETE FROM DB2QBMK WHERE ";
    if ($QBMQRY !== '') {
        $strSQL.= " QBMQRY = ? AND QBMFLG = 1 ";
        array_push($params, $QBMQRY);
    } else {
        $strSQL.= " QBMKEY = ? ";
        array_push($params, $QBMKEY);
        if ($QBMPIV !== '') {
            $strSQL.= " AND QBMPIV = ? ";
            array_push($params, $QBMPIV);
        } else if ($QBMGPH !== '') {
            $strSQL.= " AND QBMGPH = ? ";
            array_push($params, $QBMGPH);
        }
    }
    if ($QBMUSR !== '') {
        $strSQL.= " AND QBMUSR = ? ";
        array_push($params, $QBMUSR);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_DEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
/*****
 *
 *検索の保存データー削除
 *
 *****/
function cmDeleteDB2SHSD($db2con, $QRYNAME) {
    $rs = true;
    $params = array();
    //構文
    $strSQL = " DELETE FROM DB2SHSD WHERE SHSDQID = ?";
    $params = array($QRYNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_DEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
/******************************************任意**********************************/
/**
 *params blank replace 1=1
 *@strSQL is sql query
 *@params is parameter
 *
 */
function cmReplaceSQLParam($D1SQL, $params) {
    //error_log('Common Sql Error**'.print_r($params,true));
    //get Field Column Array in query
    $getQryColArr = array();
    $testQryColArr = cmQueryFieldArr($D1SQL);
    $getQryColArr = $testQryColArr["getQryColArr"];
    $spos = $testQryColArr["spos"];
    /*commentがあったら、【1=1】を上書き*/
    $pcount = 0;
    $paramDeleteKey = array();
    foreach ($getQryColArr as $key => $val) {
        e_log("foreach " . $key . "=>" . $val . "pcount=>" . $pcount);
        if (strrpos($val, "/**/")) {
            $test = chkQueryFieldCondition(array($val), array());
            $FLG = $test["FLG"][0];
            if ($FLG === "IN") { //in
                e_log("IN CONDITION");
                $quesCount = substr_count($val, '{');
                $bcount = 0;
                for ($j = 0;$j < $quesCount;$j++) {
                    $valRes = (isset($params[$pcount])) ? $params[$pcount] : ""; //preg_match("/\'\'/i",$valRes)
                    if ($valRes === "") {
                        if (isset($params[$pcount])) {
                            $paramDeleteKey[] = $pcount;
                        }
                        $bcount++;
                    }
                    $pcount++;
                }
                if (preg_match_all("/\/[*]{2}\//i", $val, $matches, PREG_OFFSET_CAPTURE)) {
                    $matchRes = $matches[0];
                }
                $splitSPos = $matchRes[0][1] + 4;
                $splitEPos = $matchRes[1][1];
                if ($bcount === $quesCount) {
                    $val = str_replace(subStr($val, $splitSPos, $splitEPos - $splitSPos), " 1=1 ", $val);
                    $getQryColArr[$key] = $val;
                } else {
                    if ($bcount < $quesCount) {
                        $fstr = "";
                        $kcount = ($quesCount - $bcount);
                        for ($k = 0;$k < $kcount;$k++) {
                            if ($k === ($kcount - 1)) {
                                $fstr.= "{" . $k . "}";
                            } else {
                                $fstr.= "{" . $k . "},";
                            }
                        }
                        $kakkoS = strpos($val, '(') + 1;
                        $kakkoE = strpos($val, ')');
                        $finalStr = subStr($val, $kakkoS, $kakkoE - $kakkoS);
                        $val = str_replace($finalStr, $fstr, $val);
                        $getQryColArr[$key] = $val;
                    }
                }
            } else if ($FLG === "BETWEEN") { //between
                e_log("BETWEEN CONDITION");
                $valRes = (isset($params[$pcount])) ? $params[$pcount] : "";
                $valRes1 = (isset($params[$pcount + 1])) ? $params[$pcount + 1] : "";
                if ($valRes === "" || $valRes1 === "") {
                    if (isset($params[$pcount])) {
                        $paramDeleteKey[] = $pcount;
                        $paramDeleteKey[] = $pcount + 1;
                    }
                    if (preg_match_all("/\/[*]{2}\//i", $val, $matches, PREG_OFFSET_CAPTURE)) {
                        $matchRes = $matches[0];
                    }
                    $splitSPos = $matchRes[0][1] + 4;
                    $splitEPos = $matchRes[1][1];
                    $val = str_replace(subStr($val, $splitSPos, $splitEPos - $splitSPos), " 1=1 ", $val);
                    $getQryColArr[$key] = $val;
                }
                $pcount++;
                $pcount++;
            } else { //simple
                $valRes = (isset($params[$pcount])) ? $params[$pcount] : "";
                $t = ($valRes === "");
                e_log("SIMPLE CONDITION" . $valRes); // || preg_match("/[A-Za-z0-9]+/i",$valRes)
                if ($valRes === "") {
                    if (isset($params[$pcount])) {
                        $paramDeleteKey[] = $pcount;
                    }
                    if (preg_match_all("/\/[*]{2}\//i", $val, $matches, PREG_OFFSET_CAPTURE)) {
                        $matchRes = $matches[0];
                    }
                    $splitSPos = $matchRes[0][1] + 4;
                    $splitEPos = $matchRes[1][1];
                    $val = str_replace(subStr($val, $splitSPos, $splitEPos - $splitSPos), " 1=1 ", $val);
                    $getQryColArr[$key] = $val;
                }
                $pcount++;
            }
        } else {
            $pcount+= $testQryColArr["postion"][$key];
        }
    }
    if ($spos < strlen($D1SQL)) {
        $getQryColArr[] = subStr($D1SQL, $spos);
    }
    $exeSQL = join(' ', $getQryColArr);
    //ブランクだたら、消すパラメータ
    if (count($paramDeleteKey) > 0) {
        foreach ($paramDeleteKey as $delv) {
            unset($params[$delv]);
        }
    }
    $params = array_values($params);
    /*sorting SQL parameter*/
    $sortArr = array();
    $spos1 = 0;
    $key1 = 1;
    while (($qpos1 = strpos($exeSQL, '}', $qpos1)) !== false) {
        $splitRes = subStr($exeSQL, $spos1, $qpos1 - $spos1);
        $splitRes = preg_replace("/\{\s*\w*\s*/i", ("{" . $key1 . "}"), $splitRes);
        error_log('after replace '.$splitRes);

        $sortArr[] = $splitRes;
        $qpos1 = $qpos1 + strlen("}");
        $spos1 = $qpos1;
        $key1++;
    }
    if ($spos1 < strlen($exeSQL)) {
        $sortArr[] = subStr($exeSQL, $spos1);
    }
    if (count($sortArr) > 0) {
        $exeSQL = join(' ', $sortArr);
    }
    //error_log("after cmRSQL" . $exeSQL);
    //error_log("after cmRPARA" . print_r($params, true));
    return array("strSQL" => $exeSQL, "params" => $params);
}
/**
 *get Field Column Array in query
 *
 */
function cmQueryFieldArr($D1SQL) {
    $paramPosition = array();
    $getQryColArr = array();
    $qpos = 0;
    $spos = 0;
    while (($qpos = strpos($D1SQL, '{', $qpos)) !== false) {
        $resStr = subStr($D1SQL, $spos, $qpos - $spos);
        if (strpos($resStr, "/**/")) {
            $qpos = $qpos + strpos(subStr($D1SQL, $qpos, strlen($D1SQL) - $qpos), "/**/") + strlen("/**/");
            $resStr = subStr($D1SQL, $spos, $qpos - $spos);
            $qpos = $qpos;
            $spos = $qpos;
            $paramPosition[] = "NO";
        } else {
            if (preg_match("/\bIN\b/i", strtoupper($resStr))) { //in
                $qpos = $qpos + strpos(subStr($D1SQL, $qpos, strlen($D1SQL) - $qpos), ")") + strlen(")");
                $resStr = subStr($D1SQL, $spos, $qpos - $spos);
                $qpos = $qpos;
                $spos = $qpos;
                $paramPosition[] = substr_count($resStr, '{');
            } else if (preg_match("/\bBETWEEN\b/i", strtoupper($resStr))) { //between
                $qpos = $qpos + strpos(subStr($D1SQL, $qpos, strlen($D1SQL) - $qpos), "}") + strlen("}");
                $qpos = $qpos + strpos(subStr($D1SQL, $qpos, strlen($D1SQL) - $qpos), "}") + strlen("}");
                $resStr = subStr($D1SQL, $spos, $qpos - $spos);
                $qpos = $qpos;
                $spos = $qpos;
                $paramPosition[] = substr_count($resStr, '{');
            } else {
                $qpos = $qpos + strpos(subStr($D1SQL, $qpos, strlen($D1SQL) - $qpos), "}") + strlen("}");
                $qpos = $qpos;
                $resStr = subStr($D1SQL, $spos, $qpos - $spos);
                $spos = $qpos;
                $paramPosition[] = 1;
            }
        }
        $getQryColArr[] = $resStr;
    }
    return array("getQryColArr" => $getQryColArr, "spos" => $spos, 'postion' => $paramPosition);
}
/**
 *get Field Column Condition Flg in query
 *
 */
function chkQueryFieldCondition($getQryColArr, $CARR) {
    $FLG = array();
    $FNCARR = $CARR;
    if (count($getQryColArr) > 0) {
        for ($i = 0;$i < count($getQryColArr);$i++) {
            $wherebyOne = $getQryColArr[$i];
            $bPattern = "/((\w)+\s*(\w)+\s*\{\s*[0-9]\s*\}\s*(\w)+\s*\{\s*[0-9]\s*\}\s*)/i"; //a between {1} and {2}
            $iPattern = "/((\w)+\s*(\w)+\s*\(\s*(\{\s*[0-9]\s*\}\s*(\,)*\s*)+\))/i"; //a in({1},...)
            if (preg_match($bPattern, $wherebyOne, $bmatches)) { //between
                $FLG[] = "BETWEEN";
                e_log("$bmatches[0]" . print_r($bmatches, true));
                $qP = strrpos(subStr($wherebyOne, strrpos($wherebyOne, "}"), strlen($wherebyOne) - strrpos($wherebyOne, "}")), "/**/"); //strrpos(subStr($bmatches[0],strrpos($bmatches[0],"}"),strlen($bmatches[0])-strrpos($bmatches[0],"}")),"/**/");
                for ($j = 0;$j < 2;$j++) {
                    $FNCARR[] = ($qP) ? 1 : 0;
                }
            } else if (preg_match($iPattern, $wherebyOne, $imatches)) { //in
                $FLG[] = "IN";
                $qC = substr_count($imatches[0], '{');
                $qP = strrpos(subStr($wherebyOne, strrpos($wherebyOne, "}"), strlen($wherebyOne) - strrpos($wherebyOne, "}")), "/**/"); //strrpos(subStr($imatches[0],strrpos($imatches[0],"}"),strlen($imatches[0])-strrpos($imatches[0],"}")),"/**/");
                for ($j = 0;$j < $qC;$j++) {
                    $FNCARR[] = ($qP) ? 1 : 0;
                }
            } else {
                $FLG[] = "SIMPLE";
                $qP = strrpos(subStr($wherebyOne, strrpos($wherebyOne, "}"), strlen($wherebyOne) - strrpos($wherebyOne, "}")), "/**/");
                $FNCARR[] = ($qP) ? 1 : 0;
            }
        }
    }
    return array("FLG" => $FLG, "CARR" => $FNCARR);
}
//testing $db2con, $FDB2CSV2['data'], $filename, $sSearch,'',$filtersearchData
function cmGetAllCount_1($db2con, $fdb2csv2, $filename, $search, $D1CFLG = '', $searchDrillList = array(), $filtersearchData = array()) {
    $data = array();
    $params = array();
    $isWhereFlg = false;
    $underscoreFlg = false;
    e_log("firstdatap1" . print_r($filtersearchData, true));
    if ($D1CFLG === '') {
        e_log("p1");
        // $strSQL = ' SELECT count(A.' . cmMer($fdb2csv2[0]['D2FLD']) . '_' . cmMer($fdb2csv2[0]['D2FILID']) . ') as COUNT ';
        $strSQL = ' SELECT count(*) as COUNT ';
        $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
        if ($search != '') {
            if (count($fdb2csv2) > 0) {
                $isWhereFlg = true;
                $strSQL.= ' WHERE ( ';
                foreach ($fdb2csv2 as $key => $value) {
                    switch ($value['D2TYPE']) {
                        case 'L':
                            $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . ' LIKE ? ';
                        break;
                        case 'T':
                            $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(8))' . ' LIKE ? ';
                        break;
                        case 'Z':
                            $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(26))' . ' LIKE ? ';
                        break;
                        default:
                            $strSQL.= cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
                        break;
                    }
                    array_push($params, '%' . $search . '%');
                    if ($key < (count($fdb2csv2) - 1)) {
                        $strSQL.= ' OR ';
                    }
                }
                $strSQL.= ' ) ';
            }
        }
        if (count($searchDrillList) > 0) {
            $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
            if(!$isWhereFlg) $isWhereFlg = true;

            $paramDSQL = array();
            foreach ($searchDrillList as $key => $value) {
                switch ($value['dtype']) {
                    case 'L':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
                    break;
                    case 'T':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
                    break;
                    case 'Z':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
                    break;
                    default:
                        $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
                    break;
                }
                $params[] = cmMer($value["val"]);
            }
            $strSQL.= join(' AND ', $paramDSQL);
            $strSQL.= ' ) ';
        }
        if (count($filtersearchData) > 0) {
            foreach ($fdb2csv2 as $key => $value) {
                if (strpos($value['D2FLD'], '_') !== false) {
                    //underscore SIGN FOUND
                    $underscoreFlg = true;
                }
            }
            if ($underscoreFlg === true) {
                e_log("result ... TRUE");
                $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
                $paramDSQL = array();
                foreach ($filtersearchData as $key => $value) {
                    list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                    e_log("testdata" . $D1COL1 . $D1COL2);
                    switch ($value['dtype']) {
                        case 'L':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                        case 'T':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                        case 'Z':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                        default:
                            $paramDSQL[] = $D1COL2 . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                    }
                    $params[] = cmMer($value['D1VAL']);
                }
                $strSQL.= join(' AND ', $paramDSQL);
                $strSQL.= ' ) ';
            } else {
                e_log("result ... FALSE");
                $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
                $paramDSQL = array();
                foreach ($filtersearchData as $key => $value) {
                    list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                    e_log("testdata" . $D1COL1 . $D1COL2);
                    switch ($value['dtype']) {
                        case 'L':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                        case 'T':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                        case 'Z':
                            $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                        default:
                            $paramDSQL[] = $D1COL2 . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                        break;
                    }
                    $params[] = cmMer($value['D1VAL']);
                }
                $strSQL.= join(' AND ', $paramDSQL);
                $strSQL.= ' ) ';
            }
        }
    } else {
        $isWhereFlg = false;
        //$strSQL = ' SELECT count(A.' . cmMer($fdb2csv2[0]['D2FLD']) . ') as COUNT ';
        $strSQL = ' SELECT count(*) as COUNT ';
        $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
        if ($search != '') {
            $rs = fngetSQLFldCCSID($db2con, $filename);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result']);
            } else {
                //e_log('fldCCSIDresult'.print_r($rs,true));
                $fldCCSIDARR = $rs['data'];
                $fldccsidCnt = $rs['CCSIDCNT'];
                if ($fldccsidCnt > 0) {
                    $fldArr = array();
                    $isWhereFlg = true;
                    foreach ($fldCCSIDARR as $fldccsid) {
                        switch ($fldccsid['DDS_TYPE']) {
                            case 'L':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(10) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL.= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(10))' . ' LIKE ? ';
                                }
                            break;
                            case 'T':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(8) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL.= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(8))' . ' LIKE ? ';
                                }
                            break;
                            case 'Z':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(26) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL.= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(26))' . ' LIKE ? ';
                                }
                            break;
                            default:
                                if ($fldccsid['CCSID'] === '65535') {
                                    if ($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW') {
                                        $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(4) CCSID 5026) ' . ' LIKE ? ';
                                    } else {
                                        $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) ' . ' LIKE ? ';
                                    }
                                } else {
                                    $fldArr[] = $fldccsid['COLUMN_NAME'] . ' LIKE ? ';
                                }
                            break;
                        }
                    }
                } else {
                    if (count($fdb2csv2) > 0) {
                        $isWhereFlg = true;
                        $strSQL.= ' WHERE ( ';
                        foreach ($fdb2csv2 as $key => $value) {
                            switch ($value['D2TYPE']) {
                                case 'L':
                                    $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(10))' . ' LIKE ? ';
                                break;
                                case 'T':
                                    $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(8))' . ' LIKE ? ';
                                break;
                                case 'Z':
                                    $strSQL.= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(26))' . ' LIKE ? ';
                                break;
                                default:
                                    $strSQL.= cmMer($value['D2FLD']) . ' LIKE ? ';
                                break;
                            }
                            array_push($params, '%' . $search . '%');
                            if ($key < (count($fdb2csv2) - 1)) {
                                $strSQL.= ' OR ';
                            }
                        }
                        $strSQL.= ' ) ';
                    }
                }
            }
        }
        if (count($searchDrillList) > 0) {
            $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
            if(!$isWhereFlg) $isWhereFlg = true;
            $paramDSQL = array();
            foreach ($searchDrillList as $key => $value) {
                switch ($value['dtype']) {
                    case 'L':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
                    break;
                    case 'T':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
                    break;
                    case 'Z':
                        $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
                    break;
                    default:
                        $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
                    break;
                }
                $params[] = cmMer($value["val"]);
            }
            $strSQL.= join(' AND ', $paramDSQL);
            $strSQL.= ' ) ';
        }
        if (count($filtersearchData) > 0) {
            $strSQL.= ($isWhereFlg) ? ' AND ( ' : ' WHERE ( ';
            $paramDSQL = array();
            foreach ($filtersearchData as $key => $value) {
                list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                switch ($value['dtype']) {
                    case 'L':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                    break;
                    case 'T':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(8))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                    break;
                    case 'Z':
                        $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                    break;
                    default:
                        $paramDSQL[] = $D1COL2 . '_' . $D1COL1 . $value['D1OPERATOR'] . ' ? ';
                    break;
                }
                $params[] = cmMer($value['D1VAL']);
            }
            $strSQL.= join(' AND ', $paramDSQL);
            $strSQL.= ' ) ';
        }
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        e_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        e_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true, 'data' => $data[0]);
        }
    }
    return $data;
}
function cmGetDB2GPK($db2con,$GPKQRY){

    $data = array();

    $strSQL  = ' SELECT A.GPKID,A.GPKNM ';
    $strSQL .= ' FROM DB2GPK as A ';
    $strSQL .= ' WHERE GPKQRY = ? ';

    $params = array(
        $GPKQRY
    );
    error_log("cmGetDB2GPK 20180919 => ".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if (count($data) === 0) {
                $data = array('result' => 'NOTEXIST_GRAPH');
            } else {
                $data = array('result' => true, 'data' => umEx($data));
            }
        }
    }
    return $data;
}

function cmGetWUUBFG($db2con, $WUUID){// for お気に入り　icon  
    
    $data = array();
    
    $params = array(
        $WUUID
    );
    
    $strSQL = ' SELECT B.WUUQFG,B.WUUBFG ';
    $strSQL .= ' FROM DB2WUSR AS B';
    $strSQL .= ' WHERE WUUID = ? ';
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    return $data;
}
/***
*全部のユーザーを取得
*
*/
function cmGetAllUser($db2con,$QRYNM){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.WUUID ';
    $strSQL .= ' FROM DB2WUSR as A ';
    $strSQL .= ' WHERE WUUID <> \'\' ';
   
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
            	$data = array('result' => true,'data'=> umEx($data));
            }
        }
    }
    return $data;

}
/*【挿入】DB2WDEF
*グループ権限付与時、ダウンロード権限自動付与
**/
function cmInsQryKengen($db2con,$usrNm,$qryNm,$csvFlg,$excelFlg){
    $strSQL = '';
    $data   = array();
    $strSQL .= ' INSERT INTO DB2WDEF ';
    $strSQL .= '        ( ';
    $strSQL .= '            WDUID, ';
    $strSQL .= '            WDNAME, ';
    $strSQL .= '            WDDWNL, ';
    $strSQL .= '            WDDWN2  ';
    $strSQL .= '        ) ';
    $strSQL .= ' VALUES ( ';
    $strSQL .= '            ?,?,?,?';
    $strSQL .= '        ) ';
    $params =array(
                $usrNm,
                $qryNm,
                $csvFlg,
                $excelFlg 
            );
    //error_log("insQryNameInGrp GG => ".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
            error_log('ERROR = '.db2_stmt_errormsg());

        $data = array(
                    'result' => 'FAIL_INS'.db2_stmt_errormsg(),
                    'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            error_log('ERROR = '.db2_stmt_errormsg());
            $data = array(
                    'result' => 'FAIL_INS'.db2_stmt_errormsg(),
                    'errcd'  => 'DB2WDEF:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*【更新】DB2WDEF
*グループ権限付与時、ダウンロード権限自動付与
**/
function cmUpdQryKengen($db2con,$usrNm,$qryNm,$csvFlg,$excelFlg){
    $strSQL = '';
    $data   = array();
   
    $strSQL .= ' UPDATE DB2WDEF ';
    $strSQL .= ' SET ';
    $strSQL .= '    WDDWNL      = ? , ';
    $strSQL .= '    WDDWN2    = ? ';
    $strSQL .= ' WHERE  ';
    $strSQL .= '    WDUID      = ? ';
    $strSQL .= ' AND  ';
    $strSQL .= '    WDNAME      = ? ';
    
    $params =array(
                $csvFlg,
                $excelFlg,
                $usrNm,
                $qryNm
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true,'data'=> umEx($data));
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* グループ権限付与時、ダウンロード権限自動付与
*-------------------------------------------------------*
*/

function cmGetUsrName($db2con,$grpId){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT WUUID ';
    $strSQL .= ' FROM DB2WUGR  ' ;
    $strSQL .= ' WHERE WUGID = ? ' ;

     $params = array(
        $grpId
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true,'data'=> umEx($data));
            }
        }
    }
    return $data;

}

function cmGetQryName($db2con,$grpId){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT WGNAME ';
    $strSQL .= ' FROM DB2WGDF  ' ;
    $strSQL .= ' WHERE WGGID = ? ' ;

     $params = array(
        $grpId
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true,'data'=> umEx($data));
            }
        }
    }
    return $data;
}
function cmGetQryNameFromDB2WDEF($db2con,$usrNm,$qryNm){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT WDNAME ';
    $strSQL .= ' FROM DB2WDEF  ' ;
    $strSQL .= ' WHERE WDUID = ? AND WDNAME = ? ' ;

     $params = array(
        $usrNm,
        $qryNm
    );
    //error_log("params data@@@@".print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true,'data'=> umEx($data));
            }
        }
    }
    return $data;
}
function cmChkQryKenGen($db2con,$usrId,$qryId){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT WGNAME ';
    $strSQL .= '    FROM DB2WGDF  AS A' ;
    $strSQL .= '    LEFT JOIN DB2WUGR AS B ON A.WGGID = B.WUGID ';
    $strSQL .= '    WHERE B.WUUID = ? and A.WGNAME = ?';    

     $params = array(
        $usrId,
        $qryId
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true,'data'=> umEx($data));
            }
        }
    }
    return $data;
}
function cmInsertDB2WSTL($db2con, $wlday, $wltime, $wluid, $jktime, $qrycnt) {
    $data = array();
    $params = array();

    $remote_addr = $_SERVER['REMOTE_ADDR'];//mppn
    $user_agent = $_SERVER['HTTP_USER_AGENT'];//mppn
    $IP_browserinfo = $remote_addr . " " . $user_agent;//mppn

    //e_log('ip info'. $IP_browserinfo);
    //e_log('IP 20190531 bottom'.print_r($_SERVER,true));

    $strSQL = ' INSERT INTO DB2WSTL ';
    $strSQL.= ' ( ';
    $strSQL.= ' WLDAY, ';
    $strSQL.= ' WLTIME, ';
    $strSQL.= ' WLUID, ';
    $strSQL.= ' WLJKTM, ';
    $strSQL.= ' WLQCNT, ';
    $strSQL.= ' WLIPBI ';
    $strSQL.= ' ) ';
    $strSQL.= ' VALUES ';
    $strSQL.= ' (?,?,?,?,?,?) ';
    $params = array($wlday, $wltime, $wluid, $jktime, $qrycnt, $IP_browserinfo);

    $stmt = db2_prepare($db2con, $strSQL);

    if ($stmt === false) {
        error_log('error = '.db2_stmt_errormsg());
        $rs = '1';
    } else {
        $r = db2_execute($stmt, $params);
        error_log('error = '.db2_stmt_errormsg());

        if ($r === false) {
            $rs = '1';
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 *
 *-------------------------------------------------------*
*/
function cmUpdDB2QHIS($db2con, $DQNAME, $DQTMAX, $DQTMIN, $DQAVG) {
    $rs = 0;
    $strSQL = ' UPDATE DB2QHIS AS A SET ';
    $strSQL.= ' A.DQTMAX =  ?, ';
    $strSQL.= ' A.DQTMIN =  ?, ';
    $strSQL.= ' A.DQTAVG =  ?  ';
    $strSQL.= ' WHERE A.DQNAME = ? ';
    $params = array($DQTMAX, $DQTMIN, $DQAVG, $DQNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        e_log("err" . db2_stmt_errormsg(), '1');
        $rs = 1;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 1;
        }
    }
    return $rs;
}