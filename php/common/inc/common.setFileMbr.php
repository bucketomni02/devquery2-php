<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : 共通
* PROGRAM NAME   : 共通ファイルマンバーセット
* PROGRAM ID     : common.setFileMbr.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/02/29
* MODIFY DATE    : 
* ============================================================
**/

/*function setFileMBR($db2con,$lib,$fil,$mbr,$tmpFilnm){
    $res = true;
    /*e_log('データ取得一回目');
    getData($db2con,$lib,$fil);
    //IBMi接続（データベース接続と共有）
    $conn = ToolkitService::getInstance($db2con, DB2_I5_NAMING_ON);
    $conn->setToolkitServiceParams(array('stateless' => true));

    //CCSIDをセット
    $conn->CLCommand('OVRDBF FILE('.$tmpFilnm.') TOFILE('.$lib.'/'.$fil.') MBR('.$mbr.') OVRSCOPE(*JOB)');
    e_log('データ取得二回目');
    getData($db2con,$lib,$tmpFilnm);
    return $res;*/
    /*$strSQL = ' CREATE ALIAS QTEMP.'.$tmpFilnm.'  FOR ';
    if($lib === '*LIBL' || $lib === ''){
        $strSQL .= $fil;
    }else{
         $strSQL .= $lib .'.'.$fil;
    }
    if($mbr !== ''){
        $strSQL .= ' ( '. $mbr .')' ;
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        e_log('prepare失敗');
        e_log(db2_stmt_errormsg());
        $res = false;
    }else{

        $rs = db2_execute($stmt);

        if($rs === false){
            e_log('execute失敗');
            e_log(db2_stmt_errormsg());
            $res = false;
        }else{
    /*
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            echo(print_r($data));
            getData
    */
          /*  e_log('1回目データ取得');
            getData($db2con,$lib,$tmpFilnm);
        }

    }
    return $res;
}*/
function setFileMBR($db2con,$lib,$fil,$mbr,$tmpFilnm,&$strSQL = ''){
    $res = true;
    $strSQL = ' CREATE OR REPLACE ALIAS QTEMP/'.$tmpFilnm.'  FOR ';
    if($lib === '*LIBL' || $lib === ''){
        $strSQL .= $fil;
    }else{
         $strSQL .= $lib .'/'.$fil;
    }
    if(cmMer($mbr) !== ''){
        $strSQL .= ' ( '. $mbr .')' ;
    }
    e_log("setFileMBR".$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $res = false;
        e_log('メンバーセット準備エラー：'.db2_stmt_errormsg().'メンバーセット：'.$strSQL);
    }else{
        $rs = db2_execute($stmt);

        if($rs === false){
            $res = false;
        }/*else{
            //e_log('1回目データ取得'.$tmpFilnm);
            getData($db2con,$lib,$tmpFilnm);
        }*/

    }
    return $res;
}
function dropFileMBR($db2con,$lib,$fil,$mbr,$tmpFilnm){
    $res = true;
   /* $strSQL = ' DROP ALIAS QTEMP/'.$tmpFilnm ;
    $stmt = @db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $res = false;
        e_log('準備：'.db2_stmt_errormsg());
    }else{
        $rs = @db2_execute($stmt);
        if($rs === false){
            $res = false;
            e_log('実行：'.db2_stmt_errormsg());
        }
    }*/

    return $res;
}
function fnGetFilLast($RDB,$LIBLLST,$LIBL,$TBLNM){
    if($RDB === '' || $RDB === RDB || $RDB === RDBNAME){
        $db2Con = cmDb2ConLib($LIBLLST);
    }else{
        $db2Con = cmDb2ConLib($LIBLLST);
    }
    $mbrnm = '';
    // メンバーデータ取得SQL
    $strSQL  = '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     A.SYS_DNAME, ';
    $strSQL .= '     A.SYS_TNAME, ';
    $strSQL .= '     A.SYS_MNAME, ';
    $strSQL .= '     A.PARTNBR, ';
    $strSQL .= '     A.LABEL ';
    $strSQL .= ' FROM ';
    $strSQL .= '     QSYS2/SYSPSTAT A ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.TABLE_SCHEMA = ? AND ';
    $strSQL .= '     A.TABLE_NAME = ? AND ';
    $strSQL .= '     A.PARTNBR = (SELECT ';
    $strSQL .= '                     MAX (B.PARTNBR) ';
    $strSQL .= '                 FROM ';
    $strSQL .= '                     QSYS2/SYSPSTAT B ';
    $strSQL .= '                 WHERE ';
    $strSQL .= '                     B.TABLE_SCHEMA = ? AND ';
    $strSQL .= '                     B.TABLE_NAME = ? ';
    $strSQL .= '                 ) ';
    $stmt = db2_prepare($db2Con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        //e_log(db2_stmt_errormsg().'メンバーデータ取得：'.$strSQL);
    }else{
        if($LIBL === '' || $LIBL === '*LIBL' ){
            $libArr = preg_split ("/\s+/", $LIBLLST );
            foreach($libArr as $lib){
                $data = array();
                $params = array($lib,$TBLNM,$lib,$TBLNM);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                    //e_log(db2_stmt_errormsg().'メンバーデータ取得：'.$strSQL.print_r($params,true));
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    if(count($data) === 1){
                        $mbrnm = $data[0]['SYS_MNAME'];
                        break;
                    }
                }
            }
        }else{
            $data = array();
            $params = array($LIBL,$TBLNM,$LIBL,$TBLNM);
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
                //e_log(db2_stmt_errormsg().'メンバーデータ取得：'.$strSQL.print_r($params,true));
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                if(count($data) > 0){
                    $mbrnm = $data[0]['SYS_MNAME'];
                }
            }
        }
    }
    cmDb2Close($db2Con);
    return $mbrnm;
}
function getData($db2con,$LIBL,$TBLNM){
    $data = array();
    $strSQL = '';
    $strSQL .= ' SELECT ';
    $strSQL .= ' * ';
    $strSQL .= ' FROM ';
    if($LIBL === ''|| $LIBL === '*LIBL' || $LIBL === '*USRLIBL'){
        $strSQL .= 'QTEMP/'.$TBLNM;
    }else{
        $strSQL .= 'QTEMP/'.$TBLNM;
    }

    $stmt = db2_prepare($db2con, $strSQL);
    if($stmt === false){
        e_log('データ取得エラー'.db2_stmt_errormsg().$strSQL);
    }else{

        $ret = db2_execute($stmt);

        if($ret === false){
            e_log(db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            e_log('データ取得結果：'.$TBLNM.print_r($data,true));
        }
    }
}
