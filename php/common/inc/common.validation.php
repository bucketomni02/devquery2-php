<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common.message.php");
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
/*
* 必須チェック
*/
function comChkReq($field,$exp){
    $res = 0;
    if($field === ''){
        $res = showMsg('FAIL_REQ',array($exp['NAME']));
    }
    return $res;
}
/*
* 半角チェック
*/
function comChkByte($field,$exp){
    $res = 0;
    $byte = checkByte($field);
    if($byte[1] > 0){
        $res = showMsg('FAIL_BYTE',array($exp['NAME']));
    }
    return $res;
}
/*
* 桁数チェック
*/
function comChkLength($field,$exp){
    $res = 0;
    if(!checkMaxLen($field,$exp['MAXLEN'])){
        $res = showMsg('FAIL_MAXLEN',array($exp['NAME']));
    }
    return $res;
}
/*
* 整数チェック
*/
function comChkNum($field,$exp){
    $res = 0;
    if(checkNuturalNum($field) == false){
		error_log('checking ******'.checkNuturalNum($field));
        $res = showMsg('CHK_FMT',array($exp['NAME'],$exp['PMSG']));
    }
    return $res;
}
/*
*
*/
function comChkValidChar($field,$exp){
    $res = 0;
    //if (preg_match('/[^0-9A-Z_#@]+/', $D1NAME)) {
    if (preg_match($exp[REXP], $field) == false) {
        
        $res = showMsg('FAIL_VLDD',array($exp['NAME'],$exp['PMSG']));
    }
    return $res;
}
/*
* バリデーションチェック
*/
function comValidate($field,$exp,$chkReq,$chkB,$chkLen,$chkN,$chkVC){
    $rtn = 0;
    $msg = '';
    $rs = 0;
    if($rtn === 0){
        if($chkReq){
            $rs = comChkReq($field,$exp);
            if($rs !== 0){
                $msg = $rs;
                $rtn = 1;
            }
        }
    }
    if($rtn === 0){
        if($chkB){
            $rs = comChkByte($field,$exp);
            if($rs !== 0){
                $msg = $rs;
                $rtn = 1;
            }
        }
    }
    if($rtn === 0){
        if($chkLen){
            $rs = comChkLength($field,$exp);
            if($rs !== 0){
                $msg = $rs;
                $rtn = 1;
            }
        }
    }
    if($rtn === 0){
        if($chkN){
            $rs = comChkNum($field,$exp);
            if($rs !== 0){
                $msg = $rs;
                $rtn = 1;
            }
        }
    }
    if($rtn === 0){
        if($chkVC){
            $rs = comChkValidChar($field,$exp);
            if($rs !== 0){
                $msg = $rs;
                $rtn = 1;
            }
        }
    }
    $rtndata = array(
        'RTN' => $rtn,
        'MSG' => $msg
    );
    return $rtndata;
}