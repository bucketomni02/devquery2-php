<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : 共通
* PROGRAM NAME   : 共通日付、時間、タイムスタンプ
* PROGRAM ID     : common.validationLTZ.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/02/29
* MODIFY DATE    : 
* ============================================================
**/
// 日付チェック
function chkVldDate($value){
    $rtn = 0;
    $varSplit = '';
    if(cmMer($value) === ''){
        $rtn = $value;
    }else{
        if(strlen($value)>6){
            switch($value[4]){
                case '/':
                    $varSplit = '/';
                break;
                case '-':
                    $varSplit = '-';
                break;
                default :
                    if(strlen($value) === 8){
                        $subStr = $value;
                        $value =  substr($subStr, 0, 4).'-'.substr($subStr, 4, 2).'-'.substr($subStr, 6, 2);
                        $varSplit = '-';
                    }else{
                        $rtn = 1;
                    }
                break;
            }
        }else{
            $rtn = 1;
        }
        if($rtn === 0){
            $valArr = explode ( $varSplit ,$value);
            if(count($valArr) === 3){
                if(strlen($valArr[0]) !== 4 ){
                    $rtn = 1;
                }else{
                    if($varSplit === '/'){
                        $varSplit = '-';
                    }
                    $dateVal = join( $varSplit ,$valArr);
                }
            }else{
                $rtn = 1;
            }
        }
        if($rtn === 0){
            $dateVal = '\''.$dateVal.'\'';
            $rtn = chkDate($dateVal);
        }
    }
    return $rtn;
}
function chkDate($dateVal){
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $res = 0;
    $data   = array();
    $params = array();
    $strSQL = ' SELECT DATE('.$dateVal.') AS RES';
    $strSQL .= ' FROM SYSIBM/SYSDUMMY1 ';
    $params = array(
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $res = 1;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $res = 1;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if(count($data)>0){
                $data = $data[0]['RES'];
                if(is_null($data) || $data === '' || $data === '0001-01-01'){
                    $res = 1;
                }else {
                    $res = $data;
                }
            }else{
                $res = 1;
            }
        }
    }
    return $res;
}
// 時刻チェック
function chkVldTime($value){
    $rtn = 0;
    $varSplit = '.';
    if(cmMer($value) === ''){
        $rtn = $value;
    }else{
        $pos = strpos($value,$varSplit);
        if($pos === false){
            $varSplit = ':';
            $pos = strpos($value,$varSplit);
        }
        if($pos === false){
            $rtn = 1;
        }
        if($rtn === 0){
            $valArr = explode ( $varSplit ,$value);
            if(count($valArr) === 3){
                $varSplit = '.';
                foreach ($valArr as $key => $val){
                    $valArr[$key] = str_pad($val, 2, "0", STR_PAD_LEFT);
                }
                $dateVal = join( $varSplit ,$valArr);
            }else{
                $rtn = 1;
            }
        }
        if($rtn === 0){
            $dateVal = '\''.$dateVal.'\'';
            $rtn = chkTime($dateVal);
        }
    }
    return $rtn;
}

function chkTime($timeVal){
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $res = 0;
    $data   = array();
    $params = array();
    $strSQL = ' SELECT Time('.$timeVal.') AS RES';
    $strSQL .= ' FROM SYSIBM/SYSDUMMY1 ';
    $params = array(
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $res = 1;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $res = 1;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if(count($data)>0){
                $data = $data[0]['RES'];
                if(is_null($data) || $data === '' || $data === '00.00.00'){
                    $res = 1;
                }else {
                    $res = $data;
                }
            }else{
                $res = 1;
            }
        }
    }
    return $res;
}

// タイムスタンプチェック
function chkVldTimeStamp($value){
    $rtn = 0;
    if(cmMer($value) === ''){
        $rtn = $value;
    }else{
        if($rtn === 0){
            $timeStampVal = '\''.$value.'\'';
            $rtn = chkTimeStamp($timeStampVal);
        }
    }
    return $rtn;
}
// タイムスタンプチェック
function chkTimeStamp($timeStampVal){
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $res = 0;
    $data   = array();
    $params = array();
    $strSQL = ' SELECT TIMESTAMP('.$timeStampVal.') AS RES';
    $strSQL .= ' FROM SYSIBM/SYSDUMMY1 ';
    $params = array(
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $res = 1;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $res = 1;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if(count($data)>0){
                $data = $data[0]['RES'];
                if(is_null($data) || $data === '' || $data === '0001-01-01-00.00.00.000000'){
                    $res = 1;
                }else {
                    $res = $data;
                }
            }else{
                $res = 1;
            }
        }
    }
    return $res;
}
