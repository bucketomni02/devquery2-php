<?php
//画面にエラーを表示しない
ini_set('display_errors', 0 );
//メモリ制限
ini_set('memory_limit', -1);
//出力エラーの種類
ini_set('error_reporting', E_ALL & ~E_NOTICE);
//PHPスクリプトの実行時間制限
ini_set("max_execution_time",0);

//DB接続設定
define('RDB','S6515E9A');
define('RDB_USER','');
define('RDB_PASSWORD','');
define('RDBNAME','S6515E9A');
define('RDB_KEY','OSCPHPQUERY');//暗号化のキー

//ASメインライブラリ
define('MAINLIB','DEVQUERY2');
define('CLLIB','DEVQUERYW');

// QSYS2/SYSCOLUMN2ビューのライブラリーとビュー名
define('SYSCOLUMN2LIB','QSYS2');
define('SYSCOLUMN2VIEW','SYSCOLUMN2');
// QSYS2にSYSCOLUMN2ビューがない場合
//define('SYSCOLUMN2',SYSCOLUMN2LIB.'/'.SYSCOLUMN2VIEW);
// QSYS2にSYSCOLUMN2ビューがある場合
define('SYSCOLUMN2','QSYS2/SYSCOLUMN2');

// SYSSCHEMASのビューのライブラリー
define('SYSSCHEMASLIB','QSYS2');
//IP
define('IP', $_SERVER['HTTP_HOST']);
//メールのリンク
define('SENDMAILIP', '192.168.1.7:10080');
//プロトコル
define('PROTOCOL', 'http');
//システム名
define('SISTEM','devquery2');
//TOUCH版システム名
define('SISTEMTOUCH','phpquery2touch');
//保存ディレクトリ(phpquery2を入れるディレクトリのパス)
define('SAVE_DIR','/www/zendsvr6/htdocs/');

//一時DB保管ライブラリ
define('SAVE_DB','DEVQUERYH');

//SAVE_DIR+SISTEM(phpquery2のパス)
define('BASE_DIR',SAVE_DIR.SISTEM);
//PROTOCOL+IP+SISTEM(phpquery2のURL)
define('BASE_URL',PROTOCOL.'://'.IP.'/'.SISTEM.'/');

//ダウンロード一時保管ディレクトリ
define('TEMP_DIR',BASE_DIR.'/php/downloadtmp/');
//Excelテンプレート保管ディレクトリ
define('ETEMP_DIR',BASE_DIR.'/php/template/');
//PHPのディレクトリ
define('PHP_DIR',BASE_DIR.'/php/');

//QTEMPに一時テーブル作成して抽出
// 0 : 作成しない。
// 1 : 作成する。
define('CRTQTEMPTBL_FLG',0);

//ログアウト時にwindow.close
// 1 : closeする
define('LOGOUTCLOSE',0);

//QCMDEXE対応:1 対応無し:0
define('QCMDEXC',1);

define('NOTOOLKIT',1);


// DBの接続生成
$user     = RDB_USER;
$password = RDB_PASSWORD;
$database = RDB;

if(QCMDEXC === 1){

    $option   = array(
        'i5_naming'=>DB2_I5_NAMING_ON
    );

    $db2config = db2_connect($database, $user, $password, $option);
    if($db2config){
        $strCMD = "CHGLIBL LIBL(".MAINLIB.")";
        $lenCMD = strlen($strCMD);
        $lenCMD = str_pad($lenCMD,10,'0',STR_PAD_LEFT).'.00000';
        $strSQL = " call QSYS2/QCMDEXC('$strCMD',$lenCMD) ";
        db2_exec($db2config,$strSQL);
    }

}else{

    $option   = array(
        'i5_naming'=>DB2_I5_NAMING_ON,
        'i5_libl' => MAINLIB
    );

    $db2config = db2_connect($database, $user, $password, $option);

}

if($db2config){
    $cfgdata = array();
    $configdata = array();
    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2COF AS A ' ;
    $stmt = db2_prepare($db2config,$strSQL);
    if($stmt === false){
        error_log('prepare error');
    }else{
        $r = db2_execute($stmt,array());
        if($r === false){
            error_log('execute error');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                foreach($row as $key => $val){
                    $row[$key] = mb_ereg_replace(" +$", "", $val);
                }
                $configdata[] = $row;
            }
        }
    }
    $cfgdata = $configdata[0];
    //セッション時間制限
    ini_set('session.gc_maxlifetime', (int)$cfgdata['COMTIM']);
    //PHPスクリプトの実行時間制限
    set_time_limit((int)$cfgdata['COFTML']);
    //出力エクセルバージョン => '2007' or '2003' or 'html'
    $cofexv = '';
    switch($cfgdata['COFEXV']){
        case '2' : $cofexv = '2003';
                     break;
        case '3' : $cofexv = '2007';
                     break;
        case '4' : $cofexv = 'XLSX';
                     break;
        case '1' :
        default : $cofexv = 'html';
                    break;
    }
    define('EXCELVERSION',$cofexv);

    //エクセルダウンロードに編集コード＝＞ぜーろ の場合は利用する、１の場合は利用しない
    define('EXCELHENSHUU',$cfgdata['COFHEN']);

    //ピボットの結合（０の場合はする、１の場合はしない）
    define('JOINPIV',$cfgdata['COFPIV']);

    //ＣＳＶファイルに編集コード＝＞ぜーろ の場合は利用する、１の場合は利用しない
    define('CSVHENSHUU',$cfgdata['COFCSV']);

    //ＨＴＭＬ形式に編集コード＝＞ぜーろ の場合は利用する、１の場合は利用しない
    define('HTMDHENSHUU',$cfgdata['COFHTM']);

    //ＨＴＭＬメールファイルに編集コード＝＞ぜーろ の場合は利用する、１の場合は利用しない
    define('HTMFHENSHUU',$cfgdata['COFHTF']);

    //条件の文言
    define('CND_EQ',$cfgdata['COFCEQ']);
    define('CND_NE',$cfgdata['COFCNE']);
    define('CND_GT',$cfgdata['COFCGT']);
    define('CND_LT',$cfgdata['COFCLT']);
    define('CND_GE',$cfgdata['COFCGE']);
    define('CND_LE',$cfgdata['COFCLE']);
    define('CND_LIKE',$cfgdata['COFCLK']);
    define('CND_LLIKE',$cfgdata['COFLLK']);
    define('CND_RLIKE',$cfgdata['COFRLK']);
    define('CND_NLIKE',$cfgdata['COFNLK']);
    define('CND_LNLIKE',$cfgdata['COFLNLK']);
    define('CND_RNLIKE',$cfgdata['COFRNLK']);
    define('CND_RANGE',$cfgdata['COFRNG']);
    define('CND_LIST',$cfgdata['COFCLST']);
    define('CND_NLIST',$cfgdata['COFNLST']);
    define('CND_IS',$cfgdata['COFCIS']);
    //テーブル表示行数
    define('DSPROW1',$cfgdata['COFRO1']);
    define('DSPROW2',$cfgdata['COFRO2']);
    define('DSPROW3',$cfgdata['COFRO3']);
    define('DSPROW4',$cfgdata['COFRO4']);
    define('CND_PVFLG',$cfgdata['COPVFLG']);//ピボット画面の表示例フラグ
    define('CND_SRSV',$cfgdata['COSRSV']);//検索条件を保存フラグ
	define('CND_MAXQRY',$cfgdata['COQRYLMT']);//クエリーの最大件数の取得
    define('CND_QRYKG',$cfgdata['COKGFLG']);//クエリー作成時にダウンロード権限を付与
    define('CND_QRYGD',$cfgdata['COGDFLG']);//グループ権限付与時、ダウンロード権限自動付与
    define('CND_COFCLR',$cfgdata['COFCLR']);//配信時のクロス集計の見出し色
 
    //テーブル初期表示件数(テーブル表示件数で指定している中から選択)
    $COFIRW = '';
    switch($cfgdata['COFIRW']){
        case '1' : $COFIRW = $cfgdata['COFRO1'];
                    break;
        case '2' : $COFIRW = $cfgdata['COFRO2'];
                    break;
        case '3' : $COFIRW = $cfgdata['COFRO3'];
                    break;
        case '4' : 
        default : $COFIRW = $cfgdata['COFRO4'];
                    break;
    }
    define('INTDSPROW',$COFIRW);

    //JavascripAjaxのタイムアウト
    define('JSTIMEOUT',$cfgdata['COFTOT'].'000');    //default 30second

    //パスワード有効期限（例：10日有効なら10を入れる）ブランクは無制限
    $COFLGD = '';
    if($cfgdata['COFLGD'] !== ''){
        $COFLGD = (int)$cfgdata['COFLGD'];
    }
    define('LOGINDAY',$COFLGD);

    //HTMLメールに出力する結果データの最大表示行数
    define('HTMLMAXROW',$cfgdata['COFHMR']);

    //ログインのパスワード間違えが許容される回数。ブランクは無制限
    $COFLCT = '';
    if($cfgdata['COFLCT'] !== ''){
        $COFLCT = (int)$cfgdata['COFLCT'];
    }
    define('LOGINCOUNT',$COFLCT);

    //URL直接実行のパラメータの種類('POST'or'HASH')
    $caxes = '';
    if($cfgdata['COFAXP']  === '1'){
        $caxes = 'POST';
    }else{
        $caxes = 'HASH';
    }
    define('AXESPARAM',$caxes);

    //数値書式設定
    define('CONSTL',$cfgdata['CONSTL']);

    //小数値の前ゼロの非表示設定(1なら非表示)
    define('ZERO_FLG',$cfgdata['COFZFG']);

    //小数部分の連続する後ろ0を削除する設定(0なら削除)
    define('BACKZERO_FLG',$cfgdata['COFBZF']);

	//クエリー一覧から実行する際に別ウィンドウ/画面遷移 
    define('COFJWF',$cfgdata['COFJWF']);
    //エラーログ出力コントロール
    //'1'→ログ全部出力　、'0'→本番用だけ出力
    define('ERRLOG', '1' );

    //BASEのWEB版の定数
    /******************************/
    //参照ファイルの最大数
    define('REFFILEMAX',(int)$cfgdata['CORFIM']);
    //参照ファイルの結合フィールドの最大数
    define('REFFLDMAX',(int)$cfgdata['CORFFM']);
    //出力できるフィールドの最大数
    define('FLDCNTMAX',(int)$cfgdata['COROUT']);
    //設定できる条件の最大数
    define('CNDCNTMAX',(int)$cfgdata['COCNDS']);
    //ライブラリリストの最大数
    define('LIBLMAX',(int)$cfgdata['COLIBM']);
    //ピボットの集計セルの色 ０＝＞色なし、１＝＞色付く
    define('PIVSCOLR',(int)$cfgdata['COFPVC']);
    //ダウンロードのループカウント
    define('DLOOPC',(int)$cfgdata['COFMEM']);

    // ユーザーにその他権限設定チェック
    define('USROTHAUTFLG',(int)$cfgdata['COOAFLG']);

    // クエリ実行制限
    // クエリー実行時間制限
    define('QRYLMTTIME',(int)$cfgdata['COQRYT']);
    // クエリー実行メモリー制限
    define('QRYLMTMRYS',(int)$cfgdata['COQRYMS']);
    //ライブラリー除外設定 0⇒除外  1⇒使用
    $colbflg='0';//デフォットは''と0です。
    switch($cfgdata['COLBFLG']){
        case '0':
        case '1':$colbflg=$cfgdata['COLBFLG'];
                break;
        default:$colbflg='0';
                break;
    }
    define('SYSLIBCHK',$colbflg);
    //カラム設定
    define('SYSCOLCFLG',$cfgdata['COLCFLG']);

    //クエリーカラム全件表示設定
    define('SYSQRYCOL',$cfgdata['COLQCOL']);
    
}else{
    // 接続失敗
    error_log('DBの接続が失敗しました。');
}

//0件メールの初期値　0:送信する 1:送信しない
define('INIT0MAL',0);

//ユーザー列設定を記憶させない 0:記憶する 1:記憶しない
define('COLMSAVE',0);