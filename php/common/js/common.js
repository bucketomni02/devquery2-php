/*
*-------------------------------------------------------* 
変数
*-------------------------------------------------------* 
*/
var cmKeyDownCode = 0;



    $(document).ready(function(){

        $("#cwspear-is-awesome").remove();
        $("textarea:last").remove();
        if($(".navbar")[0]){
            nav_h = $("#navbar").height();
            win_h = $(window).height();
            $("#wrapper").height(win_h - nav_h);
            $("#wrapper").css('min-height',win_h - nav_h+'px');
            $(window).resize(function(){
                win_h = $(window).height();
                $("#wrapper").height(win_h - nav_h);
                $("#wrapper").css('min-height',win_h - nav_h+'px');
            });
        }

    });

/*
*-------------------------------------------------------* 
別formの要素をajaxデータ用に結合
*-------------------------------------------------------* 
*/
	function cmFormjoin(data , elements){



		var input_elem = elements.getElementsByTagName("input");


		for(var i = 0, len = input_elem.length; i < len; i++) {


			if(input_elem[i].type == 'text'){
				var name = input_elem[i].name;
				var value = input_elem[i].value;
			}
			else if(input_elem[i].type == 'hidden'){
				var name = input_elem[i].name;
				var value = input_elem[i].value;
			}
			else if(input_elem[i].type == 'radio'){
				if(input_elem[i].checked){
					var name = input_elem[i].name;
					var value = input_elem[i].value;
				}
			}
			else if(input_elem[i].type == 'checkbox'){
				var name = input_elem[i].name;
				var value = input_elem[i].checked;
			}
			else{
				var name = input_elem[i].name;
				var value = input_elem[i].value;
			}
		
			if(name != null && name != ''){
                name = encodeURIComponent(name);
				if(data == null || data == ''){
					data = name + '=' + encodeURIComponent(value);
				}
				else{
					data += '&' + name + '=' + encodeURIComponent(value);
				}
			}

		}

		var select_elem = elements.getElementsByTagName("select");

		for(var i = 0, len = select_elem.length; i < len; i++) {
			

			name = select_elem[i].name;

			value = select_elem[i].value;
			if(value == null || value == ''){
				value = '';
			}

			if(data == null || data == ''){
				data = name + '=' + encodeURIComponent(value);
			}
			else{
				data += '&' + name + '=' + encodeURIComponent(value);
			}	

		}

		var textarea_elem = elements.getElementsByTagName("textarea");

		for(var i = 0, len = textarea_elem.length; i < len; i++) {
			

			name = textarea_elem[i].name;

			value = textarea_elem[i].value;
			if(value == null || value == ''){
				value = '';
			}

			if(data == null || data == ''){
				data = name + '=' + encodeURIComponent(value);
			}
			else{
				data += '&' + name + '=' + encodeURIComponent(value);
			}	

		}

		return data;

	}


function csv2json(csvArray){
  var jsonArray = [];
     
  // 1行目から「項目名」の配列を生成する
  var items = csvArray[0].split(',');
 
  // CSVデータの配列の各行をループ処理する
  //// 配列の先頭要素(行)は項目名のため処理対象外
  //// 配列の最終要素(行)は空のため処理対象外
  for (var i = 0; i < csvArray.length; i++) {
    var a_line = new Object();
    // カンマで区切られた各データに分割する
    var csvArrayD = csvArray[i].split(',');
    //// 各データをループ処理する
    for (var j = 0; j < items.length; j++) {
      // 要素名：items[j]
      // データ：csvArrayD[j]
      a_line[items[j]] = csvArrayD[j];
    }
    jsonArray.push(a_line);
  }
  //console.debug(jsonArray);
  return jsonArray;
}

function numOnly() {
  m = String.fromCharCode(event.keyCode);
  if("0123456789\b\r\t".indexOf(m, 0) < 0) return false;
  return true;
}

/*
*-------------------------------------------------------* 
  入力byte数チェック
 
   全角を2byte
   半角を1byteで判定し、
   全角文字列の開始終了に識別子分とし2byteプラスし
   指定byte数を超えていたらTrueを返却します
 
  @param str      文字列
         maxByte  有効桁数
  @return true(有効桁数オーバー) | false(有効桁数内)
*-------------------------------------------------------* 
*/
function chkMaxByte(str, maxByte){
	var rs = false;
	var valid_flg = false;
	var zenFlg = false;
	var totalByte = 0;

	for (var i=0, leng=str.length; i<leng; i++) {

		var chr = str.charCodeAt(i);

		if ( (chr >= 0x0 && chr < 0x81)
			|| (chr == 0xf8f0) || (chr == 0x00a5) || (chr >= 0xff61 && chr < 0xffa0) || (chr >= 0xf8f1 && chr < 0xf8f4)) {
			totalByte = totalByte+1;
			zenFlg = false;
		}
		else {

			totalByte = totalByte+2;

			if(!zenFlg){
				totalByte = totalByte+2;
			}
			zenFlg = true;
		}
	}

	if(totalByte > maxByte){
		rs = true;
	}
	return rs;

}

/*
*-------------------------------------------------------* 
  入力バイト数エラーメッセージ
*-------------------------------------------------------* 
*/
function ByteErrorMsg(item,byte){
	return (item+'は半角'+byte+'文字以内、全角'+((byte/2)-1)+'文字以内で入力してください');
}


function formatDate(date){

    var rs = '';

    if(date.length == 8) {
        rs = date.substr(0,4) +"/"+ date.substr(4,2) + "/" +date.substr(6,2);
    }else if(date.length == 6){
        rs = date.substr(0,4) +"/"+ date.substr(4,2);
    }

    return rs;

}

function numformat(Numeric){

    Numeric += '';

    //うっかり入っていたカンマを消す(=fig2num())
    var Separator = Numeric.indexOf(',',0);
    while (Separator != -1){
        Numeric = Numeric.substring(0, Separator) + Numeric.substring(Separator+1, Numeric.length);
        Separator = Numeric.indexOf(',',0);
    }

    //小数点を探し、小数点以下と整数部を分割して保持する
    var DecimalPoint = Numeric.lastIndexOf('.');
    if (DecimalPoint == -1){
        var Decimals = '';
        var Integers = Numeric + '';
    } else {
        var Decimals = Numeric.substring(DecimalPoint,Numeric.length) + '';
        var Integers = Numeric.substring(0,DecimalPoint) + '';
    }
    //整数部の文字列長を3の倍数にする。足りない分は手前に' 'を埋め込む
    Blanks = Integers.length % 3;
    if (Blanks != 0){
        for (var i = 0; 3-Blanks > i ; i++){
            Integers = ' ' + Integers;
        }
    }

    //整数文字列先頭から3文字おきにカンマを挿入する
    //先頭がマイナス符号の時は負数として処理する
    FigureInteger = Integers.substring(0,3);
    var j = 2;
    if (Integers.charAt(2) == '-'){
        FigureInteger = FigureInteger + Integers.substring(3,6);
        j=4;
    }
    for (i = j; Integers.length > i ; i++){
        if (i % 3 == 0){
            FigureInteger = FigureInteger + ',' + Integers.substring(i,i+3);
        }
    }

    //臨時に入れておいた' 'を削除する
    while (FigureInteger.charAt(0) == ' '){
        FigureInteger = FigureInteger.substring(1,FigureInteger.length);
    }

    //整形済みの整数部と、待避してあった小数部を連結。連結した文字列を返して終了！
    CommaNumber = FigureInteger + Decimals;
    return CommaNumber;
}