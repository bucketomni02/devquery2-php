this.setDataTable = function(selector) {

    if(TableTools){
		TableTools.DEFAULTS.sSwfPath = 'common/lib/TableTools/media/swf/copy_csv_xls_pdf.swf';
    }
    
    return selector.dataTable(
		{
        aaSorting:[],
        sDom: 'iflrtiTp',
        iDisplayLength:100,
        aaSorting:[],
        sPaginationType: "bootstrap",
	    oLanguage: {
	        sLengthMenu: "表示行数 _MENU_ 件",
	        oPaginate: {
	            sNext: "次のページ",
	            sPrevious: "前のページ"
	        },
	        sInfo: "全_TOTAL_件中 _START_件から_END_件を表示",
	        sSearch: "キーワード：",
            sZeroRecords: "データがありません",
            sInfoFiltered: "",
            sInfoEmpty:""
	    },
        fnInitComplete: function ( oSettings ) {
            $("#DataTables_Table_0_info").css("marginTop","16px");
            $("#DataTables_Table_0_info").css("float","left");
            $("#DataTables_Table_0_length").css("float","right");

        },
		oTableTools: {
			aButtons: [
				{
					sExtends: "text",
					sButtonText:"CSVダウンロード",
					sFieldSeperator: ",",
					sNewLine: "\n",
                    "sFieldBoundary": '"',
					fnClick:function(nButton,oConfig){

                        var data = $.csv.toArrays(this.fnGetTableData(oConfig));

						var form = document.getElementById('excel');
						form.proc.value = 'LogCsv';
						form.tabledata.value = JSON.stringify(data);
						form.submit();
					}
					
				}
			]
		}

    });
};