var base_url = $('#BASE_URL').val();

this.setDataTable = function(selector) {

    return selector.dataTable(
		{
        aaSorting:[],
        sDom: 'iflrtip',
        sPaginationType: "bootstrap",
        iDisplayLength:100,
        fnInitComplete: function ( oSettings ) {
            $("#DataTables_Table_0_info").css("marginTop","16px");
            $("#DataTables_Table_0_info").css("float","left");
            $("#DataTables_Table_0_length").css("float","right");
        },
	    oLanguage: {
	        sLengthMenu: "表示行数 _MENU_ 件",
	        oPaginate: {
	            sNext: "次のページ",
	            sPrevious: "前のページ"
	        },
	        sInfo: "全_TOTAL_件中 _START_件から_END_件を表示",
	        sSearch: "キーワード：",
            sZeroRecords: "データがありません",
            sInfoFiltered: "",
            sInfoEmpty:""
	    }

    });
};