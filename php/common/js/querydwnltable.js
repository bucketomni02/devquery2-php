//日付スラッシュ
function fmt1(data,type,full){

    var rs = '';

    rs = formatDate(data);

    return rs;

}

//日付スラッシュ(ゼロの場合はブランク)
function fmt2(data,type,full){

    var rs = '';

    if(data == 0){
        rs = '';
    }else{
        rs = formatDate(data);
    }

    return rs;

}

//カンマ編集
function fmt3(data,type,full){

    var rs = '';

    rs = numformat(data);

    return rs;

}

//カンマ編集(ゼロの場合はブランク)
function fmt4(data,type,full){

    var rs = '';

    if(data == 0){
        rs = '';
    }else{
        rs = numformat(data);
    }

    return rs;

}

//ゼロの場合はブランク
function fmt5(data,type,full){

    var rs = '';

    if(data == 0){
        rs = '';
    }else{
        rs = data;
    }

    return rs;

}

this.setDataTable = function(selector) {

    if(TableTools){
		TableTools.DEFAULTS.sSwfPath = 'common/lib/TableTools/media/swf/copy_csv_xls_pdf.swf';
    }

    //fdb2csv2の情報からカラムを作成
    var fdb2csv2 = jQuery.parseJSON($("#json_fdb2csv2").val());

    var column = [];

    for(var i = 0; i < fdb2csv2.length; i++){

        var sTitle  = fdb2csv2[i]['D2HED'];
        var sClass  = '';
        var mRender = '';
        var sType   = '';

        //クラス設定
        var type = fdb2csv2[i]['D2TYPE'];
        if((type == 'S')||(type == 'P')||(type == 'B')){
            sClass = 'align-right';
        }

        //フォーマット設定
        var wedt = fdb2csv2[i]['D2WEDT'];

        switch(wedt){
            case '1':
                mRender = fmt1;
                break;
            case '2':
                if((type == 'S')||(type == 'P')||(type == 'B')){
                    mRender = fmt2;
                }else{
                    mRender = fmt1;
                }
                break;
            case '3':
                mRender = fmt3;
                break;
            case '4':
                if((type == 'S')||(type == 'P')||(type == 'B')){
                    mRender = fmt4;
                }else{
                    mRender = fmt3;
                }
                break;
            case '5':
                mRender = fmt5;
                break;
        }


        //タイプ設定
        if((type == 'S')||(type == 'P')||(type == 'B')){
            sType = 'numeric';
        }else{
            sType ='string';
        }

        column[i] = {
            'sTitle':sTitle,
            'sClass':sClass,
            'mRender':mRender,
            'sType':sType
        }
    }

    //テーブル表示件数
    var dsprow1 = $("#DSPROW1").val();
    var dsprow2 = $("#DSPROW2").val();
    var dsprow3 = $("#DSPROW3").val();
    var dsprow4 = $("#DSPROW4").val();
    var intdsprow = parseInt($("#INTDSPROW").val());

    var lengthmenu = [];
    var lengthChildMenu1 = [];
    var lengthChildMenu2 = [];

    if(dsprow1.match(/[0-9]+/)){
        lengthChildMenu1.push(parseInt(dsprow1));
        lengthChildMenu2.push(parseInt(dsprow1));
    }
    if(dsprow2.match(/[0-9]+/)){
        lengthChildMenu1.push(parseInt(dsprow2));
        lengthChildMenu2.push(parseInt(dsprow2));
    }
    if(dsprow3.match(/[0-9]+/)){
        lengthChildMenu1.push(parseInt(dsprow3));
        lengthChildMenu2.push(parseInt(dsprow3));
    }
    if(dsprow4.match(/[0-9]+/)){
        lengthChildMenu1.push(parseInt(dsprow4));
        lengthChildMenu2.push(parseInt(dsprow4));
    }

    lengthmenu.push(lengthChildMenu1);
    lengthmenu.push(lengthChildMenu2);

    queryTable = selector.dataTable(
		{
        aaSorting:[],
        aoColumns:column,
        bDeferRender:true,
        bServerSide:true,
        sAjaxSource:"getJsonQueryData.php",
        sServerMethod:"POST",
        fnServerParams:function(aoData){

            var formData = $("#sch"). serializeArray();

            for(var i = 0; i < formData.length; i++){
                aoData.push({
                    "name":formData[i]['name'],
                    "value":formData[i]['value']
                });
            }

        },
        fnServerData: function(sSource, aoData, fnCallback, oSettings) {
            oSettings.jqXHR = $.ajax({
                url: sSource,
                type: "POST",
                data: aoData,
                dataType: 'json',
            })
            .done(function(res){

                if(res.tableFlg == false){
                    bootbox.alert('実行中のデータが削除されました。再度実行し直してください。');
                }

                fnCallback(res);

                $("#sch [name=b_iSortingCols]").val(res.res_iSortingCols);
                $("#sch [name=b_iSortCol_0]").val(res.res_iSortCol_0);
                $("#sch [name=b_sSortDir_0]").val(res.res_sSortDir_0);

                $("#excel [name=iSortingCols]").val(res.res_iSortingCols);
                $("#excel [name=iSortCol_0]").val(res.res_iSortCol_0);
                $("#excel [name=sSortDir_0]").val(res.res_sSortDir_0);
                $("#excel [name=sSearch]").val(res.res_sSearch);

                $.unblockUI();
            })
            .fail(function(){
                bootbox.alert('抽出件数が多すぎる為、正常に取得できませんでした。');
               $.unblockUI();
            });
        },
        sDom: 'iflrtiTp',
        aLengthMenu:lengthmenu,
        iDisplayLength:intdsprow,
        sPaginationType: "bootstrap",
	    oLanguage: {
	        sLengthMenu: "表示行数 _MENU_ 件",
            sLoadingRecords:"ロード中です...",
	        oPaginate: {
	            sNext: "次のページ",
	            sPrevious: "前のページ"
	        },
	        sInfo: "全_TOTAL_件中 _START_件から_END_件を表示",
	        sSearch: "キーワード：",
            sZeroRecords: "データがありません",
            sInfoFiltered: "",
            sInfoEmpty:""
	    },
        fnInitComplete: function ( oSettings ) {
            $.unblockUI();
            $("#DataTables_Table_0_info").css("marginTop","16px");
            $("#DataTables_Table_0_info").css("float","left");
            $("#DataTables_Table_0_length").css("float","right");

            //ユーザー権限でボタン表示、非表示
            var dwnl = $("#WUDWNL").val();
            var dwn2 = $("#WUDWN2").val();

            if(dwnl == '0'){
                $("#ToolTables_DataTables_Table_0_0").css("display","none");
            }
            if(dwn2 == '0'){
                $("#ToolTables_DataTables_Table_0_1").css("display","none");
            }
/*
            $('.dataTables_filter').append('<button type="button" id="searchButton" class="DTTT_button DTTT_button_text" style="text-align:center;min-width:40px !important;line-height:18px;">検索</button>');
            $('.dataTables_filter label').css("float","left");
            $('.dataTables_filter label').css("marginRight","5px");
*/
            $('.dataTables_filter input').unbind('keypress keyup');

			$(".dataTables_filter input").keydown(function(e){
				cmKeyDownCode = e.which;
			});

			$(".dataTables_filter input").keyup(function(e){
				if ( 13 == e.which && e.which == cmKeyDownCode ) {
                	var base_url = $('#BASE_URL').val();
                	$.blockUI({
                    	message:'<div style="font-size:25px;padding-top:25px;padding-bottom:15px;">Please wait...</div><div style="text-align:center;padding-bottom:25px;"><img src="'+base_url+'common/img/gif-load.gif" style="margin-right:5px;"></div>'
                	});
                	queryTable.fnFilter($('.dataTables_filter input').val());
				}
			});

/*
            $('.dataTables_filter #searchButton').click(function(){
                var base_url = $('#BASE_URL').val();
                $.blockUI({
                    message:'<div style="font-size:25px;padding-top:25px;padding-bottom:15px;">Please wait...</div><div style="text-align:center;padding-bottom:25px;"><img src="'+base_url+'common/img/gif-load.gif" style="margin-right:5px;"></div>'
                });
                queryTable.fnFilter($('.dataTables_filter input').val());
            });
*/
        },
		oTableTools: {
			aButtons: [
				{
					sExtends: "text",
					sButtonText:"CSVダウンロード",
					sFieldSeperator: ",",
					sNewLine: "\n",
                    "sFieldBoundary": '"',
					fnClick:function(nButton,oConfig){

                        var data = {
                            "filename":$("#excel [name=csvfilename]").val()
                        }

                		$.ajax({
                			type: "POST",
                			url: "checkTable.php",
                			dataType:'json',
                			data: data,
                			success: function(o){

                                if(o.tableFlg == false){
                                    bootbox.alert('実行中のデータが削除されました。再度実行し直してください。');
                                }else{
            						var form = document.getElementById('excel');
            						form.proc.value = 'Csv';
            						form.submit();
                                }

                			}
                		});

					}

				},
				{
					sExtends: "text",
					sButtonText:"EXCELダウンロード",
					sFieldSeperator: ",",
					sNewLine: "\n",
                    "sFieldBoundary": '"',
					fnClick:function(nButton,oConfig){


                        var data = {
                            "filename":$("#excel [name=csvfilename]").val()
                        }

                		$.ajax({
                			type: "POST",
                			url: "checkTable.php",
                			dataType:'json',
                			data: data,
                			success: function(o){

                                if(o.tableFlg == false){
                                    bootbox.alert('実行中のデータが削除されました。再度実行し直してください。');
                                }else{
            						var form = document.getElementById('excel');
            						form.proc.value = 'Excel';
            						form.submit();
                                }

                			}
                		});

					}
					
				}
			]
		}

    });

    return queryTable;
};