<?php

namespace Box\Spout\Writer\Style;

use Box\Spout\Writer\Exception\InvalidColorException;

/**
 * Class Color
 * This class provides constants and functions to work with colors
 *
 * @package Box\Spout\Writer\Style
 */
class Color
{
    /** Standard colors - based on Office Online */
    const BLACK = '000000';
    const WHITE = 'FFFFFF';
    const RED = 'FF0000';
    const DARK_RED = 'C00000';
    const ORANGE = 'FFC000';
    const YELLOW = 'FFFF00';
    const LIGHT_GREEN = '92D040';
    const GREEN = '00B050';
    const LIGHT_BLUE = '00B0E0';
    const BLUE = '0070C0';
    const DARK_BLUE = '002060';
    const PURPLE = '7030A0';
    const GRAY = '7030A0';
	const C000000 = '000000';
	const C800000 = '800000';
	const CFF0000 = 'FF0000';
	const CFF00FF = 'FF00FF';
	const CFF99CC = 'FF99CC';
	const C993300 = '993300';
	const CFF6600 = 'FF6600';
	const CFF9900 = 'FF9900';
	const CFFCC00 = 'FFCC00';
	const CFFCC99 = 'FFCC99';
	const C333300 = '333300';
	const C808000 = '808000';
	const C99CC00 = '99CC00';
	const CFFFF00 = 'FFFF00';
	const CFFFF99 = 'FFFF99';
	const C003300 = '003300';
	const C008000 = '008000';
	const C339966 = '339966';
	const C00FF00 = '00FF00';
	const CCCFFCC = 'CCFFCC';
	const C003366 = '003366';
	const C008080 = '008080';
	const C33CCCC = '33CCCC';
	const C00FFFF = '00FFFF';
	const CCCFFFF = 'CCFFFF';
	const C000080 = '000080';
	const C0000FF = '0000FF';
	const C3366FF = '3366FF';
	const C00CCFF = '00CCFF';
	const C99CCFF = '99CCFF';
	const C333399 = '333399';
	const C666699 = '666699';
	const C800080 = '800080';
	const C993366 = '993366';
	const CCC99FF = 'CC99FF';
	const C333333 = '333333';
	const C808080 = '808080';
	const C969696 = '969696';
	const CC0C0C0 = 'C0C0C0';
	const CFFFFFF = 'FFFFFF';
	const C9999FF = '9999FF';
//	const C000080 = '000080';
//	const C993366 = '993366';
//	const CFF00FF = 'FF00FF';
	const CFFFFCC = 'FFFFCC';
//	const CFFFF00 = 'FFFF00';
//	const CCCFFFF = 'CCFFFF';
//	const C00FFFF = '00FFFF';
	const C660066 = '660066';
//	const C800080 = '800080';
	const CFF8080 = 'FF8080';
//	const C800000 = '800000';
	const C0066CC = '0066CC';
//	const C008080 = '008080';
	const CCCCCFF = 'CCCCFF';
//	const C0000FF = '0000FF';


    /**
     * Returns an RGB color from R, G and B values
     *
     * @api
     * @param int $red Red component, 0 - 255
     * @param int $green Green component, 0 - 255
     * @param int $blue Blue component, 0 - 255
     * @return string RGB color
     */
    public static function rgb($red, $green, $blue)
    {
        self::throwIfInvalidColorComponentValue($red);
        self::throwIfInvalidColorComponentValue($green);
        self::throwIfInvalidColorComponentValue($blue);

        return strtoupper(
            self::convertColorComponentToHex($red) .
            self::convertColorComponentToHex($green) .
            self::convertColorComponentToHex($blue)
        );
    }

    /**
     * Throws an exception is the color component value is outside of bounds (0 - 255)
     *
     * @param int $colorComponent
     * @return void
     * @throws \Box\Spout\Writer\Exception\InvalidColorException
     */
    protected static function throwIfInvalidColorComponentValue($colorComponent)
    {
        if (!is_int($colorComponent) || $colorComponent < 0 || $colorComponent > 255) {
            throw new InvalidColorException("The RGB components must be between 0 and 255. Received: $colorComponent");
        }
    }

    /**
     * Converts the color component to its corresponding hexadecimal value
     *
     * @param int $colorComponent Color component, 0 - 255
     * @return string Corresponding hexadecimal value, with a leading 0 if needed. E.g "0f", "2d"
     */
    protected static function convertColorComponentToHex($colorComponent)
    {
        return str_pad(dechex($colorComponent), 2, '0', STR_PAD_LEFT);
    }

    /**
     * Returns the ARGB color of the given RGB color,
     * assuming that alpha value is always 1.
     *
     * @param string $rgbColor RGB color like "FF08B2"
     * @return string ARGB color
     */
    public static function toARGB($rgbColor)
    {
        return 'FF' . $rgbColor;
    }
}
