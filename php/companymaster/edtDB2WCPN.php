<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$WCVAL = $_POST['WCVAL'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('会社情報の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){          
            $rs = cmChkKenGen($db2con,'21',$userData[0]['WUSAUT']);//'1' => companyMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('会社情報の権限'));
            }
        }
    }
}

//必須チェック
/*
if($WCVAL=== ''){
    $rtn = 1;
    $msg = '表示タイトルは入力必須項目です。';
    $focus = 'WCVAL';
}
*/

//バリデーションチェック

if($rtn === 0){
    if(!checkMaxLen($WCVAL,42)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('表示タイトル'));
        $focus = 'WCVAL';
    }
}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/


if($rtn === 0){
    $rs = fnUpdateDB2WCPN($db2con,$WCVAL);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* 会社情報更新
*-------------------------------------------------------*
*/

function fnUpdateDB2WCPN($db2con,$WCVAL){


    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2WCPN ';
    $strSQL .= ' SET ';
    $strSQL .= ' WCVAL = ? ';
    $strSQL .= ' WHERE WCKEY = ? ';

    $params = array(
        $WCVAL,
        '000'
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;

}