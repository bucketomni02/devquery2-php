<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once 'common/inc/config.php';
include_once ('common/lib/spout/src/Spout/Autoloader/autoload.php');
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
include_once 'common/inc/common.inc.php';
include_once 'base/createExecuteSQL.php';
include_once 'getQryCnd.php';
$proc = $_POST['proc'];
$Sort = $_POST['Sort'];
$iSortCol_0 = $_POST['iSortCol_0'];
$sSortDir_0 = $_POST['sSortDir_0'];
$sSearch = $_POST['sSearch'];
$csvfilename = $_POST['csvfilename'];
$d1name = cmHscDe($_POST['D1NAME']);
$dctext = cmHscDe($_POST['D1TEXT']);
$dctext = replaceFilename($dctext);
$htmlFlg = (isset($_POST['htmlflg']) ? $_POST['htmlflg'] : '');
$time = $_POST['time'];
$schedule = (isset($_POST['schedule']) ? $_POST['schedule'] : '0');
//pivot用
$PMPKEY = (isset($_POST['PMPKEY']) ? $_POST['PMPKEY'] : '');
$rowstart = (isset($_POST['rowstart']) ? $_POST['rowstart'] : 0);
$rowend = (isset($_POST['rowend']) ? $_POST['rowend'] : 0);
$fcheck = (isset($_POST['fcheck']) ? json_decode($_POST['fcheck'], true) : array());
$fcombo = (isset($_POST['fcombo']) ? json_decode($_POST['fcombo'], true) : array());
$ftext = (isset($_POST['ftext']) ? json_decode($_POST['ftext'], true) : array());
$ffld = (isset($_POST['ffld']) ? json_decode($_POST['ffld'], true) : array());
$flabel = (isset($_POST['flabel']) ? json_decode($_POST['flabel'], true) : array());
$colFlg = (isset($_POST['colflg']) ? $_POST['colflg'] : '');
$pvFlg = (isset($_POST['pvflg']) ? $_POST['pvflg'] : '');//イー

$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID'])) ? $_SESSION['PHPQUERY']['user'][0]['WUUID'] : '';
//20161125 EXCEL SETTING
$CNDSDATA = (isset($_POST['CNDSDATA']) ? json_decode($_POST['CNDSDATA'], true) : array());
//アレイというのはピボットのトレイダウン
$PDRILLDOWNSEARCH = (isset($_POST['PDRILLDOWNSEARCH']) ? json_decode($_POST['PDRILLDOWNSEARCH'], true) : array());
//アレイというのはグラフのトレイダウン MSM add 20181003
$GDRILLDOWNSEARCH = (isset($_POST['GDRILLDOWNSEARCH']) ? json_decode($_POST['GDRILLDOWNSEARCH'], true) : array());
if (count($PDRILLDOWNSEARCH) == 0 && count($GDRILLDOWNSEARCH) > 0){
    $PDRILLDOWNSEARCH = $GDRILLDOWNSEARCH;
}
//for filtersearch
$filtersearchData = (isset($_POST['filtersearchData']) ? json_decode($_POST['filtersearchData'], true) : array());
$CNDSDATAF = array();
$DB2ECON = array();
$DB2EINS = array();
$DB2EINSROWMAX = 0;
$BINDEXCELVERSION = (isset($_POST['BINDEXCELVERSION']) ? $_POST['BINDEXCELVERSION'] : '');
$EXCELMAXLEN = 0;
$EMAXLEN_FILE = '';
$rtn = 0;
$msg = '';
$data = array();
$fdb2csv1 = array();
$ext = '';
$d1edkb = '';
$db2con = cmDb2Con();
$cutomtempateFlg = false;
cmSetPHPQUERY($db2con);
umEx($d1name);
// カスタムテンプレートダウンロードフラグ
$procXls = false;
e_log('**********************:createdownloadfile*****************************', '1');
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmCheckUser($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー',));
    }
}

if ($rtn === 0) {
    $chkQry = array();
    $chkQry = cmChkQuery($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID'], $d1name, $PMPKEY);
    e_log('チェックQRY：'.print_r($chkQry,true));
    if ($chkQry['result'] === 'NOTEXIST_PIV') {
        $rtn = 2;
        $msg = showMsg('NOTEXIST_GET', array('ピボット',));
    } elseif ($chkQry['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($chkQry['result'], array('クエリー',));
    } else {
        $chkQryAut = array();
        $chkQryAut = $chkQry['DB2WDEF'];
	//	e_log('$chkQryAutKSK1=>：'.print_r($chkQryAut));
        if ($proc === 'Csv') {
            if ($chkQryAut['WDDWNL'] !== '1') {
                $rtn = 1;
                $msg = showMsg('ダウンロード権限がありません。ダウンロード権限の設定が変更されている可能性があります。');
            }
        } elseif ($proc === 'Excel') {	
			//e_log('$chkQryAutKSK2=>：'.print_r($chkQryAut['WDDWN2']));				
            if ($chkQryAut['WDDWN2'] !== '1') {
                $rtn = 1;
                $msg = showMsg('ダウンロード権限がありません。ダウンロード権限の設定が変更されている可能性があります。');
            }
        }
    }
}
if ($rtn === 0) {
    //FDB2CSV1のデータを取得し、カスタムテンプレートでの設定かをチェック
    //D1EDKB='1'の場合、$procに’CSV’をセット
    $D1TMPF = '';
    $D1THFG = '';
    $D1CFLG = '';
    $RDBNM = '';
    $D1LIBL = '';
    $D1NAME = '';
	$D1SFLG ='';
    $D1TROS = 0; //template row
    $fdb2csv1 = FDB2CSV1_DATA($d1name);
    if ($fdb2csv1['result'] === true) {
        $D1TMPF = cmHsc($fdb2csv1['data'][0]['D1TMPF']);
        $D1TMPF = um($D1TMPF);
        if ($D1TMPF !== '') {
            $D1THFG = cmHsc($fdb2csv1['data'][0]['D1THFG']);
            $D1THFG = um($D1THFG);
            $ext = explode('.', $D1TMPF);
            $ext = $ext[count($ext) - 1];
            $D1TROS = um($fdb2csv1['data'][0]['D1TROS']);
        }
        $webf = um($fdb2csv1['data'][0]['D1WEBF']);
        $d1edkb = um($fdb2csv1['data'][0]['D1EDKB']);
        $RDBNM = um($fdb2csv1['data'][0]['D1RDB']);
        $D1LIBL = um($fdb2csv1['data'][0]['D1LIBL']);
        $D1EFLG = um($fdb2csv1['data'][0]['D1EFLG']);
        $D1ETYP = um($fdb2csv1['data'][0]['D1ETYP']);
        $D1CFLG = um($fdb2csv1['data'][0]['D1CFLG']);
        $D1NAME = um($fdb2csv1['data'][0]['D1NAME']);
        $D1SFLG = um($fdb2csv1['data'][0]['D1SFLG']);
    }
    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
        $systables = cmGetSystables($db2con, SAVE_DB, $csvfilename);
    } else {
        $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
        $db2RDBCon = cmDB2ConRDB($D1LIBL);
        $systables = cmGetSystables($db2RDBCon, SAVE_DB, $csvfilename);
    }
    //カラム情報を取得
    $userId = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
    $DB2WCOL = cmGetDB2WCOL($db2con, $userId, $d1name);
    $DB2WCOL = umEx($DB2WCOL);
    if (count($systables) > 0) {
        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
            $resIns = cmInsWTBL($db2con, $csvfilename);
        } else {
            $resIns = cmInsWTBL($db2con, $csvfilename, $db2RDBCon);
        }
        if ($resIns !== 0) {
            //修正用
            $msg = showMsg('FAIL_MENU_DEL', array('実行中のデータ')); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
            $rtn = 1;
        }
    } else {
        $rtn = 1;
        $msg = showMsg('FAIL_MENU_DEL', array('実行中のデータ')); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
        
    }
    if ($rtn === 0) {
        //集計するフィールド情報取得
        $rsmaster = cmGetDB2COLM($db2con, $d1name);
        if ($rsmaster['result'] !== true) {
            $msg = showMsg($rsmaster['result']);
            $rtn = 1;
        } else {
            $rsMasterArr = $rsmaster['data'];
        }
        //集計するメソッド情報取得
        $rsshukei = cmGetDB2COLTTYPE($db2con, $d1name);
        if ($rsshukei['result'] !== true) {
            $msg = showMsg($rsshukei['result']);
            $rtn = 1;
        } else {
            $rsshukeiArr = $rsshukei['data'];
        }
        if ($htmlFlg === '1' && count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
            $rst = cmLevelFieldCheck($db2con, $d1name, $rsMasterArr, $rsshukeiArr);
            if ($rst['result'] !== true) {
                $msg = showMsg($rst['result'], array('クエリー', 'カラム',));
                $rtn = 2;
            }
        }
        //EXCEL SETTING 挿入文字
        if ($d1edkb === '2') {
            //checking template or not
            $DB2ECON = cmGetDB2ECON($db2con, $d1name);
            if ($DB2ECON['result'] === true) {
                $DB2ECON = umEx($DB2ECON['data']);
            }
            $DB2EINS = cmGetDB2EINS($db2con, $d1name);
            if ($DB2EINS['result'] === true) {
                $EIROWMAX = $DB2EINS['MAXEIROW'];
                $DB2EINS = umEx($DB2EINS['data']);
            }
            $CNDSDATAF = cmCNDSDATA($webf, $db2con, $d1name, $CNDSDATA, $D1CFLG);
            $DB2EINSROWMAX = cmGetCountSearchData($DB2ECON, $EIROWMAX, $CNDSDATAF); //EXCEL SETTING 挿入文字カウント
            
        } elseif ($d1edkb !== '2' && $D1TMPF !== '' && $proc !== 'PivotExcel') {
            $DB2EINSROWMAX = $D1TROS;
        }
        //ヘッダーがあるのでプラス1
        if ($d1edkb === '2' || $proc === 'PivotExcel' || $D1TMPF === '') {
           
            $DB2EINSROWMAX+= 1;
        }
        //EXCEL SETTING 挿入文字
        if ($proc === 'Excel' && $d1edkb === '1') {
            $procXls = true;
            $proc = 'Csv';
            if ($D1THFG !== '') {
                $cutomtempateFlg = true;
            }
        }
        $csvfiledownload = '';
        //CSV作成
        if ($proc == 'Csv' && $rtn === 0) {
            $reWebf = fnGetFDB2CSV1($db2con, $WUUID, $d1name);
            $reWebf1 = fnGetDB2WUSR($db2con, $WUUID);
            if ($reWebf['result'] !== true) {
                $rtn = 1;
            } else {
                $WDSERV = $reWebf['data'][0]['WDSERV'];
                /**AKZ フォルダスペース除去20170511**/
                if ($WDSERV !== '') {
                    $WDSERVARR = explode('/', $WDSERV);
                    $WDSERV = '';
                    $WDSERVCOUNT = count($WDSERVARR) - 1;
                    for ($i = 0;$i < $WDSERVCOUNT;++$i) {
                        if (cmMer($WDSERVARR[$i]) != '') {
                            $WDSERV.= '/' . replaceFilename(trim($WDSERVARR[$i]));
                        }
                    }
                    $WDSERV.= '/' . replaceFilename(ltrim($WDSERVARR[$WDSERVCOUNT]));
                }
                /**AKZ フォルダスペース除去20170511**/
                $WUSERV = $reWebf1['data'][0]['WUSERV'];
                /**AKZ フォルダスペース除去20170511**/
                if ($WUSERV !== '') {
                    $WUSERVARR = explode('/', $WUSERV);
                    $WUSERV = '';
                    $WUSERVCOUNT = count($WUSERVARR) - 1;
                    for ($i = 0;$i < $WUSERVCOUNT;++$i) {
                        if (cmMer($WUSERVARR[$i]) != '') {
                            $WUSERV.= '/' . replaceFilename(trim($WUSERVARR[$i]));
                        }
                    }
                    $WUSERV.= '/' . replaceFilename(ltrim($WUSERVARR[$WUSERVCOUNT]));
                }
                /**AKZ フォルダスペース除去20170511**/
                $WUCTFL = $reWebf1['data'][0]['WUCTFL'];
                $WUCNFL = $reWebf1['data'][0]['WUCNFL'];
                $WDCTFL = $reWebf['data'][0]['WDCTFL'];
                $WDCNFL = $reWebf['data'][0]['WDCNFL'];
                $data_CSV1 = $reWebf['data'];
                $data_WUSR1 = $reWebf1['data'];
            }
            require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
            $ua = $_SERVER['HTTP_USER_AGENT'];
            if (!(preg_match('/Chrome/i', $ua)) && !(preg_match('/Safari/i', $ua))) {
                $d1name = mb_convert_encoding($d1name, 'SJIS-win', 'UTF-8');
            }
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . cmMer($d1name) . '.csv');
            if ($Sort !== '1') {
                if ($WDSERV !== '') {
                    if ($WDCTFL !== '1') {
                        $stream = fopen(TEMP_DIR . cmMer($d1name) . '.csv', 'w');
                    } else {
                        $stream = fopen(TEMP_DIR . cmMer($d1name) . $time . '.csv', 'w');
                    }
                } else {
                    if ($WUSERV !== '') {
                        if ($WUCTFL !== '1') {
                            $stream = fopen(TEMP_DIR . cmMer($d1name) . '.csv', 'w');
                        } else {
                            $stream = fopen(TEMP_DIR . cmMer($d1name) . $time . '.csv', 'w');
                        }
                    } else {
                        $stream = fopen(TEMP_DIR . cmMer($d1name) . $time . '.csv', 'w');
                    }
                }
            } else {
                if ($WDSERV !== '') {
                    if (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) !== '/') {
                        $WDSERV = '/' . $WDSERV . '/';
                    } elseif (substr($WDSERV, 0, 1) === '/' && substr($WDSERV, -1) !== '/') {
                        $WDSERV = $WDSERV . '/';
                    } elseif (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) === '/') {
                        $WDSERV = '/' . $WDSERV;
                    }
                    $dfilename = $dctext;
                    if ($WDCNFL === '1') {
                        $dfilename = $d1name;
                    }
                    $filepath = substr($WDSERV, 0, intval(strrpos($WDSERV, '/')) + 1);
                    if (!file_exists($filepath)) {
                        if (!mkdir($filepath, 0777, true)) {
                            $msg = showMsg('FAIL_FOLDER');
                            $rtn = 1;
                        } else {
                            if ($WDCTFL !== '1') {
                                $csvfiledownload = $WDSERV . cmMer($dfilename) . '.csv';
                            } else {
                                $csvfiledownload = $WDSERV . cmMer($dfilename) . $time . '.csv';
                            }
                            $stream = fopen($csvfiledownload, 'w');
                        }
                    } else {
                        $file = $WDSERV . cmMer($dfilename) . '_test.csv';
                        if (!file_exists($file)) {
                            $createFile = touch($file);
                        }
                        if ($createFile === false) {
                            $msg = showMsg('FAIL_FILEPATH');
                            $rtn = 1;
                        } else {
                            if ($WDCTFL !== '1') {
                                $csvfiledownload = $WDSERV . cmMer($dfilename) . '.csv';
                            } else {
                                $csvfiledownload = $WDSERV . cmMer($dfilename) . $time . '.csv';
                            }
                            unlinkFile($csvfiledownload); // unlink old file and copy temp file
                            $stream = fopen($csvfiledownload, 'w');
                            if (!$stream) {
                                filerollBack($csvfiledownload);
                                $msg = showMsg('FAIL_FILEPATH');
                                $rtn = 1;
                            } else {
                                $csvfiledownloadbk = $csvfiledownload . '_temp';
                                if (file_exists($csvfiledownloadbk)) {
                                    unlink($csvfiledownloadbk);
                                }
                            }
                            if (file_exists($file)) {
                                unlink($file);
                            }
                        }
                    }
                } else {
                    if ($WUSERV !== '') {
                        $dfilename = $dctext;
                        if ($WUCNFL === '1') {
                            $dfilename = $d1name;
                        }
                        if (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) !== '/') {
                            $WUSERV = '/' . $WUSERV . '/';
                        } elseif (substr($WUSERV, 0, 1) === '/' && substr($WUSERV, -1) !== '/') {
                            $WUSERV = $WUSERV . '/';
                        } elseif (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) === '/') {
                            $WUSERV = '/' . $WUSERV;
                        }
                        $filepath = substr($WUSERV, 0, intval(strrpos($WUSERV, '/')) + 1);
                        if (!file_exists($filepath)) {
                            if (!mkdir($filepath, 0777, true)) {
                                $msg = showMsg('FAIL_FOLDER');
                                $rtn = 1;
                            } else {
                                if ($WUCTFL !== '1') {
                                    $csvfiledownload = $WUSERV . cmMer($dfilename) . '.csv';
                                } else {
                                    $csvfiledownload = $WUSERV . cmMer($dfilename) . $time . '.csv';
                                }
                                $stream = fopen($csvfiledownload, 'w');
                            }
                        } else {
                            $file = $WUSERV . '.csv';
                            if (!file_exists($file)) {
                                $createFile = touch($file);
                            }
                            if ($createFile === false) {
                                $msg = showMsg('FAIL_FILEPATH');
                                $rtn = 1;
                            } else {
                                if ($WUCTFL !== '1') {
                                    $csvfiledownload = $WUSERV . cmMer($dfilename) . '.csv';
                                } else {
                                    $csvfiledownload = $WUSERV . cmMer($dfilename) . $time . '.csv';
                                }
                                unlinkFile($csvfiledownload); // unlink old file and copy temp file
                                $stream = fopen($csvfiledownload, 'w');
                                if (!$stream) {
                                    filerollBack($csvfiledownload);
                                    $msg = showMsg('FAIL_FILEPATH');
                                    $rtn = 1;
                                } else {
                                    $csvfiledownloadbk = $csvfiledownload . '_temp';
                                    if (file_exists($csvfiledownloadbk)) {
                                        unlink($csvfiledownloadbk);
                                    }
                                }
                                if (file_exists($file)) {
                                    unlink($file);
                                }
                            }
                        }
                    } elseif ($WUSERV === '') {
                        $stream = fopen(TEMP_DIR . cmMer($d1name) . $time . '.csv', 'w');
                        if (!$stream) {
                            $msg = showMsg('FAIL_FILEPATH');
                            $rtn = 1;
                        }
                    }
                }
            }
            $FDB2CSV2 = cmGetFDB2CSV2($db2con, $d1name, true, false, false);
            if ($FDB2CSV2['result'] === true) {
                // $FDB2CSV2     = umEx($FDB2CSV2['data']);
                $FDB2CSV2 = $FDB2CSV2['data'];
                $FDB2CSV2_ALL = cmGetFDB2CSV2($db2con, $d1name, false, false, false);
                if ($FDB2CSV2_ALL['result'] === true) {
                    $FDB2CSV2_ALL = umEx($FDB2CSV2_ALL['data']); //データ取得の時使ってる
                    if ($htmlFlg === '0') {
                        if ($colFlg === '1') {
                            $FDB2CSV2_ALL = cmColNameDiff($db2con, $FDB2CSV2_ALL, $csvfilename);
                            $FDB2CSV2 = cmColNameDiff($db2con, $FDB2CSV2, $csvfilename);
                        }
                        $DB2WCOLTEST = array();
                        $DB2WCOLTEST = ($D1SFLG === "1") ? array() : $DB2WCOL;
                        $head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOLTEST);
							
                        //$head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOL); old
                        $csvHeading = array();
                        if ($cutomtempateFlg !== true) {
                            $csvHeading = $head;
                        }
                        //cmCsvDownload($stream, $d1name, '', $csvHeading, $time, $FDB2CSV2, true, $DB2WCOL);//write heading csv
                        cmCsvDownload($stream, $d1name, '', $csvHeading, $time, $FDB2CSV2, true, $DB2WCOLTEST); //write heading csv
                        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                            $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                        } else {
                            $allcount = cmGetAllCount($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                        }
                        $lenArr = cmTotalLen($allcount['data']);
                        $lenCount = count($lenArr);
                        $estart = microtime(true);
                        for ($idx = 0;$idx < $lenCount;++$idx) {
                            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                $tabledata = cmGetDbFile($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOLTEST, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                            } else {
                                $tabledata = cmGetDbFile($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOLTEST, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                            }
                            if ($tabledata['result'] !== true) {
                                $msg = showMsg($tabledata['result']);
                                $rtn = 1;
                            } else {
                                
                                cmCsvDownload($stream, $d1name, $tabledata['data'], $head, $time, $FDB2CSV2, false, $DB2WCOLTEST);
                                unset($tabledata);
                            }
                        }
                        $eend = microtime(true);
                    } elseif ($htmlFlg === '1') {
                      
                        $DB2WCOL = array();
                        if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                            $head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOL);
                            $DB2WCOL = array();
                            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                $allcount = cmGetAllCount_1($db2con, $FDB2CSV2, $csvfilename, $sSearch, '', array(), array());
                            } else {
                                $allcount = cmGetAllCount_1($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, '', array(), array());
                            }
                            $lenArr = cmTotalLen($allcount['data']);
                            array_unshift($head, '', '');
                            $FDB2CSV2_unshift = cmArrayUnshift($FDB2CSV2);
                            $csvHeading = array();
                            if ($cutomtempateFlg !== true) {
                                $csvHeading = $head;
                            }
                            cmCsvDownload($stream, $d1name, '', $csvHeading, $time, $FDB2CSV2_unshift, true, $DB2WCOL); //write heading csv
                            $lenCount = count($lenArr);
                            for ($idx = 0;$idx < $lenCount;++$idx) {
                                // クエリ情報を取得する
                                //$dFile1 = cmGetDbFile_1($db2con, $FDB2CSV2_ALL, $csvfilename, '', '', '', '', '', $sSearch, $db2wcol, $rsMasterArr, true, '');
                                if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                    $dFile1 = cmGetDbFile_1($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', $sSearch, '', $rsMasterArr, true, '', $webf, $d1name, $CNDSDATA,'', $filtersearchData);
                                    
                                } else {
                                    $dFile1 = cmGetDbFile_1($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', $sSearch, '', $rsMasterArr, true, '', $webf, $d1name, $CNDSDATA,'', $filtersearchData);
                                   
                                }
                                if ($dFile1['result'] !== true) {
                                    $msg = showMsg($dFile1['result']);
                                    $rtn = 1;
                                } else {
                                    // 集計情報を取得する
                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                        $dFile2 = cmGetDbFile_2($db2con, $FDB2CSV2_ALL, $d1name, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $sSearch, '', $rsMasterArr, $rsshukeiArr, true, '', $webf, $d1name, $CNDSDATA, $filtersearchData);
                                    } else {
                                        $dFile2 = cmGetDbFile_2($db2RDBCon, $FDB2CSV2_ALL, $d1name, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $sSearch, '', $rsMasterArr, $rsshukeiArr, true, '', $webf, $d1name, $CNDSDATA, $filtersearchData);
                                    }
                                    if ($dFile2['result'] !== true) {
                                        $msg = showMsg($dFile2['result']);
                                        $rtn = 1;
                                    } else {
										$lenCount1 = $allcount['data'];
										if (count($filtersearchData) > 0) {
											foreach ($dFile2['data'] as $key => $row1) {
												$lenCount1 = $dFile2['data'][$key]['RN'];
											}
										}
                                        // データをマージする
                                     	error_log('csv tesing 11**********');
                                        $tabledata = cmShukeiDataMerge($db2con, $dFile1['data'], $dFile2['data'], $rsMasterArr, $rsshukeiArr, $FDB2CSV2, $FDB2CSV2_ALL, $lenCount1);                                      
                                        if ($tabledata['result'] !== true) {
                                            $msg = showMsg($tabledata['result']);
                                            $rtn = 1;
                                        } else {
                                          
                                            cmCsvDownload($stream, $d1name, $tabledata['data'], $head, $time, $FDB2CSV2_unshift, false, $DB2WCOL);
                                            unset($tabledata);
                                        }
                                    }
                                }
                            }
                        } else {
                            $db2wcol = array();
                            $head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOL);
                            $csvHeading = array();
                            if ($cutomtempateFlg !== true) {
                                $csvHeading = $head;
                            }
                            cmCsvDownload($stream, $d1name, '', $csvHeading, $time, $FDB2CSV2, true, $DB2WCOL); //write heading csv
                            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData); //アレイというのはピボットのトレイダウン
                                
                            } else {
                                $allcount = cmGetAllCount($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData); //アレイというのはピボットのトレイダウン                                
                            }
                            $lenArr = cmTotalLen($allcount['data']);
                            $lenCount = count($lenArr);
                            for ($idx = 0;$idx < $lenCount;++$idx) {
                                if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                    $tabledata = cmGetDbFile($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $db2wcol, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                                } else {
                                    $tabledata = cmGetDbFile($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $db2wcol, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                                }
                                if ($tabledata['result'] !== true) {
                                    $msg = showMsg($tabledata['result']);
                                    $rtn = 1;
                                } else {
                                 
                                    cmCsvDownload($stream, $d1name, $tabledata['data'], $head, $time, $FDB2CSV2, false, $DB2WCOL);
                                    unset($tabledata);
                                }
                            }
                        }
                    }
                    fclose($stream);
                    cmEnd($db2con, $RTCD, $JSEQ, $csvfilename, $d1name);
                    if ($webf !== '1') {
                        cmInsertDB2WLOG($db2con, $logrs, $_SESSION['PHPQUERY']['user'][0]['WUUID'], 'C', $d1name, $CNDSDATA, '0', '');
                    } else {
                        cmInsertDB2WLOG($db2con, $logrs, $_SESSION['PHPQUERY']['user'][0]['WUUID'], 'C', $d1name, $CNDSDATA, '1', '');
                    }
                } else {
                }
            } else {
            }
            if ($ext !== '') {
                if ($procXls === true && $D1TMPF !== '') {
                    if (file_exists(ETEMP_DIR . (string)$d1name . '.' . $ext)) {
                        $templateFlg = true;
                    } else {
                        $templateFlg = false;
                    }
                } else {
                    $templateFlg = true;
                }
                if ($templateFlg) {
                    if ($Sort !== '1') {
                        if ($WDSERV !== '') {
                            if (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) !== '/') {
                                $WDSERV = '/' . $WDSERV . '/';
                            } elseif (substr($WDSERV, 0, 1) === '/' && substr($WDSERV, -1) !== '/') {
                                $WDSERV = $WDSERV . '/';
                            } elseif (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) === '/') {
                                $WDSERV = '/' . $WDSERV;
                            }
                            $filepath = substr($WDSERV, 0, intval(strrpos($WDSERV, '/')) + 1);
                            if (!file_exists($filepath)) {
                                if (!mkdir($filepath, 0777, true)) {
                                    $msg = showMsg('FAIL_FOLDER');
                                    $rtn = 1;
                                } else {
                                    if ($WDCTFL !== '1') {
                                        $fcsvnm = TEMP_DIR . (string)$d1name . '.csv';
                                        $fxlsmnm = ETEMP_DIR . (string)$d1name . '.' . $ext;
                                        $fname = $WDSERV . (string)$dctext . '.zip';
                                        shell_exec('7z a ' . '"' . $fname . '"' . ' ' . '"' . $fcsvnm . '" ' . '"' . $fxlsmnm . '"' . ' -ssc');
                                        if (file_exists($fcsvnm)) {
                                            unlink($fcsvnm);
                                        }
                                    } else {
                                        $fcsvnm = TEMP_DIR . (string)$d1name . $time . '.csv';
                                        $fxlsmnm = ETEMP_DIR . (string)$d1name . $time . '.' . $ext;
                                        if (copy(ETEMP_DIR . (string)$d1name . '.' . $ext, $fxlsmnm) === false) {
                                            //echo 'no file';
                                            
                                        } else {
                                            $fname = $WDSERV . (string)$dctext . $time . '.zip';
                                            shell_exec('7z a ' . '"' . $fname . '"' . ' ' . '"' . $fcsvnm . '" ' . '"' . $fxlsmnm . '"' . ' -ssc');
                                            if (file_exists($fcsvnm)) {
                                                unlink($fcsvnm);
                                            }
                                            if (file_exists($fxlsmnm)) {
                                                unlink($fxlsmnm);
                                            }
                                        }
                                    }
                                }
                            } else {
                                $file = $WDSERV . '.zip';
                                if (!file_exists($file)) {
                                    $createFile = touch($file);
                                }
                                if ($createFile === false) {
                                    $msg = showMsg('FAIL_FILEPATH');
                                    $rtn = 1;
                                } else {
                                    if ($WDCTFL !== '1') {
                                        $fcsvnm = TEMP_DIR . (string)$d1name . '.csv';
                                        $fxlsmnm = ETEMP_DIR . (string)$d1name . '.' . $ext;
                                        $fname = $WDSERV . (string)$dctext . '.zip';
                                        $fp = shell_exec('7z a ' . '"' . $fname . '"' . ' ' . '"' . $fcsvnm . '" ' . '"' . $fxlsmnm . '"' . ' -ssc');
                                        if (file_exists($file)) {
                                            unlink($file);
                                        }
                                        if (file_exists($fcsvnm)) {
                                            unlink($fcsvnm);
                                        }
                                    } else {
                                        $fcsvnm = TEMP_DIR . (string)$d1name . $time . '.csv';
                                        $fxlsmnm = ETEMP_DIR . (string)$d1name . $time . '.' . $ext;
                                        if (copy(ETEMP_DIR . (string)$d1name . '.' . $ext, $fxlsmnm) === false) {
                                            //echo 'no file';
                                            
                                        } else {
                                            $fname = $WDSERV . (string)$dctext . $time . '.zip';
                                            $fp = shell_exec('7z a ' . '"' . $fname . '"' . ' ' . '"' . $fcsvnm . '" ' . '"' . $fxlsmnm . '"' . ' -ssc');
                                            if (file_exists($file)) {
                                                unlink($file);
                                            }
                                            if (file_exists($fcsvnm)) {
                                                unlink($fcsvnm);
                                            }
                                            if (file_exists($fxlsmnm)) {
                                                unlink($fxlsmnm);
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($WUSERV !== '') {
                                if (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) !== '/') {
                                    $WUSERV = '/' . $WUSERV . '/';
                                } elseif (substr($WUSERV, 0, 1) === '/' && substr($WUSERV, -1) !== '/') {
                                    $WUSERV = $WUSERV . '/';
                                } elseif (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) === '/') {
                                    $WUSERV = '/' . $WUSERV;
                                }
                                $filepath = substr($WUSERV, 0, intval(strrpos($WUSERV, '/')) + 1);
                                if (!file_exists($filepath)) {
                                    if (!mkdir($filepath, 0777, true)) {
                                        $msg = showMsg('FAIL_FOLDER');
                                        $rtn = 1;
                                    } else {
                                        if ($WUCTFL !== '1') {
                                            $fcsvnm = TEMP_DIR . (string)$d1name . '.csv';
                                            $fxlsmnm = ETEMP_DIR . (string)$d1name . '.' . $ext;
                                            $fname = $WUSERV . (string)$dctext . '.zip';
                                            shell_exec('7z a ' . '"' . $fname . '"' . ' ' . '"' . $fcsvnm . '" ' . '"' . $fxlsmnm . '"' . ' -ssc');
                                            if (file_exists($fcsvnm)) {
                                                unlink($fcsvnm);
                                            }
                                        } else {
                                            $fcsvnm = TEMP_DIR . (string)$d1name . $time . '.csv';
                                            $fxlsmnm = ETEMP_DIR . (string)$d1name . $time . '.' . $ext;
                                            if (copy(ETEMP_DIR . (string)$d1name . '.' . $ext, $fxlsmnm) === false) {
                                                echo 'no file';
                                            }
                                            $fname = $WUSERV . (string)$dctext . $time . '.zip';
                                            shell_exec('7z a ' . '"' . $fname . '"' . ' ' . '"' . $fcsvnm . '" ' . '"' . $fxlsmnm . '"' . ' -ssc');
                                            if (file_exists($fcsvnm)) {
                                                unlink($fcsvnm);
                                            }
                                            if (file_exists($fxlsmnm)) {
                                                unlink($fxlsmnm);
                                            }
                                        }
                                    }
                                } else {
                                    $file = $WUSERV . '.zip';
                                    if (!file_exists($file)) {
                                        $createFile = touch($file);
                                    }
                                    if ($createFile === false) {
                                        $msg = showMsg('FAIL_FILEPATH');
                                        $rtn = 1;
                                    } else {
                                        if ($WUCTFL !== '1') {
                                            $fcsvnm = TEMP_DIR . (string)$d1name . '.csv';
                                            $fxlsmnm = ETEMP_DIR . (string)$d1name . '.' . $ext;
                                            $fname = $WUSERV . (string)$dctext . '.zip';
                                            shell_exec('7z a ' . '"' . $fname . '"' . ' ' . '"' . $fcsvnm . '" ' . '"' . $fxlsmnm . '"' . ' -ssc');
                                            if (file_exists($file)) {
                                                unlink($file);
                                            }
                                            if (file_exists($fcsvnm)) {
                                                unlink($fcsvnm);
                                            }
                                        } else {
                                            $fcsvnm = TEMP_DIR . (string)$d1name . $time . '.csv';
                                            $fxlsmnm = ETEMP_DIR . (string)$d1name . $time . '.' . $ext;
                                            if (copy(ETEMP_DIR . (string)$d1name . '.' . $ext, $fxlsmnm) === false) {
                                                echo 'no file';
                                            }
                                            $fname = $WUSERV . (string)$dctext . $time . '.zip';
                                            shell_exec('7z a ' . '"' . $fname . '"' . ' ' . '"' . $fcsvnm . '" ' . '"' . $fxlsmnm . '"' . ' -ssc');
                                            if (file_exists($file)) {
                                                unlink($file);
                                            }
                                            if (file_exists($fcsvnm)) {
                                                unlink($fcsvnm);
                                            }
                                            if (file_exists($fxlsmnm)) {
                                                unlink($fxlsmnm);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $rtn = 1;
                    $msg = showMsg('NOTEXIST_TMP', array($d1name)); // $d1name . "のテンプレートは削除される可能性があります。";
                    
                }
            }
        }
        $xlsfiledownload = '';
        if ($proc == 'Excel' && $rtn === 0) {
          
            $defautFlg = false;
            if ($D1EFLG === '2' && $D1ETYP !== '') {
                $D1TMPF = ''; //クエリー実行設定に選択されてるタイプがあったら、テンプレートの設定を無効にする
                $ext = '';
                $D1ETYP = (int)$D1ETYP;
                if ($D1ETYP === 1) {
                    $EXCELVER = 'html';
                } else if ($D1ETYP === 2) {
                    $EXCELVER = '2003';
                } else if ($D1ETYP === 3) {
                    $EXCELVER = '2007';
                } else if ($D1ETYP === 4) {
                    $EXCELVER = 'XLSX';
                } else {
                    $EXCELVER = 'html';
                }
            } else if ($D1EFLG === '2' && $BINDEXCELVERSION !== '') {
                if ($BINDEXCELVERSION !== 'TEMPLATE') {
                    $EXCELVER = $BINDEXCELVERSION; //テンプレート以外のエクセルタイプ
                    $D1TMPF = ''; //クエリーウインドウ設定に選択されてるはテンプレートじゃなかったら、テンプレートの設定を無効にする
                    $ext = '';
                } else {
                    $EXCELVER = '2007'; //PHPExcel設定使うから、バージョンはテンプレートの場合（2007もしくは2003）にする
                    
                }
            } else {
                $defautFlg = true;
                $EXCELVER = EXCELVERSION;
            }
            //テンプレート存在チェック
            $templateFlg = true;
            if ($D1TMPF !== '') {
                if (file_exists('./template/' . $d1name . '.' . $ext)) {
                    $templateFlg = true;
                } else {
                    $templateFlg = false;
                }
            }
            $NOTEMPLATE = 'NOTEMPLATE';
            if ($BINDEXCELVERSION === 'TEMPLATE' || $defautFlg === true) {
                $NOTEMPLATE = 'TEMPLATE';
            }
            /*FDB2CSV1のD1EDKBが1の場合、Excel作成処理をスルー、リターン後にCSVダウンロードメソッドを呼び出し
            CSVダウンロード終了後に「クエリーID.$ext」でテンプレートファイルをそのままダウンロード
            ※上のif文により、テンプレートが登録してある時のみ$d1edkbに変更が起きる。（defaultはブランク)
            */
            if ($d1edkb !== '1') {
                //                $EXCELVER = ($D1EFLG === '2' && $BINDEXCELVERSION !== '') ? $BINDEXCELVERSION : EXCELVERSION;//EXCEL VERSION
                if ($EXCELVER === '2007' || $EXCELVER === '2003' || $D1TMPF !== '') {
                    $reWebf = fnGetFDB2CSV1($db2con, $WUUID, $d1name);
                    $reWebf1 = fnGetDB2WUSR($db2con, $WUUID);
                    if ($reWebf['result'] !== true) {
                        // e_log(showMsg($rsmaster['result']),'1');
                        $rtn = 1;
                    } else {
                        $WDSERV = $reWebf['data'][0]['WDSERV'];
                        /**AKZ フォルダスペース除去20170511**/
                        if ($WDSERV !== '') {
                            $WDSERVARR = explode('/', $WDSERV);
                            $WDSERV = '';
                            $WDSERVCOUNT = count($WDSERVARR) - 1;
                            /*foreach($WDSERVARR as $key => $value){
                                      $WDSERV .= '/'.trim($value);
                              }*/
                            for ($i = 0;$i < $WDSERVCOUNT;++$i) {
                                if (cmMer($WDSERVARR[$i]) != '') {
                                    $WDSERV.= '/' . replaceFilename(trim($WDSERVARR[$i]));
                                }
                            }
                            $WDSERV.= '/' . replaceFilename(ltrim($WDSERVARR[$WDSERVCOUNT]));
                        }
                        /**AKZ フォルダスペース除去20170511**/
                        $WUSERV = $reWebf1['data'][0]['WUSERV'];
                        /**AKZ フォルダスペース除去20170511**/
                        if ($WUSERV !== '') {
                            $WUSERVARR = explode('/', $WUSERV);
                            $WUSERV = '';
                            $WUSERVCOUNT = count($WUSERVARR) - 1;
                            /*foreach($WUSERVARR as $key => $value){
                                      $WUSERV .= '/'.trim($value);
                              }*/
                            for ($i = 0;$i < $WUSERVCOUNT;++$i) {
                                if (cmMer($WUSERVARR[$i]) != '') {
                                    $WUSERV.= '/' . replaceFilename(trim($WUSERVARR[$i]));
                                }
                            }
                            $WUSERV.= '/' . replaceFilename(ltrim($WUSERVARR[$WUSERVCOUNT]));
                        }
                        /**AKZ フォルダスペース除去20170511**/
                        $WUCTFL = $reWebf1['data'][0]['WUCTFL'];
                        $WUCNFL = $reWebf1['data'][0]['WUCNFL'];
                        $WDCTFL = $reWebf['data'][0]['WDCTFL'];
                        $WDCNFL = $reWebf['data'][0]['WDCNFL'];
                        $data_CSV1 = $reWebf['data'];
                        $data_WUSR1 = $reWebf1['data'];
                    }
                    if ($templateFlg !== false) {
                        include_once 'common/lib/PHPExcel/Classes/PHPExcel.php';
                        include_once 'common/lib/PHPExcel/Classes/PHPExcel/IOFactory.php';
                        include_once 'common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel5.php';
                        include_once 'common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
                        //MAX件数のためカウント
                        if ($ext !== '') {
                            $EXCELMAXLEN = ($ext === 'xlsm' || $ext === 'xlsx') ? 1048576 : (($ext === 'xls') ? 65536 : 0);
                            $EMAXLEN_FILE = $ext;
                        } else {
                            if ($EXCELVER === '2007') {
                                $EXCELMAXLEN = 1048576;
                                $EMAXLEN_FILE = 'xlsx';
                            } elseif ($EXCELVER === '2003') {
                                $EXCELMAXLEN = 65536;
                                $EMAXLEN_FILE = 'xls';
                            }
                            if ($reWebf['result'] !== true) {
                                // e_log(showMsg($rsmaster['result']),'1');
                                $rtn = 1;
                            } else {
                            }
                        }
                        $FDB2CSV2 = cmGetFDB2CSV2($db2con, $d1name, true, false, false);
                        if ($FDB2CSV2['result'] === true) {
                            $FDB2CSV2 = umEx($FDB2CSV2['data']);
                           
                            $FDB2CSV2_ALL = cmGetFDB2CSV2($db2con, $d1name, false, false, false);
                            if ($FDB2CSV2_ALL['result'] === true) {
                                $FDB2CSV2_ALL = umEx($FDB2CSV2_ALL['data']);
                                if ($htmlFlg === '0') {
                                    if ($colFlg === '1') {
                                        //FDB2CSV2のカラムが'DEVQUERYH'のテーブルにない場合消す
                                        $FDB2CSV2_ALL = cmColNameDiff($db2con, $FDB2CSV2_ALL, $csvfilename);
                                        $FDB2CSV2 = cmColNameDiff($db2con, $FDB2CSV2, $csvfilename);
                                    }
                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                        $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                    } else {
                                        $allcount = cmGetAllCount($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                    }
                                    //MAX件数のためカウント
                                    $DB2EINSROWMAX+= ($allcount['data'] === '') ? 0 : $allcount['data'];
                                    if ($DB2EINSROWMAX > $EXCELMAXLEN) {
                                        //check maximum length of EXCEL
                                        $rtn = 1;
                                        $msg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                    } else {
                                        $head = array();
                                        if ($headerShow !== false) {
                                            $DB2WCOLTESTING = array();
                                            $DB2WCOLTESTING = ($D1SFLG === "1") ? array() : $DB2WCOL;
                                            $head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOLTESTING);
                                        }
                                        $sheet = php_excelCreateHeading($d1name, $NOTEMPLATE);
                                        if ($sheet['MSG'] !== '') {
                                            $rtn = 1;
                                            $msg = showMsg($sheet['MSG']);
                                        } else {
                                            $book = $sheet['BOOK'];
                                            $sheet = $sheet['SHEET'];
                                            $row = '';
                                        
                                            $row = cmCreateXLS($sheet, $head, $tabledata['data'], $FDB2CSV2, $dctext, $DB2WCOLTESTING, $d1name, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $row, $htmlFlg, $NOTEMPLATE);
                                        
                                            $lenArr = cmTotalLen($allcount['data']);
                                            $lenCount = count($lenArr);
                                            for ($idx = 0;$idx < $lenCount;++$idx) {
                                                if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                    $tabledata = cmGetDbFile($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOLTESTING, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                                                } else {
                                                    $tabledata = cmGetDbFile($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOLTESTING, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                                                }
                                                
                                                if ($tabledata['result'] !== true) {
                                                    $msg = showMsg($tabledata['result']);
                                                    $rtn = 1;
                                                } else {
                                                
                                                    $row = cmCreateXLS($sheet, $head, $tabledata['data'], $FDB2CSV2, $dctext, $DB2WCOLTESTING, $d1name, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $row, $htmlFlg, $NOTEMPLATE);
                                                    unset($tabledata);
                                                }
                                            }
                                        }
                                    }
                                } elseif ($htmlFlg === '1') {
                                    $head = array();
                                    if ($headerShow !== false) {
                                        $head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOL);
                                        $head = cmHsc($head);
                                    }
                                    if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                        $DB2WCOL = array();
                                        $allcount = cmGetAllCount_1($db2con, $FDB2CSV2, $csvfilename, $sSearch, '', array(), $filtersearchData);
                                        //MAX件数のためカウント
                                        $DB2EINSROWMAX+= ($allcount['data'] === '' || cmMer($rsMasterArr[0]['DCSUMF']) === '1') ? 0 : $allcount['data']; //
                                        $DB2EINSROWMAX+= cmGetALLCOUNTSEIKYO($db2con, $d1name, $csvfilename, $rsMasterArr, $FDB2CSV2_ALL, $sSearch, '');
                                        if ($DB2EINSROWMAX > $EXCELMAXLEN) {
                                            //check maximum length of EXCEL
                                            $rtn = 1;
                                            $msg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                        } else {
                                            $lenArr = cmTotalLen($allcount['data']);
                                            $lenCount = count($lenArr);
                                            array_unshift($head, '', '');
                                            $FDB2CSV2_unshift = cmArrayUnshift($FDB2CSV2);
                                            $DB2WCOL = array();
                                            // クエリ情報を取得する
                                            $sheet = php_excelCreateHeading($d1name, $NOTEMPLATE);
                                            if ($sheet['MSG'] !== '') {
                                                $rtn = 1;
                                                $msg = showMsg($sheet['MSG']);
                                            } else {
                                                $book = $sheet['BOOK'];
                                                $sheet = $sheet['SHEET'];
                                                $row = '';
                                             
                                                $row = cmCreateXLS($sheet, $head, $tabledata['data'], $FDB2CSV2_unshift, $dctext, $DB2WCOL, $d1name, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $row, $htmlFlg, $NOTEMPLATE);
                                                for ($idx = 0;$idx < $lenCount;++$idx) {
                                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                        $dFile1 = cmGetDbFile_1($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', $sSearch, '', $rsMasterArr, true, '', $webf, $d1name, $CNDSDATA,$EXCELVER, $filtersearchData);
                                                        
                                                    } else {
                                                        $dFile1 = cmGetDbFile_1($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', $sSearch, '', $rsMasterArr, true, '', $webf, $d1name, $CNDSDATA,$EXCELVER, $filtersearchData);
                                                       
                                                    }
                                                    if ($dFile1['result'] !== true) {
                                                        $msg = showMsg($dFile1['result']);
                                                        $rtn = 1;
                                                    } else {
                                                        // 集計情報を取得する
                                                        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                            $dFile2 = cmGetDbFile_2($db2con, $FDB2CSV2_ALL, $d1name, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $sSearch, '', $rsMasterArr, $rsshukeiArr, true, '', $webf, $d1name, $CNDSDATA, $filtersearchData);
                                                        } else {
                                                            $dFile2 = cmGetDbFile_2($db2RDBCon, $FDB2CSV2_ALL, $d1name, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $sSearch, '', $rsMasterArr, $rsshukeiArr, true, '', $webf, $d1name, $CNDSDATA, $filtersearchData);
                                                        }
                                                        if ($dFile2['result'] !== true) {
                                                            $msg = showMsg($dFile2['result']);
                                                            $rtn = 1;
                                                        } else {
                                                            // データをマージする
                                                            error_log('csv tesing 1**********');
                                                            $tabledata = cmShukeiDataMerge($db2con, $dFile1['data'], $dFile2['data'], $rsMasterArr, $rsshukeiArr, $FDB2CSV2, $FDB2CSV2_ALL, $allcount['data']);
                                                            if ($tabledata['result'] !== true) {
                                                                $msg = showMsg($tabledata['result']);
                                                                $rtn = 1;
                                                            } else {
                                                              
                                                                $row = cmCreateXLS($sheet, $head, $tabledata['data'], $FDB2CSV2_unshift, $dctext, $DB2WCOL, $d1name, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $row, $htmlFlg, $NOTEMPLATE);
                                                                unset($tabledata);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        $sheet = php_excelCreateHeading($d1name, $NOTEMPLATE);
                                        if ($sheet['MSG'] !== '') {
                                            $rtn = 1;
                                            $msg = showMsg($sheet['MSG']);
                                        } else {
                                            $book = $sheet['BOOK'];
                                            $sheet = $sheet['SHEET'];
                                            $row = '';
                                           
                                            $row = cmCreateXLS($sheet, $head, $tabledata['data'], $FDB2CSV2, $dctext, $DB2WCOL, $d1name, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $row, '0', $NOTEMPLATE);
                                            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                            } else {
                                                $allcount = cmGetAllCount($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                            }
                                            //MAX件数のためカウント
                                            $DB2EINSROWMAX+= ($allcount['data'] === '') ? 0 : $allcount['data'];
                                            if ($DB2EINSROWMAX > $EXCELMAXLEN) {
                                                //check maximum length of EXCEL
                                                $rtn = 1;
                                                $msg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                            } else {
                                                $lenArr = cmTotalLen($allcount['data']);
                                                $lenCount = count($lenArr);
                                                for ($idx = 0;$idx < $lenCount;++$idx) {
                                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                        $tabledata = cmGetDbFile($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOL, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                                                    } else {
                                                        $tabledata = cmGetDbFile($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOL, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                                                    }
                                                    if ($tabledata['result'] !== true) {
                                                        $msg = showMsg($tabledata['result']);
                                                        $rtn = 1;
                                                    } else {
                                                      
                                                        $row = cmCreateXLS($sheet, $head, $tabledata['data'], $FDB2CSV2, $dctext, $DB2WCOL, $d1name, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $row, '0', $NOTEMPLATE);
                                                        unset($tabledata);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if ($rtn !== 1) {
                                    if ($WDSERV !== '') {
                                        if (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) !== '/') {
                                            $WDSERV = '/' . $WDSERV . '/';
                                        } elseif (substr($WDSERV, 0, 1) === '/' && substr($WDSERV, -1) !== '/') {
                                            $WDSERV = $WDSERV . '/';
                                        } elseif (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) === '/') {
                                            $WDSERV = '/' . $WDSERV;
                                        }
                                        $filepath = substr($WDSERV, 0, intval(strrpos($WDSERV, '/')) + 1);
                                        if (!file_exists($filepath)) {
                                            if (!mkdir($filepath, 0777, true)) {
                                                $msg = showMsg('FAIL_FOLDER');
                                                $rtn = 1;
                                            } else {
                                                $fp = cmXlsDownload($d1name, $book, $time, $WUUID, $d1name, $EXCELVER, $NOTEMPLATE);
                                            }
                                        } else {
                                            $file = $WDSERV . 'xls';
                                            if (!file_exists($file)) {
                                                $createFile = touch($file);
                                            }
                                            if ($createFile === false) {
                                                $msg = showMsg('FAIL_FILEPATH');
                                                $rtn = 1;
                                            } else {
                                                $fp = cmXlsDownload($d1name, $book, $time, $WUUID, $d1name, $EXCELVER, $NOTEMPLATE);
                                                if (file_exists($file)) {
                                                    unlink($file);
                                                }
                                            }
                                        }
                                    } else {
                                        if ($WUSERV !== '') {
                                            if (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) !== '/') {
                                                $WUSERV = '/' . $WUSERV . '/';
                                            } elseif (substr($WUSERV, 0, 1) === '/' && substr($WUSERV, -1) !== '/') {
                                                $WUSERV = $WUSERV . '/';
                                            } elseif (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) === '/') {
                                                $WUSERV = '/' . $WUSERV;
                                            }
                                            $filepath = substr($WUSERV, 0, intval(strrpos($WUSERV, '/')) + 1);
                                            if (!file_exists($filepath)) {
                                                if (!mkdir($filepath, 0777, true)) {
                                                    $msg = showMsg('FAIL_FOLDER');
                                                    $rtn = 1;
                                                } else {
                                                    $fp = cmXlsDownload($d1name, $book, $time, $WUUID, $d1name, $EXCELVER, $NOTEMPLATE);
                                                }
                                            } else {
                                                $file = $WUSERV . '.xls';
                                                if (!file_exists($file)) {
                                                    $createFile = touch($file);
                                                }
                                                if ($createFile === false) {
                                                    $msg = showMsg('FAIL_FILEPATH');
                                                    $rtn = 1;
                                                } else {
                                                    $fp = cmXlsDownload($d1name, $book, $time, $WUUID, $d1name, $EXCELVER, $NOTEMPLATE);
                                                    if (file_exists($file)) {
                                                        unlink($file);
                                                    }
                                                }
                                            }
                                        } elseif ($WUSERV === '') {
                                            $fp = cmXlsDownload($d1name, $book, $time, $WUUID, $d1name, $EXCELVER, $NOTEMPLATE);
                                        }
                                    }
                                    cmEnd($db2con, $RTCD, $JSEQ, $csvfilename, $d1name);
                                    if ($webf !== '1') {
                                        cmInsertDB2WLOG($db2con, $logrs, $_SESSION['PHPQUERY']['user'][0]['WUUID'], 'E', $d1name, $CNDSDATA, '0', '');
                                    } else {
                                        cmInsertDB2WLOG($db2con, $logrs, $_SESSION['PHPQUERY']['user'][0]['WUUID'], 'E', $d1name, $CNDSDATA, '1', '');
                                    }
                                    /*if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME)=== false){
                                        cmDb2Close($db2RDBCon);
                                    }
                                    cmDb2Close($db2con);*/
                                }
                            } else {
                                $rtn = 1;
                                $msg = showMsg('FAIL_SYS'); //'プログラムが異常終了しました。管理者にお問い合わせください。';
                                
                            }
                        } else {
                            $rtn = 1;
                            $msg = showMsg('FAIL_SYS'); //'プログラムが異常終了しました。管理者にお問い合わせください。';
                            
                        }
                    } else {
                        $rtn = 1;
                        $msg = showMsg('NOTEXIST_TMP', array($d1name)); // $d1name . "のテンプレートは削除される可能性があります。";
                        
                    }
                } else if ($EXCELVER === 'XLSX') {
                  
                    $EXCELMAXLEN = 1048576;
                    $EMAXLEN_FILE = 'xlsx';
                    $reWebf = fnGetFDB2CSV1($db2con, $WUUID, $d1name);
                    $reWebf1 = fnGetDB2WUSR($db2con, $WUUID);
                    if ($reWebf['result'] !== true) {
                        $rtn = 1;
                    } else {
                        $WDSERV = $reWebf['data'][0]['WDSERV'];
                        /**AKZ フォルダスペース除去20170511**/
                        if ($WDSERV !== '') {
                            $WDSERVARR = explode('/', $WDSERV);
                            $WDSERV = '';
                            $WDSERVCOUNT = count($WDSERVARR) - 1;
                            for ($i = 0;$i < $WDSERVCOUNT;$i++) {
                                if (cmMer($WDSERVARR[$i]) != '') {
                                    $WDSERV.= '/' . replaceFilename(trim($WDSERVARR[$i]));
                                }
                            }
                            $WDSERV.= '/' . replaceFilename(ltrim($WDSERVARR[$WDSERVCOUNT]));
                        }
                        /**AKZ フォルダスペース除去20170511**/
                        $WUSERV = $reWebf1['data'][0]['WUSERV'];
                        /**AKZ フォルダスペース除去20170511**/
                        if ($WUSERV !== '') {
                            $WUSERVARR = explode('/', $WUSERV);
                            $WUSERV = '';
                            $WUSERVCOUNT = count($WUSERVARR) - 1;
                            /*foreach($WUSERVARR as $key => $value){
                                      $WUSERV .= '/'.trim($value);
                              }*/
                            for ($i = 0;$i < $WUSERVCOUNT;$i++) {
                                if (cmMer($WUSERVARR[$i]) != '') {
                                    $WUSERV.= '/' . replaceFilename(trim($WUSERVARR[$i]));
                                }
                            }
                            $WUSERV.= '/' . replaceFilename(ltrim($WUSERVARR[$WUSERVCOUNT]));
                        }
                        /**AKZ フォルダスペース除去20170511**/
                        $WUCTFL = $reWebf1['data'][0]['WUCTFL'];
                        $WUCNFL = $reWebf1['data'][0]['WUCNFL'];
                        $WDCTFL = $reWebf['data'][0]['WDCTFL'];
                        $WDCNFL = $reWebf['data'][0]['WDCNFL'];
                        $data_CSV1 = $reWebf['data'];
                        $data_WUSR1 = $reWebf1['data'];
                    }
                    /*****spout excel setting*****/
                    //$filePath = './spout_excel/'.$d1name.'.xlsx';
                    //$ext = 'xlsx';
                    $filePath = TEMP_DIR . cmMer($d1name) . $time . '.xlsx';
                    $writer = WriterFactory::create(Type::XLSX);
                    $writer->openToFile($filePath);
                    $FDB2CSV2 = cmGetFDB2CSV2($db2con, $d1name, true, false, false);
                    if ($FDB2CSV2['result'] === true) {
                        $FDB2CSV2 = umEx($FDB2CSV2['data']);
                        $FDB2CSV2_ALL = cmGetFDB2CSV2($db2con, $d1name, false, false, false);
                        if ($FDB2CSV2_ALL['result'] === true) {
                            if ($WDSERV !== '') {
                                if (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) !== '/') {
                                    $WDSERV = '/' . $WDSERV . '/';
                                } else if (substr($WDSERV, 0, 1) === '/' && substr($WDSERV, -1) !== '/') {
                                    $WDSERV = $WDSERV . '/';
                                } else if (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) === '/') {
                                    $WDSERV = '/' . $WDSERV;
                                }
                                $filepath = substr($WDSERV, 0, intval(strrpos($WDSERV, '/')) + 1);
                                if (!file_exists($filepath)) {
                                    if (!mkdir($filepath, 0777, true)) {
                                        $msg = showMsg('FAIL_FOLDER');
                                        $rtn = 1;
                                    } else {
                                        $dfilename = $dctext;
                                        if ($WDCNFL === '1') {
                                            $dfilename = $d1name;
                                        }
                                        if ($WDCTFL !== '1') {
                                            $xlsxfiledownload = $WDSERV . cmMer($dfilename) . '.xlsx';
                                        } else {
                                            $xlsxfiledownload = $WDSERV . cmMer($dfilename) . $time . '.xlsx';
                                        }
                                        //                                              $fp   = fopen($xlsxfiledownload, 'w');
                                        $fp = $writer->openToFile($xlsxfiledownload);
                                    }
                                } else {
                                    $dfilename = $dctext;
                                    if ($WDCNFL === '1') {
                                        $dfilename = $d1name;
                                    }
                                    if ($WDCTFL !== '1') {
                                        $xlsxfiledownload = $WDSERV . cmMer($dfilename) . '.xlsx';
                                    } else {
                                        $xlsxfiledownload = $WDSERV . cmMer($dfilename) . $time . '.xlsx';
                                    }
                                    unlinkFile($xlsxfiledownload); // unlink old file and copy temp file
                                    $fp = fopen($xlsxfiledownload, 'w');
                                    if (!$fp) {
                                        filerollBack($xlsxfiledownload);
                                        $msg = showMsg('FAIL_FILEPATH');
                                        $rtn = 1;
                                    } else {
                                        $writer->openToFile($xlsxfiledownload);
                                        $xlsxfiledownloadbk = $xlsxfiledownload . "_temp";
                                        if (file_exists($xlsxfiledownloadbk)) {
                                            unlink($xlsxfiledownloadbk);
                                        }
                                    }
                                }
                            } else {
                                if ($WUSERV !== '') {
                                    if (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) !== '/') {
                                        $WUSERV = '/' . $WUSERV . '/';
                                    } else if (substr($WUSERV, 0, 1) === '/' && substr($WUSERV, -1) !== '/') {
                                        $WUSERV = $WUSERV . '/';
                                    } else if (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) === '/') {
                                        $WUSERV = '/' . $WUSERV;
                                    }
                                    $filepath = substr($WUSERV, 0, intval(strrpos($WUSERV, '/')) + 1);
                                    if (!file_exists($filepath)) {
                                        if (!mkdir($filepath, 0777, true)) {
                                            $msg = showMsg('FAIL_FOLDER');
                                            $rtn = 1;
                                        } else {
                                            $dfilename = $dctext;
                                            if ($WUCNFL === '1') {
                                                $dfilename = $d1name;
                                            }
                                            if ($WUCTFL !== '1') {
                                                $xlsxfiledownload = $WUSERV . cmMer($dfilename) . '.xlsx';
                                            } else {
                                                $xlsxfiledownload = $WUSERV . cmMer($dfilename) . $time . '.xlsx';
                                            }
                                            $fp = $writer->openToFile($xlsxfiledownload);
                                        }
                                    } else {
                                        $dfilename = $dctext;
                                        if ($WUCNFL === '1') {
                                            $dfilename = $d1name;
                                        }
                                        if ($WUCTFL !== '1') {
                                            $xlsxfiledownload = $WUSERV . cmMer($dfilename) . '.xlsx';
                                        } else {
                                            $xlsxfiledownload = $WUSERV . cmMer($dfilename) . $time . '.xlsx';
                                        }
                                        unlinkFile($xlsxfiledownload);
                                        $fp = fopen($xlsxfiledownload, 'w');
                                        if (!$fp) {
                                            filerollBack($xlsxfiledownload);
                                            $msg = showMsg('FAIL_FILEPATH');
                                            $rtn = 1;
                                        } else {
                                            $fp = $writer->openToFile($xlsxfiledownload);
                                            $xlsxfiledownloadbk = $xlsxfiledownload . "_temp";
                                            if (file_exists($xlsxfiledownloadbk)) {
                                                unlink($xlsxfiledownloadbk);
                                            }
                                        }
                                    }
                                } else if ($WUSERV === '') {
                                    $xlsxfiledownload = TEMP_DIR . cmMer($d1name) . $time . '.xlsx';
                                    $fp = fopen($xlsxfiledownload, 'w');
                                    if (!$fp) {
                                        $msg = showMsg('FAIL_FILEPATH');
                                        $rtn = 1;
                                    } else {
                                        $fp = $writer->openToFile($xlsxfiledownload);
                                    }
                                }
                            }
                            spoutMojiSounyuu($writer, $DB2EINS, $DB2ECON, $CNDSDATAF);
                            $FDB2CSV2_ALL = umEx($FDB2CSV2_ALL['data']);
                            //grid data
                            if ($rtn === 0) {
                                if ($htmlFlg === '0') {
                                    if ($colFlg === '1') {
                                        //FDB2CSV2のカラムが'DEVQUERYH'のテーブルにない場合消す
                                        $FDB2CSV2_ALL = cmColNameDiff($db2con, $FDB2CSV2_ALL, $csvfilename);
                                        $FDB2CSV2 = cmColNameDiff($db2con, $FDB2CSV2, $csvfilename);
                                    }
                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
										error_log('filtersearchData6 in createdownloadfile '.print_r($filtersearchData,true));
                                        $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                    } else {
										error_log('filtersearchData6---- in createdownloadfile '.print_r($filtersearchData,true));
                                        $allcount = cmGetAllCount($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                    }
                                    /*$style = (new StyleBuilder()) ->setFontBold()->build();
                                        $style = (new StyleBuilder()) ->setFontSize(15)->build();
                                        $style = (new StyleBuilder()) ->setFontColor(Color::Color::BLUE)->build();*/
                                    $head = array();
                                    $head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOL);
                                    $head = cmHsc($head);
                                    //$writer->addRowWithStyle($head,$style);
                                    $writer->addRow($head);
                                    unset($head);
                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
										error_log('filtersearchData7 in createdownloadfile '.print_r($filtersearchData,true));
                                        $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                    } else {
										error_log('filtersearchData7---- in createdownloadfile '.print_r($filtersearchData,true));
                                        $allcount = cmGetAllCount($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                    }
                                    //MAX件数のためカウント
                                    $DB2EINSROWMAX+= ($allcount['data'] === '') ? 0 : $allcount['data'];
                                    if ($DB2EINSROWMAX > $EXCELMAXLEN) { //check maximum length of EXCEL
                                        $rtn = 1;
                                        $msg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                    } else {
                                        $lenArr = cmTotalLen($allcount['data']);
                                        $lenCount = count($lenArr);
                                        for ($idx = 0;$idx < $lenCount;$idx++) {
                                            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                $tabledata = cmGetDbFile($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOL, true, $D1CFLG, 'XLSX', $PDRILLDOWNSEARCH, $filtersearchData);
                                            } else {
                                                $tabledata = cmGetDbFile($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOL, true, $D1CFLG, 'XLSX', $PDRILLDOWNSEARCH, $filtersearchData);
                                            }
                                            if ($tabledata['result'] !== true) {
                                                $msg = showMsg($tabledata['result']);
                                                $rtn = 1;
                                            } else {
                                                $data = $tabledata['data'];
                                                $c_data = array_chunk($data, 100);
                                                foreach ($c_data as $ck => $cv) {
                                                    $writer->addRows($cv);
                                                }
                                                unset($tabledata);
                                            }
                                        }
                                    }
                                } else if ($htmlFlg === '1') { //grid data
                                   
                                    $head = array();
                                    $head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOL);
                                    $head = cmHsc($head);
                                    if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) { //制御あるかどうかのチェック
                                        $DB2WCOL = array();
										error_log('filtersearchData8 in createdownloadfile '.print_r($filtersearchData,true));
                                        $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, '',$PDRILLDOWNSEARCH);
										error_log('filtersearchData8---- in createdownloadfile '.print_r($filtersearchData,true));

                                        //MAX件数のためカウント
                                        $DB2EINSROWMAX+= ($allcount['data'] === '' || cmMer($rsMasterArr[0]['DCSUMF']) === '1') ? 0 : $allcount['data']; //
                                        $DB2EINSROWMAX+= cmGetALLCOUNTSEIKYO($db2con, $d1name, $csvfilename, $rsMasterArr, $FDB2CSV2_ALL, $sSearch, '');
                                        if ($DB2EINSROWMAX > $EXCELMAXLEN) { //check maximum length of EXCEL
                                            $rtn = 1;
                                            $msg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                        } else {
                                            $lenArr = cmTotalLen($allcount['data']);
                                            $lenCount = count($lenArr);
                                            array_unshift($head, "", "");
                                            $writer->addRow($head);
                                            unset($head);
                                            $FDB2CSV2_unshift = cmArrayUnshift($FDB2CSV2);
                                            $DB2WCOL = array();
                                            // クエリ情報を取得する
                                            $row = '';
                                            for ($idx = 0;$idx < $lenCount;$idx++) {
                                                if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                    $dFile1 = cmGetDbFile_1($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', $sSearch, '', $rsMasterArr, true, '', $webf, $d1name, $CNDSDATA, 'XLSX', $filtersearchData);
                                                   
                                                } else {
                                                    $dFile1 = cmGetDbFile_1($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', $sSearch, '', $rsMasterArr, true, '', $webf, $d1name, $CNDSDATA, 'XLSX', $filtersearchData);
                                                   
                                                }
                                                if ($dFile1['result'] !== true) {
                                                    $msg = showMsg($dFile1['result']);
                                                    $rtn = 1;
                                                } else {
                                                    // 集計情報を取得する
                                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                        $dFile2 = cmGetDbFile_2($db2con, $FDB2CSV2_ALL, $d1name, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $sSearch, '', $rsMasterArr, $rsshukeiArr, true, '', $webf, $d1name, $CNDSDATA, $filtersearchData);
														error_log('dFile2 = '.print_r($dFile2['data'],true));
                                                    } else {
                                                        $dFile2 = cmGetDbFile_2($db2RDBCon, $FDB2CSV2_ALL, $d1name, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $sSearch, '', $rsMasterArr, $rsshukeiArr, true, '', $webf, $d1name, $CNDSDATA, $filtersearchData);
                                                    }
                                                    if ($dFile2['result'] !== true) {
                                                        $msg = showMsg($dFile2['result']);
                                                        $rtn = 1;
                                                    } else { 
														$lenCount1 = $allcount['data'];
														if (count($filtersearchData) > 0) {
															foreach ($dFile2['data'] as $key => $row1) {
																$lenCount1 = $dFile2['data'][$key]['RN'];
															}
														}
                                                        // データをマージする                                                     
                                                        $tabledata = cmShukeiDataMerge($db2con, $dFile1['data'], $dFile2['data'], $rsMasterArr, $rsshukeiArr, $FDB2CSV2, $FDB2CSV2_ALL,$lenCount1 );
                                                     
                                                        if ($tabledata['result'] !== true) {
                                                            $msg = showMsg($tabledata['result']);
                                                            $rtn = 1;
                                                        } else {
                                                            $data = $tabledata['data'];
                                                            $c_data = array_chunk($data, 100);
                                                            foreach ($c_data as $ck => $cv) {
                                                                $writer->addRows($cv);
                                                            }
                                                            $ttabledata = $tabledata;
                                                          
                                                            unset($tabledata);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else { //html normal data
                                        $writer->addRow($head);
                                        unset($head);
                                        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
							error_log('filtersearchData9 in createdownloadfile '.print_r($filtersearchData,true));

                                            $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                        } else {
							error_log('filtersearchData9--- in createdownloadfile '.print_r($filtersearchData,true));

                                            $allcount = cmGetAllCount($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                        }
                                        //MAX件数のためカウント
                                        $DB2EINSROWMAX+= ($allcount['data'] === '') ? 0 : $allcount['data'];
                                        if ($DB2EINSROWMAX > $EXCELMAXLEN) { //check maximum length of EXCEL
                                            $rtn = 1;
                                            $msg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                        } else {
                                            $lenArr = cmTotalLen($allcount['data']);
                                            $lenCount = count($lenArr);
                                            for ($idx = 0;$idx < $lenCount;$idx++) {
                                                if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                    $tabledata = cmGetDbFile($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOL, true, $D1CFLG, 'XLSX', $PDRILLDOWNSEARCH, $filtersearchData);
                                                } else {
                                                    $tabledata = cmGetDbFile($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOL, true, $D1CFLG, 'XLSX', $PDRILLDOWNSEARCH, $filtersearchData);
                                                }
                                                if ($tabledata['result'] !== true) {
                                                    $msg = showMsg($tabledata['result']);
                                                    $rtn = 1;
                                                } else {
                                                    $data = $tabledata['data'];
                                                    $c_data = array_chunk($data, 100);
                                                    foreach ($c_data as $ck => $cv) {
                                                        $writer->addRows($cv);
                                                    }
                                                    unset($tabledata);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $rtn = 1;
                            $msg = showMsg('FAIL_SYS'); //'プログラムが異常終了しました。管理者にお問い合わせください。';
                            
                        }
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_SYS'); //'プログラムが異常終了しました。管理者にお問い合わせください。';
                        
                    }
                    $writer->close();
                    if ($webf !== '1') {
                        cmInsertDB2WLOG($db2con, $logrs, $_SESSION['PHPQUERY']['user'][0]['WUUID'], 'E', $d1name, $CNDSDATA, '0', '');
                    } else {
                        cmInsertDB2WLOG($db2con, $logrs, $_SESSION['PHPQUERY']['user'][0]['WUUID'], 'E', $d1name, $CNDSDATA, '1', '');
                    }
                 
                } elseif ($EXCELVER === 'html') {
                    $reWebf = fnGetFDB2CSV1($db2con, $WUUID, $d1name);
                    $reWebf1 = fnGetDB2WUSR($db2con, $WUUID);
                    if ($reWebf['result'] !== true) {
                        $rtn = 1;
                    } else {
                        $WDSERV = $reWebf['data'][0]['WDSERV'];
                        /**AKZ フォルダスペース除去20170511**/
                        if ($WDSERV !== '') {
                            $WDSERVARR = explode('/', $WDSERV);
                            $WDSERV = '';
                            $WDSERVCOUNT = count($WDSERVARR) - 1;
                            /*foreach($WDSERVARR as $key => $value){
                                      $WDSERV .= '/'.trim($value);
                              }*/
                            for ($i = 0;$i < $WDSERVCOUNT;++$i) {
                                if (cmMer($WDSERVARR[$i]) != '') {
                                    $WDSERV.= '/' . replaceFilename(trim($WDSERVARR[$i]));
                                }
                            }
                            $WDSERV.= '/' . replaceFilename(ltrim($WDSERVARR[$WDSERVCOUNT]));
                        }
                        /**AKZ フォルダスペース除去20170511**/
                        $WUSERV = $reWebf1['data'][0]['WUSERV'];
                        /**AKZ フォルダスペース除去20170511**/
                        if ($WUSERV !== '') {
                            $WUSERVARR = explode('/', $WUSERV);
                            $WUSERV = '';
                            $WUSERVCOUNT = count($WUSERVARR) - 1;
                            /*foreach($WUSERVARR as $key => $value){
                                      $WUSERV .= '/'.trim($value);
                              }*/
                            for ($i = 0;$i < $WUSERVCOUNT;++$i) {
                                if (cmMer($WUSERVARR[$i]) != '') {
                                    $WUSERV.= '/' . replaceFilename(trim($WUSERVARR[$i]));
                                }
                            }
                            $WUSERV.= '/' . replaceFilename(ltrim($WUSERVARR[$WUSERVCOUNT]));
                        }
                        /**AKZ フォルダスペース除去20170511**/
                        $WUCTFL = $reWebf1['data'][0]['WUCTFL'];
                        $WUCNFL = $reWebf1['data'][0]['WUCNFL'];
                        $WDCTFL = $reWebf['data'][0]['WDCTFL'];
                        $WDCNFL = $reWebf['data'][0]['WDCNFL'];
                        $data_CSV1 = $reWebf['data'];
                        $data_WUSR1 = $reWebf1['data'];
                    }
                    //MAX件数のためカウント
                    $EXCELMAXLEN = ($EXCELVER === 'html') ? 65536 : 0;
                    $EMAXLEN_FILE = 'xls';
                    $B_DDATA = array(); //ブラク配列
                    $FDB2CSV2 = cmGetFDB2CSV2($db2con, $d1name, true, false, false);
                    if ($FDB2CSV2['result'] === true) {
                        $FDB2CSV2 = $FDB2CSV2['data'];
                        $FDB2CSV2_ALL = cmGetFDB2CSV2($db2con, $d1name, false, false, false);
                        if ($FDB2CSV2_ALL['result'] === true) {
                            if ($WDSERV !== '') {
                                if (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) !== '/') {
                                    $WDSERV = '/' . $WDSERV . '/';
                                } elseif (substr($WDSERV, 0, 1) === '/' && substr($WDSERV, -1) !== '/') {
                                    $WDSERV = $WDSERV . '/';
                                } elseif (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) === '/') {
                                    $WDSERV = '/' . $WDSERV;
                                }
                                $filepath = substr($WDSERV, 0, intval(strrpos($WDSERV, '/')) + 1);
                                if (!file_exists($filepath)) {
                                    if (!mkdir($filepath, 0777, true)) {
                                        $msg = showMsg('FAIL_FOLDER');
                                        $rtn = 1;
                                    } else {
                                        $dfilename = $dctext;
                                        if ($WDCNFL === '1') {
                                            $dfilename = $d1name;
                                        }
                                        if ($WDCTFL !== '1') {
                                            $xlsfiledownload = $WDSERV . cmMer($dfilename) . '.xls';
                                        } else {
                                            $xlsfiledownload = $WDSERV . cmMer($dfilename) . $time . '.xls';
                                        }
                                        $fp = fopen($xlsfiledownload, 'w');
                                    }
                                } else {
                                    $dfilename = $dctext;
                                    if ($WDCNFL === '1') {
                                        $dfilename = $d1name;
                                    }
                                    if ($WDCTFL !== '1') {
                                        $xlsfiledownload = $WDSERV . cmMer($dfilename) . '.xls';
                                    } else {
                                        $xlsfiledownload = $WDSERV . cmMer($dfilename) . $time . '.xls';
                                    }
                                    /* if (file_exists($xlsfiledownload)) {
                                                unlink($xlsfiledownload);
                                             }*/
                                    unlinkFile($xlsfiledownload); // unlink old file and copy temp file
                                    $fp = fopen($xlsfiledownload, 'w');
                                    if (!$fp) {
                                        filerollBack($xlsfiledownload);
                                        $msg = showMsg('FAIL_FILEPATH');
                                        $rtn = 1;
                                    } else {
                                        $xlsfiledownloadbk = $xlsfiledownload . '_temp';
                                        if (file_exists($xlsfiledownloadbk)) {
                                            unlink($xlsfiledownloadbk);
                                        }
                                    }
                                }
                            } else {
                                
                                if ($WUSERV !== '') {
                                    if (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) !== '/') {
                                        $WUSERV = '/' . $WUSERV . '/';
                                    } elseif (substr($WUSERV, 0, 1) === '/' && substr($WUSERV, -1) !== '/') {
                                        $WUSERV = $WUSERV . '/';
                                    } elseif (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) === '/') {
                                        $WUSERV = '/' . $WUSERV;
                                    }
                                    $filepath = substr($WUSERV, 0, intval(strrpos($WUSERV, '/')) + 1);
                                    if (!file_exists($filepath)) {
                                        if (!mkdir($filepath, 0777, true)) {
                                            $msg = showMsg('FAIL_FOLDER');
                                            $rtn = 1;
                                        } else {
                                            $dfilename = $dctext;
                                            if ($WUCNFL === '1') {
                                                $dfilename = $d1name;
                                            }
                                            if ($WUCTFL !== '1') {
                                                $xlsfiledownload = $WUSERV . cmMer($dfilename) . '.xls';
                                            } else {
                                                $xlsfiledownload = $WUSERV . cmMer($dfilename) . $time . '.xls';
                                            }
                                            $fp = fopen($xlsfiledownload, 'w');
                                        }
                                    } else {
                                        $dfilename = $dctext;
                                        if ($WUCNFL === '1') {
                                            $dfilename = $d1name;
                                        }
                                        if ($WUCTFL !== '1') {
                                            $xlsfiledownload = $WUSERV . cmMer($dfilename) . '.xls';
                                        } else {
                                            $xlsfiledownload = $WUSERV . cmMer($dfilename) . $time . '.xls';
                                        }
                                        /*if (file_exists($xlsfiledownload)) {
                                                unlink($xlsfiledownload);
                                             }*/
                                        unlinkFile($xlsfiledownload);
                                        $fp = fopen($xlsfiledownload, 'w');
                                        if (!$fp) {
                                            filerollBack($xlsfiledownload);
                                            $msg = showMsg('FAIL_FILEPATH');
                                            $rtn = 1;
                                        } else {
                                            $xlsfiledownloadbk = $xlsfiledownload . '_temp';
                                            if (file_exists($xlsfiledownloadbk)) {
                                                unlink($xlsfiledownloadbk);
                                            }
                                        }
                                    }
                                } elseif ($WUSERV === '') {
                                    $fp = fopen(TEMP_DIR . cmMer($d1name) . $time . '.xls', 'w');
                                    if (!$fp) {
                                        $msg = showMsg('FAIL_FILEPATH');
                                        $rtn = 1;
                                    }
                                }
                            }
                            excelCreateHeading($fp, $DB2ECON, $DB2EINS, $CNDSDATAF);
                            $FDB2CSV2_ALL = umEx($FDB2CSV2_ALL['data']);
                            if ($htmlFlg === '0') {
                                if ($colFlg === '1') {
                                    //FDB2CSV2のカラムが'DEVQUERYH'のテーブルにない場合消す
                                    $FDB2CSV2_ALL = cmColNameDiff($db2con, $FDB2CSV2_ALL, $csvfilename);
                                    $FDB2CSV2 = cmColNameDiff($db2con, $FDB2CSV2, $csvfilename);
                                }
                                $head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOL);
                                $head = cmHsc($head);
                                cmExcelDownload($fp, $d1name, $B_DDATA, $head, $time, $FDB2CSV2, $DB2WCOL, $DB2ECON, $DB2EINS, $CNDSDATAF, true);
                                if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
							error_log('filtersearchData10 in createdownloadfile '.print_r($filtersearchData,true));

                                    $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                } else {
							error_log('filtersearchData10---- in createdownloadfile '.print_r($filtersearchData,true));

                                    $allcount = cmGetAllCount($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                }
                                //MAX件数のためカウント
                                $DB2EINSROWMAX+= ($allcount['data'] === '') ? 0 : $allcount['data'];
                                if ($DB2EINSROWMAX > $EXCELMAXLEN) {
                                    //check maximum length of EXCEL
                                    $rtn = 1;
                                    $msg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                } else {
                                    $lenArr = cmTotalLen($allcount['data']);
                                    $lenCount = count($lenArr);
                                    for ($idx = 0;$idx < $lenCount;++$idx) {
                                        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                            $tabledata = cmGetDbFile($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOL, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                                        } else {
                                            $tabledata = cmGetDbFile($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, '', $sSearch, $DB2WCOL, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                                        }
                                        if ($tabledata['result'] !== true) {
                                            $msg = showMsg($tabledata['result']);
                                            $rtn = 1;
                                        } else {
                                            cmExcelDownload($fp, $d1name, $tabledata['data'], $head, $time, $FDB2CSV2, $DB2WCOL, $DB2ECON, $DB2EINS, $CNDSDATAF, false);
                                            unset($tabledata);
                                        }
                                    }
                                }
                            } elseif ($htmlFlg === '1') {
                                $DB2WCOL = array();
                                if (count($rsMasterArr) !== 0) {
                                    $head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOL);
                                    $head = cmHsc($head);
                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
									error_log('filtersearchData11 in createdownloadfile '.print_r($filtersearchData,true));
                                        $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, '',$PDRILLDOWNSEARCH, $filtersearchData);
                                    } else {
										error_log('filtersearchData11---- in createdownloadfile '.print_r($filtersearchData,true));
                                        $allcount = cmGetAllCount($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, '',$PDRILLDOWNSEARCH, $filtersearchData);
                                    }
                                    //MAX件数のためカウントDCSUMF
                                    $DB2EINSROWMAX+= ($allcount['data'] === '' || cmMer($rsMasterArr[0]['DCSUMF']) === '1') ? 0 : $allcount['data'];
                                    $DB2EINSROWMAX+= cmGetALLCOUNTSEIKYO($db2con, $d1name, $csvfilename, $rsMasterArr, $FDB2CSV2_ALL, $sSearch, '');
                                    if ($DB2EINSROWMAX > $EXCELMAXLEN) {
                                        //check maximum length of EXCEL
                                        $rtn = 1;
                                        $msg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                    } else {
                                        $lenArr = cmTotalLen($allcount['data']);
                                        $lenCount = count($lenArr);
                                        array_unshift($head, '', '');
                                        $FDB2CSV2_unshift = cmArrayUnshift($FDB2CSV2);
                                        $DB2WCOL = array();
                                        cmExcelDownload($fp, $d1name, $B_DDATA, $head, $time, $FDB2CSV2_unshift, $DB2WCOL, $DB2ECON, $DB2EINS, $CNDSDATAF, true);
                                        for ($idx = 0;$idx < $lenCount;++$idx) {
                                            // クエリ情報を取得する
                                            //$dFile1 = cmGetDbFile_1($db2con, $FDB2CSV2_ALL, $csvfilename, '', '', '', '', '', $sSearch, $db2wcol, $rsMasterArr, true, '');
                                            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                $dFile1 = cmGetDbFile_1($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', $sSearch, '', $rsMasterArr, true, '', $webf, $d1name, $CNDSDATA,'html', $filtersearchData);
                                                
                                            } else {
                                                $dFile1 = cmGetDbFile_1($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', $sSearch, '', $rsMasterArr, true, '', $webf, $d1name, $CNDSDATA,'html', $filtersearchData);
                                               
                                            }
                                            if ($dFile1['result'] !== true) {
                                                $msg = showMsg($dFile1['result']);
                                                $rtn = 1;
                                            } else {
                                                // 集計情報を取得する
                                                if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                    $dFile2 = cmGetDbFile_2($db2con, $FDB2CSV2_ALL, $d1name, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $sSearch, '', $rsMasterArr, $rsshukeiArr, true, '', $webf, $d1name, $CNDSDATA, $filtersearchData);
                                                } else {
                                                    $dFile2 = cmGetDbFile_2($db2RDBCon, $FDB2CSV2_ALL, $d1name, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $sSearch, '', $rsMasterArr, $rsshukeiArr, true, '', $webf, $d1name, $CNDSDATA, $filtersearchData);
                                                }
                                                if ($dFile2['result'] !== true) {
                                                    $msg = showMsg($dFile2['result']);
                                                    $rtn = 1;
                                                } else {
                                                    // データをマージする
                                                 
                                                    $tabledata = cmShukeiDataMerge($db2con, $dFile1['data'], $dFile2['data'], $rsMasterArr, $rsshukeiArr, $FDB2CSV2, $FDB2CSV2_ALL, $allcount['data']);
                                                   
                                                    if ($tabledata['result'] !== true) {
                                                        $msg = showMsg($tabledata['result']);
                                                        $rtn = 1;
                                                    } else {
                                                      
                                                        cmExcelDownload($fp, $d1name, $tabledata['data'], $head, $time, $FDB2CSV2_unshift, $DB2WCOL, $DB2ECON, $DB2EINS, $CNDSDATAF, false);
                                                        unset($tabledata);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    
                                    $DB2WCOL = array();
                                    $head = cmCreateHeaderArray($FDB2CSV2, $DB2WCOL);
                                    $head = cmHsc($head);
                                   
                                    cmExcelDownload($fp, $d1name, $B_DDATA, $head, $time, $FDB2CSV2, $DB2WCOL, $DB2ECON, $DB2EINS, $CNDSDATAF, true);
                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                        $allcount = cmGetAllCount($db2con, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                    } else {
                                        $allcount = cmGetAllCount($db2RDBCon, $FDB2CSV2, $csvfilename, $sSearch, $D1CFLG, $PDRILLDOWNSEARCH, $filtersearchData);
                                    }
                                    //MAX件数のためカウント
                                    $DB2EINSROWMAX+= ($allcount['data'] === '') ? 0 : $allcount['data'];
                                    if ($DB2EINSROWMAX > $EXCELMAXLEN) {
                                        //check maximum length of EXCEL
                                        $rtn = 1;
                                        $msg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                    } else {
                                        $lenArr = cmTotalLen($allcount['data']);
                                        $lenCount = count($lenArr);
                                        for ($idx = 0;$idx < $lenCount;++$idx) {
                                            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                $tabledata = cmGetDbFile($db2con, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, $db2wcol, $sSearch, $DB2WCOL, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                                            } else {
                                                $tabledata = cmGetDbFile($db2RDBCon, $FDB2CSV2_ALL, $csvfilename, $lenArr[$idx][0], $lenArr[$idx][1], $iSortCol_0, $sSortDir_0, $db2wcol, $sSearch, $DB2WCOL, true, $D1CFLG, '', $PDRILLDOWNSEARCH, $filtersearchData);
                                            }
                                            if ($tabledata['result'] !== true) {
                                                $msg = showMsg($tabledata['result']);
                                                $rtn = 1;
                                            } else {
                                              
                                                cmExcelDownload($fp, $d1name, $tabledata['data'], $head, $time, $FDB2CSV2, $DB2WCOL, $DB2ECON, $DB2EINS, $CNDSDATAF, false);
                                                unset($tabledata);
                                            }
                                        }
                                    }
                                }
                            }
                            cmEnd($db2con, $RTCD, $JSEQ, $csvfilename, $d1name);
                            if ($webf !== '1') {
                                cmInsertDB2WLOG($db2con, $logrs, $_SESSION['PHPQUERY']['user'][0]['WUUID'], 'E', $d1name, $CNDSDATA, '0', '');
                            } else {
                                cmInsertDB2WLOG($db2con, $logrs, $_SESSION['PHPQUERY']['user'][0]['WUUID'], 'E', $d1name, $CNDSDATA, '1', '');
                            }
                            fwrite($fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                            fclose($fp);
                        } else {
                            $rtn = 1;
                            $msg = showMsg('FAIL_SYS'); //'プログラムが異常終了しました。管理者にお問い合わせください。';
                            
                        }
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_SYS'); //'プログラムが異常終了しました。管理者にお問い合わせください。';
                        
                    }
                }
            } else {
                if (!file_exists('./template/' . $d1name . '.' . $ext)) {
                    $rtn = 1;
                    $msg = showMsg('NOTEXIST_TMP', array($d1name)); //$d1name . "のテンプレートは削除される可能性があります。";
                    
                }
            }
        } elseif ($proc === 'PivotExcel' && $rtn === 0) {
            include_once 'PivotClass.php';
            $fdb2csv1 = FDB2CSV1_DATA($d1name);
            $D1WEBF = um($fdb2csv1['data'][0]['D1WEBF']);
            $xchecked = array(); //項目表示フラグ配列（縦軸）
            $ychecked = array(); //項目表示フラグ配列（横軸）
            $xcheckedUnhide = array(); //項目表示フラグ配列（縦軸）
            $ycheckedUnhide = array(); //項目表示フラグ配列（横軸）
            $cchecked = array(); //項目表示フラグ配列（集計）
            $xcheckedcount = 0; //横軸の表示カウント
            //pivot情報取得
            $db2pmst = fnGetDB2PMST($db2con, $d1name, $PMPKEY);
            $db2pcol = fnGetDB2PCOL($db2con, $d1name, $PMPKEY);
            $db2pcal = fnGetDB2PCAL($db2con, $d1name, $PMPKEY);
            $db2pmst = umEx($db2pmst);
            $db2pcol = umEx($db2pcol);
            $db2pcal = umEx($db2pcal);
            $xyColName = array(); //WPFLD + _ + WPFILIDを格納する一次元配列 フィルター対象項目の存在チェックに使用
           
            foreach ($db2pcol as $key => $value) {
                //FILD=9999の場合は無条件でarray()
                if ($value['WPFILID'] === '9999') {
                    $checkRs = array();
                } else {
                    $checkRs = cmCheckFDB2CSV2($db2con, $d1name, $value['WPFILID'], $value['WPFLD']);
                }
                if ($checkRs === false) {
                    $rtn = 2;
                    //      $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                    $msg = showMsg('FAIL_SYS');
                } else {
                    if (count($checkRs) === 0) {
                        $checkRs = cmCheckFDB2CSV5($db2con, $d1name, $value['WPFLD']);
                        if ($checkRs === false) {
                            $rtn = 2;
                            //    $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                            $msg = showMsg('FAIL_SYS');
                            break;
                        } else {
                            if (count($checkRs) === 0) {
                                $rtn = 2;
                                $msg = showMsg('PIVOT_NO_COLS'); //'クエリーに登録されていないカラムが選択されています。クエリーの設定をご確認ください。';
                                
                            }
                        }
                    }
                }
            }
            if ($rtn === 0) {
                //ダウンロード制限を見て、制限されているカラムが使われていたらメッセージ
                foreach ($db2pcol as $key => $value) {
                    $d2dnlf = fnGetD2DNLF($db2con, $d1name, $value['WPFILID'], $value['WPFLD']);
                    if ($d2dnlf === false) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_SYS'); //'プログラムが異常終了しました。管理者にお問い合わせください。';
                        break;
                    } else {
                        if ($d2dnlf === '1') {
                            $rtn = 1;
                            $msg = showMsg('DOWN_USE'); // 'ダウンロード制限で制限されている項目が使われています。';
                            break;
                        }
                    }
                    $xyColName[] = $value['WPFLD'] . '_' . $value['WPFILID'];
                }
            }
            if ($rtn === 0) {
                //フラグ
                $flg = '5';
                //フィルターデータ
                $searchdata = $ftext;
                $chkcoldata = $fcheck;
                $search_col_arr = $ffld;
                $operator = $fcombo;
                //フィルター条件がピボットの設定に使用されているかチェック。なかったらメッセージをリターン
                foreach ($chkcoldata as $cKey => $cValue) {
                    if (!in_array($search_col_arr[$cValue], $xyColName)) {
                        $msg = showMsg('FAIL_COL', array('ピボット', 'フィルター')); //'ピボットに設定されていない項目がフィルターに指定されています。<br/>ピボット設定を確認してください。';
                        $rtn = 1;
                        break;
                    }
                }
                if ($rtn === 0) {
                    //縦横集計を配列に格納
                    $shukeiXdisplayFlg = false;
                    $shukeiYdisplayFlg = false;
                    $columndata = array();
                    $columnupdata = array();
                    $calcdata = array();
                    $sortColData = '';
                    $sortColUpData = '';
                    foreach ($db2pcol as $value) {
                        $wppflg = $value['WPPFLG'];
                        $wpseqn = $value['WPSEQN'];
                        $wpfilid = $value['WPFILID'];
                        $column = $value['WPFLD'] . '_' . $value['WPFILID'];
                        $wpfhide = $value['WPFHIDE'];
                        $wpsort = $value['WPSORT'];
                        $wpsumg = $value['WPSUMG'];
                        $d2len = $value['D2LEN'];
                        $d2dec = $value['D2DEC'];
                        $d2type = $value['D2TYPE'];
                        $d2wedt = $value['D2WEDT'];
                        switch ($wppflg) {
                            case '1':
                                $columndata[$wpseqn] = $column;
                                if ($wpsumg === '') {
                                    $shukeiXdisplayFlg = true;
                                }
                                //表示非表示配列格納
                                //表示時にオブジェクトループしているのでキーで配列格納
                                $xchecked[$column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt,);
                                if ($wpfhide === '') {
                                    ++$xcheckedcount;
                                    $xcheckedUnhide[$column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt,);
                                }
                            break;
                            case '2':
                                $columnupdata[$wpseqn] = $column;
                                if ($wpsumg === '') {
                                    $shukeiYdisplayFlg = true;
                                }
                                //表示非表示配列格納
                                $ychecked[$column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt,);
                                if ($wpfhide === '') {
                                    $ycheckedUnhide[$column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt,);
                                }
                            break;
                            case '3':
                                $calcdata[$wpseqn] = $column;
                                //表示非表示配列格納
                                $cchecked[$column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt,);
                                $cchecked['_SUM_' . $column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt,);
                            break;
                        }
                    }
                    //計算式を配列に格納
                    $num_formula = array();
                    if (count($db2pcal) > 0) {
                        for ($i = 0;$i < 10;++$i) {
                            if (isset($db2pcal[$i])) {
                                $num_formula[] = $db2pcal[$i]['WCCALC'];
                            } else {
                                $num_formula[] = '';
                            }
                        }
                    } else {
                        $num_formula = array('', '', '', '', '',);
                    }
                    $columnup_data = $columnupdata;
                    $column_data = $columndata;
                    $x_checked = $xchecked;
                    $y_checked = $ychecked;
                    $reWebf = fnGetFDB2CSV1($db2con, $WUUID, $d1name);
                    $reWebf1 = fnGetDB2WUSR($db2con, $WUUID);
                    if ($reWebf['result'] !== true) {
                        $rtn = 1;
                    } else {
                        $WDSERV = $reWebf['data'][0]['WDSERV'];
                        /**AKZ フォルダスペース除去20170511**/
                        if ($WDSERV !== '') {
                            $WDSERVARR = explode('/', $WDSERV);
                            $WDSERV = '';
                            $WDSERVCOUNT = count($WDSERVARR) - 1;
                            /*foreach($WDSERVARR as $key => $value){
                                              $WDSERV .= '/'.trim($value);
                                      }*/
                            for ($i = 0;$i < $WDSERVCOUNT;++$i) {
                                if (cmMer($WDSERVARR[$i]) != '') {
                                    $WDSERV.= '/' . replaceFilename(trim($WDSERVARR[$i]));
                                }
                            }
                            $WDSERV.= '/' . replaceFilename(ltrim($WDSERVARR[$WDSERVCOUNT]));
                        }
                        /**AKZ フォルダスペース除去20170511**/
                        $WUSERV = $reWebf1['data'][0]['WUSERV'];
                        /**AKZ フォルダスペース除去20170511**/
                        if ($WUSERV !== '') {
                            $WUSERVARR = explode('/', $WUSERV);
                            $WUSERV = '';
                            $WUSERVCOUNT = count($WUSERVARR) - 1;
                            /*foreach($WUSERVARR as $key => $value){
                                              $WUSERV .= '/'.trim($value);
                                      }*/
                            for ($i = 0;$i < $WUSERVCOUNT;++$i) {
                                if (cmMer($WUSERVARR[$i]) != '') {
                                    $WUSERV.= '/' . replaceFilename(trim($WUSERVARR[$i]));
                                }
                            }
                            $WUSERV.= '/' . replaceFilename(ltrim($WUSERVARR[$WUSERVCOUNT]));
                        }
                        /**AKZ フォルダスペース除去20170511**/
                        $WUCTFL = $reWebf1['data'][0]['WUCTFL'];
                        $WDCTFL = $reWebf['data'][0]['WDCTFL'];
                        $data_CSV1 = $reWebf['data'];
                        $data_WUSR1 = $reWebf1['data'];
                    }
                    if ($WDSERV !== '') {
                        if (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) !== '/') {
                            $WDSERV = '/' . $WDSERV . '/';
                        } elseif (substr($WDSERV, 0, 1) === '/' && substr($WDSERV, -1) !== '/') {
                            $WDSERV = $WDSERV . '/';
                        } elseif (substr($WDSERV, 0, 1) !== '/' && substr($WDSERV, -1) === '/') {
                            $WDSERV = '/' . $WDSERV;
                        }
                        $filepath = substr($WDSERV, 0, intval(strrpos($WDSERV, '/')) + 1);
                        if (!file_exists($filepath)) {
                            if (!mkdir($filepath, 0777, true)) {
                                $msg = showMsg('FAIL_FOLDER');
                                $rtn = 1;
                            } else {
                                /*if($WDCTFL !== '1'){
                                $fp = fopen($WDSERV . cmMer($dctext) . '.xls', 'w');
                                }else{
                                $fp = fopen($WDSERV . cmMer($dctext) . $time . '.xls', 'w');
                                }*/
                                if ($WDCTFL !== '1') {
                                    $xlsfiledownload = $WDSERV . cmMer($dctext) . '.xls';
                                } else {
                                    $xlsfiledownload = $WDSERV . cmMer($dctext) . $time . '.xls';
                                }
                                $fp = fopen($xlsfiledownload, 'w');
                            }
                        } else {
                            /* if($WDCTFL !== '1'){
                                $fp = fopen($WDSERV . cmMer($dctext) . '.xls', 'w');
                            }else{
                                $fp = fopen($WDSERV . cmMer($dctext) . $time . '.xls', 'w');
                            }*/
                            if ($WDCTFL !== '1') {
                                $xlsfiledownload = $WDSERV . cmMer($dctext) . '.xls';
                            } else {
                                $xlsfiledownload = $WDSERV . cmMer($dctext) . $time . '.xls';
                            }
                            /*if (file_exists($xlsfiledownload)) {
                                unlink($xlsfiledownload);
                             }*/
                            unlinkFile($xlsfiledownload); // unlink old file and copy temp file
                            $fp = fopen($xlsfiledownload, 'w');
                            if (!$fp) {
                                filerollBack($xlsfiledownload);
                                $msg = showMsg('FAIL_FILEPATH');
                                $rtn = 1;
                            } else {
                                $xlsfiledownloadbk = $xlsfiledownload . '_temp';
                                if (file_exists($xlsfiledownloadbk)) {
                                    unlink($xlsfiledownloadbk);
                                }
                            }
                        }
                    } else {
                        if ($WUSERV !== '') {
                            if (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) !== '/') {
                                $WUSERV = '/' . $WUSERV . '/';
                            } elseif (substr($WUSERV, 0, 1) === '/' && substr($WUSERV, -1) !== '/') {
                                $WUSERV = $WUSERV . '/';
                            } elseif (substr($WUSERV, 0, 1) !== '/' && substr($WUSERV, -1) === '/') {
                                $WUSERV = '/' . $WUSERV;
                            }
                            $filepath = substr($WUSERV, 0, intval(strrpos($WUSERV, '/')) + 1);
                            if (!file_exists($filepath)) {
                                if (!mkdir($filepath, 0777, true)) {
                                    $msg = showMsg('FAIL_FOLDER');
                                    $rtn = 1;
                                } else {
                                    /*
                                    if($WUCTFL !== '1'){
                                    $fp = fopen($WUSERV . cmMer($dctext) . '.xls', 'w');
                                    }else{
                                    $fp = fopen($WUSERV . cmMer($dctext) . $time . '.xls', 'w');
                                    }*/
                                    if ($WUCTFL !== '1') {
                                        $xlsfiledownload = $WUSERV . cmMer($dctext) . '.xls';
                                    } else {
                                        $xlsfiledownload = $WUSERV . cmMer($dctext) . $time . '.xls';
                                    }
                                    $fp = fopen($xlsfiledownload, 'w');
                                }
                            } else {
                                /*if($WUCTFL !== '1'){
                                    $fp = fopen($WUSERV . cmMer($dctext) . '.xls', 'w');
                                }else{
                                    $fp = fopen($WUSERV . cmMer($dctext) . $time . '.xls', 'w');
                                }*/
                                if ($WUCTFL !== '1') {
                                    $xlsfiledownload = $WUSERV . cmMer($dctext) . '.xls';
                                } else {
                                    $xlsfiledownload = $WUSERV . cmMer($dctext) . $time . '.xls';
                                }
                                unlinkFile($xlsfiledownload); // unlink old file and copy temp file
                                $fp = fopen($xlsfiledownload, 'w');
                                if (!$fp) {
                                    filerollBack($xlsfiledownload);
                                    $msg = showMsg('FAIL_FILEPATH');
                                    $rtn = 1;
                                } else {
                                    $xlsfiledownloadbk = $xlsfiledownload . '_temp';
                                    if (file_exists($xlsfiledownloadbk)) {
                                        unlink($xlsfiledownloadbk);
                                    }
                                }
                            }
                        } elseif ($WUSERV === '') {
                            $fp = fopen(TEMP_DIR . $d1name . $time . '.xls', 'w');
                            if (!$fp) {
                                $msg = showMsg('FAIL_FILEPATH');
                                $rtn = 1;
                            }
                        }
                    }
                    if ($rtn === 0) {
                        $pgstart = 0;
                        $pgend = 0;
                        $zentaiCount = 0;
                        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                            $pivotcount = new Pivot($db2con, SAVE_DB, $csvfilename, $d1name);
                        } else {
                            $pivotcount = new Pivot($db2RDBCon, SAVE_DB, $csvfilename, $d1name);
                        }
                        $pivotcount->crossData($flg, $searchdata, $chkcoldata, $search_col_arr, $operator, $column_data, $columnup_data, $calcdata, $num_formula, $rowstart, $rowend, $pgstart, $pgend, $shukeiXdisplayFlg, $shukeiYdisplayFlg, $x_checked, $y_checked, true, false, $filtersearchData);
                        $zentaiCount = $pivotcount->get_zentaiCount();
                        //MAX件数のためカウント
                        $EXCELMAXLEN = 65536;
                        $pivotCountArr = array();
                        $EMAXLEN_FILE = 'xls';
                        $pivotCountArr = cmGetHeaderCountDB2PCOL($db2con, $d1name, $PMPKEY);
                        if ($pivotCountArr['result'] === true) {
                            $DB2EINSROWMAX+= $pivotCountArr['TOTALCOUNT']; //ピボートヘッダーカウント
                            
                        }
                        $DB2EINSROWMAX+= $zentaiCount;
                        if ($DB2EINSROWMAX > $EXCELMAXLEN) {
                            //check maximum length of EXCEL
                            $rtn = 1;
                            $msg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                        } else {
                            $lenArr = cmPivotTotalLen($zentaiCount);
                            $lenCount = count($lenArr);
                            $lastrowxlsf = array(); // xls file last row
                            $xlsCount = 0;
                            if ($lenCount > 0) {
                                for ($idx = 0;$idx < $lenCount;++$idx) {
                                    $lastFlg = false;
                                    if ($idx === ($lenCount - 1)) {
                                        $lastFlg = true; //総合計出さない
                                        
                                    }
                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                        $dbCond = $db2con;
                                    } else {
                                        $dbCond = $db2RDBCon;
                                    }
                                    $pgstart = $lenArr[$idx][0];
                                    $pgend = $lenArr[$idx][1];
									
									if($pvFlg === '0'){
	                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
	                                        $pivot = new Pivot($db2con, SAVE_DB, $csvfilename, $d1name);
	                                    } else {
	                                        $pivot = new Pivot($db2RDBCon, SAVE_DB, $csvfilename, $d1name);
	                                    }
	                                    $pivotRs = $pivot->crossData($flg, $searchdata, $chkcoldata, $search_col_arr, $operator, $column_data, $columnup_data, $calcdata, $num_formula, $rowstart, $rowend, $pgstart, $pgend, $shukeiXdisplayFlg, $shukeiYdisplayFlg, $x_checked, $y_checked, false, $lastFlg, $filtersearchData);                                  
									
										$file_name = $d1name;
	                                    $columnupdata = $pivot->get_columnupdata();
	                                    $getcheadingcol = $pivot->get_getcheadingcol();
	                                    $columndata = $pivot->get_columndata();
	                                    $colslists = $pivot->get_colslists();
	                                    $countcalc = $pivot->get_countcalc();
	                                    $num_count = $pivot->get_num_count();
	                                    $rtncolumn = $pivot->get_rtncolumn();
	                                    $data_studio = $pivot->get_data_studio();
	                                    $xchecked = $xcheckedUnhide;
	                                    $ychecked = $ycheckedUnhide;
	                                    $headerFlg = false;
	                                    if ($idx === 0) {
	                                        $rtnResult = array();
	                                        $headerFlg = true;
	                                    }
	                                    $rtnArr = cmPivotDownload($fp, $file_name, $columnupdata, $getcheadingcol, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, $time, $xchecked, $ychecked, $cchecked, $xcheckedcount, $D1WEBF, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg, $rtnResult, $xlsCount, $zentaiCount, $lastrowxlsf);
	                                    
	                                    $rtnResult = $rtnArr['RTNRESULT'];
	                                    $xlsCount = $rtnArr['ROWCOUNT'];
	                                    $lastrowxlsf = $rtnArr['LASTROW'];
	                                    $data = $data_studio; 
									}
									else{
										$flg = '2';					              
					                    $getcheadingcol = array();
					                    $data_studio = array();
					                    $rtncolumn = array();
					                    $colslists = array();
										do {
					                        //   e_log("column loop ".$testcount++);// 後で消す
					                        e_log('data ' . $idx . '_' . $lenCount . '_START' . $rowstart . '_END' . $rowend);
					                        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
					                            $pivot = new Pivot($db2con, SAVE_DB, $csvfilename, $d1name);
					                        } else {
					                            $pivot = new Pivot($db2RDBcon, SAVE_DB, $csvfilename, $d1name);
					                        }
					                        $pivot->crossData($flg, $searchdata, $chkcoldata, $search_col_arr, $operator, $columndata, $columnupdata, $calcdata, $num_formula, $rowstart, $rowend, $pgstart, $pgend, $shukeiXdisplayFlg, $shukeiYdisplayFlg, $xchecked, $ychecked, false, $lastFlg, $filtersearchData);
					                        $file_name = $d1name;
					                        $columnupdata = $pivot->get_columnupdata();
	                                    	$getcheadingcol = $pivot->get_getcheadingcol();
					                        $rowstart = $pivot->get_rowstart();
					                        $rowend = $pivot->get_rowend();
					                        $endcount = $pivot->get_endcount();

					                        if (count($columnupdata) > 0) {
					                            $getcheadingcol = array_merge($getcheadingcol, $pivot->get_getcheadingcol());
					                        } else {
					                            $getcheadingcol = $pivot->get_getcheadingcol();
					                        }
					                        $columndata = $pivot->get_columndata();
					                        if (count($colslists) > 0) {
					                            $colslists = array_merge($colslists, $pivot->get_colslists());
					                        } else {
					                            $colslists = $pivot->get_colslists();
					                        }
					                        $countcalc = $pivot->get_countcalc();
					                        $num_count = $pivot->get_num_count();
					                        $rtncolumn = array_merge($rtncolumn, $pivot->get_rtncolumn());
											//e_log('Count Data studio in looping*****'.count($data_studio));
											//e_log('get_data_studio in looping*****'.print_r($pivot->get_data_studio(),true));

					                        if (count($data_studio) > 0) {
					                            foreach ($pivot->get_data_studio() as $key => $value) {
					                                foreach ($value as $k => $v) {
					                                    $data_studio[$key][$k] = $v;
					                                }
					                            }
					                        } else {
					                            $data_studio = $pivot->get_data_studio();
					                        }
					                        $gb_count = count($data_studio);
					                        $flg = '3';
					                    } while ($endcount !== $rowend && count($columnupdata) > 0);
										$xchecked = $xcheckedUnhide;
	                                    $ychecked = $ycheckedUnhide;
	                                    $headerFlg = false;
	                                    if ($idx === 0) {
	                                        $rtnResult = array();
	                                        $headerFlg = true;
	                                    }
	                                    $rtnArr = cmPivotDownload($fp, $file_name, $columnupdata, $getcheadingcol, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, $time, $xchecked, $ychecked, $cchecked, $xcheckedcount, $D1WEBF, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg, $rtnResult, $xlsCount, $zentaiCount, $lastrowxlsf);
	                                    
	                                    $rtnResult = $rtnArr['RTNRESULT'];
	                                    $xlsCount = $rtnArr['ROWCOUNT'];
	                                    $lastrowxlsf = $rtnArr['LASTROW'];
	                                    $data = $data_studio; 
									}
                                }
                            } else {
                                $data_studio = array();
                                $rtnResult = array();
                                $rtnArr = cmPivotDownload($fp, $file_name, $columnupdata, $getcheadingcol, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, $time, $xchecked, $ychecked, $cchecked, $xcheckedcount, $D1WEBF, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg, $rtnResult, $xlsCount, $zentaiCount, $lastrowxlsf);
                                
                                $rtnResult = $rtnArr['RTNRESULT'];
                                $xlsCount = $rtnArr['ROWCOUNT'];
                                $lastrowxlsf = $rtnArr['LASTROW'];
                              
                                $data = $data_studio;
                            }
                        }
                        fwrite($fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                        fclose($fp);
                        if ($webf !== '1') {
                            cmInsertDB2WLOG($db2con, $logrs, $_SESSION['PHPQUERY']['user'][0]['WUUID'], 'E', $d1name, $CNDSDATA, '0', '');
                        } else {
                            cmInsertDB2WLOG($db2con, $logrs, $_SESSION['PHPQUERY']['user'][0]['WUUID'], 'E', $d1name, $CNDSDATA, '1', '');
                        }
                    }
                    /*cmDb2Close($db2con);
                    if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
                        cmDb2Close($db2RDBCon);
                    }*/
                }
            }
        }
    }
}
// ワークテーブルのログ情報削除
$resDel = cmDelWTBL($db2con, $csvfilename);
if ($resDel !== 0) {
    //修正用
    $msg = showMsg('ログ削除失敗'); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
    $rtn = 1;
}
if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
    cmDb2Close($db2RDBcon);
}
cmDb2Close($db2con);
$rtnAry = array('RTN' => $rtn, 'MSG' => $msg, 'POSTDATA' => $_POST, 'DB2WCOL' => $DB2WCOL, 'DATA' => $tabledata['data'], 'columnupdata' => $columnupdata, 'getcheadingcol' => $getcheadingcol, 'columndata' => $columndata, 'colslists' => $colslists, 'countcalc' => $countcalc, 'num_count' => $num_count, 'rtncolumn' => $rtncolumn, 'numlist' => $numlist, 'ext' => $ext, 'd1edkb' => $d1edkb, 'FDB2CSV2' => $FDB2CSV2, 'd1name' => $d1name, //後で削除
'd1serv' => $WDSERV, 'wuserv' => $WUSERV, 'WUDID' => $WUUID, 'aaCSV1' => $data_CSV1, 'aaWUSR' => $data_WUSR1, 'DB2EINSROWMAX' => $DB2EINSROWMAX, //testing klyh
'EXCELMAXLEN' => $EXCELMAXLEN, 'fcsvnm' => $fcsvnm, 'fxlsmnm' => $fxlsmnm, 'ExcelTime' => $dexname, 'PROC' => $proc, 'SORT' => $Sort, 'EXCELVER' => $EXCELVER, 'tabledata' => $ttabledata, 'dFile1' => $dFile1);
echo (json_encode($rtnAry));
function fnGetDB2PMST($db2con, $d1name, $pmpkey) {
    $rtn = array();
    $data = array();
    $strSQL = '';
    $strSQL.= ' SELECT * FROM DB2PMST ';
    $strSQL.= ' WHERE PMNAME = ? AND PMPKEY = ? ';
    $params = array($d1name, $pmpkey,);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    $rtn = $data;
    return $rtn;
}
function fnGetDB2PCOL($db2con, $d1name, $wppkey) {
    $rtn = array();
    $data = array();
    $strSQL = '';
    $strSQL.= ' SELECT A.*,B.D2LEN,B.D2DEC,B.D2TYPE,B.D2WEDT FROM DB2PCOL AS A ';
    $strSQL.= ' LEFT JOIN ';
    $strSQL.= ' ( ';
    $strSQL.= ' SELECT D2NAME,D2FILID,D2FLD,D2TYPE,D2LEN,D2DEC,D2WEDT from FDB2CSV2 ';
    $strSQL.= ' UNION ';
    $strSQL.= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID,D5FLD AS D2FLD,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DEC AS D2DEC,D5WEDT AS D2WEDT from FDB2CSV5 ';
    $strSQL.= ' ) AS B ';
    $strSQL.= ' ON A.WPNAME = B.D2NAME AND A.WPFILID = B.D2FILID AND A.WPFLD = B.D2FLD ';
    $strSQL.= ' WHERE WPNAME = ? AND WPPKEY = ? ';
    $strSQL.= ' ORDER BY WPPFLG,WPSEQN ';
    $params = array($d1name, $wppkey,);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    $rtn = $data;
    return $rtn;
}
function fnGetDB2PCAL($db2con, $d1name, $wcckey) {
    $rtn = array();
    $data = array();
    $strSQL = '';
    $strSQL.= ' SELECT * FROM DB2PCAL ';
    $strSQL.= ' WHERE WCNAME = ? AND WCCKEY = ? ';
    $strSQL.= ' ORDER BY WCSEQN ';
    $params = array($d1name, $wcckey,);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    $rtn = $data;
    return $rtn;
}
function fnGetD2DNLF($db2con, $d1name, $d2filid, $d2fld) {
    $data = array();
    $strSQL = '';
    $strSQL.= ' SELECT A.D2DNLF FROM ';
    $strSQL.= ' ( ';
    $strSQL.= ' SELECT D2NAME,D2FILID,D2FLD,D2DNLF FROM FDB2CSV2 ';
    $strSQL.= ' UNION ';
    $strSQL.= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID,D5FLD AS D2FLD,D5DNLF AS D2DNLF FROM FDB2CSV5 ';
    $strSQL.= ' ) AS A ';
    $strSQL.= ' WHERE A.D2NAME = ? ';
    $strSQL.= ' AND A.D2FILID = ? ';
    $strSQL.= ' AND A.D2FLD = ? ';
    $params = array($d1name, $d2filid, $d2fld,);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $rs = db2_execute($stmt, $params);
        if ($rs === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data = $row['D2DNLF'];
            }
        }
    }
    return $data;
}
function fnGetFDB2CSV1($db2con, $WUUID, $D1NAME) {
    $data = array();
    $strSQL = ' SELECT ';
    $strSQL.= ' A.WDSERV,A.WDCTFL,A.WDCNFL ';
    $strSQL.= ' FROM DB2WDEF as A ';
    $strSQL.= ' WHERE A.WDUID = ? ';
    $strSQL.= ' AND A.WDNAME = ? ';
    $params = array($WUUID, $D1NAME,);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $data = array('result' => 'NOTEXIST_GET');
            } else {
                $data = array('result' => true, 'data' => umEx($data));
            }
        }
    }
    return $data;
}
function fnGetDB2WUSR($db2con, $WUUID) {
    $data = array();
    $strSQL = ' SELECT ';
    $strSQL.= ' A.WUSERV,A.WUCTFL,A.WUCNFL ';
    $strSQL.= ' FROM DB2WUSR as A ';
    $strSQL.= ' WHERE A.WUUID = ? ';
    $params = array($WUUID,);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $data = array('result' => 'NOTEXIST_GET');
            } else {
                $data = array('result' => true, 'data' => umEx($data));
            }
        }
    }
    return $data;
}
//SPOUT EXCELの見出しの上に文字挿入と抽出条件の表示設定
function spoutMojiSounyuu($writer, $DB2EINS, $DB2ECON, $CNDSDATAF) {
   
    $c = 1;
    $style = '';
    $blankArray = array('');
    foreach ($DB2EINS as $key => $value) {
        $row = (int)$value['EIROW'];
        while ($c < $row) {
            $writer->addRow($blankArray);
            $c++;
        }
        $c++;
        $eisize = (int)$value['EISIZE'];
        $eicolor = $value['EICOLR'];
        $val = $value['EITEXT'];
        $dta = array($val);
        $style = new StyleBuilder();
        if ($value['EIBOLD'] !== '') {
            $style = $style->setFontBOLD();
        }
        if ($eisize > 0) {
            $style = $style->setFontSize($eisize);
        }
        //$style = (new StyleBuilder()) ->setFontSize(24)->build();
        if ($eicolor !== '') {
            //$style = $style->setFontColor(Color::BLUE);
            if ($eicolor === "#000000") {
                $style->setFontColor(Color::C000000);
            } else if ($eicolor === "#800000") {
                $style->setFontColor(Color::C800000);
            } else if ($eicolor === "#FF0000") {
                $style->setFontColor(Color::CFF0000);
            } else if ($eicolor === "#FF00FF") {
                $style->setFontColor(Color::CFF00FF);
            } else if ($eicolor === "#FF99CC") {
                $style->setFontColor(Color::CFF99CC);
            } else if ($eicolor === "#993300") {
                $style->setFontColor(Color::C993300);
            } else if ($eicolor === "#FF6600") {
                $style->setFontColor(Color::CFF6600);
            } else if ($eicolor === "#FF9900") {
                $style->setFontColor(Color::CFF9900);
            } else if ($eicolor === "#FFCC00") {
                $style->setFontColor(Color::CFFCC00);
            } else if ($eicolor === "#FFCC99") {
                $style->setFontColor(Color::CFFCC99);
            } else if ($eicolor === "#333300") {
                $style->setFontColor(Color::C333300);
            } else if ($eicolor === "#808000") {
                $style->setFontColor(Color::C808000);
            } else if ($eicolor === "#99CC00") {
                $style->setFontColor(Color::C99CC00);
            } else if ($eicolor === "#FFFF00") {
                $style->setFontColor(Color::CFFFF00);
            } else if ($eicolor === "#FFFF99") {
                $style->setFontColor(Color::CFFFF99);
            } else if ($eicolor === "#003300") {
                $style->setFontColor(Color::C003300);
            } else if ($eicolor === "#008000") {
                $style->setFontColor(Color::C008000);
            } else if ($eicolor === "#339966") {
                $style->setFontColor(Color::C339966);
            } else if ($eicolor === "#00FF00") {
                $style->setFontColor(Color::C00FF00);
            } else if ($eicolor === "#CCFFCC") {
                $style->setFontColor(Color::CCCFFCC);
            } else if ($eicolor === "#003366") {
                $style->setFontColor(Color::C003366);
            } else if ($eicolor === "#008080") {
                $style->setFontColor(Color::C008080);
            } else if ($eicolor === "#33CCCC") {
                $style->setFontColor(Color::C33CCCC);
            } else if ($eicolor === "#00FFFF") {
                $style->setFontColor(Color::C00FFFF);
            } else if ($eicolor === "#CCFFFF") {
                $style->setFontColor(Color::CCCFFFF);
            } else if ($eicolor === "#000080") {
                $style->setFontColor(Color::C000080);
            } else if ($eicolor === "#0000FF") {
                $style->setFontColor(Color::C0000FF);
            } else if ($eicolor === "#3366FF") {
                $style->setFontColor(Color::C3366FF);
            } else if ($eicolor === "#00CCFF") {
                $style->setFontColor(Color::C00CCFF);
            } else if ($eicolor === "#99CCFF") {
                $style->setFontColor(Color::C99CCFF);
            } else if ($eicolor === "#333399") {
                $style->setFontColor(Color::C333399);
            } else if ($eicolor === "#666699") {
                $style->setFontColor(Color::C666699);
            } else if ($eicolor === "#800080") {
                $style->setFontColor(Color::C800080);
            } else if ($eicolor === "#993366") {
                $style->setFontColor(Color::C993366);
            } else if ($eicolor === "#CC99FF") {
                $style->setFontColor(Color::CCC99FF);
            } else if ($eicolor === "#333333") {
                $style->setFontColor(Color::C333333);
            } else if ($eicolor === "#808080") {
                $style->setFontColor(Color::C808080);
            } else if ($eicolor === "#969696") {
                $style->setFontColor(Color::C969696);
            } else if ($eicolor === "#C0C0C0") {
                $style->setFontColor(Color::CC0C0C0);
            } else if ($eicolor === "#FFFFFF") {
                $style->setFontColor(Color::CFFFFFF);
            } else if ($eicolor === "#9999FF") {
                $style->setFontColor(Color::C9999FF);
            } else if ($eicolor === "#FFFFCC") {
                $style->setFontColor(Color::CFFFFCC);
            } else if ($eicolor === "#660066") {
                $style->setFontColor(Color::C660066);
            } else if ($eicolor === "#FF8080") {
                $style->setFontColor(Color::CFF8080);
            } else if ($eicolor === "#0066CC") {
                $style->setFontColor(Color::C0066CC);
            } else if ($eicolor === "#CCCCFF") {
                $style->setFontColor(Color::CCCCCFF);
            }
        }
        $style = $style->build();
        $writer->addRowWithStyle($dta, $style);
        $style = '';
    }
    if (count($DB2ECON) > 0 && count($CNDSDATAF) > 0) {
        $ECFLG = $DB2ECON[0]['ECFLG'];
        if ($ECFLG === '1') {
            $writer->addRow($blankArray);
            foreach ($CNDSDATAF as $key => $value) {
                $searchDta = array();
                $isSQL = $value['isSQL'];
                if ($isSQL === '1') { //ウエブＳＱＬの場合
                    $cndlabel = cmHsc(cmHscDe($value['cndlabel']));
                    $cnddatadecode = cmHsc(cmHscDe($value['cnddata']));
                    array_push($searchDta, $cndlabel, $cnddatadecode);
                } else {
                    $gandor = cmMer($value['gandor']);
                    if ($gandor !== 'seq') {
                        $gandor = $value['gandor'];
                        array_push($searchDta, $gandor);
                    } else {
                        $gandor = '';
                    }
                    $andor = $value['andor'];
                    $cndlabel = $value['cndlabel'];
                    $cnddatadecode = $value['cnddata'];
                    $cndtype = $value['cndtype'];
                    array_push($searchDta, $andor, $cndlabel, $cnddatadecode, $cndtype);
                }
                $writer->addRow($searchDta);
            }
        }
        $ECROWN = (int)$DB2ECON[0]['ECROWN'];
        for ($i = 0;$i < $ECROWN;$i++) {
            $writer->addRow($blankArray);
        }
    }
}
