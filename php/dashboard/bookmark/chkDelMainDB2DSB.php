<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../../common/inc/config.php");
include_once ("../../common/inc/common.inc.php");
include_once ("../../licenseInfo.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
$ISDELMAIN = (isset($_POST['ISDELMAIN'])) ? $_POST['ISDELMAIN'] : '';
$DSBID = (isset($_POST['DSBID'])) ? $_POST['DSBID'] : '';
$DSBMAIN = (isset($_POST['DSBMAIN'])) ? $_POST['DSBMAIN'] : '';
/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$rtnArray = array(); //リターン配列
$rtn = 0;
$msg = '';
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true && $rs['result'] !== 'FAIL_USR_AUT') {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    }
}
//ダッシューボードが存在してるかどうかチェック
if ($rtn === 0) {
    $rs = cmCheckDB2DSB($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID'], $DSBID);
    if ($rs !== true) {
        $rtn = 2;
        $msg = showMsg($rs, array('ダッシューボードキー'));
    }
}
if ($rtn === 0) {
    //DB2DSB削除とマイン
    if ($ISDELMAIN === 'DEL') { //DB2DSB削除
        $r = fnDel_DB2DSB($db2con, $DSBID);
        if ($r !== true) {
            $rtn = 1;
            $msg = showMsg($r);
        }
    } else if ($ISDELMAIN === 'MAIN') { //DB2DSBマイン
        $r = fnUp_DB2DSB($db2con, $DSBID, $DSBMAIN);
        if ($r !== true) {
            $rtn = 1;
            $msg = showMsg($r);
        }
    }
}
cmDb2Close($db2con);
$rtnArray = array('RTN' => $rtn, 'MSG' => $msg);
echo (json_encode($rtnArray));
/**
 *削除DB2DSB
 *
 */
function fnDel_DB2DSB($db2con, $DSBID) {
    $USR_ID = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
    $rs = true;
    $strSQL = '';
    $strSQL = ' DELETE FROM DB2DSB WHERE DSBUID=? AND DSBID =? ';
    $params = array($USR_ID, $DSBID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_DEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_DEL';
        } else {
            $r = fnDel_DB2BMK($db2con, $DSBID);
            if ($r !== true) {
                $rs = $r;
            }
        }
    }
    return $rs;
}
function fnDel_DB2BMK($db2con, $DSBID) {
    $rs = true;
    $strSQL = '';
    $strSQL = ' DELETE FROM DB2BMK WHERE BMKDSBID=?';
    $params = array($DSBID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt1 === false) {
        $rs = 'FAIL_DEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
/**
 *ダッシュボードIDのため更新
 *UPDATE DB2DSB WITH DSBID
 */
function fnUp_DB2DSB($db2con, $DSBID, $DSBMAIN) {
    $USR_ID = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
    $rs = true;
    $strSQL = '';
    $strSQL = ' UPDATE DB2DSB SET DSBMAIN=? WHERE DSBUID=? AND DSBID=? ';
    $params = array($DSBMAIN, $USR_ID, $DSBID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_UPD';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_UPD';
        } else {
            if ($DSBMAIN === '1') {
                $r = fnUpdDSBUID_DB2DSB($db2con, $DSBID, $USR_ID);
                if ($r !== true) {
                    $rs = $r;
                }
            }
        }
    }
    return $rs;
}
/**
 *ユーザーIDのため更新
 *UPDATE DB2DSB WITH DSUID
 */
function fnUpdDSBUID_DB2DSB($db2con, $DSBID, $USR_ID) {
    $rs = true;
    $strSQL = '';
    $strSQL = ' UPDATE DB2DSB SET DSBMAIN=? WHERE DSBUID=? AND DSBID<>? ';
    $params = array('0', $USR_ID, $DSBID);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt1 === false) {
        $rs = 'FAIL_UPD';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}
