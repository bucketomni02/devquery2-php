<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");

$rtnArray = array();        //リターン配列
$rtn = 0;
$msg = '';
$data=array();
$totalcount=0;

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$BMKQID=(isset($_POST['BMKQID']))?$_POST['BMKQID']:'';
$BMKGID=(isset($_POST['BMKGID']))?$_POST['BMKGID']:'';
$BMKDSBID=(isset($_POST['BMKDSBID']))?$_POST['BMKDSBID']:'';
$BMKTYPE=(isset($_POST['BMKTYPE']))?$_POST['BMKTYPE']:'';
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
//ダッシューボードが存在してるかどうかチェック
if($rtn === 0){
    $rs = cmCheckDB2DSB($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$BMKDSBID);
    if($rs !== true){
        $rtn = 2;
        $msg = showMsg($rs,array('ダッシューボードキー'));
    }
}
//GET DATA COUNT
if ($rtn === 0){
    $rs = fnGETDB2BMKCOUNT($db2con,$BMKQID,$BMKGID,$BMKDSBID,$BMKTYPE);
    if($rs['result'] !== true){
    	$rtn = 1;
    	$msg = showMsg($rs['result']);
    }else{
        $totalcount=$rs['TOTALCOUNT'];
    }
}

cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'TOTALCOUNT'=>$totalcount,
);
echo(json_encode($rtnArray));

/**
*
*ブックマークカウント
*/
function fnGETDB2BMKCOUNT($db2con,$BMKQID,$BMKGID,$BMKDSBID,$BMKTYPE){
	$totalcount=0;
    $rs=true;
    $data=array();
    $params=array();
	$strSQL  ='';
    $strSQL = 'SELECT COUNT(*) AS TOTALCOUNT FROM DB2BMK AS A ';
    $strSQL.= 'WHERE BMKDSBID= ? AND BMKQID=? AND BMKGID=? AND BMKTYPE=?';
	$params = array($BMKDSBID,$BMKQID,$BMKGID,$BMKTYPE);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt===false){
		$rs= 'FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
           $rs= 'FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $totalcount = $row['TOTALCOUNT'];
            }
        }
	}
    $data=array('result'=>$rs,'TOTALCOUNT'=>$totalcount);
	return $data;
}
