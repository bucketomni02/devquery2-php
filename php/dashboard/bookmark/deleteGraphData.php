<?php

include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rtn = 0;
$msg = '';
$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID']))?$_SESSION['PHPQUERY']['user'][0]['WUUID']:'';
        $BMKDSBID =  $_POST['BMKDSBID'];
        $BMKSEQ =  $_POST['BMKSEQ'];
        $BMKQID =  $_POST['BMKQID'];
        $BMKGID =  $_POST['BMKGID'];
        $rs =  fnDeleteDB2BMKD($db2con,$BMKDSBID,$BMKSEQ,$BMKQID,$BMKGID);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('グラフ'));
        }
    $rtn = array(
       'MSG' =>$msg,
        'RTN' => $rtn
    );
echo (json_encode($rtn));

function fnDeleteDB2BMKD($db2con,$BMKDSBID,$BMKSEQ,$BMKQID,$BMKGID){
    $rs = true;
    //構文
    $strSQL  = ' DELETE FROM DB2BMK WHERE ';
    $strSQL  .= ' BMKDSBID = ?  AND BMKSEQ = ? ';
    $strSQL  .= ' AND BMKQID = ?  AND BMKGID = ? ';
    $params = array($BMKDSBID,$BMKSEQ,$BMKQID,$BMKGID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}