<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$PROC  = (isset($_POST['PROC']))?$_POST['PROC']:'';
$NEWDASHNAME  = (isset($_POST['NEWDASHNAME']))?$_POST['NEWDASHNAME']:'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtnArray = array();        //リターン配列
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true && $rs['result'] !=='FAIL_USR_AUT'){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
//チェックのダッシュボード名
if($rtn===0){
    if($PROC==='ADD'){
        //必須チェックのダッシュボード名
        if($rtn === 0){
            $NEWDASHNAME = cmMer($NEWDASHNAME);
            if($NEWDASHNAME === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('ダッシュボード名'));
                $focus = 'NEWDASHNAME';
            }
        }
        //必須MAXLENGTHのダッシュボード名
        if($rtn === 0){
            if(!checkMaxLen($NEWDASHNAME,100)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('ダッシュボード名'));
                $focus = 'NEWDASHNAME';
            }
        }
        
    }
}

//save
if ($rtn === 0){
    if($PROC==='ADD'){
        $rs =fnINS_DB2DSB($db2con,$PROC,$NEWDASHNAME);
        if($rs!== true){
        	$rtn = 1;
        	$msg = showMsg($rs);
        }
    }
}
cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnArray));

/**
*INSERT DB2DSB
*/
function fnINS_DB2DSB($db2con,$PROC,$NEWDASHNAME){
    $USR_ID=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
    $rs=true;
    $params=array();
    if($PROC==='ADD'){
        $data=fnGETMAXID_DB2DSB($db2con);
        if($data['result']!==true){
            $rs=$data['result'];
        }else{
            $strSQL  ='';
            $strSQL  .= ' INSERT INTO DB2DSB ';
            $strSQL  .='(';
            $strSQL  .='DSBID,';
            $strSQL  .='DSBUID,';
            $strSQL  .='DSBNAME,';
            $strSQL  .='DSBMAIN';
            $strSQL  .=')';
            $strSQL .= ' VALUES ( ?, ?, ?, ?) ' ;
            $stmt = db2_prepare($db2con,$strSQL);
            $params=array(++$data['DSBID'],$USR_ID,$NEWDASHNAME,'0');
            if($stmt===false){
                $rs='FAIL_INS';
                e_log('error1:'.db2_stmt_errormsg());
            }else{
                $data = db2_execute($stmt,$params);
                if($data===false){
                    $rs='FAIL_INS';
                    e_log('error2:'.db2_stmt_errormsg($stmt));
                }
            }
 
        }
    }
    return $rs;
}
/**
*
*GET MAX DSBID
*/
function fnGETMAXID_DB2DSB($db2con){
    $data=array();
    $rs=true;
    $DSBID=0;
	$strSQL  ='';
    $strSQL  = ' SELECT CASE WHEN MAX(DSBID) IS NULL  THEN 0 ELSE MAX(DSBID) END AS DSBID';
    $strSQL .= ' FROM DB2DSB ';
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt1===false){
		 $rs='FAIL_SEL';
	}else{
		$r =db2_execute($stmt);
        if($r===false){
            $rs='FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $DSBID= $row['DSBID'];
            }            
        }
	}
    $data=array('result'=>$rs,'DSBID'=>$DSBID);
	return $data;
}

