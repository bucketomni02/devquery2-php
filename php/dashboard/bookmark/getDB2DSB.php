<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$DSBID  = (isset($_POST['DSBID']))?$_POST['DSBID']:'';
$ISDASHBOARD=(isset($_POST['ISDASHBOARD']))?$_POST['ISDASHBOARD']:'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtnArray = array();        //リターン配列
$rtn = 0;
$msg = '';
$data=array();
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true && $rs['result'] !=='FAIL_USR_AUT'){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

if ($rtn === 0){
    if($ISDASHBOARD ==='DASHBOARD'){//ダッシュボードへーだからクリック
        $r=fnGetMAIN_DB2DSB($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if($r['result']!==true){
            $rtn=1;
            $msg=showMsg($r['result']);
        }else{
            $data=$r['data'];
        }
    }else{
        //DB2DSBデータ
        $r=fnGet_DB2DSB($db2con,$DSBID);
        if($r['result']!==true){
            $rtn=1;
            $msg=showMsg($r['result']);
        }else{
            $data=$r['data'];
        }
    }
}
cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'data'=> $data,
    'licenseGraph' =>$licenseGraph
);
echo(json_encode($rtnArray));

/**
*DB2DSBデータ
*
*/
function fnGet_DB2DSB($db2con,$DSBID){
    $USR_ID=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
    $data=array();
    $rs=true;
	$strSQL  ='';
    $strSQL  = ' SELECT * FROM DB2DSB WHERE DSBUID=? AND DSBID = ?';
	$params = array($USR_ID,$DSBID);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt1===false){
		 $rs='FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs='FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $data[]= $row;
            }
        }
	}
	return array('result'=>$rs,'data'=>umEx($data));
}
/**
*ダッシュボードベーダからクリック
*ダッシュボードのマインのデータをもらう
*/
function fnGetMAIN_DB2DSB($db2con,$USR_ID){
    $data=array();
    $rs=true;
	$strSQL  ='';
    $strSQL  = ' SELECT * FROM DB2DSB WHERE DSBUID=? AND DSBMAIN=?';
	$params = array($USR_ID,'1');
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt1===false){
		 $rs='FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs='FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $data[]= $row;
            }
        }
	}
	return array('result'=>$rs,'data'=>umEx($data));
}

