<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtnArray = array();        //リターン配列
$rtn = 0;
$msg = '';
$data=array();
$BMKTOTAL=0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$BMKGID=(isset($_POST['BMKGID']))?$_POST['BMKGID']:'';;
$BMKQID=(isset($_POST['BMKQID']))?$_POST['BMKQID']:'';
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
if ($rtn === 0){
    //ユーザーがダッシュボードあるかどうかチェック
    $r=fnChkCOUNT_DB2DSB($db2con);
    if($r['result']!==true){
        $rtn=1;
        $msg=showMsg($r['result']);
    }
}
if ($rtn === 0){
    //DB2DSB削除とマイン
    $r=cmGETDB2BMKCOUNT($db2con,$BMKQID,$BMKGID,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($r['result']!==true){
        $rtn=1;
        $msg=showMsg($r['result']);
    }else{
        $BMKTOTAL=$r['TOTALCOUNT'];
    }
}
if ($rtn === 0){
    //DB2DSB削除とマイン
    $r=fnGet_DB2DSB($db2con);
    if($r['result']!==true){
        $rtn=1;
        $msg=showMsg($r['result']);
    }else{
        $data=$r['data'];
    }
}
cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'BMKTOTAL' => $BMKTOTAL,
    'aaData' => umEx($data,true),

);
echo(json_encode($rtnArray));

/**
*DB2DSBデータ
*
*/
function fnGet_DB2DSB($db2con){
    $USR_ID=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
    $data=array();
    $rs=true;
	$strSQL  ='';
    $strSQL  = ' SELECT * FROM DB2DSB WHERE DSBUID=?';
	$params = array($USR_ID);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt===false){
		 $rs='FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs='FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $data[]= $row;
            }
        }
	}
	return array('result'=>$rs,'data'=>$data);
}
function fnChkCOUNT_DB2DSB($db2con){
    $USR_ID=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
    $totalcount=0;
    $rs=true;
	$strSQL  ='';
    $strSQL  = ' SELECT COUNT (*) AS TOTALCOUNT FROM DB2DSB WHERE DSBUID=?';
	$params = array($USR_ID);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt1===false){
		 $rs='FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs='FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $totalcount= $row['TOTALCOUNT'];
            }
            if($totalcount<=0){
                $rs='ダッシュボードはこのユーザーでありませんので、最初にダッシュボードを登録してください。';
            }
        }
	}
	return array('result'=>$rs);
}

