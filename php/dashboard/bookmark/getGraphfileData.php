<?php
    include_once("../../common/inc/config.php");
    include_once("../../common/inc/common.inc.php");
    include_once("../../licenseInfo.php");
    $BMKDSBID     = (isset($_POST['BMKDSBID']) ? $_POST['BMKDSBID'] : '');
    $BMKQID     = (isset($_POST['BMKQID']) ? $_POST['BMKQID'] : '');
    $BMKGID     = (isset($_POST['BMKGID']) ? $_POST['BMKGID'] : '');
    $GPKTBL     = (isset($_POST['GPKTBL']) ? $_POST['GPKTBL'] : '');
    $BMKTYPE     = (isset($_POST['BMKTYPE']) ? $_POST['BMKTYPE'] : '');
    $rowstart     = (isset($_POST['ROWSTART']) ? $_POST['ROWSTART'] : 1);
    $rowend     = (isset($_POST['ROWEND']) ? $_POST['ROWEND'] : 10);
    $totalcount  = (isset($_POST['TOTALCOUNT']) ? $_POST['TOTALCOUNT'] : 0);
    $PAGINGINFO = array();
/*    $BMKQID =  $_POST['BMKQID'];
    $BMKGID =  $_POST['BMKGID'];
    $BMKTYPE =  $_POST['BMKTYPE'];
    $GPKTBL =  $_POST['GPKTBL'];*/
    //テーブルある無しフラグ
    $rs             = true;
    $rtn            = 0;
    $msg = '';
    /*
     *-------------------------------------------------------* 
     * 処理
     *-------------------------------------------------------*
     */
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $WUUID = $_SESSION['PHPQUERY']['user'][0]['WUUID'];

    //ログインユーザが削除されたかどうかチェック
    if ($rtn === 0) {
        $rs = cmCheckUser($db2con , $WUUID);
        if ($rs['result'] !== true) {
            $rtn = 2;
            $msg = showMsg($rs['result'], array(
                'ユーザー'
            ));
        }
    }
    //ダッシューボードが存在してるかどうかチェック
    if($rtn === 0){
        $rs = cmCheckDB2DSB($db2con,$WUUID,$BMKDSBID);
        if($rs !== true){
            $rtn = 2;
            $msg = showMsg($rs,array('ダッシューボード'));
        }  
    }
  //グラフ存在チェック
if($rtn === 0){
        $rs = cmCheckDB2GPK($db2con,$BMKQID,$BMKGID);
        if($rs['rs'] === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET',array('グラフ'));
        }else if($rs['rs'] !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs['rs'],array('グラフ'));
        }
}
    //ブックマックグラフ存在チェック
    if($rtn === 0){
            $rs = cmCheckDB2BMK($db2con,$BMKDSBID,$BMKQID,$BMKGID);
            if($rs['rs'] === true){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET',array('グラフ'));
            }else if($rs['rs'] !== 'ISEXIST'){
                $rtn = 1;
                $msg = showMsg($rs['rs'],array('グラフ'));
            }
    }
 if($rtn === 0){
    $GRAPH_CONTEXT = array();
    $GRAPHINFO = array();
        $graphFile = BASE_DIR . "/php/graphfile/".$BMKQID.'-'.$BMKGID.".txt";
         if (file_exists($graphFile)) {
                $graph_context = file_get_contents($graphFile);
                $GRAPHINFO = json_decode($graph_context,true);
        }
    if((int)$BMKTYPE === 1){//DATA TABLE
    $DATA_CONTEXT = array();
        if($GPKTBL !== ''){
                $fdb2csv1Data = FDB2CSV1_DATA($BMKQID);
                $RDBNM = cmMer($fdb2csv1Data['data'][0]['D1RDB']);
                $D1WEBF = cmMer($fdb2csv1Data['data'][0]['D1WEBF']);
                if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
                        $db2connet =  $db2con;
                }else{
                    $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
                    $db2RDBcon = cmDB2ConRDB(SAVE_DB);
                    $db2connet =  $db2RDBcon;
                }
                $dbname = $GPKTBL;
                $systables = cmGetSystables($db2connet, SAVE_DB, $dbname);
                if(count($systables) > 0){
                        $res = cmGetCountGphData($db2connet,$dbname,$BMKQID,$D1WEBF,'GET',$licenseCl);
                        $totalcount = 0;
                        if($res['result'] !== true){
                                $rtn = 1;
                                $msg = showMsg($res['result'],array('グラフ'));
                        }else{
                            $totalcount = $res['COUNT'];
                            if($totalcount <= 10){
                                    $rowend = $totalcount;
                            }
                            $PAGINGINFO['ROWSTART'] = (int)$rowstart;
                            $PAGINGINFO['ROWEND'] = (int)$rowend;
                            $PAGINGINFO['TOTALCOUNT'] = (int)$totalcount;
                        }
                        if($rtn === 0){
                                $rs = cmGetGphQueryData($db2connet,$dbname,$rowstart,$rowend,$BMKQID,$D1WEBF,'GET',$licenseCl);
                                if($rs['result'] !== true){
                                    $rtn = 1;
                                    $msg = showMsg($rs['result'],array('グラフ'));
                                 }else{
                                    /*$header = cmGetFDB2CSV2($db2con,$BMKQID,false,false,false);
                                    $header = umEx($header['data'],true);*/
                                    $data = umEx($rs['data']);
                                    $header =(isset( $GRAPHINFO['HEADER']) ?  $GRAPHINFO['HEADER'] : array());
                                    $DATA_CONTEXT = array($header,$data);
                                }
                        }
               }else{
                            $rtn = 1;
                            $msg = showMsg('FAIL_DATA',array('グラフ'));
                }//
        }
    }
 }
    if($rtn === 0){
        if((int)$BMKTYPE === 0){
            $GRAPH_CONTEXT = $GRAPHINFO;
        }else{
            $GRAPH_CONTEXT = $DATA_CONTEXT;
        }
    }
    $ERRMSG = array();
    if(count($GRAPHINFO) > 0 ){
        if(count($GRAPHINFO['ERRMSG']) !== 0){
            $rtn = 1;
            $msg = $GRAPHINFO['ERRMSG'][0];
        }
        $ERRMSG = $GRAPHINFO['ERRMSG'];
    }
    $rtn = array(
        'RTN' => $rtn,
        'MSG' => $msg,
        'systables' => count($systables),
        'GRAPH_CONTEXT' =>$GRAPH_CONTEXT,
        'data' => $data,
        'ERRMSG' => $ERRMSG,
        'GRAPHINFO' =>$GRAPHINFO,
        'DATA_CONTEXT' =>$DATA_CONTEXT,
        'PAGINGINFO' => $PAGINGINFO
    );
echo (json_encode($rtn));
