<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");

/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */


$DSBID       = $_POST['DSBID'];
$aaData = array();
$sortData  = array();
$allcount = 0;
$msg = '';
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
//テーブルある無しフラグ
$rs             = true;
$rtn            = 0;
$errorFlg = 0;
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

$WUUID = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
$currentDate = date('Y/m/d');
$currentTime = date('H:i');
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmCheckUser($db2con , $WUUID);
    if ($rs['result'] !== true) {
        $rtn =2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }
}
//ダッシューボードが存在してるかどうかチェック
if($rtn === 0){
    $rs = cmCheckDB2DSB($db2con,$WUUID,$DSBID);
    if($rs !== true){
        $rtn = 2;
        $msg = showMsg($rs,array('グラフキー'));
    }
}
if($rtn === 0){
    $rs = fnGetDB2DSB($db2con, $WUUID ,$DSBID, $licenseSql);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array(
            'グラフ'
        ));
    }else{
        $aaData = umEx($rs['data']);
        foreach($aaData as $key => $value){
            $graphFile = "../graphfile/".$value['BMKQID'].'-'.$value['BMKGID'].".txt";
             $GPKBFD = $value['GPKBFD'];
             $GPKBFT = $value['GPKBFT'];
             $GPKJKD = $value['GPKJKD'];
             $GPKJKT = $value['GPKJKT'];
            $KOSHINDATE = $GPKBFD;
            $KOSHINTIME = $GPKBFT;
             if((int)$GPKBFD === (int)$GPKJKD){
                    if((int)$GPKBFT < (int)$GPKJKT){
                        $KOSHINTIME = $GPKJKT;
                    }
             }else if((int)$GPKBFD < (int)$GPKJKD){
                    $KOSHINDATE = $GPKJKD;
                    $KOSHINTIME = $GPKJKT;
             }
             $jikoUpd = cmUpdTime($KOSHINDATE,$KOSHINTIME,$currentDate,$currentTime);
             $aaData[$key]['JIKOTIME'] = $jikoUpd;
             $aaData[$key]['GRAPH_CONTEXT'] = array();
             if (file_exists($graphFile)) {
//                    $graph_context = file_get_contents($graphFile);
//                    $graphInfo = json_decode($graph_context,true);
                    $aaData[$key]['GRAPH_FILESIZE'] =  filesize($graphFile);
                   $aaData[$key]['CONTAINER'] = $key;
            }else{
                   $aaData[$key]['GRAPH_FILESIZE'] =  0;
                   $aaData[$key]['CONTAINER'] = $key;
            }
               $keyId = $value['BMKQID'].'-'.$value['BMKGID'];
               $aaData[$key][$keyId] = $aaData[$key]['GRAPH_FILESIZE'];
        }
        $allcount = count($aaData);
        $sizeArr = array();
        $sortData = $aaData;
        foreach ($sortData as $key => $row){
            $k = $row['BMKQID'].'-'.$row['BMKGID'];
            $sizeArr[$key] = $row[$k];
        }
       array_multisort($sizeArr, SORT_ASC, $sortData);
    }
}

//$schData = $_SESSION;
unset($_SESSION['PHPQUERY']['schData']);
//$unsetSCH = $_SESSION;
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'totalCount' => $allcount,
    'aaData' => $aaData,
    'RTN' => $rtn,
    'MSG' => $msg,
    'DSBID'=>$DSBID,
    'BASE_URL' =>BASE_URL,
    'sortData' => $sortData,
    'ZERO_FLG' =>ZERO_FLG,
    'BACKZERO_FLG' => BACKZERO_FLG,
    'CURRENTDATE' => $currentDate,
    'CURRENTTIME' => $currentTime
);

echo (json_encode($rtn));

function fnGetDB2DSB($db2con,$DSBUID, $DSBID,$licenseSql){

    $data = array();

    $params = array();
    $strSQL  = ' SELECT D.D1TEXT,B.*,C.* FROM( ';
    $strSQL .= ' SELECT * FROM DB2DSB AS A  ';
    $strSQL .= ' LEFT JOIN DB2BMK AS B ON B.BMKDSBID = A.DSBID ';
    $strSQL .= ' WHERE A.DSBUID = ? AND A.DSBID = ? ' ;
    $strSQL .=' )AS B LEFT JOIN DB2GPK AS C ON B.BMKQID = C.GPKQRY AND B.BMKGID = C.GPKID ';
    $strSQL .=' LEFT JOIN FDB2CSV1 AS D ON B.BMKQID = D.D1NAME ';
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' WHERE D.D1CFLG = \'\' ';
    }
    $strSQL .=' ORDER BY BMKSEQ ';
    $params = array($DSBUID, $DSBID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}