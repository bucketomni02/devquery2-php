<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");
include_once("../../base/createExecuteSQL.php");
include_once("../../getQryCnd.php");
include_once("../../chkCondParam.php");
include_once("../../base/createTblExeSQL.php");
include_once("../../chkSQLParam.php");
include_once("../../getSQLCnd.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */

$BMKDSBID     = (isset($_POST['BMKDSBID']) ? $_POST['BMKDSBID'] : '');
$BMKQID     = (isset($_POST['BMKQID']) ? $_POST['BMKQID'] : '');
$BMKGID     = (isset($_POST['BMKGID']) ? $_POST['BMKGID'] : '');
$GPKTBL     = (isset($_POST['GPKTBL']) ? $_POST['GPKTBL'] : '');
$BMKTYPE     = (isset($_POST['BMKTYPE']) ? $_POST['BMKTYPE'] : '');
$PAGINGINFO = array();
$aaData = array();
$allcount = 0;
$msg = '';
$currentDate = date('Y/m/d');
$currentTime = date('H:i');
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
//テーブルある無しフラグ
$rs             = true;
$rtn            = 0;
$errorFlg = 0;
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

$WUUID = $_SESSION['PHPQUERY']['user'][0]['WUUID'];

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmCheckUser($db2con , $WUUID);
    if ($rs['result'] !== true) {
        $rtn =2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }
}
//ダッシューボードが存在してるかどうかチェック
if($rtn === 0){
    $rs = cmCheckDB2DSB($db2con,$WUUID,$BMKDSBID);
    if($rs !== true){
        $rtn = 2;
        $msg = showMsg($rs,array('ダッシューボード'));
    }  
}
  //グラフ存在チェック
if($rtn === 0){
        $rs = cmCheckDB2GPK($db2con,$BMKQID,$BMKGID);
        if($rs['rs'] === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET',array('グラフ'));
        }else if($rs['rs'] !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs['rs'],array('グラフ'));
        }
}
//ブックマックグラフ存在チェック
if($rtn === 0){
        $rs = cmCheckDB2BMK($db2con,$BMKDSBID,$BMKQID,$BMKGID);
        if($rs['rs'] === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET',array('グラフ'));
        }else if($rs['rs'] !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs['rs'],array('グラフ'));
        }
}
if($rtn === 0){
        $fdb2csv1Data = FDB2CSV1_DATA($BMKQID);
        $RDBNM = cmMer($fdb2csv1Data['data'][0]['D1RDB']);
        $D1WEBF = cmMer($fdb2csv1Data['data'][0]['D1WEBF']);
        $D1CFLG = cmMer($fdb2csv1Data['data'][0]['D1CFLG']);
        if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
                $db2connet =  $db2con;
        }else{
            $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
            $db2RDBcon = cmDB2ConRDB(SAVE_DB);
            $db2connet =  $db2RDBcon;
        }
            if($GPKTBL !== ''){
                $dbname = $GPKTBL;
                $systables = cmGetSystables($db2connet, SAVE_DB, $dbname);//ファイル存在チェック
                if(count($systables) > 0){
                    $rs = cmDropQueryTable($db2connet,$dbname);//あったら削除
                    if($rs['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rs['result']);
                     }
                }
            }else{
              do{
                    $dbname = makeRandStr(10);
                    $systables = cmGetSystables($db2connet, SAVE_DB, $dbname);
               }while(count($systables) > 0);
            }
        if($rtn === 0){
              $rest = cmCreateQryTable($db2con, $D1WEBF, $BMKQID ,$dbname,$licenseCl,$D1CFLG);//DEVQUERYHにファイル作成
                if($rest['RTN'] == 1){
                    $rtn = 1;
               //     $msg = $rest['MSG'];//エラーコードに変更する
                     $msg = showMsg('FAIL_SEL');
                    $CHARTDATA = array();
                    $CHARTDATA['CHARTINFO'] = array();
                    $CHARTDATA['ERRMSG'] = array($msg);
                    $str_json_format = json_encode($CHARTDATA);
                    $textFileName =  BASE_DIR . "/php/graphfile/".$BMKQID.'-'.$BMKGID.'.txt';
                    if (file_exists($textFileName)) {
                        unlink($textFileName);
                    }
                    $txt = $str_json_format;
                    $myfile = fopen($textFileName, "w") or die("Unable to open file!");
                    fwrite($myfile, $txt);
                    fclose($myfile);
                }
         }
        if($rtn === 0){
//            if((int)$BMKTYPE  === 0){
                $GPHINFO = cmCreateGraphInfoData($db2con,$db2connet,$BMKQID,$dbname,$BMKGID,$D1CFLG,$currentDate,$currentTime);//グラフJSONファイル作成
                 if($GPHINFO['RTN'] !== 0){
                        $rtn = 1;
                        $msg = $GPHINFO['MSG'];
                        $CHARTDATA = array();
                        $CHARTDATA['CHARTINFO'] = array();
                        $CHARTDATA['ERRMSG'] = array($msg);
                        $str_json_format = json_encode($CHARTDATA);
                        $textFileName =  BASE_DIR . "/php/graphfile/".$BMKQID.'-'.$BMKGID.'.txt';
                        if (file_exists($textFileName)) {
                            unlink($textFileName);
                        }
                        $txt = $str_json_format;
                        $myfile = fopen($textFileName, "w") or die("Unable to open file!");
                        fwrite($myfile, $txt);
                        fclose($myfile);
                 }else{
                        $GPHINFO = $GPHINFO['GPHINFO'];
                        $graphData = cmCreateGraphInfoFile($db2con,$GPHINFO);
                        if($aaData['result'] !== null){
                            $rtn == 1;
                             $msg = showMsg($aaData['result']);
                        }
                  }
//            }else{
                if($rtn === 0){
                    $res = cmGetCountGphData($db2connet,$dbname,$BMKQID,$D1WEBF,'REFRESH',$licenseCl);
                    $totalcount = 0;
                    if($res['result'] !== true){
                            $rtn = 1;
                            $msg = showMsg($res['result'],array('グラフ'));
                    }else{
                        $totalcount = $res['COUNT'];
                        $rowend = 10;
                        if($totalcount <= 10){
                                $rowend = $totalcount;
                        }
                        $PAGINGINFO['ROWSTART'] = 1;
                        $PAGINGINFO['ROWEND'] = $rowend;
                        $PAGINGINFO['TOTALCOUNT'] = (int)$totalcount;
                    }
                   if($rtn === 0){
                        $rs = cmGetGphQueryData($db2connet,$dbname,1,$rowend,$BMKQID,$D1WEBF,'REFRESH',$licenseCl);//ファイル取得
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = showMsg($rs['result']);
                         }else{
                          /*  $header = cmGetFDB2CSV2($db2con,$BMKQID,false,false,false);
                            $header = umEx($header['data'],true);*/
                            //$header = $graphData['HEADER'];
                            $header =(isset( $graphData['HEADER']) ?  $graphData['HEADER'] : array());
                            $data = umEx($rs['data']);
                            $tableData = array($header,$data);
                        }
                    }
             }
        }
}
            if((int)$BMKTYPE  === 0){
                $aaData = $graphData;
            }else{
                $aaData = $tableData;
            }
             $JIKOTIME = showMsg("UPDJIKO", array("0 分"));
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,  
    'MSG' => $msg,
    'rs' => $rs,
    'D1WEBF' =>$D1WEBF,
     'BMKQID' =>$BMKQID,
    'dbname' =>$dbname,
    'GPHINFO' => $aaData,
    'JIKOTIME' =>$JIKOTIME,
    'PAGINGINFO' => $PAGINGINFO
);

echo (json_encode($rtn));

function fnGetDB2DSB($db2con,$DSBUID, $DSBID){

    $data = array();

    $params = array();
    $strSQL  = ' SELECT D.D1TEXT,B.*,C.* FROM( ';
    $strSQL .= ' SELECT * FROM DB2DSB AS A  ';
    $strSQL .= ' LEFT JOIN DB2BMK AS B ON B.BMKDSBID = A.DSBID ';
    $strSQL .= ' WHERE A.DSBUID = ? AND A.DSBID = ? ' ;
    $strSQL .=' )AS B LEFT JOIN DB2GPK AS C ON B.BMKQID = C.GPKQRY AND B.BMKGID = C.GPKID ';
    $strSQL .=' LEFT JOIN FDB2CSV1 AS D ON B.BMKQID = D.D1NAME ORDER BY BMKSEQ ';
    $params = array($DSBUID, $DSBID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
function updateDB2GPK($db2con,$GPKQRY,$GPKID,$CURRENTDATE,$CURRENTTIME){
    $rtn = array();
    //構文
    $strSQL = ' UPDATE DB2GPK ';
    $strSQL .= ' SET ';
    $strSQL .= ' GPKJKD = ? ';
    $strSQL .= ', GPKJKT = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' GPKQRY = ? ';
    $strSQL .= ' AND GPKID = ? ';
    $params = array(
        $CURRENTDATE,
        $CURRENTTIME,
        $GPKQRY,
        $GPKID
    );
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            $rtn = array(
                'result' => true
            );
        }
    }
    return $rtn;
}