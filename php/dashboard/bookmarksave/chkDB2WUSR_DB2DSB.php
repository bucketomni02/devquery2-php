<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */


$DSBID=(isset($_POST['DSBID']))?$_POST['DSBID']:'';

/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$allcount = 0;
$data     = array();
$rtn      = 0;
$msg      = '';
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);


//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true && $rs['result'] !=='FAIL_USR_AUT') {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }
}
//ダッシューボードが存在してるかどうかチェック
if($rtn === 0){
    if($DSBID !==''){
        $rs = cmCheckDB2DSB($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$DSBID);
        if($rs !== true){
            $rtn = 2;
            $msg = showMsg($rs,array('ダッシューボードキー'));
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo (json_encode($rtn));
