<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$UPDGRIDDATA = json_decode($_POST['UPDGRIDDATA'],true);
$PROC  = (isset($_POST['PROC']))?$_POST['PROC']:'';
$FLG=(isset($_POST['FLG']))?$_POST['FLG']:'';
$BMKTOTAL=(isset($_POST['BMKTOTAL']))?$_POST['BMKTOTAL']:'';
$BMKQID=(isset($_POST['BMKQID']))?$_POST['BMKQID']:'';
$BMKGID=(isset($_POST['BMKGID']))?$_POST['BMKGID']:'';
$BMKSEQ=(isset($_POST['BMKSEQ']))?$_POST['BMKSEQ']:'';
$BMKDSBID=(isset($_POST['BMKDSBID']))?$_POST['BMKDSBID']:'';
$BMKSIZ=(isset($_POST['BMKSIZ']))?$_POST['BMKSIZ']:'';
$BMKTYPE=(isset($_POST['BMKTYPE']))?$_POST['BMKTYPE']:'';
$DSBNAME=(isset($_POST['DSBNAME']))?$_POST['DSBNAME']:'';
$GPKNM=(isset($_POST['GPKNM']))?$_POST['GPKNM']:'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
//リターン配列
$rtnArray = array();
$rtn = 0;
$msg = '';
$gkeyDCount=0;
$dtotalcount=0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($FLG==='1'){//実行からユーザーチェック
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }
    }else{
        if($rs['result'] !== true && $rs['result'] !=='FAIL_USR_AUT'){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }
    }
}
//ダッシューボードが存在してるかどうかチェック
if($rtn === 0){
    $rs = cmCheckDB2DSB($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$BMKDSBID);
    if($rs !== true){
        $rtn = 2;
        $msg = showMsg($rs,array('ダッシューボードキー'));
    }
}
//お気にいり一覧から登録
if($rtn===0){
    if($FLG==='0'){
        if($PROC==='ADD'){
             //必須チェックのダッシュボード名
            if($rtn === 0){
                $DSBNAME = cmMer($DSBNAME);
                if($DSBNAME === ''){
                    $rtn = 1;
                    $msg = showMsg('FAIL_REQ',array('ダッシュボード名'));
                    $focus = 'DSBNAME';
                }
            }
            //必須MAXLENGTHのダッシュボード名
            if($rtn === 0){
                if(!checkMaxLen($DSBNAME,100)){
                    $rtn = 1;
                    $msg = showMsg('FAIL_MAXLEN',array('ダッシュボード名'));
                    $focus = 'DSBNAME';
                }
            }
            //チェックDUPLICATE $duprow=array();
            $QID_GID_TYPE=array();
            if($rtn === 0){
                foreach($UPDGRIDDATA as $key => $col){
                     //グラフ名
                    if($rtn === 0){
                        if($col['BMKGID']==='' || $col['BMKQID']===''){
                            $rtn = 1;
                            $msg = showMsg('FAIL_REQ',array('グラフ'));
                            break;
                        }
                    }
                    //表示タイプ
                    if($rtn === 0){
                        if($col['BMKTYPE'] === ''){
                            $rtn = 1;
                            $msg = showMsg('FAIL_REQ',array('表示タイプ'));
                            break;
                        }
                    }
                    if($rtn===0){
                        $row=$col['BMKQID'].$col['BMKGID'].$col['BMKTYPE'];
                        if(in_array($row,$QID_GID_TYPE)){
                        	$rtn = 1;
                             $msg = showMsg('CHK_DUP',array(array('クエリー','グラフ','表示タイプ')));
                        }else{
                            //サイズ
                            if($rtn === 0){
                                if($col['BMKSIZ']=== ''){
                                    $rtn = 1;
                                    $msg = showMsg('FAIL_REQ',array('サイズ'));
                                    break;
                                }
                            }
                            if($rtn === 0){
                            	$row=$col['BMKQID'].$col['BMKGID'].$col['BMKTYPE'];
                            	array_push($QID_GID_TYPE,$row);
                            }
                        }
                    }
                }
            }
        }
    }
}
//グラフサイズから登録
if($rtn===0){
    if($FLG==='2'){
        if($PROC==='ADD'){
             //グラフ名
            if($rtn === 0){
                if($GPKNM === '' || $BMKGID==='' || $BMKQID===''){
                    $rtn = 1;
                    $msg = showMsg('FAIL_REQ',array('グラフ'));
                    $focus = 'GPKNM';
                }
            }
            //サイズ
            if($rtn === 0){
                if($BMKSIZ === ''){
                    $rtn = 1;
                    $msg = showMsg('FAIL_REQ',array('サイズ'));
                    $focus = 'BMKSIZ';
                }
            }
            //表示タイプ
            if($rtn === 0){
                if($BMKTYPE === ''){
                    $rtn = 1;
                    $msg = showMsg('FAIL_REQ',array('表示タイプ'));
                    $focus = 'BMKTYPE';
                }
            }
        }
    }
}
//グラフ実行から登録
if($rtn===0){
    if( $FLG==='1'){
        if($PROC==='ADD'){
             //グラフ名
            if($rtn === 0){
                if($BMKGID==='' || $BMKQID===''){
                    $rtn = 1;
                    $msg = showMsg('FAIL_REQ',array('グラフ'));
                }
            }
            //サイズ
            if($rtn === 0){
                if($BMKSIZ === ''){
                    $rtn = 1;
                    $msg = showMsg('FAIL_REQ',array('サイズ'));
                }
            }
            //表示タイプ
            if($rtn === 0){
                if($BMKTYPE === ''){
                    $rtn = 1;
                    $msg = showMsg('FAIL_REQ',array('表示タイプ'));
                }
            }
        }
    }
}

if($rtn === 0){
    $rs = fnGETDB2BMKCOUNT($db2con,$BMKDSBID,$BMKQID,$BMKGID,$BMKTYPE,$BMKSEQ,$PROC);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $BMKTOTAL=$rs['TOTALCOUNT'];
    }
}
if($rtn===0){
    if($PROC === 'UPD'){//グラフ更新
        if($rtn === 0){
            if((int)$BMKTOTAL > 0){
          
                $m = '';
                $m = '';
                $m = '';
                $m = '';      $rtn = 1;
                $msg = showMsg('CHK_DUP',array(array('クエリー','グラフ','表示タイプ')));
             }
         }
         if($rtn === 0){
            $r=fnUpd_DB2BMK($db2con,$BMKQID,$BMKGID,$BMKDSBID,$BMKSIZ,$BMKTYPE,$BMKSEQ,$PROC);
            if($r!==true){
                $rtn = 1;
                $msg = showMsg($r);
            }
         }
    }else{
        //save
        if ($rtn === 0){
            if($FLG==='0'){//お気にいり一覧から登録
                if($PROC==='ADD'){
                    $rs=fnUpd_DB2DSB($db2con,$DSBNAME,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$BMKDSBID);
                    if($rs!==true){
                        $rtn=1;
                        $msg=showMsg($rs);
                    }else{
                        if($rtn===0){
                            if(count($UPDGRIDDATA)>20){
                            	$rtn = 1;
                            	$msg = showMsg('FAIL_DMAXLEN');
                            }else{
                                $rs =fnDB2BMK($db2con,$UPDGRIDDATA,$PROC,$BMKDSBID);
                                if($rs !== true){
                                	$rtn = 1;
                                	$msg = showMsg($rs);
                                }
                            }
                        }
                    }
                }
            }else if($FLG==='1'){//実行から登録
                if($PROC==='ADD'){
                    //新しいグラフだけDB2BMKで登録する
                    if($BMKTOTAL<=0){
                        $rs=fnJIKO_DB2BMK($db2con,$BMKQID,$BMKGID,$BMKDSBID,$BMKSIZ,$BMKTYPE);
                        if($rs!==true){
                            $rtn = 1;
                            $msg = showMsg($rs);
                        }
                    }
                }else if($PROC==='DEL'){
                    $rs=fnDEL_JIKO_DB2BMK($db2con,$BMKQID,$BMKGID,$BMKDSBID,$BMKTYPE);
                    if($rs!=true){
                        $rtn = 1;
                        $msg = showMsg($rs);
                    }
                }
                //グラフが一つのダッシュボードであるかどうかのカウント
                $rs = cmGETDB2BMKCOUNT($db2con,$BMKQID,$BMKGID,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg =  showMsg($rs['result']);
                }else{
                    $gkeyDCount=$rs['TOTALCOUNT'];
                }                
            }else if($FLG==='2'){//グラフサイズから登録
                if($PROC==='ADD'){
                    if($BMKTOTAL>0){
                        $r=fnUpd_DB2BMK($db2con,$BMKQID,$BMKGID,$BMKDSBID,$BMKSIZ,$BMKTYPE,$BMKSEQ,$PROC);
                    }else{
                        $r=fnJIKO_DB2BMK($db2con,$BMKQID,$BMKGID,$BMKDSBID,$BMKSIZ,$BMKTYPE);
                    }
                    if($r!==true){
                        $rtn = 1;
                        $msg = showMsg($r);
                    }
                }
            }
        }
    }
}
cmDb2Close($db2con);

$m = array($BMKQID,$BMKGID,$BMKDSBID,$BMKSIZ,$BMKTYPE);
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'TEST' =>$m,
    'PROC' =>$PROC,
    'GKEYDCOUNT'=>$gkeyDCount,
    'BMKTYPE'=>$BMKTYPE
);
echo(json_encode($rtnArray));
/**
*
*ブックマーク
*/
function fnDB2BMK($db2con,$UPDGRIDDATA,$PROC,$BMKDSBID){
    $rs=true;
    $data=array();
    $dtotalcount=0;
    $data=fnDelDB2BMK($db2con,$BMKDSBID);
    if($data !== true){
    	$rs=$data;
    }else{
    	$strSQL  ='';
        $strSQL  .= ' INSERT INTO DB2BMK ';
        $strSQL  .='(';
        $strSQL  .='BMKDSBID,';
        $strSQL  .='BMKSEQ,';
        $strSQL  .='BMKQID,';
        $strSQL  .='BMKGID,';
        $strSQL  .='BMKTYPE,';
        $strSQL  .='BMKSIZ';
        $strSQL  .=')';
        $strSQL .= ' VALUES ( ?, ?, ?, ?, ?, ?) ' ;
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
        	$rs= 'FAIL_INS';
        }else{
        	$seqnumber = 0;
            if(count($UPDGRIDDATA)>0){
                foreach($UPDGRIDDATA as $col){
                    $BMKQID=cmHscDe($col['BMKQID']);
                    $BMKGID=cmHscDe($col['BMKGID']);
                    //クエリーとグラフはユーザーであるかどうかチェック 
                    $isExistTot=fnGETDB2BMKCOUNTGROUP($db2con,$BMKQID,$BMKGID);
                    if($isExistTot['result'] !==true){
                        $rs=$isExistTot['result'];
            			break;
                    }else{
                        if($isExistTot['TOTALCOUNT']>0){
                            $params= array(
                                $BMKDSBID,
                    			++$seqnumber,
                                $BMKQID,
                                $BMKGID,
                                $col['BMKTYPE'],
                                $col['BMKSIZ']
                    		);
                    		$data = db2_execute($stmt,$params);
                    		if($data === false){
                    			$rs= 'FAIL_INS';
                    			break;
                    		}
                        }
                    }
            	}
            } 
    	}        
    }
	return $rs;
}
function fnDelDB2BMK($db2con,$BMKDSBID){
    $rs=true;
	$strSQL  ='';
    $strSQL = ' DELETE FROM DB2BMK WHERE BMKDSBID = ? ';
	$params = array($BMKDSBID);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt===false){
		 $rs= 'FAIL_DEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs='FAIL_DEL';
        }
	}
	return $rs;
}
/**
*
*実行のためブックマーク登録
*/
function fnJIKO_DB2BMK($db2con,$QRY,$GID,$BMKDSBID,$BMKSIZ,$BMKTYPE){
    $params=array();
    $rs=true;
    $BMKSEQ=0;
    $strSQL  ='';
    $dtotalcount=0;
    //ダッシュボード一つのため、グラフは20個以上ならエラーチェック
    $r = cmCountDB2BMK($db2con,$BMKDSBID);
    if($r['result'] !== true){
        $rs=$r['result'];
    }else{
        $dtotalcount=$r['TOTALCOUNT']+1;
        if($dtotalcount>20){
            $rs='FAIL_DMAXLEN';
        }else{
            $r=fnGETMAX_SEQ_DB2BMK($db2con,$BMKDSBID);
        	if($r['result']!==true){
        		 $rs=$r['result'];
        	}else{
                $BMKSEQ= $r['BMKSEQ'];
                $strSQL  ='';
                $strSQL  .= ' INSERT INTO DB2BMK ';
                $strSQL  .='(';
                $strSQL  .='BMKDSBID,';
                $strSQL  .='BMKSEQ,';
                $strSQL  .='BMKQID,';
                $strSQL  .='BMKGID,';
                $strSQL  .='BMKTYPE,';
                $strSQL  .='BMKSIZ';
                $strSQL  .=')';
                $strSQL .= ' VALUES ( ?, ?, ?, ?, ?, ?) ' ;
                $stmt2 = db2_prepare($db2con,$strSQL);
                if($stmt2 === false){
                	$rs= 'FAIL_INS';
                }else{
            		$params= array($BMKDSBID,++$BMKSEQ,$QRY,$GID,$BMKTYPE,$BMKSIZ);
            		$r = db2_execute($stmt2,$params);
            		if($r === false){
            			$rs='FAIL_INS';
            		}
                }
            }
        }
    }
	return $rs;
}
/**
*
*更新のためブックマーク登録
*/
function fnUpd_DB2BMK($db2con,$BMKQID,$BMKGID,$BMKDSBID,$BMKSIZ,$BMKTYPE,$BMKSEQ,$PROC){
    $params=array();
    $rs=true;
  //  $BMKSEQ=0;
	$strSQL  ='';
    $strSQL  = ' UPDATE DB2BMK SET BMKSIZ = ?,BMKTYPE = ? WHERE BMKDSBID = ? AND BMKQID = ? AND BMKGID = ? ';
	$params= array($BMKSIZ,$BMKTYPE,$BMKDSBID,$BMKQID,$BMKGID);
    if($PROC === 'UPD'){
        $strSQL .= ' AND BMKSEQ = ? ';
        array_push($params,$BMKSEQ);
    }else{
        $strSQL .= ' AND BMKTYPE = ? ';
        array_push($params,$BMKTYPE);
     }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
    	$rs= 'FAIL_UPD';
    }else{
		$r = db2_execute($stmt,$params);
		if($r === false){
			$rs='FAIL_UPD';
		}
    }
	return $rs;
}
/**
*
*更新のためブックマーク登録
*/
function fnUpd_DB2DSB($db2con,$DSBNAME,$USR_ID,$BMKDSBID){
    $params=array();
    $rs=true;
    $BMKSEQ=0;
	$strSQL  ='';
    $strSQL  = ' UPDATE DB2DSB SET DSBNAME=? WHERE DSBUID=? AND DSBID=? ';
	$params= array($DSBNAME,$USR_ID,$BMKDSBID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
    	$rs= 'FAIL_UPD';
    }else{
		$r = db2_execute($stmt,$params);
		if($r === false){
			$rs='FAIL_UPD';
		}
    }
	return $rs;
}
/**
*一番高いSEQをもらうため
*/
function fnGETMAX_SEQ_DB2BMK($db2con,$BMKDSBID){
    $BMKSEQ=0;
    $rs=true;
	$strSQL  ='';
    $strSQL  = ' SELECT CASE WHEN MAX(BMKSEQ) IS NULL  THEN 0 ELSE MAX(BMKSEQ) END AS BMKSEQ';
    $strSQL .= ' FROM DB2BMK WHERE BMKDSBID = ? ';
	$params = array($BMKDSBID);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt1===false){
		 $rs='FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs='FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $BMKSEQ= $row['BMKSEQ'];
            }
        }
    }
    return array('result'=>$rs,'BMKSEQ'=>$BMKSEQ);

}
/**
*
*実行のためブックマーク削除
*/
function fnDEL_JIKO_DB2BMK($db2con,$QRY,$GID,$BMKDSBID,$BMKTYPE){
	$data=array();
    $rs=true;
	$strSQL  ='';
    $strSQL = ' DELETE FROM DB2BMK WHERE BMKDSBID = ? AND BMKQID=? AND BMKGID=? AND BMKTYPE=? ';
	$params = array($BMKDSBID,$QRY,$GID,$BMKTYPE);
	$stmt1 = db2_prepare($db2con, $strSQL);
	if($stmt1===false){
         $rs='FAIL_DEL';
	}else{
		$r =db2_execute($stmt1,$params);
        if($r===false){
            $rs='FAIL_DEL';
        }
	}
	return $rs;
}
/**
*
*クエリーとグラフがユーザーであるかどうかのカウント
*/
function fnGETDB2BMKCOUNTGROUP($db2con,$QID,$GID){
    $usrid=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
	$totalcount=0;
    $data=array();
    $params=array();
    $rs=true;
	$strSQL  ='';
    $strSQL = 'SELECT COUNT(*) AS TOTALCOUNT FROM DB2GPK AS C ';
    $strSQL.= 'INNER JOIN ( ';
    $strSQL.= 'SELECT DISTINCT(WGNAME) FROM DB2WUGR AS A ';
    $strSQL.= 'LEFT JOIN DB2WGDF AS B ON A.WUGID=B.WGGID ';
    $strSQL.= 'WHERE WUUID=? ) AS D ';
    $strSQL.= 'ON C.GPKQRY=D.WGNAME ';
    $strSQL.= 'WHERE C.GPKQRY=? AND C.GPKID=? ';
	$params = array($usrid,$QID,$GID);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt===false){
		 $rs= 'FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs= 'FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $totalcount = $row['TOTALCOUNT'];
            }
        }
	}
        
    $data=array('result'=>$rs,'TOTALCOUNT'=>$totalcount);
	return $data;
}
function fnGETDB2BMKCOUNT($db2con,$BMKDSBID,$BMKQID,$BMKGID,$BMKTYPE,$BMKSEQ,$PROC){
	$totalcount=0;
    $data=array();
    $params=array();
    $rs=true;
    $strSQL  ='';
    $strSQL = 'SELECT COUNT(*) AS TOTALCOUNT FROM DB2BMK AS A ';
    $strSQL .= 'WHERE BMKDSBID= ? AND BMKQID=? AND BMKGID=? AND BMKTYPE= ? ';
	$params = array($BMKDSBID,$BMKQID,$BMKGID,$BMKTYPE);
     if($PROC === 'UPD'){
        $strSQL .= 'AND BMKSEQ <> ? ';
        array_push($params,$BMKSEQ);
     }
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt===false){
		 $rs= 'FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs= 'FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $totalcount = $row['TOTALCOUNT'];
            }
        }
	}
	return array('result'=>$rs,'TOTALCOUNT'=>$totalcount);
}
