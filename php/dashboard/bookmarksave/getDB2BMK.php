<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");
//リターン配列
$rtnArray = array();
$rtn = 0;
$msg = '';
$data=array();
$totalcount=0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$BMKDSBID=(isset($_POST['BMKDSBID']))?$_POST['BMKDSBID']:'';
$FLG=(isset($_POST['FLG']))?$_POST['FLG']:'';
$BMKQID=(isset($_POST['BMKQID']))?$_POST['BMKQID']:'';
$BMKGID=(isset($_POST['BMKGID']))?$_POST['BMKGID']:'';
$BMKSEQ=(isset($_POST['BMKSEQ']))?$_POST['BMKSEQ']:'';

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true && $rs['result'] !=='FAIL_USR_AUT'){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
//ダッシューボードが存在してるかどうかチェック
if($rtn === 0){
    $rs = cmCheckDB2DSB($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$BMKDSBID);
    if($rs !== true){
        $rtn = 2;
        $msg = showMsg($rs,array('ダッシューボードキー'));
    }
}
//GET DATA COUNT
if ($rtn === 0){
    $rs = fnGETDB2BMKCOUNT($db2con,$BMKDSBID,$licenseSql);
    if($rs['result'] !== true){
    	$rtn = 1;
    	$msg = showMsg($rs['result']);
    }else{
        $totalcount=$rs['TOTALCOUNT'];
    }
}


//ブックマークグラフの編集データ取得
if ($rtn === 0){
    if($FLG==='2'){//グラフサイズのため
        $rs=fnGETDB2BMKForParam($db2con,$BMKDSBID,$BMKQID,$BMKGID,$BMKSEQ,$licenseSql);
        if($rs['result']!==true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data=umEx($rs['data']);
        }
    }else{
        $rs = fnGETDB2BMK($db2con,$BMKDSBID,$licenseSql);
        if($rs['result'] !== true){
        	$rtn = 1;
        	$msg = showMsg($rs['result']);
        }else{
            $data=umEx($rs['data'],true);
        }
    }
}
cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData'=>$data,
    'iTotalRecords'=>$totalcount
);
echo(json_encode($rtnArray));
/**
*
*ブックマーク
*/
function fnGETDB2BMK($db2con,$BMKDSBID,$licenseSql){
	$data=array();
    $rs=true;
	$strSQL  ='';
    $strSQL ='SELECT  B.BMKQID,B.BMKGID,C.GPKNM,B.BMKTYPE,B.BMKSIZ FROM DB2DSB AS A ';
    $strSQL.='INNER JOIN DB2BMK AS B ON A.DSBID=B.BMKDSBID ';
    $strSQL.='INNER JOIN DB2GPK AS C ON B.BMKQID=C.GPKQRY AND B.BMKGID=C.GPKID ';

    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL.='INNER JOIN FDB2CSV1 AS F ON B.BMKQID = F.D1NAME ';
    }
    $strSQL.='WHERE B.BMKDSBID=? ';
    if(!$licenseSql){
        $strSQL .= ' AND F.D1CFLG = \'\' ';
    }

    $strSQL.='ORDER BY B.BMKDSBID,B.BMKSEQ ';
	$params = array($BMKDSBID);
    e_log('usr=>'.$strSQL.' array=>'.print_r($params,true));
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt===false){
		 $rs= 'FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs= 'FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
	}
    $data=array('result'=>$rs,'data'=>$data);
	return $data;
}
/**
*
*ブックマーク
*/
function fnGETDB2BMKForParam($db2con,$BMKDSBID,$BMKQID,$BMKGID,$BMKSEQ,$licenseSql){
	$data=array();
    $rs=true;
    $strSQL  ='';
    $strSQL  ='SELECT  * FROM DB2BMK AS A ';
    $strSQL .=' LEFT JOIN DB2GPK AS B ';
    $strSQL .=' ON A.BMKQID = B.GPKQRY AND A.BMKGID = B.GPKID ';
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL.='INNER JOIN FDB2CSV1 AS F ON B.BMKQID = F.D1NAME ';
    }
    $strSQL .=' WHERE A.BMKDSBID=? AND A.BMKQID=? AND A.BMKGID=? AND A.BMKSEQ = ? ' ;
    if(!$licenseSql){
        $strSQL .= ' AND F.D1CFLG = \'\' ';
    }

	$params = array($BMKDSBID,$BMKQID,$BMKGID,$BMKSEQ);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt===false){
		 $rs= 'FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs= 'FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
	}
    $data=array('result'=>$rs,'data'=>$data);
	return $data;
}
/**
*
*ブックマークカウント
*/
function fnGETDB2BMKCOUNT($db2con,$BMKDSBID,$licenseSql){
	$totalcount=0;
    $data=array();
    $rs=true;
    $params=array();
	$strSQL ='';
    $strSQL ='SELECT COUNT(*) AS TOTALCOUNT FROM DB2DSB AS A ';
    $strSQL.='INNER JOIN DB2BMK AS B ON A.DSBID=B.BMKDSBID ';
    $strSQL.='INNER JOIN DB2GPK AS C ON B.BMKQID=C.GPKQRY AND B.BMKGID=C.GPKID ';
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL.='INNER JOIN FDB2CSV1 AS F ON B.BMKQID = F.D1NAME ';
    }
    $strSQL.='     WHERE B.BMKDSBID=? ';
    if(!$licenseSql){
        $strSQL .= ' AND F.D1CFLG = \'\' ';
    }
	$params = array($BMKDSBID);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt===false){
		 $rs= 'FAIL_SEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs= 'FAIL_SEL';
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $totalcount = $row['TOTALCOUNT'];
            }
        }
	}
    $data=array('result'=>$rs,'TOTALCOUNT'=>$totalcount);
	return $data;
}
