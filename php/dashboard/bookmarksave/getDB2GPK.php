<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
include_once("../../licenseInfo.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */

$start      = $_POST['start'];
$length     = $_POST['length'];
$sort       = $_POST['sort'];
$sortDir    = $_POST['sortDir'];
$DSBID=(isset($_POST['DSBID']))?$_POST['DSBID']:'';

/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$allcount = 0;
$data     = array();
$rtn      = 0;
$msg      = '';
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);


//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true && $rs['result'] !=='FAIL_USR_AUT') {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }
}
//ダッシューボードが存在してるかどうかチェック
if($rtn === 0){
    if($DSBID !==''){
        $rs = cmCheckDB2DSB($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$DSBID);
        if($rs !== true){
            $rtn = 2;
            $msg = showMsg($rs,array('ダッシューボードキー'));
        }
    }
}
if($rtn === 0){
    $rs = fnGetGrapAllDataCount($db2con,$licenseSql);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg('FAIL_SEL');
    } else {
        $allcount = $rs['TOTALCOUNT'];
    }
}
if ($rtn === 0) {
    $rs = fnGetGrapAllData($db2con,$start,$length,$licenseSql);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg('FAIL_SEL');
    } else {
        $data = $rs['data'];
    }
    
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo (json_encode($rtn));

function fnGetGrapAllData($db2con,$start,$length,$licenseSql){
    $data   = array();
    $rs=true;
    $params = array();
    $params = array($_SESSION['PHPQUERY']['user'][0]['WUUID'],$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    $strSQL = '';
    $strSQL.= 'SELECT E.* FROM (';
    $strSQL.= 'SELECT C.GPKQRY AS BMKQID,C.GPKID AS BMKGID,C.GPKNM,\'0\' AS BMKSIZ,\'0\' AS BMKTYPE, ROWNUMBER() OVER(ORDER BY C.GPKQRY ) as rownum  ';
    $strSQL.= 'FROM DB2GPK AS C INNER JOIN ( ';
    $strSQL.= 'SELECT DISTINCT(WGNAME) FROM DB2WUGR AS A ';
    $strSQL.= 'LEFT JOIN DB2WGDF AS B ON A.WUGID=B.WGGID ';
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL.= 'LEFT JOIN FDB2CSV1 AS C ';
        $strSQL.= 'ON C.D1NAME = B.WGNAME ';
    }
    $strSQL.= ' WHERE WUUID=? ';
    if(!$licenseSql){
        $strSQL.= ' AND C.D1CFLG = \'\' ';
    }

    //ユーザーが作成したクエリーのグラフも取得（イー）
    $strSQL.= '     UNION';                    
    $strSQL.= '     SELECT DISTINCT(D1NAME) AS WGNAME FROM  FDB2CSV1  A';  
    $strSQL.= '         LEFT OUTER JOIN DB2QHIS B on A.D1NAME = B.DQNAME';               
    $strSQL.= '         WHERE B.DQCUSR = ? ';
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND A.D1CFLG = \'\' ';
    }
    $strSQL.= '     ) AS D '; 
    $strSQL.= 'ON C.GPKQRY=D.WGNAME) AS E';
    // 抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE E.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    e_log("fnGetGrapAllData".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs=false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs=false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return array('result'=>$rs,'data'=>$data);
}
function fnGetGrapAllDataCount($db2con,$licenseSql){
    $totalcount=0;
    $data   = array();
    $params = array();
    $strSQL = '';
    $strSQL = 'SELECT COUNT(*) AS TOTALCOUNT FROM DB2GPK AS C ';
    $strSQL.= 'INNER JOIN ( ';
    $strSQL.= 'SELECT DISTINCT(WGNAME) FROM DB2WUGR AS A ';
    $strSQL.= 'LEFT JOIN DB2WGDF AS B ON A.WUGID=B.WGGID ';
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL.= 'LEFT JOIN  PHPQUERY2.FDB2CSV1 AS C ';
        $strSQL.= 'ON C.D1NAME = B.WGNAME ';
    }
    $strSQL.= '  WHERE WUUID=? ';
    if(!$licenseSql){
        $strSQL.= ' AND C.D1CFLG = \'\' ';
    }
    //ユーザーが作成したクエリーのグラフも取得（イー）
    $strSQL.= '         UNION';                    
    $strSQL.= '         SELECT DISTINCT(D1NAME) AS WGNAME FROM  FDB2CSV1  A';  
    $strSQL.= '             LEFT OUTER JOIN DB2QHIS B on A.D1NAME = B.DQNAME';               
    $strSQL.= '             WHERE B.DQCUSR = ? ';
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND A.D1CFLG = \'\' ';
    }
    $strSQL.= '     ) AS D ';
    $strSQL.= '     ON C.GPKQRY=D.WGNAME';   

    $params = array($_SESSION['PHPQUERY']['user'][0]['WUUID'],$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    //error_log('graph  sql = '.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => false
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => false
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $totalcount = $row['TOTALCOUNT'];
            }
            $data= array('result'=>true,'TOTALCOUNT' =>$totalcount);
        }
    }
    return $data;
}