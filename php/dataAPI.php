<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込みabbcd
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");
include_once("getQryCnd.php");
include_once("base/createExecuteSQL.php");
include_once("chkCondParam.php");

/*
*-------------------------------------------------------* 
* API用GETパラメータ
*-------------------------------------------------------*
*/

//ADD
$api  = (isset($_GET['api'])?$_GET['api']:'');    //1ならAPI処理
$api_user = (isset($_GET['user'])?$_GET['user']:'');
$api_pass = (isset($_GET['pass'])?$_GET['pass']:'');
$api_query = (isset($_GET['query'])?$_GET['query']:'');
$api_pivot = (isset($_GET['pivot'])?$_GET['pivot']:'');
$api_params = (isset($_GET['params'])?$_GET['params']:'');

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$touch = (isset($_POST['TOUCH'])?$_POST['TOUCH']:'0');
$pivot = (isset($_POST['PIVOT'])?$_POST['PIVOT']:'0');
$d1name = $_POST['D1NAME'];
$pmpkey = (isset($_POST['PMPKEY'])?$_POST['PMPKEY']:'');
//以下はAXES対応
$axes = (isset($_POST['AXES'])?$_POST['AXES']:'0');
$user = (isset($_POST['USER'])?$_POST['USER']:'');
$pass = (isset($_POST['PASS'])?$_POST['PASS']:'');

//ADD
if($api === '1'){
    $user = $api_user;
    $pass = $api_pass;
    $d1name = $api_query;
    $pmpkey = $api_pivot;
}
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
//ADD
if($api === '1'){
    $usrinfo = array(array('WUUID' => $user));
}else{
    $usrinfo = (($touch == '1')?$_SESSION['PHPQUERYTOUCH']['user']:$_SESSION['PHPQUERY']['user']);
}
$initRs = '';
$doRs = '';
$filename = '';
$JSEQ = '';
$rtn = 0;
$msg = '';
$text = '';
$text_nohtml = '';
$directResultFlg = (isset($_POST['directResultFlg'])?$_POST['directResultFlg']:'0');    //直接result画面を開いたときに1になる
$directGetFlg = 0;  //直接result画面を開いたとき、直前のセッションで検索を実行した場合に1になる
$data = (isset($_POST['DATA'])?json_decode($_POST['DATA'],true):array());
$data_session = (isset($_POST['DATA'])?$_POST['DATA']:'');
//ボタン表示フラグ
$csvFlg = true;
$excelFlg = true;
$schFlg = true;
$FDB2CSV1 = array();


/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,$usrinfo[0]['WUUID'],$d1name,$pmpkey);
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 2;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 2;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}

if($rtn === 0){
    //定義名取得
    $res = fnGetD1TEXT($db2con,$d1name);
    if($res['result'] !== true){
        $rtn = 1;
        $msg = showMsg($res['result']);
    }else{            
        $FDB2CSV1 = $res['data'];
        if($pivot === '1'){
            $res = fnGetPMTEXT($db2con,$d1name,$pmpkey);
            if($res['result'] !== true){
                $rtn = 1;
                $msg = showMsg($res['result']);
            }else{
                $text = $res['PMTEXT'];
            }
        }else{
            $text = $res['D1TEXT'];
        }
    }
}
if($rtn === 0){
    $text_nohtml = cmMer($text);
    $text        = cmHsc(cmMer($text));
    $D1WEBF    = $FDB2CSV1[0]['D1WEBF'];
    //ファイル名生成
    $filename = makeRandStr(10,true);
}
if($D1WEBF !== '1'){
    //FDB2CSV3を取得(セッションに保存,tableInitで比較に使用)
    if($rtn === 0){
        $res = fnGetFDB2CSV3($db2con,$d1name);
        if($res['result'] !== true){
            $rtn = 1;
            $msg =  showMsg($res['result']);
        }else{
            $FDB2CSV3 = $res['data'];
        }    
    }

    if($rtn === 0){

        //API処理の場合はセッション関係なし
        if($api !== '1'){

            //直接result画面から来たときは直前のセッションを比較して、
            //FDB2CSV3が同じなら直前の検索状態を表示する
            if($directResultFlg === '1' && $axes === '0' && $touch === '0'){
                if(isset($_SESSION['PHPQUERY']['schData'])){
                    if($_SESSION['PHPQUERY']['schData'] !== ''){
                        if($FDB2CSV3 === $_SESSION['PHPQUERY']['FDB2CSV3']){
                            $data_session = $_SESSION['PHPQUERY']['schData'];
                            $data = json_decode($data_session,true);
                            $directGetFlg = 1;
                        }
                    }
                }
            }else if($directResultFlg === '1'  && $axes === '0' && $touch === '1'){
                if(isset($_SESSION['PHPQUERYTOUCH']['schData'])){
                    if($_SESSION['PHPQUERYTOUCH']['schData'] !== ''){
                        if($FDB2CSV3 === $_SESSION['PHPQUERYTOUCH']['FDB2CSV3']){
                            $data_session = $_SESSION['PHPQUERYTOUCH']['schData'];
                            $data = json_decode($data_session,true);
                            $directGetFlg = 1;
                        }
                    }
                }
            }

            //PC版のみ、検索状態と検索条件をセッションに保存
            if($touch === '0'){
                $_SESSION['PHPQUERY']['schData'] = $data_session;
                $_SESSION['PHPQUERY']['FDB2CSV3'] = $FDB2CSV3;
            }else if($touch === '1'){
                $_SESSION['PHPQUERYTOUCH']['schData'] = $data_session;
                $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'] = $FDB2CSV3;
            }

        }
        
    }
    if($rtn === 0){
         //検索条件チェック（ブランクチェックのみ）
        foreach($data as $key => $value){
            if($value['D3USEL'] === '1' && $value['D3DAT'] === ''){
                $rtn = 1;
                $msg = $value['D2HED'].'は入力必須です。';
                break;
            }
        }     
    }
    if($rtn === 0){
        //検索条件フラグ
        $FDB2CSV3 = umEx(cmGetFDB2CSV3($db2con,$d1name));
        if($rs === false || count($FDB2CSV3) === 0){
            $schFlg = false;
        }
    }
    if($rtn === 0){
        /****DEMO/COUNT*****************************/
        /*******************************************/
        
        //init処理
        cmInt($db2con,$initRs,$JSEQ,$d1name);
        
        //9以外の場合、ＤＯ処理
        if($initRs !== "9"){

            cmSetQTEMP($db2con);

           //初期画面の検索条件でFDB2CSV3を更新
            $r = updSearchCond($db2con,$data,$d1name);
            $rs = $r[0];

            if($rs === 1){
                $rtn = 1;
                $msg = showMsg('FAIL_INT_BYTE');//'検索データは128バイトまで入力可能です。';
            }else{

                //CL連携
                //ライセンスのCL連携権限がある場合のみ実行
                if($licenseCl === true){
                    setCmd($db2con, $d1name,$D1WEBF);
                }
                cmSetPHPQUERY($db2con);
                //DO処理
                cmDo($db2con,$doRs,$JSEQ,$filename,$d1name);

                /****DEMO/COUNT*****************************/
                /*******************************************/
                            //ログ出力
                $logrs = '';
                cmInsertDB2WLOG($db2con,$logrs,$usrinfo[0]['WUUID'],'D',$d1name,array(),'','');
                if($doRs === "1"){
                    $D2HED = cmGetD2HED($db2con,$d1name,$JSEQ);
                    $rtn = 1;
                    $msg = showMsg('FAIL_CMP',array(cmMer($D2HED)));//cmMer($D2HED).'の入力に誤りがあります。';
                }else if($doRs === "9"){
                    $rtn = 1;
                    $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
                }

            }
        }else{
            $rtn = 1;
            $msg = showMsg('FAIL_FUNC',array('クエリーの事前処理'));//'クエリーの事前処理に失敗しました。';   
        }
    }
}else{
    $setCmdFlg = true;
       // if($touch == '1'){
                /*
                * initially call tableInit from touch version if directGetFlg(initialFlg) is 1
                *to use in setCmd
                */
                $directResultFlg = (isset($_POST['directResultFlg'])?$_POST['directResultFlg']:'0');
                if($directResultFlg === '1'){
                        $setCmdData = array();
                        $DGBFLG = false;
                        $DGAFLG = false;
                        if(count($data)>0){
                                $setCmdData = $data;
                        }else{
                                $dataArr = array();
                                $rs = fnBQRYCNDDAT($db2con,$d1name);
                                if($rs['result'] !== true){
                                    $rtn = 1;
                                    $msg = showMsg($rs['result']);
                                }else{
                                    $dataArr = $rs['data'];
                                }
                                $dataArr = umEx($dataArr);
                                if(count($dataArr) > 0){
                                    $setCmdData = fnCreateCndData($dataArr);
                                }
                        }
                }else{
                        $setCmdData = $data;
                }
        /*}else{
                $setCmdData = $data;
        }*/
            //配列にCOUNTが一つだけあるとき、データーが非表示かどうかチェックして検索ボタン表示する権限
            if($setCmdData !== '' ){
                $key = key((array)$setCmdData);
                if(count($key) === 1){
                    $CNDSDATA = $setCmdData[0][CNDSDATA];
                    if(count($CNDSDATA) ===1){
                        if($CNDSDATA[0]['CNDTYP'] == '3'){
                            $setCmdFlg = false;
                        }
                    }
                }
            }

    //条件情報【BQRYNCNDとFDB2CSV2とFDB2CSV5】を取得(セッションに保存,tableInitで比較に使用)
    if($rtn === 0){
        $res = fnGetBQRYCND($db2con,$d1name);
        if($res['result'] !== true){
            $rtn = 1;
            $msg =  showMsg($res['result']);
        }else{
            $FDB2CSV3 = $res['data'];
        }
    }
    if($rtn === 0){

        //API処理の場合はセッション関係なし
        if($api !== '1'){
            //直接result画面から来たときは直前のセッションを比較して、
            //FDB2CSV3が同じなら直前の検索状態を表示する
            if($directResultFlg === '1'  && $axes === '0' && $touch === '0'){
                if(isset($_SESSION['PHPQUERY']['schData'])){
                    if($_SESSION['PHPQUERY']['schData'] !== ''){
                        if($FDB2CSV3 === $_SESSION['PHPQUERY']['FDB2CSV3']){
                            $data_session = $_SESSION['PHPQUERY']['schData'];
                            $data = json_decode($data_session,true);
                            $directGetFlg = 1;
                        }
                    }
                }
            }else if($directResultFlg === '1'  && $axes === '0' && $touch === '1'){
                if(isset($_SESSION['PHPQUERYTOUCH']['schData'])){
                    if($_SESSION['PHPQUERYTOUCH']['schData'] !== ''){
                        if($FDB2CSV3 === $_SESSION['PHPQUERYTOUCH']['FDB2CSV3']){
                            $data_session = $_SESSION['PHPQUERYTOUCH']['schData'];
                            $data = json_decode($data_session,true);
                            $directGetFlg = 1;
                        }
                    }
                }
            }

            //PC版のみ、検索状態と検索条件をセッションに保存
            if($touch === '0'){
                $_SESSION['PHPQUERY']['schData'] = $data_session;
                $_SESSION['PHPQUERY']['FDB2CSV3'] = $FDB2CSV3;
            }else if($touch === '1'){
                $_SESSION['PHPQUERYTOUCH']['schData'] = $data_session;
                $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'] = $FDB2CSV3;
            }

        }
    }
    if($rtn === 0){
        //検索条件フラグ
        if(count($FDB2CSV3) === 0 || $setCmdFlg === false){
            $schFlg = false;
        }
    }
    if($rtn === 0){
        cmSetPHPQUERY($db2con);
        $rsCLChk = chkFDB2CSV1PG($db2con,$d1name);
        if($rsCLChk['result'] !== true){
            $rtn = 1;
            $msg = $rsCLChk['result'];
        }else{
            if(count($rsCLChk['data']) > 0){
                $fdb2csv1pg = $rsCLChk['data'][0];
                if($fdb2csv1pg['DGBLIB'] !== '' || $fdb2csv1pg['DGBPGM'] !== ''){
                    $DGBFLG = true;
                }
                if($fdb2csv1pg['DGALIB'] !== '' || $fdb2csv1pg['DGAPGM'] !== ''){
                    $DGAFLG = true;
                }
            }
        }
    }
    if($rtn === 0){
        cmSetPHPQUERY($db2con);
//          $DATATEST = $data;

        $resChkCond = chkCondParamData($db2con,$d1name,$data);
        if($resChkCond['RTN'] > 0){
                $rtn = 1;
                $msg = $resChkCond['MSG'];
            }else{
                $cndParamList = $resChkCond['DATA']['BASEDCNDDATA'];
            }
            if(count($cndParamList) === 0){
                $resExeSql = runExecuteSQL($db2con,$d1name);
            }else{
                $resExeSql = runExecuteSQL($db2con,$d1name,$cndParamList);
            }
        if($resExeSql['RTN'] !== 0){
            $rtn = 1;
            $msg = $resExeSql['MSG'];
        }else{
            $qryData = $resExeSql;
        }
        
    }
    if($rtn === 0){

        cmDb2Close($db2con);

        $db2LibCon = cmDb2ConLib($qryData['LIBLIST']);


        if($licenseCl === true){
            if($DGBFLG === true || $DGAFLG === true){
                //init処理
                cmIntWC($db2LibCon,$initRs,'INT',$d1name);
            }
        }
        //9以外の場合
        if($initRs !== "9"){
            //CL連携
            //ライセンスのCL連携権限がある場合のみ実行
            if($licenseCl === true){
                if($DGBFLG === true){

                    setCmd($db2LibCon, $d1name,$D1WEBF,$setCmdData);

                    cmIntWC($db2LibCon,$initRs,'BEF',$d1name);
                    if($initRs !== "9"){
                    }else{
                        $rtn = 1;
                        $msg = showMsg('FAIL_FUNC',array('実行前CL連携の処理'));//'実行前CL連携の処理に失敗しました。';
                    }
                }

            }

            if($rtn === 0){
                    $resExec = execQry($db2LibCon,$qryData['STREXECSQL'],$qryData['EXECPARAM'],$qryData['LIBLIST']);
                    if($resExec['RTN'] !== 0){
                        $rtn = 1;
                        $msg = showMsg($resExec['MSG']);
                    }else{
                        $resCreateTbl = createTmpTable($db2LibCon,$qryData['LIBLIST'],$qryData['STREXECSQL'],$qryData['EXECPARAM'],$filename);
                        if($resCreateTbl['RTN'] !== 0){            
                            $rtn = 1;
                            $msg = showMsg($resCreateTbl['MSG']);
                        }else{
                            //ライセンスのCL連携権限がある場合のみ実行
                            if($licenseCl === true){
                                if($DGAFLG === true){
                                    cmIntWC($db2LibCon,$initRs,'AFT',$d1name);
                                    //9以外の場合、ＤＯ処理
                                    if($initRs === "9"){
                                            $rtn = 1;
                                            $msg = showMsg('FAIL_FUNC',array('実行後CL連携の処理'));//'実行後CL連携の処理に失敗しました。';
                                    }
                                }
                            }
                        }
                    }
                    $logrs = '';
                    $db2con = cmDb2Con();
                    cmSetPHPQUERY($db2con);
                    cmInsertDB2WLOG($db2con,$logrs,$usrinfo[0]['WUUID'],'D',$d1name,array(),'','');

            }
           

        }else{
            $rtn = 1;
            $msg = showMsg('FAIL_FUNC',array('CL連携の準備'));//'CL連携の準備に失敗しました。';
        }
        cmDb2Close($db2LibCon);
    }
}

//APIでは以下必要なし

if($api !== '1'){

    //CSV、EXCELダウンロト権限チェックためフラグ取得
    if($rtn === 0){
        $res = fnGetDB2WDEF($db2con,$usrinfo[0]['WUUID'],$d1name);
        if($res['result'] !== true){
            $rtn = 1;
            $msg =  showMsg($res['result']);
        }else{
            $DB2WDEF = $res['data'];
            if($DB2WDEF['WDDWNL'] !== "1"){
                $csvFlg = false;
            }
            if($DB2WDEF['WDDWN2'] !== "1"){
                $excelFlg = false;
            }
        }    
    }
    // HTMLで表示ためデータ取得
    $htmldata = array();
    if($rtn === 0){
        $rs =  fnGetDB2HTMLT($db2con,$d1name);
        if($rs['result'] === true){
                $htmldata = umEx($rs['data']);
         }else{
                $rtn = 1;
                $msg = showMsg('FAIL_FUNC',array('クエリーの実行に失敗しました。'));//'クエリーの実行に失敗しました。';
         }
    }

    //定義の条件変更することがあるかのチェック
    if($rtn === 0){
        $OLD_FDB2CSV3 = (isset($_POST['csvoldAry'])?json_decode($_POST['csvoldAry'],true):'');
        if($OLD_FDB2CSV3 !== ''){
            if($OLD_FDB2CSV3 != $FDB2CSV3){
                $rtn = 2;
                $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));//"クエリーの検索条件が変更されています。再度メニューから実行し直してください";
            }
        }
    }
    //セッションに検索データと条件データをリセット
    if($rtn !== 0){

        if($touch === '0'){
            $_SESSION['PHPQUERY']['schData'] = '';
            $_SESSION['PHPQUERY']['FDB2CSV3'] = array();
        }else if($touch === '1'){
            $_SESSION['PHPQUERYTOUCH']['schData'] = '';
            $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'] = array();
        }

    }

}

//APIの場合のみ、データ取得
if($api === '1'){
    $FDB2CSV2 = cmGetFDB2CSV2($db2con,$d1name,false,false,false);
    $data = cmGetDbFile($db2con,$FDB2CSV2['data'],$filename,'','',$iSortCol_0,$sSortDir_0,'',$sSearch,$db2wcol,false,'','',array(),array());
    if($csv_d['result'] !== true){
        $msg = showMsg($csv_d['result']);
        $rtn = 1;
    }
    
}

/*if($rtn === 0){

    $rtn = 1;
    $msg = 'ウェブで作成されたクエリーです。';
}*/
cmDb2Close($db2con);

/**return**/
$rtnArray = array(
        'RTN'          => $rtn,
        'MSG'          => $msg,
        'usrinfo'      => $usrinfo,
        'd1name'       => $d1name,
        'initRs'       => $initRs,
        'doRs'         => $doRs,
        'filename'     => $filename,
        'text'         => $text,
        'text_nohtml'  => $text_nohtml,
        'csvFlg'       => $csvFlg,
        'excelFlg'     => $excelFlg,
        'schFlg'       => $schFlg,
        'FDB2CSV1'     => $FDB2CSV1,
        'DATATEST' =>  $setCmdData,
        'data'         => ($data_session !== '')?json_decode($data_session,true):'',
        'directGetFlg' => $directGetFlg,
        'FDB2CSV3'     => $FDB2CSV3,
        'OLD_FDB2CSV3' => $OLD_FDB2CSV3,
        'DSPROW1'      => DSPROW1,
        'DSPROW2'      => DSPROW2,
        'DSPROW3'      => DSPROW3,
        'DSPROW4'      => DSPROW4,
        'HTMLDATA'     => $htmldata,
        'DB2CSV1PM'    => $DB2CSV1PM,
        'D1WEBF'       => $D1WEBF,
        'directResultFlg'  =>$directResultFlg
);
echo(json_encode($rtnArray));

/*
*-------------------------------------------------------* 
* 定義名取得
*-------------------------------------------------------*
*/

function fnGetD1TEXT($db2con,$D1NAME){

    $data = array();

    //$strSQL  = ' SELECT A.D1TEXT ';
    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;
    $strSQL .= ' WHERE D1NAME = ? ' ;

    $params = array(
        $D1NAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => umEx($data)
                ,'D1TEXT' => $data[0]['D1TEXT']
            );
        }
    }        
    return $data;
}

/*
*-------------------------------------------------------* 
* 定義名取得
*-------------------------------------------------------*
*/

function fnGetPMTEXT($db2con,$PMNAME,$PMPKEY){

    $data = array();
    
    //$strSQL  = ' SELECT A.PMTEXT ';
    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2PMST AS A ' ;
    $strSQL .= ' WHERE PMNAME = ? AND PMPKEY = ? ' ;

    $params = array(
        $PMNAME,
        $PMPKEY
    ); 
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => $data
                ,'PMTEXT' => $data[0]['PMTEXT']
            );
        }
    }        
    return $data;
}

/*
*-------------------------------------------------------* 
* CSV、EXCEL権限取得
*-------------------------------------------------------*
*/

function fnGetDB2WDEF($db2con,$WDUID,$WDNAME){

    $data = array();    
    $strSQL  = ' SELECT A.WDDWNL,A.WDDWN2 ';
    $strSQL .= ' FROM DB2WDEF AS A ' ;
    $strSQL .= ' WHERE WDUID = ? ' ;
    $strSQL .= ' AND WDNAME = ? ' ;
    $params = array(
          $WDUID
        , $WDNAME
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{

        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => $data[0]
            );
        }
    }
    return $data;
}

// FDB2CSV3を取得
function fnGetFDB2CSV3($db2con,$d3name){

    $data = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV3 AS A ' ;
    $strSQL .= ' WHERE A.D3NAME = ? ';

    $params = array(
        $d3name
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => $data
            );
        }
    }
    return $data;
}

function chkFDB2CSV1PG($db2con,$d1name){
    $data = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1PG A ' ;
    $strSQL .= ' WHERE A.DGNAME  = ? ';

    $params = array(
        $d1name
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => umEx($data,false)
            );
        }
    }
    return $data;
}
