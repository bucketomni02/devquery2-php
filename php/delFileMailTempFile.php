<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
 
/**PHPEXCELが異常終了した時にできるtmpファイルを削除する**/
foreach (glob(PHP_DIR.'csv/*') as $val) {
    checkFile($val);
}
foreach (glob(PHP_DIR.'xls/*') as $val) {
    checkFile($val);
}
foreach (glob(PHP_DIR.'html/*') as $val) {
    checkFile($val);
}
foreach (glob(PHP_DIR.'tmp_passzip/*') as $val) {
    checkFile($val);
}
foreach (glob(PHP_DIR.'tmp_zip/*') as $val) {
    checkFile($val);
}
foreach (glob(PHP_DIR.'downloadtmp/*') as $val) {
    checkFile($val);
}
function checkFile($filename){
    $datetime1 = date("Y-m-d H:i:s", filectime($filename));
    $datetime2 = date("Y-m-d H:i:s");
    $interval = date_diff(new DateTime($datetime1), new DateTime($datetime2));
    $year=$interval->format('%R%y');
    $month=$interval->format('%R%m');
    $day=$interval->format('%R%a');
    $hour=$interval->format('%R%h');
    if($day >= 1){
        @unlink($filename);
    }
}
