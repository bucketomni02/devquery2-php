<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
//ini_set('error_log', SAVE_DIR . SISTEM . '/logdelQry.log');
//
$rtn = 0;
$EXETIME = date("Y-m-d H:i:s");
e_log('ワークファイル削除呼び出し：'.$EXETIME);
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
// DB2WTBLにあるデータの中で一日前作成したテーブル情報が有ったら削除
if($rtn === 0){
    $rsDelWtbl = fnDelDB2WTBL($db2con,$EXETIME);
    if($rsDelWtbl !== true){
        $rtn = 1;
        e_log('DB2WTBLのデータ削除失敗。');
    }
}
if($rtn === 0){
    $rsDel = fnDelTmpTbl($db2con,$EXETIME);
    if($rsDel === 1){
        $rtn = 1;
        e_log('削除失敗'.$scheduleTbl['WHOUTF']);
    }
    $rsScheduleTbl = fnGetScheduleTblList($db2con);
    if($rsScheduleTbl['result'] !== true){
        $rtn = 1;
        e_log(showMsg($rsScheduleTbl['result']));
    }else{
        $rsScheduleTbl = $rsScheduleTbl['data'];
        if(count($rsScheduleTbl) > 0){
            foreach($rsScheduleTbl as $scheduleTbl){
                $rsDel = fnDelTmpTbl($db2con,$EXETIME,$scheduleTbl['WHOUTF'],$scheduleTbl['D1HOKN']);
                if($rsDel === 1){
                    $rtn = 1;
                    e_log('削除失敗'.$scheduleTbl['WHOUTF']);
                    break;
                }
            }
        }
    }
    
    cmDb2Close($db2con);
}

function fnGetScheduleTblList($db2con){
    $rtn = 0;
    $scheduleTbl = array();
    $strSQL  =  ' SELECT     A.WHNAME, ';
    $strSQL .=  '             A.WHOUTF ';
    $strSQL .=  '             ,B.D1HOKN ';
    $strSQL .=  ' FROM  ';
    $strSQL .=  '     DB2WHIS AS A  ';
    $strSQL .=  ' LEFT JOIN  FDB2CSV1 AS B  ';
    $strSQL .=  ' ON A.WHNAME = B.D1NAME  ';
    //$strSQL .=  ' RIGHT JOIN QSYS2.SYSTABLES C ';
    $strSQL .=  ' RIGHT JOIN '. SYSCOLUMN2 . ' C ';
    $strSQL .=  ' ON A.WHOUTF = C.TABLE_NAME '; 
    $strSQL .=  ' WHERE ';
    $strSQL .=  '    A.WHOUTF <> \'\' AND ';
    $strSQL .=  '    B.D1RDB IN (\'\',\''.RDBNM.'\',\''.RDBNAME.'\') AND ';
    $strSQL .=  '    B.D1HOKN > 0 ';
    $params = array($RDBNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $scheduleTbl = array('result' => 'FAIL_SEL');
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $scheduleTbl = false;
            e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                if($row === false){
                    e_log('fetcherr:'.db2_stmt_errormsg());
                }else{
                    $scheduleTbl[] = $row;
                }
            }
            $scheduleTbl = array('result' => true,'data' => umEx($scheduleTbl));
        }
    }
    e_log('scheduleRDBLIST:'.print_r($scheduleTbl,true));
    return $scheduleTbl;
}
function fnGetFDB2CSV1RDBList($db2con){
    $rtn = 0;
    $fdb2csv1Rdb = array();
    $strSQL  = ' SELECT ';
    $strSQL .= '     A.D1RDB ';
    $strSQL .= ' FROM ';
    $strSQL .= '   FDB2CSV1 A ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.D1RDB <> \'\' ';
    $strSQL .= ' AND A.D1RDB <> \''.RDB.'\' ';
    $strSQL .= ' AND A.D1RDB <> \''.RDBNAME.'\' ';
    $strSQL .= ' GROUP BY ';
    $strSQL .= '     A.D1RDB ';
    $params = array();
    //e_log($strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $fdb2csv1Rdb = array('result' => 'FAIL_SEL');
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $fdb2csv1Rdb = false;
            e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                if($row === false){
                    e_log('fetcherr:'.db2_stmt_errormsg());
                }else{
                    $fdb2csv1Rdb[] = $row;
                }
            }
            $fdb2csv1Rdb = array('result' => true,'data' => umEx($fdb2csv1Rdb));
        }
    }
    //e_log('RDBARR:'.print_r($fdb2csv1Rdb,true));
    return $fdb2csv1Rdb;
}
function fnDelTmpTbl($db2con,$exeTime,$tblname = '',$cntDay = 0){
    $rtn = 0;
    if($rtn === 0){
        $rs = fnGetTmpTblLst($db2con,$tblname);
        if($rs['result'] !== true){
            $rtn = 1;
            e_log('実行結果テーブル取得失敗。エラー内容：'.$rs['result']);
        }else{
            //e_log($tblname.print_r($rs['data'],true));
            $tmpTblList = $rs['data'];
        }
    }
    if($rtn === 0){
        if(count($tmpTblList) >0){
            foreach($tmpTblList as $tmpTbl){
                $chkRs = checkTime($tmpTbl['CREATE_TIME'],$exeTime,$cntDay);
                if($chkRs){
                    e_log('削除対象：'.$tmpTbl['TABLE_NAME'].' date: '.$cntDay);
                    //var_dump(print_r($tmpTbl,true));
                }
                if($chkRs){
                    $strDelSQL  = ' DROP TABLE '.SAVE_DB.'/'.$tmpTbl['TABLE_NAME'];
                    $result = db2_exec($db2con, $strDelSQL);
                    if ($result) {
                        e_log($tmpTbl['TABLE_NAME'].'削除成功。');
                    }else{
                        e_log($tmpTbl['TABLE_NAME'].'削除失敗。');
                        e_log('エラー内容'.db2_stmt_errormsg());
                        //$rtn = 1;
                        //break;  
                    }
                }
            }
        }
    }
    return $rtn;
}
function fnGetTmpTblLst($db2con,$tblname){
    $tmpTbl = array();
    
    $strSQL  =  ' SELECT ';
    $strSQL .=  '     TABLE_NAME,VARCHAR_FORMAT(LAST_ALTERED_TIMESTAMP, \'YYYY-MM-DD HH24:MI:SS\') AS CREATE_TIME ';
    $strSQL .=  ' FROM ';
    $strSQL .=  '     QSYS2/SYSTABLES ';
    $strSQL .=  ' WHERE ';
    $strSQL .=  '     TABLE_SCHEMA = \''.SAVE_DB.'\' AND ';
    $strSQL .=  '     TABLE_OWNER <> \'QSECOFR\' AND ';
    $strSQL .=  '     TABLE_NAME NOT IN (SELECT WTBLNM FROM DB2WTBL) AND ';
    if($tblname === ''){
        $strSQL .=  '     TABLE_NAME LIKE \'@%\' ';
        $params = array();
    }else{
        $strSQL .=  '     TABLE_NAME = ? ';
        $params = array($tblname);
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $tmpTbl = array('result' => 'FAIL_SEL');
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
         $r = db2_execute($stmt,$params);
        if($r === false){
            $tmpTbl = false;
            e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $tmpTbl[] = $row;
            }
            $tmpTbl = array('result' => true,'data' => umEx($tmpTbl));
        }
    }
    return $tmpTbl;
}

function checkTime($createtime,$exeTime,$cntDay = 0){
    $delFlg = false;
    $datetime1 = date("Y-m-d H:i:s", strtotime($createtime));
    $datetime2 = $exeTime;
    if($cntDay > 0){
        $cntdayDate = date_add(new DateTime($datetime1),date_interval_create_from_date_string('\"'.$cntDay. 'days\"'));
        $cntdayDate = date_format($cntdayDate,"Y-m-d H:i:s");
        if(new DateTime($cntdayDate) < new DateTime($datetime2)){
            $delFlg = true;
        }
    }else{
        $interval = date_diff(new DateTime($datetime1), new DateTime($datetime2));
        $year=(int)$interval->format('%R%y');
        $month=(int)$interval->format('%R%m');
        $day=(int)$interval->format('%R%a');
        $hour=(int)$interval->format('%R%h');
        $min=(int)$interval->format('%R%i');
         if($year > 0 || $month > 0 || $day > 0 || $hour > 0){
            $delFlg = true;
        }else{
            if($min >= 10 ){
                $delFlg = true;
            }
        }
    }
    return $delFlg;
}
function fnDelDB2WTBL($db2con,$EXETIME){
    $result = true;
    $params = array();
    $strSQL .= ' DELETE ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WTBL ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     WTBLCT < TIMESTAMP(\''.$EXETIME.'\')- 1 DAYS ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $result = 'FAIL_DEL';
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
         $r = db2_execute($stmt,$params);
        if($r === false){
            $result = 'FAIL_DEL';
            e_log('失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }
    }
    return $result;
}
function is_leap_year($year) {
    return ((($year % 4) == 0) && ((($year % 100) != 0) || (($year % 400) == 0)));
}