<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

//1
// 定義に使用しているすべてのライブラリーリスト取得
//fnGetRDBList 
// ある場合 ①RDBのSAVE_DBから作成されるのは10分以上の「＠」がつけている全部のテーブルを削除
//          ②保険日数を超えているスケジュール実行にテーブルを削除
// 1-②
// 削除対象テーブルと日数を取得して削除；

$rtn = 0;
$EXETIME = date("Y-m-d H:i:s");

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

if($rtn === 0){
    $rdbrs = fnGetFDB2CSV1RDBList($db2con);
    if($rdbrs['result'] !== true) {
        $rtn = 1;
        e_log(showMsg($rdbrs['result']));
    }else{
        $rdbrs = $rdbrs['data'];
    }
    if($rtn === 0){
        if(count($rdbrs)>0){
            foreach($rdbrs as $rdb){
                $_SESSION['PHPQUERY']['RDBNM'] = $rdb['D1RDB'];
                $db2RDBCon = cmDB2ConRDB(SAVE_DB);
                $rsDel = fnDelTmpTbl($db2RDBCon,$EXETIME);
                if($rsDel === 1){
                    $rtn = 1;
                    e_log('削除失敗'.$scheduleTbl['WHOUTF']);
                }
                $rsScheduleTbl = fnGetScheduleTblList($db2con,$rdb['D1RDB']);
                if($rsScheduleTbl['result'] !== true){
                    $rtn = 1;
                    e_log(showMsg($rsScheduleTbl['result']));
                    break;
                }else{
                    $rsScheduleTbl = $rsScheduleTbl['data'];
                    if(count($rsScheduleTbl) > 0){
                        foreach($rsScheduleTbl as $scheduleTbl){
                            $rsDel = fnDelTmpTbl($db2RDBCon,$EXETIME,$scheduleTbl['WHOUTF'],$scheduleTbl['D1HOKN']);
                            if($rsDel === 1){
                                $rtn = 1;
                                e_log('削除失敗'.$scheduleTbl['WHOUTF']);
                                break;
                            }
                        }
                    }
                }
                cmDb2Close($db2RDBCon);
            }
        }
    }
}

function fnGetScheduleTblList($db2con,$RDBNM){
    $rtn = 0;
    $scheduleTbl = array();
    $strSQL  =  ' SELECT     A.WHNAME, ';
    $strSQL .=  '             A.WHOUTF ';
    $strSQL .=  '             ,B.D1HOKN ';
    $strSQL .=  ' FROM  ';
    $strSQL .=  '     DB2WHIS AS A  ';
    $strSQL .=  ' LEFT JOIN  FDB2CSV1 AS B  ';
    $strSQL .=  ' ON A.WHNAME = B.D1NAME  ';
    $strSQL .=  ' WHERE ';
    $strSQL .=  '    A.WHOUTF <> \'\' AND ';
    $strSQL .=  '    B.D1RDB = ? AND ';
    $strSQL .=  '    B.D1HOKN > 0 ';
    $params = array($RDBNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $scheduleTbl = array('result' => 'FAIL_SEL');
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $scheduleTbl = false;
            e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                if($row === false){
                    e_log('fetcherr:'.db2_stmt_errormsg());
                }else{
                    $scheduleTbl[] = $row;
                }
            }
            $scheduleTbl = array('result' => true,'data' => umEx($scheduleTbl));
        }
    }
    e_log('scheduleRDBLIST:'.print_r($scheduleTbl,true));
    return $scheduleTbl;
}
function fnGetFDB2CSV1RDBList($db2con){
    $rtn = 0;
    $fdb2csv1Rdb = array();
    $strSQL  = ' SELECT ';
    $strSQL .= '     A.D1RDB ';
    $strSQL .= ' FROM ';
    $strSQL .= '   FDB2CSV1 A ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.D1RDB <> \'\' ';
    $strSQL .= ' AND A.D1RDB <> \''.RDB.'\' ';
    $strSQL .= ' AND A.D1RDB <> \''.RDBNAME.'\' ';
    $strSQL .= ' GROUP BY ';
    $strSQL .= '     A.D1RDB ';
    $params = array();
    //e_log($strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $fdb2csv1Rdb = array('result' => 'FAIL_SEL');
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $fdb2csv1Rdb = false;
            e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                if($row === false){
                    e_log('fetcherr:'.db2_stmt_errormsg());
                }else{
                    $fdb2csv1Rdb[] = $row;
                }
            }
            $fdb2csv1Rdb = array('result' => true,'data' => umEx($fdb2csv1Rdb));
        }
    }
    //e_log('RDBARR:'.print_r($fdb2csv1Rdb,true));
    return $fdb2csv1Rdb;
}
function fnDelTmpTbl($db2RDBCon,$exeTime,$tblname = '',$cntDay = 0){
    $rtn = 0;
    if($rtn === 0){
        $rs = fnGetTmpTblLst($db2RDBCon,$tblname);
        if($rs['result'] !== true){
            $rtn = 1;
            e_log('実行結果テーブル取得失敗。エラー内容：'.$rs['result']);
        }else{
            $tmpTblList = $rs['data'];
        }
    }
    if($rtn === 0){
        if(count($tmpTblList) >0){
            foreach($tmpTblList as $tmpTbl){
                $chkRs = checkTime($tmpTbl['CREATE_TIME'],$exeTime,$cntDay);
                if($chkRs){
                    e_log('削除対象：'.print_r($tmpTbl,true));
                    var_dump(print_r($tmpTbl,true));
                }
                if($chkRs){
                    $strDelSQL  = ' DROP TABLE '.SAVE_DB.'/'.$tmpTbl['TABLE_NAME'];
                    $result = db2_exec($db2RDBCon, $strDelSQL);
                    if ($result) {
                        e_log($tmpTbl['TABLE_NAME'].'削除成功。');
                    }else{
                        e_log($tmpTbl['TABLE_NAME'].'削除失敗。');
                        e_log('エラー内容'.db2_stmt_errormsg());
                        $rtn = 1;
                        break;
                    }
                }
            }
        }
    }
    return $rtn;
}
function fnGetTmpTblLst($db2con,$tblname){
    $tmpTbl = array();
    
    $strSQL  =  ' SELECT ';
    $strSQL .=  '     TABLE_NAME,VARCHAR_FORMAT(LAST_ALTERED_TIMESTAMP, \'YYYY-MM-DD HH24:MI:SS\') AS CREATE_TIME ';
    $strSQL .=  ' FROM ';
    $strSQL .=  '     QSYS2/SYSTABLES ';
    $strSQL .=  ' WHERE ';
    $strSQL .=  '     TABLE_SCHEMA = \''.SAVE_DB.'\' AND ';
    if($tblname === ''){
        $strSQL .=  '     TABLE_NAME LIKE \'@%\' ';
        $params = array();
    }else{
        $strSQL .=  '     TABLE_NAME = ? ';
        $params = array($tblname);
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $tmpTbl = array('result' => 'FAIL_SEL');
        e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
    }else{
         $r = db2_execute($stmt,$params);
        if($r === false){
            $tmpTbl = false;
            e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $tmpTbl[] = $row;
            }
            $tmpTbl = array('result' => true,'data' => umEx($tmpTbl));
        }
    }
    e_log('TempTableList:'.print_r($tmpTbl,true));
    return $tmpTbl;
}

function checkTime($createtime,$exeTime,$cntDay = 0){
    $delFlg = false;
    $datetime1 = date("Y-m-d H:i:s", strtotime($createtime));
    $datetime2 = $exeTime;
    $interval = date_diff(new DateTime($datetime1), new DateTime($datetime2));
    $year=$interval->format('%R%y');
    $month=$interval->format('%R%m');
    $day=$interval->format('%R%a');
    $hour=$interval->format('%R%h');
    $min=$interval->format('%R%i');

    if($cntDay > 0){
        if($day >= $cntDay ){
            $delFlg = true;
        }
    }else{
        if($min >= 10 ){
            $delFlg = true;
        }
    }
    return $delFlg;
}
