<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");


/**PHPEXCELが異常終了した時にできるtmpファイルを削除する**/

foreach (glob(BASE_DIR.'phpxltmp*') as $val) {
    @unlink($val);
}

/**tempファイルを削除する**/

foreach (glob(BASE_DIR.'csv/*') as $val) {
    @unlink($val);
}
foreach (glob(BASE_DIR.'tmp_passzip/*') as $val) {
    @unlink($val);
}
foreach (glob(BASE_DIR.'tmp_zip/*') as $val) {
    @unlink($val);
}
foreach (glob(BASE_DIR.'xls/*') as $val) {
    @unlink($val);
}