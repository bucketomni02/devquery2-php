<?php

include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

//ダウンロード中フラグを立てる
$_SESSION['PHPQUERY']['DOWNLOAD'] = '1';

$dlname = cmHscDe($_GET['DLNAME']);
$d1name = cmHScDe($_GET['D1NAME']);
$fname = cmHscDe($_GET['FNAME']);
$customFlg = cmHscDe($_GET['CUSTOMFLG']);

//customFlgが1の場合(Excelテンプレートでカスタマイズの場合)はphp/templateのファイルをダウンロード

//customFlgが1の場合(Excelテンプレートでカスタマイズの場合)はphp/templateのファイルをダウンロード
if($customFlg !== ''){
    $fcsvnm = TEMP_DIR.(string)$fname.'.csv';

    
 //$fp = fopen(TEMP_DIR . cmMer($d1name) . $time . '.xls', 'w');
                      

    /*$cpyFcsvnm = TEMP_DIR.(string)$dlname.'.csv';
    if(copy($fcsvnm,$cpyFcsvnm) === false){
        echo 'no file';
    }*/
    $fxlsmnm = ETEMP_DIR.(string)$fname.'.'.$customFlg;
    //template file copy
    if(copy(ETEMP_DIR.(string)$d1name.'.'.$customFlg,$fxlsmnm) === false){
        echo 'no file';
    }
    

    /*$cpyFxlsmnm = TEMP_DIR.(string)$dlname.'.xlsm';
    if(copy($fxlsmnm,$cpyFxlsmnm) === false){
        echo 'no file';
    }*/
    
    $fname = TEMP_DIR.(string)$fname.'.zip';
    $dlname = (string)$dlname.'.zip';
    //error_log('csvnm'.$fcsvnm.'/r/n xlsmnm'.$fxlsmnm.'/r/n download File path'.$fname.'/r/n dwnname'.$dlname);
    
    shell_exec('7z a ' . '"' . $fname . '"' . ' ' . '"' . $fcsvnm.'" '.'"'.$fxlsmnm .'"'. ' -ssc');

    //shell_exec('jar cvf ' . '"'.$fname . '"' .' '.'"'.$cypFcsvnm.'" '.'"'.$fxlsmnm .'"');
     unlink($fcsvnm);
     unlink($fxlsmnm);
    //unlink($cpyFcsvnm);
    //unlink($cpyFxlsmnm);
}else{
    $fname = TEMP_DIR.(string)$fname;
}

if (file_exists($fname)) {

//    $dlname = mb_convert_encoding($dlname, "sjis-win","UTF-8");

/*
    $ua = $_SERVER['HTTP_USER_AGENT'];
    if(!(preg_match("/Chrome/i",$ua)) && !(preg_match("/Safari/i",$ua))){
        $dlname = mb_convert_encoding($dlname, "sjis-win","UTF-8");
        
    }else if((preg_match("/Edge/i",$ua))){
       $dlname = mb_convert_encoding($dlname, "sjis-win","UTF-8");
    }
*/
    
    //ファイルの拡張子を取得
     
    $tmp_ary = explode('.',$dlname);
    $extension = $tmp_ary[count($tmp_ary)-1];

    switch($extension){
        case 'xlsx':
            // Excel2007形式で出力する
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            break;
        case 'xlsm':
            // Excel2007形式で出力する
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            break;
        case 'xls':
            // Excel2003形式で出力する
            header('Content-Type: application/vnd.ms-excel');
            break;
        case 'csv':
            // CSV形式で出力する
            header('Content-Type: text/csv');
            break;
        case 'zip':
            // zip 形式で出力する
            header('Content-Type: application/zip');
            break;
    }
    
    //header('Content-Disposition: attachment;filename="'.$dlname.'"');
    header('Content-Disposition: attachment; filename*=UTF-8\'\''.rawurlencode($dlname));
    
    if (!readfile($fname)) {
        //die("Cannot read the file(".$fname.")");
    }
    unlink($fname);
}else{
	echo($fname.'nofile');
}

if($customFlg === ''){
    @unlink($fname);
}

//ダウンロード中フラグを落とす
$_SESSION['PHPQUERY']['DOWNLOAD'] = '0';

exit;

