<?php

include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

$fname = '/www/zendsvr6/htdocs/devquery2/php/downloadtmp/DOWNQRY_2018921_154438.xlsx';

if (file_exists($fname)) {


   $dlname = 'ABCDEFG.xlsx';

    $ua = $_SERVER['HTTP_USER_AGENT'];
    if(!(preg_match("/Chrome/i",$ua)) && !(preg_match("/Safari/i",$ua))){
        $dlname = mb_convert_encoding($dlname, "sjis-win","UTF-8");
        
    }else if((preg_match("/Edge/i",$ua))){
       $dlname = mb_convert_encoding($dlname, "sjis-win","UTF-8");
    }

    //ファイルの拡張子を取得
     
    $tmp_ary = explode('.',$dlname);
    $extension = $tmp_ary[count($tmp_ary)-1];

    switch($extension){
        case 'xlsx':
            // Excel2007形式で出力する
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            break;
        case 'xlsm':
            // Excel2007形式で出力する
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            break;
        case 'xls':
            // Excel2003形式で出力する
            header('Content-Type: application/vnd.ms-excel');
            break;
        case 'csv':
            // CSV形式で出力する
            header('Content-Type: text/csv');
            break;
        case 'zip':
            // zip 形式で出力する
            header('Content-Type: application/zip');
            break;
    }
    
    //header('Content-Disposition: attachment;filename="'.$dlname.'"');
    header('Content-Disposition: attachment; filename*=UTF-8\'\''.rawurlencode($dlname));
    
    if (!readfile($fname)) {
        //die("Cannot read the file(".$fname.")");
    }


}else{
    echo('no file');
}