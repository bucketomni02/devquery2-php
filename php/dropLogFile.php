<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//保管日数取得
$wcval = fnGetWCVAL($db2con);
$hokn  = $wcval[0]['WCVAL'];
fnDeleteLog($db2con, $hokn);

/*
 *-------------------------------------------------------* 
 * ログ保管日数取得
 *-------------------------------------------------------*
 */

function fnGetWCVAL($db2con)
{
    $data = array();
    
    $strSQL = ' SELECT WCVAL ';
    $strSQL .= ' FROM DB2WCPN as A ';
    $strSQL .= ' WHERE WCKEY = ? ';
    
    $params = array(
        '001'
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
    
}

/*
 *-------------------------------------------------------* 
 * 渡された日数以前のログを消去
 *-------------------------------------------------------*
 */

function fnDeleteLog($db2con, $HOKN)
{
    
    $rs = '0';
    //0の場合はなにもしない
    if (cmMer($HOKN) != '0') {
        
        //今日の日付より$HOKNの日数分さかのぼった日付を取得
        $befdate = date("Ymd", strtotime(date('Ymd') . " -" . $HOKN . " day"));
        
        //$befdate以前のログを消去
        
        $strSQL = ' DELETE FROM DB2WLOG ';
        $strSQL .= ' WHERE WLDAY < ? ';
        
        $params = array(
            $befdate
        );
        
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $rs = '1';
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $rs = '1';
            } else {
                
                //構文
                $strSQL = ' DELETE FROM DB2WSDA ';
                $strSQL .= ' WHERE SDDAY < ? ';
                
                $params = array(
                    $befdate
                );
                
                $stmt = db2_prepare($db2con, $strSQL);
                if ($stmt === false) {
                    $rs = '1';
                } else {
                    $r = db2_execute($stmt, $params);
                    if ($r === false) {
                        $rs = '1';
                    } else {
                        //構文
                        $strSQL = ' DELETE FROM DB2WSDK ';
                        $strSQL .= ' WHERE SKDAY < ? ';
                        
                        $params = array(
                            $befdate
                        );
                        
                        $stmt = db2_prepare($db2con, $strSQL);
                        if ($stmt === false) {
                            $rs = '1';
                        } else {
                            $r = db2_execute($stmt, $params);
                            if ($r === false) {
                                $rs = '1';
                            }
                            
                        }
                    }
                    
                }
            }
        }
    }
    return $rs;
    
}