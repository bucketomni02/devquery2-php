<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

$stmt = db2_prepare($db2con, "CALL ".MAINLIB.".DELSCHFIL");
$ret = db2_execute($stmt);

cmDb2Close($db2con);

exit();
