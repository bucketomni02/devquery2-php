<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$d1name = $_POST['D1NAME'];
$filename = $_POST['filename'];

$RTCD = '';

//ファイル削除
cmCallPHPQUERYW($db2con,$d1name,'END',$filename,$RTCD,$JSEQ);

cmDb2Close($db2con);

$rtn = array(
    'RTCD' => $RTCD
);

echo(json_encode($rtn));
