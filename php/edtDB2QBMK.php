<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("./common/inc/config.php");
include_once("./common/inc/common.inc.php");
include_once("./licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$FLAG=(isset($_POST['flag']))?$_POST['flag']:'';
$FILETYPE=(isset($_POST['filetype']))?$_POST['filetype']:'';
$QBMKEY=(isset($_POST['qbmkey']))?$_POST['qbmkey']:'';
$D1NAME=(isset($_POST['d1name']))?$_POST['d1name']:'';
if($QBMKEY === ''){
    $QBMKEY = $D1NAME;
}
$PIVOTKEY=(isset($_POST['pivotkey']))?$_POST['pivotkey']:'';
$GRAPHKEY=(isset($_POST['graphkey']))?$_POST['graphkey']:'';
$isQGflg = false;
if($FILETYPE === 'qgroupsearch' || $FILETYPE === 'pgroupsearch' || $FILETYPE === 'ggroupsearch' || $FILETYPE === '1'){
    $isQGflg = true;
}
if($FILETYPE === 'resultgridqgroup' || $FILETYPE === 'resulthtmlqgroup' || $FILETYPE === 'resulthtmltemplateqgroup' || $FILETYPE === 'pivotresultpgroup' || $FILETYPE === 'graphresultggroup'){
    $isQGflg = true;
}
$usrinfo         = (($touch == '1') ? $_SESSION['PHPQUERYTOUCH']['user'] : $_SESSION['PHPQUERY']['user']);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
//リターン配列
$rtnArray = array();
$rtn = 0;
$msg = '';
$gkeyDCount=0;
$dtotalcount=0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//クエリー、ピボット、グラフが存在してるかどうかのチェック
$QBMFLG = 0;
if($isQGflg){
    $QBMFLG = 1;
    $rs=cmGetQueryGroup($db2con,$D1NAME,$PIVOTKEY,$GRAPHKEY);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('クエリー'));
    }else{
        $D1NAMELIST=umEx($rs['data'],true);
    }
}
if ($rtn === 0) {
    if($isQGflg){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            if($res['LASTFLG']==='1'){
                $LASTD1NAME=$res['QRYGSQRY'];
            }
            $chkQry = cmChkQueryGroup($db2con,$usrinfo[0]['WUUID'],$D1NAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if ($chkQry['result'] === 'NOTEXIST_GPH') {
                $rtn = 2;
                $msg = showMsg('NOTEXIST_GET', array(
                    'グラフ'
                ));
                break;
            }else if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 2;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }//end of for loop
    }else{
        $chkQry = array();
        $chkQry = cmChkQuery($db2con, $usrinfo[0]['WUUID'], $D1NAME,$PIVOTKEY,$GRAPHKEY);
        if ($chkQry['result'] === 'NOTEXIST_GPH') {
            $rtn = 2;
            $msg = showMsg('NOTEXIST_GET', array(
                'グラフ'
            ));
        } else if ($chkQry['result'] === 'NOTEXIST_PIV') {
            $rtn = 2;
            $msg = showMsg('NOTEXIST_GET', array(
                'ピボット'
            ));
        } else if ($chkQry['result'] !== true) {
            $rtn = 2;
            $msg = showMsg($chkQry['result'], array(
                'クエリー'
            ));
        }
    }
}
if($rtn === 0){
    $rs = fnDeleteDB2QBMK($db2con,$usrinfo[0]['WUUID'], $D1NAME,$PIVOTKEY,$GRAPHKEY,$QBMFLG);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
if($FLAG !== '1' && $rtn === 0){
    $rs = fnInsertDB2QBMK($db2con,$usrinfo[0]['WUUID'], $D1NAME,$QBMKEY,$PIVOTKEY,$GRAPHKEY,$QBMFLG);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}else{

}

cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnArray));

/**
*お気に入りに登録
*/
function fnInsertDB2QBMK($db2con,$QBMUSR,$QBMQRY,$QBMKEY,$QBMPIV,$QBMGPH,$QBMFLG){
    $BMKSEQ=0;
    $rs=true;
	$strSQL  ='';
    $strSQL .= ' INSERT INTO DB2QBMK';
    $strSQL .='(';
    $strSQL .='QBMUSR,';
    $strSQL .='QBMQRY,';
    $strSQL .='QBMKEY,';
    $strSQL .='QBMPIV,';
    $strSQL .='QBMGPH,';
    $strSQL .='QBMFLG';
    $strSQL .=')';
    $strSQL .='VALUES';
    $strSQL .='(?,?,?,?,?,?)';
	$params = array($QBMUSR,$QBMQRY,$QBMKEY,$QBMPIV,$QBMGPH,$QBMFLG);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt1===false){
		 $rs='FAIL_INS';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs='FAIL_INS';
        }
    }
    return $rs;

}
/**
*お気に入りに削除
*/

function fnDeleteDB2QBMK($db2con,$QBMUSR,$QBMQRY,$QBMPIV,$QBMGPH,$QBMFLG){
	$data=array();
    $rs=true;
	$strSQL  ='';
    $strSQL = ' DELETE FROM DB2QBMK WHERE QBMUSR = ? AND QBMQRY = ? AND QBMPIV = ? AND QBMGPH = ? AND QBMFLG = ? ';
	$params = array($QBMUSR,$QBMQRY,$QBMPIV,$QBMGPH,$QBMFLG);
	$stmt = db2_prepare($db2con, $strSQL);
	if($stmt1===false){
         $rs='FAIL_DEL';
	}else{
		$r =db2_execute($stmt,$params);
        if($r===false){
            $rs='FAIL_DEL';
        }
	}
	return $rs;
}
