<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
 *-------------------------------------------------------* 
 * リクエスト
 *-------------------------------------------------------*
 */
$WUUID = '';
if (isset($_SESSION['PHPQUERY']['LOGIN']) && $_SESSION['PHPQUERY']['LOGIN'] === '1') {
    if ($_POST['WUUID'] === '') {
        $WUUID = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
    } else {
        $WUUID = $_POST['WUUID'];
    }
} else {
    $WUUID = $_POST['WUUID'];
}


$DATA  = json_decode($_POST['DATA'], true);
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$rtn   = 0;
$msg   = '';
$focus = '';
/*
 *-------------------------------------------------------* 
 * チェック処理
 *-------------------------------------------------------*
 */



$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    if ($_POST['WUUID'] === '') {
        if ($_SESSION['PHPQUERY']['user'][0]['WUUID'] === null) {
            $rtn = 1;
            $msg = showMsg('CHECK_LOGOUT', array(
                'ユーザー'
            ));
        }
    }
}

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmCheckUser($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }
}

/*
 *-------------------------------------------------------* 
 * 更新処理
 *-------------------------------------------------------*
 */


$WUUQFG = $DATA['WUUQFG'];
$WUUBFG = $DATA['WUUBFG'];


if ($rtn === 0) {
    $rs = fnUpdateMenuSetting($db2con, $WUUID, $WUUQFG, $WUUBFG);
    if ($rs !== true) {
        $rtn = 1;
        $msg = showMsg($rs, array(
            'フィールド'
        ));
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo (json_encode($rtnAry));

/*
 *-------------------------------------------------------* 
 * ヘルプ情報更新
 *-------------------------------------------------------*
 */

function fnUpdateMenuSetting($db2con, $WUUID, $WUUQFG, $WUUBFG)
{
    
    
    $rs      = true;
    $rsCount = array();
    
    $strSQL = ' SELECT COUNT(*) AS COUNT';
    $strSQL .= ' FROM DB2WUSR';
    $strSQL .= ' WHERE WUUID = ? ';
    
    $params = array(
        $WUUID
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $rsCount[] = $row['COUNT'];
            }
            
            if ($rsCount[0] > 0) {
                //構文
                $strSQL = ' UPDATE DB2WUSR ';
                $strSQL .= ' SET ';
                $strSQL .= ' WUUQFG = ?, ';
                $strSQL .= ' WUUBFG = ? ';
                $strSQL .= ' WHERE WUUID = ? ';
                $params = array(
                    $WUUQFG,
                    $WUUBFG,
                    $WUUID
                );
                $stmt   = db2_prepare($db2con, $strSQL);
                if ($stmt === false) {
                    $rs = 'FAIL_INS';
                } else {
                    $r = db2_execute($stmt, $params);
                    if ($r === false) {
                        $rs = 'FAIL_INS';
                    }
                }
                
            } else {
                $rs = 'NOTEXIST_UPD';
            }
            
        }
    }
    return $rs;
}
