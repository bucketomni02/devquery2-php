<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/

include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");

include_once("base/createExecuteSQL.php");
include_once("getQryCnd.php");


//直接実行のパラメータを取得
$argv1 = (isset($argv[1])?$argv[1]:'');
$argv2 = (isset($argv[2])?$argv[2]:'');
$rtn = 0;
$RTCD = ' ';
//DB接続
$db2con = cmDb2Con();
//ライブラリセット
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

if($argv1 !== '' && $argv2 !== ''){
    $fdb2csv1 = FDB2CSV1_DATA($argv1);
    if(count($fdb2csv1['data']) > 0){
        $fdb2csv1 = $fdb2csv1['data'][0];
        if($fdb2csv1['D1WEBF'] === '1'){
            if($rtn === 0){
                $rs = fnBQRYCNDDAT($db2con,$argv1);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }else{
                    $data = $rs['data'];
                }
                $data = umEx($data);
                if(count($data) > 0){
                    $cnddatArr = fnCreateCndData($data);
                }
            }
            if($rtn === 0){
                $resExeSql = runExecuteSQL($db2con,$argv1);
                if($resExeSql['RTN'] !== 0){
                    $rtn = 1;
                    e_log($resExeSql['MSG'],'1');
                }else{
                    $qryData = $resExeSql;
                }
            }
            if($rtn === 0){
                $rsCLChk = chkFDB2CSV1PG($db2con,$argv1);
                if($rsCLChk['result'] !== true){
                    $rtn = 1;
                    $msg = $rsCLChk['result'];
                }else{
                    if(count($rsCLChk['data']) > 0){
                        $fdb2csv1pg = $rsCLChk['data'][0];
                        if($fdb2csv1pg['DGBLIB'] !== '' || $fdb2csv1pg['DGBPGM'] !== ''){
                            $DGBFLG = true;
                        }
                        if($fdb2csv1pg['DGALIB'] !== '' || $fdb2csv1pg['DGAPGM'] !== ''){
                            $DGAFLG = true;
                        }
                    }
                }
            }
            if($rtn === 0){
                //RDB修正
                if($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME){
                    $db2LibCon = cmDb2ConLib($qryData['LIBLIST']);
                }else{
                    $_SESSION['PHPQUERY']['RDBNM'] = $qryData['SELRDB'];
                    $db2LibCon = cmDB2ConRDB($qryData['LIBLIST']);
                }
                if($DGBFLG === true || $DGAFLG === true){
                    if($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME){
                        cmIntWC($db2LibCon,$RTCD,'INT',$argv1);
                    }
                }
                if($RTCD == ' '){
                    if ($DGBFLG === true) {
                        //CL実行
                        if($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME){
                            if ($DGBFLG === true || $DGAFLG === true) {
                                setCmd($db2LibCon, $argv1, $WEBF, $cnddatArr);
                            }
                            if ($DGBFLG === true) {
                                cmIntWC($db2LibCon, $RTCD, 'BEF', $argv1);
                            }
                        }else{
                            //RDBのCL実行呼び出し;
                            $rsClExe = callClExecute($db2LibCon,$argv1,$cnddatArr,'BEF');
                            if($rsClExe === 0){
                                $RTCD = ' ';
                            }else{
                                $RTCD == '9';
                            }
                        }
                    }
//                    $dbname = makeRandStr(10);  
                  do{
                        $dbname = makeRandStr(10);
                        e_log('テーブル名：'.$dbname);
                        $systables = cmGetSystables($db2con, SAVE_DB, $dbname);
                   }while(count($systables) > 0);
                    //$resExec = execQry($db2LibCon,$qryData['STREXECSQL'],$qryData['EXECPARAM'],$qryData['LIBLIST']);
                    $resExec = execQry($db2LibCon,$qryData['STREXECTESTSQL'],$qryData['EXECPARAM'],$qryData['LIBLIST']);
                    if($resExec['RTN'] !== 0){
                        $rtn = 1;
                        e_log(showMsg($resExec['MSG']),'1');
                    }else{
                        $resCreateTbl = createTmpTable($db2LibCon,$qryData['LIBLIST'],$qryData['STREXECSQL'],$qryData['EXECPARAM'],$dbname);
                        if($resCreateTbl['RTN'] !== 0){
                            $rtn = 1;
                            e_log("rtn createtbl".$rtn,'1');
                            e_log(showMsg($resCreateTbl['MSG']),'1');
                        }else{
                            if ($DGGFLG === true) {
                                if($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME){
                                    cmIntWC($db2LibCon, $RTCD, 'AFT', $argv1);
                                }else{
                                    //RDBのCL実行呼び出し;
                                    $rsClExe = callClExecute($db2LibCon,$argv1,$cnddatArr,'AFT');
                                    if($rsClExe === 0){
                                        $RTCD = ' ';
                                    }else{
                                        $RTCD == '9';
                                    }
                                }
                            }
                            if($DIRE !== ''){
                                if($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME){
                                    createCSVFile($db2con,$argv1,$dbname,$argv2,$fdb2csv1,'');
                                }else{
                                    createCSVFile($db2con,$argv1,$dbname,$argv2,$fdb2csv1,$db2LibCon);
                                }
                            }
                        }
                    }
                    if($rtn !== 0){
                        e_log('クエリ実行失敗','1');
                    }else{
                        $RTCD = ' ';
                    }
                }
            }
        }
    }else{
        
        e_log(showMsg('指定したクエリーがありません。'),'1');
    }
}else{
    // パラメータ空白
}


function createCSVFile($db2con,$d1name,$dbname,$DIRE,$fdb2csv1,$db2RDBCon){
    $sSearch = '';
    $db2wcol = '';
    $rtn = 0;
    
    $fdb2csv2 = cmGetFDB2CSV2($db2con,$d1name,true,false,false);
    $fdb2csv2 = umEx($fdb2csv2['data']);
    //ヘッダー情報取得
    $csv_h = cmCreateHeaderArray($fdb2csv2);
    if($db2RDBCon === ''){
        $csv_d = cmGetDbFile($db2con,$fdb2csv2,$dbname,'','','','','','',array(),false,'','',array(),array());
    }else{
        $csv_d = cmGetDbFile($db2RDBCon,$fdb2csv2,$dbname,'','','','','','',array(),false,'','',array(),array());
    }
    if($csv_d['result'] !== true){
        e_log(showMsg($csv_d['result']),'1');
        $rtn = 1;
    }
    if(substr($DIRE,0,1) !== '/'){
        $DIRE = '/'.$DIRE;
    }
    $dirArr = explode('/',$DIRE);
    if(count($dirArr)>2){
        array_pop($dirArr);
        $dir = join('/',$dirArr);
        if(!is_dir($dir)){
            $rtn = 1;
            e_log('指定したファイルディレクトリはファイルシステムにありません。','1');
        }
    }
    if($rtn === 0){
       if(!file_exists($DIRE)){
            touch($DIRE);
        }
        $fp = fopen($DIRE,'w');
        $data = $csv_d['data'];
    	$data = umEx($data);
        array_unshift($data,$csv_h);
        foreach($data as $key => $row){
            _fputcsv($fp, $row, $key,$fdb2csv1, $fdb2csv2, array());
        }
        fclose($fp);
    }
        
}
function chkFDB2CSV1PG($db2con,$d1name){
    $data = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1PG A ' ;
    $strSQL .= ' WHERE A.DGNAME  = ? ';

    $params = array(
        $d1name
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => umEx($data,false)
            );
        }
    }
    return $data;
}
function getCurrentTimeStamp($db2con){
    $data = array();
    $strSQL  = '    SELECT  ';
    $strSQL .= '    TO_CHAR(CURRENT TIMESTAMP,\'YYYYMMDDHH24MISS\') AS CURTIME';
    $strSQL .= '    FROM sysibm/sysdummy1 ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => $data[0]
            );
        }
    }
    $curTime = $data['data']['CURTIME'];
    return $curTime;

}