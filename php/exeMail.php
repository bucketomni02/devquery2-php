<?php
/*
*-------------------------------------------------------* 
* RPGで約15秒おきに実行される
* スケジュールマスターを呼んでメール配信を行う
*-------------------------------------------------------*
*/


/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/

include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");

include_once("common/lib/PHPExcel/Classes/PHPExcel.php");
include_once("common/lib/PHPExcel/Classes/PHPExcel/IOFactory.php");
include_once("common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");
include_once("common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php");
include_once("base/createExecuteSQL.php");
include_once("getQryCnd.php");

require("common/lib/PHPMailer/class.phpmailer.php");

//error_log("start ExeMail");
/*
*-------------------------------------------------------*
* 共通変数
*-------------------------------------------------------*
*/

//スケジュールマスター格納配列
$OnceDB2WSCD = array();
//現在日付時刻曜日
$nowDate = date('d');
$nowTime = date('Hi');
$nowYobi = date('w');
$nowYmd  = date('Ymd');
$nowYymm = date('Ym');

e_log('BBBEmail');
//$nowYmd = '20160908';
//$nowTime = '1600';

//直接実行フラグ
$directFlg = false;

//直接実行のパラメータを取得
$argv1 = (isset($argv[1])?$argv[1]:'');
$argv2 = (isset($argv[2])?$argv[2]:'');

if($argv1 !== ''){
    $directFlg = true;
}


//$nowYmd = '20160720';
//$nowTime = '0620';

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//ライセンスのスケジュール実行権限がない場合、ここで処理終了
if($licenseScheduleBtn === false){
    exit();
}

//DB接続
$db2con = cmDb2Con();
//ライブラリセット
cmSetPHPQUERY($db2con);

// 利用時間チェック
/*if($rtn === 0){
    $rs = cmUtilTimeCheck($db2con);
    if($rs['result'] !== true){
        exit();
    }
}*/

if($directFlg === false){
    //スケジュールマスター取得(ONCE)
    $OnceDB2WSCD = getOnceDB2WSCD($db2con,'1',$nowYmd,$nowTime);
    //スケジュールマスター取得(WEEKLY)
    $WeeklyDB2WSCD = getWeeklyDB2WSCD($db2con,'2',$nowYobi,$nowTime);
    //スケジュールマスター取得(MONTH)
    $MonthDB2WSCD = getMonthDB2WSCD($db2con,'3',$nowDate,$nowTime,$nowYmd);
    //スケジュールマスター取得(INTERVAL)
    $IntervalDB2WSCD = getIntervalDB2WSCD($db2con,'4');

    //取得したスケジュールをループ→各処理
    if($OnceDB2WSCD['result'] === true){
        exeSchedule($db2con,$OnceDB2WSCD['data'],$nowYmd,$nowTime,$licenseCl);
    }else{
        e_log(showMsg($OnceDB2WSCD['result'],array('exeMail.php','getOnceDB2WSCD')),'1');
    }
    if($WeeklyDB2WSCD['result'] === true){
        exeSchedule($db2con,$WeeklyDB2WSCD['data'],$nowYmd,$nowTime,$licenseCl);
    }else{
       e_log(showMsg($WeeklyDB2WSCD['result'],array('exeMail.php','getWeeklyDB2WSCD')),'1');
    }
    if($MonthDB2WSCD['result'] === true){
        exeSchedule($db2con,$MonthDB2WSCD['data'],$nowYmd,$nowTime,$licenseCl);
    }else{
       e_log(showMsg($MonthDB2WSCD['result'],array('exeMail.php','getMonthDB2WSCD')),'1');
    }
    if($IntervalDB2WSCD['result'] === true){
        exeSchedule($db2con,$IntervalDB2WSCD['data'],$nowYmd,$nowTime,$licenseCl);
    }else{
       e_log(showMsg($IntervalDB2WSCD['result'],array('exeMail.php','getIntervalDB2WSCD')),'1');
    }

}else if($directFlg === true){
    $burstItmLst = array();
    $burstItmLst = getBurstItmLst($db2con,$argv1,$argv2);

    if($burstItmLst['result'] == true){
        $burstItmLst = $burstItmLst['data'];
        if(count($burstItmLst) !== 0){
            for ($i=0;$i<count($burstItmLst);$i++){
                $mailAddLst = array();
                $mailAddLst = getMailAddlst($db2con,$burstItmLst[$i]);
                if($mailAddLst['result']!== true){
                    e_log(showMsg($rs['result'],array('exeMail.php','getMailAddlst')),'1');
                }else{
                    $mailAddLst=$mailAddLst['data'];
                    $rs = exeQuery($db2con,$argv1,$nowYmd,$nowTime,$licenseCl,$argv2,$burstItmLst[$i],$mailAddLst);
                }
            }
        }else{
            $rs = exeQuery($db2con,$argv1,$nowYmd,$nowTime,$licenseCl,$argv2,array(),array());
        }
    }else{
        e_log(showMsg($rs['result'],array('exeMail.php','getBurstItmLst')),'1');
    }

}

//DBCLOSE
cmDb2Close($db2con);
exit();
/*
*-------------------------------------------------------* 
*取得したスケジュールをループ→各処理
*-------------------------------------------------------*
*/
function exeSchedule($db2con,$wscd,$nowYmd,$nowTime,$licenseCl){
    $wscd = umEx($wscd,true);
    foreach($wscd as $key => $value){
        $rs;
        $d1name = $value['WSNAME'];
        $wsfrq = $value['WSFRQ'];
/*****/
        $wspkey = '';
        if($value['WSPKEY'] !== ''){
            $wspkey = ($value['WSPKEY']);
        }else{
             $wspkey = '';
        }
/*****/

        //同じ時間ですでに実行されていないかチェック(返り値 true:未実行,false:実行済)
        $rs = checkDB2WSCD($db2con,$d1name,$nowYmd,$nowTime,$wspkey);
      //  $rs = array('result' => true,'data' => 0);
        if($rs['result'] === true){

            if($rs['data'] === 0){

    			//WSFRQが4の場合、前回実行時刻から今回の実行時刻を算出して、マッチしている時にクエリーを実行
    			if($wsfrq == '4'){

    				//前回実行時刻と比較、時間が来てたら実行
    				$wstime = $value['WSTIME'];		//実行間隔
    				$wsbday = $value['WSBDAY'];		//前回実行日付
    				$wsbtim = $value['WSBTIM'];		//前回実行時刻
    				$exetim = '';					//実行時刻

    				//実行間隔から今実行するかどうかをチェック
    				$check = checkAfterTime($wstime,$nowTime);
    				//$checkがtrueの場合、実行
    				if($check === true){
    		            $DB2WSCD = updDB2WSCD($db2con,$value['WSNAME'],$value['WSFRQ'],$value['WSODAY'],$value['WSWDAY'],$value['WSMDAY'],$value['WSTIME'],$wspkey,$nowYmd,$nowTime);
    		            //クエリー実行→エクセル作成→メール配信
                        if($DB2WSCD['result'] == true){
                            //$rs = exeQuery($db2con,$d1name,$nowYmd,$nowTime,$licenseCl,$wspkey);
                            $burstItmLst = array();
                            $burstItmLst = getBurstItmLst($db2con,$d1name,$wspkey);

                            if($burstItmLst['result'] == true){
                                $burstItmLst = $burstItmLst['data'];
                                if(count($burstItmLst) !== 0){
                                    for ($i=0;$i<count($burstItmLst);$i++){
                                        $mailAddLst = array();
                                        $mailAddLst = getMailAddlst($db2con,$burstItmLst[$i]);
                                        if($mailAddLst['result']!== true){
                                            e_log(showMsg($rs['result'],array('exeMail.php','getMailAddlst')),'1');
                                            //クエリー　バーストごとのメール宛先の取得に失敗しました。
                                        }else{
                                            $mailAddLst=$mailAddLst['data'];
                                            $rs = exeQuery($db2con,$d1name,$nowYmd,$nowTime,$licenseCl,$wspkey,$burstItmLst[$i],$mailAddLst);
                                        }
                                    }
                                }else{
                                    $rs = exeQuery($db2con,$d1name,$nowYmd,$nowTime,$licenseCl,$wspkey,array(),array());
                                }
                            }else{
                                e_log(showMsg($rs['result'],array('exeMail.php','getBurstItmLst')),'1');
                                $errMsg = showMsg('FAIL_FUNC',array('バースト情報の取得'));//"バースト情報の取得に失敗しました。";
                                fnCreateHistory($db2con,$value['WSNAME'],'','',$nowYmd,$nowTime,$wspkey,$errMsg);
                               }
                        }else{
                                $errMsg = showMsg('FAIL_FUNC',array('実行日付の更新'));//" 実行日付の更新に失敗しました。";
                                fnCreateHistory($db2con,$value['WSNAME'],'','',$nowYmd,$nowTime,$wspkey,$errMsg);
                                e_log(showMsg($DB2WSCD['result'],array('exeMail.php','updDB2WSCD')),'1');
                        }
    				}

    			//WSFRQが4以外の場合、そのままクエリーを実行
    			}else{
    	          $DB2WSCD = updDB2WSCD($db2con,$value['WSNAME'],$value['WSFRQ'],$value['WSODAY'],$value['WSWDAY'],$value['WSMDAY'],$value['WSTIME'],$wspkey,$nowYmd,$nowTime);
    	          //クエリー実行→エクセル作成→メール配信
                  if($DB2WSCD['result'] == true){
                        //ミィッミィッモー
        	            //$rs = exeQuery($db2con,$d1name,$nowYmd,$nowTime,$licenseCl,$wspkey);
                        //burstitemリスト取得
                        $burstItmLst = array();
                        $burstItmLst = getBurstItmLst($db2con,$d1name,$wspkey);
                          //error_log("akzakzakzakz mail burstItmLst1 ".print_R($burstItmLst,true));
                        if($burstItmLst['result'] == true){
                            $burstItmLst = $burstItmLst['data'];
                            if(count($burstItmLst) !== 0){
//                                for ($i=0;$i<count($burstItmLst);$i++){
                                foreach($burstItmLst as $key => $value){
                                    $mailAddLst = array();
                                    $mailAddLst = getMailAddlst($db2con,$burstItmLst[$key]);
                                       // error_log("akzakzakzakz mail burstItmLstburstItmLst ".print_R($burstItmLst[$key],true));
                                    if($mailAddLst['result']!== true){
                                        e_log(showMsg($rs['result'],array('exeMail.php','getMailAddlst')),'1');
                                    }else{
                                        $mailAddLst=$mailAddLst['data'];
                                        $rs = exeQuery($db2con,$d1name,$nowYmd,$nowTime,$licenseCl,$wspkey,$burstItmLst[$key],$mailAddLst);
                                    }
                                }
                            }else{
                               // error_log("akz mailaddlst arimasen");
                                   $rs = exeQuery($db2con,$d1name,$nowYmd,$nowTime,$licenseCl,$wspkey,array(),array());
                            }
                        }else{
                            e_log(showMsg($rs['result'],array('exeMail.php','getBurstItmLst')),'1');
                            $errMsg = showMsg('FAIL_FUNC',array('バースト情報の取得'));//"バースト情報の取得に失敗しました。";
                            fnCreateHistory($db2con,$value['WSNAME'],'','',$nowYmd,$nowTime,$wspkey,$errMsg);
                        }
                    }else{
                            e_log(showMsg($DB2WSCD['result'],array('exeMail.php','updDB2WSCD')),'1');
                            $errMsg = showMsg('FAIL_FUNC',array('実行日付の更新'));//" 実行日付の更新に失敗しました。";
                            fnCreateHistory($db2con,$value['WSNAME'],'','',$nowYmd,$nowTime,$wspkey,$errMsg);
                    }
    			}

            }

        }else{
            e_log(showMsg($rs['result'],array('exeMail.php','checkDB2WSCD')),'1');
            $errMsg = showMsg('FAIL_FUNC',array('送信日付のチェック'));//" 送信日付のチェックに失敗しました。";
            fnCreateHistory($db2con,$value['WSNAME'],'','',$nowYmd,$nowTime,$wspkey,$errMsg);
        }
    
    }

}
/*
*-------------------------------------------------------* 
* burstitemリスト取得
*ミィッミィッモー
*-------------------------------------------------------*
*/

/*
*-------------------------------------------------------* 
* burstitemリスト取得
*ミィッミィッモー
*-------------------------------------------------------*
*/

function getBurstItmLst($db2con,$d1name,$wspkey){
    $rtn = array();
    $data = array();

    $strSQL = ' SELECT ';
    $strSQL .= ' WANAME, ';
    $strSQL .= ' WAPKEY, ';
    $strSQL .= ' WABAFID, ';
    $strSQL .= ' WABAFLD, ';
    $strSQL .= ' WABAFR, ';
    $strSQL .= ' WABATO, ';
    $strSQL .= ' WABFLG, ';
    $strSQL .= ' WABINC, ';
    $strSQL .= ' WLSEQL, ';
    $strSQL .= ' WLDATA FROM DB2WAUT AS A ';
    $strSQL .= ' LEFT JOIN DB2WAUL AS B ';
    $strSQL .= ' ON A.WANAME = B.WLNAME AND ';
    $strSQL .= ' A.WAPKEY = B.WLPKEY AND ';
    $strSQL .= ' A.WAMAIL = B.WLMAIL ';
    $strSQL .= ' WHERE WANAME = ? ' ;
    $strSQL .= ' AND WASPND <> \'1\' ';
    $strSQL .= ' AND WAPKEY = ?  ';
    $strSQL .= ' GROUP BY WANAME,WAPKEY,WABAFID,WABAFLD,WABAFR,WABATO,WABFLG,WABINC,WLNAME,WLSEQL,WLDATA ';
    $params = array(
        $d1name,
        $wspkey
    );

    $stmt = db2_prepare($db2con,$strSQL);
      if($stmt === false){
            $rtn = array('result' => 'FAIL_SQL');
      }else{
        $rt = db2_execute($stmt,$params);
        if($rt === false){
            $rtn = array('result' => 'FAIL_SQL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
                $data = umEx($data);
                    $burstItmLstTmp = array();
                    $WANAME = '';
                    $WAPKEY = '';
                    $WABAFID = '';
                    $WABAFLD = '';
                    $WABFLG = '';
                    $WABAFR = '';
                    $WABATO = '';
                    $WABINC = 0;
                    $IDX = 0;
                    foreach($data as $key => $value){
                        if($value['WABFLG'] !== '1'){
                                 $index = $IDX++;
                                $burstItmLstTmp[$key] = $value;
                         }else{
                                if($WABINC === $value['WABINC']){
                                            if(isset($burstItmLstTmp[$index]['WLSEQL'])){
                                                array_push($burstItmLstTmp[$index]['WLSEQL'],$value['WLSEQL']);
                                            }
                                            if(isset($burstItmLstTmp[$index]['WLDATA'])){
                                                array_push($burstItmLstTmp[$index]['WLDATA'],$value['WLDATA']);
                                            }
                                            $WABINC   =  $value['WABINC'];
                                }else{
                                        $index = $IDX++;
                                        $burstItmLstTmp[] = array(
                                        'WANAME' =>$value['WANAME'],
                                        'WAPKEY' =>$value['WAPKEY'],
                                        'WABAFID' =>$value['WABAFID'],
                                        'WABAFLD' =>$value['WABAFLD'],
                                        'WABAFR' => $value['WABAFR'],
                                        'WABATO' => $value['WABATO'],
                                        'WABFLG' =>$value['WABFLG'],
                                        'WABINC' =>$value['WABINC'],
                                        'WLSEQL' =>array($value['WLSEQL']),
                                        'WLDATA' => array($value['WLDATA'])
                                    );
                                    $WANAME  =  $value['WANAME'];
                                    $WAPKEY   =  $value['WAPKEY'];
                                    $WABAFID  =$value['WABAFID'];
                                    $WABAFLD = $value['WABAFLD'];
                                    $WABFLG   =  $value['WABFLG'];
                                    $WABAFR = $value['WABAFR'];
                                    $WABINC   =  $value['WABINC'];
                                }
                           }
                    }
              $rtn = array('result' => true,'data' =>$burstItmLstTmp);
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* 宛先のリスト取得
*ミィッミィッモー
*-------------------------------------------------------*
*/
function getMailAddlst($db2con,$burstItmlst){

    if($burstItmlst!== ''){
            $waname =  $burstItmlst['WANAME'];
            $wapkey =  $burstItmlst['WAPKEY'];
            $wabafid =  $burstItmlst['WABAFID'];
            $wabafld =  $burstItmlst['WABAFLD'];
            $wabafr =  $burstItmlst['WABAFR'];
            $wabato =  $burstItmlst['WABATO'];
            $wabflg = $burstItmlst['WABFLG'];
            $wabinc = $burstItmlst['WABINC'];
            $wlseql = $burstItmlst['WLSEQL'];
            $wldata = $burstItmlst['WLDATA'];
    }
    $params = array(
        $waname,
        $wapkey,
        $wabafid,
        $wabafld,
        $wabafr,
        $wabato
    );

    $rtn = array();
    $data = array();
    $strSQL  = ' SELECT ';
    $strSQL .=  getDb2wautColumns();
    $strSQL .= ' FROM DB2WAUT AS A ';
    $strSQL .= ' WHERE WANAME = ? ' ;
    $strSQL .= ' AND WASPND <> \'1\' ';
    $strSQL .= ' AND WAPKEY = ?  ';
    $strSQL .= ' AND WABAFID = ? ';
    $strSQL .= ' AND WABAFLD = ? ';
    $strSQL .= ' AND WABAFR = ? ';
    $strSQL .= ' AND WABATO = ? ';
     if($wabflg === '0'){
        $strSQl .= ' AND WABFLG = \'0\' ';
        $strSQL .= 'AND  WABINC = ? ';
        array_push($params,$wabinc);
    }else if($wabflg === '1'){
            $strSQL .= ' AND WABFLG = \'1\'  ';
            $strSQL .= ' AND WABINC = ? ';
            array_push($params,$wabinc);
    }else{
            $strSQL .= ' AND WABFLG = \'\' ';
     }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rtn = array('result' => 'FAIL_SQL');
    }else{
        $rt = db2_execute($stmt,$params);
        if($rt === false){
               $rtn = array('result' => 'FAIL_SQL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $rtn = array('result' => true,'data' => $data);
        }
    }
    return $rtn;
}
/*
*-------------------------------------------------------* 
* クエリー実行
*-------------------------------------------------------*
*/
function exeQuery($db2con,$d1name,$nowYmd,$nowTime,$licenseCl,$wspkey,$burstItmLst,$mailAddLst){
    $rtn      = true;
    $rs;
    $csv_h    = array();
    $csv_d    = array();
    $define   = array();
    $fdb2csv2 = array();
    $csvFlg   = '0';    //定義に対して一人でもCSV配信にチェックを入れていたら1
    $xlsFlg   = '0';    //定義に対して一人でもXLS配信にチェックを入れていたら1
    $htmlFlg  = '0';    //定義に対して一人でもHTML配信にチェックを入れていたら1
    $password = '';
    $WEBF = '';
    $DIRE = '';
    $DGBFLG = false;
    $DGAFLG = false;
    $RTCD = ' ';

    //ウェブフラグ
    $reWebf = fnGetFDB2CSV1($db2con,$d1name);
    if($reWebf['result'] !== true){
       // e_log(showMsg($rsmaster['result']),'1');
        $rtn = 1;
    }else{
        $WEBF = $reWebf['data'][0]['D1WEBF'];
        $DIRE = $reWebf['data'][0]['D1DIRE'];
    }
    //集計するフィールド情報取得
    $rsmaster = cmGetDB2COLM($db2con,$d1name);
    if($rsmaster['result'] !== true){
       // e_log(showMsg($rsmaster['result']),'1');
        $rtn = 1;
    }else{
        $rsMasterArr = $rsmaster['data'];

    }
    //集計するメソッド情報取得
    $rsshukei = cmGetDB2COLTTYPE($db2con,$d1name);
    if($rsshukei['result'] !== true){
        $msg = showMsg($rsshukei['result']);
        $rtn = 1;
    }else{
        $rsshukeiArr = $rsshukei['data'];
    } 
    if(count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0){
        $rst = cmLevelFieldCheck($db2con,$d1name,$rsMasterArr,$rsshukeiArr);
        if($rst['result'] !== true){
            e_log(showMsg($rst['result'],array('クエリー','カラム')),'1');
            //クエリー　＊showMsg($rst['result'],array('クエリー','カラム'))
            $errMsg = showMsg($rst['result'],array('クエリー','カラム'));
            fnCreateHistory($db2con,$d1name,'','',$nowYmd,$nowTime,$wspkey,$errMsg);
            $rtn = 1;
        }
    }
    if($WEBF === '1'){
        if($rtn !== 1){
            $rs = fnBQRYCNDDAT($db2con,$d1name);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
            }
            $data = umEx($data);
            if(count($data) > 0){
                $cnddatArr = fnCreateCndData($data);
            }
        }
        if($rtn !== 1){
            $resExeSql = runExecuteSQL($db2con,$d1name);
            if($resExeSql['RTN'] !== 0){
                $rtn = 1;
            }else{
                $qryData = $resExeSql;
            }
        }
        if($rtn === 1){
            //クエリー　＊クエリーの実行準備に失敗しました
            $errMsg = showMsg('FAIL_FUNC',array('クエリーの実行準備'));//"クエリーの実行準備に失敗しました。";
            fnCreateHistory($db2con,$d1name,'','',$nowYmd,$nowTime,$wspkey,$errMsg);
        }
    }
    if($rtn !== 1){
        $rsCLChk = chkFDB2CSV1PG($db2con,$d1name);
        if($rsCLChk['result'] !== true){
            $rtn = 1;
            $msg = $rsCLChk['result'];
            //クエリー　＊CL連携情報の取得に失敗しました。
            $errMsg = showMsg('FAIL_FUNC',array('CL連携情報の取得'));//"CL連携情報の取得に失敗しました。";
            fnCreateHistory($db2con,$d1name,'','',$nowYmd,$nowTime,$wspkey,$errMsg);
        }else{
            if(count($rsCLChk['data']) > 0){
                $fdb2csv1pg = $rsCLChk['data'][0];
                if($fdb2csv1pg['DGBLIB'] !== '' || $fdb2csv1pg['DGBPGM'] !== ''){
                    $DGBFLG = true;
                }
                if($fdb2csv1pg['DGALIB'] !== '' || $fdb2csv1pg['DGAPGM'] !== ''){
                    $DGAFLG = true;
                }
            }
        }
    }
    if($rtn !== 1){
        if($WEBF === '1'){
            $db2LibCon = cmDb2ConLib($qryData['LIBLIST']);
        }
    /******************************************************/
        //【Int】
        if($WEBF !== '1'){
            cmInt($db2con,$RTCD,$JSEQ,$d1name);
        }else{
           if($licenseCl === true){
                if($DGBFLG === true || $DGAFLG === true){
                    cmIntWC($db2LibCon,$RTCD,'INT',$d1name);
                }
            }
        }
        if($RTCD == ' '){
            // 【setCmd】
            if($WEBF !== '1'){
                if($licenseCl === true){
                    if($DGBFLG === true || $DGAFLG === true){
                        setCmd($db2con,$d1name,$WEBF);
                    }
                }
            }else{
                if($licenseCl === true){
                    if($DGBFLG === true || $DGAFLG === true){
                        setCmd($db2LibCon,$d1name,$WEBF,$cnddatArr);
                    }
                    if($DGBFLG === true){
                        cmIntWC($db2LibCon,$RTCD,'BEF',$d1name);
                    }
                }
            }
            // 【cmDoのRTCDが' 'の場合、WEBの場合は実行後CL】
            if($RTCD == ' '){
                $dbname = makeRandStr(10);
                if($WEBF !== '1'){
                    cmDo($db2con,$RTCD,$JSEQ,$dbname,$d1name);
                }else{
                    $resExec = execQry($db2LibCon,$qryData['STREXECTESTSQL'],$qryData['EXECPARAM'],$qryData['LIBLIST']);
                    if($resExec['RTN'] !== 0){
                        $rtn = 1;
                        $msg = showMsg($resExec['MSG']);
                        //クエリー　＊クエリーの実行チェックに失敗しました
                        $errMsg = showMsg('FAIL_FUNC',array('クエリーの実行チェック'));//"クエリーの実行チェックに失敗しました。";
                        fnCreateHistory($db2con,$d1name,'','',$nowYmd,$nowTime,$wspkey,$errMsg);
                    }else{
                        $resCreateTbl = createTmpTable($db2LibCon,$qryData['LIBLIST'],$qryData['STREXECSQL'],$qryData['EXECPARAM'],$dbname);
                        if($resCreateTbl['RTN'] !== 0){
                            $rtn = 1;
                            $msg = showMsg($resCreateTbl['MSG']);
                            //クエリー　＊クエリーの実行に失敗しました
                            $errMsg = showMsg('FAIL_FUNC',array('クエリーの実行'));//"クエリーの実行に失敗しました。";
                            fnCreateHistory($db2con,$d1name,'','',$nowYmd,$nowTime,$wspkey,$errMsg);
                        }else{
                            if($licenseCl === true){
                                if($DGGFLG === true){
                                   cmIntWC($db2LibCon,$RTCD,'AFT',$d1name);
                                }
                            }
                            if($RTCD !== ' '){
                                //クエリー　＊CLの事後処理に失敗しました。
                                $errMsg = showMsg('FAIL_FUNC',array('CLの事後処理'));//"CLの事後処理に失敗しました。";
                                fnCreateHistory($db2con,$d1name,'','',$nowYmd,$nowTime,$wspkey,$errMsg);
                            }else{
                                    if($DIRE !== ''){
                                        createCSVFile($db2con,$d1name,$dbname,$DIRE);
                                    }
                            }
                        }
                    }
                    if($rtn !== 0){
                        e_log(showMsg($msg,array('exeMail.php','クエリ実行失敗')),'1');
                    }else{
                        $RTCD = ' ';
                    }
                }
                cmInsertDB2WLOG($db2con,$logrs,'SCHEDULE','D',$d1name,array(),'','');
                if($RTCD === ' ' ){                    
                    //file名作成
                    $filename = cmMer($d1name).'_'.$nowYmd.'_'.$nowTime;
                    //実行履歴(空)を作成
                    fnCreateHistory($db2con,$d1name,$filename,$dbname,$nowYmd,$nowTime,$wspkey,'');
                    //FDB2CSV2取得
                    $fdb2csv2 = cmGetFDB2CSV2($db2con,$d1name,true,false,false);
                    $fdb2csv2 = umEx($fdb2csv2['data']);
                    $fdb2csv2_ALL = cmGetFDB2CSV2($db2con,$d1name,false,false,false);
                    $fdb2csv2_ALL = umEx($fdb2csv2_ALL['data']);

                    //ヘッダー情報取得
                    $csv_h = cmCreateHeaderArray($fdb2csv2);
                    //クエリー結果全件取得
                    //$csv_d = cmGetDbFile($db2con,$fdb2csv2,$dbname,'','','','','','',array(),true);

                    //if($wspkey === ''){
                    if(cmMer($burstItmLst['WABAFLD']) !== ''){
                        $qryCol = cmGetFDB2CSV2($db2con,$d1name,false,false,false);
                        if($qryCol['result'] !== true){
                            e_log(showMsg($qryCol,array('exeMail.php','fnGetFDB2CSV2')),'1');
                        }else{
                            $qryColData = array();
                            $qryCol = umEx($qryCol['data']);
                            foreach($qryCol as $value){
                                array_push($qryColData,$value['D2FLD']);
                            }
                        }
                        if(!in_array($burstItmLst['WABAFLD'],$qryColData)){
                            $burstItmLst = '';
                        }
                    }
                    $csv_d = cmGetDbFileBurstItm($db2con,$fdb2csv2,$burstItmLst,$dbname,'','','','','','',array(),true);
                    $DB2WSOC = chkDB2WSOC($db2con,$d1name,$wspkey);
                    if($DB2WSOC['result'] === true){ 
                        $mailFlg = '1';
                        /**0件データーを出すことをチェック**/
                        $csv_d_count = count($csv_d);
                        if($csv_d_count === 0 && $DB2WSOC['flg'] === '1'){
                            $mailFlg = '0'; // 0件データーメール配信することができません。
                        }
                        if($mailFlg !== '0'){
                            //CSV配信チェック
                            $csvFlg = chkDB2WAUTCsv($db2con,$d1name);
                            if(count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0){
                                $tabledata = fnDataShukei($db2con,$fdb2csv2,$fdb2csv2_ALL,$d1name,$dbname,$db2wcol,$rsMasterArr,$rsshukeiArr,$burstItmLst);
                                $csv_h_shukei = $csv_h;
                                array_unshift($csv_h_shukei,"","");
                                $fdb2csv2_shukei = $fdb2csv2;
                                $fdb2csv2_shukei = cmArrayUnshift($fdb2csv2_shukei);
                            }
                            if($csvFlg['result'] === true){
                                //取得データからCSV作成
                                if($csvFlg['flg'] === '1' && $rtn === true){
                                    if(count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0){
                                       exeCsv($csv_h_shukei,$tabledata,$fdb2csv2_shukei,$filename,cmMer($d1name));
                                    }
                                    else{
                                       exeCsv($csv_h,$csv_d,$fdb2csv2,$filename,cmMer($d1name));
                                    }
                                }
                            }else{
                                e_log(showMsg($csvFlg['result'],array('exeMail.php','chkDB2WAUTCsv')),'1');
                                //クエリー　＊CSV配信情報の取得に失敗しました。
                                $errMsg = showMsg('FAIL_FUNC',array('CSV配信情報の取得'));//" CSV配信情報の取得に失敗しました。";
                                 fnCreateHistory($db2con,$d1name,$filename,$dbname,$nowYmd,$nowTime,$wspkey,$errMsg);

                            }
                            //XLS配信チェック
                            $xlsFlg = chkDB2WAUTXls($db2con,$d1name,$wspkey);
                            if($xlsFlg['result'] === true){
                                //取得データからExcel作成
                                if($xlsFlg['flg'] === '1' && $rtn === true){
                                    if(count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0){
                                        exeExcel($csv_h_shukei,$tabledata,$fdb2csv2_shukei,$filename,$wspkey,$d1name,$db2con,$dbname,array());                                          
                                    }
                                    else{
                                        exeExcel($csv_h,$csv_d,$fdb2csv2,$filename,$wspkey,$d1name,$db2con,$dbname,$burstItmLst);
                                    }
                                }
                            }else{
                                e_log(showMsg($xlsFlg['result'],array('exeMail.php','chkDB2WAUTXls')),'1');
                                //クエリー　＊EXCEL配信情報の取得に失敗しました。
                                  $errMsg = showMsg('FAIL_FUNC',array('EXCEL配信情報の取得'));//" EXCEL配信情報の取得に失敗しました。";
                                  fnCreateHistory($db2con,$d1name,$filename,$dbname,$nowYmd,$nowTime,$wspkey,$errMsg);
                            }

                            //HTML配信チェック
                            $htmlFlg = chkDB2WAUTHtml($db2con,$d1name,$wspkey);
                            if($htmlFlg['result'] === true){
                                //取得データからExcel作成
                                if($htmlFlg['flg'] === '1' && $rtn === true){
                                    if(count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0){
                                        exeHtml($csv_h_shukei,$tabledata,$fdb2csv2_shukei,$filename,$wspkey,$d1name,$db2con,$dbname,array());
                                    }else{
                                        exeHtml($csv_h,$csv_d,$fdb2csv2,$filename,$wspkey,$d1name,$db2con,$dbname,$burstItmLst);
                                    }
                                }
                            }else{
                                e_log(showMsg($htmlFlg['result'],array('exeMail.php','chkDB2WAUTHtml')),'1');
                                //クエリー　＊HTML配信情報の取得に失敗しました。
                                  $errMsg = showMsg('FAIL_FUNC',array('HTML配信情報の取得'));//" HTML配信情報の取得に失敗しました。";
                                  fnCreateHistory($db2con,$d1name,$filename,$dbname,$nowYmd,$nowTime,$wspkey,$errMsg);
                            }

                            //HTML配信チェック
                            $mailHtmlFlg = chkDB2WATYPEHtml($db2con,$d1name);
                            if($mailHtmlFlg['result'] === true){
                                //正徳データーからHTML作成
                                if($mailHtmlFlg['flg'] === '1' && $rtn === true){
                                    if(cmMer($wspkey) !== ''){
                                        if($csv_d_count === 0){
                                            $tddata = _fputpivot_ZeroKen($db2con,$d1name,$filename,$wspkey,$dbname,$burstItmLst,true,'htmlcreate');
                                        }else{
                                            $tbdata = exePivotExcel($db2con,$d1name,$filename,$wspkey,$dbname,$burstItmLst,true,'htmlcreate');
                                        }
                                    }else{
                                        if(count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0){
                                            $tbdata = exeHtmlMail($csv_h_shukei,$tabledata,$fdb2csv2_shukei,$filename);
                                        }
                                        else{
                                            $tbdata = exeHtmlMail($csv_h,$csv_d,$fdb2csv2,$filename);
                                        }
                                    }
                                }
                            }else{
                                e_log(showMsg($mailHtmlFlg['result'],array('exeMail.php','chkDB2WATYPEHtml')),'1');
                                //クエリー　＊メール形式情報の取得に失敗しました。
                                $errMsg = showMsg('FAIL_FUNC',array('メール形式情報の取得'));//"メール形式情報の取得に失敗しました。";
                                fnCreateHistory($db2con,$d1name,'','',$nowYmd,$nowTime,$wspkey,$errMsg);
                            }
                            if($csvFlg['result'] === true && $xlsFlg['result'] === true && $htmlFlg['result'] === true && $mailHtmlFlg['result'] === true ){
                                //パスワード生成
                                if($csvFlg['flg'] === '1' || $xlsFlg['flg'] === '1' || $htmlFlg['flg'] === '1'){
                                    $password = MakeRandStr(16);
                                }
                                //ファイルをZip化
                                exeZip($d1name,$filename,$password,$csvFlg['flg'],$xlsFlg['flg'],$htmlFlg['flg']);
                            }
                        }
                        //メール送信
                        if($csvFlg['result'] === true && $xlsFlg['result'] === true && $mailHtmlFlg['result'] === true ){
                            //exeMail($db2con,$d1name,$filename,$dbname,$nowYmd,$nowTime,$password,$csvFlg['flg'],$xlsFlg['flg'],$tbdata,$wspkey,$mailFlg);
                            exeMail($db2con,$d1name,$mailAddLst,$filename,$dbname,$nowYmd,$nowTime,$password,$csvFlg['flg'],$xlsFlg['flg'],$htmlFlg['flg'],$tbdata,$wspkey,$mailFlg);
                        }

                    }else{
                        e_log(showMsg($DB2WSOC['result'],array('exeMail.php','chkDB2WSOC')),'1');
                    }
                }else{
                    $rtn = 1;
                }
            }else{
                //クエリー　＊CLの事前実行に失敗しました。
                  $errMsg = showMsg('FAIL_FUNC',array('CLの事前実行'));//"CLの事前実行に失敗しました。";
                  fnCreateHistory($db2con,$d1name,'','',$nowYmd,$nowTime,$wspkey,$errMsg);
           }
        }else{
            //クエリー　＊クエリーの事前処理に失敗しました。
              $errMsg = showMsg('FAIL_FUNC',array('クエリーの事前処理に失敗しました。'));//"クエリーの事前処理に失敗しました。";
              fnCreateHistory($db2con,$d1name,'','',$nowYmd,$nowTime,$wspkey,$errMsg);
        }
    
    /******************************************************/
    }

    return $rtn;
}

/*
*-------------------------------------------------------* 
* エクセル作成　保存
*-------------------------------------------------------*
*/
function exeExcel($csv_h,$csv_d,$fdb2csv2,$filename,$wspkey,$d1name,$db2con,$dbname,$burstItmLst){

    $wspkey = cmMer($wspkey);
    if($wspkey !== ''){

            $csv_d = umEx($csv_d,true);
            $csv_h = cmHsc($csv_h);
            if(count($csv_d) === 0){
                _fputpivot_ZeroKen($db2con,$d1name,$filename,$wspkey,$dbname,$burstItmLst,false,'');
            }else{
                exePivotExcel($db2con,$d1name,$filename,$wspkey,$dbname,$burstItmLst,false,'');
            }
    }else{
            $D1TMPF = '';
            $ext = getExt($d1name);

        $DB2WCOL = array();
        if(EXCELVERSION == '2003' || $ext === 'xls'){

            $csv_d = umEx($csv_d);

            $excelname = $filename.'.xls';
            $book = cmCreateXLS($csv_h,$csv_d,$fdb2csv2,$excelname,$DB2WCOL,$d1name);
            $writer = PHPExcel_IOFactory::createWriter($book, "Excel5");
            $writer->save(BASE_DIR.'/php/xls/'.$excelname);
            $book = null;
        }else if(EXCELVERSION == '2007' || $ext === 'xlsx' ){
            $csv_d = umEx($csv_d);
            $excelname = $filename.'.xlsx';
            $book = cmCreateXLS($csv_h,$csv_d,$fdb2csv2,$excelname,$DB2WCOL,$d1name);
            $writer = PHPExcel_IOFactory::createWriter($book, "Excel2007");
            $writer->save(BASE_DIR.'/php/xls/'.$excelname);
            $book = null;
        }else if(EXCELVERSION == 'html'  || $ext ===''){

            $csv_d = umEx($csv_d,true);
            $csv_h = cmHsc($csv_h);
            $excelname = $filename.'.xls';

            if(!file_exists(BASE_DIR.'/php/xls/'.$excelname)){
                touch(BASE_DIR.'/php/xls/'.$excelname);
            }

            $fp = fopen(BASE_DIR.'/php/xls/'.$excelname,'w');

            _fputexcel($fp,$csv_h,$csv_d,$fdb2csv2,array(),false);

            fclose($fp);
        }

    }

}
    function getExt($d1name){
            $ext = '';
            $fdb2csv1 = FDB2CSV1_DATA($d1name);
            if($fdb2csv1['result'] === true){
                 if($fdb2csv1['data'][0]['D1TMPF'] !== ''){
                        $D1TMPF = $fdb2csv1['data'][0]['D1TMPF'];
                            $ext = explode ('.', $D1TMPF);
                            $ext = $ext [count ($ext) - 1];
                  }
            }
    return $ext;
    }
/*
*-------------------------------------------------------* 
* HTML作成　保存
*-------------------------------------------------------*
*/
function exeHtml($csv_h,$csv_d,$fdb2csv2,$filename,$wspkey,$d1name,$db2con,$dbname,$burstItmLst){

    $csv_d = umEx($csv_d,true);
    $csv_h = cmHsc($csv_h);
    $wspkey = cmMer($wspkey);
    if($wspkey !== ''){
        if(count($csv_d) === 0){
            _fputpivot_ZeroKen($db2con,$d1name,$filename,$wspkey,$dbname,$burstItmLst,true,'');
        }else{
           exePivotExcel($db2con,$d1name,$filename,$wspkey,$dbname,$burstItmLst,true,'');
        }
    }else{

        $excelname = $filename.'.html';

        if(!file_exists(BASE_DIR.'/php/html/'.$excelname)){
            touch(BASE_DIR.'/php/html/'.$excelname);
        }

        $fp = fopen(BASE_DIR.'/php/html/'.$excelname,'w');

        _fputexcel($fp,$csv_h,$csv_d,$fdb2csv2,array(),true);

        fclose($fp);

    }

}

/*
*-------------------------------------------------------* 
*   ピッボとのエクセル作成　保存
*-------------------------------------------------------*
*/
function exePivotExcel($db2con,$d1name,$filename,$wspkey,$dbname,$burstItmLst,$htmlFlg,$htmlcreate){

    $WEBF = '';

    $reWebf = fnGetFDB2CSV1($db2con,$d1name);
    if($reWebf['result'] !== true){
       // e_log(showMsg($rsmaster['result']),'1');
        $rtn = 1;
    }else{
        $WEBF = $reWebf['data'][0]['D1WEBF'];
    }

    include_once("PivotClass.php");
    $flg = '2';
    $rowstart = '';
    $rowend = '';
    $searchdata = array();
    $chkcoldata = array();
    $search_col_arr = array();
    $operator = array();
    $SearchCol = '';
    $pivotCol = array();
    $colData = array();
    $xchecked = array();
    $ychecked = array();
    $cchecked = array();
    $xcheckedcount = 0;
    $WEBF = '';

    //ウェブフラグ
    $reWebf = fnGetFDB2CSV1($db2con,$d1name);
    if($reWebf['result'] !== true){
      //  e_log(showMsg($rsmaster['result']),'1');
        $rtn = 1;
    }else{
        $WEBF = $reWebf['data'][0]['D1WEBF'];
    }

    if($burstItmLst != ''){
        if(cmMer($burstItmLst['WABAFLD']) !== ''){
            $colData = fnGetDB2PCOL($db2con,$d1name,$wspkey);
            if($colData['result'] !== true){

            }else{
                $colData = umEx($colData['data']);
                foreach($colData as $value){
                    $col = $value['WPFLD'].'_'.$value['WPFILID'];
                    array_push($pivotCol,$col);
                }
            }
            $SearchCol = cmMer($burstItmLst['WABAFLD']).'_'.cmMer($burstItmLst['WABAFID']);
            if(in_array($SearchCol,$pivotCol)){
                        if(cmMer($burstItmLst['WABFLG']) === '0'){
                                    if(cmMer($burstItmLst['WABAFR']) !== '' && cmMer($burstItmLst['WABATO'])!== ''){
                                        $chkcoldata = array("1" => 1, "2" => 2);
                                        $searchdata = array("1" => cmMer($burstItmLst['WABAFR']), "2" => cmMer($burstItmLst['WABATO']));
                                        $search_col_arr = array("1" => $SearchCol , "2" => $SearchCol);
                                        $operator = array("1"=>'>=','2'=>'<=');
                                    }else if($burstItmLst['WABAFR'] !== ''){
                                        $chkcoldata = array("1" => 1);
                                        $searchdata = array("1" => cmMer($burstItmLst['WABAFR']));
                                        $search_col_arr = array("1" => $SearchCol);
                                        $operator = array("1"=>'=' );
                                    }
                        }else if(cmMer($burstItmLst['WABFLG']) === '1'){
                                $WLDATA = $burstItmLst['WLDATA'];
                                $idx = 1;
                                $operator['FLG'] = ' OR ';
                                foreach($WLDATA as $value){
                                    $i =  $idx++;
                                    $chkcoldata[$i]=$i;
                                    $searchdata[$i]=$value;
                                    $search_col_arr[$i]=$SearchCol;
                                    $operator[$i]= '=';
                                }
                        }
            }
        }
    }
    $db2pmst_d = fnGetDB2PMST($db2con,$d1name,$wspkey);
    if($db2pmst_d['result'] !== true){
    }else{
        $db2pmst = umEx($db2pmst_d['data']);
    }
    $db2pcol_d = fnGetDB2PCOL($db2con,$d1name,$wspkey);
    if($db2pcol_d['result'] !== true){
    }else{
        $db2pcol = umEx($db2pcol_d['data']);
        $columndata = array();
        $columnupdata = array();
        $calcdata = array();
        foreach($db2pcol as $value){
                $wppflg  = $value['WPPFLG'];
                $wpseqn  = $value['WPSEQN'];
                $wpfilid = $value['WPFILID'];
                $column  = $value['WPFLD'].'_'.$value['WPFILID'];
                $wpfhide = $value['WPFHIDE'];
                $d2len   = $value['D2LEN'];
                $d2dec   = $value['D2DEC'];
                $d2type  = $value['D2TYPE'];

                switch($wppflg){
                    case '1':
                        $columndata[$wpseqn] = $column;
                        $xchecked[$column] = array(
                            'FILID'=> $wpfilid,
                            'HIDE' => $wpfhide,
                            'LEN'  => $d2len,
                            'DEC'  => $d2dec,
                            'TYPE' => $d2type
                        );
                        if($wpfhide === ''){
                            $xcheckedcount++;
                        }
                        break;
                    case '2':
                        $columnupdata[$wpseqn] = $column;
                        //表示非表示配列格納
                        $ychecked[] = array(
                            'FILID'=> $wpfilid,
                            'HIDE' => $wpfhide,
                            'LEN'  => $d2len,
                            'DEC'  => $d2dec,
                            'TYPE' => $d2type
                        );
                        break;
                    case '3':
                        $calcdata[$wpseqn] = $column;
                        //表示非表示配列格納
                        $cchecked[$column] = array(
                            'FILID'=> $wpfilid,
                            'HIDE' => $wpfhide,
                            'LEN'  => $d2len,
                            'DEC'  => $d2dec,
                            'TYPE' => $d2type
                        );
                        break;
                }
        }
    }

    $db2pcal_d = fnGetDB2PCAL($db2con,$d1name,$wspkey);
     if($db2pcal_d['result'] !== true){
    }else{
                $db2pcal = umEx($db2pcal_d['data']);
                 $num_formula = array();
                if(count($db2pcal) > 0){

                    for($i=0;$i<10;$i++){
                        if(isset($db2pcal[$i])){
                            $num_formula[] = $db2pcal[$i]['WCCALC'];
                        }else{
                            $num_formula[] = "";
                        }
                    }
                }else{
                    $num_formula = array("","","","","");
                }
            $getcheadingcol = array();
            $data_studio  = array();
            $rtncolumn = array();
            $colslists  = array();
            $gb_count = 0;
                $pgstart = '';
                $pgend = '';
          //  for($i = 0 ; $i < 4 ;$i++){
               do{
                        $pivot = new Pivot($db2con,SAVE_DB,$dbname,$d1name);
                       // $pivot -> set_globalcount($gb_count);
                         $pivot->crossData($flg,$searchdata,$chkcoldata,$search_col_arr,$operator,$columndata,$columnupdata,$calcdata,$num_formula,$rowstart,$rowend,$pgstart,$pgend);
                        $file_name = $filename;
                        $columnupdata = $pivot->get_columnX();
                        $rowstart = $pivot->get_rowstart();
                        $rowend = $pivot->get_rowend();
                        $endcount = $pivot->get_endcount();
                        if(count($columnupdata)>0){
                            $getcheadingcol = array_merge($getcheadingcol,$pivot->get_getcheadingcol());
                        }else{
                            $getcheadingcol = $pivot->get_getcheadingcol();
                        }
                        $columndata = $pivot->get_columnY();
                        if(count($colslists)>0){
                            $colslists = array_merge($colslists,$pivot->get_colslists());
                        }else{
                            $colslists = $pivot->get_colslists();
                        }
                        $countcalc = $pivot->get_countcalc();
                        $num_count = $pivot->get_num_count();
                        $rtncolumn = array_merge($rtncolumn,$pivot->get_rtncolumn());
    //                         $data_studio = array_merge($data_studio,$pivot->get_data_studio());
                            if(count($data_studio)>0){
                                foreach($pivot->get_data_studio() as $key =>$value){
                                    foreach($value as $k =>$v){
                                        $data_studio[$key][$k] = $v;
                                    }
                                }
                            }else{
                                    $data_studio = $pivot->get_data_studio();
                            }
                        $gb_count  = count($data_studio);
                        $flg = '3';
                  }while($endcount !== $rowend && count($columnupdata)>0);
                if($htmlcreate !== ''){
                    $pivot_html = _fputpivot_html($db2con,$fp,$getcheadingcol,$columnupdata,$columndata,$colslists,$countcalc,$num_count,$rtncolumn,$data_studio,$htmlFlg,$xchecked,$ychecked,$cchecked,$xcheckedcount,$WEBF);
                    return $pivot_html;
                }else{
                        if($htmlFlg === true){

                            $excelname = $file_name.'.html';
                            if(!file_exists(BASE_DIR.'/php/html/'.$excelname)){
                                touch(BASE_DIR.'/php/html/'.$excelname);
                            }
                            $fp = fopen(BASE_DIR.'/php/html/'.$excelname,'w');

                        }else{

                            $excelname = $file_name.'.xls';
                            if(!file_exists(BASE_DIR.'/php/xls/'.$excelname)){
                                touch(BASE_DIR.'/php/xls/'.$excelname);
                            }
                            $fp = fopen(BASE_DIR.'/php/xls/'.$excelname,'w');

                        }
                        
                    _fputpivot($fp,$getcheadingcol,$columnupdata,$columndata,$colslists,$countcalc,$num_count,$rtncolumn,$data_studio,$htmlFlg,$xchecked,$ychecked,$cchecked,$xcheckedcount,$WEBF);
                }
                fclose($fp);
      }
}

function _fputpivot_ZeroKen($db2con,$d1name,$filename,$wspkey,$dbname,$burstItmLst,$htmlFlg,$htmlcreate){
    if($htmlFlg === true){
        $excelname = $filename.'.html';
        if(!file_exists(BASE_DIR.'/php/html/'.$excelname)){
            touch(BASE_DIR.'/php/html/'.$excelname);
        }
    }else{
        $ext = getExt($d1name);
        if($ext === ''){
            $ext = 'xls';
        }
        $excelname = $filename.$ext;
        if(!file_exists(BASE_DIR.'/php/xls/'.$excelname)){
            touch(BASE_DIR.'/php/xls/'.$excelname);
        }
    }

}
function _fputpivot_html($db2con,$fp,$getcheadingcol,$columnupdata,$columndata,$colslists,$countcalc,$num_count,$rtncolumn,$data_studio,$htmlFlg,$xchecked,$ychecked,$cchecked,$xcheckedcount,$d1webf){
    $colslists = umEx($colslists,true);

    $head_color = '';
    if(isset($_SESSION['PHPQUERY'])){
        if($_SESSION['PHPQUERY']['LOGIN'] === '1'){
            cmSetPHPQUERY($db2con);

            $DB2WUSR = cmGetDB2WUSR($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);

            $DB2WUSR = umEx($DB2WUSR);

            $wuclr3 = $DB2WUSR[0]['WUCLR3'];
            $wuclr3 = (($wuclr3 === '')?COLOR3:$wuclr3);

            switch($wuclr3){
                case 'red':
                    $head_color = 'background-color:#b60f0b;color:#ffffff;';
                    break;
                case 'blue':
                    $head_color = 'background-color:#005879;color:#ffffff;';
                    break;
                case 'orange':
                    $head_color = 'background-color:#a66505;color:#ffffff;';
                    break;
                case 'purple':
                    $head_color = 'background-color:#541fa7;color:#ffffff;';
                    break;
                case 'green':
                    $head_color = 'background-color:#246534;color:#ffffff;';
                    break;
                case 'fb':
                    $head_color = 'background-color:#192441;color:#ffffff;';
                    break;
                case 'muted':
                    $head_color = 'background-color:#595959;color:#ffffff;';
                    break;
                case 'dark':
                    $head_color = 'background-color:black;color:#ffffff;';
                    break;
                case 'pink':
                    $head_color = 'background-color:#6b2345;color:#ffffff;';
                    break;
                case 'brown':
                    $head_color = 'background-color:#4f2a1b;color:#ffffff;';
                    break;
                case 'sea-blue':
                    $head_color = 'background-color:#013760;color:#ffffff;';
                    break;
                case 'banana':
                    $head_color = 'background-color:#cb9704;color:#ffffff;';
                    break;
                default:
                    $head_color = 'background-color:#f5f5f5;color:#666;';
                    break;
            }

        }
    }

    if($head_color === ''){
        $head_color = 'background-color:rgb(79,129,189);color:#ffffff;';
    }
    $htmlcreate = '';
    $htmlcreate .=  '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> ';
    $htmlcreate .=  '<html><head> ';

    if($htmlFlg === true){
        $htmlcreate .= '<meta charset="UTF-8"> ';
    }

    $htmlcreate .=  '<meta http-equiv="Content-Type" content="application/vnd.ms-excel; " /> ';
    $htmlcreate .=  '<meta http-equiv="Content-Disposition" content="attachment; filename=output.xls" /> ';
    $htmlcreate .=  '<style type="text/css"> ';
    $htmlcreate .=  '<!-- .txt{mso-number-format:"\@";}--> ';
    $htmlcreate .=  '</style> ';
    $htmlcreate .=  '</head><body> ';
    $htmlcreate .=  '<table style="font-size: 13px;border-collapse: collapse;"> ';
    $htmlcreate .=  '<thead> ';
    $tdthstyle = "border: 1px solid #ccc;white-space: nowrap;padding: 5px 10px 4px 10px;";
    for($col = 0 ; $col < count($columnupdata) ;$col++){

        $htmlcreate_tmp = '';
        
        $htmlcreate_tmp .=  '<tr style="'.$head_color.'"> ';
        $htmlcreate_tmp .= '<th colspan="'.$xcheckedcount.'" style="text-align:right;border-bottom: none;'.$tdthstyle.'">'.$getcheadingcol[$col].'</th> ';
        $colspancount = 1;
        for($i = 0;$i<count($colslists);$i++){

            $headColData = $colslists[$i][$columnupdata[$col]];

            if($d1webf === '1'){
                if($ychecked[$col]['TYPE'] === 'S' || $ychecked[$col]['TYPE'] === 'P' || $ychecked[$col]['TYPE'] === 'B'){
                    $headColData = floatFormat($headColData,$ychecked[$col]['DEC'],2);
                }
            }else{

                if($ychecked[$col]['FILID'] === '9999'){
                    if($ychecked[$col]['TYPE'] === 'S' || $ychecked[$col]['TYPE'] === 'P' || $ychecked[$col]['TYPE'] === 'B'){
                        $headColData = floatFormat($headColData,$ychecked[$col]['DEC'],2);
                    }
                }

            }

            if($i+1 !== count($colslists)){

                //2行目以降の場合、親行が変更されたかどうかをチェック
                $parentChange = false;
                if($col > 0){
                    if($colslists[$i]['colspanEndFlg'] === true){
                        $parentChange = true;
                    }
                }

                if($headColData !== $colslists[$i+1][$columnupdata[$col]] || ($parentChange === true)){
                    $countddd = $colspancount *($countcalc + $num_count);
                    $htmlcreate_tmp .= '<th colspan="'.$countddd.'" style="text-align:left;border-bottom: none;'.$tdthstyle.'" class="txt">'.$headColData.'</th> ';
                    $colspancount = 0;
                    $colslists[$i]['colspanEndFlg'] = true;
                }
            }else{
                $countddd = $colspancount * ($countcalc + $num_count);
                $htmlcreate_tmp .= '<th colspan="'.$countddd.'" style="text-align:left;border-bottom: none;'.$tdthstyle.'" class="txt">'.$headColData.'</th> ';
                $colslists[$i]['colspanEndFlg'] = true;
            }
            $colspancount++;
        }
        $htmlcreate_tmp .=  '</tr> ';

        if($ychecked[$col]['HIDE'] !== '1'){
            $htmlcreate .= $htmlcreate_tmp;
        }

    }
    $htmlcreate .= '<tr> ';

    if(count($columndata) === 0){
        $htmlcreate .= '<td style="'.$bottomstyle.$head_color.$tdthstyle.'"></td> ';
    }
  $caaaaaa = '';
    foreach($rtncolumn as $key =>$val){

        $k = $columndata[$key];
        if(isset($xchecked[$k]) === false){
            $htmlcreate .= '<td style="border-bottom: none;'.$head_color.$tdthstyle.'" class="txt">'.$val[0]['COLUMN_HEADING'].'</td> ';
        }else if($xchecked[$k]['HIDE'] === ''){
            $htmlcreate .= '<td style="border-bottom: none;'.$head_color.$tdthstyle.'" class="txt">'.$val[0]['COLUMN_HEADING'].'</td> ';
        }
    }
    $htmlcreate .= '</tr> ';
        $htmlcreate .=  '</thead> ';
        $htmlcreate .=  '<tbody> ';
        $dta = array();
        $count = 0;
        for($x = 0;$x<count($data_studio);$x++){
            $count++;
            $trcolor = '#ffffff';
            if($x % 2 !== 0){
                $trcolor = 'rgb(250,250,250);';
            }

            $r =  $data_studio[$x];

            //データの最後か、configで定めた最終行の場合、trにbotder-bottomのスタイルを追加
            if($count === (int)HTMLMAXROW || (($x+1) === count($data_studio))){
                $trcolor .= 'border-bottom:1px solid #ccc;';
            }

            $htmlcreate .= '<tr style="background-color:'.$trcolor.'"> ';

            $bottomstyle = '';
            if($x+1 === count($data_studio)){
                $bottomstyle = 'border-bottom:1px solid #ccc;';
            }

            if(count($columndata) === 0){
                $htmlcreate .= '<td style="'.$bottomstyle.$head_color.$tdthstyle.'"></td> ';
            }

            $befChangeFlg = '';   //ループ中、一つ前の列が変更されたかのフラグ
            foreach($r as $i =>$val){

                if($i !== 'ROWNUM'){

                    $fg = true;
                    for($p= 0; $p<count($columndata); $p++){
                        if( $i === $columndata[$p]){
                            $fg = false;
                        }
                    }    

                    if((isset($dta[$i]) && $dta[$i] !== $val) || $befChangeFlg === true || $x === 0 || $i===$columndata[count($columndata)-1]){
                        if($fg !== false){
                            //このif文はデータ部のみ通る
                            if($val === ''){
                                $val = '<a style="color:red">Over</a>';
                                $htmlcreate .= '<td style="border: 1px solid #ccc;border-bottom:none;border-top:1px solid #ccc;text-align:right;'.$bottomstyle.'">'.$val.'</td> ';
                            }else{

                                //アンダーバーはそのまま表示、それ以外は編集
                                if($val !== '_'){

                                    if(substr($i,0,3) !== 'RES'){
                                        $pattern = '/^C[0-9]+/i';
                                        $calcKey = preg_replace($pattern,'',$i);

                                        if($d1webf === '1'){
                                            if($cchecked[$calcKey]['TYPE'] === 'S' || $cchecked[$calcKey]['TYPE'] === 'P' || $cchecked[$calcKey]['TYPE'] === 'B'){
                                                $val = floatFormat($val,$cchecked[$calcKey]['DEC'],2);
                                            }
                                        }else{
                                            if($cchecked[$calcKey]['FILID'] === '9999'){
                                                if($cchecked[$calcKey]['TYPE'] === 'S' || $cchecked[$calcKey]['TYPE'] === 'P' || $cchecked[$calcKey]['TYPE'] === 'B'){
                                                    $val = floatFormat($val,$cchecked[$calcKey]['DEC'],2);
                                                }
                                            }
                                        }
                                        

                                        
                                    }

                                    if($val !== ''){
                                        if (strpos($val,'.') !== false) {
                                            $tmp = explode('.',$val);
                                            $tmp[0] = num_format($tmp[0]);
                                            $val = $tmp[0].'.'.$tmp[1];
                                        }else{
                                              $val = num_format($val);
                                        }
                                    }

                                }

                                $htmlcreate .= '<td style="border: 1px solid #ccc;border-bottom:none;border-top:1px solid #ccc;text-align:right;'.$bottomstyle.'">'.$val.'</td> ';

                            }
                        }else{
                            //ここのelseは見出し項目の場合のみ通る
                            if($d1webf === '1'){
                                if($xchecked[$i]['TYPE'] === 'S' || $xchecked[$i]['TYPE'] === 'P' || $xchecked[$i]['TYPE'] === 'B'){
                                    $val = floatFormat($val,$xchecked[$i]['DEC'],2);
                                }
                            }else{
                                if($xchecked[$i]['FILID'] === '9999'){
                                    if($xchecked[$i]['TYPE'] === 'S' || $xchecked[$i]['TYPE'] === 'P' || $xchecked[$i]['TYPE'] === 'B'){
                                        $val = floatFormat($val,$xchecked[$i]['DEC'],2);
                                    }
                                }
                            }


                            if($xchecked[$i]['HIDE'] !== '1'){
                                $htmlcreate .= '<td style="border: 1px solid #ccc;border-bottom:none;border-top:1px solid #ccc;text-align:left;'.$bottomstyle.$head_color.'" class="txt">'.$val.'</td> ';
                            }
                        }
                        $befChangeFlg = true;
                    }else{
                        if($fg !== false){
                            if($val !== ''){

                                //アンダーバーはそのまま表示、それ以外は編集
                                if($val !== '_'){

                                    if(substr($i,0,3) !== 'RES'){
                                        $pattern = '/^C[0-9]+/i';
                                        $calcKey = preg_replace($pattern,'',$i);
                                        
                                        if($d1webf === '1'){
                                            if($cchecked[$calcKey]['TYPE'] === 'S' || $cchecked[$calcKey]['TYPE'] === 'P' || $cchecked[$calcKey]['TYPE'] === 'B'){
                                                $val = floatFormat($val,$cchecked[$calcKey]['DEC'],2);
                                            }
                                        }else{
                                            if($cchecked[$calcKey]['FILID'] === '9999'){
                                                if($cchecked[$calcKey]['TYPE'] === 'S' || $cchecked[$calcKey]['TYPE'] === 'P' || $cchecked[$calcKey]['TYPE'] === 'B'){
                                                    $val = floatFormat($val,$cchecked[$calcKey]['DEC'],2);
                                                }
                                            }
                                        }

                                    }

                                    if (strpos($val,'.') !== false) {
                                            $tmp = explode('.',$val);
                                            $tmp[0] = num_format($tmp[0]);
                                            $val = $tmp[0].'.'.$tmp[1];
                                    }else{
                                        $val = (int)$val;
                                        $val = num_format($val);
                                    }

                                }

                            }
                            $htmlcreate .= '<td style="border: 1px solid #ccc;border-bottom:none;border-top:1px solid #ccc;text-align:right;'.$bottomstyle.'">'.$val.'</td> ';
                        }else{

                            //ここのelseは見出し項目の場合のみ通る
                            if($d1webf === '1'){
                                if($xchecked[$i]['TYPE'] === 'S' || $xchecked[$i]['TYPE'] === 'P' || $xchecked[$i]['TYPE'] === 'B'){
                                    $val = floatFormat($val,$xchecked[$i]['DEC'],2);
                                }
                            }else{
                                if($xchecked[$i]['FILID'] === '9999'){
                                    if($xchecked[$i]['TYPE'] === 'S' || $xchecked[$i]['TYPE'] === 'P' || $xchecked[$i]['TYPE'] === 'B'){
                                        $val = floatFormat($val,$xchecked[$i]['DEC'],2);
                                    }
                                }
                            }

                            if($xchecked[$i]['HIDE'] !== '1'){
                                $htmlcreate .= '<td style="border: 1px solid #ccc;border-bottom:none;border-top:none;text-align:right;'.$bottomstyle.$head_color.'"></td> ';
                            }
                        }
                        $befChangeFlg = false;
                    }

                }

            }

            $dta = $r;

            $htmlcreate .= '</tr> ';
            if($count === (int)HTMLMAXROW){
                break;
            }
        }
    $htmlcreate .=  '</tbody></table><table><tr><td></td></tr>';
    $htmlcreate .= '</table></body></html> ';
    return $htmlcreate;
}

function fnGetDB2PMST($db2con,$d1name,$pmpkey){

	$rtn = array();
    $data = array();

	$strSQL = '';

	$strSQL .= ' SELECT * FROM DB2PMST ';
    $strSQL .= ' WHERE PMNAME = ? AND PMPKEY = ? ';

    $params = array($d1name,$pmpkey);

	$stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
              $rtn = array('result' => 'FAIL_SQL');
    }else{
        	$rt = db2_execute($stmt,$params);
           if($rt === false){
               $rtn = array('result' => 'FAIL_SQL');
            }else{
            	if($stmt){
            		while($row = db2_fetch_assoc($stmt)){
            			$data[] = $row;
            		}
            	}
                //$rtn = $data;
                $rtn = array('result' => true,'data' => $data);
            }
    }
	return $rtn;

}


function fnGetDB2PCOL($db2con,$d1name,$wppkey){

	$rtn = array();
    $data = array();

	$strSQL = '';

    $strSQL .= ' SELECT A.*,B.D2LEN,B.D2DEC,B.D2TYPE FROM DB2PCOL AS A ';
    $strSQL .= ' LEFT JOIN ';
    $strSQL .= ' ( ';
    $strSQL .= ' SELECT D2NAME,D2FILID,D2FLD,D2TYPE,D2LEN,D2DEC from FDB2CSV2 ';
    $strSQL .= ' UNION ';
    $strSQL .= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID,D5FLD AS D2FLD,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DEC AS D2DEC from FDB2CSV5 ';
    $strSQL .= ' ) AS B ';
    $strSQL .= ' ON A.WPNAME = B.D2NAME AND A.WPFILID = B.D2FILID AND A.WPFLD = B.D2FLD ';
    $strSQL .= ' WHERE WPNAME = ? AND WPPKEY = ? ';
    $strSQL .= ' ORDER BY WPPFLG,WPSEQN ';

    $params = array($d1name,$wppkey);

	$stmt = db2_prepare($db2con,$strSQL);
     if($stmt === false){
                 $rtn = array('result' => 'FAIL_SQL');
    }else{
        	$rt = db2_execute($stmt,$params);
            if($rt === false){
                  $rtn = array('result' => 'FAIL_SQL');
            }else{
            	if($stmt){
            		while($row = db2_fetch_assoc($stmt)){
            			$data[] = $row;
            		}
            	}
               $rtn = array('result' => true,'data' => $data);
            }
    }
	return $rtn;

}

function fnGetDB2PCAL($db2con,$d1name,$wcckey){

	$rtn = array();
    $data = array();

	$strSQL = '';

	$strSQL .= ' SELECT * FROM DB2PCAL ';
    $strSQL .= ' WHERE WCNAME = ? AND WCCKEY = ? ';
    $strSQL .= ' ORDER BY WCSEQN ';

    $params = array($d1name,$wcckey);

	$stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
    }else{
           $rt = db2_execute($stmt,$params);
            if($rt === false){
                $rtn = array('result' => 'FAIL_SQL');
            }else{
                if($stmt){
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                }
                $rtn = array('result' => true,'data' => $data);
            }
    }
	return $rtn;

}

/*
*-------------------------------------------------------* 
* CSV作成処理
*-------------------------------------------------------*
*/
function exeCsv($csv_h,$csv_d,$define,$csvname,$d1name){
    $csvname = $csvname.'.csv';

    if(!file_exists(BASE_DIR.'/php/csv/'.$csvname)){
       touch(BASE_DIR.'/php/csv/'.$csvname);
    }

    $fp = fopen(BASE_DIR.'/php/csv/'.$csvname,'w');


  //  $fp = fopen(TEMP_DIR.cmMer($csvname), 'w');
 //   error_log('aaaaaaaaaaaa'.$fp);

    $data = $csv_d;

	$data = umEx($data);
    array_unshift($data,$csv_h);
    $fdb2csv1 = FDB2CSV1_DATA($d1name);
    $fdb2csv1 = umEx($fdb2csv1);
    $fdb2csv1 = $fdb2csv1['data'][0];
 
    foreach($data as $key => $row){
        _fputcsv($fp, $row, $key, $fdb2csv1,$define, array());
    }

    fclose($fp);

}
/*
*-------------------------------------------------------* 
* HTML作成処理
*-------------------------------------------------------*
*/
function exeHtmlMail($csv_h,$csv_d,$define,$htmlname){
    $htmlname = $htmlname.'html';
    $columns = $csv_h;
    $data = $csv_d;
    $tbdata ='';
    $tbdata .= '<html>';
    $tbdata .= '<head>';
    $tbdata .= '<style type ="text/css">';
    $tbdata .= '.ii a[href^="tel:"] { color : re }';
    $tbdata .='</style>';
    $tbdata .= '</head>';
    $tbdata .= '<body>';

    $tbdata .='<table style ="width : 100%;border-collapse: collapse;white-space : nowrap !important;margin-top:10px;">';
     $tbdata .='<thead style ="background-color : rgb(79,129,189);color:#ffffff;">';
        $tbdata .='<tr>';
    foreach($columns as $key =>$value){
         $tbdata .='<th nowrap style ="border:1px solid #ccc;padding : 7px 10px 7px 10px;">'.htmlspecialchars($value).'</th>';
    }
        $tbdata .='</tr>';
    $tbdata .='</thead><tbody>';
      $count = 0;
        foreach($data as $key){
            $count++;
            $tbdata .='<tr '.(($count%2===0)?'style ="background-color : rgb(250,250,250);"':'').'>';
                foreach($key as $k=> $value){
                    $type  = $define[$k]['D2TYPE'];
                    $filid = $define[$k]['D2FILID'];
                    $dec   = $define[$k]['D2DEC'];
                    $align = '';
                    if($type === 'S' ||$type === 'P' || $type === 'B' ){
                            $align = 'text-align : right;';

                            //結果フィールドの場合は指定少数桁数で切り捨て
                            if($filid === '9999'){
                                $value = floatFormat($value,$dec,2);
                            }
                    }
                    $tbdata .='<td nowrap style="border:1px solid #ccc;padding : 5px 10px 4px 10px;text-decoration: none;'.$align.'">'.htmlspecialchars($value).'</td>';
                }
            $tbdata .='</tr>';
            if($count === (int)HTMLMAXROW){
                break;
            }
        }
    $tbdata .='</tbody></table>';
    $tbdata .= '</body></html>';
     return $tbdata;
}

/*
*-------------------------------------------------------* 
* メール送信準備
*-------------------------------------------------------*
*/
function exeMail($db2con,$d1name,$mailAddLst,$filename,$dbname,$nowYmd,$nowTime,$password,$csvFlg,$xlsFlg,$htmlFlg,$tbdata,$wspkey,$mailFlg){
    $DB2WAUT = getDB2WAUT($db2con,$d1name,$wspkey);
    if($DB2WAUT['result'] === true){
        $DB2WMAL = getDB2WMAL($db2con);
        if($DB2WMAL['result'] === true){
                $DB2WMAL = $DB2WMAL['data'];
                if(count($DB2WAUT) == 0){
                    $DB2WHIS = fnUpdateDB2WHIS($db2con,$d1name,'',$nowYmd,$nowTime,$password,$dbname,'1','1','1',$wspkey);
                        
                    if($DB2WHIS['result'] !== true){
                             $arr = array($d1name,'',$nowYmd,$nowTime,$password,$dbname,'1','1','1',$wspkey);
                            e_log("exeMail db2whis 1".print_R($arr,true),'1');
                            e_log(showMsg($DB2WHIS['result'],array('exeMail.php','fnUpdateDB2WHIS')),'1');
                    }
                }
                //error_log("akz mail ".print_R($mailAddLst,true));
                foreach($mailAddLst as $key => $value){
                    $whcsvf = '';
                    $whxlsf = '';
                    $whhtmf = '';
                    if($value['WACSV'] == '1'){$whcsvf = '1';};
                    if($value['WAXLS'] == '1'){$whxlsf = '1';};
                    if($value['WAHTML'] == '1'){$whhtmf = '1';};
                    //if($value['WAHTML'] == '1'){$whhtmlf = '1';};
                    if($value['WASNKB'] != ' '){$password = '';};
                    //TODO:DB2WHIS UPDATE HTMLFLG
                    $DB2WHIS = fnUpdateDB2WHIS($db2con,$d1name,$value['WAMAIL'],$nowYmd,$nowTime,$password,$dbname,$whcsvf,$whxlsf,$whhtmf,$wspkey);
                    if($DB2WHIS['result'] !== true){
                            $arr = array($d1name,$value['WAMAIL'],$nowYmd,$nowTime,$password,$dbname,$whcsvf,$whxlsf,$whhtmf,$wspkey);
                            e_log("exeMail db2whis 2".print_R($arr,true),'1');
                            e_log(showMsg($DB2WHIS['result'],array('exeMail.php','fnUpdateDB2WHIS')),'1');
                    }else{
                        if($mailFlg !== '0'){
                            sendMail($db2con,$value,@$DB2WMAL[0],$d1name,$filename,$dbname,$nowYmd,$nowTime,$password,$tbdata);
                        }
                    }
                }

                //zipファイル削除
                $excelext = '';
                $ext = getExt($d1name);

                if(EXCELVERSION == '2003' || $ext === 'xls'){
                    $excelext = '.xls';
                }else if(EXCELVERSION == '2007' || $ext === 'xlsx'){
                    $excelext = '.xlsx';
                }else if(EXCELVERSION == 'html'  || $ext ===''){
                    $excelext = '.xls';
                }
            /*
                if($csvFlg === '1'){
                    @unlink("csv/".$filename.'.csv');
                }
                if($xlsFlg === '1'){
                    @unlink("xls/".$filename.$excelext);
                }
                if($htmlFlg === '1'){
                    @unlink("html/".$filename.'.html');
                }

                if($csvFlg === '1' || $xlsFlg === '1'){
                    @unlink("tmp_zip/".$filename.'.csv.zip');
                    @unlink("tmp_zip/".$filename.$excelext.'.zip');
                    @unlink("tmp_zip/".$filename.'.html.zip');
                    @unlink("tmp_passzip/".$filename.'.csv.zip');
                    @unlink("tmp_passzip/".$filename.$excelext.'.zip');
                    @unlink("tmp_passzip/".$filename.'.html.zip');
                }
            */
                if(file_exists("csv/".$filename.'.csv')){
                    @unlink("csv/".$filename.'.csv');
                }
                if(file_exists("xls/".$filename.$excelext)){
                    @unlink("xls/".$filename.$excelext);
                }
                if(file_exists("html/".$filename.'.html')){
                    @unlink("html/".$filename.'.html');
                }

                if(file_exists("tmp_zip/".$filename.'.csv.zip')){
                    @unlink("tmp_zip/".$filename.'.csv.zip');
                }
                if(file_exists("tmp_zip/".$filename.$excelext.'.zip')){
                    @unlink("tmp_zip/".$filename.$excelext.'.zip');
                }                  
                if(file_exists("tmp_zip/".$filename.'.html.zip')){
                    @unlink("tmp_zip/".$filename.'.html.zip');
                } 
                if(file_exists("tmp_passzip/".$filename.'.csv.zip')){
                    @unlink("tmp_passzip/".$filename.'.csv.zip');
                }    
                if(file_exists("tmp_passzip/".$filename.$excelext.'.zip')){
                    @unlink("tmp_passzip/".$filename.$excelext.'.zip');
                }     
                if(file_exists("tmp_passzip/".$filename.'.html.zip')){
                    @unlink("tmp_passzip/".$filename.'.html.zip');
                }       
        }else{
             e_log(showMsg($DB2WMAL['result'],array('exeMail.php','getDB2WMAL')),'1');
            //クエリー　＊メールサーバー情報の取得に失敗しました。
                $errMsg = showMsg('FAIL_FUNC',array('メールサーバー情報の取得'));//"メールサーバー情報の取得に失敗しました。";
                fnCreateHistory($db2con,$d1name,$filename,$dbname,$nowYmd,$nowTime,$wspkey,$errMsg);
        }
    }else{
        e_log(showMsg($DB2WAUT['result'],array('exeMail.php','getDB2WAUT')),'1');
        //クエリー　配信情報の取得に失敗しました。
        $errMsg = showMsg('FAIL_FUNC',array('配信情報の取得'));//"配信情報の取得に失敗しました。";
        fnCreateHistory($db2con,$d1name,$filename,$dbname,$nowYmd,$nowTime,$wspkey,$errMsg);
    }
}

/*
*-------------------------------------------------------* 
* ファイルをZIP化
*-------------------------------------------------------*
*/
function exeZip($d1name,$filename,$password,$csvFlg,$xlsFlg,$htmlFlg){

    if($csvFlg === '1'){
        fnFileZip($filename.'.csv','csv',$password);
        fnFileZipNoPass($filename.'.csv','csv');
    }

    if($xlsFlg === '1'){
        $excelext = '';
       $ext = getExt($d1name);

        if(EXCELVERSION == '2003' || $ext === 'xls' ){
            $excelext = '.xls';
        }else if(EXCELVERSION == '2007'  || $ext === 'xlsx'){
            $excelext = '.xlsx';
        }else if(EXCELVERSION == 'html' || $ext ===''){
            $excelext = '.xls';
        }
        if($ext == ''){
            $ext === 'xls';
        }
        fnFileZip($filename.$excelext,'xls',$password);
        fnFileZipNoPass($filename.$excelext,'xls');

    }

    if($htmlFlg === '1'){
        fnFileZip($filename.'.html','html',$password);
        fnFileZipNoPass($filename.'.html','html');
    }
}

/*
*-------------------------------------------------------* 
* メール送信
*-------------------------------------------------------*
*/
function sendMail($db2con,$waut,$wmal,$d1name,$filename,$dbname,$nowYmd,$nowTime,$password,$tbdata){
    $address = $waut['WAMAIL'];
    $watype = $waut['WATYPE'];

    //メール送信処理
    mb_language('japanese');
    mb_internal_encoding('UTF-8');

    //インスタンス生成
    $mail = new PHPMailer();
    $mail->CharSet = 'iso-2022-jp';
    $mail->Encoding = '7bit';
/*
    $wmal['WMHOST'] = '74.125.203.108';
    $wmal['WMPORT'] = 465;
    $wmal['WMUSER'] = 'mailerphpquery@gmail.com';
    $wmal['WMPASE'] = 'mailerphpquery00';
*/
    //SMTP接続
    $mail->IsSMTP();

    //ユーザーが入力されている時のみ、SMTP認証をする
    if(cmMer($wmal['WMUSER']) !== ''){
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = 'ssl';
    }

    $mail->Host = cmMer($wmal['WMHOST']);
    $mail->Port = cmMer($wmal['WMPORT']);
    if($watype === '1'){
        $mail->IsHTML(true);
    }
    $mail->Username = cmMer($wmal['WMUSER']); //Gmailのアカウント名
    $mail->Password = cmMer($wmal['WMPASE']); //Gmailのパスワード

    $from = (($waut['WASFLG'] == '1') ? $waut['WAFRAD'] : $wmal['WMFRAD']);
    $mail->From = cmMer($from); //差出人(From)をセット
    $fromname = (($waut['WASFLG'] == '1') ? $waut['WAFRNM'] : $wmal['WMFRNM']);

    //添付ファイル取得
    $excelext = '';
       $ext = getExt($d1name);
           
    if(EXCELVERSION == '2003' || $ext === 'xls'){
        $excelext = '.xls';
    }else if(EXCELVERSION == '2007'  || $ext === 'xlsx'){
        $excelext = '.xlsx';
    }else if(EXCELVERSION == 'html'  || $ext ===''){
        $excelext = '.xls';
    }

    if($waut['WASNKB'] == ' '){
        $attachfile1 = "tmp_passzip/".$filename.'.csv.zip'; //添付ファイルパス
        $attachfile2 = "tmp_passzip/".$filename.$excelext.'.zip'; //添付ファイルパス
        $attachfile3 = "tmp_passzip/".$filename.'.html.zip'; //添付ファイルパス
    }else if($waut['WASNKB'] == '1'){
        $attachfile1 = "tmp_zip/".$filename.'.csv.zip'; //添付ファイルパス
        $attachfile2 = "tmp_zip/".$filename.$excelext.'.zip'; //添付ファイルパス
        $attachfile3 = "tmp_zip/".$filename.'.html.zip'; //添付ファイルパス
    }else if($waut['WASNKB'] == '2'){
        $attachfile1 = "csv/".$filename.'.csv'; //添付ファイルパス
        $attachfile2 = "xls/".$filename.$excelext; //添付ファイルパス
        $attachfile3 = "html/".$filename.'.html'; //添付ファイルパス
    }

    $mail->FromName = mb_encode_mimeheader(cmMer($fromname), 'JIS'); //差出人(From名)をセット
    $subject = ($waut['WASFLG'] == '1') ? $waut['WASUBJ'] : $wmal['WMSUBJ'];
    $mail->Subject = mb_encode_mimeheader($subject, 'JIS');   //件名(Subject)をセット
    /**********************************************************/
    $mailarr = explode(";",$address);
    e_log("mailAddress : ".print_R($mailarr,true),'1');
    foreach($mailarr as $key =>$value){
        $data = chkDB2MAGP($db2con,$value);
            if($data['result'] === true){
                if(count($data['data'])>0){
                        foreach($data['data'] as $key =>$value){
                                $mail->AddAddress(cmMer($value['MAMAIL'])); //宛先
                          }
                }else{
                    $mail->AddAddress(cmMer($value)); //宛先
                }
            }else{
                e_log(showMsg($data['result'],array('exeMail.php','chkDB2MAGP')),'1');
            }
    }
    /**********************************************************/
    //body作成
    $mbody = ($waut['WASFLG'] == '1') ? $waut['WABODY'] : $wmal['WMBODY'];
   // $mbody .= $mbody;
    if($watype === '1'){
        $mbody = str_replace("\n","<br/>",$mbody);
        $mbody .="\n";
        $mbody .=$tbdata;
    }
      //  $mail->Body  = $mbody;
    $mail->Body  = mb_convert_encoding($mbody,'ISO-2022-JP-MS'); //本文(Body)をセット
    //$WAPIV = $waut['WAPIV'];
   
    $mail->Body  = $WAPIV; //本文(Body)をセット
    

    if($waut['WACSV'] == '1'){
        $mail->AddAttachment($attachfile1);
    }
    if($waut['WAXLS'] == '1'){
        $mail->AddAttachment($attachfile2);
    }
    if($waut['WAHTML'] == '1'){
        $mail->AddAttachment($attachfile3);
    }
    $whzpas = $password;
    //メール送信
    $mailrs = $mail->Send();
    //メール送信が成功したらパスワードを送信(SNKBが空の場合のみ)
       if($mailrs){
                if($mailrs && ($waut['WASNKB'] == ' ') && ($waut['WACSV'] == '1' || $waut['WAXLS'] == '1' || $waut['WAHTML'] == '1')){
                    //インスタンス生成
                    $passmail = new PHPMailer();
                    $passmail->CharSet = 'iso-2022-jp';
                    $passmail->Encoding = '7bit';

                    //SMTP接続
                    $passmail->IsSMTP();
                    //ユーザーが入力されている時のみ、SMTP認証をする
                    if(cmMer($wmal['WMUSER']) !== ''){
                        $passmail->SMTPAuth = TRUE;
                        $passmail->SMTPSecure = 'ssl';
                    }
                    $passmail->Host = cmMer($wmal['WMHOST']);
                    $passmail->Port = cmMer($wmal['WMPORT']);
                    $passmail->Username = cmMer($wmal['WMUSER']); //Gmailのアカウント名
                    $passmail->Password = cmMer($wmal['WMPASE']); //Gmailのパスワード

                    $fromaddress = ($waut['WASFLG'] == '1') ? $waut['WAFRAD'] : $wmal['WMFRAD']; //差出人(From)をセット
                    $fromaddress = cmMer($fromaddress);
                    $passmail->From = $fromaddress; //差出人(From)をセット
                    $fromname = ($waut['WASFLG'] == '1') ? $waut['WAFRNM'] : $wmal['WMFRNM'];

                    $passmail->FromName = mb_encode_mimeheader(cmMer($fromname), 'JIS'); //差出人(From名)をセット
                    $subject = ($waut['WASFLG'] == '1') ? $waut['WASUBJ'] : $wmal['WMSUBJ'];
                    $passmail->Subject = mb_encode_mimeheader(cmMer($subject).'<PASS>', 'JIS');   //件名(Subject)をセット
            //        $passmail->AddAddress($address); //宛先
                    /**********************************************************/
                        foreach($mailarr as $key =>$value){
                            $data = chkDB2MAGP($db2con,$value);
                                if($data['result'] === true){
                                    if(count($data['data'])>0){
                                            foreach($data['data'] as $key =>$value){
                                                    $passmail->AddAddress($value['MAMAIL']); //宛先
                                              }
                                    }else{
                                                    $passmail->AddAddress($value); //宛先
                                    }
                                }else{
                                       e_log(showMsg($data['result'],array('exeMail.php','chkDB2MAGP')),'1');
                                }
                        }
                    /**********************************************************/
                    //body作成
                    $body  = "下記の送信済みメールの添付ファイルは、パスワード付きZIPにて暗号化されています。\n";
                    $body .= "このファイルの開封パスワードをお送り致します。\n\n";
                    $body .= "パスワード：".$whzpas."\n\n";
                    $body .= "送信済みメール\n";
                    $body .= "----------------------------------------------------------\n";
                    $body .= "From:       ".$fromaddress."\n";
                    $body .= "Subject:    ".$subject."\n";
                    $body .= "Date:       ".date('D, d M Y H:i:s')."\n";
                    $body .= "----------------------------------------------------------";
                    $passmail->Body  = mb_convert_encoding($body, 'JIS', 'UTF-8'); //本文(Body)をセット
                    if($passmail->Send()){
                    }else{
                        //個人　＊パスワードメールの送信に失敗しました。
                        //エラー内容　$mail->ErrorInfo
                        //メールサーバー情報　SMTP POST・・・
                        $errMsg = "パスワードメールの送信に失敗しました。<br/>送信エラー内容：".$mail->ErrorInfo;
                        $mltb = '';
                        $mltb .= '<h3>メールサーバー情報</h3><hr>';
                        $mltb .= '<br/>SMTPサーバー : '.cmMer($wmal["WMHOST"]);
                        $mltb .= '<br/>ポート : '.cmMer($wmal["WMPORT"]);
                        $mltb .= '<br/>ユーザー : '.cmMer($wmal["WMUSER"]);
                        $mltb .= '<br/>パスワード : '.str_repeat("●",strlen(cmMer($wmal["WMPASE"])));
                        $mltb .= '<br/>差出人アドレス : '.$from;
                        $mltb .= '<br/>差出人名 : '.$fromname;
                        $errMsg .= "<br/>".$mltb;
                        foreach($mailarr as $key =>$value){
                            $DB2WHIS = fnUpdateDB2WHISByID($db2con,$d1name,$whpkey,cmMer($value),$nowYmd,$nowTime,$errMsg);
                        }
                    }
            }
        
    }else{

        e_log($mail->ErrorInfo,'1');
        //個人　＊メールの送信に失敗しました。
        //エラー内容　$mail->ErrorInfo
        //メールサーバー情報　SMTP POST・・・
        $errMsg = showMsg('FAIL_FUNC',array('メールの送信'));//"メールの送信に失敗しました。<br/>送信エラー内容：".$mail->ErrorInfo;
        $mltb = '';
        $mltb .= '<h3>メールサーバー情報</h3><hr>';
        $mltb .= '<br/>SMTPサーバー : '.cmMer($wmal["WMHOST"]);
        $mltb .= '<br/>ポート : '.cmMer($wmal["WMPORT"]);
        $mltb .= '<br/>ユーザー : '.cmMer($wmal["WMUSER"]);
        $mltb .= '<br/>パスワード : '.str_repeat("●",strlen(cmMer($wmal["WMPASE"])));
        $mltb .= '<br/>差出人アドレス : '.$from;
        $mltb .= '<br/>差出人名 : '.$fromname;
        $errMsg .= "<br/>".$mltb;
        foreach($mailarr as $key =>$value){
            $DB2WHIS = fnUpdateDB2WHISByID($db2con,$d1name,$whpkey,cmMer($value),$nowYmd,$nowTime,$errMsg);
        }
    }

}

/*
*-------------------------------------------------------* 
* スケジュールマスター取得(直接実行)
*-------------------------------------------------------*
*/

function getDB2WSCD($db2con,$pWsname,$pWspkey){

	$data = array();
	$rtn = array();

	$params = array(
		$pWsname
	);

    $strSQL  = ' SELECT ';
    $strSQL .= getDb2wscdColumns();
    $strSQL .= ' FROM DB2WSCD AS A ';
    $strSQL .= ' WHERE WSNAME = ? ' ;

    if($pWspkey !== ''){
        $strSQL .= ' AND WSPKEY = ? ' ;
        array_push($params,$pWspkey);
    }

	$stmt = db2_prepare($db2con,$strSQL);
      if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
      }else{
        	$rt = db2_execute($stmt,$params);
            if($rt === false){
                $rtn = array('result' => 'FAIL_SQL');
            }else{
        		if($stmt){
        			while($row = db2_fetch_assoc($stmt)){
        			    $data[] = $row;
        			}
        		}
                    $rtn = array('result' => true,'data' => $data);
            }
    }

	return $rtn;

}

/*
*-------------------------------------------------------* 
* スケジュールマスター取得(ONCE)
*-------------------------------------------------------*
*/

function getOnceDB2WSCD($db2con,$pFrq,$pOday,$pTime){

	$data = array();
	$rtn = array();

            $strSQL  = ' SELECT ';
            $strSQL .= getDb2wscdColumns();
            $strSQL .= ' FROM DB2WSCD AS A ';
            $strSQL .= ' WHERE WSFRQ = ? ' ;
            $strSQL .= ' AND WSODAY = ? ' ;
            $strSQL .= ' AND WSTIME = ? ';
            $strSQL .= ' AND WSSPND <> \'1\' ';

		$params = array(
			$pFrq,
			$pOday,
            $pTime
		);

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                    $rtn = array('result' => 'FAIL_SQL');
          }else{
            	$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                        $rtn = array('result' => true,'data' => $data);
                }
        }
	return $rtn;

}

/*
*-------------------------------------------------------* 
* スケジュールマスター取得(WEEKLY)
*-------------------------------------------------------*
*/

function getWeeklyDB2WSCD($db2con,$pFrq,$pWday,$pTime){

	$data = array();
	$rtn = array();
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wscdColumns();
        $strSQL .= ' FROM DB2WSCD AS A ';
        $strSQL .= ' WHERE WSFRQ = ? ' ;
        $strSQL .= ' AND SUBSTR(WSWDAY,'.($pWday+1).',1) = \'1\' ' ;
        $strSQL .= ' AND WSTIME = ? ';
        $strSQL .= ' AND WSSPND <> \'1\' ';

		$params = array(
			$pFrq,
                $pTime
		);

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                    $rtn = array('result' => 'FAIL_SQL');
          }else{
            		$rt = db2_execute($stmt,$params);
                     if($rt === false){
                            $rtn = array('result' => 'FAIL_SQL');
                    }else{
                		if($stmt){
                			while($row = db2_fetch_assoc($stmt)){
                			    $data[] = $row;
                			}
                		}
                        $rtn = array('result' => true,'data' => $data);
                  }
            }
	return $rtn;

}

/*
*-------------------------------------------------------* 
* スケジュールマスター取得(MONTH)
*-------------------------------------------------------*
*/

function getMonthDB2WSCD($db2con,$pFrq,$pMday,$pTime,$nowYmd){
        $pMday_last = '';
        $last_day_of_month = date('Ymt', strtotime($nowYmd));
        if($nowYmd === $last_day_of_month){
            $pMday_last = 99;
        }
        $str = 'AND WSMDAY = ? ';
        if($pMday_last !== ''){
            $str = ' AND (WSMDAY = ? OR WSMDAY = ? ) ' ;
        }
	$rtn = array();
	$data = array();
            $strSQL  = ' SELECT ';
            $strSQL .= getDb2wscdColumns();
            $strSQL .= ' FROM DB2WSCD AS A ';
            $strSQL .= ' WHERE WSFRQ = ? ' ;
           // $strSQL .= ' AND WSMDAY = ? ' ;
            $strSQL .= ' AND WSTIME = ? ';
            $strSQL .= ' AND WSSPND <> \'1\' ';
            $strSQL .= ' '.$str;
		$params = array(
			$pFrq,
                $pTime,
			$pMday
		);
        if($pMday_last !== ''){
            array_push($params, $pMday_last);
        }
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
                     $rtn = array('result' => 'FAIL_SQL');
        }else{
         		$rt = db2_execute($stmt,$params);
                if($rt === false){
                        $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                        $rtn = array('result' => true,'data' => $data);
                }
            }
	return $rtn;

}

/*
*-------------------------------------------------------* 
* スケジュールマスター取得(INTERVAL)
*-------------------------------------------------------*
*/

function getIntervalDB2WSCD($db2con,$pFrq){
	$rtn = array();
	$data = array();
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wscdColumns();
        $strSQL .= ' FROM DB2WSCD AS A ';
        $strSQL .= ' WHERE WSFRQ = ? ' ;
        $strSQL .= ' AND WSSPND <> \'1\' ';

		$params = array(
			$pFrq
		);
		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                    $rtn = array('result' => true,'data' => $data);
                }
            }
	return $rtn;

}

/*
*-------------------------------------------------------* 
* DB2WAUT取得カラム
*-------------------------------------------------------*
*/
function getDb2wautColumns(){

    $strSQL  = ' A.WANAME, ';
    $strSQL  .= ' A.WAPKEY, ';
    $strSQL .= ' A.WAMAIL, ';
    $strSQL .= ' A.WACSV,  ';
    $strSQL .= ' A.WAXLS,  ';
    $strSQL .= ' A.WAHTML,  ';
    $strSQL .= ' A.WASPND, ';
    $strSQL .= ' A.WASNKB, ';
    $strSQL .= ' A.WASFLG, ';
    $strSQL .= ' A.WAFRAD, ';
    $strSQL .= ' A.WAFRNM, ';
    $strSQL .= ' A.WASUBJ, ';
    $strSQL .= ' A.WABODY, ';
    $strSQL .= ' A.WATYPE, ';
    $strSQL .= ' A.WAPIV ';

    return $strSQL;
}

/*
*-------------------------------------------------------* 
* メール配信マスター取得
*-------------------------------------------------------*
*/

function getDB2WAUT($db2con,$pName,$wapkey){
    	$rtn = array();
    	$data = array();
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wautColumns();
        $strSQL .= ' FROM DB2WAUT AS A ';
        $strSQL .= ' WHERE A.WANAME = ? ' ;
        $strSQL .= ' AND A.WASPND <> \'1\' ';
        $strSQL .= ' AND A.WAPKEY = ?  ';

		$params = array(
            $pName,
            $wapkey
		);
		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                e_log('Email Data***'.print_r($rt,true));
                if($rt === false){
                       $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                     $rtn = array('result' => true,'data' => $data);
                }
            }
	return $rtn;
}

/*
*-------------------------------------------------------* 
* 定義に対して、その中にいるユーザーが一人でも
* CSVにチェックがついているかをチェック
*-------------------------------------------------------*
*/

function chkDB2WAUTCsv($db2con,$pName){
     $rtn = array();
	$data = array();
    $flg = '0';
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wautColumns();
        $strSQL .= ' FROM DB2WAUT AS A ';
        $strSQL .= ' WHERE WANAME = ? ' ;
        $strSQL .= ' AND WASPND <> \'1\' ';
        $strSQL .= ' AND WACSV = \'1\' ';

		$params = array(
            $pName
		);

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                    $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){ 
                        $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                    //一人でもいたらtrueを返す
                    if(count($data) > 0){
                        $flg = '1';
                    }
                    $rtn = array('result' => true,'flg' => $flg);
                }
            }
	return $rtn;

}

/*
*-------------------------------------------------------* 
* 定義に対して、その中にいるユーザーが一人でも
* EXCELにチェックがついているかをチェック
*-------------------------------------------------------*
*/

function chkDB2WAUTXls($db2con,$pName,$wspkey){
	$rtn = array();
	$data = array();
    $flg = '0';
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wautColumns();
        $strSQL .= ' FROM DB2WAUT AS A ';
        $strSQL .= ' WHERE WANAME = ? ' ;
        $strSQL .= ' AND WASPND <> \'1\' ';
        $strSQL .= ' AND WAPKEY = ? ';
        $strSQL .= ' AND WAXLS = \'1\' ';

		$params = array(
            $pName,
            $wspkey
		);

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
            		$rt = db2_execute($stmt,$params);
                    if($rt === false){
                        $rtn = array('result' => 'FAIL_SQL');
                    }else{
                		if($stmt){
                			while($row = db2_fetch_assoc($stmt)){
                			    $data[] = $row;
                			}
                		}
                            //一人でもいたらtrueを返す
                            if(count($data) > 0){
                                $flg = '1';
                            }
                        $rtn = array('result' => true,'flg' => $flg);
                }
            }
	return $rtn;

}

/*
*-------------------------------------------------------* 
* 定義に対して、その中にいるユーザーが一人でも
* HTMLにチェックがついているかをチェック
*-------------------------------------------------------*
*/

function chkDB2WAUTHtml($db2con,$pName,$wspkey){
	$rtn = array();
	$data = array();
        $flg = '0';
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wautColumns();
        $strSQL .= ' FROM DB2WAUT AS A ';
        $strSQL .= ' WHERE WANAME = ? ' ;
        $strSQL .= ' AND WASPND <> \'1\' ';
        $strSQL .= ' AND WAPKEY = ? ';
        $strSQL .= ' AND WAHTML = \'1\' ';

		$params = array(
            $pName,
            $wspkey
		);

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
            		$rt = db2_execute($stmt,$params);
                    if($rt === false){
                        $rtn = array('result' => 'FAIL_SQL');
                    }else{
                		if($stmt){
                			while($row = db2_fetch_assoc($stmt)){
                			    $data[] = $row;
                			}
                		}
                            //一人でもいたらtrueを返す
                            if(count($data) > 0){
                                $flg = '1';
                            }
                        $rtn = array('result' => true,'flg' => $flg);
                }
            }
	return $rtn;

}
/*
*-------------------------------------------------------* 
* 定義に対して、その中にいるユーザーが一人でも
* HTMLにチェックがついているかをチェック
*-------------------------------------------------------*
*/
function chkDB2WATYPEHtml($db2con,$pName){
    $rtn = array();
    $data = array();
    $flg = '0';
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wautColumns();
        $strSQL .= ' FROM DB2WAUT AS A ';
        $strSQL .= ' WHERE WANAME = ? ' ;
        $strSQL .= ' AND WASPND <> \'1\' ';
        $strSQL .= ' AND WATYPE = \'1\' ';

		$params = array(
            $pName
		);

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                       //一人でもいたらtrueを返す
                    if(count($data) > 0){
                        $flg = '1';
                    }
                    $rtn = array('result' => true,'flg' => $flg);
                }
            }
	return $rtn;
}
//0件メール配信チェック
function chkDB2WSOC($db2con,$d1name,$wspkey){
    $flg = array(); 
    $data = array();
    $flg = '0';
        $strSQL  = ' SELECT SO0MAL';
        $strSQL .= ' FROM DB2WSOC ';
        $strSQL .= ' WHERE SONAME = ? ' ;
        $strSQL .= ' AND SOPKEY = ? ' ;
		$params = array(
            $d1name,
            $wspkey
		);

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                        $rtn = array('result' => 'FAIL_SQL');
                }else{
                		if($stmt){
                			while($row = db2_fetch_assoc($stmt)){
                			    $data[] = $row;
                			}
                		}
                           
                        if(count($data) > 0){
                            $flg = $data[0][SO0MAL];
                        }
                        $rtn = array('result' => true,'flg' => $flg);
                }
            }
	return $rtn;
}
/*
*-------------------------------------------------------* 
* DB2WMAL取得カラム
*-------------------------------------------------------*
*/
function getDb2wmalColumns($WMPASS){
    $strwmpass = '';
    if($WMPASS !== ''){
         $strwmpass .= ' DECRYPT_CHAR(A.WMPASE) AS WMPASE, ';
    }else{
         $strwmpass  = 'A.WMPASS AS WMPASE,';
    }

    $strSQL  = ' A.WMHOST, ';
    $strSQL .= ' A.WMPORT, ';
    $strSQL .= ' A.WMUSER, ';
    $strSQL .= $strwmpass;
    $strSQL .= ' A.WMFRAD, ';
    $strSQL .= ' A.WMFRNM, ';
    $strSQL .= ' A.WMSUBJ, ';
    $strSQL .= ' A.WMBODY ';

    return $strSQL;
}

/*
*-------------------------------------------------------* 
* メール設定取得
*-------------------------------------------------------*
*/

function getDB2WMAL($db2con){

	$data = array();
	$rtn = array();
    $WMPASS = '';
        $rs = fnGetWMPASS($db2con);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('パスワード'));
        }else{
            $WMPASS = cmMer($rs['WMPASS']);
        }
            if($WMPASS !== ''){
            	$sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

            	db2_exec($db2con,$sql);
            }
            $strSQL  = ' SELECT ';
            $strSQL .= getDb2wmalColumns($WMPASS);
            $strSQL .= ' FROM DB2WMAL AS A ';

		$params = array();
        
		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                        $rtn = array('result' => true,'data' => $data);
                }
            }
	return $rtn;
}
/**
*メールのパスワードを取得する
*WAPASSを取得
**/

function fnGetWMPASS($db2con){
        $params = array();
        $WMPASS = array();
        $strSQL  = ' SELECT A.WMPASS ';
        $strSQL .= ' FROM DB2WMAL AS A ' ;
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $WMPASS[] = $row;
                }
                 $WMPASS = $WMPASS[0]['WMPASS'];
                $data = array('result' => true,'WMPASS' => $WMPASS);
            }
        }
    return $data;
}

/*
*-------------------------------------------------------* 
* メールグループについているかをチェック
*-------------------------------------------------------*
*/
function chkDB2MAGP($db2con,$pName){
    $rtn = array();
    $data = array();
        $strSQL  = ' SELECT *';
        $strSQL .= ' FROM DB2MAAD ';
        $strSQL .=' WHERE MANAME = ? ';
		$params = array(
            $pName
		);
          //  ERROR_LOG($pName);
          //  ERROR_LOG($strSQL);
		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                       $rtn = array('result' => true,'data' => $data);
                }
        }
        return $rtn;
}

/*
*-------------------------------------------------------* 
* ファイル圧縮
*-------------------------------------------------------*
*/

function fnFileZip($file,$extension,$password){
shell_exec('7z a tmp_passzip/'.$file.'.zip -p'.$password.' '.$extension.'/'.$file);

}

/*
*-------------------------------------------------------* 
* ファイル圧縮(パスワードなし)
*-------------------------------------------------------*
*/

function fnFileZipNoPass($file,$extension){
shell_exec('7z a tmp_zip/'.$file.'.zip '.$extension.'/'.$file);

}


/*
*-------------------------------------------------------* 
* 送信履歴Update
*-------------------------------------------------------*
*/

function fnUpdateDB2WHIS($db2con,$whname,$whmail,$whbday,$whbtim,$whzpas = '',$whoutf,$whcsvf,$whxlsf,$whhtmf,$whpkey){

      $rtn = array();
	  $rs = '0';

	    //構文
		$strSQL  = ' UPDATE DB2WHIS ';
		$strSQL .= ' SET ';
		$strSQL .= ' WHCSVF = ?, ';
		$strSQL .= ' WHXLSF = ?, ';
		$strSQL .= ' WHHTMF = ?, ';
		$strSQL .= ' WHZPAS = ? ';
		$strSQL .= ' WHERE ';
		$strSQL .= ' WHNAME = ? ' ;
		$strSQL .= ' AND WHMAIL = ? ';
        $strSQL .= ' AND WHBDAY = ? ';
        $strSQL .= ' AND WHBTIM = ? ';
        $strSQL .= ' AND WHPKEY = ? ';

		$params = array(
            $whcsvf,
            $whxlsf,
            $whhtmf,
            $whzpas,
            $whname,
            $whmail,
            $whbday,
            $whbtim,
            $whpkey
		);

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
                $rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
                    $rtn = array('result' => true);
                 }
            }
	return $rtn;

}

/*
*-------------------------------------------------------* 
* DB2WSCD 実行日時更新
*-------------------------------------------------------*
*/

function updDB2WSCD($db2con,$wsname,$wsfrq,$wsoday,$wswday,$wsmday,$wstime,$wspkey,$nowYmd,$nowTime){

    $rtn = array();
	$rs = '0';

	//構文
        $strSQL  = ' UPDATE DB2WSCD ';
        $strSQL .= ' SET ';
        $strSQL .= ' WSBDAY = ?, ';
        $strSQL .= ' WSBTIM = ?, ';
        $strSQL .= ' WSSDAY = ?, ';
        $strSQL .= ' WSSTIM = ? ';
        $strSQL .= ' WHERE ';
        $strSQL .= ' WSNAME = ? ';
        $strSQL .= ' AND WSFRQ = ? ';
        $strSQL .= ' AND WSODAY = ? ';
        $strSQL .= ' AND WSWDAY = ? ';
        $strSQL .= ' AND WSMDAY = ? ';
        $strSQL .= ' AND WSTIME = ? ';
        $strSQL .= ' AND WSPKEY = ? ';

		$params = array(
            $nowYmd,
            $nowTime,
            $nowYmd,
            $nowTime,
            $wsname,
            $wsfrq,
            $wsoday,
            $wswday,
            $wsmday,
            $wstime,
            $wspkey
		);


		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
                     $rtn = array('result' => true);
                }
          }

	return $rtn;

}

function checkDB2WSCD($db2con,$d1name,$nowYmd,$nowTime,$wspkey){
    $rs = true;
	$data = array();
	$rtn = array();
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wscdColumns();
        $strSQL .= ' FROM DB2WSCD AS A ';
        $strSQL .= ' WHERE ';
        $strSQL .= ' WSNAME = ? ';
        $strSQL .= ' AND WSBDAY = ? ';
        $strSQL .= ' AND WSBTIM = ? ';
        $strSQL .= ' AND WSPKEY = ? ';

		$params = array(
            $d1name,
            $nowYmd,
            $nowTime,
            $wspkey
        );

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                    $rtn = array('result' => true,'data' => count($data));
                }
            }
	return $rtn;
}

//間隔実行チェック
function checkAfterTime($interval,$nowTime){

	$rtn = true;

	//実行間隔を時刻と分に分け、分で取得
	$interval = sprintf('%04d',$interval);
	$h = (int)substr($interval,0,2);
	$m = (int)substr($interval,2,2);

	//現在時刻
	$hn = (int)substr($nowTime,0,2);
	$mn = (int)substr($nowTime,2,2);

	if($mn%$m !== 0){
		$rtn = false;
	}else{
		if($h > 0){
			if($hn%$h !== 0){

				$rtn = false;
			}
		}
	}
	return $rtn;

}
function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $strSQL  = ' SELECT ';
    $strSQL .= '     A.D1WEBF ';
    $strSQL .= '    ,A.D1DIRE ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE A.D1NAME = ? ';


    $params = array(
        $D1NAME
    );

    $stmt = db2_prepare($db2con,$strSQL);

    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true,'data' => umEx($data));
            }
        }
    }
    return $data;

}
function fnGetFDB2CSV2($db2con,$D2NAME){

    $data = array();

    $strSQL  = ' SELECT ';
    $strSQL .= ' A.D2FLD ';
    $strSQL .= ' FROM FDB2CSV2 as A ';
    $strSQL .= ' WHERE A.D2NAME = ? ';
    $strSQL .= ' ORDER BY A.D2CSEQ ';

    $params = array(
        $D2NAME
    );

    $stmt = db2_prepare($db2con,$strSQL);

    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}
function fnDataShukei($db2con,$fdb2csv2,$fdb2csv2_ALL,$d1name,$dbname,$db2wcol,$rsMasterArr,$rsshukeiArr,$burstItmLst){
    $tabledata = array();
    e_log("Testing fnDataShukei1...");
    // クエリ情報を取得する
    $dFile1 = cmGetDbFile_1($db2con,$fdb2csv2,$dbname,'','','','','','',$db2wcol,$rsMasterArr,true,$burstItmLst);
    if($dFile1['result'] !== true){
        $msg = showMsg($dFile1['result']);
        $rtn = 1;
    }
    else{
        // 集計情報を取得する
        $dFile2 = cmGetDbFile_2($db2con,$fdb2csv2,$d1name,$dbname,'','','',$db2wcol,$rsMasterArr,$rsshukeiArr,true,$burstItmLst,'','',array(),array());
        e_log("Testing fnDataShukei2...".print_r($dFile2,true));
        if($dFile2['result'] !== true){
            $msg = showMsg($dFile2['result']);
            $rtn = 1;
        }
        else{
           // データをマージする
           $tabledata = cmShukeiDataMerge($db2con,$dFile1['data'],$dFile2['data'],$rsMasterArr,$rsshukeiArr,$fdb2csv2,$fdb2csv2_ALL);
           if($tabledata['result'] !== true){
               $msg = showMsg($tabledata['result']);
               $rtn = 1;
           }           
           else{
               $tabledata = $tabledata['data'];
           }
        }
    }
    return $tabledata;
}
function chkFDB2CSV1PG($db2con,$d1name){
    $data = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1PG A ' ;
    $strSQL .= ' WHERE A.DGNAME  = ? ';

    $params = array(
        $d1name
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => umEx($data,false)
            );
        }
    }
    return $data;
}
function createCSVFile($db2con,$d1name,$dbname,$DIRE){
    $sSearch = '';
    $db2wcol = '';
    
    $fdb2csv2 = cmGetFDB2CSV2($db2con,$d1name,true,false,false);
    $fdb2csv2 = umEx($fdb2csv2['data']);
    //ヘッダー情報取得
    $csv_h = cmCreateHeaderArray($fdb2csv2);
    $csv_d = cmGetDbFile($db2con,$fdb2csv2,$dbname,'','','','','','',array(),false,'','',array(),array());
    if($csv_d['result'] !== true){
       // error_log(showMsg($csv_d['result']));
        $rtn = 1;
    }
    if(substr($DIRE,0,1) !== '/'){
        $DIRE = '/'.$DIRE;
    }
    $fdb2csv1 = FDB2CSV1_DATA($d1name);
    $fdb2csv1 = $fdb2csv1['data'][0];
    if($fdb2csv1['D1CTFL'] === '1'){
        $fileNMArr = explode('.',$DIRE);
        $curTime = getCurrentTimeStamp($db2con);
        $fileNm = '_'.$curTime.'.'.$fileNMArr[count($fileNMArr)-1];
        array_pop($fileNMArr);
        $DIRE = join('.',$fileNMArr).$fileNm;
    }
    if(!file_exists($DIRE)){
        touch($DIRE);
    }

    $fp = fopen($DIRE,'w');
    $data = $csv_d['data'];
	$data = umEx($data);
    array_unshift($data,$csv_h);
    foreach($data as $key => $row){
        _fputcsv($fp, $row, $key,$fdb2csv1, $fdb2csv2, array());
    }
    fclose($fp);    
}
function getCurrentTimeStamp($db2con){
    $data = array();
    $strSQL  = '    SELECT  ';
    $strSQL .= '    TO_CHAR(CURRENT TIMESTAMP,\'YYYYMMDDHH24MISS\') AS CURTIME ';
    $strSQL .= '    FROM sysibm/sysdummy1 ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => $data[0]
            );
        }
    }
    $curTime = $data['data']['CURTIME'];
    return $curTime;
}
?>