<?php
/*
 *-------------------------------------------------------*
 * RPGで約15秒おきに実行される
 * スケジュールマスターを呼んでメール配信を行う
 *-------------------------------------------------------*
*/
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
//error_log('exeMailSend Call');
include_once ('common/lib/spout/src/Spout/Autoloader/autoload.php');
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
include_once ("common/inc/config.php");
//error_log('exeMailSend before common.inc.php');
include_once ("common/inc/common.inc.php");
//ini_set('error_log', '../maillog_'.date('Ymd').date('His').'.log');
ini_set('error_log', '../maillog.log');
//error_log('exeMailSend after common.inc.php');
include_once ("licenseInfo.php");
include_once ("common/lib/PHPExcel/Classes/PHPExcel.php");
include_once ("common/lib/PHPExcel/Classes/PHPExcel/IOFactory.php");
include_once ("common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");
include_once ("common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php");
include_once ("base/createExecuteSQL.php");
include_once ("getQryCnd.php");
include_once ("getSQLCnd.php");
include_once ("chkSQLParam.php");
include_once ("base/createTblExeSql.php");
include_once ("base/getChkOutputFileData.php"); //スケジュールの出力ファイルチェック
include_once ("base/fnDB2QHIS.php");
include_once ("chkCondParam.php");
include_once ("CLExecutePG.php");
//include_once ("initCheck.php");
/*
 *-------------------------------------------------------*
 * 共通変数
 *-------------------------------------------------------*
*/
//require ("common/lib/PHPMailer/PHPMailerAutoload.php");
session_start();
$ver = phpversion();
$sver = substr($ver, 0, 3);
//e_log('IP 20180913 bottom'.print_r($_SERVER,true));
$phpversion = str_replace('.', '', $sver);
if ((int)$phpversion >= 54) {
    require ("common/lib/PHPMailer/PHPMailerAutoload.php");
} else {
    require ("common/lib/@PHPMailer/class.phpmailer.php");
}
$nowDate = date('Ymd');
$nowTime = date('Hi');
$resultdata = array();
$qryCnt = 0;//190405 クエリー件数
error_log("exeMailSend実行開始：" . $nowDate . '_' . $nowTime . print_r($argv, true));
//直接実行のパラメータを取得
//$SEVLINK = $GLOBALS['SEVERLINKSS'];//Severlink

// e_log('GET_IP SEVLINK ****'.print_r($SEVLINK,true));
$argvCount = count($argv);
if ($argvCount >= 5) {
    $lastIndex = $argvCount - 1;
    $argv5 = $argv[$lastIndex]; // 1:グラフ実行
    // 2:グラフデータ実行
    // 0: argv4 <> '' ピボットメール実行
    //  : argv4 == '' クエリー実行
    $argv1 = (isset($argv[1]) ? $argv[1] : $nowDate); //日付
    $argv2 = (isset($argv[2]) ? $argv[2] : $nowTime); //時間
    $argv3 = (isset($argv[3]) ? $argv[3] : ''); //クエリー名
    if ($argv5 === '0') {
        if ($argvCount === 5) {
            $argv4 = '';
        } else {
            $argv4 = (isset($argv[4]) ? $argv[4] : ''); //ピボット
            
        }
    } else if ($argv5 === '1') {
        $argv4 = (isset($argv[4]) ? $argv[4] : ''); //グラフ
        
    } else if ($argv5 === '2') {
        $argv4 = '';
    }
} else {
    $argv1 = $nowDate; //日付
    $argv2 = $nowTime; //時間
    $lastIndex = $argvCount - 1;
    $argv5 = '0';
    $argv3 = (isset($argv[1]) ? $argv[1] : ''); //クエリー名
    if ($argvCount === 2) {
        $argv4 = '';
    } else {
        $argv4 = (isset($argv[2]) ? $argv[2] : ''); //ピボット
        
    }
}
//e_log("exeMailSendパラメータ配列：" . '実行時間' . $argv1 . '_' . $argv2 . '実行クエリー：' . $argv3 . '実行ピボット又グラフ：' . $argv4 . '実行フラグ：' . $argv5);
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
//ライセンスのスケジュール実行権限がない場合、ここで処理終了
if ($licenseScheduleBtn === false) {
    //e_log('【削除】スケジュールライセンス不可');
    exit();
}
//DB接続
$db2con = cmDb2Con();
$Seigyofilename = '';
//ライブラリセット
cmSetPHPQUERY($db2con);
if ($argv5 === '0') {
    // クエリー又ピボットメール送信
    $burstItmLst = array();
    $burstItmLst = getBurstItmLst($db2con, $argv3, $argv4);
    if ($burstItmLst['result'] == true) {
        $burstItmLst = $burstItmLst['data'];
        error_log('callExeQuery bustItmLstksk⇒' . print_r($burstItmLst, true));
        if (count($burstItmLst) === 0) {
            $rs = getWSQGFLGSCH($db2con, $argv3);
            if ($rs['result'] === true) {
                $data = $rs['data'];
                if (count($data) > 0) {
                    $WAQGFLG = $data[0]['WSQGFLG'];
                    $burstItmLst = array(array("WAQGFLG" => $WAQGFLG, 'DUMMY' => true, 'WABAFLD' => ''));
                }
            }
        }
        if (count($burstItmLst) !== 0) {
            $isQGflg = ($burstItmLst[0]['WAQGFLG'] === '1') ? true : false; //クエリーグループかどうかのFLG
            //ライセンスのクエリー連携実行権限がない場合、ここで処理終了
            if ($isQGflg && $licenseQueryGroup === false) {
                error_log('query group fail++++++++++++++++++++');
                exit();
            }
            callExeQuery($db2con, cmMer($argv3), cmMer($argv1), cmMer($argv2), $licenseCl, cmMer($argv4), $burstItmLst, $licenseIFS, $licenseSeigyo, $licenseSql, $licenseFileOut);
        }
    } else {
        //"バースト情報の取得に失敗しました。";
        //e_log(showMsg($rs['result'], array('exeMailSend.php', 'getBurstItmLst')), '1');
        $errMsg = showMsg('FAIL_FUNC', array('バースト情報の取得'));
        fnCreateHistory($db2con, cmMer($argv3), '', '', $nowYmd, $nowTime, cmMer($argv4), $errMsg);
    }
} else if ($argv5 === '2') {
    //グラフファイル作成
    $d1name = cmMer($argv3);
    $fdb2csv1Data = FDB2CSV1_DATA($d1name);
    $RDBNM = cmMer($fdb2csv1Data['data'][0]['D1RDB']);
    $D1CFLG = cmMer($fdb2csv1Data['data'][0]['D1CFLG']);
    $WEBF = cmMer($fdb2csv1Data['data'][0]['D1WEBF']);
    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
        $db2connet = $db2con;
    } else {
        $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
        $db2RDBcon = cmDB2ConRDB(SAVE_DB);
        $db2connet = $db2RDBcon;
    }
    do {
        $dbname = makeRandStr(10);
        $systables = cmGetSystables($db2connet, SAVE_DB, $dbname);
    } while (count($systables) > 0);
    $rs = cmCreateQryTable($db2con, $WEBF, $d1name, $dbname, $licenseCl, $D1CFLG);
    $gpkrs = getDBGPKQRY($db2con, $d1name, $nowDate, $nowTime);
    if ($rs['RTN'] !== 1) {
        if ($gpkrs['result'] !== true) {
            // e_log(showMsg($gpkrs['result'], array('exeMailSend.php', 'getDBGPKQRY')), '1');
            
        } else {
            $data = $gpkrs['data'];
            $msg = '';
            foreach ($data as $k => $val) {
                $GPKID = cmMer($val['GPKID']);
                $GPKTBL = cmMer($val['GPKTBL']);
                if ($GPKTBL !== '') {
                    $systables = cmGetSystables($db2connet, SAVE_DB, $GPKTBL);
                    if (count($systables) > 0) {
                        $rs = cmDropQueryTable($db2connet, $GPKTBL);
                        if ($rs['result'] !== true) {
                            $rtn = 1;
                            //e_log("<br/>drop table error " . $d1name . '' . $GPKTBL);
                            
                        } else {
                            // e_log("<br/>drop table success " . $d1name . '' . $GPKTBL);
                            
                        }
                    }
                    //       $gphdbname = $GPKTBL; //作成された同じ名前使う
                    
                }
                do {
                    $gphdbname = makeRandStr(10);
                    $systables = cmGetSystables($db2connet, SAVE_DB, $gphdbname);
                } while (count($systables) > 0);
                $rs = cmCreateGphTable($db2connet, $gphdbname, $dbname, $D1CFLG);
                if ($rs['result'] !== true) {
                    $rtn = 1;
                    //e_log("<br/>create table error " . $d1name . '' . $gphdbname);
                    
                } else {
                    // e_log("<br/>create table success " . $gphdbname);
                    $GPHINFO = cmCreateGraphInfoData($db2con, $db2connet, $d1name, $gphdbname, $GPKID);
                    if ($GPHINFO['RTN'] !== 0) {
                        $msg = $GPHINFO['MSG'];
                        $CHARTDATA = array();
                        $CHARTDATA['CHARTINFO'] = array();
                        $CHARTDATA['ERRMSG'] = array($msg);
                        $str_json_format = json_encode($CHARTDATA);
                        $textFileName = BASE_DIR . "/php/graphfile/" . $d1name . '-' . $GPKID . '.txt';
                        if (file_exists($textFileName)) {
                            unlink($textFileName);
                        }
                        $txt = $str_json_format;
                        $myfile = fopen($textFileName, "w") or die("Unable to open file!");
                        fwrite($myfile, $txt);
                        fclose($myfile);
                    } else {
                        $GPHINFO = $GPHINFO['GPHINFO'];
                        $rs = cmCreateGraphInfoFile($db2con, $GPHINFO);
                    }
                }
                if ($rtn === 1) {
                }
            }
            //e_log('d1name'.$d1name.'GPKID'.$GPKID.'dbname'.$dbname);
            if ($rtn !== 1) {
                $rs = cmDropQueryTable($db2connet, $dbname);
                if ($rs['result'] !== true) {
                    $rtn = 1;
                    e_log("<br/>drop table error " . $d1name . '' . $dbname);
                } else {
                    e_log("<br/>drop table success " . $d1name . '' . $dbname);
                }
            }
        }
    } else {
        $rtn = 1;
        $data = $gpkrs['data'];
        $GPKQRY = $d1name;
        $CHARTDATA = array();
        $CHARTDATA['CHARTINFO'] = array();
        $msg = showMsg('FAIL_SEL');
        $CHARTDATA['ERRMSG'] = array($msg);
        foreach ($data as $k => $val) {
            //結果String
            $str_json_format = json_encode($CHARTDATA);
            $GPKID = cmMer($val['GPKID']);
            $textFileName = BASE_DIR . "/php/graphfile/" . $GPKQRY . '-' . $GPKID . '.txt';
            if (file_exists($textFileName)) {
                unlink($textFileName);
            }
            $txt = $str_json_format;
            $myfile = fopen($textFileName, "w") or die("Unable to open file!");
            fwrite($myfile, $txt);
            fclose($myfile);
        }
        //e_log("table create error");
        
    }
} cmDb2Close($db2con);
/*
 *-------------------------------------------------------*
 * メール対象の定義実行前準備
 *ミィッミィッモー
 *-------------------------------------------------------*
*/
function callExeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $wspkey, $burstItmLst, $licenseIFS, $licenseSeigyo, $licenseSql, $licenseFileOut) {
    e_log('callExeQueryParamKSK:' . $db2con . $d1name . '日付：' . $nowYmd . $nowTime . '$burstItmLst:' . print_r($burstItmLst, true)); //loadmail
    $wspkey = cmMer($wspkey); // ピボットキー
    $d1name = cmMer($d1name); // クエリーキー
    $hiswspkey = $wspkey; //クエリーグループの場合ピボットの履歴を書くためのキー
    $rtn = 0; // エラーチェックフラグ
    // 定義情報
    $d1webf = ''; // 作成フラグ【WEB、5052】
    $d1cflg = ''; // ワッブで作成フラグ【通報、SQL】
    $rdbnm = ''; //　RDB
    $d1edkb = ''; // XLSカステームテンプレートフラグ
    $d1dire = ''; // CSV出力先パス
    $d1diex = ''; // xls出力先パス
    $d1soex = ''; // ピボットxls出力先パス
    $soctfl = ''; // ピボットxls出力先ファイルにタイムスタンプつけるかのフラグ
    $seigyoFlg = 0; // クエリーの制御チェック
    $zeromail = ''; // データ件数ゼローの場合送信するフラグ
    $d1tmpf = ''; // カステームテンプレートパース
    $d1tros = 0; //template row
    $tmp_d1cflg = '';
    $D1NAMELIST = array(); //クエリーグループのリスト
    $LASTD1NAME = ''; //最後のクエリーID
    $isQGflg = ($burstItmLst[0]['WAQGFLG'] === '1') ? true : false; //クエリーグループかどうかのFLG
    $RDBNameList = array(); //別の区画のためＲＤＢ名を入れる
    $RDBNameObj = array(); //別の区画のためＲＤＢ名を入れるObj
    $PHPNameList = array(); //LOCALのためＤＢの名を入れる
    $sqlChkflg = true;//クエリー連携実行の時全部のクエリーがｓｑｌかどうかチェック

    //クエリーグループなら一番最後のデータをもらう
    if ($isQGflg) { //クエリーグループ
        //クエリーグループ
        $rs = cmGetQueryGroup($db2con, $d1name, $wspkey, '');
        if ($rs['result'] !== true) {
            $rtn = 2;
            $msg = showMsg($rs['result'], array('クエリーグループ'));
        } else {
            if(!$licenseSql){
                foreach($rs['data'] as $key => $res){
                    if($res['D1CFLG'] === ''){
                        $sqlChkflg = false;
                        array_push($D1NAMELIST,$rs['data'][$key]);
                    }      
                }
                $D1NAMELIST[count($D1NAMELIST)-1]['LASTFLG'] = '1';
                $D1NAMELIST = umEx($D1NAMELIST,true);
                //ライセンスのsqlクエリー実行権限がない場合、ここで処理終了
                if ($sqlChkflg) {
                    error_log('SQL Query Group Fail***************');
                    exit();
                }
            }else{
                $D1NAMELIST=umEx($rs['data'],true);
            }            }
        //クエリーグループ
        
    }
    //get FDB2CSV1の設定と出力ファイル
    if ($isQGflg) { //クエリーグループ
        $fdb2csv1 = array();
        foreach ($D1NAMELIST as $keyIndex => $result) {
            $fdb2csv1data = FDB2CSV1_DATA($result['QRYGSQRY']);
            if ($fdb2csv1data['result'] !== true) {
                $rtn = 1;
            } else {
                $fdb2csv1data = umEx($fdb2csv1data['data']);
                $fdb2csv1data = $fdb2csv1data[0];
                //e_log('【削除】FDB2CSV1データ：'.print_r($fdb2csv1data,true));
                $rdbnm = um($fdb2csv1data['D1RDB']);
                $d1webf = um($fdb2csv1data['D1WEBF']);
                $d1cflg = $fdb2csv1data['D1CFLG'];
                // 制御取得の場合バースアイテムカウントの場合、フラグを【''】にするため
                $tmp_d1cflg = $d1cflg;
                $d1edkb = $fdb2csv1data['D1EDKB'];
                $d1outlib = cmMer($fdb2csv1data['D1OUTLIB']);
                $d1outfil = cmMer($fdb2csv1data['D1OUTFIL']);
                $d1tmpf = um(cmHsc($fdb2csv1data['D1TMPF']));
                $d1tros = um($fdb2csv1data['D1TROS']);
                $f2csv1 = array();
                $f2csv1['D1NAME'] = $result['QRYGSQRY'];
                $f2csv1['WSPKEY'] = $wspkey; //$result['PMPKEY'];
                $f2csv1['RDBNM'] = $rdbnm;
                $f2csv1['D1WEBF'] = $d1webf;
                $f2csv1['D1CFLG'] = $d1cflg;
                /***********出力ファイル*********/
                $rsDb2wsoc = getDB2WSOC($db2con, $d1name, $result['PMPKEY'], $isQGflg);
                if ($rsDb2wsoc !== '') {
                    $d1soex = getDirfileNm($rsDb2wsoc['SODIEX']);
                    $soctfl = $rsDb2wsoc['SOCTFL'];
                    $zeromail = $rsDb2wsoc['SO0MAL'] === '1' ? '1' : '';
                    //e_log('【削除】IFSCSV出力：'.$fdb2csv1data['D1DIRE']);
                    $d1dire = getDirfileNm($rsDb2wsoc['SODIRE']);
                    //e_log('【削除】IFSCSV出力①：'.$d1dire);
                    $d1diex = getDirfileNm($rsDb2wsoc['SODIEX']);
                    //e_log('【削除】IFSXLS出力①KSKd1diex：' . $d1diex);
                    //　クエリーのゼローメールチェック
                    $zeromail = $rsDb2wsoc['SO0MAL'] == 1 ? '1' : '';
                    //スケジュールの出力ファイル
                    $d1outse = cmMer($rsDb2wsoc['SOOUTSE']);
                    $d1outsr = cmMer($rsDb2wsoc['SOOUTSR']);
                    //
                    $d1outsa = cmMer($rsDb2wsoc['SOOUTSA']);
                } else {
                    $zeromail = '';
                }
                /***********出力ファイル*********/
                $f2csv1['D1DIRE'] = $d1dire;
                $f2csv1['D1DIEX'] = $d1diex;
                $f2csv1['D1EDKB'] = $d1edkb;
                $f2csv1['D1TMPF'] = $d1tmpf;
                $f2csv1['D1TROS'] = $D1TROS;
                $f2csv1['ZEROMAIL'] = $zeromail;
                $f2csv1['TMP_D1CFLG'] = $tmp_d1cflg;
                //スケジュールの出力ファイル
                $f2csv1['D1OUTLIB'] = $d1outlib;
                $f2csv1['D1OUTFIL'] = $d1outfil;
                $f2csv1['D1OUTSE'] = $d1outse;
                $f2csv1['D1OUTSR'] = $d1outsr;
                $f2csv1['D1OUTSA'] = $d1outsa;
                $f2csv1['D1SOEX'] = $d1soex;
                $f2csv1['SOCTFL'] = $soctfl;
                $f2csv1['ZEROMAIL'] = $zeromail;
                //制御のためデーフォト
                $f2csv1['SEIGYOFLG'] = 0;
                $f2csv1['RSMASTERARR'] = array();
                $f2csv1['RSSHUKEIARR'] = array();
                $fdb2csv1[$result['QRYGSQRY']][] = $f2csv1;
            }
        }
    } else {
        $fdb2csv1data = FDB2CSV1_DATA($d1name);
        if ($fdb2csv1data['result'] !== true) {
            $rtn = 1;
        } else {
            $fdb2csv1data = umEx($fdb2csv1data['data']);
            $fdb2csv1data = $fdb2csv1data[0];
            //e_log('【削除】FDB2CSV1データ：'.print_r($fdb2csv1data,true));
            $rdbnm = um($fdb2csv1data['D1RDB']);
            $d1webf = $fdb2csv1data['D1WEBF'];
            $d1cflg = $fdb2csv1data['D1CFLG'];
            // 制御取得の場合バースアイテムカウントの場合、フラグを【''】にするため
            $tmp_d1cflg = $d1cflg;
            //e_log('【削除】IFSCSV出力：'.$fdb2csv1data['D1DIRE']);
            $d1dire = getDirfileNm($fdb2csv1data['D1DIRE']);
            //e_log('【削除】IFSCSV出力①：'.$d1dire);
            $d1diex = getDirfileNm($fdb2csv1data['D1DIEX']);
            //e_log('【削除】IFSXLS出力①Khind1diex：' . $d1diex);
            $d1edkb = $fdb2csv1data['D1EDKB'];
            $d1tmpf = um(cmHsc($fdb2csv1data['D1TMPF']));
            $d1tros = um($fdb2csv1data['D1TROS']);
            //　クエリーのゼローメールチェック
            $zeromail = $fdb2csv1data['D10MAL'] == 1 ? '1' : '';
            //スケジュールの出力ファイル
            $d1outlib = cmMer($fdb2csv1data['D1OUTLIB']);
            $d1outfil = cmMer($fdb2csv1data['D1OUTFIL']);
            $d1outse = cmMer($fdb2csv1data['D1OUTSE']);
            $d1outsr = cmMer($fdb2csv1data['D1OUTSR']);
            //
            $d1outsa = cmMer($fdb2csv1data['D1OUTSA']);
        }
        if ($rtn !== 1) {
            $fdb2csv1 = array();
            $fdb2csv1['D1NAME'] = $d1name;
            $fdb2csv1['WSPKEY'] = $wspkey;
            $fdb2csv1['RDBNM'] = $rdbnm;
            $fdb2csv1['D1WEBF'] = $d1webf;
            $fdb2csv1['D1CFLG'] = $d1cflg;
            $fdb2csv1['D1DIRE'] = $d1dire;
            $fdb2csv1['D1DIEX'] = $d1diex;
            $fdb2csv1['D1EDKB'] = $d1edkb;
            $fdb2csv1['D1TMPF'] = $d1tmpf;
            $fdb2csv1['D1TROS'] = $D1TROS;
            $fdb2csv1['ZEROMAIL'] = $zeromail;
            $fdb2csv1['TMP_D1CFLG'] = $tmp_d1cflg;
            //スケジュールの出力ファイル
            $fdb2csv1['D1OUTLIB'] = $d1outlib;
            $fdb2csv1['D1OUTFIL'] = $d1outfil;
            $fdb2csv1['D1OUTSE'] = $d1outse;
            $fdb2csv1['D1OUTSR'] = $d1outsr;
            //
            $fdb2csv1['D1OUTSA'] = $d1outsa;
        }
    }
    //sqlクエリー実行権限はない時
    if($licenseSql === false && !$isQGflg && $d1cflg === '1'){
        error_log('SQL Query Fail***************');
        exit();
    }
    //FDB2CSV1からの定義情報取得
    if ($rtn !== 1) {
        // ピボットのメールの場合出力先パース
        if ($isQGflg == true) { //クエリーグループ
            //e_log('OK KSK D1NAMELIST ==>>> '.print_r($D1NAMELIST,true));
            foreach ($D1NAMELIST as $keyIndex => $result) {
                //クエリーグループのため一番最後のクエリーがピボットと制御設定をもらう
                if ($result['LASTFLG'] === '1') {
                    if ($result['PMPKEY'] !== '') {
                        if ($licenseIFS) {
                            $rsDb2wsoc = getDB2WSOC($db2con, $d1name, $result['PMPKEY'], $isQGflg);
                            if ($rsDb2wsoc !== '') {
                                $d1soex = getDirfileNm($rsDb2wsoc['SODIEX']);
                                $soctfl = $rsDb2wsoc['SOCTFL'];
                                $zeromail = $rsDb2wsoc['SO0MAL'] === '1' ? '1' : '';
                            } else {
                                $zeromail = '';
                            }
                        }
                        $fdb2csv1[$result['QRYGSQRY']][0]['D1SOEX'] = $d1soex;
                        $fdb2csv1[$result['QRYGSQRY']][0]['SOCTFL'] = $soctfl;
                        $fdb2csv1[$result['QRYGSQRY']][0]['ZEROMAIL'] = $zeromail;
                    } else {
                        if($licenseSeigyo){
                            //制御取得のチェック
                            $rsSeigyoData = cmGetSeigyoData($db2con, $result['QRYGSQRY']);
                            if ($rsSeigyoData['RESULT'] === 1) {
                                $rtn = 1;
                            } else {
                                //e_log('制御データ取得：' . print_r($rsSeigyoData, true));
                                $fdb2csv1[$result['QRYGSQRY']][0]['SEIGYOFLG'] = $rsSeigyoData['SEIGYOFLG'];
                                $fdb2csv1[$result['QRYGSQRY']][0]['RSMASTERARR'] = $rsSeigyoData['RSMASTER'];
                                $fdb2csv1[$result['QRYGSQRY']][0]['RSSHUKEIARR'] = $rsSeigyoData['RSSHUKEI'];
                            }
                            if ($fdb2csv1[$result['QRYGSQRY']][0]['SEIGYOFLG'] == 1) {
                                $rst = cmLevelFieldCheck($db2con, $result['QRYGSQRY'], $fdb2csv1[$result['QRYGSQRY']][0]['RSMASTERARR'], $fdb2csv1[$result['QRYGSQRY']][0]['RSSHUKEIARR']);
                                if ($rst['result'] !== true) {
                                    //クエリー　＊showMsg($rst['result'],array('クエリー','カラム'))
                                    $errMsg = showMsg($rst['result'], array('クエリー', 'カラム'));
                                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, $isQGflg);
                                    $rtn = 1;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if ($wspkey !== '') {
                if ($licenseIFS) {
                    $rsDb2wsoc = getDB2WSOC($db2con, $d1name, $wspkey, $isQGflg);
                    if ($rsDb2wsoc !== '') {
                        $d1soex = getDirfileNm($rsDb2wsoc['SODIEX']);
                        $soctfl = $rsDb2wsoc['SOCTFL'];
                        $zeromail = $rsDb2wsoc['SO0MAL'] === '1' ? '1' : '';
                    } else {
                        $zeromail = '';
                    }
                }
                //e_log("##########exeMailSend.php d1soex->".$d1soex.print_r($rsDb2wsoc,true));
                $fdb2csv1['D1SOEX'] = $d1soex;
                $fdb2csv1['SOCTFL'] = $soctfl;
                $fdb2csv1['ZEROMAIL'] = $zeromail;
                //  e_log("##########exeMailSend.php $fdb2csv1->".print_r($fdb2csv1,true));
                
            } else {
                if($licenseSeigyo){
                    //制御取得のチェック
                    $rsSeigyoData = cmGetSeigyoData($db2con, $d1name);
                    if ($rsSeigyoData['RESULT'] === 1) {
                        $rtn = 1;
                    } else {
                        //e_log('制御データ取得：' . print_r($rsSeigyoData, true));
                        $fdb2csv1['SEIGYOFLG'] = $rsSeigyoData['SEIGYOFLG'];
                        $fdb2csv1['RSMASTERARR'] = $rsSeigyoData['RSMASTER'];
                        $fdb2csv1['RSSHUKEIARR'] = $rsSeigyoData['RSSHUKEI'];
                    }
                    if ($fdb2csv1['SEIGYOFLG'] == 1) {
                        $rst = cmLevelFieldCheck($db2con, $d1name, $fdb2csv1['RSMASTERARR'], $fdb2csv1['RSSHUKEIARR']);
                        if ($rst['result'] !== true) {
                            //クエリー　＊showMsg($rst['result'],array('クエリー','カラム'))
                            $errMsg = showMsg($rst['result'], array('クエリー', 'カラム'));
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $AmailddLst);
                            $rtn = 1;
                        }
                    }
                }
            }
        }
    }
    // 定義のCL情報取得
    if ($licenseCl) {
        if ($rtn !== 1) {
            if ($isQGflg) { //クエリーグループ
                foreach ($D1NAMELIST as $keyIndex => $result) {
                    $rsCLChk = chkFDB2CSV1PG($db2con, $result['QRYGSQRY']);
                    if ($rsCLChk['result'] !== true) {
                        $rtn = 1;
                        $msg = $rsCLChk['result'];
                        //クエリー　＊CL連携情報の取得に失敗しました。
                        $errMsg = showMsg('FAIL_FUNC', array('CL連携情報の取得')); //"CL連携情報の取得に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, $isQGflg);
                    } else {
                        if (count($rsCLChk['data']) > 0) {
                            $fdb2csv1pg = $rsCLChk['data'];
                            $fdb2csv1[$result['QRYGSQRY']][0]['DGBFLG'] = $fdb2csv1pg['DGBFLG'] === '' ? false : true;
                            $fdb2csv1[$result['QRYGSQRY']][0]['DGAFLG'] = $fdb2csv1pg['DGAFLG'] === '' ? false : true;
                            $fdb2csv1[$result['QRYGSQRY']][0]['FDB2CSV1PG'] = $fdb2csv1pg;
                        } else {
                            $fdb2csv[$result['QRYGSQRY']][0]['DGBFLG'] = false;
                            $fdb2csv[$result['QRYGSQRY']][0]['DGBFLG'] = false;
                        }
                    }
                }
            } else {
                $rsCLChk = chkFDB2CSV1PG($db2con, $d1name);
                if ($rsCLChk['result'] !== true) {
                    $rtn = 1;
                    $msg = $rsCLChk['result'];
                    //クエリー　＊CL連携情報の取得に失敗しました。
                    $errMsg = showMsg('FAIL_FUNC', array('CL連携情報の取得')); //"CL連携情報の取得に失敗しました。";
                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                } else {
                    if (count($rsCLChk['data']) > 0) {
                        $fdb2csv1pg = $rsCLChk['data'];
                        $fdb2csv1['DGBFLG'] = $fdb2csv1pg['DGBFLG'] === '' ? false : true;
                        $fdb2csv1['DGAFLG'] = $fdb2csv1pg['DGAFLG'] === '' ? false : true;
                        $fdb2csv1['FDB2CSV1PG'] = $fdb2csv1pg;
                    } else {
                        $fdb2csv['DGBFLG'] = false;
                        $fdb2csv['DGBFLG'] = false;
                    }
                }
            }
        }
    }
    if ($rtn !== 1) {
        // クエリー実行してスケージュル用ワークテーブル取得
        if (count($burstItmLst) !== 0) {
            if ($isQGflg) { //クエリーグループ
                foreach ($D1NAMELIST as $keyIndex => $result) {
                    $noBurstFlg = false;
                    // 5250の定義の場合
                    $getFDB2CSV1 = $fdb2csv1[$result['QRYGSQRY']][0];
                    if ($getFDB2CSV1['D1WEBF'] !== '1') {
                        if ($result['PMPKEY'] === '') {
                            if ($getFDB2CSV1['SEIGYOFLG'] == 1) {
                                if ($burstItmLst[0]['WABAFLD'] === '') {
                                    // バーストアイテム無しの抽出有り
                                    // ワークテーブルはスケージュルのワークテーブル
                                    $noBurstFlg = true;
                                    $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                } else {
                                    // メールデータテーブル作成してスケージュルワークテーブル作る
                                    $noBurstFlg = false;
                                    $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                }
                            } else {
                                // 制御の取得じゃない場合、バーストアイテム無視
                                // ワークテーブルはスケージュルのワークテーブル
                                $noBurstFlg = true;
                                $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                            }
                        } else {
                            if ($burstItmLst[0]['WABAFLD'] === '') {
                                // バーストアイテム無しの抽出有り
                                // ワークテーブルはスケージュルのワークテーブル
                                $noBurstFlg = true;
                                $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                            } else {
                                // メールデータテーブル作成してスケージュルワークテーブル作る
                                $noBurstFlg = false;
                                $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                            }
                        }
                    } else {
                        if ($getFDB2CSV1['D1CFLG'] === '1') {
                            // WEBSQLの定義の場合
                            if ($result['PMPKEY'] === '') {
                                if ($getFDB2CSV1['SEIGYOFLG'] == 1) {
                                    if ($burstItmLst[0]['WABAFLD'] === '') {
                                        // バーストアイテム無しの抽出有り
                                        // ワークテーブルはスケージュルのワークテーブル
                                        $noBurstFlg = true;
                                        $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                    } else {
                                        // メールデータテーブル作成してスケージュルワークテーブル作る
                                        $noBurstFlg = false;
                                        $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                    }
                                } else {
                                    // 制御の取得じゃない場合、バーストアイテム無視
                                    // ワークテーブルはスケージュルのワークテーブル
                                    $noBurstFlg = true;
                                    $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                }
                            } else {
                                // ピボットデータ取得
                                if ($burstItmLst[0]['WABAFLD'] === '') {
                                    // バーストアイテム無しの抽出有り
                                    // ワークテーブルはスケージュルのワークテーブル
                                    $noBurstFlg = true;
                                    $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                } else {
                                    // メールデータテーブル作成してスケージュルワークテーブル作る
                                    $noBurstFlg = false;
                                    $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                }
                            }
                        } else {
                            // WEB通常の定義の場合
                            if ($result['PMPKEY'] === '') {
                                if ($getFDB2CSV1['SEIGYOFLG'] == 1) {
                                    if ($burstItmLst[0]['WABAFLD'] === '') {
                                        // バーストアイテム無しの抽出有り
                                        // ワークテーブルはスケージュルのワークテーブル
                                        $noBurstFlg = true;
                                        $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                    } else {
                                        // メールデータテーブル作成してスケージュルワークテーブル作る
                                        $noBurstFlg = false;
                                        $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                    }
                                } else {
                                    // 制御の取得じゃない場合、バーストアイテム無視
                                    // ワークテーブルはスケージュルのワークテーブル
                                    $noBurstFlg = true;
                                    $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                }
                            } else {
                                // ピボットデータ取得
                                if ($burstItmLst[0]['WABAFLD'] === '') {
                                    // バーストアイテム無しの抽出有り
                                    // ワークテーブルはスケージュルのワークテーブル
                                    $noBurstFlg = true;
                                    $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                } else {
                                    // メールデータテーブル作成してスケージュルワークテーブル作る
                                    $noBurstFlg = false;
                                    $fdb2csv1[$result['QRYGSQRY']][0]['BustFlg'] = $noBurstFlg;
                                }
                            }
                        }
                    }
                }
            } else {
                $noBurstFlg = false;
                // 5250の定義の場合
                if ($fdb2csv1['D1WEBF'] !== '1') {
                    if ($wspkey === '') {
                        if ($fdb2csv1['SEIGYOFLG'] == 1) {
                            if ($burstItmLst[0]['WABAFLD'] === '') {
                                // バーストアイテム無しの抽出有り
                                // ワークテーブルはスケージュルのワークテーブル
                                $noBurstFlg = true;
                            } else {
                                // メールデータテーブル作成してスケージュルワークテーブル作る
                                $noBurstFlg = false;
                            }
                        } else {
                            // 制御の取得じゃない場合、バーストアイテム無視
                            // ワークテーブルはスケージュルのワークテーブル
                            $noBurstFlg = true;
                        }
                    } else {
                        if ($burstItmLst[0]['WABAFLD'] === '') {
                            // バーストアイテム無しの抽出有り
                            // ワークテーブルはスケージュルのワークテーブル
                            $noBurstFlg = true;
                        } else {
                            // メールデータテーブル作成してスケージュルワークテーブル作る
                            $noBurstFlg = false;
                        }
                    }
                } else {
                    if ($fdb2csv1['D1CFLG'] === '1') {
                        // WEBSQLの定義の場合
                        if ($wspkey === '') {
                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                if ($burstItmLst[0]['WABAFLD'] === '') {
                                    // バーストアイテム無しの抽出有り
                                    // ワークテーブルはスケージュルのワークテーブル
                                    $noBurstFlg = true;
                                } else {
                                    // メールデータテーブル作成してスケージュルワークテーブル作る
                                    $noBurstFlg = false;
                                }
                            } else {
                                // 制御の取得じゃない場合、バーストアイテム無視
                                // ワークテーブルはスケージュルのワークテーブル
                                $noBurstFlg = true;
                            }
                        } else {
                            // ピボットデータ取得
                            if ($burstItmLst[0]['WABAFLD'] === '') {
                                // バーストアイテム無しの抽出有り
                                // ワークテーブルはスケージュルのワークテーブル
                                $noBurstFlg = true;
                            } else {
                                // メールデータテーブル作成してスケージュルワークテーブル作る
                                $noBurstFlg = false;
                            }
                        }
                    } else {
                        // WEB通常の定義の場合
                        if ($wspkey === '') {
                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                if ($burstItmLst[0]['WABAFLD'] === '') {
                                    // バーストアイテム無しの抽出有り
                                    // ワークテーブルはスケージュルのワークテーブル
                                    $noBurstFlg = true;
                                } else {
                                    // メールデータテーブル作成してスケージュルワークテーブル作る
                                    $noBurstFlg = false;
                                }
                            } else {
                                // 制御の取得じゃない場合、バーストアイテム無視
                                // ワークテーブルはスケージュルのワークテーブル
                                $noBurstFlg = true;
                            }
                        } else {
                            // ピボットデータ取得
                            if ($burstItmLst[0]['WABAFLD'] === '') {
                                // バーストアイテム無しの抽出有り
                                // ワークテーブルはスケージュルのワークテーブル
                                $noBurstFlg = true;
                            } else {
                                // メールデータテーブル作成してスケージュルワークテーブル作る
                                $noBurstFlg = false;
                            }
                        }
                    }
                }
            }
        }
    }
    if ($rtn !== 1) {
        if ($isQGflg) { //クエリーグループ
            $kcount = 0;
            foreach ($D1NAMELIST as $keyIndex => $result) {
                /*$kcount++;
                if($kcount===10){
                    e_log('forloooping count 10 time');
                    break;
                }*/
                $wrktblnm = '';
                $getFDB2CSV1 = $fdb2csv1[$result['QRYGSQRY']][0];
                $paramQGData = array();
                $paramQGData['ISQGFLG'] = $isQGflg;
                $paramQGData['LASTFLG'] = $result['LASTFLG'];
                $paramQGData['D1NAME'] = $result['QRYGSQRY'];
                $paramQGData['KEYINDEX'] = $keyIndex;
                $paramQGData['RDBNameList'] = $RDBNameList;
                $paramQGData['RDBNameObj'] = $RDBNameObj;
                $paramQGData['PHPNameList'] = $PHPNameList;
                //DBの接続のためArrayを使う
                if ($getFDB2CSV1['D1WEBF'] === '1') {
                    if (cmMer($getFDB2CSV1['D1RDB']) === '' || cmMer($getFDB2CSV1['D1RDB']) === RDB || cmMer($getFDB2CSV1['D1RDB']) === RDBNAME) {
                        $sPHPKey = array_search(RDB, $PHPNameList);
                        //DB CONNECTION
                        if ($keyIndex === 0 || $sPHPKey === false) {
                            $PHPNameList[] = RDB;
                            $option = array('i5_naming' => DB2_I5_NAMING_ON);
                            $db2LibCon = cmDb2ConOption($option);
                            $db2LibConTmp = $db2LibCon;
                            //e_log('【DB CONNECTION NEW】');
                        } else {
                            //e_log('【DB CONNECTION OVERRIDE】');
                            $db2LibCon = $db2LibConTmp;
                        }
                    } else {
                        $sRDBKey = array_search(cmMer($getFDB2CSV1['D1RDB']), $RDBNameList);
                        if ($sRDBKey === false) { //ＲＤＢ名が新しいなら動く
                            $RDBNameList[] = cmMer($getFDB2CSV1['D1RDB']);
                            //DB CONNECTION
                            $option = array('i5_naming' => DB2_I5_NAMING_ON);
                            $db2LibCon = cmDb2ConOptionRDB($option);
                            $RDBNameObj[$getFDB2CSV1['D1RDB']][] = $db2LibCon;
                            e_log('【RDB CONNECTION NEW】');
                        } else {
                            e_log('【RDB CONNECTION OVERRIDE】');
                            $db2LibCon = $RDBNameObj[$getFDB2CSV1['D1RDB']][0];
                        }
                    }
                    $paramQGData['RDBNameList'] = $RDBNameList;
                    $paramQGData['RDBNameObj'] = $RDBNameObj;
                    $paramQGData['PHPNameList'] = $PHPNameList;
                    $paramQGData['db2LibCon'] = $db2LibCon;
                }
                //e_log('【削除】FDB2CSV1データ：'.print_r($fdb2csv1,true));
                foreach ($burstItmLst as $key => $value) {
                    $mailAddLst = array();
                    $mailAddLst = getMailAddlst($db2con, $burstItmLst[$key]);
                    if ($mailAddLst['result'] !== true) {
                        // e_log(showMsg($rs['result'], array('exeMail.php', 'getMailAddlst')), '1');
                        $errMsg = showMsg('FAIL_FUNC', array('メールアドレスリスト取得'));
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $result['PMPKEY'], $errMsg, $isQGflg);
                    } else {
                        $mailAddLst = $mailAddLst['data'];
                        if ($value['WABAFLD'] === '') {
                            $burstItmLst[$key] = array();
                        }
                        // 5250の定義の場合定義実行
                        if ($getFDB2CSV1['D1WEBF'] !== '1') {
                            if ($result['PMPKEY'] !== '') {
                                // ピボットデータ取得
                                if ($key === 0) {
                                    if ($fdb2csv1[$result['QRYGSQRY']][0]['BustFlg']) {
                                        // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、ピボットワークテーブル：●　、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, $paramQGData, $hiswspkey);
                                    } else {
                                        // ①⇒バーストあり取得　【ワークテーブル　：●　、制御ワークテーブル：●　、IFS用ピボットワークデーブル：●、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, $paramQGData, $hiswspkey);
                                    }
                                    $wrktblnm = $rs['WRKTBLNAME'];
                                } else {
                                    // ②バースト有りのデータ取得　【ワークテーブル　：①　、ピボットワークテーブル：●　、IFS：×】
                                    $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, $paramQGData, $hiswspkey);
                                    $wrktblnm = $rs['WRKTBLNAME'];
                                }
                            } else {
                                //制御データ取得
                                if ($getFDB2CSV1['SEIGYOFLG'] == 1) {
                                    if ($key === 0) {
                                        if ($fdb2csv1[$result['QRYGSQRY']][0]['BustFlg']) {
                                            // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、制御ワークテーブル：●　、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS, $licenseFileOut,$result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, $paramQGData, $hiswspkey);
                                        } else {
                                            // ①⇒バーストあり取得　【ワークテーブル　：●　、制御ワークテーブル：●　、IFS用ピボットワークデーブル：●、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, $paramQGData, $hiswspkey);
                                        }
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                    } else {
                                        // ②⇒バースト無し全部のデータ取得　【ワークテーブル　：①　、制御ワークテーブル：●　、IFS：×】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, $paramQGData, $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                    }
                                } else {
                                    // 普通データ取得
                                    if ($key === 0) {
                                        // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, $paramQGData, $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                    } else {
                                        // ②⇒バースト無し全部のデータ取得　【ワークテーブル　：①　、IFS：×】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, $paramQGData, $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                    }
                                }
                            }
                        } else {
                            if ($getFDB2CSV1['D1CFLG'] === '1') {
                                //WEBSQL定義の場合
                                if ($result['PMPKEY'] !== '') {
                                    // ピボットデータ取得
                                    if ($key === 0) {
                                        if ($fdb2csv1[$result['QRYGSQRY']][0]['BustFlg']) {
                                            // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、ピボットデータのワークテーブル：●　、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, $paramQGData, $hiswspkey);
                                        } else {
                                            // ②⇒バーストあり取得　【ワークテーブル　：×　、IFS用ピボットワークデーブル：●、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, $paramQGData, $hiswspkey);
                                        }
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                    } else {
                                        // ③バースト有りのデータ取得　【ワークテーブル　：●　、ピボットワークテーブル：●　、IFS：×】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, $paramQGData, $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                    }
                                } else {
                                    //制御データ取得
                                    if ($getFDB2CSV1['SEIGYOFLG'] == 1) {
                                        if ($key === 0) {
                                            if ($fdb2csv1[$result['QRYGSQRY']][0]['BustFlg']) {
                                                // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、制御のワークテーブル：●　、IFS：●】
                                                $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, $paramQGData, $hiswspkey);
                                            } else {
                                                // ②⇒バーストあり取得　【ワークテーブル　：×　、IFS用ピボットワークデーブル：●、IFS：●】
                                                $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, $paramQGData, $hiswspkey);
                                            }
                                            $wrktblnm = $rs['WRKTBLNAME'];
                                            $fdb2csv1[$result['QRYGSQRY']][0]['QRYDATA'] = $rs['QRYDATA'];
                                        } else {
                                            // ③バースト有りのデータ取得　【ワークテーブル　：●　、ピボットワークテーブル：●　、IFS：×】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, $paramQGData, $hiswspkey);
                                            $wrktblnm = $rs['WRKTBLNAME'];
                                            $fdb2csv1[$result['QRYGSQRY']][0]['QRYDATA'] = $rs['QRYDATA'];
                                        }
                                    } else {
                                        // 普通データ取得
                                        if ($key === 0) {
                                            // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $licenseFileOut,$result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, $paramQGData, $hiswspkey);
                                            $wrktblnm = $rs['WRKTBLNAME'];
                                            $fdb2csv1[$result['QRYGSQRY']][0]['QRYDATA'] = $rs['QRYDATA'];
                                        } else {
                                            // ②⇒バースト無し全部のデータ取得　【ワークテーブル　：①　、IFS：×】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, $paramQGData, $hiswspkey);
                                            $wrktblnm = $rs['WRKTBLNAME'];
                                            $fdb2csv1[$result['QRYGSQRY']][0]['QRYDATA'] = $rs['QRYDATA'];
                                        }
                                    }
                                }
                            } else {
                                //WEB通常定義の場合
                                if ($result['PMPKEY'] !== '') {
                                    // ピボットデータ取得
                                    if ($key === 0) {
                                        if ($fdb2csv1[$result['QRYGSQRY']][0]['BustFlg']) {
                                            // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、ピボットデータのワークテーブル：●　、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, $paramQGData, $hiswspkey);
                                        } else {
                                            // ②⇒バーストあり取得　【ワークテーブル　：×　、IFS用ピボットワークデーブル：●、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, $paramQGData, $hiswspkey);
                                        }
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                        $fdb2csv1[$result['QRYGSQRY']][0]['QRYDATA'] = $rs['QRYDATA'];
                                    } else {
                                        // ③バースト有りのデータ取得　【ワークテーブル　：●　、ピボットワークテーブル：●　、IFS：×】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 3, $paramQGData, $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                        $fdb2csv1[$result['QRYGSQRY']][0]['QRYDATA'] = $rs['QRYDATA'];
                                    }
                                } else {
                                    //制御データ取得
                                    e_log('制御データ取得:keyIndex' . $keyIndex . 'KEY:' . $key . 'FDB2CSV1:' . print_r($getFDB2CSV1, true) . 'PARAMQGDATA:' . print_r($paramQGData, true));
                                    if ($getFDB2CSV1['SEIGYOFLG'] == 1) {
                                        if ($key === 0) {
                                            if ($getFDB2CSV1['BustFlg']) {
                                                // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、IFS：●】
                                                $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, $paramQGData, $hiswspkey);
                                            } else {
                                                // ②⇒バーストあり取得　【ワークテーブル　：●、一時制御ワークテーブル　：●　、IFS：●「ワークテーブルより」】
                                                $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, $paramQGData, $hiswspkey);
                                            }
                                            $wrktblnm = $rs['WRKTBLNAME'];
                                            $fdb2csv1[$result['QRYGSQRY']][0]['QRYDATA'] = $rs['QRYDATA'];
                                        } else {
                                            // ③バースト有りのデータ取得　【一時制御ワークテーブル：●　　、IFS：×】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 3, $paramQGData, $hiswspkey);
                                            $wrktblnm = $rs['WRKTBLNAME'];
                                            $fdb2csv1[$result['QRYGSQRY']][0]['QRYDATA'] = $rs['QRYDATA'];
                                        }
                                    } else {
                                        // 普通データ取得
                                        if ($key === 0) {
                                            // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, $paramQGData, $hiswspkey);
                                            $wrktblnm = $rs['WRKTBLNAME'];
                                            $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                        } else {
                                            // ②⇒バースト無し全部のデータ取得　【ワークテーブル：①　、IFS：×】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $result['PMPKEY'], $getFDB2CSV1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, $paramQGData, $hiswspkey);
                                            $wrktblnm = $rs['WRKTBLNAME'];
                                            $fdb2csv1[$result['QRYGSQRY']][0]['QRYDATA'] = $rs['QRYDATA'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $wrktblnm = '';
            //e_log('【削除】FDB2CSV1データ：'.print_r($fdb2csv1,true));
            foreach ($burstItmLst as $key => $value) {
                $mailAddLst = array();
                $mailAddLst = getMailAddlst($db2con, $burstItmLst[$key]);
                if ($mailAddLst['result'] !== true) {
                    e_log(showMsg($rs['result'], array('exeMail.php', 'getMailAddlst')), '1');
                    $errMsg = showMsg('FAIL_FUNC', array('メールアドレスリスト取得'));
                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $isQGflg);
                } else {
                    $mailAddLst = $mailAddLst['data'];
                    if ($value['WABAFLD'] === '') {
                        $burstItmLst[$key] = array();
                    }
                    // 5250の定義の場合定義実行
                    if ($fdb2csv1['D1WEBF'] !== '1') {
                        if ($wspkey !== '') {
                            // ピボットデータ取得
                            if ($key === 0) {
                                if ($noBurstFlg) {
                                    // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、ピボットワークテーブル：●　、IFS：●】
                                    $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, array(), $hiswspkey);
                                } else {
                                    // ①⇒バーストあり取得　【ワークテーブル　：●　、制御ワークテーブル：●　、IFS用ピボットワークデーブル：●、IFS：●】
                                    $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, array(), $hiswspkey);
                                }
                                $wrktblnm = $rs['WRKTBLNAME'];
                            } else {
                                // ②バースト有りのデータ取得　【ワークテーブル　：①　、ピボットワークテーブル：●　、IFS：×】
                                $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, array(), $hiswspkey);
                                $wrktblnm = $rs['WRKTBLNAME'];
                            }
                        } else {
                            //制御データ取得
                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                if ($key === 0) {
                                    if ($noBurstFlg) {
                                        // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、制御ワークテーブル：●　、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, array(), $hiswspkey);
                                    } else {
                                        // ①⇒バーストあり取得　【ワークテーブル　：●　、制御ワークテーブル：●　、IFS用ピボットワークデーブル：●、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, array(), $hiswspkey);
                                    }
                                    $wrktblnm = $rs['WRKTBLNAME'];
                                } else {
                                    // ②⇒バースト無し全部のデータ取得　【ワークテーブル　：①　、制御ワークテーブル：●　、IFS：×】
                                    $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, array(), $hiswspkey);
                                    $wrktblnm = $rs['WRKTBLNAME'];
                                }
                            } else {
                                // 普通データ取得
                                if ($key === 0) {
                                    // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、IFS：●】
                                    $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, array(), $hiswspkey);
                                    $wrktblnm = $rs['WRKTBLNAME'];
                                } else {
                                    // ②⇒バースト無し全部のデータ取得　【ワークテーブル　：①　、IFS：×】
                                    $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, array(), $hiswspkey);
                                    $wrktblnm = $rs['WRKTBLNAME'];
                                }
                            }
                        }
                    } else {
                        if ($fdb2csv1['D1CFLG'] === '1') {
                            //WEBSQL定義の場合
                            if ($wspkey !== '') {
                                // ピボットデータ取得
                                if ($key === 0) {
                                    if ($noBurstFlg) {
                                        // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、ピボットデータのワークテーブル：●　、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, array(), $hiswspkey);
                                    } else {
                                        // ②⇒バーストあり取得　【ワークテーブル　：×　、IFS用ピボットワークデーブル：●、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, array(), $hiswspkey);
                                    }
                                    $wrktblnm = $rs['WRKTBLNAME'];
                                } else {
                                    // ③バースト有りのデータ取得　【ワークテーブル　：●　、ピボットワークテーブル：●　、IFS：×】
                                    $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, array(), $hiswspkey);
                                    $wrktblnm = $rs['WRKTBLNAME'];
                                }
                            } else {
                                //制御データ取得
                                if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                    if ($key === 0) {
                                        if ($noBurstFlg) {
                                            // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、制御のワークテーブル：●　、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, array(), $hiswspkey);
                                        } else {
                                            // ②⇒バーストあり取得　【ワークテーブル　：×　、IFS用ピボットワークデーブル：●、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, array(), $hiswspkey);
                                        }
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                        $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                    } else {
                                        // ③バースト有りのデータ取得　【ワークテーブル　：●　、ピボットワークテーブル：●　、IFS：×】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, array(), $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                        $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                    }
                                } else {
                                    // 普通データ取得
                                    if ($key === 0) {
                                        // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, array(), $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                        $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                    } else {
                                        // ②⇒バースト無し全部のデータ取得　【ワークテーブル　：①　、IFS：×】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, array(), $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                        $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                    }
                                }
                            }
                        } else {
                            //WEB通常定義の場合
                            if ($wspkey !== '') {
                                // ピボットデータ取得
                                if ($key === 0) {
                                    if ($noBurstFlg) {
                                        // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、ピボットデータのワークテーブル：●　、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, array(), $hiswspkey);
                                    } else {
                                        // ②⇒バーストあり取得　【ワークテーブル　：×　、IFS用ピボットワークデーブル：●、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, array(), $hiswspkey);
                                    }
                                    $wrktblnm = $rs['WRKTBLNAME'];
                                    $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                } else {
                                    // ③バースト有りのデータ取得　【ワークテーブル　：●　、ピボットワークテーブル：●　、IFS：×】
                                    $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 3, array(), $hiswspkey);
                                    $wrktblnm = $rs['WRKTBLNAME'];
                                    $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                }
                            } else {
                                //制御データ取得
                                if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                    if ($key === 0) {
                                        if ($noBurstFlg) {
                                            // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、IFS：●】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, array(), $hiswspkey);
                                        } else {
                                            // ②⇒バーストあり取得　【ワークテーブル　：●、一時制御ワークテーブル　：●　、IFS：●「ワークテーブルより」】
                                            $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 2, array(), $hiswspkey);
                                        }
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                        $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                    } else {
                                        // ③バースト有りのデータ取得　【一時制御ワークテーブル：●　　、IFS：×】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 3, array(), $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                        $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                    }
                                } else {
                                    // 普通データ取得
                                    if ($key === 0) {
                                        // ①⇒バースト無し全部のデータ取得　【ワークテーブル　：●　、IFS：●】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 1, array(), $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                        $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                    } else {
                                        // ②⇒バースト無し全部のデータ取得　【ワークテーブル：①　、IFS：×】
                                        $rs = exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmLst[$key], $mailAddLst, $wrktblnm, 0, array(), $hiswspkey);
                                        $wrktblnm = $rs['WRKTBLNAME'];
                                        $fdb2csv1['QRYDATA'] = $rs['QRYDATA'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
function getWSQGFLGSCH($db2con, $d1name) {
    $data = array();
    $rtn = array();
    $qryData = array();
    $strSQL = '';
    $strSQL.= " SELECT A.* FROM ";
    $strSQL.= " ( ";
    $strSQL.= " SELECT  D1NAME,'0' AS WSQGFLG FROM FDB2CSV1 ";
    $strSQL.= " UNION  ";
    $strSQL.= " SELECT DISTINCT QRYGSID AS D1NAME,'1' AS WSQGFLG  FROM DB2QRYGS ";
    $strSQL.= " ) AS A ";
    $strSQL.= " WHERE A.D1NAME = ? ";
    $params = array($d1name);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                $data = umEx($data, true);
            }
        }
        $rtn = array('result' => true, 'data' => $data);
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * メール対象の定義実行
 * ミィッミィッモー
 * $paramQGDataクエリーグループのため、必要なデータが入れる
 *-------------------------------------------------------*
*/
function exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $licenseIFS,$licenseFileOut, $wspkey, $fdb2csv1, $burstItmInfo, $mailAddLst, $wrktblnm, $noBurstFlg = 0, $paramQGData = array(), $hiswspkey) {
    if (count($burstItmLst) > 0) {
        if (isset($burstItmInfo[0]['DUMMY'])) {
            $dummy = $burstItmInfo[0]['DUMMY'];
            e_log(print_r($burstItmInfo, true) . "dummy is ");
            if ($dummy === true) {
                $burstItmInfo = array();
            }
        }
    }
    $RTCD = ' ';
    $currentDate = date('Ymd');
    $exeFlg = true;
    $wspkey = cmMer($wspkey);
    $templateErrCnt = 0;
    //スケージュルワークテーブル作成フラグ取得
    if ($noBurstFlg === 0) {
        $exeFlg = false;
    }
    //クエリーグループＦＬＧ
    $isQGflg = (count($paramQGData) > 0) ? $paramQGData['ISQGFLG'] : false;
    // ワーブで作成したクエリーの場合
    if ($isQGflg) { //クエリーグループ
        if ($fdb2csv1['D1WEBF'] === '1') {
            if ($exeFlg) {
                // SQLで作成したクエリー
                if ($fdb2csv1['D1CFLG'] === '1') {
                    // 定義実行するためのSQLクエリー条件情報取得
                    if ($rtn !== 1) {
                        $res = fnGetBSQLCND($db2con, $paramQGData['D1NAME']);
                        if ($res['result'] !== true) {
                            $rtn = 1;
                            $msg = showMsg($res['result']);
                        } else {
                            $data = umEx($res['data']);
                        }
                        $cnddatArr = $data;
                        if (count($data) > 0) {
                            $cnddatArr = cmCalendarDataParamSQL($currentDate, $cnddatArr);
                            $resChkCond = chkSQLParamData($db2con, $paramQGData['D1NAME'], $cnddatArr);
                            if ($resChkCond['RTN'] > 0) {
                                $rtn = 1;
                                $msg = $resChkCond['MSG'];
                            } else {
                                $cndParamList = $resChkCond['DATA']['BASEFRMPARAMDATA'];
                            }
                        }
                        if ($rtn === 1) {
                            //クエリー　＊クエリーの実行準備に失敗しました
                            $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                        }
                    }
                    // 定義実行するためのSQLクエリー情報取得
                    if ($rtn !== 1) {
                        $resSQLInfo = getQrySQLData($db2con, $paramQGData['D1NAME']);
                        if ($resSQLInfo['RTN'] !== 0) {
                            $rtn = 1;
                            $msg = $resSQLInfo['MSG'];
                        } else {
                            $qryData = $resSQLInfo['QRYDATA'];
                        }
                        if ($rtn === 1) {
                            //クエリー　＊クエリーの実行準備に失敗しました
                            $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                        }
                    }
                } else {
                    // 通常で作成したクエリー
                    if ($rtn !== 1) {
                        // 定義実行するための通常クエリー条件情報取得
                        $rs = fnBQRYCNDDAT($db2con, $paramQGData['D1NAME']);
                        if ($rs['result'] !== true) {
                            $rtn = 1;
                            $msg = showMsg($rs['result']);
                        } else {
                            $data = $rs['data'];
                        }
                        $data = umEx($data);
                        if (count($data) > 0) {
                            $cnddatArr = fnCreateCndData($data);
                            $cnddatArr = cmCalendarDataParamWeb($currentDate, $cnddatArr);
                            $resChkCond = chkCondParamData($db2con, $paramQGData['D1NAME'], $cnddatArr);
                            if ($resChkCond['RTN'] > 0) {
                                $rtn = 1;
                                $msg = $resChkCond['MSG'];
                            }
                        }
                        if ($rtn === 1) {
                            //クエリー　＊クエリーの実行準備に失敗しました
                            $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                        }
                    }
                    // 定義実行するための通常クエリー情報取得
                    if ($rtn !== 1) {
                        //e_log('【削除】実行種類①:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                        if ($wspkey === '') {
                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                if (count($burstItmInfo) === 0) {
                                    //e_log('【削除】制御データ取得SQL①：'.count($burstItmInfo).'data:'.print_r($burstItmInfo,true));
                                    $resExeSql = runExecuteSQLSEIGYORN($db2con, $paramQGData['D1NAME']);
                                    if ($resExeSql['RTN'] !== 0) {
                                        $rtn = 1;
                                        $msg = $resExeSql['MSG'];
                                    } else {
                                        $qryData = $resExeSql;
                                        $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                    }
                                } else {
                                    $resExeSql = runExecuteSQLSEIGYORN($db2con, $paramQGData['D1NAME'], NULL, '', $burstItmInfo);
                                    //e_log('【削除】制御データ取得SQL：'.print_r($resExeSql,true));
                                    if ($resExeSql['RTN'] !== 0) {
                                        $rtn = 1;
                                        $msg = $resExeSql['MSG'];
                                    } else {
                                        $qryData = $resExeSql;
                                        $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                    }
                                }
                            } else {
                                $resExeSql = runExecuteSQL($db2con, $paramQGData['D1NAME']);
                                if ($resExeSql['RTN'] !== 0) {
                                    $rtn = 1;
                                } else {
                                    $qryData = $resExeSql;
                                    $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                }
                            }
                        } else {
                            //e_log('メールピボットSQL実行:' . $d1name . 'ピボットキー' . $wspkey);
                            if (count($burstItmInfo) === 0) {
                                $resExeSql = runExecuteSQLPIVOT($db2con, $paramQGData['D1NAME'], $wspkey);
                                if ($resExeSql['RTN'] !== 0) {
                                    $rtn = 1;
                                } else {
                                    $qryData = $resExeSql;
                                    $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                }
                            } else {
                                $resExeSql = runExecuteSQLPIVOT($db2con, $paramQGData['D1NAME'], $wspkey, NULL, $burstItmInfo);
                                if ($resExeSql['RTN'] !== 0) {
                                    $rtn = 1;
                                } else {
                                    $qryData = $resExeSql;
                                    $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                }
                            }
                        }
                    }
                    if ($rtn === 1) {
                        //クエリー　＊クエリーの実行準備に失敗しました
                        $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                    }
                }
            } else {
                $qryData = $fdb2csv1['QRYDATA'];
                unset($fdb2csv1['QRYDATA']);
            }
        }
    } else {
        if ($fdb2csv1['D1WEBF'] === '1') {
            if ($exeFlg) {
                // SQLで作成したクエリー
                if ($fdb2csv1['D1CFLG'] === '1') {
                    // 定義実行するためのSQLクエリー条件情報取得
                    if ($rtn !== 1) {
                        $res = fnGetBSQLCND($db2con, $d1name);
                        if ($res['result'] !== true) {
                            $rtn = 1;
                            $msg = showMsg($res['result']);
                        } else {
                            $data = umEx($res['data']);
                        }
                        $cnddatArr = $data;
                        if (count($data) > 0) {
                            $cnddatArr = cmCalendarDataParamSQL($currentDate, $cnddatArr);
                            $resChkCond = chkSQLParamData($db2con, $d1name, $cnddatArr);
                            if ($resChkCond['RTN'] > 0) {
                                $rtn = 1;
                                $msg = $resChkCond['MSG'];
                            } else {
                                $cndParamList = $resChkCond['DATA']['BASEFRMPARAMDATA'];
                            }
                        }
                        if ($rtn === 1) {
                            //クエリー　＊クエリーの実行準備に失敗しました
                            $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                        }
                    }
                    // 定義実行するためのSQLクエリー情報取得
                    if ($rtn !== 1) {
                        $resSQLInfo = getQrySQLData($db2con, $d1name);
                        if ($resSQLInfo['RTN'] !== 0) {
                            $rtn = 1;
                            $msg = $resSQLInfo['MSG'];
                        } else {
                            $qryData = $resSQLInfo['QRYDATA'];
                        }
                        if ($rtn === 1) {
                            //クエリー　＊クエリーの実行準備に失敗しました
                            $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                        }
                    }
                } else {
                    // 通常で作成したクエリー
                    if ($rtn !== 1) {
                        // 定義実行するための通常クエリー条件情報取得
                        $rs = fnBQRYCNDDAT($db2con, $d1name);
                        if ($rs['result'] !== true) {
                            $rtn = 1;
                            $msg = showMsg($rs['result']);
                        } else {
                            $data = $rs['data'];
                        }
                        $data = umEx($data);
                        if (count($data) > 0) {
                            $cnddatArr = fnCreateCndData($data);
                            $cnddatArr = cmCalendarDataParamWeb($currentDate, $cnddatArr);
                            $resChkCond = chkCondParamData($db2con, $d1name, $cnddatArr);
                            if ($resChkCond['RTN'] > 0) {
                                $rtn = 1;
                                $msg = $resChkCond['MSG'];
                            } else {
                                $cndParamList = $resChkCond['DATA']['BASEDCNDDATA'];
                            }
                        }
                        if ($rtn === 1) {
                            //クエリー　＊クエリーの実行準備に失敗しました
                            $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                        }
                    }
                    // 定義実行するための通常クエリー情報取得
                    if ($rtn !== 1) {
                        //e_log('【削除】実行種類①:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                        if ($wspkey === '') {
                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                if (count($burstItmInfo) === 0) {
                                    //e_log('【削除】制御データ取得SQL①：'.count($burstItmInfo).'data:'.print_r($burstItmInfo,true));
                                    $resExeSql = runExecuteSQLSEIGYORN($db2con, $d1name);
                                    if ($resExeSql['RTN'] !== 0) {
                                        $rtn = 1;
                                        $msg = $resExeSql['MSG'];
                                    } else {
                                        $qryData = $resExeSql;
                                        $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                    }
                                } else {
                                    // $resExeSql = runExecuteSQLSEIGYORN($db2con, $d1name, NULL, '', $burstItmInfo);
                                    if (count($cndParamList) === 0) {
                                        $resExeSql = runExecuteSQLSEIGYORN($db2con, $d1name, NULL, '', $burstItmInfo);
                                    } else {
                                        $resExeSql = runExecuteSQLSEIGYORN($db2con, $d1name, $cndParamList, '', $burstItmInfo);
                                    }
                                    //e_log('【削除】制御データ取得SQL：'.print_r($resExeSql,true));
                                    if ($resExeSql['RTN'] !== 0) {
                                        $rtn = 1;
                                        $msg = $resExeSql['MSG'];
                                    } else {
                                        $qryData = $resExeSql;
                                        $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                    }
                                }
                            } else {
                                //$resExeSql = runExecuteSQL($db2con, $d1name);
                                if (count($cndParamList) === 0) {
                                    $resExeSql = runExecuteSQL($db2con, $d1name);
                                } else {
                                    $resExeSql = runExecuteSQL($db2con, $d1name, $cndParamList);
                                }
                                //e_log("////////////////////////////////////".print_r($cnddatArr,true));
                                if ($resExeSql['RTN'] !== 0) {
                                    $rtn = 1;
                                } else {
                                    $qryData = $resExeSql;
                                    $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                }
                            }
                        } else {
                            //e_log('メールピボットSQL実行:' . $d1name . 'ピボットキー' . $wspkey);
                            if (count($burstItmInfo) === 0) {
                                // $resExeSql = runExecuteSQLPIVOT($db2con, $d1name, $wspkey);
                                if (count($cndParamList) === 0) {
                                    $resExeSql = runExecuteSQLPIVOT($db2con, $d1name, $wspkey);
                                } else {
                                    $resExeSql = runExecuteSQLPIVOT($db2con, $d1name, $wspkey, $cndParamList);
                                }
                                if ($resExeSql['RTN'] !== 0) {
                                    $rtn = 1;
                                } else {
                                    $qryData = $resExeSql;
                                    $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                }
                            } else {
                                if (count($cndParamList) === 0) {
                                    $resExeSql = runExecuteSQLPIVOT($db2con, $d1name, $wspkey, NULL, $burstItmInfo);
                                } else {
                                    $resExeSql = runExecuteSQLPIVOT($db2con, $d1name, $wspkey, $cndParamList, $burstItmInfo);
                                }
                                if ($resExeSql['RTN'] !== 0) {
                                    $rtn = 1;
                                } else {
                                    $qryData = $resExeSql;
                                    $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                }
                            }
                        }
                    }
                    if ($rtn === 1) {
                        //クエリー　＊クエリーの実行準備に失敗しました
                        $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                    }
                }
            } else {
                $qryData = $fdb2csv1['QRYDATA'];
                unset($fdb2csv1['QRYDATA']);
            }
        }
    }
    // データベース接続
    //if($exeFlg){
    //e_log('【削除】：クエリーデータ：'.print_r($qryData,true));
    //e_log('【削除】：FDB2CSV1データ：'.print_r($fdb2csv1,true));
    if ($isQGflg) { //クエリーグループ
        if ($fdb2csv1['D1WEBF'] === '1') {
            if ($fdb2csv1['D1CFLG'] === '1') {
                $fdb2csv1Info = $qryData['FDB2CSV1'][0];
                $libArr = preg_split("/\s+/", $fdb2csv1Info['D1LIBL']);
                $LIBLIST = join(' ', $libArr);
                if (cmMer($fdb2csv1Info['D1RDB']) === '' || cmMer($fdb2csv1Info['D1RDB']) === RDB || cmMer($fdb2csv1Info['D1RDB']) === RDBNAME) {
                    $db2LibCon = $paramQGData['db2LibCon'];
                    $db2LibListChange = cmPHPCHGLIB($db2LibCon, $LIBLIST);
                    //$db2LibCon = cmDb2ConLib($LIBLIST);
                    
                } else {
                    $_SESSION['PHPQUERY']['RDBNM'] = cmMer($fdb2csv1Info['D1RDB']);
                    $db2LibCon = $paramQGData['db2LibCon'];
                    $db2LibListChange = cmRDBCHGLIBL($db2LibCon, $LIBLIST);
                    //$db2LibCon = cmDB2ConRDB($LIBLIST);
                    
                }
            } else {
                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                    //e_log('ライブラリーリスト：'.print_r($qryData,true));
                    //$db2LibCon = cmDb2ConLib($qryData['LIBLIST']);
                    $db2LibCon = $paramQGData['db2LibCon'];
                    $db2LibListChange = cmPHPCHGLIB($db2LibCon, $qryData['LIBLIST']);
                } else {
                    $_SESSION['PHPQUERY']['RDBNM'] = $qryData['SELRDB'];
                    //$db2LibCon = cmDB2ConRDB($qryData['LIBLIST']);
                    $db2LibCon = $paramQGData['db2LibCon'];
                    $db2LibListChange = cmRDBCHGLIBL($db2LibCon, $qryData['LIBLIST']);
                }
            }
        }
    } else {
        if ($fdb2csv1['D1WEBF'] === '1') {
            if ($fdb2csv1['D1CFLG'] === '1') {
                $fdb2csv1Info = $qryData['FDB2CSV1'][0];
                $libArr = preg_split("/\s+/", $fdb2csv1Info['D1LIBL']);
                $LIBLIST = join(' ', $libArr);
                if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                    $db2LibCon = cmDb2ConLib($LIBLIST);
                } else {
                    $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1Info['D1RDB'];
                    $db2LibCon = cmDB2ConRDB($LIBLIST);
                }
            } else {
                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                    //e_log('ライブラリーリスト：'.print_r($qryData,true));
                    $db2LibCon = cmDb2ConLib($qryData['LIBLIST']);
                } else {
                    $_SESSION['PHPQUERY']['RDBNM'] = $qryData['SELRDB'];
                    $db2LibCon = cmDB2ConRDB($qryData['LIBLIST']);
                }
            }
        }
    }
    if ($exeFlg) {
        if ($rtn !== 1) {
            if ($isQGflg) { //クエリーグループ
                if ($fdb2csv1['D1WEBF'] !== '1') {
                    cmInt($db2con, $RTCD, $JSEQ, $paramQGData['D1NAME']);
                } else {
                    // SQLの定義のCLINIT実行
                    if ($fdb2csv1['D1CFLG'] === '1') {
                        if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                            if ($licenseCl === true) {
                                if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                    cmIntWC($db2LibCon, $RTCD, 'INT', $paramQGData['D1NAME']);
                                }
                            }
                        } else {
                            $RTCD = ' ';
                        }
                    } else {
                        // 通常定義のCLINIT実行
                        if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                            if ($licenseCl === true) {
                                if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                    cmIntWC($db2LibCon, $RTCD, 'INT', $paramQGData['D1NAME']);
                                }
                            }
                        } else {
                            $RTCD = ' ';
                        }
                    }
                }
                if ($RTCD == ' ') {
                    // 【setCmd】
                    // CL実行前処理行う
                    if ($licenseCl === true) {
                        if ($fdb2csv1['D1WEBF'] !== '1') {
                            // 5250で作成したクエリーの実行前CL呼び出し
                            if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                setCmd($db2con, $paramQGData['D1NAME'], $fdb2csv1['D1WEBF']);
                            }
                        } else {
                            if ($fdb2csv1['D1CFLG'] === '1') {
                                // SQLで作成したクエリーの実行前CL呼び出し
                                if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                    if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                        if ($fdb2csv1['D1CFLG'] === '1') {
                                            setSQLCmd($db2LibCon, $paramQGData['D1NAME'], $fdb2csv1['D1WEBF'], $cnddatArr);
                                        } else {
                                            setCmd($db2LibCon, $paramQGData['D1NAME'], $fdb2csv1['D1WEBF'], $cnddatArr);
                                        }
                                    }
                                    if ($fdb2csv1['DGBFLG'] === true) {
                                        cmIntWC($db2LibCon, $RTCD, 'BEF', $paramQGData['D1NAME']);
                                    }
                                } else {
                                    //RDBのCL実行呼び出し;
                                    if ($fdb2csv1['DGBFLG'] === true) {
                                        $rsClExe = callClExecute($db2LibCon, $paramQGData['D1NAME'], $cnddatArr, 'BEF');
                                        if ($rsClExe === 0) {
                                            $RTCD = ' ';
                                        } else {
                                            $RTCD == '9';
                                        }
                                    }
                                }
                            } else {
                                // 通常で作成したクエリーの実行前CL呼び出し
                                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                    if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                        setCmd($db2LibCon, $paramQGData['D1NAME'], $fdb2csv1['D1WEBF'], $cnddatArr);
                                    }
                                    if ($fdb2csv1['DGBFLG'] === true) {
                                        cmIntWC($db2LibCon, $RTCD, 'BEF', $paramQGData['D1NAME']);
                                    }
                                } else {
                                    //RDBのCL実行呼び出し;
                                    if ($fdb2csv1['DGBFLG'] === true) {
                                        $rsClExe = callClExecute($db2LibCon, $paramQGData['D1NAME'], $cnddatArr, 'BEF');
                                        if ($rsClExe === 0) {
                                            $RTCD = ' ';
                                        } else {
                                            $RTCD == '9';
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($RTCD !== ' ') {
                        //クエリー　＊CLの事前実行に失敗しました。
                        $errMsg = showMsg('FAIL_FUNC', array('CLの事前実行')); //"CLの事前実行に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, array(), $isQGflg);
                        $rtn = 1;
                    }
                }
            } else {
                if ($fdb2csv1['D1WEBF'] !== '1') {
                    cmInt($db2con, $RTCD, $JSEQ, $d1name);
                } else {
                    e_log("before cltest true");
                    if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                        e_log("cltest true1");
                    }
                    // SQLの定義のCLINIT実行
                    if ($fdb2csv1['D1CFLG'] === '1') {
                        if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                            if ($licenseCl === true) {
                                if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                    cmIntWC($db2LibCon, $RTCD, 'INT', $d1name);
                                }
                            }
                        } else {
                            $RTCD = ' ';
                        }
                    } else {
                        // 通常定義のCLINIT実行
                        if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                            if ($licenseCl === true) {
                                if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                    cmIntWC($db2LibCon, $RTCD, 'INT', $d1name);
                                }
                            }
                        } else {
                            $RTCD = ' ';
                        }
                    }
                }
                if ($RTCD == ' ') {
                    // 【setCmd】
                    // CL実行前処理行う
                    if ($licenseCl === true) {
                        e_log("before cltest true2");
                        if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                            e_log("cltest true2");
                        }
                        if ($fdb2csv1['D1WEBF'] !== '1') {
                            // 5250で作成したクエリーの実行前CL呼び出し
                            if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                setCmd($db2con, $d1name, $fdb2csv1['D1WEBF']);
                            }
                        } else {
                            if ($fdb2csv1['D1CFLG'] === '1') {
                                // SQLで作成したクエリーの実行前CL呼び出し
                                if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                    if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                        if ($fdb2csv1['D1CFLG'] === '1') {
                                            setSQLCmd($db2LibCon, $d1name, $fdb2csv1['D1WEBF'], $cnddatArr);
                                        } else {
                                            setCmd($db2LibCon, $d1name, $fdb2csv1['D1WEBF'], $cnddatArr);
                                        }
                                    }
                                    if ($fdb2csv1['DGBFLG'] === true) {
                                        cmIntWC($db2LibCon, $RTCD, 'BEF', $d1name);
                                    }
                                } else {
                                    //RDBのCL実行呼び出し;
                                    if ($fdb2csv1['DGBFLG'] === true) {
                                        $rsClExe = callClExecute($db2LibCon, $d1name, $cnddatArr, 'BEF');
                                        if ($rsClExe === 0) {
                                            $RTCD = ' ';
                                        } else {
                                            $RTCD == '9';
                                        }
                                    }
                                }
                            } else {
                                // 通常で作成したクエリーの実行前CL呼び出し
                                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                    if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                        setCmd($db2LibCon, $d1name, $fdb2csv1['D1WEBF'], $cnddatArr);
                                    }
                                    if ($fdb2csv1['DGBFLG'] === true) {
                                        cmIntWC($db2LibCon, $RTCD, 'BEF', $d1name);
                                    }
                                } else {
                                    //RDBのCL実行呼び出し;
                                    if ($fdb2csv1['DGBFLG'] === true) {
                                        $rsClExe = callClExecute($db2LibCon, $d1name, $cnddatArr, 'BEF');
                                        if ($rsClExe === 0) {
                                            $RTCD = ' ';
                                        } else {
                                            $RTCD == '9';
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($RTCD !== ' ') {
                        //クエリー　＊CLの事前実行に失敗しました。
                        $errMsg = showMsg('FAIL_FUNC', array('CLの事前実行')); //"CLの事前実行に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, array(), false);
                        $rtn = 1;
                    }
                }
            }
        }
    }
    /******************* ワークテーブル作成***************************************************/
    if ($exeFlg) {
        if ($rtn !== 1) {
            if ($isQGflg) { //クエリーグループ
                if ($fdb2csv1['D1WEBF'] !== '1') {
                    do {
                        if ($noBurstFlg === 0) {
                            $dbname = makeRandStr(10, true);
                        } else {
                            $dbname = makeRandStr(10);
                        }
                        //e_log('【削除】スケジュールワークテーブル：'.$dbname);
                        $schDBname = $dbname;
                        if ($fdb2csv1['RDBNM'] === '') {
                            $systables = cmGetSystables($db2con, SAVE_DB, $dbname);
                        } else {
                            $systables = cmGetSystables($db2LibCon, SAVE_DB, $dbname);
                        }
                    } while (count($systables) > 0);
                } else {
                    if ($fdb2csv1['D1CFLG'] !== '1') {
                        do {
                            if ($noBurstFlg === 0) {
                                $dbname = makeRandStr(10, true);
                                $schDBname = $dbname;
                            } else if ($noBurstFlg === 1) {
                                $dbname = makeRandStr(10);
                                $schDBname = $dbname;
                                e_log('SCHEDULE　の　保存テーブル①：' . $schDBname . 'table name:' . $dbname);
                            } else if ($noBurstFlg === 2) {
                                $schDBname = makeRandStr(10);
                                $dbname = makeRandStr(10, true);
                                e_log('SCHEDULE　の　保存テーブル②：' . $schDBname . 'table name:' . $dbname);
                            } else if ($noBurstFlg === 3) {
                                $dbname = makeRandStr(10, true);
                                $schDBname = $wrktblnm;
                            }
                            if ($noBurstFlg !== 0) {
                                e_log('SCHEDULE　の　保存テーブル：' . $schDBname);
                            }
                            if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === 'RDBNM' || $fdb2csv1['RDBNM'] === $RDBNAME) {
                                $systables = cmGetSystables($db2con, SAVE_DB, $dbname);
                            } else {
                                $systables = cmGetSystables($db2LibCon, SAVE_DB, $dbname);
                            }
                            if ($noBurstFlg === 2) {
                                if (count($systables) === 0) {
                                    if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === 'RDBNM' || $fdb2csv1['RDBNM'] === $RDBNAME) {
                                        $systables = cmGetSystables($db2con, SAVE_DB, $schDBname);
                                    } else {
                                        $systables = cmGetSystables($db2LibCon, SAVE_DB, $schDBname);
                                    }
                                }
                            }
                        }
                        while (count($systables) > 0);
                    } else {
                        do {
                            if ($noBurstFlg === 0) {
                                $dbname = makeRandStr(10, true);
                            } else {
                                $dbname = makeRandStr(10);
                            }
                            //e_log('【削除】スケジュールワークテーブル：'.$dbname);
                            $schDBname = $dbname;
                            if ($fdb2csv1['RDBNM'] === '') {
                                $systables = cmGetSystables($db2con, SAVE_DB, $dbname);
                            } else {
                                $systables = cmGetSystables($db2LibCon, SAVE_DB, $dbname);
                            }
                        } while (count($systables) > 0);
                    }
                }
            } else {
                if ($fdb2csv1['D1WEBF'] !== '1') {
                    do {
                        if ($noBurstFlg === 0) {
                            $dbname = makeRandStr(10, true);
                        } else {
                            $dbname = makeRandStr(10);
                        }
                        //e_log('【削除】スケジュールワークテーブル：'.$dbname);
                        $schDBname = $dbname;
                        if ($fdb2csv1['RDBNM'] === '') {
                            $systables = cmGetSystables($db2con, SAVE_DB, $dbname);
                        } else {
                            $systables = cmGetSystables($db2LibCon, SAVE_DB, $dbname);
                        }
                    } while (count($systables) > 0);
                } else {
                    if ($fdb2csv1['D1CFLG'] !== '1') {
                        do {
                            if ($noBurstFlg === 0) {
                                $dbname = makeRandStr(10, true);
                                $schDBname = $dbname;
                            } else if ($noBurstFlg === 1) {
                                $dbname = makeRandStr(10);
                                $schDBname = $dbname;
                                e_log('SCHEDULE　の　保存テーブル①：' . $schDBname . 'table name:' . $dbname);
                            } else if ($noBurstFlg === 2) {
                                $schDBname = makeRandStr(10);
                                $dbname = makeRandStr(10, true);
                                e_log('SCHEDULE　の　保存テーブル②：' . $schDBname . 'table name:' . $dbname);
                            } else if ($noBurstFlg === 3) {
                                $dbname = makeRandStr(10, true);
                                $schDBname = $wrktblnm;
                            }
                            if ($noBurstFlg !== 0) {
                                e_log('SCHEDULE　の　保存テーブル：' . $schDBname);
                            }
                            if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === 'RDBNM' || $fdb2csv1['RDBNM'] === $RDBNAME) {
                                $systables = cmGetSystables($db2con, SAVE_DB, $dbname);
                            } else {
                                $systables = cmGetSystables($db2LibCon, SAVE_DB, $dbname);
                            }
                            if ($noBurstFlg === 2) {
                                if (count($systables) === 0) {
                                    if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === 'RDBNM' || $fdb2csv1['RDBNM'] === $RDBNAME) {
                                        $systables = cmGetSystables($db2con, SAVE_DB, $schDBname);
                                    } else {
                                        $systables = cmGetSystables($db2LibCon, SAVE_DB, $schDBname);
                                    }
                                }
                            }
                        }
                        while (count($systables) > 0);
                    } else {
                        do {
                            if ($noBurstFlg === 0) {
                                $dbname = makeRandStr(10, true);
                            } else {
                                $dbname = makeRandStr(10);
                            }
                            //e_log('【削除】スケジュールワークテーブル：'.$dbname);
                            $schDBname = $dbname;
                            if ($fdb2csv1['RDBNM'] === '') {
                                $systables = cmGetSystables($db2con, SAVE_DB, $dbname);
                            } else {
                                $systables = cmGetSystables($db2LibCon, SAVE_DB, $dbname);
                            }
                        } while (count($systables) > 0);
                    }
                }
            }
        }
    } else {
        $dbname = $wrktblnm;
    }
    //e_log('【削除】.スケージュルテーブル：' . $dbname . '   ワークテーブル:' . $schDBname.'cnddatArr'.print_r($cnddatArr,true));
    $WLSQLB = '';
    $logd1name = '';
    if ($rtn !== 1) {
        // 5250のクエリー実行、IFS出力
        if ($isQGflg) { //クエリーグループ
            $logd1name = $paramQGData['D1NAME'];
            if ($fdb2csv1['D1WEBF'] !== '1') {
                if ($noBurstFlg !== 0) {
                    $rs = true;
                    $dataArr = cmGetFDB2CSV3($db2con, $paramQGData['D1NAME']);
                    $dataArr = cmCalendarDataParam($currentDate, $dataArr);
                    cmSetQTEMP($db2con);
                    //初期画面の検索条件でFDB2CSV3を更新
                    $r = updSearchCond($db2con, $dataArr, $paramQGData['D1NAME']);
                    $rs = $r[0];
                    if ($rs === 1) {
                        $rtn = 1;
                        //$msg = showMsg('FAIL_INT_BYTE');//'検索データは128バイトまで入力可能です。';
                        $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg);
                    } else {
                        cmSetPHPQUERY($db2con);
                        cmDo($db2con, $RTCD, $JSEQ, $dbname, $paramQGData['D1NAME']);
                        e_log('5250のクエリー実行終了。');
                    }
                    if ($RTCD === "1") {
                        $D2HED = cmGetD2HED($db2con, $paramQGData['D1NAME'], $JSEQ);
                        $rtn = 1;
                        //$msg   = cmMer($D2HED) . 'の入力に誤りがあります。';
                        $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                    } else if ($RTCD === "9") {
                        $rtn = 1;
                        // $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
                        $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                    }
                }
                //利用してワークテーブルのデータ挿入
                if ($rtn !== 1) {
                    $schDBname = $dbname;
                    $wrkTblLst[] = $dbname;
                    $resIns = cmInsWTBL($db2con, $dbname);
                    if ($resIns !== 0) {
                        e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                    }
                }
                // 制御又ピボットの場合ワークテーブルを新しく作る
                //e_log('【削除】実行種類②:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                if ($rtn !== 1) {
                    if (cmMer($wspkey) === '') {
                        if ($fdb2csv1['SEIGYOFLG'] == 1) {
                            // 制御の取得
                            //e_log('黒い画面の制御データ取得：' . $dbname);
                            $rstemp = cmRecreateSeigyoTbl5250($db2con, $paramQGData['D1NAME'], $dbname, '', $burstItmInfo);
                            if ($rstemp !== '') {
                                $dbname = $rstemp;
                                //e_log('黒い画面の制御データ取得後：' . $dbname.'SYPS output'.$schDBname);
                                
                            } else {
                                $rtn = 1;
                                // $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
                                $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行失敗')); //"クエリーの実行準備に失敗しました。";
                                fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                            }
                            //利用してワークテーブルのデータ挿入
                            if ($rtn !== 1) {
                                $wrkTblLst[] = $dbname;
                                $resIns = cmInsWTBL($db2con, $dbname);
                                if ($resIns !== 0) {
                                    e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                                }
                            }
                        }
                    } else {
                        //e_log('【削除】5250ピボットデータ取得:');
                        //ピボットの取得
                        $rstemp = cmRecreateRnPivotTbl5250($db2con, $paramQGData['D1NAME'], $wspkey, $dbname, $burstItmInfo);
                        if ($rstemp !== '') {
                            $dbname = $rstemp;
                        } else {
                            $rtn = 1;
                            // $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
                            $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行失敗')); //"クエリーの実行準備に失敗しました。";
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                        }
                        //利用してワークテーブルのデータ挿入
                        if ($rtn !== 1) {
                            $wrkTblLst[] = $dbname;
                            $resIns = cmInsWTBL($db2con, $dbname);
                            if ($resIns !== 0) {
                                e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                            }
                        }
                    }
                }
            } else {
                // WEBで作成した定義の場合
                $WLSQLB = '';
                if ($fdb2csv1['D1CFLG'] === '1') {
                    if ($noBurstFlg !== 0) {
                        //SQLで作成した定義の場合
                        e_log('SQLで作成した定義実行してワークテーブル:開始');
                        if (count($qryData['BSQLCND']) > 0) {
                            $paramArr = bindParamArr($cndParamList);
                        } else {
                            $paramArr = array();
                        }
                        $strSQL = $qryData['BSQLEXEDAT']['EXESQL'];
                        /*DBのフィードがブランクの場合【1=1】に代る*/
                        $repArr = array();
                        if (count($paramArr) > 0) {
                            if (count($paramArr['WITHPARAMDATA']) > 0) {
                                $strSQL = bindWithParamSQLQry($strSQL, $paramArr['WITHPARAMDATA']);
                            }
                            //$strExeSQLParam = bindParamSQLQry($strSQL, $paramArr['PARAMDATA']);20180525
                            $repArr = cmReplaceSQLParam($strSQL, $paramArr['PARAMDATA']);
                            $strExeSQLParam = bindParamSQLQry($repArr["strSQL"], $repArr["params"]);
                            e_log("SQLクエリーに任意をバインドする前、結果:" . bindParamSQLQry($strSQL, $paramArr['PARAMDATA']) . print_r($paramArr['PARAMDATA'], true));
                        } else {
                            $strExeSQLParam = $strSQL;
                            $repArr["params"] = $paramArr;
                        }
                        $WLSQLB = $strExeSQLParam;
                        $rsExeSQL = execSQLQry($db2LibCon, $strExeSQLParam, $repArr["params"]);
                        if ($rsExeSQL['RTN'] !== 0) {
                            $rtn = 1;
                            $msg = $rsExeSQL['MSG'];
                            //$errMsg = showMsg('FAIL_FUNC', array('クエリーの実行チェック')); //"クエリーの実行チェックに失敗しました。";
                            $errMsg = $msg;
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                        } else {
                            $estart = microtime(true);
                            $rsCreate = createSQLQryTbl($db2LibCon, $strExeSQLParam, $repArr["params"], $dbname);
                            if ($rsCreate['RTN'] !== 0) {
                                $rtn = 1;
                                $msg = $rsCreate['MSG'];
                                //クエリー　＊クエリーの実行に失敗しました
                                $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行')); //"クエリーの実行に失敗しました。";
                                fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                            }
                        }
                        $qryCnt = $rsCreate['QRYCNT'];
                        $eend = microtime(true);
                        e_log('SQLで作成した定義実行してワークテーブル:終了');
                    }
                    //利用してワークテーブルのデータ挿入
                    if ($rtn !== 1) {
                        $schDBname = $dbname;
                        $wrkTblLst[] = $dbname;
                        $resIns = cmInsWTBL($db2con, $dbname);
                        if ($resIns !== 0) {
                            e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                        }
                    }
                    // 制御又ピボットの場合ワークテーブルを新しく作る
                    //e_log('【削除】実行種類②:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                    if ($rtn !== 1) {
                        $schDBname = $dbname;
                        e_log('SQLクエリワークテーブル1：' . print_r($rsCreate, true) . 'aaatable:' . $dbname);
                        //$dbname = $rsCreate['TMPTBLNM'];
                        if ($wspkey === '') {
                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                //制御取得
                                $rsSeigyoCreate = createSeigyoSQLQryTbl($paramQGData['D1NAME'], $schDBname, '', $burstItmInfo);
                                $dbname = $rsSeigyoCreate;
                                $Seigyofilename = $rsSeigyoCreate;//MSM
                                e_log('制御情報取得テーブル1257：' . $dbname);
                                $fdb2csv1['TMP_D1CFLG'] = '';
                            }
                        } else {
                            $rsPivotCreate = createPivotSQLQryTbl($paramQGData['D1NAME'], $wspkey, $schDBname, '', $burstItmInfo);
                            $dbname = $rsPivotCreate;
                            $fdb2csv1['TMP_D1CFLG'] = '';
                        }
                    }
                    e_log('SQLクエリワークテーブル：' . $dbname);

                    if ($licenseCl === true) {
                        if ($rtn !== 1) {
                            if ($fdb2csv1['DGAFLG'] === true) {
                                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                    cmIntWC($db2LibCon, $RTCD, 'AFT', $paramQGData['D1NAME']);
                                } else {
                                    //RDBのCL実行呼び出し;
                                    $rsClExe = callClExecute($db2LibCon, $paramQGData['D1NAME'], $cnddatArr, 'AFT');
                                    if ($rsClExe === 0) {
                                        $RTCD = ' ';
                                    } else {
                                        $RTCD == '9';
                                    }
                                }
                            }
                        }
                    }
                    if ($RTCD !== ' ') {
                        //クエリー　＊CLの事後処理に失敗しました。
                        $rtn = 1;
                        $errMsg = showMsg('FAIL_FUNC', array('CLの事後処理')); //"CLの事後処理に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                    }

                } else {
                    $strSQLdata = '';
                    if ($noBurstFlg !== 0) {
                        //通常で作成した定義の場合
                        e_log('通常で作成した定義実行してワークテーブル:' . $dbname . ' 開始');
                        $WLSQLB = $qryData['STREXECSQL'];
                        if ($fdb2csv1['SEIGYOFLG'] == 1 || cmMer($wspkey) !== '') {
                            if (CRTQTEMPTBL_FLG === 1) {
                                if (count($qryData['MBRDATALST']) > 0) {
                                    foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                        $strSQL = '';
                                        if ($mbrdata['FILTYP'] === 'L' || $mbrdata['FILTYP'] === 'V') {
                                            $rs = dropTmpFile($db2LibCon, 'QTEMP', $mbrdata['TBLNM']);
                                            $rs = createTmpFil($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                            if ($rs !== true) {
                                                e_log('テーブル設定失敗');
                                                $rtn = 1;
                                                $msg = showMsg($rs);
                                                break;
                                            }
                                        } else {
                                            //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                            $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'], $strSQL);
                                            if ($rs !== true) {
                                                e_log('テーブル設定失敗');
                                                $rtn = 1;
                                                $msg = showMsg($rs);
                                                break;
                                            } else {
                                                if ($strSQLdata !== '') {
                                                    $strSQLdata.= '<br/>';
                                                }
                                                $strSQLdata.= $strSQL;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (count($qryData['MBRDATALST']) > 0) {
                                    foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                        $strSQL = '';
                                        //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                        $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'], $strSQL);
                                        if ($rs !== true) {
                                            e_log('テーブル設定失敗');
                                            $rtn = 1;
                                            $msg = showMsg($rs);
                                            break;
                                        } else {
                                            if ($strSQLdata !== '') {
                                                $strSQLdata.= '<br/>';
                                            }
                                            $strSQLdata.= $strSQL;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (count($qryData['MBRDATALST']) > 0) {
                                foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                    $strSQL = '';
                                    //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                    $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'], $strSQL);
                                    if ($rs !== true) {
                                        e_log('テーブル設定失敗');
                                        $rtn = 1;
                                        $msg = showMsg($rs);
                                        break;
                                    } else {
                                        if ($strSQLdata !== '') {
                                            $strSQLdata.= '<br/>';
                                        }
                                        $strSQLdata.= $strSQL;
                                    }
                                }
                            }
                        }
                        $WLSQLB = $strSQLdata . '<br/><br/>' . $WLSQLB;
                        if ($rtn !== 1) {
                            $resExec = execQry($db2LibCon, $qryData['STREXECTESTSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'], $qryData['MBRDATALST']);
                            //$resExec = execQry($db2LibCon, $qryData['STREXECSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'], $qryData['MBRDATALST']);
                            if ($resExec['RTN'] !== 0) {
                                $rtn = 1;
                                $msg = showMsg($resExec['MSG']);
                                //クエリー　＊クエリーの実行チェックに失敗しました
                                //$errMsg = showMsg('FAIL_FUNC', array('クエリーの実行チェック')); //"クエリーの実行チェックに失敗しました。";
                                $errMsg = $msg;
                                fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                            }
                            if ($rtn !== 1) {
                                $estart = microtime(true);
                                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                    $resCreateTbl = createTmpTable($db2LibCon, $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $dbname, $qryData['MBRDATALST']);
                                } else {
                                    $resCreateTbl = createTmpTableRDB($db2con, $db2LibCon, $qryData['SELRDB'], $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $dbname, $qryData['MBRDATALST']);
                                }
                                if ($resCreateTbl['RTN'] !== 0) {
                                    $rtn = 1;
                                    $msg = showMsg($resCreateTbl['MSG']);
                                    //クエリー　＊クエリーの実行に失敗しました
                                    $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行')); //"クエリーの実行に失敗しました。";
                                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                                }
                            }
                            $qryCnt = $resCreateTbl['QRYCNT'];
                            $eend = microtime(true);
                        }
                        if ($licenseCl === true) {
                            if ($rtn !== 1) {
                                if ($fdb2csv1['DGAFLG'] === true) {
                                    if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                        cmIntWC($db2LibCon, $RTCD, 'AFT', $paramQGData['D1NAME']);
                                    } else {
                                        //RDBのCL実行呼び出し;
                                        $rsClExe = callClExecute($db2LibCon, $paramQGData['D1NAME'], $cnddatArr, 'AFT');
                                        if ($rsClExe === 0) {
                                            $RTCD = ' ';
                                        } else {
                                            $RTCD == '9';
                                        }
                                    }
                                }
                            }
                        }
                        if ($RTCD !== ' ') {
                            //クエリー　＊CLの事後処理に失敗しました。
                            $rtn = 1;
                            $errMsg = showMsg('FAIL_FUNC', array('CLの事後処理')); //"CLの事後処理に失敗しました。";
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                        }
                    }
                    e_log('通常で作成した定義実行してワークテーブル 2232:' . $dbname . ' 終了' . $schDBname);
                    if ($rtn !== 1) {
                        if (($noBurstFlg === 2 || $noBurstFlg === 3) === false) {
                            $schDBname = $dbname;
                        }
                        $wrkTblLst[] = $dbname;
                        $resIns = cmInsWTBL($db2con, $dbname);
                        if ($resIns !== 0) {
                            e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                        }
                    }
                    e_log('通常で作成した定義実行してワークテーブル①:' . $dbname . ' 終了' . $schDBname);
                }
            }
        } else {
            $logd1name = $d1name;
            if ($fdb2csv1['D1WEBF'] !== '1') {
                if ($noBurstFlg !== 0) {
                    $rs = true;
                    $dataArr = cmGetFDB2CSV3($db2con, $d1name);
                    $dataArr = cmCalendarDataParam($currentDate, $dataArr);
                    cmSetQTEMP($db2con);
                    //初期画面の検索条件でFDB2CSV3を更新
                    $r = updSearchCond($db2con, $dataArr, $d1name);
                    $rs = $r[0];
                    if ($rs === 1) {
                        $rtn = 1;
                        //$msg = showMsg('FAIL_INT_BYTE');//'検索データは128バイトまで入力可能です。';
                        $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg);
                    } else {
                        cmSetPHPQUERY($db2con);
                        cmDo($db2con, $RTCD, $JSEQ, $dbname, $d1name);
                    }
                    if ($RTCD === "1") {
                        $D2HED = cmGetD2HED($db2con, $d1name, $JSEQ);
                        $rtn = 1;
                        //$msg   = cmMer($D2HED) . 'の入力に誤りがあります。';
                        $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                    } else if ($RTCD === "9") {
                        $rtn = 1;
                        // $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
                        $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                    }
                }
                //利用してワークテーブルのデータ挿入
                if ($rtn !== 1) {
                    $schDBname = $dbname;
                    $wrkTblLst[] = $dbname;
                    $resIns = cmInsWTBL($db2con, $dbname);
                    if ($resIns !== 0) {
                        e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                    }
                }
                // 制御又ピボットの場合ワークテーブルを新しく作る
                //e_log('【削除】実行種類②:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                if ($rtn !== 1) {
                    if (cmMer($wspkey) === '') {
                        if ($fdb2csv1['SEIGYOFLG'] == 1) {
                            // 制御の取得
                            //e_log('黒い画面の制御データ取得：' . $dbname);
                            $rstemp = cmRecreateSeigyoTbl5250($db2con, $d1name, $dbname, '', $burstItmInfo);
                            if ($rstemp !== '') {
                                $dbname = $rstemp;
                                //e_log('黒い画面の制御データ取得後：' . $dbname.'SYPS output'.$schDBname);
                                
                            } else {
                                $rtn = 1;
                                // $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
                                $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行失敗')); //"クエリーの実行準備に失敗しました。";
                                fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                            }
                            //利用してワークテーブルのデータ挿入
                            if ($rtn !== 1) {
                                $wrkTblLst[] = $dbname;
                                $resIns = cmInsWTBL($db2con, $dbname);
                                if ($resIns !== 0) {
                                    e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                                }
                            }
                        }
                    } else {
                        //e_log('【削除】5250ピボットデータ取得:');
                        //ピボットの取得
                        $rstemp = cmRecreateRnPivotTbl5250($db2con, $d1name, $wspkey, $dbname, $burstItmInfo);
                        if ($rstemp !== '') {
                            $dbname = $rstemp;
                        } else {
                            $rtn = 1;
                            // $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
                            $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行失敗')); //"クエリーの実行準備に失敗しました。";
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                        }
                        //利用してワークテーブルのデータ挿入
                        if ($rtn !== 1) {
                            $wrkTblLst[] = $dbname;
                            $resIns = cmInsWTBL($db2con, $dbname);
                            if ($resIns !== 0) {
                                e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                            }
                        }
                    }
                }
            } else {
                // WEBで作成した定義の場合
                if ($fdb2csv1['D1CFLG'] === '1') {
                    if ($noBurstFlg !== 0) {
                        //SQLで作成した定義の場合
                        e_log('SQLで作成した定義実行してワークテーブル:開始');
                        if (count($qryData['BSQLCND']) > 0) {
                            $paramArr = bindParamArr($cndParamList);
                        } else {
                            $paramArr = array();
                        }
                        $strSQL = $qryData['BSQLEXEDAT']['EXESQL'];
                        /*DBのフィードがブランクの場合【1=1】に代る*/
                        $repArr = array();
                        if (count($paramArr) > 0) {
                            if (count($paramArr['WITHPARAMDATA']) > 0) {
                                $strSQL = bindWithParamSQLQry($strSQL, $paramArr['WITHPARAMDATA']);
                            }
                            $repArr = cmReplaceSQLParam($strSQL, $paramArr['PARAMDATA']);
                            //$strExeSQLParam = bindParamSQLQry($strSQL, $paramArr['PARAMDATA']);20180525
                            $strExeSQLParam = bindParamSQLQry($repArr["strSQL"], $repArr["params"]);
                            e_log("SQLクエリーに任意をバインドする前、結果:" . bindParamSQLQry($strSQL, $paramArr['PARAMDATA']) . print_r($paramArr['PARAMDATA'], true));
                        } else {
                            $strExeSQLParam = $strSQL;
                            $repArr["params"] = $paramArr;
                        }
                        $WLSQLB = $strExeSQLParam;
                        $rsExeSQL = execSQLQry($db2LibCon, $strExeSQLParam, $repArr["params"]);
                        if ($rsExeSQL['RTN'] !== 0) {
                            $rtn = 1;
                            $msg = $rsExeSQL['MSG'];
                            //$errMsg = showMsg('FAIL_FUNC', array('クエリーの実行チェック')); //"クエリーの実行チェックに失敗しました。";
                            $errMsg = $msg;
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                        } else {
                            $estart = microtime(true);
                            $rsCreate = createSQLQryTbl($db2LibCon, $strExeSQLParam, $repArr["params"], $dbname);
                            if ($rsCreate['RTN'] !== 0) {
                                $rtn = 1;
                                $msg = $rsCreate['MSG'];
                                //クエリー　＊クエリーの実行に失敗しました
                                $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行')); //"クエリーの実行に失敗しました。";
                                fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                            }
                        }
                        $qryCnt = $rsCreate['QRYCNT'];
                        $eend = microtime(true);
                        e_log('SQLで作成した定義実行してワークテーブル:終了');
                    }
                    //利用してワークテーブルのデータ挿入
                    if ($rtn !== 1) {
                        $schDBname = $dbname;
                        $wrkTblLst[] = $dbname;
                        $resIns = cmInsWTBL($db2con, $dbname);
                        if ($resIns !== 0) {
                            e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                        }
                    }
                    // 制御又ピボットの場合ワークテーブルを新しく作る
                    //e_log('【削除】実行種類②:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                    if ($rtn !== 1) {
                        $schDBname = $dbname;
                        e_log('SQLクエリワークテーブル1：' . print_r($rsCreate, true) . 'aaatable:' . $dbname);
                        //$dbname = $rsCreate['TMPTBLNM'];
                        if ($wspkey === '') {
                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                //制御取得
                                $rsSeigyoCreate = createSeigyoSQLQryTbl($d1name, $schDBname, '', $burstItmInfo);
                                //$dbname = $rsSeigyoCreate;
                                $Seigyofilename = $rsSeigyoCreate;//MSM
                                $fdb2csv1['TMP_D1CFLG'] = '';
                            }
                        } else {
                            $rsPivotCreate = createPivotSQLQryTbl($d1name, $wspkey, $schDBname, '', $burstItmInfo);
                            $dbname = $rsPivotCreate;
                            $fdb2csv1['TMP_D1CFLG'] = '';
                        }
                    }
                    e_log('SQLクエリワークテーブル：' . $dbname);

                    if ($licenseCl === true) {
                        if ($rtn !== 1) {
                            if ($fdb2csv1['DGAFLG'] === true) {
                                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                    cmIntWC($db2LibCon, $RTCD, 'AFT', $d1name);
                                } else {
                                    //RDBのCL実行呼び出し;
                                    $rsClExe = callClExecute($db2LibCon, $d1name, $cnddatArr, 'AFT');
                                    if ($rsClExe === 0) {
                                        $RTCD = ' ';
                                    } else {
                                        $RTCD == '9';
                                    }
                                }
                            }
                        }
                    }
                    if ($RTCD !== ' ') {
                        //クエリー　＊CLの事後処理に失敗しました。
                        $rtn = 1;
                        $errMsg = showMsg('FAIL_FUNC', array('CLの事後処理')); //"CLの事後処理に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                    }

                } else {
                    if ($noBurstFlg !== 0) {
                        $strSQL = '';
                        $strSQLdata = '';
                        $WLSQLB = $qryData['STREXECSQL'];
                        //通常で作成した定義の場合
                        if ($fdb2csv1['SEIGYOFLG'] == 1 || cmMer($wspkey) !== '') {
                            if (CRTQTEMPTBL_FLG === 1) {
                                if (count($qryData['MBRDATALST']) > 0) {
                                    foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                        if ($mbrdata['FILTYP'] === 'L' || $mbrdata['FILTYP'] === 'V') {
                                            $rs = dropTmpFile($db2LibCon, 'QTEMP', $mbrdata['TBLNM']);
                                            $rs = createTmpFil($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                            if ($rs !== true) {
                                                e_log('テーブル設定失敗');
                                                $rtn = 1;
                                                $msg = showMsg($rs);
                                                break;
                                            }
                                        } else {
                                            //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                            $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'], $strSQL);
                                            if ($rs !== true) {
                                                e_log('テーブル設定失敗');
                                                $rtn = 1;
                                                $msg = showMsg($rs);
                                                break;
                                            } else {
                                                if ($strSQLdata !== '') {
                                                    $strSQLdata.= '<br/>';
                                                }
                                                $strSQLdata.= $strSQL;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (count($qryData['MBRDATALST']) > 0) {
                                    foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                        //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                        $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'], $strSQL);
                                        if ($rs !== true) {
                                            e_log('テーブル設定失敗');
                                            $rtn = 1;
                                            $msg = showMsg($rs);
                                            break;
                                        } else {
                                            if ($strSQLdata !== '') {
                                                $strSQLdata.= '<br/>';
                                            }
                                            $strSQLdata.= $strSQL;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (count($qryData['MBRDATALST']) > 0) {
                                foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                    //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                    $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'], $strSQL);
                                    if ($rs !== true) {
                                        e_log('テーブル設定失敗');
                                        $rtn = 1;
                                        $msg = showMsg($rs);
                                        break;
                                    } else {
                                        if ($strSQLdata !== '') {
                                            $strSQLdata.= '<br/>';
                                        }
                                        $strSQLdata.= $strSQL;
                                    }
                                }
                            }
                        }
                        $WLSQLB = $strSQLdata . '<br/><br/>' . $WLSQLB;
                        if ($rtn !== 1) {
                            $resExec = execQry($db2LibCon, $qryData['STREXECTESTSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'], $qryData['MBRDATALST']);
                            //$resExec = execQry($db2LibCon, $qryData['STREXECSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'], $qryData['MBRDATALST']);
                            if ($resExec['RTN'] !== 0) {
                                $rtn = 1;
                                $msg = showMsg($resExec['MSG']);
                                //クエリー　＊クエリーの実行チェックに失敗しました
                                //$errMsg = showMsg('FAIL_FUNC', array('クエリーの実行チェック')); //"クエリーの実行チェックに失敗しました。";
                                $errMsg = $msg;
                                fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                            }
                            $estart = microtime(true);
                            if ($rtn !== 1) {
                                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                    $resCreateTbl = createTmpTable($db2LibCon, $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $dbname, $qryData['MBRDATALST']);
                                } else {
                                    $resCreateTbl = createTmpTableRDB($db2con, $db2LibCon, $qryData['SELRDB'], $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $dbname, $qryData['MBRDATALST']);
                                }
                                if ($resCreateTbl['RTN'] !== 0) {
                                    $rtn = 1;
                                    $msg = showMsg($resCreateTbl['MSG']);
                                    //クエリー　＊クエリーの実行に失敗しました
                                    $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行')); //"クエリーの実行に失敗しました。";
                                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                                }
                            }
                            $qryCnt = $resCreateTbl['QRYCNT'];
                            $eend = microtime(true);
                        }
                        if ($licenseCl === true) {
                            if ($rtn !== 1) {
                                if ($fdb2csv1['DGAFLG'] === true) {
                                    if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                        cmIntWC($db2LibCon, $RTCD, 'AFT', $d1name);
                                    } else {
                                        //RDBのCL実行呼び出し;
                                        $rsClExe = callClExecute($db2LibCon, $d1name, $cnddatArr, 'AFT');
                                        if ($rsClExe === 0) {
                                            $RTCD = ' ';
                                        } else {
                                            $RTCD == '9';
                                        }
                                    }
                                }
                            }
                        }
                        if ($RTCD !== ' ') {
                            //クエリー　＊CLの事後処理に失敗しました。
                            $rtn = 1;
                            $errMsg = showMsg('FAIL_FUNC', array('CLの事後処理')); //"CLの事後処理に失敗しました。";
                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                        }
                    }
                    e_log('通常で作成した定義実行してワークテーブル 2540:' . $dbname . ' 終了' . $schDBname);
                    if ($rtn !== 1) {
                        if (($noBurstFlg === 2 || $noBurstFlg === 3) === false) {
                            $schDBname = $dbname;
                        }
                        $wrkTblLst[] = $dbname;
                        $resIns = cmInsWTBL($db2con, $dbname);
                        if ($resIns !== 0) {
                            e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                        }
                    }
                    e_log('通常で作成した定義実行してワークテーブル➋:' . $dbname . ' 終了' . $schDBname);
                    
                }
            }
        }
    }
    if ($rtn !== 1) {
        if ($licenseIFS === true) {
            //e_log('【削除】IFS開始:'.$noBurstFlg);
            //e_log('LicenseIFS QGFLG:'.$isQGflg."FDB2CSV1:".print_r($fdb2csv1,true));
            if ($isQGflg) {
                if ($noBurstFlg === 1 || $noBurstFlg === 2) {
                    $curTime = getCurrentTimeStamp($db2con);
                    if ($fdb2csv1['D1WEBF'] !== '1') {
                        //IFS出力先に設定されたEXCELファイルが作成される。
                        //e_log('KSKD1DIEX【削除】IFS出力開始'.$fdb2csv1['D1DIEX']);
                        //e_log('KSKD1DIEX【削除】実行種類③:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                        if (cmMer($wspkey) === '') {
                            if ($fdb2csv1['D1DIEX'] !== '') {
                                //e_log('【削除】IFSXLS出力開始'.'DIR:'.$fdb2csv1['D1DIEX'].'WEBF'.$fdb2csv1['D1WEBF']);
                                // XLSのテンプレートがある場合、ファイルあるかチェック
                                e_log("Seigyofilename ###1 => ".$Seigyofilename." , schDBname => ".$schDBname);
                                if ($fdb2csv1['SEIGYOFLG'] == 1 && trim($Seigyofilename," ") !== ""){
                                    $schDBname = $Seigyofilename;
                                }
                                if (chkXlsTemplate($paramQGData['D1NAME'], $fdb2csv1['D1TMPF'])) {
                                    //　IFSのXLS出力
                                    $rsCreateExl = createExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], '', $paramQGData['D1NAME'], $schDBname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1DIEX'], $mailAddLst, $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $cnddatArr,$Seigyofilename);//MSM20180830
                                    if ($rsCreateExl !== 0) {
                                        $errMsg = 'EXCELファイル作成失敗';
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), $isQGflg);
                                    }
                                } else {
                                    if ($templateErrCnt === 0) {
                                        $errMsg = 'IFSにXLSファイル出力失敗:' . showMsg('NOTEXIST_TMP', array($d1name));
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), $isQGflg);
                                    }
                                }
                            }
                        } else {
                            // e_log('MyatMoe else【削除】実行種類③:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                            //ピボット実行
                            if ($fdb2csv1['D1SOEX'] !== '') {
                                if ($noBurstFlg === 2) {
                                    $pivottemp = '';
                                    $pivottemp = cmRecreateRnPivotTbl5250($db2con, $paramQGData['D1NAME'], $wspkey, $schDBname, array());
                                    if ($pivottemp !== '') {
                                        $pvdbname = $pivottemp;
                                    }
                                    $fdb2csv1['TMP_D1CFLG'] = '';
                                } else {
                                    $pvdbname = $dbname;
                                }
                                $piexfi = createPivotExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $paramQGData['D1NAME'], $pvdbname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1SOEX'], $fdb2csv1['D1WEBF'], $cnddatArr, $wspkey, $fdb2csv1['SOCTFL']);
                                //e_log("PT1KSK".$fdb2csv1['D1DIEX']);
                                if ($piexfi !== 0) {
                                    $errMsg = showMsg('FAIL_SEL');
                                    fnCreateHistory($db2con, $d1name, $filename, $pvdbname, $nowYmd, $nowTime, '', $errMsg, array(), $isQGflg);
                                }
                            }
                        }
                    } else {
                        if ($fdb2csv1['D1CFLG'] === '1') {
                            e_log("Seigyofilename ###2 => ".$Seigyofilename." , schDBname => ".$schDBname);
                            if ($fdb2csv1['SEIGYOFLG'] == 1 && trim($Seigyofilename," ") !== ""){
                                $schDBname = $Seigyofilename;
                                $dbname = $Seigyofilename;
                            }
                            //e_log('【削除】実行種類⑥①:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                            if (cmMer($wspkey) === '') {
                                if ($fdb2csv1['D1DIRE'] !== '') {
                                    //e_log('【削除】IFSのSQLのCSV取得'.$fdb2csv1['D1DIRE'].$schDBname);
                                    $rsCreateCsv = createCSVFile($db2con, $db2LibCon, $paramQGData['D1NAME'], $fdb2csv1['D1CFLG'], $dbname, $nowYmd, $nowTime, $fdb2csv1['D1DIRE'], $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $curTime,$Seigyofilename);//MSM20180830
                                    if ($rsCreateCsv !== 0) {
                                        $errMsg = showMsg('FAIL_SEL');
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $rsCreateCsv, array());
                                    }
                                }
                                if ($fdb2csv1['D1DIEX'] !== '') {
                                    //e_log('【削除】IFSXLS出力開始'.'DIR:'.$fdb2csv1['D1DIEX'].'WEBF'.$fdb2csv1['D1WEBF']);
                                    // XLSのテンプレートがある場合、ファイルあるかチェック
                                    if (chkXlsTemplate($d1name, $fdb2csv1['D1TMPF'])) {
                                        //　IFSのXLS出力
                                        $rsCreateExl = createExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $fdb2csv1['D1CFLG'], $paramQGData['D1NAME'], $schDBname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1DIEX'], $mailAddLst, $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $cnddatArr,$Seigyofilename);//MSM20180830
                                        if ($rsCreateExl !== 0) {
                                            $errMsg = 'EXCELファイル作成失敗';
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), $isQGflg);
                                        }
                                    } else {
                                        if ($templateErrCnt === 0) {
                                            $errMsg = 'IFSにXLSファイル出力失敗:' . showMsg('NOTEXIST_TMP', array($d1name));
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), $isQGflg);
                                        }
                                    }
                                }
                            } else {
                                //ピボット実行
                                if ($fdb2csv1['D1SOEX'] !== '') {
                                    if ($noBurstFlg === 2) {
                                        $pivottemp = '';
                                        //$pivottemp = createPivotSQLQryTbl($db2con, $d1name, $wspkey, $schDBname, array());
                                        $pivottemp = createPivotSQLQryTbl($paramQGData['D1NAME'], $wspkey, $schDBname, array());
                                        if ($pivottemp !== '') {
                                            $pvdbname = $pivottemp;
                                        }
                                    } else {
                                        $pvdbname = $dbname;
                                    }
                                    $piexfi = createPivotExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $paramQGData['D1NAME'], $pvdbname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1SOEX'], $fdb2csv1['D1WEBF'], $cnddatArr, $wspkey, $fdb2csv1['SOCTFL']);
                                    if ($piexfi !== 0) {
                                        $errMsg = showMsg('FAIL_SEL');
                                        fnCreateHistory($db2con, $d1name, $filename, $pvdbname, $nowYmd, $nowTime, '', $errMsg, array(), $isQGflg);
                                    }
                                }
                            }
                        } else {
                            //e_log('【削除】IFS出力開始');
                            if ($noBurstFlg === 2) {
                                //e_log('【削除】IFS出力開始：制御又ピボットに全データ取得');
                                // 全部のデータ取得実行必要
                                if (cmMer($wspkey) === '') {
                                    $resExeSql = runExecuteSQL($db2con, $paramQGData['D1NAME']);
                                } else {
                                    $resExeSql = runExecuteSQLPIVOT($db2con, $paramQGData['D1NAME'], $wspkey);
                                }
                                if ($resExeSql['RTN'] !== 0) {
                                    //クエリー　＊クエリーの実行準備に失敗しました
                                    $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                                } else {
                                    $qryData = $resExeSql;
                                    $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                    if ($fdb2csv1['SEIGYOFLG'] == 1 || cmMer($wspkey) !== '') {
                                        if (CRTQTEMPTBL_FLG === 1) {
                                            if (count($qryData['MBRDATALST']) > 0) {
                                                foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                                    if ($mbrdata['FILTYP'] === 'L' || $mbrdata['FILTYP'] === 'V') {
                                                        $rs = dropTmpFile($db2LibCon, 'QTEMP', $mbrdata['TBLNM']);
                                                        $rs = createTmpFil($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                                        if ($rs !== true) {
                                                            e_log('テーブル設定失敗');
                                                            $rtn = 1;
                                                            $msg = showMsg($rs);
                                                            break;
                                                        }
                                                    } else {
                                                        //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                                        $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                                        if ($rs !== true) {
                                                            e_log('テーブル設定失敗');
                                                            $rtn = 1;
                                                            $msg = showMsg($rs);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if (count($qryData['MBRDATALST']) > 0) {
                                                foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                                    //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                                    $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                                    if ($rs !== true) {
                                                        e_log('テーブル設定失敗');
                                                        $rtn = 1;
                                                        $msg = showMsg($rs);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if (count($qryData['MBRDATALST']) > 0) {
                                            foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                                //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                                $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                                if ($rs !== true) {
                                                    e_log('テーブル設定失敗');
                                                    $rtn = 1;
                                                    $msg = showMsg($rs);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if ($rtn !== 1) {
                                        $resExec = execQry($db2LibCon, $qryData['STREXECTESTSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'], $qryData['MBRDATALST']);
                                        //$resExec = execQry($db2LibCon, $qryData['STREXECSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'], $qryData['MBRDATALST']);
                                        if ($resExec['RTN'] !== 0) {
                                            $msg = showMsg($resExec['MSG']);
                                            //クエリー　＊クエリーの実行チェックに失敗しました
                                            //$errMsg = showMsg('FAIL_FUNC', array('クエリーの実行チェック')); //"クエリーの実行チェックに失敗しました。";
                                            $errMsg = $msg;
                                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst);
                                        } else {
                                            if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                                $resCreateTbl = createTmpTable($db2LibCon, $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $schDBname, $qryData['MBRDATALST']);
                                            } else {
                                                $resCreateTbl = createTmpTableRDB($db2con, $db2LibCon, $qryData['SELRDB'], $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $schDBname, $qryData['MBRDATALST']);
                                            }
                                            if ($resCreateTbl['RTN'] !== 0) {
                                                $msg = showMsg($resCreateTbl['MSG']);
                                                //クエリー　＊クエリーの実行に失敗しました
                                                $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行')); //"クエリーの実行に失敗しました。";
                                                fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                                            }
                                        }
                                    }
                                }
                            }
                            if (cmMer($wspkey) === '') {
                                e_log("Seigyofilename ###3 => ".$Seigyofilename." , schDBname => ".$schDBname);
                                if ($fdb2csv1['SEIGYOFLG'] == 1 && trim($Seigyofilename," ") !== ""){
                                    $schDBname = $Seigyofilename;
                                }
                                if ($fdb2csv1['D1DIRE'] !== '') {
                                    $rsCreateCsv = createCSVFile($db2con, $db2LibCon, $paramQGData['D1NAME'], $fdb2csv1['D1CFLG'], $schDBname, $nowYmd, $nowTime, $fdb2csv1['D1DIRE'], $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $curTime,$Seigyofilename);//MSM20180830
                                    if ($rsCreateCsv !== 0) {
                                        $errMsg = showMsg('FAIL_SEL');
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $rsCreateCsv, array(), $isQGflg);
                                    }
                                }
                                if ($fdb2csv1['D1DIEX'] !== '') {
                                    //e_log('【削除】IFSのXLS取得'.$fdb2csv1['D1DIEX'].$schDBname);
                                    // XLSのテンプレートがある場合、ファイルあるかチェック
                                    if (chkXlsTemplate($paramQGData['D1NAME'], $fdb2csv1['D1TMPF'])) {
                                        $rsCreateExl = createExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $fdb2csv1['D1CFLG'], $paramQGData['D1NAME'], $schDBname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1DIEX'], $mailAddLst, $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $cnddatArr,$Seigyofilename);//MSM20180830
                                        if ($rsCreateExl !== 0) {
                                            $errMsg = showMsg('FAIL_SEL');
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), $isQGflg);
                                        }
                                    } else {
                                        if ($templateErrCnt === 0) {
                                            $errMsg = 'IFSにXLSファイル出力失敗:' . showMsg('NOTEXIST_TMP', array($d1name));
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), $isQGflg);
                                        }
                                    }
                                }
                            } else {
                                if ($fdb2csv1['D1SOEX'] !== '') {
                                    //e_log('【削除】IFSのピボット取得:'.$schDBname.'クエリー：'.$d1name.'ピボット：'.$wspkey);
                                    $piexfi = createPivotExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $paramQGData['D1NAME'], $schDBname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1SOEX'], $fdb2csv1['D1WEBF'], $cnddatArr, $wspkey, $fdb2csv1['SOCTFL']);
                                    if ($piexfi !== 0) {
                                        $errMsg = showMsg('FAIL_SEL');
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), $isQGflg);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if ($noBurstFlg === 1 || $noBurstFlg === 2) {
                    $curTime = getCurrentTimeStamp($db2con);
                    if ($fdb2csv1['D1WEBF'] !== '1') {
                        //IFS出力先に設定されたEXCELファイルが作成される。
                        //e_log('【削除】IFS出力開始'.$fdb2csv1['D1DIEX']);
                        //e_log('【削除】実行種類③:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                        if (cmMer($wspkey) === '') {
                            if ($fdb2csv1['D1DIEX'] !== '') {
                                //e_log('【削除】IFSXLS出力開始'.'DIR:'.$fdb2csv1['D1DIEX'].'WEBF'.$fdb2csv1['D1WEBF']);
                                // XLSのテンプレートがある場合、ファイルあるかチェック
                                    e_log("Seigyofilename ###4 => ".$Seigyofilename." , schDBname => ".$schDBname);
                                    if ($fdb2csv1['SEIGYOFLG'] == 1 && trim($Seigyofilename," ") !== ""){
                                        $schDBname = $Seigyofilename;
                                    }
                                if (chkXlsTemplate($d1name, $fdb2csv1['D1TMPF'])) {
                                    //　IFSのXLS出力
                                    $rsCreateExl = createExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], '', $d1name, $schDBname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1DIEX'], $mailAddLst, $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $cnddatArr,$Seigyofilename);//MSM20180830
                                    if ($rsCreateExl !== 0) {
                                        $errMsg = 'EXCELファイル作成失敗';
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), false);
                                    }
                                } else {
                                    if ($templateErrCnt === 0) {
                                        $errMsg = 'IFSにXLSファイル出力失敗:' . showMsg('NOTEXIST_TMP', array($d1name));
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), false);
                                    }
                                }
                            }
                        } else {
                            //ピボット実行
                            if ($fdb2csv1['D1SOEX'] !== '') {
                                if ($noBurstFlg === 2) {
                                    $pivottemp = '';
                                    $pivottemp = cmRecreateRnPivotTbl5250($db2con, $d1name, $wspkey, $schDBname, array());
                                    if ($pivottemp !== '') {
                                        $pvdbname = $pivottemp;
                                    }
                                    $fdb2csv1['TMP_D1CFLG'] = '';
                                } else {
                                    $pvdbname = $dbname;
                                }
                                $piexfi = createPivotExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $d1name, $pvdbname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1SOEX'], $fdb2csv1['D1WEBF'], $cnddatArr, $wspkey, $fdb2csv1['SOCTFL']);
                                if ($piexfi !== 0) {
                                    $errMsg = showMsg('FAIL_SEL');
                                    fnCreateHistory($db2con, $d1name, $filename, $pvdbname, $nowYmd, $nowTime, '', $errMsg, array(), false);
                                }
                            }
                        }
                    } else {
                        if ($fdb2csv1['D1CFLG'] === '1') {
                            e_log("Seigyofilename ###5 => ".$Seigyofilename." , schDBname => ".$schDBname);
                            if ($fdb2csv1['SEIGYOFLG'] == 1 && trim($Seigyofilename," ") !== ""){
                                $schDBname = $Seigyofilename;
                               // $dbname = $Seigyofilename;
                            }
                            //e_log('【削除】実行種類⑥①:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                            if (cmMer($wspkey) === '') {
                                if ($fdb2csv1['D1DIRE'] !== '') {
                                    //e_log('【削除】IFSのSQLのCSV取得'.$fdb2csv1['D1DIRE'].$schDBname);
                                    //$rsCreateCsv = createCSVFile($db2con, $db2LibCon, $d1name, $fdb2csv1['D1CFLG'], $schDBname, $nowYmd, $nowTime, $fdb2csv1['D1DIRE'], $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $curTime);
                                    $rsCreateCsv = createCSVFile($db2con, $db2LibCon, $d1name, $fdb2csv1['D1CFLG'], $schDBname, $nowYmd, $nowTime, $fdb2csv1['D1DIRE'], $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $curTime,$Seigyofilename);//MSM20180830
                                    if ($rsCreateCsv !== 0) {
                                        $errMsg = showMsg('FAIL_SEL');
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $rsCreateCsv, array(), false);
                                    }
                                }
                                if ($fdb2csv1['D1DIEX'] !== '') {
                                    e_log('【削除】IFSXLS出力開始' . 'DIR:' . $fdb2csv1['D1DIEX'] . 'WEBF' . $fdb2csv1['D1WEBF']);
                                    // XLSのテンプレートがある場合、ファイルあるかチェック
                                    if (chkXlsTemplate($d1name, $fdb2csv1['D1TMPF'])) {
                                        //　IFSのXLS出力
                                        $rsCreateExl = createExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $fdb2csv1['D1CFLG'], $d1name, $schDBname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1DIEX'], $mailAddLst, $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $cnddatArr,$Seigyofilename);//MSM20180830
                                        if ($rsCreateExl !== 0) {
                                            $errMsg = 'EXCELファイル作成失敗';
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), false);
                                        }
                                    } else {
                                        if ($templateErrCnt === 0) {
                                            $errMsg = 'IFSにXLSファイル出力失敗:' . showMsg('NOTEXIST_TMP', array($d1name));
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), false);
                                        }
                                    }
                                }
                            } else {
                                //ピボット実行
                                if ($fdb2csv1['D1SOEX'] !== '') {
                                    if ($noBurstFlg === 2) {
                                        $pivottemp = '';
                                        //$pivottemp = createPivotSQLQryTbl($db2con, $d1name, $wspkey, $schDBname, array());
                                        $pivottemp = createPivotSQLQryTbl($d1name, $wspkey, $schDBname, array());
                                        if ($pivottemp !== '') {
                                            $pvdbname = $pivottemp;
                                        }
                                    } else {
                                        $pvdbname = $dbname;
                                    }
                                    $piexfi = createPivotExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $d1name, $pvdbname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1SOEX'], $fdb2csv1['D1WEBF'], $cnddatArr, $wspkey, $fdb2csv1['SOCTFL']);
                                    if ($piexfi !== 0) {
                                        $errMsg = showMsg('FAIL_SEL');
                                        fnCreateHistory($db2con, $d1name, $filename, $pvdbname, $nowYmd, $nowTime, '', $errMsg, array(), false);
                                    }
                                }
                            }
                        } else {
                            //e_log('【削除】IFS出力開始');
                            if ($noBurstFlg === 2) {
                                //e_log('【削除】IFS出力開始：制御又ピボットに全データ取得');
                                // 全部のデータ取得実行必要
                                if (cmMer($wspkey) === '') {
                                    $resExeSql = runExecuteSQL($db2con, $d1name);
                                } else {
                                    $resExeSql = runExecuteSQLPIVOT($db2con, $d1name, $wspkey);
                                }
                                if ($resExeSql['RTN'] !== 0) {
                                    //クエリー　＊クエリーの実行準備に失敗しました
                                    $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行準備')); //"クエリーの実行準備に失敗しました。";
                                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                                } else {
                                    $qryData = $resExeSql;
                                    $exeSQL = array('EXETESTSQL' => $qryData['STREXECTESTSQL'], 'EXESQL' => $qryData['STREXECSQL'], 'LOGSQL' => $qryData['LOG_SQL']);
                                    if ($fdb2csv1['SEIGYOFLG'] == 1 || cmMer($wspkey) !== '') {
                                        if (CRTQTEMPTBL_FLG === 1) {
                                            if (count($qryData['MBRDATALST']) > 0) {
                                                foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                                    if ($mbrdata['FILTYP'] === 'L' || $mbrdata['FILTYP'] === 'V') {
                                                        $rs = dropTmpFile($db2LibCon, 'QTEMP', $mbrdata['TBLNM']);
                                                        $rs = createTmpFil($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                                        if ($rs !== true) {
                                                            e_log('テーブル設定失敗');
                                                            $rtn = 1;
                                                            $msg = showMsg($rs);
                                                            break;
                                                        }
                                                    } else {
                                                        //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                                        $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                                        if ($rs !== true) {
                                                            e_log('テーブル設定失敗');
                                                            $rtn = 1;
                                                            $msg = showMsg($rs);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if (count($qryData['MBRDATALST']) > 0) {
                                                foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                                    //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                                    $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                                    if ($rs !== true) {
                                                        e_log('テーブル設定失敗');
                                                        $rtn = 1;
                                                        $msg = showMsg($rs);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    } else { 
                                        if (count($qryData['MBRDATALST']) > 0) {
                                            foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                                //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                                $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                                if ($rs !== true) {
                                                    e_log('テーブル設定失敗');
                                                    $rtn = 1;
                                                    $msg = showMsg($rs);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if ($rtn !== 1) {
                                        $resExec = execQry($db2LibCon, $qryData['STREXECTESTSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'], $qryData['MBRDATALST']);
                                        //$resExec = execQry($db2LibCon, $qryData['STREXECSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'], $qryData['MBRDATALST']);
                                        if ($resExec['RTN'] !== 0) {
                                            $msg = showMsg($resExec['MSG']);
                                            //クエリー　＊クエリーの実行チェックに失敗しました
                                            //$errMsg = showMsg('FAIL_FUNC', array('クエリーの実行チェック')); //"クエリーの実行チェックに失敗しました。";
                                            $errMsg = $msg;
                                            fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                                        } else {
                                            if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                                $resCreateTbl = createTmpTable($db2LibCon, $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $schDBname, $qryData['MBRDATALST']);
                                            } else {
                                                $resCreateTbl = createTmpTableRDB($db2con, $db2LibCon, $qryData['SELRDB'], $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $schDBname, $qryData['MBRDATALST']);
                                            }
                                            if ($resCreateTbl['RTN'] !== 0) {
                                                $msg = showMsg($resCreateTbl['MSG']);
                                                //クエリー　＊クエリーの実行に失敗しました
                                                $errMsg = showMsg('FAIL_FUNC', array('クエリーの実行')); //"クエリーの実行に失敗しました。";
                                                fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                                            }
                                        }
                                    }
                                }
                            }
                            if (cmMer($wspkey) === '') {
                                e_log("Seigyofilename ###6 => ".$Seigyofilename." , schDBname => ".$schDBname);

                                if ($fdb2csv1['SEIGYOFLG'] == 1 && trim($Seigyofilename," ") !== ""){
                                    $schDBname = $Seigyofilename;
                                }
                                if ($fdb2csv1['D1DIRE'] !== '') {
                                    $rsCreateCsv = createCSVFile($db2con, $db2LibCon, $d1name, $fdb2csv1['D1CFLG'], $schDBname, $nowYmd, $nowTime, $fdb2csv1['D1DIRE'], $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $curTime,$Seigyofilename);//MSM20180830
                                    if ($rsCreateCsv !== 0) {
                                        $errMsg = showMsg('FAIL_SEL');
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), false);
                                    }
                                }
                                if ($fdb2csv1['D1DIEX'] !== '') {
                                    //e_log('【削除】IFSのXLS取得'.$fdb2csv1['D1DIEX'].$schDBname);
                                    // XLSのテンプレートがある場合、ファイルあるかチェック
                                    if (chkXlsTemplate($d1name, $fdb2csv1['D1TMPF'])) {
                                        $rsCreateExl = createExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $fdb2csv1['D1CFLG'], $d1name, $schDBname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1DIEX'], $mailAddLst, $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $cnddatArr,$Seigyofilename);//MSM20180830
                                        if ($rsCreateExl !== 0) {
                                            $errMsg = showMsg('FAIL_SEL');
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), false);
                                        }
                                    } else {
                                        if ($templateErrCnt === 0) {
                                            $errMsg = 'IFSにXLSファイル出力失敗:' . showMsg('NOTEXIST_TMP', array($d1name));
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), false);
                                        }
                                    }
                                }
                            } else {
                                if ($fdb2csv1['D1SOEX'] !== '') {
                                    //e_log('$$$$$$$$$$$$【削除】IFSのピボット取得:'.$schDBname.'クエリー：'.$d1name.'ピボット：'.$wspkey);
                                    $piexfi = createPivotExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $d1name, $schDBname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1SOEX'], $fdb2csv1['D1WEBF'], $cnddatArr, $wspkey, $fdb2csv1['SOCTFL']);
                                    if ($piexfi !== 0) {
                                        $errMsg = showMsg('FAIL_SEL');
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array(), false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    //e_log("実行ローグとクエリー実行履歴のため:$exeFlg:".$exeFlg);
    // 実行ローグとクエリー実行履歴のため
    if ($exeFlg) {
        if ($fdb2csv1['D1WEBF'] !== '1') {
            $rs = cmInsertDB2WLOG($db2con, $logrs, 'SCHEDULE', 'D', $logd1name, array(), '0', '');
            cmInsertDB2WSTL($db2con, $rs['DATE'],$rs['TIME'],'SCHEDULE',$jktime,$qryCnt);//クエリー実行時間追加
        } else {
            $sql_wlsqlb = $WLSQLB;
            if (strlen($sql_wlsqlb) > 32730) {
                $sql_wlsqlb = showMsg('INVALID_SQLLENGTH');
            }
            $jktime = $eend - $estart;//190405
            $rs = cmInsertDB2WLOG($db2con, $logrs, 'SCHEDULE', 'D', $logd1name, array(), '1', $sql_wlsqlb);
            cmInsertDB2WSTL($db2con, $rs['DATE'],$rs['TIME'],'SCHEDULE',$jktime,$qryCnt);//クエリー実行時間追加
        }
        fnDB2QHISExe($db2con, $logd1name, 'SCHEDULE', $isQGflg);
        //クエリー実行時間の最大、最小、平均アップデート
        $dataHis = fnGetDB2QHIS($db2con,$logd1name);
        $max = $dataHis['data'][0]['DQTMAX'];
        $min = $dataHis['data'][0]['DQTMIN'];
        if($max !== '.00' && $min !== '.00'){
            $max = max($max,$jktime);
            $min = min($max,$jktime);
        }else{
            $max = $jktime;
            $min = $jktime;
        }
        cmUpdDB2QHIS($db2con, $logd1name, $max, $min, ($max + $min)/2); 
    }
    //////////////////////////////////メール送信ためのファイル作成開始//////////////////////////////////////
    //e_log("メール送信ためのファイル作成開始:RTN: ".$rtn.' ,ISQGFLG: '.$isQGflg.' ,pivotkey: '.$hiswspkey);
    // メール送信ためのファイル作成開始
    if ($rtn !== 1) {
        //e_log('【削除】実行種類③0:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
        // 定義のワークテーブルとカラムチェックしてFDB2CSV2取得
        if ($isQGflg) {
            $filename = '';
            $fdb2csv2 = array();
            $fdb2csv2_ALL = array();
            $csv_h = array();
            // xls,csv,htmlの一時ファイル名
            $filename = (cmMer($d1name) . '_' . $nowYmd . '_' . $nowTime);
            //e_log('【削除】実行種類③0①①:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $hiswspkey, '', array(), $isQGflg);
            //e_log('【削除】実行種類③0①:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
            $wspkey = cmMer($wspkey);
            $rsfdb2csv2 = cmGetFDB2CSV2($db2con, $paramQGData['D1NAME'], true,false,false);
            if ($rsfdb2csv2['result'] !== true) {
                e_log(showMsg($rsfdb2csv2['result'], array('exeMail.php', 'cmGetFDB2CSV2')), '1');
                $rtn = 1;
            } else {
                $fdb2csv2 = umEx($rsfdb2csv2['data']);
                $rsfdb2csv2 = cmGetFDB2CSV2($db2con, $paramQGData['D1NAME'], false,false,false);
                if ($rsfdb2csv2['result'] !== true) {
                    e_log(showMsg($rsfdb2csv2['result'], array('exeMail.php', 'cmGetFDB2CSV2')), '1');
                    $rtn = 1;
                } else {
                    $fdb2csv2_ALL = umEx($rsfdb2csv2['data']);
                    //ヘッダー情報取得
                    //e_log('②【削除】ヘッダー情報取得'.print_r($fdb2csv2_ALL,true));
                    $csv_h = cmCreateHeaderArray($fdb2csv2_ALL);
                    if (cmMer($burstItmInfo['WABAFLD']) !== '') {
                        $qryCol = $fdb2csv2_ALL;
                        $qryColData = array();
                        foreach ($qryCol as $value) {
                            array_push($qryColData, $value['D2FLD']);
                        }
                        //e_log('【削除】burstItmInfo after header check①:'.$burstItmInfo['WABAFLD'].print_r($qryColData,true));
                        if (!in_array($burstItmInfo['WABAFLD'], $qryColData)) {
                            $burstItmInfo = '';
                        }
                    }
                }
            }
            //スケジュールの出力ファイル
            //e_log('スケジュールの出力ファイル:$RTN'.$rtn.'FDB2CSV1:'.print_r($fdb2csv1,true));
            if($licenseFileOut){
                if ($rtn !== 1) {
                    if (cmMer($fdb2csv1['D1WEBF']) !== '') {
                        if (cmMer($fdb2csv1['D1OUTSE']) === '1') {
                            if (cmMer($fdb2csv1['D1OUTLIB']) !== '' && cmMer($fdb2csv1['D1OUTFIL']) !== '') {
                                $isTmp = true;
                                //スケジュールのファイル出力の上書きがない場合、SYSからライブラリーとファイルチェック
                                if ($fdb2csv1['D1OUTSA'] !== '1') {
                                    if (cmMer($fdb2csv1['D1OUTSR']) === '0' || cmMer($fdb2csv1['D1OUTSR']) === '') {
                                        $r = fnChkExistFileOutput($db2LibCon, cmMer($fdb2csv1['D1OUTLIB']), cmMer($fdb2csv1['D1OUTFIL']));
                                        if ($r['result'] !== true) {
                                            $rtn = ($r['rtn'] === 1) ? 1 : $rtn;
                                            $isTmp = false;
                                            $eMsg = ($r['rtn'] === 4) ? showMsg('FAIL_OUT', array($fdb2csv1['D1OUTLIB'], $fdb2csv1['D1OUTFIL'])) : showMsg($r['result']);
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $hiswspkey, $eMsg, $mailAddLst, $isQGflg);
                                        }
                                    }
                                }
                                //出力ファイル作成
                                //e_log('出力ファイル作成:DBNAME:'.$dbname.':D1OUTLIB：'.$fdb2csv1['D1OUTLIB'].':D1OUTFIL:'.$fdb2csv1['D1OUTFIL'].':ISTMP:'.$isTmp.':D1NAME: '.$fdb2csv1['D1NAME']);
                                if ($isTmp) {
                                    $add_flg = ($fdb2csv1['D1OUTSA'] === '1') ? true : false;
                                    $r = createOutputFileTable($db2LibCon, $dbname, $fdb2csv1['D1OUTLIB'], $fdb2csv1['D1OUTFIL'], $fdb2csv1['D1NAME'], $add_flg);
                                    if ($r['RTN'] !== 0) {
                                        //$rtn = 1;//Cause whethere true or false do mail send
                                        $eMsg = showMsg($r['MSG']);
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $hiswspkey, $msg, $mailAddLst, $isQGflg);
                                    } else {
                                        if ($paramQGData['LASTFLG'] === '1') {
                                            $eMsg = showMsg($r['MSG']);
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $hiswspkey, $eMsg, $mailAddLst, $isQGflg);
                                        }
                                    }
                                }
                            } else {
                                $eMsg = showMsg('FAIL_EXISTLIB');
                                fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $hiswspkey, $eMsg, $mailAddLst, $isQGflg);
                            }
                        }
                    }
                }
            }
            //クエリーグループの一番最クエリーIDしか消す
            if ($paramQGData['LASTFLG'] !== '1') {
                $rs = dropTmpFile($db2con, SAVE_DB, $dbname);
                if ($rs === false) {
                    $rtn = 1;
                    $msg = showMsg('FAIL_FUNC', array(SAVE_DB . '/' . $dbname . 'のドロップ')); //'CL連携の準備に失敗しました。';
                    
                }
            }
        } else {
            $filename = '';
            $fdb2csv2 = array();
            $fdb2csv2_ALL = array();
            $csv_h = array();
            // xls,csv,htmlの一時ファイル名
            $filename = (cmMer($d1name) . '_' . $nowYmd . '_' . $nowTime);
            //e_log('【削除】実行種類③0①①:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, '', array(), $isQGflg);
            //e_log('【削除】実行種類③0①:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
            $wspkey = cmMer($wspkey);
            $rsfdb2csv2 = cmGetFDB2CSV2($db2con, $d1name, true,false,false);
            if ($rsfdb2csv2['result'] !== true) {
                //e_log(showMsg($rsfdb2csv2['result'], array('exeMail.php', 'cmGetFDB2CSV2')), '1');
                $rtn = 1;
            } else {
                $fdb2csv2 = umEx($rsfdb2csv2['data']);
                //e_log('【削除】実行種類③0②:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                $rsfdb2csv2 = cmGetFDB2CSV2($db2con, $d1name, false,false,false);
                if ($rsfdb2csv2['result'] !== true) {
                    $rtn = 1;
                } else {
                    $fdb2csv2_ALL = umEx($rsfdb2csv2['data']);
                    //ヘッダー情報取得
                    //e_log('②【削除】ヘッダー情報取得'.print_r($fdb2csv2_ALL,true));
                    $csv_h = cmCreateHeaderArray($fdb2csv2_ALL);
                    if (cmMer($burstItmInfo['WABAFLD']) !== '') {
                        $qryCol = $fdb2csv2_ALL;
                        $qryColData = array();
                        foreach ($qryCol as $value) {
                            array_push($qryColData, $value['D2FLD']);
                        }
                        //e_log('【削除】burstItmInfo after header check①:'.$burstItmInfo['WABAFLD'].print_r($qryColData,true));
                        if (!in_array($burstItmInfo['WABAFLD'], $qryColData)) {
                            $burstItmInfo = '';
                        }
                    }
                }
            }
            if($licenseFileOut){
                //スケジュールの出力ファイル
                if ($rtn !== 1) {
                    if (cmMer($fdb2csv1['D1WEBF']) !== '') {
                        if ($fdb2csv1['D1OUTSE'] === '1') {
                            if ($fdb2csv1['D1OUTLIB'] !== '' && $fdb2csv1['D1OUTFIL'] !== '') {
                                $isTmp = true;
                                //スケジュールのファイル出力の上書きがない場合、SYSからライブラリーとファイルチェック
                                if ($fdb2csv1['D1OUTSA'] !== '1') {
                                    if ($fdb2csv1['D1OUTSR'] === '0' || $fdb2csv1['D1OUTSR'] === '') {
                                        $r = fnChkExistFileOutput($db2LibCon, $fdb2csv1['D1OUTLIB'], $fdb2csv1['D1OUTFIL']);
                                        if ($r['result'] !== true) {
                                            $rtn = ($r['rtn'] === 1) ? 1 : $rtn;
                                            $isTmp = false;
                                            $eMsg = ($r['rtn'] === 4) ? showMsg('FAIL_OUT', array($fdb2csv1['D1OUTLIB'], $fdb2csv1['D1OUTFIL'])) : showMsg($r['result']);
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $eMsg, $mailAddLst, $isQGflg);
                                        }
                                    }
                                }
                                //出力ファイル作成
                                if ($isTmp) {
                                    $add_flg = ($fdb2csv1['D1OUTSA'] === '1') ? true : false;
                                    $r = createOutputFileTable($db2LibCon, $dbname, $fdb2csv1['D1OUTLIB'], $fdb2csv1['D1OUTFIL'], $fdb2csv1['D1NAME'], $add_flg);
                                    if ($r['RTN'] !== 0) {
                                        //$rtn =1;//Cause whethere true or false do mail send
                                        $msg = showMsg($r['MSG']);
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $msg, $mailAddLst, $isQGflg);
                                    } else {
                                        $eMsg = showMsg($r['MSG']);
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $eMsg, $mailAddLst, $isQGflg);
                                    }
                                }
                            } else {
                                $eMsg = showMsg('FAIL_EXISTLIB');
                                fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $eMsg, $mailAddLst, $isQGflg);
                            }
                        }
                    }
                }
            }
        }
    }
    if ($rtn !== 1) {
        if ($isQGflg) { //クエリーグループ
            if ($paramQGData['LASTFLG'] === '1') {
                if ($rtn !== 1) {
                    //e_log('【削除】実行種類③①:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                    $wspkey = cmMer($wspkey);
                    // csv,xls,htmlのため取得データカウントとメール送信フラグ取得
                    $rownum = '';
                    $csv_header = $csv_h;
                    $h_fdb2csv2 = $fdb2csv2;
                    //e_log('【削除】FDB2CSV2取得ヘダー①：'.'csv_hcount:'.count($csv_h).'fdb2csv2count'.count($fdb2csv2).print_r($h_fdb2csv2,true));
                    //e_log('【削除】カウント取得①'.$fdb2csv1['RDBNM'].print_r($fdb2csv1,true));
                    if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === RDB || $fdb2csv1['RDBNM'] === RDBNAME) {
                        //e_log('【削除】カウント取得①1708' . $dbname);
                        $csv_db_count = cmGetAllCountBurstItm($db2con, $burstItmInfo, $dbname, $fdb2csv1['TMP_D1CFLG']);
                    } else {
                        //e_log('【削除】カウント取得⑫1711' . $dbname);
                        $csv_db_count = cmGetAllCountBurstItm($db2LibCon, $burstItmInfo, $dbname, $fdb2csv1['TMP_D1CFLG']);
                    }
                    $allcount = $csv_db_count[0];
                    // ゼロー件の場合メール送信かのチェック
                    $mailFlg = '1';
                    if ($allcount == 0 && $fdb2csv1['ZEROMAIL'] == 1) {
                        $mailFlg = '0'; // 0件データーメール配信することができません。
                        
                    }
                }
                //e_log("添付ファイルの種別のフラグ取得");
                // 添付ファイルの種別のフラグ取得
                if ($rtn !== 1) {
                    //e_log('【削除】実行種類③②:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                    $csvFlg = '';
                    $xlsFlg = '';
                    $htmlFlg = '';
                    $mailHtmlFlg = '';
                    $wspkey = cmMer($wspkey);
                    $rsDb2wautFil = getDB2WAUTFilFlg($db2con, $d1name, $wspkey);
                    if ($rsDb2wautFil['result'] === true) {
                        if (count($rsDb2wautFil['data']) > 0) {
                            $csvFlg = $rsDb2wautFil['data']['WACSV'];
                            $xlsFlg = $rsDb2wautFil['data']['WAXLS'];
                            $htmlFlg = $rsDb2wautFil['data']['WAHTML'];
                            $mailHtmlFlg = $rsDb2wautFil['data']['WATYPE'];
                        }
                    }
                }
                //e_log("DB2EINS rtn " . $rtn);
                if ($rtn !== 1) {
                    //e_log('【削除】実行種類③③:' . ($wspkey === '' ? 'クエリー実行' : 'ピボット実行'));
                    $DB2EINSROWMAX = 0;
                    $xlsCustomFlg = '';
                    if ($mailFlg !== '0') {
                        //e_log("mailFlg rtn " . $mailFlg);
                        $DB2ECON = array();
                        $DB2EINS = array();
                        $CNDSDATAF = array();
                        // Excelテンプレートの文字挿入設定の場合
                        if ($fdb2csv1['D1EDKB'] === '2') {
                            $DB2ECON = cmGetDB2ECON($db2con, $paramQGData['D1NAME']);
                            if ($DB2ECON['result'] === true) {
                                $DB2ECON = umEx($DB2ECON['data']);
                            }
                            $DB2EINS = cmGetDB2EINS($db2con, $paramQGData['D1NAME']);
                            if ($DB2EINS['result'] === true) {
                                $EIROWMAX = $DB2EINS['MAXEIROW'];
                                $DB2EINS = umEx($DB2EINS['data']);
                            }
                            // e_log("DB2EINS original " . print_r($DB2EINS, true));
                            $CNDSDATAF = cmCNDSDATA($fdb2csv1['D1WEBF'], $db2con, $paramQGData['D1NAME'], array(), $fdb2csv1['D1CFLG']);
                            $DB2EINSROWMAX = cmGetCountSearchData($DB2ECON, $EIROWMAX, $CNDSDATAF); //EXCEL SETTING 挿入文字カウント
                            
                        } else if ($fdb2csv1['D1EDKB'] !== '2' && $fdb2csv1['D1TMPF'] !== '' && cmMer($wspkey) === '') {
                            // xlsの印字開始行番号取得
                            $DB2EINSROWMAX = $fdb2csv1['D1TROS'];
                        }
                        // ベダーのためデータ開始行の番号を1足す
                        if ($fdb2csv1['D1EDKB'] === '2' || cmMer($wspkey) !== '' || $fdb2csv1['D1TMPF'] === '') {
                            $DB2EINSROWMAX+= 1;
                        }
                        // 制御取得の場合列に二つの列追加
                        if ($fdb2csv1['SEIGYOFLG'] == 1) {
                            $csv_header = $csv_h;
                            array_unshift($csv_header, "", "");
                            $h_fdb2csv2 = $fdb2csv2;
                            $h_fdb2csv2 = cmArrayUnshift($h_fdb2csv2);
                        }
                        $csv_d = array();
                        //取得データでCSVファイル作成
                        if ($csvFlg === '1') {
                            //e_log('【削除】CSVファイル作成開始　ヘーダ印字');
                            require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
                            $csv_fp = exeCsvstream($filename);
                            exeCsv($csv_fp, $csv_header, $csv_d, $h_fdb2csv2, $paramQGData['D1NAME'], $filename, $mailAddLst, $nowYmd, $nowTime, true);
                            //e_log('【削除】CSVファイル作成終了　ヘーダ印字');
                            
                        }
                        $EXCELVER = false; //エクセルバージョン管理
                        //取得したデータでXLSファイル作成
                        if ($xlsFlg === '1') {
                            //e_log('【削除】XLSファイル作成開始　ヘーダ印字 PIVOTKEY:'.$wspkey);
                            //e_log('【削除】実行種類④:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                            if (cmMer($wspkey) !== '') {
                                //e_log('【削除】XLSフラグチェックPIVOT');
                                // ピボットのxlsファイル作成
                                $ISEXCELMAXLEN = 0;
                            } else {
                                // クエリのxlsファイル作成
                                $resXlsCustom = '';
                                if ($fdb2csv1['D1EDKB'] === '1') {
                                    $xlsCustomFlg = $fdb2csv1['D1EDKB'];
                                }
                                // XLSテンプレートがある場合ファイルあるかをチェックする
                                if (chkXlsTemplate($paramQGData['D1NAME'], $fdb2csv1['D1TMPF'])) {
                                    // かステムテンプレートじゃないの場合
                                    if ($xlsCustomFlg === '') {
                                        //e_log('【削除】XLSフラグチェックReachCustom無し');
                                        $sheet = '';
                                        $book = '';
                                        $excel_fp = '';
                                        $filepath = BASE_DIR . '/php/xls/';
                                        //  $filepath = '/QNTC/192.168.1.12/TEST/';
                                        $D1TMPF = '';
                                        $ext = getExt($paramQGData['D1NAME']);
                                        //MAX件数のためカウント
                                        if ($ext !== '') {
                                            $EXCELMAXLEN = ($ext === 'xlsm' || $ext === 'xlsx') ? 1048576 : (($ext === 'xls') ? 65536 : 0);
                                            $EMAXLEN_FILE = $ext;
                                        } else {
                                            if (EXCELVERSION === '2007' || EXCELVERSION === 'XLSX') {
                                                $EXCELMAXLEN = 1048576;
                                                $EMAXLEN_FILE = 'xlsx';
                                            } else if (EXCELVERSION === '2003' || EXCELVERSION === 'html') {
                                                $EXCELMAXLEN = 65536;
                                                $EMAXLEN_FILE = 'xls';
                                            }
                                        }
                                        //if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                        if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                            $DB2EINSROWMAX+= ($allcount === 0 || $allcount === '' || cmMer($fdb1csv1['RSMASTERARR'][0]['DCSUMF']) === '1') ? 0 : $allcount;
                                            //e_log('【削除】制御カウント取得①：'.print_r($fdb2csv2,true).print_r($fdb2csv1['RSMASTERARR'],true));
                                            $DB2EINSROWMAX+= cmGetALLCOUNTSEIKYO($db2con, $paramQGData['D1NAME'], $dbname, $fdb2csv1['RSMASTERARR'], $fdb2csv2, '', $burstItmInfo);
                                        } else {
                                            //e_log('【削除】XLSシート取得カウント制御じゃない：'.$allcount);
                                            $DB2EINSROWMAX+= ($allcount === 0 || $allcount === '') ? 0 : $allcount;
                                        }
                                        //e_log('in exeMailSend.php $DB2EINSROWMAX=>'.$DB2EINSROWMAX.'$EXCELMAXLEN=>'.$EXCELMAXLEN);
                                        if ($DB2EINSROWMAX > $EXCELMAXLEN) {
                                            // 取得結果データ件数はEXCELの最大行数を超えている場合
                                            $ISEXCELMAXLEN = 1;
                                            $errMsg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                            $mailAddLst[0]['WAXLS'] = '9';
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, false);
                                        } else {
                                            //【削除】取得結果データ件数はEXCELの最大行数を超えていない場合、XLSファイル作成
                                            $ISEXCELMAXLEN = 0;
                                            if (EXCELVERSION == 'XLSX' && $ext === '') {
                                                $EXCELVER = true;
                                                $filepath = BASE_DIR . '/php/xls/';
                                                $excelname = $filename . '.xlsx';
                                                $xlsxPath = $filepath . $excelname;
                                                $writer = WriterFactory::create(Type::XLSX);
                                                $writer->openToFile($xlsxPath);
                                                spoutMojiSounyuu($writer, $DB2EINS, $DB2ECON, $CNDSDATAF); //SPOUT EXCELの見出しの上に文字挿入と抽出条件の表示設定
                                                e_log("DB2EINS" . print_r($DB2EINS, true));
                                                $writer->addRow($csv_header);
                                            } else {
                                                if (EXCELVERSION == '2003' || EXCELVERSION == '2007' || $ext !== '') {
                                                    if ($ext !== '') {
                                                    } else if (EXCELVERSION == '2003') {
                                                        $ext = 'xls';
                                                    } else if (EXCELVERSION == '2007') {
                                                        $ext = 'xlsx';
                                                    }
                                                    $excelname = $filename . '.' . $ext;
                                                    $sheet = php_excelCreateHeading($paramQGData['D1NAME']);
                                                    $DB2WCOL = array();
                                                    $book = $sheet['BOOK'];
                                                    $sheet = $sheet['SHEET'];
                                                } else if (EXCELVERSION == 'html' || $ext === '') {
                                                    $excelname = $filename . '.xls';
                                                    if (!file_exists($filepath . $excelname)) {
                                                        touch($filepath . $excelname);
                                                    }
                                                    $excel_fp = fopen(BASE_DIR . '/php/xls/' . $excelname, 'w');
                                                    excelCreateHeading($excel_fp, $DB2ECON, $DB2EINS, $CNDSDATAF);
                                                }
                                                $rownum = exeExcel($excel_fp, $csv_header, $csv_d, $h_fdb2csv2, $filename, $wspkey, $paramQGData['D1NAME'], $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $sheet, $rownum, '0', $cnddatArr);
                                            }
                                        }
                                    } else {
                                        require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
                                        $csv_temp = exeCsvstream($filename, $xlsCustomFlg);
                                        exeCsv($csv_temp, $csv_header, $csv_d, $h_fdb2csv2, $paramQGData['D1NAME'], $filename, $mailAddLst, $nowYmd, $nowTime, true, $xlsCustomFlg);
                                    }
                                } else {
                                    if ($templateErrCnt === 0) {
                                        $errMsg = showMsg('NOTEXIST_TMP', array($d1name));
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $hiswspkey, $errMsg, array(), false);
                                    }
                                }
                            }
                            //e_log('【削除】XLSファイル作成終了　ヘーダ印字');
                            
                        }
                        //取得データでHTMLファイル作成
                        if ($htmlFlg === '1') {
                            $filepath = BASE_DIR . '/php/html/';
                            $htmlname = $filename . '.html';
                            if (!file_exists($filepath . $htmlname)) {
                                touch($filepath . $htmlname);
                            }
                            $html_d = $csv_d;
                            $html_fp = fopen($filepath . $htmlname, 'w');
                            excelCreateHeading($html_fp, $DB2ECON, $DB2EINS, $CNDSDATAF);
                            exeHtml($html_fp, $csv_header, $html_d, $h_fdb2csv2, $filename, $wspkey, $paramQGData['D1NAME'], $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $cnddatArr);
                        }
                        if ($rtn !== 1) {
                            //e_log('【削除】取得データ印字開始');
                            $lenArr = cmTotalLen($allcount);
                            $lenCount = count($lenArr);
                            $rtnMail = 0;
                            // htmlフォマットメール送信用
                            $breakFlg = false;
                            $qryHtmlCount = 0;
                            $htmltabledata = array();
                            $tbdata = '';
                            //e_log('【削除】実行種類⑤:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                            if (cmMer($wspkey) === '') {
                                //e_log('【削除】クエリーループしてデータ取得：'.$allcount.'length'.$lenCount );
                                for ($idx = 0;$idx < $lenCount;$idx++) {
                                    //e_log('【削除】ループしてデータ取得：'.print_r($burstItmInfo,true) );
                                    if ($rtnMail !== 1) { //ロープしながら、エラーが出たら、rtnMailは１になって続かない
                                        if ($fdb2csv1['SEIGYOFLG'] != 1) {
                                            //e_log('【削除】制御じゃないのデータ取得');
                                            if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === RDB || $fdb2csv1['RDBNM'] === RDBNAME) {
                                                //e_log('【削除】ループしてデータ取得①：'.$allcount );
                                                //e_log('【削除】バーストデータ①：'.$dbname );
                                                $csv_d = cmGetDbFileBurstItm($db2con, $fdb2csv2, $burstItmInfo, $dbname, $fdb2csv1['TMP_D1CFLG'], $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', '', array(), true);
                                            } else {
                                                //e_log('【削除】ループしてデータ取得②：'.$allcount );
                                                //e_log('【削除】バーストデータ②：'.$dbname );
                                                $csv_d = cmGetDbFileBurstItm($db2LibCon, $fdb2csv2, $burstItmInfo, $dbname, $fdb2csv1['TMP_D1CFLG'], $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', '', array(), true);
                                            }
                                        } else {
                                            //e_log('【削除】制御のデータ取得');
                                            if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === RDB || $fdb2csv1['RDBNM'] === RDBNAME) {
                                                $tabledata = fnDataShukei($db2con, $fdb2csv2, $fdb2csv2_ALL, $paramQGData['D1NAME'], $dbname, $db2wcol, $fdb2csv1['RSMASTERARR'], $fdb2csv1['RSSHUKEIARR'], $burstItmInfo, $lenArr[$idx][0], $lenArr[$idx][1], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $allcount);
                                            } else {
                                                $tabledata = fnDataShukei($db2LibCon, $fdb2csv2, $fdb2csv2_ALL, $paramQGData['D1NAME'], $dbname, $db2wcol, $fdb2csv1['RSMASTERARR'], $fdb2csv1['RSSHUKEIARR'], $burstItmInfo, $lenArr[$idx][0], $lenArr[$idx][1], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $allcount);
                                            }
                                            $csv_h_shukei = $csv_h;
                                            array_unshift($csv_h_shukei, "", "");
                                            $fdb2csv2_shukei = $fdb2csv2;
                                            $fdb2csv2_shukei = cmArrayUnshift($fdb2csv2_shukei);
                                        }
                                        //e_log('【削除】CSVFLG：'.$csvFlg . ' XLSFLG:'.$xlsFlg. ' HTMLFLG:'.$htmlFlg. ' MAILHTMLFLG:'.$mailHtmlFlg);
                                        if ($csvFlg === '1') {
                                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                                //e_log('【削除】CSV配信制御データ④'.count($tabledata));
                                                exeCsv($csv_fp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, cmMer($paramQGData['D1NAME']), $filename, $mailAddLst, $nowYmd, $nowTime, false);
                                            } else {
                                                //e_log('【削除】CSV配信データ③'.count($csv_d));
                                                exeCsv($csv_fp, $csv_h, $csv_d, $fdb2csv2, cmMer($paramQGData['D1NAME']), $filename, $mailAddLst, $nowYmd, $nowTime, false);
                                            }
                                        }
                                        if ($xlsFlg === '1') {
                                            if (chkXlsTemplate($paramQGData['D1NAME'], $fdb2csv1['D1TMPF'])) {
                                                if ($xlsCustomFlg === '') {
                                                    if ($ISEXCELMAXLEN === 0) { //MAX件数のためカウントのチェック
                                                        if (EXCELVERSION == 'XLSX' && $EXCELVER === true) {
                                                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                                                $c_data = array_chunk($tabledata, 100);
                                                            } else {
                                                                $c_data = array_chunk($csv_d, 100);
                                                            }
                                                            foreach ($c_data as $ck => $cv) {
                                                                $writer->addRows($cv);
                                                            }
                                                        } else {
                                                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                                                //e_log('【削除】XLS制御配信データ④'.count($tabledata));
                                                                $rownum = exeExcel($excel_fp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename, $wspkey, $paramQGData['D1NAME'], $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $sheet, $rownum, '1', $cnddatArr);
                                                            } else {
                                                                //e_log('【削除】XLS配信データ⑤'.count($csv_d));
                                                                $rownum = exeExcel($excel_fp, $csv_h, $csv_d, $fdb2csv2, $filename, $wspkey, $paramQGData['D1NAME'], $db2con, $dbname, $burstItmInfo, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $sheet, $rownum, '1', $cnddatArr);
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                                        //e_log('【削除】XLSテンプレート制御CSV配信データ④'.count($tabledata));
                                                        exeCsv($csv_temp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, cmMer($paramQGData['D1NAME']), $filename, $mailAddLst, $nowYmd, $nowTime, false, $xlsCustomFlg);
                                                    } else {
                                                        //e_log('【削除】XLSテンプレートCSV配信データ⑤'.count($csv_d));
                                                        exeCsv($csv_temp, $csv_h, $csv_d, $fdb2csv2, cmMer($paramQGData['D1NAME']), $filename, $mailAddLst, $nowYmd, $nowTime, false, $xlsCustomFlg);
                                                    }
                                                }
                                            } else {
                                                if ($templateErrCnt === 0) {
                                                    $errMsg = showMsg('NOTEXIST_TMP', array($d1name));
                                                    fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $hiswspkey, $errMsg, array(), $isQGflg);
                                                }
                                            }
                                        }
                                        //取得データでHTMLファイル作成
                                        if ($htmlFlg === '1') {
                                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                                //e_log('【削除】HTML制御データ①'.count($tabledata));
                                                exeHtml($html_fp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename, $wspkey, $paramQGData['D1NAME'], $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $cnddatArr);
                                            } else {
                                                //e_log('【削除】HTMLデータ①'.count($csv_d));
                                                exeHtml($html_fp, $csv_h, $csv_d, $fdb2csv2, $filename, $wspkey, $paramQGData['D1NAME'], $db2con, $dbname, $burstItmInfo, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $cnddatArr);
                                            }
                                        }
                                        // htmlフォマットでのメール送信
                                        if ($mailHtmlFlg === '1') {
                                            $lastRow = false;
                                            if ($idx === $lenCount - 1) {
                                                $lastRow = true;
                                            }
                                            if ($breakFlg !== true) {
                                                if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                                    $rtnTbdata = exeHtmlMail($csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename, $fdb2csv1['D1WEBF'], $idx, $lastRow, $qryHtmlCount);
                                                    $qryHtmlCount = $rtnTbdata['QRYHTMLCOUNT'];
                                                    $breakFlg = $rtnTbdata['HTMLCREATEBREAK'];
                                                    $tbdata.= $rtnTbdata['TBDATA'];
                                                } else {
                                                    $rtnTbdata = exeHtmlMail($csv_h, $csv_d, $fdb2csv2, $filename, $fdb2csv1['D1WEBF'], $idx, $lastRow, $qryHtmlCount);
                                                    $qryHtmlCount = $rtnTbdata['QRYHTMLCOUNT'];
                                                    $breakFlg = $rtnTbdata['HTMLCREATEBREAK'];
                                                    $tbdata.= $rtnTbdata['TBDATA'];
                                                }
                                            }
                                            if ($breakFlg === true) {
                                                if ($xlsFlg !== '1' && $csvFlg !== '1' && $htmlFlg !== '1') {
                                                    break; // break from querycount loop
                                                    
                                                }
                                            }
                                        }
                                    }
                                    unset($csv_d);
                                    unset($tabledata);
                                }
                                //e_log('【削除】実行種類⑥:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                                if (cmMer($wspkey) === '') {
                                    if ($csvFlg === '1') {
                                        fclose($csv_fp); //csv file close
                                        $csvname = $filename . '.csv';
                                        $filepath = BASE_DIR . '/php/csv/';
                                        makeCopyFile($filepath, $csvname, $mailAddLst, $nowYmd, $nowTime, '.csv');
                                    }
                                    if ($xlsFlg === '1') {
                                        // かステムテンプレートじゃないの場合
                                        if ($xlsCustomFlg === '') {
                                            if ($ISEXCELMAXLEN === 0) { //MAX件数のためカウントのチェック
                                                $filepath = BASE_DIR . '/php/xls/';
                                                //  $filepath = '/QNTC/192.168.1.12/TEST/';
                                                if (EXCELVERSION == 'html' && $ext === '') {
                                                    $excelname = $filename . '.xls';
                                                    fwrite($excel_fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                                                    fclose($excel_fp);
                                                    $ext = '.xls';
                                                } else {
                                                    if ((EXCELVERSION == '2003' && $ext === '') || $ext === 'xls') {
                                                        $excelname = $filename . '.xls';
                                                        $writer = PHPExcel_IOFactory::createWriter($book, "Excel5");
                                                        $writer->save($filepath . $excelname);
                                                        $book = null;
                                                        $ext = '.xls';
                                                    } else if ((EXCELVERSION == '2007' && $ext === '') || $ext === 'xlsx') {
                                                        $excelname = $filename . '.xlsx';
                                                        $writer = PHPExcel_IOFactory::createWriter($book, "Excel2007");
                                                        $writer->save($filepath . $excelname);
                                                        $book = null;
                                                        $ext = '.xlsx';
                                                    } else if (EXCELVERSION == 'XLSX' && $ext === '') {
                                                        $excelname = $filename . '.xlsx';
                                                        $ext = '.xlsx';
                                                        $writer->close();
                                                    }
                                                }
                                                makeCopyFile($filepath, $excelname, $mailAddLst, $nowYmd, $nowTime, $ext);
                                            }
                                        } else {
                                            //エクセルテンプレートの場合通る
                                            $csvname = $filename . '.csv';
                                            $cExt = getExt($d1name);
                                            $filepath = BASE_DIR . '/php/xls/';
                                            //  $filepath = '/QNTC/192.168.1.12/TEST/';
                                            $templateFn = $paramQGData['D1NAME'] . '.' . $cExt;
                                            $tofilename = $filename . '.' . $cExt;
                                            fclose($csv_temp);
                                            makeCopyFile($filepath, $csvname, $mailAddLst, $nowYmd, $nowTime, '.csv');
                                            foreach ($mailAddLst as $key => $value) {
                                                if (cmMer($value['WAQUNM']) !== '' && cmMer($value['WAQUNM']) !== null) {
                                                    $WAQUNM = replaceFilename(cmMer($value['WAQUNM']));
                                                    $tofilename = $WAQUNM . '_' . $nowYmd . '_' . $nowTime . '.' . $cExt;
                                                    //e_log('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                                    shell_exec('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                                } else {
                                                    $tofilename = $paramQGData['D1NAME'] . '_' . $nowYmd . '_' . $nowTime . '.' . $cExt;
                                                    //e_log('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                                    shell_exec('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                                }
                                                //e_log('カスタム:' . ETEMP_DIR . $templateFn . 'go' . $templatecfn);
                                                
                                            }
                                        }
                                    }
                                    if ($htmlFlg === '1') {
                                        $filepath = BASE_DIR . '/php/html/';
                                        $excelname = $filename . '.html';
                                        fwrite($html_fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                                        fclose($html_fp);
                                        makeCopyFile($filepath, $excelname, $mailAddLst, $nowYmd, $nowTime, '.html');
                                    }
                                }
                            } else {
                                //ピボットの場合
                                //if ($csvFlg['result'] === true && $xlsFlg['result'] === true && $htmlFlg['result'] === true && $mailHtmlFlg['result'] === true) {
                                $pivotFlg = array();
                                $pivotFlg['XLSFLG'] = $xlsFlg === '1' ? true : false;
                                $pivotFlg['HTMLFLG'] = $htmlFlg === '1' ? true : false;
                                $pivotFlg['MAILHTMLFLG'] = $mailHtmlFlg === '1' ? true : false;
                                if ($pivotFlg['XLSFLG'] === true || $pivotFlg['HTMLFLG'] === true || $pivotFlg['MAILHTMLFLG'] === true) {
                                    if ($allcount === 0) {
                                        $tbdata = _fputpivot_ZeroKen($db2con, $paramQGData['D1NAME'], $filename, $wspkey, $dbname, $burstItmInfo, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $pivotFlg);
                                    } else {
                                        $pivotRtn = exePivotExcel($db2con, $paramQGData['D1NAME'], $filename, $wspkey, $dbname, $burstItmInfo, true, 'htmlcreate', $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $cnddatArr, $DB2EINSROWMAX, $pivotFlg, '', '');
                                        if ($pivotRtn['RTN'] === 0) {
                                            $tbdata = $pivotRtn['PIVOTHTML'];
                                            if ($pivotRtn['EXEMAXLEN'] === 'FAIL_EXEMAXLEN') {
                                                $ISEXCELMAXLEN = 1;
                                                $mailAddLst[0]['WAXLS'] = '9';
                                                $errMsg = showMsg('FAIL_EXEMAXLEN', array('xls', '65536'));
                                                fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $hiswspkey, $errMsg, $mailAddLst, $isQGflg);
                                            }
                                        } else {
                                            $rtn = 1;
                                        }
                                    }
                                }
                                //}
                                
                            }
                            if ($rtn !== 1) {
                                //パスワード生成
                                if ($csvFlg === '1' || $xlsFlg === '1' || $htmlFlg === '1') {
                                    $password = MakeRandStr(16);
                                }
                                //ファイルをZip化
                                exeZip($paramQGData['D1NAME'], $filename, $password, $csvFlg, $xlsFlg, $htmlFlg, $mailAddLst, $nowYmd, $nowTime, $wspkey, $xlsCustomFlg, $ISEXCELMAXLEN);
                            }
                            //メール送信
                            if ($rtn !== 1) {
                                $templateFlg = chkXlsTemplate($paramQGData['D1NAME'], $fdb2csv1['D1TMPF']);
                                exeMail($db2con, $d1name, $mailAddLst, $filename, $dbname, $nowYmd, $nowTime, $password, $csvFlg, $xlsFlg, $htmlFlg, $tbdata, $wspkey, $mailFlg, $xlsCustomFlg, $templateFlg, $paramQGData);
                            }
                        }
                    }
                }
                // ワークテーブルのログ情報削除
                //e_log('【削除】利用しているワークテーブルリスト：' . print_r($wrkTblLst, true));
                foreach ($wrkTblLst as $wrkTbl) {
                    // ワークテーブルのログ情報削除
                    //e_log('【削除】使用したワークテーブル①：'.print_r($wrkTblLst,true));
                    $resDel = cmDelWTBL($db2con, $wrkTbl);
                    if ($resDel !== 0) {
                        //修正用
                        $msg = showMsg('ログ削除失敗'); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
                        $rtn = 1;
                    }
                }
            }
        } else {
            if ($rtn !== 1) {
                //e_log('【削除】実行種類③①:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                $wspkey = cmMer($wspkey);
                // csv,xls,htmlのため取得データカウントとメール送信フラグ取得
                $rownum = '';
                $csv_header = $csv_h;
                $h_fdb2csv2 = $fdb2csv2;
                //e_log('【削除】FDB2CSV2取得ヘダー①：'.'csv_hcount:'.count($csv_h).'fdb2csv2count'.count($fdb2csv2).print_r($h_fdb2csv2,true));
                //e_log('【削除】カウント取得①'.$fdb2csv1['RDBNM'].print_r($fdb2csv1,true));
                if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === RDB || $fdb2csv1['RDBNM'] === RDBNAME) {
                    $csv_db_count = cmGetAllCountBurstItm($db2con, $burstItmInfo, $dbname, $fdb2csv1['TMP_D1CFLG']);
                } else {
                    $csv_db_count = cmGetAllCountBurstItm($db2LibCon, $burstItmInfo, $dbname, $fdb2csv1['TMP_D1CFLG']);
                }
                $allcount = $csv_db_count[0];
                // ゼロー件の場合メール送信かのチェック
                $mailFlg = '1';
                if ($allcount == 0 && $fdb2csv1['ZEROMAIL'] == 1) {
                    $mailFlg = '0'; // 0件データーメール配信することができません。
                    
                }
            }
            //e_log("添付ファイルの種別のフラグ取得");
            // 添付ファイルの種別のフラグ取得
            if ($rtn !== 1) {
                //e_log('【削除】実行種類③②:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                $csvFlg = '';
                $xlsFlg = '';
                $htmlFlg = '';
                $mailHtmlFlg = '';
                $wspkey = cmMer($wspkey);
                $rsDb2wautFil = getDB2WAUTFilFlg($db2con, $d1name, $wspkey);
                if ($rsDb2wautFil['result'] === true) {
                    $csvFlg = $rsDb2wautFil['data']['WACSV'];
                    $xlsFlg = $rsDb2wautFil['data']['WAXLS'];
                    $htmlFlg = $rsDb2wautFil['data']['WAHTML'];
                    $mailHtmlFlg = $rsDb2wautFil['data']['WATYPE'];
                }
            }
            if ($rtn !== 1) {
                //e_log('【削除】実行種類③③:' . ($wspkey === '' ? 'クエリー実行' : 'ピボット実行'));
                //e_log("exemailsend cmCalendarDataParamWeb excel ".print_r($cnddatArr,true));
                $DB2EINSROWMAX = 0;
                $xlsCustomFlg = '';
                if ($mailFlg !== '0') {
                    e_log("mailFlg rtn " . $mailFlg);
                    $DB2ECON = array();
                    $DB2EINS = array();
                    $CNDSDATAF = array();
                    // Excelテンプレートの文字挿入設定の場合
                    if ($fdb2csv1['D1EDKB'] === '2') {
                        $DB2ECON = cmGetDB2ECON($db2con, $d1name);
                        if ($DB2ECON['result'] === true) {
                            $DB2ECON = umEx($DB2ECON['data']);
                        }
                        $DB2EINS = cmGetDB2EINS($db2con, $d1name);
                        if ($DB2EINS['result'] === true) {
                            $EIROWMAX = $DB2EINS['MAXEIROW'];
                            $DB2EINS = umEx($DB2EINS['data']);
                        }
                        e_log("DB2EINS original " . print_r($DB2EINS, true));
                        $CNDSDATAF = cmCNDSDATA($fdb2csv1['D1WEBF'], $db2con, $d1name, array(), $fdb2csv1['D1CFLG']);
                        $DB2EINSROWMAX = cmGetCountSearchData($DB2ECON, $EIROWMAX, $CNDSDATAF); //EXCEL SETTING 挿入文字カウント
                        
                    } else if ($fdb2csv1['D1EDKB'] !== '2' && $fdb2csv1['D1TMPF'] !== '' && cmMer($wspkey) === '') {
                        // xlsの印字開始行番号取得
                        $DB2EINSROWMAX = $fdb2csv1['D1TROS'];
                    }
                    // ベダーのためデータ開始行の番号を1足す
                    if ($fdb2csv1['D1EDKB'] === '2' || cmMer($wspkey) !== '' || $fdb2csv1['D1TMPF'] === '') {
                        $DB2EINSROWMAX+= 1;
                    }
                    // 制御取得の場合列に二つの列追加
                    if ($fdb2csv1['SEIGYOFLG'] == 1) {
                        $csv_header = $csv_h;
                        array_unshift($csv_header, "", "");
                        $h_fdb2csv2 = $fdb2csv2;
                        $h_fdb2csv2 = cmArrayUnshift($h_fdb2csv2);
                    }
                    $csv_d = array();
                    //取得データでCSVファイル作成
                    if ($csvFlg === '1') {
                        //e_log('【削除】CSVファイル作成開始　ヘーダ印字');
                        require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
                        $csv_fp = exeCsvstream($filename);
                        exeCsv($csv_fp, $csv_header, $csv_d, $h_fdb2csv2, $d1name, $filename, $mailAddLst, $nowYmd, $nowTime, true);
                        //e_log('【削除】CSVファイル作成終了　ヘーダ印字');
                        
                    }
                    $EXCELVER = false; //エクセルバージョン管理
                    //取得したデータでXLSファイル作成
                    if ($xlsFlg === '1') {
                        //e_log('【削除】XLSファイル作成開始　ヘーダ印字 PIVOTKEY:'.$wspkey);
                        //e_log('【削除】実行種類④:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                        if (cmMer($wspkey) !== '') {
                            //e_log('【削除】XLSフラグチェックPIVOT');
                            // ピボットのxlsファイル作成
                            $ISEXCELMAXLEN = 0;
                        } else {
                            // クエリのxlsファイル作成
                            $resXlsCustom = '';
                            if ($fdb2csv1['D1EDKB'] === '1') {
                                $xlsCustomFlg = $fdb2csv1['D1EDKB'];
                            }
                            // XLSテンプレートがある場合ファイルあるかをチェックする
                            if (chkXlsTemplate($d1name, $fdb2csv1['D1TMPF'])) {
                                // かステムテンプレートじゃないの場合
                                if ($xlsCustomFlg === '') {
                                    e_log('【削除】XLSフラグチェックReachCustom無し');
                                    $sheet = '';
                                    $book = '';
                                    $excel_fp = '';
                                    $filepath = BASE_DIR . '/php/xls/';
                                    //  $filepath = '/QNTC/192.168.1.12/TEST/';
                                    $D1TMPF = '';
                                    $ext = getExt($d1name);
                                    //MAX件数のためカウント
                                    if ($ext !== '') {
                                        $EXCELMAXLEN = ($ext === 'xlsm' || $ext === 'xlsx') ? 1048576 : (($ext === 'xls') ? 65536 : 0);
                                        $EMAXLEN_FILE = $ext;
                                    } else {
                                        if (EXCELVERSION === '2007' || EXCELVERSION === 'XLSX') {
                                            $EXCELMAXLEN = 1048576;
                                            $EMAXLEN_FILE = 'xlsx';
                                        } else if (EXCELVERSION === '2003' || EXCELVERSION === 'html') {
                                            $EXCELMAXLEN = 65536;
                                            $EMAXLEN_FILE = 'xls';
                                        }
                                    }
                                    //if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                    if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                        $DB2EINSROWMAX+= ($allcount === 0 || $allcount === '' || cmMer($fdb1csv1['RSMASTERARR'][0]['DCSUMF']) === '1') ? 0 : $allcount;
                                        //e_log('【削除】制御カウント取得①：'.print_r($fdb2csv2,true).print_r($fdb2csv1['RSMASTERARR'],true));
                                        $DB2EINSROWMAX+= cmGetALLCOUNTSEIKYO($db2con, $d1name, $dbname, $fdb2csv1['RSMASTERARR'], $fdb2csv2, '', $burstItmInfo);
                                    } else {
                                        //e_log('【削除】XLSシート取得カウント制御じゃない：'.$allcount);
                                        $DB2EINSROWMAX+= ($allcount === 0 || $allcount === '') ? 0 : $allcount;
                                    }
                                    //e_log('in exeMailSend.php $DB2EINSROWMAX=>'.$DB2EINSROWMAX.'$EXCELMAXLEN=>'.$EXCELMAXLEN);
                                    if ($DB2EINSROWMAX > $EXCELMAXLEN) {
                                        // 取得結果データ件数はEXCELの最大行数を超えている場合
                                        $ISEXCELMAXLEN = 1;
                                        $errMsg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                                        $mailAddLst[0]['WAXLS'] = '9';
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, false);
                                    } else {
                                        //【削除】取得結果データ件数はEXCELの最大行数を超えていない場合、XLSファイル作成
                                        $ISEXCELMAXLEN = 0;
                                        if (EXCELVERSION == 'XLSX' && $ext === '') {
                                            $EXCELVER = true;
                                            $filepath = BASE_DIR . '/php/xls/';
                                            $excelname = $filename . '.xlsx';
                                            $xlsxPath = $filepath . $excelname;
                                            $writer = WriterFactory::create(Type::XLSX);
                                            $writer->openToFile($xlsxPath);
                                            spoutMojiSounyuu($writer, $DB2EINS, $DB2ECON, $CNDSDATAF); //SPOUT EXCELの見出しの上に文字挿入と抽出条件の表示設定
                                            $writer->addRow($csv_header);
                                        } else {
                                            if (EXCELVERSION == '2003' || EXCELVERSION == '2007' || $ext !== '') {
                                                if ($ext !== '') {
                                                } else if (EXCELVERSION == '2003') {
                                                    $ext = 'xls';
                                                } else if (EXCELVERSION == '2007') {
                                                    $ext = 'xlsx';
                                                }
                                                $excelname = $filename . '.' . $ext;
                                                $sheet = php_excelCreateHeading($d1name);
                                                $DB2WCOL = array();
                                                $book = $sheet['BOOK'];
                                                $sheet = $sheet['SHEET'];
                                            } else if (EXCELVERSION == 'html' || $ext === '') {
                                                $excelname = $filename . '.xls';
                                                if (!file_exists($filepath . $excelname)) {
                                                    touch($filepath . $excelname);
                                                }
                                                $excel_fp = fopen(BASE_DIR . '/php/xls/' . $excelname, 'w');
                                                excelCreateHeading($excel_fp, $DB2ECON, $DB2EINS, $CNDSDATAF);
                                            }
                                            $rownum = exeExcel($excel_fp, $csv_header, $csv_d, $h_fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $sheet, $rownum, '0', $cnddatArr);
                                        }
                                    }
                                } else {
                                    require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
                                    $csv_temp = exeCsvstream($filename, $xlsCustomFlg);
                                    exeCsv($csv_temp, $csv_header, $csv_d, $h_fdb2csv2, $d1name, $filename, $mailAddLst, $nowYmd, $nowTime, true, $xlsCustomFlg);
                                }
                            } else {
                                if ($templateErrCnt === 0) {
                                    $errMsg = showMsg('NOTEXIST_TMP', array($d1name));
                                    fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg, array(), false);
                                }
                            }
                        }
                        //e_log('【削除】XLSファイル作成終了　ヘーダ印字');
                        
                    }
                    //取得データでHTMLファイル作成
                    if ($htmlFlg === '1') {
                        $filepath = BASE_DIR . '/php/html/';
                        $htmlname = $filename . '.html';
                        if (!file_exists($filepath . $htmlname)) {
                            touch($filepath . $htmlname);
                        }
                        $html_d = $csv_d;
                        $html_fp = fopen($filepath . $htmlname, 'w');
                        excelCreateHeading($html_fp, $DB2ECON, $DB2EINS, $CNDSDATAF);
                        exeHtml($html_fp, $csv_header, $html_d, $h_fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $cnddatArr);
                    }
                    if ($rtn !== 1) {
                        //e_log('【削除】取得データ印字開始');
                        $lenArr = cmTotalLen($allcount);
                        $lenCount = count($lenArr);
                        $rtnMail = 0;
                        // htmlフォマットメール送信用
                        $breakFlg = false;
                        $qryHtmlCount = 0;
                        $htmltabledata = array();
                        $tbdata = '';
                        //e_log('【削除】実行種類⑤:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                        if (cmMer($wspkey) === '') {
                            //e_log('【削除】クエリーループしてデータ取得：'.$allcount.'length'.$lenCount );
                            for ($idx = 0;$idx < $lenCount;$idx++) {
                                //e_log('【削除】ループしてデータ取得：'.print_r($burstItmInfo,true) );
                                if ($rtnMail !== 1) { //ロープしながら、エラーが出たら、rtnMailは１になって続かない
                                    if ($fdb2csv1['SEIGYOFLG'] != 1) {
                                        //e_log('【削除】制御じゃないのデータ取得');
                                        if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === RDB || $fdb2csv1['RDBNM'] === RDBNAME) {
                                            //e_log('【削除】ループしてデータ取得①：'.$allcount );
                                            //e_log('【削除】バーストデータ①：'.$dbname );
                                            $csv_d = cmGetDbFileBurstItm($db2con, $fdb2csv2, $burstItmInfo, $dbname, $fdb2csv1['TMP_D1CFLG'], $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', '', array(), true);
                                        } else {
                                            //e_log('【削除】ループしてデータ取得②：'.$allcount );
                                            //e_log('【削除】バーストデータ②：'.$dbname );
                                            $csv_d = cmGetDbFileBurstItm($db2LibCon, $fdb2csv2, $burstItmInfo, $dbname, $fdb2csv1['TMP_D1CFLG'], $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', '', array(), true);
                                        }
                                    } else {
                                        //e_log('【削除】制御のデータ取得');
                                        if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === RDB || $fdb2csv1['RDBNM'] === RDBNAME) {
                                            $tabledata = fnDataShukei($db2con, $fdb2csv2, $fdb2csv2_ALL, $d1name, $schDBname, $db2wcol, $fdb2csv1['RSMASTERARR'], $fdb2csv1['RSSHUKEIARR'], $burstItmInfo, $lenArr[$idx][0], $lenArr[$idx][1], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $allcount);
                                        } else {
                                            $tabledata = fnDataShukei($db2LibCon, $fdb2csv2, $fdb2csv2_ALL, $d1name, $schDBname, $db2wcol, $fdb2csv1['RSMASTERARR'], $fdb2csv1['RSSHUKEIARR'], $burstItmInfo, $lenArr[$idx][0], $lenArr[$idx][1], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $allcount);
                                        }
                                        $csv_h_shukei = $csv_h;
                                        array_unshift($csv_h_shukei, "", "");
                                        $fdb2csv2_shukei = $fdb2csv2;
                                        $fdb2csv2_shukei = cmArrayUnshift($fdb2csv2_shukei);
                                    }
                                    //e_log('【削除】CSVFLG：'.$csvFlg . ' XLSFLG:'.$xlsFlg. ' HTMLFLG:'.$htmlFlg. ' MAILHTMLFLG:'.$mailHtmlFlg);
                                    if ($csvFlg === '1') {
                                        if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                            //e_log('【削除】CSV配信制御データ④'.count($tabledata));
                                            exeCsv($csv_fp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, cmMer($d1name), $filename, $mailAddLst, $nowYmd, $nowTime, false);
                                        } else {
                                            //e_log('【削除】CSV配信データ③'.count($csv_d));
                                            exeCsv($csv_fp, $csv_h, $csv_d, $fdb2csv2, cmMer($d1name), $filename, $mailAddLst, $nowYmd, $nowTime, false);
                                        }
                                    }
                                    if ($xlsFlg === '1') {
                                        if (chkXlsTemplate($d1name, $fdb2csv1['D1TMPF'])) {
                                            if ($xlsCustomFlg === '') {
                                                if ($ISEXCELMAXLEN === 0) { //MAX件数のためカウントのチェック
                                                    if (EXCELVERSION == 'XLSX' && $EXCELVER === true) {
                                                        if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                                            $c_data = array_chunk($tabledata, 100);
                                                        } else {
                                                            $c_data = array_chunk($csv_d, 100);
                                                        }
                                                        foreach ($c_data as $ck => $cv) {
                                                            $writer->addRows($cv);
                                                        }
                                                    } else {
                                                        if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                                            //e_log('【削除】XLS制御配信データ④'.count($tabledata));
                                                            $rownum = exeExcel($excel_fp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename, $wspkey, $d1name, $db2con, $schDBname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $sheet, $rownum, '1', $cnddatArr);
                                                        } else {
                                                            //e_log('【削除】XLS配信データ⑤'.count($csv_d));
                                                            $rownum = exeExcel($excel_fp, $csv_h, $csv_d, $fdb2csv2, $filename, $wspkey, $d1name, $db2con, $schDBname, $burstItmInfo, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $sheet, $rownum, '1', $cnddatArr);
                                                        }
                                                    }
                                                }
                                            } else {
                                                if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                                    //e_log('【削除】XLSテンプレート制御CSV配信データ④'.count($tabledata));
                                                    exeCsv($csv_temp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, cmMer($d1name), $filename, $mailAddLst, $nowYmd, $nowTime, false, $xlsCustomFlg);
                                                } else {
                                                    //e_log('【削除】XLSテンプレートCSV配信データ⑤'.count($csv_d));
                                                    exeCsv($csv_temp, $csv_h, $csv_d, $fdb2csv2, cmMer($d1name), $filename, $mailAddLst, $nowYmd, $nowTime, false, $xlsCustomFlg);
                                                }
                                            }
                                        } else {
                                            if ($templateErrCnt === 0) {
                                                $errMsg = showMsg('NOTEXIST_TMP', array($d1name));
                                                fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg, array(), $isQGflg);
                                            }
                                        }
                                    }
                                    //取得データでHTMLファイル作成
                                    if ($htmlFlg === '1') {
                                        if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                            //e_log('【削除】HTML制御データ①'.count($tabledata));
                                            exeHtml($html_fp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename, $wspkey, $d1name, $db2con, $schDBname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $cnddatArr);
                                        } else {
                                            //e_log('【削除】HTMLデータ①'.count($csv_d));
                                            exeHtml($html_fp, $csv_h, $csv_d, $fdb2csv2, $filename, $wspkey, $d1name, $db2con, $schDBname, $burstItmInfo, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $cnddatArr);
                                        }
                                    }
                                    // htmlフォマットでのメール送信
                                    if ($mailHtmlFlg === '1') {
                                        $lastRow = false;
                                        if ($idx === $lenCount - 1) {
                                            $lastRow = true;
                                        }
                                        if ($breakFlg !== true) {
                                            if ($fdb2csv1['SEIGYOFLG'] == 1) {
                                                $rtnTbdata = exeHtmlMail($csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename, $fdb2csv1['D1WEBF'], $idx, $lastRow, $qryHtmlCount);
                                                $qryHtmlCount = $rtnTbdata['QRYHTMLCOUNT'];
                                                $breakFlg = $rtnTbdata['HTMLCREATEBREAK'];
                                                $tbdata.= $rtnTbdata['TBDATA'];
                                            } else {
                                                $rtnTbdata = exeHtmlMail($csv_h, $csv_d, $fdb2csv2, $filename, $fdb2csv1['D1WEBF'], $idx, $lastRow, $qryHtmlCount);
                                                $qryHtmlCount = $rtnTbdata['QRYHTMLCOUNT'];
                                                $breakFlg = $rtnTbdata['HTMLCREATEBREAK'];
                                                $tbdata.= $rtnTbdata['TBDATA'];
                                            }
                                        }
                                        if ($breakFlg === true) {
                                            if ($xlsFlg !== '1' && $csvFlg !== '1' && $htmlFlg !== '1') {
                                                break; // break from querycount loop
                                                
                                            }
                                        }
                                    }
                                }
                                unset($csv_d);
                                unset($tabledata);
                            }
                            //e_log('【削除】実行種類⑥:'.($wspkey === '' ? 'クエリー実行':'ピボット実行'));
                            if (cmMer($wspkey) === '') {
                                if ($csvFlg === '1') {
                                    fclose($csv_fp); //csv file close
                                    $csvname = $filename . '.csv';
                                    $filepath = BASE_DIR . '/php/csv/';
                                    makeCopyFile($filepath, $csvname, $mailAddLst, $nowYmd, $nowTime, '.csv');
                                }
                                if ($xlsFlg === '1') {
                                    // かステムテンプレートじゃないの場合
                                    if ($xlsCustomFlg === '') {
                                        if ($ISEXCELMAXLEN === 0) { //MAX件数のためカウントのチェック
                                            $filepath = BASE_DIR . '/php/xls/';
                                            //  $filepath = '/QNTC/192.168.1.12/TEST/';
                                            if (EXCELVERSION == 'html' && $ext === '') {
                                                $excelname = $filename . '.xls';
                                                fwrite($excel_fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                                                fclose($excel_fp);
                                                $ext = '.xls';
                                            } else {
                                                if ((EXCELVERSION == '2003' && $ext === '') || $ext === 'xls') {
                                                    $excelname = $filename . '.xls';
                                                    $writer = PHPExcel_IOFactory::createWriter($book, "Excel5");
                                                    $writer->save($filepath . $excelname);
                                                    $book = null;
                                                    $ext = '.xls';
                                                } else if ((EXCELVERSION == '2007' && $ext === '') || $ext === 'xlsx') {
                                                    $excelname = $filename . '.xlsx';
                                                    $writer = PHPExcel_IOFactory::createWriter($book, "Excel2007");
                                                    $writer->save($filepath . $excelname);
                                                    $book = null;
                                                    $ext = '.xlsx';
                                                } else if (EXCELVERSION == 'XLSX' && $ext === '') {
                                                    $excelname = $filename . '.xlsx';
                                                    $ext = '.xlsx';
                                                    e_log("gonna finish." . date("Ymd His"));
                                                    $writer->close();
                                                    e_log("finish." . date("Ymd His"));
                                                }
                                            }
                                            makeCopyFile($filepath, $excelname, $mailAddLst, $nowYmd, $nowTime, $ext);
                                        }
                                    } else {
                                        //エクセルテンプレートの場合通る
                                        $csvname = $filename . '.csv';
                                        $cExt = getExt($d1name);
                                        $filepath = BASE_DIR . '/php/xls/';
                                        //  $filepath = '/QNTC/192.168.1.12/TEST/';
                                        $templateFn = $d1name . '.' . $cExt;
                                        $tofilename = $filename . '.' . $cExt;
                                        fclose($csv_temp);
                                        makeCopyFile($filepath, $csvname, $mailAddLst, $nowYmd, $nowTime, '.csv');
                                        foreach ($mailAddLst as $key => $value) {
                                            if (cmMer($value['WAQUNM']) !== '' && cmMer($value['WAQUNM']) !== null) {
                                                $WAQUNM = replaceFilename(cmMer($value['WAQUNM']));
                                                $tofilename = $WAQUNM . '_' . $nowYmd . '_' . $nowTime . '.' . $cExt;
                                                //e_log('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                                shell_exec('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                            } else {
                                                $tofilename = $d1name . '_' . $nowYmd . '_' . $nowTime . '.' . $cExt;
                                                //e_log('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                                shell_exec('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                            }
                                            //e_log('カスタム:' . ETEMP_DIR . $templateFn . 'go' . $templatecfn);
                                            
                                        }
                                    }
                                }
                                if ($htmlFlg === '1') {
                                    $filepath = BASE_DIR . '/php/html/';
                                    $excelname = $filename . '.html';
                                    fwrite($html_fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                                    fclose($html_fp);
                                    makeCopyFile($filepath, $excelname, $mailAddLst, $nowYmd, $nowTime, '.html');
                                }
                            }
                        } else {
                            //ピボットの場合
                            //if ($csvFlg['result'] === true && $xlsFlg['result'] === true && $htmlFlg['result'] === true && $mailHtmlFlg['result'] === true) {
                            $pivotFlg = array();
                            $pivotFlg['XLSFLG'] = $xlsFlg === '1' ? true : false;
                            $pivotFlg['HTMLFLG'] = $htmlFlg === '1' ? true : false;
                            $pivotFlg['MAILHTMLFLG'] = $mailHtmlFlg === '1' ? true : false;
                            if ($pivotFlg['XLSFLG'] === true || $pivotFlg['HTMLFLG'] === true || $pivotFlg['MAILHTMLFLG'] === true) {
                                if ($allcount === 0) {
                                    $tbdata = _fputpivot_ZeroKen($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmInfo, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $pivotFlg);
                                } else {
                                    $pivotRtn = exePivotExcel($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmInfo, true, 'htmlcreate', $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $cnddatArr, $DB2EINSROWMAX, $pivotFlg, '', '');
                                    if ($pivotRtn['RTN'] === 0) {
                                        $tbdata = $pivotRtn['PIVOTHTML'];
                                        if ($pivotRtn['EXEMAXLEN'] === 'FAIL_EXEMAXLEN') {
                                            $ISEXCELMAXLEN = 1;
                                            $mailAddLst[0]['WAXLS'] = '9';
                                            $errMsg = showMsg('FAIL_EXEMAXLEN', array('xls', '65536'));
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst, $isQGflg);
                                        }
                                    } else {
                                        $rtn = 1;
                                    }
                                }
                            }
                            //}
                            
                        }
                        if ($rtn !== 1) {
                            //パスワード生成
                            if ($csvFlg === '1' || $xlsFlg === '1' || $htmlFlg === '1') {
                                $password = MakeRandStr(16);
                            }
                            //ファイルをZip化
                            exeZip($d1name, $filename, $password, $csvFlg, $xlsFlg, $htmlFlg, $mailAddLst, $nowYmd, $nowTime, $wspkey, $xlsCustomFlg, $ISEXCELMAXLEN);
                        }
                        //スケジュールの出力ファイル
                        /*if($rtn !==1){
                            if($fdb2csv1['D1OUTSE']==='1'){
                                if($fdb2csv1['D1OUTLIB'] !=='' && $fdb2csv1['D1OUTFIL'] !==''){
                                    $isTmp=true;
                                    //スケジュールのファイル出力の上書きがない場合、SYSからライブラリーとファイルチェック
                                    if ($fdb2csv1['D1OUTSA'] !== '1') {
                                        if($fdb2csv1['D1OUTSR'] ==='0' || $fdb2csv1['D1OUTSR'] ==='' ){
                                            if($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === RDB || $fdb2csv1['RDBNM'] === RDBNAME){
                                                $r=fnChkExistFileOutput($db2con,$fdb2csv1['D1OUTLIB'],$fdb2csv1['D1OUTFIL']);
                                            }else{
                                                $r=fnChkExistFileOutput($db2LibCon,$fdb2csv1['D1OUTLIB'],$fdb2csv1['D1OUTFIL']);
                                            }
                                            if($r['result']!==true){
                                                $rtn=($r['rtn']===1)?1:$rtn;
                                                $isTmp=false;
                                                $eMsg=($r['rtn']===4)?showMsg('FAIL_OUT',array($fdb2csv1['D1OUTLIB'],$fdb2csv1['D1OUTFIL'])):
                                                        showMsg($r['result']);
                                                fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $eMsg, $mailAddLst,$isQGflg);
                                            }
                                        }
                                    }
                                    //出力ファイル作成
                                    if($isTmp){
                                        $add_flg = ($fdb2csv1['D1OUTSA'] === '1')?true:false;
                                        if($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === RDB || $fdb2csv1['RDBNM'] === RDBNAME){
                                            $r=createOutputFileTable($db2con,$dbname,$fdb2csv1['D1OUTLIB'],$fdb2csv1['D1OUTFIL'],$add_flg);
                                        }else{
                                            $r=createOutputFileTable($db2LibCon,$dbname,$fdb2csv1['D1OUTLIB'],$fdb2csv1['D1OUTFIL'],$add_flg);
                                        }
                                        if ($r['RTN'] !== 0) {
                                            //$rtn = 1;//Cause whethere true or false do mail send
                                            $msg = showMsg($r['MSG']);
                                        }else{
                                            $eMsg = showMsg($r['MSG']);
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $eMsg, $mailAddLst,$isQGflg);
                                        }
                                    }
                                }else{
                                    $eMsg = showMsg('FAIL_EXISTLIB');
                                    fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $eMsg, $mailAddLst,$isQGflg);
                                }
                            }
                        }*/
                        //メール送信
                        if ($rtn !== 1) {
                            $templateFlg = chkXlsTemplate($d1name, $fdb2csv1['D1TMPF']);
                            exeMail($db2con, $d1name, $mailAddLst, $filename, $dbname, $nowYmd, $nowTime, $password, $csvFlg, $xlsFlg, $htmlFlg, $tbdata, $wspkey, $mailFlg, $xlsCustomFlg, $templateFlg, array());
                        }
                    }
                }
            }
            // ワークテーブルのログ情報削除
            e_log('【削除】利用しているワークテーブルリスト：' . print_r($wrkTblLst, true));
            foreach ($wrkTblLst as $wrkTbl) {
                // ワークテーブルのログ情報削除
                //e_log('【削除】使用したワークテーブル①：'.print_r($wrkTblLst,true));
                $resDel = cmDelWTBL($db2con, $wrkTbl);
                if ($resDel !== 0) {
                    //修正用
                    $msg = showMsg('ログ削除失敗'); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
                    $rtn = 1;
                }
            }
        }
    }
    e_log('PARAMQGDATA:' . print_r($paramQGData, true));
    if ($isQGflg) {
        if ($paramQGData['LASTFLG'] === '1') {
            e_log('CLOSE $db2LibCon');
            cmDb2Close($db2LibCon);
        }
    } else {
        if (($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === RDB || $fdb2csv1['RDBNM'] === RDBNAME) === false) {
            e_log('CLOSE $db2LibCon');
            cmDb2Close($db2LibCon);
        }
    }
    if ($rtn !== 1) {
        $rtnArr = array('result' => true, 'WRKTBLNAME' => $schDBname, 'QRYDATA' => $qryData);
    } else {
        $rtnArr = array('result' => false, 'WRKTBLNAME' => '', 'QRYDATA' => '');
    }
    return $rtnArr;
}
function getDBGPKQRY($db2con, $GPKQRY, $nowDate, $nowTime) {
    $data = array();
    $rtn = array();
    $qryData = array();
    $strSQL = '';
    $strSQL.= ' SELECT * FROM ';
    $strSQL.= ' DB2GPK ';
    $strSQL.= ' WHERE GPKQRY = ? ';
    $strSQL.= ' AND GPKBFD = ? ';
    $strSQL.= ' AND GPKBFT = ? ';
    $params = array($GPKQRY, $nowDate, $nowTime);
    //e_log($strSQL.print_R($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                $data = umEx($data, true);
            }
        }
        $rtn = array('result' => true, 'data' => $data);
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * burstitemリスト取得
 *ミィッミィッモー
 *-------------------------------------------------------*
*/
function getBurstItmLst($db2con, $d1name, $wspkey) {
    $rtn = array();
    $data = array();
    $strSQL = ' SELECT ';
    $strSQL.= '    WANAME, ';
    $strSQL.= '    WAPKEY, ';
    $strSQL.= '    WABAFID, ';
    $strSQL.= '    WABAFLD, ';
    $strSQL.= '    WABAFR, ';
    $strSQL.= '    WABATO, ';
    $strSQL.= '    WABFLG, ';
    $strSQL.= '    WABINC, ';
    $strSQL.= '    WLSEQL, ';
    $strSQL.= '    WLDATA, ';
    $strSQL.= '    WAQGFLG, ';
    $strSQL.= '    WAPIV, ';//MSM add for mail sent link 20180906
    $strSQL.= '    WANGFLG, ';//MSM add for mail sent link 20180911
    $strSQL.= '    WAGPID ';//MSM add for mail sent link 20180911
    $strSQL.= ' FROM DB2WAUT AS A ';
    $strSQL.= ' LEFT JOIN DB2WAUL AS B ';
    $strSQL.= ' ON A.WANAME = B.WLNAME AND ';
    $strSQL.= '    A.WAPKEY = B.WLPKEY AND ';
    $strSQL.= '    A.WAMAIL = B.WLMAIL ';
    $strSQL.= ' WHERE WANAME = ? ';
    $strSQL.= ' AND WASPND <> \'1\' ';
    $strSQL.= ' AND WAPKEY = ?  ';
    $strSQL.= ' GROUP BY WANAME,WAPKEY,WABAFID,WABAFLD,WABAFR,WABATO,WABFLG,WABINC,WLNAME,WLSEQL,WLDATA,WAQGFLG,WAPIV,WANGFLG,WAGPID ';//MSM add WAPIV,WANGFLG,WAGPID
    $strSQL.= ' ORDER BY WABAFLD ';
    $params = array($d1name, $wspkey);
    //e_log('KSK exeMailSend log 3⇒'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = umEx($data);
            //e_log('【削除】バーストアイテム取得データ_パラメータ：'.print_r($params,true));
            //e_log('【削除】バーストアイテム取得データ_SQL：'.$strSQL,true);
            //e_log('【削除】バーストアイテム取得データ_結果:'.print_r($data,true));
            $burstItmLstTmp = array();
            $WANAME = '';
            $WAPKEY = '';
            $WABAFID = '';
            $WABAFLD = '';
            $WABFLG = '';
            $WABAFR = '';
            $WABATO = '';
            $WABINC = 0;
            $IDX = 0;
            foreach ($data as $key => $value) {
                if ($value['WABFLG'] !== '1') {
                    // 抽出タイプはRangeに場合
                    $index = $IDX++;
                    $burstItmLstTmp[$key] = $value;
                } else {
                    //抽出タイプはリストの場合
                    if ($WABINC === $value['WABINC']) {
                        if (isset($burstItmLstTmp[$index]['WLSEQL'])) {
                            array_push($burstItmLstTmp[$index]['WLSEQL'], $value['WLSEQL']);
                        }
                        if (isset($burstItmLstTmp[$index]['WLDATA'])) {
                            array_push($burstItmLstTmp[$index]['WLDATA'], $value['WLDATA']);
                        }
                        $WABINC = $value['WABINC'];
                    } else {
                        $index = $IDX++;
                        $burstItmLstTmp[] = array('WANAME' => $value['WANAME'], 'WAPKEY' => $value['WAPKEY'], 'WAQGFLG' => $value['WAQGFLG'], 'WABAFID' => $value['WABAFID'], 'WABAFLD' => $value['WABAFLD'], 'WABAFR' => $value['WABAFR'], 'WABATO' => $value['WABATO'], 'WABFLG' => $value['WABFLG'], 'WABINC' => $value['WABINC'], 'WLSEQL' => array($value['WLSEQL']), 'WLDATA' => array($value['WLDATA']));
                        $WANAME = $value['WANAME'];
                        $WAPKEY = $value['WAPKEY'];
                        $WABAFID = $value['WABAFID'];
                        $WABAFLD = $value['WABAFLD'];
                        $WABFLG = $value['WABFLG'];
                        $WABAFR = $value['WABAFR'];
                        $WABINC = $value['WABINC'];
                    }
                }
            }
            $rtn = array('result' => true, 'data' => $burstItmLstTmp);
        }
    }
    //e_log('【削除】バーストアイテム取得データ_配列結果:'.print_r($burstItmLstTmp,true));
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * 宛先のリスト取得
 *ミィッミィッモー
 *-------------------------------------------------------*
*/
function getMailAddlst($db2con, $burstItmlst) {
    if ($burstItmlst !== '') {
        $waname = $burstItmlst['WANAME'];
        $wapkey = $burstItmlst['WAPKEY'];
        $wabafid = $burstItmlst['WABAFID'];
        $wabafld = $burstItmlst['WABAFLD'];
        $wabafr = $burstItmlst['WABAFR'];
        $wabato = $burstItmlst['WABATO'];
        $wabflg = $burstItmlst['WABFLG'];
        $wabinc = $burstItmlst['WABINC'];
        $wlseql = $burstItmlst['WLSEQL'];
        $wldata = $burstItmlst['WLDATA'];
    }
    $params = array($waname, $wapkey, $wabafid, $wabafld, $wabafr, $wabato);
    $rtn = array();
    $data = array();
    $strSQL = ' SELECT ';
    $strSQL.= getDb2wautColumns();
    $strSQL.= ' FROM DB2WAUT AS A ';
    $strSQL.= ' WHERE WANAME = ? ';
    $strSQL.= ' AND WASPND <> \'1\' ';
    $strSQL.= ' AND WAPKEY = ?  ';
    $strSQL.= ' AND WABAFID = ? ';
    $strSQL.= ' AND WABAFLD = ? ';
    $strSQL.= ' AND WABAFR = ? ';
    $strSQL.= ' AND WABATO = ? ';
    if ($wabflg === '0') {
        $strSQl.= ' AND WABFLG = \'0\' ';
        $strSQL.= 'AND  WABINC = ? ';
        array_push($params, $wabinc);
    } else if ($wabflg === '1') {
        $strSQL.= ' AND WABFLG = \'1\'  ';
        $strSQL.= ' AND WABINC = ? ';
        array_push($params, $wabinc);
    } else {
        $strSQL.= ' AND WABFLG = \'\' ';
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * ピボットの出力先ファイル情報取得
 *-------------------------------------------------------*
*/
function getDB2WSOC($db2con, $soname, $sopkey, $isQGflg = false) {
    $rtn = 0;
    $data = array();
    $params = array();
    $strSQL = ' SELECT * FROM DB2WSOC';
    $strSQL.= ' WHERE SONAME = ? ';
    $params = array($soname);
    if ($isQGflg) {
        $strSQL.= ' AND SOQGFLG=\'1\' ';
        if ($sopkey !== '') {
            $strSQL.= ' AND   SOPKEY = ?  ';
            array_push($params, $sopkey);
        }
    } else {
        $strSQL.= ' AND   SOPKEY = ?  ';
        $strSQL.= ' AND SOQGFLG<>\'1\' ';
        array_push($params, $sopkey);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rtn = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = umEx($data);
            if (count($data) > 0) {
                $data = $data[0];
            } else {
                $rtn = 'NODATA';
            }
        }
    }
    if ($rtn === 0) {
        $data = $data;
    } else {
        $data = '';
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * 定義のCLデータ取得
 *-------------------------------------------------------*
*/
function chkFDB2CSV1PG($db2con, $d1name) {
    $data = array();
    $strSQL.= '    SELECT ';
    $strSQL.= '        A.DGNAME, ';
    $strSQL.= '        A.DGBLIB, ';
    $strSQL.= '        A.DGBPGM, ';
    $strSQL.= '        A.DGCLIB, ';
    $strSQL.= '        A.DGCPGM, ';
    $strSQL.= '        A.DGCBNM, ';
    $strSQL.= '        A.DGALIB, ';
    $strSQL.= '        A.DGAPGM, ';
    $strSQL.= '        CASE ';
    $strSQL.= '            WHEN A.DGBLIB <> \'\' AND A.DGBPGM <> \'\' THEN \'1\' ';
    $strSQL.= '            ELSE \'\' ';
    $strSQL.= '        END AS DGBFLG , ';
    $strSQL.= '        CASE ';
    $strSQL.= '            WHEN A.DGALIB <> \'\' AND A.DGAPGM <> \'\' THEN \'1\' ';
    $strSQL.= '            ELSE \'\' ';
    $strSQL.= '        END AS DGAFLG ';
    $strSQL.= '    FROM ';
    $strSQL.= '        FDB2CSV1PG A ';
    $strSQL.= '    WHERE ';
    $strSQL.= '       A.DGNAME  = ? ';
    $strSQL.= '    AND ((A.DGBLIB <> \'\' AND A.DGBPGM <> \'\') ';
    $strSQL.= '    OR  (A.DGALIB <> \'\' AND A.DGAPGM <> \'\')) ';
    $params = array($d1name);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
        e_log(db2_stmt_errormsg() . $strSQL);
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = umEx($data);
            if (count($data > 0)) {
                $data = $data[0];
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
function getCurrentTimeStamp($db2con) {
    $data = array();
    $strSQL = '    SELECT  ';
    $strSQL.= '    TO_CHAR(CURRENT TIMESTAMP,\'YYYYMMDDHH24MISS\') AS CURTIME ';
    $strSQL.= '    FROM sysibm/sysdummy1 ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data[0]);
        }
    }
    $curTime = $data['data']['CURTIME'];
    return $curTime;
}
/*
 *-------------------------------------------------------*
 * メール配信マスター取得
 *-------------------------------------------------------*
*/
function getDB2WAUT($db2con, $pName, $wapkey) {
    $rtn = array();
    $data = array();
    $strSQL = ' SELECT ';
    $strSQL.= getDb2wautColumns();
    $strSQL.= ' FROM DB2WAUT AS A ';
    $strSQL.= ' WHERE A.WANAME = ? ';
    $strSQL.= ' AND A.WASPND <> \'1\' ';
    $strSQL.= ' AND A.WAPKEY = ?  ';
    $params = array($pName, $wapkey);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
        //e_log('【削除】テーブルデータ取得エラー①：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
            // e_log('【削除】テーブルデータ取得エラー②：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
            
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * DB2WAUT取得カラム
 *-------------------------------------------------------*
*/
function getDb2wautColumns() {
    $strSQL = ' A.WANAME, ';
    $strSQL.= ' A.WAPKEY, ';
    $strSQL.= ' A.WAMAIL, ';
    $strSQL.= ' A.WAMACC, ';
    $strSQL.= ' A.WACSV,  ';
    $strSQL.= ' A.WAXLS,  ';
    $strSQL.= ' A.WAHTML, ';
    $strSQL.= ' A.WASPND, ';
    $strSQL.= ' A.WASNKB, ';
    $strSQL.= ' A.WASFLG, ';
    $strSQL.= ' A.WAFRAD, ';
    $strSQL.= ' A.WAFRNM, ';
    $strSQL.= ' A.WASUBJ, ';
    $strSQL.= ' A.WABODY, ';
    $strSQL.= ' A.WATYPE, ';
    $strSQL.= ' A.WAQUNM, ';
    $strSQL.= ' A.WAFONT, ';
    $strSQL.= ' A.WAQGFLG, ';
    $strSQL.= ' A.WAPIV, ';//MSM add for mail sent link 20180906
    $strSQL.= ' A.WANGFLG, ';//MSM add for mail sent link 20180911
    $strSQL.= ' A.WAGPID ';//MSM add for mail sent link 20180911
    return $strSQL;
}
/*
 *-------------------------------------------------------*
 * ファイルをコーピするファンクション
 *-------------------------------------------------------*
*/
function makeCopyFile($filepath, $fromfilename, $mailAddLst, $nowYmd, $nowTime, $ext) {
    foreach ($mailAddLst as $key => $value) {
        if (cmMer($value['WAQUNM']) !== '' && cmMer($value['WAQUNM']) !== null) {
            $WAQUNM = replaceFilename(cmMer($value['WAQUNM']));
            $tofilename = $WAQUNM . '_' . $nowYmd . '_' . $nowTime . $ext;
            shell_exec('cp ' . $filepath . '"' . $fromfilename . '"' . ' ' . $filepath . '"' . $tofilename . '"');
        }
    }
}
/*
 *-------------------------------------------------------*
 * エクセル作成　保存
 *-------------------------------------------------------*
*/
function exeExcel($fp, $csv_h, $csv_d, $fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, $burstItmLst, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg, $sheet, $row, $htmlFlg, $cnddatArr, $DB2EINSROWMAX = 0, $filepath = '') {
    $wspkey = cmMer($wspkey);
    $pivotresult = '';
    if ($filepath === '') {
        $filepath = BASE_DIR . '/php/xls/';
    }
    if ($wspkey !== '') {
        /*     $csv_h = cmHsc($csv_h);
        if ($csv_d === 0) { //データの合計
        _fputpivot_ZeroKen($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmLst, false, '', $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $filepath);
        return false; //ループからbreak
        } else {
        $pivotresult = exePivotExcel($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmLst, false, '', $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $cnddatArr, $DB2EINSROWMAX, $filepath);
        if ($pivotresult === 'FAIL_EXEMAXLEN')
        return $pivotresult;
        else
        return false;
        }*/
    } else {
        $D1TMPF = '';
        $ext = getExt($d1name);
        //        e_log('Excel template Version:'.EXCELVERSION.'EXT:'.$ext);
        $DB2WCOL = array();
        if ((EXCELVERSION == '2003' && $ext === '') || $ext === 'xls') {
            $csv_d = umEx($csv_d);
            $excelname = $filename . '.xls';
            //e_log('【削除】FDB2CSV2データ①：'.print_r($fdb2csv2,true));
            $row = cmCreateXLS($sheet, $csv_h, $csv_d, $fdb2csv2, $excelname, $DB2WCOL, $d1name, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg, $row, $htmlFlg);
            //            $row      = cmCreateXLS($csv_h, $csv_d, $fdb2csv2, $excelname, $DB2WCOL, $d1name,$DB2ECON,$DB2EINS,$CNDSDATAF);
            /*$writer    = PHPExcel_IOFactory::createWriter($book, "Excel5");
            $writer->save($filepath . $excelname);
            $book = null;*/
            $ext = '.xls';
            return $row;
        } else if ((EXCELVERSION == '2007' && $ext === '') || $ext === 'xlsx') {
            $csv_d = umEx($csv_d);
            $excelname = $filename . '.xlsx';
            //e_log('【削除】FDB2CSV2データ②：'.print_r($fdb2csv2,true));
            $row = cmCreateXLS($sheet, $csv_h, $csv_d, $fdb2csv2, $excelname, $DB2WCOL, $d1name, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg, $row, $htmlFlg);
            //            $row      = cmCreateXLS($csv_h, $csv_d, $fdb2csv2, $excelname, $DB2WCOL, $d1name,$DB2ECON,$DB2EINS,$CNDSDATAF);
            /*$writer    = PHPExcel_IOFactory::createWriter($book, "Excel2007");
            $writer->save($filepath . $excelname);
            $book = null;*/
            $ext = '.xlsx';
            return $row;
        } else if (EXCELVERSION == 'html' || $ext === '') {
            $csv_d = umEx($csv_d, true);
            $csv_h = cmHsc($csv_h);
            /*$excelname = $filename . '.xls';
            
            if (!file_exists($filepath . $excelname)) {
            touch($filepath . $excelname);
            }
            $fp = fopen($filepath . $excelname, 'w');*/
            //            e_log("ここも行きました。");
            _fputexcel($fp, $csv_h, $csv_d, $fdb2csv2, array(), false, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg);
            //    fclose($fp);
            $ext = '.xls';
        }
        //        makeCopyFile($filepath, $excelname, $mailAddLst, $nowYmd, $nowTime, $ext);
        
    }
}
function getExt($d1name) {
    $ext = '';
    $fdb2csv1 = FDB2CSV1_DATA($d1name);
    if ($fdb2csv1['result'] === true) {
        $D1TMPF = cmMer($fdb2csv1['data'][0]['D1TMPF']);
        if ($D1TMPF !== '') {
            //            $D1TMPF = cmMer($fdb2csv1['data'][0]['D1TMPF']);
            $ext = explode('.', $D1TMPF);
            $ext = $ext[count($ext) - 1];
        }
    }
    return $ext;
}
/*
 *-------------------------------------------------------*
 * HTML作成　保存
 *-------------------------------------------------------*
*/
function exeHtml($fp, $csv_h, $csv_d, $fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, $burstItmLst, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg, $cnddatArr) {
    $pivotresult = '';
    $csv_d = umEx($csv_d, true);
    $csv_h = cmHsc($csv_h);
    $wspkey = cmMer($wspkey);
    if ($wspkey !== '') {
        //e_log("pivot key->".$wspkey."pivot mail->".$csv_d.'*****'.print_r($csv_d,true));
        //        if (count($csv_d) === 0) {
        /*      if ($csv_d === 0) {
        _fputpivot_ZeroKen($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmLst, true, '', $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF);
        } else {
        $pivotresult = exePivotExcel($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmLst, true, '', $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $cnddatArr);
        }
        return false; //ループからbreak*/
    } else {
        /*   $filepath  = BASE_DIR . '/php/html/';
        $excelname = $filename . '.html';
        
        if (!file_exists($filepath . $excelname)) {
        touch($filepath . $excelname);
        }
        
        $fp = fopen($filepath . $excelname, 'w');
        */
        _fputexcel($fp, $csv_h, $csv_d, $fdb2csv2, array(), false, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg);
        // fclose($fp);
        //        makeCopyFile($filepath, $excelname, $mailAddLst, $nowYmd, $nowTime, '.html');
        
    }
}
/*
 *-------------------------------------------------------*
 *   ピッボとのエクセル作成　保存
 *-------------------------------------------------------*
*/
/*
 *-------------------------------------------------------*
 *   ピッボとのエクセル作成　保存
 *-------------------------------------------------------*
*/
function exePivotExcel($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmLst, $htmlFlg, $htmlcreate, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $cnddatArr, $DB2EINSROWMAX = 0, $pivotFlg, $fpath = '', $D1DIEX = '') {
    /* $WEBF = '';
    $RDBNM = '';
    
    $reWebf = fnGetFDB2CSV1($db2con, $d1name);
    if ($reWebf['result'] !== true) {
    $rtn = 1;
    } else {
    $WEBF = $reWebf['data'][0]['D1WEBF'];
    $RDBNM = $reWebf['data'][0]['D1RDB'];
    }*/
    //e_log("exePivotExcel start:" .$fpath);
    include_once ("PivotClass.php");
    $pivotRtn = array();
    $pivotRtn['RTN'] = 0;
    $pivotRtn['PIVOTHTML'] = '';
    $pivotRtn['EXEMAXLEN'] = '';
    $searchdata = array();
    $chkcoldata = array();
    $search_col_arr = array();
    $operator = array();
    $SearchCol = '';
    $pivotCol = array();
    $colData = array();
    $xchecked = array();
    $ychecked = array();
    $xcheckedUnhide = array(); //項目表示フラグ配列（縦軸）
    $ycheckedUnhide = array(); //項目表示フラグ配列（横軸）
    $cchecked = array();
    $xcheckedcount = 0;
    $WEBF = '';
    $RDBNM = '';
    //ウェブフラグ
    $reWebf = fnGetFDB2CSV1($db2con, $d1name);
    if ($reWebf['result'] !== true) {
        $rtn = 1;
    } else {
        $WEBF = $reWebf['data'][0]['D1WEBF'];
        $RDBNM = $reWebf['data'][0]['D1RDB'];
    }
    if ($burstItmLst != '') {
        if (cmMer($burstItmLst['WABAFLD']) !== '') {
            $colData = fnGetDB2PCOL($db2con, $d1name, $wspkey);
            if ($colData['result'] !== true) {
            } else {
                $colData = umEx($colData['data']);
                foreach ($colData as $value) {
                    $col = $value['WPFLD'] . '_' . $value['WPFILID'];
                    array_push($pivotCol, $col);
                }
            }
            $SearchCol = cmMer($burstItmLst['WABAFLD']) . '_' . cmMer($burstItmLst['WABAFID']);
            if (in_array($SearchCol, $pivotCol)) {
                if (cmMer($burstItmLst['WABFLG']) === '0') {
                    if (cmMer($burstItmLst['WABAFR']) !== '' && cmMer($burstItmLst['WABATO']) !== '') {
                        $chkcoldata = array("1" => 1, "2" => 2);
                        $searchdata = array("1" => cmMer($burstItmLst['WABAFR']), "2" => cmMer($burstItmLst['WABATO']));
                        $search_col_arr = array("1" => $SearchCol, "2" => $SearchCol);
                        $operator = array("1" => '>=', '2' => '<=');
                    } else if ($burstItmLst['WABAFR'] !== '') {
                        $chkcoldata = array("1" => 1);
                        $searchdata = array("1" => cmMer($burstItmLst['WABAFR']));
                        $search_col_arr = array("1" => $SearchCol);
                        $operator = array("1" => '=');
                    }
                } else if (cmMer($burstItmLst['WABFLG']) === '1') {
                    $WLDATA = $burstItmLst['WLDATA'];
                    $idx = 1;
                    $operator['FLG'] = ' OR ';
                    foreach ($WLDATA as $value) {
                        $i = $idx++;
                        $chkcoldata[$i] = $i;
                        $searchdata[$i] = $value;
                        $search_col_arr[$i] = $SearchCol;
                        $operator[$i] = '=';
                    }
                }
            }
        }
    }
    $db2pmst_d = fnGetDB2PMST($db2con, $d1name, $wspkey);
    if ($db2pmst_d['result'] !== true) {
    } else {
        $db2pmst = umEx($db2pmst_d['data']);
    }
    //e_log('ピボットデータ取得①：'.print_r(db2pmst_d,true));
    $db2pcol_d = fnGetDB2PCOL($db2con, $d1name, $wspkey);
    if ($db2pcol_d['result'] !== true) {
    } else {
        //e_log('ピボットデータ取得②：'.print_r(db2pcol_d,true));
        $db2pcol = umEx($db2pcol_d['data']);
        foreach ($db2pcol as $key => $value) {
            //FILD=9999の場合は無条件でarray()
            if ($value['WPFILID'] === '9999') {
                $checkRs = array();
            } else {
                $checkRs = cmCheckFDB2CSV2($db2con, $d1name, $value['WPFILID'], $value['WPFLD']);
            }
            if ($checkRs === false) {
                $pivotRtn['RTN'] = 1;
                $errMsg = showMsg('FAIL_SYS');
                fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
            } else {
                if (count($checkRs) === 0) {
                    $checkRs = cmCheckFDB2CSV5($db2con, $d1name, $value['WPFLD'],"");//MSM add "" for missing parameter error 20180918
                    if ($checkRs === false) {
                        $pivotRtn['RTN'] = 1;
                        $errMsg = showMsg('FAIL_SYS');
                        fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                        break;
                    } else {
                        if (count($checkRs) === 0) {
                            $pivotRtn['RTN'] = 1;
                            $errMsg = showMsg('PIVOT_NO_COLS'); //'クエリーに登録されていないカラムが選択されています。クエリーの設定をご確認ください。';
                            fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                            break;
                        }
                    }
                }
            }
        }
        if ($pivotRtn['RTN'] === 0) {
            $shukeiXdisplayFlg = false;
            $shukeiYdisplayFlg = false;
            $columndata = array();
            $columnupdata = array();
            $calcdata = array();
            foreach ($db2pcol as $value) {
                $wppflg = $value['WPPFLG'];
                $wpseqn = $value['WPSEQN'];
                $wpfilid = $value['WPFILID'];
                $wpsort = $value['WPSORT'];
                $column = $value['WPFLD'] . '_' . $value['WPFILID'];
                $wpfhide = $value['WPFHIDE'];
                $wpsort = $value['WPSORT'];
                $wpsumg = $value['WPSUMG'];
                $d2len = $value['D2LEN'];
                $d2dec = $value['D2DEC'];
                $d2type = $value['D2TYPE'];
                $d2wedt = $value['D2WEDT'];
                $orderDir = '';
                if ($wpsort == '1') {
                    $orderDir = ' ASC';
                } else if ($wpsort == '0') {
                    $orderDir = ' DESC';
                }
                switch ($wppflg) {
                    case '1':
                        $columndata[$wpseqn] = $column;
                        if ($wpsumg === "") {
                            $shukeiXdisplayFlg = true;
                        }
                        $xchecked[$column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt);
                        if ($wpfhide === '') {
                            $xcheckedcount++;
                            $xcheckedUnhide[$column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt);
                        }
                    break;
                    case '2':
                        $columnupdata[$wpseqn] = $column;
                        if ($wpsumg === "") {
                            $shukeiYdisplayFlg = true;
                        }
                        //表示非表示配列格納
                        $ychecked[$column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt);
                        if ($wpfhide === '') {
                            $ycheckedUnhide[$column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt);
                        }
                    break;
                    case '3':
                        $calcdata[$wpseqn] = $column;
                        //表示非表示配列格納
                        $cchecked[$column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => $d2wedt);
                        $cchecked['_SUM_' . $column] = array('FILID' => $wpfilid, 'HIDE' => $wpfhide, 'SORT' => $wpsort, 'SUMG' => $wpsumg, 'LEN' => $d2len, 'DEC' => $d2dec, 'TYPE' => $d2type, 'WEDT' => '');
                    break;
                }
            }
        }
    }
    if ($pivotRtn['RTN'] === 0) {
        $db2pcal_d = fnGetDB2PCAL($db2con, $d1name, $wspkey);
        if ($db2pcal_d['result'] !== true) {
        } else {
            $db2pcal = umEx($db2pcal_d['data']);
            $num_formula = array();
            if (count($db2pcal) > 0) {
                for ($i = 0;$i < 10;$i++) {
                    if (isset($db2pcal[$i])) {
                        $num_formula[] = $db2pcal[$i]['WCCALC'];
                    } else {
                        $num_formula[] = "";
                    }
                }
            } else {
                $num_formula = array("", "", "", "", "");
            }
            $getcheadingcol = array();
            $data_studio = array();
            $rtncolumn = array();
            $colslists = array();
            $gb_count = 0;
            $pgstart = '';
            $pgend = '';
            $flg = '2';
            //       for($q = 0 ; $q <2; $q++){
            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                $dbCond = $db2con;
            } else {
                $dbCond = $db2RDBcon;
            }
            //QUERY TABLE CHECK
            if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
                $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
                $db2RDBcon = cmDB2ConRDB(SAVE_DB);
            }
            //$fp = fopen(TEMP_DIR . cmMer($d1name) . $time . '.xls', 'w');
            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                $pivotCount = new Pivot($db2con, SAVE_DB, $dbname, $d1name);
            } else {
                $pivotCount = new Pivot($db2RDBcon, SAVE_DB, $dbname, $d1name);
            }
            $columnup_data = $columnupdata;
            $column_data = $columndata;
            $x_checked = $xchecked;
            $y_checked = $ychecked;
            $rowstart = '';
            $rowend = '';
            $pgstart = 0;
            $pgend = 0;
            $zentaiCount = 0;
            //e_log('【削除】ピボットデータ①：'.print_r($columndata,true));
            //e_log('【削除】ピボットデータ②：'.print_r($xchecked,true));
            //e_log('【削除】ピボットデータ③：'.print_r($ychecked,true));
            //e_log('【削除】ピボットデータ④：'.print_r($chkcoldata,true));
            $pivotCount->crossData($flg, $searchdata, $chkcoldata, $search_col_arr, $operator, $columndata, $columnupdata, $calcdata, $num_formula, $rowstart, $rowend, $pgstart, $pgend, $shukeiXdisplayFlg, $shukeiYdisplayFlg, $xchecked, $ychecked, true, false);
            $zentaiCount = $pivotCount->get_zentaiCount();
            //MAX件数のためカウント
            $EXCELMAXLEN = 65536;
            $pivotCountArr = array();
            $pivotCountArr = cmGetHeaderCountDB2PCOL($db2con, $d1name, $wspkey);
            if ($pivotCountArr['result'] === true) {
                $DB2EINSROWMAX+= $pivotCountArr['TOTALCOUNT']; //ピボートヘッダーカウント
                
            }
            $DB2EINSROWMAX+= $zentaiCount;
            //e_log('in exeMailSend.php pivot $DB2EINSROWMAX=>' . $DB2EINSROWMAX . '$EXCELMAXLEN=>' . $EXCELMAXLEN . '$pivotFlg[XLSFLG]=>' . $pivotFlg['XLSFLG']);
            //htmlcreate//
            if ($DB2EINSROWMAX > $EXCELMAXLEN && $pivotFlg['XLSFLG'] === true) { //check maximum length of EXCEL Myatsu change XLSFLG
                $pivotRtn['EXEMAXLEN'] = 'FAIL_EXEMAXLEN';
                $pivotFlg['XLSFLG'] = false;
            }
            $pivot_html = '';
            $htmlCreateBreak = false;
            $htmlCreateCount = 0;
            $htmlCount = 0;
            $xlsCount = 0;
            $lastrowhtml = array(); //htmlformat's last row
            $lastrowxlsf = array(); // xls file last row
            $lastrowhtmlf = array(); // html  file last row
            if ($pivotFlg['XLSFLG'] === true || $pivotFlg['HTMLFLG'] === true || $pivotFlg['MAILHTMLFLG'] === true) { //Myatsu change XLSFLG
                // e_log('************************no error**************************************');
                $lenArr = cmPivotTotalLen($zentaiCount);
                $lenCount = count($lenArr);
                for ($idx = 0;$idx < $lenCount;$idx++) { ///
                    //e_log("row loop ".$idx.$lenCount);// 後で消す
                    if ($htmlCreateBreak === true) { //EXCEL or HTML　ファイルが送らない場合はループからでる
                        if ($pivotFlg['HTMLFLG'] !== true && $pivotFlg['XLSFLG'] !== true) {
                            // e_log("break");
                            break;
                        }
                    }
                    $flg = '2';
                    $rowstart = '';
                    $rowend = '';
                    $getcheadingcol = array();
                    $data_studio = array();
                    $rtncolumn = array();
                    $colslists = array();
                    $pgstart = $lenArr[$idx][0];
                    $pgend = $lenArr[$idx][1];
                    $headerFlg = false;
                    if ($idx === 0) {
                        $headerFlg = true;
                    }
                    $lastFlg = false;
                    if ($idx === ($lenCount - 1)) {
                        $lastFlg = true; //総合計出さない
                        
                    }
                    $columnupdata = $columnup_data;
                    $columndata = $column_data;
                    $xchecked = $x_checked;
                    $ychecked = $y_checked;
                    do {
                        //   e_log("column loop ".$testcount++);// 後で消す
                        //e_log('data ' . $idx . '_' . $lenCount . 'START' . $rowstart . 'END' . $rowend);
                        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                            $pivot = new Pivot($db2con, SAVE_DB, $dbname, $d1name);
                        } else {
                            $pivot = new Pivot($db2RDBcon, SAVE_DB, $dbname, $d1name);
                        }
                        $pivot->crossData($flg, $searchdata, $chkcoldata, $search_col_arr, $operator, $columndata, $columnupdata, $calcdata, $num_formula, $rowstart, $rowend, $pgstart, $pgend, $shukeiXdisplayFlg, $shukeiYdisplayFlg, $xchecked, $ychecked, false, $lastFlg);
                        $file_name = $filename;
                        $columnupdata = $pivot->get_columnupdata();
                        $rowstart = $pivot->get_rowstart();
                        $rowend = $pivot->get_rowend();
                        $endcount = $pivot->get_endcount();

                        if (count($columnupdata) > 0) {
                            $getcheadingcol = array_merge($getcheadingcol, $pivot->get_getcheadingcol());
                        } else {
                            $getcheadingcol = $pivot->get_getcheadingcol();
                        }
                        $columndata = $pivot->get_columndata();
                        if (count($colslists) > 0) {
                            $colslists = array_merge($colslists, $pivot->get_colslists());
                        } else {
                            $colslists = $pivot->get_colslists();
                        }
                        $countcalc = $pivot->get_countcalc();
                        $num_count = $pivot->get_num_count();
                        $rtncolumn = array_merge($rtncolumn, $pivot->get_rtncolumn());
						//e_log('Count Data studio in looping*****'.count($data_studio));
						//e_log('get_data_studio in looping*****'.print_r($pivot->get_data_studio(),true));

                        if (count($data_studio) > 0) {
                            foreach ($pivot->get_data_studio() as $key => $value) {
                                foreach ($value as $k => $v) {
                                    $data_studio[$key][$k] = $v;
                                }
                            }
                        } else {
                            $data_studio = $pivot->get_data_studio();
                        }
                        $gb_count = count($data_studio);
                        $flg = '3';
                    } while ($endcount !== $rowend && count($columnupdata) > 0);
                    if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
                        cmDb2Close($db2RDBcon);
                    }
                    $filepath = '';
                    $ext = '';
                    $xchecked = $xcheckedUnhide;
                    $ychecked = $ycheckedUnhide;
                    if ($pivotFlg['MAILHTMLFLG'] === true) {
                        if ($htmlCreateBreak !== true) {
                            //e_log("html create");
                            $htmlCreateRtn = _fputpivot_html($db2con, $fphtml, $getcheadingcol, $columnupdata, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, $htmlFlg, $xchecked, $ychecked, $cchecked, $xcheckedcount, $WEBF, $htmlCreateCount, $idx, $zentaiCount, $lastrowhtml);
                            $pivot_html.= $htmlCreateRtn['PIVOTHTML'];
                            $htmlCreateCount = $htmlCreateRtn['HTMLCREATECOUNT'];
                            $htmlCreateBreak = $htmlCreateRtn['HTMLCREATEBREAK'];
                            $lastrowhtml = $htmlCreateRtn['LASTROW'];
                        } else {
                            //e_log("html not create");
                            
                        }
                    }
                    if ($idx === 0) {
                        $rtnResultHtml = array();
                        if ($pivotFlg['HTMLFLG'] === true) {
                            $excelname = $file_name . '.html';
                            if (!file_exists(BASE_DIR . '/php/html/' . $excelname)) {
                                touch(BASE_DIR . '/php/html/' . $excelname);
                            }
                            $fphtml = fopen(BASE_DIR . '/php/html/' . $excelname, 'w');
                            $filepathhtml = BASE_DIR . '/php/html/';
                        }
                    }
                    if ($pivotFlg['HTMLFLG'] === true) {
                        $rtnFileHtml = _fputpivot($fphtml, $getcheadingcol, $columnupdata, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, true, $xchecked, $ychecked, $cchecked, $xcheckedcount, $WEBF, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg, $rtnResultHtml, $htmlCount, $zentaiCount, $lastrowhtmlf);
                        $rtnResultHtml = $rtnFileHtml['RTNRESULT'];
                        $htmlCount = $rtnFileHtml['ROWCOUNT'];
                        $lastrowhtmlf = $rtnFileHtml['LASTROW'];
                    }
                    //e_log("IDX KSK ==>> ".$idx."lenCount ==>> ".$lenCount);
                    if ($idx === 0) {
                        $rtnResultXls = array();
                        if ($pivotFlg['XLSFLG'] === true) {
                            //e_log("filepath of pivotksk=".$fpath);
                            $excelname = $file_name . '.xls';
                            if ($fpath == '') {
                                $filepath = BASE_DIR . '/php/xls/';
                            } else {
                                $filepath = $fpath;
                            }
                            if (!file_exists($filepath . $excelname)) {
                                touch($filepath . $excelname);
                            }
                            $fpxls = fopen($filepath . $excelname, 'w');
                            $filepathxls = $filepath;
                            //e_log("ParameterTestingKSK ".$filepath."excelname ==>> ".$excelname);
                            
                        }
                    }
                    if ($pivotFlg['XLSFLG'] === true) {
                        $rtnFileXls = _fputpivot($fpxls, $getcheadingcol, $columnupdata, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, false, $xchecked, $ychecked, $cchecked, $xcheckedcount, $WEBF, $DB2ECON, $DB2EINS, $CNDSDATAF, $headerFlg, $rtnResultXls, $xlsCount, $zentaiCount, $lastrowxlsf);
                        $rtnResultXls = $rtnFileXls['RTNRESULT'];
                        $xlsCount = $rtnFileXls['ROWCOUNT'];
                        $lastrowxlsf = $rtnFileXls['LASTROW'];
                    }
                    unset($data_studio);
                    /**dev=> awwkyaye => out of memory test **/
                    if ($lastFlg === true) {
                        if ($pivotFlg['HTMLFLG'] === true) {
                            fwrite($fphtml, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                            fclose($fphtml);
                            $excelname = $file_name . '.html';
                            $ext = '.html';
                            $ARR = array($filepathhtml, $excelname, $mailAddLst, $nowYmd, $nowTime, $ext);
                            makeCopyFile($filepathhtml, $excelname, $mailAddLst, $nowYmd, $nowTime, $ext);
                        }
                        if ($pivotFlg['XLSFLG'] === true) {
                            fwrite($fpxls, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                            fclose($fpxls);
                            $excelname = $file_name . '.xls';
                            $ext = '.xls';
                            $ARR = array($filepathxls, $excelname, $mailAddLst, $nowYmd, $nowTime, $ext);
                            makeCopyFile($filepathxls, $excelname, $mailAddLst, $nowYmd, $nowTime, $ext);
                        }
                    }
                } //end of for
                /*if ($htmlFlg !== true) {
                return '';
                }*/
                $pivotRtn['PIVOTHTML'] = $pivot_html;
            }
        }
    } return $pivotRtn;
}
//function _fputpivot_ZeroKen($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmLst, $htmlFlg, $htmlcreate, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF,$pivotFlg)
function _fputpivot_ZeroKen($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmLst, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $pivotFlg, $fpath = '') {
    $filepath = '';
    $ext1 = '';
    //e_log("_fputpivot_ZeroKen filepath".$fpath);
    if ($pivotFlg['HTMLFLG'] === true) {
        $excelname = $filename . '.html';
        if (!file_exists(BASE_DIR . '/php/html/' . $excelname)) {
            touch(BASE_DIR . '/php/html/' . $excelname);
        }
        $filepath = BASE_DIR . '/php/html/';
        $ext1 = '.html';
        $fp = fopen(BASE_DIR . '/php/html/' . $excelname, 'w');
        _fputPivotExcelFirstData($fp, $d1webf, $DB2ECON, $DB2EINS, $CNDSDATAF);
        fclose($fp);
        makeCopyFile($filepath, $excelname, $mailAddLst, $nowYmd, $nowTime, $ext1);
    }
    if ($pivotFlg['XLSFLG'] === true) {
        /* $ext = getExt($d1name);
         if ($ext === '') {*/
        $ext = '.xls';        
        $excelname = $filename . $ext;
        if ($fpath == '') {
            $filepath = BASE_DIR . '/php/xls/';
        } else {
            $filepath = $fpath;
        }
        if (!file_exists($filepath . $excelname)) {
            touch(filepath . $excelname);
        }
        $ext1 = $ext;
        $fp = fopen($filepath . $excelname, 'w');
        _fputPivotExcelFirstData($fp, $d1webf, $DB2ECON, $DB2EINS, $CNDSDATAF);
        fclose($fp);
        makeCopyFile($filepath, $excelname, $mailAddLst, $nowYmd, $nowTime, $ext1);
    }
}
function _fputpivot_html($db2con, $fp, $getcheadingcol, $columnupdata, $columndata, $colslists, $countcalc, $num_count, $rtncolumn, $data_studio, $htmlFlg, $xchecked, $ychecked, $cchecked, $xcheckedcount, $d1webf, $count, $idxflg, $zentaiCount, $lastrow) {
    $colslists = umEx($colslists, true);
    //sFlg : 0=> head_color ; 1 => result_color
    $head_color = '';
    if (isset($_SESSION['PHPQUERY'])) {
        if ($_SESSION['PHPQUERY']['LOGIN'] === '1') {
            $db2con = cmDb2Con();
            cmSetPHPQUERY($db2con);
            $DB2WUSR = cmGetDB2WUSR($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
            $DB2WUSR = umEx($DB2WUSR);
            cmDb2Close($db2con);
            $wuclr3 = $DB2WUSR[0]['WUCLR3'];
            $wuclr3 = (($wuclr3 === '') ? COLOR3 : $wuclr3);
            error_log('wuclr3 = '.$wuclr3);
        }
    }else{
        $wuclr3 = CND_COFCLR;
    }
    switch ($wuclr3) {
        case 'red':
            $head_color = 'background-color:#b60f0b;color:#ffffff;';
            $result_color = 'background-color:#d9100d;color:#ffffff;';
            $result_shukei = 'background-color:#f7cfce;';
        break;
        case 'blue':
            $head_color = 'background-color:#005879;color:#ffffff;';
            $result_color = 'background-color:#007099;color:#ffffff;';
            $result_shukei = 'background-color:#cce2ea;';
        break;
        case 'orange':
            $head_color = 'background-color:#a66505;color:#ffffff;';
            $result_color = 'background-color:#df8807;color:#ffffff;';
            $result_shukei = 'background-color:#f8e7cd;';
        break;
        case 'purple':
            $head_color = 'background-color:#541fa7;color:#ffffff;';
            $result_color = 'background-color:#7a3ddb;color:#ffffff;';
            $result_shukei = 'background-color:#e4d8f7;';
        break;
        case 'green':
            $head_color = 'background-color:#246534;color:#ffffff;';
            $result_color = 'background-color:#2f8344;color:#ffffff;';
            $result_shukei = 'background-color:#d5e6d9;';
        break;
        case 'fb':
            $head_color = 'background-color:#192441;color:#ffffff;';
            $result_color = 'background-color:#2b3d6e;color:#ffffff;';
            $result_shukei = 'background-color:#d4d8e2;';
        break;
        case 'muted':
            $head_color = 'background-color:#595959;color:#ffffff;';
            $result_color = 'background-color:#737373;color:#ffffff;';
            $result_shukei = 'background-color:#e3e3e3;';
        break;
        case 'dark':
            $head_color = 'background-color:black;color:#ffffff;';
            $result_color = 'background-color:#494c50;color:#ffffff;';
            $result_shukei = 'background-color:#dadbdc;';
        break;
        case 'pink':
            $head_color = 'background-color:#6b2345;color:#ffffff;';
            $result_color = 'background-color:#9a3263;color:#ffffff;';
            $result_shukei = 'background-color:#ead6df;';
        break;
        case 'brown':
            $head_color = 'background-color:#4f2a1b;color:#ffffff;';
            $result_color = 'background-color:#5f3221;color:#ffffff;';
            $result_shukei = 'background-color:#dfd6d2;';
        break;
        case 'sea-blue':
            $head_color = 'background-color:#013760;color:#ffffff;';
            $result_color = 'background-color:#01487e;color:#ffffff;';
            $result_shukei = 'background-color:#ccdae5;';
        break;
        case 'banana':
            $head_color = 'background-color:#cb9704;color:#ffffff;';
            $result_color = 'background-color:#fab905;color:#ffffff;';
            $result_shukei = 'background-color:#fef1cd;';
        break;
        default:
            $head_color = 'background-color:#f3f3f3;color:#666;';
            $result_color = 'background-color:#f9f9f9;color:#666;';
            $result_shukei = 'background-color:#fdfdfd;';
        break;
    }   
    if ($head_color === '') {
        $head_color = 'background-color:rgb(79,129,189);color:#ffffff;';
        $result_color = 'background-color:#608dc3;color:#ffffff;';
        $result_shukei = 'background-color:#dbe5f1;';
    }

    $sFlg = '0';
    $h_color = $head_color;
    $colyHedColor = array_keys($ychecked);
    $colyHed = array();
    $shukeiArr = array();
    $shukeiArrColor = array();
    $breakFlg = false;
    $htmlCreateRtn = array();
    $htmlcreate = '';
    if ($idxflg === 0) {
        $htmlcreate.= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> ';
        $htmlcreate.= '<html><head> ';
        if ($htmlFlg === true) {
            $htmlcreate.= '<meta charset="UTF-8"> ';
        }
        $htmlcreate.= '<meta http-equiv="Content-Type" content="application/vnd.ms-excel; " /> ';
        $htmlcreate.= '<meta http-equiv="Content-Disposition" content="attachment; filename=output.xls" /> ';
        $htmlcreate.= '<style type="text/css"> ';
        $htmlcreate.= '<!-- .txt{mso-number-format:"\@";}--> ';
        $htmlcreate.= '</style> ';
        $htmlcreate.= '</head><body> ';
        $htmlcreate.= '<table style ="font-size : 12px;border-collapse: collapse;"> ';
        $htmlcreate.= '<thead> ';
        $tdthstyle = "border: 1px solid #ccc;white-space: nowrap;padding: 5px 10px 4px 10px;";
        for ($col = 0;$col < count($columnupdata);$col++) {
            if (PIVSCOLR === 1) {
                if ($sFlg === '0') {
                    $sFlg = '1';
                    $h_color = $head_color;
                } else {
                    $sFlg = '0';
                    $h_color = $result_color;
                }
            }
            $colyHed[$columnupdata[$col]] = $h_color;
            $colNm = $columnupdata[$col];
            $htmlcreate_tmp = '';
            $rowVal = '';
            $htmlcreate_tmp.= '<tr> ';
            //$getcheadingcol[$col] = preg_replace('/\s/', '&nbsp;', cmMer($getcheadingcol[$col]));
            $hedCol = str_replace(" ", "&#32;", cmMer($getcheadingcol[$col]));
            $hedCol = mb_convert_encoding($hedCol, 'HTML-ENTITIES', 'UTF-8');
            $htmlcreate_tmp.= '<th colspan="' . $xcheckedcount . '" style="text-align:right;border-bottom: none;' . $tdthstyle . $h_color . '">' . $hedCol . '</th> ';
            $colspancount = 1;
            for ($i = 0;$i < count($colslists);$i++) {
                $headColData = $colslists[$i][$columnupdata[$col]];
                if ($i + 1 !== count($colslists)) {
                    //2行目以降の場合、親行が変更されたかどうかをチェック
                    $parentChange = false;
                    if ($col > 0) {
                        if ($colslists[$i]['colspanEndFlg'] === true) {
                            $parentChange = true;
                        }
                    }
                    $heflg = true;
                    if ($headColData === '' && $colslists[$i + 1][$columnupdata[$col] . '_GP'] === '1') {
                        $heflg = false;
                    }
                    if ($headColData !== $colslists[$i + 1][$columnupdata[$col]] || ($parentChange === true) || $heflg === false) {
                        // 編集コードは「少数切捨て」の場合データのフォーマットセット
                        $headColData = pivotFloatFormat($d1webf, $ychecked[$colNm]['FILID'], $ychecked[$colNm]['TYPE'], $headColData, $ychecked[$colNm]['DEC'], 2, $ychecked[$colNm]['WEDT']);
                        $countddd = $colspancount * ($countcalc + $num_count);
                        $rowsp = '';
                        $headColNm = '';
                        if ($colslists[$i][$columnupdata[$col] . '_GP'] === '1') {
                            $headColData = '計';
                            $rowsp = ' rowspan = "' . (count($columnupdata) - $col) . '"';
                            $headColNm = pivotFloatFormat($d1webf, $ychecked[$columnupdata[$col - 1]]['FILID'], $ychecked[$columnupdata[$col - 1]]['TYPE'], $colslists[$i][$columnupdata[$col - 1]], $ychecked[$columnupdata[$col - 1]]['DEC'], 2, $ychecked[$columnupdata[$col - 1]]['WEDT']);
                        } else {
                            if (in_array($i, $shukeiArr)) {
                                $rowsp = false;
                            }
                        }
                        if ($rowsp !== false) {
                            $shukeiColColor = $h_color;
                            if ($headColData === '計') {
                                array_push($shukeiArr, $i);
                                $shukeiArrColor[$i] = $columnupdata[$col - 1];
                                $shukeiColColor = $colyHed[$columnupdata[$col - 1]];
                            }
                            //$headColData=preg_replace('/\s/', '&nbsp;', cmMer($headColData));
                            $headColData = str_replace(" ", "&#32;", cmMer($headColData));
                            $pData = mb_convert_encoding(($headColNm . $headColData), 'HTML-ENTITIES', 'UTF-8');
                            $htmlcreate_tmp.= '<th colspan="' . $countddd . '"' . $rowsp . ' style="text-align:left;border-bottom: none;' . $tdthstyle . $shukeiColColor . '" class="txt">' . $pData . '</th> ';
                        }
                        $colspancount = 0;
                        $colslists[$i]['colspanEndFlg'] = true;
                    }
                } else {
                    // 編集コードは「少数切捨て」の場合データのフォーマットセット
                    $headColData = pivotFloatFormat($d1webf, $ychecked[$colNm]['FILID'], $ychecked[$colNm]['TYPE'], $headColData, $ychecked[$colNm]['DEC'], 2, $ychecked[$colNm]['WEDT']);
                    $countddd = $colspancount * ($countcalc + $num_count);
                    $rowsp = '';
                    $headColNm = '';
                    if ($colslists[$i][$columnupdata[$col] . '_GP'] === '1') {
                        $headColData = '計';
                        $rowsp = ' rowspan = "' . (count($columnupdata) - $col) . '"';
                        $headColNm = pivotFloatFormat($d1webf, $ychecked[$columnupdata[$col - 1]]['FILID'], $ychecked[$columnupdata[$col - 1]]['TYPE'], $colslists[$i][$columnupdata[$col - 1]], $ychecked[$columnupdata[$col - 1]]['DEC'], 2, $ychecked[$columnupdata[$col - 1]]['WEDT']);
                    } else {
                        if (in_array($i, $shukeiArr)) {
                            $rowsp = false;
                        }
                    }
                    if ($rowsp !== false) {
                        $shukeiColColor = $h_color;
                        if ($headColData === '計') {
                            array_push($shukeiArr, $i);
                            $shukeiArrColor[$i] = $columnupdata[$col - 1];
                            $shukeiColColor = $colyHed[$columnupdata[$col - 1]];
                        }
                        //$headColData=preg_replace('/\s/', '&nbsp;', cmMer($headColData));
                        $headColData = str_replace(" ", "&#32;", cmMer($headColData));
                        $pData = mb_convert_encoding(($headColNm . $headColData), 'HTML-ENTITIES', 'UTF-8');
                        $htmlcreate_tmp.= '<th colspan="' . $countddd . '"' . $rowsp . ' style="text-align:left;border-bottom: none;' . $tdthstyle . $shukeiColColor . '" class="txt">' . $pData . '</th> ';
                    }
                    $colslists[$i]['colspanEndFlg'] = true;
                }
                $rowVal = $headColData;
                $colspancount++;
            }
            $rowspan = 0;
            if ($col === 0 && count((array)$ychecked) > 0) {
                if ($ychecked[$colNm]['SUMG'] !== '1') {
                    $rowspan = 1;
                    $colFlg = true;
                } else {
                    $colFlg = false;
                }
                if ($rowspan !== 0) {
                    if ($colFlg !== false) {
                        $cTotal = $countcalc + $num_count;
                        $pData = '総合計';
                        $pData = mb_convert_encoding(($pData), 'HTML-ENTITIES', 'UTF-8');
                        $shukeiArrColor[$i] = $columnupdata[$col];
                        $htmlcreate_tmp.= '<th style = "' . $head_color . '" rowspan ="' . count((array)$ychecked) . '"  colspan ="' . $cTotal . '">' . $pData . '</th>';
                    }
                }
                //}
                
            }
            $htmlcreate_tmp.= '</tr> ';
            if ($ychecked[$col]['HIDE'] !== '1') {
                $htmlcreate.= $htmlcreate_tmp;
            }
        }
        $htmlcreate.= '<tr> ';
        if (count($columndata) === 0) {
            $htmlcreate.= '<td style="' . $bottomstyle . $head_color . $tdthstyle . $head_color . '"></td> ';
        }
        if (PIVSCOLR === 1) {
            $shukeiColorArr = array();
            $resColcount = $countcalc + $num_count;
            foreach ($shukeiArrColor as $key => $val) {
                $sCol = $key * $resColcount;
                $sColRes = $shukeiArrColor[$key];
                $cColRes = $colyHed[$sColRes];
                for ($cidx = 0;$cidx < $resColcount;$cidx++) {
                    $cCol = $sCol + $cidx;
                    $shukeiColorArr[$cCol] = $cColRes;
                }
            }
        }
        $colHedColor = array_keys($xchecked);
        $colHed = array();
        $countIdx = 0;
        $hd_sFlg = $sFlg;
        //sFlg : 0=> head_color ; 1 => result_color
        $sFlg = '0';
        $countScolor = 0;
        foreach ($rtncolumn as $key => $val) {
            if (PIVSCOLR === 1) {
                if ($xcheckedcount > $countIdx) {
                    $hedKey = $colHedColor[$key];
                    if ($sFlg === '0') {
                        $sFlg = '1';
                        $h_color = $head_color;
                    } else {
                        $sFlg = '0';
                        $h_color = $result_color;
                    }
                    $colHed[$hedKey] = $h_color;
                } else {
                    $sFlg = $head_color;
                    if (count($columnupdata) > 0) {
                        if ($hd_sFlg === '0') {
                            $h_color = $head_color;
                        } else {
                            $h_color = $result_color;
                        }
                    }
                    if ($shukeiColorArr[$countScolor] !== null) {
                        $h_color = $shukeiColorArr[$countScolor];
                    }
                    $countScolor++;
                }
                $countIdx++;
            }
            $k = $columndata[$key];
            //$val[0]['COLUMN_HEADING'] = preg_replace('/\s/', '&nbsp;', cmMer($val[0]['COLUMN_HEADING']));
            $xColHD = str_replace(" ", "&#32;", cmMer($val[0]['COLUMN_HEADING']));
            $xColHD = mb_convert_encoding($xColHD, 'HTML-ENTITIES', 'UTF-8');
            if (isset($xchecked[$k]) === false) {
                $htmlcreate.= '<td style="border-bottom: none;' . $h_color . $tdthstyle . '" class="txt">' . $xColHD . '</td> ';
            } else if ($xchecked[$k]['HIDE'] === '') {
                $htmlcreate.= '<td style="border-bottom: none;' . $h_color . $tdthstyle . '" class="txt">' . $xColHD . '</td> ';
            }
        }
        $htmlcreate.= '</tr> ';
        $htmlcreate.= '</thead> ';
        $htmlcreate.= '<tbody> ';
    } //idx === 0
    $dta = $lastrow;
    $h_color = $head_color;
    for ($x = 0;$x < count($data_studio);$x++) {
        $count++;
        $trcolor = '#ffffff';
        if ($x % 2 !== 0) {
            $trcolor = 'rgb(250,250,250);';
        }
        $r = $data_studio[$x];
        //データの最後か、configで定めた最終行の場合、trにbotder-bottomのスタイルを追加
        if ($count === (int)HTMLMAXROW || $count === $zentaiCount) { //(($x + 1) === count($data_studio)) ) {
            $trcolor.= 'border-bottom:1px solid #ccc;';
        }
        $htmlcreate.= '<tr style="background-color:' . $trcolor . '"> ';
        $bottomstyle = '';
        //if ($x + 1 === count($data_studio)) {
        if ($count === $zentaiCount) {
            $bottomstyle = 'border-bottom:1px solid #ccc;';
        }
        if (count($columndata) === 0) {
            $htmlcreate.= '<td style="' . $bottomstyle . $head_color . $tdthstyle . '"></td> ';
        }
        $colVal = '';
        $sumFlg = - 1;
        $befChangeFlg = false; //ループ中、一つ前の列が変更されたかのフラグ
        $tFlg = false;
        $total = 0;
        $shukeiFlg = '';
        $countIdx = 0;
        $countScolor = 0;
        $sFlg = '0';
        $a = 0;
        foreach ($r as $i => $val) {
            if ($i !== 'ROWNUM' && strpos($i, 'GP') === false) {
                if (PIVSCOLR === 1) {
                    if ($sFlg === '0') {
                        $sFlg = '1';
                        $h_color = $head_color;
                    } else {
                        $sFlg = '0';
                        $h_color = $result_color;
                    }
                    $countIdx++; // 色変更フラグ
                    
                }
                $sumFlg+= 1;
                $fg = true;
                for ($p = 0;$p < count($columndata);$p++) {
                    if ($i === $columndata[$p]) {
                        $fg = false;
                    }
                }
                $flg = false;
                if ($val === '' && $r[$i . 'GP'] === '1') {
                    $flg = true;
                }
                $a++;
                if ((isset($dta[$i]) && $dta[$i] !== $val) || $befChangeFlg === true || ($x === 0 && $idxflg === 0) || $i === $columndata[count($columndata) - 1] || $flg === true) {
                    if ($fg !== false) {
                        //このif文はデータ部のみ通る
                        if ($val === '') {
                            $val = '<a style="color:red">Over</a>';
                            //$val = preg_replace('/\s/', '&nbsp;', cmMer($val));
                            $val = str_replace(" ", '&#32;', cmMer($val));
                            $val = mb_convert_encoding(($val), 'HTML-ENTITIES', 'UTF-8');
                            //縦軸集計カラー
                            $tdCssColor = '';
                            if ($shukeiFlg === '1') {
                                $tdCssColor = $result_shukei;
                            }
                            //横軸集計カラー
                            if (PIVSCOLR === 1) {
                                if ($shukeiColorArr[$countScolor] !== null) {
                                    $tdCssColor = $result_shukei;
                                }
                                $countScolor++;
                            }
                            $htmlcreate.= '<td style="border: 1px solid #ccc;border-bottom:none;border-top:1px solid #ccc;text-align:right;' . $bottomstyle . $tdCssColor . '">' . $val . '</td> ';
                        } else {
                            //アンダーバーはそのまま表示、それ以外は編集
                            if ($val !== '_') {
                                if (substr($i, 0, 3) !== 'RES') {
                                    $pattern = '/^C[0-9]+/i';
                                    $calcKey = preg_replace($pattern, '', $i);
                                    $val = pivotFloatFormat($d1webf, $cchecked[$calcKey]['FILID'], $cchecked[$calcKey]['TYPE'], $val, $cchecked[$calcKey]['DEC'], 2, $cchecked[$calcKey]['WEDT']);
                                }
                                if ($val !== '') {
                                    if (strpos($val, '.') !== false) {
                                        $tmp = explode('.', $val);
                                        $tmp[0] = num_format($tmp[0]);
                                        $val = $tmp[0] . '.' . $tmp[1];
                                    } else {
                                        $val = num_format($val);
                                    }
                                }
                            }
                            //$val = preg_replace('/\s/', '&nbsp;', cmMer($val));
                            $val = str_replace(" ", "&#32;", cmMer($val));
                            $val = mb_convert_encoding(($val), 'HTML-ENTITIES', 'UTF-8');
                            //縦軸集計カラー
                            $tdCssColor = '';
                            if ($shukeiFlg === '1') {
                                $tdCssColor = $result_shukei;
                            }
                            //横軸集計カラー
                            if (PIVSCOLR === 1) {
                                if ($shukeiColorArr[$countScolor] !== null) {
                                    $tdCssColor = $result_shukei;
                                }
                                $countScolor++;
                            }
                            $htmlcreate.= '<td style="border: 1px solid #ccc;border-bottom:none;border-top:1px solid #ccc;text-align:right;' . $tdCssColor . $bottomstyle . '">' . $val . '</td> ';
                        }
                    } else {
                        //ここのelseは見出し項目の場合のみ通る
                        if ($x === 0 && $a === 1) {
                            e_log("data_studio firstdata" . print_r($data_studio[0], true));
                            $a = 1;
                        }
                        if ($val !== '') {
                            $val = pivotFloatFormat($d1webf, $xchecked[$i]['FILID'], $xchecked[$i]['TYPE'], $val, $xchecked[$i]['DEC'], 2, $xchecked[$i]['WEDT']);
                        }
                        if ($xchecked[$i]['HIDE'] !== '1') {
                            $colSp = '';
                            $colSu = '';
                            if ($r[$i . 'GP'] === '1') {
                                // $colSu =$r[array_keys($xchecked)[$sumFlg-1]];
                                $prevCol = array_keys($xchecked);
                                $prevColum = $prevCol[$sumFlg - 1];
                                $colSu = $r[$prevColum];
                                if ($colSu === NULL) {
                                    $colSu = '総合';
                                } else {
                                    $colSu = pivotFloatFormat($d1webf, $xchecked[$prevColum]['FILID'], $xchecked[$prevColum]['TYPE'], $colSu, $xchecked[$prevColum]['DEC'], 2, $xchecked[$prevColum]['WEDT']);
                                }
                                $val = 'TOTAL';
                                $tFlg = true;
                                $total = (count((array)$xchecked) - $sumFlg);
                                $colSp = ' colspan = "' . (count((array)$xchecked) - $sumFlg) . '"';
                            }
                            if ($tFlg === true) {
                                if ($total !== (count((array)$xchecked) - $sumFlg)) {
                                    $colSp = false;
                                }
                            }
                            if ($colSp !== false) {
                                //$val = preg_replace('/\s/', '&nbsp;', cmMer($val));
                                //$val = str_replace(" ", "&#32;", cmMer($val));
                                //$val = mb_convert_encoding(($val),'HTML-ENTITIES','UTF-8');
                                $val = ($val !== 'TOTAL') ? $val : '計';
                                if ($val === "計" && PIVSCOLR === 1) {
                                    $shukeiFlg = '1';
                                    if ($colSu !== '総合') {
                                        $h_color = $colHed[$prevColum]; //column heading color array
                                        
                                    } else {
                                        $h_color = $head_color;
                                    }
                                }
                                $pData = $colSu . $val;
                                $pData = str_replace(" ", "&#32;", cmMer($pData));
                                $pData = mb_convert_encoding($pData, 'HTML-ENTITIES', 'UTF-8');
                                $htmlcreate.= '<td' . $colSp . '  style="white-space : nowrap;border: 1px solid #ccc;border-bottom:none;border-top:1px solid #ccc;text-align:left;' . $bottomstyle . $h_color . '" class="txt">' . $pData . '</td> ';
                            }
                        }
                        $colVal = $val;
                        $val = '';
                    }
                    $befChangeFlg = true;
                } else {
                    if ($fg !== false) {
                        if ($val !== '') {
                            //アンダーバーはそのまま表示、それ以外は編集
                            if ($val !== '_') {
                                if (substr($i, 0, 3) !== 'RES') {
                                    $pattern = '/^C[0-9]+/i';
                                    $calcKey = preg_replace($pattern, '', $i);
                                    $calcKey = preg_replace($pattern, '', $i);
                                    $val = pivotFloatFormat($d1webf, $cchecked[$calcKey]['FILID'], $cchecked[$calcKey]['TYPE'], $val, $cchecked[$calcKey]['DEC'], 2, $cchecked[$calcKey]['WEDT']);
                                }
                                if (strpos($val, '.') !== false) {
                                    $tmp = explode('.', $val);
                                    $tmp[0] = num_format($tmp[0]);
                                    $val = $tmp[0] . '.' . $tmp[1];
                                } else {
                                    $val = (int)$val;
                                    $val = num_format($val);
                                }
                            }
                        }
                        //$val=preg_replace('/\s/', '&nbsp;', cmMer($val));
                        $val = str_replace(" ", "&#32;", cmMer($val));
                        $val = mb_convert_encoding(($val), 'HTML-ENTITIES', 'UTF-8');
                        //縦軸集計カラー
                        $tdCssColor = '';
                        if ($shukeiFlg === '1') {
                            $tdCssColor = $result_shukei;
                        }
                        //横軸集計カラー
                        if (PIVSCOLR === 1) {
                            if ($shukeiColorArr[$countScolor] !== null) {
                                $tdCssColor = $result_shukei;
                            }
                            $countScolor++;
                        }
                        $htmlcreate.= '<td style="border: 1px solid #ccc;border-bottom:none;border-top:1px solid #ccc;text-align:right;' . $bottomstyle . $tdCssColor . '">' . $val . '</td> ';
                    } else {
                        //ここのelseは見出し項目の場合のみ通る
                        if ($x === 1 && $a === 1) {
                            e_log("data_studio seconddata" . print_r($data_studio[0], true));
                        }
                        //ここのelseは見出し項目の場合のみ通る
                        $val = pivotFloatFormat($d1webf, $xchecked[$i]['FILID'], $xchecked[$i]['TYPE'], $val, $xchecked[$i]['DEC'], 2, $xchecked[$i]['WEDT']);
                        if ($xchecked[$i]['HIDE'] !== '1') {
                            $htmlcreate.= '<td style="border: 1px solid #ccc;border-bottom:none;border-top:none;text-align:right;' . $bottomstyle . $h_color . '"></td> ';
                        }
                    }
                    $befChangeFlg = false;
                }
            }
        }
        $dta = $r;
        $htmlcreate.= '</tr> ';
        if ($count === (int)HTMLMAXROW || $count === $zentaiCount) {
            $breakFlg = true;
            break;
        }
    }
    if ($breakFlg === true) {
        $htmlcreate.= '</tbody></table><table><tr><td></td></tr>';
        $htmlcreate.= '</table></body></html> ';
    }
    $htmlCreateRtn['PIVOTHTML'] = $htmlcreate;
    $htmlCreateRtn['HTMLCREATECOUNT'] = $count;
    $htmlCreateRtn['HTMLCREATEBREAK'] = $breakFlg;
    $htmlCreateRtn['LASTROW'] = array();
    $datacount = count($data_studio);
    if ($datacount > 0) {
        $lastrow = $data_studio[$datacount - 1];
        $htmlCreateRtn['LASTROW'] = $lastrow;
        e_log("<br/>last data datastudio" . print_R($da, true));
    }
    return $htmlCreateRtn;
}
function fnGetDB2PMST($db2con, $d1name, $pmpkey) {
    $rtn = array();
    $data = array();
    $strSQL = '';
    $strSQL.= ' SELECT * FROM DB2PMST ';
    $strSQL.= ' WHERE PMNAME = ? AND PMPKEY = ? ';
    $params = array($d1name, $pmpkey);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            //$rtn = $data;
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}
function fnGetDB2PCOL($db2con, $d1name, $wppkey) {
    $rtn = array();
    $data = array();
    $strSQL = '';
    $strSQL.= ' SELECT A.*,B.D2LEN,B.D2DEC,B.D2TYPE,B.D2WEDT FROM DB2PCOL AS A ';
    $strSQL.= ' LEFT JOIN ';
    $strSQL.= ' ( ';
    $strSQL.= ' SELECT D2NAME,D2FILID,D2FLD,D2TYPE,D2LEN,D2DEC,D2WEDT from FDB2CSV2 ';
    $strSQL.= ' UNION ';
    $strSQL.= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID,D5FLD AS D2FLD,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DEC AS D2DEC,D5WEDT AS D2WEDT from FDB2CSV5 ';
    $strSQL.= ' ) AS B ';
    $strSQL.= ' ON A.WPNAME = B.D2NAME AND A.WPFILID = B.D2FILID AND A.WPFLD = B.D2FLD ';
    $strSQL.= ' WHERE WPNAME = ? AND WPPKEY = ? ';
    $strSQL.= ' ORDER BY WPPFLG,WPSEQN ';
    $params = array($d1name, $wppkey);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}
function fnGetDB2PCAL($db2con, $d1name, $wcckey) {
    $rtn = array();
    $data = array();
    $strSQL = '';
    $strSQL.= ' SELECT * FROM DB2PCAL ';
    $strSQL.= ' WHERE WCNAME = ? AND WCCKEY = ? ';
    $strSQL.= ' ORDER BY WCSEQN ';
    $params = array($d1name, $wcckey);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}
function exeCsvstream($filename, $xlsCustomFlg = '') {
    $csvname = $filename . '.csv';
    $filepath = BASE_DIR . '/php/csv/';
    if ($xlsCustomFlg !== '') {
        $filepath = BASE_DIR . '/php/xls/';
    }
    if (!file_exists($filepath . $csvname)) {
        touch($filepath . $csvname);
    }
    $stream = fopen($filepath . $csvname, 'w');
    return $stream;
}
/*
 *-------------------------------------------------------*
 * CSV作成処理
 *-------------------------------------------------------*
*/
function exeCsv($fp, $csv_h, $csv_d, $define, $d1name, $filename, $mailAddLst, $nowYmd, $nowTime, $headerFlg = false, $xlsCustomFlg = '', $filepath = '') {
    //file名作成
    $csvname = $filename . '.csv';
    if ($filepath === '') {
        $filepath = BASE_DIR . '/php/csv/';
    }
    if ($xlsCustomFlg !== '') {
        $cExt = getExt($d1name);
        if ($filepath === '') {
            $filepath = BASE_DIR . '/php/xls/';
        } else {
            $filepath = BASE_DIR . '/php/tmp_zip/';
        }
        $templateFn = $d1name . '.' . $cExt;
        $tofilename = $filename . '.' . $cExt;
    }
    /*if (!file_exists($filepath . $csvname)) {
    touch($filepath . $csvname);
    }
    
    $fp = fopen($filepath . $csvname, 'w');*/
    $data = $csv_d;
    //$data = umEx($data);
    //    array_unshift($data, $csv_h);　
    $headerCnt = count($csv_h);
    $fdb2csv1 = FDB2CSV1_DATA($d1name);
    //$fdb2csv1 = umEx($fdb2csv1);
    if ($headerFlg === true) {
        $D1THFG = cmHsc($fdb2csv1['data'][0]['D1THFG']);
        $D1THFG = um($D1THFG);
        $cutomtempateFlg = false;
        if ($xlsCustomFlg !== '') {
            if ($D1THFG !== '') {
                $cutomtempateFlg = true; //ヘッダ出力がない
                
            }
        }
        if ($cutomtempateFlg === true) {
            $data = array();
        } else {
            $data = array($csv_h);
        }
    }
    $fdb2csv1 = $fdb2csv1['data'][0];
    //        while(list($key, $value) = each($data)){
    foreach ($data as $key => $value) {
        _fputcsv($fp, $value, $key, $fdb2csv1, $define, $headerCnt, $headerFlg, array());
    }
    //ファイルのコーピ
    //    makeCopyFile($filepath, $csvname, $mailAddLst, $nowYmd, $nowTime, '.csv');
    if ($xlsCustomFlg !== '') {
        foreach ($mailAddLst as $key => $value) {
            if (cmMer($value['WAQUNM']) !== '' && cmMer($value['WAQUNM']) !== null) {
                $WAQUNM = replaceFilename(cmMer($value['WAQUNM']));
                $tofilename = $WAQUNM . '_' . $nowYmd . '_' . $nowTime . '.' . $cExt;
                //e_log('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                shell_exec('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
            } else {
                $tofilename = $d1name . '_' . $nowYmd . '_' . $nowTime . '.' . $cExt;
                //e_log('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                shell_exec('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
            }
        }
        //e_log('カスタム:' . ETEMP_DIR . $templateFn . 'go' . $templatecfn);
        
    }
}
/*
 *-------------------------------------------------------*
 * HTML作成処理
 *-------------------------------------------------------*
*/
function exeHtmlMail($csv_h, $csv_d, $define, $htmlname, $WEBF, $idx, $lastRow, $count) {
    //e_log('header arr'.print_r($csv_h,true));
    $htmlname = $htmlname . 'html';
    $columns = $csv_h;
    $breakFlg = false;
    $data = $csv_d;
    if ($idx === 0) {
        $tbdata = '';
        $tbdata.= '<html>';
        $tbdata.= '<head>';
        $tbdata.= '<style type ="text/css">';
        $tbdata.= '.ii a[href^="tel:"] { color : re }';
        $tbdata.= '</style>';
        $tbdata.= '</head>';
        $tbdata.= '<body>';
        $tbdata.= '<table style ="font-size : 12px;width : 100%;border-collapse: collapse;white-space : nowrap !important;margin-top:10px;">';
        $tbdata.= '<thead style ="background-color : rgb(79,129,189);color:#ffffff;">';
        $tbdata.= '<tr>';
        foreach ($columns as $key => $value) {
            $pData = str_replace(" ", "&#32;", cmMer(htmlspecialchars($value)));
            $pData = mb_convert_encoding(($pData), 'HTML-ENTITIES', 'UTF-8');
            $tbdata.= '<th nowrap style ="border:1px solid #ccc;padding : 7px 10px 7px 10px;">' . $pData . '</th>';
        }
        $tbdata.= '</tr>';
        $tbdata.= '</thead><tbody>';
    }
    foreach ($data as $key) {
        $count++;
        $tbdata.= '<tr ' . (($count % 2 === 0) ? 'style ="background-color : rgb(250,250,250);"' : '') . '>';
        foreach ($key as $k => $value) {
            $type = $define[$k]['D2TYPE'];
            $filid = $define[$k]['D2FILID'];
            $dec = $define[$k]['D2DEC'];
            $wedt = $define[$k]['D2WEDT'];
            $align = '';
            if ($type === 'S' || $type === 'P' || $type === 'B') {
                $align = 'text-align : right;';
                $value = floatFormat($value, $dec, 2);
            }
            $value = cmMer($value);
            $isNum = checkNum($value);
            if ($isNum == true) {
                //結果フィールドの場合は指定少数桁数で切り捨て
                //                if ($filid === '9999' || $WEBF === '1') {
                //               }
                $value = back_front_numFlg($value);
                $value = henshuFormatSetting($value, $wedt, $type, true, HTMDHENSHUU);
            }
            // 編集コードは「少数切捨て」の場合データのフォーマットセット
            /*   switch ($wedt) {
            case 'A':
            $value =  floatFormat($value, 0, 2);
            break;
            default:
            $value =  $value;
            break;
            }*/
            //$tbdata .= '<td nowrap style="border:1px solid #ccc;padding : 5px 10px 4px 10px;text-decoration: none;' . $align . '">' . preg_replace('/\s/', '&nbsp;', cmMer(htmlspecialchars($value))) . '</td>';
            $pData = str_replace(" ", "&#32;", cmMer(htmlspecialchars($value)));
            $pData = mb_convert_encoding(($pData), 'HTML-ENTITIES', 'UTF-8');
            $tbdata.= '<td nowrap style="border:1px solid #ccc;padding : 5px 10px 4px 10px;text-decoration: none;' . $align . '">' . $pData . '</td>';
        }
        $tbdata.= '</tr>';
        if ($count === (int)HTMLMAXROW) {
            $breakFlg = true;
            break;
        }
    }
    if ($lastRow === true || $breakFlg === true) {
        $tbdata.= '</tbody></table>';
        $tbdata.= '</body></html>';
    }
    $rtnTbdata = array();
    $rtnTbdata['HTMLCREATEBREAK'] = $breakFlg;
    $rtnTbdata['QRYHTMLCOUNT'] = $count;
    $rtnTbdata['TBDATA'] = $tbdata;
    return $rtnTbdata;
}
/*
 *-------------------------------------------------------*
 * メール送信準備
 * $paramQGDataはクエリーグループのため必要なデータ
 *-------------------------------------------------------*
*/
function exeMail($db2con, $d1name, $mailAddLst, $filename, $dbname, $nowYmd, $nowTime, $password, $csvFlg, $xlsFlg, $htmlFlg, $tbdata, $wspkey, $mailFlg, $xlsCustomFlg = '', $templateFlg = true, $paramQGData = array()) {
    $excelext = '';
    $DB2WAUT = getDB2WAUT($db2con, $d1name, $wspkey);
    $isQGflg = (count($paramQGData) > 0) ? $paramQGData['ISQGFLG'] : false;
    if ($DB2WAUT['result'] === true) {
        $DB2WMAL = getDB2WMAL($db2con);
        if ($DB2WMAL['result'] === true) {
            $DB2WMAL = $DB2WMAL['data'];
            e_log('fnUpdateDB2WHIS count => ' . count($DB2WAUT));
            if (count($DB2WAUT) == 0) {
                $DB2WHIS = fnUpdateDB2WHIS($db2con, $d1name, '', '', $nowYmd, $nowTime, $password, $dbname, '1', '1', '1', $wspkey, $isQGflg);
                e_log('fnUpdateDB2WHIS => ' . $wspkey . ' , ' . $isQGflg);
                if ($DB2WHIS['result'] !== true) {
                    $arr = array($d1name, '', $nowYmd, $nowTime, $password, $dbname, '1', '1', '1', $wspkey);
                    //                    e_log("exeMail db2whis 1" . print_R($arr, true), '1');
                    e_log(showMsg($DB2WHIS['result'], array('exeMail.php', 'fnUpdateDB2WHIS')), '1');
                }
            }
            //  $mailAddLst = umEx($mailAddLst,true);
            foreach ($mailAddLst as $key => $value) {
                $whcsvf = '';
                $whxlsf = '';
                $whhtmf = '';
                $usrfilename = '';
                if ($value['WACSV'] == '1') {
                    $whcsvf = '1';
                };
                if ($value['WAXLS'] == '1' && $templateFlg === true) {
                    $whxlsf = '1';
                };
                if ($value['WAHTML'] == '1') {
                    $whhtmf = '1';
                };
                //if($value['WAHTML'] == '1'){$whhtmlf = '1';};
                $tmppassword = $password;
                if (cmMer($value['WASNKB']) !== '') {
                    $tmppassword = '';
                }
                //TODO:DB2WHIS UPDATE HTMLFLG
                $DB2WHIS = fnUpdateDB2WHIS($db2con, $d1name, $value['WAMAIL'], $value['WAMACC'], $nowYmd, $nowTime, $tmppassword, $dbname, $whcsvf, $whxlsf, $whhtmf, $wspkey, $isQGflg);
                if ($DB2WHIS['result'] !== true) {
                    $arr = array($d1name, $value['WAMAIL'], $nowYmd, $nowTime, $tmppassword, $dbname, $whcsvf, $whxlsf, $whhtmf, $wspkey);
                    e_log(showMsg($DB2WHIS['result'], array('exeMail.php', 'fnUpdateDB2WHIS')), '1');
                } else {
                    if ($mailFlg !== '0') {
                        $usfile = false;
                        if (cmMer($value['WAQUNM']) !== '' && cmMer($value['WAQUNM']) !== null) {
                            $usfile = true;
                            $WAQUNM = replaceFilename(cmMer($value['WAQUNM']));
                            $usrfilename = $WAQUNM . '_' . $nowYmd . '_' . $nowTime;
                        } else {
                            $usrfilename = $filename;
                        }
                        sendMail($db2con, $value, @$DB2WMAL[0], $d1name, $usrfilename, $dbname, $nowYmd, $nowTime, $tmppassword, $tbdata, $wspkey, $xlsCustomFlg, $paramQGData);
                        if (cmMer($wspkey) !== '' && cmMer($wspkey) !== null) {
                            $excelext = '.xls';
                        } else {
                            //zipファイル削除
                            if ($isQG) $ext = getExt($d1name);
                            if ($xlsCustomFlg === '1') {
                                $excelext = '.' . $ext;
                            } else if ((EXCELVERSION == '2003' && $ext === '') || $ext === 'xls') {
                                $excelext = '.xls';
                            } else if (((EXCELVERSION == '2007' || EXCELVERSION == 'XLSX') && $ext === '') || $ext === 'xlsx') {
                                $excelext = '.xlsx';
                            } else if (EXCELVERSION == 'html' || $ext === '') {
                                $excelext = '.xls';
                            }
                        }
                        if ($usfile === true) {
                            if (file_exists("csv/" . $usrfilename . '.csv')) {
                                @unlink("csv/" . $usrfilename . '.csv');
                            }
                            if (file_exists("xls/" . $usrfilename . $excelext)) {
                                @unlink("xls/" . $usrfilename . $excelext);
                            }
                            if (file_exists("xls/" . $usrfilename . '.csv')) {
                                @unlink("xls/" . $usrfilename . '.csv');
                            }
                            if (file_exists("html/" . $usrfilename . '.html')) {
                                @unlink("html/" . $usrfilename . '.html');
                            }
                            if (file_exists("tmp_zip/" . $usrfilename . '.csv.zip')) {
                                @unlink("tmp_zip/" . $usrfilename . '.csv.zip');
                            }
                            if (file_exists("tmp_zip/" . $usrfilename . '.zip')) {
                                @unlink("tmp_zip/" . $usrfilename . '.zip');
                            }
                            if (file_exists("tmp_zip/" . $usrfilename . $excelext . '.zip')) {
                                @unlink("tmp_zip/" . $usrfilename . $excelext . '.zip');
                            }
                            if (file_exists("tmp_zip/" . $usrfilename . '.html.zip')) {
                                @unlink("tmp_zip/" . $usrfilename . '.html.zip');
                            }
                            if (file_exists("tmp_passzip/" . $usrfilename . '.csv.zip')) {
                                @unlink("tmp_passzip/" . $usrfilename . '.csv.zip');
                            }
                            if (file_exists("tmp_passzip/" . $usrfilename . '.zip')) {
                                @unlink("tmp_passzip/" . $usrfilename . '.zip');
                            }
                            if (file_exists("tmp_passzip/" . $usrfilename . $excelext . '.zip')) {
                                @unlink("tmp_passzip/" . $usrfilename . $excelext . '.zip');
                            }
                            if (file_exists("tmp_passzip/" . $usrfilename . '.html.zip')) {
                                @unlink("tmp_passzip/" . $usrfilename . '.html.zip');
                            }
                        }
                    }
                }
            }
            if (file_exists("csv/" . $filename . '.csv')) {
                @unlink("csv/" . $filename . '.csv');
            }
            if (file_exists("xls/" . $filename . $excelext)) {
                @unlink("xls/" . $filename . $excelext);
            }
            if (file_exists("xls/" . $usrfilename . '.csv')) {
                @unlink("xls/" . $usrfilename . '.csv');
            }
            if (file_exists("html/" . $filename . '.html')) {
                @unlink("html/" . $filename . '.html');
            }
            if (file_exists("tmp_zip/" . $filename . '.csv.zip')) {
                @unlink("tmp_zip/" . $filename . '.csv.zip');
            }
            if (file_exists("tmp_zip/" . $usrfilename . '.zip')) {
                @unlink("tmp_zip/" . $usrfilename . '.zip');
            }
            if (file_exists("tmp_zip/" . $filename . $excelext . '.zip')) {
                @unlink("tmp_zip/" . $filename . $excelext . '.zip');
            }
            if (file_exists("tmp_zip/" . $filename . '.html.zip')) {
                @unlink("tmp_zip/" . $filename . '.html.zip');
            }
            if (file_exists("tmp_passzip/" . $filename . '.csv.zip')) {
                @unlink("tmp_passzip/" . $filename . '.csv.zip');
            }
            if (file_exists("tmp_passzip/" . $usrfilename . '.zip')) {
                @unlink("tmp_passzip/" . $usrfilename . '.zip');
            }
            if (file_exists("tmp_passzip/" . $filename . $excelext . '.zip')) {
                @unlink("tmp_passzip/" . $filename . $excelext . '.zip');
            }
            if (file_exists("tmp_passzip/" . $filename . '.html.zip')) {
                @unlink("tmp_passzip/" . $filename . '.html.zip');
            }
        } else {
            e_log(showMsg($DB2WMAL['result'], array('exeMail.php', 'getDB2WMAL')), '1');
            //クエリー　＊メールサーバー情報の取得に失敗しました。
            $errMsg = "メールサーバー情報の取得に失敗しました。";
            fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, $wspkey, $errMsg, array(), $isQGflg);
        }
    } else {
        e_log(showMsg($DB2WAUT['result'], array('exeMail.php', 'getDB2WAUT')), '1');
        //クエリー　配信情報の取得に失敗しました。
        $errMsg = showMsg('FAIL_FUNC', array('配信情報の取得')); //"配信情報の取得に失敗しました。";
        fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, $wspkey, $errMsg, array(), $isQGflg);
    }
}
/*
 *-------------------------------------------------------*
 * ファイルをZIP化
 *-------------------------------------------------------*
*/
function exeZip($d1name, $filename, $password, $csvFlg, $xlsFlg, $htmlFlg, $mailAddLst, $nowYmd, $nowTime, $wspkey, $xlsCustomFlg = '', $ISEXCELMAXLEN) {
    e_log('かステムテンプレート：' . $xlsCustomFlg);
    $userfilename = '';
    foreach ($mailAddLst as $key => $value) {
        if (cmMer($value['WAQUNM']) !== '' && cmMer($value['WAQUNM']) !== null) {
            $WAQUNM = replaceFilename(cmMer($value['WAQUNM']));
            $userfilename = $WAQUNM . '_' . $nowYmd . '_' . $nowTime;
        } else {
            $userfilename = $filename;
        }
        if ($csvFlg === '1') {
            fnFileZip($userfilename . '.csv', 'csv', $password);
            fnFileZipNoPass($userfilename . '.csv', 'csv');
        }
        if ($xlsFlg === '1') {
            if (cmMer($wspkey) !== '') {
                $excelext = '.xls';
            } else {
                $excelext = '';
                $ext = getExt($d1name);
                if ($xlsCustomFlg !== '') {
                    $excelext = '.' . $ext;
                } else if ((EXCELVERSION == '2003' && $ext === '') || $ext === 'xls') {
                    $excelext = '.xls';
                } else if (((EXCELVERSION == '2007' || EXCELVERSION == 'XLSX') && $ext === '') || $ext === 'xlsx') {
                    $excelext = '.xlsx';
                } else if (EXCELVERSION == 'html' || $ext === '') {
                    $excelext = '.xls';
                }
                /*  if ($ext == '') {
                $excelext = '.xls';
                }*/
            }
            if ($xlsCustomFlg === '') {
                if ($ISEXCELMAXLEN === 0) { //MAX件数のためカウントのチェック
                    fnFileZipNoPass($userfilename . $excelext, 'xls', $xlsCustomFlg);
                    fnFileZip($userfilename . $excelext, 'xls', $password, $xlsCustomFlg);
                }
            } else {
                fnFileZipNoPass($userfilename, 'xls', $xlsCustomFlg, $excelext);
                fnFileZip($userfilename, 'xls', $password, $xlsCustomFlg, $excelext);
            }
        }
        if ($htmlFlg === '1') {
            fnFileZip($userfilename . '.html', 'html', $password);
            fnFileZipNoPass($userfilename . '.html', 'html');
        }
    }
}
/*
 *-------------------------------------------------------*
 * メール送信
 *-------------------------------------------------------*
*/
function sendMail($db2con, $waut, $wmal, $d1name, $filename, $dbname, $nowYmd, $nowTime, $password, $tbdata, $wspkey, $xlsCustomFlg = '', $paramQGData) {
    e_log('メール送信：' . print_r($waut, true));
    foreach ($waut as $key => $value) {
        $waut[$key] = cmMer($value);
    }
    foreach ($wmal as $key => $value) {
        $wmal[$key] = cmMer($value);
    }
    $address = $waut['WAMAIL'];
    $ccAddress = $waut['WAMACC'];
    $watype = $waut['WATYPE'];
    $fontsize = $waut['WAFONT'];
    $isQGflg = (count($paramQGData) > 0) ? $paramQGData['ISQGFLG'] : false;
    //メール送信処理
    mb_language('japanese');
    mb_internal_encoding('UTF-8');
    //インスタンス生成
    $mail = new PHPMailer();
    $mail->CharSet = 'iso-2022-jp';
    $mail->Encoding = '7bit';
    /*
    $wmal['WMHOST'] = '74.125.203.108';
    $wmal['WMPORT'] = 465;
    $wmal['WMUSER'] = 'mailerphpquery@gmail.com';
    $wmal['WMPASE'] = 'mailerphpquery00';
    */
    //SMTP接続
    $mail->IsSMTP();
    //ユーザーが入力されている時のみ、SMTP認証をする
    if (cmMer($wmal['WMUSER']) !== '') {
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = 'ssl';
    }
    $mail->Host = cmMer($wmal['WMHOST']);
    $mail->Port = cmMer($wmal['WMPORT']);
    if ($watype === '1') {
        $mail->IsHTML(true);
    }
    $mail->Username = cmMer($wmal['WMUSER']); //Gmailのアカウント名
    $mail->Password = cmMer($wmal['WMPASE']); //Gmailのパスワード
    $from = (($waut['WASFLG'] == '1') ? $waut['WAFRAD'] : $wmal['WMFRAD']);
    $mail->From = cmMer($from); //差出人(From)をセット
    $fromname = (($waut['WASFLG'] == '1') ? $waut['WAFRNM'] : $wmal['WMFRNM']);
    //ピボットのエクセルEXTENSION
    if (cmMer($wspkey) !== '') {
        $excelext = '.xls';
    } else {
        //添付ファイル取得
        $excelext = '';
        $ext = getExt($d1name);
        if ($xlsCustomFlg !== '') {
            $excelext = '';
        } else if ((EXCELVERSION == '2003' && $ext === '') || $ext === 'xls') {
            $excelext = '.xls';
        } else if (((EXCELVERSION == '2007' || EXCELVERSION == 'XLSX') && $ext === '') || $ext === 'xlsx') {
            $excelext = '.xlsx';
        } else if (EXCELVERSION == 'html' || $ext === '') {
            $excelext = '.xls';
        }
    }
    if (cmMer($waut['WASNKB']) === '') {
        $attachfile1 = "tmp_passzip/" . $filename . '.csv.zip'; //添付ファイルパス
        if ($xlsCustomFlg === '') {
            $attachfile2 = "tmp_passzip/" . $filename . $excelext . '.zip'; //添付ファイルパス
            
        } else {
            $attachfile2 = "tmp_passzip/" . $filename . $excelext . '.zip'; //添付ファイルパス
            
        }
        // e_log('xls2' . $attachfile2);
        $attachfile3 = "tmp_passzip/" . $filename . '.html.zip'; //添付ファイルパス
        
    } else if ($waut['WASNKB'] == '1') {
        $attachfile1 = "tmp_zip/" . $filename . '.csv.zip'; //添付ファイルパス
        if ($xlsCustomFlg === '') {
            $attachfile2 = "tmp_zip/" . $filename . $excelext . '.zip'; //添付ファイルパス
            
        } else {
            $attachfile2 = "tmp_zip/" . $filename . $excelext . '.zip'; //添付ファイルパス
            
        }
        $attachfile3 = "tmp_zip/" . $filename . '.html.zip'; //添付ファイルパス
        // e_log('xls1' . $attachfile2);
        
    } else if ($waut['WASNKB'] == '2') {
        $attachfile1 = "csv/" . $filename . '.csv'; //添付ファイルパス
        if ($xlsCustomFlg === '') {
            $attachfile2 = "xls/" . $filename . $excelext; //添付ファイルパス
            e_log("attachfile2" . $attachfile2);
        } else {
            $attachfile2 = "tmp_zip/" . $filename . $excelext . '.zip'; //添付ファイルパス
            
        }
        e_log('xlsaa' . $attachfile2);
        $attachfile3 = "html/" . $filename . '.html'; //添付ファイルパス
        
    }
    $mail->FromName = mb_encode_mimeheader(cmMer($fromname), 'JIS'); //差出人(From名)をセット
    $subject = ($waut['WASFLG'] == '1') ? $waut['WASUBJ'] : $wmal['WMSUBJ'];
    $mail->Subject = mb_encode_mimeheader($subject, 'JIS'); //件名(Subject)をセット
    /**********************************************************/
    $mailarr = explode(";", $address);
    foreach ($mailarr as $key => $value) {
        $data = chkDB2MAGP($db2con, $value);
        if ($data['result'] === true) {
            if (count($data['data']) > 0) {
                foreach ($data['data'] as $key => $value) {
                    $mail->AddAddress(cmMer($value['MAMAIL'])); //宛先
                    
                }
            } else {
                $mail->AddAddress(cmMer($value)); //宛先
                
            }
        } else {
            e_log(showMsg($data['result'], array('exeMail.php', 'chkDB2MAGP')), '1');
        }
    }
    /**********************************************************/
    // CC アドレスのセット
    /**********************************************************/
    if ($ccAddress !== null && $ccAddress !== '') {
        $ccmailarr = explode(";", $ccAddress);
        foreach ($ccmailarr as $key => $value) {
            $data = chkDB2MAGP($db2con, $value);
            if ($data['result'] === true) {
                if (count($data['data']) > 0) {
                    foreach ($data['data'] as $key => $value) {
                        $mail->AddCC(cmMer($value['MAMAIL'])); //宛先
                        
                    }
                } else {
                    $mail->AddCC(cmMer($value)); //宛先
                    
                }
            } else {
                e_log(showMsg($data['result'], array('exeMail.php', 'chkDB2MAGP')), '1');
            }
        }
    }
    /**********************************************************/
    //body作成
    $mbody = ($waut['WASFLG'] == '1') ? $waut['WABODY'] : $wmal['WMBODY'];
    //MSM add for query link mail sent 20180906
    $mlink = "";

    $clink   = '';
    //e_log('GET_IP SENDMAILIP ###'.SENDMAILIP);

    $clink = PROTOCOL.'://'.SENDMAILIP.'/'.SISTEM.'/'."#query/".$waut['WANAME'];    
    if($waut['WAPIV'] === "1"){//chk for PIVOT or not
        if($waut['WAPKEY'] !== ""){
            if($waut['WAQGFLG'] === "1"){//chk for PIV GROUP or not
                $mlink = $clink."/pivotresultpgroup/".$waut['WAPKEY'];
            }else if($waut['WAQGFLG'] === "0"){
                $mlink = $clink."/pivotresult/".$waut['WAPKEY'];
            }    
        }
    }
    if ($waut['WANGFLG'] === "0") {//for Normal 通常 link 
        if($waut['WAQGFLG'] === "1"){
            $mlink = $clink."/resultgridqgroup";// graphresultggroup
        }else if($waut['WAQGFLG'] === "0"){
            $mlink = $clink."/resulthtml";
        }
    }
    if($waut['WANGFLG'] === "1"){//for Graph グラフ　link
        if($waut['WAGPID'] !== ""){
            $gpid = "";
            $gpid = explode(",", $waut['WAGPID']);

            for($i=0;$i< count($gpid);$i++){
                if($waut['WAQGFLG'] === "1"){// graph group
                    $mlink .= $clink."/graphresultggroup/".$gpid[$i]."\n";
                }else{
                    $mlink .= $clink."/graphresult/".$gpid[$i]."\n";
                }  
            }                
        }/*else{
            $rs = cmGetLastQueryGroup($db2con,$waut['WANAME']);
            error_log("cmGetQueryGroup result 20180919=> ".print_r($rs,true));
            if($rs['result'] !== true){
                $rtn = 2;
                $msg = showMsg($rs['result'],array('クエリー'));
            }else{
                $D1NAMELIST=umEx($rs['data'],true);
            }
            if($D1NAMELIST != ""){
                foreach($D1NAMELIST as $key => $res){
                        $LASTD1NAME = $res['D1NAME'];
                }
                
                $graphdata = "";
                error_log("LASTD1NAME 20180919 => ".print_r($LASTD1NAME,true)); 
                $graphdata = cmGetDB2GPK($db2con,$LASTD1NAME); 
                if($graphdata['result'] !== true){
                    $rtn = 2;
                    $msg = showMsg($graphdata['result'],array('クエリー'));
                }else{
                    $getgraphid = umEx($graphdata['data'],true);
                }
                error_log("getgraphid 20180919 => ".print_r($getgraphid,true)); 
                if($getgraphid != ""){
                    foreach($getgraphid as $key => $gpid){
                        $graphid = $gpid['GPKID'];
                    }
                    error_log("GraphID 20180919 => ".print_r($graphid,true)); 
                    $mlink .= $clink."/graphresultggroup/".$graphid; 
                }else{
                    $mlink .= "NOTEXIST_GRAPHGROUP_LINK";
                } 
            } 
             //$mlink .= $clink."/graphresultggroup/".$LASTD1NAME; 
        }*/     
    }
    //MSM end 
    // $mbody .= $mbody;
    if ($watype === '1') {
        $mbody = str_replace("\n", "<br/>", $mbody);
        $mbody.= "\n";
        $fontsize = $waut['WAFONT'];
        if ((int)$fontsize === 0) {
            $fontsize = "12";
        }
        e_log("<br/>fontsize " . $fontsize);
        $tbdata = str_replace('table style ="font-size : 12px;', 'table style ="font-size : ' . $fontsize . 'px;', $tbdata);
        //$aaa = "this is test!!";
        $mbody.= $tbdata;
        //$mbody.= $aaa;
    }
    //MSM add
    $mbody.= "\n";
    $mbody.= $mlink;

    $mail->Body  = $mbody;
    $mail->Body = mb_convert_encoding($mbody, 'ISO-2022-JP-MS'); //本文(Body)をセット

    if ($waut['WACSV'] == '1') {
        $mail->AddAttachment($attachfile1);
    }
    if ($waut['WAXLS'] == '1') {
        $mail->AddAttachment($attachfile2);
    }
    if ($waut['WAHTML'] == '1') {
        $mail->AddAttachment($attachfile3);
    }
    $whzpas = $password;
    $mail->SMTPOptions = array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));
    //メール送信
    $mailrs = $mail->Send();
    //メール送信が成功したらパスワードを送信(SNKBが空の場合のみ)
    if ($mailrs) {
        e_log('メール送信が成功したらパスワードを送信(SNKBが空の場合のみ)');
        if ($mailrs && (cmMer($waut['WASNKB']) === '') && ($waut['WACSV'] == '1' || $waut['WAXLS'] == '1' || $waut['WAHTML'] == '1')) {
            e_log('REACH IT');
            //インスタンス生成
            $passmail = new PHPMailer();
            $passmail->CharSet = 'iso-2022-jp';
            $passmail->Encoding = '7bit';
            //SMTP接続
            $passmail->IsSMTP();
            //ユーザーが入力されている時のみ、SMTP認証をする
            if (cmMer($wmal['WMUSER']) !== '') {
                $passmail->SMTPAuth = TRUE;
                $passmail->SMTPSecure = 'ssl';
            }
            $passmail->Host = cmMer($wmal['WMHOST']);
            $passmail->Port = cmMer($wmal['WMPORT']);
            $passmail->Username = cmMer($wmal['WMUSER']); //Gmailのアカウント名
            $passmail->Password = cmMer($wmal['WMPASE']); //Gmailのパスワード
            $fromaddress = ($waut['WASFLG'] == '1') ? $waut['WAFRAD'] : $wmal['WMFRAD']; //差出人(From)をセット
            $fromaddress = cmMer($fromaddress);
            $passmail->From = $fromaddress; //差出人(From)をセット
            $fromname = ($waut['WASFLG'] == '1') ? $waut['WAFRNM'] : $wmal['WMFRNM'];
            $passmail->FromName = mb_encode_mimeheader(cmMer($fromname), 'JIS'); //差出人(From名)をセット
            $subject = ($waut['WASFLG'] == '1') ? $waut['WASUBJ'] : $wmal['WMSUBJ'];
            $passmail->Subject = mb_encode_mimeheader(cmMer($subject) . '<PASS>', 'JIS'); //件名(Subject)をセット
            //        $passmail->AddAddress($address); //宛先
            /**********************************************************/
            foreach ($mailarr as $key => $value) {
                $data = chkDB2MAGP($db2con, $value);
                if ($data['result'] === true) {
                    if (count($data['data']) > 0) {
                        foreach ($data['data'] as $key => $value) {
                            $passmail->AddAddress($value['MAMAIL']); //宛先
                            
                        }
                    } else {
                        $passmail->AddAddress($value); //宛先
                        
                    }
                } else {
                    e_log(showMsg($data['result'], array('exeMail.php', 'chkDB2MAGP')), '1');
                }
            }
            /**********************************************************/
            // passCC アドレスのセット
            /**********************************************************/
            if ($ccAddress !== null && $ccAddress !== '') {
                foreach ($ccmailarr as $key => $value) {
                    $data = chkDB2MAGP($db2con, $value);
                    if ($data['result'] === true) {
                        if (count($data['data']) > 0) {
                            foreach ($data['data'] as $key => $value) {
                                $passmail->AddCC(cmMer($value['MAMAIL'])); //宛先
                                
                            }
                        } else {
                            $passmail->AddCC(cmMer($value)); //宛先
                            
                        }
                    } else {
                        e_log(showMsg($data['result'], array('exeMail.php', 'chkDB2MAGP')), '1');
                    }
                }
            }
            /**********************************************************/
            //body作成
            $body = "下記の送信済みメールの添付ファイルは、パスワード付きZIPにて暗号化されています。\n";
            $body.= "このファイルの開封パスワードをお送り致します。\n\n";
            $body.= "パスワード：" . $whzpas . "\n\n";
            $body.= "送信済みメール\n";
            $body.= "----------------------------------------------------------\n";
            $body.= "From:       " . $fromaddress . "\n";
            $body.= "Subject:    " . $subject . "\n";
            $body.= "Date:       " . date('D, d M Y H:i:s') . "\n";
            $body.= "----------------------------------------------------------";
            $passmail->Body = mb_convert_encoding($body, 'JIS', 'UTF-8'); //本文(Body)をセット
            $passmail->SMTPOptions = array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));
            if ($passmail->Send()) {
            } else {
                //個人　＊パスワードメールの送信に失敗しました。
                //エラー内容　$mail->ErrorInfo
                //メールサーバー情報　SMTP POST・・・
                $errMsg = "パスワードメールの送信に失敗しました。<br/>送信エラー内容：" . $mail->ErrorInfo;
                $mltb = '';
                $mltb.= '<h3>メールサーバー情報</h3><hr>';
                $mltb.= '<br/>SMTPサーバー : ' . cmMer($wmal["WMHOST"]);
                $mltb.= '<br/>ポート : ' . cmMer($wmal["WMPORT"]);
                $mltb.= '<br/>ユーザー : ' . cmMer($wmal["WMUSER"]);
                $mltb.= '<br/>パスワード : ' . str_repeat("●", strlen(cmMer($wmal["WMPASE"])));
                $mltb.= '<br/>差出人アドレス : ' . $from;
                $mltb.= '<br/>差出人名 : ' . $fromname;
                $errMsg.= "<br/>" . $mltb;
                foreach ($mailarr as $key => $value) {
                    $DB2WHIS = fnUpdateDB2WHISByID($db2con, $d1name, $whpkey, cmMer($value), $nowYmd, $nowTime, $errMsg, $isQGflg);
                }
            }
        }
    } else {
        e_log($mail->ErrorInfo, '1');
        //個人　＊メールの送信に失敗しました。
        //エラー内容　$mail->ErrorInfo
        //メールサーバー情報　SMTP POST・・・
        $errMsg = "メールの送信に失敗しました。<br/>送信エラー内容：" . $mail->ErrorInfo;
        $mltb = '';
        $mltb.= '<h3>メールサーバー情報</h3><hr>';
        $mltb.= '<br/>SMTPサーバー : ' . cmMer($wmal["WMHOST"]);
        $mltb.= '<br/>ポート : ' . cmMer($wmal["WMPORT"]);
        $mltb.= '<br/>ユーザー : ' . cmMer($wmal["WMUSER"]);
        $mltb.= '<br/>パスワード : ' . str_repeat("●", strlen(cmMer($wmal["WMPASE"])));
        $mltb.= '<br/>差出人アドレス : ' . $from;
        $mltb.= '<br/>差出人名 : ' . $fromname;
        $errMsg.= "<br/>" . $mltb;
        foreach ($mailarr as $key => $value) {
            $DB2WHIS = fnUpdateDB2WHISByID($db2con, $d1name, $whpkey, cmMer($value), $nowYmd, $nowTime, $errMsg, $isQGflg);
        }
    }
}
/*
 *-------------------------------------------------------*
 * スケジュールマスター取得(直接実行)
 *-------------------------------------------------------*
*/
function getDB2WSCD($db2con, $pWsname, $pWspkey) {
    $data = array();
    $rtn = array();
    $params = array($pWsname);
    $strSQL = ' SELECT ';
    $strSQL.= getDb2wscdColumns();
    $strSQL.= ' FROM DB2WSCD AS A ';
    $strSQL.= ' WHERE WSNAME = ? ';
    if ($pWspkey !== '') {
        $strSQL.= ' AND WSPKEY = ? ';
        array_push($params, $pWspkey);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * 定義に対して、その中にいるユーザーが一人でもあるCSVとHTMLとXLSのフラグ取得
 *-------------------------------------------------------*
*/
function getDB2WAUTFilFlg($db2con, $waname, $wapkey = '') {
    $rtn = array();
    $data = array();
    $strSQL.= '    SELECT ';
    $strSQL.= '        MAX(A.WACSV) AS WACSV, ';
    $strSQL.= '        MAX(A.WAXLS) AS WAXLS, ';
    $strSQL.= '        MAX(A.WAHTML) AS WAHTML, ';
    $strSQL.= '        CASE WHEN MAX(A.WATYPE) = \'1\' THEN MAX(A.WATYPE) ';
    $strSQL.= '        ELSE \'\' END AS WATYPE ';
    $strSQL.= '    FROM DB2WAUT A ';
    $strSQL.= '    WHERE ';
    $strSQL.= '        A.WANAME = ? ';
    $strSQL.= '    AND A.WAPKEY = ? ';
    $strSQL.= '    AND A.WASPND <> \'1\' ';
    $params = array($waname, $wapkey);
    e_log("MSM getDB2WAUTFilFlg => " . $strSQL . print_r($params, true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            if (count($data) > 0) {
                $data = umEx($data) [0];
            }
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * 定義に対して、その中にいるユーザーが一人でも
 * CSVにチェックがついているかをチェック
 *-------------------------------------------------------*
*/
function chkDB2WAUTCsv($db2con, $pName) {
    $rtn = array();
    $data = array();
    $flg = '0';
    $strSQL = ' SELECT ';
    $strSQL.= getDb2wautColumns();
    $strSQL.= ' FROM DB2WAUT AS A ';
    $strSQL.= ' WHERE WANAME = ? ';
    $strSQL.= ' AND WASPND <> \'1\' ';
    $strSQL.= ' AND WACSV = \'1\' ';
    $params = array($pName);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            //一人でもいたらtrueを返す
            if (count($data) > 0) {
                $flg = '1';
            }
            $rtn = array('result' => true, 'flg' => $flg);
        }
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * 定義に対して、その中にいるユーザーが一人でも
 * EXCELにチェックがついているかをチェック
 *-------------------------------------------------------*
*/
function chkDB2WAUTXls($db2con, $pName, $wspkey) {
    $rtn = array();
    $data = array();
    $flg = '0';
    $strSQL = ' SELECT ';
    $strSQL.= getDb2wautColumns();
    $strSQL.= ' FROM DB2WAUT AS A ';
    $strSQL.= ' WHERE WANAME = ? ';
    $strSQL.= ' AND WASPND <> \'1\' ';
    $strSQL.= ' AND WAPKEY = ? ';
    $strSQL.= ' AND WAXLS = \'1\' ';
    $params = array($pName, $wspkey);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            //一人でもいたらtrueを返す
            if (count($data) > 0) {
                $flg = '1';
            }
            $rtn = array('result' => true, 'flg' => $flg);
        }
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * 定義に対して、その中にいるユーザーが一人でも
 * HTMLにチェックがついているかをチェック
 *-------------------------------------------------------*
*/
function chkDB2WAUTHtml($db2con, $pName, $wspkey) {
    $rtn = array();
    $data = array();
    $flg = '0';
    $strSQL = ' SELECT ';
    $strSQL.= getDb2wautColumns();
    $strSQL.= ' FROM DB2WAUT AS A ';
    $strSQL.= ' WHERE WANAME = ? ';
    $strSQL.= ' AND WASPND <> \'1\' ';
    $strSQL.= ' AND WAPKEY = ? ';
    $strSQL.= ' AND WAHTML = \'1\' ';
    $params = array($pName, $wspkey);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            //一人でもいたらtrueを返す
            if (count($data) > 0) {
                $flg = '1';
            }
            $rtn = array('result' => true, 'flg' => $flg);
        }
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * 定義に対して、その中にいるユーザーが一人でも
 * HTMLにチェックがついているかをチェック
 *-------------------------------------------------------*
*/
function chkDB2WATYPEHtml($db2con, $pName, $wspkey) {
    $rtn = array();
    $data = array();
    $flg = '0';
    $strSQL = ' SELECT ';
    $strSQL.= getDb2wautColumns();
    $strSQL.= ' FROM DB2WAUT AS A ';
    $strSQL.= ' WHERE WANAME = ? ';
    $strSQL.= ' AND WAPKEY = ? ';
    $strSQL.= ' AND WASPND <> \'1\' ';
    $strSQL.= ' AND WATYPE = \'1\' ';
    $params = array($pName, $wspkey);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            //一人でもいたらtrueを返す
            if (count($data) > 0) {
                $flg = '1';
            }
            $rtn = array('result' => true, 'data' => $data, 'flg' => $flg);
        }
    }
    return $rtn;
}
//0件メール配信チェック
function chkDB2WSOC($db2con, $d1name, $wspkey) {
    $data = array();
    $flg = '1';
    if (cmMer($wspkey) != '') {
        $strSQL = ' SELECT SO0MAL';
        $strSQL.= ' FROM DB2WSOC ';
        $strSQL.= ' WHERE SONAME = ? ';
        $strSQL.= ' AND SOPKEY = ? ';
        $params = array($d1name, $wspkey);
    } else {
        $strSQL = ' SELECT D10MAL';
        $strSQL.= ' FROM FDB2CSV1 ';
        $strSQL.= ' WHERE D1NAME = ? ';
        $params = array($d1name);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            if (count($data) > 0) {
                if (cmMer($wspkey) != '') {
                    $flg = $data[0]['SO0MAL'];
                } else {
                    $flg = $data[0]['D10MAL'];
                }
            }
            $rtn = array('result' => true, 'flg' => $flg);
        }
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * DB2WMAL取得カラム
 *-------------------------------------------------------*
*/
function getDb2wmalColumns($WMPASS) {
    $strwmpass = '';
    if ($WMPASS !== '') {
        $strwmpass.= ' DECRYPT_CHAR(A.WMPASE) AS WMPASE, ';
    } else {
        $strwmpass = 'A.WMPASS AS WMPASE,';
    }
    $strSQL = ' A.WMHOST, ';
    $strSQL.= ' A.WMPORT, ';
    $strSQL.= ' A.WMUSER, ';
    $strSQL.= $strwmpass;
    $strSQL.= ' A.WMFRAD, ';
    $strSQL.= ' A.WMFRNM, ';
    $strSQL.= ' A.WMSUBJ, ';
    $strSQL.= ' A.WMBODY ';
    return $strSQL;
}
/*
 *-------------------------------------------------------*
 * メール設定取得
 *-------------------------------------------------------*
*/
function getDB2WMAL($db2con) {
    $data = array();
    $rtn = array();
    $WMPASS = '';
    $rs = fnGetWMPASS($db2con);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array('パスワード'));
    } else {
        $WMPASS = cmMer($rs['WMPASS']);
    }
    if ($WMPASS !== '') {
        $sql = 'SET ENCRYPTION PASSWORD = \'' . RDB_KEY . '\'';
        db2_exec($db2con, $sql);
    }
    $strSQL = ' SELECT ';
    $strSQL.= getDb2wmalColumns($WMPASS);
    $strSQL.= ' FROM DB2WMAL AS A ';
    $params = array();
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}
/**
 *メールのパスワードを取得する
 *WAPASSを取得
 *
 */
function fnGetWMPASS($db2con) {
    $params = array();
    $WMPASS = array();
    $strSQL = ' SELECT A.WMPASS ';
    $strSQL.= ' FROM DB2WMAL AS A ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $WMPASS[] = $row;
            }
            $WMPASS = $WMPASS[0]['WMPASS'];
            $data = array('result' => true, 'WMPASS' => $WMPASS);
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * メールグループについているかをチェック
 *-------------------------------------------------------*
*/
function chkDB2MAGP($db2con, $pName) {
    $rtn = array();
    $data = array();
    $strSQL = ' SELECT *';
    $strSQL.= ' FROM DB2MAAD ';
    $strSQL.= ' WHERE MANAME = ? ';
    $params = array($pName);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}
/*
 *-------------------------------------------------------*
 * ファイル圧縮
 *-------------------------------------------------------*
*/
function fnFileZip($file, $extension, $password, $xlsCustomFlg = '', $ext = '') {
    if ($xlsCustomFlg === '') {
        shell_exec('7z a ' . '"' . 'tmp_passzip/' . $file . '.zip' . '"' . ' -p' . $password . ' ' . '"' . PHP_DIR . $extension . '/' . $file . '"');
    } else {
        //  e_log('7z a ' . '"' . 'tmp_passzip/' . $file . '.zip' . '"' . ' -p' . $password . ' ' . '"' . PHP_DIR . $extension . '/' . $file . $ext . '" ' . '"' . PHP_DIR . $extension . '/' . $file . '.csv"');
        shell_exec('7z a ' . '"' . 'tmp_passzip/' . $file . '.zip' . '"' . ' -p' . $password . ' ' . '"' . PHP_DIR . $extension . '/' . $file . $ext . '" ' . '"' . PHP_DIR . $extension . '/' . $file . '.csv"');
    }
}
/*
 *-------------------------------------------------------*
 * ファイル圧縮(パスワードなし)
 *-------------------------------------------------------*
*/
function fnFileZipNoPass($file, $extension, $xlsCustomFlg = '', $ext = '') {
    if ($xlsCustomFlg === '') {
        shell_exec('7z a ' . '"' . 'tmp_zip/' . $file . '.zip' . '"' . ' ' . '"' . PHP_DIR . $extension . '/' . $file . '"');
    } else {
        e_log('7z a ' . '"' . 'tmp_zip/' . $file . '.zip' . '"' . ' ' . '"' . PHP_DIR . $extension . '/' . $file . $ext . '" ' . '"' . PHP_DIR . $extension . '/' . $file . '.csv"');
        shell_exec('7z a ' . '"' . 'tmp_zip/' . $file . '.zip' . '"' . ' ' . '"' . PHP_DIR . $extension . '/' . $file . $ext . '" ' . '"' . PHP_DIR . $extension . '/' . $file . '.csv"');
    }
}
/*
 *-------------------------------------------------------*
 * 送信履歴Update
 *-------------------------------------------------------*
*/
function fnUpdateDB2WHIS($db2con, $whname, $whmail, $whmacc, $whbday, $whbtim, $whzpas = '', $whoutf, $whcsvf, $whxlsf, $whhtmf, $whpkey, $isQGflg) {
    $rtn = array();
    $rs = '0';
    //構文
    $strSQL = ' UPDATE DB2WHIS ';
    $strSQL.= ' SET ';
    $strSQL.= ' WHMACC = ?, ';
    $strSQL.= ' WHCSVF = ?, ';
    $strSQL.= ' WHXLSF = ?, ';
    $strSQL.= ' WHHTMF = ?, ';
    $strSQL.= ' WHZPAS = ? ';
    $strSQL.= ' WHERE ';
    $strSQL.= ' WHNAME = ? ';
    $strSQL.= ' AND WHMAIL = ? ';
    $strSQL.= ' AND WHBDAY = ? ';
    $strSQL.= ' AND WHBTIM = ? ';
    $strSQL.= ' AND WHPKEY = ? ';
    $params = array($whmacc, $whcsvf, $whxlsf, $whhtmf, $whzpas, $whname, $whmail, $whbday, $whbtim, $whpkey);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            $rtn = array('result' => true);
        }
    }
    return $rtn;
}
function fnGetFDB2CSV1($db2con, $D1NAME) {
    $data = array();
    $strSQL = ' SELECT ';
    $strSQL.= '     A.D1WEBF ';
    $strSQL.= '    ,A.D1CFLG ';
    $strSQL.= '    ,A.D1DIRE ';
    $strSQL.= '    ,A.D1DIEX ';
    $strSQL.= '    ,A.D1RDB ';
    $strSQL.= '    ,A.D1EDKB ';
    $strSQL.= '    ,A.D1TMPF ';
    $strSQL.= '    ,A.D1TROS ';
    $strSQL.= ' FROM FDB2CSV1 as A ';
    $strSQL.= ' WHERE A.D1NAME = ? ';
    $params = array($D1NAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $data = array('result' => 'NOTEXIST_GET');
            } else {
                $data = array('result' => true, 'data' => umEx($data));
            }
        }
    }
    return $data;
}
function fnGetFDB2CSV2($db2con, $D2NAME) {
    $data = array();
    $strSQL = ' SELECT ';
    $strSQL.= ' A.D2FLD ';
    $strSQL.= ' FROM FDB2CSV2 as A ';
    $strSQL.= ' WHERE A.D2NAME = ? ';
    $strSQL.= ' ORDER BY A.D2CSEQ ';
    $params = array($D2NAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => $data);
        }
    }
    return $data;
}
//$db2con, $fdb2csv2, $fdb2csv2_ALL, $paramQGData['D1NAME'], $dbname, $db2wcol, $fdb2csv1['RSMASTERARR'], $fdb2csv1['RSSHUKEIARR'], $burstItmInfo, $lenArr[$idx][0], $lenArr[$idx][1], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $allcount
function fnDataShukei($db2con, $fdb2csv2, $fdb2csv2_ALL, $d1name, $dbname, $db2wcol, $rsMasterArr, $rsshukeiArr, $burstItmLst, $start, $length, $WEBF, $d1name_, $cnddatArr, $allcount) {
    $db2conLocal = cmDb2Con();
    //ライブラリセット
    cmSetPHPQUERY($db2conLocal);
    $tabledata = array();
    // クエリ情報を取得する
    $dFile1 = cmGetDbFile_1($db2con, $fdb2csv2, $dbname, $start, $length, '', '', '', '', $db2wcol, $rsMasterArr, true, $burstItmLst, $WEBF, $d1name, $cnddatArr);
    if ($dFile1['result'] !== true) {
        $msg = showMsg($dFile1['result']);
        $rtn = 1;
    } else {
        // 集計情報を取得する
        $dFile2 = cmGetDbFile_2($db2con, $fdb2csv2, $d1name, $dbname, $start, $length, '', $db2wcol, $rsMasterArr, $rsshukeiArr, true, $burstItmLst, $WEBF, $d1name, $cnddatArr, array());
        //e_log('【削除：制御集計データ：'.print_r($dFile2,true));
        if ($dFile2['result'] !== true) {
            $msg = showMsg($dFile2['result']);
            $rtn = 1;
        } else {
            // データをマージする
            $tabledata = cmShukeiDataMerge($db2conLocal, $dFile1['data'], $dFile2['data'], $rsMasterArr, $rsshukeiArr, $fdb2csv2, $fdb2csv2_ALL, $allcount);
            //e_log('【削除：制御集計データ結果：'.print_r($tabledata,true));
            if ($tabledata['result'] !== true) {
                $msg = showMsg($tabledata['result']);
                $rtn = 1;
            } else {
                $tabledata = $tabledata['data'];
                $resultdata = $tabledata;
            }
        }
    }
    cmDb2Close($db2conLocal);
    return $tabledata;
}
/*
 *-------------------------------------------------------*
 * メールを送る同期にEXCEL出力先にEXCELを出力する
 *-------------------------------------------------------*
*/
function createExcelFile($db2con, $db2LibCon, $RDBNM, $PARMD1CFLG, $d1name, $dbname, $nowYmd, $nowTime, $curTime, $DIEX, $mailAddLst, $ZEROMAIL, $webf, $cnddatArr, $Seigyofilename = '') {
    //e_log('【削除】IFSEXCEL作成開始'.$dbname.'ZEROMAILFLG'.$ZEROMAIL);
    $rtn = 0;
    $outFlg = true;
    // $fileext = '';
    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
        e_log('【削除】カウント取得①5294createExcelFile' . $dbname);
        $csv_db_count = cmGetAllCountBurstItm($db2con, '', $dbname, $PARMD1CFLG);
    } else {
        e_log('【削除】カウント取得②5297createExcelFile' . $dbname);
        $csv_db_count = cmGetAllCountBurstItm($db2LibCon, '', $dbname, $PARMD1CFLG);
    }
    if ($csv_db_count === false) {
        $rtn = 1;
    } else {
        $allcount = $csv_db_count[0];
    }
    //e_log("count point1******".$allcount);
    if ($ZEROMAIL == '1' && $allcount === 0) {
        $outFlg = false;
    }
    if ($outFlg) {
        //e_log('【削除】XLSIFS出力先'.$DIEX);
        if (substr($DIEX, 0, 1) !== '/') {
            $DIEX = '/' . $DIEX;
        }
        //e_log("DIEX value後".$DIEX);
        $xlsCustomFlg = '';
        $resXlsCustom = FDB2CSV1_DATA($d1name);
        if ($resXlsCustom['result'] === true) {
            $D1EDKB = cmMer($resXlsCustom['data'][0]['D1EDKB']);
            $D1CFLG = cmMer($resXlsCustom['data'][0]['D1CFLG']);
            $D1TMPF = cmMer($resXlsCustom['data'][0]['D1TMPF']);
            $D1TROS = cmMer($resXlsCustom['data'][0]['D1TROS']);
        } else {
            $rtn = 1;
        }
        //20170412SYPS追加
        //パスがない場合、パスを作成してファイルを出力するようの対応
        $filepath = substr($DIEX, 0, intval(strrpos($DIEX, '/')) + 1);
        $fileallname = substr($DIEX, intval(strrpos($DIEX, '/')) + 1, strlen($DIEX));
        /* if (strpos($fileallname, '.') !== false) {
            $filenm   = substr($fileallname, 0, intval(strrpos($fileallname, '.')));
            $fileext  = substr($fileallname, intval(strrpos($fileallname, '.')) + 1, strlen($fileallname));
        }else{*/
        //   $filenm   = $fileallname;
        //}
        $filename = $fileallname;
        /*** かステムテンプレートの場合
         ＥＸＣＥＬファイルに全角があったらunderscore（_）に変える***/
        if ($D1EDKB === '1') {
            $filename = replaceSaveExcelFileName($filename);
        }
        if (!file_exists($filepath)) {
            // error_log('file_exists($filepath)=false');
            if (!mkdir($filepath, 0777, true)) {
                $errMsg = showMsg('NO_FILEPATH');
                fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, '', $errMsg, array());
            }
        }
        $fdb2csv1 = FDB2CSV1_DATA($d1name);
        if ($fdb2csv1['result'] === true) {
            $fdb2csv1 = $fdb2csv1['data'][0];
            $D1CTFL = cmMer($fdb2csv1['D1CTFL']);
            if ($D1CTFL === '1') {
                $fileNm = '_' . $curTime;
                $filename = $filename . $fileNm;
            }
        } else {
            $rtn = 1;
        }
        //FDB2CSV2取得
        $fdb2csv2 = cmGetFDB2CSV2($db2con, $d1name, true,false,false);
        if ($fdb2csv2['result'] === true) {
            $fdb2csv2 = umEx($fdb2csv2['data']);
        } else {
            $rtn = 1;
        }
        $fdb2csv2_ALL = cmGetFDB2CSV2($db2con, $d1name, false,false,false);
        if ($fdb2csv2_ALL['result'] === true) {
            $fdb2csv2_ALL = umEx($fdb2csv2_ALL['data']);
        } else {
            $rtn = 1;
        }
        $rsMasterArr = array();
        $rsshukeiArr = array();
        // 集計するフィールド情報取得
        $rsmaster = cmGetDB2COLM($db2con, $d1name);
        if ($rsmaster['result'] !== true) {
            $msg = showMsg($rsmaster['result']);
            $rtn = 1;
        } else {
            $rsMasterArr = $rsmaster['data'];
        }
        // 集計するメソッド情報取得
        $rsshukei = cmGetDB2COLTTYPE($db2con, $d1name);
        if ($rsshukei['result'] !== true) {
            $msg = showMsg($rsshukei['result']);
            $rtn = 1;
        } else {
            $rsshukeiArr = $rsshukei['data'];
        }
        //カラム情報取得
        //        e_log(' ③【削除】カラム情報 fdb2csv2' . print_r($fdb2csv2, true));
        $csv_h = cmCreateHeaderArray($fdb2csv2, array());
        $rownum = '';
        $sSearch = '';
        $h_fdb2csv2 = $fdb2csv2;
        $csv_header = $csv_h; // heading
        $DB2EINSROWMAX = 0;
        $D1TMPF = cmMer($D1TMPF);
        if ($D1EDKB === '1') {
            $xlsCustomFlg = $D1EDKB;
        }
        $DB2ECON = array();
        $DB2EINS = array();
        $CNDSDATAF = array();
        //EXCEL SETTING 挿入文字//
        if ($D1EDKB === '2') { //check php excel setting
            $DB2ECON = cmGetDB2ECON($db2con, $d1name);
            if ($DB2ECON['result'] === true) {
                $DB2ECON = umEx($DB2ECON['data']);
            } else {
                $rtn = 1;
            }
            $DB2EINS = cmGetDB2EINS($db2con, $d1name);
            if ($DB2EINS['result'] === true) {
                $EIROWMAX = $DB2EINS['MAXEIROW'];
                $DB2EINS = umEx($DB2EINS['data']);
            } else {
                $rtn = 1;
            }
            $CNDSDATAF = cmCNDSDATA($webf, $db2con, $d1name, array(), $D1CFLG);
            $DB2EINSROWMAX = cmGetCountSearchData($DB2ECON, $EIROWMAX, $CNDSDATAF); //EXCEL SETTING 挿入文字カウント
            
        } else if ($D1EDKB !== '2' && $D1TMPF !== '' && cmMer($wspkey) === '') {
            $DB2EINSROWMAX = $D1TROS;
        }
        //確かにベダーがあるのでプラス1
        if ($D1EDKB === '2' || cmMer($wspkey) !== '' || $D1TMPF === '') {
            $DB2EINSROWMAX+= 1;
        }
        // かステムテンプレートじゃないの場合
        if ($D1EDKB !== '1') {
            $sheet = '';
            $book = '';
            $excel_fp = '';
            $ext = getExt($d1name);
            //MAX件数のためカウント
            if ($ext !== '') {
                $EXCELMAXLEN = ($ext === 'xlsm' || $ext === 'xlsx') ? 1048576 : (($ext === 'xls') ? 65536 : 0);
                $EMAXLEN_FILE = $ext;
            } else {
                if (EXCELVERSION === '2007' || EXCELVERSION == 'XLSX') {
                    $EXCELMAXLEN = 1048576;
                    $EMAXLEN_FILE = 'xlsx';
                } else if (EXCELVERSION === '2003' || EXCELVERSION === 'html') {
                    $EXCELMAXLEN = 65536;
                    $EMAXLEN_FILE = 'xls';
                }
            }
            $DB2EINSROWMAX+= ($allcount === 0 || $allcount === '') ? 0 : $allcount;
            //e_log('in createExcelFile $DB2EINSROWMAX=>'.$DB2EINSROWMAX.'$EXCELMAXLEN=>'.$EXCELMAXLEN);//
            if ($DB2EINSROWMAX > $EXCELMAXLEN) { //check maximum length of EXCEL
                $ISEXCELMAXLEN = 1;
                $errMsg = showMsg('FAIL_EXEMAXLEN', array($EMAXLEN_FILE, $EXCELMAXLEN));
                fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, '', $errMsg, array());
            } else {
                $ISEXCELMAXLEN = 0;
                $EXCELVER = false; ////エクセルバージョン管理
                if (EXCELVERSION == 'XLSX' && $ext === '') {
                    $EXCELVER = true;
                    $excelname = $filename . '.xlsx';
                    $xlsxPath = $filepath . $excelname;
                    $writer = WriterFactory::create(Type::XLSX);
                    $writer->openToFile($xlsxPath);
                    spoutMojiSounyuu($writer, $DB2EINS, $DB2ECON, $CNDSDATAF); //SPOUT EXCELの見出しの上に文字挿入と抽出条件の表示設定
                    $writer->addRow($csv_header);
                } else {
                    if (EXCELVERSION == '2003' || EXCELVERSION == '2007' || $ext !== '') {
                        if ($ext !== '') {
                        } else if (EXCELVERSION == '2003') {
                            $ext = 'xls';
                        } else if (EXCELVERSION == '2007') {
                            $ext = 'xlsx';
                        }
                        $excelname = $filename . '.' . $ext;
                        $sheet = php_excelCreateHeading($d1name);
                        $DB2WCOL = array();
                        $book = $sheet['BOOK'];
                        $sheet = $sheet['SHEET'];
                    } else if (EXCELVERSION == 'html' || $ext === '') {
                        $excelname = $filename . '.xls';
                        if (!file_exists($filepath . $excelname)) {
                            touch($filepath . $excelname);
                        }
                        $excel_fp = fopen($filepath . $excelname, 'w');
                        excelCreateHeading($excel_fp, $DB2ECON, $DB2EINS, $CNDSDATAF);
                    }
                    //e_log('【削除】FDB2CSV2データexeExcel③：'.print_r($h_fdb2csv2,true) );
                    $rownum = exeExcel($excel_fp, $csv_header, array(), $h_fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, array(), array(), $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $sheet, $rownum, '0', $cnddatArr, $filepath);
                }
            }
        } else {
            require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
            $csvname = $filename . '.csv';
            $csv_temp = fopen(BASE_DIR . '/php/tmp_zip/' . $csvname, 'w');
            exeCsv($csv_temp, $csv_header, array(), $h_fdb2csv2, cmMer($d1name), $filename, array(), $nowYmd, $nowTime, true, $xlsCustomFlg, $filepath);
        }
        $lenArr = cmTotalLen($allcount);
        $lenCount = count($lenArr);
        for ($idx = 0;$idx < $lenCount;$idx++) {
            if (count($rsMasterArr) > 0 && count($rsshukeiArr) > 0) { //【削除】IFSExcel
                if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                    $csv_d = fnDataShukei($db2con, $fdb2csv2, $fdb2csv2_ALL, $d1name, $dbname, $db2wcol, $rsMasterArr, $rsshukeiArr, $burstItmInfo, $lenArr[$idx][0], $lenArr[$idx][1], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $allcount);
                } else {
                    $csv_d = fnDataShukei($db2LibCon, $fdb2csv2, $fdb2csv2_ALL, $d1name, $dbname, $db2wcol, $rsMasterArr, $rsshukeiArr, $burstItmInfo, $lenArr[$idx][0], $lenArr[$idx][1], $fdb2csv1['D1WEBF'], $d1name, $cnddatArr, $allcount);
                }
            } else {
                if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                    $csv_d = cmGetDbFile($db2con, $fdb2csv2_ALL, $dbname, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', '', array(), true, $PARMD1CFLG, EXCELVERSION, array(), array());
                    if (count($csv_d) > 0) {
                        $csv_d = $csv_d['data'];
                    }
                } else {
                    $csv_d = cmGetDbFile($db2LibCon, $fdb2csv2_ALL, $dbname, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', '', array(), true, $PARMD1CFLG, EXCELVERSION, array(), array());
                    if (count($csv_d) > 0) {
                        $csv_d = $csv_d['data'];
                    }
                }
            }
            if (count($csv_d) === 0) {
                $rtn = 1;
            } else {
                $csv_d = $csv_d;
            }
            // かステムテンプレートじゃないの場合
            if ($D1EDKB !== '1') {
                if ($ISEXCELMAXLEN === 0) { //MAX件数のためカウントのチェック
                    //e_log('【削除】FDB2CSV2データexeExcel④：'.print_r($fdb2csv2,true) )
                    if (EXCELVERSION == 'XLSX' && $EXCELVER === true) {
                        $c_data = array_chunk($csv_d, 100);
                        foreach ($c_data as $ck => $cv) {
                            $writer->addRows($cv);
                        }
                    } else {
                        $rownum = exeExcel($excel_fp, $csv_h, $csv_d, $fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, array(), array(), $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $sheet, $rownum, '1', $cnddatArr, $filepath);
                    }
                }
            } else {
                exeCsv($csv_temp, $csv_h, $csv_d, $fdb2csv2, cmMer($d1name), $filename, array(), $nowYmd, $nowTime, false, $xlsCustomFlg, $filepath);
                fclose($csv_temp);
            }
        }
        // かステムテンプレートじゃないの場合
        if ($D1EDKB !== '1') {
            // $fileext = cmMer($fileext);
            if ($ISEXCELMAXLEN === 0) { //MAX件数のためカウントのチェック
                if (EXCELVERSION == 'html' && $ext === '') {
                    /* if ($fileext !== '') {
                        $excelname = $filename . '.' . $fileext;
                        if ((EXCELVERSION === '2007' && $fileext !== 'xlsx') || ((EXCELVERSION === 'html' || EXCELVERSION === '2003') && $fileext !== 'xls')) {
                            $errMsg = showMsg('NO_MATCHVER');
                            fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, '', $errMsg, array());
                        }
                    } else {*/
                    $excelname = $filename . '.xls';
                    //  }
                    fwrite($excel_fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                    fclose($excel_fp);
                    // $ext = '.xls';
                    
                } else {
                    if (EXCELVERSION == 'XLSX' && $EXCELVER === true) {
                        $excelname = $filename . '.xlsx';
                        $ext = '.xlsx';
                        $writer->close();
                    } else if ((EXCELVERSION == '2003' && $ext === '') || $ext === 'xls') {
                        /* if ($fileext !== '') {
                            $excelname = $filename . '.' . $fileext;
                            if ((EXCELVERSION === '2007' && $fileext !== 'xlsx') || ((EXCELVERSION === 'html' || EXCELVERSION === '2003') && $fileext !== 'xls')) {
                                $errMsg = showMsg('NO_MATCHVER');
                                fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, '', $errMsg, array());
                            }
                        } else {*/
                        $excelname = $filename . '.xls';
                        // }
                        $writer = PHPExcel_IOFactory::createWriter($book, "Excel5");
                        $writer->save($filepath . $excelname);
                        $book = null;
                        // $ext  = '.xls';
                        
                    } else if ((EXCELVERSION == '2007' && $ext === '') || $ext === 'xlsx') {
                        /* if ($fileext !== '') {
                            $excelname = $filename . '.' . $fileext;
                            if ((EXCELVERSION === '2007' && $fileext !== 'xlsx') || ((EXCELVERSION === 'html' || EXCELVERSION === '2003') && $fileext !== 'xls')) {
                                $errMsg = showMsg('NO_MATCHVER');
                                fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, '', $errMsg, array());
                            }
                        } else {*/
                        $excelname = $filename . '.xlsx';
                        // }
                        $writer = PHPExcel_IOFactory::createWriter($book, "Excel2007");
                        $writer->save($filepath . $excelname);
                        $book = null;
                        // $ext  = '.xlsx';
                        
                    }
                }
            }
        } else {
            $tmpzippath = BASE_DIR . '/php/tmp_zip/';
            $cExt = getExt($d1name);
            $templateFn = $d1name . '.' . $cExt;
            shell_exec('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $tmpzippath . '"' . $filename . '.' . $cExt . '"');
            shell_exec('7z a ' . '"' . $tmpzippath . $filename . '.zip' . '"' . ' ' . '"' . $tmpzippath . $filename . '.' . $cExt . '" ' . '"' . $tmpzippath . $filename . '.csv"');
            shell_exec('cp ' . '"' . $tmpzippath . $filename . '.zip' . '"' . ' ' . '"' . $filepath . $filename . '.zip' . '"');
            if (file_exists($tmpzippath . $filename . '.' . $cExt)) {
                @unlink($tmpzippath . $filename . '.' . $cExt);
            }
            if (file_exists($tmpzippath . $filename . '.' . 'csv')) {
                @unlink($tmpzippath . $filename . '.' . 'csv');
            }
            if (file_exists($tmpzippath . $filename . '.zip')) {
                @unlink($tmpzippath . $filename . '.zip');
            }
        }
    }
    return $rtn;
}
function createPivotExcelFile($db2con, $db2LibCon, $RDBNM, $d1name, $dbname, $nowYmd, $nowTime, $curTime, $SOEX, $webf, $cnddatArr, $wspkey, $SOCTFL, $D1DIEX = '') {
    $rtn = 0;
    $outFlg = true;
    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
        $csv_db_count = cmGetAllCountBurstItm($db2con, '', $dbname, '');
    } else {
        $csv_db_count = cmGetAllCountBurstItm($db2LibCon, '', $dbname, '');
    }
    if ($csv_db_count === false) {
        $rtn = 1;
    } else {
        $allcount = $csv_db_count[0];
    }
    if (substr($SOEX, 0, 1) !== '/') {
        $SOEX = '/' . $SOEX;
    }
    $fpath = substr($SOEX, 0, intval(strrpos($SOEX, '/')) + 1);
    $fileallname = substr($SOEX, intval(strrpos($SOEX, '/')) + 1, strlen($SOEX));
    $filename = $fileallname;
    //e_log("1createPivotExcelFile : ".$SOEX."fpath:".$fpath."fileallname:".$fileallname);
    if (!file_exists($fpath)) {
        if (!mkdir($fpath, 0777, true)) {
            $errMsg = showMsg('NO_FILEPATH');
            fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, '', $errMsg, array());
        }
    }
    if ($SOCTFL === '1') {
        $fileNm = '_' . $curTime;
        $filename = $filename . $fileNm;
    }
    //e_log("2createPivotExcelFile : ".$SOEX."fpath:".$fpath."filename:".$filename);
    $pivotFlg = array();
    $pivotFlg['XLSFLG'] = true;
    //e_log('ここくる20170522');
    //e_log("ここきった"."wspkey".$wspkey."DB2ECON".$DB2ECON."DB2EINS".$DB2EINS);
    //e_log("Hello Hello klyh exePivotExcel testing in :allcount".$allcount);
    if ($allcount === 0) {
        //e_log("33333Hello Hello klyh exePivotExcel testing in :allcount".$fpath);
        $tbdata = _fputpivot_ZeroKen($db2con, $d1name, $filename, $wspkey, $dbname, array(), array(), $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $pivotFlg, $fpath);
    } else {
        //  e_log("Hello Hello klyh exePivotExcel testing in :createPivotExcelFile");
        $pivotRtn = exePivotExcel($db2con, $d1name, $filename, $wspkey, $dbname, array(), true, 'htmlcreate', array(), $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $cnddatArr, $DB2EINSROWMAX, $pivotFlg, $fpath, $D1DIEX);
        if ($pivotRtn['RTN'] === 0) {
            $tbdata = $pivotRtn['PIVOTHTML'];
            if ($pivotRtn['EXEMAXLEN'] === 'FAIL_EXEMAXLEN') {
                $ISEXCELMAXLEN = 1;
                $mailAddLst[0]['WAXLS'] = '9';
                $errMsg = showMsg('FAIL_EXEMAXLEN', array('xls', '65536'));
                fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
            }
        } else {
            $rtn = 1;
        }
    }
    //e_log("$$$$$$$$$$$createPivotExcelFile".print_r($tbdata,true));
    return $rtn;
}
/***ＥＸＣＥＬファイルに全角があったらunderscore（_）に変える***/
function replaceSaveExcelFileName($filename) {
    $zCnt = 0;
    $sCnt = 0;
    $zn0 = 0;
    $zn = 0;
    $strlen = mb_strlen($filename, "UTF-8");
    $replaceStr = '';
    //１文字ずつ比較
    for ($i = 0;$i < $strlen;$i++) {
        $evalstr = mb_substr($filename, $i, 1, "UTF-8");
        //1byte??
        if ((strlen(bin2hex($evalstr)) / 2) == 1) {
            $zn = 0;
            $sCnt = $sCnt + 1;
            //半角ｶﾅ??
            $replaceStr.= $evalstr;
        } else if (preg_match('/^(?:\xEF\xBD[\xA1-\xBF]|\xEF\xBE[\x80-\x9F]){1,100}$/', $evalstr)) {
            $zn = 0;
            $sCnt = $sCnt + 1;
            $replaceStr.= $evalstr;
        } else {
            $zn = 1;
            $sCnt = $sCnt + 2;
            $replaceStr.= "_";
        }
    }
    return $replaceStr;
}
function createCSVFile($db2con, $db2LibCon, $d1name, $D1CFLG, $dbname, $nowYmd, $nowTime, $DIRE, $ZEROMAIL, $webf, $d1name_, $CNDSDATA, $curTime, $Seigyofilename = '') {
    e_log('【削除】IFSCSV出力①'.$Seigyofilename."②:".$dbname);
    $rtn = 0;
    require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
    $sSearch = '';
    $db2wcol = '';
    $outFlg = true;
    //$csv_db_count = cmGetAllCountBurstItm($db2LibCon, '', $dbname, $D1CFLG);
    $csv_db_count = cmGetAllCountBurstItm($db2LibCon, '', $dbname, $D1CFLG);
    //e_log('【削除】IFSCSV出力①カウント：'.$DIRE.'datacount'.print_r($csv_db_count,true));
    $allcount = $csv_db_count[0];
    $fdb2csv2 = cmGetFDB2CSV2($db2con, $d1name, true,false,false);
    $fdb2csv2 = umEx($fdb2csv2['data']);
    $fdb2csv2_all = cmGetFDB2CSV2($db2con, $d1name, false,false,false);
    $fdb2csv2_all = umEx($fdb2csv2_all['data']);
    if ($ZEROMAIL === '1' && $allcount === 0) {
        $outFlg = false;
    }
    if ($outFlg) {
        //e_log('【削除】IFSCSV出力②カウント：'.$DIRE.$dbname);
        if (substr($DIRE, 0, 1) !== '/') {
            $DIRE = '/' . $DIRE;
        }
        //20170412SYPS追加
        //パスがない場合、パスを作成してファイルを出力するようの対応
        $fpath = substr($DIRE, 0, intval(strrpos($DIRE, '/')) + 1);
        $fname = substr($DIRE, intval(strrpos($DIRE, '/')) + 1, strlen($DIRE));
        if (!file_exists($fpath)) {
            if (!mkdir($fpath, 0777, true)) {
                $errMsg = showMsg('NO_FILEPATH');
                fnCreateHistory($db2con, $d1name, $filename, $dbname, $nowYmd, $nowTime, '', $errMsg, array());
            }
        }
        $fdb2csv1 = FDB2CSV1_DATA($d1name);
        $fdb2csv1 = umEx($fdb2csv1['data']);
        $fdb2csv1 = $fdb2csv1[0];
        $D1CTFL = cmMer($fdb2csv1['D1CTFL']);
        if ($D1CTFL === '1') {
            if (strpos($fname, '.') !== false) {
                $filenm1 = substr($fname, 0, intval(strrpos($fname, '.')));
                $filenm2 = substr($fname, intval(strrpos($fname, '.')), strlen($fname));
                $fname = $filenm1 . '_' . $curTime . $filenm2;
            } else {
                $fname = $fname . '_' . $curTime;
            }
        }
        $rsMasterArr = array();
        $rsshukeiArr = array();
        // 集計するフィールド情報取得
        $rsmaster = cmGetDB2COLM($db2con, $d1name);
        if ($rsmaster['result'] !== true) {
            $msg = showMsg($rsmaster['result']);
            $rtn = 1;
        } else {
            $rsMasterArr = $rsmaster['data'];
        }
        // 集計するメソッド情報取得
        $rsshukei = cmGetDB2COLTTYPE($db2con, $d1name);
        if ($rsshukei['result'] !== true) {
            $msg = showMsg($rsshukei['result']);
            $rtn = 1;
        } else {
            $rsshukeiArr = $rsshukei['data'];
        }
        //end testing code
        $DIRE = $fpath . $fname;
        $piec  = explode(".", $fname);
        $DIRETYP = $fpath .$piec[0].'.typ';
        error_log('driectory = '.$DIRETYP);
        if (!file_exists($DIRE)) {
            $createFile = touch($DIRE);
            //e_log('【削除】IFSCSV出力③ファイル作成：'.$DIRE.'datacount'.print_r($csv_db_count,true));

            $createFiletype = touch($DIRETYP);
            $csv = ''; $D1CDLM = ',';
            $fp = fopen($DIRETYP, 'w');
            foreach ($fdb2csv2 as $key => $col) {
                $type = $fdb2csv2[$key]['D2TYPE'];
                $dec = $fdb2csv2[$key]['D2DEC'];
                //結果フィールドの場合は指定少数桁数で切り捨て
                if ($type === 'S' || $type === 'P' || $type === 'B') {
                    if($dec > 0){
                        $coll = '"' . 'float' . '"';
                    }else{
                        $coll = '"' . 'int' . '"';
                    }
                }else{
                    $coll = '"' . 'string' . '"';
                }
                $csv.= $coll;
                if (($key + 1) < count($fdb2csv2)) {
                    $csv.= $D1CDLM;
                }
            }
            fwrite($fp, $csv);
            fclose($fp);
        }
        if ($createFile === false) {
            $rtn = 'IFS出力先に設定されたCSVファイルが作成されませんでした。<br>指定のディレクトリが存在しないか、権限がありません。';
            //e_log('【削除】IFSCSV出力④'.$rtn);
            
        } else {
            $fp = fopen($DIRE, 'w');
            //ヘッダー情報取得
            //e_log('①【削除】ヘッダー情報取得'.print_r($fdb2csv2,true));
            $csv_h = cmCreateHeaderArray($fdb2csv2, array());
            $data = array($csv_h);
            $headerCnt = count($csv_h);
            foreach ($data as $key => $value) {
                _fputcsv($fp, $value, $key, $fdb2csv1, $fdb2csv2, $headerCnt, true, array());
            }
            $lenArr = cmTotalLen($allcount);
            $rtnMail = 0;
            $lenCount = count($lenArr);
            for ($idx = 0;$idx < $lenCount;$idx++) {
                if (count($rsMasterArr) > 0 && count($rsshukeiArr) > 0) { //【削除】IFSCSV
                    $tabledata = fnDataShukei($db2con, $fdb2csv2, $fdb2csv2_ALL, $d1name, $dbname, $db2wcol, $rsMasterArr, $rsshukeiArr, $burstItmInfo, $lenArr[$idx][0], $lenArr[$idx][1], $fdb2csv1['D1WEBF'], $d1name, $CNDSDATA, $allcount);
                    $data = $tabledata;
                    $tabledataresult = "false";
                } else {
                    $tabledata = cmGetDbFile($db2LibCon, $fdb2csv2_all, $dbname, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', '', array(), true, $D1CFLG, '', array(), array());
                    $data = $tabledata['data'];
                    $tabledataresult = $tabledata['result'];
                }
                if (count($tabledata) > 0) {
                    foreach ($data as $key => $value) {
                        _fputcsv($fp, $value, $key, $fdb2csv1, $fdb2csv2, $headerCnt, false, array());
                    }
                } else {
                    $rtn = showMsg($tabledataresult);
                }
            }
            fclose($fp);
        }
    }
    return $rtn;
}
function getDirfileNm($filenm) {
    //e_log('【削除】元のファイル：'.$filenm);
    if ($filenm !== '') {
        $filenmArr = explode('/', $filenm);
        $filenm = '';
        $fileCnt = count($filenmArr) - 1;
        for ($i = 0;$i < $fileCnt;$i++) {
            if (cmMer($filenmArr[$i]) != '') {
                $filenm.= '/' . replaceFilename(trim($filenmArr[$i]));
            }
        }
        $filenm.= '/' . replaceFilename(ltrim($filenmArr[$fileCnt]));
    }
    //e_log('【削除】dircheckのファイル：'.$filenm);
    return $filenm;
}
// xls のテンプレートファイルあるかのチェック
function chkXlsTemplate($d1name, $d1tmpf) {
    //e_log ('【削除】templatename :'.$d1tmpf);
    $templateFlg = true;
    if ($d1tmpf !== '') {
        $ext = getExt($d1name);
        if (file_exists("./template/" . $d1name . '.' . $ext)) {
            $templateFlg = true;
        } else {
            $templateFlg = false;
        }
    }
    return $templateFlg;
}
//SPOUT EXCELの見出しの上に文字挿入と抽出条件の表示設定
function spoutMojiSounyuu($writer, $DB2EINS, $DB2ECON, $CNDSDATAF) {
    e_log("spoutMojiSounyuu");
    /* e_log("DB2EINS".print_r($DB2EINS,true));
     e_log("DB2ECON".print_r($DB2ECON,true));*/
    //  e_log("CNDSDATAF".print_r($CNDSDATAF,true));
    $c = 1;
    $style = '';
    $blankArray = array('');
    foreach ($DB2EINS as $key => $value) {
        $row = (int)$value['EIROW'];
        while ($c < $row) {
            $writer->addRow($blankArray);
            $c++;
        }
        $c++;
        $eisize = (int)$value['EISIZE'];
        $eicolor = $value['EICOLR'];
        $val = $value['EITEXT'];
        $dta = array($val);
        $style = new StyleBuilder();
        if ($value['EIBOLD'] !== '') {
            $style = $style->setFontBOLD();
        }
        if ($eisize > 0) {
            e_log("eisize" . $eisize);
            $style = $style->setFontSize($eisize);
        }
        //$style = (new StyleBuilder()) ->setFontSize(24)->build();
        if ($eicolor !== '') {
            //$style = $style->setFontColor(Color::BLUE);
            if ($eicolor === "#000000") {
                $style->setFontColor(Color::C000000);
            } else if ($eicolor === "#800000") {
                $style->setFontColor(Color::C800000);
            } else if ($eicolor === "#FF0000") {
                $style->setFontColor(Color::CFF0000);
            } else if ($eicolor === "#FF00FF") {
                $style->setFontColor(Color::CFF00FF);
            } else if ($eicolor === "#FF99CC") {
                $style->setFontColor(Color::CFF99CC);
            } else if ($eicolor === "#993300") {
                $style->setFontColor(Color::C993300);
            } else if ($eicolor === "#FF6600") {
                $style->setFontColor(Color::CFF6600);
            } else if ($eicolor === "#FF9900") {
                $style->setFontColor(Color::CFF9900);
            } else if ($eicolor === "#FFCC00") {
                $style->setFontColor(Color::CFFCC00);
            } else if ($eicolor === "#FFCC99") {
                $style->setFontColor(Color::CFFCC99);
            } else if ($eicolor === "#333300") {
                $style->setFontColor(Color::C333300);
            } else if ($eicolor === "#808000") {
                $style->setFontColor(Color::C808000);
            } else if ($eicolor === "#99CC00") {
                $style->setFontColor(Color::C99CC00);
            } else if ($eicolor === "#FFFF00") {
                $style->setFontColor(Color::CFFFF00);
            } else if ($eicolor === "#FFFF99") {
                $style->setFontColor(Color::CFFFF99);
            } else if ($eicolor === "#003300") {
                $style->setFontColor(Color::C003300);
            } else if ($eicolor === "#008000") {
                $style->setFontColor(Color::C008000);
            } else if ($eicolor === "#339966") {
                $style->setFontColor(Color::C339966);
            } else if ($eicolor === "#00FF00") {
                $style->setFontColor(Color::C00FF00);
            } else if ($eicolor === "#CCFFCC") {
                $style->setFontColor(Color::CCCFFCC);
            } else if ($eicolor === "#003366") {
                $style->setFontColor(Color::C003366);
            } else if ($eicolor === "#008080") {
                $style->setFontColor(Color::C008080);
            } else if ($eicolor === "#33CCCC") {
                $style->setFontColor(Color::C33CCCC);
            } else if ($eicolor === "#00FFFF") {
                $style->setFontColor(Color::C00FFFF);
            } else if ($eicolor === "#CCFFFF") {
                $style->setFontColor(Color::CCCFFFF);
            } else if ($eicolor === "#000080") {
                $style->setFontColor(Color::C000080);
            } else if ($eicolor === "#0000FF") {
                $style->setFontColor(Color::C0000FF);
            } else if ($eicolor === "#3366FF") {
                $style->setFontColor(Color::C3366FF);
            } else if ($eicolor === "#00CCFF") {
                $style->setFontColor(Color::C00CCFF);
            } else if ($eicolor === "#99CCFF") {
                $style->setFontColor(Color::C99CCFF);
            } else if ($eicolor === "#333399") {
                $style->setFontColor(Color::C333399);
            } else if ($eicolor === "#666699") {
                $style->setFontColor(Color::C666699);
            } else if ($eicolor === "#800080") {
                $style->setFontColor(Color::C800080);
            } else if ($eicolor === "#993366") {
                $style->setFontColor(Color::C993366);
            } else if ($eicolor === "#CC99FF") {
                $style->setFontColor(Color::CCC99FF);
            } else if ($eicolor === "#333333") {
                $style->setFontColor(Color::C333333);
            } else if ($eicolor === "#808080") {
                $style->setFontColor(Color::C808080);
            } else if ($eicolor === "#969696") {
                $style->setFontColor(Color::C969696);
            } else if ($eicolor === "#C0C0C0") {
                $style->setFontColor(Color::CC0C0C0);
            } else if ($eicolor === "#FFFFFF") {
                $style->setFontColor(Color::CFFFFFF);
            } else if ($eicolor === "#9999FF") {
                $style->setFontColor(Color::C9999FF);
            } else if ($eicolor === "#FFFFCC") {
                $style->setFontColor(Color::CFFFFCC);
            } else if ($eicolor === "#660066") {
                $style->setFontColor(Color::C660066);
            } else if ($eicolor === "#FF8080") {
                $style->setFontColor(Color::CFF8080);
            } else if ($eicolor === "#0066CC") {
                $style->setFontColor(Color::C0066CC);
            } else if ($eicolor === "#CCCCFF") {
                $style->setFontColor(Color::CCCCCFF);
            }
        }
        $style = $style->build();
        $writer->addRowWithStyle($dta, $style);
        $style = '';
    }
    if (count($DB2ECON) > 0) {
        $ECFLG = $DB2ECON[0]['ECFLG'];
        if ($ECFLG === '1' && count($CNDSDATAF) > 0) {
            $writer->addRow($blankArray);
            foreach ($CNDSDATAF as $key => $value) {
                $searchDta = array();
                $isSQL = $value['isSQL'];
                if ($isSQL === '1') { //ウエブＳＱＬの場合
                    $cndlabel = cmHsc(cmHscDe($value['cndlabel']));
                    $cnddatadecode = cmHsc(cmHscDe($value['cnddata']));
                    array_push($searchDta, $cndlabel, $cnddatadecode);
                } else {
                    $gandor = $value['gandor'];
                    if ($CNDSDATA[$i]['gandor'] !== 'seq') {
                        $gandor = $value['gandor'];
                    } else {
                        $gandor = '';
                    }
                    $andor = $value['andor'];
                    $cndlabel = $value['cndlabel'];
                    $cnddatadecode = $value['cnddata'];
                    $cndtype = $value['cndtype'];
                    array_push($searchDta, $gandor, $andor, $cndlabel, $cnddatadecode, $cndtype);
                }
                $writer->addRow($searchDta);
            }
        }
        $ECROWN = (int)$DB2ECON[0]['ECROWN'];
        for ($i = 0;$i < $ECROWN;$i++) {
            $writer->addRow($blankArray);
        }
    }
}
