<?php
/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");
include_once("getQryCnd.php");
include_once("getSQLCnd.php");
include_once("base/createExecuteSQL.php");
include_once("chkCondParam.php");
include_once("chkSQLParam.php");
include_once("base/fnDB2QHIS.php");
include_once("CLExecutePG.php");
include_once("base/createTblExeSql.php");
include_once("base/getChkOutputFileData.php");

/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */
$d1name  = (isset($_GET['D1NAME']) ? $_GET['D1NAME'] : '');
$rtn = 0;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);


//$data = cmChkQuery($db2con, '', $d1name, '','');
$rs = fnGetFDB2CSV1($db2con,$d1name);

    if($rs['RTN'] !== 0){
        $rtn = $rs['RTN'];
        $msg = showMsg($rs['MSG'],array('クエリー'));
    }else{
        $fdb2csv1 = $rs['DATA'];
        $SQLCFLG  = $fdb2csv1['D1CFLG'];
        $D1LIBL   = $fdb2csv1['D1LIBL'];
        $D1RDB    = $fdb2csv1['D1RDB'] ==='' ? RDB : $fdb2csv1['D1RDB']; 
        if($SQLCFLG === '1'){
            $rs = getBaseSQLData($db2con,$fdb2csv1);
            if($rs['RTN'] !== 0){
                $rtn = $rs['RTN'];
                $msg = $rs['MSG'];
            }else{
                $basesqldata = $rs['BASESQLDATA'];
            }
        }else{
            $rs = generateBaseFileData($db2con,$fdb2csv1);
            if($rs['RTN'] !== 0){
                $rtn = $rs['RTN'] ;
                $msg = $rs['MSG'];
            }else{
                $basefiledata = $rs['BASEFILEDATA'];
            }
        }
    }
$D1LIBL= $basefiledata['D1LIBL'];
$liblArr = explode(" ",$D1LIBL);
$libl = array();
foreach($liblArr as $key => $value){
    if($value !== ''){
        array_push($libl,$value);
    }
}
$reffileArr = array();
$REFFILE = $basefiledata['REFFILE'];
foreach($REFFILE as $rkey => $rvalue){
    $ref['RTRLIB'] = $rvalue['RTRLIB'];
    $ref['RTRFIL'] = $rvalue['RTRFIL'];
    $ref['REFIDX'] = $rkey + 1;
    array_push($reffileArr,$ref);
}

$BASEFILEINFO   = array(
        "D1FILE" => "URIAGE",
        "D1FILLIB" => "*LIBL",
        "D1FILMBR" => "",
        "D1NAME" => "A_SEIGYO",
        "D1NAMENEW" => "",
        "D1TEXT" => "A_SEIGYO",
        "RDBNM" => "",
        "REFFILE" => $reffileArr,
        "SELLIBLIST" => $libl
);
$RTNINITBASEFLD = array(); 
$FILECNT        = '';
$PROC           = 'ADD';
//  チェックライブラリー又ファイルを変更するか？
if($rtn === 0){
    $compLibl = $BASEFILEINFO['D1FILLIB'];
    $compFile = $BASEFILEINFO['D1FILE'] ;
    $oriLibl  = $ori_FDB2CSV1['D1FILLIB'];
    $oriFile  = $ori_FDB2CSV1['D1FILE'];
    $orilibList = preg_replace('/\s(?=\s)/', '', $ori_FDB2CSV1['D1LIBL']);
    $complibList = join(' ',$BASEFILEINFO['SELLIBLIST']);
    $chkChgFileInfo = chkChangeFile($oriLibl,$oriFile,$compLibl,$compFile,$orilibList,$complibList);
}
$RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
if($RDBNM === ''){
    if($rtn === 0){
        $paramFILTYP='0-P';
		$FILE = 'P';
        $rs = getSYSCOLUMN2($db2con,$BASEFILEINFO['D1FILE'] ,$BASEFILEINFO['D1FILLIB'],$BASEFILEINFO['SELLIBLIST'],$paramFILTYP,$FILE);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}


$PRIMARYD = $data;
if($rtn === 0){
    $fileInfoData['D2FILID'] = 0;
    $fileInfoData['D2FDESC'] = 'プライマリ';
    $fileInfoData['FILENM'] = $BASEFILEINFO['D1FILE'];
    $fileInfoData['LIBNM'] = $BASEFILEINFO['D1FILLIB'];
    $fileInfoData['FILTYP'] = 'P';
    $fileInfoData['STRDATA'] = $data;
    $STRDATA1 =  $data;
    $RTNINITBASEFLD[] = $fileInfoData;
    $allShow[]=$data;//全てチェック

}
$filCnt = 0;
if($rtn === 0){
    $REFFILE = $BASEFILEINFO['REFFILE'];
    if(count($REFFILE) > 0){
        $rs = fnGetBREFTBL($db2con,$BASEFILEINFO['D1NAME']);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $oriBREFFIL = $rs['data'];
            if(count($oriBREFFIL) === 0){
                $chgFileCnt = true;
            }else{
                $chgFileCnt = false;
            }
        }
        if($rtn === 0){
            foreach ($REFFILE as $RFILVAL) {
                //  チェックライブラリー又ファイルを変更するか？
                $chgFileFlg = $chgFileCnt;
                if($chgFileFlg === false){
                    if($rtn === 0){
                        foreach($oriBREFFIL as $oriBREFFILVAL){
                            if($RFILVAL['REFIDX'] == $oriBREFFILVAL['RTRSEQ']){
                                $compLibl = $RFILVAL['RTRLIB'];
                                $compFile = $RFILVAL['RTRFIL'];
                                $oriLibl  = $oriBREFFILVAL['RTRLIB'];
                                $oriFile  = $oriBREFFILVAL['RTRFIL'] ;
                                $chgFileFlg = chkChangeFile($oriLibl,$oriFile,$compLibl,$compFile,$orilibList,$complibList);
                                break;
                            }else{
                                $chgFileFlg = true;
                            }
                        }
                    }    
                }
                if($RDBNM === ''){
                    $paramFILTYP=$RFILVAL['REFIDX'].'-R';
					$FILE = 'S'.$RFILVAL['REFIDX'];
                    if($PROC === 'EDIT'){

                    }else{
                        $rs = getSYSCOLUMN2($db2con,$RFILVAL['RTRFIL'] ,$RFILVAL['RTRLIB'],$BASEFILEINFO['SELLIBLIST'],$paramFILTYP,$FILE);
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = showMsg($rs['result']);
                            break;
                        }else{
                            $data = $rs['data'];
                        }
                    }
                }
                if($rtn === 0){
                    $filCnt = $RFILVAL['REFIDX'];
                    $fileInfoData['D2FILID'] = $RFILVAL['REFIDX'];
                    $fileInfoData['D2FDESC'] = '参照'.$RFILVAL['REFIDX'];
                    $fileInfoData['FILENM'] = $RFILVAL['RTRFIL'];
                    $fileInfoData['LIBNM'] = $RFILVAL['RTRLIB'];
                    $fileInfoData['FILTYP'] = 'R';
                    $fileInfoData['STRDATA'] = $data;
                    $RTNINITBASEFLD[] = $fileInfoData;
                    $allShow[]=$data;//全てチェック
                }
            }
        }
    }
}
if($rtn === 0){
    $paramFILTYP=($filCnt+1).'-0';
	$FILE = 'K';
    if($PROC == 'EDIT'){    

    }else{
        $rs = getSYSCOLUMN2($db2con,'' ,'','',$paramFILTYP,$FILE);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}
if($rtn === 0){
    $fileInfoData['D2FILID'] = $filCnt+1;
    $fileInfoData['D2FDESC'] = '結果フィールド';
    $fileInfoData['FILENM'] = '';
    $fileInfoData['LIBNM'] = '';
    $fileInfoData['FILTYP'] = '0';
    $fileInfoData['STRDATA'] = $data;
    $RTNINITBASEFLD[] = $fileInfoData;
    $allShow[]=$data;//全てチェック
}
funUpdateFDB2CSV2($db2con,$d1name,$allShow);
cmDb2Close($db2con);

echo("COLUCMDATA <pre>");
    print_r($allShow);
echo("</pre>");


function funUpdateFDB2CSV2($db2con,$D1NAME,$allShow){
    $strSQL = ' UPDATE FDB2CSV2 SET D2HED = ? WHERE D2NAME = ? AND D2FLD = ? AND D2FILID = ? ';
    $rtn = 0;
    foreach($allShow as $d2filid => $value){
        echo("<br/>");
        foreach($value as $k => $val){
            $d2fld = $val['COLUMN_NAME'];
            $d2hed = $val['COLUMN_HEADING'];
            $params = array($d2hed,$D1NAME,$d2fld,$d2filid);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rtn = 1;
                break;
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    echo("inner error");
                    $rtn = 1;
                    break;
                }else{
                    echo($d2hed.'-'.$D1NAME.'-'.$d2fld.'-'.$d2filid.'<br/>');
                }
            }
        }
        if($rtn === 1){
            echo("error");
            break;
        }
    }
}

function fnGetFDB2CSV1($db2con,$D1NAME){
    $data = array();
    $rtn = 0;
    $msg = '';
    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rtn = 1;
        $msg = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rtn = 1;
            $msg = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rtn = 3;
                $msg = 'NOTEXIST_GET';
            }else{
                $data = umEx($data);
                $data = $data[0];
                if($data['D1WEBF'] !== '1'){
                    $rtn = 3;
                    $msg = 'MSGQRCRE';
                }
            }
        }
    }
    $rtnArr = array(
                'RTN' => $rtn,
                'MSG' => $msg,
                'DATA'=> $data
            );
    return $rtnArr;
}
function generateBaseFileData($db2con,$fdb2csv1){
    $rtn = 0;
    $msg = '';
    if($rtn === 0){
        $basefileData = array();
        $basefileData['D1NAME'] = $fdb2csv1['D1NAME'];
        $basefileData['D1FILE'] = $fdb2csv1['D1FILE'];
        $basefileData['D1FILMBR'] = $fdb2csv1['D1FILMBR'];
        if($fdb2csv1data['D1FILLIB'] === ''){
            $fdb2csv1data['D1FILLIB'] = '*USRLIBL';
        }
        $basefileData['D1FILLIB'] = $fdb2csv1['D1FILLIB'];
        $basefileData['D1TEXT'] = $fdb2csv1['D1TEXT'];
        $basefileData['D1LIBL'] = $fdb2csv1['D1LIBL'];
        $basefileData['D1RDB'] = (cmMer($fdb2csv1['D1RDB'])=='' ? RDB : cmMer($fdb2csv1['D1RDB'])); 
        $res = getBREFTBLData($db2con,$basefileData['D1NAME']);
        if($res['result'] !== true ){
            $rtn = 1;
            $msg = showMsg($res['result']);
        }else{
            $breftbldata = array();
            if(count($res['data'])>0){
                foreach($res['data'] as $breftblitm){
                    $res1 = getBREFFLDData($db2con,$breftblitm['RTQRYN'],$breftblitm['RTRSEQ']);
                    if($res1['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                        break;
                    }
                    else{
                        $brefflddata = array();
                        foreach($res1['data'] as $resBreffld){
                            $breffldinfo = array();
                            $breffldinfo['RFFSEQ'] = $resBreffld['RFFSEQ'];
                            $breffldinfo['RFRKBN'] = $resBreffld['RFRKBN'];
                            $breffldinfo['RFPFNM'] = $resBreffld['RFPFNM'];
                            $breffldinfo['RFRFNM'] = $resBreffld['RFRFNM'];
                            $brefflddata[] = $breffldinfo;
                        }
                        $breftblinfo = array();
                        $breftblinfo['RTQRYN']  = $breftblitm['RTQRYN'];
                        $breftblinfo['RTRSEQ']  = $breftblitm['RTRSEQ'];
                        $breftblinfo['RTRFIL']  = $breftblitm['RTRFIL'];
                        $breftblinfo['RTRMBR']  = $breftblitm['RTRMBR'];
                        if( $breftblitm['RTRLIB'] === ''){
                             $breftblitm['RTRLIB'] = '*USRLIBL';
                        }
                        $breftblinfo['RTRLIB']  = $breftblitm['RTRLIB'];
                        $breftblinfo['RTDESC']  = $breftblitm['RTDESC'];
                        $breftblinfo['RTJTYP']  = $breftblitm['RTJTYP'];
                        $breftblinfo['BREFFLD'] = $brefflddata;
                        
                        $breftbldata[] = $breftblinfo;
                    }
                }
            }
            $basefileData['REFFILE'] = $breftbldata;
        }
    }
    $rtnArr = array( 
                    'RTN' => $rtn ,
                    'MSG' => $msg ,
                    'BASEFILEDATA' => $basefileData
                );
                
    return $rtnArr;
}
// BASESQLDATA
function getBaseSQLData($db2con,$fdb2csv1){
    $rtn = 0;
    $msg = '';
    if($rtn === 0){
        $basesqldata = array();
        $basesqldata['D1NAME'] = $fdb2csv1['D1NAME'];
        $basesqldata['D1TEXT'] = $fdb2csv1['D1TEXT'];
        $basesqldata['D1LIBL'] = $fdb2csv1['D1LIBL'];
        $basesqldata['D1RDB'] = $fdb2csv1['D1RDB'] === '' ? RDB : $fdb2csv1['D1RDB']; 
        $res = fnGetBSQLDAT($db2con,$basesqldata['D1NAME']);
        if($res['result'] !== true ){
            $rtn = 1;
            $msg = showMsg($res['result']);
        }else{
            e_log('BASESQLDATA:'.print_r($basesqldata,true));
            $basesqldata['D1SQL'] = $res['data']['BEXESQL'];
        }
    }
    $rtnArr = array( 
                    'RTN' => $rtn ,
                    'MSG' => $msg ,
                    'BASESQLDATA' => $basesqldata
                );
                
    return $rtnArr;
}
function fnGetBSQLDAT($db2con,$QRYNM){
    $data = array();
    $params = array();
    
    $strSQL .= '    SELECT ';
    $strSQL .= '        BSQLNM, ';
    $strSQL .= '        BSQLFLG, ';
    $strSQL .= '        BEXESQL ';
    $strSQL .= '    FROM ';
    $strSQL .= '        BSQLDAT ';
    $strSQL .= '    WHERE ';
    $strSQL .= '        BSQLNM = ? AND ';
    $strSQL .= '        BSQLFLG = \'\' ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        array_push($params,$QRYNM);
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}
function getBREFTBLData($db2con,$QRYNM){
    $data = array();
    $params = array();
    
    $strSQL  =  '   SELECT A.RTQRYN ';
    $strSQL .=  '        , A.RTRSEQ ';
    $strSQL .=  '        , A.RTRFIL ';
    $strSQL .=  '        , A.RTRMBR ';
    $strSQL .=  '        , A.RTRLIB ';
    $strSQL .=  '        , A.RTDESC ';
    $strSQL .=  '        , CASE A.RTJTYP ';  
    $strSQL .=  '          WHEN \'2\' THEN \'4\' '; 
    $strSQL .=  '          WHEN \'3\' THEN \'5\' '; 
    $strSQL .=  '          WHEN \'1\' THEN \'6\' '; 
    $strSQL .=  '          WHEN \'7\' THEN \'8\' ';
    $strSQL .=  '          ELSE A.RTJTYP ';
    $strSQL .=  '        END AS RTJTYP ';
    $strSQL .=  '   FROM BREFTBL A ';
    $strSQL .=  '   WHERE A.RTQRYN = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        array_push($params,$QRYNM);
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
function getBREFFLDData($db2con,$QRYNM,$RTRSEQ){
    $data = array();
    
    $strSQL .= '    SELECT A.RFQRYN ';
    $strSQL .= '        , A.RFRSEQ ';
    $strSQL .= '        , A.RFFSEQ ';
    $strSQL .= '        , A.RFPFID ';
    $strSQL .= '        , A.RFPFNM  AS RFPF ';
    $strSQL .= '        , CASE A.RFPFID '; 
    $strSQL .= '            WHEN 0 THEN concat(\'P.\',A.RFPFNM) ';
    $strSQL .= '            ELSE  concat(concat(concat(\'S\', A.RFPFID),\'.\'),A.RFPFNM) ';
    $strSQL .= '        END AS RFPFNM '; 
    $strSQL .= '        , A.RFRFID '; 
    $strSQL .= '        , A.RFRFNM  AS RFRF ';
    $strSQL .= '        , concat(concat(concat(\'S\', A.RFRFID),\'.\'),A.RFRFNM) AS RFRFNM ';
    $strSQL .= '        , CASE A.RFRKBN ';
    $strSQL .= '            WHEN \'\' THEN \'EQ\' ';
    $strSQL .= '            ELSE  A.RFRKBN ';
    $strSQL .= '        END AS RFRKBN '; 
    $strSQL .= '    FROM   BREFFLD A ';
    $strSQL .= '    WHERE  A.RFQRYN = ? ';
    $strSQL .= '    AND  A.RFRSEQ = ? ';
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($QRYNM,$RTRSEQ);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}

/**
  *---------------------------------------------------------------------------
  * 区分データを取得
  * 
  * RESULT
  *    01：区分データリスト
  * 
  * @param String  func       区分種別キー値
  *
  *---------------------------------------------------------------------------
  **/
function getSYSCOLUMN2($db2con,$TABLE_NAME,$TABLE_SCHEMA,$LIBLIST,$FILTYP,$FILE){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL =' SELECT \'\' AS SEQ ';
    $strSQL .='       ,A.SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
    $strSQL .='       ,A.LENGTH ';
    $strSQL .='       ,CASE A.DDS_TYPE';
    $strSQL .='         WHEN \'H\' THEN \'A\'';
    $strSQL .='         WHEN \'J\' THEN \'0\'';
    $strSQL .='         WHEN \'E\' THEN \'0\'';
    $strSQL .='         ELSE A.DDS_TYPE ';
    $strSQL .='         END AS DDS_TYPE';
    $strSQL .='       ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG==='1'){
        $strSQL .='       ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .='       ,A.COLUMN_HEADING ';
    }
    $strSQL .='       ,\'\' AS FORMULA ';
    $strSQL .='       ,\'\' AS SORTNO ';
    $strSQL .='       ,\'\' AS SORTTYP ';
    $strSQL .='       ,\'\' AS EDTCD ';
    $strSQL .='       ,A.ORDINAL_POSITION ';
    $strSQL .='       , \''.$FILTYP.'\' AS FILTYP  ';
    $strSQL .='       , \''.$FILE.'\' AS FILE  ';

    $strSQL .=' FROM ' . SYSCOLUMN2 .' A ';
    $strSQL .=' WHERE A.TABLE_SCHEMA = ? ';
    $strSQL .=' AND A.TABLE_NAME = ? ';
    //$strSQL .=' AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\')';
//    $strSQL .=' AND A.DDS_TYPE NOT IN(\'5\')';
    $strSQL .=' ORDER BY A.ORDINAL_POSITION ';
    
    e_log("getSYSCOLUMN2 akz ".$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$TABLE_NAME);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
            //$data = umEx($data,true);
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME);
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                //$data = umEx($data,true);
                $data = umEx($data);
                $data = array('result' => true,'data' => $data);
            }
        }
        
    }
    return $data;
}

/*
function fnGetFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}*/

function chkChangeFile($oriLibl,$oriFile,$compLibl,$compFile,$orilibList,$complibList){
    if(cmMer($compLibl) === '' || cmMer($compLibl) === '*USRLIBL'){
        $compLibl = '*LIBL';
    }else{
        $compLibl = cmMer($compLibl);
    }
    $compFile = cmMer($compFile) ;
    if(cmMer($oriLibl) === '' || cmMer($oriLibl) === '*USRLIBL'){
        $oriLibl = '*LIBL';
    }else{
        $oriLibl = cmMer($oriLibl);
    }
    $oriFile  = cmMer($oriFile);
    $chgFileFlg = false;
    if($compLibl !== $oriLibl){
        $chgFileFlg = true;
    }else{
        if($oriLibl === '*LIBL'){
            if($orilibList !== $complibList){
                $chgFileFlg = true;
            }else{
                if($compFile !== $oriFile){
                    //e_log($compFile.'dfd'.$oriFile);
                    $chgFileFlg = true;
                }
            }
        }else{
            if($compFile !== $oriFile){
                $chgFileFlg = true;
            }
        }
    }
    return $chgFileFlg;
}
function fnGetBREFTBL($db2con,$D1NAME){
    $data = array();

    $params = array();

    $strSQL .= ' SELECT A.* ';
    $strSQL .= ' FROM BREFTBL as A ';
    $strSQL .= ' WHERE RTQRYN <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND RTQRYN = ? ';
        array_push($params,$D1NAME);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,false));
        }
    }
    return $data;
}