<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("base/comGetFDB2CSV2_CCSID.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */
$SEARCHDATA = (isset($_POST['SEARCHDATA'])?$_POST['SEARCHDATA']:'');
$D3CANL     = (isset($_POST['D3CANL'])?$_POST['D3CANL']:'');
$D3CANF     = (isset($_POST['D3CANF'])?$_POST['D3CANF']:'');
$D3CMBR     = (isset($_POST['D3CMBR'])?$_POST['D3CMBR']:'');
$D3CANC     = (isset($_POST['D3CANC'])?$_POST['D3CANC']:'');
$D3CANG     = (isset($_POST['D3CANG'])?$_POST['D3CANG']:'');
$D3NAMG     = (isset($_POST['D3NAMG'])?$_POST['D3NAMG']:'');
$D3NAMC     = (isset($_POST['D3NAMC'])?$_POST['D3NAMC']:'');
$D3CAST     = (isset($_POST['D3CAST'])?$_POST['D3CAST']:'');
$D3NAST     = (isset($_POST['D3NAST'])?$_POST['D3NAST']:'');
$D1NAME     = (isset($_POST['D1NAME'])?$_POST['D1NAME']:'');
$start      = (isset($_POST['start'])?$_POST['start']:0);
$length     = (isset($_POST['length'])?$_POST['length']:100);
$sort       = (isset($_POST['sort'])?$_POST['sort']:'');
$sortDir    = (isset($_POST['sortDir'])?$_POST['sortDir']:'');

/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$allcount = 0;
$data     = array();
$rtn      = 0;
$msg      = '';
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);


//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmCheckUser($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }
}
if($rtn === 0){
    $fdb2csv1data = FDB2CSV1_DATA($D1NAME); 
    if($fdb2csv1data['result'] !== true){
        $rtn = 1;
        $msg = showMsg($fdb2csv1data['result'],array('クエリー'));
    }else{
        $fdb2csv1data = umEx($fdb2csv1data['data']);
        if(count($fdb2csv1data) === 0){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET',array('クエリー'));
        }else{
            $fdb2csv1data = $fdb2csv1data[0];
            $LIBLLIST =  preg_split ("/\s+/", $fdb2csv1data['D1LIBL'] );
        }
    }
}
if($rtn === 0){
    if (strtoupper($D3CANL) === '*LIBL' || strtoupper($D3CANL) === '*USRLIBL'){
        $D1LIBL = $fdb2csv1data['D1LIBL'];
        cmDb2Close($db2con);
        $db2con = cmDb2ConLib($D1LIBL);
    }
}
if($fdb2csv1data['D1RDB'] === '' || $fdb2csv1data['D1RDB'] === RDB || $fdb2csv1data['D1RDB'] === RDBNAME){
    $db2RDBCon = $db2con;
}else{
    $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1data['D1RDB'];
    $db2RDBCon = cmDB2ConRDB($fdb2csv1data['D1LIBL'] );
}
if($rtn === 0){
    $rs = fnGetSYSCOLCCSID($db2RDBCon,$D3CANL,$D3CANF,$D3CANC,$LIBLLIST);
    if($rs['RTN'] !== 0){
        $rtn = 1;
        $msg = $rs['MSG'];
        e_log('CCSID取得エラー：'.print_r($rs,true).$D3CANL.$D3CANF.$D3CANC.print_r($LIBLLIST,true));
    }else{
        $D3CANCDATA = $rs['FLDDATA'];
        $FILINFO    = $rs['FILDATA'];
    }
}
if ($D3NAMG === '1') {
    if($rtn === 0){
        $rs = fnGetSYSCOLCCSID($db2RDBCon,$D3CANL,$D3CANF,$D3NAMC,$LIBLLIST);
        if($rs['RTN'] !== 0){
            $rtn = 1;
            $msg = $rs['MSG'];
        }else{
            $D3NAMCDATA = $rs['FLDDATA'];
        }
    }
}else{
    $D3NAMCDATA = array();
}
// メンバー取得
$D3CMBR = strtoupper($D3CMBR);
if($D3CMBR === '' || $D3CMBR === '*FIRST'){
    // 処理なし
    $D3CMBR = '';
}else if($D3CMBR === '*LAST'){
    $D3CMBR = fnGetFilLast($fdb2csv1data['D1RDB'],$fdb2csv1data['D1LIBL'],$$FILINFO['SYSTEM_TABLE_SCHEMA'],$FILINFO['SYSTEM_TABLE_NAME']);
}
if($rtn === 0){
    if(CRTQTEMPTBL_FLG === 1){
        e_log('【削除】テーブルタイプ取得にエラー：'.print_r($LIBLLIST,true));
        $tbltype = getTableType($fdb2csv1data['D1RDB'],$LIBLLIST,$D3CANL,$D3CANF,$D3CMBR);
        if($tbltype === 'L' || $tbltype === 'V' ){
            $rs = dropTmpFile($db2RDBCon,'QTEMP','QTEMP_'.$D3CANF);
            $rs = createTmpFil($db2RDBCon,$D3CANL,$D3CANF,$D3CMBR,'QTEMP_'.$D3CANF);
            if($rs !== true){
                e_log('テーブル設定失敗');
                $msg = showMsg($rs);
                $rtn = 1;
            }
        }else{
            //$rs = dropFileMBR($db2RDBCon,$D3CANL,$D3CANF,$D3CMBR,'QTEMP_'.$D3CANF);
            $rs = setFileMBR($db2RDBCon,$D3CANL,$D3CANF,$D3CMBR,'QTEMP_'.$D3CANF);
            if($rs !== true){
                e_log('テーブル設定失敗');
                $msg = showMsg($rs);
                $rtn = 1;
            }
        }
    }else{
        //$rs = dropFileMBR($db2RDBCon,$D3CANL,$D3CANF,$D3CMBR,'QTEMP_'.$D3CANF);
        $rs = setFileMBR($db2RDBCon,$D3CANL,$D3CANF,$D3CMBR,'QTEMP_'.$D3CANF);
        if($rs !== true){
            e_log('テーブル設定失敗');
            $msg = showMsg($rs);
            $rtn = 1;
        }
    }
}
if($rtn === 0){
    $rs = fnGetAllCount($db2RDBCon, $SEARCHDATA, $D3CANL, $D3CANF, $D3CANC, $D3CANG, $D3NAMG, $D3NAMC,$D3CANCDATA,$D3NAMCDATA);
    if ($rs['result'] !== true) {
        $rtn = 1;
        //$msg = showMsg('データが正しく取得できませんでした。<br/>検索候補設定を確認してください。');
        $msg = showMsg('FAIL_GETCAN',array('データ','検索候補'));
    } else {
        $allcount = $rs['data'];
    }
}
if ($rtn === 0) {
    $rs = fnGetCanData($db2RDBCon, $SEARCHDATA, $D3CANL, $D3CANF, $D3CANC, $D3CANG, $D3NAMG, $D3NAMC, $D3CAST, $D3NAST,$D3CANCDATA,$D3NAMCDATA, $start, $length);
    if ($rs['result'] !== true) {
        $rtn = 1;
        //$msg = showMsg('データが正しく取得できませんでした。<br/>検索候補設定を確認してください。');
        $msg = showMsg('FAIL_GETCAN',array('データ','検索候補'));
    } else {
        $data = $rs['data'];
    }
}
/*if($rtn === 0){
    if(CRTQTEMPTBL_FLG === 1){
        if($tbltype === 'L' || $tbltype === 'V' ){
            //$rs = dropTmpFile($db2RDBCon,'QTEMP','QTEMP_'.$D3CANF);
        }else{
            //$rs = dropFileMBR($db2RDBCon,$D3CANL,$D3CANF,$D3CMBR,'QTEMP_'.$D3CANF);
        }
    }else{
        //$rs = dropFileMBR($db2RDBCon,$D3CANL,$D3CANF,$D3CMBR,'QTEMP_'.$D3CANF);
    }
}*/
cmDb2Close($db2con);

if(($fdb2csv1data['D1RDB'] === '' || $fdb2csv1data['D1RDB'] === RDB || $fdb2csv1data['D1RDB'] === RDBNAME) === false){
    cmDb2Close($db2RDBCon);
}
/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo (json_encode($rtn));

/*
 *-------------------------------------------------------* 
 * ユーザーマスター取得
 *-------------------------------------------------------*
 */

function fnGetCanData($db2con, $SEARCHDATA, $D3CANL, $D3CANF, $D3CANC, $D3CANG, $D3NAMG, $D3NAMC, $D3CAST, $D3NAST,$D3CANCDATA,$D3NAMCDATA, $start = '', $length = '')
{
    $data = array();
    
    $params = array();
    $ORDERING = '';
    if ($D3CAST !== '' && $D3NAST !== '') {
        $ORDERING .= ' ORDER BY B.CAN,B.CANNAM ';
    } else if ($D3CAST !== '') {
        $ORDERING .= ' ORDER BY B.CAN ';
    } else if ($D3NAST !== '') {
        $ORDERING .= ' ORDER BY B.CANNAM ';
    }
    
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT ';
    $strSQL .= ' ROWNUMBER() OVER( '.$ORDERING;
    $strSQL .= ' ) AS rownum ';
    $strSQL .= ',B.CAN';
    if ($D3NAMG === '1') {
        $strSQL .= ' ,B.CANNAM';
    }
    $strSQL .= ' FROM ( ';
    $strSQL .= ' SELECT ' ;
    if($D3CANCDATA['CCSID'] === '65535'){

        if($D3CANCDATA['COLUMN_NAME'] === 'BREAKLVL' || $D3CANCDATA['COLUMN_NAME'] === 'OVERFLOW'){
            $strSQL .= 'CAST('.$D3CANC .' AS CHAR(4) CCSID 5026) AS CAN ';
        }else{
            $strSQL .= 'CAST('.$D3CANC .' AS CHAR('.$D3CANCDATA['LENGTH'].') CCSID 5026) AS CAN ';
        }


    }else{
        $strSQL .=  $D3CANC . ' AS CAN ';
    }
    if ($D3NAMG === '1') {
        if($D3NAMCDATA['CCSID'] === '65535'){

            if($D3CANCDATA['COLUMN_NAME'] === 'BREAKLVL' || $D3CANCDATA['COLUMN_NAME'] === 'OVERFLOW'){
                $strSQL .= ', CAST('.$D3NAMC .' AS CHAR(4) CCSID 5026) AS CANNAM ';
            }else{
                $strSQL .= ', CAST('.$D3NAMC .' AS CHAR('.$D3NAMCDATA['LENGTH'].') CCSID 5026) AS CANNAM ';
            }


        }else{
            $strSQL .= ' , ' . $D3NAMC . ' AS CANNAM ';
        }
    }
    /*if(strtoupper($D3CANL) === '*LIBL' || strtoupper($D3CANL) === '*USRLIBL'){
        $strSQL .= ' FROM ' . $D3CANF;
    }else{
        $strSQL .= ' FROM ' . $D3CANL . '/' . $D3CANF;
    }*/
    $strSQL .= ' FROM ' . 'QTEMP/QTEMP_'. $D3CANF ;
    if ($D3CANG === '1') {
        $strSQL .= ' GROUP BY ' . $D3CANC . ' ';
        if ($D3NAMG === '1') {
            $strSQL .= ' , ' . $D3NAMC . ' ';
        }
    }
    $strSQL .= ' ) as B ';
    if ($SEARCHDATA != '') {
        $strSQL .= '     WHERE B.CAN LIKE ? ';
        array_push($params, '%' . $SEARCHDATA . '%');
        if ($D3NAMG === '1') {
            $strSQL .= '     OR  B.CANNAM LIKE ?  ';
            array_push($params, '%' . $SEARCHDATA . '%');
        }
    } 
    $strSQL .= $ORDERING;
    $strSQL .= ' ) as A ';
    
    //抽出範囲指定
    if (($start != '') && ($length != '')) {
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params, $start + 1);
        array_push($params, $start + $length);
    }
  /*  if ($D3CAST !== '' && $D3NAST !== '') {
        $strSQL .= ' ORDER BY CAN,CANNAM ';
    } else if ($D3CAST !== '') {
        $strSQL .= ' ORDER BY CAN ';
    } else if ($D3NAST !== '') {
        $strSQL .= ' ORDER BY CANNAM ';
    }*/
    e_log('検索候補実行SQL：'.$strSQL);
    $stmt = db2_prepare($db2con, $strSQL);
    
    if ($stmt === false) {
        $data = array(
            'result' => false
        );
    } else {
        
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => false
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    
    return $data;
    
}

/*
 *-------------------------------------------------------* 
 * 全件カウント取得
 *-------------------------------------------------------*
 */

function fnGetAllCount($db2con, $SEARCHDATA, $D3CANL, $D3CANF, $D3CANC, $D3CANG, $D3NAMG, $D3NAMC,$D3CANCDATA,$D3NAMCDATA)
{
    $data   = array();
    $params = array();
    $strSQL = ' SELECT count(B.' . $D3CANC . ') as COUNT from ( ';
    //$strSQL = ' SELECT count(*) as COUNT from ( ';
    $strSQL .= ' SELECT  ';
    if($D3CANCDATA['CCSID'] === '65535'){
        if($D3CANCDATA['COLUMN_NAME'] === 'BREAKLVL' || $D3CANCDATA['COLUMN_NAME'] === 'OVERFLOW'){
            $strSQL .= 'CAST(A.'.$D3CANC .' AS CHAR(4) CCSID 5026) AS '.$D3CANC;
        }else{
            $strSQL .= 'CAST(A.'.$D3CANC .' AS CHAR('.$D3CANCDATA['LENGTH'].') CCSID 5026) AS '.$D3CANC;
        }
    }else{
        $strSQL .=  'A.'. $D3CANC . ' AS '.$D3CANC;
    }
    if ($D3NAMG === '1') {
        if($D3NAMCDATA['CCSID'] === '65535'){
            if($D3CANCDATA['COLUMN_NAME'] === 'BREAKLVL' || $D3CANCDATA['COLUMN_NAME'] === 'OVERFLOW'){
                $strSQL .= ', CAST(A.'.$D3NAMC .' AS CHAR(4) CCSID 5026) AS  '.$D3NAMC;
            }else{
                $strSQL .= ', CAST(A.'.$D3NAMC .' AS CHAR('.$D3NAMCDATA['LENGTH'].') CCSID 5026) AS  '.$D3NAMC;
            }
        }else{
            $strSQL .= ' , A.' . $D3NAMC . ' AS '.$D3NAMC;
        }
    }
    //$strSQL .= ' FROM ' . $D3CANL . '/' . $D3CANF . ' as A ';
    /*if(strtoupper($D3CANL) === '*LIBL' || strtoupper($D3CANL) === '*USRLIBL'){
        $strSQL .= ' FROM ' . $D3CANF. ' as A ';
    }else{
        $strSQL .= ' FROM ' . $D3CANL . '/' . $D3CANF. ' as A ';
    }*/
    $strSQL .= ' FROM ' . 'QTEMP/QTEMP_'. $D3CANF . ' AS A ';
    if ($D3CANG === '1') {
        $strSQL .= ' GROUP BY A.' . $D3CANC . ' ';
        if ($D3NAMG === '1') {
            $strSQL .= ' ,A.' . $D3NAMC . ' ';
        }
    }
    $strSQL .= ' ) as B ';
    if ($SEARCHDATA <> '') {
        $strSQL .= 'WHERE B.' . $D3CANC . '  LIKE ? ';
        array_push($params, '%' . $SEARCHDATA . '%');
        if ($D3NAMG === '1') {
            $strSQL .= 'OR B.' . $D3NAMC . ' LIKE ? ';
            array_push($params, '%' . $SEARCHDATA . '%');
        }
    }
    e_log('実行SQL:'.$strSQL);
    $stmt = db2_prepare($db2con, $strSQL);
    
    if ($stmt === false) {
        $data = array(
            'result' => false
        );
        e_log('error:'.db2_stmt_errormsg());
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $data = array(
                'result' => false
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                if($row === false){
                    e_log(db2_stmt_errormsg());
                }
                $data[] = $row['COUNT'];
            }
            $data = array(
                'result' => true,
                'data' => $data[0]
            );
        }
    }
    return $data;
}
