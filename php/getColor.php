<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("licenseInfo.php");
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$usrinfo = $_SESSION['PHPQUERY']['user'];
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$usrId = $usrinfo[0]['WUUID'];

$db2con = cmDb2Con();

//INT処理
cmSetPHPQUERY($db2con);

// ユーザがログアウトかどうかチェック
if($rtn === 0){
    if($_SESSION['PHPQUERY']['user'][0]['WUUID'] === null){
        $rtn = 1;
        $msg = showMsg('CHECK_LOGOUT',array('ユーザー'));
    }
}


//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = fnGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

$DB2WUSR = fnGetDB2WUSR($db2con,$usrId);

$DB2WUSR = umEx($DB2WUSR);
//システムリスト
$sysMenuList   = cmGetDB2COF($db2con);
if ($sysMenuList['result'] === true) {
    $syscouqfg = $sysMenuList['data'][0]['COUQFG'];
}

cmDb2Close($db2con);

$wuclr1 = $DB2WUSR[0]['WUCLR1'];
$wuclr2 = $DB2WUSR[0]['WUCLR2'];
$wuclr3 = $DB2WUSR[0]['WUCLR3'];
$wuauth =  $DB2WUSR[0]['WUAUTH'];

$wuclr1 = (($wuclr1 === '')?$licenseColor1:$wuclr1);
$wuclr2 = (($wuclr2 === '')?$licenseColor2:$wuclr2);
$wuclr3 = (($wuclr3 === '')?$licenseColor3:$wuclr3);

$rtnArray = array(
    'RTN'    => $rtn,
    'MSG'    => $msg,
    'WUCLR1' => $wuclr1,
    'WUCLR2' => $wuclr2,
    'WUCLR3' => $wuclr3,
    'session' => $_SESSION,
    'WUAUTH' => $wuauth,

);

echo(json_encode($rtnArray));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2WUSR($db2con,$id){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WUSR as A ';
    $strSQL .= ' WHERE WUUID <> \'\' ';
    
    if($id != ''){
        $strSQL .= ' AND WUUID = ? ';
        array_push($params,$id);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }

    return $data;

}

/*
*-------------------------------------------------------* 
* ログインユーザが削除されるかどうかチェック
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $id     ユーザーID
* 
*-------------------------------------------------------*
*/

function fnGetWUAUTH($db2con,$id){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WUSR as A ';
    $strSQL .= ' WHERE WUUID <> \'\' ';
    
    if($id != ''){
        $strSQL .= ' AND WUUID = ? ';
        array_push($params,$id);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;

}