<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("licenseInfo.php");
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$name = '';
$rtn = 0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rs = fnGetDB2WCPN($db2con);

cmDb2Close($db2con);

if($rtn === 0){
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $name = cmHsc($rs['data']['KEY000']);
        if($name === ''){
            $name = $licenseCompanyName;
        }
        $data['NAME'] = $name;
    }
}
/**return**/
$rtn = array(
    'aaData' => $data,
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2WCPN($db2con){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WCPN AS A ' ;

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data['KEY'.$row['WCKEY']] = cmMer($row['WCVAL']);
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}