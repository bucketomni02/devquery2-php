<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
$ver = phpversion();
$sver = substr($ver,0,3);
$phpversion = str_replace('.','',$sver);

$ary = array(
    'IP' => IP,
    'PROTOCOL'     => PROTOCOL,
    'SISTEM'       => SISTEM,
    'SAVE_DIR'     => SAVE_DIR,
    'EXCELVERSION' => EXCELVERSION,
	'BASE_DIR'     => BASE_DIR,
	'BASE_URL'     => BASE_URL,
    'MAINLIB'      => MAINLIB,
    'JSTIMEOUT'    => JSTIMEOUT,
    'CND_EQ'       => CND_EQ,
    'CND_NE'       => CND_NE,
    'CND_GT'       => CND_GT,
    'CND_LT'       => CND_LT,
    'CND_GE'       => CND_GE,
    'CND_LE'       => CND_LE,
    'CND_LIKE'     => CND_LIKE,
    'CND_LLIKE'    => CND_LLIKE,
    'CND_RLIKE'    => CND_RLIKE,
    'CND_NLIKE'    => CND_NLIKE,
    'CND_LNLIKE'   => CND_LNLIKE,
    'CND_RNLIKE'   => CND_RNLIKE,
    'CND_RANGE'    => CND_RANGE,
    'CND_LIST'     => CND_LIST,
    'CND_NLIST'    => CND_NLIST,
    'CND_IS'       => CND_IS,
    'DSPROW1'      => DSPROW1,
    'DSPROW2'      => DSPROW2,
    'DSPROW3'      => DSPROW3,
    'DSPROW4'      => DSPROW4,
    'INTDSPROW'      => INTDSPROW,
	'phpversion' => (int)$phpversion,
    'PVFLG'      => CND_PVFLG,
    'SRSAVE'      => CND_SRSV
,

);

echo(json_encode($ary));
