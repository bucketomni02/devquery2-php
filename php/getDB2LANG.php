<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("licenseInfo.php");
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$name = '';
$rtn = 0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rs = fnGetDB2LANG($db2con);
if($rtn === 0){
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $langdata = $rs['data'];
    }
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'aaData' => $langdata,
    'LANGDATA' => umEx($langdata,true),
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 言語取得
*-------------------------------------------------------*
*/

function fnGetDB2LANG($db2con){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2LANG AS A ' ;
    $strSQL .= ' ORDER BY LANID ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            foreach($data as $key => $rowData){
                $data[$key]['LANNM'] = cmHsc($rowData['LANNM']);
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}