<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("./common/inc/config.php");
include_once("./common/inc/common.inc.php");
include_once("./licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$FLAG=(isset($_POST['FLAG']))?$_POST['FLAG']:'';
$FILETYPE=(isset($_POST['filetype']))?$_POST['filetype']:'';
$D1NAME=(isset($_POST['d1name']))?$_POST['d1name']:'';
$PIVOTKEY=(isset($_POST['pivotkey']))?$_POST['pivotkey']:'';
$GRAPHKEY=(isset($_POST['graphkey']))?$_POST['graphkey']:'';
$isQGflg = false;
if($FILETYPE === 'qgroupsearch' || $FILETYPE === 'pgroupsearch' || $FILETYPE === 'ggroupsearch' || $FILETYPE === '1'){
    $isQGflg = true;
}
if($FILETYPE === 'resultgridqgroup' || $FILETYPE === 'resulthtmlqgroup' || $FILETYPE === 'resulthtmltemplateqgroup' || $FILETYPE === 'pivotresultpgroup' || $FILETYPE === 'graphresultggroup'){
    $isQGflg = true;
}
$usrinfo         = (($touch == '1') ? $_SESSION['PHPQUERYTOUCH']['user'] : $_SESSION['PHPQUERY']['user']);
$bmkexist = false;
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
//リターン配列
$rtnArray = array();
$rtn = 0;
$msg = '';
$gkeyDCount=0;
$dtotalcount=0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//クエリー、ピボット、グラフが存在してるかどうかのチェック
$QBMFLG = 0;
if($isQGflg){
    $QBMFLG = 1;
    $rs=cmGetQueryGroup($db2con,$D1NAME,$PIVOTKEY,$GRAPHKEY);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('クエリー'));
    }else{
        $D1NAMELIST=umEx($rs['data'],true);
    }
}
if ($rtn === 0) {
    if($isQGflg){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            if($res['LASTFLG']==='1'){
                $LASTD1NAME=$res['QRYGSQRY'];
            }
            $chkQry = cmChkQueryGroup($db2con,$usrinfo[0]['WUUID'],$D1NAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if ($chkQry['result'] === 'NOTEXIST_GPH') {
                $rtn = 2;
                $msg = showMsg('NOTEXIST_GET', array(
                    'グラフ'
                ));
                break;
            }else if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 2;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }//end of for loop
    }else{
        $chkQry = array();
        $chkQry = cmChkQuery($db2con, $usrinfo[0]['WUUID'], $D1NAME,$PIVOTKEY,$GRAPHKEY);
        if ($chkQry['result'] === 'NOTEXIST_GPH') {
            $rtn = 2;
            $msg = showMsg('NOTEXIST_GET', array(
                'グラフ'
            ));
        } else if ($chkQry['result'] === 'NOTEXIST_PIV') {
            $rtn = 2;
            $msg = showMsg('NOTEXIST_GET', array(
                'ピボット'
            ));
        } else if ($chkQry['result'] !== true) {
            $rtn = 2;
            $msg = showMsg($chkQry['result'], array(
                'クエリー'
            ));
        }
    }
}
if($rtn === 0){
    $rs = cmGetDB2QBMK($db2con,$usrinfo[0]['WUUID'], $D1NAME,$PIVOTKEY,$GRAPHKEY,$QBMFLG);
    if($rtn === 0){
        if($rs['result'] !== true){
            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            if($rs['datacount'] > 0){
                $bmkexist = true;
            }
        }
    }
}
if ($rtn === 0) {//MSM add for bmfoler icon show/hide
    $rs = cmGetWUUBFG($db2con, $usrinfo[0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result']);
    } else {
        $WUUBFG = $rs['data'][0]['WUUBFG'];
    }
}

cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'BMKEXIST'=> $bmkexist,
    'WUUBFG' => $WUUBFG
);
echo(json_encode($rtnArray));


