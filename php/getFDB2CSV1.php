<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$touch = (isset($_POST['TOUCH'])?$_POST['TOUCH']:'0');
$d1name = $_POST['D1NAME'];
$usrinfo = (($touch == '1')?$_SESSION['PHPQUERYTOUCH']['user']:$_SESSION['PHPQUERY']['user']);
$subtyp = (isset($_POST['SUBTYP'])?$_POST['SUBTYP']:'');
$pmpkey ='';
$gphkey ='';
if($subtyp==='pivot' || $subtyp==='pgroupsearch' ){
    $pmpkey = (isset($_POST['SUBKEY'])?$_POST['SUBKEY']:'');
}else if($subtyp === 'graph' || $subtyp==='ggroupsearch'){
    $gphkey = (isset($_POST['SUBKEY'])?$_POST['SUBKEY']:'');
}

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$csvFlg = true;
$excelFlg = true;
$BMKTOTAL=0;
//一番最後のクエリーを知るため
$D1NAMELIST=array();
$LASTD1NAME='';

$bmkexist = false;
//グループかクエリー
$QBMFLG = 0;

/*
*-------------------------------------------------------*
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmCheckUser($db2con,$usrinfo[0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
if($rtn===0){
    if($subtyp ==='qgroupsearch' || $subtyp ==='pgroupsearch'  || $subtyp ==='ggroupsearch'){
        $QBMFLG = 1;
        $rs=cmGetQueryGroup($db2con,$d1name,$pmpkey,$gphkey);
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリー'));
        }else{
            if(!$licenseSql){
                error_log('license sql = '.$licenseSql);
                foreach($rs['data'] as $key => $res){
                    if($res['D1CFLG'] === ''){
                        array_push($D1NAMELIST,$rs['data'][$key]);
                    }      
                }
                $D1NAMELIST[count($D1NAMELIST)-1]['LASTFLG'] = '1';
                $D1NAMELIST = umEx($D1NAMELIST,true);
            }else{
                $D1NAMELIST=umEx($rs['data'],true);
            }
        }
    }
}
if($rtn === 0){
    if($subtyp ==='qgroupsearch' || $subtyp ==='pgroupsearch' || $subtyp ==='ggroupsearch'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            if($res['LASTFLG']==='1'){
                $LASTD1NAME=$res['QRYGSQRY'];
            }
            $chkQry = cmChkQueryGroup($db2con,$usrinfo[0]['WUUID'],$d1name,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if ($chkQry['result'] === 'NOTEXIST_GPH') {
                $rtn = 2;
                $msg = showMsg('NOTEXIST_GET', array(
                    'グラフ'
                ));
                break;
            }else if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 2;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }
    }else{
        $chkQry = array();
        $chkQry = cmChkQuery($db2con,$usrinfo[0]['WUUID'],$d1name,$pmpkey,$gphkey);
        if ($chkQry['result'] === 'NOTEXIST_GPH') {
            $rtn = 2;
            $msg = showMsg('NOTEXIST_GET', array(
                'グラフ'
            ));
        }else if($chkQry['result'] === 'NOTEXIST_PIV'){
            $rtn = 2;
            $msg = showMsg('NOTEXIST_GET',array('ピボット'));
        }else if($chkQry['result'] !== true){
            $rtn = 2;
            $msg = showMsg($chkQry['result'],array('クエリー'));
        }
    }
}

if($rtn===0){
    if($subtyp ==='qgroupsearch' || $subtyp ==='pgroupsearch' || $subtyp ==='ggroupsearch'){
        foreach($D1NAMELIST as $res){
            $rs = fnGetFDB2CSV1($db2con,$res['QRYGSQRY'],$res['PMPKEY'],$res['GPHKEY']);
            if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                    break;
            }else{
                if(count($rs['data']) === 0){
                    $rtn = 1;
                    $msg = showMsg('NOTEXIST_GET',array('クエリー'));
                    break;
                }else{
                    $rsData=umEx($rs['data'],true);
                    foreach($rsData as $res){
                        $data[] =$res ;
                    }
                }
            }
        }
    }else{
        $rs = fnGetFDB2CSV1($db2con,$d1name,$pmpkey,$gphkey);
        if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
        }else{
            if(count($rs['data']) === 0){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET',array('クエリー'));
            }else{
                $data = umEx($rs['data'],true);
            }
        }
    }
}
//CSV、EXCELダウンロト権限チェックためフラグ取得
if($rtn === 0){
    if($subtyp ==='qgroupsearch' || $subtyp ==='pgroupsearch' || $subtyp ==='ggroupsearch'){
        if($LASTD1NAME !==''){
            $res = fnGetDB2WDEF($db2con,$usrinfo[0]['WUUID'],$LASTD1NAME);
            if($res['result'] !== true){
                $rtn = 1;
                $msg =  showMsg($res['result']);
            }else{
                $DB2WDEF = $res['data'];
                if($DB2WDEF['WDDWNL'] !== "1"){
                    $csvFlg = false;
                }
                if($DB2WDEF['WDDWN2'] !== "1"){
                    $excelFlg = false;
                }
            }
        }
    }else{
        $res = fnGetDB2WDEF($db2con,$usrinfo[0]['WUUID'],$d1name);
        if($res['result'] !== true){
            $rtn = 1;
            $msg =  showMsg($res['result']);
        }else{
            $DB2WDEF = $res['data'];
            if($DB2WDEF['WDDWNL'] !== "1"){
                $csvFlg = false;
            }
            if($DB2WDEF['WDDWN2'] !== "1"){
                $excelFlg = false;
            }
        }
    }
}
//ブックマークカウント
if($rtn === 0){
    if($subtyp === 'graph'){
        $res = cmGETDB2BMKCOUNT($db2con,$d1name,$gphkey,$usrinfo[0]['WUUID']);
        if($res['result'] !== true){
            $rtn = 1;
            $msg =  showMsg($res['result']);
        }else{
            $BMKTOTAL=$res['TOTALCOUNT'];
        }
    }else if($subtyp ==='ggroupsearch'){
        if($LASTD1NAME !==''){
            $res = cmGETDB2BMKCOUNT($db2con,$LASTD1NAME,$gphkey,$usrinfo[0]['WUUID']);
            if($res['result'] !== true){
                $rtn = 1;
                $msg =  showMsg($res['result']);
            }else{
                $BMKTOTAL=$res['TOTALCOUNT'];
            }
        }
    }
}
if($rtn === 0){
    $rs = cmGetDB2QBMK($db2con,$usrinfo[0]['WUUID'], $d1name,$pmpkey,$gphkey,$QBMFLG);
    if($rtn === 0){
        if($rs['result'] !== true){
            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            if($rs['datacount'] > 0){
                $bmkexist = true;
            }
        }
    }
}
if ($rtn === 0) {//MSM add for bmfoler icon show/hide
    $rs = cmGetWUUBFG($db2con, $usrinfo[0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result']);
    } else {
        $WUUBFG = $rs['data'][0]['WUUBFG'];
    }
}
//検索画面で権限チェック MSM add 20190405
if ($rtn === 0) {
    $chkQry = array();
    $chkQry = cmChkQuery($db2con, $usrinfo[0]['WUUID'], $d1name, $pmpkey, $gphkey);

    error_log("cmChkQuery getFDB2CSV1.php => ".print_r($chkQry,true));
    error_log("cmChkQuery COUNT getFDB2CSV1.php => ".$chkQry['COUNT']);

    if($usrinfo[0]['WUAUTH'] === "3" || $usrinfo[0]['WUAUTH'] === '4'){
        $chkQryUsr = chkVldQryUsr($db2con,$d1name,$usrinfo[0]['WUAUTH']);

        if($chkQryUsr === 'NO_QRY'){//NOTEXIST_GET MSM change 20190405 クエリーが存在しないのか、権限がないのか、区別できるようにする
            $rtn = 2;
            $msg = showMsg('NO_QRY', array(
                'クエリー'
            ));
        }else if($chkQryUsr === 'NO_PERMIT'){

            $rtn = 2;
            $msg = showMsg('NO_PERMIT', array(
                'クエリー'
            ));//MSM change 20190405 クエリーが存在しないのか、権限がないのか、区別できるようにする
            //error_log("chkQryUsr NO_PERMIT MSG => ".$rtn);
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }

        //作成権限がない場合でもグループ権限があれば実行可能
        if($rtn !== 0){
            if($chkQry['COUNT'] > 0){
                $rtn = 0;
            }
        }
    }
    
}

if($subtyp === 'graph' || $subtyp ==='ggroupsearch'){
    $csvFlg = false;
    $excelFlg = false;
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'DATA' =>$data ,
    'RTN' => $rtn,
    'MSG' => $msg,
    'csvFlg'=> $csvFlg,
    'excelFlg'=> $excelFlg,
    'chkQry' =>$chkQry,
    'BMKTOTAL'=>$BMKTOTAL,
    'LASTD1NAME'=>$LASTD1NAME,
    'GROUPDATA'=>$D1NAMELIST,
    'BMKEXIST'=> $bmkexist,
    'WUUBFG' => $WUUBFG
);
echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con,$d1name,$pmpkey,$gphkey){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.*,B.*,C.* '; 
    $strSQL .= ' FROM FDB2CSV1 AS A ';
    $strSQL .= ' LEFT JOIN DB2PMST AS B ';
    $strSQL .= ' ON D1NAME = PMNAME AND PMPKEY = ? ';
    $strSQL .= ' LEFT JOIN DB2GPK AS C ';
    $strSQL .= ' ON D1NAME = GPKQRY AND GPKID = ? ';
    $strSQL .= ' WHERE D1NAME = ? ';
    $params[]=$pmpkey;
    $params[]=$gphkey;
    $params[]=$d1name;
    /*e_log('in getFDB2CSV1 PARAMS'.print_r($params,true));
    e_log('in getFDB2CSV1 SQL->'.$strSQL);*/
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* CSV、EXCEL権限取得
*-------------------------------------------------------*
*/

function fnGetDB2WDEF($db2con,$WDUID,$WDNAME){

    $data = array();    
    $strSQL  = ' SELECT A.WDDWNL,A.WDDWN2 ';
    $strSQL .= ' FROM DB2WDEF AS A ' ;
    $strSQL .= ' WHERE WDUID = ? ' ;
    $strSQL .= ' AND WDNAME = ? ' ;
    $params = array(
          $WDUID
        , $WDNAME
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{

        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => $data[0]
            );
        }
    }
    return $data;
}