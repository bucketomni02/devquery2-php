<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$d1name = $_POST['D1NAME'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'14',$userData[0]['WUSAUT']);//'14' =>制御レベル設定
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('制御レベル設定の権限'));
            }
        }
    }
}
if($rtn === 0){
    //ユーザー名取得
    $rs = fnGetFDB2CSV2($db2con,$d1name);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result'],array('クエリー'));
        $rtn = 2;
    }else{
        $data = $rs['data'];
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'DATA' => umEx($data),
    'aaData' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));


/*
 *-------------------------------------------------------*
 * FDB2CSV2取得
 *-------------------------------------------------------*
*/
function fnGetFDB2CSV2($db2con, $D1NAME) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT A.* FROM ( ';
    $strSQL.= ' SELECT D2NAME,D2FILID,D2FLD,D2HED,D2CSEQ,D2WEDT, ';
    $strSQL.= ' D2TYPE,D2LEN,D2DEC,D2DNLF ';
    $strSQL.= ' FROM FDB2CSV2 ';
    $strSQL.= ' WHERE D2NAME = ? ';
    //$strSQL.= ' AND D2CSEQ > 0 ';
    $strSQL.= ' UNION ALL ';
    $strSQL.= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID, ';
    $strSQL.= ' D5FLD AS D2FLD,D5HED AS D2HED,D5CSEQ AS D2CSEQ, ';
    $strSQL.= ' D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DEC AS D2DEC,D5DNLF AS D2DNLF ';
    $strSQL.= ' FROM FDB2CSV5 ';
    $strSQL.= ' WHERE D5NAME = ?  ';//AND D5CSEQ > 0
    $strSQL.= ' ) AS A ';
    //$strSQL.= 'ORDER BY A.D2CSEQ ASC ';
    $strSQL.= 'ORDER BY CASE WHEN A.D2CSEQ=0 THEN NULL ELSE A.D2CSEQ END ASC ';
    $params = array($D1NAME, $D1NAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                foreach ($row as $k => $v) {
                    $row[$k] = cmMer($v);
                }
                $data[] = $row;
            }
            if (count($data) === 0 && $gphFlg !== true) {
                $data = array('result' => 'NOTEXIST_GET');
            } else {
                $data = array('result' => true, 'data' => $data);
            }
        }
    }
    return $data;
}