<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */

$d1name   = (isset($_POST['D1NAME'])) ? $_POST['D1NAME'] : '';
$d1event  = (isset($_POST['D1EVENT'])) ? $_POST['D1EVENT'] : '';
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$allcount = 0;
$data     = array();
$rtn      = 0;
$msg      = '';
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */
$csv_d    = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
          
            $rs = cmChkKenGen($db2con,'19',$userData[0]['WUSAUT']);//'1' => mailMaster
       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('CL連携'));
            }
        }
    }
}

if ($rtn === 0) {
    //ユーザー名取得
    $rs = cmGetFDB2CSV2($db2con, $d1name,false,false,false);
    if ($rs['result'] !== true) {
        $msg = showMsg($rs['result'], array(
            'クエリー'
        ));
        $rtn = 1;
    } else {
        $data = umEx($rs['data']);
    }
}
if ($rtn === 0) {
    $rs = fnGetFDB2CSV1PM($db2con, $d1name, $d1event);
    if ($rs['result'] !== true) {
        $msg = showMsg($rs['result'], array(
            'クエリー'
        ));
        $rtn = 1;
    } else {
        $datapm = umEx($rs['data']);
    }
}
e_log('data' . print_r($data, true));
e_log('datapm' . print_r($datapm, true));
foreach ($data as $key => $value) {
    $data[$key]['TXTFLG'] = '';
    foreach ($datapm as $pkey => $pvalue) {
        // if($data[$key]['D2NAME'] === $datapm[$pkey]['DMNAME'] && $data[$key]['D2FILID'] === $datapm[$pkey]['DMSCID'] && $data[$key]['D2FLD'] === $datapm[$pkey]['DMSCFD']){
        //  if($value['D2FLD'] === $pvalue['DMSCFD']){//障害ー307の対応でコンメントアウトした
        //障害ー307の対応で 20170126 SYPS 追加        
        if (($value['D2NAME'] === $pvalue['DMNAME']) && ($value['D2FILID'] === $pvalue['DMSCID']) && ($value['D2FLD'] === $pvalue['DMSCFD'])) {
            $data[$key]['TXTFLG']  = $datapm[$pkey]['DMSEQ'];
            $data[$key]['DMPTYPE'] = $datapm[$pkey]['DMPTYPE'];
            $data[$key]['DMLEN']   = $datapm[$pkey]['DMLEN'];
            $data[$key]['DMSCAL']  = $datapm[$pkey]['DMSCAL'];
        }
    }
}


cmDb2Close($db2con);

/**return**/
$rtn = array(
    'aaData' => $data,
    'RTN' => $rtn,
    'MSG' => $msg,
    'datapm' => $datapm
);

echo (json_encode($rtn));

