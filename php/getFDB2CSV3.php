<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* ライセンス管理　
*-------------------------------------------------------*
*/
include_once("licenseInfo.php");

/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/
$user = $_POST['USER'];
$pass = $_POST['PASS'];
$url_ = $_POST['URL_CHK'];
$url_cs = $_POST['URL_CS'];
$URL_CHK = true;

$PivotFlg = true;       //ピボット実行権限チェックフラグ
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtnArray = array();
$rtn = 0;
$msg = '';
$rs = '';
$url = '';
$usrinfo = array();

//ピボット実行権限チェック
if($licensePivot == false){
    $PivotFlg = false;
}

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rtn = 0;
 $d1name = (isset($_POST['D1NAME']))?$_POST['D1NAME']:'';
    $data =  cmGetFDB2CSV3($db2con , $d1name);
    if($data === false){
        $rtn = 1;
        $msg = showMsg('FAIL_FUNC',array('データ取得'));//'データ取得に失敗しました。';
     }

cmDb2Close($db2con);

/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA'=>$data
);
echo(json_encode($rtnArray));

