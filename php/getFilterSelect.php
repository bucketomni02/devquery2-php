<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$wpname = $_POST['WPNAME'];
$wppkey = $_POST['WPPKEY'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmCheckUser($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$wpname,$wppkey);
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 2;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 2;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($rtn === 0){
    $data = fnGetDB2PCOL($db2con,$wpname,$wppkey);

    if(count($data) === 0){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET_QRY',array('データ','ピボット'));// 'データが取得できませんでした。ピボット設定が削除されている可能性があります。';
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'DATA' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* DB2PCOL取得
*-------------------------------------------------------*
*/

function fnGetDB2PCOL($db2con,$wpname,$wppkey){

    $data = array();

    $params = array();

    /*$strSQL .= ' SELECT DISTINCT B.WPFILID,B.WPFLD,C.D2HED,C.D2TYPE FROM ( ';
    $strSQL .= ' SELECT A.WPPFLG,A.WPSEQN,A.WPFILID,A.WPFLD ';
    $strSQL .= ' FROM DB2PCOL AS A ';
    $strSQL .= ' WHERE WPNAME = ? ';
    $strSQL .= ' AND WPPKEY = ? ';
    $strSQL .= ' AND ( WPPFLG = ? OR WPPFLG = ? ) ';
    $strSQL .= ' ORDER BY A.WPPFLG,A.WPSEQN ';
    $strSQL .= ' ) AS B LEFT JOIN FDB2CSV2 AS C ON ';
    $strSQL .= ' B.WPFILID = C.D2FILID AND B.WPFLD = C.D2FLD ';*/

    $strSQL  = ' SELECT DISTINCT B.WPFILID,B.WPFLD,C.D2HED,C.D2TYPE FROM ( ';
    $strSQL .= ' SELECT A.WPPFLG,A.WPSEQN,A.WPFILID,A.WPFLD ';
    $strSQL .= ' FROM DB2PCOL AS A ';
    $strSQL .= ' WHERE WPNAME = ? ';
    $strSQL .= ' AND WPPKEY = ? ';
    $strSQL .= ' AND ( WPPFLG = ? OR WPPFLG = ? ) ';
    $strSQL .= ' ORDER BY A.WPPFLG,A.WPSEQN ';
    $strSQL .= ' ) AS B ';
    $strSQL .= ' LEFT JOIN (';
    $strSQL .= ' SELECT D.* FROM ( ';
    $strSQL .= ' SELECT D2NAME,D2FILID,D2FLD,D2HED,D2CSEQ,D2WEDT, ';
    $strSQL .= ' D2TYPE,D2LEN,D2DNLF ';
    $strSQL .= ' FROM FDB2CSV2 ' ;
    $strSQL .= ' WHERE D2NAME = ? ';
    $strSQL .= ' AND D2CSEQ > 0 ';
    $strSQL .= ' UNION ALL ';
    $strSQL .= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID, ';
    $strSQL .= ' D5FLD AS D2FLD,D5HED AS D2HED,D5CSEQ AS D2CSEQ, ';
    $strSQL .= ' D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DNLF AS D2DNLF ';
    $strSQL .= ' FROM FDB2CSV5 ';
    $strSQL .= ' WHERE D5NAME = ? AND D5CSEQ > 0 ';
    $strSQL .= ' ) AS D )';
    $strSQL .= ' AS C ON ';
    $strSQL .= ' B.WPFILID = C.D2FILID AND B.WPFLD = C.D2FLD ';

    $params = array($wpname,$wppkey,'1','2',$wpname,$wpname);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}