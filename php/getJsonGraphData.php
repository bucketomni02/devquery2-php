<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("common/inc/config.php");
include_once ("common/inc/common.inc.php");
include_once ("base/comGetFDB2CSV2_CCSID.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
$d1name = $_POST['D1NAME'];
$QRYNAME = $d1name;
$GPHKEY = $_POST['GPHKEY'];
$filename = $_POST['csvfilename'];
$oldfilename = $_POST['oldFilename'];
$isQGflg = (isset($_POST['ISQGFLG']) ? $_POST['ISQGFLG'] : ''); //クエリーグループならＴＲＵＥ
$filtersearchData = (isset($_POST['filtersearchData']) ? json_decode($_POST['filtersearchData'], true) : array());
$aaData = array();
$checkRs = array();
$LINEARR = array(); //GPCFM => 1
$LINE = array();
$BARARR = array(); //GPCFM => 2
$BAR = array();
$AREAARR = array(); //GPCFM => 3
$AREA = array();
$PIEARR = array(); //GPCFM => 4
$PIE = array();
$BMKTOTAL = 0;
$D1TEXT = '';
$GHEADER = array();
/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
//テーブルある無しフラグ
$rs = true;
$rtn = 0;
$errorFlg = 0;
//クエリーグループ
$D1NAMELIST = array(); //クエリーグループの全部データ
$LASTD1NAME = ''; //クエリーグループの一番最後のクエリーＩＤ
$isQGflg = ($isQGflg === 'true') ? true : false;

$bmkexist = false;
//グループかクエリー
$QBMFLG = 0;

/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmCheckUser($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array('ユーザー'));
    }
}
//クエリーグループ
if ($rtn === 0) {
    if ($isQGflg) {
        $QBMFLG = 1;
        $rs = cmGetQueryGroup($db2con, $d1name, '', '');
        if ($rs['result'] !== true) {
            $rtn = 2;
            $msg = showMsg($rs['result'], array('クエリー'));
        } else {
            if(!$licenseSql){
                foreach($rs['data'] as $key => $res){
                    if($res['D1CFLG'] === ''){
                        array_push($D1NAMELIST,$rs['data'][$key]);
                    }      
                }
                $D1NAMELIST[count($D1NAMELIST)-1]['LASTFLG'] = '1';
                $D1NAMELIST = umEx($D1NAMELIST,true);
            }else{
                $D1NAMELIST=umEx($rs['data'],true);
            }
        }
    }
}
if ($rtn === 0) {
    $chkQry = array();
    if ($isQGflg) {
        foreach ($D1NAMELIST as $key => $res) {
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID'], $d1name, $res['QRYGSQRY'], $res['LASTFLG'], '', $res['GPHKEY']);
            if ($chkQry['result'] !== true) {
                $rtn = 2;
                $msg = showMsg($chkQry['result'], array('クエリー'));
                break;
            } else {
                //一番最後の
                if ($res['LASTFLG'] === '1') {
                    $d1name = $res['QRYGSQRY'];
                    $D1TEXT.= $chkQry['FDB2CSV1']['D1TEXT'];
                } else {
                    $D1TEXT.= $chkQry['FDB2CSV1']['D1TEXT'] . ',';
                }
            }
        } //end of for loop
        
    } else {
        $chkQry = array();
        $chkQry = cmChkQuery($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID'], $d1name, '', $GPHKEY);
        if ($chkQry['result'] !== true) {
            $rtn = 2;
            $msg = showMsg($chkQry['result'], array('クエリー'));
        } else {
            $D1TEXT = $chkQry['FDB2CSV1']['D1TEXT'];
        }
    }
}
if ($rtn === 0) {
    $db2connect = '';
    $fdb2csv1Data = FDB2CSV1_DATA($d1name);
    $RDBNM = cmMer($fdb2csv1Data['data'][0]['D1RDB']);
    $D1CFLG = cmMer($fdb2csv1Data['data'][0]['D1CFLG']);
    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
        //$systables = cmGetSystables($db2con, SAVE_DB, $filename);
        $db2connect = $db2con;
    } else {
        $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
        $db2RDBcon = cmDB2ConRDB(SAVE_DB);
        $db2connect = $db2RDBcon;
        // $systables = cmGetSystables($db2RDBcon, SAVE_DB, $filename);
        
    }
    $systables = cmGetSystables($db2connect, SAVE_DB, $filename);
    if (count($systables) > 0) {
        //グラフ存在チェック
        $rs = cmCheckDB2GPK($db2con, $d1name, $GPHKEY);
        if ($rs['rs'] === true) {
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET', array('グラフ'));
        } else if ($rs['rs'] !== 'ISEXIST') {
            $rtn = 1;
            $msg = showMsg($rs['rs'], array('グラフ'));
        }
        if ($rtn === 0) {
            $rs = cmSelDB2GPK($db2con, $d1name, $GPHKEY);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result'], array('グラフ'));
            } else {
                $gpk = umEx($rs['data'], true);
                $xcol = $gpk[0]['GPKXCO'] . '_' . $gpk[0]['GPKXID'];
                foreach ($gpk as $key => $val) {
                    $gpk[$key][$xcol] = $val['GPKHED'];
                    $GHEADER[0][$xcol] = $val['GPKHED'];
                    //FILD=9999の場合は無条件でarray()
                    if ($gpk[0]['GPKXID'] === '9999') {
                        $checkRs = array();
                    } else {
                        $checkRs = cmCheckFDB2CSV2($db2con, $d1name, $val['GPKXID'], $val['GPKXCO']);
                    }
                    if ($checkRs === false) {
                        $rtn = 2;
                        //      $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                        $msg = showMsg('FAIL_SYS');
                    } else {
                        if (count($checkRs) === 0) {
                            $checkRs = cmCheckFDB2CSV5($db2con, $d1name, $val['GPKXCO'],"");// MSM add "" for Missing argument error
                            if ($checkRs === false) {
                                $rtn = 2;
                                //    $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                                $msg = showMsg('FAIL_SYS');
                                break;
                            } else {
                                if (count($checkRs) === 0) {
                                    $rtn = 2;
                                    $msg = showMsg('PIVOT_NO_COLS'); //'クエリーに登録されていないカラムが選択されています。クエリーの設定をご確認ください。';
                                    
                                }
                            }
                        }
                    }
                }
                $BAR[$xcol] = array();
                $LINE[$xcol] = array();
                $AREA[$xcol] = array();
                if ($rtn === 0) {
                    $rs = cmSelDB2GPC($db2con, $d1name, $GPHKEY);
                    if ($rs['result'] !== true) {
                        $rtn = 1;
                        $msg = showMsg($rs['result'], array('グラフ'));
                    } else {
                        $gpc = array();
                        $gpcArr = umEx($rs['data'], true);
                        foreach ($gpcArr as $key => $val) {
                            if ($val['GPCYID'] === '9999') {
                                $checkRs = array();
                            } else {
                                $checkRs = cmCheckFDB2CSV2($db2con, $d1name, $val['GPCYID'], $val['GPCYCO']); //クエリーカラム
                                
                            }
                            if ($checkRs === false) {
                                $rtn = 2;
                                //      $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                                $msg = showMsg('FAIL_SYS');
                            } else {
                                if (count($checkRs) === 0) {
                                    $checkRs = cmCheckFDB2CSV5($db2con, $d1name, $val['GPCYCO']); //結果フィールドカラム
                                    if ($checkRs === false) {
                                        $rtn = 2;
                                        //    $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                                        $msg = showMsg('FAIL_SYS');
                                        break;
                                    } else {
                                        if (count($checkRs) === 0) {
                                            $rtn = 2;
                                            $msg = showMsg('PIVOT_NO_COLS'); //'クエリーに登録されていないカラムが選択されています。クエリーの設定をご確認ください。';
                                            
                                        }
                                    }
                                } //count
                                
                            } //checkRs === false
                            if ($rtn === 0) {
                                $gpc_inArr = array();
                                $gpcLbl = ($val['GPCLBL'] !== '') ? $val['GPCLBL'] : $val['GPCHED'];
                                $gpc_inArr['LABEL'] = $gpcLbl;
                                $gpc_inArr['COLOR'] = $val['GPCCLR'];
                                $gpc_inArr['GTYPE'] = $val['GPCFM'];
                                $gpc_inArr['SEQ'] = $val['GPCSEQ'];
                                $ycol = $val['GPCYCO'] . '_' . $val['GPCYID'];
                                $GHEADER[$key + 1][$ycol] = $gpcLbl;
                                $gpc[$ycol] = $gpc_inArr;
                                $ycol = $val['GPCSEQ'] . '_' . $val['GPCYCO'] . '_' . $val['GPCYID'];
                                $BAR[$ycol] = array();
                                $LINE[$ycol] = array();
                                $AREA[$ycol] = array();
                            } else {
                                break;
                            }
                        } //foreach gpcarr
                        
                    } //GPC
                    
                }
            }
        }
    }
    if ($D1CFLG === '') {
        if ($rtn === 0) {
            $rs = cmGetGraphData($db2connect, $filename, $gpk, $gpcArr,$filtersearchData);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result']);
            } else {
                $data = $rs['data'];
                $gphInfo = cmCreateGphArray($data, $gpk, $gpc, $LINE, $AREA, $BAR, $GHEADER);
            }
        }
    } else {
        if ($rtn === 0) {
            $rs = cmGetGraphSQLData($db2connect, $oldfilename, $gpk, $gpcArr,$filtersearchData);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result']);
            } else {
                $data = $rs['data'];
                $gphInfo = cmCreateGphArray($data, $gpk, $gpc, $LINE, $AREA, $BAR, $GHEADER);
            }
        }
    }
} else {
    $msg = showMsg('FAIL_MENU_DEL', array('実行中のデータ')); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
    $rtn = 1;
    $allcount = 0;
    $errorFlg = 1;
}
//GET BMKTOTAL
if ($rtn === 0) {
    $res = cmGETDB2BMKCOUNT($db2con, $d1name, $GPHKEY, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($res['result'] !== true) {
        $rtn = 1;
        $errorFlg = 1;
        $msg = showMsg($res['result']);
    } else {
        $BMKTOTAL = $res['TOTALCOUNT'];
    }
}

if($rtn === 0){
    $rs = cmGetDB2QBMK($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'], $QRYNAME,'',$GPHKEY,$QBMFLG);
    if($rtn === 0){
        if($rs['result'] !== true){
            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            if($rs['datacount'] > 0){
                $bmkexist = true;
            }
        }
    }
}
if ($rtn === 0) {//MSM add for bmfoler icon show/hide
    $rs = cmGetWUUBFG($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result']);
    } else {
        $WUUBFG = $rs['data'][0]['WUUBFG'];
    }
}

$WUPSWE="";//graph drilldown new tab GiGi
if($rtn === 0){
    $rs = fnGetWUPSWE($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $WUPSWE = $rs['WUPSWE'];
       // $data = $rs['data'];
    }
}//graph drilldown new tab GiGi

if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
    cmDb2Close($db2RDBcon);
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'iTotalDisplayRecords' => $allcount,
    'aaData' => $aaData, 'data' => $data,
    'rtn' => $rtn, 'msg' => $msg,
    'filename' => $filename,
    'oldfilename' => $oldfilename,//MSM add
    'd1name' => $d1name,
    'errorFlg' => $errorFlg,
    'BMKTOTAL' => $BMKTOTAL,
    'D1TEXT' => $D1TEXT,
    'GHEADER' => $GHEADER,
    'GPHINFO' => $gphInfo,
    'graphtbl' => $graphtbl,
    'gpk' => $gpk,
    'BMKEXIST'=> $bmkexist,
    'filtersearchData' => $filtersearchData,
    'ISQGFLG' => $ISQGFLG,
    'DSPROW1' => DSPROW1,
    'DSPROW2' => DSPROW2,
    'DSPROW3' => DSPROW3,
    'DSPROW4' => DSPROW4,
    'INTDSPROW'=>INTDSPROW,
    'WUUBFG' => $WUUBFG,
    'WUUID' => $_SESSION['PHPQUERY']['user'][0]['WUUID'],//graph drilldown new tab GiGi
    'WUPSWE' => $WUPSWE//graph drilldown new tab GiGi
);
echo (json_encode($rtn));

function fnGetWUPSWE($db2con,$WUUID){//graph drilldown new tab GiGi
    $rs=true;
    $WUPSWE = "";
    $params = array();

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';
    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SEL';
        }
    }
    if($rs=true){
        $strSQL  = ' SELECT DECRYPT_CHAR(B.WUPSWE) AS WUPSWE ';
        $strSQL .= ' FROM DB2WUSR as B WHERE B.WUUID=? ';
        $stmt = db2_prepare($db2con,$strSQL);
        $params=array($WUUID);
        if($stmt === false){
            $rs='FAIL_SEL';
        }else{          
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs='FAIL_SEL';
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $WUPSWE = $row["WUPSWE"];
                }
            }
        }
    }
    return array('result'=>$rs,'WUPSWE'=>cmMer($WUPSWE));

}


