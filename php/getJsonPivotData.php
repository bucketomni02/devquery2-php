<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
//ini_set("max_execution_time",3000);
set_time_limit(300);
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("PivotClass.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */


$d1name         = $_POST['D1NAME'];
$QRYNAME        = $d1name;
$pmpkey         = $_POST['PMPKEY'];
$filename       = $_POST['csvfilename'];
$iDisplayStart  = (isset($_POST['iDisplayStart']) ? $_POST['iDisplayStart'] : '');
$iDisplayLength = (isset($_POST['iDisplayLength']) ? $_POST['iDisplayLength'] : '');
$iSortCol_0     = (isset($_POST['iSortCol_0']) ? $_POST['iSortCol_0'] : '');
$sSortDir_0     = (isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : '');
$sSearch        = (isset($_POST['sSearch']) ? $_POST['sSearch'] : '');
//for filtersearch
$filtersearchData = (isset($_POST['filtersearchData']) ? json_decode($_POST['filtersearchData'], true) : array());
$flg            = (isset($_POST['flg']) ? $_POST['flg'] : '');
$rowstart       = (isset($_POST['rowstart']) ? (int) $_POST['rowstart'] : '');
$rowend         = (isset($_POST['rowend']) ? (int) $_POST['rowend'] : '');
$fcheck         = (isset($_POST['fcheck']) ? json_decode($_POST['fcheck'], true) : array());
$fcombo         = (isset($_POST['fcombo']) ? json_decode($_POST['fcombo'], true) : array());
$ftext          = (isset($_POST['ftext']) ? json_decode($_POST['ftext'], true) : array());
$ffld           = (isset($_POST['ffld']) ? json_decode($_POST['ffld'], true) : array());
$O_db2pcal      = (isset($_POST['O_db2pcal']) ? json_decode($_POST['O_db2pcal'], true) : array());
$O_db2pcol      = (isset($_POST['O_db2pcol']) ? json_decode($_POST['O_db2pcol'], true) : array());
$isQGflg 		= (isset($_POST['ISQGFLG']) ? $_POST['ISQGFLG'] : '');//クエリーグループならＴＲＵＥ
$db2pcal        = array();
$db2pcol        = array();
//一つ前のソート状態
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
//テーブルある無しフラグ
$rs             = true;
$rtn            = 0;
//データ配列
$csv_d          = array();
$len_d          = array(); //抽出データの最大桁数
$msg            = '';
//クエリーグループ
$D1NAMELIST=array();//クエリーグループの全部データ
$LASTD1NAME='';//クエリーグループの一番最後のクエリーＩＤ
$D1TEXT = '';
$isQGflg=($isQGflg==='true')?true:false;

$getwheresearch = '';
$getwhereparam  = '';
$columndata     = '';
$calcdata       = '';
$rtncolumn      = '';
$columnupdata   = '';
$getcheadingcol = '';
$colslists      = '';
$countcalc      = '';
$searchcolumns  = '';
$flg            = $flg;
$num_count      = '';
$endcount       = '';
$xchecked       = array(); //項目情報配列（縦軸）
$xcheckedUnhide = array(); //項目情報配列（縦軸）(非表示が入ってない)
$ychecked       = array(); //項目情報配列（横軸）
$ycheckedUnhide = array(); //項目情報配列（横軸）(非表示が入ってない)
$cchecked       = array(); //項目情報配列（集計）
$xcheckedcount  = 0; //縦軸の表示カウント
$ycheckedcount  = 0; //横軸の表示カウント
$errorFlg       = 0; //テーブルがなかった場合に1
$shukeiType     = true;
$fdb2csv2       = array();
$fieldLenArray  = array();
$db2jksk = array();
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */
$csv_d          = array();
$bmkexist = false;
//グループかクエリー
$QBMFLG = 0;
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmCheckUser($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }
}
//クエリーグループ
if($rtn===0){
    if($isQGflg){
        $QBMFLG = 1;
        $rs=cmGetQueryGroup($db2con,$d1name,'','');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリー'));
        }else{
            if(!$licenseSql){
                foreach($rs['data'] as $key => $res){
                    if($res['D1CFLG'] === ''){
                        array_push($D1NAMELIST,$rs['data'][$key]);
                    }      
                }
                $D1NAMELIST[count($D1NAMELIST)-1]['LASTFLG'] = '1';
                $D1NAMELIST = umEx($D1NAMELIST,true);
            }else{
                $D1NAMELIST=umEx($rs['data'],true);
            }
        }
    }
}
if ($rtn === 0) {
    if($isQGflg){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID'],$d1name,$res['QRYGSQRY'],$res['LASTFLG'],'','');
            if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }else{
                //一番最後の
                if($res['LASTFLG']==='1'){
                    $fdb2csv1Data = $chkQry['FDB2CSV1'];
                    $d1name=$res['QRYGSQRY'];
                    $D1TEXT.= $chkQry['FDB2CSV1']['D1TEXT'];
                }else {
                    $D1TEXT.= $chkQry['FDB2CSV1']['D1TEXT'] . ',';
                }
            }
        }//end of for loop
    }else{
        $chkQry = array();
        $chkQry = cmChkQuery($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID'], $d1name, '');
        if ($chkQry['result'] !== true) {
            $rtn = 2;
            $msg = showMsg($chkQry['result'], array(
                'クエリー'
            ));
        } else {
            $D1TEXT = $chkQry['FDB2CSV1']['D1TEXT'];
        }
    }
}

if ($rtn === 0) {
    $fdb2csv2 = cmGetFDB2CSV2($db2con, $d1name,false,false,false);
    if ($fdb2csv2 === false) {
        $rtn = 1;
        $msg = showMsg('FAIL_SEL');
    } else {
        $fdb2csv2 = umEx($fdb2csv2);
        foreach ($fdb2csv2 as $key => $value) {
            //結果フィールドで数値項目の場合のみ、配列に追加
            if ($value['D2FILID'] === '9999' && ($value['D2TYPE'] === 'S') || ($value['D2TYPE'] === 'P') || ($value['D2TYPE'] === 'B')) {
                $fieldLenArray[$value['D2FLD'] . '_' . $value['D2FILID']] = array(
                    'LEN' => $value['D2LEN'],
                    'DEC' => $value['D2DEC']
                );
            }
        }
    }
}

if ($rtn === 0) {
    $fdb2csv1Data = FDB2CSV1_DATA($d1name);
    $RDBNM = cmMer($fdb2csv1Data['data'][0]['D1RDB']);
    //e_log('RDBDATA:'.$RDBNM.print_r($fdb2csv1Data,true));
    if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
        $systables = cmGetSystables($db2con, SAVE_DB, $filename);
    }else{
        $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
        $db2RDBcon = cmDB2ConRDB(SAVE_DB);
        $systables = cmGetSystables($db2RDBcon, SAVE_DB, $filename);
    }
    //pivot情報取得
    $db2pmst = cmGetDB2PMST($db2con, $d1name, $pmpkey);
    $db2pcol = cmGetDB2PCOL($db2con, $d1name, $pmpkey);
    $db2pcal = cmGetDB2PCAL($db2con, $d1name, $pmpkey);
    
    $db2pmst = umEx($db2pmst);
    $db2pcol = umEx($db2pcol);
    $db2pcal = umEx($db2pcal);
    
    if (count($systables) > 0) {
        if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
           $resIns = cmInsWTBL($db2con,$filename);
        }else{
           $resIns = cmInsWTBL($db2con,$filename,$db2RDBcon);
        }
        if($resIns !== 0){
            //修正用
            $msg = showMsg('ログ挿入失敗しました。');//'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
            $rtn = 1;
        }
    }else{
        //テブールを作る前に設定したピボットデータと同じかどうかチェック
        if ($O_db2pcol !== $db2pcol || $O_db2pcal !== $db2pcal) {
            $rtn      = 2;
            $errorFlg = 0;
            $msg      = showMsg('FAIL_MENU_REQ',array('ピボットの設定'));//'ピボットの設定が変更されました。<br/>再度メニューから実行するか、検索し直してください。';
        } else {
            $rtn      = 1;
            $errorFlg = 1;
            $msg      = showMsg('FAIL_MENU_DEL',array('実行中のデータ'));//'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
        }
        $allcount = 0;
        //}
    }
    if($rtn === 0){
        $xyColName = array(); //WPFLD + _ + WPFILIDを格納する一次元配列 フィルター対象項目の存在チェックに使用
        
        //クエリーのFDB2CSV2orFDB2CSV5と整合性があるかチェック
        //同時に
        foreach ($db2pcol as $key => $value) {
            
            //FILD=9999の場合は無条件でarray()
            if ($value['WPFILID'] === '9999') {
                $checkRs = array();
            } else {
                $checkRs = cmCheckFDB2CSV2($db2con, $d1name, $value['WPFILID'], $value['WPFLD']);
            }
            if ($checkRs === false) {
                $rtn = 2;
                //      $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                $msg = showMsg('FAIL_SYS');
            } else {
                if (count($checkRs) === 0) {
                    $checkRs = cmCheckFDB2CSV5($db2con, $d1name, $value['WPFLD']);
                    if ($checkRs === false) {
                        $rtn = 2;
                        //    $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                        $msg = showMsg('FAIL_SYS');
                        break;
                    } else {
                        if (count($checkRs) === 0) {
                            $rtn = 2;
                            $msg = showMsg('PIVOT_NO_COLS');//'クエリーに登録されていないカラムが選択されています。クエリーの設定をご確認ください。';
                        }
                    }
                }
            }
            
            $xyColName[] = $value['WPFLD'] . '_' . $value['WPFILID'];
            
        }
        
        if (count($O_db2pcol) > 0) {
            //                if( count($O_db2pcol) !== count($db2pcol) || count($O_db2pcal) !== count($db2pcal) ){
            if ($O_db2pcol !== $db2pcol || $O_db2pcal !== $db2pcal) {
                $rtn = 2;
                $msg = showMsg('FAIL_MENU_REQ',array('ピボットの設定'));//'ピボットの設定が変更されました。<br/>再度メニューから実行するか、検索し直してください。';
            } else {
                
            }
        }
        if ($rtn === 0) {
            
            //フィルターデータ
            $searchdata     = $ftext;
            $chkcoldata     = $fcheck;
            $search_col_arr = $ffld;
            $operator       = $fcombo;
            
            //フィルター条件がピボットの設定に使用されているかチェック。なかったらメッセージをリターン
            foreach ($chkcoldata as $cKey => $cValue) {
                if (!in_array($search_col_arr[$cValue], $xyColName)) {
                    $msg = showMsg('FAIL_COL',array('ピボット','フィルター'));//'ピボットに設定されていない項目がフィルターに指定されています。<br/>ピボット設定を確認してください。';
                    $rtn = 1;
                    break;
                }
            }
            
            if ($rtn === 0) {
                $shukeiXdisplayFlg = false;
                $shukeiYdisplayFlg = false;
                //縦横集計を配列に格納
                $columndata        = array();
                $columnupdata      = array();
                $calcdata          = array();
                $sortColData       = '';
                $sortColUpData     = '';
                foreach ($db2pcol as $value) {
                    $wppflg  = $value['WPPFLG'];
                    $wpseqn  = $value['WPSEQN'];
                    $wpfilid = $value['WPFILID'];
					error_log('columupdata ***'.$value['WPFLD'].'>>>'.$value['WPFILID']);					
                    $column  = $value['WPFLD'] . '_' . $value['WPFILID'];
                    $wpfhide = $value['WPFHIDE'];
                    $wpsort  = $value['WPSORT'];
                    $wpsumg  = $value['WPSUMG'];
                    $d2len   = $value['D2LEN'];
                    $d2dec   = $value['D2DEC'];
                    $d2type  = $value['D2TYPE'];
                    $d2wedt  = $value['D2WEDT'];
                    switch ($wppflg) {
                        case '1':
                            $columndata[$wpseqn] = $column;
                            if ($wpsumg === "") {
                                $shukeiXdisplayFlg = true;
                            }
                            //表示非表示配列格納
                            //表示時にオブジェクトループしているのでキーで配列格納
                            $xchecked[$column] = array(
                                'FILID' => $wpfilid,
                                'HIDE' => $wpfhide,
                                'SORT' => $wpsort,
                                'SUMG' => $wpsumg,
                                'LEN' => $d2len,
                                'DEC' => $d2dec,
                                'TYPE' => $d2type,
                                'WEDT' => $d2wedt
                            );
                            if ($wpfhide === '') {
                                $xcheckedcount++;
                                $xcheckedUnhide[$column] = array(
                                    'FILID' => $wpfilid,
                                    'HIDE' => $wpfhide,
                                    'SORT' => $wpsort,
                                    'SUMG' => $wpsumg,
                                    'LEN' => $d2len,
                                    'DEC' => $d2dec,
                                    'TYPE' => $d2type,
                                    'WEDT' => $d2wedt
                                );
                            }
                            break;
                        case '2':
                            $columnupdata[$wpseqn] = $column;
                            if ($wpsumg === "") {
                                $shukeiYdisplayFlg = true;
                            }
                            //表示非表示配列格納
                            $ychecked[$column] = array(
                                'FILID' => $wpfilid,
                                'HIDE' => $wpfhide,
                                'SORT' => $wpsort,
                                'SUMG' => $wpsumg,
                                'LEN' => $d2len,
                                'DEC' => $d2dec,
                                'TYPE' => $d2type,
                                'WEDT' => $d2wedt
                            );
                            if ($wpfhide === '') {
                                $ycheckedUnhide[$column] = array(
                                    'FILID' => $wpfilid,
                                    'HIDE' => $wpfhide,
                                    'SORT' => $wpsort,
                                    'SUMG' => $wpsumg,
                                    'LEN' => $d2len,
                                    'DEC' => $d2dec,
                                    'TYPE' => $d2type,
                                    'WEDT' => $d2wedt
                                );
                            }
                            break;
                        case '3':
                            if($d2type === 'S' || $d2type === 'P' || $d2type === 'B'){
                                    $calcdata[$wpseqn] = $column;
                                    //表示非表示配列格納 
                                    $cchecked[$column] = array(
                                        'FILID' => $wpfilid,
                                        'HIDE' => $wpfhide,
                                        'SORT' => $wpsort,
                                        'SUMG' => $wpsumg,
                                        'LEN' => $d2len,
                                        'DEC' => $d2dec,
                                        'TYPE' => $d2type,
                                        'WEDT' => $d2wedt
                                    );
                                }else{
                                    $shukeiType = false;
                                }

                            break;
                    }
                    if($shukeiType === false){
                            $rtn = 1;
                            $msg = showMsg('PIVOT_SHU_TYPE');  //集計するタイプがS,P,Bじゃないかったら、エラー
                            break;
                    }
                }
            }
            
        if ($rtn === 0) {
                //計算式を配列に格納
                $num_formula = array();
                if (count($db2pcal) > 0) {
                    for ($i = 0; $i < 10; $i++) {
                        if (isset($db2pcal[$i])) {
                            $num_formula[] = $db2pcal[$i]['WCCALC'];
                        } else {
                            $num_formula[] = "";
                        }
                    }
                    
                } else {
                    $num_formula = array(
                        "",
                        "",
                        "",
                        "",
                        ""
                    );
                }
                
                $pgstart = $iDisplayStart + 1;
                $pgend   = ($pgstart - 1) + $iDisplayLength;
                
           
	             if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
	                 $pivot   = new Pivot($db2con, SAVE_DB, $filename, $d1name);
	                 $pivotcount   = new Pivot($db2con, SAVE_DB, $filename, $d1name);
	             }else{
	                 $pivot   = new Pivot($db2RDBcon, SAVE_DB, $filename, $d1name);
	                 $pivotcount   = new Pivot($db2RDBcon, SAVE_DB, $filename, $d1name);
	             }

				if($flg !== 'P'){ //ピボット画面の設定が約99列になった時すること
	                $zentaiCount = 0;
	                $pivotcount->crossData($flg, $searchdata, $chkcoldata, $search_col_arr, $operator, $columndata, $columnupdata, $calcdata, $num_formula, $rowstart, $rowend, $pgstart, $pgend, $shukeiXdisplayFlg, $shukeiYdisplayFlg, $xchecked, $ychecked, true,
	                    '',$filtersearchData);
	                if ($pivotc !== false) {
	                    $zentaiCount = $pivotcount->get_zentaiCount();
	                }
	                $pivotRs = $pivot->crossData($flg, $searchdata, $chkcoldata, $search_col_arr, $operator, $columndata, $columnupdata, $calcdata, $num_formula, $rowstart, $rowend, $pgstart, $pgend, $shukeiXdisplayFlg, $shukeiYdisplayFlg, $xchecked, $ychecked, false,'',$filtersearchData);	           
	                if ($pivotRs !== false) {
	                    $csv_d    = $pivot->get_data_studio();
	                    $allcount = $pivot->get_totalcount();
	                    
	                    if ($allcount !== -1) {
	                        $getwheresearch = $pivot->get_wheresearch();
	                        $getwhereparam  = $pivot->get_whereparam();
							//$getwhereparam	= array();
	                        $columndata     = $pivot->get_columndata();
	                        $calcdata       = $pivot->get_columnC();
	                        $rtncolumn      = $pivot->get_rtncolumn();
	                        $columnupdata   = $pivot->get_columnupdata();
	                        $getcheadingcol = $pivot->get_getcheadingcol();
	                        $colslists      = $pivot->get_colslists();
	                        $colInd         = 0;
	                        $precol         = '';
	                        $countcalc      = $pivot->get_countcalc();
	                        $rowstart       = $pivot->get_rowstart();
	                        $rowend         = $pivot->get_rowend(); 
	                        $searchcolumns  = $pivot->get_searchcolumns();
	                        $flg            = $flg;
	                        $num_count      = $pivot->get_num_count();
	                        $endcount       = $pivot->get_endcount();
	                        $num_formula    = $pivot->get_num_formula();
	                    } else {
	                        $rtn = 2;
	                        $msg = showMsg('PIVOT_CHK_COUNT');//'指定されたページのデータが取得できませんでした</br>ピボットの設定が変更された可能性があります。';
	                    }  
	                }
	                
	                cmEnd($db2con, $RTCD, $JSEQ, $filename, $d1name);
	                if ($RTCD === '9') {
	                    $rtn = 1; 
	                    $msg = showMsg('FAIL_FUNC',array('クエリーの事後処理'));//'クエリーの事後処理に失敗しました。';
	                }
	                
	                if ($pivotRs === false) {
	                    $rtn = 1;
	                    $msg = showMsg('PIVOT_CHK');//'集計できませんでした。設定をご確認ください。';
	                }
				}
				else{ // ピボット画面の設定が全部になった時すること
		            $zentaiCount = 0;
		            $flg = '2';

					$pivotcount->crossData($flg, $searchdata, $chkcoldata, $search_col_arr, $operator, $columndata, $columnupdata, $calcdata, $num_formula, $rowstart, $rowend, $pgstart, $pgend, $shukeiXdisplayFlg, $shukeiYdisplayFlg, $xchecked, $ychecked, true,
					'',$filtersearchData);

            		$zentaiCount = $pivotcount->get_zentaiCount(); 
	                $getcheadingcol = array();
	                $data_studio = array();
	                $rtncolumn = array();
	                $colslists = array();
	                
	                do {
						error_log('testing server timeout 1****');

	                    error_log('data ' . $idx . '_' . $lenCount . 'START = ' . $rowstart . 'END = ' . $rowend.'PAGE START = '.$pgstart.'PAGE END - '.$pgend);						
	                
						if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
	                        $pivot = new Pivot($db2con, SAVE_DB, $filename, $d1name);
	                    } else {
	                        $pivot = new Pivot($db2RDBcon, SAVE_DB, $filename, $d1name);
	                    }
	                    $pivotRs = $pivot->crossData($flg, $searchdata, $chkcoldata, $search_col_arr, $operator, $columndata, $columnupdata, $calcdata, $num_formula, $rowstart, $rowend, $pgstart, $pgend, $shukeiXdisplayFlg, $shukeiYdisplayFlg, $xchecked, $ychecked, false,'',$filtersearchData);			
						if ($pivotRs !== false) {

							$rowstart = $pivot->get_rowstart();
		                    $rowend = $pivot->get_rowend();
		                    $endcount = $pivot->get_endcount();

		                    if (count($columnupdata) > 0) {
		                        $getcheadingcol = array_merge($getcheadingcol, $pivot->get_getcheadingcol());
		                    } else {
		                        $getcheadingcol = $pivot->get_getcheadingcol();
		                    }
		                    $columndata = $pivot->get_columndata();
		                    if (count($colslists) > 0) {
		                        $colslists = array_merge($colslists, $pivot->get_colslists());
		                    } else {
		                        $colslists = $pivot->get_colslists();
		                    }
		                    $countcalc = $pivot->get_countcalc();
		                    $num_count = $pivot->get_num_count();
		  					//$searchcolumns  = $pivot->get_searchcolumns();  

		                    $rtncolumn = array_merge($rtncolumn, $pivot->get_rtncolumn());

		                    if (count($data_studio) > 0) {
		                        foreach ($pivot->get_data_studio() as $key => $value) {
		                            foreach ($value as $k => $v) {
		                                $data_studio[$key][$k] = $v;
		                            }
		                        }
		                    } else {
		                        $data_studio = $pivot->get_data_studio();
		                    }
		                    $gb_count = count($data_studio);
		                    $flg = '3';
						}
						else{
							 $rtn = 1;
	                    	 $msg = showMsg('PIVOT_CHK');//'集計できませんでした。設定をご確認ください。';
							 break;
						}
	                } while ($endcount !== $rowend && count($columnupdata) > 0);
					if ($pivotRs !== false) {

	                    $allcount = $pivot->get_totalcount();
		                if ($allcount !== -1) {

							$csv_d = $data_studio;
							$getcheadingcol = '';
			                $getcheadingcol = $pivot->get_getcheadingcol();
			                $columnupdata   = $pivot->get_columnupdata();
			                $rowstart = 1;
			                $num_formula    = $pivot->get_num_formula();
			                $getwhereparam  = $pivot->get_whereparam();
							//e_log('get_data_studio in looping*****'.print_r($data_studio,true));
						   //e_log('colists in looping*****'.print_r($colslists,true));

						} else {
		                    $rtn = 2;
		                    $msg = showMsg('PIVOT_CHK_COUNT');//'指定されたページのデータが取得できませんでした</br>ピボットの設定が変更された可能性があります。';
		                }
					}
	                cmEnd($db2con, $RTCD, $JSEQ, $filename, $d1name);
					if ($RTCD === '9') {
	                    $rtn = 1; 
	                    $msg = showMsg('FAIL_FUNC',array('クエリーの事後処理'));//'クエリーの事後処理に失敗しました。';
	                }
	                
	                if ($pivotRs === false) {
	                    $rtn = 1;
	                    $msg = showMsg('PIVOT_CHK');//'集計できませんでした。設定をご確認ください。';
	                }

                }
            }
            
        }
        
    } 
    $csv_d = umEx($csv_d);
}

if($rtn === 0){
    $rs = cmGetDB2QBMK($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'], $QRYNAME,$pmpkey,'',$QBMFLG);
    if($rtn === 0){
        if($rs['result'] !== true){
            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            if($rs['datacount'] > 0){
                $bmkexist = true;
            }
        }
    }
}

// 文字の色付け（条件付き書式）
if($rtn === 0){
    $rs = cmGetDB2JKSK($db2con,$d1name);
    //if($rtn === 0){
        if($rs['result'] !== true){
            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            $db2jksk = $rs['data'];
        }
    //}
}

if ($rtn === 0) {//MSM add for bmfoler icon show/hide
    $rs = cmGetWUUBFG($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result']);
    } else {
        $WUUBFG = $rs['data'][0]['WUUBFG'];
    }
}
$WUPSWE="";//pivot drilldown new tab GiGi
if($rtn === 0){
    $rs = fnGetWUPSWE($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $WUPSWE = $rs['WUPSWE'];
       // $data = $rs['data'];
    }
}//pivot drilldown new tab GiGi
// ワークテーブルのログ情報削除
$resDel = cmDelWTBL($db2con,$filename);
if($resDel !== 0){
   // e_log('削除テスト：'.$resDel.'aaaa'.$filename);
    //修正用
    $msg = showMsg('ログ削除失敗');//'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
    $rtn = 1;
}
if(($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false){
    cmDb2Close($db2RDBcon);
}

cmDb2Close($db2con);
//error_log(' iTotalRecords****'.$allcount);
//error_log(' colslists****'.print_r($colslists,true));
//error_log(' iTotalRecords****'.$rowend);
//e_log('get_data_studio in end*****'.print_r($data_studio,true));


/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'iTotalDisplayRecords' => $allcount,
    'aaData' => $csv_d,
    'rtn' => $rtn,
    'msg' => $msg,
    'filename' => $filename,
    'd1name' => $d1name,
    'RTCD' => $RTCD,
    'len_d' => $len_d,
    "getwheresearch" => $getwheresearch,
    "getwhereparam" => $getwhereparam,
    "columndata" => $columndata,
    "calcdata" => $calcdata,
    "rtncolumn" => $rtncolumn,
    "columnupdata" => $columnupdata,
    "getcheadingcol" => $getcheadingcol,
    "colslists" => umEx($colslists, true),
    "countcalc" => $countcalc,
    "rowstart" => $rowstart,
    "rowend" => $rowend,
    "searchcolumns" => $searchcolumns,
    "flg" => $flg,
    "shukeiType" =>$shukeiType,
    "num_count" => $num_count,
    "endcount" => $endcount,
    "errorFlg" => $errorFlg,
    "pivotRs" => $pivotRs,
    "fieldLenArray" => $fieldLenArray,
    "db2pcol" => $db2pcol,
    "xchecked" => $xcheckedUnhide,
    "ychecked" => $ycheckedUnhide,
    "cchecked" => $cchecked,
    "xcheckedcount" => $xcheckedcount,
    "ZERO_FLG" => ZERO_FLG,
    "num_formula" => $num_formula,
    "db2pmst" => $db2pmst,
    "db2pcol" => $db2pcol,
    "db2pcal" => $db2pcal,
    "O_db2pcol" => $O_db2pcol,
    "O_db2pcal" => $O_db2pcal,
    "refreshFlg" => $refreshFlg,
    "sizexchecked" => sizeof($xchecked),
    "BACKZERO_FLG" => BACKZERO_FLG,
    "pgstart" => $pgstart,
    "pgend" => $pgend,
    "zentaiCount" => $zentaiCount,
    'PIVSCOLR'=>PIVSCOLR,
    'D1TEXT' => $D1TEXT,
    'BMKEXIST'=> $bmkexist,
    'DSPROW1' => DSPROW1,
    'DSPROW2' => DSPROW2,
    'DSPROW3' => DSPROW3,
    'DSPROW4' => DSPROW4,
	'INTDSPROW'=>INTDSPROW,
	'DB2JKSK'=> $db2jksk,
    'test' => $pivotRs,
    'WUUBFG' => $WUUBFG,
    'WUUID' => $_SESSION['PHPQUERY']['user'][0]['WUUID'],//pivot drilldown new tab GiGi
    'WUPSWE' => $WUPSWE//pivot drilldown new tab GiGi
);
unset($csv_d);
echo (json_encode($rtn));

/*
 *-------------------------------------------------------* 
 * 画面定義内容をFDB2CSV3に更新
 *-------------------------------------------------------*
 */

function fnSearchUpdate($db2con, $sch_data) {
    
    foreach ($sch_data as $k1 => $v1) {
        foreach ($v1 as $k2 => $v2) {
            foreach ($v2 as $k3 => $v3) {
                
                $rs = cmUpdFDB2CSV3($db2con, $k1, $k2, $k3, $v3['dat'], $v3['type']);
                
            }
        }
    }
}

/*
 *-------------------------------------------------------* 
 * 項目の最大桁数の値を取得
 *-------------------------------------------------------*
 */
function fnGetMaxLenValue($db2con, $fdb2csv2, $filename, $start, $length, $sortcol, $sortdir, $sortingcols = 0, $search) {
    
    $rtn = array();
    
    foreach ($fdb2csv2 as $csv2row) {
        $colName = cmMer($csv2row['D2FLD']) . '_' . cmMer($csv2row['D2FILID']);
        $maxdata = '';
        $params  = array();
        
        $strSQL = '';
        
        $strSQL .= ' SELECT ' . $colName . ' ';
        $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' ';
        $strSQL .= ' WHERE LENGTH(RTRIM(' . $colName . ')) = ';
        $strSQL .= ' ( ';
        $strSQL .= ' SELECT ';
        $strSQL .= ' MAX(LENGTH(RTRIM(A.' . $colName . '))) ';
        $strSQL .= ' FROM( ';
        $strSQL .= ' SELECT B.* , ';
        
        $strSQL .= ' ROWNUMBER() OVER ( ';
        
        //ソート
        if ($sortcol !== "") {
            $sortColumn = $sortcol;
            $strSQL .= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
        }
        
        $strSQL .= ') ';
        $strSQL .= ' AS rownum ';
        $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' as B ';
        
        //検索条件
        if ($search != '') {
            if (count($fdb2csv2) > 0) {
                $strSQL .= ' WHERE ';
                foreach ($fdb2csv2 as $key => $value) {
                    $strSQL .= cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
                    array_push($params, '%' . $search . '%');
                    
                    if ($key < (count($fdb2csv2) - 1)) {
                        $strSQL .= ' OR ';
                    }
                }
            }
        }
        
        $strSQL .= ' ) AS A ';
        
        //抽出範囲指定
        if (($start != '') && ($length != '')) {
            $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
            array_push($params, $start + 1);
            array_push($params, $start + $length);
        }
        
        $strSQL .= ' ) FETCH FIRST 1 ROWS ONLY ';
        
        
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $rtn = false;
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $rtn = false;
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $maxdata = $row[$colName];
                }
                $rtn[$colName] = cmMer($maxdata);
            }
        }
    }
    return $rtn;
}

function fnGetMaxLen($db2con, $fdb2csv2, $filename) {
    
    $rtn = array();
    
    $strSelect = '';
    foreach ($fdb2csv2 as $csv2row) {
        $colname = cmMer($csv2row['D2FLD']) . '_' . cmMer($csv2row['D2FILID']);
        $strSelect .= 'max(length(rtrim(A.' . $colname . '))) AS ' . $colname . ',';
    }
    $strSelect = rtrim($strSelect, ',');
    
    $strSQL = '';
    
    $strSQL .= ' SELECT ' . $strSelect . ' ';
    $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' AS A ';
    
    $params = array();
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data = $row;
            }
        }
    }
    
    $rtn = $data;
    return $rtn;
}

function fnGetWUPSWE($db2con,$WUUID){//pivot drilldown new tab GiGi
    $rs=true;
    $WUPSWE = "";
    $params = array();

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';
    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SEL';
        }
    }
    if($rs=true){
        $strSQL  = ' SELECT DECRYPT_CHAR(B.WUPSWE) AS WUPSWE ';
        $strSQL .= ' FROM DB2WUSR as B WHERE B.WUUID=? ';
        $stmt = db2_prepare($db2con,$strSQL);
        $params=array($WUUID);
        if($stmt === false){
            $rs='FAIL_SEL';
        }else{          
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs='FAIL_SEL';
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $WUPSWE = $row["WUPSWE"];
                }
            }
        }
    }
    return array('result'=>$rs,'WUPSWE'=>cmMer($WUPSWE));

}
