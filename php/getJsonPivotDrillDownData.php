<?php
/*
*-------------------------------------------------------*
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once ("common/inc/config.php");

include_once ("common/inc/common.inc.php");

include_once ("base/comGetFDB2CSV2_CCSID.php");
include_once("base/createExecuteSQL.php");

/*
*-------------------------------------------------------*
* DataTableリクエスト
*-------------------------------------------------------*
*/
$d1name = $_POST['D1NAME'];
$QRYNAME = $d1name;
$filename = $_POST['csvfilename'];
$querydevice = $_POST['querydevice'];
$iDisplayStart = (isset($_POST['iDisplayStart']) ? $_POST['iDisplayStart'] : '');
$iDisplayLength = (isset($_POST['iDisplayLength']) ? $_POST['iDisplayLength'] : '');
$iSortCol_0 = (isset($_POST['iSortCol_0']) ? $_POST['iSortCol_0'] : '');
$sSortDir_0 = (isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : '');
// 検索キー
$sSearch = (isset($_POST['sSearch']) ? $_POST['sSearch'] : '');
// [HTMLか、グリッドか決めるフラッグ]
$htmlFlg = (isset($_POST['htmlflg']) ? $_POST['htmlflg'] : '0');
//ツーバーからのチェックボックスから合計行を判断する場合
$sumflg = (isset($_POST['sumflg']) ? $_POST['sumflg'] : '0');
$isQGflg = (isset($_POST['ISQGFLG']) ? $_POST['ISQGFLG'] : '');//クエリーグループならＴＲＵＥ

//ピボットのトレイダウンの検索パーラメータ入れる事
$PDRILLDOWNSEARCH=(isset($_POST['PDRILLDOWNSEARCH']) ? json_decode($_POST['PDRILLDOWNSEARCH'], true) : array());
//for filtersearch
$filtersearchData = (isset($_POST['filtersearchData']) ? json_decode($_POST['filtersearchData'], true) : array());
//クエリーについてデータ
$chkQry = array();
$csvFlg=true;
$excelFlg=true;


/*
*-------------------------------------------------------*
* 変数
*-------------------------------------------------------*
*/
// テーブルある無しフラグ
// $rs = true;
$rtn = 0;
// データ配列
$csv_d = array();
$len_d = array(); //抽出データの最大桁数
$msg = '';
$errorFlg = 0; //テーブルがなかった場合に1
$fstcol = 1;
//クエリーグループ
$D1NAMELIST=array();//クエリーグループの全部データ
$LASTD1NAME='';//クエリーグループの一番最後のクエリーＩＤ
$DB2WDEF=array();
$D1TEXT = '';
$isQGflg=($isQGflg==='true')?true:false;
$bmkexist = false;
//グループかクエリー
$QBMFLG = 0;
$D1CFLG='';
$D1WEBF='';
/*
*-------------------------------------------------------*
* 処理
*-------------------------------------------------------*
*/
$csv_d = array();
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
if ($querydevice !== 'TOUCH') {
   $usrinfo = $_SESSION['PHPQUERY']['user'];
}else {
   $usrinfo = $_SESSION['PHPQUERYTOUCH']['user'];
}
// ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
   $rs = cmCheckUser($db2con, $usrinfo[0]['WUUID']);
   if ($rs['result'] !== true) {
      $rtn = 1;
      $msg = showMsg($rs['result'], array(
         'ユーザー'
      ));
   }
}
//クエリーグループ
if($rtn===0){
    if($isQGflg){
        $QBMFLG = 1;
        $rs=cmGetQueryGroup($db2con,$d1name,'','');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリー'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
        }
    }
}
if ($rtn === 0) {
   $chkQry = array();
    if($isQGflg){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,$usrinfo[0]['WUUID'],$d1name,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }else{
                //一番最後の
                if($res['LASTFLG']==='1'){
                    $fdb2csv1Data = $chkQry['FDB2CSV1'];
					$DB2WDEF=$chkQry['DB2WDEF'];
                    $d1name=$res['QRYGSQRY'];
                    $D1TEXT.= $chkQry['FDB2CSV1']['D1TEXT'];
                }else{
                    $D1TEXT.= $chkQry['FDB2CSV1']['D1TEXT'] . ',';
                }
            }
        }//end of for loop
    }else{
       $chkQry = array();
       $chkQry = cmChkQuery($db2con, $usrinfo[0]['WUUID'], $d1name, '');
       if ($chkQry['result'] !== true) {
          $rtn = 1;
          $msg = showMsg($chkQry['result'], array(
             'クエリー'
          ));
       }else{
            $fdb2csv1Data = $chkQry['FDB2CSV1'];
			$DB2WDEF=$chkQry['DB2WDEF'];
            $D1TEXT = $chkQry['FDB2CSV1']['D1TEXT'];
       }
    }
}
e_log("getJsonPivotDrillDown.php rtn".$rtn);


//CSV、EXCELダウンロト権限チェックためフラグ取得
if ($rtn === 0) {
    if ($DB2WDEF['WDDWNL'] !== "1") {
        $csvFlg = false;
    }
    if ($DB2WDEF['WDDWN2'] !== "1") {
        $excelFlg = false;
    }
}
if ($rtn === 0) {
    $text_nohtml = $text;
    $text        = cmHsc($text);
    //$LASTD1NAMEというのはクエリーグループの一番最後のクエリー
    $D1WEBF      = ($LASTD1NAME !=='')?$FDB2CSV1[$LASTD1NAME][0]['D1WEBF']:$FDB2CSV1['D1WEBF'];
    // SQLで作成クエリーチェック
    $D1CFLG      = ($LASTD1NAME !=='')?$FDB2CSV1[$LASTD1NAME][0]['D1CFLG']:$FDB2CSV1['D1CFLG'];
    $RDBNM       =($LASTD1NAME !=='')?$FDB2CSV1[$LASTD1NAME][0]['D1RDB']: $FDB2CSV1['D1RDB'];
}
if ($rtn === 0) {
   $RDBNM = $fdb2csv1Data['D1RDB'];
	// クエリー作成フラグ取得
	$D1CFLG = $fdb2csv1Data['D1CFLG'];
	$D1WEBF = $fdb2csv1Data['D1WEBF'];
   if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
      $systables = cmGetSystables($db2con, SAVE_DB, $filename);
   }else {
      $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
      $db2RDBcon = cmDB2ConRDB(SAVE_DB);
      $systables = cmGetSystables($db2RDBcon, SAVE_DB, $filename);
   }
   if (count($systables) > 0) {
      if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
         $resIns = cmInsWTBL($db2con, $filename);
      }else {
         $resIns = cmInsWTBL($db2con, $filename, $db2RDBcon);
      }
      if ($resIns !== 0) {
         // 修正用
         $msg = showMsg('FAIL_MENU_DEL', array(
            '実行中のデータ'

         )); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
         $rtn = 1;
      }
   }
   else {
      $msg = showMsg('FAIL_MENU_DEL', array(
         '実行中のデータ'
      )); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
      $rtn = 1;
      $allcount = 0;
      $errorFlg = 1;
   }
}
if ($rtn === 0) {
   if ($querydevice === 'PC') {
	  error_log("BEFORE DB2WCOL".$usrinfo[0]['WUUID']."-".$d1name);
      $db2wcol = cmGetDB2WCOL($db2con, $usrinfo[0]['WUUID'], $d1name);
	  error_log("AFTER DB2WCOL".print_r($db2wcol,true));
   }
   $FDB2CSV2 = cmGetFDB2CSV2($db2con, $d1name);
   // HTMLtemplate チェック
   $html_templ = false;
   $htmldata = array();
   $html_tagdata = array();
   $html_context = '';
   if ($rtn === 0) {
      $rs = fnGetDB2HTMLT($db2con, $d1name);
      if ($rs['result'] === true) {
         $htmldata = $rs['data'];
         if (count($htmldata) > 0) {
            $html_templ = true;
            $html_filedata = "./htmltemplate/" . $d1name . ".html";
            $html_context = file_get_contents($html_filedata);
            if (!mb_check_encoding($html_context, 'UTF-8')) {
               $rtn = 1;
               $msg = showMsg('HTML_UTF'); //"HTMLテンプレートはUTF-8のファイルのみ登録できます。";
            }
            if ($rtn === 0) {
               $htmltag = fnGetDB2HTMLK_INSHI($db2con);
               if ($htmltag['result'] === true) {
                  $html_tagdata = $htmltag['data'];
                  foreach($html_tagdata as $key => $value) {
                     $l_tag = '<' . $value['HTMLNM'];
                     $u_tag = '<' . strtoupper($value['HTMLNM']);
                     if (strpos($html_context, $l_tag) !== false || strpos($html_context, $u_tag)) {
                        $rtn = 1;
                        $msg = showMsg('HTML_LIMIT'); //" HTMLに禁止タグが含まれています。";
                        break;
                     }
                  }
               }
            }
         }
      }else {
         $rtn = 1;
         $msg = showMsg('FAIL_FUNC', array(
            'クエリーの実行'
         )); //'クエリーの実行に失敗しました。';
      }
   }
   if ($rtn === 0) {
      // $rsWdtlArr = $rsWdtl['data'];
      // CL 実行があるかどうかチェック
      $DB2CSV1PM = array();
      $DB2CSV1PG = array();
      $rs = fnDB2CSV1PG($db2con, $d1name);
      if ($rs['result'] === true) {
         $DB2CSV1PG = $rs['data'];
         $DGCLIG = '';
         $DGCPGM = '';
         if (count($DB2CSV1PG) > 0) { //CL実行がある
            $DGCLIG = $DB2CSV1PG[0]['DGCLIB'];
            $DGCPGM = $DB2CSV1PG[0]['DGCPGM'];
         }
        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
           $allcount = cmGetAllCount($db2con,$FDB2CSV2['data'], $filename,$sSearch,$D1CFLG,$PDRILLDOWNSEARCH);
        }else {
           $allcount = cmGetAllCount($db2RDBcon,$FDB2CSV2['data'], $filename,$sSearch,$D1CFLG,$PDRILLDOWNSEARCH);
        }
        if ($allcount['result'] !== true) {
           // error_log(print_r($allcount,true));
           $msg = showMsg($allcount['result']);
          
           $rtn = 1;
        }
        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
			error_log('PDRILLDOWNSEARCH = '.print_r($PDRILLDOWNSEARCH,true));
			$csv_d=cmGetDbFile($db2con,$FDB2CSV2['data'], $filename,$iDisplayStart, $iDisplayLength, $iSortCol_0, $sSortDir_0, $sortingcols = 0, $sSearch,$db2wcol,false, $D1CFLG,$EXCELVER = '',$PDRILLDOWNSEARCH,$filtersearchData);
			error_log('false 1');
        }else {
			$csv_d=cmGetDbFile($db2RDBcon,$FDB2CSV2['data'], $filename,$iDisplayStart, $iDisplayLength, $iSortCol_0, $sSortDir_0, $sortingcols = 0, $sSearch,$db2wcol,false, $D1CFLG,$EXCELVER = '',$PDRILLDOWNSEARCH,$filtersearchData);
        	error_log('false 2');
		}
        if ($csv_d['result'] !== true) {
			$msg = showMsg($csv_d['result']);          
			$rtn = 1;
        }
      }else {
         $rtn = 1;
         $msg = $rs['result'];
      }
      cmEnd($db2con, $RTCD, $JSEQ, $filename, $d1name);
      if ($RTCD === '9') {
         $rtn = 1;
         $msg = showMsg('FAIL_FUNC', array(
            'クエリーの事後処理'
         )); //'クエリーの事後処理に失敗しました。';
      }
   }
}
if ($rtn === 0) {
   $fstcol = $csv_d['firstcol'];
   $csv_d = umEx($csv_d['data'], (($querydevice !== 'TOUCH' && $html_templ !== true) ? true : false));
}

if($rtn === 0){// MSM add for bmfolder exist or not
  foreach($D1NAMELIST as $key => $res){
    $rs = cmGetDB2QBMK($db2con,$usrinfo[0]['WUUID'], $QRYNAME,$res['PMPKEY'],'',$QBMFLG);
  }
    if($rtn === 0){
        if($rs['result'] !== true){
            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            if($rs['datacount'] > 0){
                $bmkexist = true;
            }
        }
    }
}

if ($rtn === 0) {//MSM add for bmfolder icon show/hide
    $rs = cmGetWUUBFG($db2con, $usrinfo[0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result']);
    } else {
        $WUUBFG = $rs['data'][0]['WUUBFG'];
    }
}

// ワークテーブルのログ情報削除
$resDel = cmDelWTBL($db2con, $filename);
if ($resDel !== 0) {
   error_log('削除テスト：' . $resDel . 'aaaa' . $filename);
   // 修正用
   $msg = showMsg('ログ削除失敗'); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
   $rtn = 1;
}
if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
   cmDb2Close($db2RDBcon);
}
// ワークテーブルのログ情報削除
$resDel = cmDelWTBL($db2con, $filename);
if ($resDel !== 0) {
   error_log('削除テスト：' . $resDel . 'aaaa' . $filename);
   // 修正用
   $msg = showMsg('ログ削除失敗'); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
   $rtn = 1;
}
if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
   cmDb2Close($db2RDBcon);
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'csvFlg'=>$csvFlg,
    'excelFlg'=>$excelFlg,
	'html_context' => $html_context,
	'res_iSortCol_0' => $iSortCol_0,
	'res_sSortDir_0' => $sSortDir_0,
	'res_sSearch' => $searchlist,
	'iTotalRecords' => $allcount['data'],
	'iTotalDisplayRecords' => $allcount['data'],
	'shukeiData' => umEx($shukeiResult['data'], true) ,
	'firstcol' => $fstcol,
	'aaData' => $csv_d,
	'rsmaster' => $rsMasterArr ,
	'rsshukei' => $rsshukeiArr ,
	'rsWdtl' => $rsWdtlArr ,
	'rsDrgs' => $rsDrgsArr ,
	'rtn' => $rtn,
	'msg' => $msg,
	'filename' => $filename,
	'd1name' => $d1name,
	'RTCD' => $RTCD,
	"errorFlg" => $errorFlg,
	'len_d' => $len_d,
	'html_tagdata' => $html_tagdata,
	'ZERO_FLG' => ZERO_FLG,
	'htmlFlg' => $htmlFlg,
	'DB2CSV1PG' => $DB2CSV1PG,
	'usrinfo' => $usrinfo,
	'BACKZERO_FLG' => BACKZERO_FLG,
	'PROTOCOL' => PROTOCOL,
	'IP' => IP,
	'SISTEM' => SISTEM,
	'D1WEBF'=>$D1WEBF,
	'D1CFLG' => $D1CFLG,
	'D1TEXT' => $D1TEXT,
	'BMKEXIST'=> $bmkexist,
	'testklyh' => array($usrinfo[0]['WUUID'], $QRYNAME,'','',$QBMFLG),
	'chkQry'=>$chkQry,//test
	'FDB2CSV1'=>$fdb2csv1Data,//クエリーについてボタン設定
	'AXESPARAM'=>AXESPARAM,
	'PDRILLDOWNSEARCH'=>$PDRILLDOWNSEARCH,//testing
  'WUUBFG' => $WUUBFG
);
unset($csv_d);
echo (json_encode($rtn));
/*
*-------------------------------------------------------*
* 画面定義内容をFDB2CSV3に更新
*-------------------------------------------------------*
*/
function fnSearchUpdate($db2con, $sch_data)
{
   foreach($sch_data as $k1 => $v1) {
      foreach($v1 as $k2 => $v2) {
         foreach($v2 as $k3 => $v3) {
            $rs = cmUpdFDB2CSV3($db2con, $k1, $k2, $k3, $v3['dat'], $v3['type']);
         }
      }
   }
}
/*
*-------------------------------------------------------*
* 項目の最大桁数の値を取得
*-------------------------------------------------------*
*/
function fnGetMaxLenValue($db2con, $fdb2csv2, $filename, $start, $length, $sortcol, $sortdir, $sortingcols = 0, $search)
{
   $rtn = array();
   foreach($fdb2csv2 as $csv2row) {
      $colName = $csv2row['D2FLD'] . '_' . $csv2row['D2FILID'];
      $maxdata = '';
      $params = array();
      $strSQL = '';
      $strSQL.= ' SELECT ' . $colName . ' ';
      $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' ';
      $strSQL.= ' WHERE LENGTH(RTRIM(' . $colName . ')) = ';
      $strSQL.= ' ( ';
      $strSQL.= ' SELECT ';
      $strSQL.= ' MAX(LENGTH(RTRIM(A.' . $colName . '))) ';
      $strSQL.= ' FROM( ';
      $strSQL.= ' SELECT B.* , ';
      $strSQL.= ' ROWNUMBER() OVER ( ';
      // ソート
      if ($sortcol !== "") {
         $sortColumn = $sortcol;
         $strSQL.= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
      }
      $strSQL.= ') ';
      $strSQL.= ' AS rownum ';
      $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as B ';
      // 検索条件
      if ($search != '') {
         if (count($fdb2csv2) > 0) {
            $strSQL.= ' WHERE ';
            $paramSQL = array();
            foreach($fdb2csv2 as $key => $value) {
               $paramSQL[] = $value['D2FLD'] . '_' . $value['D2FILID'] . ' LIKE ? ';
               $params[] = '%' . $search . '%';
               /*if ($key < (count($fdb2csv2) - 1)) {
                  $strSQL.= ' OR ';
               }*/
               $strSQL .= join(' OR ', $paramSQL);
            }
         }
      }
      $strSQL.= ' ) AS A ';
      // 抽出範囲指定
      if (($start != '') && ($length != '')) {
         $strSQL.= ' WHERE A.rownum BETWEEN ? AND ? ';
         $params[] = $start + 1;
         $params[] = $start + $length;
      }
      $strSQL.= ' ) FETCH FIRST 1 ROWS ONLY ';
      $stmt = db2_prepare($db2con, $strSQL);
      if ($stmt === false) {
         $rtn = false;
      }
      else {
         $r = db2_execute($stmt, $params);
         if ($r === false) {
            $rtn = false;
         }
         else {
            while ($row = db2_fetch_assoc($stmt)) {
               $maxdata = $row[$colName];
            }
            $rtn[$colName] = cmMer($maxdata);
         }
      }
   }
   return $rtn;
}
function fnGetMaxLen($db2con, $fdb2csv2, $filename)
{
   $rtn = array();
   $strSelect = '';
   foreach($fdb2csv2 as $csv2row) {
      $colname = $csv2row['D2FLD'] . '_' . $csv2row['D2FILID'];
      $strSelect.= 'max(length(rtrim(A.' . $colname . '))) AS ' . $colname . ',';
   }
   $strSelect = rtrim($strSelect, ',');
   $strSQL = '';
   $strSQL.= ' SELECT ' . $strSelect . ' ';
   $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' AS A ';
   $params = array();
   $stmt = db2_prepare($db2con, $strSQL);
   if ($stmt === false) {
      $data = false;
   }
   else {
      db2_execute($stmt, $params);
      if ($r === false) {
         $data = false;
      }
      else {
         while ($row = db2_fetch_assoc($stmt)) {
            $data = $row;
         }
      }
   }
   $rtn = $data;
   return $rtn;
}
function fnDB2CSV1PG($db2con, $d1name)
{
   $rs = array();
     $fdb2csv1pg = array();
   // FDB2CSV1PGの取得
   $strSQL = ' SELECT A.DGNAME,A.DGCLIB,A.DGCPGM,A.DGCBNM,B.D1LIBL ';
   $strSQL.= ' FROM FDB2CSV1PG AS A ';
   $strSQL.= ' JOIN FDB2CSV1 AS B ON A.DGNAME = B.D1NAME ';
   $strSQL.= ' WHERE A.DGNAME = ? ';
   $stmt = db2_prepare($db2con, $strSQL);
   if ($stmt === false) {
      $rs = array(
         'result' => 'FAIL_SEL'
      );
   }
   else {
      $params = array(
         $d1name
      );
      $r = db2_execute($stmt, $params);
      if ($r === false) {
         $rs = array(
            'result' => 'FAIL_SEL'
         );
      }
      else {
         while ($row = db2_fetch_assoc($stmt)) {
            foreach($row as $k => $v){
                $row[$k] = cmMer($v);
            }
            $fdb2csv1pg[] = $row;
         }
         $rs = array(
            'result' => true,
            'data' => $fdb2csv1pg
         );
      }
   }
   return $rs;
}
/*
 *-------------------------------------------------------* 
 * 全件カウント取得
 *-------------------------------------------------------*
 */
/*
function fnGetAllCount($db2con, $fdb2csv2, $filename,$search,$searchlist, $D1CFLG = ''){
    $data   = array();
    $params = array();
	$getSQL="";
	$sysdata=array();
	$getTmpCOLDATA=fnGetColumnDataType($db2con,$filename,$searchlist);
	if($getTmpCOLDATA["result"]!==true){
		$data=array("result"=>$getTmpCOLDATA["result"]);
	}else{
	    $getSQL =$getTmpCOLDATA["SQL"];
	    $sysdata=$getTmpCOLDATA["SYSDATA"];
		$searchlist=$getTmpCOLDATA["SEARCHDATA"];
	    $strSQL = ' SELECT count(*) as COUNT ';
	    $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
	    if (count($searchlist)>0) {
			$strSQL .= ' WHERE ';
			foreach ($searchlist as $key => $value) {
				switch ($value['dtype']) {
				    case 'L':
				        $strSQL .= 'CAST('.cmMer($value["key"]). ' AS CHAR(10))' . ' = ? ';
				        break;
				    case 'T':
				        $strSQL .= 'CAST('.cmMer($value["key"]). ' AS CHAR(8))' . '  = ? ';
				        break;
				    case 'Z':
				        $strSQL .= 'CAST('.cmMer($value["key"]). ' AS CHAR(26))' . ' = ? ';
				        break;
				    default:
				        $strSQL .= cmMer($value["key"])." = ? ";
				        break;
				}
				$params[]=cmMer($value["val"]);
				if ($key < (count($searchlist) - 1)) {
					$strSQL .= ' AND ';
				}
			}
	    }
        if ($search != '') {
            if (count($fdb2csv2) > 0) {
                $strSQL .= (count($searchlist)>0)? ' AND ( ': ' WHERE ( ';
                $paramSQL = array();
                foreach ($fdb2csv2 as $key => $value) {
                    switch ($value['D2TYPE']) {
                        case 'L':
                            $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . ' LIKE ? ';
                            break;
                        case 'T':
                            $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(8))' . ' LIKE ? ';
                            break;
                        case 'Z':
                            $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(26))' . ' LIKE ? ';
                            break;
                        default:
                            $paramSQL[] = cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
                            break;
                    }
                    $params[] = '%' . $search . '%';
                }
                $strSQL .= join(' OR ', $paramSQL);
                $strSQL .= " ) ";

            }
        }
	    $stmt = db2_prepare($db2con, $strSQL);
	    if ($stmt === false) {
	        error_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
	        $data = array(
	            'result' => 'FAIL_SEL'
	        );
	    } else {
	        $r = db2_execute($stmt, $params);
	        error_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
	        if ($r === false) {
	            $data = array(
	                'result' => 'FAIL_SEL'
	            );
	        } else {
	            while ($row = db2_fetch_assoc($stmt)) {
	                $data[] = $row['COUNT'];
	            }
	            $data = array(
	                'result' => true,
	                'data' => $data[0]
	            );
	        }
	    }
	}
    return $data;
}*/

/*
function fnGetDbFile($db2con,$fdb2csv2,$filename,$search,$searchlist,$D1CFLG,$start,$length,$sort,$sortDir){
	$data = array();
	$params=array();
	$testparams=array();
	$testStr='';
	$getSQL="";
	$sysdata=array();
	$getTmpCOLDATA=fnGetColumnDataType($db2con,$filename,$searchlist,$D1CFLG);
	if($getTmpCOLDATA["result"]!==true){
		$data=array("result"=>$getTmpCOLDATA["result"]);
	}else{
	    $getSQL =$getTmpCOLDATA["SQL"];
	    $sysdata=$getTmpCOLDATA["SYSDATA"];
		$searchlist=$getTmpCOLDATA["SEARCHDATA"];
		$FINALSQL=$getTmpCOLDATA["FINALSQL"];
	    $strSQL   = ' SELECT C.*';
	    $strSQL  .= 'FROM ( SELECT A.* ,';
		$strSQL  .= ' ROWNUMBER() OVER( ';
		if($sort !== ''){
			$strSQL .= ' ORDER BY A.'.$sort.' '.$sortDir.' ';
		}
	    $strSQL .= ' ) as rownum ';
	    $strSQL .= ' FROM (  ';
		$strSQL.=' SELECT '.$getSQL.' ';
		$testStr.=' SELECT '.$getSQL.' ';
	    $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
	    $testStr .= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
		if (count($searchlist)>0) {
			$strSQL .= ' WHERE ';
			$testStr .=' WHERE ';
			foreach ($searchlist as $key => $value) {
				switch ($value['dtype']) {
				    case 'L':
				        $strSQL .= 'CAST('.cmMer($value["key"]). ' AS CHAR(10))' . ' = ? ';
						$testStr.='CAST('.cmMer($value["key"]). ' AS CHAR(10))' . " = '".cmMer($value["val"])."'";
				        break;
				    case 'T':
				        $strSQL .= 'CAST('.cmMer($value["key"]). ' AS CHAR(8))' . '  = ? ';
						$testStr.='CAST('.cmMer($value["key"]). ' AS CHAR(8))' . " = '".cmMer($value["val"])."'";
				        break;
				    case 'Z':
				        $strSQL .= 'CAST('.cmMer($value["key"]). ' AS CHAR(26))' . ' = ? ';
						$testStr.='CAST('.cmMer($value["key"]). ' AS CHAR(26))' ." = '".cmMer($value["val"])."'";
				        break;
				    default:
				        $strSQL .= cmMer($value["key"])." = ? ";
						if($value['dtype']==="P" || $value['dtype']==="S" ||$value['dtype']==="B"){
							$testStr .= cmMer($value["key"])." = ".cmMer($value["val"]);
						}else{
							$testStr .= cmMer($value["key"])." = '".cmMer($value["val"])."'";
						}
				        break;
				}
				$params[]=cmMer($value["val"]);
				$testparams[]=cmMer($value["val"]);
				if ($key < (count($searchlist) - 1)) {
					$strSQL .= ' AND ';
					$testStr .=' AND ';
				}
			}
		}
        if ($search != '') {
            if (count($fdb2csv2) > 0) {
                $strSQL .= (count($searchlist)>0)? ' AND ( ': ' WHERE ( ';
				$testStr.= (count($searchlist)>0)? ' AND ( ': ' WHERE ( ';
                $paramSQL = array();
				$testparamSQL=array();
                foreach ($fdb2csv2 as $key => $value) {
                    switch ($value['D2TYPE']) {
                        case 'L':
                            $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . ' LIKE ? ';
                            $testparamSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' ." LIKE '".'%' . $search . '%'."'";
                            break;
                        case 'T':
                            $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(8))' . ' LIKE ? ';
                            $testparamSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . " LIKE '".'%' . $search . '%'."'";
                            break;
                        case 'Z':
                            $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(26))' . ' LIKE ? ';
                            $testparamSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . " LIKE '".'%' . $search . '%'."'";
                            break;
                        default:
                            $paramSQL[] = cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
							$testparamSQL[] = cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . " LIKE '".'%' . $search . '%'."'";
                            break;
                    }
                    $params[] = '%' . $search . '%';
                }
                $strSQL .= join(' OR ', $paramSQL);
				$testStr.= join(' OR ', $testparamSQL);
                $strSQL .= " ) ";
				$testStr.= " ) ";

            }
        }
	    $strSQL .= ' ) as A ';
	    $strSQL .= ' ) as C ';
		if(($start != '')&&($length != '')){
		  $strSQL .= ' WHERE C.rownum BETWEEN ? AND ? ';
		  array_push($params,$start + 1);
		  array_push($params,$start + $length);
		}
		$stmt = db2_prepare($db2con,$strSQL);
		if($stmt === false){
		  $data = array('result' => 'FAIL_SEL');
		}else{
			$r = db2_execute($stmt,$params);
			if($r === false){
				$data = array('result' => 'FAIL_SEL');
			}else{
				while ($row = db2_fetch_array($stmt)) {
					unset($row[count($row) - 1]); //最後の列を削除
					foreach($row as $key => $val){
						$row[$key]=$val;
					}
					$data[] = $row;
				}
				$data = array('result' => true,'data' => $data,'createSQL'=>$testStr,"FINALSQL"=>$FINALSQL,'createParam'=>$testparams);
			}
		}
	}
    return $data;
}
*/

//ピボットと制御なら全部のカラムを使わないので、まずSELECT QRY
function fnGetColumnDataType($db2con,$filename,$searchlist,$D1CFLG){
	$rs=true;
	$data=array();
	$outCName="";
	$outCNameSQL="";
	$sysdata=array();
    $selTblSQL = '';
    $selTblSQL.= ' SELECT SYSTEM_COLUMN_NAME AS COLUMN_NAME,DDS_TYPE, ';
    if(SYSCOLCFLG==='1'){
        $selTblSQL.='COLUMN_TEXT  AS COLUMN_HEADING ';
    }else{
        $selTblSQL.='COLUMN_HEADING ';
    }
    $selTblSQL.=' FROM ' . SYSCOLUMN2 . ' ';
    $selTblSQL.= 'WHERE TABLE_SCHEMA = ? AND   TABLE_NAME =? AND SYSTEM_COLUMN_NAME  NOT IN(?,?,?,?) ';
    $stmt = db2_prepare($db2con, $selTblSQL);
    if ($stmt === false) {
        $rs='FAIL_SEL';
        error_log('クエリー実行失敗しました。' . db2_stmt_errormsg());
    } else {
      $param = array(SAVE_DB, $filename, 'ROWNUM', 'RN', 'XROWNUM', 'YROWNUM');
      $r = db2_execute($stmt, $param);
      if ($r === false) {
        $rs='FAIL_SEL';
          error_log('クエリー実行失敗しました。'. db2_stmt_errormsg()); //'クエリー実行失敗しました。'.db2_stmt_errormsg();            
      } else {
			$sysdata = array();
			while ($row = db2_fetch_assoc($stmt)) {
				$sysdata[] = $row;
				$outCName.= $row['COLUMN_NAME'] . ',';
				if($D1CFLG==="1"){
					$outCNameSQL.= $row['COLUMN_NAME'] ."AS ".preg_replace('/_[0-9]+(\s)*/i','',$row['COLUMN_NAME']). ',';
				}else{
					$outCNameSQL.= $row['COLUMN_NAME'] . ',';
				}
				for($i=0;$i<count($searchlist);$i++){
					$stext=$searchlist[$i];
					if(cmMer($stext["key"])===cmMer($row['COLUMN_NAME'])){
						$searchlist[$i]["dtype"]=cmMer($row['DDS_TYPE']);
					}
				}
			}
			//一番後ろのコンマを消す
			$outCName = substr($outCName, 0, -1);
			$outCNameSQL=substr($outCNameSQL, 0, -1);
      }
    }
  return array("result"=>$rs,"SQL"=>$outCName,"FINALSQL"=>$outCNameSQL,"SYSDATA"=>$sysdata,"SEARCHDATA"=>$searchlist);
}

function fnDropTmp($db2con,$newfilename){
	$strSQL = ' DROP TABLE '.SAVE_DB."/".$newfilename;
	$stmt   = @db2_prepare($db2con, $strSQL);
	if ($stmt === false) {
	    // error
	} else {
	    $rs = @db2_execute($stmt);
	}
}

/*
 *-------------------------------------------------------* 
 * CLで作成されたDBを読み込み
 *-------------------------------------------------------*
 */
function fnGetDbFile($db2con, $fdb2csv2, $filename, $start, $length, $sortcol, $sortdir, $sortingcols = 0, $search, $db2wcol = array(), $dnlFlg = false, $D1CFLG = '',$EXCELVER = '',$searchDrillList=array())
{
    $rtn        = 0;
    $data       = array();
    $params     = array();
    $selecttype = array();

    error_log("このへcmGetDbFileんか********".print_r($db2wcol,true).'FLG'.$D1CFLG);
   
    if ($rtn === 0) {
        $rsChk = chkFDB2CSV2WKTblCol($db2con, $filename, $fdb2csv2, $D1CFLG);
        if ($rsChk['result'] !== true) {
            $rtn  = 1;
            $data = array(
                'result' => $rsChk['result'] // recreate table error
            );
            //error_log("ここ入ったね＜＞");
            
        }
    }
    if ($rtn === 0) {
        //フィールドIDとフィールド名をキーにしてダウンロード可否フラグを持たせる配列
        //error_log('【削除】FDB2SCV2データcmGetDbFile'.print_r($fdb2csv2,true));
        $compFDB2CSV2 = comparsionFDB2CSV2($fdb2csv2);
//        error_log('【削除】compFDB2CSV2データcmGetDbFile'.print_r($compFDB2CSV2,true));
        $strSQL = '';
        if ($D1CFLG === '') {
            $strSQL .= ' SELECT ';
            if (count($db2wcol) > 0) {
                $wcol = $db2wcol;
                foreach ($wcol as $key => $value) {
                    //array_push($selecttype, $value['WCTYPE']);
                    //ダウンロードフラグがtrueの場合、FDB2CSV2のダウンロードフラグを見る
                    if ($dnlFlg === true) {
                        if ($value['WCHIDE'] === '0' && isset($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']])) {
                            if ($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']] !== '1') {
			                    $selecttype[] = $value['WCTYPE'];
                                $strSQL .= ' A.' . $value['WCINDX'];
                                $strSQL .= ' , ';
                            }
                        }
                    } else {
                        $strSQL .= ' A.' . $value['WCINDX'];
                        $strSQL .= ' , ';
                    }
                }
                $strSQL .= ' A.ROWNO ';
                //array_push($selecttype, "wcol");
                $selecttype[] = 'wcol';
            } else {
                if ($dnlFlg === true) {
                    foreach ($fdb2csv2 as $key => $value) {
                        //array_push($selecttype, $value['D2TYPE']);
                        if ($value['D2DNLF'] !== '1') {
	                        $selecttype[] = $value['D2TYPE'];
                            $strSQL .= ' A.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                            $strSQL .= ' , ';
                        }
                    }
                    $strSQL .= ' A.ROWNO ';
                } else {
                    foreach ($fdb2csv2 as $key => $value) {
                        $strSQL .= ' A.' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']);
                        
                        if (($key + 1) < count($fdb2csv2)) {
                            $strSQL .= ' , ';
                        }
                        //array_push($selecttype, $value['D2TYPE']);
                        $selecttype[] = $value['D2TYPE'];
                    }
                    $strSQL .= ' , A.ROWNO ';
                }
                //array_push($selecttype, "fdb2csv");
                $selecttype[] = 'fdb2csv';
            }
            $strSQL .= ' FROM( ';
            $strSQL .= ' SELECT B.* , ';
            
            $strSQL .= ' ROWNUMBER() OVER ( ';
            
            //ソート
            if ($sortcol !== "") {
                $sortColumn = $sortcol;
                $strSQL .= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
            }
            
            $strSQL .= ') ';
            $strSQL .= ' AS ROWNO ';
            $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' as B ';
            $isWhereFlg=false;
            if ($search != '') {
                if (count($fdb2csv2) > 0) {
					$isWhereFlg=true;
                    $strSQL .= ' WHERE ( ';
                    $paramSQL = array();
                    foreach ($fdb2csv2 as $key => $value) {
                        switch ($value['D2TYPE']) {
                            case 'L':
                                $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . ' LIKE ? ';
                                break;
                            case 'T':
                                $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(8))' . ' LIKE ? ';
                                break;
                            case 'Z':
                                $paramSQL[] = 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(26))' . ' LIKE ? ';
                                break;
                            default:
                                $paramSQL[] = cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
                                break;
                        }
                        $params[] = '%' . $search . '%';
                    }
                    $strSQL .= join(' OR ', $paramSQL);
					$strSQL .= ' ) ';
                }
            }
			if (count($searchDrillList)>0) {
				$strSQL .= ($isWhereFlg)? ' AND ( ':' WHERE ( ';
				$paramDSQL = array();
				foreach ($searchDrillList as $key => $value) {
				    switch ($value['dtype']) {
				        case 'L':
				            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
				            break;
				        case 'T':
				            $paramDSQL[] = 'CAST(' .cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
				            break;
				        case 'Z':
				            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
				            break;
				        default:
				            $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
				            break;
				    }
				    $params[] =cmMer($value["val"]);
				}
				$strSQL .= join(' AND ', $paramDSQL);
				$strSQL .= ' ) ';
			}
            $strSQL .= ' ) AS A ';
            
            //抽出範囲指定
            if (($start !== '') && ($length !== '')) {
                $strSQL .= ' WHERE A.ROWNO BETWEEN ? AND ? ';
                /*array_push($params, $start + 1);
                array_push($params, $start + $length);*/
                $params[] = $start + 1;
                $params[] = $start + $length;
            }
            $strSQL .= ' ORDER BY A.ROWNO ';
        } else {
            if ($sortcol !== '') {
                $sortColArr = explode('_', $sortcol);
                if ($sortColArr[count($sortColArr) - 1] === '0') {
                    array_pop($sortColArr);
                    $sortcol = join($sortColArr);
                }
            }
            $strSQL .= ' SELECT ';
            if (count($db2wcol) > 0) {
                $wcol = umEx($db2wcol);
                foreach ($wcol as $key => $value) {
                    //array_push($selecttype, $value['WCTYPE']);
                    //ダウンロードフラグがtrueの場合、FDB2CSV2のダウンロードフラグを見る
                    if ($dnlFlg === true) {
                        if ($value['WCHIDE'] === '0' && isset($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']])) {
                            if ($compFDB2CSV2['filid' . $value['WCFILID']][$value['WCINDX']] !== '1') {
			                    $selecttype[] = $value['WCTYPE'];
                                $strSQL .= ' A.' . $value['WCFLD'] . '_' . $value['WCFILID'];
                                $strSQL .= ' , ';
                            }
                        }
                    } else {
                        $strSQL .= ' A.' . $value['WCFLD'] . '_' . $value['WCFILID'];
                        $strSQL .= ' , ';
                    }
                }
                $strSQL .= ' A.ROWNO ';
                //array_push($selecttype, "wcol");
                $selecttype[] = 'wcol';
            } else {
                //$fdb2csv2 = umEx($fdb2csv2);
                if ($dnlFlg === true) {
                    foreach ($fdb2csv2 as $key => $value) {
                        //array_push($selecttype, $value['D2TYPE']);
                        if ($value['D2DNLF'] !== '1') {
		                     $selecttype[] = $value['D2TYPE'];
                            $strSQL .= ' A.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                            $strSQL .= ' , ';
                        }
                    }
                    $strSQL .= ' A.ROWNO ';
                } else {
                    $paramSQL = array();
                    foreach ($fdb2csv2 as $key => $value) {
                        $paramSQL[]   = ' A.' . $value['D2FLD'] . '_' . $value['D2FILID'];
                        /*if (($key + 1) < count($fdb2csv2)) {
                        $strSQL .= ' , ';
                        }*/
                        //array_push($selecttype, $value['D2TYPE']);
                        $selecttype[] = $value['D2TYPE'];
                    }
                    $strSQL .= join(' , ', $paramSQL);
                    $strSQL .= ' , A.ROWNO ';
                }
                array_push($selecttype, "fdb2csv");
            }
            $strSQL .= ' FROM( ';
            $strSQL .= ' SELECT  ';
            $rs = fngetSQLFldCCSID($db2con, $filename);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result']);
            } else {
                error_log('fldCCSIDresult'.print_r($rs,true));
                $fldCCSIDARR = $rs['data'];
                $fldccsidCnt = $rs['CCSIDCNT'];
                if ($fldccsidCnt > 0) {
                    $fldArr = array();
                    foreach ($fldCCSIDARR as $fldccsid) {
                        if ($fldccsid['CCSID'] === '65535' && $fldccsid['DDS_TYPE'] !== '5' && $fldccsid['DDS_TYPE'] !== 'H') {

                            if($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW'){
                                $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(4) CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                            }else{
                                $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                            }


                        } else {
                            $fldArr[] = $fldccsid['COLUMN_NAME'] . ' AS ' . $fldccsid['COLUMN_NAME'] . '_0 ';
                        }
                    }
                } else {
                    foreach ($fdb2csv2 as $key => $value) {
                        $fldArr[] = $value['D2FLD'] . ' AS ' . cmMer($value['D2FLD']) . '_0 ';
                    }
                }
            }
            $strSQL .= join(',', $fldArr);
            $strSQL .= ' , ROWNUMBER() OVER ( ';
            
            //ソート
            if ($sortcol !== "") {
                $sortColumn = $sortcol;
                $strSQL .= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
            }
            $strSQL .= ') ';
            $strSQL .= ' AS ROWNO ';
            $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' as B ';
            
            if ($search != '') {
                if (count($fldCCSIDARR) > 0) {
                    $strSQL .= ' WHERE ';
                    $paramSQL = array();
                    foreach ($fldCCSIDARR as $key => $value) {
                        switch ($value['DDS_TYPE']) {
                            case 'L':
                                if ($value['CCSID'] == '65535') {
                                    $paramSQL[] = 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(10)  CCSID 5026)' . ' LIKE ? ';
                                } else {
                                    $paramSQL[] = 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(10) )' . ' LIKE ? ';
                                }
                                break;
                            case 'T':
                                if ($value['CCSID'] == '65535') {
                                    $paramSQL[] = 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(8) CCSID 5026)' . ' LIKE ? ';
                                } else {
                                    $paramSQL[] = 'CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(8))' . ' LIKE ? ';
                                }
                                break;
                            case 'Z':
                                if ($value['CCSID'] === '65535') {
                                    $paramSQL[] = ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(26) CCSID 5026)' . ' LIKE ? ';
                                } else {
                                    $paramSQL[] = ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(26))' . ' LIKE ? ';
                                }
                                break;
                            default:
                                if ($value['CCSID'] === '65535' && $value['DDS_TYPE'] !== '5' && $value['DDS_TYPE'] !== 'H') {
                                    if($value['COLUMN_NAME'] === 'BREAKLVL' || $value['COLUMN_NAME'] === 'OVERFLOW'){
                                        $paramSQL[] = ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(4) CCSID 5026)' . ' LIKE ? ';
                                    }else{
                                        $paramSQL[] = ' CAST(' . $value['COLUMN_NAME'] . ' AS CHAR(' . $value['LENGTH'] . ') CCSID 5026)' . ' LIKE ? ';
                                    }

                                } else {
                                    $paramSQL[] = $value['COLUMN_NAME'] . ' LIKE ? ';
                                }
                                break;
                        }
                        $params[] = '%' . $search . '%';
                    }
                    $strSQL .= join(' OR ', $paramSQL);
                }
            }
            $strSQL .= ' ) AS A ';
            
            //抽出範囲指定
            if (($start !== '') && ($length !== '')) {
                $strSQL .= ' WHERE A.ROWNO BETWEEN ? AND ? ';
                //array_push($params, $start + 1);
                //array_push($params, $start + $length);
                $params[] = $start + 1;
                $params[] = $start + $length;
            }
            $strSQL .= ' ORDER BY A.ROWNO ';
        }
        
error_log("getJsonPivotDrillDown.php Count=>".$strSQL.print_r($params,true));
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $rtn  = 1;
            $data = array(
                'result' => 'FAIL_SEL' //normal sql error
            );
        } else {
            $r = db2_execute($stmt, $params);
            //error_log('実行SQL：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
            if ($r === false) {
                $rtn  = 1;
                $data = array(
                    'result' => 'FAIL_SEL' // recreate table error
                );
            } else {
                while ($row = db2_fetch_array($stmt)) {
                    unset($row[count($row) - 1]); //最後の列を削除
					if($dnlFlg === true && $EXCELVER === 'XLSX'){
						foreach($row as $key => $val){
							if($selecttype[$key] === 'S' || $selecttype[$key] === 'P' || $selecttype[$key] === 'B'){
								$row[$key] = (float)$val;
							}else{
								$row[$key] = (string)$val;
							}
						}
					}
                    $data[] = $row;
                }
                $data = array(
                    'result' => true,
                    'data' => $data,
                    'selecttype' => $selecttype
                );
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------* 
 * 全件カウント取得
 *-------------------------------------------------------*
 */

function fnGetAllCount($db2con, $fdb2csv2, $filename, $search, $D1CFLG = '',$searchDrillList=array())
{
    $data   = array();
    $params = array();
	$isWhereFlg=false;
    if ($D1CFLG === '') {
        // $strSQL = ' SELECT count(A.' . cmMer($fdb2csv2[0]['D2FLD']) . '_' . cmMer($fdb2csv2[0]['D2FILID']) . ') as COUNT ';
        $strSQL = ' SELECT count(*) as COUNT ';
        $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
        if ($search != '') {
            if (count($fdb2csv2) > 0) {
				$isWhereFlg=true;
                $strSQL .= ' WHERE ';
                foreach ($fdb2csv2 as $key => $value) {
                    switch ($value['D2TYPE']) {
                        case 'L':
                            $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . ' LIKE ? ';
                            break;
                        case 'T':
                            $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(8))' . ' LIKE ? ';
                            break;
                        case 'Z':
                            $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(26))' . ' LIKE ? ';
                            break;
                        default:
                            $strSQL .= cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
                            break;
                    }
                    $params[]= '%' . $search . '%';
                    if ($key < (count($fdb2csv2) - 1)) {
                        $strSQL .= ' OR ';
                    }
                }
            }
        }
		if (count($searchDrillList)>0) {
			$strSQL .= ($isWhereFlg)? ' AND ( ':' WHERE ( ';
			$paramDSQL = array();
			foreach ($searchDrillList as $key => $value) {
			    switch ($value['dtype']) {
			        case 'L':
			            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
			            break;
			        case 'T':
			            $paramDSQL[] = 'CAST(' .cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
			            break;
			        case 'Z':
			            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
			            break;
			        default:
			            $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
			            break;
			    }
			    $params[] =cmMer($value["val"]);
			}
			$strSQL .= join(' AND ', $paramDSQL);
			$strSQL .= ' ) ';
		}
    } else {
        //$strSQL = ' SELECT count(A.' . cmMer($fdb2csv2[0]['D2FLD']) . ') as COUNT ';
        $strSQL = ' SELECT count(*) as COUNT ';
        $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
        if ($search != '') {
            $rs = fngetSQLFldCCSID($db2con, $filename);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result']);
            } else {
                //error_log('fldCCSIDresult'.print_r($rs,true));
                $fldCCSIDARR = $rs['data'];
                $fldccsidCnt = $rs['CCSIDCNT'];
                if ($fldccsidCnt > 0) {
                    $fldArr = array();
                    foreach ($fldCCSIDARR as $fldccsid) {
                        switch ($fldccsid['DDS_TYPE']) {
                            case 'L':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(10) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL .= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(10))' . ' LIKE ? ';
                                }
                                break;
                            case 'T':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(8) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL .= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(8))' . ' LIKE ? ';
                                }
                                break;
                            case 'Z':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(26) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL .= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(26))' . ' LIKE ? ';
                                }
                                break;
                            default:
                                if ($fldccsid['CCSID'] === '65535') {

                                    if($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW'){
                                        $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(4) CCSID 5026) ' . ' LIKE ? ';
                                    }else{

                                        $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) ' . ' LIKE ? ';

                                    }


                                } else {
                                    $fldArr[] = $fldccsid['COLUMN_NAME'] . ' LIKE ? ';
                                }
                                break;
                        }
                    }
                } else {
                    if (count($fdb2csv2) > 0) {
						$isWhereFlg=true;
                        $strSQL .= ' WHERE ';
                        foreach ($fdb2csv2 as $key => $value) {
                            switch ($value['D2TYPE']) {
                                case 'L':
                                    $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(10))' . ' LIKE ? ';
                                    break;
                                case 'T':
                                    $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(8))' . ' LIKE ? ';
                                    break;
                                case 'Z':
                                    $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(26))' . ' LIKE ? ';
                                    break;
                                default:
                                    $strSQL .= cmMer($value['D2FLD']) . ' LIKE ? ';
                                    break;
                            }
                            array_push($params, '%' . $search . '%');
                            if ($key < (count($fdb2csv2) - 1)) {
                                $strSQL .= ' OR ';
                            }
                        }
                    }
					if (count($searchDrillList)>0) {
						$strSQL .= ($isWhereFlg)? ' ( ':' WHERE ( ';
						$paramDSQL = array();
						foreach ($searchDrillList as $key => $value) {
						    switch ($value['dtype']) {
						        case 'L':
						            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
						            break;
						        case 'T':
						            $paramDSQL[] = 'CAST(' .cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
						            break;
						        case 'Z':
						            $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
						            break;
						        default:
						            $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
						            break;
						    }
						    $params[] =cmMer($value["val"]);
						}
						$strSQL .= join(' AND ', $paramDSQL);
						$strSQL .= ' ) ';
					}
                }
            }
        }
    }
error_log("getJsonPivotDrillDown.php All count=>".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        error_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        error_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row['COUNT'];
            }
            $data = array(
                'result' => true,
                'data' => $data[0]
            );
        }
    }
    return $data;
}

