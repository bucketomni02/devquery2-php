<?php
/*
*-------------------------------------------------------*
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once ("common/inc/config.php");

include_once ("common/inc/common.inc.php");

include_once ("base/comGetFDB2CSV2_CCSID.php");

include_once("licenseInfo.php");

/*
*-------------------------------------------------------*
* DataTableリクエスト
*-------------------------------------------------------*
*/
$d1name = $_POST['D1NAME'];
$QRYNAME = $d1name;
$filename = $_POST['csvfilename'];
$querydevice = $_POST['querydevice'];
$iDisplayStart = (isset($_POST['iDisplayStart']) ? $_POST['iDisplayStart'] : '');
$iDisplayLength = (isset($_POST['iDisplayLength']) ? $_POST['iDisplayLength'] : '');
$iSortCol_0 = (isset($_POST['iSortCol_0']) ? $_POST['iSortCol_0'] : '');
$sSortDir_0 = (isset($_POST['sSortDir_0']) ? $_POST['sSortDir_0'] : '');
// 検索キー
$sSearch = (isset($_POST['sSearch']) ? $_POST['sSearch'] : '');
$filtersearchData = (isset($_POST['filtersearchData']) ? json_decode($_POST['filtersearchData'], true) : array());
// [HTMLか、グリッドか決めるフラッグ]
$htmlFlg = (isset($_POST['htmlflg']) ? $_POST['htmlflg'] : '0');
//ツーバーからのチェックボックスから合計行を判断する場合
$sumflg = (isset($_POST['sumflg']) ? $_POST['sumflg'] : '0');
$isQGflg = (isset($_POST['ISQGFLG']) ? $_POST['ISQGFLG'] : '');//クエリーグループならＴＲＵＥ



/*
*-------------------------------------------------------*
* 変数
*-------------------------------------------------------*
*/
// テーブルある無しフラグ
// $rs = true;
$rtn = 0;
// データ配列
$csv_d = array();
$len_d = array(); //抽出データの最大桁数
$db2jksk = array();//array for DB2JKSK
$msg = '';
$errorFlg = 0; //テーブルがなかった場合に1
$fstcol = 1;
//クエリーグループ
$D1NAMELIST=array();//クエリーグループの全部データ
$LASTD1NAME='';//クエリーグループの一番最後のクエリーＩＤ
$D1TEXT = '';
$isQGflg=($isQGflg==='true')?true:false;
$bmkexist = false;
//グループかクエリー
$QBMFLG = 0;
/*
*-------------------------------------------------------*
* 処理
*-------------------------------------------------------*
*/

$csv_d = array();
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
if ($querydevice !== 'TOUCH') {
   $usrinfo = $_SESSION['PHPQUERY']['user'];
}else {
   $usrinfo = $_SESSION['PHPQUERYTOUCH']['user'];
}
// ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
   $rs = cmCheckUser($db2con, $usrinfo[0]['WUUID']);
   if ($rs['result'] !== true) {
      $rtn = 1;
      $msg = showMsg($rs['result'], array(
         'ユーザー'
      ));

   }
}
//クエリーグループ
if($rtn===0){
    if($isQGflg){
        $QBMFLG = 1;
        $rs=cmGetQueryGroup($db2con,$d1name,'','');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリー'));
           
        }else{
            if(!$licenseSql){
                foreach($rs['data'] as $key => $res){
                    if($res['D1CFLG'] === ''){
                        array_push($D1NAMELIST,$rs['data'][$key]);
                    }      
                }
                $D1NAMELIST[count($D1NAMELIST)-1]['LASTFLG'] = '1';
                $D1NAMELIST = umEx($D1NAMELIST,true);
            }else{
                $D1NAMELIST=umEx($rs['data'],true);
            }
        }
    }
}
if ($rtn === 0) {
   $chkQry = array();
    if($isQGflg){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,$usrinfo[0]['WUUID'],$d1name,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリー'));
           
                break;
            }else{
                //一番最後の
                if($res['LASTFLG']==='1'){
                    $fdb2csv1Data = $chkQry['FDB2CSV1'];
                    $d1name=$res['QRYGSQRY'];
                   
                    $D1TEXT.= $chkQry['FDB2CSV1']['D1TEXT'];
                }else{
                    $D1TEXT.= $chkQry['FDB2CSV1']['D1TEXT'] . ',';
                }
            }
        }//end of for loop
    }else{
       $chkQry = array();
       $chkQry = cmChkQuery($db2con, $usrinfo[0]['WUUID'], $d1name, '');
	
       if ($chkQry['result'] !== true) {
          
          $rtn = 1;
          $msg = showMsg($chkQry['result'], array(
             'クエリー'
          ));
          error_log('group error***'.print_r($msg,true));
          
       }else{
            $fdb2csv1Data = $chkQry['FDB2CSV1'];   
            $D1TEXT = $chkQry['FDB2CSV1']['D1TEXT'];
       }
    }
}
if ($rtn === 0) {
   $RDBNM = $fdb2csv1Data['D1RDB'];
   // クエリー作成フラグ取得
   $D1CFLG = $fdb2csv1Data['D1CFLG'];
   
   if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
      $systables = cmGetSystables($db2con, SAVE_DB, $filename);
   }else {
      $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
      $db2RDBcon = cmDB2ConRDB(SAVE_DB);
      $systables = cmGetSystables($db2RDBcon, SAVE_DB, $filename);
   }
   if (count($systables) > 0) {

      if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
         $resIns = cmInsWTBL($db2con, $filename);
      }else {
         $resIns = cmInsWTBL($db2con, $filename, $db2RDBcon);
      }
      if ($resIns !== 0) {
         // 修正用
         $msg = showMsg('FAIL_MENU_DEL', array(
            '実行中のデータ'

         )); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
         $rtn = 1;
      }
   }
   else {

      $msg = showMsg('FAIL_MENU_DEL', array(
         '実行中のデータ'
      )); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
      $rtn = 1;
      $allcount = 0;
      $errorFlg = 1;
   }
}
if ($rtn === 0) {
   if ($querydevice === 'PC') {
      $db2wcol = cmGetDB2WCOL($db2con, $usrinfo[0]['WUUID'], $d1name);    
   }
   $FDB2CSV2 = cmGetFDB2CSV2($db2con, $d1name,false,false,false);
   // HTMLtemplate チェック
   $html_templ = false;
   $htmldata = array();
   $html_tagdata = array();
   $html_context = '';
   if ($rtn === 0) {
      $rs = fnGetDB2HTMLT($db2con, $d1name);
      if ($rs['result'] === true) {
         $htmldata = $rs['data'];
         if (count($htmldata) > 0) {
            $html_templ = true;
            $html_filedata = "./htmltemplate/" . $d1name . ".html";
            $html_context = file_get_contents($html_filedata);
            if (!mb_check_encoding($html_context, 'UTF-8')) {
               $rtn = 1;
               $msg = showMsg('HTML_UTF'); //"HTMLテンプレートはUTF-8のファイルのみ登録できます。";

            }
            if ($rtn === 0) {
               $htmltag = fnGetDB2HTMLK_INSHI($db2con);
               if ($htmltag['result'] === true) {
                  $html_tagdata = $htmltag['data'];
                  foreach($html_tagdata as $key => $value) {
                     $l_tag = '<' . $value['HTMLNM'];
                     $u_tag = '<' . strtoupper($value['HTMLNM']);
                     if (strpos($html_context, $l_tag) !== false || strpos($html_context, $u_tag)) {
                        $rtn = 1;
                        $msg = showMsg('HTML_LIMIT'); //" HTMLに禁止タグが含まれています。";
                        break;
                     }
                  }
               }
            }
         }
      }else {
         $rtn = 1;
         $msg = showMsg('FAIL_FUNC', array(
            'クエリーの実行'
         )); //'クエリーの実行に失敗しました。';
      }
   }
   //ツーバーからのチェックボックスから合計行を判断する場合
   if($rtn === 0){
        if($sumflg !== '0'){
            $rs = cmUpdDB2COLM($db2con, $d1name, $sumflg);
            if ($rs['result'] !== true) {
                $msg = showMsg($rs['result']);
                $rtn = 1;
            }
        }
   }
    $rsMasterArr = array();
    $rsshukeiArr = array();
    if($licenseSeigyo){
        if ($rtn === 0) {
           // 集計するフィールド情報取得
           $rsmaster = cmGetDB2COLM($db2con, $d1name);
           if ($rsmaster['result'] !== true) {
              $msg = showMsg($rsmaster['result']);
              $rtn = 1;
           }else {
              $rsMasterArr = $rsmaster['data'];
           }
        }
         
        if ($rtn === 0) {
           // 集計するメソッド情報取得
           $rsshukei = cmGetDB2COLTTYPE($db2con, $d1name);
           if ($rsshukei['result'] !== true) {
              $msg = showMsg($rsshukei['result']);
              $rtn = 1;
           }else {
              $rsshukeiArr = $rsshukei['data'];
           }
        }
    }
	$FDB2CSV2ALL=array();
	//制御のためSEQがいれてないカラムをもらう事
	if ($rtn === 0) {
		// HTMLからか、グリッドか決める
		if ($htmlFlg === '1') {
			if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
			
				$fdb2csvalltest = cmGetFDB2CSV2($db2con,$d1name,false,false,true);
				if($fdb2csvalltest['result']===true){
					$FDB2CSV2ALL = umEx($fdb2csvalltest['data']);
				}
			}
		}
	}

   if ($rtn === 0) {

      // 詳細情報画面に設定されたデータ取得
      $rsWdtlArr = array();
      $rsWdtl = cmGetDB2WDTL($db2con, $d1name, '', '');
      if ($rsWdtl['result'] !== true) {
         $msg = showMsg($rsWdtl['result']);
         $rtn = 1;
      }else {
         $rsWdtlArr = $rsWdtl['data'];
      }
   }
   if ($rtn === 0) {
	//	e_log("3hello hello");
      // クエリー連携情報画面に設定されたデータ取得
      $rsDrgs = cmGetDB2DRGS($db2con, $d1name);
      $rsDrgsArr = array();
      if ($rsDrgs['result'] !== true) {
         $msg = showMsg($rsDrgs['result']);
         $rtn = 1;
      }else {
         $rsDrgsArr = $rsDrgs['data'];
      }
   }
   if ($rtn === 0) {
      
		//e_log("4hello hello");
      // $rsWdtlArr = $rsWdtl['data'];
      // CL 実行があるかどうかチェック
      $DB2CSV1PM = array();
      $DB2CSV1PG = array();
      // $rs =  fnGetFDB2CSV1PM($db2con, $d1name ,'3');
      $rs = fnDB2CSV1PG($db2con, $d1name);
      
      if ($rs['result'] === true) {
         
         $DB2CSV1PG = $rs['data'];
         $DGCLIG = '';
         $DGCPGM = '';
         if (count($DB2CSV1PG) > 0) { //CL実行がある
            $DGCLIG = $DB2CSV1PG[0]['DGCLIB'];
            $DGCPGM = $DB2CSV1PG[0]['DGCPGM'];
         }
        
         // HTMLからか、グリッドか決める
         if ($htmlFlg === '1') {
            if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
               $rst = cmLevelFieldCheck($db2con, $d1name, $rsMasterArr, $rsshukeiArr);
				//e_log("5hello hello");
               if ($rst['result'] !== true) {
                  $msg = showMsg($rst['result'], array(
                     'クエリー',
                     'カラム'
                  ));
                  $rtn = 2;
               }
            }
            if ($rtn === 0) {
                
               if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0 && $html_templ === false) {
				//	e_log("1=>Get:");
                   $estart = microtime(true);
                   if(empty($rsMasterArr[0]['DCSUMF']) || $rsMasterArr[0]['DCSUMF'] === ''|| $rsMasterArr[0]['DCSUMF'] === ""|| $rsMasterArr[0]['DCSUMF'] === null){
                     if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                        $allcount = fnGetAllCount($db2con, $FDB2CSV2['data'], $filename, $sSearch,'',array(),$filtersearchData);
                     }else {
                        $allcount = cmGetAllCount($db2RDBcon, $FDB2CSV2['data'], $filename, $sSearch,'',array(),$filtersearchData);
                     }
                     // $allcount = cmGetAllCount($db2con,$FDB2CSV2['data'],$filename,$sSearch);
                     if ($allcount['result'] !== true) {

                        $msg = showMsg($allcount['result']);
                         
                        $rtn = 1;
                     }
                     $eend = microtime(true);
                     //e_log( '①クエリーカウント取得処理時間：' . ($eend - $estart) . '秒');
                     $estart = microtime(true);
                     // CLで作成されたDBを読み込み
                     
                     if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {                                         
                        $csv_d = cmGetDbFile_1($db2con, $FDB2CSV2['data'], $filename, $iDisplayStart, $iDisplayLength, $iSortCol_0, $sSortDir_0, '', $sSearch, '', $rsMasterArr, false, '','','','','',$filtersearchData);                                              
                     }else {                    
                        $csv_d = cmGetDbFile_1($db2RDBcon, $FDB2CSV2['data'], $filename, $iDisplayStart, $iDisplayLength, $iSortCol_0, $sSortDir_0, '', $sSearch, '', $rsMasterArr, false, '','','','','',$filtersearchData);                                            
                     }
                     if ($csv_d['result'] !== true) {
                        $msg = showMsg($csv_d['result']);                        
                        $rtn = 1;
                     }else {
                         $eend = microtime(true);
                         //e_log( '②クエリーデータ取得処理時間：' . ($eend - $estart) . '秒');
                         $estart = microtime(true);
                        // CLで作成された集計するデータを読み込み
                        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                           $shukeiResult = cmGetDbFile_2($db2con, $FDB2CSV2['data'], $d1name, $filename, $iDisplayStart, $iDisplayLength, $sSearch, '', $rsMasterArr, $rsshukeiArr, false, '','','',array(), $filtersearchData);
                        }else {
                           $shukeiResult = cmGetDbFile_2($db2RDBcon, $FDB2CSV2['data'], $d1name, $filename, $iDisplayStart, $iDisplayLength, $sSearch, '', $rsMasterArr, $rsshukeiArr, false, '',$filtersearchData);
                        }
                        // $shukeiResult = cmGetDbFile_2($db2con,$FDB2CSV2['data'],$d1name,$filename,$iDisplayStart,$iDisplayLength,$sSearch,'',$rsMasterArr,$rsshukeiArr,false,'');
                        if ($shukeiResult['result'] !== true) {
                           $msg = showMsg($shukeiResult['result']);
                           $rtn = 1;
                        }
                        $eend = microtime(true);
                        //e_log( '③クエリー集計取得処理時間：' . ($eend - $estart) . '秒');
                     }
                  }else {
						//e_log("2=>Get:");
                     // 合計結果のみ表示する処理
                     if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                        $shukeiResult = cmGetDbFile_2($db2con, $FDB2CSV2['data'], $d1name, $filename, $iDisplayStart, $iDisplayLength, $sSearch, '', $rsMasterArr, $rsshukeiArr, false, '','','','',$filtersearchData);                     
                     }else {
                        $shukeiResult = cmGetDbFile_2($db2RDBcon, $FDB2CSV2['data'], $d1name, $filename, $iDisplayStart, $iDisplayLength, $sSearch, '', $rsMasterArr, $rsshukeiArr, false, '',$filtersearchData);
                     }
                     if ($shukeiResult['result'] !== true) {

                        $msg = showMsg($shukeiResult['result']);                       
                        $rtn = 1;
                     }else {
                           // 総合行リターン用
                          $allcount['data'] = cmGetALLCOUNTSEIKYO($db2con, $d1name, $filename, $rsMasterArr, $FDB2CSV2['data'], $sSearch, '',true);
                           // ページングリターン用
                          $csv_d = $shukeiResult;                                                
                     }
                  }
               }else {
                  // $allcount = cmGetAllCount($db2con,$FDB2CSV2['data'],$filename,$sSearch);
                  if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                     $allcount = cmGetAllCount($db2con, $FDB2CSV2['data'], $filename, $sSearch, $D1CFLG,array(),$filtersearchData);//イー190205
                     //e_log("t9");
                  }else {
                     $allcount = cmGetAllCount($db2RDBcon, $FDB2CSV2['data'], $filename, $sSearch, $D1CFLG,array(),$filtersearchData);//イー190205
                    // e_log("t10");
                  }
                  if ($allcount['result'] !== true) {

                     $msg = showMsg($allcount['result']);
                     $rtn = 1;
                  }
                  if ($DGCLIG !== '' && $DGCPGM !== '') { //CL実行がある
                     // $csv_d = cmGetDbFile_CL($db2con,$FDB2CSV2['data'],$filename,$iDisplayStart,$iDisplayLength,$iSortCol_0,$sSortDir_0,'',$sSearch,$db2wcol);
                     if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                     
                        $csv_d = cmGetDbFile_CL($db2con, $FDB2CSV2['data'], $filename, $iDisplayStart, $iDisplayLength, $iSortCol_0, $sSortDir_0, '', $sSearch, array() , $D1CFLG);
                      
                     }else {
                        
                        $csv_d = cmGetDbFile_CL($db2RDBcon, $FDB2CSV2['data'], $filename, $iDisplayStart, $iDisplayLength, $iSortCol_0, $sSortDir_0, '', $sSearch, array() , $D1CFLG);
                       // e_log("t12");
                     }
                     if ($csv_d['result'] !== true) {

                        $msg = showMsg($csv_d['result']);
                        $rtn = 1;
                     }
                  }else {
                     // $csv_d = cmGetDbFile($db2con,$FDB2CSV2['data'],$filename,$iDisplayStart,$iDisplayLength,$iSortCol_0,$sSortDir_0,'',$sSearch,$db2wcol,false);
                     // $csv_d = cmGetDbFile($db2con,$FDB2CSV2['data'],$filename,$iDisplayStart,$iDisplayLength,$iSortCol_0,$sSortDir_0,'',$sSearch);
                     if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                        $csv_d = cmGetDbFile($db2con, $FDB2CSV2['data'], $filename, $iDisplayStart, $iDisplayLength, $iSortCol_0, $sSortDir_0, '', $sSearch, array() , false, $D1CFLG,'',array(),$filtersearchData);
						
                    
                     }else {
                        $csv_d = cmGetDbFile($db2RDBcon, $FDB2CSV2['data'], $filename, $iDisplayStart, $iDisplayLength, $iSortCol_0, $sSortDir_0, '', $sSearch, array() , false, $D1CFLG,'',array(),$filtersearchData);
                    
                     }
                     if ($csv_d['result'] !== true) {

                        $msg = showMsg($csv_d['result']);
                        $rtn = 1;
                     }
                  }
               }
            }
         }else if ($htmlFlg === '0') {
            // $allcount = cmGetAllCount($db2con,$FDB2CSV2['data'],$filename,$sSearch);
            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
               $allcount = cmGetAllCount($db2con, $FDB2CSV2['data'], $filename, $sSearch, $D1CFLG,array(), $filtersearchData);
               //e_log("t15");
            }else {
               $allcount = cmGetAllCount($db2RDBcon, $FDB2CSV2['data'], $filename, $sSearch, $D1CFLG,array(), $filtersearchData);
               //e_log("t16");
            }
            if ($allcount['result'] !== true) {
               // e_log(print_r($allcount,true));

               $msg = showMsg($allcount['result']);              
               $rtn = 1;
            }
            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                $csv_d = cmGetDbFile($db2con, $FDB2CSV2['data'], $filename, $iDisplayStart, $iDisplayLength, $iSortCol_0, $sSortDir_0, '', $sSearch, $db2wcol, false, $D1CFLG,'',array(), $filtersearchData);
             	
              
            }else {
               $csv_d = cmGetDbFile($db2RDBcon, $FDB2CSV2['data'], $filename, $iDisplayStart, $iDisplayLength, $iSortCol_0, $sSortDir_0, '', $sSearch, $db2wcol, false, $D1CFLG,'',array(), $filtersearchData);
              

            }
            // $csv_d = cmGetDbFile($db2con,$FDB2CSV2['data'],$filename,$iDisplayStart,$iDisplayLength,$iSortCol_0,$sSortDir_0,'',$sSearch,$db2wcol,false);
            if ($csv_d['result'] !== true) {
               // e_log(print_r($csv_d,true));

               $msg = showMsg($csv_d['result']);
               $rtn = 1;
            }
            //   $DB2CSV1PM = array();
         }
      }else {
         $rtn = 1;
         $msg = $rs['result'];
      }
      cmEnd($db2con, $RTCD, $JSEQ, $filename, $d1name);
      if ($RTCD === '9') {

         $rtn = 1;
         $msg = showMsg('FAIL_FUNC', array(
            'クエリーの事後処理'
         )); //'クエリーの事後処理に失敗しました。';

      }
   }
}
if($rtn === 0){
    $rs = cmGetDB2QBMK($db2con,$usrinfo[0]['WUUID'], $QRYNAME,'','',$QBMFLG);
    if($rtn === 0){
        if($rs['result'] !== true){

            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            if($rs['datacount'] > 0){
                $bmkexist = true;
            }
        }
    }
}

if ($rtn === 0) {
   $fstcol = $csv_d['firstcol'];
   $csv_d = umEx($csv_d['data'], (($querydevice !== 'TOUCH' && $html_templ !== true) ? true : false));
  
}

if ($rtn === 0) {//MSM add for bmfoler icon show/hide
    $rs = cmGetWUUBFG($db2con, $usrinfo[0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result']);
    } else {
        $WUUBFG = $rs['data'][0]['WUUBFG'];
    }
}

// ワークテーブルのログ情報削除
$resDel = cmDelWTBL($db2con, $filename);
if ($resDel !== 0) {
   e_log('削除テスト：' . $resDel . 'aaaa' . $filename);
   // 修正用
   $msg = showMsg('ログ削除失敗'); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
   $rtn = 1;
}
if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
   cmDb2Close($db2RDBcon);
}
// 文字の色付け（条件付き書式）
if($rtn === 0){
    $rs = cmGetDB2JKSK($db2con,$d1name);

    if($rtn === 0){
        if($rs['result'] !== true){

            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            $db2jksk = $rs['data'];
        }
    }

}

$fnameS = array();
$fnameF = array();
if($rtn === 0){
   for($f = 1;$f <= 6;$f++){
       $fnameS = explode(",",cmMer($rsMasterArr[0]['DCFLD' . $f]));
   
       if(count($fnameS) > 1){
            $fnameS = $fnameS[count($fnameS)-1];
            $fnameS = explode("-",$fnameS);
            $fnameS = $fnameS[1].'_'.$fnameS[0];
         //   $fnameS = preg_replace("([0-9-]+)", "_", $fnameS);
         //   $fnameS = trim($fnameS, "_");
       }else{
            $fnameS = $fnameS[0].'_'.cmMer($rsMasterArr[0]['DCFILID' . $f]);
       }
       array_push($fnameF,$fnameS);
    
   }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
   'html_context' => $html_context,
   'res_iSortCol_0' => $iSortCol_0,
   'res_sSortDir_0' => $sSortDir_0,
   'res_sSearch' => $sSearch,
   'iTotalRecords' => $allcount['data'],
   'iTotalDisplayRecords' => $allcount['data'],
   'shukeiData' => umEx($shukeiResult['data'], true),
   'firstcol' => $fstcol,
   'aaData' => $csv_d,
   'rsmaster' => $rsMasterArr ,
   'rsshukei' => $rsshukeiArr ,
   'rsWdtl' => $rsWdtlArr ,
   'rsDrgs' => $rsDrgsArr ,
   'rtn' => $rtn,
   'msg' => $msg,
   'filename' => $filename,
   'd1name' => $d1name,
   'RTCD' => $RTCD,
   "errorFlg" => $errorFlg,
   'len_d' => $len_d,
   'html_tagdata' => $html_tagdata,
   'ZERO_FLG' => ZERO_FLG,
   'htmlFlg' => $htmlFlg,
   'DB2CSV1PG' => $DB2CSV1PG,
   'usrinfo' => $usrinfo,
   'BACKZERO_FLG' => BACKZERO_FLG,
   'PROTOCOL' => PROTOCOL,
   'IP' => IP,
   'SISTEM' => SISTEM,
   'D1CFLG' => $D1CFLG,
   'D1TEXT' => $D1TEXT,
   'BMKEXIST'=> $bmkexist,
   'a' => array($usrinfo[0]['WUUID'], $QRYNAME,'','',$QBMFLG),
   'DB2JKSK'=> umEx($db2jksk),
   'FDB2CSV2ALL'=>$FDB2CSV2ALL,
   'FNAME' => $fnameF,
   'WUUBFG' => $WUUBFG
   
);
unset($csv_d);
echo (json_encode($rtn));
/*
*-------------------------------------------------------*
* 画面定義内容をFDB2CSV3に更新
*-------------------------------------------------------*
*/
function fnSearchUpdate($db2con, $sch_data)
{
   foreach($sch_data as $k1 => $v1) {
      foreach($v1 as $k2 => $v2) {
         foreach($v2 as $k3 => $v3) {
            $rs = cmUpdFDB2CSV3($db2con, $k1, $k2, $k3, $v3['dat'], $v3['type']);
         }
      }
   }
}
/*
*-------------------------------------------------------*
* 項目の最大桁数の値を取得
*-------------------------------------------------------*
*/
function fnGetMaxLenValue($db2con, $fdb2csv2, $filename, $start, $length, $sortcol, $sortdir, $sortingcols = 0, $search)
{
   $rtn = array();
   foreach($fdb2csv2 as $csv2row) {
      $colName = $csv2row['D2FLD'] . '_' . $csv2row['D2FILID'];
      $maxdata = '';
      $params = array();
      $strSQL = '';
      $strSQL.= ' SELECT ' . $colName . ' ';
      $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' ';
      $strSQL.= ' WHERE LENGTH(RTRIM(' . $colName . ')) = ';
      $strSQL.= ' ( ';
      $strSQL.= ' SELECT ';
      $strSQL.= ' MAX(LENGTH(RTRIM(A.' . $colName . '))) ';
      $strSQL.= ' FROM( ';
      $strSQL.= ' SELECT B.* , ';
      $strSQL.= ' ROWNUMBER() OVER ( ';
      // ソート
      if ($sortcol !== "") {
         $sortColumn = $sortcol;
         $strSQL.= ' ORDER BY B.' . $sortColumn . ' ' . $sortdir . ' ';
      }
      $strSQL.= ') ';
      $strSQL.= ' AS rownum ';
      $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' as B ';
      // 検索条件
      if ($search != '') {
         if (count($fdb2csv2) > 0) {
            $strSQL.= ' WHERE ';
            $paramSQL = array();
            foreach($fdb2csv2 as $key => $value) {
               $paramSQL[] = $value['D2FLD'] . '_' . $value['D2FILID'] . ' LIKE ? ';
               $params[] = '%' . $search . '%';
               /*if ($key < (count($fdb2csv2) - 1)) {
                  $strSQL.= ' OR ';
               }*/
               $strSQL .= join(' OR ', $paramSQL);
            }
         }
      }
      $strSQL.= ' ) AS A ';
      // 抽出範囲指定
      if (($start != '') && ($length != '')) {
         $strSQL.= ' WHERE A.rownum BETWEEN ? AND ? ';
         $params[] = $start + 1;
         $params[] = $start + $length;
      }
      $strSQL.= ' ) FETCH FIRST 1 ROWS ONLY ';
      $stmt = db2_prepare($db2con, $strSQL);
      if ($stmt === false) {
         $rtn = false;
      }
      else {
         $r = db2_execute($stmt, $params);
         if ($r === false) {
            $rtn = false;
         }
         else {
            while ($row = db2_fetch_assoc($stmt)) {
               $maxdata = $row[$colName];
            }
            $rtn[$colName] = cmMer($maxdata);
         }
      }
   }
   return $rtn;
}
function fnGetMaxLen($db2con, $fdb2csv2, $filename)
{
   $rtn = array();
   $strSelect = '';
   foreach($fdb2csv2 as $csv2row) {
      $colname = $csv2row['D2FLD'] . '_' . $csv2row['D2FILID'];
      $strSelect.= 'max(length(rtrim(A.' . $colname . '))) AS ' . $colname . ',';
   }
   $strSelect = rtrim($strSelect, ',');
   $strSQL = '';
   $strSQL.= ' SELECT ' . $strSelect . ' ';
   $strSQL.= ' FROM ' . SAVE_DB . '/' . $filename . ' AS A ';
   $params = array();
   $stmt = db2_prepare($db2con, $strSQL);
   if ($stmt === false) {
      $data = false;
   }
   else {
      db2_execute($stmt, $params);
      if ($r === false) {
         $data = false;
      }
      else {
         while ($row = db2_fetch_assoc($stmt)) {
            $data = $row;
         }
      }
   }
   $rtn = $data;
   return $rtn;
}
function fnDB2CSV1PG($db2con, $d1name)
{
   $rs = array();
     $fdb2csv1pg = array();
   // FDB2CSV1PGの取得
   $strSQL = ' SELECT A.DGNAME,A.DGCLIB,A.DGCPGM,A.DGCBNM,B.D1LIBL ';
   $strSQL.= ' FROM FDB2CSV1PG AS A ';
   $strSQL.= ' JOIN FDB2CSV1 AS B ON A.DGNAME = B.D1NAME ';
   $strSQL.= ' WHERE A.DGNAME = ? ';
   $stmt = db2_prepare($db2con, $strSQL);
   if ($stmt === false) {
      $rs = array(
         'result' => 'FAIL_SEL'
      );
   }
   else {
      $params = array(
         $d1name
      );
      $r = db2_execute($stmt, $params);
      if ($r === false) {
         $rs = array(
            'result' => 'FAIL_SEL'
         );
      }
      else {
         while ($row = db2_fetch_assoc($stmt)) {
            foreach($row as $k => $v){
                $row[$k] = cmMer($v);
            }
            $fdb2csv1pg[] = $row;
         }
         $rs = array(
            'result' => true,
            'data' => $fdb2csv1pg
         );
      }
   }
   return $rs;
}
//$db2con, $FDB2CSV2['data'], $d1name, $filename, $iDisplayStart, $iDisplayLength, $sSearch, '', $rsMasterArr, $rsshukeiArr, false, '','','','',$filtersearchData
function fnGetDbFile_2($db2con, $fdb2csv2, $d1name, $filename, $start, $length, $search, $db2wcol, $rsMasterArr, $rsshukeiArr, $dnlFlg = false, $burstItmLst, $webf = '', $d1name2 = '', $CNDSDATA = array(), $filtersearchData=array())
{
    $rtn         = 0;
    $data        = array();
    $params      = array();
    $levelFlg    = '';
    $gpFlg       = '';
    $shukeiFlg   = '';
    $rsMasterArr = umEx($rsMasterArr);
    $rsshukeiArr = umEx($rsshukeiArr);
    $fdb2csv2    = umEx($fdb2csv2);
    $colres = cmGetOrdColumnInfo($db2con,$d1name);
    $colsorting = $colres['data'];
    $colsortingArr = array();
  
    foreach($colsorting as $ckey => $cval){
        $cid = $cval['D2FLD'].'_'.$cval['D2FILID'];
        $colsortingArr[$cid] = $cval['D2RSTP'];
    }
          

/**20180213**/
        $keyArr = array();
        $i      = 1;
        while ($i <= 6) {
            if ($rsMasterArr[0]['DCFLD' . $i] !== '') {
                $keyArr[] = $rsMasterArr[0]['DCFLD' . $i] . '_' . $rsMasterArr[0]['DCFILID' . $i];
            }
            ++$i;
        }
        $shukeiArr = array();
        $selFld    = array();
        $shukeiFld = array();
        foreach ($rsshukeiArr as $key => $row) {
            if ($row['DTSUM'] === '1') {
                $shukeiFld[] = 'A.SUM_' . $row['DTFLD'] . '_' . $row['DTFILID'];
                $shukeiArr[] = 'SUM (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS SUM_' . $row['DTFLD'] . '_' . $row['DTFILID'];
            }
            if ($row['DTAVLG'] === '1') {
                $shukeiFld[] = 'A.AVG_' . $row['DTFLD'] . '_' . $row['DTFILID'];
                $shukeiArr[] = 'AVG (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS AVG_' . $row['DTFLD'] . '_' . $row['DTFILID'];
            }
            if ($row['DTMAXV'] === '1') {
                $shukeiFld[] = 'A.MAX_' . $row['DTFLD'] . '_' . $row['DTFILID'];
                $shukeiArr[] = 'MAX (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS MAX_' . $row['DTFLD'] . '_' . $row['DTFILID'];
            }
            if ($row['DTMINV'] === '1') {
                $shukeiFld[] = 'A.MIN_' . $row['DTFLD'] . '_' . $row['DTFILID'];
                $shukeiArr[] = 'MIN (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS MIN_' . $row['DTFLD'] . '_' . $row['DTFILID'];
            }
            if ($row['DTCONT'] === '1') {
                $shukeiFld[] = 'A.COUNT_' . $row['DTFLD'] . '_' . $row['DTFILID'];
                $shukeiArr[] = 'COUNT (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS COUNT_' . $row['DTFLD'] . '_' . $row['DTFILID'];
            }
        }
        $keyDataArr = $keyArr;
        $keyValArr  = $keyArr;
        $keyData    = array();
        $ordKeyArr  = array();
        $selKeyArr  = array();

        foreach ($keyArr as $k => $kVal) {
            if (empty($keyDataArr)) {
                $keyData[] = '';
            } else {
                $keyData[] = join(',', $keyDataArr);
            }

            foreach ($fdb2csv2 as $fdb2csv2data) {
                if ($fdb2csv2data['D2FLD'] . '_' . $fdb2csv2data['D2FILID'] === $kVal) {
                    $datatype = $fdb2csv2data['D2TYPE'];
                    $sSort = '';
                    if(isset($colsortingArr[$kVal])){
                        $sSort = $colsortingArr[$kVal];
                    }
                    switch ($datatype) {
                        case 'L':
                            $selFld[]    = 'CHAR(A.' . $kVal . ') AS ' . $kVal;
                            $ordStr      = str_pad('9', 10, "9");
                            if($sSort === 'DESC'){
                                $ordStr = '-'.$ordStr;
                            }
                            $ordKeyArr[] = ' CASE  WHEN ' . $kVal . ' IS NULL THEN \'' . $ordStr . '\' ELSE ' . $kVal . ' END '.$sSort.', ' . $kVal.' '.$sSort;
                            break;
                        case 'T':
                            $selFld[]    = 'CHAR(A.' . $kVal . ') AS ' . $kVal;
                            $ordStr      = str_pad('9', 8, "9");
                            if($sSort === 'DESC'){
                                $ordStr = '-'.$ordStr;
                            }
                            $ordKeyArr[] = ' CASE  WHEN ' . $kVal . ' IS NULL THEN \'' . $ordStr . '\' ELSE ' . $kVal . ' END '.$sSort.', ' . $kVal.' '.$sSort;
                            break;
                        case 'Z':
                            $selFld[]    = 'VARCHAR_FORMAT(A.' . $kVal . ', \'YYYY-MM-DD HH24:MI:SS.nnnnnn\') AS ' . $kVal;
                            $ordStr      = str_pad('9', 26, "9");
                            if($sSort === 'DESC'){
                                $ordStr = '-'.$ordStr;
                            }
                            $ordKeyArr[] = ' CASE  WHEN ' . $kVal . ' IS NULL THEN \'' . $ordStr . '\' ELSE ' . $kVal . ' END '.$sSort.', ' . $kVal.' '.$sSort;
                            break;
                        case 'P':
                        case 'S':
                        case 'B':
                            $selFld[]    = 'A.' . $kVal;
                            $ordStr      = str_pad('9', $fdb2csv2data['D2LEN'] + 1, "9");
                            if($sSort === 'DESC'){
                                $ordStr = '-'.$ordStr;
                            }
                            $ordKeyArr[] = ' CASE  WHEN ' . $kVal . ' IS NULL THEN ' . $ordStr . ' ELSE ' . $kVal . ' END '.$sSort.', ' . $kVal.' '.$sSort;
                            break;
                        case 'A':
                        case 'O':
                            $selFld[]    = 'A.' . $kVal;
                            $ordStr      = str_pad('9', $fdb2csv2data['D2LEN'] + 1, "9");
                            if($sSort === 'DESC'){
                                $ordStr = '-'.$ordStr;
                            }
                            $ordKeyArr[] = ' CASE  WHEN ' . $kVal . ' IS NULL THEN \'' . $ordStr . '\' ELSE ' . $kVal . ' END '.$sSort.', ' . $kVal.' '.$sSort;
                            break;
                        default:
                            break;
                    }
                    break;
                }
            }
            if ($k === 0) {
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyDataArr[count($keyValArr) - 1] . '\'AS CHECK ';
            } else {
                $c = count($keyArr);
                $vc = count($keyValArr);
                foreach ($fdb2csv2 as $fdb2csv2OrdData) {
                    if ($fdb2csv2OrdData['D2FLD'] . '_' . $fdb2csv2OrdData['D2FILID'] === $keyArr[$c - $k]) {
                        $ordDatatype = $fdb2csv2OrdData['D2TYPE'];
                        switch ($ordDatatype) {
                            case 'L':
                                $keyArr[$c - $k] = ' CAST(NULL AS DATE) AS ' . $keyArr[$c - $k];
                                $selKeyArr[]                 = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1)] . '\'AS CHECK ';
                                break;
                            case 'T':
                                $keyArr[$c - $k] = ' CAST(NULL AS TIME) AS ' . $keyArr[$c - $k];
                                $selKeyArr[]                 = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1)] . '\'AS CHECK ';
                                break;
                            case 'Z':
                                $keyArr[$c - $k] = ' CAST(NULL AS TIMESTAMP) AS ' . $keyArr[$c - $k];
                                $selKeyArr[]                 = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1)] . '\'AS CHECK ';
                                break;
                            case 'P':
                            case 'S':
                            case 'B':
                                $keyArr[$c - $k] = ' CAST(NULL AS INT) AS ' . $keyArr[$c - $k];
                                $selKeyArr[]                 = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1)] . '\'AS CHECK ';
                                break;
                            case 'A':
                            case 'O':
                                $keyArr[$c - $k] = ' CAST(NULL AS CHAR) AS ' . $keyArr[$c - $k];
                                $selKeyArr[]                 = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1)] . '\'AS CHECK ';
                                break;
                            default:
                                $keyArr[$c - $k] = ' CAST(NULL AS CHAR) AS ' . $keyArr[$c - $k];
                                $selKeyArr[]                 = join(',', $keyArr) . ',\'' . $keyDataArr[$vc - ($k + 1)] . '\'AS CHECK ';
                                break;
                        }
                        break;
                    }
                }
            }
            $rmData = array_pop($keyDataArr);
        }
/**20180213**/
    //データを集計するクエリの作成
    if (CRTQTEMPTBL_FLG === 1) {
        /***20180213の文を消した***/
        foreach ($fdb2csv2 as $fdata) {
            if ($fdata['D2FLD'] . '_' . $fdata['D2FILID'] === $keyArr[0]) {
                $datatype = $fdata['D2TYPE'];
                break;
            }
        }
        switch ($datatype) {
            case 'L':
                $keyArr[0]   = ' CAST(NULL AS DATE) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
                break;
            case 'T':
                $keyArr[0]   = ' CAST(NULL AS TIME) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
                break;
            case 'Z':
                $keyArr[0]   = ' CAST(NULL AS TIMESTAMP) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
                break;
            case 'P':
            case 'S':
            case 'B':
                $keyArr[0]   = ' CAST(NULL AS INT) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
                break;
            case 'A':
            case 'O':
                $keyArr[0]   = ' CAST(NULL AS CHAR) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
                break;
            default:
                $keyArr[0]   = ' CAST(NULL AS CHAR) AS ' . $keyArr[0];
                $selKeyArr[] = join(',', $keyArr) . ',\'' . $keyValArr[0] . '\'AS CHECK ';
                break;
        }
        $shukeiData = join(',', $shukeiArr);
        $strSQLArr  = array();
        $kd = count($keyData);
        $cnt = 0;
        while($cnt <= $kd){
            $strSQL = '';
            $strSQL .= ' SELECT ';
            $strSQL .= $selKeyArr[$cnt];
            $strSQL .= ' , ' . $shukeiData;
            $strSQL .= ' , MAX(ROWNUM ) as RN ';
            $strSQL .= ' FROM ';
            $strSQL .= SAVE_DB . '/' . $filename;
            if ($cnt < $kd) {
                $strSQL .= ' GROUP BY ';
                $strSQL .= $keyData[$cnt];
            }
            $strSQLArr[] = $strSQL;
            ++$cnt;
        }
     
        $strSQL = ' SELECT ';
        $strSQL .= '    * ';
        $strSQL .= ' FROM (';
        $strSQL .= '    SELECT ';
        $strSQL .= join(',', $selFld);
        $strSQL .= ', A.CHECK , ';
        $strSQL .= join(',', $shukeiFld);
        $strSQL .= ', A.RN ';
        $strSQL .= ', CASE WHEN A.RN IS NOT NULL THEN  ROWNUMBER() OVER (   ) END AS ROWNUM ';
        $strSQL .= '    FROM ( ';
        $strSQL .= join(' UNION ALL ', $strSQLArr);
        $strSQL .= '    ) A ';
        $strSQL .= ' ) B';

        if(count($filtersearchData)>0){
            
                $paramDSQL = array();
                $strSQL .= ' WHERE ';
                foreach ($filtersearchData as $key => $value) {
                      list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                      switch ($value['dtype']) {
                          case 'L':
                              $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                          case 'T':
                              $paramDSQL[] = 'CAST(' .cmMer($D1COL2) . ' AS CHAR(8))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                          case 'Z':
                              $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                          default:
                              $paramDSQL[] = $D1COL2 .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                }
                $params[] =cmMer($value['D1VAL']);
              }
                $strSQL .= join(' AND ',$paramDSQL);
            } 

        if (($start !== '') && ($length !== '')) {
          if(count($filtersearchData)>0){
            if ($rsMasterArr[0]['DCSUMF'] === '') {
                $strSQL .= ' AND B.RN BETWEEN ? AND ? ';
            } else {
                if ($dnlFlg === false) {
                    $strSQL .= ' AND B.rownum BETWEEN ? AND ? ';
                } else {
                    $strSQL .= ' AND B.RN BETWEEN ? AND ? ';
                }
            }
          }
          else{
              if ($rsMasterArr[0]['DCSUMF'] === '') {
                $strSQL .= ' WHERE B.RN BETWEEN ? AND ? ';
              } else {
                  if ($dnlFlg === false) {
                      $strSQL .= ' WHERE B.rownum BETWEEN ? AND ? ';
                  } else {
                      $strSQL .= ' WHERE B.RN BETWEEN ? AND ? ';
                  }
              }
          }
            $params[] = $start + 1;
            $params[] = $start + $length;
        }
        $strSQL .= '    ORDER BY  ';
        $strSQL .= join(', ', $ordKeyArr);
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            //   $rtn  = 1;
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
                e_log('【削除】：制御合計データ取得：akz' . $strSQL . print_r($params, true));
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                //  $rtn  = 1;
                $data = array(
                    'result' => 'FAIL_SEL'
                );
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                $data = array(
                    'result' => true,
                    'data' => $data
                );
            }
        }
        
    } else {
        if (!empty($rsMasterArr) && !empty($rsshukeiArr)) {
            $strSQL = ' SELECT F.*  ';
            $strSQL .= ' FROM(  ';
            $strSQL .= ' SELECT P.*, ';
            $strSQL .= ' CASE WHEN P.RN IS NOT NULL THEN ';
            $strSQL .= ' rownumber() over (  ';
            $strSQL .= ' ) END AS rownum ';
            $strSQL .= ' FROM ( ';
            
            $strSQL .= ' SELECT ';
            //DB2COLMテーブルのレベル情報を取得する
            $i = 1;
            while ($i <= 6) {
                if ($rsMasterArr[0]['DCFLD' . $i] !== '') {
                    $levelFlg .= $rsMasterArr[0]['DCFLD' . $i] . '_' . $rsMasterArr[0]['DCFILID' . $i] . ',';
                    if ($rsMasterArr[0]['DCFLD' . ($i + 1)] !== '' && $i < 6) {
                        $gpFlg .= ' WHEN GROUPING (' . $rsMasterArr[0]['DCFLD' . ($i + 1)] . '_' . $rsMasterArr[0]['DCFILID' . ($i + 1)] . ' ) = 1 THEN \'' . $rsMasterArr[0]['DCFLD' . ($i)] . '_' . $rsMasterArr[0]['DCFILID' . ($i)] . '\'';
                    } else {
                        $gpFlg .= ' ELSE \'' . cmMer($rsMasterArr[0]['DCFLD' . $i]) . '_' . cmMer($rsMasterArr[0]['DCFILID' . $i]) . '\'' . ' END ';
                    }
                }
                ++$i;
            }
            //DB2COLTテーブルの集計情報を取得する
            foreach ($rsshukeiArr as $key => $row) {
                if ($row['DTSUM'] === '1') {
                    $shukeiFlg .= 'SUM (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS SUM_' . $row['DTFLD'] . '_' . $row['DTFILID'] . ',';
                }
                if ($row['DTAVLG'] === '1') {
                    $shukeiFlg .= 'AVG (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS AVG_' . $row['DTFLD'] . '_' . $row['DTFILID'] . ',';
                }
                if ($row['DTMAXV'] === '1') {
                    $shukeiFlg .= 'MAX (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS MAX_' . $row['DTFLD'] . '_' . $row['DTFILID'] . ',';
                }
                if ($row['DTMINV'] === '1') {
                    $shukeiFlg .= 'MIN (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS MIN_' . $row['DTFLD'] . '_' . $row['DTFILID'] . ',';
                }
                if ($row['DTCONT'] === '1') {
                    $shukeiFlg .= 'COUNT (' . $row['DTFLD'] . '_' . $row['DTFILID'] . ') AS COUNT_' . $row['DTFLD'] . '_' . $row['DTFILID'] . ',';
                }
            }
            // レベルフラッグの連結
            $strSQL .= $levelFlg;
            // groupingストリングの連結
            $levelFlgCnt = explode(',', rtrim($levelFlg, ','));
            if (count($levelFlgCnt) > 1) {
                $strSQL .= ' CASE ';
                $strSQL .= $gpFlg;
            } else {
                $strSQL .= '\'' . $levelFlgCnt[0] . '\'';
            }
            $strSQL .= ' AS CHECK ,';
            // 集計フラッグの連結
            $strSQL .= $shukeiFlg;
            $strSQL .= ' MAX ( B.ROWNUM ) as RN';
            $strSQL .= ' FROM ';
            $strSQL .= SAVE_DB . '/' . $filename . ' B';
            $strSQL .= ' GROUP BY ROLLUP ( ';
            $strSQL .= rtrim($levelFlg, ',');
            $strSQL .= ' ) ';
            $strSQL .= ' ) AS P ) AS F';
            
            if(count($filtersearchData)>0){
            
                $paramDSQL = array();
                $strSQL .= ' WHERE ';
                foreach ($filtersearchData as $key => $value) {
                      list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                      switch ($value['dtype']) {
                          case 'L':
                              $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                          case 'T':
                              $paramDSQL[] = 'CAST(' .cmMer($D1COL2) . ' AS CHAR(8))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                          case 'Z':
                              $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                          default:
                              $paramDSQL[] = $D1COL2 .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                }
                $params[] =cmMer($value['D1VAL']);
              }
                $strSQL .= join(' AND ',$paramDSQL);
            } 

            //抽出範囲指定
            if (($start !== '') && ($length !== '')) {
              if(count($filtersearchData)>0){
                if ($rsMasterArr[0]['DCSUMF'] === '') {
                    $strSQL .= ' AND F.RN BETWEEN ? AND ? ';
                } else {
                    if ($dnlFlg === false) {
                        $strSQL .= ' AND F.rownum BETWEEN ? AND ? ';
                    } else {
                        $strSQL .= ' AND F.RN BETWEEN ? AND ? ';
                    }
                }
              }
              else{
                   if ($rsMasterArr[0]['DCSUMF'] === '') {
                    $strSQL .= ' WHERE F.RN BETWEEN ? AND ? ';
                    } else {
                        if ($dnlFlg === false) {
                            $strSQL .= ' WHERE F.rownum BETWEEN ? AND ? ';
                        } else {
                            $strSQL .= ' WHERE F.RN BETWEEN ? AND ? ';
                        }
                    }
              }
                $strSQL .= '    ORDER BY  ';
                $strSQL .= join(', ', $ordKeyArr);

                $params[] = $start + 1;
                $params[] = $start + $length;
            }
            e_log("\n>>><<<<".$strSQL.print_r($params, true));
            e_log("SQL Query --->".$strSQL);
            $stmt = db2_prepare($db2con, $strSQL);

            if ($stmt === false) {
                $data = array(
                    'result' => 'FAIL_SEL'
                );
            } else {
                e_log('【削除】：制御合計データ取得：' . $strSQL . print_r($params, true));
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    // $rtn  = 1;
                    $data = array(
                        'result' => 'FAIL_SEL'
                    );
                } else {
                    while ($row = db2_fetch_assoc($stmt)) {
                        $data[] = $row;
                    }
                    $data = array(
                        'result' => true,
                        'data' => $data
                    );
                }
            }
        }
    }
    return $data;
}

//testing $db2con, $FDB2CSV2['data'], $filename, $sSearch,'',$filtersearchData
function fnGetAllCount($db2con, $fdb2csv2, $filename, $search, $D1CFLG = '',$searchDrillList=array(), $filtersearchData=array())
{
    $data   = array();
    $params = array();
    $isWhereFlg=false;
    $underscoreFlg=false;
   
    if ($D1CFLG === '') {
          e_log("p1");
          // $strSQL = ' SELECT count(A.' . cmMer($fdb2csv2[0]['D2FLD']) . '_' . cmMer($fdb2csv2[0]['D2FILID']) . ') as COUNT ';
          $strSQL = ' SELECT count(*) as COUNT ';
          $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
          if ($search != '') {
              if (count($fdb2csv2) > 0) {
              $isWhereFlg=true;
                  $strSQL .= ' WHERE ( ';
                  foreach ($fdb2csv2 as $key => $value) {
                      switch ($value['D2TYPE']) {
                          case 'L':
                              $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(10))' . ' LIKE ? ';
                              break;
                          case 'T':
                              $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(8))' . ' LIKE ? ';
                              break;
                          case 'Z':
                              $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' AS CHAR(26))' . ' LIKE ? ';
                              break;
                          default:
                              $strSQL .= cmMer($value['D2FLD']) . '_' . cmMer($value['D2FILID']) . ' LIKE ? ';
                              break;
                      }
                      array_push($params, '%' . $search . '%');
                      if ($key < (count($fdb2csv2) - 1)) {
                          $strSQL .= ' OR ';
                      }
                  }
          $strSQL .= ' ) ';
              }
          }
      if (count($searchDrillList)>0) {
        $strSQL .= ($isWhereFlg)? ' AND ( ':' WHERE ( ';
        $paramDSQL = array();
        foreach ($searchDrillList as $key => $value) {
            switch ($value['dtype']) {
                case 'L':
                    $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
                    break;
                case 'T':
                    $paramDSQL[] = 'CAST(' .cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
                    break;
                case 'Z':
                    $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
                    break;
                default:
                    $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
                    break;
            }
            $params[] =cmMer($value["val"]);
        }
        $strSQL .= join(' AND ', $paramDSQL);
        $strSQL .= ' ) ';
      }
          if(count($filtersearchData)>0){
          
            foreach ($fdb2csv2 as $key => $value) {
              if (strpos($value['D2FLD'], '_') !== false) {
                //underscore SIGN FOUND
                $underscoreFlg = true;
              }
             }
             if($underscoreFlg === true)
             {
                e_log("result ... TRUE");
                $strSQL .= ($isWhereFlg)? ' AND ( ':' WHERE ( ';
                  $paramDSQL = array();
                  foreach ($filtersearchData as $key => $value) {
                        list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                        e_log("testdata".$D1COL1.$D1COL2);
                        switch ($value['dtype']) {
                            case 'L':
                                $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                                break;
                            case 'T':
                                $paramDSQL[] = 'CAST(' .cmMer($D1COL2) . ' AS CHAR(8))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                                break;
                            case 'Z':
                                $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                                break;
                            default:
                                $paramDSQL[] = $D1COL2 .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                                break;
                  }
                  $params[] =cmMer($value['D1VAL']);
                }
                  $strSQL .= join(' AND ', $paramDSQL);
                  $strSQL .= ' ) ';
             }
             else{
                  e_log("result ... FALSE");
                  $strSQL .= ($isWhereFlg)? ' AND ( ':' WHERE ( ';
                  $paramDSQL = array();
                  foreach ($filtersearchData as $key => $value) {
                        list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                        e_log("testdata".$D1COL1.$D1COL2);
                        switch ($value['dtype']) {
                            case 'L':
                                $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                                break;
                            case 'T':
                                $paramDSQL[] = 'CAST(' .cmMer($D1COL2) . ' AS CHAR(8))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                                break;
                            case 'Z':
                                $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                                break;
                            default:
                                $paramDSQL[] = $D1COL2 .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                                break;
                  }
                  $params[] =cmMer($value['D1VAL']);
                }
                  $strSQL .= join(' AND ', $paramDSQL);
                  $strSQL .= ' ) ';
             }
                  

          }  
      }
     else {
        $isWhereFlg=false;
        //$strSQL = ' SELECT count(A.' . cmMer($fdb2csv2[0]['D2FLD']) . ') as COUNT ';
        $strSQL = ' SELECT count(*) as COUNT ';
        $strSQL .= ' FROM ' . SAVE_DB . '/' . $filename . ' as A ';
        if ($search != '') {
            $rs = fngetSQLFldCCSID($db2con, $filename);
            if ($rs['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result']);
            } else {
                //e_log('fldCCSIDresult'.print_r($rs,true));
                $fldCCSIDARR = $rs['data'];
                $fldccsidCnt = $rs['CCSIDCNT'];
                if ($fldccsidCnt > 0) {
                    $fldArr = array();
                    $isWhereFlg=true;
                    foreach ($fldCCSIDARR as $fldccsid) {
                        switch ($fldccsid['DDS_TYPE']) {
                            case 'L':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(10) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL .= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(10))' . ' LIKE ? ';
                                }
                                break;
                            case 'T':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(8) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL .= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(8))' . ' LIKE ? ';
                                }
                                break;
                            case 'Z':
                                if ($fldccsid['CCSID'] === '65535') {
                                    $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(26) CCSID 5026) ' . ' LIKE ? ';
                                } else {
                                    $strSQL .= 'CAST(' . $fldccsid['COLUMN_NAME'] . ' AS CHAR(26))' . ' LIKE ? ';
                                }
                                break;
                            default:
                                if ($fldccsid['CCSID'] === '65535') {

                                    if($fldccsid['COLUMN_NAME'] === 'BREAKLVL' || $fldccsid['COLUMN_NAME'] === 'OVERFLOW'){
                                        $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(4) CCSID 5026) ' . ' LIKE ? ';
                                    }else{

                                        $fldArr[] = 'CAST(' . $fldccsid['COLUMN_NAME'] . '  AS CHAR(' . $fldccsid['LENGTH'] . ') CCSID 5026) ' . ' LIKE ? ';

                                    }


                                } else {
                                    $fldArr[] = $fldccsid['COLUMN_NAME'] . ' LIKE ? ';
                                }
                                break;
                        }
                      }
                    } else {
                        if (count($fdb2csv2) > 0) {
                            $isWhereFlg=true;
                            $strSQL .= ' WHERE ( ';
                            foreach ($fdb2csv2 as $key => $value) {
                                switch ($value['D2TYPE']) {
                                    case 'L':
                                        $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(10))' . ' LIKE ? ';
                                        break;
                                    case 'T':
                                        $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(8))' . ' LIKE ? ';
                                        break;
                                    case 'Z':
                                        $strSQL .= 'CAST(' . cmMer($value['D2FLD']) . ' AS CHAR(26))' . ' LIKE ? ';
                                        break;
                                    default:
                                        $strSQL .= cmMer($value['D2FLD']) . ' LIKE ? ';
                                        break;
                                }
                                array_push($params, '%' . $search . '%');
                                if ($key < (count($fdb2csv2) - 1)) {
                                    $strSQL .= ' OR ';
                                }
                            }
                $strSQL .= ' ) ';
                        }
                    }
            }
        }
        if (count($searchDrillList)>0) {
          $strSQL .= ($isWhereFlg)? ' AND ( ':' WHERE ( ';
          $paramDSQL = array();
          foreach ($searchDrillList as $key => $value) {
              switch ($value['dtype']) {
                  case 'L':
                      $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(10))' . ' = ? ';
                      break;
                  case 'T':
                      $paramDSQL[] = 'CAST(' .cmMer($value["key"]) . ' AS CHAR(8))' . ' = ? ';
                      break;
                  case 'Z':
                      $paramDSQL[] = 'CAST(' . cmMer($value["key"]) . ' AS CHAR(26))' . ' = ? ';
                      break;
                  default:
                      $paramDSQL[] = cmMer($value["key"]) . ' = ? ';
                      break;
              }
              $params[] =cmMer($value["val"]);
          }
          $strSQL .= join(' AND ', $paramDSQL);
          $strSQL .= ' ) ';
        }
        if(count($filtersearchData)>0){
                $strSQL .= ($isWhereFlg)? ' AND ( ':' WHERE ( ';
                $paramDSQL = array();
                foreach ($filtersearchData as $key => $value) {
                      list($D1COL1, $D1COL2) = explode('-', $value['D1COL']);
                      switch ($value['dtype']) {
                          case 'L':
                              $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(10))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                          case 'T':
                              $paramDSQL[] = 'CAST(' .cmMer($D1COL2) . ' AS CHAR(8))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                          case 'Z':
                              $paramDSQL[] = 'CAST(' . cmMer($D1COL2) . ' AS CHAR(26))' .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                          default:
                              $paramDSQL[] = $D1COL2 .'_'.$D1COL1. $value['D1OPERATOR'] . ' ? ';
                              break;
                }
                $params[] =cmMer($value['D1VAL']);
              }
                $strSQL .= join(' AND ', $paramDSQL);
                $strSQL .= ' ) ';
            }  
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        e_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        e_log('実行SQL：' . $strSQL . print_r($params, true) . db2_stmt_errormsg());
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row['COUNT'];
            }
            $data = array(
                'result' => true,
                'data' => $data[0]
            );
        }
    }
    return $data;
}