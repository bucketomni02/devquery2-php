<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
//必要なさそうなのでコメント
//include_once('licenseInfo.php');
include_once('common/inc/config.php');
include_once('common/inc/common.inc.php');
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$usrinfo = $_SESSION['PHPQUERY']['user'];
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$usrId = $usrinfo[0]['WUUID'];

$db2con = cmDb2Con();

cmSetPHPQUERY($db2con);

// ユーザがログアウトかどうかチェック
if($rtn === 0){
    if($_SESSION['PHPQUERY']['user'][0]['WUUID'] === null){
        $rtn = 1;
        $msg = showMsg('CHECK_LOGOUT',array('ユーザー'));
    }
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = fnGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

$DB2WUSR = fnGetDB2WUSR($db2con,$usrId);

//$DB2WUSR = umEx($DB2WUSR);
if($DB2WUSR[0]['WULANG'] === ''){
    $WULANG = '001';
}else{
    $WULANG = $DB2WUSR[0]['WULANG'];
}
if($WULANG !== '001'){
    $WULANG = fnChkDB2LANG($db2con,$WULANG);
}
cmDb2Close($db2con);
$rtnArray = array(
    'RTN'    => $rtn,
    'MSG'    => $msg,
    'WULANG' => $WULANG,
    'USER'   => $DB2WUSR[0]
);

echo(json_encode($rtnArray));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2WUSR($db2con,$id){

    $data = array();

    $params = array($id);

    $strSQL  = ' SELECT A.WUUID,A.WULANG ';
    $strSQL .= ' FROM DB2WUSR as A ';
    $strSQL .= ' WHERE WUUID <> \'\' ';
    $strSQL .= ' AND WUUID = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }

    return $data;

}

/*
*-------------------------------------------------------* 
* ログインユーザが削除されるかどうかチェック
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $id     ユーザーID
* 
*-------------------------------------------------------*
*/

function fnGetWUAUTH($db2con,$id){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WUSR as A ';
    $strSQL .= ' WHERE WUUID <> \'\' ';
    
    if($id != ''){
        $strSQL .= ' AND WUUID = ? ';
        array_push($params,$id);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;

}
function fnChkDB2LANG($db2con,$WULANG){

    $data = array();
    $rs = '001';

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2LANG as A ';
    $strSQL .= ' WHERE LANID = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        // 処理なし
    }else{
        $params = array($WULANG);
        $r = db2_execute($stmt,$params);
        if($r === false){
            // 処理なし
        }else{
            while($row = db2_fetch_assoc($stmt)){

                $nrow = array();
                foreach($row as $key => $value){
                    $nrow[$key] = um($value);
                }

                $data[] = $nrow;
            }
            //$data = umEx($data);
            if(count($data) >  0){
                $rs = $data[0]['LANID'];
            }
        }
    }
    return $rs;


}