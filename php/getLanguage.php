<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("licenseInfo.php");
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$name = '';
$rtn = 0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rs = fnGetDB2LANG($db2con);
if($rtn === 0){
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $langdata = $rs['data'];
    }
}
//アイテム取得
if($rtn === 0){
    $langItmArr = array();
    $rsDefaultItm = fnGetDefaultText($db2con);
    if($rsDefaultItm['result'] !== true){
        $msg = showMsg($rsDefaultItm['result']);
        $rtn = 1;
    }else{
        $langItmArr['001'] = createItmData($rsDefaultItm['data']);
    }
    if($rtn === 0){
        foreach($langdata as $lang){
            $rsLangItm = fnGetDB2MTEXT($db2con,$lang['LANID']);
            if($rsLangItm['result'] !== true){
                $msg = showMsg($rsLangItm['result']);
                $rtn = 1;
                break;
            }else{
                $langItmArr[$lang['LANID']] = createItmData($rsLangItm['data']);
            }
        }
    }
}
// メッセージ取得
if($rtn === 0){
    $langMsgArr = array();
    $rsDefaultMsg = fnGetDefaultMsgTxt($db2con);
    if($rsDefaultMsg['result'] !== true){
        $msg = showMsg($rsDefaultMsg['result']);
        $rtn = 1;
    }else{
        $langMsgArr['001'] = createMsgData($rsDefaultMsg['data']);
    }
    if($rtn === 0){
        foreach($langdata as $lang){
            $rsLangMsg = fnGetMsgDB2MTEXT($db2con,$lang['LANID']);
            if($rsLangMsg['result'] !== true){
                $msg = showMsg($rsLangMsg['result']);
                $rtn = 1;
                break;
            }else{
                //e_log(print_r($rsLangMsg['data'],true));
                $langMsgArr[$lang['LANID']] = createMsgData($rsLangMsg['data']);
            }
        }
    }
}
// 固定データ取得
if($rtn === 0){
    $langConstArr = array();
    $rsDefaultConst = fnGetDefaultConstTxt($db2con);
    if($rsDefaultConst['result'] !== true){
        $msg = showMsg($rsDefaultConst['result']);
        $rtn = 1;
    }else{
        $langConstArr['001'] = createConstData($rsDefaultConst['data']);
    }
    if($rtn === 0){
        foreach($langdata as $lang){
            $rsLangConst = fnGetConstDB2MTEXT($db2con,$lang['LANID']);
            if($rsLangConst['result'] !== true){
                $msg = showMsg($rsLangConst['result']);
                $rtn = 1;
                break;
            }else{
                //e_log(print_r($rsLangConst['data'],true));
                $langConstArr[$lang['LANID']] = createConstData($rsLangConst['data']);
            }
        }
    }
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'aaData' => $langdata,
    'LANGITM' => $langItmArr,
    'LANGMSG' => $langMsgArr,
    'LANGCONST' => $langConstArr,
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 言語取得
*-------------------------------------------------------*
*/

function fnGetDB2LANG($db2con){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2LANG AS A ' ;
    $strSQL .= ' ORDER BY LANID ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* アイテムテキスト取得
*-------------------------------------------------------*
*/
function fnGetDB2MTEXT($db2con,$LANID){
    $data = array();

    $params = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     \''.$LANID.'\', ';
    $strSQL .= '     A.FRMID, ';
    $strSQL .= '     A.ITMID, ';
    $strSQL .= '     A.ITMKBN, ';
    $strSQL .= '     (CASE ';
    $strSQL .= '     WHEN B.ITMTEXT IS NULL THEN A.ITMTEXT ';
    $strSQL .= '     WHEN B.ITMTEXT = \'\' THEN A.ITMTEXT ';
    $strSQL .= '     ELSE B.ITMTEXT ';
    $strSQL .= '     END)  ITMTEXT, ';
    $strSQL .= '     B.ITMKBN AS TXTKBN ';
    $strSQL .= ' FROM DB2MITM A '; 
    $strSQL .= ' LEFT JOIN  ';
    $strSQL .= ' ( SELECT * FROM DB2MTEXT WHERE LANCD = ?) B '; 
    $strSQL .= ' ON A.ITMTEXT = B.ITMNM ';
    $strSQL .= ' ORDER BY ';
    $strSQL .= '     B.LANCD, ';
    $strSQL .= '     A.FRMID, ';
    $strSQL .= '     A.ITMKBN, ';
    $strSQL .= '     A.ITMID ';
    $stmt = db2_prepare($db2con,$strSQL);
    //e_log('言語テキスト取得:'.$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($LANID);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* アイテムデータ配列作成
*-------------------------------------------------------*
*/
function createItmData($itmData){
    $itmDataArr = array();
    foreach($itmData as $itm){
        $itminfo = array();
        if($itm['ITMKBN'] == 1){
            $itminfo['ITMID'] = $itm['FRMID'];
            $itminfo['ITMTEXT'] = $itm['ITMTEXT'];
        }else{
            $itminfo['ITMID'] = $itm['FRMID'].'_'.$itm[ITMID];
            $itminfo['ITMTEXT'] = $itm['ITMTEXT'];
        }
        $itmDataArr[$itminfo['ITMID']] = $itminfo['ITMTEXT'];
    }
    return $itmDataArr;
}
/*
*-------------------------------------------------------* 
* Defaultアイテムデータ配列作成
*-------------------------------------------------------*
*/
function fnGetDefaultText($db2con){
    $data = array();

    $params = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     \'001\' AS LANCD, ';
    $strSQL .= '     FRMID, ';
    $strSQL .= '     ITMID, ';
    $strSQL .= '     ITMTEXT, ';
    $strSQL .= '     ITMKBN , ';
    $strSQL .= '     \'1\' AS TXTKBN ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2MITM ';
    $strSQL .= ' ORDER BY ';
    $strSQL .= '     ITMKBN, ';
    $strSQL .= '     FRMID, ';
    $strSQL .= '     ITMID ';

    $stmt = db2_prepare($db2con,$strSQL);
    //e_log('言語テキDefault取得:'.$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* Defaultメッセージデータ配列作成
*-------------------------------------------------------*
*/
function fnGetDefaultMsgTxt($db2con){
    $data = array();

    $params = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     \'001\' AS LANCD, ';
    $strSQL .= '     MSGCD, ';
    $strSQL .= '     MSGTEXT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2MMSG ';
    $strSQL .= ' WHERE MSGFLG = 1 ';

    $stmt = db2_prepare($db2con,$strSQL);
    //e_log('言語テキDefault取得:'.$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* メッセージデータ配列作成
*-------------------------------------------------------*
*/
function createMsgData($msgData){
    $msgDataArr = array();
    foreach($msgData as $msg){
        $msgInfo = array();
        $msgInfo['MSGCD'] = $msg['MSGCD'];
        $msgInfo['MSGTEXT'] = $msg['MSGTEXT'];
        $msgDataArr[$msgInfo['MSGCD']] = $msgInfo['MSGTEXT'];
    }
    return $msgDataArr;
}

/*
*-------------------------------------------------------* 
* メッセージテキスト取得
*-------------------------------------------------------*
*/
function fnGetMsgDB2MTEXT($db2con,$LANID){
    $data = array();

    $params = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     \''.$LANID.'\', ';
    $strSQL .= '     A.MSGCD, ';
    $strSQL .= '     (CASE ';
    $strSQL .= '     WHEN B.ITMTEXT IS NULL THEN A.MSGTEXT ';
    $strSQL .= '     WHEN B.ITMTEXT = \'\' THEN A.MSGTEXT ';
    $strSQL .= '     ELSE B.ITMTEXT ';
    $strSQL .= '     END)  MSGTEXT ';
    $strSQL .= ' FROM DB2MMSG A '; 
    $strSQL .= ' LEFT JOIN  ';
    $strSQL .= ' ( SELECT * FROM DB2MTEXT WHERE LANCD = ?) B '; 
    $strSQL .= ' ON A.MSGTEXT = B.ITMNM ';
    $strSQL .= ' WHERE A.MSGFLG = \'1\' '; 
    $strSQL .= ' ORDER BY ';
    $strSQL .= '     B.LANCD ';
    $stmt = db2_prepare($db2con,$strSQL);
    //e_log('言語テキスト取得:'.$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($LANID);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* 固定テキスト取得
*-------------------------------------------------------*
*/
function fnGetConstDB2MTEXT($db2con,$LANID){
    $data = array();

    $params = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     \''.$LANID.'\', ';
    $strSQL .= '     A.KCD, ';
    $strSQL .= '     (CASE ';
    $strSQL .= '     WHEN B.ITMTEXT IS NULL THEN A.KTEXT ';
    $strSQL .= '     WHEN B.ITMTEXT = \'\' THEN A.KTEXT ';
    $strSQL .= '     ELSE B.ITMTEXT ';
    $strSQL .= '     END)  KTEXT ';
    $strSQL .= ' FROM DB2MCONST A '; 
    $strSQL .= ' LEFT JOIN  ';
    $strSQL .= ' ( SELECT * FROM DB2MTEXT WHERE LANCD = ?) B '; 
    $strSQL .= ' ON A.KTEXT = B.ITMNM ';
    $strSQL .= ' WHERE A.KFLG = \'1\' '; 
    $strSQL .= ' ORDER BY ';
    $strSQL .= '     B.LANCD ';
    $stmt = db2_prepare($db2con,$strSQL);
    //e_log('言語テキスト取得:'.$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $params = array($LANID);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* Defaultメッセージデータ配列作成
*-------------------------------------------------------*
*/
function fnGetDefaultConstTxt($db2con){
    $data = array();

    $params = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     \'001\' AS LANCD, ';
    $strSQL .= '     KCD, ';
    $strSQL .= '     KTEXT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2MCONST ';
    $strSQL .= ' WHERE KFLG = 1 ';

    $stmt = db2_prepare($db2con,$strSQL);
    //e_log('言語テキDefault取得:'.$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* メッセージデータ配列作成
*-------------------------------------------------------*
*/
function createConstData($constData){
    $constDataArr = array();
    foreach($constData as $const){
        $constInfo = array();
        $constInfo['KCD'] = $const['KCD'];
        $constInfo['KTEXT'] = $const['KTEXT'];
        $constDataArr[$constInfo['KCD']] = $constInfo['KTEXT'];
    }
    return $constDataArr;
}