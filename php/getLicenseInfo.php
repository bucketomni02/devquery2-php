<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("licenseInfo.php");
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

//scriptに必要なライセンス情報を追加してく
$ary = array(
    'licenseLogoPath' => $licenseLogoPath,
    'licenseColor1'   => $licenseColor1,
    'licenseColor2'   => $licenseColor2,
    'licenseColor3'   => $licenseColor3,
    'licenseTitle'    => $licenseTitle,
    'licenseProgressText' => $licenseProgressText,
    'licenseFavicon'  => $licenseFavicon,
    'licensePivot'  => $licensePivot,
    'licenseGraph'  => $licenseGraph
);

echo(json_encode($ary));
