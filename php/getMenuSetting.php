<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");
/*
 *-------------------------------------------------------* 
 * リクエスト
 *-------------------------------------------------------*
 */
$WUUID = '';
if (isset($_SESSION['PHPQUERY']['LOGIN']) && $_SESSION['PHPQUERY']['LOGIN'] === '1') {
    if ($_POST['WUUID'] == '') {
        $WUUID = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
    } else {
        $WUUID = $_POST['WUUID'];
    }
} else {
    $WUUID = $_POST['WUUID'];
}

/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$data = array();
$rtn  = 0;
$msg  = '';
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    if ($WUUID == '') {
        if ($WUUID === null) {
            $rtn = 1;
            $msg = showMsg('CHECK_LOGOUT', array(
                'ユーザー'
            ));
        }
    }
}

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmCheckUser($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }
}

if ($rtn === 0) {
    $rs = fnGetMenuSetting($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result']);
    } else {
        $data = $rs['data'];
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => umEx($data, $false)
);

echo (json_encode($rtn));

/*
 *-------------------------------------------------------* 
 * DB2WUSR取得(1行)
 *-------------------------------------------------------*
 */

function fnGetMenuSetting($db2con, $WUUID)
{
    
    $data = array();
    
    $params = array(
        $WUUID
    );
    
    $strSQL = ' SELECT B.WUUQFG,B.WUUBFG ';
    $strSQL .= ' FROM DB2WUSR AS B';
    $strSQL .= ' WHERE WUUID = ? ';
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    return $data;
}