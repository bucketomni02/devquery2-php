<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");


function fnGetBQRYCND($db2con,$qrynm){
    $data = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM BQRYCND AS A ' ;
    $strSQL .= ' WHERE A.CNQRYN = ? ';
    //$strSQL .= ' AND   A.CNSTKB <> ? ';
    $strSQL .= ' ORDER BY A.CNMSEQ ';
    $strSQL .= '        , A.CNSSEQ ';

    $params = array(
        $qrynm
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => umEx($data)
            );
        }
    }
    return $data;
}

function fnBQRYCNDDAT($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';
	$strimArr = array();
	$j = array();    	

    $strSQL .= '       SELECT QRYCND.CNQRYN ';
    $strSQL .= '            , QRYCND.CNFILID ';
    $strSQL .= '            , QRYCND.FILSEQ ';
    $strSQL .= '            , QRYCND.CNFIL ';
    $strSQL .= '            , QRYCND.CNMSEQ ';
    $strSQL .= '            , GRPCND.CNDCNT '; 
    $strSQL .= '            , GRPCND.CNSCNT '; 
    $strSQL .= '            , GRPCND.CDCNT '; 
    $strSQL .= '            , QRYCND.CNSSEQ '; 
    $strSQL .= '            , QRYCND.CNFLDN ';
    $strSQL .= '            , QRYCND.D2HED ';
    $strSQL .= '            , QRYCND.D2TYPE ';
    $strSQL .= '            , QRYCND.D2LEN ';
    $strSQL .= '            , QRYCND.D2DEC ';
    $strSQL .= '            , QRYCND.CNAOKB ';
    $strSQL .= '            , QRYCND.CNCKBN '; 
    $strSQL .= '            , QRYCND.CNSTKB '; 
    $strSQL .= '            , QRYCND.CNCANL '; 
    $strSQL .= '            , QRYCND.CNCANF '; 
    $strSQL .= '            , QRYCND.CNCMBR '; 
    $strSQL .= '            , QRYCND.CNCANC '; 
    $strSQL .= '            , QRYCND.CNCANG '; 
    $strSQL .= '            , QRYCND.CNNAMG '; 
    $strSQL .= '            , QRYCND.CNNAMC ';
    //ソート用カラム導入
    $strSQL .= '            , QRYCND.CNCAST ';
    $strSQL .= '            , QRYCND.CNNAST ';

    //カレンダ用カラム導入
    $strSQL .= '            , QRYCND.CNDFMT ';
    $strSQL .= '            , QRYCND.CNDSFL ';

    //日付のデータ用カラム導入
    $strSQL .= '            , QRYCND.CNDFIN ';
    $strSQL .= '            , QRYCND.CNDFPM ';
    $strSQL .= '            , QRYCND.CNDFDY ';
    $strSQL .= '            , QRYCND.CNDFFL ';
    $strSQL .= '            , QRYCND.CNDTIN ';
    $strSQL .= '            , QRYCND.CNDTPM ';
    $strSQL .= '            , QRYCND.CNDTDY ';
    $strSQL .= '            , QRYCND.CNDTFL ';
    $strSQL .= '            , QRYCND.CNDSCH ';

    // ヘルプ情報取得
    $strSQL .= '            , QRYCND.DHINFO ';
    $strSQL .= '            , QRYCND.DHINFG ';

    $strSQL .= '            , QRYCND.CDCDCD '; 
    $strSQL .= '            , QRYCND.CDDAT '; 
    $strSQL .= '            , QRYCND.CDBCNT '; 
    $strSQL .= '            , QRYCND.CDFFLG '; 
    $strSQL .= '       FROM ';         
    $strSQL .= '           ( SELECT ';     
    $strSQL .= '                    A.CNQRYN '; 
    $strSQL .= '                  , A.CNFILID '; 
    $strSQL .= '                  , FILCD.FILSEQ '; 
    $strSQL .= '                  , ( CASE '; 
    $strSQL .= '                         WHEN A.CNFILID = 0 THEN \'P\' ';
    $strSQL .= '                         WHEN A.CNFILID <= FILCD.FILSEQ  ';
    $strSQL .= '                         THEN CONCAT(\'S\', A.CNFILID )  ';
    $strSQL .= '                         ELSE \'K\' ';               
    $strSQL .= '                      END ';              
    $strSQL .= '                     ) AS CNFIL ';       
    $strSQL .= '                  , A.CNMSEQ '; 
    $strSQL .= '                  , A.CNSSEQ ';
    $strSQL .= '                  , A.CNFLDN ';
    $strSQL .= '                  , A.D2HED ';
    $strSQL .= '                  , A.D2TYPE ';
    $strSQL .= '                  , A.D2LEN ';
    $strSQL .= '                  , A.D2DEC ';
    $strSQL .= '                  , A.CNAOKB ';
    $strSQL .= '                  , A.CNCKBN '; 
    $strSQL .= '                  , A.CNSTKB ';
    $strSQL .= '                  , A.CNCANL ';
    $strSQL .= '                  , A.CNCANF ';
    // テーブルメンバー追加
    $strSQL .= '                  , A.CNCMBR ';
    $strSQL .= '                  , A.CNCANC ';
    $strSQL .= '                  , A.CNCANG ';
    $strSQL .= '                  , A.CNNAMG ';
    $strSQL .= '                  , A.CNNAMC ';
    //ソート用カラム導入
    $strSQL .= '                  , A.CNCAST ';
    $strSQL .= '                  , A.CNNAST ';
    //カレンダ用カラム導入
    $strSQL .= '                  , A.CNDFMT ';
    $strSQL .= '                  , A.CNDSFL ';
    //日付のデータ用カラム導入
    $strSQL .= '                  , A.CNDFIN ';
    $strSQL .= '                  , A.CNDFPM ';
    $strSQL .= '                  , A.CNDFDY ';
    $strSQL .= '                  , A.CNDFFL ';
    $strSQL .= '                  , A.CNDTIN ';
    $strSQL .= '                  , A.CNDTPM ';
    $strSQL .= '                  , A.CNDTDY ';
    $strSQL .= '                  , A.CNDTFL ';
    $strSQL .= '                  , A.CNDSCH ';
    // ヘルプ情報取得
    $strSQL .= '                  , HCND.DHINFO ';
    $strSQL .= '                  , HCND.DHINFG ';

    $strSQL .= '                  , B.CDCDCD '; 
    $strSQL .= '                  , B.CDDAT ';
    $strSQL .= '                  , B.CDBCNT ';
     $strSQL .= '                 , B.CDFFLG ';
    $strSQL .= '             FROM ';
    $strSQL .= '                ( SELECT AA.* ';
    $strSQL .= '                  FROM '; 
    $strSQL .= '                   (SELECT QCND.* '; 
    $strSQL .= '                        , FLDINFO.D2HED ';
    $strSQL .= '                        , FLDINFO.D2TYPE ';
    $strSQL .= '                        , FLDINFO.D2LEN ';
    $strSQL .= '                        , FLDINFO.D2DEC ';
    $strSQL .= '                    FROM BQRYCND QCND '; 
    $strSQL .= '                    LEFT JOIN ( ';
    $strSQL .= '                        SELECT FLDDATA.* '; 
    $strSQL .= '                        FROM ( '; 
    $strSQL .= '                             SELECT D2NAME ';
    $strSQL .= '                                  , D2FILID ';
    $strSQL .= '                                  , D2FLD ';
    $strSQL .= '                                  , D2HED ';
    $strSQL .= '                                  , D2CSEQ ';
    $strSQL .= '                                  , D2WEDT ';
    $strSQL .= '                                  , D2TYPE ';
    $strSQL .= '                                  , D2LEN ';
    $strSQL .= '                                  , D2DEC ';
    $strSQL .= '                                  , D2DNLF '; 
    $strSQL .= '                            FROM FDB2CSV2 '; 
    $strSQL .= '                            WHERE D2NAME = ? '; 
    $strSQL .= '                        UNION ALL  ';
    $strSQL .= '                            SELECT  D5NAME AS D2NAME ';
    $strSQL .= '                                   , 9999 AS D2FILID  ';
    $strSQL .= '                                   , D5FLD AS D2FLD ';
    $strSQL .= '                                   , D5HED AS D2HED ';
    $strSQL .= '                                   , D5CSEQ AS D2CSEQ ';
    $strSQL .= '                                   , D5WEDT AS D2WEDT ';
    $strSQL .= '                                   , D5TYPE AS D2TYPE ';
    $strSQL .= '                                   , D5LEN AS D2LEN ';
    $strSQL .= '                                   , D5DEC AS D2DEC ';
    $strSQL .= '                                   , D5DNLF AS D2DNLF  ';
    $strSQL .= '                            FROM FDB2CSV5  ';
    $strSQL .= '                            WHERE D5NAME = ? '; 
    $strSQL .= '                        )  FLDDATA '; 
    $strSQL .= '                    ) FLDINFO ';   
    $strSQL .= '                    ON QCND.CNQRYN    = FLDINFO.D2NAME ';
    $strSQL .= '                    AND QCND.CNFILID  = FLDINFO.D2FILID ';
    $strSQL .= '                    AND QCND.CNFLDN   = FLDINFO.D2FLD ';
    $strSQL .= '                    WHERE QCND.CNQRYN = ? ';
    $strSQL .= '                   )   AA  ';
    $strSQL .= '                ) A  ';
    $strSQL .= '               LEFT JOIN  BCNDDAT B  ';
    $strSQL .= '               ON    A.CNQRYN  = B.CDQRYN  ';
    $strSQL .= '               AND   A.CNFILID = B.CDFILID ';    
    $strSQL .= '               AND   A.CNMSEQ  = B.CDMSEQ '; 
    $strSQL .= '               AND   A.CNSSEQ  = B.CDSSEQ  ';
    // ヘルプ情報取得
    $strSQL .= '               LEFT JOIN  DB2HCND HCND  ';
    $strSQL .= '               ON    A.CNQRYN  = HCND.DHNAME  ';
    $strSQL .= '               AND   A.CNFILID = HCND.DHFILID ';    
    $strSQL .= '               AND   A.CNMSEQ  = HCND.DHMSEQ '; 
    $strSQL .= '               AND   A.CNSSEQ  = HCND.DHSSEQ  ';

    $strSQL .= '               LEFT JOIN  ( '; 
    $strSQL .= '                   SELECT RTQRYN,MAX(RTRSEQ) AS FILSEQ ';            
    $strSQL .= '                   FROM BREFTBL '; 
    $strSQL .= '                   WHERE RTQRYN = ? '; 
    $strSQL .= '                   GROUP BY  RTQRYN ) FILCD  ';
    $strSQL .= '               ON FILCD.RTQRYN = A.CNQRYN  ';   
    $strSQL .= '               WHERE   A.CNQRYN  = ? '; 
   // $strSQL .= '                 AND   A.CNSTKB <>  \'3\' ';
    $strSQL .= '               ORDER BY A.CNMSEQ ,A.CNSSEQ  ,B.CDCDCD  ';
    $strSQL .= '           ) QRYCND ,  ';
    $strSQL .= '           (  ';
    $strSQL .= '               SELECT A.CNQRYN  ';
    $strSQL .= '                    , A.CNMSEQ  ';
    $strSQL .= '                    , B.CNDCNT  ';
    $strSQL .= '                    , A.CNSCNT  ';             
    $strSQL .= '                    , C.CDSSEQ  ';
    $strSQL .= '                    , C.CDCNT  ';
    $strSQL .= '               FROM (  '; 
    $strSQL .= '                       SELECT CNQRYN  ';
    $strSQL .= '                            , CNMSEQ  ';           
    $strSQL .= '                            , COUNT(CNSSEQ) CNSCNT  ';
    $strSQL .= '                         FROM BQRYCND  ';
    $strSQL .= '                        WHERE CNQRYN  = ?  ';
    //$strSQL .= '                          AND CNSTKB <>  \'3\' ';
    $strSQL .= '                     GROUP BY CNQRYN ';               
    $strSQL .= '                            , CNMSEQ  ';
    $strSQL .= '                   ) A  ';
    $strSQL .= '                   ,(  ';
    $strSQL .= '                       SELECT CNQRYN  ';
    $strSQL .= '                            , MAX(CNMSEQ) AS CNDCNT  ';
    $strSQL .= '                         FROM BQRYCND   ';     
    $strSQL .= '                        WHERE CNQRYN  = ?  ';
   // $strSQL .= '                          AND CNSTKB <>  \'3\' ';
    $strSQL .= '                     GROUP BY CNQRYN  ';
    $strSQL .= '                   ) B  ';
    $strSQL .= '                   ,( ';
    $strSQL .= '                       SELECT QRYC.CNQRYN AS CDQRYN '; 
    $strSQL .= '                            , QRYC.CNMSEQ AS CDMSEQ  ';
    $strSQL .= '                            , QRYC.CNSSEQ AS CDSSEQ '; 
    $strSQL .= '                            , COUNT(CDAT.CDCDCD) CDCNT  ';        
    $strSQL .= '                         FROM BQRYCND QRYC  '; 
    $strSQL .= '                    LEFT JOIN BCNDDAT CDAT  ';
    $strSQL .= '                           ON QRYC.CNQRYN  = CDAT.CDQRYN  ';
    $strSQL .= '                          AND QRYC.CNMSEQ = CDAT.CDMSEQ  '; 
    $strSQL .= '                          AND QRYC.CNSSEQ = CDAT.CDSSEQ  ';
    $strSQL .= '                        WHERE QRYC.CNQRYN  = ?  ';
   // $strSQL .= '                          AND QRYC.CNSTKB <>  \'3\' ';
    $strSQL .= '                     GROUP BY QRYC.CNQRYN ';
    $strSQL .= '                            , QRYC.CNMSEQ ';
    $strSQL .= '                            , QRYC.CNSSEQ ';
    $strSQL .= '                            , CDAT.CDQRYN ';
    $strSQL .= '                            , CDAT.CDMSEQ ';
    $strSQL .= '                            , CDAT.CDSSEQ ';
    $strSQL .= '                   )C ';
    $strSQL .= '               WHERE A.CNQRYN = B.CNQRYN ';
    $strSQL .= '                 AND A.CNQRYN = C.CDQRYN ';
    $strSQL .= '                 AND B.CNQRYN = C.CDQRYN ';
    $strSQL .= '                 AND A.CNMSEQ = C.CDMSEQ ';
    $strSQL .= '           ) GRPCND ';    
    $strSQL .= '       WHERE QRYCND.CNQRYN =  GRPCND.CNQRYN ';
    $strSQL .= '         AND QRYCND.CNMSEQ =  GRPCND.CNMSEQ ';
    $strSQL .= '         AND QRYCND.CNSSEQ =  GRPCND.CDSSEQ ';
    $strSQL .= '       ORDER BY QRYCND.CNMSEQ ';
    $strSQL .= '              , QRYCND.CNSSEQ ';
	
    $params = array($QRYNM,$QRYNM,$QRYNM,$QRYNM,$QRYNM,$QRYNM,$QRYNM,$QRYNM);
	//error_log('query = '.$strSQL);
    e_log("/**//**//**/");
    e_log($strSQL.print_r($params,true));
    e_log("/**//**//**/");
     $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
			//実行画面に検索フィードの名取得
			foreach($data as $key => $value){
				if($value['CDFFLG'] === '1'){	
					$strimArr = explode('.',$value['CDDAT']);
					if($strimArr[0] === 'K'){
						$j = fnGetFDB2CSV5Shuotku($db2con,$QRYNM,$strimArr[1]);
						$data[$key]["D2HED1"] = $j['data']['D2HED'];
					}
					else{
						$j = fnGetFDB2CSV2Shuotku($db2con,$QRYNM,$strimArr[1]); 
						$data[$key]["D2HED1"] = $j['data']['D2HED'];
					}
				}
			}
			//*******************************
            $data = array('result' => true,'data' => $data);
        }
    }
    //error_log("CNNDATA ??? :".print_r($data,true));
    return $data;
}

function fnCreateCndData($bqrycnddat){
    $cdcnt = '';   
    $cnmseq = '';
    $cnsseq = '';
    $bqrycnddatRes = array();
   
    foreach($bqrycnddat as $bqrycnddatdata){
        if($cnmseq != $bqrycnddatdata['CNMSEQ']){
            $bqrycnd = array();
            $cnmseq = $bqrycnddatdata['CNMSEQ'];
            if($bqrycnddatdata['CNSSEQ'] == 1){
                $cnsseq = '';
                $bqrycnd['CNDIDX'] = $bqrycnddatdata['CNMSEQ'];
                $bqrycnd['SQRYAO'] = $bqrycnddatdata['CNAOKB'];
                $bqrycnd['CDATACNT'] = $bqrycnddatdata['CNSCNT'];
                $bqrycnd['CNDSDATA'] = array();
            }
        }
		e_log("getQryCnd.php fnCreateCndData=>".$cnmseq."=>".$bqrycnddatdata['CNMSEQ']);
        if($cnmseq == $bqrycnddatdata['CNMSEQ']){
            if($cnsseq != $bqrycnddatdata['CNSSEQ']){
                $cnsseq = $bqrycnddatdata['CNSSEQ'];
                $bqrycndinfo = array();
                
                $bqrycndinfo['CNDDATA'] = array();
                $bqrycndinfo['D2HED'] = array();
                $cdcnt = '';
                $cdcnt = $bqrycnddatdata['CDCNT'];
                $bqrycndinfo['CDATAIDX'] = $cnsseq;
                if($bqrycnddatdata['CNSSEQ'] > 1){
                    $bqrycndinfo['CNDANDOR'] = $bqrycnddatdata['CNAOKB'];
                }else{
                    $bqrycndinfo['CNDANDOR'] = '';
                }
                if($bqrycnddatdata['CNFIL'] === ''){
                    $bqrycndinfo['CNDFLD']   = $bqrycnddatdata['CNFLDN'];
                }else{
                    $bqrycndinfo['CNDFLD']   = $bqrycnddatdata['CNFIL'].'.'.$bqrycnddatdata['CNFLDN'];
                }
                $bqrycndinfo['CNFILID']  = $bqrycnddatdata['CNFILID'];
                $bqrycndinfo['CNDFHED']  = cmHsc($bqrycnddatdata['D2HED']);
                $bqrycndinfo['CNDFTYP']  = $bqrycnddatdata['D2TYPE'];
                $bqrycndinfo['CNDFLEN']  = $bqrycnddatdata['D2LEN'];
                $bqrycndinfo['CNDFDEC']  = $bqrycnddatdata['D2DEC'];
                $bqrycndinfo['CNDKBN']   = $bqrycnddatdata['CNCKBN'];
                $bqrycndinfo['CNDTYP']   = $bqrycnddatdata['CNSTKB'];
                $bqrycndinfo['CNCANL']   = $bqrycnddatdata['CNCANL'];
                $bqrycndinfo['CNCANF']   = $bqrycnddatdata['CNCANF'];
                // テーブルメンバー挿入
                $bqrycndinfo['CNCMBR']   = $bqrycnddatdata['CNCMBR'];
                $bqrycndinfo['CNCANC']   = $bqrycnddatdata['CNCANC'];
                $bqrycndinfo['CNCANG']   = $bqrycnddatdata['CNCANG'];
                $bqrycndinfo['CNNAMG']   = $bqrycnddatdata['CNNAMG'];
                $bqrycndinfo['CNNAMC']   = $bqrycnddatdata['CNNAMC'];
                //ソート用カラム導入
                $bqrycndinfo['CNCAST']   = $bqrycnddatdata['CNCAST'];
                $bqrycndinfo['CNNAST']   = $bqrycnddatdata['CNNAST'];

                //カレンダ用カラム導入
                $bqrycndinfo['CNDFMT']   = $bqrycnddatdata['CNDFMT'];
                $bqrycndinfo['CNDSFL']   = $bqrycnddatdata['CNDSFL'];
                
                //日付のデータ用カラム導入
                $bqrycndinfo['CNDFIN']   = $bqrycnddatdata['CNDFIN'];
                $bqrycndinfo['CNDFPM']   = $bqrycnddatdata['CNDFPM'];
                $bqrycndinfo['CNDFDY']   = $bqrycnddatdata['CNDFDY'];
                $bqrycndinfo['CNDFFL']   = $bqrycnddatdata['CNDFFL'];
                $bqrycndinfo['CNDTIN']   = $bqrycnddatdata['CNDTIN'];
                $bqrycndinfo['CNDTPM']   = $bqrycnddatdata['CNDTPM'];
                $bqrycndinfo['CNDTDY']   = $bqrycnddatdata['CNDTDY'];
                $bqrycndinfo['CNDTFL']   = $bqrycnddatdata['CNDTFL'];
                $bqrycndinfo['CNDSCH']   = $bqrycnddatdata['CNDSCH'];
                //日付のデータ用カラム導入
                $bqrycndinfo['DHINFO']   = cmHsc($bqrycnddatdata['DHINFO']);
                $bqrycndinfo['DHINFG']   = $bqrycnddatdata['DHINFG'];

                $bqrycndinfo['CDCNT']    = $bqrycnddatdata['CDCNT'];
                $bqrycndinfo['CDFFLG']    = $bqrycnddatdata['CDFFLG'];
                $bqrycndinfo['D2HED1']    = $bqrycnddatdata['D2HED1'];
            }
			
            if($bqrycndinfo['CDATAIDX'] == $bqrycnddatdata['CNSSEQ']){
                if($bqrycndinfo['CDCNT'] == 0){
                    if($bqrycnddatdata['CNCKBN'] === 'RANGE'){
                        $bqrycndinfo['CNDDATA'][] = '';
                    }
                    $bqrycndinfo['CNDDATA'][] = '';
                    $bqrycnd['CNDSDATA'][]  = $bqrycndinfo;
                }else{
                    if((int)$bqrycnddatdata['CDBCNT'] > 0){
                        $bqrycnddatdata['CDDAT'] = str_pad($bqrycnddatdata['CDDAT'], (int)$bqrycnddatdata['CDBCNT']);                 
                    }
                    $bqrycndinfo['CNDDATA'][] = $bqrycnddatdata['CDDAT'];
                    $bqrycndinfo['D2HED'][] = $bqrycnddatdata['D2HED'];
                   
                    if ($bqrycndinfo['CDCNT'] == count($bqrycndinfo['CNDDATA'])){
                        $bqrycnd['CNDSDATA'][]  = $bqrycndinfo;
                    }
                }
                
            }
            if($bqrycnd['CDATACNT'] == count($bqrycnd['CNDSDATA'])){
                $bqrycnddatRes[] = $bqrycnd;
            }
        }
    }
    return $bqrycnddatRes;
}

//実行画面に検索フィードの名取得
function fnGetFDB2CSV2Shuotku($db2con,$qrynm,$fldnm){
    $data = array();
    $strSQL  = ' SELECT DISTINCT A.D2HED ';
    $strSQL .= ' FROM FDB2CSV2 AS A ' ;
    $strSQL .= ' WHERE A.D2NAME = ? AND A.D2FLD = ?';
    $params = array(
        $qrynm,
		$fldnm	
    );
	$stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => $data[0]
            );
        }
    }
    return $data;
}
//実行画面に検索フィードの名取得
function fnGetFDB2CSV5Shuotku($db2con,$qrynm,$fldnm){
    $data = array();
    $strSQL  = ' SELECT DISTINCT A.D5HED ';
    $strSQL .= ' FROM FDB2CSV5 AS A ' ;
    $strSQL .= ' WHERE A.D5NAME = ? AND A.D5FLD = ?';
    $params = array(
        $qrynm,
		$fldnm	
    );
   $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{

        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => $data[0]
            );
        }
    }
    return $data;
}
