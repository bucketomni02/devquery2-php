<?php
/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("base/createExecuteSQL.php");

/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */
$filename=(isset($_POST['filename']) ? $_POST['filename'] : '');
$outputfdata     = json_decode($_POST['OUTPUTFDATA'],true);
/*
 *-------------------------------------------------------* 
 * 処理 
 *-------------------------------------------------------*
 */
$data=array();
$systables=array();
$rtn=0;
$msg='';
$RDBNM='';

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmCheckUser($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
if($rtn===0){
    if(count($outputfdata)>0){
        $D1NAME=cmMer($outputfdata[0]['D1NAME']);
        //チェッククエリー
        if($rtn === 0){
            $chkQry = array();
            $chkQry = cmChkQuery($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$D1NAME,'');
            if($chkQry['result'] !== true){
                $rtn = 1;
                $msg = showMsg($chkQry['result'],array('クエリー'));
            }else{
                $RDBNM =  cmMer($chkQry['FDB2CSV1']['D1RDB']);
            }
        }
    }
}
//出力ファイルのためTB作成  $RDBNM
if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
    $db2RDBOutputFileCon = cmDB2ConRDB(SAVE_DB);
}

if(count($outputfdata)>0){
    $D1OUTLIB=strtoupper(cmMer($outputfdata[0]['D1OUTLIB']));
    $D1OUTFIL=strtoupper(cmMer($outputfdata[0]['D1OUTFIL']));
    $D1OLDOUTLIB=cmMer($outputfdata[0]['D1OLDOUTLIB']);
    $D1OLDOUTFIL=cmMer($outputfdata[0]['D1OLDOUTFIL']);
    $D1OUTR=cmMer($outputfdata[0]['D1OUTR']);
    $D1OUTA=cmMer($outputfdata[0]['D1OUTA']);
	
	// Search
	e_log("D1OUTA");
	e_log("\n".print_r($outputfdata,true)."\n");
	e_log("D1OUTA");
	//
	
    //TMPテーブルあるかどうかチェック
    if ($rtn === 0) {
        if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
          $systables = cmGetSystables($db2con, SAVE_DB, $filename);
        }else{
          $systables = cmGetSystables($db2RDBOutputFileCon,SAVE_DB, $filename);
        }
        if (count($systables)<=0){
            $msg = showMsg('FAIL_MENU_DEL', array(
            '出力ファイルの実行中のデータ'
            ));
            $rtn = 1;
        }
        /*if (count($systables) > 0) {
            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                $resIns = cmInsWTBL($db2con, $filename);
            }else{
                $resIns = cmInsWTBL($db2con, $filename,$db2RDBOutputFileCon);
            }
            if ($resIns !== 0) {
                 // 修正用
                 $msg = showMsg('FAIL_MENU_DEL', array(
                    '出力ファイルの実行中のデータ'
                 )); //'実行中のデータが削除されました。</br>再度メニューから実行するか、検索し直してください。';
                 $rtn = 1;
            }
        }else {
            $msg = showMsg('FAIL_MENU_DEL', array(
            '出力ファイルの実行中のデータ'
            ));
            $rtn = 1;
        }*/
    }
    //出力ファイルのため
    if($rtn===0){
        if($D1OUTLIB !=='' && $D1OUTFIL !==''){
            $add_flg = ($D1OUTA == '1')?true:false;
            e_log("++++++++++++++++++++".$add_flg);
            e_log('出力ファイルワインドウ動く');
            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                $r=createOutputFileTable($db2con,$filename,$D1OUTLIB,$D1OUTFIL,$D1NAME,$add_flg);
            }else{
                $r=createOutputFileTable($db2RDBOutputFileCon,$filename,$D1OUTLIB,$D1OUTFIL,$D1NAME,$add_flg);
            }
            e_log(print_r($r,true));
            if ($r['RTN'] !== 0) {
                $rtn = 1;
            }
            $msg = showMsg($r['MSG']);
        }
    }
}
//出力ファイルのためTB作成  $RDBNM
if (($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) === false) {
    cmDb2Close($db2RDBOutputFileCon);
}

cmDb2Close($db2con);

/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo (json_encode($rtnArray));


// 【出力ファイルの更新】FDB2CSV1
function fnUpdOUTFILEFDB2CSV1($db2con,$fdb2csv1){
    $strSQL = '';
    $data   = array();
   
    $strSQL .= ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= '    D1OUTLIB    = ? , ';
    $strSQL .= '    D1OUTFIL    = ?  ';
    $strSQL .= ' WHERE  ';
    $strSQL .= '    D1NAME      = ? ';
    
    $params =array(
                strtoupper($fdb2csv1['D1OUTLIB']),
                strtoupper($fdb2csv1['D1OUTFIL']),
                $fdb2csv1['D1NAME']
             );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false ){
        $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
    }else{
        $res = db2_execute($stmt,$params);
        if($res === false){
            $data = array(
                    'result' => 'FAIL_UPD',
                    'errcd'  => 'FDB2CSV1:'.db2_stmt_errormsg()
                );
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}