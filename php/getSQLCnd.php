<?php
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
function fnGetBSQLCND($db2con,$qrynm){
    $data = array();

    $strSQL .= ' SELECT *';
    $strSQL .= ' FROM ';
    $strSQL .= '     BSQLCND A ';
    $strSQL .= ' WHERE ';
    $strSQL .= '    CNQRYN = ? ';
    $strSQL .= ' ORDER BY ';
    $strSQL .= '     CNDSEQ ';
    $params = array(
        $qrynm
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        //e_log('条件取得：'.$strSQL.print_r($params,true));
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => umEx($data)
            );
        }
    }
    return $data;
}

function fnBSQLHCND($db2con,$QRYNM){
    $data = array();
    $params = array();   
    $strSQL .= ' SELECT * ';
    $strSQL .= ' FROM ';
    $strSQL .= '     BSQLCND A ';
    $strSQL .= ' LEFT JOIN  ';
    $strSQL .= '     DB2HCND B ';
    $strSQL .= ' ON A.CNQRYN = B.DHNAME ';
    $strSQL .= ' AND A.CNDSEQ = B.DHMSEQ ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.CNQRYN = ? ';
    $strSQL .= ' ORDER BY ';
    $strSQL .= '     A.CNDSEQ ';

    $params = array($QRYNM);
	error_log('fnBSQLHCND = '.$strSQL.' params = '.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    //e_log('$DataKSK2=>'.print_r( $data));
    return $data;
}

