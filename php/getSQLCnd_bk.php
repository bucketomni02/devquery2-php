<?php
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
function fnGetBSQLCND($db2con,$qrynm){
    $data = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     CNQRYN, ';
    $strSQL .= '     CNDSEQ, ';
    $strSQL .= '     CNDKBN, ';
    $strSQL .= '     CNDNM, ';
    $strSQL .= '     CNDWNM, ';
    $strSQL .= '     CNDDTYP, ';
    $strSQL .= '     CNDSTKB, ';
    $strSQL .= '     CNDDAT, ';
    $strSQL .= '     CNDBCNT, ';
    $strSQL .= '     CNCANL, ';
    $strSQL .= '     CNCANF, ';
    $strSQL .= '     CNCMBR, ';
    $strSQL .= '     CNCANC, ';
    $strSQL .= '     CNCANG, ';
    $strSQL .= '     CNNAMG, ';
    $strSQL .= '     CNNAMC, ';
    $strSQL .= '     CNDFMT, ';
    $strSQL .= '     CNDSFL, ';
    $strSQL .= '     CNDFIN, ';
    $strSQL .= '     CNDFPM, ';
    $strSQL .= '     CNDFDY, ';
    $strSQL .= '     CNDFFL, ';
    $strSQL .= '     CNDSCH ';
    $strSQL .= ' FROM ';
    $strSQL .= '     BSQLCND A ';
    $strSQL .= ' WHERE ';
    $strSQL .= '    CNQRYN = ? ';
    $strSQL .= ' ORDER BY ';
    $strSQL .= '     CNDSEQ ';
    $params = array(
        $qrynm
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        //e_log('条件取得：'.$strSQL.print_r($params,true));
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array(
                 'result' => true
                ,'data'   => umEx($data)
            );
        }
    }
    return $data;
}

function fnBSQLHCND($db2con,$QRYNM){
    $data = array();
    $params = array();
    $strSQL = '';
    $strSQL .= ' SELECT ';
    $strSQL .= '     CNQRYN, ';
    $strSQL .= '     CNDSEQ, ';
    $strSQL .= '     CNDKBN, ';
    $strSQL .= '     CNDNM, ';
    $strSQL .= '     CNDWNM, ';
    $strSQL .= '     CNDDTYP, ';
    $strSQL .= '     CNDSTKB, ';
    $strSQL .= '     CNDDAT, ';
    $strSQL .= '     CNDBCNT, ';
    $strSQL .= '     CNCANL, ';
    $strSQL .= '     CNCANF, ';
    $strSQL .= '     CNCMBR, ';
    $strSQL .= '     CNCANC, ';
    $strSQL .= '     CNCANG, ';
    $strSQL .= '     CNNAMG, ';
    $strSQL .= '     CNNAMC, ';
    $strSQL .= '     CNDFMT, ';
    $strSQL .= '     CNDSFL, ';
    $strSQL .= '     CNDFIN, ';
    $strSQL .= '     CNDFPM, ';
    $strSQL .= '     CNDFDY, ';
    $strSQL .= '     CNDFFL, ';
    $strSQL .= '     DHINFO, ';
    $strSQL .= '     DHINFG ';
    $strSQL .= ' FROM ';
    $strSQL .= '     BSQLCND A ';
    $strSQL .= ' LEFT JOIN  ';
    $strSQL .= '     DB2HCND B ';
    $strSQL .= ' ON A.CNQRYN = B.DHNAME ';
    $strSQL .= ' AND A.CNDSEQ = B.DHMSEQ ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.CNQRYN = ? ';
    $strSQL .= ' ORDER BY ';
    $strSQL .= '     A.CNDSEQ ';

    $params = array($QRYNM);
     $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
