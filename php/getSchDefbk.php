<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once('common/inc/config.php');
include_once('common/inc/common.inc.php');
include_once('getQryCnd.php');
include_once('getSQLCnd.php');
include_once('licenseInfo.php');
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$d1name         = (isset($_POST['D1NAME'])? $_POST['D1NAME']:'');
$querydevice    = (isset($_POST['querydevice'])? $_POST['querydevice']:'');
$CSV2ARY        = (isset($_POST['CSV2ARY'])?json_decode($_POST['CSV2ARY'],true):array());
$DRILLFLG       = (isset($_POST['DRILLFLG']) ? $_POST['DRILLFLG'] : '');
$FROMTOOL     = (isset($_POST['FROMTOOL']) ? $_POST['FROMTOOL'] : '0');
$AXESFLG         = (isset($_POST['AXESFLG']) ? $_POST['AXESFLG'] : '');
$isQGflg            = (isset($_POST['ISQGFLG'])?$_POST['ISQGFLG']:false);//クエリーグループならＴＲＵＥ
$subtyp = (isset($_POST['SUBTYP'])?$_POST['SUBTYP']:'');
$pmpkey ='';
$gphkey ='';
if($subtyp==='pivot' || $subtyp==='pgroupsearch' ){
    $pmpkey = (isset($_POST['PMPKEY'])?$_POST['PMPKEY']:'');
}else if($subtyp === 'graph' || $subtyp==='ggroupsearch'){
    $gphkey = (isset($_POST['GPHKEY'])?$_POST['GPHKEY']:'');
}
$SHSDDATA  = array();
$fdb2csv1 = array();
$cnddatArr = '';
$rtn = 0; 
$msg = '';
$totalCount = 0;
// HTMLで表示ためデータ取得
$htmldata = array();
$currentDate = date('Y/m/d');//20161020
$rs = true;
//一番最後のクエリーを知るため
$LASTD1NAME='';
$isQGflg=($isQGflg==='true')?true:false;



/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

if($querydevice !== 'TOUCH'){
    $usrinfo = $_SESSION['PHPQUERY']['user'];
}else{
    $usrinfo = $_SESSION['PHPQUERYTOUCH']['user'];
}
//flg to set session data(axes user and password)
if($AXESFLG === '1'){
    $user = (isset($_POST['AXES_DRILL_USER']) ? $_POST['AXES_DRILL_USER'] : '');
    $pass = (isset($_POST['AXES_DRILL_PASS']) ? $_POST['AXES_DRILL_PASS'] : '');
    if($rtn !== 1){
        $usrinfo = chkDB2WUSR($db2con,$user,$pass);
        $usrinfo = umEx($usrinfo);
        if(count($usrinfo) > 0){
            $_SESSION['PHPQUERY']['user'] = $usrinfo;
        }else{
            $rtn = 1;
            //IDまたはパスワードが正しくありません。;
            $msg =showMsg('FAIL_LGN');
        }
    }
}
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmCheckUser($db2con,$usrinfo[0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

if($rtn===0){
    if($isQGflg){
        $rs=cmGetQueryGroup($db2con,$d1name,$pmpkey,$gphkey);
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリー'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
        }
    }
}
if($rtn === 0){
    if($isQGflg){//クエリーグループ
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            if($res['LASTFLG']==='1'){
                $LASTD1NAME=$res['QRYGSQRY'];
            }
            $chkQry = cmChkQueryGroup($db2con,$usrinfo[0]['WUUID'],$d1name,$res['QRYGSQRY'],$res['LASTFLG'],'','');
            if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリーグループ'));
                break;
            }
        }
    }else{
        $chkQry = cmChkQuery($db2con,$usrinfo[0]['WUUID'],$d1name,'');
        if($chkQry['result'] !== true){
            $rtn = 2;
            $msg = showMsg($chkQry['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    if($DRILLFLG == '1'){
        if($licenseDrilldown !== true){
            if($FROMTOOL === '1'){
                // ツールバーからならダッシュボートへ戻る
                $rtn = 2;
            }else{
                $rtn = 1;
            }
            // クエリー連携は使用できません。
            $msg = showMsg('FAIL_NUSE',array('クエリー連携'));
        }else{
            if ($rtn === 0) {
                if($isQGflg){
                    if($LASTD1NAME!==''){
                        $rs = fnGetDB2HTMLT($db2con, $LASTD1NAME);
                    }
                }else{
                    $rs = fnGetDB2HTMLT($db2con, $d1name);
                }
                if ($rs['result'] === true) {
                    $htmldata = umEx($rs['data']);
                } else {
                    $rtn = 1;
                    // 'クエリーの実行に失敗しました。';
                    $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));
                }
            }
        }
    }

}

/********************実行形式表示開始****************************/

if($rtn === 0){
    //クエリーデータ取得
    if($isQGflg){//クエリーグループ
        $LASTFLG=0;
        foreach($D1NAMELIST as $key=> $res){
            $rs = getFDB2CSV1Data($db2con,$res['QRYGSQRY']);
            if($rs['result'] !== true){
                $msg = showMsg($rs['result'],array('クエリー'));
                $rtn = 1;
            }else{
                $resultList = $rs['data'][0];
                $resultList['RDOFLG'] = 'STANDARD';
                $resultList['SEIGYOFLG']=0;//制御あるかどうかのため
                $resultList['DCSUMF']='';//合計のため
                $resultList['LASTFLG']=$res['LASTFLG'];//一番最後のクエリー
                $fdb2csv1[$res['QRYGSQRY']][]=$resultList;
                if($res['LASTFLG']==='1'){
                    $D1CFLG = $resultList['D1CFLG'];
                    $D1WEBF = $resultList['D1WEBF'];
                }
            }
        }
    }else{
        $rs = getFDB2CSV1Data($db2con,$d1name);
        if($rs['result'] !== true){
            $msg = showMsg($rs['result'],array('クエリー'));
            $rtn = 1;
        }else{
            $fdb2csv1 = $rs['data'];
            $D1WEBF = $fdb2csv1[0]['D1WEBF'];
            $fdb2csv1[0]['RDOFLG'] = 'STANDARD';
            // SQL文で作成したクエリチェックフラグ取得
            $D1CFLG = $fdb2csv1[0]['D1CFLG'];
            //制御合計があるかどうかのチェック
            $fdb2csv1[0]['DCSUMF']='';//合計のため
            $fdb2csv1[0]['SEIGYOFLG']=0;//制御あるかどうかのため
            $fdb2csv1[0]['LASTFLG']=1;
        }
    }
}
// htmlテンプレート
if($rtn === 0){
    if($isQGflg){//クエリーグループ
        if($LASTD1NAME!==''){
            $rshtml = getDB2HTMLT($db2con,$LASTD1NAME);
            if($rshtml['result'] !== true){
                $msg = showMsg($rshtml['result']);
                $rtn = 1;
                break;
            }else{
                if(count($rshtml['data']) > 0){
                    $fdb2csv1[$LASTD1NAME][0]['RDOFLG'] = 'CUSTOM';
                }
            }
        }
    }else{
        $rshtml = getDB2HTMLT($db2con,$d1name);
        if($rshtml['result'] !== true){
            $msg = showMsg($rshtml['result']);
            $rtn = 1;
        }else{
            if(count($rshtml['data']) > 0){
                $fdb2csv1[0]['RDOFLG'] = 'CUSTOM';
            }
        }
    }
}
//制御取得のチェック
if($rtn === 0){
    if($isQGflg){//クエリーグループ
        if($LASTD1NAME!==''){
            $rsSeigyoData = cmGetSeigyoData($db2con, $LASTD1NAME);
            if ($rsSeigyoData['RESULT'] === 1) {
               $msg = showMsg($rsmaster['result']);
               $rtn = 1;
            } else {
               $rsMasterArr = $rsSeigyoData['RSMASTER'];
               $rsshukeiArr = $rsSeigyoData['RSSHUKEI'];
               $fdb2csv1[$LASTD1NAME][0]['SEIGYOFLG']   = $rsSeigyoData['SEIGYOFLG'];
               $fdb2csv1[$LASTD1NAME][0]['DCSUMF']=cmMer($rsMasterArr[0]['DCSUMF']);
               if($fdb2csv1[$LASTD1NAME][0]['RDOFLG'] === 'STANDARD'){
                   if($rsSeigyoData['SEIGYOFLG'] == 1){
                       $fdb2csv1[$LASTD1NAME][0]['RDOFLG'] = 'CUSTOM';
                   }
               }
            }
        }
    }else{
        $rsSeigyoData = cmGetSeigyoData($db2con, $d1name);
        if ($rsSeigyoData['RESULT'] === 1) {
           $msg = showMsg($rsmaster['result']);
           $rtn = 1;
        } else {
           $rsMasterArr = $rsSeigyoData['RSMASTER'];
           $rsshukeiArr = $rsSeigyoData['RSSHUKEI'];
           $fdb2csv1[0]['SEIGYOFLG']   = $rsSeigyoData['SEIGYOFLG'];
           $fdb2csv1[0]['DCSUMF']=cmMer($rsMasterArr[0]['DCSUMF']);
           if($fdb2csv1[0]['RDOFLG'] === 'STANDARD'){
               if($rsSeigyoData['SEIGYOFLG'] == 1){
                   $fdb2csv1[0]['RDOFLG'] = 'CUSTOM';
               }
           }
        }
    }
}
//実行中CLチェック
if($rtn === 0){
    if($isQGflg){//クエリーグループ
        if($LASTD1NAME!==''){
            if($fdb2csv1[$LASTD1NAME][0]['RDOFLG'] === 'STANDARD'){
                $DB2CSV1PG = array();
                $rs = fnDB2CSV1PG($db2con,$LASTD1NAME);
                if($rs['result'] === true){
                    $DB2CSV1PG = umEx($rs['data']);
                    $DGCLIG = '';
                    $DGCPGM = '';
                    if(count($DB2CSV1PG) > 0){
                        //CL実行がある
                        $DGCLIB = $DB2CSV1PG[0]['DGCLIB'];
                        $DGCPGM = $DB2CSV1PG[0]['DGCPGM'];
                        if($DGCLIB !== '' &&  $DGCPGM !== ''){
                            //CL実行がある
                            $fdb2csv1[$LASTD1NAME][0]['RDOFLG'] = 'CUSTOM';
                        }
                    }
                }else{
                    $rtn = 1;
                    $msg =$rs['result'] ;
                }
            }
        }
    }else{
        if($fdb2csv1[0]['RDOFLG'] === 'STANDARD'){
            $DB2CSV1PG = array();
            $rs = fnDB2CSV1PG($db2con,$d1name);
            if($rs['result'] === true){
                $DB2CSV1PG = umEx($rs['data']);
                $DGCLIG = '';
                $DGCPGM = '';
                if(count($DB2CSV1PG) > 0){
                    //CL実行がある
                    $DGCLIB = $DB2CSV1PG[0]['DGCLIB'];
                    $DGCPGM = $DB2CSV1PG[0]['DGCPGM'];
                    if($DGCLIB !== '' &&  $DGCPGM !== ''){
                        //CL実行がある
                        $fdb2csv1[0]['RDOFLG'] = 'CUSTOM';
                    }
                }
            }else{
                $rtn = 1;
                $msg =$rs['result'] ;
            }
        }
    }
}
//詳細又Drilldownチェック
if($rtn === 0){
    if($isQGflg){//クエリーグループ
        if($LASTD1NAME!==''){
            if($fdb2csv1[$LASTD1NAME][0]['RDOFLG'] === 'STANDARD'){
                $raas = fnDB2WDTLGRS($db2con,$LASTD1NAME);
                if($raas['result'] !== true){
                    $rtn = 1;
                    $msg =showMsg($raas['result']) ;
                }else{
                    $DB2WDTL = $raas['data']['TOTALCOUNT'];
                    if((int)$DB2WDTL > 0){
                        //クエリー詳細データがある
                       $fdb2csv1[$LASTD1NAME][0]['RDOFLG'] = 'CUSTOM';
                    }
                }
            }
        }
    }else{
        if($fdb2csv1[0]['RDOFLG'] === 'STANDARD'){
            $raas = fnDB2WDTLGRS($db2con,$d1name);
            if($raas['result'] !== true){
                $rtn = 1;
                $msg =showMsg($raas['result']) ;
            }else{
                $DB2WDTL = $raas['data']['TOTALCOUNT'];
                if((int)$DB2WDTL > 0){
                    //クエリー詳細データがある
                   $fdb2csv1[0]['RDOFLG'] = 'CUSTOM';
                }
            }
        }
    }
}

/********************実行形式表示終了****************************/
if($isQGflg){//くえりーグループ
    $FDB2CSV3=array();
    foreach($D1NAMELIST as $keyIndex => $result){
        $inData=array();
        $inData=$fdb2csv1[$result['QRYGSQRY']][0];
        if($inData['D1WEBF'] === ''){
            if($rtn === 0){
                $fdb3=umEx(cmGetFDB2CSV3($db2con,$result['QRYGSQRY']));//rs はなんで必要か？
                $FDB2CSV3[$result['QRYGSQRY']][] = $fdb3;
                if(count($CSV2ARY)>0){
                    if($FDB2CSV3[$result['QRYGSQRY']][0] != $CSV2ARY[$result['QRYGSQRY']][0]){
                        $rtn = 2;
                        //クエリーの検索条件が変更されています。再度メニューから実行し直してください;
                        $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));
                        break;
                    }
                }
            }
        }else{
            if($inData['D1CFLG'] === ''){
                if($rtn === 0){
                    $res = fnGetBQRYCND($db2con,$result['QRYGSQRY']);
                    if($res['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                    }else{
                        $fdb3=umEx($res['data']);
                        $FDB2CSV3[$result['QRYGSQRY']][] = $fdb3;
                    }
                   if(count($CSV2ARY)>0){
                        if($FDB2CSV3[$result['QRYGSQRY']][0] != $CSV2ARY[$result['QRYGSQRY']][0]){
                            $rtn = 2;
                            //クエリーの検索条件が変更されています。再度メニューから実行し直してください;
                            $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));
                            break;
                        }
                    }
                }
                if($rtn === 0){
                    $rs = fnBQRYCNDDAT($db2con,$result['QRYGSQRY']);
                    if($rs['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rs['result']);
                    }else{
                        $data = $rs['data'];
                    }
                    $data = umEx($data);
                    $cnd=array();
                    if(count($data) > 0){
                        $cnd= fnCreateCndData($data);
                    }
                    //$cnddatArr[$result['QRYGSQRY']][] =$cnd;
                        $QRYGSQRY = $result['QRYGSQRY'];
                        $cnddatArr[$QRYGSQRY][]=$cnd;
                }
            }else{
                if($rtn === 0){
                    $res = fnGetBSQLCND($db2con,$result['QRYGSQRY']);
                    if($res['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                        break;
                    }else{
                        $fdb3=umEx($res['data']);
                        $FDB2CSV3[$result['QRYGSQRY']][]=$fdb3;
                    }                    
                    if(count($CSV2ARY)>0){
                        if($FDB2CSV3[$result['QRYGSQRY']][0] != $CSV2ARY[$result['QRYGSQRY']][0]){
                            $rtn = 2;
                            //クエリーの検索条件が変更されています。再度メニューから実行し直してください;
                            $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));
                            break;
                        }
                    }
                }
                if($rtn === 0){
                    $rs = fnBSQLHCND($db2con,$result['QRYGSQRY']);
                    if($rs['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rs['result']);
                        break;
                    }else{
                        $data = $rs['data'];
                        if(count($data) > 0){
                            // 条件に関数のデータあるかのチェックチェック
                            foreach($data as $k => $dCnd){
                                if($dCnd['CNDDTYP'] === '6'){
                                    $resFuncData = getFuncdata($db2con,$dCnd['CNDDAT']);
                                    $data[$k]['CNDFUNC'] = $data[$k]['CNDDAT'];
                                    $data[$k]['CNDDAT'] = $resFuncData;
                                }
                            }
                        }
                        $QRYGSQRY = $result['QRYGSQRY'];
                        $cnddatArr[$QRYGSQRY][]=$data;
                    }
                }
            }
            $rs = fnGetDB2SHSD($db2con,$usrinfo[0]['WUUID'],$d1name,$QRYGSQRY,$pmpkey,$gphkey);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $SHSDDATA[$QRYGSQRY] = $rs['SHSDDATA'];
            }
        }
    }
    $data = $FDB2CSV3;
}else{
    if($D1WEBF === ''){
        if($rtn === 0){
            $FDB2CSV3 = cmGetFDB2CSV3($db2con,$d1name);//rs はなんで必要か？
            if(count($CSV2ARY)>0){
                if($FDB2CSV3 != $CSV2ARY){
                    $rtn = 2;
                    //クエリーの検索条件が変更されています。再度メニューから実行し直してください;
                    $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));
                }
            }
        }
    }else{
        if($D1CFLG === ''){
            if($rtn === 0){
                $res = fnGetBQRYCND($db2con,$d1name);
                if($res['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($res['result']);
                }else{
                    $FDB2CSV3 = $res['data'];
                }
                if(count($CSV2ARY)>0){
                    if($FDB2CSV3 != $CSV2ARY){
                        $rtn = 2;
                        //クエリーの検索条件が変更されています。再度メニューから実行し直してください;
                        $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));
                    }
                }
            }
            if($rtn === 0){
                $rs = fnBQRYCNDDAT($db2con,$d1name);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }else{
                    $data = $rs['data'];
                }
                $data = umEx($data);
                if(count($data) > 0){
                    $cnddatArr = fnCreateCndData($data);
                }
                $rs = fnGetDB2SHSD($db2con,$usrinfo[0]['WUUID'],'',$d1name,$pmpkey,$gphkey);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }else{
                    $SHSDDATA[$d1name] = $rs['SHSDDATA'];
                    if(count($rs['SHSDDATA']) > 0 && count($data) > 0){
                        $cnddatArr = fnReplaceSchDtaWeb($cnddatArr,$rs['SHSDDATA']);
                    }
                }
            }
        }else{
            if($rtn === 0){
                $res = fnGetBSQLCND($db2con,$d1name);
                if($res['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($res['result']);
                }else{
                    $FDB2CSV3 = $res['data'];
                }
                if(count($CSV2ARY)>0){
                    if($FDB2CSV3 != $CSV2ARY){
                        $rtn = 2;
                        //クエリーの検索条件が変更されています。再度メニューから実行し直してください;
                        $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));
                    }
                }
            }
            if($rtn === 0){
                $rs = fnBSQLHCND($db2con,$d1name);
                $abc = $rs;
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }else{
                    $data = $rs['data'];
                    if(count($data) > 0){
                        // 条件に関数のデータあるかのチェックチェック
                        foreach($data as $k => $dCnd){
                            if($dCnd['CNDDTYP'] === '6'){
                                $resFuncData = getFuncdata($db2con,$dCnd['CNDDAT']);
                                $data[$k]['CNDFUNC'] = $data[$k]['CNDDAT'];
                                $data[$k]['CNDDAT'] = $resFuncData;
                            }
                        }
                        $cnddatArr = $data;
                        $rs = fnGetDB2SHSD($db2con,$usrinfo[0]['WUUID'],'',$d1name,$pmpkey,$gphkey);
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = showMsg($rs['result']);
                        }else{
                            $SHSDDATA[$d1name] = $rs['SHSDDATA'];
                            if(count($SHSDDATA) > 0){
                                $cnddatArr = fnReplaceSchDtaSQL($cnddatArr,$SHSDDATA);
                            }
                        }
                    }
                }
            }
        }
    }
    $data = umEx($FDB2CSV3);

}

// 検索条件のヘルプ情報取得
$DB2HCND = array();
if($rtn === 0){
    if($isQGflg){//クエリーグループ
        if(count($data) >0 ){
            foreach($D1NAMELIST as $key =>$res){
                $rsHCND = fnGetDB2HCND($db2con,$result['QRYGSQRY']);
                if($rsHCND['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rsHCND['result']);
                }else{
                    $result=umEx($rsHCND['data']);
                    $DB2HCND[$res['QRYGSQRY']][] =$result; 
                }
            }
        }
    }else{
        if(count($data) >0 ){
            $rsHCND = fnGetDB2HCND($db2con,$d1name);
            if($rsHCND['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rsHCND['result']);
            }else{
                $DB2HCND = umEx($rsHCND['data']);
            }
        }
    }
}
//検索条件とヘルプ情報取得
$FDB2CSV3HCND = array();
if($rtn === 0){
    if($isQGflg){
        foreach($D1NAMELIST as $res){
            $inData=array();
            $inData=$fdb2csv1[$res['QRYGSQRY']][0];
            if($inData['D1WEBF'] !== '1'){
                if(count($data) >0 ){
                    $rsCND = fnGetFDB2CSV3HCND($db2con,$res['QRYGSQRY']);
                    if($rsCND['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rsCND['result']);
                    }else{
                        $FDB2CSV3HCND[$res['QRYGSQRY']][] = umEx($rsCND['data']);
                    }
                }
            }
        }
    }else{
        if($D1WEBF !== '1'){
            if(count($data) >0 ){
                $rsCND = fnGetFDB2CSV3HCND($db2con,$d1name);
                if($rsCND['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rsCND['result']);
                }else{
                    $FDB2CSV3HCND = umEx($rsCND['data']);
                }
            }
        }
    }
}

cmDb2Close($db2con);
$ary = array(
    'data'    => $FDB2CSV3,
    'csvoldAry' =>$FDB2CSV3,
    'cndData' => $cnddatArr,
    'RTN'     => $rtn,
    'MSG'     => $msg,
    'usrinfo' => $usrinfo,
    'currentDate' =>$currentDate,
    'FDB2CSV1' =>$fdb2csv1,     
    'DB2WDTL' =>$DB2WDTL,
    'DB2HCND' => $DB2HCND,
    'FDB2CSV3HCND'=>$FDB2CSV3HCND,
    'D1WEBF' =>$D1WEBF,
    'SERACHKEYARR' =>$SEARCHKEYARR,
    'HTMLDATA' => $htmldata,
    'D1CFLG' => $D1CFLG,
    'BASE_URL' => BASE_URL,
    'D1NAMELIST'=>$D1NAMELIST,
    'SHSDDATA' => $SHSDDATA
);
echo(json_encode($ary));


function fnReplaceSchDtaSQL($sqlData,$SHSDDATA){
    $shsd = array();
    foreach($SHSDDATA as $key => $value){
       $idx3 = $value['SHSDSQ3'];
       $shsd[$idx3] = $value['SHSDDTA'];
    }
    foreach($sqlData as $key => $value){
            $cndseq = $value['CNDSEQ'];
            $sqlData[$key]['CNDDAT'] = $shsd[$cndseq];
    }
    return $sqlData;
}

function fnReplaceSchDtaWeb($webData,$SHSDDATA){
    $shsd = array();
    foreach($SHSDDATA as $key => $value){
       $idx1 = $value['SHSDSQ1'];
       $idx2 = $value['SHSDSQ2'];
       $idx3 = $value['SHSDSQ3'];
       $shsd[$idx1][$idx2][$idx3] = $value['SHSDDTA'];
    }
    for ($idx = 0; $idx < count($webData); $idx++) {
        $cdnsdata = $webData[$idx]['CNDSDATA'];
        $CNDIDX =  $webData[$idx]['CNDIDX'];
        for ($idxd = 0; $idxd < count($cdnsdata); $idxd++) {
            $cdata = $cdnsdata[$idxd];
            $CDATAIDX =  $cdnsdata[$idxd]['CDATAIDX'];
            if ($cdata['CNDKBN'] === "RANGE") {
                $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][0] = $shsd[$CNDIDX][$CDATAIDX][1];
                $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][1] = $shsd[$CNDIDX][$CDATAIDX][2];
           } else if ($cdata['CNDKBN'] === "LIST" || $cdata['CNDKBN'] === "NLIST") {
                $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'] = array();
                for ($idL = 0; $idL < count($cdata['CNDDATA']); $idL++) {
                    $index = $idL + 1;
                    if(isset($shsd[$CNDIDX][$CDATAIDX][$index])){
                        array_push($webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'],$shsd[$CNDIDX][$CDATAIDX][$index]);
                    }else{
                        unset($webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][$idL]);
                    }
                }
            } else {
                $webData[$idx]['CNDSDATA'][$idxd]['CNDDATA'][0] = $shsd[$CNDIDX][$CDATAIDX][1];
            }
        }
    } //for idx end
    return $webData;
}

//保存された検索データー
function fnGetDB2SHSD($db2con,$UID,$QRG,$QID,$QPV,$QGP){
    $data = array();
    $params = array();

    $strSQL = ' SELECT * FROM DB2SHSD WHERE ';
    $strSQL .= ' SHSDUID = ? AND SHSDQRG = ? AND SHSDQID = ? AND SHSDQPV = ? AND SHSDQGP = ? ';
    $params = array($UID,$QRG,$QID,$QPV,$QGP);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $SHSDDATA = array();
            while($row = db2_fetch_assoc($stmt)){
                $SHSDDATA[] = $row;
            }
            $data = array('result' => true,'SHSDDATA' => umEx($SHSDDATA,true));
        }
    }
   return $data;
}
function getFuncdata ($db2con,$func){
    $rtn = $func;
    $data = array();
    if($func !== ''){
        $strSQL .= ' SELECT ';
        $strSQL .= $func.' AS RES ';
        $strSQL .= ' FROM ';
        $strSQL .= '     SYSIBM.SYSDUMMY1 ';

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            e_log('関数取得エラー①'.$strSQL . db2_stmt_errormsg());
        }else{
            $r = db2_execute($stmt);
            if($r === false){
                e_log('関数取得エラー②'.$strSQL . db2_stmt_errormsg());
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = umEx($data);
                $rtn = $data[0]['RES'];
            }
        }
    }
    return $rtn ;
}
// FDB2CSV1のデータ取得
function getFDB2CSV1Data($db2con,$D1NAME){

    $data = array();
    $params = array();

    $strSQL .= ' SELECT ';
    $strSQL .= '     D1TEXT, ';
    $strSQL .= '     D1DISF, ';
    $strSQL .= '     DISCSN, ';
    $strSQL .= '     D1INFO, ';
    $strSQL .= '     D1INFG, ';
    $strSQL .= '     D1WEBF, ';
    $strSQL .= '     D1CFLG ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     D1NAME = ? ';

    $params = array(
        $D1NAME
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET'); 
            }else{
               $data = array('result' => true,'data' => umEx($data,true));
            }
        }
    }
    return $data;
}

//CL中があるかどうかのチェック
function fnDB2CSV1PG($db2con,$d1name){
    $rs = array();
    //FDB2CSV1PGの取得
    $strSQL .= ' SELECT ';
    $strSQL .= '     A.DGNAME, ';
    $strSQL .= '     A.DGCLIB, ';
    $strSQL .= '     A.DGCPGM, ';
    $strSQL .= '     A.DGCBNM, ';
    $strSQL .= '     B.D1LIBL ';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1PG AS A ';
    $strSQL .= ' JOIN FDB2CSV1 AS B ';
    $strSQL .= ' ON A.DGNAME = B.D1NAME ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.DGNAME = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false){
                $rs = array('result' => 'FAIL_SEL');
    }else{
        $params = array(
            $d1name
        );
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $fdb2csv1pg[] = $row;
            }
            $fdb2csv1pg = umEx($fdb2csv1pg);
            $rs = array('result' => true,'data' =>$fdb2csv1pg);
        }
    }
    return $rs;
}
//詳細データが入ってるかどうかのチェック
function fnDB2WDTLGRS($db2con,$d1name){
    $rs = array();
    $DB2WDTL = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '     COUNT(*) AS TOTALCOUNT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     (SELECT DISTINCT ';
    $strSQL .= '         DTNAME ';
    $strSQL .= '     FROM ';
    $strSQL .= '         DB2WDTL ';
    $strSQL .= ' UNION '; 
    $strSQL .= ' SELECT DISTINCT ';
    $strSQL .= '     DRKMTN AS DTNAME ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2DRGS ';
    $strSQL .= '     ) AS A ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.DTNAME = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false){
        $rs = array('result' => 'FAIL_SEL');
    }else{
        $params = array(
            $d1name
        );
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $DB2WDTL = $row;
              }
            $rs = array('result' => true,'data' =>$DB2WDTL);
       }
    }
    return $rs;
}
// 検索条件のヘルプ情報取得
function fnGetDB2HCND($db2con,$d1name){
    $strSQL = '';
    $data = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '     DHNAME , ' ;
    $strSQL .= '     DHFILID , ' ;
    $strSQL .= '     DHMSEQ , ' ;
    $strSQL .= '     DHSSEQ , ' ;
    $strSQL .= '     DHINFO , ' ;
    $strSQL .= '     DHINFG ' ;
    $strSQL .= ' FROM DB2HCND'; 
    $strSQL .= ' WHERE DHNAME = ? ';
    $params = array(
                $d1name 
            );
    //e_log('SQL文：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true , 'data' => $data );
        }
    }
    return $data;
}


/*
*-------------------------------------------------------* 
* ログインチェック
*-------------------------------------------------------*
*/

function chkDB2WUSR($db2con,$user,$pass){

    $data = array();

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $data = false;
    }else{        
        $r = db2_execute($stmt);
        if($r === false){
            $data = false;
        }else{
            $strSQL .= '    SELECT ';
            $strSQL .= '        A.WUUID, ';
            $strSQL .= '        A.WUUNAM, ';
            $strSQL .= '        A.WUAUTH, ';
            $strSQL .= '        A.WUDWNL, ';
            $strSQL .= '        A.WUDWN2, ';
            $strSQL .= '        A.WUSIZE ';
            $strSQL .= '    FROM ';
            $strSQL .= '        DB2WUSR AS A ';
            $strSQL .= '    WHERE ';
            $strSQL .= '        WUUID = ? AND ';
            $strSQL .= '        DECRYPT_CHAR(WUPSWE) = ? ';
            $params = array(
                $user,
                $pass
            );

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $data = false;
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = false;
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                }
            }
        }
    }
    return $data;
}

// DB2HTMLT取得
function getDB2HTMLT($db2con,$D1NAME){
    $data = array();
    $params = array();
    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM DB2HTMLT ';
    $strSQL .= ' WHERE HTMLTD = ?';

    $params = array(
            $D1NAME
        );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' =>umEx($data));
        }
    }
    return $data;
}