<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/
$d1name = $_POST['D1NAME'];
$tblName = $_POST['tblName'];
$D1CFLG = (isset($_POST['D1CFLG'])?$_POST['D1CFLG']:'');//SQLクエリーのFLG
$iDisplayStart = $_POST['iDisplayStart'];
$iDisplayLength = $_POST['iDisplayLength'];
$iSortCol_0 = $_POST['iSortCol_0'];
$sSortDir_0 = $_POST['sSortDir_0'];
$sSearch = $_POST['sSearch'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$csv_d = array();
$msg = '';
$valueArr = array();
$tmpFDB2CSV2 = array();
$D1TMPF = '';//エクセルテンプレート
/*
*-------------------------------------------------------* 
* データ取得処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmCheckUser($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$d1name,'','');
    if($chkQry['result'] !== true){
        $rtn = 2;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($rtn === 0){
    $fdb2csv1Data = fnGetFDB2CSV1($db2con,$d1name);
    $fdb2csv1Data = umEx($fdb2csv1Data);
    if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME) === false){
        $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1Data[0]['D1RDB'];
        $db2conRdb = cmDB2ConRDB($fdb2csv1Data[0]['D1LIBL']);
    }
    if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME)){
        $systables = cmGetSystables($db2con,SAVE_DB,$tblName);
    }else{
        $systables = cmGetSystables($db2conRdb,SAVE_DB,$tblName);
    }
    if(count($systables) > 0){
        //$name = fnGetName($db2con,$tblName);
        $fdb2csv2 = cmGetFDB2CSV2($db2con,$d1name,false,false,false);
        $fdb2csv2 = $fdb2csv2['data'];
        
        if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME)){
            //FDB2CSV2のカラムが'DEVQUERYH'のテーブルにない場合消す
            $tmpFDB2CSV2 = cmColNameDiff($db2con,$fdb2csv2,$tblName,$D1CFLG);
            $allcount = cmGetAllCount($db2con,$tmpFDB2CSV2,$tblName,$sSearch,$D1CFLG,array());
        }else{
            //FDB2CSV2のカラムが'DEVQUERYH'のテーブルにない場合消す
            $tmpFDB2CSV2 = cmColNameDiff($db2conRdb,$fdb2csv2,$tblName,$D1CFLG);
            $allcount = cmGetAllCount($db2conRdb,$tmpFDB2CSV2,$tblName,$sSearch,$D1CFLG,array());
        }
        if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME) === false){
            cmDb2Close($db2conRdb);
        }
    }
}
//テンプレートチェックのデータ取得
if($rtn === 0){
	$fdb2csv1 = FDB2CSV1_DATA($d1name);
    if ($fdb2csv1['result'] === true) {
        $fdb2csv1 = umEx($fdb2csv1['data']);
		if(count($fdb2csv1) > 0){
	        $D1TMPF = $fdb2csv1[0]['D1TMPF'];
		}
    }else{
        $rtn = 1;
        $msg = showMsg($fdb2csv1['result']);
	}
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'res_iSortCol_0' => $iSortCol_0,
    'res_sSortDir_0' => $sSortDir_0,
    'res_sSearch' => $sSearch,
    'iTotalRecords' => $allcount['data'],
    'iTotalDisplayRecords' => $allcount['data'],
    'BACKZERO_FLG' => BACKZERO_FLG,
    'ZERO_FLG' => ZERO_FLG,
	'D1TMPF' => $D1TMPF,
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* クエリー名を取得
*-------------------------------------------------------*
*/

function fnGetName($db2con,$tblName){

    $data = array();

    $strSQL  = ' SELECT A.WHNAME ';
    $strSQL .= ' FROM DB2WHIS as A ' ;
    $strSQL .= ' WHERE WHOUTF = ? ';

    $params = array($tblName);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = $data[0]['WHNAME'];
        }
    } 
    return $data;

}
function fnGetFDB2CSV1($db2con,$d1name){

    $data = array();

    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM FDB2CSV1 as A ' ;
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array($d1name);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    } 
    return $data;
}

