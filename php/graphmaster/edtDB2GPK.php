<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['PROC'])?$_POST['PROC']:'');
$formValues = json_decode($_POST['formValues'],true);
$listData = json_decode($_POST['listData'],true);

$GPKQRY = (isset($_POST['GPKQRY'])?$_POST['GPKQRY']:'');
if($proc === 'DEL'){
    $GPKID = (isset($_POST['GPKID'])?$_POST['GPKID']:'');
}else{
    $GPKID = (isset($formValues['GPKID'])?$formValues['GPKID']:'');
}
$GPKNM = (isset($formValues['GPKNM'])?$formValues['GPKNM']:'');
$GPKFM = (isset($formValues['GPKFM'])?$formValues['GPKFM']:'');
$GPKXCO = (isset($formValues['GPKXCO'])?$formValues['GPKXCO']:'');
$GPKXID = (isset($formValues['GPKXID'])?$formValues['GPKXID']:'');
$GPKXNM = (isset($formValues['GPKXNM'])?$formValues['GPKXNM']:'');
$GPKYNM = '';
if($GPKFM !== '4'){
    $GPKYNM = (isset($formValues['GPKYNM2'])?$formValues['GPKYNM2']:'');
}else{
    $GPKYNM = (isset($formValues['GPKYNM1'])?$formValues['GPKYNM1']:'');
}
$GPKCFG = (isset($formValues['GPKCFG'])?$formValues['GPKCFG']:'');
$GPKDFG = (isset($formValues['GPKDFG'])?$formValues['GPKDFG']:'');//MSM add for drilldown link 21080925 
$GPJKCHK = (isset($formValues['GPJKCHK'])?$formValues['GPJKCHK']:'');//全部の軸を表示するフラグ（イー）

//htmlspecialcharsをデコード
$GPKQRY = cmHscDe($GPKQRY);
$GPKID = cmHscDe($GPKID);
$GPKNM = cmHscDe($GPKNM);
$GPKFM = cmHscDe($GPKFM);
$GPKXCO = cmHscDe($GPKXCO);
$GPKXID = cmHscDe($GPKXID);
$GPKCFG = cmHscDe($GPKCFG);
$GPKDFG = cmHscDe($GPKDFG);//MSM 21080925 
$GPJKCHK = cmHscDe($GPJKCHK);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID']))?$_SESSION['PHPQUERY']['user'][0]['WUUID']:'';
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$WUUID);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'30',$userData[0]['WUSAUT']);//'1' => グラフマスター
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グラフ作成の権限'));
            }
        }
    }
}
if($rtn === 0){
    $chkQry = cmChkQuery($db2con, '' , $GPKQRY, '','');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$GPKQRY,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのグラフ作成の権限がありません。');
           $msg = showMsg('FAIL_QRY_USR',array('グラフ作成の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
//check and get  RDB connection
if($rtn === 0){
        $fdb2csv1Data = FDB2CSV1_DATA($d1name);
        $RDBNM = cmMer($fdb2csv1Data['data'][0]['D1RDB']);
        if($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME){
                $db2connet =  $db2con;
        }else{
            $_SESSION['PHPQUERY']['RDBNM'] = $RDBNM;
            $db2RDBcon = cmDB2ConRDB(SAVE_DB);
            $db2connet =  $db2RDBcon;
        }
}
if($proc === 'DEL' && $rtn === 0){
    //グラフ存在チェック
        $rs = cmCheckDB2GPK($db2con,$GPKQRY,$GPKID);
        if($rs['rs'] === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET',array('グラフ'));
        }else if($rs['rs'] !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs['rs'],array('グラフ'));
        }else{
            $gpkdata = $rs['data'];
        }
        if($rtn === 0){
            //Ｙ軸にある元のデータを削除
            $rs =  fnDeleteDB2GPK($db2con,$GPKQRY,$GPKID);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('グラフ'));
            }else{
                //Ｙ軸にある元のデータを削除
                $rs =  fnDeleteDB2GPC($db2con,$GPKQRY,$GPKID);
                if($rs !== true){
                    $rtn = 1;
                    $msg = showMsg($rs,array('グラフ'));
                }else{//ブックマークの削除
                    $rs=cmDelDB2BMK($db2con,$GPKID,$GPKQRY,1);
                    if($rs['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rs['result']);
                    }else{
                         //テーブル削除
                            $GPKTBL = cmMer($gpkdata[0]['GPKTBL']);
                            if($GPKTBL !== ''){
                                $rs = cmDropQueryTable($db2connet,$GPKTBL);
                                if($rs['result'] !== true){
                                    $rtn = 1;
                                    $msg = showMsg($rs['result']);
                                 }
                            }
                            if($rtn === 0){
                                //file delete...
                                $textFileName =  BASE_DIR . "/php/graphfile/".$GPKQRY.'-'.$GPKID.'.txt';
                                if (file_exists($textFileName)) {
//                                    unlink($textFileName);
                                       if (unlink($textFileName)) { 
                                          e_log('File deleted');
                                       } else {
                                            $rtn = 1;
                                             $err = error_get_last();
                                              $msg = $err['message'];
                                       }
                                }
                            }
                    }
                }
            }
        }
        if($rtn === 0){
            //お気に入りにグラフを削除
            $rs = cmDeleteDB2QBMK($db2con,'',$GPKQRY,'',$GPKID,'');
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }   
        }
}else{
    if($rtn === 0){
        //必須チェック
        if($GPKID === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('グラフキー'));
            $focus = 'GPKID';
        }
    }
     if($rtn === 0){
        if (preg_match('/[^0-9A-Za-z_#@]+/', $GPKID)) {
            $msg = showMsg('FAIL_KEYENTRY',array('グラフキー'));
            $rtn = 1;
        }
     }
    //バリデーションチェック
    if($rtn === 0){
        if(!checkMaxLen($GPKID,10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('グラフキー'));
            $focus = 'GPKID';
        }
    }
    if($rtn === 0){
        $byte = checkByte($GPKID);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('グラフキー'));
            $focus = 'GPKID';
        }
    }
    if($rtn === 0){
        $GPKNM = cmMer($GPKNM);
        if($GPKNM === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('名称'));
            $focus = 'GPKNM';
        }
    }
    //名称の桁数チェック
    if($rtn === 0){
        if(!checkMaxLen($GPKNM,50)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('名称'));
            $focus = 'GPKNM';
        }
    }
    if($rtn === 0){
        if($GPKXCO  === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_SLECJS',array('横軸'));
            $focus = 'GPKXCO';
        }
    }
/*    if($rtn === 0){
        if($GPKXNM  === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('縦軸名'));
            $focus = 'GPKXNM';
        }
    }*/
    //名称の桁数チェック
    if($rtn === 0){
        if(!checkMaxLen($GPKXNM,50)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('横軸名'));
            $focus = 'GPKXNM';
        }
    }
/*
    if($rtn === 0){
        if($GPKYNM  === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('Ｙ軸名'));
            $focus = 'GPKYNM';
        }
    }*/
    //名称の桁数チェック
    if($rtn === 0){
        if(!checkMaxLen($GPKYNM,50)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('縦軸名'));
            $focus = 'GPKYNM';
        }
    }
    if($rtn === 0){
            //FILD=9999の場合は無条件でarray()
            if ($GPKXID === '9999') {
                $checkRs = array();
            } else {
                $checkRs = cmCheckFDB2CSV2($db2con, $GPKQRY, $GPKXID, $GPKXCO);
            }
            if ($checkRs === false) {
                $rtn = 2;
                //      $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                $msg = showMsg('FAIL_SYS');
            } else {
                if (count($checkRs) === 0) {
                    $checkRs = cmCheckFDB2CSV5($db2con, $GPKQRY, $GPKXCO);
                    if ($checkRs === false) {
                        $rtn = 1;
                        //    $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                        $msg = showMsg('FAIL_SYS');
                    } else {
                        if (count($checkRs) === 0) {
                            $rtn = 1;
                            $msg = showMsg('PIVOT_NO_COLS');//'クエリーに登録されていないカラムが選択されています。クエリーの設定をご確認ください。';
                        }
                    }
                }
            }
    }
    if($rtn === 0){
        if(count($listData) === 0 ){
            $rtn = 1;
            $msg = showMsg('FAIL_SLECJS',array('縦軸'));
        }else{
	     $dups = array();
            foreach($listData as $key =>$value){
                    //FILD=9999の場合は無条件でarray()
                    if ($value['GPCYID'] === '9999') {
                        $checkRs = array();
                    } else {
                        $checkRs = cmCheckFDB2CSV2($db2con, $GPKQRY, $value['GPCYID'], $value['GPCYCO']);
                    }
                    if ($checkRs === false) {
                        $rtn = 1;
                        //      $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                        $msg = showMsg('FAIL_SYS');
                    } else {
                        if (count($checkRs) === 0) {
                            $checkRs = cmCheckFDB2CSV5($db2con, $GPKQRY, $value['GPCYCO'],"");//MSM add "" to fix Missing argument error 20180914
                            if ($checkRs === false) {
                                $rtn = 1;
                                //    $msg = 'プログラムが異常終了しました。管理者にお問い合わせください。';
                                $msg = showMsg('FAIL_SYS');
                                break;
                            } else {
                                if (count($checkRs) === 0) {
                                    $rtn = 1;
                                    $msg = showMsg('PIVOT_NO_COLS');//'クエリーに登録されていないカラムが選択されています。クエリーの設定をご確認ください。';
                                }
                            }
                        }
                    }
                    if($rtn === 0){
                        $gpclbl = cmHscDe($value['GPCLBL']);
                        if($gpclbl !== ''){
                            if(!checkMaxLen($gpclbl,20)){
                                $rtn = 1;
                                $msg = showMsg('FAIL_MAXLEN',array('ラベル'));
                                break;
                            }
                        }
                    }
		    $GPCSOS = $value['GPCSOS'];
		    $GPCSFG = cmMer($value['GPCSFG']);
			if($rtn === 0 && $GPCSOS !== ''){
				if (in_array($GPCSOS, $dups)){
			            $rtn = 1;
			            $msg = showMsg('CHK_DUP', array(
			                'SORT順'
			            ));
				}
			}
			if($rtn === 0){
			    if ($GPCSOS === '' && $GPCSFG !== '') {
			        $rtn = 1;
			        $msg = showMsg('FAIL_REQ',array('SORT順'));
                                break;
			    }
			}
			if($rtn === 0){
			    if ($GPCSOS !== '' && $GPCSFG === '') {
			        $rtn = 1;
			        $msg = showMsg('FAIL_REQ',array('昇順/降順'));
                                break;
			    }
			}
			if($rtn === 0 && $GPCSOS !== ''){
			    if((int)$GPCSOS<=0){
			        $rtn = 1;
			        $msg = showMsg('CHK_FMT',array('SORT順','ゼロ以上'));
                                break;
			    }
			}
			if($rtn === 0 && $GPCSOS !== ''){
			    if(!checkNum($GPCSOS) || strpos($GPCSOS, '.')){
			        $rtn = 1;
			        $msg = showMsg('CHK_FMT',array('SORT順','数値'));
                                break;
			    }
			}
			if($rtn === 0 && $GPCSOS !== ''){
				$GPCSOS = cmHscDe($GPCSOS);
				if($GPCSOS !== ''){
				    if(!checkMaxLen($GPCSOS,5)){
				        $rtn = 1;
				        $msg = showMsg('FAIL_MAXLEN',array('SORT順'));
				        break;
				    }
				}
			}
		       if ($rtn === 0) {
	                 array_push($dups, $GPCSOS);
		       }
            }
        }
    }
  if($rtn === 0){
    if($proc === 'UPD'){
        //グラフ存在チェック
        $rs = cmCheckDB2GPK($db2con,$GPKQRY,$GPKID);
        if($rs['rs'] === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_UPD',array('グラフキー'));
        }else if($rs['rs'] !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs['rs'],array('グラフキー'));
        }
        if($rtn === 0){
             //Ｘ軸のデータを登録
			//TTA add condition for GPKDFG and GPNTFG flag
            if ($GPKDFG == '') {
                $GPNTFG = '';
            }
            $rs = fnUpdateDB2GPK($db2con,$GPKQRY,$GPKID,$GPKNM,$GPKFM,$GPKXCO,$GPKXID,$GPKXNM,$GPKYNM,$GPKCFG,$GPKDFG,$GPJKCHK);//MSM add GPKDFG 20180925
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('グラフ'));
            }  
        }
    }else{
    //グラフ存在チェック
        $rs = cmCheckDB2GPK($db2con,$GPKQRY,$GPKID);
        if($rs['rs'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['rs'],array('グラフキー'));
        }
        if($rtn === 0){
             //Ｘ軸のデータを登録
			//TTA add condition for GPKDFG and GPNTFG flag
            if ($GPKDFG == '') {
                $GPNTFG = '';
            }
            $rs = fnInsertDB2GPK($db2con,$GPKQRY,$GPKID,$GPKNM,$GPKFM,$GPKXCO,$GPKXID,$GPKXNM,$GPKYNM,$GPKCFG,$GPKDFG,$GPJKCHK);//MSM add GPKDFG 20180925
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('グラフ'));
            }
        }
    }
      if($rtn === 0){
            //Ｙ軸にある元のデータを削除
            $rs =  fnDeleteDB2GPC($db2con,$GPKQRY,$GPKID);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('グラフ'));
            }else{
                //Ｙ軸のデータを登録
                $rs = fnInsertDB2GPC($db2con,$GPKQRY,$GPKID,$listData);
                if($rs !== true){
                    $rtn = 1;
                    $msg = showMsg($rs,array('グラフ'));
                }
            }
        }
    }
 }

if($rtn !== 0){
    db2_rollback($db2con);
}else{
    db2_commit($db2con);
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'formValues' => $formValues,
    'listData' => $listData
);

echo(json_encode($rtn));

//DB2GPKにグラフキー情報を登録
function fnInsertDB2GPK($db2con,$GPKQRY,$GPKID,$GPKNM,$GPKFM,$GPKXCO,$GPKXID,$GPKXNM,$GPKYNM,$GPKCFG,$GPKDFG,$GPJKCHK){//MSM add GPKDFG 20180925
    $GPKCUD = date('Ymd');//20170228
    $GPKCUT = date('hi');//20170228
    $GPKCUD = 0;
    $GPKCUT = 0;
    $GPKDAT = '1';
    $rs = true;
    $params = array($GPKQRY,$GPKID,$GPKNM,$GPKFM,$GPKXCO,$GPKXID,$GPKXNM,$GPKYNM,$GPKCFG,$GPKCUD,$GPKCUT,$GPKDAT,$GPKDFG,$GPJKCHK);
    $strSQL  = ' INSERT INTO DB2GPK ';
    $strSQL .= ' ( GPKQRY,GPKID,GPKNM,GPKFM,GPKXCO,GPKXID,GPKXNM,GPKYNM,GPKCFG,GPKCUD,GPKCUT,GPKDAT,GPKDFG,GPJKCHK) ';
    $strSQL .= ' VALUES ' ;
    $strSQL .= ' (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
         error_log("statement fail " . db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
         error_log("statement fail " . db2_stmt_errormsg());
        }
    }
    return $rs;
}
//DB2GPKにグラフキー情報を更新
function fnUpdateDB2GPK($db2con,$GPKQRY,$GPKID,$GPKNM,$GPKFM,$GPKXCO,$GPKXID,$GPKXNM,$GPKYNM,$GPKCFG,$GPKDFG,$GPJKCHK){//MSM add GPKDFG 20180925
    $rs = true;
    //構文
    $strSQL  = ' UPDATE DB2GPK ';
    $strSQL .= ' SET ';
    $strSQL .= ' GPKNM = ?, ';
    $strSQL .= ' GPKFM = ?, ';
    $strSQL .= ' GPKXCO = ?, ';
    $strSQL .= ' GPKXID = ?, ';
    $strSQL .= ' GPKXNM = ?, ';
    $strSQL .= ' GPKYNM = ?, ';
    $strSQL .= ' GPKCFG = ?,';
    $strSQL .= ' GPKDFG = ?,';
    $strSQL .= ' GPJKCHK = ?';
    $strSQL .= ' WHERE GPKQRY = ? AND GPKID = ? ';
    $params = array($GPKNM,$GPKFM,$GPKXCO,$GPKXID,$GPKXNM,$GPKYNM,$GPKCFG,$GPKDFG,$GPJKCHK,$GPKQRY,$GPKID);
    $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_UPD';
            error_log("execute fail " . db2_stmt_errormsg());
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                error_log("execute fail " . db2_stmt_errormsg());
                 $rs = 'FAIL_UPD';
            }
    }
    return $rs;
}
//DB2GPCにある元のデータを削除
function fnDeleteDB2GPC($db2con,$GPCQRY,$GPCID){
    $rs = true;
    //構文
    $strSQL  = ' DELETE FROM DB2GPC WHERE ';
    $strSQL  .= ' GPCQRY = ?  AND GPCID = ? ';
    $params = array($GPCQRY,$GPCID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
//DB2GPCにある元のデータを削除
function  fnDeleteDB2GPK($db2con,$GPKQRY,$GPKID){
    $rs = true;
    //構文
    $strSQL  = ' DELETE FROM DB2GPK WHERE ';
    $strSQL  .= ' GPKQRY = ?  AND GPKID = ? ';
    $params = array($GPKQRY,$GPKID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
//DB2GPCにＹ軸の登録
function fnInsertDB2GPC($db2con,$GPCQRY,$GPCID,$listData){
    $rs = true;
    $strSQL  = ' INSERT INTO DB2GPC ';
    $strSQL .= ' ( GPCSEQ,GPCQRY,GPCID,GPCYCO,GPCYID,GPCLBL,GPCFM,GPCCAL,GPCCLR,GPCSOS,GPCSFG) ';
    $strSQL .= ' VALUES ' ;
    $strSQL .= ' (?,?,?,?,?,?,?,?,?,?,?) ';
    foreach($listData as $key => $value){
        $params = array($value['GPCSEQ'],$GPCQRY,$GPCID,$value['GPCYCO'],$value['GPCYID'],$value['GPCLBL'],$value['GPCFM'],$value['GPCCAL'],$value['GPCCLR'],$value['GPCSOS'],$value['GPCSFG']);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_INS';
            break;
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                error_log("statement fail " . db2_stmt_errormsg());
                $rs = 'FAIL_INS';
                break;
            }
        }
    }
    return $rs;
}
