<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['PROC'])?$_POST['PROC']:'');
$formValues = json_decode($_POST['formValues'],true);
$GPKQRY = (isset($_POST['GPKQRY'])?$_POST['GPKQRY']:'');
$GPKID = (isset($_POST['GPKID'])?$_POST['GPKID']:'');
$GPKNUM = (isset($formValues['GPKNUM'])?$formValues['GPKNUM']:'');
$GPKDAT = (isset($formValues['GPKDAT'])?$formValues['GPKDAT']:'');
$GPKEND = (isset($formValues['GPKEND'])?$formValues['GPKEND']:'');
$GPKCUD = (isset($formValues['GPKCUD'])?$formValues['GPKCUD']:date('Y/m/d'));//20170228
$GPKCUT = (isset($formValues['GPKCUT'])?$formValues['GPKCUT']:date('h:i'));//20170228
$GPKCUD = str_replace('/','-',$GPKCUD);
$userDateTime = $GPKCUD.' '.$GPKCUT.':00';
$currentDateTime = date('Y-m-d H:i:s');
$GPKCUD = (int)str_replace('-','',$GPKCUD);
$GPKCUT = (int)str_replace(':','',$GPKCUT);
//htmlspecialcharsをデコード
$GPKQRY = cmHscDe($GPKQRY);
$GPKID = cmHscDe($GPKID);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID']))?$_SESSION['PHPQUERY']['user'][0]['WUUID']:'';
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$WUUID);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'30',$userData[0]['WUSAUT']);//'1' => グラフマスター
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グラフ作成の権限'));
            }
        }
    }
}
if($rtn === 0){
    $chkQry = cmChkQuery($db2con, '' , $GPKQRY, '' , '');
    if( $chkQry['result'] !== true ) {
        $rtn = 2;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$GPKQRY,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのグラフ作成の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('グラフ作成の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    if ($GPKNUM === '') {
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array($FROM.'実行間隔'));
    }
}
if($rtn === 0){
    if(!checkNum($GPKNUM)){
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array($FROM.'実行間隔','数値'));
    }
}
if($rtn === 0){
    if (strpos($GPKNUM, '.') !== false) {
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array($FROM.'実行間隔','数値'));
    }
}
if($rtn === 0){
    if(!checkMaxLen($GPKNUM,3)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array($FROM.'実行間隔'));
    }
}
if($rtn === 0){
	
    if((int)$GPKNUM<5 && (int)$GPKNUM !== 0 && (int)$GPKDAT === 1){
        $rtn = 1;
        $msg = showMsg('TIME_CHK',array($FROM.'実行間隔','5分'));
    }
}
if($rtn === 0 && (int)$GPKNUM > 0){
    $rs = checkValidDate($db2con,$userDateTime,$currentDateTime,$GPKDAT,$GPKNUM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }
}
 if($rtn === 0){
    //グラフ存在チェック
    $rs = cmCheckDB2GPK($db2con,$GPKQRY,$GPKID);
        if($rs['rs'] === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_UPD',array('グラフ'));
        }else if($rs['rs'] !== 'ISEXIST'){
            $rtn = 3;
            $msg = showMsg($rs['rs'],array('グラフ'));
        }
    if($rtn === 0){
         //Ｘ軸のデータを登録
        $rs = fnUpdateDB2GPK($db2con,$GPKQRY,$GPKID,$GPKNUM,$GPKDAT,$GPKCUD,$GPKCUT,$GPKEND);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('グラフ'));
        }
    }
 }

if($rtn !== 0){
    db2_rollback($db2con);
}else{
    db2_commit($db2con);
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'GPKCUD' =>$GPKCUD,
    'GPKCUT' =>$GPKCUT,
    'formValues' => $formValues,
    'currentDateTime' => $currentDateTime,
	'userDateTime' => $userDateTime,
    'rs' => $rs
 );

echo(json_encode($rtn));

function checkValidDate($db2con,$userDateTime,$currentDateTime,$GPKDAT,$GPKNUM){
    if($GPKDAT === '1'){
        $GPKNUM = $GPKNUM.' MINUTES';
    }else if($GPKDAT === '2'){
        $GPKNUM = $GPKNUM.' HOURS';
    }else if($GPKDAT == '3'){
        $GPKNUM = $GPKNUM.' DAYS';
    }else{
        $GPKNUM = $GPKNUM.' MONTH';
    }
    $data = array();
    $count = 0;
    $strSQL = ' SELECT COUNT(*) AS RESULTCOUNT FROM (';
//    $strSQL .= ' SELECT TIMESTAMP (\''.$userDateTime.'\') + '.$GPKNUM.' AS RESULTDATE FROM DB2GPK FETCH FIRST 1 ROW ONLY  ';
    $strSQL .= ' SELECT TIMESTAMP (\''.$userDateTime.'\') AS RESULTDATE FROM DB2GPK FETCH FIRST 1 ROW ONLY  ';
    $strSQL .= ' ) AS A WHERE A.RESULTDATE >= ?  ';
    $params = array($currentDateTime);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $count = (int)$row['RESULTCOUNT'];
            }
            if($count > 0){
                $data = array('result' => true);
            }else{
                $data = array('result' => 'FAIL_DATE');
            }
        }
    }
    return $data;
}

//DB2GPKにグラフキー情報を更新
function fnUpdateDB2GPK($db2con,$GPKQRY,$GPKID,$GPKNUM,$GPKDAT,$GPKCUD,$GPKCUT,$GPKEND){
    $rs = true;
    //構文
    $strSQL  = ' UPDATE DB2GPK ';
    $strSQL .= ' SET ';
    $strSQL .= ' GPKNUM = ? , ';
    $strSQL .= ' GPKDAT = ? , ';
    $strSQL .= ' GPKCUD = ? , ';
    $strSQL .= ' GPKCUT = ? ,';
    $strSQL .= ' GPKFLG = ? , ';
    $strSQL .= ' GPKEND = ? ';
    $strSQL .= ' WHERE GPKQRY = ? AND GPKID = ? ';
    $params = array($GPKNUM,$GPKDAT,$GPKCUD,$GPKCUT,0,$GPKEND,$GPKQRY,$GPKID);
    $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
             e_log("execute fail " . db2_stmt_errormsg());
            $rs = 'FAIL_UPD';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log("execute fail " . db2_stmt_errormsg());
                 $rs = 'FAIL_UPD';
            }
    }
    return $rs;
}