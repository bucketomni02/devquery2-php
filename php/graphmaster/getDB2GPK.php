<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once('../common/inc/config.php');
include_once('../common/inc/common.inc.php');
include_once('../licenseInfo.php');
//$licenseType = 'FREE';

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$GPKQRY = (isset($_POST['GPKQRY'])?$_POST['GPKQRY']:'');
$GPKID = (isset($_POST['GPKID'])?$_POST['GPKID']:'');
$GPKNM =(isset($_POST['GPKNM'])?$_POST['GPKNM']:'');
$start =(isset($_POST['start'])?$_POST['start']:'');
$length =(isset($_POST['length'])?$_POST['length']:'');
$sort =(isset($_POST['sort'])?$_POST['sort']:'');
$sortDir =(isset($_POST['sortDir'])?$_POST['sortDir']:'');

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmlspecialcharsをデコード
$GPKQRY = cmHscDe($GPKQRY);
$GPKID = cmHscDe($GPKID);
$GPKNM = cmHscDe($GPKNM);
$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID']))?$_SESSION['PHPQUERY']['user'][0]['WUUID']:'';
$gpk = array();
$gpc = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$WUUID);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'30',$userData[0]['WUSAUT']);//'1' => グラフマスター
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グラフ作成の権限'));
            }
        }
    }
}
if($rtn === 0){
    $chkQry = cmChkQuery($db2con, '' , $GPKQRY, '' ,'');
    if($chkQry['result'] !== true){
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$GPKQRY,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのグラフ作成の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('グラフ作成の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($proc === 'EDIT' && $rtn === 0){
    //グラフ存在チェック
        $rs = cmCheckDB2GPK($db2con,$GPKQRY,$GPKID);
        if($rs['rs'] === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET',array('グラフ'));
        }else if($rs['rs'] !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs['rs'],array('グラフ'));
        }
    if($rtn === 0){
        $rs = cmSelDB2GPK($db2con,$GPKQRY,$GPKID);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('グラフ'));
        }else{
            $gpk = umEx($rs['data']);
            $rs = cmSelDB2GPC($db2con,$GPKQRY,$GPKID);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result'],array('グラフ'));
            }else{
                $gpc = umEx($rs['data'],true);
            }
        }
    }
//    $htmlFlg = false;
}else{
    if($rtn === 0){
        $rs = fnGetAllCount($db2con,$GPKQRY);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetDB2GPK($db2con,$GPKQRY,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}

cmDb2Close($db2con);


/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg),
    'GPK' => $gpk,
    'GPC' => $gpc,
    'GPKQRY' => $GPKQRY
);

echo(json_encode($rtn));


/*
*-------------------------------------------------------* 
* グラフマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2GPK($db2con,$GPKQRY,$start,$length,$sort,$sortDir){

    $data = array();
        $strSQL  = ' SELECT C.*';
        $strSQL  .= 'FROM( SELECT A.* ,';
        $strSQL .= ' ROWNUMBER() OVER( ';
        if($sort !== ''){
            $strSQL .= ' ORDER BY A.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= ' ORDER BY A.GPKID ASC ';
        }
        $strSQL .= ' ) as rownum ';
        $strSQL .= ' FROM ( SELECT B.GPKQRY,B.GPKID,B.GPKNM , B.GPKBFD, B.GPKBFT, B.GPKNUM, '; 
        $strSQL .= ' CASE ';
        $strSQL .= ' WHEN GPKDAT IS NULL OR GPKDAT = \'\' THEN \'0 分\' ';
        $strSQL .= ' WHEN GPKDAT = \'1\' THEN CONCAT(GPKNUM,\' 分\') ';
        $strSQL .= ' WHEN GPKDAT = \'2\' THEN CONCAT(GPKNUM,\' 時\') ';
        $strSQL .= ' WHEN GPKDAT = \'3\' THEN CONCAT(GPKNUM,\' 日\') ';
        $strSQL .= ' WHEN GPKDAT = \'4\' THEN CONCAT(GPKNUM,\' 月\') ';
        $strSQL .= ' END AS JIKOKANKAKU  ';
        $strSQL .= ' FROM DB2GPK as B ';

        $strSQL .= ' WHERE GPKQRY <> \'\' ';

        $strSQL .= ' AND GPKID <> \'\' ';

       $strSQL .= ' AND GPKQRY = ? ';
        $strSQL .= ' ) as A ';
        $strSQL .= ' ) as C ';
        //抽出範囲指定
       $params = array();
       array_push($params,$GPKQRY);
        if(($start != '')&&($length != '')){
            $strSQL .= ' WHERE C.rownum BETWEEN ? AND ? ';
            array_push($params,$start + 1);
            array_push($params,$start + $length);
        }
        e_log($strSQL);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$GPKQRY){
    $data = array();

    $strSQL  = ' SELECT count(A.GPKID) as COUNT ';
    $strSQL .= ' FROM  DB2GPK as A ' ;
    $strSQL .= ' WHERE GPKQRY <> \'\' ';
    $strSQL .= ' AND GPKID <> \'\' ';
    $strSQL .= ' AND GPKQRY = ? ';
    $params = array($GPKQRY);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* ユーザーマスター取得(一行)
*-------------------------------------------------------*
*/

function fnSelDB2AMST($db2con,$AMNAME){

    $data = array();

    $params = array($AMNAME);
    $rs = true;


        $strSQL  = ' SELECT ';
        $strSQL .= ' A.AMNAME,A.AMTEXT  ';
        $strSQL .= ' FROM DB2AMST as A ';
        $strSQL .= ' WHERE AMNAME = ? ';

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                if(count($data)=== 0){
                    $data = array('result' => 'NOTEXIST_GET');
                }else{
                    $data = array('result' => true,'data' => $data);
                }
            }
        }
    return $data;
}
