<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$GPKQRY = (isset($_POST['GPKQRY'])?$_POST['GPKQRY']:'');
$GPKID = $_POST['GPKID'];
$GPKNM = $_POST['GPKNM'];
$currentDate     = date('Ymd');
$currentTime     = date('Hi');
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmlspecialcharsをデコード
$GPKQRY = cmHscDe($GPKQRY);
$GPKID = cmHscDe($GPKID);
$GPKNM = cmHscDe($GPKNM);
$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID']))?$_SESSION['PHPQUERY']['user'][0]['WUUID']:'';
$gpk = array();
$gpc = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$WUUID);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'30',$userData[0]['WUSAUT']);//'1' => グラフマスター
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グラフ作成の権限'));
            }
        }
    }
}
if($rtn === 0){
    $chkQry = cmChkQuery($db2con, '' , $GPKQRY, '' ,'');
    if($chkQry['result'] !== true){
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$GPKQRY,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのグラフ作成の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('グラフ作成の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
 if($rtn === 0){
//グラフ存在チェック
    $rs = cmCheckDB2GPK($db2con,$GPKQRY,$GPKID);
    if($rs['rs'] === true){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET',array('グラフ'));
    }else if($rs['rs'] !== 'ISEXIST'){
        $rtn = 1;
        $msg = showMsg($rs['rs'],array('グラフ'));
    }
    if($rtn === 0){
        $rs = fnGetDB2GPK($db2con,$GPKQRY,$GPKID);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
  }
if(count($data) > 0){
    $GPKCUD = cmMer($data[0]['GPKCUD']);
    $GPKCUT = cmMer($data[0]['GPKCUT']);
     if((int)$GPKCUD === 0 && (int)$GPKCUT === 0 ){
        $GPKCUD = (int)cmMer($data[0]['GPKCUD']);
        $GPKCUT = (int)cmMer($data[0]['GPKCUT']);
     }
}
 
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'currentDate' => $currentDate,
    'currentTime' => $currentTime,
    'GPKCUD' => $GPKCUD,
    'GPKCUT' => $GPKCUT,
    'aaData' => umEx($data)
);

echo(json_encode($rtn));


/*
*-------------------------------------------------------* 
* グラフマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2GPK($db2con,$GPKQRY,$GPKID){

    $data = array();
        $strSQL  = ' SELECT A.GPKNUM,A.GPKDAT,A.GPKCUD,A.GPKCUT,A.GPKEND ';
        $strSQL .= ' FROM DB2GPK AS A ';
        $strSQL .= ' WHERE GPKQRY <> \'\' ';
        $strSQL .= ' AND GPKID <> \'\' ';
       $strSQL .= ' AND GPKQRY = ? ';
       $strSQL .= ' AND GPKID = ? ';
       $params = array($GPKQRY,$GPKID);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                 e_log("statement fail " . db2_stmt_errormsg());
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;

}
