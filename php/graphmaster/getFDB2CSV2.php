<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$d1name = (isset($_POST['GPKQRY'])?$_POST['GPKQRY']:'');
$jiku = (isset($_POST['JIKU'])?$_POST['JIKU']:'');
$jikuFlg = false;
if($jiku === 'Y'){
    $jikuFlg = true;
}
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'30',$userData[0]['WUSAUT']);//'1' => グラフマスター
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グラフ作成の権限'));
            }
        }
    }
}
//クエリー存在チェック
    if($rtn === 0){
        $chkQry = array();
        $chkQry = cmChkQuery($db2con,'',$d1name,'','');
        if($chkQry['result'] !== true){
            $rtn = 1;
            $msg = showMsg($chkQry['result'],array('クエリー'));
        }
    }
if($rtn === 0){
    //ユーザー名取得
    $rs = cmGetFDB2CSV2($db2con,$d1name,false,$jikuFlg,false);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result'],array('クエリー'));
        $rtn = 1;
    }else{
        $data = $rs['data'];
    }
}
if($rtn === 0){
    if(count($data) === 0){
        if($jiku === 'Y'){
            $msg = showMsg('JIKUERR1',array('縦軸'));
        }else{
            $msg = showMsg('NOTEXIST',array('設定できる項目'));
        }
        $rtn = 1;
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'aaData' => umEx($data,true),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));
