<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WQGID = cmHscDe($_POST['WQGID']);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$groupKengenFlg = '1';
$userKengenFlg = '1';
$queryKengenFlg = '1';
$queryData=array();
/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('グループ作成の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'2',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $groupKengenFlg = '';
            }
            $rs = cmChkKenGen($db2con,'4',$userData[0]['WUSAUT']);//'4' => グループクエリー権限
            if($rs['result'] !== true){
                $queryKengenFlg = '';
            }
            $rs = cmChkKenGen($db2con,'5',$userData[0]['WUSAUT']);//'5' =>グループユーザー権限
            if($rs['result'] !== true){
                $userKengenFlg = '';
            }
        }
        if($groupKengenFlg === '' && $userKengenFlg === '' && $queryKengenFlg === '' ){
            $rtn = 2;
            $msg =  showMsg($rs['result'],array('グループ作成の権限'));
        }else if($groupKengenFlg === ''){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('グループ作成の権限'));
         }
    }
}

//グループ存在チェック
if($rtn === 0){
    $rs = fnCheckDB2WQGR($db2con,$WQGID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('グループ'));
    }
}
//ブックマークのためクエリーリスト
if($rtn === 0){
    $r = cmGETQRY_DB2WGDF($db2con,$WQGID);
    if($r['result'] !== true){
        $rtn = 1;
        $msg = showMsg($r['result']);
    }else{
        $queryData=umEx($r['data']);
    }
}
if($rtn === 0){
    $rs = fnDeleteDB2WQGR($db2con,$WQGID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

if($rtn === 0){
    $rs = fnDeleteDB2WGDF($db2con,$WQGID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

if($rtn === 0){
    $rs = fnDeleteDB2WUGR($db2con,$WQGID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

if($rtn === 0){
    $rs = fnDeleteDB2WSGP($db2con,$WQGID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
//ブックマークのため削除
if($rtn === 0){
    if(count($queryData)>0){
        foreach($queryData as $value){
            $r=cmCountDB2WGDF_DB2WUGR($db2con,$value['WGNAME']);
            if($r['result']!==true){
                $rtn=1;
                $msg=$r['result'];
                break;
            }else{
                if($r['TOTALCOUNT']<=0){
                    $r=cmDelDB2BMK($db2con,'',$value['WGNAME'],2);
                    if($r['result']!==true){
                        $rtn=1;
                        $msg=$r['result'];
                        break;
                    }
                }
            }
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* グループ存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WQGR($db2con,$WQGID){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.WQGID ';
    $strSQL .= ' FROM DB2WQGR  AS A ' ;
    $strSQL .= ' WHERE A.WQGID = ? ' ;

    $params = array(
        $WQGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_DEL';
            }
        }
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* グループ削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WQGR($db2con,$WQGID){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WQGR ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WQGID = ? ' ;

    $params = array(
        $WQGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* クエリー別グループ定義削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WGDF($db2con,$WGGID){

    $rs = true;

    //構文

    $strSQL  = ' DELETE FROM DB2WGDF ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WGGID = ? ' ;

    $params = array(
        $WGGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* グループ権限ファイル削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WUGR($db2con,$WUGID){

    $rs = true;

    //構文

    $strSQL  = ' DELETE FROM DB2WUGR ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUGID = ? ' ;

    $params = array(
        $WUGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* グループ権限ファイル削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WSGP($db2con,$WUGID){

    $rs = true;

    //構文

    $strSQL  = ' DELETE FROM DB2WSGP ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' GPMID = ? ' ;
    $strSQL .= ' OR GPSID = ? ' ;

    $params = array(
        $WUGID
        , $WUGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }

    return $rs;

}
