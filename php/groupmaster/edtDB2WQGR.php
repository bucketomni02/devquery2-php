<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = $_POST['PROC'];
$WQGID = $_POST['WQGID'];
$WQUNAM = $_POST['WQUNAM'];
$WQHFLG = $_POST['WQHFLG'];
$kengenflg = (isset($_POST['KENGENFLG']) ? $_POST['KENGENFLG'] : '');
$groupKengenFlg = '1';
$userKengenFlg = '1';
$queryKengenFlg = '1';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$WQGID = cmHscDe($WQGID);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('グループ作成の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'2',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $groupKengenFlg = '';
            }
            $rs = cmChkKenGen($db2con,'4',$userData[0]['WUSAUT']);//'4' => グループクエリー権限
            if($rs['result'] !== true){
                $queryKengenFlg = '';
            }
            $rs = cmChkKenGen($db2con,'5',$userData[0]['WUSAUT']);//'5' =>グループユーザー権限
            if($rs['result'] !== true){
                $userKengenFlg = '';
            }
        }
            if($groupKengenFlg === '' && $userKengenFlg === '' && $queryKengenFlg === '' ){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループ作成の権限'));
            }else if($groupKengenFlg === ''){
                $rtn = 3;
                $msg =  showMsg('NOTEXIST',array('グループ作成の権限'));
             }
    }
    }

if($rtn === 0){
    //必須チェック
    if($WQGID === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('ID'));
        $focus = 'WQGID';
    }
}
if($rtn === 0){
    //必須チェック
    if(cmMer($WQGID) === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_SPACECHK',array('ID'));
        $focus = 'WQGID';
    }
}

if($rtn === 0){
    if($WQUNAM === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('グループ名'));
        $focus = 'WQUNAM';
    }
}

if($rtn === 0){
    //必須チェック
    if(cmMer($WQUNAM) === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_SPACECHK',array('グループ名'));
        $focus = 'WQGID';
    }
}
//バリデーションチェック
if($rtn === 0){
    $byte = checkByte($WQGID);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('ID'));
        $focus = 'WQGID';
    }
}

if($rtn === 0){
    if(!checkMaxLen($WQGID,10)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('ID'));
        $focus = 'WQGID';
    }
}

if($rtn === 0){
    if(!checkMaxLen($WQUNAM,32)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('グループ名'));
        $focus = 'WQUNAM';
    }
}

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
if($proc === 'UPD'){

    //グループ存在チェック
    if($rtn === 0){
        $rs = fnCheckDB2WQGR($db2con,$WQGID);
        if($rs === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_UPD',array('グループID'));
            $focus = 'WQGID';
        }else if($rs !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs,array('グループID'));
            $focus = 'WQGID';
        }
    }

    if($rtn === 0){
        $rs = fnUpdateDB2WQGR($db2con,$WQGID,$WQUNAM,$WQHFLG,$WQTILH);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }

}else if($proc === 'ADD'){

    //グループ存在チェック
    if($rtn === 0){
        $rs = fnCheckDB2WQGR($db2con,$WQGID);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('グループID'));
            $focus = 'WQGID';
        }
    }
/*
    //ライセンスチェック
    if($rtn === 0 && $licenseGroup !== ''){
        $count = fnCountDB2WQGR($db2con);
        if((int)$count >= (int)$licenseGroup){
            $rtn = 1;
            $msg = 'グループは'.$licenseGroup.'個まで登録できます。';
            $focus = 'WQGID';
        }
    }
*/
    if($rtn === 0){
        $rs = fnInsertDB2WQGR($db2con,$WQGID,$WQUNAM,$WQHFLG,$WQTILH);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }

}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'groupKengenFlg' => $groupKengenFlg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* 現在のグループ数をリターン
*-------------------------------------------------------*
*/

function fnCountDB2WQGR($db2con){

    $data = array();
    $rs = true;


    $strSQL  = ' SELECT count(*) as COUNT ';
    $strSQL .= ' FROM DB2WQGR AS A ' ;

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $rs = array('result' => true,'data' => $data[0]['COUNT']);
        }            
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* ユーザー存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WQGR($db2con,$WQGID){

    $data = array();
    $rs = true;


    $strSQL  = ' SELECT A.WQGID ';
    $strSQL .= ' FROM DB2WQGR AS A ' ;
    $strSQL .= ' WHERE A.WQGID = ? ' ;

    $params = array(
        $WQGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* グループ更新
*-------------------------------------------------------*
*/

function fnUpdateDB2WQGR($db2con,$WQGID,$WQUNAM,$WQHFLG,$WQTILH){

    $rs = true;

    //構文

    $strSQL  = ' UPDATE DB2WQGR ';
    $strSQL .= ' SET ';
    $strSQL .= ' WQUNAM = ? ,';
    $strSQL .= ' WQHFLG = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WQGID = ? ' ;

    $params = array(
        $WQUNAM,
        $WQHFLG,
        $WQGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* グループ追加
*-------------------------------------------------------*
*/

function fnInsertDB2WQGR($db2con,$WQGID,$WQUNAM,$WQHFLG,$WQTILH){

    $rs = true;

    //構文

    $strSQL  = ' INSERT INTO DB2WQGR ';
    $strSQL .= ' ( ';
    $strSQL .= ' WQGID, ';
    $strSQL .= ' WQUNAM, ';
    $strSQL .= ' WQHFLG ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ' ;
    $strSQL .= ' (?,?,?) ';

    $params = array(
        $WQGID,
        $WQUNAM,
        $WQHFLG
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;

}