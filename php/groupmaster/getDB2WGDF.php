<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$WQGID = $_POST['WQGID'];

$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$wqunam = '';
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
//$WQGID = cmHscDe($WQGID);
error_log('WQGID****'.print_r($WQGID,true));

$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'4',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループクエリー権限の権限'));
            }
        }
    }
}
//グループ存在チェック
if($rtn === 0){
    $rs = fnCheckDB2WQGR($db2con,$WQGID);
    if($rs === 'NOTEXIST_GET'){
        $rtn = 3;
        $msg = showMsg($rs,array('グループ'));
    }else if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('グループ'));
    }
}

if($rtn === 0){
    $res = fnGetAllCount($db2con,$WQGID);
    if($res['result'] !== true){
        $rtn = 1;
        $msg = showMsg($res['result']);
    }else{
        $allcount = $res['data'];
    }
}
if($rtn === 0){        
    $res = fnGetDB2WGDF($db2con,$WQGID,$start,$length,$sort,$sortDir);
    if($res['result'] === true){
        $data = $res['data'];
      
    }else{
        $rtn = 1;
        $msg = showMsg($res['result']);
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'WQGID' => cmHsc($WQGID),
    'RTN' => $rtn,
    'MSG' => $msg,
    'licenseType' => $licenseType
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/

function fnGetDB2WGDF($db2con,$WQGID,$start = '',$length = '',$sort = '',$sortDir = ''){

   $data = array();

   $params = array();

    $strSQL  = ' SELECT A.* FROM ';
    $strSQL .= ' ( ' ;
    $strSQL .= ' SELECT B.* , ROWNUMBER() OVER(  ORDER BY WGNAME ASC  ) ' ;
    $strSQL .= ' AS rownum ' ;
    $strSQL .= ' FROM ( ';
    $strSQL .= ' select C.* from ';
    $strSQL .= ' ( ' ;
    $strSQL .= ' select d1name,d1text,ifnull(WGGID,\'\') as wggid,WGNAME,BB.WGQGFLG,WGNRFLG  FROM  ';
    $strSQL .= ' ( ' ;
    $strSQL .= ' SELECT D1NAME,D1TEXT, 	\'\' AS WGQGFLG  	FROM FDB2CSV1  ';
    $strSQL .= ' UNION ALL ';
    $strSQL .= ' SELECT QRYGID AS D1NAME, 	QRYGNAME AS D1TEXT, 	\'1\' AS WGQGFLG  	FROM DB2QRYG  ';
    $strSQL .= ' ) BB ';
    $strSQL .= ' LEFT OUTER JOIN DB2WGDF ON D1NAME = WGNAME ';
    $strSQL .= ' ) as C  ) ';
    $strSQL .= ' as B  WHERE D1NAME <> \'\' ';
    $strSQL .= ' ) as A ';
  
    
    if($WQGID != ''){
        $strSQL .= ' WHERE WGGID = ?  ';
        array_push($params,$WQGID);
    }
  
  /*  if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ?  ';  
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }     */
    $strSQL .= ' order by CAST((CASE WHEN WGNRFLG = \'\' THEN 0 ELSE WGNRFLG END) AS INT) ASC, ';
    $strSQL .= ' WGNAME ASC  ';
    $stmt = db2_prepare($db2con,$strSQL);
    error_log('STRSQLGET***'.print_r($strSQL,true));
    error_log('PARAMSGET***'.print_r($params,true));
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }

    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$WQGID){
    $data = array();

    $strSQL  = ' SELECT count(*) as COUNT ';
    $strSQL .= ' FROM DB2WGDF as A ' ;
    $strSQL .= ' LEFT JOIN  FDB2CSV1 as B ' ;
    $strSQL .= ' ON A.WGNAME = B.D1NAME ' ;
    $strSQL .= ' WHERE A.WGGID = ? ';

    $params = array(
        $WQGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    error_log('STRSQL***'.print_r($strSQL,true));
    error_log('PARAMS***'.print_r($params,true));
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;
}

/*
*-------------------------------------------------------* 
* グループ名取得
*-------------------------------------------------------*
*/

function fnGetWQUNAM($db2con,$id,$WUAUTH){

 /*   $data = array();
    $params = array();
 
    $strSQL .= '    SELECT ';
    $strSQL .= '        A.WQUNAM ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '     ,   IFNULL(B.WUGID, \'\') AS WUGID ';
    }
    $strSQL .= '    FROM ';
    $strSQL .= '        DB2WQGR AS A ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '        LEFT JOIN ';
        $strSQL .= '        ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                WUGID ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2WUGR ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                WUUID = ? ';
        $strSQL .= '        ) B ';
        $strSQL .= '    ON A.WQGID = B.WUGID ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= '    WHERE ';
    $strSQL .= '        WQGID = ? ';
    array_push($params,$id);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
            e_log($strSQL.print_r($params,true).db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
            e_log($strSQL.print_r($params,true).db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
           if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET');
             }else{
                if($WUAUTH === '3' || $WUAUTH === '4'){
                    if($data[0][WUGID] === ''){
                        $data = array('result' => 'ログインユーザーに指定したグループに対してのグループ権限はありません。');
                    }else{
                        $data = array('result' => true,'data' => $data);
                    }
                }else{
                    $data = array('result' => true,'data' => $data);
                }
            }
        }
    }
    return $data;   */
}


/*
*-------------------------------------------------------* 
* グループ存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WQGR($db2con,$WQGID){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.WQGID ';
    $strSQL .= ' FROM DB2WQGR  AS A ' ;
    $strSQL .= ' WHERE A.WQGID = ? ' ;

    $params = array(
        $WQGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}
