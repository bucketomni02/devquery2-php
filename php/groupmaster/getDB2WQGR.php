<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$WQGID  = $_POST['WQGID'];
$WQUNAM = $_POST['WQUNAM'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$session = array();
$session['wqgid']  = $WQGID;
$session['wqunam']  = $WQUNAM;
$session['start']   = $start;
$session['length']  = $length;
$session['sort']    = $sort;
$session['sortDir'] = $sortDir;

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$htmlFlg = true;
$groupKengenFlg = '1';
$userKengenFlg = '1';
$queryKengenFlg = '1';
$subGpKengenFlg = '1';
$rtn = 0;
$kengenflg = (isset($_POST['KENGENFLG']) ? $_POST['KENGENFLG'] : '');
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$WQGID = cmHscDe($WQGID);
$WQUNAM = cmHscDe($WQUNAM);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $userKengenFlg = '';
            $groupKengenFlg = '';
            $subGpKengenFlg = '1';
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'2',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $groupKengenFlg = '';
                $subGpKengenFlg = '';
            }
            $rs = cmChkKenGen($db2con,'4',$userData[0]['WUSAUT']);//'4' => グループクエリー権限
            if($rs['result'] !== true){
                $queryKengenFlg = '';
            }
            $rs = cmChkKenGen($db2con,'5',$userData[0]['WUSAUT']);//'5' =>グループユーザー権限
            if($rs['result'] !== true){
                $userKengenFlg = '';
            }
        }
        if($rtn === 0){
            if($kengenflg !== "1"){// 1=> from group edit window
                if($groupKengenFlg === '' && $userKengenFlg === '' && $queryKengenFlg === '' ){
                    $rtn = 2;
                    $msg =  showMsg($rs['result'],array('グループ作成の権限'));
                }
            }else{
                if($groupKengenFlg === ''){
                    $rtn = 3;
                    $msg =  showMsg('NOTEXIST',array('グループ作成の権限'));
                }
            }
        }
    }
}
if($proc === 'EDIT'){
    if($rtn === 0){
        $res = fnSelDB2WQGR($db2con,$WQGID);
        if($res['result'] === true){
            if(count($res['data']) === 0){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET',array('グループ'));
            }else{
                $data = $res['data'];
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $res = fnGetAllCount($db2con,$userData[0]['WUAUTH'],$WQGID,$WQUNAM);
        if($res['result'] === true){
            $allcount = $res['data'];
            $res = fnGetDB2WQGR($db2con,$userData[0]['WUAUTH'],$WQGID,$WQUNAM,$start,$length,$sort,$sortDir);
            if($res['result'] === true){
                $data = $res['data'];
            }else{
                $rtn = 1;
                $msg = showMsg($res['result']);
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
}

cmDb2Close($db2con);
/**return**/
$rtn = array(
    'iTotalRecords'     => $allcount,
    'aaData'            => umEx($data,$htmlFlg),
    'RTN'               => $rtn,
    'MSG'               => $msg,
    'groupKengenFlg'    => $groupKengenFlg,
    'userKengenFlg'     => $userKengenFlg,
    'queryKengenFlg'    => $queryKengenFlg,
    'subGpKengenFlg'    => $subGpKengenFlg,
    'groupname'         => $WQUNAM,
    'session'           => $session,
    'WQGID'             => $WQGID,
    'WQUNAM'            => $WQUNAM
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/

function fnGetDB2WQGR($db2con,$WUAUTH,$WQGID,$WQUNAM,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.WQGID ASC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM DB2WQGR as B ';
    $strSQL .= ' WHERE WQGID <> \'\' ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '    AND WQGID IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            WUGID ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WUGR ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WUUID = ? ';
        $strSQL .= '    ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']); 
    }
    if($WQGID != ''){
        $strSQL .= ' AND WQGID like ? ';
        array_push($params,'%'.$WQGID.'%');
    }

    if($WQUNAM != ''){
        $strSQL .= ' AND WQUNAM like ? ';
        array_push($params,'%'.$WQUNAM.'%');
    }
    
    $strSQL .= ' ) as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    //e_log("fnGetDB2WQGR++++++++++++++".$strSQL);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$WUAUTH,$WQGID,$WQUNAM){
    $rs = array();
    $params = array();

    $strSQL  = ' SELECT count(A.WQGID) as COUNT ';
    $strSQL .= ' FROM DB2WQGR as A ' ;
    $strSQL .= ' WHERE WQGID <> \'\' ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '    AND WQGID IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            WUGID ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WUGR ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WUUID = ? ';
        $strSQL .= '    ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']); 
    }
    if($WQGID != ''){
        $strSQL .= ' AND WQGID like ? ';
        array_push($params,'%'.$WQGID.'%');
    }

    if($WQUNAM != ''){
        $strSQL .= ' AND WQUNAM like ? ';
        array_push($params,'%'.$WQUNAM.'%');
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rs[] = $row['COUNT'];
            }
            $rs = array('result' => true,'data' => $rs[0]);
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* DB2WQGR取得(一行)
*-------------------------------------------------------*
*/

function fnSelDB2WQGR($db2con,$WQGID){

    $data = array();

    $params = array($WQGID);

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WQGR as A ';
    $strSQL .= ' WHERE WQGID = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}