<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");

//e_log('*********getDB2WSGP***************');
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$WQGID = $_POST['WQGID'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$gpflg = 0;   // 普通：0 サブグループ１：1
$wqunam = '';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$WQGID = cmHscDe($WQGID);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'2',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループ作成の権限'));
            }
        }
    }
}
//グループ存在チェック
//サブグループとして使えるチェック
if($rtn === 0){
    $rs = fnChkDB2SGP($db2con,$WQGID);
    //e_log('行５５'.print_r($rs,true));
    if($rs['result'] === 'NOTEXIST_GET'){
        $rtn = 3;
        $msg = showMsg($rs['result'],array('グループ'));
    }else if($rs['result'] === 'NOVLD_SGP'){
        $rtn = 3;
        // NOTVLD_ADD => このグループにサブグループ設定することができません。
        $msg = showMsg($rs['result'],array('グループ'));
    }else if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('グループ'));
    }
}
//e_log('サブグループ:'.print_r($rs,true));
if($rtn === 0){
    if($rs['data'][0]['S1GPFLG'] == 1){
        $gpflg = 1;
        $res = fnGetAllS1GPCNT($db2con,$userData[0]['WUAUTH'],$WQGID);
        if($res['result'] === true){
            if($res['result'] === true){
                $allcount = $res['data'];
                $res = fnGetS1GPDB2WSGP($db2con,$userData[0]['WUAUTH'] ,$start,$length,$sort,$sortDir,$WQGID);
                if($res['result'] === true){
                    $data = $res['data'];
                }else{
                    $rtn = 1;
                    $msg = showMsg($res['result'],array('ユーザー'));
                }
            }else{
                $rtn = 1;
                $msg = showMsg($res['result']);
            }
        }
    }else if($rs['data'][0]['MGPFLG'] == 1 || ($rs['data'][0]['S1GPFLG'] === 0 && $rs['data'][0]['MGPFLG'] === 0)){
        $res = fnGetAllSGPCNT($db2con,$userData[0]['WUAUTH'],$WQGID);
        if($res['result'] === true){
            if($res['result'] === true){
                $allcount = $res['data'];
                $res = fnGetSGPDB2WSGP($db2con,$userData[0]['WUAUTH'],$start,$length,$sort,$sortDir,$WQGID);
                if($res['result'] === true){
                    $data = $res['data'];
                }else{
                    $rtn = 1;
                    $msg = showMsg($res['result'],array('ユーザー'));
                }
            }else{
                $rtn = 1;
                $msg = showMsg($res['result']);
            }
        }
    }
}

//グループ名取得
if($rtn === 0){
    $res = fnGetWQUNAM($db2con,$WQGID,$userData[0]['WUAUTH']);
    if($res['result'] === true){
        $wqunam = $res['data'][0]['WQUNAM'];
    }else if($res['result'] === 'FAIL_SEL'){
        $rtn = 1;
        $msg = showMsg($res['result'],array('グループID'));
    }else{
        $rtn = 3;
        $msg = showMsg($res['result'],array('グループID'));
    } 
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'WQUNAM' => cmHsc($wqunam),
    'GPFLG'  => $gpflg,
    'RTN' => $rtn,
    'MSG' => $msg,
    'licenseType' =>$licenseType
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 全件カウント取得 マイングループID以外
*-------------------------------------------------------*
*/
// サブグループ追加先のグループはほかのグループのサブグールプとして設定されたことがある場合
function fnGetAllS1GPCNT($db2con,$WUAUTH,$WQGID){
    $data = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '     COUNT(A.WQGID) AS COUNT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     ( ';
    $strSQL .= '    SELECT GPDATA.* ';
    $strSQL .= '    FROM  ';
    $strSQL .= '        ( ';
    $strSQL .= '            SELECT  ';
    $strSQL .= '                C.*  ';
    $strSQL .= '            FROM  ';
    $strSQL .= '                (SELECT  ';
    $strSQL .= '                    *  ';
    $strSQL .= '                FROM  ';
    $strSQL .= '                    DB2WQGR  ';
    $strSQL .= '                WHERE  ';
    $strSQL .= '                    WQGID NOT IN ( ';
    $strSQL .= '                        SELECT  ';
    $strSQL .= '                            GPSID ';
    $strSQL .= '                        FROM ';
    $strSQL .= '                            DB2WSGP  ';
    $strSQL .= '                        WHERE GPSID IN ( ';
    $strSQL .= '                            SELECT DISTINCT  ';
    $strSQL .= '                                GPMID AS WQGID  ';
    $strSQL .= '                            FROM  ';
    $strSQL .= '                                DB2WSGP MGP ';
    $strSQL .= '                            INTERSECT ';
    $strSQL .= '                            SELECT ';
    $strSQL .= '                                GPSID AS WQGID ';
    $strSQL .= '                            FROM ';
    $strSQL .= '                                DB2WSGP ';
    $strSQL .= '                        ) ';
    $strSQL .= '                            UNION ';
    $strSQL .= '                            SELECT GPMID AS WQGID ';
    $strSQL .= '                            FROM DB2WSGP ';
    $strSQL .= '                        )  ';
    $strSQL .= '                    ) C  ';
    $strSQL .= '                    WHERE  ';
    $strSQL .= '                        C.WQGID <> \'\'       AND  ';
    $strSQL .= '                        C.WQGID <> ?  ';
    $strSQL .= '                    ORDER BY  ';
    $strSQL .= '                        C.WQGID ';
    $strSQL .= '                )GPDATA  ';
    $params = array($WQGID);
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '            WHERE WQGID IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    WUGID ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WUGR ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WUUID = ? ';
        $strSQL .= '                ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= '         )A ';
    $strSQL .= ' WHERE A.WQGID <> \'\' ';
    $strSQL .= ' AND A.WQGID <> ? ';
    array_push($params,$WQGID);
    e_log('カウント取得①：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log('データカウント取得失敗：'.$strSQL.'param:'.print_r($params,true).db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* DB2WSGP取得
*-------------------------------------------------------*
*/

function fnGetS1GPDB2WSGP($db2con,$WUAUTH,$start = '',$length = '',$sort = '',$sortDir = '',$WQGID){

    $data = array();

    $params = array();
    $strSQL .= '     SELECT A.* ';
    $strSQL .= '     FROM ( ';
    $strSQL .= '        SELECT B.* , ROWNUMBER() OVER(  ';
    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.WQGID ASC ';
    }
    $strSQL .= '            ) as rownum  ';
    $strSQL .= '        FROM  ';
    $strSQL .= '        (  ';
    $strSQL .= '            SELECT GPDATA.*, SGPDATA .*, ';
    $strSQL .= '                CASE WHEN SGPDATA.GPMID IS NULL THEN 0  ';
    $strSQL .= '                ELSE 1 END AS GPSIDFLG  ';
    $strSQL .= '            FROM  ';
    $strSQL .= '                ( ';
    $strSQL .= '                    SELECT  ';
    $strSQL .= '                        C.*  ';
    $strSQL .= '                    FROM  ';
    $strSQL .= '                        (SELECT  ';
    $strSQL .= '                            *  ';
    $strSQL .= '                        FROM  ';
    $strSQL .= '                            DB2WQGR  ';
    $strSQL .= '                        WHERE  ';
    $strSQL .= '                            WQGID NOT IN ( ';
    $strSQL .= '                                    SELECT  ';
    $strSQL .= '                                        GPSID ';
    $strSQL .= '                                    FROM ';
    $strSQL .= '                                        DB2WSGP  ';
    $strSQL .= '                                    WHERE GPSID IN ( ';
    $strSQL .= '                                        SELECT DISTINCT  ';
    $strSQL .= '                                            GPMID AS WQGID  ';
    $strSQL .= '                                        FROM  ';
    $strSQL .= '                                            DB2WSGP MGP ';
    $strSQL .= '                                        INTERSECT ';
    $strSQL .= '                                        SELECT ';
    $strSQL .= '                                            GPSID AS WQGID ';
    $strSQL .= '                                        FROM ';
    $strSQL .= '                                            DB2WSGP ';
    $strSQL .= '                                    ) ';
    $strSQL .= '                                    UNION ';
    $strSQL .= '                                    SELECT GPMID AS WQGID ';
    $strSQL .= '                                    FROM DB2WSGP ';
    $strSQL .= '                            )  ';
    $strSQL .= '                        ) C  ';
    $strSQL .= '                    WHERE  ';
    $strSQL .= '                        C.WQGID <> \'\'       AND  ';
    $strSQL .= '                        C.WQGID <> ?  ';
    $strSQL .= '                    ORDER BY  ';
    $strSQL .= '                        C.WQGID ';
    $strSQL .= '                )GPDATA  ';
    $strSQL .= '            LEFT JOIN   ';
    $strSQL .= '                (SELECT * FROM DB2WSGP WHERE GPMID = ?)SGPDATA  ';
    $strSQL .= '            ON GPDATA.WQGID = SGPDATA.GPSID  ';
    $strSQL .= '        )B ';
    $params = array($WQGID,$WQGID);
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '        WHERE B.WQGID IN ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                WUGID ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2WUGR ';
        $strSQL .= '            WHERE WUUID = ?  ';
        $strSQL .= '        )  ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= '    )  A  ';
    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    e_log('データ取得2：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
// サブグループ追加先のグループはほかのグループのサブグールプとして設定されたことがある場合
function fnGetAllSGPCNT($db2con,$WUAUTH,$WQGID){
    $data = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '     COUNT(A.WQGID) AS COUNT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     ( ';
    $strSQL .= '    SELECT GPDATA.* ';
    $strSQL .= '    FROM  ';
    $strSQL .= '        ( ';
    $strSQL .= '            SELECT  ';
    $strSQL .= '                C.*  ';
    $strSQL .= '            FROM  ';
    $strSQL .= '                (SELECT  ';
    $strSQL .= '                    *  ';
    $strSQL .= '                FROM  ';
    $strSQL .= '                    DB2WQGR  ';
    $strSQL .= '                WHERE  ';
    $strSQL .= '                    WQGID NOT IN ( ';
    $strSQL .= '                        ( ';
    $strSQL .= '                            SELECT  ';
    $strSQL .= '                                GPMID ';
    $strSQL .= '                            FROM ';
    $strSQL .= '                                DB2WSGP  ';
    $strSQL .= '                            WHERE GPSID IN ( ';
    $strSQL .= '                                SELECT DISTINCT  ';
    $strSQL .= '                                    GPMID AS WQGID  ';
    $strSQL .= '                                FROM  ';
    $strSQL .= '                                    DB2WSGP MGP ';
    $strSQL .= '                                INTERSECT ';
    $strSQL .= '                                SELECT ';
    $strSQL .= '                                    GPSID AS WQGID ';
    $strSQL .= '                                FROM ';
    $strSQL .= '                                    DB2WSGP ';
    $strSQL .= '                            ) ';
    $strSQL .= '                        ) ';
    $strSQL .= '                        UNION ';
    $strSQL .= '                        SELECT GPMID AS WQGID ';
    $strSQL .= '                        FROM DB2WSGP ';
    $strSQL .= '                        WHERE GPSID = ? ';
    $strSQL .= '                    )  ';
    $strSQL .= '                ) C  ';
    $strSQL .= '            WHERE '; 
    $strSQL .= '                C.WQGID <> \'\'       AND  ';
    $strSQL .= '                C.WQGID <> ?  ';
    $strSQL .= '            ORDER BY '; 
    $strSQL .= '                C.WQGID ';
    $strSQL .= '        )GPDATA  ';
    $params = array($WQGID,$WQGID);
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '        WHERE WQGID IN (SELECT ';
        $strSQL .= '                        WUGID ';
        $strSQL .= '                    FROM ';
        $strSQL .= '                        DB2WUGR ';
        $strSQL .= '                    WHERE ';
        $strSQL .= '                        WUUID = ? ';
        $strSQL .= '                    ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        e_log('管理グループと作成者取得');
    }
    $strSQL .= '         )A ';
    $strSQL .= ' WHERE A.WQGID <> \'\' ';
    $strSQL .= ' AND A.WQGID <> ? ';
    array_push($params,$WQGID);
    e_log('ALLGPDATA:'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* DB2WSGP取得
*-------------------------------------------------------*
*/

function fnGetSGPDB2WSGP($db2con,$WUAUTH,$start = '',$length = '',$sort = '',$sortDir = '',$WQGID){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.WQGID ASC ';
    }
    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM ';
    $strSQL .= '    ( ';
    $strSQL .= '    SELECT GPDATA.* , ';
    $strSQL .= '            CASE WHEN SGPDATA.GPSID IS NULL THEN 0 ELSE 1 END AS GPSIDFLG ';
    $strSQL .= '    FROM  ';
    $strSQL .= '        ( ';
    $strSQL .= '            SELECT  ';
    $strSQL .= '                C.*  ';
    $strSQL .= '            FROM  ';
    $strSQL .= '                (SELECT  ';
    $strSQL .= '                    *  ';
    $strSQL .= '                FROM  ';
    $strSQL .= '                    DB2WQGR  ';
    $strSQL .= '                WHERE  ';
    $strSQL .= '                    WQGID NOT IN ( ';
    $strSQL .= '                        ( ';
    $strSQL .= '                            SELECT  ';
    $strSQL .= '                                GPMID ';
    $strSQL .= '                            FROM ';
    $strSQL .= '                                DB2WSGP  ';
    $strSQL .= '                            WHERE GPSID IN ( ';
    $strSQL .= '                                SELECT DISTINCT  ';
    $strSQL .= '                                    GPMID AS WQGID  ';
    $strSQL .= '                                FROM  ';
    $strSQL .= '                                    DB2WSGP MGP ';
    $strSQL .= '                                INTERSECT ';
    $strSQL .= '                                SELECT ';
    $strSQL .= '                                    GPSID AS WQGID ';
    $strSQL .= '                                FROM ';
    $strSQL .= '                                    DB2WSGP ';
    $strSQL .= '                            ) ';
    $strSQL .= '                        ) ';
    $strSQL .= '                        UNION ';
    $strSQL .= '                        SELECT GPMID AS WQGID ';
    $strSQL .= '                        FROM DB2WSGP ';
    $strSQL .= '                        WHERE GPSID = ? ';
    $strSQL .= '                    )  ';
    $strSQL .= '                ) C  ';
    $strSQL .= '            WHERE '; 
    $strSQL .= '                C.WQGID <> \'\'       AND  ';
    $strSQL .= '                C.WQGID <> ?  ';
    $strSQL .= '            ORDER BY '; 
    $strSQL .= '                C.WQGID ';
    $strSQL .= '        )GPDATA  ';
    $strSQL .= '    LEFT JOIN ';
    $strSQL .= '        (SELECT ';
    $strSQL .= '            * ';
    $strSQL .= '        FROM ';
    $strSQL .= '            DB2WSGP ';
    $strSQL .= '        WHERE ';
    $strSQL .= '            GPMID = ? ';
    $strSQL .= '        )SGPDATA';
    $strSQL .= '    ON GPDATA.WQGID = SGPDATA.GPSID ';
    $strSQL .= '       )B ';
    $params = array($WQGID,$WQGID,$WQGID);
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '        WHERE B.WQGID IN ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                WUGID ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2WUGR ';
        $strSQL .= '            WHERE WUUID = ?  ';
        $strSQL .= '        )  ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= '    )  A ';
    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    e_log('データ取得1：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* グループ名取得
*-------------------------------------------------------*
*/

function fnGetWQUNAM($db2con,$id,$WUAUTH){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT A.WQUNAM ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '     ,   IFNULL(B.WUGID, \'\') AS WUGID ';
    }
    $strSQL .= ' FROM DB2WQGR AS A ' ;
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '        LEFT JOIN ';
        $strSQL .= '        ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                WUGID ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2WUGR ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                WUUID = ? ';
        $strSQL .= '        ) B ';
        $strSQL .= '    ON A.WQGID = B.WUGID ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= ' WHERE WQGID = ? ';
    array_push($params,$id);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)>0){
               if($WUAUTH === '3' || $WUAUTH === '4'){
                    if($data[0][WUGID] === ''){
                        $data = array('result' => 'ログインユーザーに指定したグループに対してのグループ権限はありません。');
                    }else{
                        $data = array('result' => true,'data' => $data);
                    }
                }else{
                    $data = array('result' => true,'data' => $data);
                }
            }else{
                $data = array('result' => 'NOTEXIST_GET');
            }
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* グループ存在チェック いたらtrue
* 一段階のグループならサブグループ追加できません。
*-------------------------------------------------------*
*/
function fnChkDB2SGP($db2con,$WQGID){
    $data = array();
    $rs = array();
    $strSQL .= '    SELECT     A.*, ';
    $strSQL .= '      CASE WHEN B.WQGID IS NULL THEN 1 ELSE 0 END AS SUBBTNFLG, ';
    $strSQL .= '      CASE WHEN S1GP.WQGID IS NULL THEN 0 ELSE 1 END AS S1GPFLG, ';
    $strSQL .= '      CASE WHEN MGP.WQGID IS NULL THEN 0 ELSE 1 END AS MGPFLG ';
    $strSQL .= '    FROM  DB2WQGR A ';
    $strSQL .= '    LEFT JOIN ';
    $strSQL .= '    ( ';
    $strSQL .= '        SELECT ';
    $strSQL .= '            GPSID WQGID ';
    $strSQL .= '        FROM ';
    $strSQL .= '            DB2WSGP ';
    $strSQL .= '        WHERE ';
    $strSQL .= '            GPMID  IN ';
    $strSQL .= '            ( ';
    $strSQL .= '                SELECT ';
    $strSQL .= '                    GPMID AS WQGID ';
    $strSQL .= '                FROM ';
    $strSQL .= '                    DB2WSGP ';
    $strSQL .= '                INTERSECT ';
    $strSQL .= '                SELECT ';
    $strSQL .= '                    GPSID AS WQGID ';
    $strSQL .= '                FROM ';
    $strSQL .= '                    DB2WSGP ';
    $strSQL .= '            ) ';
    $strSQL .= '    ) B ';
    $strSQL .= '    ON A.WQGID = B.WQGID  ';
    $strSQL .= '    LEFT JOIN ';
    $strSQL .= '    ( ';
    $strSQL .= '        ( ';
    $strSQL .= '            SELECT ';
    $strSQL .= '                GPMID AS WQGID ';
    $strSQL .= '            FROM ';
    $strSQL .= '                DB2WSGP ';
    $strSQL .= '            INTERSECT ';
    $strSQL .= '            SELECT ';
    $strSQL .= '                GPSID AS WQGID ';
    $strSQL .= '            FROM ';
    $strSQL .= '                DB2WSGP  ';
    $strSQL .= '        ) ';
    $strSQL .= '        UNION ';
    $strSQL .= '        ( ';
    $strSQL .= '            SELECT ';
    $strSQL .= '                GPSID AS WQGID ';
    $strSQL .= '            FROM ';
    $strSQL .= '                DB2WSGP ';
    $strSQL .= '        ) ';
    $strSQL .= '    ) S1GP  ';
    $strSQL .= '    ON A.WQGID = S1GP.WQGID ';
    $strSQL .= '    LEFT JOIN ';
    $strSQL .= '        ( ';
    $strSQL .= '            SELECT DISTINCT ';
    $strSQL .= '                GPMID AS WQGID ';
    $strSQL .= '            FROM ';
    $strSQL .= '                DB2WSGP ';
    $strSQL .= '            WHERE ';
    $strSQL .= '            GPMID IN ';
    $strSQL .= '                ( ';
    $strSQL .= '                    SELECT ';
    $strSQL .= '                        GPMID AS WQGID ';
    $strSQL .= '                    FROM ';
    $strSQL .= '                        DB2WSGP ';
    $strSQL .= '                ) ';
    $strSQL .= '        )  MGP ';
    $strSQL .= '    ON A.WQGID = MGP.WQGID   ';
    $strSQL .= '    WHERE A.WQGID      = ? ';
    $strSQL .= '    ORDER BY A.WQGID ';
    $params = array(
        $WQGID
    );
    e_log('VALID CHK SQL'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = array('result' => 'NOTEXIST_GET');
            }else{
                if($data[0]['SUBBTNFLG'] == 0){
                    $rs = array('result' => 'NOVLD_SGP');
                }else{
                    $rs = array('result' => true,'data'=> $data);
                }
            }
        }
    }
    return $rs;
}
