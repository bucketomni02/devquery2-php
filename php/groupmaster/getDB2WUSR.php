<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
//include_once ("../schedulemaster/edtDB2WSCD.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WQGID = $_POST['WQGID'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$wqunam = '';
$WUUID = $_POST['WUUID'];
$WUUNAM = $_POST['WUUNAM'];
$SEHFLAG = $_POST['SEHFLAG'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$WQGID = cmHscDe($WQGID);
$WUUID = cmHscDe($WUUID);
$WUUNAM = cmHscDe($WUUNAM);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('グループユーザー権限の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'5',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループユーザー権限の権限'));
            }
        }
    }
}
 // if($SEHFLAG != '0'){
	//グループ存在チェック
    
	if($rtn === 0){
	    $rs = fnCheckDB2WQGR($db2con,$WQGID);
	    if($rs === 'NOTEXIST_GET'){
	        $rtn = 3;
	        $msg = showMsg($rs,array('グループ'));
	    }else if($rs !== true){
	        $rtn = 1;
	        $msg = showMsg($rs,array('グループ'));
	    }
	}

	if($rtn === 0){
	    $res = fnGetAllCount($db2con,$WQGID,$WUUID,$WUUNAM);
	    if($res['result'] === true){
	        $allcount = $res['data'];
	        $res = fnGetDB2WUSR($db2con,$start,$length,$sort,$sortDir,$WQGID,$WUUID,$WUUNAM);
	        if($res['result'] === true){
	            $data = $res['data'];
	        }else{
	            $rtn = 1;
	            $msg = showMsg($res['result'],array('ユーザー'));
	        }
	    }else{
	        $rtn = 1;
	        $msg = showMsg($res['result']);
	    }
	}

	//グループ名取得
	if($rtn === 0){
	    $res = fnGetWQUNAM($db2con,$WQGID);
	    if($res['result'] === true){
	 //       if(count($res['data'])>0){
	            $wqunam = $res['data'][0]['WQUNAM'];
	 /*       }else{
	            //存在しないグループのチェック
	                $rtn = 1;
	                $msg = showMsg('NOTEXIST_GET',array('グループＩＤ'));
	        }*/
	    }else{
	        $rtn = 1;
	        $msg = showMsg($res['result'],array('グループＩＤ'));
	    }
	}
/* }else{
   
    if($rtn === 0){
        $res = fnUSERGetAllCount($db2con,$userData[0]['WUAUTH'],$USRID,$USRNAM);
        if($res['result'] === true){
            $allcount = $res['data'];
            $res = fnUSERGetDB2WQGR($db2con,$userData[0]['WUAUTH'],$WQGID,$USRID,$USRNAM,$start,$length,$sort,$sortDir);
            if($res['result'] === true){
                $data = $res['data'];
              
            }else{
                $rtn = 1;
                $msg = showMsg($res['result']);
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }  
 }  */

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'WQUNAM' => cmHsc($wqunam),
    'WUUID' => cmHsc($WUUID),
    'WUUNAM' => cmHsc($WUUNAM),
    'SEHFLAG' => cmHsc($SEHFLAG),
    'RTN' => $rtn,
    'MSG' => $msg,
    'licenseType' =>$licenseType
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* DB2WUSR取得
*-------------------------------------------------------*
*/

function fnGetDB2WUSR($db2con,$start = '',$length = '',$sort = '',$sortDir = '',$WQGID,$WUUID,$WUUNAM){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.WUUID ASC ';
    }

    $strSQL .= ' ) as rownum ';

    $strSQL .= ' FROM ';
    $strSQL .= ' ( ';
    $strSQL .= ' select C.*,case C.wugid when \'\' then \'0\' else \'1\' end as DEF from ';
    $strSQL .= ' ( ';
    $strSQL .= ' select D.wuuid,wuunam,ifnull(WUGID,\'\') as wugid ';
    $strSQL .= ' FROM DB2WUSR as D LEFT OUTER JOIN DB2WUGR as E ON D.WUUID = E.WUUID AND WUGID = ? '; 
    $strSQL .= ' ) as C ';

    $strSQL .= ' ) as B ';

    $strSQL .= ' WHERE WUUID <> \'\' ';
 
    array_push($params,$WQGID);

    if($WUUID != ''){
        $strSQL .= ' AND WUUID like ? ';
        array_push($params,'%'.$WUUID.'%');
    }

    if($WUUNAM != ''){
        $strSQL .= ' AND WUUNAM like ? ';
        array_push($params,'%'.$WUUNAM.'%');
    }
    
    
    $strSQL .= ' ) as A ';

   

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }

    $stmt = db2_prepare($db2con,$strSQL);
  
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
               if(count($data)=== 0){
                    $data = array('result' => 'NOTEXIST_GET');
                }else{
                    $data = array('result' => true,'data' => $data);
                }
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$WQGID,$WUUID='',$WUUNAM=''){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT count(A.WUUID) as COUNT ';
    $strSQL .= ' FROM DB2WUSR as A ' ;
    $strSQL .= ' WHERE WUUID <> \'\' ';
 
    //array_push($params,$WQGID);

    if($WUUID != ''){
        $strSQL .= ' AND WUUID like ? ';
        array_push($params,'%'.$WUUID.'%');
    }

    if($WUUNAM != ''){
        $strSQL .= ' AND WUUNAM like ? ';
        array_push($params,'%'.$WUUNAM.'%');
    }
    

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}

/*
*-------------------------------------------------------* 
* グループ名取得
*-------------------------------------------------------*
*/

function fnGetWQUNAM($db2con,$id){

    $data = array();

    $strSQL  = ' SELECT A.WQUNAM ';
    $strSQL .= ' FROM DB2WQGR AS A ' ;
    $strSQL .= ' WHERE WQGID = ? ';

    $params = array($id);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)>0){
                $data = array('result' => true,'data' => $data);
            }else{
                $data = array('result' => 'NOTEXIST_GET');
            }
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* グループ存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WQGR($db2con,$WQGID){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.WQGID ';
    $strSQL .= ' FROM DB2WQGR  AS A ' ;
    $strSQL .= ' WHERE A.WQGID = ? ' ;

    $params = array(
        $WQGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}


/*function fnUSERGetAllCount($db2con,$WUAUTH,$USRID,$USRNAM){
    $rs = array();
    $params = array();

    $strSQL  = ' SELECT count(A.WUUID) as COUNT ';
    $strSQL .= ' FROM DB2WUSR as A ' ;
    $strSQL .= ' WHERE WUUID <> \'\' ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '    AND WUUID IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            WUUID ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WUSR ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WUUID = ? ';
        $strSQL .= '    ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']); 
    }
  
    if($USRID != ''){
        $strSQL .= ' AND WUUID like ? ';
        array_push($params,'%'.$USRID.'%');
    }

    if($USRNAM != ''){
        $strSQL .= ' AND WUUNAM like ? ';
        array_push($params,'%'.$USRNAM.'%');
    }

    $stmt = db2_prepare($db2con,$strSQL);
   
    if($stmt === false){
        $rs = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rs[] = $row['COUNT'];
            }
            $rs = array('result' => true,'data' => $rs[0]);
        }
    }
    return $rs;

}

function fnUSERGetDB2WQGR($db2con,$WUAUTH,$WQGID,$USRID,$USRNAM,$start = '',$length = '',$sort = '',$sortDir = ''){

     $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.WUUID ASC ';
    }

    $strSQL .= ' ) as rownum ';

    $strSQL .= ' FROM ';
    $strSQL .= ' ( ';
    $strSQL .= ' select C.*,case C.wugid when \'\' then \'0\' else \'1\' end as DEF from ';
    $strSQL .= ' ( ';
    $strSQL .= ' select D.wuuid,wuunam,ifnull(WUGID,\'\') as wugid ';
    $strSQL .= ' FROM DB2WUSR as D LEFT OUTER JOIN DB2WUGR as E ON D.WUUID = E.WUUID AND WUGID = ? '; 
    $strSQL .= ' ) as C ';

    $strSQL .= ' ) as B ';

    $strSQL .= ' WHERE WUUID <> \'\' ';
    array_push($params,$WQGID);

    if($USRID != ''){
        $strSQL .= ' AND WUUID like ? ';
        array_push($params,'%'.$USRID.'%');
    }

    if($USRNAM != ''){
        $strSQL .= ' AND WUUNAM like ? ';
        array_push($params,'%'.$USRNAM.'%');
    }
    
    $strSQL .= ' ) as A ';

    

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }

    $stmt = db2_prepare($db2con,$strSQL);
 
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
               if(count($data)=== 0){
                    $data = array('result' => 'NOTEXIST_GET');
                }else{
                    $data = array('result' => true,'data' => $data);
                }
        }
    }
    return $data;

}   */

