<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$D1NAME = $_POST['D1NAME'];
$D1TEXT = $_POST['D1TEXT'];
$WQGID = $_POST['WQGID'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$wqunam = '';
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$WQGID = cmHscDe($WQGID);


$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'4',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループクエリー権限の権限'));
            }
        }
    }
}
//グループ存在チェック
if($rtn === 0){
    $rs = fnCheckDB2WQGR($db2con,$WQGID);
    if($rs === 'NOTEXIST_GET'){
        $rtn = 3;
        $msg = showMsg($rs,array('グループ'));
    }else if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('グループ'));
    }
}

if($rtn === 0){
    $res = fnGetAllCount($db2con,$userData[0]['WUAUTH'],$D1NAME,$D1TEXT,$WQGID,$licenseQueryGroup,$licenseSql);
    if($res['result'] !== true){
        $rtn = 1;
        $msg = showMsg($res['result']);
    }else{
        $allcount = $res['data'];
    }
}
if($rtn === 0){        
    $res = fnGetFDB2CSV1($db2con,$D1NAME,$D1TEXT,$userData[0]['WUAUTH'],$start,$length,$sort,$sortDir,$WQGID,$licenseQueryGroup,$licenseSql);
    if($res['result'] === true){
        $data = $res['data'];
        $res = fnGetWQUNAM($db2con,$WQGID,$userData[0]['WUAUTH']);
        if($res['result'] === true){
            $wqunam = $res['data'][0]['WQUNAM'];
        }else if($res['result'] === 'FAIL_SEL'){
            $rtn = 1;
            $msg = showMsg($res['result'],array('グループID'));
        }else{
            $rtn = 3;
            $msg = showMsg($res['result'],array('グループID'));
        }                
    }else{
        $rtn = 1;
        $msg = showMsg($res['result']);
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'WQUNAM' => cmHsc($wqunam),
    'RTN' => $rtn,
    'MSG' => $msg,
    'licenseType' => $licenseType
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con,$D1NAME,$D1TEXT,$WUAUTH,$start = '',$length = '',$sort = '',$sortDir = '',$WGGID,$licenseQueryGroup,$licenseSql){
   error_log('WUAUTH***'.print_r($WUAUTH,true));
    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.D1NAME ASC ';
    }

    $strSQL .= ' ) as rownum ';

    $strSQL .= ' FROM ';
    $strSQL .= ' ( ';
    $strSQL .= ' select C.*,case C.wggid when \'\' then \'0\' else \'1\' end as DEF ';
    $strSQL .= '   from ';
    $strSQL .= ' ( ';
    $strSQL .= ' select d1name,d1text,ifnull(WGGID,\'\') as wggid,WGNAME,BB.WGQGFLG ';
    $strSQL .= ' FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME,D1TEXT, ';
		$strSQL .= '			\'\' AS WGQGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
		$strSQL .= '	UNION ALL ';
		$strSQL .= '	SELECT DISTINCT D1NAME,D1TEXT,\'1\' AS WGQGFLG FROM ( ';
	    $strSQL .= '    SELECT  QRYGID AS D1NAME, ';
		$strSQL .= '	QRYGNAME AS D1TEXT ';
	   // $strSQL .= '    FROM DB2QRYG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') A';
        }
		$strSQL .= ' 		WHERE QRYGWUUID= ? ';
		$strSQL .= '	UNION ALL ';
		$strSQL .= '	SELECT QRYGID AS D1NAME, ';
		$strSQL .= '	QRYGNAME AS D1TEXT ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') A ';
        }	 
        $strSQL .= '    	WHERE QRYGID IN ( ';
	    $strSQL .= '        	SELECT DISTINCT ';
	    $strSQL .= '            	WGNAME ';
	    $strSQL .= '        	FROM ';
	    $strSQL .= '            DB2WGDF ';
	    $strSQL .= '        	WHERE ';
	    $strSQL .= '            	WGGID IN (SELECT ';
	    $strSQL .= '                            WUGID ';
	    $strSQL .= '                        FROM ';
	    $strSQL .= '                            DB2WUGR ';
	    $strSQL .= '                        WHERE ';
	    $strSQL .= '                            WUUID = ?  ';
	    $strSQL .= '                        ) ';
	    $strSQL .= '            AND WGQGFLG=\'1\' ';
	    $strSQL .= '    	) ) AB ';
        $strSQL .= '    ) BB ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);

    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME,D1TEXT, ';
		$strSQL .= '			\'\' AS WGQGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
		$strSQL .= '		UNION ALL ';
		$strSQL .= '		SELECT QRYGID AS D1NAME, ';
		$strSQL .= '		QRYGNAME AS D1TEXT, ';
		$strSQL .= '		\'1\' AS WGQGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') A';
        }		$strSQL .= '		WHERE QRYGWUUID= ? ';
        $strSQL .= '    ) BB ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
		$strSQL .= ' ( ';
		$strSQL .= '	SELECT D1NAME,D1TEXT, ';
		$strSQL .= '	\'\' AS WGQGFLG ';
		$strSQL .= ' 	FROM FDB2CSV1 ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= 'WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '	UNION ALL	';
		$strSQL .= '	SELECT QRYGID AS D1NAME, ';
		$strSQL .= '	QRYGNAME AS D1TEXT, ';
		$strSQL .= '	\'1\' AS WGQGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') A';
        }		
        $strSQL .= ' ) BB ';
        //$strSQL .= '     FDB2CSV1 AS B ';
    }
    $strSQL .= ' LEFT OUTER JOIN DB2WGDF ON D1NAME = WGNAME  ';
    $strSQL .= '    AND WGGID = ? '; 
    array_push($params,$WGGID);

    //$strSQL .= '    AND WGQGFLG<>\'1\' AND WGGID = ? '; 
    $strSQL .= ' ) as C ';

    /*$strSQL .=' UNION ALL ';
    $strSQL .='     SELECT G.QRYGID AS d1name,G.QRYGNAME AS d1text,ifnull(H.WGGID,\'\') ';
    $strSQL .='         ,ifnull(H.WGNAME,\'\'),case ifnull(H.WGGID,\'\') when \'\' then \'0\' else \'1\' end as DEF';
    $strSQL .='         ,case QRYGID when \'\' then \'\' else \'1\' end as WGQGFLG ';
    $strSQL .='     FROM DB2QRYG AS G ';
    $strSQL .='     LEFT OUTER JOIN DB2WGDF AS H ON ';
    $strSQL .='     G.QRYGID=H.WGNAME AND H.WGQGFLG=\'1\' AND H.WGGID=? ';*/
    $strSQL .= ' ) as B ';

    $strSQL .= ' WHERE D1NAME <> \'\' ';
	
	if($D1NAME != ''){
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params,'%'.$D1NAME.'%');
    }

    if($D1TEXT != ''){
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params,'%'.$D1TEXT.'%');
    }
    
    $strSQL .= ' ) as A ';

    //array_push($params,$WGGID);

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    error_log("クエリー情報取得：".$strSQL.'パラメータ：'.print_r($params,true)); 

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
                $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$WUAUTH,$D1NAME = '',$D1TEXT = '',$WGGID,$licenseQueryGroup,$licenseSql){
    $data = array();
    $params = array();
    $strSQL ='';
    $strSQL .=' SELECT COUNT(E.D1NAME) AS COUNT';
    $strSQL .= ' FROM (';
    $strSQL  .= '    SELECT D1NAME,D1TEXT';
    $strSQL .= '    FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME,D1TEXT ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
		$strSQL .= '	UNION ALL ';
		$strSQL .= '	SELECT DISTINCT D1NAME,D1TEXT FROM (';
	    $strSQL .= '    SELECT  QRYGID AS D1NAME, QRYGNAME AS D1TEXT  ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') A ';
        }		$strSQL .= ' 		WHERE QRYGWUUID= ? ';
		$strSQL .= '	UNION ALL ';
		$strSQL .= '	SELECT QRYGID AS D1NAME, QRYGNAME AS D1TEXT  ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') A ';
        }	    
        $strSQL .= '    	WHERE QRYGID IN ( ';
	    $strSQL .= '        	SELECT DISTINCT ';
	    $strSQL .= '            	WGNAME ';
	    $strSQL .= '        	FROM ';
	    $strSQL .= '            DB2WGDF ';
	    $strSQL .= '        	WHERE ';
	    $strSQL .= '            	WGGID IN (SELECT ';
	    $strSQL .= '                            WUGID ';
	    $strSQL .= '                        FROM ';
	    $strSQL .= '                            DB2WUGR ';
	    $strSQL .= '                        WHERE ';
	    $strSQL .= '                            WUUID = ?  ';
	    $strSQL .= '                        ) ';
	    $strSQL .= '            AND WGQGFLG=\'1\' ';
	    $strSQL .= '    	) ) AB';
        $strSQL .= '    ) B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        /*$strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '        ) ';*/
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME,D1TEXT ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
		$strSQL .= '		UNION ALL ';
		$strSQL .= '		SELECT QRYGID AS D1NAME, QRYGNAME AS D1TEXT ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS A';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') A';
        }
		$strSQL .= '		WHERE QRYGWUUID= ? ';
        $strSQL .= '    ) B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);

    }else{
		$strSQL .= ' ( ';
		$strSQL .= '	SELECT D1NAME,D1TEXT ';
		$strSQL .= ' 	FROM FDB2CSV1 ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= ' WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '	UNION ALL	';
		$strSQL .= '	SELECT QRYGID AS D1NAME, QRYGNAME AS D1TEXT ';
		if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') A ';
        }
		$strSQL .= ' ) B';
        //$strSQL .= '  (SELECT D1NME FROM FDB2CSV1 ) B';
    }
    /*$strSQL .= ' UNION ALL ' ;
    $strSQL .= '    SELECT QRYGID AS D1NAME ';
    $strSQL .= '    FROM DB2QRYG ';*/
    $strSQL .=' ) E ';

    $strSQL .= ' WHERE D1NAME <> \'\' ';
	if($D1NAME != ''){
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params,'%'.$D1NAME.'%');
    }

    if($D1TEXT != ''){
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params,'%'.$D1TEXT.'%');
    }

    e_log("クエリー情報取得カウント：".$strSQL.'パラメータ：'.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}

/*
*-------------------------------------------------------* 
* グループ名取得
*-------------------------------------------------------*
*/

function fnGetWQUNAM($db2con,$id,$WUAUTH){

    $data = array();
    $params = array();
    /*$strSQL  = ' SELECT A.WQUNAM ';
    $strSQL .= ' FROM DB2WQGR AS A ' ;
    $strSQL .= ' WHERE WQGID = ? ';*/
    $strSQL .= '    SELECT ';
    $strSQL .= '        A.WQUNAM ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '     ,   IFNULL(B.WUGID, \'\') AS WUGID ';
    }
    $strSQL .= '    FROM ';
    $strSQL .= '        DB2WQGR AS A ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '        LEFT JOIN ';
        $strSQL .= '        ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                WUGID ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2WUGR ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                WUUID = ? ';
        $strSQL .= '        ) B ';
        $strSQL .= '    ON A.WQGID = B.WUGID ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= '    WHERE ';
    $strSQL .= '        WQGID = ? ';
    array_push($params,$id);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
            e_log($strSQL.print_r($params,true).db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
            e_log($strSQL.print_r($params,true).db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
           if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET');
             }else{
                if($WUAUTH === '3' || $WUAUTH === '4'){
                    if($data[0][WUGID] === ''){
                        $data = array('result' => 'ログインユーザーに指定したグループに対してのグループ権限はありません。');
                    }else{
                        $data = array('result' => true,'data' => $data);
                    }
                }else{
                    $data = array('result' => true,'data' => $data);
                }
            }
        }
    }
    return $data;
}


/*
*-------------------------------------------------------* 
* グループ存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WQGR($db2con,$WQGID){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.WQGID ';
    $strSQL .= ' FROM DB2WQGR  AS A ' ;
    $strSQL .= ' WHERE A.WQGID = ? ' ;

    $params = array(
        $WQGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}
