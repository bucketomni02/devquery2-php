<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WGGID = cmHscDe($_POST['WGGID']);
$data = json_decode($_POST['DATA'],true);
$rtn = 0;
$msg = '';
$queryData=array();

/*
*-------------------------------------------------------* 
* 権限更新処理
*-------------------------------------------------------*
*/

$checkRs = false;
$defRs = false;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'4',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループクエリー権限の権限'));
            }
        }
    }
}
//グループ存在チェック
if($rtn === 0){
    $rs = fnCheckDB2WQGR($db2con,$WGGID,$userData[0]['WUAUTH']);
    if($rs === 'NOTEXIST_GET'){
        $rtn = 3;
        $msg = showMsg($rs,array('グループ'));
    }else if ($rs === 'FAILACC'){
        $rtn = 3;
        $msg = showMsg('ログインユーザーに指定したグループに対してのグループクエリー権限はありません。');
    }else if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('グループ'));
    }
}
//ブックマークのためクエリーリスト
if($rtn === 0){
    $r = cmGETQRY_DB2WGDF($db2con,$WGGID);
    if($r['result'] !== true){
        $rtn = 1;
        $msg = showMsg($r['result']);
    }else{
        $queryData=umEx($r['data']);
    }
}
if($rtn === 0){
    foreach($data as $page){
        foreach($page as $key => $value){
            $D1NAME = cmHscDe($value['D1NAME']);
            $WGQGFLG=cmHscDe($value['WGQGFLG']);
            /** クエリー存在チェック**/        
            $chkQry = array();
            if($WGQGFLG==='1'){
                $chkQry=cmChkDB2QRYG($db2con,$D1NAME);
                if($chkQry['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($chkQry['result'],array('クエリーグループ'));
                    break;
                }
            }else{
                $chkQry = cmChkQuery($db2con,'',$D1NAME,'');
                if($chkQry['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($chkQry['result'],array('クエリー'));
                    break;
                }
            }
            if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
                if($WGQGFLG !=='1'){
                    $qryUsrChk = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
                    if($qryUsrChk === 'NOTEXIST_GET'){
                        $rtn = 1;
                        $msg = 'ログインユーザーに指定したクエリーに対してのグループクエリー権限はありません。';
                        break;
                    }else if($qryUsrChk !== true){
                        $rtn = 1;
                        $msg = showMsg($qryUsrChk,array('クエリー'));
                        break;
                    }
                }
            }
            //権限ありにした場合
            if($value['DEF'] === "1"){

                //存在確認
                $checkRs = fnCheckDB2WGDF($db2con,$WGGID,$D1NAME,$WGQGFLG);

                //なかったらインサート
                if($checkRs['result'] === true){
                    $checkRs = $checkRs['data'];
                }else{
                    $rtn = 1;
                    $msg = showMsg($checkRs['result']);
                    break;
                }
                if($rtn === 0 && ($checkRs === 0)){
                    $defRs = fnInsertDB2WGDF($db2con,$WGGID,$D1NAME,$WGQGFLG);
                    if($defRs !== true){
                        $rtn = 1;
                        $msg = showMsg($defRs);
                        break;
                    }
                }
            //権限なしにした場合
            }else if($value['DEF'] === "0"){

                //存在確認
                $checkRs = fnCheckDB2WGDF($db2con,$WGGID,$D1NAME,$WGQGFLG);

                //あったら削除
                if($checkRs['result'] === true){
                    $checkRs = $checkRs['data'];
                }else{
                    $rtn = 1;
                    $msg = showMsg($checkRs['result']);
                    break;
                }
                if($rtn === 0 && ($checkRs > 0)){
                    $defRs = fnDeleteDB2WGDF($db2con,$WGGID,$D1NAME,$WGQGFLG);
                    if($defRs !== true){
                        $rtn = 1;
                        $msg = showMsg($defRs);
                        break;
                    }else{
                        $rs = fnDeleteBmkQry($db2con,$WGGID,$D1NAME);
                    }
                }
                //グループ権限付与時、ダウンロード権限自動付与
                if (CND_QRYGD === "1" ) {
                    $rsUsr = cmGetUsrName($db2con,$WGGID);
                    if ($rsUsr['result'] !== 'NOTEXIST_GET') {
                        foreach($rsUsr['data'] as $usrNm){
                            $chk = cmChkQryKenGen($db2con,$usrNm['WUUID'],$D1NAME);
                            if($chk['result'] === 'NOTEXIST_GET'){
                                $rss = cmUpdQryKengen($db2con,$usrNm['WUUID'],$D1NAME,'','');
                            }
                        }
                    }
                }
            }
        }
    }
}
//グループ権限付与時、ダウンロード権限自動付与(GI)
if ($rtn === 0) {
   if (CND_QRYGD === "1" ) {
        $rsUsr = cmGetUsrName($db2con,$WGGID);
        if ($rsUsr['result'] !== 'NOTEXIST_GET') {
            $rsQry = cmGetQryName($db2con,$WGGID);
            if ($rsQry['result'] !== 'NOTEXIST_GET') {
                foreach($rsUsr['data'] as $usrNm){
                    foreach ($rsQry['data'] as $qryNm) {
                        $name = cmGetQryNameFromDB2WDEF($db2con,$usrNm['WUUID'],$qryNm['WGNAME']);
                        if ($name['result'] === 'NOTEXIST_GET') {
                            $rss = cmInsQryKengen($db2con,$usrNm['WUUID'],$qryNm['WGNAME'],'1','1');
                            if($rss['result'] !== true){
                                $rtn = 1;
                                $msg = $rss['result'];
                                break;
                            }
                        }else{
                            $rss = cmUpdQryKengen($db2con,$usrNm['WUUID'],$qryNm['WGNAME'],'1','1');
                            if($rss['result'] !== true){
                                $rtn = 1;
                                $msg = $rss['result'];
                                break;
                            }
                        }
                    }
                }
            }
        }
   }
}

//ブックマークのため削除
if($rtn === 0){
    if(count($queryData)>0){
        foreach($queryData as $value){
            $r=cmCountDB2WGDF_DB2WUGR($db2con,$value['WGNAME']);
            if($r['result']!==true){
                $rtn=1;
                $msg=$r['result'];
                break;
            }else{
                if($r['TOTALCOUNT']<=0){
                    $r=cmDelDB2BMK($db2con,'',$value['WGNAME'],2);
                    if($r['result']!==true){
                        $rtn=1;
                        $msg=$r['result'];
                        break;
                    }
                }
            }
        }
    }
}

if($rtn !== 0){
    db2_rollback($db2con);
}else{
    db2_commit($db2con);
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'checkRs' => $checkRs,
    'defRs' => $defRs,
    'DATA' => $data,
    'WGGID' => $WGGID,
    'MSG' => $msg,
    'RTN' => $rtn
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ユーザー情報取得
*-------------------------------------------------------*
*/

function fnCheckDB2WGDF($db2con,$WGGID,$WGNAME,$WGQGFLG){

    $data = array();
    $params=array();
    $strSQL  = ' SELECT A.WGGID ';
    $strSQL .= ' FROM DB2WGDF AS A ' ;
    $strSQL .= ' WHERE A.WGGID = ? ' ;
    $strSQL .= ' AND A.WGNAME = ? ' ;
    $strSQL .= ' AND A.WGQGFLG =? ';
    $params[]=$WGGID;
    $params[]=$WGNAME;
    $params[]=$WGQGFLG;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => count($data));
        }            
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 定義追加
*-------------------------------------------------------*
*/

function fnInsertDB2WGDF($db2con,$WGGID,$WGNAME,$WGQGFLG){
   
    $WGNRFLG = '0';
    $rs = true;
    $params=array();
    //構文
    $strSQL  = ' INSERT INTO DB2WGDF ';
    $strSQL .= ' ( ';
    $strSQL .= '    WGGID, ';
    $strSQL .= '    WGNAME, ';
    $strSQL .= '    WGQGFLG,';
    $strSQL .= '    WGNRFLG ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ' ;
    $strSQL .= ' ( ?, ?, ?, ? ) ';
    
    $params[]=$WGGID;
    $params[]=$WGNAME;
    $params[]=$WGQGFLG;
    $params[]=$WGNRFLG;

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* 定義削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WGDF($db2con,$WGGID,$WGNAME,$WGQGFLG){

    $rs = true;
    $params=array();
    //構文
    $strSQL  = ' DELETE FROM DB2WGDF ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WGGID = ? ';
    $strSQL .= ' AND WGNAME = ? ';
    $strSQL .= ' AND WGQGFLG = ? ';
    $params[]=$WGGID;
    $params[]=$WGNAME;
    $params[]=$WGQGFLG;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === fasle){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}

/****
*自分が抜いたグループからユーザー取得
*取得されたユーザーでクエリーがお気に入りにあったら削除
****/
function fnDeleteBmkQry($db2con,$WGGID,$D1NAME){
    $data = array();
    $qryData = array();
    $strSQL = '';
    $strSQL .= ' SELECT * FROM DB2WUGR WHERE WUGID = ? ';
    $params = array($WGGID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === fasle){
            $rs = 'FAIL_DEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $qryData[] = $row;
            }
            if(count($qryData) > 0 ){//他のグループに同じクエリー保存されてるかどうかチェック
                $strSQL = '';
                $strSQL .= ' SELECT * FROM ';
                $strSQL .= ' (SELECT * FROM DB2WGDF WHERE WGGID <> ?) AS A ';
                $strSQL .= ' LEFT JOIN ';
                $strSQL .= ' (SELECT * FROM DB2WUGR  WHERE WUUID = ? AND WUGID <> ?) AS B ';
                $strSQL .= ' ON A.WGGID = B.WUGID';
                $strSQL .= ' WHERE A.WGNAME = ? ';
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = 'FAIL_DEL';
                }else{
                    foreach($qryData as  $key => $value){
                        $USERID = $value['WUUID'];
                        $params = array($WGGID,$USERID,$WGGID,$D1NAME);
                        $r = db2_execute($stmt,$params);
                        if($r === fasle){
                            $rs = 'FAIL_DEL';
                            break;
                        }else{
                            while($row = db2_fetch_assoc($stmt)){
                                $data[] = $row;
                            }
                            if(count($data) === 0 ){
                                   $rs = cmDeleteDB2QBMK($db2con,'',$D1NAME,'','',$USERID);
                            }
                        }
                        //$rs = cmDeleteDB2QBMK($db2con,'',$D1NAME,'','',$WUUID);
                    }
                }
            }
        }
    }
    return $rs;
}
/*
*-------------------------------------------------------* 
* グループ存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WQGR($db2con,$WQGID,$WUAUTH){

    $data   = array();
    $rs     = true;
    $params = array();

    $strSQL  = ' SELECT A.WQGID ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '    , IFNULL(B.WUGID, \'\') AS WUGID ';
    }
    $strSQL .= ' FROM DB2WQGR  AS A ' ;
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '    LEFT JOIN ';
        $strSQL .= '        ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                WUGID ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2WUGR ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                WUUID = ? ';
        $strSQL .= '        ) B ';
        $strSQL .= '    ON A.WQGID = B.WUGID ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= '    WHERE ';
    $strSQL .= '        WQGID = ? ';
    array_push($params,$WQGID);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }else{
                if($data[0]['WUGID'] === ''){
                    $rs = 'FAILACC';
                }
            }
        }
    }
    return $rs;
}

function fnChkQryUsr($db2con,$QRYNM,$WUAUTH){
    $data = array();
    $params = array();
    $rs = true;
    $strSQL .= '';
    $strSQL .= '        SELECT ';
    $strSQL .= '            * ';
    $strSQL .= '        FROM ';
    $strSQL .= '            FDB2CSV1 ';
    if($WUAUTH === '3'){
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= '        AND D1NAME = ? ';
    array_push($params,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
        e_log('エラー①：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'FAILACC';
            }
        }
    }
    return $rs;
}