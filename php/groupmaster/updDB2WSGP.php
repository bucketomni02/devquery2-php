<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$GPMID = cmHscDe($_POST['GPMID']);
$GPFLG = cmHscDe($_POST['GPFLG']);
$data = json_decode($_POST['DATA'],true);
$rtn = 0;
$msg = '';

/*
*-------------------------------------------------------* 
* 権限更新処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'2',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループ作成の権限'));
            }
        }
    }
}
//グループ存在チェック
if($rtn === 0){
    $rs = fnCheckDB2WQGR($db2con,$GPMID,$userData[0]['WUAUTH']);
    if($rs === 'NOTEXIST_GET'){
        $rtn = 3;
        $msg = showMsg($rs,array('グループ'));
    }else if($rs === 'FAIL_ACC'){
        $rtn = 3;
        $msg = showMsg('指定したグループに対してのグループ権限はログインユーザーにありません。');
    }else if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('グループ'));
		$msg = 1;
    }
}
if($rtn === 0){
    foreach($data as $value){
        $GPSID = cmHscDe($value['GPSID']);
        // クエリー存在チェック
        $chkGp = array();
        $chkGp = fnCheckDB2WQGR($db2con,$GPSID,$userData[0]['WUAUTH']);
        if($chkGp === 'NOTEXIST_GET'){
			$msg = 2;
            $rtn = 1;
            $msg = showMsg($chkGp,array('グループ'));
            break;
        }else if($chkGp === 'FAIL_ACC'){
            $rtn = 1;
            $msg = showMsg('指定したグループに対してのグループ権限はログインユーザーにありません。');
            break;
        }else if($chkGp !== true){
            $rtn = 1;
            $msg = showMsg($chkGp,array('グループ'));
            break;
        }
		/************必要かな【他のグループあれば正しく動く】************/
        if($rtn === 0){
            if($GPFLG == 0 ){
                $vldGp = fnChkVldSGP($db2con,$GPMID,$GPSID);
                if($vldGp !== true){
                    $rtn = 1;
                    $msg = showMsg($vldGp,array('グループ'));
					//$msg .= 'aye';
                    break;
                }
            }else if ($GPFLG == 1){
                $vldGp = fnChkVldSGP1($db2con,$GPMID,$GPSID);
                if($vldGp !== true){
                    $rtn = 1;
                    $msg = showMsg($vldGp,array('グループ'));
					//$msg .= 'kyaw';
                    break;
                }
            }
        }
		/************必要かな************/

        /*// 保存できるグループのチェック
        if($GPFLG == 0 ){
            //一段階として登録できるチェック
            $vldGp = fnChkVldS1GP($db2con,$GPMID,$GPSID);
            if($vldGp !== true){
                $rtn = 1;
                $msg = showMsg($vldGp,array('グループ'));
                break;
            }
        }else{
            //二段階として登録できるチェック
            $vldGp = fnChkVldS2GP($db2con,$GPMID,$GPSID);
            if($vldGp !== true){
                $rtn = 1;
                $msg = showMsg($vldGp,array('グループ'));
                break;
            }        
        }*/

        //権限ありにした場合
        if($rtn === 0){
            if($value['GPSIDFLG'] === "1"){
                //存在確認
                $checkRs = fnCheckDB2WSGP($db2con,$GPMID,$GPSID);
                //なかったらインサート
                if($checkRs['result'] === true){
                    $checkRs = $checkRs['data'];
                }else{
                    $rtn = 1;
                    $msg = showMsg($checkRs['result']);
                    break;
                }
                if($rtn === 0 && ($checkRs === 0)){
                    $insRs = fnInsertDB2WSGP($db2con,$GPMID,$GPSID);
                    if($insRs !== true){
                        $rtn = 1;
                        $msg = showMsg($insRs);
                        break;
                    }
                }
            //権限なしにした場合
            }else if($value['GPSIDFLG'] === "0"){

                //存在確認
                $checkRs = fnCheckDB2WSGP($db2con,$GPMID,$GPSID);

                //あったら削除
                if($checkRs['result'] === true){
                    $checkRs = $checkRs['data'];
                }else{
                    $rtn = 1;
                    $msg = showMsg($checkRs['result']);
                    break;
                }
                if($rtn === 0 && ($checkRs > 0)){
                    $delRs = fnDeleteDB2WSGP($db2con,$GPMID,$GPSID);
                    if($delRs !== true){
                        $rtn = 1;
                        $msg = showMsg($delRs);
                        break;
                    }
                }
            }
        }
    }
}

if($rtn !== 0){
    db2_rollback($db2con);
}else{
    db2_commit($db2con);
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'DATA' => $data,
    'GPMID' => $GPMID,
    'MSG' => $msg,
    'RTN' => $rtn
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ユーザー情報取得
*-------------------------------------------------------*
*/

function fnCheckDB2WSGP($db2con,$GPMID,$GPSID){

    $data = array();

    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM DB2WSGP AS A ' ;
    $strSQL .= ' WHERE A.GPMID = ? ' ;
    $strSQL .= ' AND A.GPSID = ? ' ;

    $params = array(
        $GPMID,
        $GPSID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => count($data));
        }            
    }
    return $data;
}
function fnChkVldSGP1 ($db2con,$GPMID,$GPSID){
    $rs = true;
    $data = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '     * ';
    $strSQL .= ' FROM ';
    $strSQL .= '     ( ';
    $strSQL .= '    SELECT GPDATA.* ';
    $strSQL .= '    FROM  ';
    $strSQL .= '        ( ';
    $strSQL .= '                     SELECT  ';
    $strSQL .= '                        C.*  ';
    $strSQL .= '                    FROM  ';
    $strSQL .= '                        (SELECT  ';
    $strSQL .= '                            *  ';
    $strSQL .= '                        FROM  ';
    $strSQL .= '                            DB2WQGR  ';
    $strSQL .= '                        WHERE  ';
    $strSQL .= '                            WQGID NOT IN ( ';
    $strSQL .= '                                    SELECT  ';
    $strSQL .= '                                        GPSID ';
    $strSQL .= '                                    FROM ';
    $strSQL .= '                                        DB2WSGP  ';
    $strSQL .= '                                    WHERE GPSID IN ( ';
    $strSQL .= '                                        SELECT DISTINCT  ';
    $strSQL .= '                                            GPMID AS WQGID  ';
    $strSQL .= '                                        FROM  ';
    $strSQL .= '                                            DB2WSGP MGP ';
    $strSQL .= '                                        INTERSECT ';
    $strSQL .= '                                        SELECT ';
    $strSQL .= '                                            GPSID AS WQGID ';
    $strSQL .= '                                        FROM ';
    $strSQL .= '                                            DB2WSGP ';
    $strSQL .= '                                    ) ';
    $strSQL .= '                                    UNION ';
    $strSQL .= '                                    SELECT GPMID AS WQGID ';
    $strSQL .= '                                    FROM DB2WSGP ';
    $strSQL .= '                            )  ';
    $strSQL .= '                        ) C  ';
    $strSQL .= '                    WHERE  ';
    $strSQL .= '                        C.WQGID <> \'\'       AND  ';
    $strSQL .= '                        C.WQGID <> ?  ';
    $strSQL .= '                    ORDER BY  ';
    $strSQL .= '                        C.WQGID ';
    $strSQL .= '                )GPDATA  ';
    $strSQL .= '         )A ';
    $strSQL .= ' WHERE A.WQGID <> \'\' ';
    $strSQL .= ' AND A.WQGID <> ? ';
    $params = array($GPMID,$GPSID);
    e_log('実行SQL267：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs =  'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)==0){
                $rs = 'NOVLD_SGP';
            }
        }
    }
    return $rs;
}
function fnChkVldSGP($db2con,$GPMID,$GPSID){
    $rs = true;
    $data = array();
    $strSQL .= ' SELECT ';
    $strSQL .= '     * ';
    $strSQL .= ' FROM ';
    $strSQL .= '     ( ';
    $strSQL .= '    SELECT GPDATA.* ';
    $strSQL .= '    FROM  ';
    $strSQL .= '        ( ';
    $strSQL .= '            SELECT  ';
    $strSQL .= '                C.*  ';
    $strSQL .= '            FROM  ';
    $strSQL .= '                (SELECT  ';
    $strSQL .= '                    *  ';
    $strSQL .= '                FROM  ';
    $strSQL .= '                    DB2WQGR  ';
    $strSQL .= '                WHERE  ';
    $strSQL .= '                    WQGID NOT IN ( ';
    $strSQL .= '                        ( ';
    $strSQL .= '                            SELECT  ';
    $strSQL .= '                                GPMID ';
    $strSQL .= '                            FROM ';
    $strSQL .= '                                DB2WSGP  ';
    $strSQL .= '                            WHERE GPSID IN ( ';
    $strSQL .= '                                SELECT DISTINCT  ';
    $strSQL .= '                                    GPMID AS WQGID  ';
    $strSQL .= '                                FROM  ';
    $strSQL .= '                                    DB2WSGP MGP ';
    $strSQL .= '                                INTERSECT ';
    $strSQL .= '                                SELECT ';
    $strSQL .= '                                    GPSID AS WQGID ';
    $strSQL .= '                                FROM ';
    $strSQL .= '                                    DB2WSGP ';
    $strSQL .= '                            ) ';
    $strSQL .= '                        ) ';
    $strSQL .= '                        UNION ';
    $strSQL .= '                        SELECT GPMID AS WQGID ';
    $strSQL .= '                        FROM DB2WSGP ';
    $strSQL .= '                        WHERE GPSID = ? ';
    $strSQL .= '                    )  ';
    $strSQL .= '                ) C  ';
    $strSQL .= '            WHERE '; 
    $strSQL .= '                C.WQGID <> \'\'       AND  ';
    $strSQL .= '                C.WQGID <> ?  ';
    $strSQL .= '            ORDER BY '; 
    $strSQL .= '                C.WQGID ';
    $strSQL .= '        )GPDATA  ';
    $strSQL .= '         )A ';
    $strSQL .= ' WHERE A.WQGID <> \'\' ';
    $strSQL .= ' AND A.WQGID <> ? ';
    $params = array($GPMID,$GPMID,$GPSID);
    e_log('実行SQL338：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs =  'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            /*if(count($data) < 2){
                $rs = true;
            }else{*/
            if(count($data) === 0){
                $rs = 'NOVLD_SGP';
			}
        }
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* 定義追加
*-------------------------------------------------------*
*/

function fnInsertDB2WSGP($db2con,$GPMID,$GPSID){


    $rs = true;

    //構文
    $strSQL  = ' INSERT INTO DB2WSGP ';
    $strSQL .= ' ( ';
    $strSQL .= ' GPMID, ';
    $strSQL .= ' GPSID ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ' ;
    $strSQL .= ' (?,?) ';

    $params = array(
        $GPMID,
        $GPSID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* 定義削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WSGP($db2con,$GPMID,$GPSID){


    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WSGP ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' GPMID = ? ';
    $strSQL .= ' AND GPSID = ? ';

    $params = array(
        $GPMID,
        $GPSID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === fasle){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}
/*
*-------------------------------------------------------* 
* グループ存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WQGR($db2con,$GPMID,$WUAUTH){

    $data = array();
    $rs = true;
    $params = array();

    $strSQL  = ' SELECT A.WQGID ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '    ,IFNULL(B.WUGID, \'\') AS WUGID';
    }
    $strSQL .= ' FROM DB2WQGR  AS A ' ;
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '    LEFT JOIN  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            WUGID ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WUGR ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WUUID = ? ';
        $strSQL .= '    ) B ';
        $strSQL .= '    ON A.WQGID = B.WUGID ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']); 
    }
    $strSQL .= ' WHERE A.WQGID = ? ' ;
    array_push($params,$GPMID); 
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }else{
                if($WUAUTH === '3' || $WUAUTH === '4'){
                    if($data[0]['WUGID'] === ''){
                        $rs = 'FAIL_ACC';
                    }
                }
            }
        }
    }
    return $rs;
}

// check for 二段階のサブグループ
function fnChkVldS2GP($db2con,$GPMID,$GPSID){
    $data = array();
    $rs = true;

    $strSQL .= ' SELECT ';
    $strSQL .= '     A.* ';
    $strSQL .= ' FROM ';
    $strSQL .= '     ( ';
    $strSQL .= '         SELECT ';
    $strSQL .= '             * ';
    $strSQL .= '         FROM ';
    $strSQL .= '             DB2WQGR ';
    $strSQL .= '         WHERE WQGID NOT IN ( SELECT DISTINCT GPMID AS WQGID  FROM DB2WSGP MGP ) ';
    $strSQL .= '         )A ';
    $strSQL .= ' WHERE A.WQGID <> \'\' ';
    $strSQL .= ' AND A.WQGID <> ? ';
    $strSQL .= ' AND A.WQGID = ? ';
    $params = array($GPMID,$GPSID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs =  'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)==0){
                $rs = 'NOVLD_SGP';
            }
        }
    }
    return $rs;
}
// check for 一段階のサブグループ
function fnChkVldS1GP($db2con,$GPMID,$GPSID){
    $data = array();
    $rs = true;

    $strSQL .= ' SELECT ';
    $strSQL .= '     A.* ';
    $strSQL .= ' FROM ';
    $strSQL .= '     (SELECT ';
    $strSQL .= '         * ';
    $strSQL .= '     FROM ';
    $strSQL .= '         DB2WQGR ';
    $strSQL .= '     WHERE ';
    $strSQL .= '         WQGID NOT IN ';
    $strSQL .= '          (SELECT DISTINCT ';
    $strSQL .= '             GPMID AS WQGID ';
    $strSQL .= '         FROM ';
    $strSQL .= '             DB2WSGP ';
    $strSQL .= '         WHERE ';
    $strSQL .= '             GPMID NOT IN ';
    $strSQL .= '                 (SELECT ';
    $strSQL .= '                     GPMID AS WQGID ';
    $strSQL .= '                 FROM ';
    $strSQL .= '                     DB2WSGP ';
    $strSQL .= '                 INTERSECT ';
    $strSQL .= '                 SELECT ';
    $strSQL .= '                     GPSID AS WQGID ';
    $strSQL .= '                 FROM ';
    $strSQL .= '                     DB2WSGP ';
    $strSQL .= '                 ) ';
    $strSQL .= '             ) ';
    $strSQL .= '         ) A ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     A.WQGID <> \'\'  AND ';
    $strSQL .= '     A.WQGID <> ?  AND';
    $strSQL .= '     A.WQGID = ? ';
    $params = array($GPMID,$GPSID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs =  'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)==0){
                $rs = 'NOVLD_SGP';
            }
        }
    }
    return $rs;
}