<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WUGID = cmHscDe($_POST['WUGID']);
$DEF = cmHscDe($_POST['DEF']);
$data = json_decode($_POST['DATA'],true);
$rtn = 0;
$msg = '';
$queryData=array();

/*
*-------------------------------------------------------* 
* 権限更新処理
*-------------------------------------------------------*
*/

$checkRs = false;
$defRs = false;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'5',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループユーザー権限の権限'));
            }
        }
    }
}
//グループ存在チェック
if($rtn === 0){
    $rs = fnCheckDB2WQGR($db2con,$WUGID);
    if($rs === 'NOTEXIST_GET'){
        $rtn = 3;
        $msg = showMsg($rs,array('グループ'));
    }else if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('グループ'));
    }
}
//ブックマークのためクエリーリスト
if($rtn === 0){
    $r = cmGETQRY_DB2WGDF($db2con,$WUGID);
    if($r['result'] !== true){
        $rtn = 1;
        $msg = showMsg($r['result']);
    }else{
        $queryData=umEx($r['data']);
    }
}
if($rtn === 0){
    foreach($data as $value){
        //foreach($page as $key => $value){
            $WUUID = cmHscDe($value['WUUID']);
            //ユーザが存在チェック
            if($rtn === 0){
                $rs = cmCheckUser($db2con,$WUUID);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result'],array('ユーザー'));
                    break;
                }
            }
            //権限ありにした場合
            if($value['DEF'] === "1"){//value['DEF']

                //存在確認
                $checkRs = fnCheckDB2WUGR($db2con,$WUGID,$WUUID);
                if($checkRs['result'] === true){
                    $checkRs = $checkRs['data'];
                }else{
                    $rtn = 1;
                    $msg = showMsg($checkRs['result']);
                    break;
                }
                if($rtn === 0 && ($checkRs === 0)){
                    $defRs = fnInsertDB2WUGR($db2con,$WUGID,$WUUID);
                    if($defRs !== true){
                        $rtn = 1;
                        $msg = showMsg($defRs);
                        break;
                    }
                }
            //権限なしにした場合
            }else if($value['DEF'] === "0"){//value['DEF']

                //存在確認
                $checkRs = fnCheckDB2WUGR($db2con,$WUGID,$WUUID);

                //あったら削除
                if($checkRs['result'] === true){
                    $checkRs = $checkRs['data'];
                }else{
                    $rtn = 1;
                    $msg = showMsg($checkRs['result']);
                    break;
                }
                if($rtn === 0 && ($checkRs > 0)){
                    $defRs = fnDeleteDB2WUGR($db2con,$WUGID,$WUUID);
                    if($defRs !== true){
                        $rtn = 1;
                        $msg = showMsg($defRs);
                        break;
                    }else{
                        $rs = fnDeleteBmkQry($db2con,$WUUID,$WUGID);
                    }
                }
                 //グループ権限付与時、ダウンロード権限自動付与
                if (CND_QRYGD === "1" ) {
                    $rsQry = cmGetQryName($db2con,$WUGID);
                    if ($rsQry['result'] !== 'NOTEXIST_GET') {
                        foreach($rsQry['data'] as $qryNm){
                            $chk = cmChkQryKenGen($db2con,$WUUID,$qryNm['WGNAME']);
                            if($chk['result'] === 'NOTEXIST_GET'){
                                $rss = cmUpdQryKengen($db2con,$WUUID,$qryNm['WGNAME'],'','');
                            }
                        }
                    }
                }
            }
        //}
    }
}
//グループ権限付与時、ダウンロード権限自動付与(GI)
if ($rtn === 0) {
   if (CND_QRYGD === "1" ) {
        $rsUsr = cmGetUsrName($db2con,$WUGID);
        if ($rsUsr['result'] !== 'NOTEXIST_GET') {
            $rsQry = cmGetQryName($db2con,$WUGID);
            if ($rsQry['result'] !== 'NOTEXIST_GET') {
                foreach($rsUsr['data'] as $usrNm){
                    foreach ($rsQry['data'] as $qryNm) {
                        $name = cmGetQryNameFromDB2WDEF($db2con,$usrNm['WUUID'],$qryNm['WGNAME']);
                        if ($name['result'] === 'NOTEXIST_GET') {
                            $rss = cmInsQryKengen($db2con,$usrNm['WUUID'],$qryNm['WGNAME'],'1','1');
                            if($rss['result'] !== true){
                                $rtn = 1;
                                $msg = $rss['result'];
                                break;
                            }
                        }else{
                            $rss = cmUpdQryKengen($db2con,$usrNm['WUUID'],$qryNm['WGNAME'],'1','1');
                            if($rss['result'] !== true){
                                $rtn = 1;
                                $msg = $rss['result'];
                                break;
                            }
                        }
                    }
                }
            }
        }
   }
}


//ブックマークのため削除
if($rtn === 0){
    if(count($queryData)>0){
        foreach($queryData as $value){
            $r=cmCountDB2WGDF_DB2WUGR($db2con,$value['WGNAME']);
            if($r['result']!==true){
                $rtn=1;
                $msg=$r['result'];
                break;
            }else{
                if($r['TOTALCOUNT']<=0){
                    $r=cmDelDB2BMK($db2con,'',$value['WGNAME'],2);
                    if($r['result']!==true){
                        $rtn=1;
                        $msg=$r['result'];
                        break;
                    }
                }
            }
        }
    }
}

if($rtn !== 0){
    db2_rollback($db2con);
}else{
    db2_commit($db2con);
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'checkRs' => $checkRs,
    'defRs' => $defRs,
    'DATA' => $data,
    'MSG' => $msg,
    'RTN' => $rtn
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ユーザー情報取得
*-------------------------------------------------------*
*/

function fnCheckDB2WUGR($db2con,$WUGID,$WUUID){

    $data = array();

    $strSQL  = ' SELECT A.WUGID ';
    $strSQL .= ' FROM DB2WUGR AS A ' ;
    $strSQL .= ' WHERE A.WUGID = ? ' ;
    $strSQL .= ' AND A.WUUID = ? ' ;

    $params = array(
        $WUGID,
        $WUUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
           $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
             $data = array('result' => true,'data' => count($data));
        }
    }

    return $data;

}

/*
*-------------------------------------------------------* 
* ユーザー追加
*-------------------------------------------------------*
*/

function fnInsertDB2WUGR($db2con,$WUGID,$WUUID){


    $rs = true;

    //構文
    $strSQL  = ' INSERT INTO DB2WUGR ';
    $strSQL .= ' ( ';
    $strSQL .= ' WUUID, ';
    $strSQL .= ' WUGID ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ' ;
    $strSQL .= ' (?,?) ';

    $params = array(
        $WUUID,
        $WUGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* ユーザー削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WUGR($db2con,$WUGID,$WUUID){


    $rs = true;

    //構文

    $strSQL  = ' DELETE FROM DB2WUGR ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUUID = ? ';
    $strSQL .= ' AND WUGID = ? ';

    $params = array(
        $WUUID,
        $WUGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}

/****
*自分が抜いたグループからクエリー取得
*取得されたクエリーがお気に入りにあったら削除
****/
function fnDeleteBmkQry($db2con,$USERID,$WGGID){
    $data = array();
    $qryData = array();
    $strSQL = '';
    $strSQL .= ' SELECT * FROM DB2WGDF WHERE WGGID = ? ';
    $params = array($WGGID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === fasle){
            $rs = 'FAIL_DEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $qryData[] = $row;
            }
            if(count($qryData) > 0 ){
                $strSQL = '';
                $strSQL .= ' SELECT * FROM ';
                $strSQL .= ' (SELECT * FROM DB2WGDF WHERE WGGID <> ?) AS A ';
                $strSQL .= ' LEFT JOIN ';
                $strSQL .= ' (SELECT * FROM DB2WUGR  WHERE WUUID = ? AND WUGID <> ?) AS B ';
                $strSQL .= ' ON A.WGGID = B.WUGID';
                $strSQL .= ' WHERE A.WGNAME = ? ';//他のグループに同じクエリー保存されてるかどうかチェック
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = 'FAIL_DEL';
                }else{
                    foreach($qryData as  $key => $value){
                        $D1NAME = $value['WGNAME'];
                        $params = array($WGGID,$USERID,$WGGID,$D1NAME);
                        $r = db2_execute($stmt,$params);
                        if($r === fasle){
                            $rs = 'FAIL_DEL';
                            break;
                        }else{
                            while($row = db2_fetch_assoc($stmt)){
                                $data[] = $row;
                            }
                            if(count($data) === 0 ){
                                   $rs = cmDeleteDB2QBMK($db2con,'',$D1NAME,'','',$USERID);
                            }
                        }
                        //$rs = cmDeleteDB2QBMK($db2con,'',$D1NAME,'','',$WUUID);
                    }
                }
            }
        }
    }
    return $rs;
}
/*
*-------------------------------------------------------* 
* グループ存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WQGR($db2con,$WQGID){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.WQGID ';
    $strSQL .= ' FROM DB2WQGR  AS A ' ;
    $strSQL .= ' WHERE A.WQGID = ? ' ;

    $params = array(
        $WQGID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}