<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WGGID = cmHscDe($_POST['WGGID']);
$data = json_decode($_POST['DATA'],true);
$PROC = $_POST['PROC'];
$rtn = 0;
$msg = '';
$queryData=array();

/*
*-------------------------------------------------------* 
* 権限更新処理
*-------------------------------------------------------*
*/
error_log('DataGroupDta***'.print_r($data,true));
$checkRs = false;
$defRs = false;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'4',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループクエリー権限の権限'));
            }
        }
    }
}

if($rtn === 0){   
   if(count($data) != 0){   
	    $res = updGetDB2WGDF($db2con,$WGGID,$data);
        
	    if($res !== true){
           error_log('Result***'.print_r($res,true));
            $rtn = 1;
            $msg = showMsg($res);
        }
  }
} 



if($rtn !== 0){
    db2_rollback($db2con);
}else{
    db2_commit($db2con);
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'checkRs' => $checkRs,
    'defRs' => $defRs,
    'DATA' => $data,
    'WGGID' => $WGGID,
    'MSG' => $msg,
    'RTN' => $rtn
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ユーザー情報取得
*-------------------------------------------------------*
*/

function updGetDB2WGDF($db2con,$WGGID,$data){
  error_log('WGGID DATA**'.print_r($data[0]['WGGID'],true));
  $rs = true;
  $WGNRFLG = '0';
  $strSQL = ' UPDATE DB2WGDF ';
  $strSQL .= ' SET ';
  $strSQL .= ' WGNRFLG = ? ';
  $strSQL .= ' WHERE ';
  $strSQL .= ' WGGID = ? ';
  
  $params = array(
      $WGNRFLG,
      $data[0]['WGGID']
  );
  
  $stmt = db2_prepare($db2con, $strSQL);
  if ($stmt === false) {
      $rs = 'FAIL_UPD';
  } else {
     $r = db2_execute($stmt,$params);
		if($r === false){
		       $rs = 'FAIL_UPD';
	    }else{

		  for($i = 0;$i < count($data);$i++){	    
		        $strSQL  = ' UPDATE DB2WGDF ';
			    $strSQL .= ' SET ';
			    $strSQL .= ' WGNRFLG = ? ';
			    $strSQL .= ' WHERE ';
			    $strSQL .= '    WGGID = ? ';
			    $strSQL .= ' AND WGNAME = ? ';

			    $params = array(
			        $i+1,
			        $data[$i]['WGGID'],
			        $data[$i]['WGNAME']
			     
			    );   

		        error_log('Strsql***'.print_r($strSQL,true));
		        error_log('Params***'.print_r($params,true));
			    $stmt = db2_prepare($db2con,$strSQL);
			    if($stmt === false){
			        $rs = 'FAIL_UPD';
		            error_log('AAA');
			    }else{
			        $r = db2_execute($stmt,$params);
			        if($r === false){
			            $rs = 'FAIL_UPD';
		                error_log('BBB');
			        }
	             }
			  }  
	     }
    }
    
    return $rs;
}