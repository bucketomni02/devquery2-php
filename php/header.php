<?php

?>


<script type="text/javascript">

    function LogOut(){

        var data = {
            'proc':'LogOut'
        }

        $.ajax({
            type: "GET",
            url: "ajax.php",
            dataType:'json',
            data: data,
            success: function(o){

                window.location = '';

            }
        });

    }

    function gomain(){
        var form = document.getElementById('form');
        form.proc.value = 'Int';
        form.GROUP.value = '';
        form.D1NAME.value = '';
        form.D1TEXT.value = '';
        form.DEFINE.value = '';
        form.action = 'main.php';
        form.submit();
    }

</script>

<header>
    <div class='navbar' id="navbar">
        <div class='navbar-inner'>
            <div class='container-fluid'>
                <a class='brand' onclick="gomain();" >
                    <span class='hidden-phone' style="cursor:pointer;">PHP QUERY</span>
                </a>
                <a class='toggle-nav btn pull-left' href='#'>
                    <i class='icon-reorder'></i>
                </a>
                <ul class='nav pull-right'>

<!--
                    <li class='dropdown light only-icon'>
                        <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                            <i class='icon-adjust'></i>
                        </a>

                        <ul class='dropdown-menu color-settings'>
                            <li class='color-settings-body-color'>
                                <div class='color-title'>Body color</div>
                                <a data-change-to='/common/lib/flatty/html/assets/stylesheets/light-theme.css' href='#'>
                                    Light
                                    <small>(default)</small>
                                </a>
                                <a data-change-to='/common/lib/flatty/html/assets/stylesheets/dark-theme.css' href='#'>
                                    Dark
                                </a>
                                <a data-change-to='/common/lib/flatty/html/assets/stylesheets/dark-blue-theme.css' href='#'>
                                    Dark blue
                                </a>
                            </li>
                            <li class='divider'></li>
                            <li class='color-settings-contrast-color'>
                                <div class='color-title'>Contrast color</div>
                                <a href="#" data-change-to="contrast-red"><i class='icon-adjust text-red'></i>
                                    Red
                                    <small>(default)</small>
                                </a>
                                <a href="#" data-change-to="contrast-blue"><i class='icon-adjust text-blue'></i>
                                    Blue
                                </a>
                                <a href="#" data-change-to="contrast-orange"><i class='icon-adjust text-orange'></i>
                                    Orange
                                </a>
                                <a href="#" data-change-to="contrast-purple"><i class='icon-adjust text-purple'></i>
                                    Purple
                                </a>
                                <a href="#" data-change-to="contrast-green"><i class='icon-adjust text-green'></i>
                                    Green
                                </a>
                                <a href="#" data-change-to="contrast-muted"><i class='icon-adjust text-muted'></i>
                                    Muted
                                </a>
                                <a href="#" data-change-to="contrast-fb"><i class='icon-adjust text-fb'></i>
                                    Facebook
                                </a>
                                <a href="#" data-change-to="contrast-dark"><i class='icon-adjust text-dark'></i>
                                    Dark
                                </a>
                                <a href="#" data-change-to="contrast-pink"><i class='icon-adjust text-pink'></i>
                                    Pink
                                </a>
                                <a href="#" data-change-to="contrast-grass-green"><i class='icon-adjust text-grass-green'></i>
                                    Grass green
                                </a>
                                <a href="#" data-change-to="contrast-sea-blue"><i class='icon-adjust text-sea-blue'></i>
                                    Sea blue
                                </a>
                                <a href="#" data-change-to="contrast-banana"><i class='icon-adjust text-banana'></i>
                                    Banana
                                </a>
                                <a href="#" data-change-to="contrast-dark-orange"><i class='icon-adjust text-dark-orange'></i>
                                    Dark orange
                                </a>
                                <a href="#" data-change-to="contrast-brown"><i class='icon-adjust text-brown'></i>
                                    Brown
                                </a>
                            </li>
                        </ul>

                    </li>
-->

                    <li class='dropdown dark user-menu'>
                        <a class='dropdown-toggle'  style="padding-top:10px;padding-bottom:10px;" data-toggle='dropdown' href='#'>
                            <span class='user-name hidden-phone'><?=$_SESSION['PHPQUERY']['user'][0]['WUUNAM']?></span>
                            <b class='caret'></b>
                        </a>
                        <ul class='dropdown-menu'>
                            <li>
                                <a style="cursor:pointer;" data-toggle="modal" data-target=".colorModal">
                                    <i class='icon-asterisk'></i>
                                    カラー設定
                                </a>
                            </li>
                            <li>
                                <a onclick="LogOut();"style="cursor:pointer;">
                                    <i class='icon-signout'></i>
                                    ログアウト
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>

<script>

function changeColor(){

    var form = document.getElementById('config-color');

    var data = $("#config-color").serializeArray();

    $.ajax({
        type:"POST",
        url:"ajax.php",
        dataType:"json",
        data:data
    }).done(function(res){

        //今までの色をクリア
        $("body").removeClass("contrast-"+$("#color1").val());
        $("#box-title").removeClass("title-"+$("#color2").val());
        $("#DataTables_Table_0").removeClass("table-"+$("#color3").val());

        $("#color1").val($("[name=maincolor]:checked").val());
        $("#color2").val($("[name=titlecolor]:checked").val());
        $("#color3").val($("[name=theadcolor]:checked").val());

        //新しい色を付与
        $("body").addClass("contrast-"+$("[name=maincolor]:checked").val());
        $("#box-title").addClass("title-"+$("[name=titlecolor]:checked").val());
        $("#DataTables_Table_0").addClass("table-"+$("[name=theadcolor]:checked").val());

        $(".colorModal").modal("hide");
        
        setTimeout(function(){
            bootbox.alert("色を変更しました。");
        },0);

    }).fail(function(res){

    });

}
$(document).ready(function(){

    $(".colorModal").on('show.bs.modal',function(e){
        $("#maincolor-"+$("#color1").val()).attr('checked',true);
        $("#titlecolor-"+$("#color2").val()).attr('checked',true);
        $("#theadcolor-"+$("#color3").val()).attr('checked',true);
    });
});
</script>

<!--カラー設定モーダル-->
<div class="modal fade colorModal" style="display:none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">カラー設定</h4>
            </div>
            <div class="modal-body">
                <form id="config-color">
                    <?
                        $color1 = cmMer($_SESSION['PHPQUERY']['user'][0]['WUCLR1']);
                        $color2 = cmMer($_SESSION['PHPQUERY']['user'][0]['WUCLR2']);
                        $color3 = cmMer($_SESSION['PHPQUERY']['user'][0]['WUCLR3']);
                    ?>
                    <input type="hidden" name="proc" value="changeColor">
                    <input type="hidden" id="color1" name="color1" value="<?=($color1 !== '') ? $color1 : COLOR1 ?>">
                    <input type="hidden" id="color2" name="color2" value="<?=($color2 !== '') ? $color2 : COLOR2 ?>">
                    <input type="hidden" id="color3" name="color3" value="<?=($color3 !== '') ? $color3 : COLOR3 ?>">
                    <table class="color-table">
                        <tr>
                            <th class="color-table-th">メイン</th>
                            <td>
                                <input type="radio" id="maincolor-red" name="maincolor" value="red" <?=($color1 == 'red')? 'checked' : '' ;?>><i class='icon-adjust text-red'></i>Red
                            </td>
                            <td>
                                <input type="radio" id="maincolor-blue" name="maincolor" value="blue" <?=($color1 == 'blue')? 'checked' : '' ;?>><i class='icon-adjust text-blue'></i>Blue
                            </td>
                            <td>
                                <input type="radio" id="maincolor-orange" name="maincolor" value="orange" <?=($color1 == 'orange')? 'checked' : '' ;?>><i class='icon-adjust text-orange'></i>Orange
                            </td>
                            <td>
                                <input type="radio" id="maincolor-purple" name="maincolor" value="purple" <?=($color1 == 'purple')? 'checked' : '' ;?>><i class='icon-adjust text-purple'></i>Purple
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input type="radio" id="maincolor-green" name="maincolor" value="green" <?=($color1 == 'green')? 'checked' : '' ;?>><i class='icon-adjust text-green'></i>Green
                            </td>
                            <td>
                                <input type="radio" id="maincolor-fb" name="maincolor" value="fb" <?=($color1 == 'fb')? 'checked' : '' ;?>><i class='icon-adjust text-fb'></i>Facebook
                            </td>
                            <td>
                                <input type="radio" id="maincolor-muted" name="maincolor" value="muted" <?=($color1 == 'muted')? 'checked' : '' ;?>><i class='icon-adjust text-muted'></i>Muted
                            </td>
                            <td>
                                <input type="radio" id="maincolor-dark" name="maincolor" value="dark" <?=($color1 == 'dark')? 'checked' : '' ;?>><i class='icon-adjust text-dark'></i>Dark
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input type="radio" id="maincolor-pink" name="maincolor" value="pink" <?=($color1 == 'pink')? 'checked' : '' ;?>><i class='icon-adjust text-pink'></i>Pink
                            </td>
                            <td>
                                <input type="radio" id="maincolor-brown" name="maincolor" value="brown" <?=($color1 == 'brown')? 'checked' : '' ;?>><i class='icon-adjust text-brown'></i>Brown
                            </td>
                            <td>
                                <input type="radio" id="maincolor-sea-blue" name="maincolor" value="sea-blue" <?=($color1 == 'sea-blue')? 'checked' : '' ;?>><i class='icon-adjust text-sea-blue'></i>SeaBlue
                            </td>
                            <td>
                                <input type="radio" id="maincolor-banana" name="maincolor" value="banana" <?=($color1 == 'banana')? 'checked' : '' ;?>><i class='icon-adjust text-banana'></i>Banana
                            </td>
                        </tr>
                    </table>
                    </br>
                    <table class="color-table">
                        <tr>
                            <th class="color-table-th">タイトル</th>
                            <td>
                                <input type="radio" id="titlecolor-red" name="titlecolor" value="red" <?=($color2 == 'red')? 'checked' : '' ;?>><i class='icon-adjust text-red'></i>Red
                            </td>
                            <td>
                                <input type="radio" id="titlecolor-blue" name="titlecolor" value="blue" <?=($color2 == 'blue')? 'checked' : '' ;?>><i class='icon-adjust text-blue'></i>Blue
                            </td>
                            <td>
                                <input type="radio" id="titlecolor-orange" name="titlecolor" value="orange" <?=($color2 == 'orange')? 'checked' : '' ;?>><i class='icon-adjust text-orange'></i>Orange
                            </td>
                            <td>
                                <input type="radio" id="titlecolor-purple" name="titlecolor" value="purple" <?=($color2 == 'purple')? 'checked' : '' ;?>><i class='icon-adjust text-purple'></i>Purple
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input type="radio" id="titlecolor-green" name="titlecolor" value="green" <?=($color2 == 'green')? 'checked' : '' ;?>><i class='icon-adjust text-green'></i>Green
                            </td>
                            <td>
                                <input type="radio" id="titlecolor-fb" name="titlecolor" value="fb" <?=($color2 == 'fb')? 'checked' : '' ;?>><i class='icon-adjust text-fb'></i>Facebook
                            </td>
                            <td>
                                <input type="radio" id="titlecolor-muted" name="titlecolor" value="muted" <?=($color2 == 'muted')? 'checked' : '' ;?>><i class='icon-adjust text-muted'></i>Muted
                            </td>
                            <td>
                                <input type="radio" id="titlecolor-dark" name="titlecolor" value="dark" <?=($color2 == 'dark')? 'checked' : '' ;?>><i class='icon-adjust text-dark'></i>Dark
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input type="radio" id="titlecolor-pink" name="titlecolor" value="pink" <?=($color2 == 'pink')? 'checked' : '' ;?>><i class='icon-adjust text-pink'></i>Pink
                            </td>
                            <td>
                                <input type="radio" id="titlecolor-brown" name="titlecolor" value="brown" <?=($color2 == 'brown')? 'checked' : '' ;?>><i class='icon-adjust text-brown'></i>Brown
                            </td>
                            <td>
                                <input type="radio" id="titlecolor-sea-blue" name="titlecolor" value="sea-blue" <?=($color2 == 'sea-blue')? 'checked' : '' ;?>><i class='icon-adjust text-sea-blue'></i>SeaBlue
                            </td>
                            <td>
                                <input type="radio" id="titlecolor-banana" name="titlecolor" value="banana" <?=($color2 == 'banana')? 'checked' : '' ;?>><i class='icon-adjust text-banana'></i>Banana
                            </td>
                        </tr>
                    </table>
                    </br>
                    <table class="color-table">
                        <tr>
                            <th>見出し</th>
                            <td>
                                <input type="radio" id="theadcolor-red" name="theadcolor" value="red" <?=($color3 == 'red')? 'checked' : '' ;?>><i class='icon-adjust text-red'></i>Red
                            </td>
                            <td>
                                <input type="radio" id="theadcolor-blue" name="theadcolor" value="blue" <?=($color3 == 'blue')? 'checked' : '' ;?>><i class='icon-adjust text-blue'></i>Blue
                            </td>
                            <td>
                                <input type="radio" id="theadcolor-orange" name="theadcolor" value="orange" <?=($color3 == 'orange')? 'checked' : '' ;?>><i class='icon-adjust text-orange'></i>Orange
                            </td>
                            <td>
                                <input type="radio" id="theadcolor-purple" name="theadcolor" value="purple" <?=($color3 == 'purple')? 'checked' : '' ;?>><i class='icon-adjust text-purple'></i>Purple
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input type="radio" id="theadcolor-green" name="theadcolor" value="green" <?=($color3 == 'green')? 'checked' : '' ;?>><i class='icon-adjust text-green'></i>Green
                            </td>
                            <td>
                                <input type="radio" id="theadcolor-fb" name="theadcolor" value="fb" <?=($color3 == 'fb')? 'checked' : '' ;?>><i class='icon-adjust text-fb'></i>Facebook
                            </td>
                            <td>
                                <input type="radio" id="theadcolor-muted" name="theadcolor" value="muted" <?=($color3 == 'muted')? 'checked' : '' ;?>><i class='icon-adjust text-muted'></i>Muted
                            </td>
                            <td>
                                <input type="radio" id="theadcolor-dark" name="theadcolor" value="dark" <?=($color3 == 'dark')? 'checked' : '' ;?>><i class='icon-adjust text-dark'></i>Dark
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input type="radio" id="theadcolor-pink" name="theadcolor" value="pink" <?=($color3 == 'pink')? 'checked' : '' ;?>><i class='icon-adjust text-pink'></i>Pink
                            </td>
                            <td>
                                <input type="radio" id="theadcolor-brown" name="theadcolor" value="brown" <?=($color3 == 'brown')? 'checked' : '' ;?>><i class='icon-adjust text-brown'></i>Brown
                            </td>
                            <td>
                                <input type="radio" id="theadcolor-sea-blue" name="theadcolor" value="sea-blue" <?=($color3 == 'sea-blue')? 'checked' : '' ;?>><i class='icon-adjust text-sea-blue'></i>SeaBlue
                            </td>
                            <td>
                                <input type="radio" id="theadcolor-banana" name="theadcolor" value="banana" <?=($color3 == 'banana')? 'checked' : '' ;?>><i class='icon-adjust text-banana'></i>Banana
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td colspan="4">
                                <input type="radio" id="theadcolor-default" name="theadcolor" value="default" <?=($color3 == 'default')? 'checked' : '' ;?>><i class='icon-adjust text-default'></i>Default
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-primary" onclick="changeColor();" value="OK">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->