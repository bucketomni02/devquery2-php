<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['proc']))?$_POST['proc']:'';
$HTMLID = (isset($_POST['HTMLID']))?$_POST['HTMLID']:'';
$HTMLNM = (isset($_POST['HTMLNM']))?strtolower($_POST['HTMLNM']):'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/
$HTMLID = cmHscDe($HTMLID);
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('HTML禁止タグの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){          
            $rs = cmChkKenGen($db2con,'24',$userData[0]['WUSAUT']);//'1' => htmlMaster       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('HTML禁止タグの権限'));
            }
        }
    }
}
    if($proc === 'DEL'){
        if($rtn === 0){
            //削除対象ＨＴＭＬ名存在チェック処理
            $rs = fnCheckDB2HTMLK($db2con,$HTMLID,'HTMLID','');
            if( $rs === true){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_UPD',array(array('HTML','名')));
                $focus = 'HTMLNM';
            }
        }
        if($rtn === 0){
                //ＨＴＭＬタグを削除する
                 $rs = fnDeleteDB2HTMLK($db2con,$HTMLID);
                  if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array(array('HTML','名')));
                        $focus = 'HTMLNM';
                    }
        }
    }else{
            /*
            *-------------------------------------------------------* 
            * バリデーションチェック
            *-------------------------------------------------------*
            */

                        // ＨＴＭＬ名必須チェック
                        if($rtn === 0){
                              if($HTMLNM  === ''){
                                $rtn = 1;
                                $msg = showMsg('FAIL_REQ',array(array('HTML','名')));
                                $focus = 'HTMLNM';
                            }
                        }

                        // ＨＴＭＬ名半角チェック
                        if($rtn === 0){
                            $byte = checkByte($HTMLNM);
                            if($byte[1] > 0){
                                $rtn = 1;
                                $msg = showMsg('FAIL_BYTE',array(array('HTML','名')));
                                $focus = 'HTMLNM';
                            }
                        }

                        // ＨＴＭＬ名入力桁数チェック
                        if($rtn === 0){
                            if(!checkMaxLen($HTMLNM,50)){
                                $rtn = 1;
                                $msg = showMsg('FAIL_MAXLEN',array(array('HTML','名')));
                                $focus = 'HTMLNM';
                            }
                        }
                      /*  if($rtn === 0){
                            if(preg_match("/^[0-9]+$/", $HTMLNM) === 1) {
                                $rtn = 1;
                                $msg = 'ＨＴＭＬ名をa-zA-Zで入力してください';
                                $focus = 'HTMLNM';
                            }else if(preg_match("/^[a-zA-Z0-9]+$/", $HTMLNM) !== 1) {
                                $rtn = 1;
                                $msg = 'ＨＴＭＬ名をa-zA-Zで入力してください';
                                $focus = 'HTMLNM';
                            }
                        }*/
                        if($rtn === 0){
                              $f_html = $HTMLNM[0];
                              $s_html = substr($HTMLNM,1);
                            if(preg_match("/^[a-zA-Z!]+$/", $f_html) !== 1) {
                                $rtn = 1;
                                $msg   = showMsg('FAIL_VLDD',array('禁止タグ',array('先頭が','「a-zA-Z!」')));//'禁止タグに使用できない文字が含まれています。<br/>禁止タグには次の文字しか使えません。<br/>先頭が「a-zA-Z!」';
                                $focus = 'HTMLNM';
                            }
                        }
                        if($rtn === 0 && $s_html !== false){
                            if(preg_match("/^[a-zA-Z1-9]+$/", $s_html) !== 1){
                                $rtn = 1;
                                $msg   = showMsg('FAIL_VLDD',array('禁止タグ',array('2','文字目以降が','「a-zA-Z1-9」')));//'禁止タグに使用できない文字が含まれています。<br/>>禁止タグには次の文字しか使えません。<br/> 2文字目以降が「a-zA-Z1-9」 ';
                                $focus = 'HTMLNM';
                            }
                        }
            /*
            *-------------------------------------------------------* 
            * 更新処理
            *-------------------------------------------------------*
            */

            if($proc === 'ADD'){
                if($rtn === 0){
                    //ＨＴＭＬ名存在チェック処理
                    $rs = fnCheckDB2HTMLK($db2con,$HTMLNM,'HTMLNM','');
                    if( $rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array(array('HTML','名')));
                        $focus = 'HTMLNM';
                    }
                }
                if($rtn === 0){
                    //新ＨＴＭＬ名を登録処理
                    $rs = fnInsertDB2HTMLK($db2con,$HTMLNM);
                    if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs);
                    }
                }
            }else if($proc === 'UPD'){
                    if($rtn === 0){
                        //更新対象ＨＴＭＬ名存在チェック処理
                        $rs = fnCheckDB2HTMLK($db2con,$HTMLID,'HTMLID','');
                        if( $rs === true){
                            $rtn = 1;
                            $msg = showMsg('NOTEXIST_UPD',array(array('HTML','名')));
                            $focus = 'HTMLNM';
                        }
                    }
                   if($rtn === 0){
                        //更新対象ＨＴＭＬ名存在チェック処理
                        $rs = fnCheckDB2HTMLK($db2con,$HTMLNM,'HTMLNM',$HTMLID);
                        if( $rs !== true){
                            $rtn = 1;
                            $msg = showMsg($rs,array(array('HTML','名')));
                            $focus = 'HTMLNM';
                        }
                    }
                    if($rtn === 0){
                        //更新対象ＨＴＭＬ名を変更する処理
                        $rs = fnUpdateDB2HTMLK($db2con,$HTMLID,$HTMLNM);
                        if( $rs !== true){
                            $rtn = 1;
                            $msg = showMsg($rs);
                        }
                    }
            }
    }
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'f_html' =>$f_html,
    's_html' =>$s_html
);

echo(json_encode($rtnAry));

/**
*-------------------------------------------------------* 
* メールグアドレスが存在するかどうかのチェックすること
* 
* RESULT
*    01：存在する場合     true
*    02：存在しない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $HTMLNM ＨＴＭＬ名
* 
*-------------------------------------------------------*
*/
function fnCheckDB2HTMLK($db2con,$HTML_,$HTMLCOL,$HTMLID){

    $data = array();
    $rs = true;
    $params = array($HTML_);
    $strSQL  = ' SELECT A.HTMLID,A.HTMLNM ' ;
    $strSQL .= ' FROM DB2HTMLK AS A ' ;
    $strSQL .= ' WHERE A.'.$HTMLCOL.' = ? ' ;
    if($HTMLID !== ''){
        $strSQL .= ' AND A.HTMLID <> ? ';
        array_push($params,$HTMLID);
    }

    $stmt = db2_prepare($db2con,$strSQL);
   // $stmt = false;
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* 新メールグアドレスを登録すること
* 
* RESULT
*    01：登録終了の場合   true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $HTMLNM ＨＴＭＬ名
* 
*-------------------------------------------------------*
*/
function fnInsertDB2HTMLK($db2con,$HTMLNM){
    $HTMLID = getHTMLIDRandom($db2con);
    $rs = true;
    //構文
    $strSQL  = ' INSERT INTO DB2HTMLK ';
    $strSQL .= ' ( ';
    $strSQL .= ' HTMLID, ';
    $strSQL .= ' HTMLNM ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?) ';

    $params = array($HTMLID,strtolower($HTMLNM));

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}

function getHTMLIDRandom($db2con){
        $rs = true;
        $count  =0;
        $HTMLID = '';
        do{
                $count++;
                $HTMLID = $rand = substr(md5(microtime()),rand(0,26),6);
                 $res = fnCheckDB2HTMLK($db2con,$HTMLID,'HTMLID','');
                if($res !== true){
                    $test = false;
                    $rs = true;
                }else{
                    $rs = false;
                }
        }while($rs !== false);
        return $HTMLID;
}
/**
*-------------------------------------------------------* 
* メールグアドレスを更新すること
* 
* RESULT
*    01：更新終了の場合   true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $HTMLNM ＨＴＭＬ名
* 
*-------------------------------------------------------*
*/
function fnUpdateDB2HTMLK($db2con,$HTMLID,$HTMLNM){

    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2HTMLK ';
    $strSQL .= ' SET ';
    $strSQL .= ' HTMLNM = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' HTMLID = ? ';
    $params = array(strtolower($HTMLNM),$HTMLID);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}

function fnDeleteDB2HTMLK($db2con,$HTMLID){
    $rs = true;
    $strSQL = 'DELETE FROM DB2HTMLK ';
    $strSQL .= ' WHERE HTMLID = ? ';
    $params = array($HTMLID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}