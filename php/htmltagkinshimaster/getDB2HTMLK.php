<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$HTMLID = $_POST['HTMLID'];
$HTMLNM = (isset($_POST['HTMLNM'])) ? strtolower($_POST['HTMLNM']) : '' ;
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$htmlFlg = true;
$rtn = 0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$HTMLID = cmHscDe($HTMLID);
$HTMLNM = cmHscDe($HTMLNM);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('HTML禁止タグの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'24',$userData[0]['WUSAUT']);//'1' => htmlMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('HTML禁止タグの権限'));
            }
        }
    }
}

if($proc == 'EDIT'){
    if($rtn === 0){
        $res = fnSelDB2HTMLK($db2con,$HTMLID);
        if($res['result'] === true){
            if(count($res['data']) === 0){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET',array('ＨＴＭＬタグ'));
            }else{
                $data = $res['data'];
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $res = fnGetAllCount($db2con,$HTMLID,$HTMLNM);
        if($res['result'] === true){
            $allcount = $res['data'];
            $res = fnGetDB2HTMLK($db2con,$HTMLID,$HTMLNM,$start,$length,$sort,$sortDir);
            if($res['result'] === true){
                $data = $res['data'];
            }else{
                $rtn = 1;
                $msg = showMsg($res['result']);
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
}

cmDb2Close($db2con);
/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));


/*
*-------------------------------------------------------* 
* メールグループ取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $HTMLID メールグループID
* @param String  $HTMLNM メールグループ名
* @param String  $start
* @param String  $length
* @param String  $sort
* @param String  $sortDir
* 
*-------------------------------------------------------*
*/

function fnGetDB2HTMLK($db2con,$HTMLID,$HTMLNM,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();

    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.HTMLNM ASC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM DB2HTMLK as B ';
    $strSQL .= ' WHERE HTMLNM <> \'\' ';
    
    if($HTMLNM != ''){
        $strSQL .= ' AND HTMLNM like ? ';
        array_push($params,'%'.$HTMLNM.'%');
    }
    $strSQL .= ' ) as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* メールグループ全件カウント取得処理
* 
* RESULT
*    01：データ件数
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* 
*-------------------------------------------------------*
*/
function fnGetAllCount($db2con,$HTMLID,$HTMLNM){
    $data = array();

    $strSQL  = ' SELECT count(A.HTMLNM) as COUNT ';
    $strSQL .= ' FROM DB2HTMLK as A ' ;
    $strSQL .= ' WHERE HTMLNM <> \'\' ';

    $params = array();

    if($HTMLNM != ''){
        $strSQL .= ' AND HTMLNM like ? ';
        array_push($params,'%'.$HTMLNM.'%');
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}
/*
*-------------------------------------------------------* 
* 対象メールグループデータ取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* 
*-------------------------------------------------------*
*/
function fnSelDB2HTMLK($db2con,$HTMLID){

    $data = array();

    $params = array($HTMLID);

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2HTMLK as A ';
    $strSQL .= ' WHERE HTMLID = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}