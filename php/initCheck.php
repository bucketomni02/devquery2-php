﻿<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */

include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */

$rtn = 0;
$msg = ''; //エラーメッセージ
$keychk = '';//解約確認キー
session_start();
//color用
//language用
$usrifclr = $_SESSION['PHPQUERY']['user'];
$usrId    = $usrifclr[0]['WUUID'];

$licenseCheck = (isset($_POST['licenseCheck']) ? $_POST['licenseCheck'] : '');
$loginCheck   = (isset($_POST['loginCheck']) ? $_POST['loginCheck'] : '');
$getColor     = (isset($_POST['getColor']) ? $_POST['getColor'] : '');
$chkLangUsr   = (isset($_POST['chkLangUsr']) ? $_POST['chkLangUsr'] : '');
e_log('initcheck'.$licenseCheck.'--------'.$loginCheck.'--------'.$getColor.'--------'.$chkLangUsr);

//ログインユーザが削除されたかどうかチェック
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

if ($rtn === 0 && $licenseCheck === '1') {
    /*
     *-------------------------------------------------------* 
     * 共通変数
     *-------------------------------------------------------*
     */
    $FileFlg     = true; //ファイルの存在チェック
    $DateFlg     = true; //ライセンス期限チェックフラグ
    $SerialFlg   = true; //シリアルNoチェックフラグ
    $UserFlg     = true; //ユーザー数チェックフラグ
    $GroupFlg    = true; //ユーザー数チェックフラグ
    $QueryFlg    = true; //クエリー登録数チェックフラグ
    $ClFlg       = true; //事前処理権限チェックフラグ
    $ScheduleFlg = true; //スケジュール自動実行権限チェックフラグ
    $PivotFlg    = true; //ピボット実行権限チェックフラグ
    $GraphFlg    = true; //ピボット実行権限チェックフラグ
    $errFlg      = true;
    $beforErr    = false; //ユーザー、グループの登録前チェック
    $SeigyoFlg   = true; //制御実行権限チェックフラグ
    $QueryGroupFlg = true;//クエリーグループ実行権限チェックフラグ
    $SqlFlg      = true;//sqlクエリー権限チェックフラグ
    $FileOutflg  = true;//ファイル出力権限チェックフラグ
    $flag        = (isset($_POST['flag']) ? $_POST['flag'] : '');
    $LICFLG      = (isset($_POST['LICFLG']) ? $_POST['LICFLG'] : '');

    if ($flag === "1") {
        
        $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if ($rs['result'] !== true) {
            $rtn = 2;
            $msg = showMsg($rs['result'], array(
                'ユーザー'
            ));
        } else {
            $userData = umEx($rs['data']);
            if ($userData[0]['WUAUTH'] === '2') {
                $rs = cmChkKenGen($db2con, '29', $userData[0]['WUSAUT']); //'6' =>  ダウンロード権限
                if ($rs['result'] !== true) {
                    $rtn = 2;
                    $msg = showMsg($rs['result'], array(
                        'ライセンス情報の権限'
                    ));
                }
            }
        }
        
    }
    if ($rtn === 0) {
        if (file_exists('licenseInfo.php')) {
            include_once("licenseInfo.php");
        } else {
            $FileFlg = false;
            $errFlg  = false;
            if (isset($_SESSION['PHPQUERY'])) {
                $_SESSION['PHPQUERY'] = array();
            }
            if (isset($_SESSION['PHPQUERYTOUCH'])) {
                $_SESSION['PHPQUERYTOUCH'] = array();
            }
        }
        //シリアルNoチェック
        if ($errFlg === true) {
            if ($licenseSerial != '') {
                 $getserial = cmGETSRLNBR($db2con);
                if (trim($getserial) != $licenseSerial) {
                    $SerialFlg = false;
                    $errFlg    = false;
                    $msg       = showMsg('FAIL_CHK', array(
                        'シリアル番号'
                    )); //'シリアル番号が間違っています。';
                    if (isset($_SESSION['PHPQUERY'])) {
                        $_SESSION['PHPQUERY'] = array();
                    }
                    if (isset($_SESSION['PHPQUERYTOUCH'])) {
                        $_SESSION['PHPQUERYTOUCH'] = array();
                    }
                }

            }
        }
        $errChk = '';
        //ユーザー数チェック
        if ($errFlg === true) {
            if ($licenseUser != '') {

                $count = fnGetUserCount($db2con, $licenseUser);
                if (isset($_POST['BeforAddUser'])) {
                    if ($count >= $licenseUser) {
                        $UserFlg  = false;
                        //     $errFlg = false;
                        $beforErr = true;
                        $msg      = showMsg('NO_ADD_BUT', array(
                            'ユーザー'
                        )); //'ユーザーはこれ以上追加できません。';
                    }
                } else {
                    if ($count > $licenseUser) {
                        $UserFlg = false;
                        $errChk  = 'ユーザー';
                        if ($licenseType === 'FREE') {
                            $licenseUser_F = $licenseUser_F . '(現在：' . $count . ')';
                        }
                    }
                }

            }
        }
        
        //グループ数チェック
        if ($errFlg === true) {
            if ($licenseGroup != '') {

                $count = fnGetGroupCount($db2con);
                if (isset($_POST['BeforAddGroup'])) {
                    if ($count >= $licenseGroup) {
                        $GroupFlg = false;
                        //   $errFlg = false;
                        $beforErr = true;
                        $msg      = showMsg('NO_ADD_BUT', array(
                            'グループ'
                        )); //'グループはこれ以上追加できません。';
                    }
                } else {
                    if ($count > $licenseGroup) {
                        if ($errChk !== '') {
                            $errChk .= '/';
                        }
                        $errChk .= 'グループ';
                        $GroupFlg = false;
                    }
                }
            }
        }
        
        //定義数チェック
        if ($errFlg === true) {
            if ($licenseQuery != '') {

                $count = fnGetQueryCount($db2con);
                if ($count > $licenseQuery) {
                    $QueryFlg = false;
                    if ($errChk !== '') {
                        $errChk .= '/';
                    }
                    $errChk .= 'クエリー';
                    if ($licenseType === 'FREE') {
                        $licenseQuery_F = $licenseQuery_F . '(現在：' . $count . ')';
                    }
                }
            }
        }

		if($errFlg === true){
            if($licenseKukaku != ''){

               // $db2con = cmDb2con();
              //  cmSetPHPQUERY($db2con);
                $count = fnGetKukakuCount($db2con);
                if($count > (int)$licenseKukaku){
                    $KukakuFlg = false;
                    //$errFlg = false;
                        if($errChk !== ''){
                                $errChk .= '/';
                        }
                        $errChk .= '区画';
                        if($licenseType === 'FREE'){
                            $licenseKukaku_F = $licenseKukaku_F.'(現在：'.$count.')';
                        }
                    //$msg = 'クエリー登録数が制限を超えています。<br/>使えるクエリーは'.$licenseQuery.'個までです。';
                }
              //  cmDb2Close($db2con);
            }
        }
        if ($errChk !== '') {
            $licenseText = showMsg('FAIL_MAXLEN', array(
                $errChk
            )); //$errChk.' が制限を超えています。';
        }
        //事前処理登録権限チェック
        if ($licenseCl == false) {
            $ClFlg = false;
        }
        
        //スケジュール自動実行権限チェック
        if ($licenseSchedule == false) {
            $ScheduleFlg = false;
        }
        //グラフ実行権限チェック
        if ($licenseGraph == false) {
            $GraphFlg = false;
        }
        //ピボット実行権限チェック
        if ($licensePivot == false) {
            $PivotFlg = false;
        }
        //制御実行権限チェック
        if ($licenseSeigyo == false) {
            $SeigyoFlg = false;
        }
        //クエリーグループ実行権限チェック
        if ($licenseQueryGroup == false) {
            $QueryGroupFlg = false;
        }
        //ｓｑｌクエリー実行権限チェック
        if ($licenseSql == false) {
            $SqlFlg = false;
        }
        //ファイル出力実行権限チェック
        if ($licenseFileOut == false) {
            $FileOutFlg = false;
        }
        $flg = 0;
        $rtn = 0;
        $FLG = (isset($_POST['FLG'])) ? $_POST['FLG'] : '';
        if ($errFlg !== false) {
            //ピッボト作成権限
            if ($FLG === 'PIVOTCREATE') {
                if ($licensePivotCreate === false) {
                    $rtn = 1;
                }
            }
            //クエリーダウロードできるかの権限
            if ($FLG === 'DOWNLOAD') {
                if ($licenseExcelcsvDownload === false) {
                    $rtn = 1;
                }
            }
            //スケジュールボタンの使用権限
            if ($FLG === 'SCHBTN') {
                if ($licenseScheduleBtn === false) {
                    $rtn = 1;
                }
            }
            
            if ($rtn === 1) {
                $msg = showMsg('FAIL_FREE_LICENSE'); //"フリー版ではご利用いただけません。";
            }
        } else {
            if ($FLG === 'QRYJIKO') {
                $msg = showMsg('FAIL_FREE_LICENSEOVER'); //"フリー版の上限を超えています。</br>ライセンス情報を確認してください。";
            } else if ($FLG === 'SCHBTN' || $FLG === 'DOWNLOAD') {
                $msg = showMsg('FAIL_FREE_LICENSE'); //"フリー版ではご利用いただけません。";
            }
        }
        
        //ライセンスエラーがある場合、セッションをクリア
        if ($errFlg === false) {
            
            //登録前エラーチェックがtrue(エラーがあった)の場合はスルー
            if ($beforErr !== true) {
                if (!isset($_POST['BeforAdd'])) {
                    if (isset($_SESSION['PHPQUERY'])) {
                        $_SESSION['PHPQUERY'] = array();
                    }
                    if (isset($_SESSION['PHPQUERYTOUCH'])) {
                        $_SESSION['PHPQUERYTOUCH'] = array();
                    }
                }
            }
        }
        /**
         *フリー日付の修正（２０１６１０１３）
         **/
        if ($licenseDate_SF !== '~') {
            $licenseDate_SF = date("Ymd", strtotime("+1 day", strtotime($licenseDate_SF)));
        }
    }
}
if ($rtn === 0 && $loginCheck === '1') {
    
    //checklogin
    $touch    = (isset($_POST['TOUCH']) ? $_POST['TOUCH'] : '0');
    $lasthash = (isset($_POST['lasthash']) ? $_POST['lasthash'] : '');
    
    $login   = '';
    $usrinfo = array();

    $msg = '';
    if ($touch == '1') {
        e_log("TestingS1");
        $login   = $_SESSION['PHPQUERYTOUCH']['LOGIN'];
        $userid  = $_SESSION['PHPQUERYTOUCH']['user'][0]['WUUID'];
        $usrinfo = regetDB2WUSR($db2con, $rs, $userid);
        //ユーザーが削除されていた場合、ログアウト状態にする(ログイン状態の場合のみチェック)
        if ($login === '1') {
            if (count($usrinfo) === 0) {
                $login = '';
                $msg   = showMsg('NOTEXIST_GET', array(
                    'ユーザー'
                ));
            } else {
                $usrinfo                           = umEx($usrinfo);
                $_SESSION['PHPQUERYTOUCH']['user'] = $usrinfo;
            }
        }
        
        //現在のHASHを保存
        if ($lasthash !== '') {
            $_SESSION['PHPQUERYTOUCH']['lasthash'] = $lasthash;
        }
    } else {
        e_log("TestingS2");
        $login   = $_SESSION['PHPQUERY']['LOGIN'];
        $userid  = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
        $usrinfo = regetDB2WUSR($db2con, $rs, $userid);
        
        //ユーザーが削除されていた場合、ログアウト状態にする(ログイン状態の場合のみチェック)
        if ($login === '1') {
            if (count($usrinfo) === 0) {
                $login = '';
                $msg   = showMsg('NOTEXIST_GET', array(
                    'ユーザー'
                ));
            } else {
                $usrinfo                      = umEx($usrinfo);
                $_SESSION['PHPQUERY']['user'] = $usrinfo;
            }
        }
        
        //現在のHASHを保存
        if ($lasthash !== '') {
            $_SESSION['PHPQUERY']['lasthash'] = $lasthash;
        }
    }
}
if ($rtn === 0 && $getColor === '1') {
    e_log("TestingS3");
    // ユーザがログアウトかどうかチェック
    if ($rtn === 0) {
        if ($_SESSION['PHPQUERY']['user'][0]['WUUID'] === null) {
            $rtn = 1;
            $msg = showMsg('CHECK_LOGOUT', array(
                'ユーザー'
            ));
        }
    }    
    //ログインユーザが削除されたかどうかチェック
    if ($rtn === 0) {
        $rs = fnGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if ($rs['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($rs['result'], array(
                'ユーザー'
            ));
        }
    }
    
    $DB2WUSR = fnGetDB2WUSR($db2con, $usrId);
    
    $DB2WUSR = umEx($DB2WUSR);
    $DB2WUSR[0]['WUPSWE'] = '';
    
    $wuclr1 = $DB2WUSR[0]['WUCLR1'];
    $wuclr2 = $DB2WUSR[0]['WUCLR2'];
    $wuclr3 = $DB2WUSR[0]['WUCLR3'];
    
    $wuclr1 = (($wuclr1 === '') ? $licenseColor1 : $wuclr1);
    $wuclr2 = (($wuclr2 === '') ? $licenseColor2 : $wuclr2);
    $wuclr3 = (($wuclr3 === '') ? $licenseColor3 : $wuclr3);
}
if ($rtn === 0 && $chkLangUsr === '1') {
    e_log("TestingS4");
    // ユーザがログアウトかどうかチェック
    if ($rtn === 0) {
        if ($_SESSION['PHPQUERY']['user'][0]['WUUID'] === null) {
            $rtn = 1;
            $msg = showMsg('CHECK_LOGOUT', array(
                'ユーザー'
            ));
        }
    }    
    
    //ログインユーザが削除されたかどうかチェック
    if ($rtn === 0) {
        $rs = fnGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if ($rs['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($rs['result'], array(
                'ユーザー'
            ));
        }
    }
    
    $DB2WUSR = fnGetDB2WUSR($db2con, $usrId);
        $DB2WUSR[0]['WUPSWE'] = '';
    $DB2WUSR = umEx($DB2WUSR);
    if ($DB2WUSR[0]['WULANG'] === '') {
        $WULANG = '001';
    } else {
        $WULANG = $DB2WUSR[0]['WULANG'];
    }
    if ($WULANG !== '001') {
        $WULANG = fnChkDB2LANG($db2con, $WULANG);
    }
}
if($LICFLG === 'CANCEL'){
    if($rtn === 0){
        //ライセンス＝＞1の場合はシリアルキーなし、ブラクの場合はシリアルキーあり
        if($SERIALK_CONTROL === ''){
            $LICSER = cmGETSRLNBR($db2con);
            $LICSER = (isset($LICSER))?trim($LICSER):'';
        }
        $keychk = encryptCancelKey($LICSER);
    }
}

if($rtn === 0){

    $last_bk = "";
    $files = scandir("ziptmp/"); 
    if(count($files) > 0){
        //error_log("backup folder list => ".print_r($files,true));
        $last_bk = end($files);
    }
}

$ary = array(
    'licenseTitle' => $licenseTitle,
    'FileFlg' => $FileFlg,
    'DateFlg' => $DateFlg,
    'SerialFlg' => $SerialFlg,
    'UserFlg' => $UserFlg,
    'GroupFlg' => $GroupFlg,
    'QueryFlg' => $QueryFlg,
    'ClFlg' => $ClFlg,
    'PivotFlg' => $PivotFlg,
    'GraphFlg' => $GraphFlg,
    'ScheduleFlg' => $ScheduleFlg,
    'ErrFlg' => $errFlg,
    'SeigyoFlg' => $SeigyoFlg,
    'QueryGroupFlg' => $QueryGroupFlg,
    'SqlFlg'=> $SqlFlg,
    'FileOutFlg'=> $FileOutFlg,

    'licenseDate_S' => $licenseDate_S,
    'licenseDate_E' => $licenseDate_E,
    "licenseType" => $licenseType,
    "licenseTitle" => $licenseTitle,
    'licenseUser' => $licenseUser, //プラン
    'licenseQuery' => $licenseQuery,
    'licenseExcelcsvDownload' => $licenseExcelcsvDownload,
    'licenseSchedule' => $licenseSchedule,
    'licensePivot' => $licensePivot,
    'licenseCl' => $licenseCl,
    'licenseLog' => $licenseLog,
    'licenseTemplate' => $licenseTemplate,
    'licenseIFS' => $licenseIFS,
    'licenseDrilldown' => $licenseDrilldown,
    'licenseGraph' => $licenseGraph,
    'licenseSeigyo' => $licenseSeigyo,
    'licenseQueryGroup' => $licenseQueryGroup,
    'licenseSql'   => $licenseSql,
    'licenseFileOut'   => $licenseFileOut,

    'licenseDate_SP' => $licenseDate_SP,
    "licenseDate_SF" => $licenseDate_SF,
    'licenseDate_EP' => $licenseDate_EP,
    "licenseTitle_F" => $licenseTitle_F,
    'licenseUser_F' => $licenseUser_F, //フリー
    'licenseQuery_F' => $licenseQuery_F,
    'licenseExcelcsvDownload_F' => $licenseExcelcsvDownload_F,
    'licenseSchedule_F' => $licenseSchedule_F,
    'licenseScheduleBtn_F' => $licenseScheduleBtn_F,
    'licensePivot_F' => $licensePivot_F,
    'licenseCl_F' => $licenseCl_F,
    'licenseShow_F' => $licenseShow_F,
    'licenseLog_F' => $licenseLog_F,
    'licenseTemplate_F' => $licenseTemplate_F,
    'licenseIFS_F' => $licenseIFS_F,
    'licenseDrilldown_F' => $licenseDrilldown_F,
    'licenseGraph_F' => $licenseGraph_F,
    'licenseSeigyo_F' => $licenseSeigyo_F,
    'licenseQueryGroup_F' => $licenseQueryGroup_F,
    'licenseSql_F' => $licenseSql_F,
    'licenseFileOut_F' => $licenseFileOut_F,

    "licenseTitle_T" => $licenseTitle_T,
    'licenseUser_T' => $licenseUser_T, //トライアル
    'licenseQuery_T' => $licenseQuery_T,
    'licenseExcelcsvDownload_T' => $licenseExcelcsvDownload_T,
    'licenseSchedule_T' => $licenseSchedule_T,
    'licenseScheduleBtn_T' => $licenseScheduleBtn_T,
    'licensePivot_T' => $licensePivot_T,
    'licenseCl_T' => $licenseCl_T,
    'licenseShow_T' => $licenseShow_T,
    'licenseDate_ST' => $licenseDate_ST,
    'licenseDate_ET' => $licenseDate_ET,
    'licenseLog_T' => $licenseLog_T,
    'licenseTemplate_T' => $licenseTemplate_T,
    'licenseIFS_T' => $licenseIFS_T,
    'licenseDrilldown_T' => $licenseDrilldown_T,
    'licenseGraph_T' => $licenseGraph_T,
    'licenseSeigyo_T' => $licenseSeigyo_T,
    'licenseQueryGroup_T' => $licenseQueryGroup_T,
    'licenseSql_T' => $licenseSql_T,
    'licenseFileOut_T' => $licenseFileOut_T,

    "licenseTitle_P" => $licenseTitle_P,
    'licenseUser_P' => $licenseUser_P, //プラン
    'licenseQuery_P' => $licenseQuery_P,
    'licenseExcelcsvDownload_P' => $licenseExcelcsvDownload_P,
    'licenseSchedule_P' => $licenseSchedule_P,
    'licenseScheduleBtn_P' => $licenseScheduleBtn_P,
    'licensePivot_P' => $licensePivot_P,
    'licenseCl_P' => $licenseCl_P,
    'licenseDate_SP' => $licenseDate_SP,
    'licenseDate_EP' => $licenseDate_EP,
    'licenseShow_P' => $licenseShow_P,
    'licenseLog_P' => $licenseLog_P,
    'licenseTemplate_P' => $licenseTemplate_P,
    'licenseIFS_P' => $licenseIFS_P,
    'licenseDrilldown_P' => $licenseDrilldown_P,
    'licenseGraph_P' => $licenseGraph_P,
    'licenseSeigyo_P' => $licenseSeigyo_P,
    'licenseQueryGroup_P' => $licenseQueryGroup_P,
    'licenseSql_P' => $licenseSql_P,
    'licenseFileOut_P' => $licenseFileOut_P,

    'flag' => $flag,
    'RTN' => $rtn,
	'KukakuFlg' => $KukakuFlg,
	'licenseKukaku_F' => $licenseKukaku_F,
	'licenseKukaku_T' => $licenseKukaku_T,
	'licenseKukaku_P' => $licenseKukaku_P,
    //checklicense    
    'LOGIN' => $login,
    'USR' => $usrinfo,
    'MSG' => $msg,
    'licenseText' => $licenseText,
    'AXESPARAM' => AXESPARAM,
    'SESSION' => $_SESSION,
    /////////////////////color
    'WUCLR1' => $wuclr1,
    'WUCLR2' => $wuclr2,
    'WUCLR3' => $wuclr3,
    /////////////////////languageUser
    'WULANG' => $WULANG,
    'USER' => $DB2WUSR[0],
    'BASE_URL' => BASE_URL,
    'CANCELKEY'=>$keychk,
    'LastBackup' =>$last_bk

);
 
//BASE側ライセンスチェック用
fnPUTLICINF($db2con, $ary);
cmDb2Close($db2con);
echo (json_encode($ary));

/*
 *-------------------------------------------------------* 
 * ライセンス情報書き込み
 *-------------------------------------------------------*
 */
function fnPUTLICINF($db2con, $ary)
{
    
    $flg = array();
    $ret = '0';
    
    $strSQL = ' UPDATE DB2WLIC SET ';
    $strSQL .= ' WLDATE = ? ';
    $strSQL .= ',WLSRLN = ? ';
    $strSQL .= ',WLUSER = ? ';
    $strSQL .= ',WLQRY  = ? ';
    $strSQL .= ',WLCL   = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $ret = '1';
    } else {
        if ($ary['DateFlg']) {
            $flg[] = '1';
        } else {
            $flg[] = '0';
        }
        if ($ary['SerialFlg']) {
            $flg[] = '1';
        } else {
            $flg[] = '0';
        }
        if ($ary['UserFlg']) {
            $flg[] = '1';
        } else {
            $flg[] = '0';
        }
        if ($ary['QueryFlg']) {
            $flg[] = '1';
        } else {
            $flg[] = '0';
        }
        if ($ary['ClFlg']) {
            $flg[] = '1';
        } else {
            $flg[] = '0';
        }
        $r = db2_execute($stmt, $flg);
        if ($r === false) {
            $ret = '1';
        }
    }
    return $ret;
    
}

/*
 *-------------------------------------------------------* 
 * ユーザー数取得
 *-------------------------------------------------------*
 */
function fnGetUserCount($db2con, $licenseUser)
{
    
    $data = array();
    
    $strSQL = ' SELECT COUNT(A.WUUID) AS COUNT ';
    $strSQL .= ' FROM DB2WUSR as A ';
    
    $params = array();
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = $data[0]['COUNT'];
        }
    }
    return $data;
}

/*
 *-------------------------------------------------------* 
 * グループ数取得
 *-------------------------------------------------------*
 */
function fnGetGroupCount($db2con)
{
    
    $data = array();
    
    $strSQL = ' SELECT COUNT(A.WQGID) AS COUNT ';
    $strSQL .= ' FROM DB2WQGR as A ';
    
    $params = array();
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = $data[0]['COUNT'];
        }
    }
    
    return $data;
    
}

/*
 *-------------------------------------------------------* 
 * クエリー登録数取得
 *-------------------------------------------------------*
 */
function fnGetQueryCount($db2con)
{
    
    $data = array();
    
    $strSQL = ' SELECT COUNT(A.D1NAME) AS COUNT ';
    $strSQL .= ' FROM FDB2CSV1 AS A ';
    
    $params = array();
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = $data[0]['COUNT'];
        }
    }
    return $data;
    
}
//check license


/*
 *-------------------------------------------------------* 
 * ユーザー情報取得
 *-------------------------------------------------------*
 */

function regetDB2WUSR($db2con, &$rs = false, $user)
{
    
    $data = array();
    
    $strSQL = ' SELECT A.WUUID,A.WUUNAM,A.WUAUTH,A.WUDWNL,A.WUDWN2,A.WUSIZE,A.WUSAUT FROM DB2WUSR AS A ';
    $strSQL .= ' WHERE WUUID = ? ';
    $params = array(
        $user
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
        if (count($data) > 0) {
            $rs = true;
        }
    }
    
    return $data;
    
}
/*
*-------------------------------------------------------* 
* 区画登録数取得
*-------------------------------------------------------*
*/
function fnGetKukakuCount($db2con){

    $data = array();
    $strSQL  = ' SELECT count(A.RDBNM) as COUNT ';
    $strSQL .= ' FROM DB2RDB as A ' ;
    $strSQL .= ' WHERE RDBNM <> \'\' ';

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = $data[0]['COUNT'];
        }
    }
    return $data;

}
//color
/*
 *-------------------------------------------------------* 
 * ユーザーマスター取得
 *-------------------------------------------------------*
 */

function fnGetDB2WUSR($db2con, $id)
{
    
    $data = array();
    
    $params = array();
    
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WUSR as A ';
    $strSQL .= ' WHERE WUUID <> \'\' ';
    
    if ($id != '') {
        $strSQL .= ' AND WUUID = ? ';
        array_push($params, $id);
    }
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}

/*
 *-------------------------------------------------------* 
 * ログインユーザが削除されるかどうかチェック
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * @param String  $id     ユーザーID
 * 
 *-------------------------------------------------------*
 */

function fnGetWUAUTH($db2con, $id)
{
    
    $data = array();
    
    $params = array();
    
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WUSR as A ';
    $strSQL .= ' WHERE WUUID <> \'\' ';
    
    if ($id != '') {
        $strSQL .= ' AND WUUID = ? ';
        array_push($params, $id);
    }
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $data = array(
                    'result' => 'NOTEXIST_GET'
                );
            } else {
                $data = array(
                    'result' => true
                );
            }
        }
    }
    return $data;
    
}
/////////////////////////////////////////////////////////////////////////langUsr

function fnChkDB2LANG($db2con, $WULANG)
{

	$data = array();
	$rs   = '001';

	$params = array();

	$strSQL = ' SELECT A.* ';
	$strSQL .= ' FROM DB2LANG as A ';
	$strSQL .= ' WHERE LANID = ? ';

	$stmt = db2_prepare($db2con, $strSQL);
	if ($stmt === false) {
		// 処理なし
	} else {
		$params = array(
		$WULANG
		);
		$r      = db2_execute($stmt, $params);
		if ($r === false) {
		// 処理なし
		} else {
			while ($row = db2_fetch_assoc($stmt)) {
				$data[] = $row;
			}
			$data = umEx($data);
			if (count($data) > 0) {
				$rs = $data[0]['LANID'];
			}
		}
	}
	return $rs;
}



/*
 *-------------------------------------------------------* 
 * ログインチェック
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * @param String  $user  ユーザーID
 * @param String  $pass パスワード
 * 
 *-------------------------------------------------------*
 */

/*function LoginDB2WUSR($db2con, $user, $pass) {

$data = array();

$sql   = 'SET ENCRYPTION PASSWORD = \'' . RDB_KEY . '\'';
$stmt1 = db2_prepare($db2con, $sql);
if ($stmt1 === false) {
$data = array(
'result' => 'FAIL_SEL'
);
} else {
$r = db2_execute($stmt1);
if ($r === false) {
$data = array(
'result' => 'FAIL_SEL'
);
} else {
$strSQL = ' SELECT A.WUUID,A.WUUNAM,A.WUAUTH,A.WUDWNL,A.WUDWN2,A.WUSIZE,A.WUSAUT FROM DB2WUSR AS A ';
$strSQL .= ' WHERE WUUID = ? ';
$strSQL .= ' AND DECRYPT_CHAR(WUPSWE) = ? ';
$params = array(
$user,
$pass
);
e_log('SQL'.$strSQL);
e_log('param'.print_r($params,true));

$stmt = db2_prepare($db2con, $strSQL);
if ($stmt === false) {
$data = array(
'result' => 'FAIL_SEL'
);
} else {
$r = db2_execute($stmt, $params);

if ($r === false) {
$data = array(
'result' => 'FAIL_SEL'
);
} else {
while ($row = db2_fetch_assoc($stmt)) {
$data[] = $row;
}
$data = array(
'result' => true,
'data' => $data
);
}

}

}
}

return $data;

}*/


/*
 *-------------------------------------------------------* 
 * 前ログイン日の時間をチェックすること
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * @param String  $WUIID  ユーザーID
 * 
 *-------------------------------------------------------*
 */
/*function fnCheckWUEDAY($db2con, $user) {
$data = array();

$strSQL = ' SELECT WUEDAY ';
$strSQL .= ' FROM DB2WUSR ';
$strSQL .= ' WHERE WUUID = ? ';

$params = array(
$user
);

$stmt = db2_prepare($db2con, $strSQL);
if ($stmt === false) {
$data = array(
'result' => 'FAIL_SEL'
);
} else {
$r = db2_execute($stmt, $params);

if ($r === false) {
$data = array(
'result' => 'FAIL_SEL'
);
} else {
while ($row = db2_fetch_assoc($stmt)) {
$data[] = $row;
}
$data = array(
'result' => true,
'data' => $data
);
}
}


return $data;

}*/
/*
 *-------------------------------------------------------* 
 * DB2WUSRのWULGNC COLUMN データ取得
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * 
 *-------------------------------------------------------**/

/*function fnCHECKWULGNC($db2con, $user) {
$data = array();

//構文
$strSQL = ' SELECT WULGNC, WUAUTH, WUSAUT';
$strSQL .= ' FROM DB2WUSR ';
$strSQL .= ' WHERE ';
$strSQL .= ' WUUID = ? ';

$params = array(
$user
);

$stmt = db2_prepare($db2con, $strSQL);
if ($stmt === false) {
$data = array(
'result' => 'FAIL_SEL'
);
} else {
$r = db2_execute($stmt, $params);
if ($r === false) {
$data = array(
'result' => 'FAIL_SEL'
);
} else {
while ($row = db2_fetch_assoc($stmt)) {
$data[] = $row;
}
$data = array(
'result' => true,
'data' => $data
);
}
}

return $data;

}*/


/*
 *-------------------------------------------------------* 
 * DB2WUSRのWULGNC COLUMN データ更新
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * 
 *-------------------------------------------------------**/

/*function fnUpdWULGNC($db2con, $user, $loginCount) {

$rs     = true;
//構文
$strSQL = ' UPDATE DB2WUSR ';
$strSQL .= ' SET WULGNC = ? ';
$strSQL .= ' WHERE ';
$strSQL .= ' WUUID = ? ';
$params = array(
$loginCount,
$user
);


$stmt = db2_prepare($db2con, $strSQL);
if ($stmt === false) {
$rs = 'FAIL_UPD';
} else {
$r = db2_execute($stmt, $params);
if ($r === false) {
$rs = 'FAIL_UPD';
}
}

return $rs;

}*/

/*function  fnUpWULANG($db2con,$user,$lang){
$rs = true;
$strSQL  = ' UPDATE DB2WUSR ';
$strSQL .= ' SET WULANG = ?';
$strSQL .= ' WHERE ';
$strSQL .= ' WUUID = ? ';
$stmt = db2_prepare($db2con, $strSQL);
if ($stmt === false) {
$rs = 'FAIL_UPD';
} else {
if($lang === '001'){
$lang = '';
}
$params = array($lang,$user);
$r = db2_execute($stmt, $params);
if ($r === false) {
$rs = 'FAIL_UPD';
}
}
return $rs;
}*/