<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$RDBNM   = (isset($_POST['RDBNM'])?$_POST['RDBNM']:'');
$RDBHNM  = (isset($_POST['RDBHNM'])? $_POST['RDBHNM']: '');
$RDBUSR  = (isset($_POST['RDBUSR'])? $_POST['RDBUSR']: '');
$RDBPASS  = (isset($_POST['RDBPASS']) ? $_POST['RDBPASS']: '');
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/
$RDBNM   = cmHscDe($RDBNM);
$RDBHNM  = cmHscDe($RDBHNM);
$RDBUSR  = cmHscDe($RDBUSR);
$RDBPASS = cmHscDe($RDBPASS);
$db2con  = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー名'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('区画の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){          
            $rs = cmChkKenGen($db2con,'25',$userData[0]['WUSAUT']);//'1' => kukakuMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('区画の権限'));
            }
        }
    }
}
if($proc === 'DEL'){
    if($rtn === 0){
        //削除対象RDB名存在チェック処理
        $rs = fnCheckDB2RDB($db2con,$RDBNM);
        if( $rs === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_DEL',array('区画'));
            $focus = 'RDBNM';
        }
    }
    if($rtn === 0){
        //削除対象のRDB名を使用しているクエリがあるかのチェック処理
        $rs = fnChkRDBQry($db2con,$RDBNM);
        if( $rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('区画','クエリー'));
            $focus = 'RDBNM';
        }
    }
    if($rtn === 0){
        //ＨＴＭＬタグを削除する
        $rs = fnDeleteDB2RDB($db2con,$RDBNM);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('区画'));
            $focus = 'RDBNM';
        }
    }
   if($rtn === 0){
        $rs = fnDeleteDB2LBLS($db2con,$RDBNM);
        if($rs !== true){
            $rtn = 1;
            $focus = 'RDBNM';
        }
    }
}else{
	if($proc === 'ADD' && $licenseKukaku !==''){
		//区画ライセンスチェック
		$rs = fnGetKukakuCount($db2con);
		if($rs === false){
	        $rtn = 1;
	        $msg = showMsg('FAIL_SQL',array('区画'));
		}else{
			$count = $rs;
	        if($count >= (int)$licenseKukaku){
		        $rtn = 1;
		        $msg = showMsg('NO_ADD_BUT',array('区画'));
	        }
		}
	}
    // RDB名必須チェック
    if($rtn === 0){
          if($RDBNM  === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('区画名'));
            $focus = 'RDBNM';
        }
    }
    // RDB名必須チェック
    if($rtn === 0){
          if(cmMer($RDBNM)  === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_SPACECHK',array('区画名'));
            $focus = 'RDBNM';
        }
    }
    // RDB名入力桁数チェック
    if($rtn === 0){
        if(!checkMaxLen($RDBNM,18)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('区画名'));
            $focus = 'RDBNM';
        }
    }
    // RDB名のバイトチェック
    if($rtn === 0){
        $byte = checkByte($RDBNM);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('区画名'));
            $focus = 'RDBNM';
        }
    }
    // RDB名の正規表現
    if($rtn === 0){
        if (mb_ereg('^[ｱ-ﾝ]+$', $RDBNM)) {
            $rtn = 1;
            $msg = showMsg('FAIL_BYTKANA',array('区画名'));
            $focus = 'RDBNM';
        }
    }
    // RDB名の正規表現
    if($rtn === 0){
        if ($RDBNM === RDB || $RDBNM === RDBNAME) {
            $rtn = 1;
            $msg = showMsg('FAIL_SAVECHK',array('システムのメーン区画'));//showMsg('システムのメーン区画は登録できません。');
            $focus = 'RDBNM';
        }
    }

    /*// RDBユーザー必須チェック
    if($rtn === 0){
          if($RDBUSR  === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('ユーザー'));
            $focus = 'RDBUSR';
        }
    }*/

    // RDBユーザー入力桁数チェック
    if($rtn === 0){
        if(!checkMaxLen($RDBHNM,50)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('名前'));
            $focus = 'RDBHNM';
        }
    }
    // RDBユーザー入力桁数チェック
    if($rtn === 0){
        if(!checkMaxLen($RDBUSR,10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('ユーザー名'));
            $focus = 'RDBUSR';
        }
    }

    // RDBユーザーのバイトチェック
    if($rtn === 0){
        $byte = checkByte($RDBUSR);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('ユーザー名'));
            $focus = 'RDBUSR';
        }
    }
    // RDBユーザーの正規表現
    if($rtn === 0){
        if (mb_ereg('^[ｱ-ﾝ]+$', $RDBUSR)) {
            $rtn = 1;
            $msg = showMsg('FAIL_BYTKANA',array('区画ユーザー'));
            $focus = 'RDBUSR';
        }
    }

    // RDBパスワード必須チェック
    if($rtn === 0){
          if($RDBPASS  === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('パスワード'));
            $focus = 'RDBPASS';
        }
    }
    // RDBパスワード入力桁数チェック
    if($rtn === 0){
        if(!checkMaxLen($RDBPASS,10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('パスワード'));
            $focus = 'RDBPASS';
        }
    }

    // RDBパスワードのバイトチェック
    if($rtn === 0){
        $byte = checkByte($RDBPASS);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('パスワード'));
            $focus = 'RDBPASS';
        }
    }
    // RDBパスワードの正規表現
    if($rtn === 0){
        if (mb_ereg('^[ｱ-ﾝ]+$', $RDBPASS)) {
            $rtn = 1;
            $msg = showMsg('FAIL_BYTKANA',array('パスワード'));
            $focus = 'RDBPASS';
        }
    }


if($proc === 'ADD'){
    if($rtn === 0){
        //区画存在チェック処理
        $rs = fnCheckDB2RDB($db2con,$RDBNM);
        if( $rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('区画'));
            $focus = 'RDBNM';
        }
    }
    if($rtn === 0){
        //新区画を登録処理
        $rs = fnInsertDB2RDB($db2con,$RDBNM,$RDBHNM,$RDBUSR,$RDBPASS);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}else if($proc === 'UPD'){
    if($rtn === 0){
        //更新対象区画存在チェック処理
        $rs = fnCheckDB2RDB($db2con,$RDBNM);
        if( $rs === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_UPD',array('区画'));
            $focus = 'RDBNM';
        }
    }
    if($rtn === 0){
        //更新対象区画を変更する処理
        $rs = fnUpdateDB2RDB($db2con,$RDBNM,$RDBHNM,$RDBUSR,$RDBPASS);
        if( $rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
	'licenseKukaku' =>$licenseKukaku,
	'count' => $count
);

echo(json_encode($rtnAry));

/**
*-------------------------------------------------------* 
* 区画が存在するかどうかのチェックすること
* 
* RESULT
*    01：存在する場合     true
*    02：存在しない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $RDBNM 区画名
* 
*-------------------------------------------------------*
*/
function fnCheckDB2RDB($db2con,$RDBNM){

    $data = array();
    $rs = true;

    $strSQL .= ' SELECT ';
    $strSQL .= '     RDBNM, ';
    $strSQL .= '     RDBUSR, ';
    $strSQL .= '     RDBPASS ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2RDB ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     RDBNM = ? ';

    $params = array($RDBNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* 新区画を登録すること
* 
* RESULT
*    01：登録終了の場合   true
*    02：エラーの場合     false
* 
* @param Object  $db2con  DBコネクション
* @param String  $RDBNM   区画名
* @param String  $RDBUSR  区画ユーザー
* @param String  $RDBPASS 区画パスワード
* 
*-------------------------------------------------------*
*/
function fnInsertDB2RDB($db2con,$RDBNM,$RDBHNM,$RDBUSR,$RDBPASS){

    $rs = true;
    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SQL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SQL';
        }
    }
    if($rs === true)
    {   //構文
        $strSQL  = ' INSERT INTO DB2RDB ';
        $strSQL .= ' ( ';
        $strSQL .= '     RDBNM, ';
        $strSQL .= '     RDBHNM, ';
        $strSQL .= '     RDBUSR, ';
        $strSQL .= '     RDBPASS ';
        $strSQL .= ' ) ';
        $strSQL .= ' VALUES ';
        $strSQL .= ' (?,?,?,CAST(encrypt(CAST(? AS VARCHAR(10))) AS CHAR(32) FOR BIT DATA)) ';

        $params = array($RDBNM,$RDBHNM,$RDBUSR,$RDBPASS);

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_INS';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_INS';
            }
        }
    }
    return $rs;
}


/**
*-------------------------------------------------------* 
* メールグアドレスを更新すること
* 
* RESULT
*    01：更新終了の場合   true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $HTMLNM ＨＴＭＬ名
* 
*-------------------------------------------------------*
*/
function fnUpdateDB2RDB($db2con,$RDBNM,$RDBHNM,$RDBUSR,$RDBPASS){

    $rs = true;
    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SQL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SQL';
        }
    }
    if($rs === true)
    {
        //構文
        $strSQL  = ' UPDATE DB2RDB ';
        $strSQL .= ' SET ';
        $strSQL .= ' RDBUSR = ? , ';
        $strSQL .= ' RDBHNM = ? , ';
        $strSQL .= ' RDBPASS = CAST(encrypt(CAST(? AS VARCHAR(10))) AS CHAR(32) FOR BIT DATA) ';
        $strSQL .= ' WHERE ';
        $strSQL .= ' RDBNM = ? ';

        $params = array($RDBUSR,$RDBHNM,$RDBPASS,$RDBNM);

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_UPD';
        }else{
            e_log($strSQL.print_r($params,true));
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_UPD';
            }
        }
    }
    return $rs;
}

function fnDeleteDB2RDB($db2con,$RDBNM){
    $rs = true;
    $strSQL = 'DELETE FROM DB2RDB ';
    $strSQL .= ' WHERE RDBNM = ? ';
    $params = array($RDBNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
function fnDeleteDB2LBLS($db2con,$RDBNM){
    $rs = true;
    $strSQL = 'DELETE FROM DB2LBLS ';
    $strSQL .= ' WHERE LIBSDB = ? ';
    $params = array($RDBNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
function fnChkRDBQry($db2con,$RDBNM){
    $data = array();
    $rs = true;

    $strSQL .= ' SELECT ';
    $strSQL .= '      COUNT(*) AS COUNT';
    $strSQL .= ' FROM ';
    $strSQL .= '     FDB2CSV1 ';
    $strSQL .= ' WHERE ';
    $strSQL .= '      D1RDB  = ? ';

    $params = array($RDBNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if($data[0]['COUNT']>0){
                $rs = 'NOCAN_DEL';
            }
        }
    }
    return $rs;
}
/*
*-------------------------------------------------------* 
* 区画登録数取得
*-------------------------------------------------------*
*/
function fnGetKukakuCount($db2con){

    $data = array();
    $strSQL  = ' SELECT count(A.RDBNM) as COUNT ';
    $strSQL .= ' FROM DB2RDB as A ' ;
    $strSQL .= ' WHERE RDBNM <> \'\' ';

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = $data[0]['COUNT'];
        }
    }
    return $data;

}