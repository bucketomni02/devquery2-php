<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$PROC    = (isset($_POST['PROC'])?$_POST['PROC']:'');
$RDBNM   = (isset($_POST['RDBNM'])?$_POST['RDBNM']:'');
$RDBHNM  = (isset($_POST['RDBHNM'])?$_POST['RDBHNM']: '');
$RDBUSR  = (isset($_POST['RDBUSR'])?$_POST['RDBUSR']: '');
$start   = $_POST['start'];
$length  = $_POST['length'];
$sort    = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$htmlFlg = true;
$rtn = 0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$RDBNM   = cmHscDe($RDBNM);
$RDBUSR  = cmHscDe($RDBUSR);
$RDBHNM  = cmHscDe($RDBHNM);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('区画の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'25',$userData[0]['WUSAUT']);//'1' => kukakuMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('区画の権限'));
            }
        }
    }
}

if($PROC == 'EDIT'){
    if($rtn === 0){
        $res = fnSelDB2RDB($db2con,$RDBNM);
        if($res['result'] === true){
            if(count($res['data']) === 0){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET',array('区画'));
            }else{
                $data = $res['data'];
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $res = fnGetAllCount($db2con,$RDBNM,$RDBHNM,$RDBUSR);
        if($res['result'] === true){
            $allcount = $res['data'];
            $res = fnGetDB2RDB($db2con,$RDBNM,$RDBHNM,$RDBUSR,$start,$length,$sort,$sortDir);
            if($res['result'] === true){
                $data = $res['data'];
            }else{
                $rtn = 1;
                $msg = showMsg($res['result']);
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'DATA' => umEx($data,$htmlFlg),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));


/*
*-------------------------------------------------------* 
* メールグループ取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $RDBNM  RDB名
* @param String  $RDBUSR RDBユーザー
* @param String  $start
* @param String  $length
* @param String  $sort
* @param String  $sortDir
* 
*-------------------------------------------------------*
*/

function fnGetDB2RDB($db2con,$RDBNM,$RDBHNM,$RDBUSR,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();
    $rs = true;
    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';
    
    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SEL';
        }
    }

    if($rs === true){
        $strSQL = ' SELECT A.* ';
        $strSQL .= ' FROM ( SELECT B.RDBNM,B.RDBHNM,B.RDBUSR,DECRYPT_CHAR(B.RDBPASS) AS RDBPASS, ROWNUMBER() OVER( ';

        if($sort !== ''){
            $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= ' ORDER BY B.RDBNM ASC ';
        }

        $strSQL .= ' ) as rownum ';
        $strSQL .= ' FROM DB2RDB as B ';
        $strSQL .= ' WHERE RDBNM <> \'\' ';
        
        if($RDBNM != ''){
            $strSQL .= ' AND RDBNM like ? ';
            array_push($params,'%'.$RDBNM.'%');
        }

        if($RDBHNM != ''){
            $strSQL .= ' AND RDBHNM like ? ';
            array_push($params,'%'.$RDBHNM.'%');
        }

        if($RDBUSR != ''){
            $strSQL .= ' AND RDBUSR like ? ';
            array_push($params,'%'.$RDBUSR.'%');
        }

        $strSQL .= ' ) as A ';

        //抽出範囲指定
        if(($start != '')&&($length != '')){
            $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
            array_push($params,$start + 1);
            array_push($params,$start + $length);
        }
        e_log($strSQL.print_r($params,true));
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    }else{
        $data = array('result' => 'FAIL_SEL');
    }

    return $data;
}
/*
*-------------------------------------------------------* 
* メールグループ全件カウント取得処理
* 
* RESULT
*    01：データ件数
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $RDBNM  RDB名
* @param String  $RDBUSR RDBユーザー
* @param String  $start
* @param String  $length
* @param String  $sort
* @param String  $sortDir
* 
*-------------------------------------------------------*
*/
function fnGetAllCount($db2con,$RDBNM,$RDBHNM,$RDBUSR){
    $data = array();

    $strSQL  = ' SELECT count(A.RDBNM) as COUNT ';
    $strSQL .= ' FROM DB2RDB as A ' ;
    $strSQL .= ' WHERE RDBNM <> \'\' ';

    $params = array();

    if($RDBNM != ''){
        $strSQL .= ' AND RDBNM like ? ';
        array_push($params,'%'.$RDBNM.'%');
    }
    if($RDBHNM != ''){
        $strSQL .= ' AND RDBHNM like ? ';
        array_push($params,'%'.$RDBHNM.'%');
    }
    if($RDBUSR != ''){
        $strSQL .= ' AND RDBUSR like ? ';
        array_push($params,'%'.$RDBUSR.'%');
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}
/*
*-------------------------------------------------------* 
* 対象メールグループデータ取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $RDBNM  RDB名
* 
*-------------------------------------------------------*
*/
function fnSelDB2RDB($db2con,$RDBNM){

    $data = array();

    $params = array($RDBNM);

    $rs = true;
    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';
    
    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SEL';
        }
    }

    if($rs === true){
        $strSQL  = ' SELECT A.RDBNM,A.RDBHNM,A.RDBUSR,DECRYPT_CHAR(A.RDBPASS) AS RDBPASS ';
        $strSQL .= ' FROM DB2RDB as A ';
        $strSQL .= ' WHERE RDBNM = ? ';

        $stmt = db2_prepare($db2con,$strSQL);
        e_log($strSQL.print_r(params,true));
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    }
    return $data;
}