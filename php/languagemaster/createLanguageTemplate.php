<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/
$LANCD   = (isset($_POST['LANCD'])?$_POST['LANCD']:'');

$rtn      = 0;
$msg      = '';
$fileName = '';
$ext      = '';
$data     = array();
$db2con   = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
//ログインユーザが管理ユーザーかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('言語の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'26',$userData[0]['WUSAUT']);//'1' => languageMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('言語の権限'));
            }
        }
    }
}
$header = array(
    'ITMNM'   => '項目名',
    'ITMTEXT' => '表示名',
    );
if(cmMer($LANCD) === '' ){
    if($rtn === 0){
        $rs = fnGetDB2MTEXT($db2con);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $dataArr = umEx($rs['data']);
        }
    }
}else{
    if($rtn === 0){
        $rs = fnCheckDB2LANG($db2con,$LANCD);
        if($rs !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg('NOT_EXIST_UPD',array('言語'));
        }
    }
    if($rtn === 0){
        $rs = fnGetDB2MTEXTBYLAN($db2con,$LANCD);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $dataArr = umEx($rs['data']);
        }
    }
}
if ($rtn === 0) {
    if (EXCELVERSION === '2007' || EXCELVERSION === '2003' || EXCELVERSION === 'html' || EXCELVERSION === 'XLSX') {
        include_once("../common/lib/PHPExcel/Classes/PHPExcel.php");
        include_once("../common/lib/PHPExcel/Classes/PHPExcel/IOFactory.php");
        include_once("../common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");
        include_once("../common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php");

        $res = createXls($header,$dataArr);
        if(count($res>0)){
            $fileName = $res['FILENM'];
            $ext      = $res['EXT'];
        }
    }else if(EXCELVERSION === 'html'){
        $res = creteHtmlXls($header,$dataArr);
        if(count($res>0)){
            $fileName = $res['FILENM'];
            $ext      = $res['EXT'];
        }
    }
}

$rtnAry = array(
    'RTN'     => $rtn,
    'MSG'     => $msg,
    'HEADER'  => $header,
    'DATA'    => $dataArr,
    'FILENAME'=> $fileName,
    'EXT'     => $ext
);
echo (json_encode($rtnAry));

// クエリーマスタ情報取得
function fnGetDB2MTEXT($db2con){

    $data = array();
    $params = array();

    $strSQL  =  '   SELECT ';
    $strSQL  .=  '   ITMNM, ';
    $strSQL  .=  '   \'\' ';
    $strSQL  .=  '   FROM ';
    $strSQL  .=  '   DB2MTEXT ';
    $strSQL  .=  '   WHERE LANCD = \'001\'';
    $strSQL  .= '    ORDER BY ITMKBN ';
    $stmt = db2_prepare($db2con,$strSQL);
    e_log($strSQL.'aaa'.$SORT.$SORTDIR);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
            $err =db2_stmt_errormsg();
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $err =db2_stmt_errormsg();
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

// クエリーマスタ情報取得
function fnGetDB2MTEXTBYLAN($db2con,$LANCD){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT ';
    $strSQL .= '     A.ITMNM, ';
    $strSQL .= '     B.ITMTEXT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     (SELECT ';
    $strSQL .= '         * ';
    $strSQL .= '     FROM ';
    $strSQL .= '         DB2MTEXT ';
    $strSQL .= '     WHERE ';
    $strSQL .= '         LANCD = \'001\' ';
    $strSQL .= '     )A '; 
    $strSQL .= '     LEFT JOIN  ( ';
    $strSQL .= '     SELECT ';
    $strSQL .= '         * ';
    $strSQL .= '     FROM ';
    $strSQL .= '         DB2MTEXT ';
    $strSQL .= '     WHERE ';
    $strSQL .= '         LANCD = ? ';
    $strSQL .= '     )B  ';
    $strSQL .= '     ON A.ITMNM = B.ITMNM ';
    $strSQL .= '    ORDER BY B.ITMTEXT ';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
            $err =db2_stmt_errormsg();
    }else{
        $params = array($LANCD);
        $r = db2_execute($stmt,$params);
        if($r === false){
            $err =db2_stmt_errormsg();
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function createXls($headerData,$dataArr){
    $data = array();

    $book = new PHPExcel();
    $book->setActiveSheetIndex(0);
    $book->getActiveSheet()->setTitle("情報");
    $sheet = $book->getActiveSheet();
    $col = 0;
    $row = 1;

    // ヘーダ―印字
    foreach ($headerData as $key => $value) {
        $value = cmMer($value);
        $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
        $col++;
    }
    $row = $row + 1;

    //データ印字
    while (list($key1, $value1) = each($dataArr)) {
        $col = 0;
        foreach ($headerData as $hkey => $hvalue) {
            $val = cmMer($value1[$hkey]);
            $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($val, PHPExcel_Cell_DataType::TYPE_STRING);
            $col++;
        }
        unset($dataArr[$key1]);
        $row++;
    }

    $fileNm = 'itmInfo_'.date('YmdHis');
    if (EXCELVERSION === '2003' || EXCELVERSION === 'html') {
        $writer = PHPExcel_IOFactory::createWriter($book,'Excel5');
        $ext = 'xls';
    } else if (EXCELVERSION === '2007' || EXCELVERSION === 'XLSX') {
        $writer = PHPExcel_IOFactory::createWriter($book,'Excel2007');
        $ext = 'xlsx';
    }
    $writer->save(TEMP_DIR . cmMer($fileNm) . '.' . $ext);
    e_log('Template path'.TEMP_DIR . cmMer($fileNm) . '.' . $ext);
    $data = array(
        'FILENM' => $fileNm ,
        'EXT'    => $ext
   );
    return $data;
}

function creteHtmlXls($headerData,$dataArr){
    $data = array();
    $file_name = 'itmInfo_'.date('YmdHis');
    $ext       = 'xls';
    $fp = fopen(TEMP_DIR . cmMer($file_name).'.'.$ext, 'w');
    $decStyle = 'mso-number-format:\#\,\#\#0\'';
    $tableClass = '';
    if (isset($_SESSION['PHPQUERY'])) {
        if ($_SESSION['PHPQUERY']['LOGIN'] === '1') {
            $db2con = cmDb2Con();
            cmSetPHPQUERY($db2con);
            
            $DB2WUSR = cmGetDB2WUSR($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
            
            $DB2WUSR = umEx($DB2WUSR);
            
            cmDb2Close($db2con);
            
            $wuclr3 = $DB2WUSR[0]['WUCLR3'];
            $wuclr3 = (($wuclr3 === '') ? COLOR3 : $wuclr3);
            
            switch ($wuclr3) {
                case 'red':
                    $tableClass = 'table th{background-color:#b60f0b;color:#ffffff;}';
                    break;
                case 'blue':
                    $tableClass = 'table th{background-color:#005879;color:#ffffff;}';
                    break;
                case 'orange':
                    $tableClass = 'table th{background-color:#a66505;color:#ffffff;}';
                    break;
                case 'purple':
                    $tableClass = 'table th{background-color:#541fa7;color:#ffffff;}';
                    break;
                case 'green':
                    $tableClass = 'table th{background-color:#246534;color:#ffffff;}';
                    break;
                case 'fb':
                    $tableClass = 'table th{background-color:#192441;color:#ffffff;}';
                    break;
                case 'muted':
                    $tableClass = 'table th{background-color:#595959;color:#ffffff;}';
                    break;
                case 'dark':
                    $tableClass = 'table th{background-color:black;color:#ffffff;}';
                    break;
                case 'pink':
                    $tableClass = 'table th{background-color:#6b2345;color:#ffffff;}';
                    break;
                case 'brown':
                    $tableClass = 'table th{background-color:#4f2a1b;color:#ffffff;}';
                    break;
                case 'sea-blue':
                    $tableClass = 'table th{background-color:#013760;color:#ffffff;}';
                    break;
                case 'banana':
                    $tableClass = 'table th{background-color:#cb9704;color:#ffffff;}';
                    break;
                default:
                    $tableClass = 'table th{background-color:#f5f5f5;color:#666;}';
                    break;
            }
            
        }
    }
    if ($tableClass === '') {
        $tableClass = 'table th{background-color:rgb(79,129,189);color:#ffffff;}';
    }
    
    fwrite($fp, '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">');
    fwrite($fp, '<html><head>');
    
    if ($htmlFlg === true) {
        fwrite($fp, '<meta charset="UTF-8">');
    }
    
    fwrite($fp, '<meta http-equiv="Content-Type" content="application/vnd.ms-excel; " />');
    fwrite($fp, '<meta http-equiv="Content-Disposition" content="attachment; filename=output.xls" />');
    fwrite($fp, '<style type="text/css">');
    fwrite($fp, '<!-- .txt{mso-number-format:"\@";}-->');
    fwrite($fp, 'table{font-size:13px;}');
    fwrite($fp, $tableClass);
    fwrite($fp, '</style>');
    fwrite($fp, '</head><body>');
    fwrite($fp, '<table border="1">');
    fwrite($fp, '<thead><tr>');

    foreach ($headerData as $key => $value) {
        $value = str_replace(" ","&#32;",cmMer($value));
        $value = mb_convert_encoding($value, 'HTML-ENTITIES','UTF-8');
        fwrite($fp, '<th nowrap class="xls-header txt">' . $value . '</th>');
    }
    fwrite($fp, '</tr></thead><tbody>');
    foreach ($dataArr as $key => $row) {
        
        $bgcolor = '';
        if ($key % 2 !== 0) {
            $bgcolor = 'bgcolor="#dddddd"';
        }
        fwrite($fp, '<tr ' . $bgcolor . '>');
        
        foreach ($headerData as $hkey => $val) {
            $col = $row[$hkey];
            $col = str_replace(" ","&#32;",cmMer($col));
            $col = mb_convert_encoding($col,'HTML-ENTITIES','UTF-8');
            if($hkey === 'DQEKAI' ){
                fwrite($fp, '<td nowrap align="right" style="' . $decStyle . '">' . $col . '</td>');
            }else{
                fwrite($fp, '<td nowrap align="left" class="txt">' . $col . '</td>');
            }
        }
        fwrite($fp, '</tr>');

    }
    fwrite($fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');

    fclose($fp);

    $data = array(
        'FILENM' => $file_name ,
        'EXT'    => $ext
   );
    return $data;
}
function fnCheckDB2LANG($db2con,$LANID){

    $data = array();
    $rs = true;

    $strSQL .= ' SELECT ';
    $strSQL .= '     LANID, ';
    $strSQL .= '     LANNM, ';
    $strSQL .= '     LANFIL ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2LANG ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     LANID = ? ';

    $params = array($LANID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}
