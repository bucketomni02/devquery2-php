<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$PROC    = (isset($_POST['PROC'])?$_POST['PROC']:'');
$LANID   = (isset($_POST['LANID'])?$_POST['LANID']:'');
$LANNM   = (isset($_POST['LANNM'])? $_POST['LANNM']: '');
$LANFIL  = (isset($_POST['LANFIL']) ? $_POST['LANFIL']: '');
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$success = true;
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/
$LANID   = cmHscDe($LANID);
$LANNM   = cmHscDe($LANNM);
$LANFIL  = cmHscDe($LANFIL);
$db2con  = cmDb2Con();
cmSetPHPQUERY($db2con);
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('言語の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'26',$userData[0]['WUSAUT']);//'1' => languageMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('言語の権限'));
            }
        }
    }
}
if($PROC === 'DEL'){
    if($rtn === 0){
        //言語ID存在チェック処理
        $rs = fnCheckDB2LANG($db2con,$LANID);
        if( $rs === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_DEL',array('言語'));
            $focus = 'LANID';
        }
    }
    if($rtn === 0){
        //言語を削除する
        $rs = fnDeleteDB2LANG($db2con,$LANID);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('言語'));
        }else{
            $rs = fnDelDB2MTEXT($db2con,$LANID);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('言語'));
            }
        }
    }
}else{
    // 言語ID必須チェック
    if($rtn === 0){
          if($LANID  === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('言語ID'));
            $focus = 'LANID';
        }
    }
    // 言語ID必須チェック
    if($rtn === 0){
          if(cmMer($LANID)  === '001'){
            $rtn = 1;
            $msg = showMsg('FAIL_SYS_DEF',array('言語ID',array('システム','デフォールト')));
            $focus = 'LANID';
        }
    }
    // 言語ID必須チェック
    if($rtn === 0){
          if(cmMer($LANID)  === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_SPACECHK',array('言語ID'));
            $focus = 'LANID';
        }
    }
    // 言語ID入力桁数チェック
    if($rtn === 0){
        if(!checkMaxLen($LANID,10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('言語ID'));
            $focus = 'LANID';
        }
    }
    // 言語IDのバイトチェック
    if($rtn === 0){
        $byte = checkByte($LANID);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('言語ID'));
            $focus = 'LANID';
        }
    }
    // 言語IDの正規表現
    if($rtn === 0){
        if (mb_ereg('^[ｱ-ﾝ]+$', $LANID)) {
            $rtn = 1;
            $msg = showMsg('FAIL_BYTKANA',array('言語ID'));
            $focus = 'LANID';
        }
    }
    if($rtn === 0){
        if($PROC === 'ADD'){
            //追加対象言語存在チェック処理
            $rs = fnCheckDB2LANG($db2con,$LANID);
            if($rs !== true){
                 $rtn = 1;
                $msg = showMsg($rs,array('言語ID'));
                $focus = 'LANID';
            }
        }else if($PROC === 'UPD'){
            //更新対象言語存在チェック処理
            $rs = fnCheckDB2LANG($db2con,$LANID);
            if( $rs === true){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_UPD',array('言語ID'));
                $focus = 'LANID';
            }
        }
    }

    // 言語名入力桁数チェック
    if($rtn === 0){
        if(!checkMaxLen($LANNM,50)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('言語名'));
            $focus = 'LANNM';
        }
    }

    if(isset($_FILES)){
        //e_log('files'.print_r($_FILES,true));
        $txt_lanfil = $_FILES['LANFIL']['name'];
        if($txt_lanfil !== ''){
            $ext = explode ('.', $txt_lanfil);
            $ext = $ext [count ($ext) - 1];
            $tmp_lanfil = $_FILES['LANFIL']['tmp_name'];
            $LANFIL = $txt_lanfil;
        }else{
            $LANFIL = '';
        }
    }
    if($rtn === 0){
        if($PROC === 'ADD'){
            //新区画を登録処理
            $rs = fnInsertDB2LANG($db2con,$LANID,$LANNM,$LANFIL);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }else if($PROC === 'UPD'){
            //更新対象区画を変更する処理
            $rs = fnUpdateDB2LANG($db2con,$LANID,$LANNM,$LANFIL);
            if( $rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }
    }
    if($rtn === 0){
        //テンプレートのファイルデータを読み込み開始
        if($LANFIL !== ''){
            e_log('templatefile読み込み');
            $files = glob(BASE_DIR."/php/template/".$LANID.".*");
                foreach ($files as $file) {
                    unlink($file);
                }
            $lantemp = $LANID.'.'.$ext;
            $flg = move_uploaded_file ($tmp_lanfil, "../langtemplate/".$lantemp);
            if($flg === true){
                $templateData = fnReadTemplate(BASE_DIR."/php/langtemplate/".$lantemp);
                if($templateData['result'] !== true){
                    $rtn = 1;
                    $success = false;
                    $msg = showMsg($templateData['result']);
                }else{
                    //項目マスタから抽出
                    $rs = fnGetDB2MTEXT($db2con);
                    if($rs['result'] !== true){
                        $rtn = 1;
                        $success = false;
                        $msg  = showMsg('FAIL_CHK',array('テンプレートデータ'));//'テンプレートデータが間違っています。';
                    }else{
                        $mtext = $rs['data'];
                        $rsIns = fnInsDB2MTEXT($db2con,$LANID,$mtext,$templateData['data']);
                        if($rsIns !== true){
                            $rtn = 1;
                            $success = false;
                            $msg  = showMsg($rsIns);
                        }
                    }
                }
            }else{
                $rtn = 1;
                $success = false;
                $msg  = showMsg('FAIL_FILE_INS',array('テンプレート'));//'テンプレートの登録処理ができませんでした。';
            }
        }
        //テンプレートのファイルデータを読み込み終了
    }
}
if($rtn === 0){
    db2_commit($db2con);
}else{
    db2_rollback($db2con);
}
cmDb2Close($db2con);

if($rtn !== 0){
    $success = false;
}
/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'TEMPLATEDATA' => $templateData,
    'success' =>$success
);
echo(json_encode($rtnAry));

/**
*-------------------------------------------------------* 
* 区画が存在するかどうかのチェックすること
* 
* RESULT
*    01：存在する場合     true
*    02：存在しない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $RDBNM 区画名
* 
*-------------------------------------------------------*
*/
function fnCheckDB2LANG($db2con,$LANID){

    $data = array();
    $rs = true;

    $strSQL .= ' SELECT ';
    $strSQL .= '     LANID, ';
    $strSQL .= '     LANNM, ';
    $strSQL .= '     LANFIL ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2LANG ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     LANID = ? ';

    $params = array($LANID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* 新区画を登録すること
* 
* RESULT
*    01：登録終了の場合   true
*    02：エラーの場合     false
* 
* @param Object  $db2con  DBコネクション
* @param String  $RDBNM   区画名
* @param String  $RDBUSR  区画ユーザー
* @param String  $RDBPASS 区画パスワード
* 
*-------------------------------------------------------*
*/
function fnInsertDB2LANG($db2con,$LANID,$LANNM,$LANFIL){

    $rs = true;
   //構文
        $strSQL  = ' INSERT INTO DB2LANG ';
        $strSQL .= ' ( ';
        $strSQL .= '     LANID, ';
        $strSQL .= '     LANNM, ';
        $strSQL .= '     LANFIL ';
        $strSQL .= ' ) ';
        $strSQL .= ' VALUES ';
        $strSQL .= ' (?,?,?) ';

        $params = array($LANID,$LANNM,$LANFIL);

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_INS';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_INS';
            }
        }
    return $rs;
}


/**
*-------------------------------------------------------* 
* メールグアドレスを更新すること
* 
* RESULT
*    01：更新終了の場合   true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $HTMLNM ＨＴＭＬ名
* 
*-------------------------------------------------------*
*/
function fnUpdateDB2LANG($db2con,$LANID,$LANNM,$LANFIL){

    $rs = true;
    $params = array();

    //構文
    $strSQL  = ' UPDATE DB2LANG ';
    $strSQL .= ' SET ';
    $strSQL .= ' LANNM = ? ';
    array_push($params,$LANNM);
    if($LANFIL !== ''){
        $strSQL .= ' , LANFIL = ? ';
        array_push($params,$LANFIL);
    }
    $strSQL .= ' WHERE ';
    $strSQL .= ' LANID = ? ';
    array_push($params,$LANID);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        //e_log($strSQL.print_r($params,true));
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }

    return $rs;
}

function fnDeleteDB2LANG($db2con,$LANID){
    $rs = true;
    $strSQL = 'DELETE FROM DB2LANG ';
    $strSQL .= ' WHERE LANID = ? ';
    $params = array($LANID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
function fnReadTemplate($filename){
    include_once("../common/lib/PHPExcel/Classes/PHPExcel/IOFactory.php");
    $objPHPExcel = PHPExcel_IOFactory::load($filename);
    $dataArr = array();
    $dcnt    = 0;
    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
        $worksheetTitle     = $worksheet->getTitle();
        $highestRow         = $worksheet->getHighestRow(); 
        $highestColumn      = $worksheet->getHighestColumn(); 
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $nrColumns = ord($highestColumn) - 64;
        for ($row = 1; $row <= $highestRow; ++ $row) {
            $rowData = array();
            for ($col = 0; $col < $highestColumnIndex; ++ $col) {
                $cell = $worksheet->getCellByColumnAndRow($col, $row);
                $val = $cell->getValue();
                if(is_object($val)){
                  $val= $val->__toString();
                }
                $rowData[] = $val;
            }
            //e_log('ファイルデータ：[0]'.($rowData[0] === null ? 'NULL':$rowData[0]).'[1]:'.($rowData[1]===null?'NULL':$rowData[1]));
            if($rowData[0] !== null){
                if($rowData[1] === null){
                    $dcnt = $dcnt +1;
                    $dataArr[$rowData[0]] = '';
                }else{
                    $dataArr[$rowData[0]] = $rowData[1];
                }
            }
        }
    }
    $rtnArr = array();
    if(count($dataArr) === 0){
        $rtnArr['result'] = '登録されたテンプレートファイルにデータがありません。';
    }else if($dcnt === count($dataArr)){
        $rtnArr['result'] = 'テンプレートファイルに表示項目全部に記入されていません。';
    }else{
        $rtnArr['result'] = true;
        $rtnArr['data']  = $dataArr;
    }
    //e_log('結果配列：'.print_r($rtnArr,true));
    return $rtnArr;
}
function fnGetDB2MTEXT($db2con){
    $data = array();
    $params = array();

    $strSQL  =  '   SELECT ';
    $strSQL  .=  '   ITMNM ';
    $strSQL  .=  '   FROM ';
    $strSQL  .=  '   DB2MTEXT ';
    $strSQL  .=  '   WHERE LANCD = \'001\'';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
            $err =db2_stmt_errormsg();
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $err =db2_stmt_errormsg();
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $data = array('result' => 'FAIL_SEL');
            }else{
                $data = array('result' => true,'data' => umEx($data));
            }
        }
    }
    //e_log(print_r($data,true));
    return $data;
}
function fnInsDB2MTEXT($db2con,$LANID,$mtext,$templateData){
    $data = true;
    $strSQL  = 'DELETE  FROM DB2MTEXT WHERE LANCD = ?';
    $params = array($LANID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data ='FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log(db2_stmt_errormsg($stmt));
            $data = 'FAIL_DEL';
        }else{
            $strSQL  = ' INSERT INTO ';
            $strSQL .= ' DB2MTEXT (LANCD,ITMNM,ITMTEXT,ITMKBN)';
            $strSQL .= ' VALUES (?,?,?,?)';
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                e_log(db2_stmt_errormsg($stmt));
                $data ='FAIL_INS';
            }else{
                $tempChk = array();
                foreach ($mtext as $itmnm){
                    $key = cmMer($itmnm['ITMNM']);
                    if($templateData[$key] !== null ){
                        $params = array($LANID,$key,$templateData[$key],'1');
                        $r = db2_execute($stmt,$params);
                        if($r === false){
                            e_log(db2_stmt_errormsg($stmt));
                            $data = 'FAIL_INS';
                            break;
                        }else{
                            $data = true;
                        }
                    }else{
                        //e_log('keydata'.$templateData[$key]);
                        $tempChk[] = $key;
                    }
                }
                if(count($tempChk) === count($mtext)){
                    //e_log(print_r($tempChk,true).count($tempChk).print_r($mtext,true).count($mtext));
                    $data = '登録されたテンプレートファイルに登録対象項目キーのデータがはいていませんので再登録してください。';
                }
            }
        }
    }
    return $data;
}
function fnDelDB2MTEXT($db2con,$LANID){
    $data = true;
    $strSQL  = 'DELETE  FROM DB2MTEXT WHERE LANCD = ?';
    $params = array($LANID);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data ='FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log(db2_stmt_errormsg($stmt));
            $data = 'FAIL_DEL';
        }else{
            $files = glob(BASE_DIR."/php/langtemplate/".$LANCD.".*");
            foreach ($files as $file) {
                unlink($file);
            }
        }
    }
    return $data;
}