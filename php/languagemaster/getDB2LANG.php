<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$PROC    = (isset($_POST['PROC'])?$_POST['PROC']:'');
$LANID   = (isset($_POST['LANID'])?$_POST['LANID']:'');
$LANNM   = (isset($_POST['LANNM'])?$_POST['LANNM']: '');
$start   = $_POST['start'];
$length  = $_POST['length'];
$sort    = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$htmlFlg = true;
$rtn = 0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$LANID   = cmHscDe($LANID);
$LANNM   = cmHscDe($LANNM);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg('NOTEXIST',array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('言語の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'26',$userData[0]['WUSAUT']);//'1' => languageMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('言語の権限'));
            }
        }
    }
}

if($PROC == 'EDIT'){
    if($rtn === 0){
        $res = fnSelDB2LANG($db2con,$LANID);
        if($res['result'] === true){
            if(count($res['data']) === 0){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET',array('言語'));
            }else{
                $data = $res['data'];
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $res = fnGetAllCount($db2con,$LANID,$LANNM);
        if($res['result'] === true){
            $allcount = $res['data'];
            $res = fnGetDB2LANG($db2con,$LANID,$LANNM,$start,$length,$sort,$sortDir);
            if($res['result'] === true){
                $data = $res['data'];
            }else{
                $rtn = 1;
                $msg = showMsg($res['result']);
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'DATA' => umEx($data,$htmlFlg),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));


/*
*-------------------------------------------------------* 
* メールグループ取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $RDBNM  RDB名
* @param String  $RDBUSR RDBユーザー
* @param String  $start
* @param String  $length
* @param String  $sort
* @param String  $sortDir
* 
*-------------------------------------------------------*
*/

function fnGetDB2LANG($db2con,$LANID,$LANNM,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();
        $strSQL = ' SELECT A.* ';
        $strSQL .= ' FROM ( SELECT B.LANID,B.LANNM,B.LANFIL, ROWNUMBER() OVER( ';

        if($sort !== ''){
            $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= ' ORDER BY B.LANID ASC ';
        }

        $strSQL .= ' ) as rownum ';
        $strSQL .= ' FROM DB2LANG as B ';
        $strSQL .= ' WHERE LANID <> \'\' ';
        
        if($LANID != ''){
            $strSQL .= ' AND LANID like ? ';
            array_push($params,'%'.$LANID.'%');
        }

        if($LANNM != ''){
            $strSQL .= ' AND LANNM like ? ';
            array_push($params,'%'.$LANNM.'%');
        }

        $strSQL .= ' ) as A ';

        //抽出範囲指定
        if(($start != '')&&($length != '')){
            $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
            array_push($params,$start + 1);
            array_push($params,$start + $length);
        }
        e_log($strSQL.print_r($params,true));
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }

    return $data;
}
/*
*-------------------------------------------------------* 
* メールグループ全件カウント取得処理
* 
* RESULT
*    01：データ件数
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $RDBNM  RDB名
* @param String  $RDBUSR RDBユーザー
* @param String  $start
* @param String  $length
* @param String  $sort
* @param String  $sortDir
* 
*-------------------------------------------------------*
*/
function fnGetAllCount($db2con,$LANID,$LANNM){
    $data = array();

    $strSQL  = ' SELECT count(A.LANID) as COUNT ';
    $strSQL .= ' FROM DB2LANG as A ' ;
    $strSQL .= ' WHERE LANID <> \'\' ';

    $params = array();

    if($LANID != ''){
        $strSQL .= ' AND LANID like ? ';
        array_push($params,'%'.$LANID.'%');
    }
    if($LANNM != ''){
        $strSQL .= ' AND LANNM like ? ';
        array_push($params,'%'.$LANNM.'%');
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}
/*
*-------------------------------------------------------* 
* 対象メールグループデータ取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $RDBNM  RDB名
* 
*-------------------------------------------------------*
*/
function fnSelDB2LANG($db2con,$LANID){

    $data = array();

    $params = array($LANID);


    $strSQL  = ' SELECT A.LANID,A.LANNM,A.LANFIL ';
    $strSQL .= ' FROM DB2LANG as A ';
    $strSQL .= ' WHERE LANID = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    e_log($strSQL.print_r(params,true));
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    
    return $data;
}