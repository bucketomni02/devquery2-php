<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$success = true;
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$LANID = (isset($_POST['LANID']))?$_POST['LANID']:'';
$LANNM = (isset($_POST['LANNM']))?$_POST['LANNM']:'';
$LANFIL = (isset($_POST['LANFIL']))?$_POST['LANFIL']:'';
$PROC  = (isset($_POST['PROC']))?$_POST['PROC']:'';

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
          
            $rs = cmChkKenGen($db2con,'26',$userData[0]['WUSAUT']);//'1' => languageMaster
       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('言語の権限'));
            }
        }
    }
}
// 言語ID必須チェック
if($rtn === 0){
      if($LANID  === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('言語ID'));
        $focus = 'LANID';
    }
}
// 言語ID必須チェック
if($rtn === 0){
      if(cmMer($LANID)  === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_SPACECHK',array('言語ID'));
        $focus = 'LANID';
    }
}
// 言語ID入力桁数チェック
if($rtn === 0){
    if(!checkMaxLen($LANID,10)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('言語ID'));
        $focus = 'LANID';
    }
}
// 言語IDのバイトチェック
if($rtn === 0){
    $byte = checkByte($LANID);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('言語ID'));
        $focus = 'LANID';
    }
}
// 言語IDの正規表現
if($rtn === 0){
    if (mb_ereg('^[ｱ-ﾝ]+$', $LANID)) {
        $rtn = 1;
        $msg = showMsg('FAIL_BYTKANA',array('言語ID'));
        $focus = 'LANID';
    }
}
/*if($rtn === 0){
    if($PROC === 'ADD'){
        //追加対象言語存在チェック処理
        $rs = fnCheckDB2LANG($db2con,$LANID);
        if($rs !== true){
             $rtn = 1;
            $msg = showMsg($rs,array('言語ID'));
            $focus = 'LANID';
        }
    }else if($PROC === 'UPD'){
        //更新対象言語存在チェック処理
        $rs = fnCheckDB2LANG($db2con,$LANID);
        if( $rs === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_UPD',array('言語ID'));
            $focus = 'LANID';
        }
    }
}*/

// 言語名入力桁数チェック
if($rtn === 0){
    if(!checkMaxLen($LANNM,50)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('言語名'));
        $focus = 'LANNM';
    }
}
if($rtn === 0){
    if(cmMer($LANFIL) === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_SET',array('言語ファイル'));
        $focus = 'LANFIL';
    }
}
if($rtn === 0){
    if(strripos($LANFIL, '\\') !== false) {
        $LANFIL =substr($LANFIL,12);
    }
    $ext = explode ('.', $LANFIL);
    $ext = $ext [count ($ext) - 1];
    if($ext === 'xls' || $ext === 'xlsx'){
        $rtn = 0;
    }else{
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array('言語ファイル',array('EXCEL','ファイル')));
        $focus = 'LANFIL';
    }
    if(!checkMaxLen($LANFIL,50)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array(array('言語ファイル','テンプレート','名')));
    }
}

// テンプレートチェック
if($rtn === 0){
    
}
/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'LANFIL' => $LANFIL,
    'LANMN'  => $LANNM,
    'LANID'  => $LANID,
    'PROC'   => $PROC,
    'FOCUS' => $focus
);
//e_log('戻り値:'.print_r($rtnArray,true));
echo(json_encode($rtnArray));