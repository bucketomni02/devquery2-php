<?php
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
//ログファイルセット
$varlog = './readAllCsvErrlog.log';
$varlogfile  = 'readAllCsvErrlog_'.date("YmdHis").'.log';
$varlogDir = './'. $varlogfile;
if (file_exists($varlog)){
    if(copy($varlog,$varlogDir)){
         @unlink($varlog);
    }
}
ini_set('error_log', './readAllCsvErrlog.log');
$DIRE = BASE_DIR.'/php/languagemaster/readCsvData/*.csv';
$filename = '';
$delimiter = "\t";
//$delimiter = ",";
//初期値
$lanArr = array();
$frmArr= array();
$fieldArr= array();
$msgArr = array();
$frmrow = 0;
$fldrow = 0;
if($filename === ''){
    foreach(glob($DIRE) as $filename){
        echo ($filename.'<br>');
        if($filename !== ''){
            // ファイルポインタを開く
            if (($handle = fopen($filename, "r")) !== FALSE) {
                $lanSEQ = -1;
                $fieldInfo = array();
                $row = 0;
                while (($data = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
                    $num = count($data);
                    $row++;
                    $fieldData = array();
                    for ($c=0; $c < $num; $c++) {
                        $fieldData[] = $data[$c];
                        //echo($c.'=>'.$data[$c].'<br>');
                    }
                     $fieldInfo[$row] = $fieldData;
                    //echo(print_r($fieldData,true));
                }
                fclose($handle);
                //echo('dataarray'.'<br>'.print_r($fieldInfo,true));
                //error_log(print_r($fieldInfo,true));
                foreach($fieldInfo as $rowno => $lines){
                    if($lines[0] == 0){
                        $lanSEQ = $lanSEQ +1;
                        $lanArrdata= array();
                        $lanArrdata['ROW'] = $rowno;
                        $lanArrdata['LANID'] = $lines[1];
                        $lanArrdata['LANNM'] = $lines[2];
                        $lanArr[$lanSEQ] = $lanArrdata;
                    }
                    else if($lines[0] == 1){
                        $frmArrdata = array();
                        foreach($lines as $key => $val){
                            if($key == 1){
                                $frmArrdata['FRMID'] = $val;
                                $frmArrdata['FLDID'] = $val;
                            }else if($key > 1){
                                $frmArrdata[$lanArr[($key-2)]['LANID']] = $val;
                            }
                        }
                        $frmArrdata['ITMKBN'] = '1';
                        $frmArr[$frmrow] = $frmArrdata;
                        $frmrow ++;
                    }
                    else if($lines[0] == 2){
                        $fldArrdata = array();
                        foreach($lines as $key => $val){
                            if($key == 1){
                                $fldArrdata['FRMID'] = $val;
                            }else if($key == 2){
                                $fldArrdata['FLDID'] = $val;
                            }else if($key > 2){
                                $fldArrdata[$lanArr[($key-3)]['LANID']] = $val;
                            }
                        }
                        $fldArrdata['ITMKBN'] = '2';
                        $fldArr[$fldrow] = $fldArrdata;
                        $fldrow ++;
                    }
                    //メッセージテキスト
                    else if($lines[0] == 3){
                        $msgArrdata = array();
                        foreach($lines as $key => $val){
                            if($key == 1){
                                $msgArrdata['MSGCD'] = $val;
                            }else if($key == 2){
                                $msgArrdata['MSGFLG'] = $val;
                            }else if($key > 2){
                                $msgArrdata[$lanArr[($key-3)]['LANID']] = $val;
                            }
                        }
                        $msgArrdata['ITMKBN'] = '3';
                        $msgArr[$rowno] = $msgArrdata;
                    }
                    //固定テキスト
                    else if($lines[0] == 4){
                        $constArrdata = array();
                        foreach($lines as $key => $val){
                            if($key == 1){
                                $constArrdata['KCD'] = $val;
                            }else if($key == 2){
                                $constArrdata['KFLG'] = $val;
                            }else if($key > 2){
                                $constArrdata[$lanArr[($key-3)]['LANID']] = $val;
                            }
                        }
                        $constArrdata['ITMKBN'] = '4';
                        $constArr[$rowno] = $constArrdata;
                    }
                }
                /*e_log('実行クエリー名'.$filename.'\r\n');
                e_log('言語配列=>'.print_r(count($lanArr),true).'\r\n');
                e_log('画面配列=>'.print_r(count($frmArr),true).'\r\n');
                e_log('画面配列データ=>'.print_r(($frmArr),true).'\r\n');
                e_log('フィールド配列=>'.print_r(count($fldArr),true).'\r\n');
                e_log('メッセージ配列=>'.print_r(count($msgArr),true).'\r\n');
                e_log('固定データ配列=>'.print_r(count($constArr),true).'\r\n');*/
            }
        }
    }
}

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rtn = 0;
if(count($frmArr)> 0){
    $rs = fnInsDB2MITM($db2con,$frmArr);
    if($rs === false){
        $rtn =1;
        e_log($key.'行目のデータが間違っている。');
        echo($key.'行目のデータが間違っている。');
    }
    
}
if($rtn === 0){
    if (count($fldArr)> 0){
        $rs = fnInsDB2MITM($db2con,$fldArr);
        if($rs !== true){
            $rtn =1;
            e_log($rs.'行目のデータが間違っている。');
            echo($key.'行目のデータが間違っている。');
        }
    }
}
if($rtn === 0){
    if (count($msgArr)> 0){
        $rs = fnInsDB2MMSG($db2con,$msgArr);
        if($rs !== true){
            $rtn =1;
            e_log($rs.'行目のデータが間違っている。');
            echo($key.'行目のデータが間違っている。');
        }
    }
}
if($rtn === 0){
    if (count($constArr)> 0){
        $rs = fnInsDB2MCONST($db2con,$constArr);
        if($rs !== true){
            $rtn =1;
            e_log($rs.'行目のデータが間違っている。');
            echo($key.'行目のデータが間違っている。');
        }
    }
}
if($rtn === 0){
    $rs = insDB2MTEXT($db2con);
    if($rs !== true){
        $rtn =1;
        e_log('マスタテーブル作りの失敗:'.$rs);
    }
}

cmDb2Close($db2con);
// DB2MITMテーブル挿入、更新処理
// DB2MITMに挿入ためチェックしてある場合更新、ない場合挿入
// 画面と画面項目情報を保持している
function fnInsDB2MITM($db2con,$itminfo){
    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM DB2MITMT ';
    $strSQL .= ' WHERE ITMKBN = ?';
    $strSQL .= ' AND   FRMID  = ?';
    $strSQL .= ' AND   ITMID  = ?';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $result = true;
        foreach($itminfo as $key => $itmdata){
            $params  = array(
                $itmdata['ITMKBN'],
                $itmdata['FRMID'],
                $itmdata['FLDID']
            );
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log(db2_stmt_errormsg($stmt));
                $data = array('result' => 'FAIL_SEL');
                $result = 'FAILSEL';
                break;
            }else{
                $data = array();
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                if(count($data) > 0){
                    //update
                    $updRs = updDB2MITM($db2con,$itmdata);
                    if($updRs['result'] !== true){
                        $result = $key;
                        break;
                    }
                }else{
                    $insRs = insDB2MITM($db2con,$itmdata);
                    if($insRs['result'] !== true){
                        $result = $key;
                        e_log('キーデータ：'.$key);
                        e_log(print_r($itminfo,true));
                        break;
                    }
                }
            }
        }
    }
    return $result;        
}
//　メッセージデータ挿入
// DB2MMSGテーブル挿入、更新処理
// DB2MMSGに挿入ためチェックしてある場合更新、ない場合挿入
// メッセージ情報を保持している
function fnInsDB2MMSG($db2con,$msgInfo){
    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM DB2MMSGT ';
    $strSQL .= ' WHERE   MSGCD  = ?';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $result = true;
        foreach($msgInfo as $key => $itmdata){
            $params  = array(
                $itmdata['MSGCD']
            );
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log(db2_stmt_errormsg($stmt));
                $data = array('result' => 'FAIL_SEL');
                $result = 'FAILSEL';
                break;
            }else{
                $data = array();
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                if(count($data) > 0){
                    //update
                    $updRs = updDB2MMSG($db2con,$itmdata);
                    if($updRs['result'] !== true){
                        $result = $key;
                        e_log('キーデータ：'.$key);
                        e_log('msgdata:'.print_r($msgInfo,true));
                        break;
                    }
                }else{
                    $insRs = insDB2MMSG($db2con,$itmdata);
                    if($insRs['result'] !== true){
                        $result = $key;
                        break;
                    }
                }
            }
        }
    }
    return $result;        
}
//　固定データ挿入
// DB2MCONSTテーブル挿入、更新処理
// DB2MCONSTに挿入ためチェックしてある場合更新、ない場合挿入
// メッセージ情報を保持している
function fnInsDB2MCONST($db2con,$constInfo){
    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM DB2MCONSTT ';
    $strSQL .= ' WHERE   KCD  = ?';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $result = true;
        foreach($constInfo as $key => $constdata){
            $params  = array(
                $constdata['KCD']
            );
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log(db2_stmt_errormsg($stmt));
                $data = array('result' => 'FAIL_SEL');
                $result = 'FAILSEL';
                break;
            }else{
                $data = array();
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                if(count($data) > 0){
                    //update
                    $updRs = updDB2MCONST($db2con,$constdata);
                    if($updRs['result'] !== true){
                        $result = $key;
                        break;
                    }
                }else{
                    $insRs = insDB2MCONST($db2con,$constdata);
                    if($insRs['result'] !== true){
                        $result = $key;
                        break;
                    }
                }
            }
        }
    }
    return $result;        
}
// DB2MITM 更新処理
function updDB2MITM ($db2con , $itmdata){    
    $strSQL  = ' UPDATE ';
    $strSQL .= ' DB2MITMT ';
    $strSQL .= ' SET ITMTEXT = ?';
    $strSQL .= ' WHERE ITMKBN = ? ';
    $strSQL .= ' AND FRMID = ? ';
    $strSQL .= ' AND ITMID = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
     if($stmt === false){
        e_log('項目更新準備：'.db2_stmt_errormsg($stmt));
        $data = array('result' => 'FAIL_UPD');
    }else{
        $params = array(
                    $itmdata['001'],
                    $itmdata['ITMKBN'],
                    $itmdata['FRMID'],
                    $itmdata['FLDID']
                    );
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log('項目更新実行：'.db2_stmt_errormsg($stmt));
            $data = array('result' => 'FAIL_UPD');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// DB2MITM 挿入処理
function insDB2MITM ($db2con , $itmdata){

    $strSQL  = ' INSERT ';
    $strSQL .= ' INTO DB2MITMT (ITMKBN,FRMID,ITMID,ITMTEXT) ';
    $strSQL .= ' VALUES (?,?,?,?) ';
    $stmt = db2_prepare($db2con,$strSQL);
     if($stmt === false){
        e_log('項目挿入準備：'.db2_stmt_errormsg($stmt));
        $data = array('result' => 'FAIL_INS');
    }else{
        $params = array(
                    $itmdata['ITMKBN'],
                    $itmdata['FRMID'],
                    $itmdata['FLDID'],
                    $itmdata['001']
                    );
        
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log('項目挿入実行：'.db2_stmt_errormsg($stmt));
            e_log('項目パラメータ：'.print_r($itmdata,true));
            $data = array('result' => 'FAIL_INS');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
function updDB2MMSG ($db2con , $itmdata){    
    $strSQL  = ' UPDATE ';
    $strSQL .= ' DB2MMSG ';
    $strSQL .= ' SET MSGTEXTT = ?';
    $strSQL .= '   , MSGFLG = ?';
    $strSQL .= ' WHERE MSGCD = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
     if($stmt === false){
        e_log('メッセージ更新準備：'.db2_stmt_errormsg($stmt));
        $data = array('result' => 'FAIL_UPD');
    }else{
        $params = array(
                    $itmdata['001'],
                    $itmdata['MSGFLG'],
                    $itmdata['MSGCD']
                    );
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log('メッセージ更新実行：'.db2_stmt_errormsg($stmt));
            $data = array('result' => 'FAIL_UPD');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}

function insDB2MMSG ($db2con , $itmdata){
    $strSQL  = ' INSERT ';
    $strSQL .= ' INTO DB2MMSGT (MSGCD,MSGTEXT,MSGFLG) ';
    $strSQL .= ' VALUES (?,?,?) ';
    $stmt = db2_prepare($db2con,$strSQL);
     if($stmt === false){
        e_log('メッセージ挿入準備：'.db2_stmt_errormsg($stmt));
        $data = array('result' => 'FAIL_INS');
    }else{
        $params = array(
                    $itmdata['MSGCD'],
                    $itmdata['001'],
                    $itmdata['MSGFLG']
                    );
        
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log('メッセージ挿入実行：'.db2_stmt_errormsg($stmt));
            $data = array('result' => 'FAIL_INS');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
//DB2MCONST挿入処理
function insDB2MCONST ($db2con , $constdata){
    $strSQL  = ' INSERT ';
    $strSQL .= ' INTO DB2MCONSTT (KCD,KTEXT,KFLG) ';
    $strSQL .= ' VALUES (?,?,?) ';
    $stmt = db2_prepare($db2con,$strSQL);
     if($stmt === false){
        e_log('固定挿入準備：'.db2_stmt_errormsg($stmt));
        $data = array('result' => 'FAIL_INS');
    }else{
        $params = array(
                    $constdata['KCD'],
                    $constdata['001'],
                    $constdata['KFLG']
                    );
        
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log('固定挿入実行：'.db2_stmt_errormsg($stmt));
            $data = array('result' => 'FAIL_INS');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
//DB2MCONST更新処理
function updDB2MCONST ($db2con , $constdata){
    
    $strSQL  = ' UPDATE ';
    $strSQL .= ' DB2MCONSTT ';
    $strSQL .= ' SET KTEXT = ?';
    $strSQL .= '   , KFLG = ?';
    $strSQL .= ' WHERE KCD = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
     if($stmt === false){
        e_log('固定更新準備：'.db2_stmt_errormsg($stmt));
        $data = array('result' => 'FAIL_UPD');
    }else{
        $params = array(
                    $constdata['001'],
                    $constdata['KFLG'],
                    $constdata['KCD']
                    );
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log('固定更新実行：'.db2_stmt_errormsg($stmt));
            $data = array('result' => 'FAIL_UPD');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
//DB2MTEXT挿入処理
function insDB2MTEXT($db2con){
    $rtn = 0;
    $data = true;
    if($rtn === 0){
        // DB2MTEXTのディフォルト言語CD【001】全部削除。
        $strSQL  = 'DELETE  FROM DB2MTEXTT WHERE LANCD = \'001\'';
        $params = array();
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rtn = 1;
            $data ='FAIL_DEL【DB2MTEXT】';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rtn = 1;
                e_log(db2_stmt_errormsg($stmt));
                $data = 'FAIL_DEL【DB2MTEXT】';
            }
        }
    }
    if($rtn === 0){
        //アイテムデータ登録
        $strSQL  = ' INSERT INTO DB2MTEXTT ';
        $strSQL .= '     SELECT \'001\' AS LANCD ';
        //$strSQL .= '    ,(ROW_NUMBER() OVER(ORDER BY A.ITMTEXT)) AS ITMCD ';
        $strSQL .= '    ,A.ITMTEXT AS ITMNM ';
        $strSQL .= '    ,A.ITMTEXT ';
        $strSQL .= '    ,\'1\'';
        $strSQL .= '     FROM ( ';
        $strSQL .= '         SELECT ITMTEXT ';
        $strSQL .= '         FROM DB2MITMT ';
        $strSQL .= '         GROUP BY ITMTEXT ';
        $strSQL .= '     ) AS A ';
        $params = array();
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            e_log('項目テキストをDB2MTEXTに挿入準備'.db2_stmt_errormsg($stmt));
            $data ='FAIL_INS【DB2MITM】';
            $rtn = 1;
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log('項目テキストをDB2MTEXTに挿入実行'.db2_stmt_errormsg($stmt));
                $data = 'FAIL_INS【DB2MITM】';
                $rtn = 1;
            }
        }
    }
    if($rtn === 0){
        //MSG TEXT挿入
        $strSQL  = ' INSERT INTO DB2MTEXTT ';
        $strSQL .= '     SELECT \'001\' AS LANCD ';
        $strSQL .= '    ,A.MSGTEXT AS ITMNM ';
        $strSQL .= '    ,A.MSGTEXT ';
        $strSQL .= '    ,\'2\'';
        $strSQL .= '     FROM ( ';
        $strSQL .= '         SELECT MSGTEXT ';
        $strSQL .= '         FROM DB2MMSGT ';
        $strSQL .= '         WHERE MSGTEXT NOT IN (SELECT ITMNM FROM DB2MTEXT WHERE LANCD = \'001\')';
        $strSQL .= '         GROUP BY MSGTEXT ';
        $strSQL .= '     ) AS A ';
        $params = array();
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            e_log('メッセージテキストをDB2MTEXTに挿入準備'.db2_stmt_errormsg($stmt));
            $data = 'FAIL_INS【DB2MMSG】';
            $rtn = 1;
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log('メッセージテキストをDB2MTEXTに挿入実行'.db2_stmt_errormsg($stmt));
                $data = 'FAIL_INS【DB2MMSG】';
                $rtn = 1;
            }
        }
    }
    if($rtn === 0){
        // 固定データ挿入
        $strSQL  = ' INSERT INTO DB2MTEXTT ';
        $strSQL .= '     SELECT \'001\' AS LANCD ';
        $strSQL .= '    ,A.KTEXT AS ITMNM ';
        $strSQL .= '    ,A.KTEXT ';
        $strSQL .= '    ,\'3\'';
        $strSQL .= '     FROM ( ';
        $strSQL .= '         SELECT KTEXT ';
        $strSQL .= '         FROM DB2MCONST ';
        $strSQL .= '         WHERE KTEXT NOT IN (SELECT ITMNM FROM DB2MTEXT  WHERE LANCD = \'001\')';
        $strSQL .= '         GROUP BY KTEXT ';
        $strSQL .= '     ) AS A ';
        $params = array();
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            e_log('固定テキストをDB2MTEXTに挿入準備'.db2_stmt_errormsg($stmt));
            $data = 'FAIL_INS【DB2MCONST】';
            $rtn = 1;
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log('固定テキストをDB2MTEXTに挿入実行'.db2_stmt_errormsg($stmt));
                $data = 'FAIL_INS【DB2MCONST】';
                $rtn = 1;
            }
        }
    }
    if($rtn === 0){
        // 区分データ
        $strSQL  = ' INSERT INTO DB2MTEXTT ';
        $strSQL .= '     SELECT \'001\' AS LANCD ';
        $strSQL .= '        ,A.KBNNM AS ITMNM ';
        $strSQL .= '        ,A.KBNNM ';
        $strSQL .= '        ,\'4\'';
        $strSQL .= '     FROM ( ';
        $strSQL .= '         SELECT KBNNM ';
        $strSQL .= '         FROM DB2MKBN ';
        $strSQL .= '         WHERE ';
        $strSQL .= '            KBNNM <> \'\' AND ';
        $strSQL .= '            KBNNM NOT IN (SELECT ITMNM FROM DB2MTEXT  WHERE LANCD = \'001\') AND ';
        $strSQL .= '            KBNKB NOT LIKE \'%X%\' AND ';
        $strSQL .= '            KBNKB <> \'000\' ';
        $strSQL .= '         GROUP BY KBNNM ';
        $strSQL .= '     ) AS A ';
        $params = array();
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            e_log('区分テキストをDB2MTEXTに挿入準備'.db2_stmt_errormsg($stmt));
            $data = 'FAIL_INS【DB2MKBN】';
            $rtn = 1;
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log('区分テキストをDB2MTEXTに挿入実行'.db2_stmt_errormsg($stmt));
                $data = 'FAIL_INS【DB2MKBN】';
                $rtn = 1;
            }
        }
    }
    return $data;
}


