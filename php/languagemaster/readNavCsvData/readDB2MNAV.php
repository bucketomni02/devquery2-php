<?php
include_once("../../common/inc/config.php");
include_once("../../common/inc/common.inc.php");
//ログファイル管理
//変数初期化
$varlog = './readDB2MNAV.log';
$varlogfile  = 'readDB2MNAV_'.date("YmdHis").'.log';
$varlogDir = './'. $varlogfile;
if (file_exists($varlog)){
    if(copy($varlog,$varlogDir)){
         @unlink($varlog);
    }
}
ini_set('error_log', $varlog);
//実行先のCSVファイル名
$filename = 'DB2MNAV.csv';
//実行先のCSVのデリミタ
$delimiter = "\t";
//$delimiter = ",";
// ファイルポインタを開く
if (($handle = fopen($filename, "r")) !== FALSE) {
    $fieldInfo = array();
    $row = 0;
    while (($data = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
        $num = count($data);
        $row++;
        $fieldData = array();
        for ($c=0; $c < $num; $c++) {
            $fieldData[] = $data[$c];
        }
         $fieldInfo[$row] = $fieldData;
    }
    fclose($handle);
    $navArr = array();
    error_log('ファイルデータ:'.print_r($fieldInfo,true));
    foreach($fieldInfo as $rowno => $lines){
        $navArrData = array();
        foreach($lines as $key => $val){
            switch($key){
                case 0:
                    $navArrData['NAVID'] = $val;
                    break;
                case 1:
                    $navArrData['NAVTEXT'] = $val;
                    break;
                case 2:
                    $navArrData['NAVLEAF'] = $val;
                    break;
                case 3:
                    $navArrData['NAVHREF'] = $val;
                    break;
                case 4:
                    $navArrData['NAVMENU'] = $val;
                    break;
                case 5:
                    $navArrData['NAVORD'] = $val;
                    break;
                default:
                    break;
            }
        }
        $navArr[$rowno] = $navArrData;
    }
    e_log('ナビケション配列=>'.print_r($navArr,true).'\r\n');
    echo('実行ファイル：'.$filename);
}
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rtn = 0;
//画面情報挿入
if(count($navArr)> 0){
    //e_log('ナビケション挿入=>'.print_r($navArr,true).'\r\n');
    $rs = fnInsMNAV($db2con,$navArr);
    if($rs === false){
        $rtn =1;
        e_log('画面データ読み込みエラー：'.$key.'行目のデータが間違っている。');
        echo('画面データ読み込みエラー：'.$key.'行目のデータが間違っている。');
    }
}
if($rtn === 1){
    echo('ファイル読み込みエラーが発生しました。ログに確認してください。');
}
cmDb2Close($db2con);

// DB2MITMテーブル挿入、更新処理
// DB2MITMに挿入ためチェックしてある場合更新、ない場合挿入
// 画面と画面項目情報を保持している
function fnInsMNAV($db2con,$itminfo){
    //e_log('ナビケション挿入=>'.print_r($itminfo,true).'\r\n');
    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM DB2MNAV ';
    $strSQL .= ' WHERE NAVID = ?';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $result = true;
        foreach($itminfo as $key => $itmdata){
            $params  = array(
                $itmdata['NAVID']
            );
            //e_log('fnInsMNAV:'.$strSQL.print_r($params,true));
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log(db2_stmt_errormsg($stmt));
                $data = array('result' => 'FAIL_SEL');
                $result = 'FAILSEL';
                break;
            }else{
                $data = array();
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                e_log('NAVDATA:'.print_r($data,true));
                if(count($data) > 0){
                    //update
                    $updRs = updDB2MNAV($db2con,$itmdata);
                    if($updRs['result'] !== true){
                        $result = $key;
                        break;
                    }
                }else{
                    $insRs = insDB2MNAV($db2con,$itmdata);
                    if($insRs['result'] !== true){
                        $result = $key;
                        break;
                    }
                }
            }
        }
    }
    return $result;
}

// DB2MNAV 更新処理
function updDB2MNAV ($db2con , $itmdata){
    $strSQL  = ' UPDATE ';
    $strSQL .= '    DB2MNAV ';
    $strSQL .= ' SET  NAVTEXT = ? ';
    $strSQL .= '    , NAVLEAF = ? ';
    $strSQL .= '    , NAVHREF = ? ';
    $strSQL .= '    , NAVMENU = ? ';
    $strSQL .= '    , NAVORD = ? ';
    $strSQL .= ' WHERE NAVID = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
     if($stmt === false){
        e_log('項目更新準備：'.db2_stmt_errormsg($stmt));
        $data = array('result' => 'FAIL_UPD');
    }else{
        $params = array(
                    $itmdata['NAVTEXT'],
                    $itmdata['NAVLEAF'],
                    $itmdata['NAVHREF'],
                    $itmdata['NAVMENU'],
                    intval($itmdata['NAVORD']),
                    $itmdata['NAVID']
                    );
        //e_log('UPD SQL:'.$strSQL.print_r($itmdata,true));
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log('項目更新実行：'.db2_stmt_errormsg($stmt));
            $data = array('result' => 'FAIL_UPD');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
// DB2MITM 挿入処理
function insDB2MNAV ($db2con , $itmdata){

    $strSQL  = ' INSERT ';
    $strSQL .= ' INTO DB2MNAV (NAVID,NAVTEXT,NAVLEAF,NAVHREF,NAVMENU,NAVORD) ';
    $strSQL .= ' VALUES (?,?,?,?,?,?) ';
    $stmt = db2_prepare($db2con,$strSQL);
     if($stmt === false){
        e_log('項目挿入準備：'.db2_stmt_errormsg($stmt));
        $data = array('result' => 'FAIL_INS');
    }else{
        $params = array(
                    $itmdata['NAVID'],
                    $itmdata['NAVTEXT'],
                    $itmdata['NAVLEAF'],
                    $itmdata['NAVHREF'],
                    $itmdata['NAVMENU'],
                    intval($itmdata['NAVORD'])
                    );
        //e_log('INS SQL:'.$strSQL.print_r($itmdata,true));
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log('項目挿入実行：'.db2_stmt_errormsg($stmt));
            $data = array('result' => 'FAIL_INS');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
