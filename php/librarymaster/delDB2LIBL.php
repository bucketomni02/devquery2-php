<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : ライブラリー
* PROGRAM NAME   : ライブラリー削除
* PROGRAM ID     : delDB2LIBL.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2017/05/08
* MODIFY DATE    : 
* ============================================================
**/

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$LIBLNM = cmHscDe($_POST['LIBLNM']);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('ライブラリー除外の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'31',$userData[0]['WUSAUT']);//'31' => LibraryJyogaiMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライブラリー除外の権限'));
            }
        }
    }
}
//ライブラリーID存在チェック
if($rtn === 0){
    $rs = fnCheckDB2LIBL($db2con,$LIBLNM);
    if( $rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('ライブラリーID'));
    }
}

//ライブラリーの削除
if($rtn === 0){
    $rs = fnDeleteDB2LIBL($db2con,$LIBLNM);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

if($rtn === 0){
    $rs = fnDeleteDB2LBLS($db2con,$LIBLNM);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

if($rtn === 0){
        $rs = fnDeleteDB2WUSRDel($db2con,$LIBLNM);//ユーザー権限ブラク更新
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* 削除対象ライブラリーが存在かどうかをチェックすること
* 
* RESULT
*    01：データがある場合 true
*    02：データがない場合 false
* 
* @param Object  $db2con DBコネクション
* @param String  $LIBLNM ライブラリーID
*-------------------------------------------------------*
*/



/*
*-------------------------------------------------------* 
* 削除対象ライブラリーを削除すること
* 
* RESULT
*    01：削除終了の場合 true
*    02：エラーの場合 false
* 
* @param Object  $db2con DBコネクション
* @param String  $LIBLNM ライブラリーID
*-------------------------------------------------------*
*/

function fnDeleteDB2LIBL($db2con,$LIBLNM){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2LIBL ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' LIBLNM = ? ' ;
   
  
    $params = array(
        $LIBLNM
       
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}

function fnDeleteDB2LBLS($db2con,$LIBSNM){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2LBLS ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' LIBSNM = ? ' ;
   
  
    $params = array(
        $LIBSNM
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}

function fnCheckDB2LIBL($db2con,$LIBLNM){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.LIBLNM ';
    $strSQL .= ' FROM DB2LIBL AS A ' ;
    $strSQL .= ' WHERE A.LIBLNM = ? ' ;

    $params = array(
        $LIBLNM
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_DEL';
            }
        }
    }
    return $rs;
}
//パワーユーザー権限にブラク更新
function fnDeleteDB2WUSRDel($db2con,$LIBLNM){
    $rs = true;

    //構文
    $strSQL  = " UPDATE DB2WUSR SET WULIBL = ?";
    $strSQL .= " WHERE ";
    $strSQL .= " WULIBL = ? " ;
    $params = array('',$LIBLNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}