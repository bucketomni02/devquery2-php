<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("./getAllLibraryCommon.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['PROC']))?cmMer($_POST['PROC']):'';
$LIBLNM = (isset($_POST['LIBLNM']))?cmMer($_POST['LIBLNM']):'';
$LIBLTE = (isset($_POST['LIBLTE']))?cmMer($_POST['LIBLTE']):'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/
/*
$LIBLNM = cmHscDe($LIBLNM);
$LIBLTE = cmHscDe($LIBLTE);*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('ライブラリー除外の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'31',$userData[0]['WUSAUT']);//'31' => LibraryJyogaiMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライブラリー除外の権限'));
            }
        }
    }
}

//ライブラリーID必須チェック
if($rtn === 0){
    if($LIBLNM  === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('ライブラリーID'));
        $focus = 'LIBLNM';
         
    }
}
if($rtn === 0){
        if (preg_match('/[^0-9A-Za-z_#@]+/', $LIBLNM)) {
                $msg = showMsg('FAIL_KEYENTRY',array('ライブラリーID'));
                $rtn = 1;
        }
}

/*if($rtn === 0){
    $byte = checkByte($LIBLNM);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('ライブラリーID'));
        $focus = 'LIBLNM';
        
    }
}*/
//ライブラリーIDの桁数チェック
if($rtn === 0){
    if(!checkMaxLen($LIBLNM,10)){
        $rtn = 1;
        
       $msg = showMsg('FAIL_MAXLEN',array('ライブラリーID'));
        $focus = 'LIBLNM';
    }
}
 /*if($rtn === 0){
        if(cmMer($LIBLNM) !== ''){
            if (cmMer($LIBLNM) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('ライブラリーID'));
            }
        }
    }*/
//ライブラリー名必須チェック
if($rtn === 0){
    if($LIBLTE  === ''){
        $rtn = 1;
         $msg = showMsg('FAIL_REQ',array('ライブラリー名'));
        
        $focus = 'LIBLTE';
    }
}
//ライブラリー名の桁数チェック
if($rtn === 0){
    if(!checkMaxLen($LIBLTE,50)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('ライブラリー名'));
        $focus = 'LIBLTE';
        
    }
}
/* if($rtn === 0){
        if(cmMer($LIBLTE) !== ''){
            if (cmMer($LIBLTE) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('ライブラリー名'));
            }
        }
    }*/

/*
*-------------------------------------------------------* 
* ライブラリー更新処理
*-------------------------------------------------------*
*/

if($proc === 'ADD'){
   if($rtn === 0){
        
        $rs = fnCheckDB2LIBL($db2con,$LIBLNM);
        
        if($rs !== true){
            $rtn = 1;
            //$rs = 'ISEXIST';
            $msg = showMsg($rs,array('ライブラリーID'));
            $focus = 'LIBLNM';
        }
    }
    if($rtn === 0){
        //新ライブラリーを登録処理
        $rs = fnInsertDB2LIBL($db2con,$LIBLNM,$LIBLTE);
       
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}else if($proc === 'UPD'){
    if($rtn === 0){
        //ライブラリー存在チェック処理
        $rs = fnCheckDB2LIBL($db2con,$LIBLNM);
        if($rs === true){
            $rtn = 1;
            $rs = 'NOTEXIST_GET';
            $msg = showMsg($rs,array('ライブラリーID'));
            $focus = 'LIBLNM';
        }else if($rs !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs,array('ライブラリーID'));
            $focus = 'LIBLNM';
        }
    }
    if($rtn === 0){
        //ライブラリーを更新処理
        $rs = fnUpdateDB2LIBL($db2con,$LIBLNM,$LIBLTE);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}

cmDb2Close($db2con);
/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
     'proc' => $proc,
);

echo(json_encode($rtnAry));

/**
*-------------------------------------------------------* 
* ライブラリーが更新すること
* 
* RESULT
*    01：更新終了場合     true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $LIBLNM ライブラリーID
* @param String  $LIBLTE ライブラリー名
* 
*-------------------------------------------------------*
*/
function fnUpdateDB2LIBL($db2con,$LIBLNM,$LIBLTE){

    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2LIBL ';
    $strSQL .= ' SET ';
    $strSQL .= ' LIBLTE = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' LIBLNM = ? ';

    $params = array($LIBLTE,$LIBLNM);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }

    return $rs;
}
/**
*-------------------------------------------------------* 
* 新ライブラリーを登録すること
* 
* RESULT
*    01：登録終了の場合     true
*    02：エラー場合         false
* 
* @param Object  $db2con DBコネクション
* @param String  $LIBLNM ライブラリーID
* @param String  $LIBLTE ライブラリー名
* 
*-------------------------------------------------------*
*/
function fnInsertDB2LIBL($db2con,$LIBLNM,$LIBLTE){

    $rs = true;
    //構文
    $strSQL  = ' INSERT INTO DB2LIBL ';
    $strSQL .= ' ( ';
    $strSQL .= ' LIBLNM, ';
    $strSQL .= ' LIBLTE ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?) ';

    $params = array($LIBLNM,$LIBLTE);
   
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* ライブラリーが存在するかどうかをチェックすること
* 
* RESULT
*    01：存在する場合   true
*    02：存在ない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $LIBLNM ライブラリーID
* 
*-------------------------------------------------------*
*/
function fnCheckDB2LIBL($db2con,$LIBLNM){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.LIBLNM ' ;
    $strSQL .= ' FROM DB2LIBL AS A ' ;
    $strSQL .= ' WHERE A.LIBLNM = ? ' ;

    $params = array($LIBLNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
          while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}
/*
function  fnInsertDB2LBLS($db2con,$LIBLNM,$libraryData){
        $rs = true;
        //構文
        $strSQL  = ' INSERT INTO DB2LBLS ';
        $strSQL .= ' ( ';
                $strSQL .= ' LIBSNM, ';
                $strSQL .= ' LIBSID, ';
                $strSQL .= ' LIBSFG ';
        $strSQL .= ' ) ';
        $strSQL .= ' VALUES ';
        $strSQL .= ' (?,?,?) ';
        foreach($libraryData as $key => $value){
                $params = array($LIBLNM,$value['SCHEMA_NAME'],'0');
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                        $rs = 'FAIL_INS';
                         break;
                }else{
                        $r = db2_execute($stmt,$params);
                        if($r === false){
                            $rs = 'FAIL_INS';
                             break;
                        }
                }
        }
        return $rs;
}*/