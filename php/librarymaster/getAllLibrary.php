<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("./getAllLibraryCommon.php");
include_once("../base/chkRDB.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$LIBNAME = (isset($_POST['LIBNAME']))?cmMer($_POST['LIBNAME']):'';
$LIBSNM = (isset($_POST['LIBSNM']))?cmMer($_POST['LIBSNM']):'';
$start =(isset($_POST['start']))?cmMer($_POST['start']):'';
$length = (isset($_POST['length']))?cmMer($_POST['length']):'';
$sort = (isset($_POST['sort']))?cmMer($_POST['sort']):'';
$sortDir = (isset($_POST['sortDir']))?cmMer($_POST['sortDir']):'';
$LIBLTE = '';
$RDBNM  = (isset($_POST['RDBNM']))?cmMer($_POST['RDBNM']):'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('ライブラリー除外の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'31',$userData[0]['WUSAUT']);//'1' => UsercontrolMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライブラリー除外の権限'));
            }
        }
    }
}
if($rtn === 0){
      $rs = cmSelDB2LIBL($db2con, $LIBSNM);
      if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result'],array('ライブラリー'));
      }else{
                $LIBLTE = $rs['data'][0]['LIBLTE'];
      }
}
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
if($rtn === 0){
    if($RDBNM !== ''){
        $rs = chkRDB($RDBNM);
        if($rs['RTN'] !== 0){
            $rtn = 1;
            $msg = $rs['MSG'];
        }
    }
} 
//$RDBNM   = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
if($rtn === 0){
    if($RDBNM === '' || $RDBNM === RDBNAME || $RDBNM === RDB){
        $db2conRDB = $db2con;
    }else{
        $db2conRDB = cmDB2ConRDB();
    }
}
if($rtn === 0){
    if(($RDBNM === '' || $RDBNM === RDBNAME || $RDBNM === RDB) === false){
        $resCreate = createQTEMPDB2LBLS($db2conRDB,$LIBSNM);
        if($resCreate !== true){
            $rtn = 1;
            $msg = showMsg($resCreate);
        }
    }
}
if($rtn === 0){
        $rs = fnGetAllCount($db2conRDB,$LIBNAME);
        if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
        }else{
                $allcount = $rs['data'];
        }
}
if($rtn === 0){
        $rs = fnGetLibData($db2conRDB,$LIBNAME,$LIBSNM,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
        }else{
                $data = $rs['data'];
        }
}

cmDb2Close($db2conRDB);
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData'        => umEx($data,true),
    'RTN'           => $rtn,
    'MSG'           => $msg,
    'LIBLTE' =>$LIBLTE,
    'filterLibList' => $filterLibList,
	'LIBSNM' => $LIBSNM
);

echo(json_encode($rtn));