<?php

/*
*-------------------------------------------------------* 
* ラブラリーマスター取得
*-------------------------------------------------------*
*/

function fnGetLibData($db2con,$LibName = '',$LIBSNM,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();
    if(CRTQTEMPTBL_FLG === 1){
        createTmpFil($db2con,SYSSCHEMASLIB,'SYSSCHEMAS','','SYSSCHEMAS');
    }

    $strSQL = ' SELECT A.SCHEMA_NAME ,A.ROWNUM,CASE WHEN C.LIBSFG IS NULL THEN 0 ELSE C.LIBSFG END AS LIBKEN';
    $strSQL .= ' FROM ( SELECT B.SCHEMA_NAME';
    $strSQL .= ' , B.SCHEMA_TEXT';
    $strSQL .= ' , ROWNUMBER() OVER( ';

    if($sort != ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.SCHEMA_NAME ASC ';
    }

    $strSQL .= ' ) as ROWNUM ';
    $strSQL .= ' FROM  ';
    $strSQL .= '    (SELECT SYSTEM_SCHEMA_NAME AS SCHEMA_NAME ,SCHEMA_TEXT  FROM ';
    if(CRTQTEMPTBL_FLG === 1){
        $strSQL .= '        QTEMP/SYSSCHEMAS ';
    }else{
        $strSQL .= '        '.SYSSCHEMASLIB.'/SYSSCHEMAS ';
    }
    $strSQL .= '  WHERE SYSTEM_SCHEMA_NAME <> \'QTEMP\' ';
    $strSQL .= '     ) B';
    $strSQL .= ' WHERE B.SCHEMA_NAME <> \'\'';
    $strSQL .= ' ) as A ';
    $strSQL .= ' LEFT JOIN  ';
    $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
    if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
        $strSQL .= ' (SELECT * FROM '.MAINLIB.'/DB2LBLS WHERE LIBSFG = 1 AND LIBSNM= ?) ';
        array_push($params,$LIBSNM);
    }else{
        $strSQL .= 'QTEMP/DB2LBLS ';
    }
    $strSQL .= ' AS C ON A.SCHEMA_NAME = C.LIBSID ';
    $strSQL .= ' WHERE A.ROWNUM <> 0';
    if($LibName != ''){
        $strSQL .= ' AND A.SCHEMA_NAME IN ( ? )';
        array_push($params,$LibName);
    }

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' AND A.ROWNUM BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    e_log($strSQL);
    e_log($_SESSION['PHPQUERY']['RDBNM']." LIBSNM:".$LIBSNM."aye".print_R($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log('ライブラリー除外データ取得：'.$strSQL.db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$LibName = ''){
    $data = array();
    $params = array();
    $strSQL  = ' SELECT count(B.SCHEMA_NAME) as COUNT from ( ';
    $strSQL .= ' SELECT ';
    $strSQL .= ' A.SYSTEM_SCHEMA_NAME AS SCHEMA_NAME ';
    $strSQL .= ' , A.SCHEMA_TEXT ';
    $strSQL .= ' FROM '  ;
    $strSQL .= '    (SELECT * FROM ';
    $strSQL .= '        '.SYSSCHEMASLIB.'/SYSSCHEMAS ';
    $strSQL .= '  WHERE SYSTEM_SCHEMA_NAME <> \'QTEMP\' ';
    $strSQL .= ' ) A';
    $strSQL .= ' WHERE A.SYSTEM_SCHEMA_NAME <> \'\'';

    if($LibName != ''){
        $strSQL .= ' AND A.SYSTEM_SCHEMA_NAME IN ( ? )';
        array_push($params,$LibName);
    }
    $strSQL .= ' ) as B ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => false);
        e_log('ライブラリー除外データ取得：'.$strSQL.db2_stmt_errormsg());
    }else{

        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => false);
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}