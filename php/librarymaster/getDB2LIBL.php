<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$LIBLNM = $_POST['LIBLNM'];
$LIBLTE = $_POST['LIBLTE'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

$session = array();
$session['LIBLNM']  = $LIBLNM;
$session['LIBLTE']  = $LIBLTE;
$session['start']  = $start;
$session['length']   = $length;
$session['sort']  = $sort;
$session['sortDir']    = $sortDir;


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmlspecialcharsをデコード
$LIBLNM = cmHscDe($LIBLNM);
$LIBLTE = cmHscDe($LIBLTE);
$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID']))?$_SESSION['PHPQUERY']['user'][0]['WUUID']:'';
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$WUUID);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('ライブラリー除外の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'31',$userData[0]['WUSAUT']);//'31' => LibraryJyogaiMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライブラリー除外の権限'));
            }
        }
    }
}

if($proc === 'EDIT'){
    if($rtn === 0){
        $rs = cmSelDB2LIBL($db2con,$LIBLNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ライブラリーID'));
        }else{
            $data = $rs['data'];
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $rs = fnGetAllCount($db2con,$LIBLNM,$LIBLTE);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetDB2LIBL($db2con,$LIBLNM,$LIBLTE,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}

cmDb2Close($db2con);


/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg),
    '_SESSION'=>$_SESSION,
    'WUUID' => $WUUID,
    'session' => $session
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2LIBL($db2con,$LIBLNM,$LIBLTE,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();
        $strSQL  = ' SELECT A.* ';
        $strSQL .= ' FROM ( SELECT B.LIBLNM,B.LIBLTE , ROWNUMBER() OVER( ';

        if($sort !== ''){
            $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= ' ORDER BY B.LIBLNM ASC ';
        }

        $strSQL .= ' ) as rownum ';
//        $strSQL .= ' FROM DB2LIBL as B ';
        $strSQL .= ' FROM DB2LIBL as B ';

        $strSQL .= ' WHERE LIBLNM <> \'\' ';
        if($LIBLNM != ''){
            $strSQL .= ' AND LIBLNM like ? ';
            array_push($params,'%'.$LIBLNM.'%');
        }

        if($LIBLTE != ''){
            $strSQL .= ' AND LIBLTE like ? ';
            array_push($params,'%'.$LIBLTE.'%');
        }
        
        $strSQL .= ' ) as A ';
        
        //抽出範囲指定
        if(($start != '')&&($length != '')){
            $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
            array_push($params,$start + 1);
            array_push($params,$start + $length);
        }
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$LIBLNM = '',$LIBLTE = ''){
    $data = array();

    $strSQL  = ' SELECT count(A.LIBLNM) as COUNT ';
    $strSQL .= ' FROM  DB2LIBL as A ' ;
    $strSQL .= ' WHERE LIBLNM <> \'\' ';

    $params = array();

    if($LIBLNM != ''){
        $strSQL .= ' AND LIBLNM like ? ';
        array_push($params,'%'.$LIBLNM.'%');
    }

    if($LIBLTE != ''){
        $strSQL .= ' AND LIBLTE like ? ';
        array_push($params,'%'.$LIBLTE.'%');
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;

}
