<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("./getAllLibraryCommon.php");
include_once("../base/chkRDB.php");


/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$allcheck     = $_POST['ALLCHECK'];
$LIBSNM = (isset($_POST['LIBSNM']))?cmMer($_POST['LIBSNM']):'';
$LIBSDB = (isset($_POST['LIBSDB']))?cmMer($_POST['LIBSDB']):'';
$DATA = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/
/*
$LIBLNM = cmHscDe($LIBLNM);
$LIBLTE = cmHscDe($LIBLTE);*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'31',$userData[0]['WUSAUT']);//'31' => LibraryJyogaiMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライブラリー除外の権限'));
            }
        }
    }
}
    if($rtn === 0){
        //ライブラリー処理
        $rs = fnCheckDB2LIBL($db2con,$LIBSNM);
        if($rs === true){
            $rtn = 1;
            $rs = 'NOTEXIST_GET';
            $msg = showMsg($rs,array('ライブラリーID'));
        }else if($rs !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs,array('ライブラリーID'));
        }
    }

 
  if($rtn === 0){
        $rs = fnGetLibData($db2con,$LIBNAME,$LIBSNM,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
        }else{
                $Libdata = $rs['data'];
        }
   }   



if($allcheck === '2'){
   
  if(count($DATA) === 0){
    if($rtn === 0){
        foreach($Libdata as $key => $value){
          
            $rs = fnCheckDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB);
            if($rs === false){
                    $rtn = 1;
                    $msg = showMsg('FAIL_SEL',array('ライブラリーID'));
                    break;
            }else if($rs === 'EXIST'){
                $rs = fnUpdDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB,'1');
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }else{
                $rs = fnInsertDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB,'1');
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }
        }
    }
 }else{

   if($rtn === 0){
        foreach($Libdata as $key => $value){
          
            $rs = fnCheckDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB);
            if($rs === false){
                    $rtn = 1;
                    $msg = showMsg('FAIL_SEL',array('ライブラリーID'));
                    break;
            }else if($rs === 'EXIST'){
                $rs = fnUpdDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB,'1');
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }else{
                $rs = fnInsertDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB,'1');
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }
        }
   }
   if($rtn === 0){
        //ライブラリー削除
       foreach($DATA as $key => $value){
         
            $rs = fnCheckDB2LBLS($db2con,$LIBSNM,$value['LIBSID'],$value['LIBSDB']);
            if($rs === false){
                    $rtn = 1;
                    $msg = showMsg('FAIL_SEL',array('ライブラリーID'));
                    break;
            }else if($rs === 'EXIST'){
                $rs = fnUpdDB2LBLS($db2con,$LIBSNM,$value['LIBSID'],$value['LIBSDB'],$value['LIBSFG']);
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }else{
                $rs = fnInsertDB2LBLS($db2con,$LIBSNM,$value['LIBSID'],$value['LIBSDB'],$value['LIBSFG']);
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
             }
	      } 
       }
    }
}else if($allcheck === "3"){
    if(count($DATA) === 0){
       if($rtn === 0){
        //ライブラリー削除
       foreach($Libdata as $key => $value){
        
            $rs = fnCheckDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB);
            if($rs === false){
                    $rtn = 1;
                    $msg = showMsg('FAIL_SEL',array('ライブラリーID'));
                    break;
            }else if($rs === 'EXIST'){
                $rs = fnUpdDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB,'0');
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }else{
                $rs = fnInsertDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB,'0');
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }
        }
     } 
   }else{
      if($rtn === 0){
        //ライブラリー削除
       foreach($Libdata as $key => $value){
          
            $rs = fnCheckDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB);
            if($rs === false){
                    $rtn = 1;
                    $msg = showMsg('FAIL_SEL',array('ライブラリーID'));
                    break;
            }else if($rs === 'EXIST'){
                $rs = fnUpdDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB,'0');
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }else{
                $rs = fnInsertDB2LBLS($db2con,$LIBSNM,$value['SCHEMA_NAME'],$LIBSDB,'0');
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }
	  }
 }     


      if($rtn === 0){
        foreach($DATA as $key => $value){
          
            $rs = fnCheckDB2LBLS($db2con,$LIBSNM,$value['LIBSID'],$value['LIBSDB']);
            if($rs === false){
                    $rtn = 1;
                    $msg = showMsg('FAIL_SEL',array('ライブラリーID'));
                    break;
            }else if($rs === 'EXIST'){
                $rs = fnUpdDB2LBLS($db2con,$LIBSNM,$value['LIBSID'],$value['LIBSDB'],$value['LIBSFG']);
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }else{
                $rs = fnInsertDB2LBLS($db2con,$LIBSNM,$value['LIBSID'],$value['LIBSDB'],$value['LIBSFG']);
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                 }
              }
          }
       } 
    }  
 }else{
   
    if($rtn === 0){
        foreach($DATA as $key => $value){
            
            $rs = fnCheckDB2LBLS($db2con,$LIBSNM,$value['LIBSID'],$value['LIBSDB']);
            if($rs === false){
                    $rtn = 1;
                    $msg = showMsg('FAIL_SEL',array('ライブラリーID'));
                    break;
            }else if($rs === 'EXIST'){
                
                $rs = fnUpdDB2LBLS($db2con,$LIBSNM,$value['LIBSID'],$value['LIBSDB'],$value['LIBSFG']);
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }else{
               
                $rs = fnInsertDB2LBLS($db2con,$LIBSNM,$value['LIBSID'],$value['LIBSDB'],$value['LIBSFG']);
                if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs,array('ライブラリーID'));
                        break;
                }
            }
        }
     }
  }

   if($rtn === 0){
        //ライブラリー削除
        $rs = fnDelDB2LBLS($db2con,$LIBSNM);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('ライブラリーID'));
        }
    } 

cmDb2Close($db2con);
/**return**/
$rtnAry = array(
        'iTotalRecords' => $allcount,
        'RTN' => $rtn,
        'MSG' => $msg,
        'LIBSNM' => $LIBSNM,
        'DATA' => $DATA,
        'aaData' => umEx($Libdata,true),
        'ALLCHECK'=> $allcheck
        
);

echo(json_encode($rtnAry));


/**
*-------------------------------------------------------* 
* ライブラリーが存在するかどうかをチェックすること
* 
* RESULT
*    01：存在する場合   true
*    02：存在ない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $LIBLNM ライブラリーID
* 
*-------------------------------------------------------*
*/
function fnCheckDB2LIBL($db2con,$LIBLNM){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.LIBLNM ' ;
    $strSQL .= ' FROM DB2LIBL AS A ' ;
    $strSQL .= ' WHERE A.LIBLNM = ? ' ;
    $params = array($LIBLNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
          while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}
function fnCheckDB2LBLS($db2con,$LIBSNM,$LIBSID,$LIBSDB){

    $data = array();
    $rs = true;
    $strSQL  = ' SELECT * ' ;
    $strSQL .= ' FROM DB2LBLS AS A ' ;
    $strSQL .= ' WHERE A.LIBSNM = ? ' ;
    $strSQL .= ' AND A.LIBSID = ? ' ;
    $strSQL .= ' AND A.LIBSDB = ? ' ;
    $params = array($LIBSNM,$LIBSID,$LIBSDB);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $r=db2_execute($stmt,$params);

        if($r === false){
            $rs = false;
        }else{
          while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'EXIST';
            }else{
                $rs = 'NEXIST';
            }
        }
    }
    return $rs;
}

/***
*ライブラリーを削除
***/
function  fnDelDB2LBLS($db2con,$LIBSNM){
        
        $rs = true;
        //構文
        $strSQL  = ' DELETE FROM DB2LBLS WHERE LIBSNM = ? AND LIBSFG = ? ';
        $params = array($LIBSNM,'0');
        e_log($strSQL);
        e_log(print_R($params,true));
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
                $rs = 'FAIL_DEL';
        }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $rs = 'FAIL_DEL';
                }
        }
        return $rs;
}

/***
*ライブラリーを登録
***/
function  fnInsertDB2LBLS($db2con,$LIBSNM,$LIBSID,$LIBSDB,$LIBSFG){
        $rs = true;
        //構文
        $strSQL  = ' INSERT INTO DB2LBLS ';
        $strSQL .= ' ( ';
                $strSQL .= ' LIBSNM, ';
                $strSQL .= ' LIBSID, ';
                $strSQL .= ' LIBSDB, ';
                $strSQL .= ' LIBSFG ';
        $strSQL .= ' ) ';
        $strSQL .= ' VALUES ';
        $strSQL .= ' (?,?,?,?) ';
       
        $params = array($LIBSNM,$LIBSID,$LIBSDB,$LIBSFG);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
                e_log(db2_stmt_errormsg());
                $rs = 'FAIL_INS';
        }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                e_log(db2_stmt_errormsg());
                    $rs = 'FAIL_INS';
                }
        }
        return $rs;
}
/***
*update
**/
function  fnUpdDB2LBLS($db2con,$LIBSNM,$LIBSID,$LIBSDB,$LIBSFG){
        $rs = true;
        //構文
        $strSQL  = ' UPDATE DB2LBLS ';
        $strSQL  .= ' SET LIBSFG = ?  ';
        $strSQL .= ' WHERE LIBSNM = ? AND LIBSDB = ? AND LIBSID = ?';
      
        $params = array($LIBSFG,$LIBSNM,$LIBSDB,$LIBSID);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
                 e_log(db2_stmt_errormsg());
                $rs = 'FAIL_UPD';
        }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $rs = 'FAIL_UPD';
                }
        }
        return $rs;
}