<?php

$Today = date('Y-m-d');
$today = str_replace("-","",$Today);
error_log('license chk type = '.$licenseType);
if($licenseDate_SP === '' && $licenseDate_EP === ''){
    $licenseType = $licenseType3;
    $licenseShow_P = true;

}else{
/*
    if($licenseDate_EP < $today){
            $licenseShow_P = false;
    }
*/
        if($licenseDate_ET !== '20000000' && $licenseDate_EP !== '20000000'){
            if((int)$licenseDate_ST <= (int)$today && (int)$licenseDate_ET >= (int)$today){
                //ライセンスがトライアルの時
                $licenseType = $licenseType2;
            }else if(((int)$licenseDate_SP <= (int)$today && (int)$licenseDate_EP >= (int)$today)){
                //ライセンスがプランの時
                $licenseType = $licenseType3;
            }else if((int)$licenseDate_SP <= (int)$today && $licenseDate_EP === ''){
                //ライセンスのプランの完了日がブラックの時と両親（開始日、完了日）がブラックの時
                $licenseType = $licenseType3;
            }else{
                //ライセンスがフリーの時
                $licenseType = $licenseType1;
            }
        }else{
            $licenseType = $licenseType3;
        }
    //いつからフリー版なるのを決める。
   if($licenseDate_ET !== '' && $licenseDate_EP !== '' ){
        if($licenseDate_ET !== '20000000' && $licenseDate_EP !== '20000000'){
            if((int)$licenseDate_ET < (int)$licenseDate_EP){
                    $licenseDate_SF = $licenseDate_EP;
            }else{
                    $licenseDate_SF = $licenseDate_ET;
            }
        }
    }else if($licenseDate_ET !== '' && $licenseDate_ET !== '20000000'){
                $licenseDate_SF = $licenseDate_ET;
    }else if($licenseDate_EP !== '' && $licenseDate_EP !== '20000000'){
            $licenseDate_SF = $licenseDate_EP;
    }
}
if($licenseType === 'FREE'){
        $licenseText = $licenseText_F;
        $licenseDate = $licenseDate_F;
        $licenseDate_S = '';
        $licenseDate_E = '';
        $licenseSerial = $licenseSerial_F;        // シリアルNo(例：シリアル番号65094CAの場合 => '65094CA')　チェックなし = ''
        $licenseUser = $licenseUser_F;          // ユーザー数(例：30人まで登録可能の場合 => 30)　無制限 = ''
        $licenseQuery = $licenseQuery_F;         // クエリー登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
        $licenseGroup = $licenseGroup_F;         // グループ登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
        $licenseKukaku = $licenseKukaku_F;         // 区画登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
        $licenseCl = $licenseCl_F;          // CL事前処理の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseSchedule = $licenseSchedule_F;    // スケジュール実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseScheduleBtn = $licenseScheduleBtn_F;    // スケジュールリストボタン実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licensePivot = $licensePivot_F;       // ピボット実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licensePivotCreate = $licensePivotCreate_F;                        //ビボッとの作成使用権限
        $licenseExcelcsvDownload = $licenseExcelcsvDownload_F;     //クエリーのダウンロード使用権限
        $licenseLog = $licenseLog_F;
        $licenseTemplate = $licenseTemplate_F;
        $licenseIFS = $licenseIFS_F;
        $licenseDrilldown = $licenseDrilldown_F;
        $licenseGraph = $licenseGraph_F;
        $licenseSeigyo = $licenseSeigyo_F; //制御実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseQueryGroup = $licenseQueryGroup_F; //クエリーグループ実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseSql = $licenseSql_F; //sqlクエリー実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseFileOut = $licenseFileOut_F; //ファイル出力実行の使用権限(例：使用権限あり => true、使用権限なし => false)


}else if($licenseType === 'TRIAL'){
        $yy = substr($licenseDate_ET,0,4);
        $mm = substr($licenseDate_ET,4,2);
        $dd = substr($licenseDate_ET,6,2);
        $licenseText = $licenseText_T."（".$yy."年".$mm."月".$dd."日）まで";
        $licenseDate = $yy."年".$mm."月".$dd."日まで";
        $licenseDate_S = $licenseDate_ST;
        $licenseDate_E = $licenseDate_ET;
        $licenseSerial = $licenseSerial_T;        // シリアルNo(例：シリアル番号65094CAの場合 => '65094CA')　チェックなし = ''
        $licenseUser = $licenseUser_T;          // ユーザー数(例：30人まで登録可能の場合 => 30)　無制限 = ''
        $licenseQuery = $licenseQuery_T;         // クエリー登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
        $licenseGroup = $licenseGroup_T;         // グループ登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
        $licenseKukaku = $licenseKukaku_T;         // 区画登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
        $licenseCl = $licenseCl_T;          // CL事前処理の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseSchedule = $licenseSchedule_T;    // スケジュール実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseScheduleBtn = $licenseScheduleBtn_T;    // スケジュールリストボタン実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licensePivot = $licensePivot_T;                          // ピボット実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licensePivotCreate = $licensePivotCreate_T;                      //ビボッとの作成使用権限       
        $licenseExcelcsvDownload = $licenseExcelcsvDownload_T;   //クエリーのダウンロード使用権限
        $licenseLog = $licenseLog_T;
        $licenseTemplate = $licenseTemplate_T;
        $licenseIFS = $licenseIFS_T;
        $licenseDrilldown = $licenseDrilldown_T;
        $licenseGraph = $licenseGraph_T;
        $licenseSeigyo = $licenseSeigyo_T; //制御実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseQueryGroup = $licenseQueryGroup_T; //クエリーグループ実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseSql = $licenseSql_T; //sqlクエリー実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseFileOut = $licenseFileOut_T; //ファイル出力実行の使用権限(例：使用権限あり => true、使用権限なし => false)

}else{
        if($licenseDate_EP !== ''){
            $yy = substr($licenseDate_EP,0,4);
            $mm = substr($licenseDate_EP,4,2);
            $dd = substr($licenseDate_EP,6,2);
            $licenseDate =  $yy."年".$mm."月".$dd."日まで";
            
        }else{
            $licenseDate = '';
        }
        $licenseDate_S = $licenseDate_SP;
        $licenseDate_E = $licenseDate_EP;
		$licenseText = $licenseText_P;
		$datetime1 =  new DateTime(date("Y-m-d"));
		$datetime2 = new DateTime($licenseDate_EP);
		$interval = $datetime1->diff($datetime2);
        if($licenseDate_EP !== ''){
            $diff = $interval->format('%R%a');
    		$diffplus = $interval->format('%a');
    		if($diff <= 30 && $diff >= 0){
            	$licenseText = 'ライセンス期限：あと'.($diffplus+1).'日';
    		}
        }
				
        $licenseSerial = $licenseSerial_P;        // シリアルNo(例：シリアル番号65094CAの場合 => '65094CA')　チェックなし = ''
        $licenseUser = $licenseUser_P;          // ユーザー数(例：30人まで登録可能の場合 => 30)　無制限 = ''
        $licenseQuery = $licenseQuery_P;         // クエリー登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
        $licenseGroup = $licenseGroup_P;         // グループ登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
        $licenseKukaku = $licenseKukaku_P;         // 区画登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
        $licenseCl = $licenseCl_P;          // CL事前処理の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseSchedule = $licenseSchedule_P;    // スケジュール実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseScheduleBtn = $licenseScheduleBtn_P;    // スケジュールリストボタン実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licensePivot = $licensePivot_P;                          // ピボット実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licensePivotCreate = $licensePivotCreate_P;                      //ビボッとの作成使用権限
        $licenseExcelcsvDownload = $licenseExcelcsvDownload_P;   //クエリーのダウンロード使用権限
        $licenseLog = $licenseLog_P;
        $licenseTemplate = $licenseTemplate_P;
        $licenseIFS = $licenseIFS_P;
        $licenseDrilldown = $licenseDrilldown_P;
        $licenseGraph = $licenseGraph_P;
        $licenseSeigyo = $licenseSeigyo_P; //制御実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseQueryGroup = $licenseQueryGroup_P; //クエリーグループ実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseSql = $licenseSql_P; //sqlクエリー実行の使用権限(例：使用権限あり => true、使用権限なし => false)
        $licenseFileOut = $licenseFileOut_P; //ファイル出力実行の使用権限(例：使用権限あり => true、使用権限なし => false)
}

/*if($licensePivot === true){
    $licenseSeigyo = true;
    $licenseQueryGroup = true;
}else{
    $licenseSeigyo = false;
    $licenseQueryGroup = false;
}*/