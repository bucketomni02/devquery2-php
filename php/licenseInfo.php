<?php
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
//include_once("licenseUpdateOperate.php");
//include_once("licenseUpdateRandom.php");
include_once("licenseUpdateOperate_4_2_2.php");
include_once("licenseUpdateRandom_4_2_2.php");
//ライセンス＝＞1の場合はシリアルキーなし、ブラクの場合はシリアルキーあり
$SERIALK_CONTROL = '';
/***
*version-
*TRIAL
* FREE
* PLAN
*
***/

/***
*
*
*
***/
/***
*licenseType3
*********
*Basic
*Professional
*Advance
*Premium
**/
//$licenseSeigyoSpecialFlg = false;//ＴＲＵＥになる場合はピボットフラグＦＡＬＳＥになっても制御は使えるようになります。
//$licenseSql = true;
//$licenseFileOut = true;
$LIC_UPD_FLG =  (isset($_POST['LIC_UPD_FLG']) ? $_POST['LIC_UPD_FLG'] : '');
$lic_db2con = cmDb2Con();
cmSetPHPQUERY($lic_db2con);
$licenseUpdateLICNUM = getUpateLicenseInfo($lic_db2con);
$LICNUM = $licenseUpdateLICNUM['data'][0]['LICNUM'];//ライセンス番号
$LICNUM = cmMer($LICNUM);
$mainSerialKey = '';
//SERIALK_CONTROL =>1 の場合はシリアルキーなしでライセンスを作る
if($SERIALK_CONTROL === ''){
    $mainSerialKey = cmGETSRLNBR($lic_db2con);
}
$licenseUpdateInfo = decryptOptionKey($LICNUM,$mainSerialKey);
//$licenseUpdateInfo = getLicenseReplace();
$licenseType1 = 'FREE';
$licenseType2 = 'TRIAL';
$licenseType3 = $licenseUpdateInfo[1]['licenseType'];//プラン
$licenseType4 = $licenseUpdateInfo[2]['licenseType'];//トライアル版に使う
//$licenseType = 'Professional';       // ライセンスタイプ
//$licenseDate = '2019/9/30';          // ライセンス期限(例：2014年9月30日までの場合 => '20140930')　無期限 = ''

/*
*-------------------------------------------------------* 
* ライセンス管理
*-------------------------------------------------------*
*
*/

/*
*
*
*　 FREE
*
*/
$F_LICE = $licenseUpdateInfo[0];
$licenseText_F = "フリー版"; //ツールバーに表示する文字
$licenseTitle_F = "FREE"; //ライセンス情報に表示する
$licenseDate_SF = '~';
$licenseSerial_F = '';        // シリアルNo(例：シリアル番号65094CAの場合 => '65094CA')　チェックなし = ''
$licenseUser_F = (int)$F_LICE['licenseUser'] > 0 ? (int)$F_LICE['licenseUser'] : '';           // ユーザー数(例：30人まで登録可能の場合 => 30)　無制限 = ''
$licenseQuery_F = (int)$F_LICE['licenseQuery'] > 0 ? (int)$F_LICE['licenseQuery'] : '';        // クエリー登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
$licenseGroup_F = (int)$F_LICE['licenseGroup'] > 0 ? (int)$F_LICE['licenseGroup'] : '';         // グループ登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
$licenseKukaku_F = (int)$F_LICE['licenseKukaku'] > 0 ? (int)$F_LICE['licenseKukaku'] : '';       // 区画登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
$licenseCl_F = $F_LICE['licenseCl'] == '1' ? true : false;          // CL事前処理の使用権限(例：使用権限あり => true、使用権限なし => false)
$licenseSchedule_F = $F_LICE['licenseSchedule'] == '1' ? true : false;    // スケジュール実行の使用権限(例：使用権限あり => true、使用権限なし => false)
$licenseScheduleBtn_F = $F_LICE['licenseScheduleBtn'] == '1' ? true : false;    // スケジュールリストボタン実行の使用権限(例：使用権限あり => true、使用権限なし => false)
$licensePivot_F = $F_LICE['licensePivot'] == '1' ? true : false;       // ピボット実行の使用権限(例：使用権限あり => true、使用権限なし => false) //制御の使用権限
$licensePivotCreate_F = $F_LICE['licensePivotCreate'] == '1' ? true : false;  //ビボッとの作成使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseExcelcsvDownload_F = $F_LICE['licenseExcelcsvDownload'] == '1' ? true : false;//クエリーのダウンロード使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseShow_F = $F_LICE['licenseShow'] == '1' ? true : false;
$licenseLog_F = $F_LICE['licenseLog'] == '1' ? true : false;//操作ログ使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseTemplate_F = $F_LICE['licenseTemplate'] == '1' ? true : false;//テンプレート使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseIFS_F = $F_LICE['licenseIFS'] == '1' ? true : false;//IFSフォルダ出力使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseDrilldown_F = $F_LICE['licenseDrilldown'] == '1' ? true : false;//クエリー連携使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseGraph_F = $F_LICE['licenseGraph'] == '1' ? true : false;//グラフ使用権限(例：使用権限あり => true、使用権限なし => false)
$licenseSeigyo_F = $F_LICE['licenseSeigyo'] == '1' ? true : false;//制御使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseQueryGroup_F = $F_LICE['licenseQueryGroup'] == '1' ? true : false;//クエリーグループ使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseSql_F = $F_LICE['licenseSql'] == '1' ? true : false;//sqlクエリー使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseFileOut_F = $F_LICE['licenseFileOut'] == '1' ? true : false;//ファイル出力使用権限(例：使用権限あり => true、使用権限なし => false) 

/*
*
*
*　 PLAN
*
*/
$P_LICE = $licenseUpdateInfo[1];
$licenseText_P = ""; //ツールバーに表示する文字
$licenseTitle_P = $licenseType3; //ライセンス情報に表示する
$licenseDate_SP = $P_LICE['licenseDate_S'] !== '20000000' ? $P_LICE['licenseDate_S'] : '';  // ライセンス期限(例：-年-月-日からの場合 => '20140930')　無期限 = ''　プランなしの場合は0
$licenseDate_EP = $P_LICE['licenseDate_E'] !== '20000000' ? $P_LICE['licenseDate_E'] : '';  // ライセンス期限(例：-年-月-日までの場合 => '20140930')　無期限 = ''　プランなしの場合は0
$licenseSerial_P = $P_LICE['licenseSerial'] !== '0000000' ? $P_LICE['licenseSerial'] : '';        // シリアルNo(例：シリアル番号65094CAの場合 => '65094CA')　チェックなし = ''
$licenseUser_P = (int)$P_LICE['licenseUser'] > 0 ? (int)$P_LICE['licenseUser'] : '';          // ユーザー数(例：30人まで登録可能の場合 => 30)　無制限 = ''
$licenseQuery_P = (int)$P_LICE['licenseQuery'] > 0 ? (int)$P_LICE['licenseQuery'] : '';         // クエリー登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
$licenseGroup_P = (int)$P_LICE['licenseGroup'] > 0 ? (int)$P_LICE['licenseGroup'] : '';        // グループ登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
$licenseKukaku_P = (int)$P_LICE['licenseKukaku'] > 0 ? (int)$P_LICE['licenseKukaku'] : '';         // 区画登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
$licenseCl_P = $P_LICE['licenseCl'] == '1' ? true : false;          // CL事前処理の使用権限(例：使用権限あり => true、使用権限なし => false)
$licenseSchedule_P = $P_LICE['licenseSchedule'] == '1' ? true : false;     // スケジュール実行の使用権限(例：使用権限あり => true、使用権限なし => false)
$licenseScheduleBtn_P = $P_LICE['licenseScheduleBtn'] == '1' ? true : false;   // スケジュールリストボタン実行の使用権限(例：使用権限あり => true、使用権限なし => false)
$licensePivot_P = $P_LICE['licensePivot'] == '1' ? true : false;     // ピボット実行の使用権限(例：使用権限あり => true、使用権限なし => false) //制御の使用権限
$licensePivotCreate_P = $P_LICE['licensePivotCreate'] == '1' ? true : false;              //ビボッとの作成使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseExcelcsvDownload_P = $P_LICE['licenseExcelcsvDownload'] == '1' ? true : false;     //クエリーのダウンロード使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseShow_P = $P_LICE['licenseShow'] == '1' ? true : false;
$licenseLog_P = $P_LICE['licenseLog'] == '1' ? true : false;//操作ログ使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseTemplate_P = $P_LICE['licenseTemplate'] == '1' ? true : false;//テンプレート使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseIFS_P = $P_LICE['licenseIFS'] == '1' ? true : false;//IFSフォルダ出力使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseDrilldown_P = $P_LICE['licenseDrilldown'] == '1' ? true : false;//クエリー連携使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseGraph_P = $P_LICE['licenseGraph'] == '1' ? true : false;//グラフ使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseSeigyo_P = $P_LICE['licenseSeigyo'] == '1' ? true : false;//制御使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseQueryGroup_P = $P_LICE['licenseQueryGroup'] == '1' ? true : false;//クエリーグループ使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseSql_P = $P_LICE['licenseSql'] == '1' ? true : false;//sqlクエリー使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseFileOut_P = $P_LICE['licenseFileOut'] == '1' ? true : false;//ファイル出力使用権限(例：使用権限あり => true、使用権限なし => false) 

/*
*
*
*　 TRIAL
*
*/

$T_LICE = $licenseUpdateInfo[2];
$licenseText_T = "無料トライアル中";//シールバーに表示する文字
$licenseTitle_T = $licenseType4."(無料トライアル)";//ライセンス情報に表示する
$licenseDate_ST = $T_LICE['licenseDate_S'] !== '20000000' ? $T_LICE['licenseDate_S'] : '';         // ライセンス期限(例：-年-月-日 からの場合 => '20140930')　トライアルなし = ''
$licenseDate_ET = $T_LICE['licenseDate_E'] !== '20000000' ? $T_LICE['licenseDate_E'] : '';       // ライセンス期限(例：-年-月-日までの場合 => '20140930')　トライアルなし = ''
$licenseSerial_T = $T_LICE['licenseSerial'] !== '0000000' ? $T_LICE['licenseSerial'] : '';        // シリアルNo(例：シリアル番号65094CAの場合 => '65094CA')　チェックなし = ''
$licenseUser_T = (int)$T_LICE['licenseUser'] > 0 ? (int)$T_LICE['licenseUser'] : '';             // ユーザー数(例：30人まで登録可能の場合 => 30)　無制限 = ''
$licenseQuery_T = (int)$T_LICE['licenseQuery'] > 0 ? (int)$T_LICE['licenseQuery'] : '';          // クエリー登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
$licenseGroup_T = (int)$T_LICE['licenseGroup'] > 0 ? (int)$T_LICE['licenseGroup'] : '';          // グループ登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
$licenseKukaku_T = (int)$T_LICE['licenseKukaku'] > 0 ? (int)$T_LICE['licenseKukaku'] : '';        // 区画登録数(例：3つまで登録可能の場合 => 3)　無制限 = ''
$licenseCl_T = $T_LICE['licenseCl'] == '1' ? true : false;         // CL事前処理の使用権限(例：使用権限あり => true、使用権限なし => false)
$licenseSchedule_T = $T_LICE['licenseSchedule'] == '1' ? true : false;   // スケジュール実行の使用権限(例：使用権限あり => true、使用権限なし => false)
$licenseScheduleBtn_T = $T_LICE['licenseScheduleBtn'] == '1' ? true : false;    // スケジュールリストボタン実行の使用権限(例：使用権限あり => true、使用権限なし => false)
$licensePivot_T = $T_LICE['licensePivot'] == '1' ? true : false;        // ピボット実行の使用権限(例：使用権限あり => true、使用権限なし => false) //制御の使用権限
$licensePivotCreate_T = $T_LICE['licensePivotCreate'] == '1' ? true : false;             //ビボッとの作成使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseExcelcsvDownload_T = $T_LICE['licenseExcelcsvDownload'] == '1' ? true : false;       //クエリーのダウンロード使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseShow_T =$T_LICE['licenseShow'] == '1' ? true : false;    //トライアルのデータ詳細の表示(例：表示 => true、非表示 => false) 
$licenseLog_T = $T_LICE['licenseLog'] == '1' ? true : false;//操作ログ使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseTemplate_T = $T_LICE['licenseTemplate'] == '1' ? true : false;//テンプレート使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseIFS_T = $T_LICE['licenseIFS'] == '1' ? true : false;//IFSフォルダ出力使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseDrilldown_T = $T_LICE['licenseDrilldown'] == '1' ? true : false;//クエリー連携使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseGraph_T = $T_LICE['licenseGraph'] == '1' ? true : false;//グラフ使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseSeigyo_T = $T_LICE['licenseSeigyo'] == '1' ? true : false;//制御使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseQueryGroup_T = $T_LICE['licenseQueryGroup'] == '1' ? true : false;//クエリーグループ使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseSql_T = $T_LICE['licenseSql'] == '1' ? true : false;//sqlクエリー使用権限(例：使用権限あり => true、使用権限なし => false) 
$licenseFileOut_T = $T_LICE['licenseFileOut'] == '1' ? true : false;//ファイル出力使用権限(例：使用権限あり => true、使用権限なし => false) 

//new
$licenseProgressText = 'PHPQUERY';                                  //初期ロード時のプログレスバーに出るテキスト(TOUCHのログインヘッダーにも使用)
$licenseLogoPath = './common/images/phpquery_logo1814.png';                                      //ログイン画面ロゴイメージパス(ルートディレクトリからのパス、非表示はブランク)
$licenseCompanyName = 'PHPQUERY';                                   //デフォルトの会社情報(DBがブランクの場合に反映)
$licenseFavicon = './common/images/favicon20190508.ico';       //facivon(ルートディレクトリからのパス)
$licenseTitle = 'PHPQUERY';                                         //webページのタイトル(ブランクはIPになる)

//カラー設定
$licenseColor1 = 'sea-blue';
$licenseColor2 = '';
$licenseColor3 = 'gray';
/******************************
red
blue
orange
purple
green
fb
muted
dark
pink
brown
sea-blue
banana
gray(テーブルヘッダのみ)
******************************/
function getUpateLicenseInfo($db2con){
        $data = array();
        $params = array();
        $strSQL = 'SELECT * FROM DB2LICE ORDER BY LICKEY DESC FETCH FIRST 1 ROW ONLY';
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt == false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r == false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;
}
include_once("licenseChk.php");

