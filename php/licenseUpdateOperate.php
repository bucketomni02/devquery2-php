<?php
/*
※大切な注意※
新しいライセンスフラグが増やしたら、下記の機能を必ず変更してください。
※getLicenseSubStr()
※subStrOptionKey()
*/
/***
*RAMDOM FUNCTION
*createRandom => create random with a-zA-Z0-9
*getflgArray => replacing key of flg
*getLicenseReplace => license empty array
*getYearRandom => Replacing year 2000 - 2999
*getSerialRandom => encrypt serial random key number
*getRandomArr => encrypt random array 
*
***/

/***
*
*encryptSerialKey => Encrypting serial key
*encryptLicenseInfo => Encryping licenseInfo
*encryptConvertUnknown=>converting UnknownString
*encryptConvertUnknownString=>making unknown three string array to one string
*decryptSerialKey => Decrypting serial key
*decryptConvertArray=>convert optionkey to 3 array
*decryptConvertKnown =>reget origin data by decryping
*decryptingKey => decripting and replacing the origin data
*decryptLicenseInfo=>decrypting licenseInfo
*convertString =>converting array to 3 string array
*convertSubStrAry =>split 3 string data

*
***/

/**
*
*Encrypting option key
*
**/
function encryptOptionKey($licenseInfo,$serialKey){
         if($serialKey === ''){
            $serialKey = '0000000';
         }
        $encryptSerialKey = encryptSerialKey($serialKey);
        $encryptLicenseInfo = encryptLicenseInfo($licenseInfo);
        $licenseStringArray = convertString($encryptLicenseInfo);
        $subStrAry = convertSubStrAry($licenseStringArray,$encryptSerialKey,1);
        $unknown = encryptConvertUnknown($subStrAry);
        $optionKey = encryptConvertUnknownString($unknown);
        return $optionKey;
}
/**
*
*Decrypting option key
*
**/
function decryptOptionKey($optionKey,$serialKey){
         if($serialKey === ''){
            $serialKey = '0000000';
         }
        $encryptSerialKey = encryptSerialKey($serialKey);
        $licenseStringArray = decryptConvertArray($optionKey);
        $subStrAry = convertSubStrAry($licenseStringArray,$encryptSerialKey,0);
        $optionArr =  decryptConvertKnown($subStrAry,$encryptSerialKey);
        $option = decryptingKey($optionArr);
        $optionData = decryptLicenseInfo($option);
       return $optionData;
}
/**
*Encryping licenseInfo
**/
function encryptLicenseInfo($licenseInfo){
        $keyFlgArr = getflgArray();
        $yearRandom = getYearRandom(); // Replacing year 2000 - 2999
        foreach($licenseInfo as $key => $value){
               $licenseInfo[$key]['licenseType'] = $keyFlgArr[$key]['licenseType'][$value['licenseType']];
               if($key >0 ){
                   if($licenseInfo[$key]['licenseDate_S'] === ''){
                       $licenseInfo[$key]['licenseDate_S'] = '20000000';
                    }
                       $start_arr = str_split($licenseInfo[$key]['licenseDate_S'], 2);
                       $startDate = $yearRandom[$start_arr[0]].$start_arr[1].$start_arr[2].$start_arr[3];
                       $licenseInfo[$key]['licenseDate_S'] = $startDate;

                   if($licenseInfo[$key]['licenseDate_E'] === ''){
                       $licenseInfo[$key]['licenseDate_E'] = '20000000';
                    }
                    $end_arr = str_split($licenseInfo[$key]['licenseDate_E'], 2);
                    $endDate = $yearRandom[$end_arr[0]].$end_arr[1].$end_arr[2].$end_arr[3];
                    $licenseInfo[$key]['licenseDate_E'] = $endDate;
                   if($licenseInfo[$key]['licenseSerial'] === ''){
                       $licenseInfo[$key]['licenseSerial'] = '0000000';
                    }
                    $licenseSerial = encryptSerialKey($licenseInfo[$key]['licenseSerial']);
                    $licenseInfo[$key]['licenseSerial'] = $licenseSerial;
                    $licenseVersion = $licenseInfo[$key]['licenseVersion'];
                    $licenseInfo[$key]['licenseVersion'] = versionReplacing(0,$licenseVersion);
                }
                $licenseUser = $licenseInfo[$key]['licenseUser'];
                while(strlen($licenseUser)<3 || $licenseUser === ''){
                        $licenseUser = '0'.$licenseUser;
               } 
               $licenseInfo[$key]['licenseUser'] = $licenseUser;

               $licenseQuery = $licenseInfo[$key]['licenseQuery'];
                while(strlen($licenseQuery)<3 || $licenseQuery === ''){
                        $licenseQuery = '0'.$licenseQuery;
               }
               $licenseInfo[$key]['licenseQuery'] = $licenseQuery;

               $licenseGroup = $licenseInfo[$key]['licenseGroup'];
                while(strlen($licenseGroup)<3 || $licenseGroup === ''){
                        $licenseGroup = '0'.$licenseGroup;
               }
               $licenseInfo[$key]['licenseGroup'] = $licenseGroup;
               $licenseKukaku = $licenseInfo[$key]['licenseKukaku'];
                while(strlen($licenseKukaku)<3 || $licenseKukaku === ''){
                        $licenseKukaku = '0'.$licenseKukaku;
               }
               $licenseInfo[$key]['licenseKukaku'] = $licenseKukaku;
               $licenseInfo[$key]['licenseCl'] = $keyFlgArr[$key]['licenseCl'][$value['licenseCl']];
               $licenseInfo[$key]['licenseSchedule'] = $keyFlgArr[$key]['licenseSchedule'][$value['licenseSchedule']];
               $licenseInfo[$key]['licenseScheduleBtn'] = $keyFlgArr[$key]['licenseScheduleBtn'][$value['licenseScheduleBtn']];
               $licenseInfo[$key]['licensePivot'] = $keyFlgArr[$key]['licensePivot'][$value['licensePivot']];
               $licenseInfo[$key]['licensePivotCreate'] = $keyFlgArr[$key]['licensePivotCreate'][$value['licensePivotCreate']];
               $licenseInfo[$key]['licenseExcelcsvDownload'] = $keyFlgArr[$key]['licenseExcelcsvDownload'][$value['licenseExcelcsvDownload']];
               $licenseInfo[$key]['licenseShow'] = $keyFlgArr[$key]['licenseShow'][$value['licenseShow']];
               $licenseInfo[$key]['licenseLog'] = $keyFlgArr[$key]['licenseLog'][$value['licenseLog']];
               $licenseInfo[$key]['licenseTemplate'] = $keyFlgArr[$key]['licenseTemplate'][$value['licenseTemplate']];
               $licenseInfo[$key]['licenseIFS'] = $keyFlgArr[$key]['licenseIFS'][$value['licenseIFS']];
               $licenseInfo[$key]['licenseDrilldown'] = $keyFlgArr[$key]['licenseDrilldown'][$value['licenseDrilldown']];
               $licenseInfo[$key]['licenseGraph'] = $keyFlgArr[$key]['licenseGraph'][$value['licenseGraph']];
        }
    return $licenseInfo;
}

/**
*decrypting licenseInfo
**/
/**
*decrypting licenseInfo
**/
function decryptLicenseInfo($optionData){
        $rtn = 0;
        $keyFlgArr = getflgArray();
        $yearRandom = getYearRandom(); // Replacing year 2000 - 2999
        foreach($optionData as $key => $value){
//                if($optionData[$key]['licenseDate_E'] !== ''){
                if($key > 0){
                   $start = $optionData[$key]['licenseDate_S'];
                   $startDate = array_search($start[0],$yearRandom).substr($start,1);
                    if($startDate !== false){
                       $optionData[$key]['licenseDate_S'] = $startDate;
                   }else{
                        $rtn = 1;
                        break;
                    }
                   $end = $optionData[$key]['licenseDate_E'];
                   $endDate = array_search($end[0],$yearRandom).substr($end,1);
                    if($endDate !== false){
                       $optionData[$key]['licenseDate_E'] = $endDate;
                    }else{
                        $rtn = 1;
                        break;
                    }
                    $licenseType = $keyFlgArr[$key]['licenseType'];
                    $type = array_search($value['licenseType'],$licenseType);
                    if($type !== false){
                        if($type === 1){
                            $licenseType = 'BASIC';
                        }else if($type === 2){
                            $licenseType = 'ADVANCE';
                        }else if($type === 3){
                            $licenseType = 'PREMIUM';
                        }else{
                            $licenseType = 'ENTERPRISE';
                        }
                        $optionData[$key]['licenseType'] = $licenseType ;
                    }else{
                        $rtn = 1;
                        break;
                    }
                    $licenseVersion = versionReplacing(1,$optionData[$key]['licenseVersion']);
                    if($licenseVersion !== false){
                        $optionData[$key]['licenseVersion'] = $licenseVersion;
                    }else{
                        $rtn = 1;
                        break;
                    }
                }
                $cl = $keyFlgArr[$key]['licenseCl'];
                $cl = array_search($value['licenseCl'],$cl);
                if($cl !== false){
                    $optionData[$key]['licenseCl'] =  $cl;
                }else{
                        $rtn = 1;
                        break;
                }
                $schedule = $keyFlgArr[$key]['licenseSchedule'];
                $schedule = array_search($value['licenseSchedule'],$schedule);
                if($schedule !== false){
                    $optionData[$key]['licenseSchedule'] = $schedule;
                }else{
                        $rtn = 1;
                        break;
                }
                $scheduleBtn = $keyFlgArr[$key]['licenseScheduleBtn'];
                $scheduleBtn = array_search($value['licenseScheduleBtn'],$scheduleBtn);
                if($scheduleBtn !== false){
                    $optionData[$key]['licenseScheduleBtn'] = $scheduleBtn;
                }else{
                        $rtn = 1;
                        break;
                }
                $pivot = $keyFlgArr[$key]['licensePivot'];
                $pivot = array_search($value['licensePivot'],$pivot);
                if($pivot !== false) {
                    $optionData[$key]['licensePivot'] = $pivot;
                }else{
                        $rtn = 1;
                        break;
                }
                $pivotC = $keyFlgArr[$key]['licensePivotCreate'];
                $pivotC = array_search($value['licensePivotCreate'],$pivotC);
                if($pivotC !== false){
                    $optionData[$key]['licensePivotCreate'] = $pivotC;
                }else{
                        $rtn = 1;
                        break;
                }
                $excsv = $keyFlgArr[$key]['licenseExcelcsvDownload'];
                $excsv = array_search($value['licenseExcelcsvDownload'],$excsv);
                if($excsv !== false){
                    $optionData[$key]['licenseExcelcsvDownload'] = $excsv;
                }else{
                        $rtn = 1;
                        break;
                }
                $show = $keyFlgArr[$key]['licenseShow'];
                $show = array_search($value['licenseShow'],$show);
                if($show !== false){
                    $optionData[$key]['licenseShow'] = $show;
                }else{
                        $rtn = 1;
                        break;
                }
                $log = $keyFlgArr[$key]['licenseLog'];
                $log = array_search($value['licenseLog'],$log);
                if($log !== false){
                    $optionData[$key]['licenseLog'] = $log;
                }else{
                        $rtn = 1;
                        break;
                }
                $template = $keyFlgArr[$key]['licenseTemplate'];
                $template =array_search($value['licenseTemplate'],$template);
                if($template !== false){
                    $optionData[$key]['licenseTemplate'] =$template;
                }else{
                        $rtn = 1;
                        break;
                }
                $ifs = $keyFlgArr[$key]['licenseIFS'];
                $ifs = array_search($value['licenseIFS'],$ifs);
                if($ifs !== false){
                    $optionData[$key]['licenseIFS'] = $ifs;
                }else{
                        $rtn = 1;
                        break;
                }
                $drill = $keyFlgArr[$key]['licenseDrilldown'];
                $drill = array_search($value['licenseDrilldown'],$drill);
                if($drill !== false){
                    $optionData[$key]['licenseDrilldown'] = $drill;
                }else{
                        $rtn = 1;
                        break;
                }
                $graph = $keyFlgArr[$key]['licenseGraph'];
                $graph = array_search($value['licenseGraph'],$graph);
                if($graph !== false){
                    $optionData[$key]['licenseGraph'] = $graph;
                }else{
                        $rtn = 1;
                        break;
                }
        }
    if($rtn === 0){
        return $optionData;
    }else{
        return false;
    }
}

/**
*
*Encrypting serial key
*
**/
function encryptSerialKey($serialKey){
    $serialKeySplit = str_split($serialKey);
    $serialRandomArr = getSerialRandom();
    $encryptSerialKey = '';
    foreach($serialKeySplit  as $key => $value){
        if (preg_match('/^[-]?[0-9]*\.?[0-9]+$/', $value)){
            $encryptSerialKey .= '0'.$value;
        }else{
            $encryptSerialKey .= $serialRandomArr[$value];
        }
    }
    return $encryptSerialKey;
}


/**
*
*decrypting serial key
*
**/
function decryptSerialKey($encSerialKey){
    $serialRandomArr = getSerialRandom();
    $decryptSerialKey  = '';
    $start = 0;
    $len = strlen($encSerialKey);
    do{
        $value = substr($encSerialKey,$start,2);
        if((int)$value < 10){
             $decryptSerialKey .= (int)$value;
        }else{
             $decryptSerialKey .=array_search($value, $serialRandomArr);
        }
        $start +=2;
    }while($len > $start);
    return $decryptSerialKey;
}
/**
*
*converting array to 3 string array
*
**/
function convertString($licenseInfo){
    $licenseStringArray = array();
    foreach($licenseInfo as $key =>$value){
        $licen = '';
        foreach($value as $k => $v){
                $licen .= $v;
        }
        array_push($licenseStringArray,$licen);
    }
    return $licenseStringArray;
}
/**
*
*
*split 3 string data
*
*
**/
function convertSubStrAry($licenseArr,$encryptSerialKey,$flg){
    $serialSpilt = str_split($encryptSerialKey, 2);
    $SubAry = array(); 
    foreach($licenseArr as $key =>$value){
        $subDta = array();
        $flgC = 0;
        $serialFlg = 0;
        $len =  strlen($value);
        do{
            if($flg === 1){
               $v = substr($value,$flgC,3);
               $v_len = strlen($v);
                $v = (int)$v + (int)$serialSpilt[$serialFlg] ; //encrypt
                if($v >999){
                    $v = $v -1000;
                 } 
                while(strlen((string)$v) < $v_len){
                            $v = '0'.$v;
                }
                array_push($subDta,$v);
                if($serialFlg+1<count($serialSpilt)){
                    $serialFlg++;
                }else{
                   $serialFlg = 0;
                 }
                $flgC +=3;
            }else{
                 $v = substr($value,$flgC,2);
                 array_push($subDta,$v);    //decrypt
                $flgC +=2;
            }
        }while($len > $flgC);
        array_push($SubAry,$subDta);
    }
    return $SubAry;
}

/***
*
*converting UnknownString
*
***/
function encryptConvertUnknown($licenseArrSubString){
    $licenseRandomArr = getRandomArr();
    $unknown = array();
    foreach($licenseArrSubString as $key => $value){
        $unknownArr = array();
        foreach($value as $k => $v){
                  $inv = (int)$v;
                array_push($unknownArr,$licenseRandomArr[$inv]);
        }
        array_push($unknown,$unknownArr);
    }
    return $unknown;
}
/**
*
*
*making unknown three string array to one string
*
**/
function encryptConvertUnknownString($unknown){
    $optionKey = '';
    foreach($unknown as $key => $value){
        $option = '';
        foreach($value as $k => $v){
                $option .= $v;
        }
        $optionKey .=$option;
    }
   return $optionKey;
}
/**
*
*convert optionkey to 3 array
*
**/
function decryptConvertArray($optionKey){
        $licenseArr = array();
        $subStrOpt = subStrOptionKey();
        $st = 0;
        foreach($subStrOpt as $key => $value){
                $licen = substr($optionKey,$st,$value);
                $st += $value;
                array_push($licenseArr,$licen);
        }
    return $licenseArr;
}

/**
*
*reget origin data by decryping
*
**/
function decryptConvertKnown($optionKeyArr,$encryptSerialKey){
   $serialSpilt = str_split($encryptSerialKey, 2);
    $licenseRandomArr = getRandomArr();
    $parentString = array();

    foreach($optionKeyArr as $key => $value){
        $childString = '';
        $serialFlg = 0;
        foreach($value as $k => $v){
            $ranKey = array_search($v, $licenseRandomArr);
            $ranKey = (int)$ranKey - (int)$serialSpilt[$serialFlg];
             if($ranKey < 0){
                    $ranKey = $ranKey+1000;
             }
                    if($k === count($value)-1){
                            //no action in last value of in 0 index
                    }else{
                        $len = 3 - strlen($ranKey) ;
                         while( $len > 0){//前ゼロconcat
                                $ranKey = '0'.$ranKey;
                                $len--;
                         }
                    }
             //   }
                if($serialFlg+1<count($serialSpilt)){
                    $serialFlg++;
                }else{
                   $serialFlg = 0;
                 }
           $childString .= $ranKey;
        }
            array_push($parentString,$childString);
    } 

    return $parentString;
}

/**
*
*decripting and replacing the origin data
*
**/
function decryptingKey($optionArr){
    $licenseInfoReplace = getLicenseReplace();//get empty license array
    $optArr = array();
        $subStrArr = getLicenseSubStr();
         foreach($optionArr as $key => $value){
            $len =  strlen($value);
            $opt = array();
            $flgC = 0;
            $idx = 0;
            if($key ===0) {
                for($i = 0; $i<5; $i++){
                        array_push($opt,'');
                }
            }
            do{
                array_push($opt,substr($value,$flgC,$subStrArr[$key][$idx]));
                $flgC +=$subStrArr[$key][$idx];
                $idx++;
                if(count($subStrArr[$key]) === $idx){
                    break;
                }
            }while($len > $flgC);
            array_push($optArr,$opt);
        } 
        //error_log("optArr".print_r($optArr,true)); 
     foreach($licenseInfoReplace as $key =>$value){
                $idx = 0;
                foreach($value as $k => $v){
                      $val = $optArr[$key][$idx];
                         if(strlen($val) === 14){
                             $val  =  decryptSerialKey($val);
                         }
                    $licenseInfoReplace[$key][$k] = $val;
                    $idx++;
                }
        }
    return $licenseInfoReplace;
}

//trimming string array
function getLicenseSubStr(){
    $licenseSubStr = array(
        array(3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1),//free
        array(1,7,7,14,3,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1),//plan
        array(1,7,7,14,3,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1)//trial
    );
    return $licenseSubStr;
}
//substring of optionKey
    function subStrOptionKey(){
        $optSubStr = array(16,38,38);
        return $optSubStr;
    }

/*
*version replacing 
* flg => 0 convert to unknown
* flg => 1 revert from unknown
*
*/
function versionReplacing($flg,$val){
    $versionRe = verionReplace();
    $val = str_split($val);
    $value = '';
   if($flg === 0){
        foreach($val as $k => $v){
                $value .= $versionRe[$v];
        }
    }else{
        foreach($val as $k => $v){
             $vl = array_search($v,$versionRe);
            $value .= $vl;
            /*if($vl === false){
                $value = false;
            }else
                 $value .= $vl;
            }*/
        }
    }
    return $value;
}
?>