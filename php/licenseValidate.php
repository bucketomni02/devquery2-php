<?php
include_once("licenseInfo.php");
$rtn = 0;
$msg = '';
$FLG = (isset($_POST['FLG'])) ? $_POST['FLG'] : '';
/*if($licenseChk === false && $FLG !== '1'){
    $rtn = 1;
    $msg = "体験デモ版の試用期間が終了しました。継続利用いただく場合は、正規版ライセンスをご購入ください。";
}*/

if($FLG === 'PIVOTCREATE'){
    if($licensePivotCreate === false){
        $rtn = 1;
    }
}
if($FLG === 'DOWNLOAD'){
    if($licenseExcelcsvDownload === false){
        $rtn = 1;
    }
}
if($FLG === 'SCHBTN'){
    if($licenseScheduleBtn === false){
        $rtn = 1;
    }
}
if($rtn === 1){
        $msg = showMsg('FAIL_CHKLICENSE');//"体験デモ版の試用期間が終了しました。継続利用いただく場合は、正規版ライセンスをご購入ください。";
}
$rtn = array(
    "RTN" => $rtn,
    "MSG" => $msg,
    "FLG" => $FLG,
    'licenseDate_S' => $licenseDate_S,
    'licenseDate_E' => $licenseDate_E,
    "licenseType" =>$licenseType,
    "licenseTitle"=> $licenseTitle,
    'licenseUser' =>$licenseUser,//プラン
    'licenseQuery' => $licenseQuery,
    'licenseExcelcsvDownload' => $licenseExcelcsvDownload,
    'licenseSchedule' => $licenseSchedule,
    'licensePivot' => $licensePivot,
    'licenseCl' =>$licenseCl,
    'licenseDate_SP' => $licenseDate_SP,
    "licenseDate_SF" => $licenseDate_SF,
    'licenseDate_EP' =>$licenseDate_EP,
    "licenseTitle_F"=> $licenseTitle_F,
    'licenseUser_F' =>$licenseUser_F,//フリー
    'licenseQuery_F' => $licenseQuery_F,
    'licenseExcelcsvDownload_F' => $licenseExcelcsvDownload_F,
    'licenseSchedule_F' => $licenseSchedule_F,
    'licensePivot_F' => $licensePivot_F,
    'licenseCl_F' =>$licenseCl_F, 
    "licenseTitle_T"=> $licenseTitle_T,
    'licenseUser_T' =>$licenseUser_T,//トライアル
    'licenseQuery_T' => $licenseQuery_T,
    'licenseExcelcsvDownload_T' => $licenseExcelcsvDownload_T,
    'licenseSchedule_T' => $licenseSchedule_T,
    'licensePivot_T' => $licensePivot_T,
    'licenseCl_T' =>$licenseCl_T, 
    'licenseShow_T' => $licenseShow_T,
    'licenseDate_ST' => $licenseDate_ST,
    'licenseDate_ET' => $licenseDate_ET,
    "licenseTitle_P"=> $licenseTitle_P,
    'licenseUser_P' =>$licenseUser_P,//プラン
    'licenseQuery_P' => $licenseQuery_P,
    'licenseExcelcsvDownload_P' => $licenseExcelcsvDownload_P,
    'licenseSchedule_P' => $licenseSchedule_P,
    'licensePivot_P' => $licensePivot_P,
    'licenseCl_P' =>$licenseCl_P,
    'licenseDate_SP' => $licenseDate_SP,
    'licenseDate_EP ' =>$licenseDate_EP,
    'licenseShow_P' => $licenseShow_P
);
echo(json_encode($rtn));