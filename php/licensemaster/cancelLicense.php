<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/

/*include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseUpdateOperate_4_1_0.php");
include_once("../licenseUpdateRandom_4_1_0.php");
$SERIALK_CONTROL = '';
*/
include_once("../licenseInfo.php");

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$LICNUM = (isset($_POST['LICNUM']))?$_POST['LICNUM']:'';
$licenseVersion = (isset($_POST['licenseVersion']))?$_POST['licenseVersion']:'';
$licenseVersion = (int)$licenseVersion;
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

$LICNUM = cmHscDe($LICNUM);
$LICSER = '';
//ライセンス＝＞1の場合はシリアルキーなし、ブラクの場合はシリアルキーあり
if($SERIALK_CONTROL === ''){
    $LICSER = cmGETSRLNBR($db2con);
    $LICSER = (isset($LICSER))?trim($LICSER):'';
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('ライセンス情報の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'29',$userData[0]['WUSAUT']);//'1' => LicenseMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライセンス情報の権限'));
            }
        }
    }
}
if($rtn === 0){
    //必須チェック
    if($LICNUM === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('解約キー'));
    }
}
//バリデーションチェック
if($rtn === 0){
    if(!checkMaxLen($LICNUM,14)){//190325
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('解約キー'));
    }
}
if($rtn === 0){
    $byte = checkByte($LICNUM);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('解約キー'));
    }
}
if ($rtn === 0) {
   if(strlen($LICNUM) != strlen(utf8_decode($LICNUM))){
        $rtn   = 1;
        $msg   = showMsg('FAIL_KANA',array('解約キー'));
    }
}
if ($rtn === 0) {
   if(strlen($LICNUM) !== 14){//190308
        $rtn   = 1;
        $msg   = showMsg('FAIL_CMP',array('解約キー'));
    }
}
if ($rtn === 0) {
    if($licenseVersion < 410){
            $rtn   = 1;
            $msg   = showMsg('FAIL_CMP',array('解約キー'));
    }
}
if($rtn === 0){
    $dcryptserialKey = decryptSerialCancelKey($LICNUM);
    if($dcryptserialKey === $LICSER){
        $licenseType = $licenseType1;
       // $keychk = encryptCancelKey($LICSER);
        $rtn = 0;
    }else{
        $rtn = 1;
        $msg = showMsg('解約手続きは失敗しました。',array('解約キー'));
    }
}

if($rtn === 0){
    $LICKEY = date('YmdHis');
    $LICFLG = 1;
    $license = array($LICKEY,$LICSER,'',$LICFLG);
    $rs = fnInsertDB2LIC($db2con,$license);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg('解約手続きは失敗しました。',array('解約キー'));
    }
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'LIC'=>$LICSER,
    'dcryptserialKey '=>$dcryptserialKey,
    'SERIALK_CONTROL' =>$SERIALK_CONTROL,
    'licenseType'=>$licenseType
  //  'keychk'=>$keychk
);

echo(json_encode($rtnAry));



/***
*ライセンス登録
***/
function fnInsertDB2LIC($db2con,$license){
    $rs = true;
    $params = $license;
    $strSQL = 'INSERT INTO DB2LICE ';
    $strSQL .='(';
    $strSQL .='LICKEY,';
    $strSQL .='LICSER,';
    $strSQL .='LICNUM,';
    $strSQL .='LICFLG';
    $strSQL .=')';
    $strSQL .='VALUES';
    $strSQL .='(?,?,?,?)';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}
 
