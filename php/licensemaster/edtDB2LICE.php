<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/

/*include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseUpdateOperate_4_1_0.php");
include_once("../licenseUpdateRandom_4_1_0.php");
$SERIALK_CONTROL = '';
*/
include_once("../licenseInfo.php");

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$LICNUM = (isset($_POST['LICNUM']))?$_POST['LICNUM']:'';
$licenseVersion = (isset($_POST['licenseVersion']))?$_POST['licenseVersion']:'';
$licenseVersion = (int)$licenseVersion;
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

$LICNUM = cmHscDe($LICNUM);
$LICSER = '';
//ライセンス＝＞1の場合はシリアルキーなし、ブラクの場合はシリアルキーあり
if($SERIALK_CONTROL === ''){
    $LICSER = cmGETSRLNBR($db2con);
    $LICSER = (isset($LICSER))?trim($LICSER):'';
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('ライセンス情報の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'29',$userData[0]['WUSAUT']);//'1' => LicenseMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライセンス情報の権限'));
            }
        }
    }
}
if($rtn === 0){
    //必須チェック
    if($LICNUM === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('ライセンスキー'));
    }
}
//バリデーションチェック
if($rtn === 0){
    if(!checkMaxLen($LICNUM,100)){//190325
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('ライセンスキー'));
    }
}
if($rtn === 0){
    $byte = checkByte($LICNUM);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('ライセンスキー'));
    }
}
if ($rtn === 0) {
   if(strlen($LICNUM) != strlen(utf8_decode($LICNUM))){
        $rtn   = 1;
        $msg   = showMsg('FAIL_KANA',array('ライセンスキー'));
    }
}
if ($rtn === 0) {
   if(strlen($LICNUM) !== 100){//190308
        $rtn   = 1;
        $msg   = showMsg('FAIL_CMP',array('ライセンスキー'));
    }
}
if ($rtn === 0) {
    if($licenseVersion < 410){
            $rtn   = 1;
            $msg   = showMsg('FAIL_CMP',array('ライセンスキー'));
    }
}
if($rtn === 0){
    $licenseUpdateInfo = decryptOptionKey($LICNUM,$LICSER);
    if($licenseUpdateInfo === false){

        $rtn = 1;
    }else{
                $tSerial = $licenseUpdateInfo[2]['licenseSerial'] !== '0000000' ? $licenseUpdateInfo[2]['licenseSerial'] : '';
                $pSerial = $licenseUpdateInfo[1]['licenseSerial'] !== '0000000' ? $licenseUpdateInfo[1]['licenseSerial'] : '';
                $tVersion = (int)$licenseUpdateInfo[2]['licenseVersion'];
                $pVersion = (int)$licenseUpdateInfo[1]['licenseVersion'];
                 if($LICSER != $pSerial || $LICSER !=  $tSerial){
                    $rtn   = 1;
                }
                if ($rtn === 0) {
                    if($licenseVersion < 410){
                            $rtn   = 1;
                            $msg   = showMsg('FAIL_CMP',array('ライセンスキー'));
                    }
                }
                if ($rtn === 0) {
                    if($tVersion < 410){
                            $rtn   = 1;
                            $msg   = showMsg('FAIL_CMP',array('ライセンスキー'));
                    }
                }
                if ($rtn === 0) {
                    if($pVersion < 410){
                            $rtn   = 1;
                            $msg   = showMsg('FAIL_CMP',array('ライセンスキー'));
                    }
                }
                if ($rtn === 0) {
                    if($tVersion !== $tVersion){
                            $rtn   = 1;
                            $msg   = showMsg('FAIL_CMP',array('ライセンスキー'));
                    }
                }
                if($rtn === 0){
                        foreach($licenseUpdateInfo as $key => $value){
                            $licenseCl  = $licenseUpdateInfo[$key]['licenseCl']; 
                            if($licenseCl === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseSchedule  = $licenseUpdateInfo[$key]['licenseSchedule'];
                            if($licenseSchedule === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseScheduleBtn  = $licenseUpdateInfo[$key]['licenseScheduleBtn'];
                            if($licenseScheduleBtn === false){
                                $rtn = 1;
                                $break;
                            }
                            $licensePivot  = $licenseUpdateInfo[$key]['licensePivot'];
                            if($licensePivot === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseGraph  = $licenseUpdateInfo[$key]['licenseGraph'];
                            if($licenseGraph === false){
                                $rtn = 1;
                                $break;
                            }
                            $licensePivotCreate  = $licenseUpdateInfo[$key]['licensePivotCreate'];
                            if($licensePivotCreate === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseExcelcsvDownload  = $licenseUpdateInfo[$key]['licenseExcelcsvDownload'];
                            if($licenseExcelcsvDownload === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseShow  = $licenseUpdateInfo[$key]['licenseShow'];
                            if($licenseShow === false){
                                $rtn = 1;
                                $break;
                            }
                            /***20170522***/
                            $licenseLog  = $licenseUpdateInfo[$key]['licenseLog'];
                            if($licenseLog === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseTemplate  = $licenseUpdateInfo[$key]['licenseTemplate'];
                            if($licenseTemplate === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseIFS  = $licenseUpdateInfo[$key]['licenseIFS'];
                            if($licenseIFS === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseDrilldown  = $licenseUpdateInfo[$key]['licenseDrilldown'];
                            if($licenseDrilldown === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseGraph  = $licenseUpdateInfo[$key]['licenseGraph'];
                            if($licenseGraph === false){
                                $rtn = 1;
                                $break;
                            }
                            //190325　制御とクエリーグループの追加
                            $licenseSeigyo  = $licenseUpdateInfo[$key]['licenseSeigyo'];
                            if($licenseSeigyo === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseQueryGroup  = $licenseUpdateInfo[$key]['licenseQueryGroup'];
                            if($licenseQueryGroup === false){
                                $rtn = 1;
                                $break;
                            }//190325
                            //　sqlクエリーとファイル出力の追加
                            $licenseSql  = $licenseUpdateInfo[$key]['licenseSql'];
                            if($licenseSql === false){
                                $rtn = 1;
                                $break;
                            }
                            $licenseFileOut  = $licenseUpdateInfo[$key]['licenseFileOut'];
                            if($licenseFileOut === false){
                                $rtn = 1;
                                $break;
                            }//190516
                            /***20170522***/
                        }
                }
    }
    if($rtn === 1){
        error_log('FAIL*************');
        $msg = showMsg('FAIL_CMP',array('ライセンスキー'));
    }
}

if($rtn === 0){
        $LICKEY = date('YmdHis');
        $LICFLG = 1;
        $license = array($LICKEY,$LICSER,$LICNUM,$LICFLG);
        $rs = fnInsertDB2LIC($db2con,$license);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'LIC'=>$LICSER,
    'pSerial' =>$pSerial,
    'tSerial' =>$tSerial,
    'SERIALK_CONTROL' =>$SERIALK_CONTROL,
    'licenseUpdateInfo' =>$licenseUpdateInfo
);

echo(json_encode($rtnAry));


/***
*ライセンス登録
***/
function fnInsertDB2LIC($db2con,$license){
    $rs = true;
    $params = $license;
    $strSQL = 'INSERT INTO DB2LICE ';
    $strSQL .='(';
    $strSQL .='LICKEY,';
    $strSQL .='LICSER,';
    $strSQL .='LICNUM,';
    $strSQL .='LICFLG';
    $strSQL .=')';
    $strSQL .='VALUES';
    $strSQL .='(?,?,?,?)';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}