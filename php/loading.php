<html>
<head>
<meta charset="UTF-8">
<title>PHP QUERY</title>
<script type="text/javascript" src="../common/lib/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="../common/lib/blockui.js"></script>
<link rel="stylesheet" href="../common/css/common.css" type="text/css"/>

<style type="text/css">
.loadingmsg{
    margin-top:15px;
}
</style>

<script type="text/javascript">

var loadconf = {
    message:'<img src="../common/images/loading.gif"/><div class="loadingmsg">データを作成しています。</div>',
    css:{backgroundColor:'transparent'}
}

$(function(){
    $.blockUI(loadconf);
});
</script>

</head>
<body>
</body>
</html>