<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");
/*
 *-------------------------------------------------------* 
 * リクエスト
 *-------------------------------------------------------*
 */
$touch         = (isset($_POST['TOUCH']) ? $_POST['TOUCH'] : '0');
$AXESURL       = (isset($_POST['AXESURL']) ? $_POST['AXESURL'] : '');
$user          = $_POST['USER'];
$pass          = $_POST['PASS'];
$lang          = (isset($_POST['LANG']) ? $_POST['LANG'] : '');
//$clflg = $_POST['CLFLG'];
//$scheduleflg = $_POST['SCHEDULEFLG'];
$clflg         = true;
$scheduleflg   = true;
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$rtnArray      = array();
$rtn           = 0;
$msg           = '';
$wuedayRes     = '';
$url           = '';
$check         = '';
$usrinfo       = array();
$checkCount    = '';
$checkCountArr = array();
$loginCount    = 0;
$rs            = true;

$ChkActiveUserArr = array();
//事前処理登録権限チェック
if ($licenseCl == false) {
    $clflg = false;
}

//スケジュール自動実行権限チェック
if ($licenseSchedule == false) {
    $scheduleflg = false;
}
$_SESSION['LANGCD']  = $lang;
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */
if ($AXESURL === '1') {
    $axespar = $_POST['AXESPARAM'];
    if (AXESPARAM !== $axespar) {
        $rtn = 1;
        if (AXESPARAM === 'HASH') {
            $directlogin = 'directlogin/ユーザー名/パスワード';
        } else {
            $directlogin = 'directlogin';
        }
        $msg = showMsg('LOGIN_AXESURL',array($directlogin));//"パラメータに誤りがあります。<br/>" . $directlogin . "形で渡してください。";
    }
}
if($rtn === 0){
    $byte = checkByte($user);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_LGN');
    }
}
if($rtn === 0){
    $byte = checkByte($pass);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_LGN');
    }
}
if ($rtn === 0) {
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
    $today     = '';
    $diff      = 0;
    $result    = '';
    $wueday    = '';
    $wuedayArr = array();
    $today     = date('Y-m-d');
    
    //バリデーションチェック
    $wuedayRes = fnCheckWUEDAY($db2con, $user);
    if ($wuedayRes['result'] === true) {
        $wuedayArr = $wuedayRes['data'];
        $wueday    = $wuedayArr[0]['WUEDAY'];
    } else {
        $rtn = 1;
        $msg = showMsg($wuedayRes['result']);
    }
    $result = date('Y-m-d', strtotime($wueday));
    
    if ($wueday !== "0") {
        $diff = (strtotime($today) - strtotime($result)) / 24 / 3600;
    }
    //MSM chk for Active User or not COASVR,COAFLG, COAHOS, COADO
    $ChkActiveUser = fnCHECKactive($db2con);
    $ChkActiveUserArr = umEx($ChkActiveUser['data']);
    if($ChkActiveUserArr[0]['COAFLG'] !== ""){
        $chkUser = "Active_User";
    }else{
        $chkUser = "Normal_User";
    }
//
    $usrinfo = LoginDB2WUSR($db2con, $user, $pass,$chkUser);

    if ($usrinfo['result'] === true) {
        $usrinfo['data'] = umEx($usrinfo['data']);
        if (count($usrinfo['data']) === 0) {
            
            $checkCount    = fnCHECKWULGNC($db2con, $user);
            $checkCountArr = $checkCount['data'];
            $chkLgcount = false;
            if($checkCountArr[0]['WUAUTH'] === '2'){//パワーユーザー
                $rs = cmChkKenGen($db2con,'1',$checkCountArr[0]['WUSAUT']);//'1' => userMaster
                if($rs['result'] !== true){
                     $chkLgcount = true;//ユーザー作成権限がありません。
                 }
            }
            //一般ユーザーかつLOGINCOUNTがブランク以外の時にログインカウントチェック
            if (($checkCountArr[0]['WUAUTH'] === '0' && LOGINCOUNT !== '') || $chkLgcount === true) {
                $loginCount = (int) $checkCountArr[0]['WULGNC'];
                if ($loginCount < (int) LOGINCOUNT) {
                    $rtn = 1;
                    $msg = showMsg('FAIL_LGN');
                    
                    $loginCount++;
                    $rs = fnUpdWULGNC($db2con, $user, $loginCount);
                    if ($rs === false) {
                        $rtn = 1;
                        $msg = showMsg($rs);
                    }
                } else {
                    $rtn = 1;
                    $msg = showMsg('ACC_LCK');
                }
                
            } else {
                $rtn = 1;
                $msg = showMsg('FAIL_LGN');
            }
            
            
        }else {
            //MSM add 
            if($chkUser === "Active_User"){
                //,$WUAID,$WUADO,$WUAFLG
                $Active_UserId      = $usrinfo['data'][0]["WUAID"];
                $Active_UserFlg     = $usrinfo['data'][0]["WUAFLG"];
                $ldaphost           = $ChkActiveUserArr[0]['COASVR']; //"192.168.1.12" / ldap サーバー　 / COASVR
                $ldapport           = $ChkActiveUserArr[0]['COAHOS']; //389 / ldap サーバーのポート番号 /COAHOS
                
                if($Active_UserFlg == 1){//get ドメイン(@omni-s.local) from user 
                    $Active_UserDomain  = $usrinfo['data'][0]["WUADO"];
                }else{//get ドメイン(@omni-s.local) from system
                    $Active_UserDomain  = $ChkActiveUserArr[0]['COADO'];
                }
                if($Active_UserId !== "" && $Active_UserDomain !== ""){
                    $user_domain = $Active_UserId.$Active_UserDomain;
                    //e_log("ACTIVE user_domain => ".$user_domain);  
                }else{
                    $rtn = 1;
                    $msg = showMsg('ACTIVE_IDFAIL');
                }
                
                if($ldaphost  !== "" && $ldapport != ""){
                    $Active_chk = ldap_conn($ldaphost,$ldapport,$user_domain,$pass);
                    
                    if ($Active_chk['result'] === true) {
                        $user_flg = "A";
                    }else{
                        //e_log("ACTIVE ldap_conn error => ".$Active_chk['msg']);
                        $rtn = 1;
                        $msg = showMsg('ACTIVE_LGNFAIL');
                        //clean_all_processes();  
                        //break;
                    }
                }else{
                    $rtn = 1;
                    $msg = showMsg('ACTIVE_LGNFAIL');
                    //clean_all_processes();  
                    //break;
                }
                    
            }else{
                //normal user
                $user_flg = "N";  
                //e_log("MSM USER CHECK 2=> ".$chkUser."===>".$user_flg);
            }//MSM end
            
            //LOGINDAYがブランク以外の時に有効期限をチェック
            if(LOGINDAY !== '' && ($diff > (int) LOGINDAY)) {
                $rtn = 2;
                $msg = showMsg('FAIL_EXP', array(
                    'パスワード'
                ));
            }else{
                $checkCount    = fnCHECKWULGNC($db2con, $user);
                $checkCountArr = $checkCount['data'];
                if ($checkCount['result'] === true) {
                    if (LOGINCOUNT !== '' && ((int) $checkCountArr[0]['WULGNC'] === (int) LOGINCOUNT)) {
                        $rtn = 1;
                        $msg = showMsg('ACC_LCK');
                    } else {
                        //実行ログ
                        $rs = cmInsertDB2WLOG($db2con, $rs, $user, 'L', '',array(),'','');
                        cmInsertDB2WSTL($db2con, $rs['DATE'],$rs['TIME'],$user,'0','0');//mppn add log for login
                        
                        if ($touch == '1') {
                            $_SESSION['PHPQUERYTOUCH']['user']        = $usrinfo['data'];
                            //$_SESSION['PHPQUERYTOUCH']['luser']       = $user;
                            $_SESSION['PHPQUERYTOUCH']['LOGIN']       = '1';
                            $_SESSION['PHPQUERYTOUCH']['ClFlg']       = $clflg;
                            $_SESSION['PHPQUERYTOUCH']['ScheduleFlg'] = $scheduleflg;
                            $_SESSION['PHPQUERYTOUCH']['BASE_URL']    = BASE_URL;
                            
                            if (!isset($_SESSION['PHPQUERYTOUCH']['lasthash'])) {
                                $_SESSION['PHPQUERYTOUCH']['lasthash'] = '';
                            }
                            
                        } else {
                            $_SESSION['PHPQUERY']['user']        = $usrinfo['data'];
                            //$_SESSION['PHPQUERY']['luser']       = $user;
                            $_SESSION['PHPQUERY']['LOGIN']       = '1';
                            $_SESSION['PHPQUERY']['ClFlg']       = $clflg;
                            $_SESSION['PHPQUERY']['ScheduleFlg'] = $scheduleflg;
                            $_SESSION['PHPQUERY']['BASE_URL']    = BASE_URL;
                            
                            if (!isset($_SESSION['PHPQUERY']['lasthash'])) {
                                $_SESSION['PHPQUERY']['lasthash'] = '';
                            }
                        }
                        if ($checkCountArr[0]['WUAUTH'] === '0') {
                            $loginCount = 0;
                            $rs         = fnUpdWULGNC($db2con, $user, $loginCount);
                            if ($rs === false) {
                                $rtn = 1;
                                $msg = showMsg($rs);
                            }
                        }
                    }
                } else {
                    $rtn = 1;
                    $msg = showMsg($usrinfo['result']);
                }
            }
        }
        
    } else {
        $rtn = 1;
        $msg = showMsg($usrinfo['result']);
    }
}
/*if($rtn === 0){
    // ログイン成功
    // 言語UPDATE
    $rs = fnUpWULANG($db2con,$user,$lang);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}*/

cmDb2Close($db2con);


/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'URL' => $url,
    'USR' => $usrinfo['data'],
    'licenseText' => $licenseText,
    'session' => $_SESSION
);
echo (json_encode($rtnArray));


/*
 *-------------------------------------------------------* 
 * ログインチェック
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * @param String  $user  ユーザーID
 * @param String  $pass パスワード
 * 
 *-------------------------------------------------------*
 */

function LoginDB2WUSR($db2con, $user, $pass,$chkUser) {
    
    $data = array();
    
    $sql   = 'SET ENCRYPTION PASSWORD = \'' . RDB_KEY . '\'';
    $stmt1 = db2_prepare($db2con, $sql);
    if ($stmt1 === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt1);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            $strSQL = ' SELECT A.WUUID,A.WUUNAM,A.WUAUTH,A.WUDWNL,A.WUDWN2,A.WUSIZE,A.WUSAUT,A.WUAID,A.WUADO,A.WUAFLG FROM DB2WUSR AS A '; //MSM add A.WUAID,A.WUADO,A.WUAFLG for Active user
            if($chkUser === "Active_User"){
                $strSQL .= ' WHERE WUAID = ? ';
                $params = array(
                    $user
                );
            }else{
                $strSQL .= ' WHERE WUUID = ? ';
                $strSQL .= ' AND DECRYPT_CHAR(WUPSWE) = ? ';
                $params = array(
                    $user,
                    $pass
                );
            }

            //e_log('ACTIVE chkUser MSM=> '.$chkUser);
            //e_log('ACTIVE SQL LoginDB2WUSR MSM'.$strSQL.print_r($params,true));
            
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $data = array(
                    'result' => 'FAIL_SEL'
                );
            } else {
                $r = db2_execute($stmt, $params);
                
                if ($r === false) {
                    $data = array(
                        'result' => 'FAIL_SEL'
                    );
                } else {
                    while ($row = db2_fetch_assoc($stmt)) {
                        $data[] = $row;
                    }
                    $data = array(
                        'result' => true,
                        'data' => $data
                    );
                }
                
            }
            
        }
    }
    
    return $data;
    
}


/*
 *-------------------------------------------------------* 
 * 前ログイン日の時間をチェックすること
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * @param String  $WUIID  ユーザーID
 * 
 *-------------------------------------------------------*
 */
function fnCheckWUEDAY($db2con, $user) {
    $data = array();
    
    $strSQL = ' SELECT WUEDAY ';
    $strSQL .= ' FROM DB2WUSR ';
    $strSQL .= ' WHERE WUUID = ? ';
    
    $params = array(
        $user
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    
    
    return $data;
    
}
/*
 *-------------------------------------------------------* 
 * DB2WUSRのWULGNC COLUMN データ取得
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * 
 *-------------------------------------------------------**/

function fnCHECKWULGNC($db2con, $user) {
    $data = array();
    
    //構文
    $strSQL = ' SELECT WULGNC, WUAUTH, WUSAUT';
    $strSQL .= ' FROM DB2WUSR ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUUID = ? ';
    
    $params = array(
        $user
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    
    return $data;
    
}


/*
 *-------------------------------------------------------* 
 * DB2WUSRのWULGNC COLUMN データ更新
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * 
 *-------------------------------------------------------**/

function fnUpdWULGNC($db2con, $user, $loginCount) {
    
    $rs     = true;
    //構文
    $strSQL = ' UPDATE DB2WUSR ';
    $strSQL .= ' SET WULGNC = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUUID = ? ';
    $params = array(
        $loginCount,
        $user
    );
    
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_UPD';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_UPD';
        }
    }
    
    return $rs;
    
}

function  fnUpWULANG($db2con,$user,$lang){
    $rs = true;
    $strSQL  = ' UPDATE DB2WUSR ';
    $strSQL .= ' SET WULANG = ?';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUUID = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_UPD';
    } else {
        if($lang === '001'){
            $lang = '';
        }
        $params = array($lang,$user);
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}

//MSM add for 通常 OR ActiveDirectory
function fnCHECKactive($db2con) {
    $data = array();
    
    //構文
    $strSQL = ' SELECT COASVR,COAFLG, COAHOS, COADO';
    $strSQL .= ' FROM DB2COF ';
     
    $params = array();
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    
    return $data;
    
}
//MSM add for Active user check
function ldap_conn($ldaphost,$ldapport,$ldap_dn,$ldap_pass){
     //$ldaphost = "192.168.1.12";  // ldap サーバー　 / COASVR 
     //$ldapport = 389;                 // ldap サーバーのポート番号 /COAHOS

     // LDAP に接続します
     //$ldap_dn = "S100056@omni-s.local"; 
     //$ldap_pass = "s-kabe0000";

     $ldapconn = ldap_connect($ldaphost, $ldapport)
               or die("Could not connect to $ldaphost");
                 
     /*e_log("ACTIVE ldaphost => ".$ldaphost);  
     e_log("ACTIVE ldapport => ".$ldapport); 
     e_log("ACTIVE ldap_dn => ".$ldap_dn);  
     e_log("ACTIVE ldap_pass => ".$ldap_pass);   */
     if($ldapconn){//ユーザー　＝＞　S050014　、ドメイン　＝＞　@omni-s.local
          $ldapbind = ldap_bind($ldapconn, $ldap_dn, $ldap_pass);
          if($ldapbind){
               $e_msg= 'LDAP bind succeeded';
               $data = array(
                   'result' => true,
                   'msg' => $msg
               );  

          }
          else{
               $msg= 'LDAP bind failed';
               $e_no     = ldap_errno($ldapconn);
               $e_msg    = ldap_err2str(ldap_errno($ldapconn));
               $error    = "ldap_errorno is ".$e_no."and ldap_errormsg is ".$e_msg;
               $msg      = $msg."<br />".$error;
               $data = array(
                   'result' => false,
                   'msg' => $msg
               );  
          }
     }
     else{
          $msg= 'LDAP connect failed.';
          $data = array(
              'result' => false,
              'msg' => $msg
          );     
     }

     return $data;
}
