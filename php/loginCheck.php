<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once('common/inc/config.php');
include_once('common/inc/common.inc.php');
include_once('licenseInfo.php');
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/
$touch = (isset($_POST['TOUCH'])?$_POST['TOUCH']:'0');
$lasthash = (isset($_POST['lasthash'])?$_POST['lasthash']:'');
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$login = '';
$usrinfo = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$msg = '';
if($touch == '1'){

    $login = $_SESSION['PHPQUERYTOUCH']['LOGIN'];
    $userid = $_SESSION['PHPQUERYTOUCH']['user'][0]['WUUID'];
    $usrinfo = regetDB2WUSR($db2con,$userid);
    //ユーザーが削除されていた場合、ログアウト状態にする(ログイン状態の場合のみチェック)
    if($login === '1'){
        if(count($usrinfo) === 0){
            $login = '';
             $msg = showMsg('NOTEXIST_GET',array('ユーザー'));
         }else{
            $usrinfo = umEx($usrinfo);
            $_SESSION['PHPQUERYTOUCH']['user'] = $usrinfo;
        }
    }

    //現在のHASHを保存
    if($lasthash !== ''){
        $_SESSION['PHPQUERYTOUCH']['lasthash'] = $lasthash;
    }

}else{

    $login = $_SESSION['PHPQUERY']['LOGIN'];
    $userid = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
    $usrinfo = regetDB2WUSR($db2con,$userid);

    //ユーザーが削除されていた場合、ログアウト状態にする(ログイン状態の場合のみチェック)
    if($login === '1'){
        if(count($usrinfo) === 0){
            $login = '';
            $msg = showMsg('NOTEXIST_GET',array('ユーザー'));
        }else{
            $usrinfo = umEx($usrinfo);
            $_SESSION['PHPQUERY']['user'] = $usrinfo;
        }
    }

    //現在のHASHを保存
    if($lasthash !== ''){
        $_SESSION['PHPQUERY']['lasthash'] = $lasthash;
    }

}


/**return**/
$rtnArray = array(
    'LOGIN' => $login,
    'USR' => $usrinfo,
    'MSG' =>$msg,
    'licenseText' =>$licenseText,
    'AXESPARAM' => AXESPARAM,
    'SESSION' =>$_SESSION
);
echo(json_encode($rtnArray));

/*
*-------------------------------------------------------* 
* ユーザー情報取得
*-------------------------------------------------------*
*/

function regetDB2WUSR($db2con,$user){

    $data = array();

    $strSQL  = ' SELECT A.WUUID,A.WUUNAM,A.WUAUTH,A.WUDWNL,A.WUDWN2,A.WUSIZE,A.WUSAUT FROM DB2WUSR AS A ';
    $strSQL .= ' WHERE WUUID = ? ' ;
    $params = array(
        $user
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }

    }

    return $data;

}