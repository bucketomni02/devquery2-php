<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 1;
            $msg =  showMsg('NOTEXIST',array(array('実行ローグ全て消去','権限')));
        }else if ($userData[0]['WUAUTH'] === '2'){          
            $rs = cmChkKenGen($db2con,'23',$userData[0]['WUSAUT']);//'1' => logMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('実行ログの権限'));
            }
        }
    }
}
if($rtn === 0){
    $rs = fnDeleteDB2WLOG($db2con);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ログ削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WLOG($db2con){

    $rs = true;

    //構文

    $strSQL  = ' DELETE FROM DB2WLOG ';

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
        else{
            //構文
            $strSQL  = ' DELETE FROM DB2WSDA ';

            $params = array();

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $rs = 'FAIL_DEL';
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $rs = 'FAIL_DEL';
                }else{
                    //構文
                    $strSQL  = ' DELETE FROM DB2WSDK ';

                    $params = array();

                    $stmt = db2_prepare($db2con,$strSQL);
                    if($stmt === false){
                        $rs = 'FAIL_DEL';
                    }else{
                        $r = db2_execute($stmt,$params);
                        if($r === false){
                            $rs = 'FAIL_DEL';
                        }

                    }
                }

            }
        }

    }
    return $rs;
}

