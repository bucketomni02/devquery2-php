<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$DATA = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$check = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array(array('実行ログ連携','保険設定','権限')));
        }else if ($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'23',$userData[0]['WUSAUT']);//'1' => logMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('実行ログ連携'));
            }
        }
    }
}

//バリデーションチェック
if($rtn === 0){
    $check = checkNum($DATA['wcval']);
    if($check === false){
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array('保管日数','数字'));
        $focus = 'wcval';
    }
}

if($rtn === 0){
    if(!checkMaxLen($DATA['wcval'],6)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('保管日数'));
        $focus = 'wcval';
    }
}

if($rtn === 0){
    if(checkNuturalNum($DATA['wcval']) === false){
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array('保管日数','数値'));
        $focus = 'wcval';
    }
}


/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/


if($rtn === 0){

    $rs = fnUpdateWCVAL($db2con,cmStr0($DATA['wcval']));

    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }else{
        //登録された内容でログ消去
        $hokn = cmStr0($DATA['wcval']);
        $rs = fnDeleteLog($db2con,$hokn);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => $DATA
);

echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* 保管日数更新
*-------------------------------------------------------*
*/

function fnUpdateWCVAL($db2con,$WCVAL){

    $rs = true;

    //構文

    $strSQL  = ' UPDATE DB2WCPN ';
    $strSQL .= ' SET ';
    $strSQL .= ' WCVAL = ? ';
    $strSQL .= ' WHERE WCKEY = ? ';

    $params = array(
        $WCVAL,
        '001'
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    } 

    return $rs;

}

/*
*-------------------------------------------------------* 
* 渡された日数以前のログを消去
*-------------------------------------------------------*
*/

function fnDeleteLog($db2con,$HOKN){

    $rs = true;

    //0の場合はなにもしない
    if($HOKN !== '0'){

        //今日の日付より$HOKNの日数分さかのぼった日付を取得
        $befdate = date("Ymd", strtotime(date('Ymd')." -".$HOKN." day"));

        //$befdate以前のログを消去

        $strSQL  = ' DELETE FROM DB2WLOG ';
        $strSQL .= ' WHERE WLDAY < ? ';

        $params = array(
            $befdate
        );

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_DEL';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_DEL';
            }
        }
    }
    return $rs;

}