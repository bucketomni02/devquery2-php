<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$SDAY = $_POST['SDAY'];
$STIME = $_POST['STIME'];
$SUID = $_POST['SUID'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

if($rtn === 0){
    $rs = fnGetDB2SQL($db2con,$SDAY,$STIME,$SUID);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
    if(count($data) === 0){
        $rtn = 3;
        $msg = showMsg('DEL_FUNC',array('ログ情報'));//'ログ情報が削除されました。';
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'DATA' => umEx($data,true),
    'RTN'=> $rtn,
    'MSG'=> $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 実行SQL情報取得
*-------------------------------------------------------*
*/

function fnGetDB2SQL($db2con,$SDAY,$STIME,$SUID){

    $data = array();

    $params = array();


   /* $strSQL  = ' SELECT ';
   /* $strSQL .= '     A.S1DAY , ';
    $strSQL .= '     A.S1TIME , ';
    $strSQL .= '     A.S1UID , ';
    $strSQL .= '     A.S1JSQL , ';
    $strSQL .= '     CASE ';
    $strSQL .= '     WHEN B.S2JSQL<>\'\' THEN 1 ';
    $strSQL .= '     ELSE 0 ';
    $strSQL .= '     END AS SQL2FLG '; 
    $strSQL .= '     A.S1JSQL ';
    $strSQL .= ' FROM DB2SQL1 A ';
    $strSQL .= ' LEFT JOIN DB2SQL2 B ';
    $strSQL .= ' ON  A.S1DAY  = B.S2DAY ';
    $strSQL .= ' AND A.S1TIME = B.S2TIME ';
    $strSQL .= ' AND A.S1UID  = B.S2UID ';
    $strSQL .= ' WHERE A.S1DAY = ? ';
    $strSQL .= ' AND   A.S1TIME = ? ';
    $strSQL .= ' AND   A.S1UID  = ? ';*/
    $strSQL = ' SELECT ';
    $strSQL .= '     A.S1JSQL  ';
    $strSQL .= ' FROM DB2SQL1 A ';
    $strSQL .= ' WHERE A.S1DAY = ? ';
    $strSQL .= ' AND   A.S1TIME = ? ';
    $strSQL .= ' AND   A.S1UID  = ? ';
    $params = array($SDAY,$STIME,$SUID);
    e_log($strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        e_log(db2_cursor_type ($stmt ));
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            if(db2_fetch_assoc($stmt) === false){
                e_log('【エラーコード：'. db2_stmt_errormsg().'】');
           // }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                    e_log('AAA'.print_r($row,true));
                    $data = array('result' => true,'data' => $data);
                }
            }
        }
    }

    return $data;

}