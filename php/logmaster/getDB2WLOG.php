<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../getQryCnd.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$SDAY = $_POST['SDAY'];
$EDAY = $_POST['EDAY'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'23',$userData[0]['WUSAUT']);//'1' => logMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('実行ログの権限'));
            }
        }
    }
}

if($rtn === 0){
    $rs = fnGetAllCount($db2con,$WUAUTH,$SDAY,$EDAY);
    if($rs['result'] !== true){
        $msg =showMsg($rs['result']);
        $rtn = 1;
    }else{
        $allcount = $rs['data'];
    }
}

if($rtn === 0){//MSM add for chk
    $ChkActiveUser = fnCHECKactive($db2con);
    $ChkActiveUserArr = umEx($ChkActiveUser['data']);
    if($ChkActiveUserArr[0]['COAFLG'] !== ""){
        $chkUser = "Active_User";
    }else{
        $chkUser = "Normal_User";
    }

    $rs = fnGetDB2WLOG($db2con,$WUAUTH,$SDAY,$EDAY,$chkUser,$start,$length,$sort,$sortDir);
    if($rs['result'] !== true){
        $msg =showMsg($rs['result']);
        $rtn = 1;
    }else{
        $data = ($rs['data']);
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'USRAUTH' => $userData[0]['WUAUTH'],
    'chkUser' => $chkUser,
    'RTN'=> $rtn,
    'MSG'=> $msg,
    'ZERO_FLG'=> ZERO_FLG,
    'BACKZERO_FLG'=> BACKZERO_FLG
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2WLOG($db2con,$WUAUTH,$SDAY,$EDAY,$chkUser,$start = '',$length = '',$sort = '',$sortDir = ''){//MSM add new parameter for chk
    $data = array();
    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( ';
    $strSQL .= '    SELECT B.* , ';
	$strSQL .= '	C.D1TEXT,';
//    $strSQL .= '    C.D1WEBF, ';
    $strSQL .= '    CASE WHEN B.WLKBN=\'P\' THEN \'1\' ELSE C.D1WEBF END D1WEBF, ';
    $strSQL .= '    ROWNUMBER() OVER( ';

    if($sort !== ''){
        if($sort === 'D1TEXT'){
            $strSQL .= '        ORDER BY C.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= '        ORDER BY B.'.$sort.' '.$sortDir.' ';
        }
    }else{
        $strSQL .= '        ORDER BY B.WLDAY DESC,WLTIME DESC ';
    }
    $strSQL .= '    ) as rownum ';
    $strSQL .= '    FROM ( ';
    $strSQL .= '        SELECT ';
    $strSQL .= '            E.WUUNAM, ';
    $strSQL .= '            D.WLDAY,D.WLTIME,';
    $strSQL .= '            F.WLJKTM,';//実行時間
    $strSQL .= '            F.WLQCNT,';//件数
    $strSQL .= '            F.WLIPBI,';//mppn
    if($chkUser ==='Normal_User'){//MSM add for chk 
        $strSQL .= '        D.WLUID,D.WLUID AS WUID,';
    }else if($chkUser ==='Active_User'){
        $strSQL .= '        E.WUAID AS WLUID,D.WLUID AS WUID,';
    }
    $strSQL .= '            D.WLKBN,WLNAME,LENGTH(RTRIM(D.WLSQLB)) AS WLSQLB ';
    $strSQL .= '        FROM ';
    $strSQL .= '            DB2WLOG AS D ';
    $strSQL .= '        LEFT JOIN DB2WUSR AS E ';
    $strSQL .= '        ON D.WLUID = E.WUUID  OR D.WLUID = E.WUAID';//MSM add new condition for active user
    $strSQL .= '        LEFT JOIN DB2WSTL AS F'; //クエリー実行時間と件数取得
    $strSQL .= '        ON        D.WLDAY  = F.WLDAY';
    $strSQL .= '        AND       D.WLTIME = F.WLTIME ';
    $strSQL .= '        AND       D.WLUID  = F.WLUID';
    
    $strSQL .= '    ) AS B ';
    $strSQL .= '    LEFT JOIN FDB2CSV1 AS C ';
    $strSQL .= '    ON B.WLNAME = C.D1NAME ';

    $strSQL .= '    WHERE  WLDAY <> 0 ';
    if($WUAUTH === '3'){
        $strSQL .= '        AND ( ';
        $strSQL .= '        WLUID IN ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                DISTINCT WUUID ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2WUGR ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                WUGID IN ( ';
        $strSQL .= '                    SELECT ';
        $strSQL .= '                        WUGID ';
        $strSQL .= '                    FROM ';
        $strSQL .= '                        DB2WUGR ';
        $strSQL .= '                    WHERE ';
        $strSQL .= '                        WUUID = ?';
        $strSQL .= '                ) ';
        $strSQL .= '        ) ';
        $strSQL .= '        AND WLNAME = \'\'';
        $strSQL .= '             OR ';
        $strSQL .= '            WLNAME IN (SELECT ';
        $strSQL .= '                            D1NAME ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            FDB2CSV1 ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            D1NAME IN ( ';
        $strSQL .= '                                ( ';
        $strSQL .= '                                    SELECT DISTINCT ';
        $strSQL .= '                                        WGNAME ';
        $strSQL .= '                                    FROM ';
        $strSQL .= '                                        DB2WGDF ';
        $strSQL .= '                                    WHERE ';
        $strSQL .= '                                        WGGID IN ( ';
        $strSQL .= '                                            SELECT ';
        $strSQL .= '                                                WUGID ';
        $strSQL .= '                                            FROM ';
        $strSQL .= '                                                DB2WUGR ';
        $strSQL .= '                                            WHERE ';
        $strSQL .= '                                                WUUID = ? ';
        $strSQL .= '                                        ) ';
        $strSQL .= '                                ) UNION  ( ';
        $strSQL .= '                                    SELECT ';
        $strSQL .= '                                        DQNAME';
        $strSQL .= '                                    FROM ';
        $strSQL .= '                                        DB2QHIS';
        $strSQL .= '                                    RIGHT JOIN ';
        $strSQL .= '                                        FDB2CSV1 ';
        $strSQL .= '                                    ON DQNAME = D1NAME ';
        $strSQL .= '                                    WHERE ';
        $strSQL .= '                                        DQNAME <> \'\' ';
        $strSQL .= '                                    AND DQCUSR = ?  ';
        $strSQL .= '                                ) ';
        $strSQL .= '                            ) ';
        $strSQL .= '                        ) ';
		$strSQL .= '		OR ';
		$strSQL .= ' 		( WLKBN=\'P\' AND WLNAME<>\'\' AND C.D1NAME IS NULL ) ';//プレビューの場合は保存しない事もあるから、チェックを入れてないです。
        $strSQL .= '                    ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '        AND ( ';
        $strSQL .= '            WLUID = ? OR ';
        $strSQL .= '            WLNAME IN (SELECT ';
        $strSQL .= '                            D1NAME ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            FDB2CSV1 ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            D1NAME IN ( ';
        $strSQL .= '                                    SELECT ';
        $strSQL .= '                                        DQNAME';
        $strSQL .= '                                    FROM ';
        $strSQL .= '                                        DB2QHIS';
        $strSQL .= '                                    RIGHT JOIN ';
        $strSQL .= '                                        FDB2CSV1 ';
        $strSQL .= '                                    ON DQNAME = D1NAME ';
        $strSQL .= '                                    WHERE ';
        $strSQL .= '                                        DQNAME <> \'\' ';
        $strSQL .= '                                    AND DQCUSR = ?  ';
        $strSQL .= '                            ) ';
        $strSQL .= '                        ) ';
		$strSQL .= '			OR ';
		$strSQL .= ' 			( WLKBN=\'P\' AND WLNAME<>\'\' AND C.D1NAME IS NULL ) ';//プレビューの場合は保存しない事もあるから、チェックを入れてないです。
        $strSQL .= '                    ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }    
    if($SDAY != ''){
        $strSQL .= '    AND WLDAY >= ? ';
        array_push($params,str_replace("/","",$SDAY));
    }

    if($EDAY != ''){
        $strSQL .= '    AND WLDAY <= ? ';
        array_push($params,str_replace("/","",$EDAY));
    }

    $strSQL .= '    ORDER BY WLDAY DESC,WLTIME DESC ' ;
    
    $strSQL .= ') as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    e_log('LOGSQLJIKKOUQUERY'.$strSQL.'PARAMS = '.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        //e_log('LOGSQLJIKKOUPARAM'.print_r($params,true));
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            error_log('data array = '.print_r($data[1],true));

            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$WUAUTH,$SDAY = '',$EDAY = ''){
    $data = array();
    $params = array();

    $strSQL .= '    SELECT ';
    $strSQL .= '        COUNT(*) AS COUNT ';
/*    $strSQL .= '    FROM ';
    $strSQL .= '        DB2WLOG ';
*/

    $strSQL .= '    FROM ( ';
    $strSQL .= '        SELECT ';
    $strSQL .= '            E.WUUNAM, ';
    $strSQL .= '            D.WLDAY,D.WLTIME,D.WLUID,D.WLKBN,WLNAME,LENGTH(RTRIM(D.WLSQLB)) AS WLSQLB ';
    $strSQL .= '        FROM ';
    $strSQL .= '            DB2WLOG AS D ';
    $strSQL .= '        LEFT JOIN DB2WUSR AS E ';
    $strSQL .= '        ON D.WLUID = E.WUUID ';
    $strSQL .= '    ) AS B ';
    $strSQL .= '    LEFT JOIN FDB2CSV1 AS C ';
    $strSQL .= '    ON B.WLNAME = C.D1NAME ';

    $strSQL .= '    WHERE WLDAY <> 0 ';
    if($WUAUTH === '3'){
        $strSQL .= '        AND ( ';
        $strSQL .= '        WLUID IN ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                DISTINCT WUUID ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2WUGR ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                WUGID IN ( ';
        $strSQL .= '                    SELECT ';
        $strSQL .= '                        WUGID ';
        $strSQL .= '                    FROM ';
        $strSQL .= '                        DB2WUGR ';
        $strSQL .= '                    WHERE ';
        $strSQL .= '                        WUUID = ?';
        $strSQL .= '                ) ';
        $strSQL .= '        ) ';
        $strSQL .= '        AND WLNAME = \'\'';
        $strSQL .= '             OR ';
        $strSQL .= '            WLNAME IN (SELECT ';
        $strSQL .= '                            D1NAME ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            FDB2CSV1 ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            D1NAME IN ( ';
        $strSQL .= '                                ( ';
        $strSQL .= '                                    SELECT DISTINCT ';
        $strSQL .= '                                        WGNAME ';
        $strSQL .= '                                    FROM ';
        $strSQL .= '                                        DB2WGDF ';
        $strSQL .= '                                    WHERE ';
        $strSQL .= '                                        WGGID IN ( ';
        $strSQL .= '                                            SELECT ';
        $strSQL .= '                                                WUGID ';
        $strSQL .= '                                            FROM ';
        $strSQL .= '                                                DB2WUGR ';
        $strSQL .= '                                            WHERE ';
        $strSQL .= '                                                WUUID = ? ';
        $strSQL .= '                                        ) ';
        $strSQL .= '                                ) UNION  ( ';
        $strSQL .= '                                    SELECT ';
        $strSQL .= '                                        DQNAME ';
        $strSQL .= '                                    FROM ';
        $strSQL .= '                                        DB2QHIS ';
        $strSQL .= '                                    RIGHT JOIN ';
        $strSQL .= '                                        FDB2CSV1 ';
        $strSQL .= '                                    ON DQNAME = D1NAME ';
        $strSQL .= '                                    WHERE ';
        $strSQL .= '                                        DQNAME <> \'\' ';
        $strSQL .= '                                    AND DQCUSR = ?  ';
        $strSQL .= '                                ) ';
        $strSQL .= '                            ) ';
        $strSQL .= '                        ) ';
		$strSQL .= '						OR ';
		$strSQL .= ' 						( WLKBN=\'P\' AND WLNAME<>\'\' AND C.D1NAME IS NULL ) ';//プレビューの場合は保存しない事もあるから、チェックを入れてないです。
        $strSQL .= '                    ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '        AND ( ';
        $strSQL .= '            WLUID = ? OR ';
        $strSQL .= '            WLNAME IN (SELECT ';
        $strSQL .= '                            D1NAME ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            FDB2CSV1 ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            D1NAME IN ( ';
        $strSQL .= '                                    SELECT ';
        $strSQL .= '                                        DQNAME ';
        $strSQL .= '                                    FROM ';
        $strSQL .= '                                        DB2QHIS ';
        $strSQL .= '                                    RIGHT JOIN ';
        $strSQL .= '                                        FDB2CSV1 ';
        $strSQL .= '                                    ON DQNAME = D1NAME ';
        $strSQL .= '                                    WHERE ';
        $strSQL .= '                                        DQNAME <> \'\' ';
        $strSQL .= '                                    AND DQCUSR = ?  ';
        $strSQL .= '                            ) ';
        $strSQL .= '                        ) ';
		$strSQL .= ' 			OR ';
		$strSQL .= ' 			( WLKBN=\'P\' AND WLNAME<>\'\' AND C.D1NAME IS NULL ) ';//プレビューの場合は保存しない事もあるから、チェックを入れてないです。
        $strSQL .= '                    ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    if($SDAY != ''){
        $strSQL .= ' AND WLDAY >= ? ';
        array_push($params,str_replace("/","",$SDAY));
    }

    if($EDAY != ''){
        $strSQL .= ' AND WLDAY <= ? ';
        array_push($params,str_replace("/","",$EDAY));
    }
    //e_log('実行ログデータ取得：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;

}

//MSM add for 通常 OR ActiveDirectory
function fnCHECKactive($db2con) {
    $data = array();
    
    //構文
    $strSQL = ' SELECT COASVR,COAFLG, COAHOS, COADO';
    $strSQL .= ' FROM DB2COF ';
     
    $params = array();
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    
    return $data;
    
}