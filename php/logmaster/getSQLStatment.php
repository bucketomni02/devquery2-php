<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../base/createTblExeSQL.php");
include_once("../base/createExecuteSQL.php");

$data          = array();
$rtn           = 0;
$msg           = '';

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$WLDAY    = (isset($_POST['WLDAY'])?$_POST['WLDAY']:'');
$WLTIME    = (isset($_POST['WLTIME'])?$_POST['WLTIME']:'');
$WLUID    = (isset($_POST['WLUID'])?$_POST['WLUID']:'');
$WLNAME    = (isset($_POST['WLNAME'])?$_POST['WLNAME']:'');
$WLKBN    = (isset($_POST['WLKBN'])?$_POST['WLKBN']:'');
$strExeSQLParam = '';
//DB接続
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

$rs = fnGetstrExeSQL($db2con,$WLDAY,$WLTIME,$WLUID,$WLNAME,$WLKBN);
if($rs['result'] !== true){
    $rtn = 1;
    $msg =  showMsg($rs['result']);
}else{
    $data =$rs['data'];
    if(count($data) > 0){
        for($i = 1 ; $i <= 7 ; $i++){
            $val = $data[0]['WLSQLB'.$i];
            if($val !== ''){
                $strExeSQLParam .= $val;
            }
        }
    }
}

cmDb2Close($db2con);

$rtn = array(
    'RTN'       => $rtn,
    'MSG'       => $msg,
    'strExeSQLParam' => $strExeSQLParam,
    'data' => $data
);
echo(json_encode($rtn));

function fnGetstrExeSQL($db2con,$WLDAY,$WLTIME,$WLUID,$WLNAME,$WLKBN){
    $data =  array();
    $strSQL = "";
    $strSQL .= " SELECT ";
    $strSQL .= " CASE WHEN LENGTH(TRIM(SUBSTR(WLSQLB,1,5000))) > 0 THEN TRIM(SUBSTR(WLSQLB,1,5000))  ELSE '' END AS WLSQLB1,";
    $strSQL .= " CASE WHEN LENGTH(TRIM(SUBSTR(WLSQLB,5001,5000))) > 0 THEN TRIM(SUBSTR(WLSQLB,5001,5000))  ELSE '' END AS WLSQLB2,";
    $strSQL .= " CASE WHEN LENGTH(TRIM(SUBSTR(WLSQLB,10001,5000))) > 0 THEN TRIM(SUBSTR(WLSQLB,10001,5000))  ELSE '' END AS WLSQLB3,";
    $strSQL .= " CASE WHEN LENGTH(TRIM(SUBSTR(WLSQLB,15001,5000))) > 0 THEN TRIM(SUBSTR(WLSQLB,15001,5000)) ELSE '' END AS WLSQLB4,";
    $strSQL .= " CASE WHEN LENGTH(TRIM(SUBSTR(WLSQLB,20001,5000))) > 0 THEN TRIM(SUBSTR(WLSQLB,20001,5000)) ELSE '' END AS WLSQLB5,";
    $strSQL .= " CASE WHEN LENGTH(TRIM(SUBSTR(WLSQLB,25001,5000))) > 0 THEN TRIM(SUBSTR(WLSQLB,25001,5000)) ELSE '' END AS WLSQLB6,";
    $strSQL .= " CASE WHEN LENGTH(TRIM(SUBSTR(WLSQLB,30001,2730))) > 0 THEN TRIM(SUBSTR(WLSQLB,30001,2730)) ELSE '' END AS WLSQLB7";
    $strSQL .= " FROM( ";
    $strSQL .= " SELECT TRIM(WLSQLB) as WLSQLB FROM DB2WLOG WHERE WLDAY = ? AND WLTIME = ? AND WLUID = ? ";
  //  $strSQL .= " SELECT TRIM(WLSQLB) as WLSQLB FROM DB2WLOG WHERE WLUID = ? AND WLNAME = ? GROUP BY WLSQLB ";
    $strSQL .= ") AS A";
    $param = array($WLDAY,$WLTIME,$WLUID);
 //   $param = array($WLUID,$WLNAME);
    e_log($strSQL.print_r($param,true));
	$stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
         $data = array('result' => 'FAIL_SEL');
    }else{

	    $r = db2_execute($stmt,$param);
		if($r === false){
            $data = array('result' => 'FAIL_SEL');
		}else{
            while($row = db2_fetch_assoc($stmt)){
			    $data[] = $row;
			}
            $data = array('result' => true,'data' => $data);
        }
    }

	return $data;
}
