<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../getQryCnd.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$SDAY   = $_POST['SDAY'];
$STIME  = $_POST['STIME'];
$QRYNM  = $_POST['D1NAME'];
$SUSER  = $_POST['USER'];
$WEBFLG = $_POST['D1WEBF'];
//定義FLG
$WLKBN  = $_POST['WLKBN'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/

$data = array();
$rtn = 0;
$msg = '';
$cnddatArr=array();
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
          
            $rs = cmChkKenGen($db2con,'23',$userData[0]['WUSAUT']);//'1' => logMaster
       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('実行ログの権限'));
            }
        }
    }
}

if($rtn === 0){
    if($WEBFLG !== '1'){
        $rs = fnFdb2csv3SrData($db2con,$QRYNM,$SDAY,$STIME,$SUSER);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
        $cnddatArr = umEx($data);

    }
    else{
		$rs=($WLKBN==="P")?fnBqrycndSrDataPreview($db2con,$QRYNM,$SDAY,$STIME,$SUSER):
				fnBqrycndSrData($db2con,$QRYNM,$SDAY,$STIME,$SUSER);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
        $data = umEx($data);
        if(count($data) > 0){
            $cnddatArr = fnCreateCndData($data);			
        }
    }
}
 
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN'=> $rtn,
    'MSG'=> $msg,
    'cndData' => $cnddatArr
);

echo(json_encode($rtn));

function fnBqrycndSrData($db2con, $QRYNM, $sday, $stime, $suser)
{
    $data   = array();
    $params = array();
    $strSQL = '';
    
    $strSQL = '  SELECT QRYCND.CNQRYN    ';
    $strSQL .= '      , QRYCND.CNFILID   ';
    $strSQL .= '      , QRYCND.FILSEQ    ';
    $strSQL .= '      , QRYCND.CNFIL     ';
    $strSQL .= '      , QRYCND.CNMSEQ    ';
    $strSQL .= '      , GRPCND.CNDCNT    ';
    $strSQL .= '      , GRPCND.CNSCNT    ';
    $strSQL .= '      , GRPCND.CDCNT     ';
    $strSQL .= '      , QRYCND.CNSSEQ    ';
    $strSQL .= '      , QRYCND.CNFLDN    ';
    $strSQL .= '      , QRYCND.D2HED     ';
    $strSQL .= '      , QRYCND.D2TYPE    ';
    $strSQL .= '      , QRYCND.D2LEN     ';
    $strSQL .= '      , QRYCND.D2DEC     ';
    $strSQL .= '      , QRYCND.CNAOKB    ';
    $strSQL .= '      , QRYCND.CNCKBN    ';
    $strSQL .= '      , QRYCND.CNSTKB    ';
    $strSQL .= '      , QRYCND.CNCANL    ';
    $strSQL .= '      , QRYCND.CNCANF    ';
    $strSQL .= '      , QRYCND.CNCANC    ';
    $strSQL .= '      , QRYCND.CNCANG    ';
    $strSQL .= '      , QRYCND.CNNAMG    ';
    $strSQL .= '      , QRYCND.CNNAMC    ';
    $strSQL .= '      , QRYCND.CNCAST    ';
    $strSQL .= '      , QRYCND.CNNAST    ';
    $strSQL .= '      , QRYCND.CNDFMT    ';
    $strSQL .= '      , QRYCND.CNDSFL    ';
    $strSQL .= '      , QRYCND.CNDFIN    ';
    $strSQL .= '      , QRYCND.CNDFPM    ';
    $strSQL .= '      , QRYCND.CNDFDY    ';
    $strSQL .= '      , QRYCND.CNDFFL    ';
    $strSQL .= '      , QRYCND.CNDTIN    ';
    $strSQL .= '      , QRYCND.CNDTPM    ';
    $strSQL .= '      , QRYCND.CNDTDY    ';
    $strSQL .= '      , QRYCND.CNDTFL    ';
    $strSQL .= '      , QRYCND.CNDSCH    ';
    $strSQL .= '      , QRYCND.tblName    ';
    $strSQL .= '      , QRYCND.DHINFG    ';
    $strSQL .= '      , QRYCND.CDCDCD    ';
    $strSQL .= '      , QRYCND.CDDAT     ';
    $strSQL .= ' FROM           ';
    $strSQL .= '     ( SELECT     ';
    $strSQL .= '              A.CNQRYN  ';
    $strSQL .= '            , A.CNFILID  ';
    $strSQL .= '            , FILCD.FILSEQ ';
    $strSQL .= '            , ( CASE  ';
    $strSQL .= '                   WHEN A.CNFILID = 0 THEN  \'P \' ';
    $strSQL .= '                   WHEN A.CNFILID <= FILCD.FILSEQ  ';
    $strSQL .= '                   THEN CONCAT( \'S \', A.CNFILID )  ';
    $strSQL .= '                   ELSE  \'K \'      ';
    $strSQL .= '                END        ';
    $strSQL .= '               ) AS CNFIL      ';
    $strSQL .= '            , A.CNMSEQ     ';
    $strSQL .= '            , A.CNSSEQ     ';
    $strSQL .= '            , A.CNFLDN     ';
    $strSQL .= '            , A.D2HED      ';
    $strSQL .= '            , A.D2TYPE     ';
    $strSQL .= '            , A.D2LEN      ';
    $strSQL .= '            , A.D2DEC      ';
    $strSQL .= '            , A.CNAOKB     ';
    $strSQL .= '            , A.CNCKBN     ';
    $strSQL .= '            , A.CNSTKB     ';
    $strSQL .= '            , A.CNCANL     ';
    $strSQL .= '            , A.CNCANF     ';
    $strSQL .= '            , A.CNCANC     ';
    $strSQL .= '            , A.CNCANG     ';
    $strSQL .= '            , A.CNNAMG     ';
    $strSQL .= '            , A.CNNAMC     ';
    $strSQL .= '            , A.CNCAST     ';
    $strSQL .= '            , A.CNNAST     ';
    $strSQL .= '            , A.CNDFMT     ';
    $strSQL .= '            , A.CNDSFL     ';
    $strSQL .= '            , A.CNDFIN     ';
    $strSQL .= '            , A.CNDFPM     ';
    $strSQL .= '            , A.CNDFDY     ';
    $strSQL .= '            , A.CNDFFL     ';
    $strSQL .= '            , A.CNDTIN     ';
    $strSQL .= '            , A.CNDTPM     ';
    $strSQL .= '            , A.CNDTDY     ';
    $strSQL .= '            , A.CNDTFL     ';
    $strSQL .= '            , A.CNDSCH     ';
    $strSQL .= '            , HCND.DHNAME AS tblName  ';
    $strSQL .= '            , HCND.DHINFG  ';
    $strSQL .= '            , B.CDCDCD     ';
    $strSQL .= '            , B.CDDAT      ';
    $strSQL .= '       FROM                ';
    $strSQL .= '          ( SELECT AA.*    ';
    $strSQL .= '            FROM           ';
    $strSQL .= '             (SELECT QCND.*   ';
    $strSQL .= '                  , FLDINFO.D2HED   ';
    $strSQL .= '                  , FLDINFO.D2TYPE  ';
    $strSQL .= '                  , FLDINFO.D2LEN   ';
    $strSQL .= '                  , FLDINFO.D2DEC   ';
    $strSQL .= '              FROM BQRYCND QCND     ';
    $strSQL .= '              LEFT JOIN (           ';
    $strSQL .= '                  SELECT FLDDATA.*  ';
    $strSQL .= '                  FROM (            ';
    $strSQL .= '                       SELECT D2NAME   ';
    $strSQL .= '                            , D2FILID  ';
    $strSQL .= '                            , D2FLD    ';
    $strSQL .= '                            , D2HED    ';
    $strSQL .= '                            , D2CSEQ   ';
    $strSQL .= '                                 , D2WEDT   ';
    $strSQL .= '                                 , D2TYPE   ';
    $strSQL .= '                                 , D2LEN    ';
    $strSQL .= '                                 , D2DEC    ';
    $strSQL .= '                                 , D2DNLF   ';
    $strSQL .= '                           FROM FDB2CSV2    ';
    $strSQL .= '                           WHERE D2NAME = ?  ';
    $strSQL .= '                       UNION ALL   ';
    $strSQL .= '                           SELECT  D5NAME AS D2NAME      ';
    $strSQL .= '                                  , 9999 AS D2FILID      ';
    $strSQL .= '                                  , D5FLD AS D2FLD       ';
    $strSQL .= '                                  , D5HED AS D2HED       ';
    $strSQL .= '                                  , D5CSEQ AS D2CSEQ     ';
    $strSQL .= '                                  , D5WEDT AS D2WEDT     ';
    $strSQL .= '                                  , D5TYPE AS D2TYPE     ';
    $strSQL .= '                                  , D5LEN AS D2LEN       ';
    $strSQL .= '                                  , D5DEC AS D2DEC       ';
    $strSQL .= '                                  , D5DNLF AS D2DNLF     ';
    $strSQL .= '                           FROM FDB2CSV5                 ';
    $strSQL .= '                           WHERE D5NAME = ?   ';
    $strSQL .= '                       )  FLDDATA                        ';
    $strSQL .= '                   ) FLDINFO                             ';
    $strSQL .= '                   ON QCND.CNQRYN    = FLDINFO.D2NAME    ';
    $strSQL .= '                   AND QCND.CNFILID  = FLDINFO.D2FILID   ';
    $strSQL .= '                   AND QCND.CNFLDN   = FLDINFO.D2FLD     ';
    $strSQL .= '                   WHERE QCND.CNQRYN = ?      ';
    $strSQL .= '                  )   AA                                 ';
    $strSQL .= '               ) A                                       ';
    $strSQL .= '              LEFT JOIN  (SELECT                         ';
    $strSQL .= '                           Q.WLNAME as CDQRYN,           ';
    $strSQL .= '                           P.SDDAY as SDDAY,             ';
    $strSQL .= '                           P.SDTIME as SDTIME,           ';
    $strSQL .= '                           P.SDUID as SDUID,             ';
    $strSQL .= '                           P.SDMSEQ as CDMSEQ ,          ';
    $strSQL .= '                           P.SDSSEQ as CDSSEQ,           ';
    $strSQL .= '                           P.SDCDCD as CDCDCD,           ';
    $strSQL .= '                           P.SDDAT as CDDAT              ';
    $strSQL .= '                       FROM                              ';
    $strSQL .= '                           DB2WSDA as P left join DB2WLOG as Q on P.SDDAY = Q.WLDAY and P.SDTIME=Q.WLTIME and P.SDUID = Q.WLUID ';
    $strSQL .= '                       WHERE  SDDAY = ?';
    $strSQL .= '                       AND    SDTIME = ?';
    $strSQL .= '                       AND SDUID = ? ';
    $strSQL .= '                       AND Q.WLNAME = ? ';
    $strSQL .= '                       ) B   ';
    $strSQL .= '              ON    A.CNQRYN  = B.CDQRYN   ';
    $strSQL .= '              AND   A.CNMSEQ  = B.CDMSEQ   ';
    $strSQL .= '              AND   A.CNSSEQ  = B.CDSSEQ   ';
    $strSQL .= '              LEFT JOIN  DB2HCND HCND   ';
    $strSQL .= '              ON    A.CNQRYN  = HCND.DHNAME    ';
    $strSQL .= '              AND   A.CNFILID = HCND.DHFILID   ';
    $strSQL .= '              AND   A.CNMSEQ  = HCND.DHMSEQ    ';
    $strSQL .= '              AND   A.CNSSEQ  = HCND.DHSSEQ    ';
    $strSQL .= '              LEFT JOIN  (                     ';
    $strSQL .= '                  SELECT RTQRYN,MAX(RTRSEQ) AS FILSEQ   ';
    $strSQL .= '                  FROM BREFTBL                        ';
    $strSQL .= '                  WHERE RTQRYN = ?         ';
    $strSQL .= '                  GROUP BY  RTQRYN ) FILCD            ';
    $strSQL .= '              ON FILCD.RTQRYN = A.CNQRYN              ';
    $strSQL .= '              WHERE   A.CNQRYN  = ?        ';
    $strSQL .= '              ORDER BY A.CNMSEQ ,A.CNSSEQ  ,B.CDCDCD  ';
    $strSQL .= '          ) QRYCND ,                                  ';
    $strSQL .= '          (                                           ';
    $strSQL .= '              SELECT A.CNQRYN                         ';
    $strSQL .= '                   , A.CNMSEQ                         ';
    $strSQL .= '                   , B.CNDCNT                         ';
    $strSQL .= '                   , A.CNSCNT                         ';
    $strSQL .= '                   , C.CDSSEQ                         ';
    $strSQL .= '                   , C.CDCNT                          ';
    $strSQL .= '              FROM (                                  ';
    $strSQL .= '                      SELECT CNQRYN                   ';
    $strSQL .= '                           , CNMSEQ                   ';
    $strSQL .= '                           , COUNT(CNSSEQ) CNSCNT     ';
    $strSQL .= '                        FROM BQRYCND                  ';
    $strSQL .= '                       WHERE CNQRYN  = ?   ';
    $strSQL .= '                    GROUP BY CNQRYN                   ';
    $strSQL .= '                           , CNMSEQ                   ';
    $strSQL .= '                  ) A                                 ';
    $strSQL .= '                  ,(                                  ';
    $strSQL .= '                      SELECT CNQRYN                   ';
    $strSQL .= '                           , MAX(CNMSEQ) AS CNDCNT    ';
    $strSQL .= '                        FROM BQRYCND                  ';
    $strSQL .= '                       WHERE CNQRYN  = ?   ';
    $strSQL .= '                    GROUP BY CNQRYN                   ';
    $strSQL .= '                  ) B                                 ';
    $strSQL .= '                  ,(                                  ';
    $strSQL .= '                      SELECT QRYC.CNQRYN AS CDQRYN    ';
    $strSQL .= '                           , QRYC.CNMSEQ AS CDMSEQ    ';
    $strSQL .= '                           , QRYC.CNSSEQ AS CDSSEQ    ';
    $strSQL .= '                           , COUNT(CDAT.CDCDCD) CDCNT ';
    $strSQL .= '                        FROM BQRYCND QRYC             ';
    $strSQL .= '                   LEFT JOIN (SELECT                  ';
    $strSQL .= '                               Q.WLNAME as CDQRYN,    ';
    $strSQL .= '                               P.SDDAY as SDDAY,      ';
    $strSQL .= '                               P.SDTIME as SDTIME,    ';
    $strSQL .= '                               P.SDUID as SDUID,      ';
    $strSQL .= '                               P.SDMSEQ as CDMSEQ ,   ';
    $strSQL .= '                               P.SDSSEQ as CDSSEQ,    ';
    $strSQL .= '                               P.SDCDCD as CDCDCD,    ';
    $strSQL .= '                               P.SDDAT as CDDAT       ';
    $strSQL .= '                           FROM                       ';
    $strSQL .= '                               DB2WSDA as P left join DB2WLOG as Q on P.SDDAY = Q.WLDAY and P.SDTIME=Q.WLTIME and P.SDUID = Q.WLUID';
    $strSQL .= '                           WHERE  SDDAY = ?';
    $strSQL .= '                           AND    SDTIME = ?';
    $strSQL .= '                           AND SDUID = ? ';
    $strSQL .= '                           AND Q.WLNAME = ? ';
    $strSQL .= '                           ) CDAT   ';
    $strSQL .= '                          ON QRYC.CNQRYN  = CDAT.CDQRYN    ';
    $strSQL .= '                         AND QRYC.CNMSEQ = CDAT.CDMSEQ     ';
    $strSQL .= '                         AND QRYC.CNSSEQ = CDAT.CDSSEQ     ';
    $strSQL .= '                       WHERE QRYC.CNQRYN  = ?   ';
    $strSQL .= '                    GROUP BY QRYC.CNQRYN                   ';
    $strSQL .= '                           , QRYC.CNMSEQ                   ';
    $strSQL .= '                           , QRYC.CNSSEQ                   ';
    $strSQL .= '                           , CDAT.CDQRYN                   ';
    $strSQL .= '                           , CDAT.CDMSEQ                   ';
    $strSQL .= '                           , CDAT.CDSSEQ                   ';
    $strSQL .= '                  )C                                       ';
    $strSQL .= '             WHERE A.CNQRYN = B.CNQRYN                     ';
    $strSQL .= '               AND A.CNQRYN = C.CDQRYN                     ';
    $strSQL .= '               AND B.CNQRYN = C.CDQRYN                     ';
    $strSQL .= '               AND A.CNMSEQ = C.CDMSEQ                     ';
    $strSQL .= '         ) GRPCND                                          ';
    $strSQL .= '     WHERE QRYCND.CNQRYN =  GRPCND.CNQRYN                  ';
    $strSQL .= '       AND QRYCND.CNMSEQ =  GRPCND.CNMSEQ                  ';
    $strSQL .= '       AND QRYCND.CNSSEQ =  GRPCND.CDSSEQ                  ';
    $strSQL .= '     ORDER BY QRYCND.CNMSEQ                                ';
    $strSQL .= '            , QRYCND.CNSSEQ  ';
    
    
    $params = array(
        $QRYNM,
        $QRYNM,
        $QRYNM,
        $sday,
        $stime,
        $suser,
        $QRYNM,
        $QRYNM,
        $QRYNM,
        $QRYNM,
        $QRYNM,
        $sday,
        $stime,
        $suser,
        $QRYNM,
        $QRYNM
    );
   	//e_log("ロックの検索" . $strSQL . print_r($params, true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = umEx($data, true);
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    return $data;
}

function fnBqrycndSrDataPreview($db2con, $QRYNM, $sday, $stime, $suser)
{
    $data   = array();
    $params = array();
    $strSQL = '';
    
    $strSQL = '  SELECT QRYCND.CNQRYN    ';
    $strSQL .= '      , QRYCND.CNFILID   ';
    $strSQL .= '      , QRYCND.FILSEQ    ';
    $strSQL .= '      , QRYCND.CNFIL     ';
    $strSQL .= '      , QRYCND.CNMSEQ    ';

    $strSQL .= '      , GRPCND.CNDCNT    ';
    $strSQL .= '      , GRPCND.CNSCNT    ';
    $strSQL .= '      , GRPCND.CDCNT     ';

    $strSQL .= '      , QRYCND.CNSSEQ    ';
    $strSQL .= '      , QRYCND.CNFLDN    ';
    $strSQL .= '      , QRYCND.D2HED     ';
    $strSQL .= '      , QRYCND.D2TYPE    ';
    $strSQL .= '      , QRYCND.D2LEN     ';
    $strSQL .= '      , QRYCND.D2DEC     ';
    $strSQL .= '      , QRYCND.CNAOKB    ';
    $strSQL .= '      , QRYCND.CNCKBN    ';
    $strSQL .= '      , QRYCND.CNSTKB    ';
    $strSQL .= '      , QRYCND.CNCANL    ';
    $strSQL .= '      , QRYCND.CNCANF    ';
    $strSQL .= '      , QRYCND.CNCANC    ';
    $strSQL .= '      , QRYCND.CNCANG    ';
    $strSQL .= '      , QRYCND.CNNAMG    ';
    $strSQL .= '      , QRYCND.CNNAMC    ';
    $strSQL .= '      , QRYCND.CNCAST    ';
    $strSQL .= '      , QRYCND.CNNAST    ';
    $strSQL .= '      , QRYCND.CNDFMT    ';
    $strSQL .= '      , QRYCND.CNDSFL    ';
    $strSQL .= '      , QRYCND.CNDFIN    ';
    $strSQL .= '      , QRYCND.CNDFPM    ';
    $strSQL .= '      , QRYCND.CNDFDY    ';
    $strSQL .= '      , QRYCND.CNDFFL    ';
    $strSQL .= '      , QRYCND.CNDTIN    ';
    $strSQL .= '      , QRYCND.CNDTPM    ';
    $strSQL .= '      , QRYCND.CNDTDY    ';
    $strSQL .= '      , QRYCND.CNDTFL    ';
    $strSQL .= '      , QRYCND.CNDSCH    ';
    $strSQL .= '      , QRYCND.tblName    ';
    $strSQL .= '      , QRYCND.DHINFG    ';
    $strSQL .= '      , QRYCND.CDCDCD    ';
    $strSQL .= '      , QRYCND.CDDAT     ';
    $strSQL .= ' FROM           ';
    $strSQL .= '     (  SELECT ';
    $strSQL .= '              Q.WLNAME AS CNQRYN ';
    $strSQL .= '            ,\'\' AS CNFILID     ';
    $strSQL .= '            ,\'\' AS FILSEQ      ';
    $strSQL .= '            ,\'\' AS CNFIL       ';
    $strSQL .= '            ,P.SDMSEQ AS CNMSEQ  ';
    $strSQL .= '            ,P.SDSSEQ AS CNSSEQ';
    $strSQL .= '            ,\'\' AS CNFLDN     ';
    $strSQL .= '            ,P.SDFLD AS D2HED      ';
    $strSQL .= '            ,\'\' AS D2TYPE     ';
    $strSQL .= '            ,\'\' AS D2LEN		';
    $strSQL .= '            ,\'\' AS D2DEC		';
    $strSQL .= '            ,P.SDANDOR AS CNAOKB	';
    $strSQL .= '            ,P.SDKBN AS CNCKBN     ';
    $strSQL .= '            ,P.SDSTKB AS CNSTKB     ';
    $strSQL .= '            ,\'\' AS CNCANL     ';
    $strSQL .= '            ,\'\' AS CNCANF     ';
    $strSQL .= '            ,\'\' AS CNCANC     ';
    $strSQL .= '            ,\'\' AS CNCANG     ';
    $strSQL .= '            ,\'\' AS CNNAMG     ';
    $strSQL .= '            ,\'\' AS CNNAMC     ';
    $strSQL .= '            ,\'\' AS CNCAST     ';
    $strSQL .= '            ,\'\' AS CNNAST     ';
    $strSQL .= '            ,\'\' AS CNDFMT     ';
    $strSQL .= '            ,\'\' AS CNDSFL     ';
    $strSQL .= '            ,\'\' AS CNDFIN     ';
    $strSQL .= '            ,\'\' AS CNDFPM     ';
    $strSQL .= '            ,\'\' AS CNDFDY     ';
    $strSQL .= '            ,\'\' AS CNDFFL     ';
    $strSQL .= '            ,\'\' AS CNDTIN     ';
    $strSQL .= '            ,\'\' AS CNDTPM     ';
    $strSQL .= '            ,\'\' AS CNDTDY     ';
    $strSQL .= '            ,\'\' AS CNDTFL     ';
    $strSQL .= '            ,\'\' AS CNDSCH     ';
    $strSQL .= '            ,\'\' AS tblName  	';
    $strSQL .= '            ,\'\' AS DHINFG  	';
    $strSQL .= '            ,P.SDCDCD AS CDCDCD ';
    $strSQL .= '            ,P.SDDAT AS CDDAT   ';
    $strSQL .= '			,P.SDDAY as SDDAY   ';
    $strSQL .= '			,P.SDTIME as SDTIME ';
    $strSQL .= '			,P.SDUID as SDUID   ';
    $strSQL .= '	FROM                        ';
    $strSQL .= '		DB2WSDA as P left join DB2WLOG as Q on P.SDDAY = Q.WLDAY and P.SDTIME=Q.WLTIME and P.SDUID = Q.WLUID ';
    $strSQL .= '		WHERE  SDDAY = ?';
    $strSQL .= '			AND    SDTIME = ?';
    $strSQL .= '			AND SDUID = ? ';
    $strSQL .= '			AND Q.WLNAME = ? ';
    $strSQL .= '	) QRYCND ';

    $strSQL .= '    ,(                       ';
    $strSQL .= '              SELECT A.CNQRYN                         ';
    $strSQL .= '                   , A.CNMSEQ                         ';
    $strSQL .= '                   , B.CNDCNT                         ';
    $strSQL .= '                   , A.CNSCNT                         ';
    $strSQL .= '                   , C.CDSSEQ                         ';
    $strSQL .= '                   , C.CDCNT                          ';
    $strSQL .= '              FROM (                                  ';
	$strSQL .= '						SELECT AA.CNQRYN';
	$strSQL .= '							,AA.CNMSEQ         ';
	$strSQL .= '							,COUNT(AA.SDSSEQ) AS CNSCNT         ';
    $strSQL .= '						FROM ';
	$strSQL .='                         ( ';
	$strSQL .='                          	SELECT QQ.WLNAME AS CNQRYN ';
	$strSQL .='                          		,PP.SDMSEQ AS CNMSEQ ';
	$strSQL .='                          		,PP.SDSSEQ AS SDSSEQ ';
	$strSQL .='                          		,PP.SDCDCD AS SDCDCD ';
    $strSQL .= '							FROM DB2WSDA as PP left join DB2WLOG as QQ on PP.SDDAY = QQ.WLDAY and PP.SDTIME=QQ.WLTIME and PP.SDUID = QQ.WLUID ';
    $strSQL .= '								WHERE  PP.SDDAY = ?';
    $strSQL .= '									AND PP.SDTIME = ?';
    $strSQL .= '									AND PP.SDUID = ? ';
    $strSQL .= '									AND QQ.WLNAME = ? ';
    $strSQL .= '                    		GROUP BY QQ.WLNAME    ';
    $strSQL .= '                           			, PP.SDMSEQ        ';
    $strSQL .= '                           			, PP.SDSSEQ        ';
    $strSQL .= '                           			, PP.SDCDCD        ';
    $strSQL .= '                        ) AA       ';
    $strSQL .= '                    	GROUP BY AA.CNQRYN    ';
    $strSQL .= '                           		,AA.CNMSEQ        ';
    $strSQL .= '                  ) A                                 ';
    $strSQL .= '                  ,(                                  ';
	$strSQL .= '						SELECT CNQRYN    ';
    $strSQL .= '                        	,MAX(BB.CNMSEQ) AS CNDCNT    ';
    $strSQL .= '						FROM                              ';
	$strSQL .='                         ( ';
	$strSQL .='                          	SELECT QQ.WLNAME AS CNQRYN ';
	$strSQL .='                          		,PP.SDMSEQ AS CNMSEQ ';
	$strSQL .='                          		,PP.SDSSEQ AS SDSSEQ ';
	$strSQL .='                          		,PP.SDCDCD AS SDCDCD ';
    $strSQL .= '							FROM DB2WSDA as PP left join DB2WLOG as QQ on PP.SDDAY = QQ.WLDAY and PP.SDTIME=QQ.WLTIME and PP.SDUID = QQ.WLUID ';
    $strSQL .= '								WHERE  PP.SDDAY = ?';
    $strSQL .= '									AND PP.SDTIME = ?';
    $strSQL .= '									AND PP.SDUID = ? ';
    $strSQL .= '									AND QQ.WLNAME = ? ';
    $strSQL .= '                    		GROUP BY QQ.WLNAME    ';
    $strSQL .= '                           			, PP.SDMSEQ        ';
    $strSQL .= '                           			, PP.SDSSEQ        ';
    $strSQL .= '                           			, PP.SDCDCD        ';
    $strSQL .= '                        ) BB       ';
    $strSQL .= '                    	GROUP BY BB.CNQRYN    ';
    $strSQL .= '                  ) B                                 ';
    $strSQL .= '                  ,(                                  ';
	$strSQL .= '						SELECT 	CC.CDQRYN,CC.CDMSEQ,CC.CDCNT,CC.CDSSEQ	  ';
	$strSQL .= '						FROM	  ';
	$strSQL .= '						(	  ';		
	$strSQL .= '							SELECT 	QQ.WLNAME AS CDQRYN      ';
	$strSQL .= '								,PP.SDMSEQ AS CDMSEQ      ';
	$strSQL .= '								,PP.SDSSEQ AS CDSSEQ      ';
	$strSQL .= '								,COUNT(PP.SDCDCD) AS CDCNT         ';
    $strSQL .= '							FROM                              ';
    $strSQL .= '								DB2WSDA as PP left join DB2WLOG as QQ on PP.SDDAY = QQ.WLDAY and PP.SDTIME=QQ.WLTIME and PP.SDUID = QQ.WLUID ';
    $strSQL .= '							WHERE  PP.SDDAY = ?';
    $strSQL .= '								AND PP.SDTIME = ?';
    $strSQL .= '								AND PP.SDUID = ? ';
    $strSQL .= '								AND QQ.WLNAME = ? ';
    $strSQL .= '         					GROUP BY QQ.WLNAME ';
    $strSQL .= '          						, PP.SDMSEQ     ';
    $strSQL .= '          						, PP.SDSSEQ     ';
    $strSQL .= '          				) CC     ';
    $strSQL .= '          				ORDER BY CC.CDSSEQ     ';
    $strSQL .= '                  )C                                       ';
    $strSQL .= '             WHERE A.CNQRYN = B.CNQRYN                     ';
    $strSQL .= '               AND A.CNQRYN = C.CDQRYN                     ';
    $strSQL .= '               AND B.CNQRYN = C.CDQRYN                     ';
    $strSQL .= '               AND A.CNMSEQ = C.CDMSEQ                     ';
    $strSQL .= '         ) GRPCND                                          ';
    $strSQL .= '     WHERE QRYCND.CNQRYN =  GRPCND.CNQRYN                  ';
    $strSQL .= '       AND QRYCND.CNMSEQ =  GRPCND.CNMSEQ                  ';
    $strSQL .= '       AND QRYCND.CNSSEQ =  GRPCND.CDSSEQ                  ';

    $strSQL .= '     ORDER BY QRYCND.CNMSEQ ';
    $strSQL .= '            , QRYCND.CNSSEQ  ';
    
    $params = array(
        $sday,
        $stime,
        $suser,
        $QRYNM,
		$sday,
        $stime,
        $suser,
        $QRYNM,
		$sday,
        $stime,
        $suser,
        $QRYNM,
		$sday,
        $stime,
        $suser,
        $QRYNM
    );
   	//e_log("ロックのピレビュー検索" . $strSQL . print_r($params, true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = umEx($data, true);
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    return $data;
}

/*
 *-------------------------------------------------------* 
 * 検索情報とヘルプ取得
 *-------------------------------------------------------*
 */
function fnFdb2csv3SrData($db2con,$QRYNM,$SDAY,$STIME,$SUSER)
{
    $data   = array();
    $params = array();
    
    $strSQL .= '         SELECT    ';
    $strSQL .= '             A.*,    ';
    $strSQL .= '             B.* ,    ';
    $strSQL .= '             C.SKDAT    ';
    $strSQL .= '         FROM    ';
    $strSQL .= '             FDB2CSV3 AS A   JOIN FDB2CSV2 as B       ';
    $strSQL .= '             ON A.D3NAME = B.D2NAME       ';
    $strSQL .= '             AND A.D3FILID = B.D2FILID       ';
    $strSQL .= '             AND A.D3FLD = B.D2FLD INNER JOIN (SELECT    ';
    $strSQL .= '                                                  Q.WLNAME as WLNAME,    ';
    $strSQL .= '                                                  P.*    ';
    $strSQL .= '                                              FROM    ';
    $strSQL .= '                                                 DB2WSDK as P LEFT JOIN DB2WLOG as Q     ';
    $strSQL .= '                                                 on P.SKDAY = Q.WLDAY     ';
    $strSQL .= '                                                 and P.SKTIME=Q.WLTIME     ';
    $strSQL .= '                                                 and P.SKUID = Q.WLUID    ';
    $strSQL .= '                                              WHERE    ';
    $strSQL .= '                                                  SKDAY = ? AND    ';
    $strSQL .= '                                                  SKTIME = ? AND    ';
    $strSQL .= '                                                  SKUID = ? AND    ';
    $strSQL .= '                                                  Q.WLNAME = ?    ';
    $strSQL .= '                                              ) as C  ON A.D3NAME = C.WLNAME and A.D3JSEQ =C.SKSEQ ';
    $strSQL .= '         WHERE    ';
    $strSQL .= '             A.D3NAME = ?  AND    ';
    $strSQL .= '             A.D3USEL <> \'3\'    ';
    $strSQL .= '         ORDER BY    ';
    $strSQL .= '             A.D3JSEQ ASC    ';
    
    $params = array(
        $SDAY,
        $STIME,
        $SUSER,
        $QRYNM,
        $QRYNM
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                if(cmMer($row['D2HED']) !== ''){
                    $row['D2HED'] = cmHsc($row['D2HED']);
                }
                if(cmMer($row['DHINFO']) !== ''){
                    $row['DHINFO'] = cmHsc($row['DHINFO']);
                }
                $data[] = $row;
            }
            $data = array('result' => true,'data'=>$data);
        }
    }
    return $data;
}