<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array(array('実行ログ連携','保険設定','権限')));
        }else if($userData[0]['WUAUTH'] === '2'){          
            $rs = cmChkKenGen($db2con,'23',$userData[0]['WUSAUT']);//'1' => logMaster       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('実行ログの権限'));
            }
        }
    }
}

if($rtn === 0){
    $rs = fnGetWCVAL($db2con);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $db2wcpn = $rs['data'];
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
	'aaData' => umEx($db2wcpn),
    'RTN' => $rtn,
    'MSG'=> $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ログ保管日数取得
*-------------------------------------------------------*
*/

function fnGetWCVAL($db2con){
	$data = array();
	$strSQL  = ' SELECT WCVAL ';
	$strSQL .= ' FROM DB2WCPN as A ' ;
	$strSQL .= ' WHERE WCKEY = ? ';

	$params = array('001');

	$stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
         $data = array('result' => 'FAIL_SEL');
    }else{
	    $r = db2_execute($stmt,$params);
		if($r === false){
            $data = array('result' => 'FAIL_SEL');
		}else{
            while($row = db2_fetch_assoc($stmt)){
			    $data[] = $row;
			}
            $data = array('result' => true,'data' => $data);
        }
    }
	return $data;

}