<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$touch = (isset($_POST['TOUCH'])?$_POST['TOUCH']:'0');

$user = array();
if($touch == '1'){
	$user = $_SESSION['PHPQUERYTOUCH']['user'];
}else{
	$user = $_SESSION['PHPQUERY']['user'];
}

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rs = cmInsertDB2WLOG($db2con,$logrs,$user[0]['WUUID'],'O','',array(),'','');
cmInsertDB2WSTL($db2con, $rs['DATE'],$rs['TIME'],$user[0]['WUUID'],'0','0');//mppn add log for logout
cmDb2Close($db2con);

if($touch == '1'){
	$_SESSION['PHPQUERYTOUCH'] = array();
}else{
	$_SESSION['PHPQUERY'] = array();
}

$rtn = array(
    'LOGOUTCLOSE'=>LOGOUTCLOSE
);

echo(json_encode($rtn));