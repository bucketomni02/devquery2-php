<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : メール配信グループ設定
* PROGRAM NAME   : メールアドレス削除
* PROGRAM ID     : delDB2MAAD.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/02/23
* MODIFY DATE    : 
* ============================================================
**/

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$MANAME = cmHscDe($_POST['MANAME']);
$MAMAIL = cmHscDe($_POST['MAMAIL']);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('メール配信グループの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'18',$userData[0]['WUSAUT']);//'1' => mailMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('メール配信グループの権限'));
            }
        }
    }
}
//メールグループ存在チェック
if($rtn === 0){
    $rs = fnCheckDB2MAGP($db2con,$MANAME);
    if($rs === 'NOTEXIST_DEL'){
        $rtn = 3;
        $msg = showMsg($rs,array('メールグループ'));
    }else if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('メールグループ'));
    }
}
//メールアドレス存在チェック
if($rtn === 0){
    $rs = fnCheckDB2MAAD($db2con,$MANAME,$MAMAIL);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('メールアドレス'));
    }
}
//メールアドレスの削除
if($rtn === 0){
    $rs = fnDeleteDB2MAAD($db2con,$MANAME,$MAMAIL);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}


cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* 削除対象メールアドレスが存在かどうかをチェックすること
* 
* RESULT
*    01：データがある場合 true
*    02：データがない場合 false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* @param String  $MAMAIL メールアドレス
*-------------------------------------------------------*
*/

function fnCheckDB2MAAD($db2con,$MANAME,$MAMAIL){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.MANAME ';
    $strSQL .= ' FROM DB2MAAD AS A ' ;
    $strSQL .= ' WHERE A.MANAME = ? ' ;
    $strSQL .= ' AND A.MAMAIL = ? ' ;

    $params = array(
        $MANAME,
        $MAMAIL
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_DEL';
            }
        }
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* 削除対象メールアドレスを削除すること
* 
* RESULT
*    01：削除終了の場合 true
*    02：エラーの場合 false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* @param String  $MAMAIL メールアドレス
*-------------------------------------------------------*
*/

function fnDeleteDB2MAAD($db2con,$MANAME,$MAMAIL){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2MAAD ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' MANAME = ? ' ;
    $strSQL .= ' AND ';
    $strSQL .= ' MAMAIL = ? ' ;

    $params = array(
        $MANAME
        ,$MAMAIL
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}

function fnCheckDB2MAGP($db2con,$MANAME){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.MGTEXT ';
    $strSQL .= ' FROM DB2MAGP AS A ' ;
    $strSQL .= ' WHERE A.MGNAME = ? ' ;

    $params = array(
        $MANAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_DEL';
            }
        }
    }
    return $rs;
}
