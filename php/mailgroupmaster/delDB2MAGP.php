<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$MGNAME = cmHscDe($_POST['MGNAME']);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('メール配信グループの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'18',$userData[0]['WUSAUT']);//'1' => mailMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('メール配信グループの権限'));
            }
        }
    }
}
//ユーザー存在チェック
if($rtn === 0){
    $rs = fnCheckDB2MAGP($db2con,$MGNAME);
    if( $rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('メールグループ'));
    }
}

if($rtn === 0){
    $rs = fnDeleteDB2MAGP($db2con,$MGNAME);
    if( $rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

if($rtn === 0){
    $rs = fnDeleteDB2MAAD($db2con,$MGNAME);
    if( $rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnAry));


/**
*-------------------------------------------------------* 
* 削除対象メールグループが存在するかどうかをチェックする
* 
* RESULT
*    01：データがある場合 true
*    02：データがない場合 false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID

* 
*-------------------------------------------------------*
*/

function fnCheckDB2MAGP($db2con,$MGNAME){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.MGNAME ';
    $strSQL .= ' FROM DB2MAGP AS A ' ;
    $strSQL .= ' WHERE A.MGNAME = ? ' ;

    $params = array(
        $MGNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_DEL';
            }
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* 削除対象メールグループ削除すること
* 
* RESULT
*    01：削除終了の場合 true
*    02：エラーの場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID

* 
*-------------------------------------------------------*
*/

function fnDeleteDB2MAGP($db2con,$MGNAME){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2MAGP ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' MGNAME = ? ' ;

    $params = array(
        $MGNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}

/**
*-------------------------------------------------------* 
* 削除対象メールグループのメールアドレスを削除すること
* 
* RESULT
*    01：削除終了の場合 true
*    02：エラーの場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* 
*-------------------------------------------------------*
*/

function fnDeleteDB2MAAD($db2con,$MGNAME){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2MAAD ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' MANAME = ? ' ;

    $params = array(
        $MGNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
