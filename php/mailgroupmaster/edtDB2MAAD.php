<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['proc']))?cmMer($_POST['proc']):'';
$MANAME = (isset($_POST['MANAME']))?cmMer($_POST['MANAME']):'';
$MAMAIL = (isset($_POST['MAMAIL']))?cmMer($_POST['MAMAIL']):'';
$MAMAILOLD = (isset($_POST['MAMAILOLD']))?cmMer($_POST['MAMAILOLD']):'';
$MAMEMO = (isset($_POST['MAMEMO']))?cmMer($_POST['MAMEMO']):'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/
$MANAME = cmHscDe($MANAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('メール配信グループの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'18',$userData[0]['WUSAUT']);//'1' => mailMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('メール配信グループの権限'));
            }
        }
    }
}

/*
*-------------------------------------------------------* 
* バリデーションチェック
*-------------------------------------------------------*
*/
// メールアドレス必須チェック
if($rtn === 0){
    if($MAMAIL  === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('メールアドレス'));
        $focus = 'MAMAIL';
    }
}

// メールアドレス半角チェック
if($rtn === 0){
    $byte = checkByte($MAMAIL);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('メールアドレス'));
        $focus = 'MAMAIL';
    }
}

// メールアドレス入力桁数チェック
if($rtn === 0){
    if(!checkMaxLen($MAMAIL,256)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('メールアドレス'));
        $focus = 'MAMAIL';
    }
}
// メールアドレス入力桁数チェック
if($rtn === 0){
   $rs = cmMailFormatChk($MAMAIL);
    if($rs === false){
        $rtn = 1;
        $msg = showMsg('FAIL_CHK',array('メールアドレス'));
        $focus = 'MAMAIL';
    }
}
// メーモ入力桁数チェック
if($rtn === 0){
    if($MAMEMO !== ''){
        if(!checkMaxLen($MAMEMO,128)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('メモ'));
            $focus = 'MAMEMO';
        }
    }
}
/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/
if($proc === 'ADD'){
    if($rtn === 0){
        //メールアドレス存在チェック処理
        $rs = fnCheckDB2MAAD($db2con,$MANAME,$MAMAIL);
        if( $rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('メールアドレス'));
            $focus = 'MAMAIL';
        }
    }
    if($rtn === 0){
        //新メールアドレスを登録処理
        $rs = fnInsertDB2MAAD($db2con,$MANAME,$MAMAIL,$MAMEMO);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}else if($proc === 'UPD'){
    if($MAMAIL !== $MAMAILOLD ){
        if($rtn === 0){
            //更新対象メールアドレス存在チェック処理
            $rs = fnCheckDB2MAAD($db2con,$MANAME,$MAMAILOLD);
            if( $rs === true){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_UPD',array('メールアドレス'));
                $focus = 'MAMAIL';
            }
        }
        if($rtn === 0){
            //更新対象メールアドレス存在チェック処理
            $rs = fnCheckDB2MAAD($db2con,$MANAME,$MAMAIL);
            if( $rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('メールアドレス'));
                $focus = 'MAMAIL';
            }
        }
    }else{
	    if($rtn === 0){
            //更新対象メールアドレス存在チェック処理
            $rs = fnCheckDB2MAAD($db2con,$MANAME,$MAMAIL);
            if( $rs === true){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_UPD',array('メールアドレス'));
                $focus = 'MAMAIL';
            }
        }	
	}
	if($rtn === 0){
		//更新対象メールアドレスを変更する処理
		$rs = fnUpdateDB2MAAD($db2con,$MANAME,$MAMAIL,$MAMEMO,$MAMAILOLD);
		if( $rs !== true){
			$rtn = 1;
			$msg = showMsg($rs);
		}
	}
}


cmDb2Close($db2con);
/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/**
*-------------------------------------------------------* 
* メールグアドレスが存在するかどうかのチェックすること
* 
* RESULT
*    01：存在する場合     true
*    02：存在しない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $MAMAIL メールアドレス
* 
*-------------------------------------------------------*
*/
function fnCheckDB2MAAD($db2con,$MANAME,$MAMAIL){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.MANAME ' ;
    $strSQL .= ' FROM DB2MAAD AS A ' ;
    $strSQL .= ' WHERE A.MANAME = ? ' ;
    $strSQL .= ' AND A.MAMAIL = ? ';

    $params = array($MANAME,$MAMAIL);

    $stmt = db2_prepare($db2con,$strSQL);
   // $stmt = false;
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* 新メールグアドレスを登録すること
* 
* RESULT
*    01：登録終了の場合   true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $MAMAIL メールアドレス
* @param String  $MAMEMO メモ
* 
*-------------------------------------------------------*
*/
function fnInsertDB2MAAD($db2con,$MANAME,$MAMAIL,$MAMEMO){

    $rs = true;
    //構文
    $strSQL  = ' INSERT INTO DB2MAAD ';
    $strSQL .= ' ( ';
    $strSQL .= ' MANAME, ';
    $strSQL .= ' MAMAIL, ';
    $strSQL .= ' MAMEMO ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?,?) ';

    $params = array($MANAME,$MAMAIL,$MAMEMO);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* メールグアドレスを更新すること
* 
* RESULT
*    01：更新終了の場合   true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $MAMAIL メールアドレス
* @param String  $MAMEMO メモ
* 
*-------------------------------------------------------*
*/
function fnUpdateDB2MAAD($db2con,$MANAME,$MAMAIL,$MAMEMO,$MAMAILOLD){
    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2MAAD ';
    $strSQL .= ' SET ';
    $strSQL .= ' MAMAIL = ?, ';
    $strSQL .= ' MAMEMO = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' MANAME = ? ';
    $strSQL .= ' AND ';
    $strSQL .= ' MAMAIL = ? ';

    $params = array($MAMAIL,$MAMEMO,$MANAME,$MAMAILOLD);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}
