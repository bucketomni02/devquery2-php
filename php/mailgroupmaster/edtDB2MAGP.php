<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['proc']))?$_POST['proc']:'';
$MGNAME = (isset($_POST['MGNAME']))?$_POST['MGNAME']:'';
$MGTEXT = (isset($_POST['MGTEXT']))?$_POST['MGTEXT']:'';

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$MGNAME = cmHscDe($MGNAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('メール配信グループの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'18',$userData[0]['WUSAUT']);//'1' => MAILGROUPMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('メール配信グループの権限'));
            }
        }
    }
}

//メールグループID必須チェック
if($rtn === 0){
    if($MGNAME  === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('グループID'));
        $focus = 'MGNAME';
    }
}
//メールグループIDの半角チェック
if($rtn === 0){
    $byte = checkByte($MGNAME);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('グループID'));
        $focus = 'MGNAME';
    }
}
//メールグループIDの桁数チェック
if($rtn === 0){
    if(!checkMaxLen($MGNAME,10)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('グループID'));
        $focus = 'MGNAME';
    }
}
//メールグループIDのメール形式チェック
if($rtn === 0){
    if (preg_match("/^([a-zA-Z0-9])*([a-zA-Z0-9\._-])+@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $MGNAME)){
        $rtn = 1;
        $msg = showMsg('FAIL_FMT',array('グループID','メール'));
        $focus = 'MGNAME';
    }
}
//メールグループ名必須チェック
if($rtn === 0){
    if($MGTEXT  === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('グループ名'));
        $focus = 'MGTEXT';
    }
}
//メールグループ名の桁数チェック
if($rtn === 0){
    if(!checkMaxLen($MGTEXT,50)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('グループ名'));
        $focus = 'MGTEXT';
    }
}

/*
*-------------------------------------------------------* 
* メールグループ更新処理
*-------------------------------------------------------*
*/

if($proc === 'ADD'){
    if($rtn === 0){
        //メールグループ存在チェック処理
        $rs = fnCheckDB2MAGP($db2con,$MGNAME);
        if($rs !== 'NOTEXIST_UPD'){
            $rtn = 1;
            $rs = 'ISEXIST';
            $msg = showMsg($rs,array('メールグループ'));
            $focus = 'MGNAME';
        }
    }
    if($rtn === 0){
        //新メールグループを登録処理
        $rs = fnInsertDB2MAGP($db2con,$MGNAME,$MGTEXT);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}else if($proc === 'UPD'){
    if($rtn === 0){
        //メールグループ存在チェック処理
        $rs = fnCheckDB2MAGP($db2con,$MGNAME);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('メールグループ'));
            $focus = 'MGNAME';
        }
    }
    if($rtn === 0){
        //メールグループを更新処理
        $rs = fnUpdateDB2MAGP($db2con,$MGNAME,$MGTEXT);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/**
*-------------------------------------------------------* 
* メールグループが更新すること
* 
* RESULT
*    01：更新終了場合     true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $MGTEXT メールグループ名
* 
*-------------------------------------------------------*
*/
function fnUpdateDB2MAGP($db2con,$MGNAME,$MGTEXT){

    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2MAGP ';
    $strSQL .= ' SET ';
    $strSQL .= ' MGTEXT = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' MGNAME = ? ';

    $params = array($MGTEXT,$MGNAME);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }

    return $rs;
}
/**
*-------------------------------------------------------* 
* 新メールグループを登録すること
* 
* RESULT
*    01：登録終了の場合     true
*    02：エラー場合         false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $MGTEXT メールグループ名
* 
*-------------------------------------------------------*
*/
function fnInsertDB2MAGP($db2con,$MGNAME,$MGTEXT){

    $rs = true;
    //構文
    $strSQL  = ' INSERT INTO DB2MAGP ';
    $strSQL .= ' ( ';
    $strSQL .= ' MGNAME, ';
    $strSQL .= ' MGTEXT ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?) ';

    $params = array($MGNAME,$MGTEXT);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* メールグループが存在するかどうかをチェックすること
* 
* RESULT
*    01：存在する場合   true
*    02：存在ない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* 
*-------------------------------------------------------*
*/
function fnCheckDB2MAGP($db2con,$MGNAME){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.MGNAME ' ;
    $strSQL .= ' FROM DB2MAGP AS A ' ;
    $strSQL .= ' WHERE A.MGNAME = ? ' ;

    $params = array($MGNAME);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_UPD';
            }else{
                $rs = true;
            }
        }
    }        
    return $rs;
}