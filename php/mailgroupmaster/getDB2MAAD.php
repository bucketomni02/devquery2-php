<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$MANAME = $_POST['MANAME'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$MAMAIL = $_POST['MAMAIL'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
$MGTEXT ='';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$MANAME = cmHscDe($MANAME);
$MAMAIL = cmHscDe($MAMAIL);

$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === 4){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('メール配信グループの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'18',$userData[0]['WUSAUT']);//'1' => mailMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('メール配信グループの権限'));
            }
        }
    }
}

if($proc == 'EDIT'){
    if($rtn === 0){
        $res = fnSelDB2MAGP($db2con,$MANAME);
        if($res['result'] === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg($res['result'],array('メールグループＩＤ'));
        }else if($res['result'] !== true ){
            $rtn = 1;
            $msg = showMsg($res['result'],array('メールグループＩＤ'));
        }else{
            $res = fnSelDB2MAAD($db2con,$MANAME,$MAMAIL);
            if($res['result'] === true){
                if(count($res['data']) === 0){
                    $rtn = 1;
                    $msg = showMsg('NOTEXIST_GET',array('メールアドレス'));
                }else{
                    $data = $res['data'];
                }
            }else{
                $rtn = 1;
                $msg = showMsg($res['result']);
            }
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $res = fnSelDB2MAGP($db2con,$MANAME);
        if($res['result'] === 'NOTEXIST_GET'){
            $rtn = 1;
            $msg = showMsg($res['result'],array('メールグループＩＤ'));
        }else if($res['result'] !== true ){
            $rtn = 1;
            $msg = showMsg($res['result'],array('メールグループＩＤ'));
        }else{
            $MGTEXT = $res['data'][0]['MGTEXT'];
            $res = fnGetAllCount($db2con,$MANAME,$MAMAIL);
            if($res['result'] === true){
                $allcount = $res['data'];
                $res = fnGetDB2MAAD($db2con,$start,$length,$sort,$sortDir,$MANAME,$MAMAIL);
                if($res['result'] === true){
                    $data = $res['data'];
                }else{
                    $rtn = 1;
                    $msg = showMsg($res['result']);
                }
            }else{
                $rtn = 1;
                $msg = showMsg($res['result']);
            }
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'MGTEXT' => cmHsc($MGTEXT),     
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* グループごとにメールアドレス取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $start
* @param String  $length
* @param String  $sortDir
* @param String  $MANAME メールグループID
* @param String  $MAMAIL メールアドレス
* 
*-------------------------------------------------------*
*/

function fnGetDB2MAAD($db2con,$start = '',$length = '',$sort = '',$sortDir = '',$MANAME,$MAMAIL){

    $data = array();
    $r = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.MAMAIL ASC ';
    }
    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM DB2MAAD as B ';
    $strSQL .= ' WHERE MANAME <> \'\' ';
    
    if($MANAME != ''){
        $strSQL .= ' AND MANAME = ?';
        array_push($params,$MANAME);
    }

    if($MAMAIL != ''){
        $strSQL .= ' AND MAMAIL like ? ';
        array_push($params,'%'.$MAMAIL.'%');
    }
    
    $strSQL .= ' ) as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* グループごとにメールアドレス全件カウント取得処理
* 
* RESULT
*    01：データ件数
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* 
*-------------------------------------------------------*
*/
function fnGetAllCount($db2con,$MANAME,$MAMAIL){
    $data = array();
    $params = array($MANAME);

    $strSQL  = ' SELECT count(A.MAMAIL) as COUNT ';
    $strSQL .= ' FROM DB2MAAD as A ' ;
    $strSQL .= ' WHERE MANAME = ? ';
    if($MAMAIL != ''){
        $strSQL .= ' AND MAMAIL like ? ';
        array_push($params,'%'.$MAMAIL.'%');
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* グループごとに対象メールアドレスデータ取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* @param String  $MAMAIL メールアドレス
* 
*-------------------------------------------------------*
*/
function fnSelDB2MAAD($db2con,$MANAME,$MAMAIL){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2MAAD as A ';
    $strSQL .= ' WHERE MANAME = ? ';
    $strSQL .= ' AND MAMAIL = ? ';

    $params = array($MANAME,$MAMAIL);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 対象メールグループデータ取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* 
*-------------------------------------------------------*
*/
function fnSelDB2MAGP($db2con,$MGNAME){

    $data = array();

    $params = array($MGNAME);

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2MAGP as A ';
    $strSQL .= ' WHERE MGNAME = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)>0){
                $data = array('result' => true,'data' => $data);
            }else{
                $data = array('result' => 'NOTEXIST_GET');
            }
        }
    }
    return $data;
}