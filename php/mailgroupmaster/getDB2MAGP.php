<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$MGNAME = $_POST['MGNAME'];
$MGTEXT = $_POST['MGTEXT'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$session = array();
$session['mgname']  = $MGNAME;
$session['mgtext']  = $MGTEXT;
$session['start']   = $start;
$session['length']  = $length;
$session['sort']    = $sort;
$session['sortDir'] = $sortDir;

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$htmlFlg = true;
$rtn = 0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$MGNAME = cmHscDe($MGNAME);
$MGTEXT = cmHscDe($MGTEXT);
$flg = (isset($_POST['FLAG']) ? $_POST['FLAG'] : '');// schedule mail gorup 

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('メール配信グループの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            if($flg !== '1'){
                $rs = cmChkKenGen($db2con,'18',$userData[0]['WUSAUT']);//'1' => mailMaster
                if($rs['result'] !== true){
                    $rtn = 2;
                    $msg =  showMsg($rs['result'],array('メール配信グループの権限'));
                }
            }
        }
    }
}

if($proc == 'EDIT'){
    if($rtn === 0){
        $res = fnSelDB2MAGP($db2con,$MGNAME);
        if($res['result'] === true){
            if(count($res['data']) === 0){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET',array('メールグループ'));
            }else{
                $data = $res['data'];
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $res = fnGetAllCount($db2con,$MGNAME,$MGTEXT);
        if($res['result'] === true){
            $allcount = $res['data'];
            $res = fnGetDB2MAGP($db2con,$MGNAME,$MGTEXT,$start,$length,$sort,$sortDir);
            if($res['result'] === true){
                $data = $res['data'];
            }else{
                $rtn = 1;
                $msg = showMsg($res['result']);
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg),
    'RTN' => $rtn,
    'MSG' => $msg,
    'mmkdata' => $flg,
    'session' => $session
);

echo(json_encode($rtn));


/*
*-------------------------------------------------------* 
* メールグループ取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $MGTEXT メールグループ名
* @param String  $start
* @param String  $length
* @param String  $sort
* @param String  $sortDir
* 
*-------------------------------------------------------*
*/

function fnGetDB2MAGP($db2con,$MGNAME,$MGTEXT,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();

    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.MGNAME ASC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM DB2MAGP as B ';
    $strSQL .= ' WHERE MGNAME <> \'\' ';
    
    if($MGNAME != ''){
        $strSQL .= ' AND MGNAME like ? ';
        array_push($params,'%'.$MGNAME.'%');
    }

    if($MGTEXT != ''){
        $strSQL .= ' AND MGTEXT like ? ';
        array_push($params,'%'.$MGTEXT.'%');
    }
    
    $strSQL .= ' ) as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* メールグループ全件カウント取得処理
* 
* RESULT
*    01：データ件数
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* 
*-------------------------------------------------------*
*/
function fnGetAllCount($db2con,$MGNAME,$MGTEXT){
    $data = array();

    $strSQL  = ' SELECT count(A.MGNAME) as COUNT ';
    $strSQL .= ' FROM DB2MAGP as A ' ;
    $strSQL .= ' WHERE MGNAME <> \'\' ';

    $params = array();

    if($MGNAME != ''){
        $strSQL .= ' AND MGNAME like ? ';
        array_push($params,'%'.$MGNAME.'%');
    }

    if($MGTEXT != ''){
        $strSQL .= ' AND MGTEXT like ? ';
        array_push($params,'%'.$MGTEXT.'%');
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}
/*
*-------------------------------------------------------* 
* 対象メールグループデータ取得処理
* 
* RESULT
*    01：結果データセット
*    02：データがない場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* 
*-------------------------------------------------------*
*/
function fnSelDB2MAGP($db2con,$MGNAME){

    $data = array();

    $params = array($MGNAME);

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2MAGP as A ';
    $strSQL .= ' WHERE MGNAME = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}