<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$DATA = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('メールサーバーの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'20',$userData[0]['WUSAUT']);//'1' => mailMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('メールサーバーの権限'));
            }
        }
    }
}

//バリデーションチェック
if($rtn === 0){
    $byte = checkByte($DATA['WMHOST']);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('SMTPサーバー'));
        $focus = 'WMHOST';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['WMHOST'],260)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('SMTPサーバー'));
        $focus = 'WMHOST';
    }
}
if($rtn === 0){
    if(!checkNum($DATA['WMPORT'])){
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array('ポート','数値'));
        $focus = 'WMPORT';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['WMPORT'],5)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('ポート'));
        $focus = 'WMPORT';
    }
}
if($rtn === 0){
    $byte = checkByte($DATA['WMUSER']);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('ユーザー'));
        $focus = 'WMUSER';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['WMUSER'],256)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('ユーザー'));
        $focus = 'WMUSER';
    }
}

if($rtn === 0){
    $byte = checkByte($DATA['WMPASE']);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('パスワード'));
        $focus = 'WMPASS';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['WMPASE'],60)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('パスワード'));
        $focus = 'WMPASS';
    }
}
if($rtn === 0){
    $byte = checkByte($DATA['WMFRAD']);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('差出人アドレス'));
        $focus = 'WMFRAD';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['WMFRAD'],256)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('差出人アドレス'));
        $focus = 'WMFRAD';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['WMFRNM'],128)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('差出人名'));
        $focus = 'WMFRNM';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['WMSUBJ'],128)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('メールタイトル'));
        $focus = 'WMSUBJ';
    }
}
if($rtn === 0){
    if(cmMer($DATA['WMBODY'])===''){
        $rtn = 1;
         $msg = showMsg('FAIL_REQ',array('本文'));
        $focus = 'WMBODY';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['WMBODY'],1536)){
        $rtn = 1;
         $msg = showMsg('FAIL_MAXLEN',array('本文'));
        $focus = 'WMBODY';
    }
}


/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/


if($rtn === 0){
    $rs = fnDeleteDB2WMAL($db2con);
    if($rs !== true){
        $msg = showMsg($rs);
    }else{
        $rs = fnInsertDB2WMAL($db2con,$DATA);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'PASS' =>$DATA
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* メールマスター削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WMAL($db2con){


    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WMAL ';

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* メールマスター更新
*-------------------------------------------------------*
*/

function fnInsertDB2WMAL($db2con,$DATA){
    $WMPASS = '';
    $WMPASE = '';
    if($DATA['WMPASE'] !== ''){
        $WMPASS = '1';
    }else{
        $WMPASS = '';
    }

    $rs = true;

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';
    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{

            //構文
            $strSQL  = ' INSERT INTO DB2WMAL ';
            $strSQL .= ' ( ';
            $strSQL .= ' WMHOST, ';
            $strSQL .= ' WMPORT, ';
            $strSQL .= ' WMUSER, ';
            $strSQL .= ' WMPASS, ';
            $strSQL .= ' WMPASE, ';
            $strSQL .= ' WMFRAD, ';
            $strSQL .= ' WMFRNM, ';
            $strSQL .= ' WMSUBJ, ';
            $strSQL .= ' WMBODY ';
            $strSQL .= ' ) ';
            $strSQL .= ' VALUES(?,?,?,?,CAST(encrypt(CAST(? AS VARCHAR(60))) AS CHAR(72) FOR BIT DATA),?,?,?,?) ';

            $params = array(
                $DATA['WMHOST'],
                cmStr0($DATA['WMPORT']),
                $DATA['WMUSER'],
                $WMPASS,
                $DATA['WMPASE'],
                $DATA['WMFRAD'],
                $DATA['WMFRNM'],
                $DATA['WMSUBJ'],
                $DATA['WMBODY']
            );

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                 $rs = 'FAIL_UPD';
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $rs = 'FAIL_UPD';
                }
            }
        }
    }
    return $rs;

}