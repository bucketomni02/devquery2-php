<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
$WMPASS = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('メールサーバーの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){          
            $rs = cmChkKenGen($db2con,'20',$userData[0]['WUSAUT']);//'1' => mailMaster       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('メールサーバーの権限'));
            }
        }
    }
}
//WMPASSのフラグチェック
if($rtn === 0){
    $rs = fnGetWMPASS($db2con);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('パスワード'));
    }else{
        $WMPASS = cmMer($rs['WMPASS']);
    }
}
if($rtn == 0){
    $res = fnGetDB2WMAL($db2con,$WMPASS);
    if($res['result'] !== true){
        $msg = showMsg($res['result']);
        $rtn = 1;
    }else{
        $data = umEx($res['data']);
        $data = $data[0];
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'DATA' => $data,
    'MSG' => $msg,
    'RTN' => $rtn,
    'WMPASS' =>$WMPASS
);

echo(json_encode($rtn));

/*
*
*WAPASSを取得
*/

function fnGetWMPASS($db2con){
        $params = array();
        $WMPASS = array();
        $strSQL  = ' SELECT A.WMPASS ';
        $strSQL .= ' FROM DB2WMAL AS A ' ;
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $WMPASS[] = $row;
                }
                 $WMPASS = $WMPASS[0]['WMPASS'];
                $data = array('result' => true,'WMPASS' => $WMPASS);
            }
        }
    return $data;
}

/*
*-------------------------------------------------------* 
* メール設定取得
*-------------------------------------------------------*
*/

function fnGetDB2WMAL($db2con,$WMPASS){
    $rtn = 0;
    $data = array();
    $params = array();
      $strWmpase  = '';
            if($WMPASS !== ''){
                $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';
                $stmt = db2_prepare($db2con,$sql);
                if($stmt === false){
                    $rtn = 1;
                    $data = array('result' => 'FAIL_SEL');
                }else{
                            $r = db2_execute($stmt);
                            if($r === false){
                                $rtn = 1;
                                $data = array('result' => 'FAIL_SEL');
                            }else{
                                $strWmpase  = 'DECRYPT_CHAR(A.WMPASE) AS WMPASE,';
                            }
                    }
            }else{
                  $strWmpase  = 'A.WMPASS AS WMPASE,';
            }
            if($rtn !== 1){
                    $strSQL  = ' SELECT A.WMHOST,A.WMPORT,A.WMUSER,A.WMPASS,'.$strWmpase;
                    $strSQL .= ' A.WMFRAD,A.WMFRNM,A.WMSUBJ,A.WMBODY ';
                    $strSQL .= ' FROM DB2WMAL AS A ' ;
                    $stmt = db2_prepare($db2con,$strSQL);
                    if($stmt === false){
                        $data = array('result' => 'FAIL_SEL');
                    }else{
                        $r = db2_execute($stmt,$params);
                        if($r === false){
                            $data = array('result' => 'FAIL_SEL');
                        }else{
                            while($row = db2_fetch_assoc($stmt)){
                                $data[] = $row;
                            }
                            $data = array('result' => true,'data' => $data);
                        }
                    }
                }
    return $data;
}