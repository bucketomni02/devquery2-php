<?php
// ファイルとディレクトリに空白又ファイル名として使用できない文字がないかをチェックして変換
function getDirfileNm($filenm){
     if($filenm !== ''){
        $filenmArr =  explode('/', $filenm);
        $filenm = '';
        $fileCnt = count($filenmArr) - 1;
        for($i = 0 ; $i < $fileCnt ; $i++){
            if(cmMer($filenmArr[$i]) != ''){
                $filenm .= '/'.replaceFilename(trim($filenmArr[$i]));
            }
        }
        $filenm .= '/'.replaceFilename(ltrim($filenmArr[$fileCnt]));
    }
    return $filenm;
}
function callExeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $wspkey, $burstItmLst,$licenseIFS){
    $wspkey      = cmMer($wspkey);
    $d1name      = cmMer($d1name);

    $rtn = 0;
    $rs;
    $csv_h       = array();
    $csv_d       = array();
    $tabledata   = array();
    
    $define      = array();
    $fdb2csv2    = array();
    $csvFlg      = '0'; //定義に対して一人でもCSV配信にチェックを入れていたら1
    $xlsFlg      = '0'; //定義に対して一人でもXLS配信にチェックを入れていたら1
    $htmlFlg     = '0'; //定義に対して一人でもHTML配信にチェックを入れていたら1
    $password    = '';
    $DGBFLG      = false;
    $DGAFLG      = false;
    
    // 登録した利用しているワークテーブルリスト
    $wrkTblLst = array();
    
    // 定義情報
    $d1webf      = '';
    $d1edkb      = '';
    $d1dire        = '';
    $d1diex        = '';
    $d1soex        = '';
    $soctfl      = '';
    $rdbnm       = '';
    $d1tros      = 0; //template row
    $d1tmpf      = '';
    $templateFlg = true;
    $seigyoFlg   = 0;
    $ZEROMAIL    = '';
    //ウェブフラグ
    $fdb2csv1data      = FDB2CSV1_DATA($db2con, $d1name);
    $fdb2csv1data      = umEx($fdb2csv1data['data']);
    $fdb2cav1data      = $fdb2csv1data[0];
    if ($fdb2csv1data['result'] !== true) {
        $rtn = 1;
    } else {
        $rdbnm              = $fdb2csv1data['D1RDB'];
        $d1webf             = $fdb2csv1data['D1WEBF'];
        $d1cflg             = $fdb2csv1data['D1CFLG'];
        // 制御取得の場合バースアイテムカウントの場合、フラグを【''】にするため
        $TMP_D1CFLG         = $D1CFLG ;
        // ｃｓｖの出力先パス
        $d1dire             = getDirfileNm($fdb2csv1data['D1DIRE']);
         // Excelの出力先パス
        $d1diex             = getDirfileNm($fdb2csv1data['D1DIEX']);
        $d1edkb             = $fdb2csv1data['D1EDKB'];
        $d1tmpf             = cmHsc($fdb2csv1data['D1TMPF']);
        $d1tmpf             = um($d1tmpf);
        $ZEROMAIL           = $fdb2csv1data['D10MAL'] === 1 ? 1 : '';
        //    テンプレートチェック開始
        $templateFlgErrCount = 0;
        if ($d1tmpf !== '') {
            $d1tros = um($fdb2csv1data['D1TROS']);
            $ext    = getExt($d1name);
            if (file_exists("./template/" . $d1name . '.' . $ext)) {
                $templateFlg = true;
            } else {
                $templateFlg = false;
            }
        }
        //    テンプレートチェック終了
    }
    if($rtn !== 1){
        $fdb2csv1 = array();
        $fdb2csv1['D1NAME'] = $d1name;
        $fdb2csv1['RDBNM']  = $RDBNM;
        $fdb2csv1['D1WEBF'] = $d1webf;
        $fdb2csv1['D1CFLG'] = $d1cflg;
        $fdb1csv1['D1DIRW'] = $d1dire;
        $fdb2csv1['D1DIEX'] = $d1diex;
        $fdb2csv1['D1EDKB'] = $d1edkb;
        $fdb2csv1['D1TMPF'] = $d1tmpf;
        $fdb2csv1['D1TROS'] = $D1TROS;
        $fdb2csv1['ZEROMAIL'] = $ZEROMAIL;
    }
    if($rtn !== 1){
        // ピボットのメールの場合出力先パース
        if($wspkey != ''){
            if($licenseIFS){
                $rsDb2wsoc = getDB2WSOC($d1name,$wspkey);
                if($rsDb2wsoc !== ''){
                    $d1soex = getDirfileNm($rsDb2wsoc['SODIEX']);
                    $soctfl = $rsDb2wsoc['SOCTFL'];
                    $ZEROMAIL = $rsDb2wsoc['SO0MAL'];
                }
            }
            $fdb2csv1['D1SOEX'] = $d1soex;
            $fdb2csv1['SOCTFL'] = $soctfl;
            $fdb2csv1['ZEROMAIL'] = $ZEROMAIL;
        }else{
            //制御取得のチェック
            $rsSeigyoData = cmGetSeigyoData($db2con, $d1name);
            if ($rsSeigyoData['RESULT'] === 1) {
                $rtn = 1;
            } else {
                $fdb2csv1['SEIGYOFLG']   = $rsSeigyoData['SEIGYOFLG'];
                $fdb2csv1['RSMASTERARR'] = $rsSeigyoData['RSMASTER'];
                $fdb2csv1['RSSHUKEIARR'] = $rsSeigyoData['RSSHUKEI'];
            }
            if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                $rst = cmLevelFieldCheck($db2con, $d1name, $rsMasterArr, $rsshukeiArr);
                if ($rst['result'] !== true) {
                    //クエリー　＊showMsg($rst['result'],array('クエリー','カラム'))
                    $errMsg = showMsg($rst['result'], array(
                        'クエリー',
                        'カラム'
                    ));
                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                    $rtn = 1;
                }
            }
        }
    }
    // 定義のCL情報取得
    if($licenseCl){
        if ($rtn !== 1) {
            $rsCLChk = chkFDB2CSV1PG($db2con, $d1name);
            if ($rsCLChk['result'] !== true) {
                $rtn    = 1;
                $msg    = $rsCLChk['result'];
                //クエリー　＊CL連携情報の取得に失敗しました。
                $errMsg = showMsg('FAIL_FUNC', array(
                    'CL連携情報の取得'
                )); //"CL連携情報の取得に失敗しました。";
                fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
            } else {
                if (count($rsCLChk['data']) > 0) {
                    $fdb2csv1pg = $rsCLChk['data'];
                    /*if ($fdb2csv1pg['DGBLIB'] !== '' || $fdb2csv1pg['DGBPGM'] !== '') {
                        $DGBFLG = true;
                    }
                    if ($fdb2csv1pg['DGALIB'] !== '' || $fdb2csv1pg['DGAPGM'] !== '') {
                        $DGAFLG = true;
                    }*/
                    $fdb2csv1['DGBFLG'] = $fdb2csv1pg['DGBFLG'] === ''? false: true;
                    $fdb2csv1['DGAFLG'] = $fdb2csv1pg['DGAFLG'] === ''? false: true;
                    $fdb2csv1['FDB2CSV1PG'] = $fdb2csv1pg;
                }else{
                    $fdb2csv['DGBFLG'] = false;
                    $fdb2csv['DGBFLG'] = false;
                }
            }
        }
    }
    // クエリー実行してスケージュル用ワークテーブル取得
    if (count($burstItmLst) !== 0) {
        $noBurstFlg = false;
        if($wspkey === ''){
            if($fdb2csv1['SEIGYOFLG'] !== 1){
                if($burstItmLst[0]['WABAFLD'] === ''){
                    // バーストアイテム無しの抽出有り
                    // ワークテーブルはスケージュルのワークテーブル
                    $noBurstFlg = true;
                }else{
                    // メールデータテーブル作成してスケージュルワークテーブル作る
                    $noBurstFlg = false;
                }
            }else{
                // 制御の取得じゃない場合、バーストアイテム無視
                // ワークテーブルはスケージュルのワークテーブル
                $noBurstFlg = true;
            }
        }else{
            if($burstItmLst[0]['WABAFLD'] === ''){
                // バーストアイテム無しの抽出有り
                // ワークテーブルはスケージュルのワークテーブル
                 $noBurstFlg = true;
            }else{
               // メールデータテーブル作成してスケージュルワークテーブル作る
               $noBurstFlg = false;
            }
        }
    }
    $wrktblnm = '';
    foreach ($burstItmLst as $key => $value) {
        $mailAddLst = array();
        $mailAddLst = getMailAddlst($db2con, $burstItmLst[$key]);
        if ($mailAddLst['result'] !== true) {
            e_log(showMsg($rs['result'], array(
                'exeMail.php',
                'getMailAddlst'
            )), '1');
        } else {
            $mailAddLst = $mailAddLst['data'];
            if (cmMer($value['WABAFLD']) === '') {
                $burstItmLst[$key] = array();
            }
            if($noBurstFlg){
                if($wspkey !== ''){
                    if($key === 0){
                        $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4,$fdb2csv1,$burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm,1);
                        $wrktblnm = $rs['WRKTBLNAME'];
                    }else{
                        $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4,$fdb2csv1,$burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm);
                        $wrktblnm = $rs['WRKTBLNAME'];
                    }
                }else{
                    if($fdb2csv1['SEIGYOFLG'] == 1){
                        if($key === 0){
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4,$fdb2csv1, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm,1);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }else{
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4,$fdb2csv1, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }
                    }else{
                        if($key === 0){
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4,$fdb2csv1, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm,1);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }else{
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4,$fdb2csv1, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }
                    }
                }
            }else{
                if($wspkey !== ''){
                    if($fdb2csv1[$D1WEBF] !== '1'){
                        if($key === 0){
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm,2);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }else{
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }
                    }else{
                        if($key === 0){
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, array(), $mailAddLst, $licenseIFS,$wrktblnm,2);
                            $wrktblnm = $rs['WRKTBLNAME'];
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }else{
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }
                    }
                }else{
                    if($fdb2csv1[$D1WEBF] !== '1'){
                        if($key === 0){
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm,1);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }else{
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }
                    }else{
                        if($fdb2csv1['SEIGYOFLG'] == 1){
                            if($key === 0){
                                $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, array(), $mailAddLst, $licenseIFS,$wrktblnm,2);
                                $wrktblnm = $rs['WRKTBLNAME'];
                                $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm);
                                $wrktblnm = $rs['WRKTBLNAME'];
                            }else{
                                $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm);
                                $wrktblnm = $rs['WRKTBLNAME'];
                            }
                        }else{
                            $rs = exeQuery($db2con, $argv3, $argv1, $argv2, $licenseCl, $argv4, $burstItmLst[$key], $mailAddLst, $licenseIFS,$wrktblnm);
                            $wrktblnm = $rs['WRKTBLNAME'];
                        }
                    }
                }
            }
        }
    }
}

function exeQuery($db2con, $d1name, $nowYmd, $nowTime, $licenseCl, $wspkey,$fdb2csv1, $burstItmLst, $mailAddLst, $licenseIFS,$wrktblnm,$noBurstFlg = 0){
    $RTCD        = ' ';
    $currentDate = date('Ymd');
    $exeFlg = true;
    // WEBで作成したクエリーの場合、定義のSQL取得
    if($noBurstFlg === 0){
        if($fdb2csv1['D1WEBF'] !== 1){
            $exeflg = false;
        }else{
            if($wspkey !== ''){
                $exeFlg = false;
            }else{
                if($fdb1csv1['SEIGYOFLG'] != 1){
                    $exeFlg = false;
                }
            }
        }
    }
    if($exeFlg){
        if ($fdb2csv1['D1WEBF'] === '1') {
            // SQLで作成したクエリー
            if ($fdb2csv1['D1CFLG'] === '1') {
                //　定義実行するためのSQLクエリー条件情報取得
                if ($rtn !== 1) {
                    $res = fnGetBSQLCND($db2con, $d1name);
                    if ($res['result'] !== true) {
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                    } else {
                        $data = umEx($res['data']);
                    }
                    $cnddatArr = $data;
                    if (count($data) > 0) {
                        $cnddatArr  = cmCalendarDataParamSQL($currentDate, $cnddatArr);
                        $resChkCond = chkSQLParamData($db2con, $d1name, $cnddatArr);
                        if ($resChkCond['RTN'] > 0) {
                            $rtn = 1;
                            $msg = $resChkCond['MSG'];
                        } else {
                            $cndParamList = $resChkCond['DATA']['BASEFRMPARAMDATA'];
                        }
                    }
                    if($rtn === 1){
                        //クエリー　＊クエリーの実行準備に失敗しました
                        $errMsg = showMsg('FAIL_FUNC', array(
                            'クエリーの実行準備'
                        )); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                    }
                }
                //　定義実行するためのSQLクエリー情報取得
                if ($rtn !== 1) {
                    $resSQLInfo = getQrySQLData($db2con, $d1name);
                    if ($resSQLInfo['RTN'] !== 0) {
                        $rtn = 1;
                        $msg = $resSQLInfo['MSG'];
                    } else {
                        $qryData = $resSQLInfo['QRYDATA'];
                    }
                    if ($rtn === 1) {
                        //クエリー　＊クエリーの実行準備に失敗しました
                        $errMsg = showMsg('FAIL_FUNC', array(
                            'クエリーの実行準備'
                        )); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                    }
                }
            } else {
                // 通常で作成したクエリー
                if ($rtn !== 1) {
                    //　定義実行するための通常クエリー条件情報取得
                    $rs = fnBQRYCNDDAT($db2con, $d1name);
                    if ($rs['result'] !== true) {
                        $rtn = 1;
                        $msg = showMsg($rs['result']);
                    } else {
                        $data = $rs['data'];
                    }
                    $data = umEx($data);
                    if (count($data) > 0) {
                        $cnddatArr  = fnCreateCndData($data);
                        $cnddatArr  = cmCalendarDataParamWeb($currentDate, $cnddatArr);
                        $resChkCond = chkCondParamData($db2con, $d1name, $cnddatArr);
                        if ($resChkCond['RTN'] > 0) {
                            $rtn = 1;
                            $msg = $resChkCond['MSG'];
                        }
                    }
                    if($rtn === 1){
                        //クエリー　＊クエリーの実行準備に失敗しました
                        $errMsg = showMsg('FAIL_FUNC', array(
                            'クエリーの実行準備'
                        )); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                    }
                }
                // 定義実行するための通常クエリー情報取得
                if ($rtn !== 1) {
                    if ($wspkey === '') {
                        if ($seigyoFlg === 1) {
                            if (count($burstItmLst) === 0) {
                                $resExeSql = runExecuteSQLSEIGYORN($db2con, $d1name);
                                if ($resExeSql['RTN'] !== 0) {
                                    $rtn = 1;
                                    $msg = $resExeSql['MSG'];
                                } else {
                                    $qryData = $resExeSql;
                                    $exeSQL  = array(
                                        'EXETESTSQL' => $qryData['STREXECTESTSQL'],
                                        'EXESQL' => $qryData['STREXECSQL'],
                                        'LOGSQL' => $qryData['LOG_SQL']
                                    );
                                }
                            } else {
                                $resExeSql = runExecuteSQLSEIGYORN($db2con, $d1name, NULL, '', $burstItmLst);
                                if ($resExeSql['RTN'] !== 0) {
                                    $rtn = 1;
                                    $msg = $resExeSql['MSG'];
                                } else {
                                    $qryData = $resExeSql;
                                    $exeSQL  = array(
                                        'EXETESTSQL' => $qryData['STREXECTESTSQL'],
                                        'EXESQL' => $qryData['STREXECSQL'],
                                        'LOGSQL' => $qryData['LOG_SQL']
                                    );
                                }
                            }
                        } else {
                            $resExeSql = runExecuteSQL($db2con, $d1name);
                            if ($resExeSql['RTN'] !== 0) {
                                $rtn = 1;
                            } else {
                                $qryData = $resExeSql;
                                $exeSQL  = array(
                                    'EXETESTSQL' => $qryData['STREXECTESTSQL'],
                                    'EXESQL' => $qryData['STREXECSQL'],
                                    'LOGSQL' => $qryData['LOG_SQL']
                                );
                            }
                        }
                    } else {
                        e_log('メールピボットSQL実行:' . $d1name . 'ピボットキー' . $wspkey);
                        if (count($burstItmLst) === 0) {
                            $resExeSql = runExecuteSQLPIVOT($db2con, $d1name, $wspkey);
                            if ($resExeSql['RTN'] !== 0) {
                                $rtn = 1;
                            } else {
                                $qryData = $resExeSql;
                                $exeSQL  = array(
                                    'EXETESTSQL' => $qryData['STREXECTESTSQL'],
                                    'EXESQL' => $qryData['STREXECSQL'],
                                    'LOGSQL' => $qryData['LOG_SQL']
                                );
                            }
                        } else {
                            $resExeSql = runExecuteSQLPIVOT($db2con, $d1name, $wspkey, NULL, $burstItmLst);
                            if ($resExeSql['RTN'] !== 0) {
                                $rtn = 1;
                            } else {
                                $qryData = $resExeSql;
                                $exeSQL  = array(
                                    'EXETESTSQL' => $qryData['STREXECTESTSQL'],
                                    'EXESQL' => $qryData['STREXECSQL'],
                                    'LOGSQL' => $qryData['LOG_SQL']
                                );
                            }
                        }
                    }
                }
                if ($rtn === 1) {
                    //クエリー　＊クエリーの実行準備に失敗しました
                    $errMsg = showMsg('FAIL_FUNC', array(
                        'クエリーの実行準備'
                    )); //"クエリーの実行準備に失敗しました。";
                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                }
            }
        }
    }
    // データベース接続
    if ($fdb2csv1['D1WEBF'] === '1') {
        if($fdb2csv1['D1CFLG'] === '1' ){
            $fdb2csv1Info = $qryData['FDB2CSV1'][0];
            $libArr       = preg_split("/\s+/", $fdb2csv1Info['D1LIBL']);
            $LIBLIST      = join(' ', $libArr);
            if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                $db2LibCon = cmDb2ConLib($LIBLIST);
            } else {
                $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1Info['D1RDB'];
                $db2LibCon                     = cmDB2ConRDB($LIBLIST);
            }
        }else{
            if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                //e_log('ライブラリーリスト：'.print_r($qryData,true));
                $db2LibCon = cmDb2ConLib($qryData['LIBLIST']);
            } else {
                $_SESSION['PHPQUERY']['RDBNM'] = $qryData['SELRDB'];
                $db2LibCon                     = cmDB2ConRDB($qryData['LIBLIST']);
            }
        }
    }
    if($exeFlg){
        if($rtn !== 1){
            if ($fdb2csv1['D1WEBF'] !== '1') {
                cmInt($db2con, $RTCD, $JSEQ, $d1name);
            } else {
                // SQLの定義のCLINIT実行
                if ($fdb2csv1['D1CFLG'] === '1') {
                    if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                        if ($licenseCl === true) {
                            if ($DGBFLG === true || $DGAFLG === true) {
                                cmIntWC($db2LibCon, $RTCD, 'INT', $d1name);
                            }
                        }
                    } else {
                        $RTCD = ' ';
                    }
                } else {
                    // 通常定義のCLINIT実行
                    if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                        if ($licenseCl === true) {
                            if ($DGBFLG === true || $DGAFLG === true) {
                                cmIntWC($db2LibCon, $RTCD, 'INT', $d1name);
                            }
                        }
                    } else {
                        $RTCD = ' ';
                    }
                }
            }
            if ($RTCD == ' ') {
                // 【setCmd】
                // CL実行前処理行う
                if($licenseCl === true){
                    if ($fdb2csv1['D1WEBF'] !== '1') {
                        // 5250で作成したクエリーの実行前CL呼び出し
                        if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                            setCmd($db2con, $d1name, $fdb2csv1['D1WEBF']);
                        }
                    } else {
                        if ($fdb2csv1['D1CFLG'] === '1') {
                            // SQLで作成したクエリーの実行前CL呼び出し
                            if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                 if ($fdb2csv1['DGBFLG'] === true || $fdb2csv1['DGAFLG'] === true) {
                                    if($fdb2csv1['D1CFLG'] === '1'){
                                        setSQLCmd($db2LibCon, $d1name, $WEBF, $cnddatArr);
                                    }else{
                                        setCmd($db2LibCon, $d1name, $WEBF, $cnddatArr);
                                    }
                                }
                                if ($fdb2csv1['DGBFLG'] === true) {
                                    cmIntWC($db2LibCon, $RTCD, 'BEF', $d1name);
                                }
                            } else {
                                //RDBのCL実行呼び出し;
                                if ($fdb2csv1['DGBFLG'] === true) {
                                    $rsClExe = callClExecute($db2LibCon, $d1name, $cnddatArr, 'BEF');
                                    if ($rsClExe === 0) {
                                        $RTCD = ' ';
                                    } else {
                                        $RTCD == '9';
                                    }
                                }
                            }
                        } else {
                            // 通常で作成したクエリーの実行前CL呼び出し
                            if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                if ($DGBFLG === true || $DGAFLG === true) {
                                    setCmd($db2LibCon, $d1name, $WEBF, $cnddatArr);
                                }
                                if ($DGBFLG === true) {
                                    cmIntWC($db2LibCon, $RTCD, 'BEF', $d1name);
                                }
                            } else {
                                //RDBのCL実行呼び出し;
                                if ($DGBFLG === true) {
                                    $rsClExe = callClExecute($db2LibCon, $d1name, $cnddatArr, 'BEF');
                                    if ($rsClExe === 0) {
                                        $RTCD = ' ';
                                    } else {
                                        $RTCD == '9';
                                    }
                                }
                            }
                        }
                    }
                }
                if($RTCD !== ' '){
                    //クエリー　＊CLの事前実行に失敗しました。
                    $errMsg = showMsg('FAIL_FUNC', array(
                        'CLの事前実行'
                    )); //"CLの事前実行に失敗しました。";
                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg);
                    $rtn = 1;
                }
            }
        }
    }
    // ワークテーブル作成
    if($exeFlg){
        if($rtn !== 1){
            do {
                if($noBurstFlg === 0){
                    $dbname    = makeRandStr(10,true);
                }else{
                    $dbname    = makeRandStr(10);
                }
                e_log('スケジュールワークテーブル：'.$dbname);
                if($noBurstFlg　!==　0){
                    e_log('SCHEDULE　の　保存テーブル：'.$dbname);
                }
                $schDBname = $dbname;
                if($fdb2csc1['D1WEBF'] === '1'){
                    $systables = cmGetSystables($db2LibCon, SAVE_DB, $dbname);
                }else{
                    $systables = cmGetSystables($db2con, SAVE_DB, $dbname);
                }
            } while (count($systables) > 0);
        }
    }else{
        $dbname = $wrktblnm
    }
    
    if($rtn !== 1){
        // 5250のクエリー実行、IFS出力
        if($fdb2csv1['D1WEBF'] !== '1'){
            if($noBurstFlg　 !== 0){
                $rs      = true;
                $dataArr = umEx(cmGetFDB2CSV3($db2con, $rs, $d1name));
                $dataArr = cmCalendarDataParam($currentDate, $dataArr);
                cmSetQTEMP($db2con);
                //初期画面の検索条件でFDB2CSV3を更新
                $r  = updSearchCond($db2con, $dataArr, $d1name);
                $rs = $r[0];
                if ($rs === 1) {
                    $rtn    = 1;
                    //$msg = showMsg('FAIL_INT_BYTE');//'検索データは128バイトまで入力可能です。';
                    $errMsg = showMsg('FAIL_FUNC', array(
                        'クエリーの実行準備'
                    )); //"クエリーの実行準備に失敗しました。";
                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg);
                } else {
                    cmSetPHPQUERY($db2con);
                    cmDo($db2con, $RTCD, $JSEQ, $dbname, $d1name);
                }
                if ($RTCD === "1") {
                    $D2HED  = cmGetD2HED($db2con, $d1name, $JSEQ);
                    $rtn    = 1;
                    //$msg   = cmMer($D2HED) . 'の入力に誤りがあります。';
                    $errMsg = showMsg('FAIL_FUNC', array(
                        'クエリーの実行準備'
                    )); //"クエリーの実行準備に失敗しました。";
                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                } else if ($RTCD === "9") {
                    $rtn    = 1;
                    // $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
                    $errMsg = showMsg('FAIL_FUNC', array(
                        'クエリーの実行準備'
                    )); //"クエリーの実行準備に失敗しました。";
                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                }
            }
            //利用してワークテーブルのデータ挿入
            if($rtn !== 1){
                $schDBname = $dbname;
                $wrkTblLst[] = $dbname;
                $resIns = cmInsWTBL($db2con, $dbname);
                if ($resIns !== 0) {
                    e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                }
            }
            // 制御又ピボットの場合ワークテーブルを新しく作る
            if ($wspkey === '') {
                if ($fdb2csv1['SEIGYOFLG'] == 1) {
                    // 制御の取得
                    //e_log('黒い画面の制御データ取得：' . $dbname);
                    $rstemp = cmRecreateSeigyoTbl5250($db2con, $d1name, $dbname, '', $burstItmLst);
                    if ($rstemp !== '') {
                        $dbname = $rstemp;
                        //e_log('黒い画面の制御データ取得後：' . $dbname.'SYPS output'.$schDBname);
                    }else{
                        $rtn    = 1;
                        // $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
                        $errMsg = showMsg('FAIL_FUNC', array(
                            'クエリーの実行失敗'
                        )); //"クエリーの実行準備に失敗しました。";
                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                    }
                    //利用してワークテーブルのデータ挿入
                    if($rtn !== 1){
                        $wrkTblLst[] = $dbname;
                        $resIns = cmInsWTBL($db2con, $dbname);
                        if ($resIns !== 0) {
                            e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                        }
                    }
                }
            } else {
                //e_log('ピボットデータ取得:');
                //ピボットの取得
                $rstemp = cmRecreateRnPivotTbl5250($db2con, $d1name, $wspkey, $dbname, $burstItmLst);
                if ($rstemp !== '') {
                    $dbname = $rstemp;
                }else{
                    $rtn    = 1;
                    // $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
                    $errMsg = showMsg('FAIL_FUNC', array(
                        'クエリーの実行失敗'
                    )); //"クエリーの実行準備に失敗しました。";
                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                }
                //利用してワークテーブルのデータ挿入
                if($rtn !== 1){
                    $wrkTblLst[] = $dbname;
                    $resIns = cmInsWTBL($db2con, $dbname);
                    if ($resIns !== 0) {
                        e_log(showMsg('利用しているワークテーブルのローグ情報保存失敗。'), '1');
                    }
                }
            }
        }
    }
    if($rtn !== 1){
        if($fdb2csv1['D1WEBF'] !== 1){
            //IFS出力先に設定されたEXCELファイルが作成される。
            if($licenseIFS === true && $noBurstFlg !== 0 ){
                $curTime = getCurrentTimeStamp($db2con);
                if(cmMer($wspkey === '')){
                    if($fdb2csv1['D1DIEX'] !== ''){
                        //　クエリー実行
                        //テンプレートがないときとテンプレートが登録されて存在するときtemplateFlg がtrueになります。
                        if ($templateFlg !== true) {
                            $errMsg = showMsg('NOTEXIST_TMP', array(
                                $d1name
                            ));
                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg);
                            $templateFlgErrCount = 1; //　一回しか登録しないように
                        }else{
                            $rsCreateExl = createExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], '', $d1name, $schDBname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1DIEX'], $mailAddLst, $fdb2csv1['ZEROMAIL'], $fdb2csv1['D1WEBF'], $cnddatArr);
                            if ($rsCreateExl !== 0) {
                                $errMsg = showMsg('FAIL_SEL');
                                fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, '', $errMsg, array());
                            }
                        }
                    }
                }else{
                    //ピボット実行
                    if($fdb2csv1['D1SOEX'] !== ''){
                        if($noBurstFlg === 2){
                            $pivottemp = '';
                            $pivottemp = cmRecreateRnPivotTbl5250($db2con, $d1name, $wspkey, $schDBname, array());
                            if ($pivottemp !== '') {
                                $pvdbname = $pivottemp;
                            }else{
                                
                            }
                        }else{
                            $pvdbname = $dbname;
                        }
                        $piexfi = createPivotExcelFile($db2con, $db2LibCon, $fdb2csv1['RDBNM'], $d1name, $pvdbname, $nowYmd, $nowTime, $curTime, $fdb2csv1['D1SOEX'], $fdb2csv1['D1WEBF'], $cnddatArr,$wspkey,$fdb2csv1['SOCTFL']);
                        if ($piexfi !== 0) {
                            $errMsg = showMsg('FAIL_SEL');
                            fnCreateHistory($db2con, $d1name, $filename, $pvdbname, $nowYmd, $nowTime, '', $errMsg, array());
                        }
                    }
                }
            }
        }
    }
    // 実行ローグとクエリー実行履歴のため
    if($exeFlg){
        if ($fdb2csv2['D1WEBF'] !== '1') {
            cmInsertDB2WLOG($db2con, $logrs, 'SCHEDULE', 'D', $d1name, array(), '0');
        } else {
            cmInsertDB2WLOG($db2con, $logrs, 'SCHEDULE', 'D', $d1name, array(), '1');
        }
        fnDB2QHISExe($db2con, $d1name, 'SCHEDULE');
    }
    // メール送信ためのファイル作成開始
    if($rtn !== 1){
        // 定数　変数
        $filename       = '';
        $fdb2csv2       = array();
        $fdb2csv2_ALL   = array();
        $csv_h          = array();
        
        $filename = (cmMer($d1name) . '_' . $nowYmd . '_' . $nowTime);

        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, '');
        $rsfdb2csv2 = cmGetFDB2CSV2($db2con, $d1name, true);
        if ($rsfdb2csv2['result'] !== true) {
            e_log(showMsg($rsfdb2csv2['result'], array(
                'exeMail.php',
                'cmGetFDB2CSV2'
            )), '1');
            $rtn = 1;
        }else{
            $fdb2csv2 = umEx($rsfdb2csv2['data']);
            $rsfdb2csv2 = cmGetFDB2CSV2($db2con, $d1name, false);
            if ($rsfdb2csv2['result'] !== true) {
                e_log(showMsg($rsfdb2csv2['result'], array(
                    'exeMail.php',
                    'cmGetFDB2CSV2'
                )), '1');
                $rtn = 1;
            }else{
                $fdb2csv2_ALL = umEx($rsfdb2csv2['data']);
                //ヘッダー情報取得
                $csv_h        = cmCreateHeaderArray($fdb2csv2_ALL);
                if (cmMer($burstItmLst['WABAFLD']) !== '') {
                    $qryCol = $fdb2csv2_ALL;
                    $qryColData = array();
                    $qryCol     = umEx($qryCol['data']);
                    foreach ($qryCol as $value) {
                        array_push($qryColData, $value['D2FLD']);
                    }
                    if (!in_array($burstItmLst['WABAFLD'], $qryColData)) {
                        $burstItmLst = '';
                    }
                }
            }
        }
    }
    if($rtn !== 1){
        /***ＣＳＶ、ＥＸＣＥＬ、ＨＴＭＬのヘッダ****/
        $rownum     = '';
        $csv_header = $csv_h; // heading
        $h_fdb2csv2 = $fdb2csv2;
        if ($fdb2csv1['RDBNM'] === '' || $fdb2csv1['RDBNM'] === RDB || $fdb2csv1['RDBNM'] === RDBNAME) {
            $csv_db_count = cmGetAllCountBurstItm($db2con, $burstItmLst, $dbname, $fdb2csv1['TMP_D1CFLG']);
        } else {
            $csv_db_count = cmGetAllCountBurstItm($db2LibCon, $burstItmLst, $dbname, $fdb2csv1['TMP_D1CFLG']);
        }
        $allcount = $csv_db_count[0];
        $mailFlg = '1';
        if($allcount == 0 && $fdb2csv1['ZEROMAIL'] == 1){
            $mailFlg = '0'; // 0件データーメール配信することができません。
        }
    }
    if($rtn !== 0){
        $DB2EINSROWMAX = 0;
        if ($mailFlg !== '0') {
            //EXCEL SETTING 挿入文字//
            if ($fdb2csv1['D1EDKB'] === '2') { //check php excel setting
                $DB2ECON   = array();
                $DB2EINS   = array();
                $CNDSDATAF = array();
                $DB2ECON   = cmGetDB2ECON($db2con, $d1name);
                if ($DB2ECON['result'] === true) {
                    $DB2ECON = umEx($DB2ECON['data']);
                }
                $DB2EINS = cmGetDB2EINS($db2con, $d1name);
                if ($DB2EINS['result'] === true) {
                    $EIROWMAX = $DB2EINS['MAXEIROW'];
                    $DB2EINS  = umEx($DB2EINS['data']);
                }
                $CNDSDATAF     = cmCNDSDATA($fdb2csv1['D1WEBF'], $db2con, $d1name, array(), $fdb2csv1['D1CFLG']);
                $DB2EINSROWMAX = cmGetCountSearchData($DB2ECON, $EIROWMAX, $CNDSDATAF); //EXCEL SETTING 挿入文字カウント
            } else if ($fdb2csv1['D1EDKB'] !== '2' && $fdb2csv1['D1TMPF'] !== '' && $wspkey === '') {
                $DB2EINSROWMAX = $fdb2csv1['D1TROS'];
            }
            //確かにベダーがあるのでプラス1
            if ($fdb2csv1['D1EDKB'] === '2' || $wspkey !== '' || $fdb2csv1['D1TMPF'] === '') {
                $DB2EINSROWMAX += 1;
            }
            if (count($fdb2csv1['RSMASTERARR']) !== 0 && count($fdb2csv1['RSSHUKEIARR']) !== 0) {
                $csv_header = $csv_h; // heading
                array_unshift($csv_header, "", "");
                $h_fdb2csv2 = $fdb2csv2;
                $h_fdb2csv2 = cmArrayUnshift($h_fdb2csv2);
            }
            $csv_d  = array();
            //CSV配信チェック
            $csvFlg = chkDB2WAUTCsv($db2con, $d1name);
            if ($csvFlg['result'] === true) {
                //取得データからCSV作成
                if ($csvFlg['flg'] === '1' && $rtn !== 1) {
                    require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
                    $csv_fp = exeCsvstream($filename);
                    exeCsv($csv_fp, $csv_header, $csv_d, $h_fdb2csv2, $d1name, $filename, $mailAddLst, $nowYmd, $nowTime, true);
                }
            }
            // ピボットファイルフラグ
            $pivotFlg                = array();
            $pivotFlg['XLSFLG']      = false;
            $pivotFlg['HTMLFLG']     = false;
            $pivotFlg['MAILHTMLFLG'] = false;
            //XLS配信チェック
            $xlsCustomFlg            = '';
            $xlsFlg                  = chkDB2WAUTXls($db2con, $d1name, $wspkey);
            if ($wspkey !== '') {
                $templateFlg = true;
            }
            if ($xlsFlg['result'] === true) {
                //テンプレートがないときとテンプレートが登録されて存在するときtemplateFlg がtrueになります。
                if ($templateFlg !== true && $xlsFlg['flg'] === '1') {
                    if ($templateFlgErrCount === 0) {
                        $errMsg = showMsg('NOTEXIST_TMP', array(
                            $d1name
                        ));
                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg);
                        $templateFlgErrCount = 1; //　一回しか登録しないように
                    }
                }
                //取得データからExcel作成
                if ($xlsFlg['flg'] === '1' && $rtn !== 1 && $templateFlg === true) {
                    if ($wspkey !== '') { //クエリーのエクセル作成
                        $pivotFlg['XLSFLG'] = true;
                        $ISEXCELMAXLEN      = 0;
                    } else {
                        //e_log("templateFlg create is true");
                        $resXlsCustom = '';
                        if ($fdb2csv1['D1EDKB'] === '1') {
                            $xlsCustomFlg = $D1EDKB;
                        }
                        // かステムテンプレートじゃないの場合
                        if ($xlsCustomFlg === '') {
                            $sheet    = '';
                            $book     = '';
                            $excel_fp = '';
                            $filepath = BASE_DIR . '/php/xls/';
                            //  $filepath = '/QNTC/192.168.1.12/TEST/';
                            $D1TMPF   = '';
                            $ext      = getExt($d1name);
                            //MAX件数のためカウント
                            if ($ext !== '') {
                                $EXCELMAXLEN  = ($ext === 'xlsm' || $ext === 'xlsx') ? 1048576 : (($ext === 'xls') ? 65536 : 0);
                                $EMAXLEN_FILE = $ext;
                            } else {
                                if (EXCELVERSION === '2007') {
                                    $EXCELMAXLEN  = 1048576;
                                    $EMAXLEN_FILE = 'xlsx';
                                } else if (EXCELVERSION === '2003' || EXCELVERSION === 'html') {
                                    $EXCELMAXLEN  = 65536;
                                    $EMAXLEN_FILE = 'xls';
                                }
                            }
                            if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                $DB2EINSROWMAX += ($allcount === 0 || $allcount === '' || cmMer($rsMasterArr[0]['DCSUMF']) === '1') ? 0 : $allcount;
                                $DB2EINSROWMAX += cmGetALLCOUNTSEIKYO($db2con, $d1name, $dbname, $rsMasterArr, $fdb2csv2, '', $burstItmLst);
                            } else {
                                $DB2EINSROWMAX += ($allcount === 0 || $allcount === '') ? 0 : $allcount;
                            }
                            //e_log('in exeMailSend.php $DB2EINSROWMAX=>'.$DB2EINSROWMAX.'$EXCELMAXLEN=>'.$EXCELMAXLEN);
                            if ($DB2EINSROWMAX > $EXCELMAXLEN) { //check maximum length of EXCEL
                                $ISEXCELMAXLEN          = 1;
                                $errMsg                 = showMsg('FAIL_EXEMAXLEN', array(
                                    $EMAXLEN_FILE,
                                    $EXCELMAXLEN
                                ));
                                $mailAddLst[0]['WAXLS'] = '9';
                                fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                            } else {
                                $ISEXCELMAXLEN = 0;
                                if (EXCELVERSION == '2003' || EXCELVERSION == '2007' || $ext !== '') {
                                    if ($ext !== '') {
                                    } else if (EXCELVERSION == '2003') {
                                        $ext = 'xls';
                                    } else if (EXCELVERSION == '2007') {
                                        $ext = 'xlsx';
                                    }
                                    $excelname = $filename . '.' . $ext;
                                    $sheet     = php_excelCreateHeading($d1name);
                                    $DB2WCOL   = array();
                                    $book      = $sheet['BOOK'];
                                    $sheet     = $sheet['SHEET'];
                                } else if (EXCELVERSION == 'html' || $ext === '') {
                                    $excelname = $filename . '.xls';
                                    if (!file_exists($filepath . $excelname)) {
                                        touch($filepath . $excelname);
                                    }
                                    $excel_fp = fopen(BASE_DIR . '/php/xls/' . $excelname, 'w');
                                    //   $excel_fp = fopen('/QNTC/192.168.1.12/TEST/' . $excelname, 'w');
                                    excelCreateHeading($excel_fp, $DB2ECON, $DB2EINS, $CNDSDATAF);
                                }
                                $rownum = exeExcel($excel_fp, $csv_header, $csv_d, $h_fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $sheet, $rownum, '0', $cnddatArr);
                            }
                        } else {
                            require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
                            $csv_temp = exeCsvstream($filename, $xlsCustomFlg);
                            exeCsv($csv_temp, $csv_header, $csv_d, $h_fdb2csv2, $d1name, $filename, $mailAddLst, $nowYmd, $nowTime, true, $xlsCustomFlg);
                        }
                    }
                }
            }
            //HTML配信チェック
            if($rtn !== 1){
                $htmlFlg = chkDB2WAUTHtml($db2con, $d1name, $wspkey);
                if ($htmlFlg['result'] === true) {
                    //取得データからHTML作成
                    if ($htmlFlg['flg'] === '1') {
                        $filepath  = BASE_DIR . '/php/html/';
                        $excelname = $filename . '.html';
                        if (!file_exists($filepath . $excelname)) {
                            touch($filepath . $excelname);
                        }
                        $html_d = $csv_d;
                        $wspkey = cmMer($wspkey);
                        if ($wspkey !== '') { //クエリーのエクセル作成
                            $pivotFlg['HTMLFLG'] = true;
                            //    $html_d = $allcount;
                        } else {
                            $html_fp = fopen($filepath . $excelname, 'w');
                            excelCreateHeading($html_fp, $DB2ECON, $DB2EINS, $CNDSDATAF);
                            exeHtml($html_fp, $csv_header, $html_d, $h_fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $cnddatArr);
                        }
                    }
                }
            }

            //HTMLフォーマット配信チェック
            if($rtn !== 1){
                $mailHtmlFlg = chkDB2WATYPEHtml($db2con, $d1name, $wspkey); //正徳データーからHTML作成
                if ($mailHtmlFlg['result'] === true) {
                    if ($mailHtmlFlg['flg'] === '1' && $rtn !== 1) {
                        if (cmMer($wspkey) !== '') {
                            $pivotFlg['MAILHTMLFLG'] = true;
                        }
                    }
                }
            }
        }
    }
}





                    //file名作成
                    $filename = (cmMer($d1name) . '_' . $nowYmd . '_' . $nowTime);
                    ////実行履歴(空)を作成
                    fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, '');
                    //FDB2CSV2取得
                    $fdb2csv2     = cmGetFDB2CSV2($db2con, $d1name, true);
                    $fdb2csv2     = umEx($fdb2csv2['data']);
                    $fdb2csv2_ALL = cmGetFDB2CSV2($db2con, $d1name, false);
                    $fdb2csv2_ALL = umEx($fdb2csv2_ALL['data']);
                    //ヘッダー情報取得
                    $csv_h        = cmCreateHeaderArray($fdb2csv2);
                    //クエリー結果全件取得
                    //$csv_d = cmGetDbFile($db2con,$fdb2csv2,$dbname,'','','','','','',array(),true);
                    //if($wspkey === ''){                
                    if (cmMer($burstItmLst['WABAFLD']) !== '') {
                        $qryCol = cmGetFDB2CSV2($db2con, $d1name);
                        if ($qryCol['result'] !== true) {
                            e_log(showMsg($qryCol, array(
                                'exeMail.php',
                                'fnGetFDB2CSV2'
                            )), '1');
                        } else {
                            $qryColData = array();
                            $qryCol     = umEx($qryCol['data']);
                            foreach ($qryCol as $value) {
                                array_push($qryColData, $value['D2FLD']);
                            }
                        }
                        if (!in_array($burstItmLst['WABAFLD'], $qryColData)) {
                            $burstItmLst = '';
                        }
                    }
                    /***ＣＳＶ、ＥＸＣＥＬ、ＨＴＭＬのヘッダ****/
                    $rownum     = '';
                    $csv_header = $csv_h; // heading
                    $h_fdb2csv2 = $fdb2csv2;
                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                        $csv_db_count = cmGetAllCountBurstItm($db2con, $burstItmLst, $dbname, $TMP_D1CFLG);
                    } else {
                        $csv_db_count = cmGetAllCountBurstItm($db2LibCon, $burstItmLst, $dbname, $TMP_D1CFLG);
                    }
                    $allcount = $csv_db_count[0];
                    $DB2WSOC  = chkDB2WSOC($db2con, $d1name, $wspkey);
                    //e_log("観点１20170525".$DB2WSOC['flg'].'wspkey'.$wspkey);
                    if ($DB2WSOC['result'] === true) {
                        $mailFlg = '1';

                        /**0件データーを出すことをチェック**/
                        if ($allcount === 0 && $DB2WSOC['flg'] === '1') {
                            $mailFlg = '0'; // 0件データーメール配信することができません。
                        }
                        //e_log("mailFlg" . $mailFlg);
                        $DB2EINSROWMAX = 0;
                        if ($mailFlg !== '0') {
                            //EXCEL SETTING 挿入文字//
                            if ($d1edkb === '2') { //check php excel setting
                                $DB2ECON   = array();
                                $DB2EINS   = array();
                                $CNDSDATAF = array();
                                $DB2ECON   = cmGetDB2ECON($db2con, $d1name);
                                if ($DB2ECON['result'] === true) {
                                    $DB2ECON = umEx($DB2ECON['data']);
                                }
                                $DB2EINS = cmGetDB2EINS($db2con, $d1name);
                                if ($DB2EINS['result'] === true) {
                                    $EIROWMAX = $DB2EINS['MAXEIROW'];
                                    $DB2EINS  = umEx($DB2EINS['data']);
                                }
                                $CNDSDATAF     = cmCNDSDATA($WEBF, $db2con, $d1name, array(), $D1CFLG);
                                $DB2EINSROWMAX = cmGetCountSearchData($DB2ECON, $EIROWMAX, $CNDSDATAF); //EXCEL SETTING 挿入文字カウント
                            } else if ($d1edkb !== '2' && $d1tmpf !== '' && cmMer($wspkey) === '') {
                                $DB2EINSROWMAX = $D1TROS;
                            }
                            //確かにベダーがあるのでプラス1
                            if ($d1edkb === '2' || cmMer($wspkey) !== '' || $d1tmpf === '') {
                                $DB2EINSROWMAX += 1;
                            }
                        }
                    }
                    if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                        $csv_header = $csv_h; // heading
                        array_unshift($csv_header, "", "");
                        $h_fdb2csv2 = $fdb2csv2;
                        $h_fdb2csv2 = cmArrayUnshift($h_fdb2csv2);
                    }
                    $csv_d  = array();
                    //CSV配信チェック
                    $csvFlg = chkDB2WAUTCsv($db2con, $d1name);
                    if ($csvFlg['result'] === true) {
                        //取得データからCSV作成
                        if ($csvFlg['flg'] === '1' && $rtn !== 1) {
                            require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
                            $csv_fp = exeCsvstream($filename);
                            exeCsv($csv_fp, $csv_header, $csv_d, $h_fdb2csv2, cmMer($d1name), $filename, $mailAddLst, $nowYmd, $nowTime, true);
                        }
                    }
                    /**ピボットファイルフラグ**/
                    $pivotFlg                = array();
                    $pivotFlg['XLSFLG']      = false;
                    $pivotFlg['HTMLFLG']     = false;
                    $pivotFlg['MAILHTMLFLG'] = false;
                    //XLS配信チェック
                    $xlsCustomFlg            = '';
                    $xlsFlg                  = chkDB2WAUTXls($db2con, $d1name, $wspkey);
                    $wspkey                  = cmMer($wspkey);
                    if ($wspkey !== '') {
                        $templateFlg = true;
                    }
                    if ($xlsFlg['result'] === true) {
                        //テンプレートがないときとテンプレートが登録されて存在するときtemplateFlg がtrueになります。
                        if ($templateFlg !== true && $xlsFlg['flg'] === '1') {
                            if ($templateFlgErrCount === 0) {
                                $errMsg = showMsg('NOTEXIST_TMP', array(
                                    $d1name
                                ));
                                fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg);
                                $templateFlgErrCount = 1; //　一回しか登録しないように
                            }
                        }
                        //取得データからExcel作
                        if ($xlsFlg['flg'] === '1' && $rtn !== 1 && $templateFlg === true) {
                            if ($wspkey !== '') { //クエリーのエクセル作成
                                $pivotFlg['XLSFLG'] = true;
                                $ISEXCELMAXLEN      = 0;
                            } else {
                                //e_log("templateFlg create is true");
                                $resXlsCustom = FDB2CSV1_DATA($d1name);
                                $resXlsCustom = $resXlsCustom['data'][0];
                                $D1EDKB       = cmMer($resXlsCustom['D1EDKB']);
                                if ($D1EDKB === '1') {
                                    $xlsCustomFlg = $D1EDKB;
                                }
                                // かステムテンプレートじゃないの場合
                                if ($xlsCustomFlg === '') {
                                    $sheet    = '';
                                    $book     = '';
                                    $excel_fp = '';
                                    $filepath = BASE_DIR . '/php/xls/';
                                    //  $filepath = '/QNTC/192.168.1.12/TEST/';
                                    $D1TMPF   = '';
                                    $ext      = getExt($d1name);
                                    //MAX件数のためカウント
                                    if ($ext !== '') {
                                        $EXCELMAXLEN  = ($ext === 'xlsm' || $ext === 'xlsx') ? 1048576 : (($ext === 'xls') ? 65536 : 0);
                                        $EMAXLEN_FILE = $ext;
                                    } else {
                                        if (EXCELVERSION === '2007') {
                                            $EXCELMAXLEN  = 1048576;
                                            $EMAXLEN_FILE = 'xlsx';
                                        } else if (EXCELVERSION === '2003' || EXCELVERSION === 'html') {
                                            $EXCELMAXLEN  = 65536;
                                            $EMAXLEN_FILE = 'xls';
                                        }
                                    }
                                    if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                        $DB2EINSROWMAX += ($allcount === 0 || $allcount === '' || cmMer($rsMasterArr[0]['DCSUMF']) === '1') ? 0 : $allcount;
                                        $DB2EINSROWMAX += cmGetALLCOUNTSEIKYO($db2con, $d1name, $dbname, $rsMasterArr, $fdb2csv2, '', $burstItmLst);
                                    } else {
                                        $DB2EINSROWMAX += ($allcount === 0 || $allcount === '') ? 0 : $allcount;
                                    }
                                    //e_log('in exeMailSend.php $DB2EINSROWMAX=>'.$DB2EINSROWMAX.'$EXCELMAXLEN=>'.$EXCELMAXLEN);
                                    if ($DB2EINSROWMAX > $EXCELMAXLEN) { //check maximum length of EXCEL
                                        $ISEXCELMAXLEN          = 1;
                                        $errMsg                 = showMsg('FAIL_EXEMAXLEN', array(
                                            $EMAXLEN_FILE,
                                            $EXCELMAXLEN
                                        ));
                                        $mailAddLst[0]['WAXLS'] = '9';
                                        fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                                    } else {
                                        $ISEXCELMAXLEN = 0;
                                        if (EXCELVERSION == '2003' || EXCELVERSION == '2007' || $ext !== '') {
                                            if ($ext !== '') {
                                            } else if (EXCELVERSION == '2003') {
                                                $ext = 'xls';
                                            } else if (EXCELVERSION == '2007') {
                                                $ext = 'xlsx';
                                            }
                                            $excelname = $filename . '.' . $ext;
                                            $sheet     = php_excelCreateHeading($d1name);
                                            $DB2WCOL   = array();
                                            $book      = $sheet['BOOK'];
                                            $sheet     = $sheet['SHEET'];
                                        } else if (EXCELVERSION == 'html' || $ext === '') {
                                            $excelname = $filename . '.xls';
                                            if (!file_exists($filepath . $excelname)) {
                                                touch($filepath . $excelname);
                                            }
                                            $excel_fp = fopen(BASE_DIR . '/php/xls/' . $excelname, 'w');
                                            //   $excel_fp = fopen('/QNTC/192.168.1.12/TEST/' . $excelname, 'w');
                                            excelCreateHeading($excel_fp, $DB2ECON, $DB2EINS, $CNDSDATAF);
                                        }
                                        $rownum = exeExcel($excel_fp, $csv_header, $csv_d, $h_fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $sheet, $rownum, '0', $cnddatArr);
                                    }
                                } else {
                                    require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
                                    $csv_temp = exeCsvstream($filename, $xlsCustomFlg);
                                    exeCsv($csv_temp, $csv_header, $csv_d, $h_fdb2csv2, cmMer($d1name), $filename, $mailAddLst, $nowYmd, $nowTime, true, $xlsCustomFlg);
                                }
                            }
                        }
                    }
                    //HTML配信チェック
                    $htmlFlg = chkDB2WAUTHtml($db2con, $d1name, $wspkey);
                    //e_log("************pivot->".$wspkey."html flg *****end of excel create".print_r($htmlFlg,true));
                    if ($htmlFlg['result'] === true) {
                        //取得データからExcel作成
                        if ($htmlFlg['flg'] === '1' && $rtn !== 1) {
                            $filepath  = BASE_DIR . '/php/html/';
                            $excelname = $filename . '.html';
                            if (!file_exists($filepath . $excelname)) {
                                touch($filepath . $excelname);
                            }
                            $html_d = $csv_d;
                            $wspkey = cmMer($wspkey);
                            if ($wspkey !== '') { //クエリーのエクセル作成
                                $pivotFlg['HTMLFLG'] = true;
                                //    $html_d = $allcount;
                            } else {
                                $html_fp = fopen($filepath . $excelname, 'w');
                                excelCreateHeading($html_fp, $DB2ECON, $DB2EINS, $CNDSDATAF);
                                exeHtml($html_fp, $csv_header, $html_d, $h_fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, true, $cnddatArr);
                            }
                        }
                    }
                    //HTMLフォーマット配信チェック
                    $mailHtmlFlg = chkDB2WATYPEHtml($db2con, $d1name, $wspkey); //正徳データーからHTML作成
                    if ($mailHtmlFlg['result'] === true) {
                        if ($mailHtmlFlg['flg'] === '1' && $rtn !== 1) {
                            if (cmMer($wspkey) !== '') {
                                $pivotFlg['MAILHTMLFLG'] = true;
                            }
                        }
                    }
                    $lenArr        = cmTotalLen($allcount);
                    $rtnMail       = 0;
                    $qryHtmlCount  = 0;
                    $breakFlg      = false;
                    $lenCount      = count($lenArr);
                    $htmltabledata = array();
                    $wspkey        = cmMer($wspkey);
                    $tbdata        = '';
                    if ($wspkey === '') { //ピボットじゃない場合
                        for ($idx = 0; $idx < $lenCount; $idx++) {
                            if ($rtnMail !== 1) { //ロープしながら、エラーが出たら、rtnMailは１になって続かない
                                if (count($rsMasterArr) === 0 && count($rsshukeiArr) === 0) {
                                    if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                        $csv_d = cmGetDbFileBurstItm($db2con, $fdb2csv2, $burstItmLst, $dbname, $D1CFLG, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', '', array(), true, $WEBF, $d1name, $cnddatArr);
                                    } else {
                                        $csv_d = cmGetDbFileBurstItm($db2LibCon, $fdb2csv2, $burstItmLst, $dbname, $D1CFLG, $lenArr[$idx][0], $lenArr[$idx][1], '', '', '', '', array(), true, $WEBF, $d1name, $cnddatArr);
                                    }
                                }
                                //                    $csv_d   = cmGetDbFileBurstItm($db2con, $fdb2csv2, $burstItmLst, $dbname, 0, 10, '', '', '', '', array(), true);
                                // カスタムテンプレートチェックフラグ
                                //$DB2WSOC = chkDB2WSOC($db2con, $d1name, $wspkey);
                                //e_log("このへんのデータ20170525".$DB2WSOC['flg']);
                                if ($DB2WSOC['result'] === true) {
                                    $mailFlg = '1';
                                    /**0件データーを出すことをチェック**/
                                    //$csv_d_count = count($csv_d);
                                    if ($allcount === 0 && $DB2WSOC['flg'] === '1') {
                                        $mailFlg = '0'; // 0件データーメール配信することができません。
                                    }
                                    if ($mailFlg !== '0') {
                                        if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                            if ($RDBNM === '' || $RDBNM === RDB || $RDBNM === RDBNAME) {
                                                $tabledata = fnDataShukei($db2con, $fdb2csv2, $fdb2csv2_ALL, $d1name, $dbname, $db2wcol, $rsMasterArr, $rsshukeiArr, $burstItmLst, $lenArr[$idx][0], $lenArr[$idx][1], $WEBF, $d1name, $cnddatArr, $allcount);
                                            } else {
                                                $tabledata = fnDataShukei($db2LibCon, $fdb2csv2, $fdb2csv2_ALL, $d1name, $dbname, $db2wcol, $rsMasterArr, $rsshukeiArr, $burstItmLst, $lenArr[$idx][0], $lenArr[$idx][1], $WEBF, $d1name, $cnddatArr, $allcount);
                                            }
                                            //$tabledata    = fnDataShukei($db2con, $fdb2csv2, $fdb2csv2_ALL, $d1name, $dbname, $db2wcol, $rsMasterArr, $rsshukeiArr, $burstItmLst);
                                            $csv_h_shukei = $csv_h;
                                            array_unshift($csv_h_shukei, "", "");
                                            $fdb2csv2_shukei = $fdb2csv2;
                                            $fdb2csv2_shukei = cmArrayUnshift($fdb2csv2_shukei);
                                        }
                                        //CSV配信チェック
                                        //$csvFlg = chkDB2WAUTCsv($db2con, $d1name);
                                        if ($csvFlg['result'] === true) {
                                            //取得データからCSV作成
                                            if ($csvFlg['flg'] === '1' && $rtn !== 1) {
                                                if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                                    exeCsv($csv_fp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, cmMer($d1name), $filename, $mailAddLst, $nowYmd, $nowTime, false);
                                                } else {
                                                    exeCsv($csv_fp, $csv_h, $csv_d, $fdb2csv2, cmMer($d1name), $filename, $mailAddLst, $nowYmd, $nowTime, false);
                                                }
                                            }
                                        } else {
                                            $rtnMail = 1;
                                            e_log(showMsg($csvFlg['result'], array(
                                                'exeMail.php',
                                                'chkDB2WAUTCsv'
                                            )), '1');
                                            //クエリー　＊CSV配信情報の取得に失敗しました。
                                            $errMsg = showMsg('FAIL_FUNC', array(
                                                'CSV配信情報の取得'
                                            )); //" CSV配信情報の取得に失敗しました。";
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg);
                                        }
                                        //XLS配信チェック
                                        // $xlsFlg = chkDB2WAUTXls($db2con, $d1name, $wspkey);
                                        if ($xlsFlg['result'] === true) {
                                            //テンプレートがないときとテンプレートが登録されて存在するときtemplateFlg がtrueになります。
                                            if ($templateFlg !== true && $xlsFlg['flg'] === '1') {
                                                if ($templateFlgErrCount === 0) {
                                                    $errMsg = showMsg('NOTEXIST_TMP', array(
                                                        $d1name
                                                    ));
                                                    fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg);
                                                    $templateFlgErrCount = 1; //　一回しか登録しないように
                                                }
                                            }
                                            //取得データからExcel作成
                                            if ($xlsFlg['flg'] === '1' && $rtn !== 1 && $templateFlg === true) {
                                                /*     $resXlsCustom = FDB2CSV1_DATA($d1name);
                                                $resXlsCustom = $resXlsCustom['data'][0];
                                                $D1EDKB = cmMer($resXlsCustom['D1EDKB']);*/
                                                if ($D1EDKB === '1') {
                                                    $xlsCustomFlg = $D1EDKB;
                                                }
                                                // かステムテンプレートじゃないの場合
                                                if ($xlsCustomFlg === '') {
                                                    if ($ISEXCELMAXLEN === 0) { //MAX件数のためカウントのチェック
                                                        if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                                            $rownum = exeExcel($excel_fp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename, $wspkey, $d1name, $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $sheet, $rownum, '1', $cnddatArr);
                                                        } else {
                                                            $rownum = exeExcel($excel_fp, $csv_h, $csv_d, $fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, $burstItmLst, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $sheet, $rownum, '1', $cnddatArr);
                                                        }
                                                    }
                                                } else {
                                                    if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                                        exeCsv($csv_temp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, cmMer($d1name), $filename, $mailAddLst, $nowYmd, $nowTime, false, $xlsCustomFlg);
                                                    } else {
                                                        exeCsv($csv_temp, $csv_h, $csv_d, $fdb2csv2, cmMer($d1name), $filename, $mailAddLst, $nowYmd, $nowTime, false, $xlsCustomFlg);
                                                    }
                                                }
                                            }
                                        } else {
                                            $rtnMail = 1;
                                            e_log(showMsg($xlsFlg['result'], array(
                                                'exeMail.php',
                                                'chkDB2WAUTXls'
                                            )), '1');
                                            //クエリー　＊EXCEL配信情報の取得に失敗しました。
                                            $errMsg = " EXCEL配信情報の取得に失敗しました。";
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg);
                                        }
                                        //HTML配信チェック
                                        //   $htmlFlg = chkDB2WAUTHtml($db2con, $d1name, $wspkey);
                                        if ($htmlFlg['result'] === true) {
                                            //取得データからExcel作成
                                            if ($htmlFlg['flg'] === '1' && $rtn !== 1) {
                                                if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                                    exeHtml($html_fp, $csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename, $wspkey, $d1name, $db2con, $dbname, array(), $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $cnddatArr);
                                                } else {
                                                    exeHtml($html_fp, $csv_h, $csv_d, $fdb2csv2, $filename, $wspkey, $d1name, $db2con, $dbname, $burstItmLst, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, false, $cnddatArr);
                                                }
                                            }
                                        } else {
                                            $rtnMail = 1;
                                            e_log(showMsg($htmlFlg['result'], array(
                                                'exeMail.php',
                                                'chkDB2WAUTHtml'
                                            )), '1');
                                            //クエリー　＊HTML配信情報の取得に失敗しました。
                                            $errMsg = " HTML配信情報の取得に失敗しました。";
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg);
                                        }
                                    }
                                } else {
                                    e_log(showMsg($DB2WSOC['result'], array(
                                        'exeMail.php',
                                        'chkDB2WSOC'
                                    )), '1');
                                }
                                //e_log('mailFlg check mail flg->' . $rtn . '*****->' . $mailFlg);
                                if ($rtn !== 1 && $mailFlg !== '0') {
                                    if ($mailHtmlFlg['result'] === true) {
                                        //正徳データーからHTML作成
                                        if ($mailHtmlFlg['flg'] === '1' && $rtn !== 1) {
                                            /*if (cmMer($wspkey) !== '') {
                                            e_log("htmlformat create");
                                            $pivotFlg['MAILHTMLFLG'] = true;
                                            } else {*/
                                            /*if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                            $htmltabledata = $tabledata;
                                            } else {
                                            $htmltabledata = $csv_d;
                                            }*/
                                            $lastRow = false;
                                            if ($idx === $lenCount - 1) {
                                                $lastRow = true;
                                            }
                                              if ($breakFlg !== true) {
                                                    if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                                                        //                                                    $tbdata = exeHtmlMail($csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename,$WEBF);
                                                        $rtnTbdata    = exeHtmlMail($csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename, $WEBF, $idx, $lastRow, $qryHtmlCount);
                                                        $qryHtmlCount = $rtnTbdata['QRYHTMLCOUNT'];
                                                        $breakFlg     = $rtnTbdata['HTMLCREATEBREAK'];
                                                        $tbdata .= $rtnTbdata['TBDATA'];
                                                    } else {
                                                        //                                                    $tbdata = exeHtmlMail($csv_h, $csv_d, $fdb2csv2, $filename,$WEBF);
                                                        $rtnTbdata    = exeHtmlMail($csv_h, $csv_d, $fdb2csv2, $filename, $WEBF, $idx, $lastRow, $qryHtmlCount);
                                                        $qryHtmlCount = $rtnTbdata['QRYHTMLCOUNT'];
                                                        $breakFlg     = $rtnTbdata['HTMLCREATEBREAK'];
                                                        $tbdata .= $rtnTbdata['TBDATA'];
                                                    }
                                                }
                                            if ($breakFlg === true) {
                                                if ($xlsFlg['flg'] !== '1' && $csvFlg['flg'] !== '1' && $htmlFlg['flg'] !== '1') {
                                                    break; // break from querycount loop
                                                }
                                            }
                                            //}
                                        }
                                    } else {
                                        e_log(showMsg($mailHtmlFlg['result'], array(
                                            'exeMail.php',
                                            'chkDB2WATYPEHtml'
                                        )), '1');
                                        //クエリー　＊メール形式情報の取得に失敗しました。
                                        $errMsg = showMsg('FAIL_FUNC', array(
                                            'メール形式情報の取得'
                                        )); //"メール形式情報の取得に失敗しました。";
                                        fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg);
                                    }
                                }
                            }
                            unset($csv_d);
                            unset($tabledata);
                            
                        }
                        if (cmMer($wspkey) === '') {
                            if ($csvFlg['flg'] === '1') {
                                fclose($csv_fp); //csv file close
                                $csvname  = $filename . '.csv';
                                $filepath = BASE_DIR . '/php/csv/';
                                makeCopyFile($filepath, $csvname, $mailAddLst, $nowYmd, $nowTime, '.csv');
                            }
                            if ($xlsFlg['flg'] === '1') {
                                // かステムテンプレートじゃないの場合
                                if ($xlsCustomFlg === '') {
                                    if ($ISEXCELMAXLEN === 0) { //MAX件数のためカウントのチェック
                                        $filepath = BASE_DIR . '/php/xls/';
                                        //  $filepath = '/QNTC/192.168.1.12/TEST/';
                                        if (EXCELVERSION == 'html' && $ext === '') {
                                            $excelname = $filename . '.xls';
                                            fwrite($excel_fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                                            fclose($excel_fp);
                                            $ext = '.xls';
                                        } else {
                                            if ((EXCELVERSION == '2003' && $ext === '') || $ext === 'xls') {
                                                $excelname = $filename . '.xls';
                                                $writer    = PHPExcel_IOFactory::createWriter($book, "Excel5");
                                                $writer->save($filepath . $excelname);
                                                $book = null;
                                                $ext  = '.xls';
                                            } else if ((EXCELVERSION == '2007' && $ext === '') || $ext === 'xlsx') {
                                                $excelname = $filename . '.xlsx';
                                                $writer    = PHPExcel_IOFactory::createWriter($book, "Excel2007");
                                                $writer->save($filepath . $excelname);
                                                $book = null;
                                                $ext  = '.xlsx';
                                            }
                                        }
                                        makeCopyFile($filepath, $excelname, $mailAddLst, $nowYmd, $nowTime, $ext);
                                    }
                                } else {
                                    //エクセルテンプレートの場合通る
                                    $csvname    = $filename . '.csv';
                                    $cExt       = getExt($d1name);
                                    $filepath   = BASE_DIR . '/php/xls/';
                                    //  $filepath = '/QNTC/192.168.1.12/TEST/';
                                    $templateFn = $d1name . '.' . $cExt;
                                    $tofilename = $filename . '.' . $cExt;
                                    fclose($csv_temp);
                                    makeCopyFile($filepath, $csvname, $mailAddLst, $nowYmd, $nowTime, '.csv');
                                    foreach ($mailAddLst as $key => $value) {
                                        if (cmMer($value['WAQUNM']) !== '' && cmMer($value['WAQUNM']) !== null) {
                                            $WAQUNM     = replaceFilename(cmMer($value['WAQUNM']));
                                            $tofilename = $WAQUNM . '_' . $nowYmd . '_' . $nowTime . '.' . $cExt;
                                            //e_log('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                            shell_exec('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                        } else {
                                            $tofilename = $d1name . '_' . $nowYmd . '_' . $nowTime . '.' . $cExt;
                                            //e_log('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                            shell_exec('cp ' . ETEMP_DIR . '"' . $templateFn . '"' . ' ' . $filepath . '"' . $tofilename . '"');
                                        }
                                        //e_log('カスタム:' . ETEMP_DIR . $templateFn . 'go' . $templatecfn);
                                    }
                                }
                            }
                            if ($htmlFlg['flg'] === '1') {
                                $filepath  = BASE_DIR . '/php/html/';
                                $excelname = $filename . '.html';
                                fwrite($html_fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');
                                fclose($html_fp);
                                makeCopyFile($filepath, $excelname, $mailAddLst, $nowYmd, $nowTime, '.html');
                            }
                        }
                    } /////
                    //e_log('KLYH check mail flg->'.$rtn.'*****->'.$mailFlg);
                    /*   if ($rtn !== 1 && $mailFlg !== '0') {
                    //HTMLフォーマット配信チェック
                    $mailHtmlFlg = chkDB2WATYPEHtml($db2con, $d1name, $wspkey);
                    if ($mailHtmlFlg['result'] === true) {
                    //正徳データーからHTML作成
                    if ($mailHtmlFlg['flg'] === '1' && $rtn !== 1) {
                    if (cmMer($wspkey) !== '') {
                    e_log("htmlformat create");
                    $pivotFlg['MAILHTMLFLG'] = true;
                    } else {
                    if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                    //                                                    $tbdata = exeHtmlMail($csv_h_shukei, $tabledata, $fdb2csv2_shukei, $filename,$WEBF);
                    $tbdata = exeHtmlMail($csv_h_shukei, $htmltabledata, $fdb2csv2_shukei, $filename, $WEBF);
                    } else {
                    //                                                    $tbdata = exeHtmlMail($csv_h, $csv_d, $fdb2csv2, $filename,$WEBF);
                    $tbdata = exeHtmlMail($csv_h, $htmltabledata, $fdb2csv2, $filename, $WEBF);
                    }
                    }
                    }
                    } else {
                    e_log(showMsg($mailHtmlFlg['result'], array(
                    'exeMail.php',
                    'chkDB2WATYPEHtml'
                    )), '1');
                    //クエリー　＊メール形式情報の取得に失敗しました。
                    $errMsg = showMsg('FAIL_FUNC', array(
                    'メール形式情報の取得'
                    )); //"メール形式情報の取得に失敗しました。";
                    fnCreateHistory($db2con, $d1name, '', '', $nowYmd, $nowTime, $wspkey, $errMsg);
                    }*/
                    if ($wspkey !== '') {
                        if ($csvFlg['result'] === true && $xlsFlg['result'] === true && $htmlFlg['result'] === true && $mailHtmlFlg['result'] === true) {
                            if ($pivotFlg['XLSFLG'] === true || $pivotFlg['HTMLFLG'] === true || $pivotFlg['MAILHTMLFLG'] === true) {
                                //e_log("<br/>pivot mail send");
                                if ($allcount === 0) {
                                    $tbdata = _fputpivot_ZeroKen($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmLst, $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $pivotFlg);
                                } else {
                                    $pivotRtn = exePivotExcel($db2con, $d1name, $filename, $wspkey, $dbname, $burstItmLst, true, 'htmlcreate', $mailAddLst, $nowYmd, $nowTime, $DB2ECON, $DB2EINS, $CNDSDATAF, $cnddatArr, $DB2EINSROWMAX, $pivotFlg);
                                    if ($pivotRtn['RTN'] === 0) {
                                        $tbdata = $pivotRtn['PIVOTHTML'];
                                        if ($pivotRtn['EXEMAXLEN'] === 'FAIL_EXEMAXLEN') {
                                            $ISEXCELMAXLEN          = 1;
                                            $mailAddLst[0]['WAXLS'] = '9';
                                            $errMsg                 = showMsg('FAIL_EXEMAXLEN', array(
                                                'xls',
                                                '65536'
                                            ));
                                            fnCreateHistory($db2con, $d1name, $filename, $schDBname, $nowYmd, $nowTime, $wspkey, $errMsg, $mailAddLst);
                                        }
                                    } else {
                                        $rtn = 1;
                                    }
                                }
                            }
                        }
                    }
                    if ($csvFlg['result'] === true && $xlsFlg['result'] === true && $htmlFlg['result'] === true && $mailHtmlFlg['result'] === true && $rtn !== 1) {
                        //パスワード生成
                        if ($csvFlg['flg'] === '1' || $xlsFlg['flg'] === '1' || $htmlFlg['flg'] === '1') {
                            $password = MakeRandStr(16);
                        }
                        //ファイルをZip化
                        exeZip($d1name, $filename, $password, $csvFlg['flg'], $xlsFlg['flg'], $htmlFlg['flg'], $mailAddLst, $nowYmd, $nowTime, $wspkey, $xlsCustomFlg, $ISEXCELMAXLEN);
                    }
                    //メール送信
                    //e_log('hello finished csvflg' . print_r($csvFlg, true) . '***** xlsFlg->' . print_r($xlsFlg, true) . '*****mailHtmlFlg->' . print_r($mailHtmlFlg, true));
                    if ($csvFlg['result'] === true && $xlsFlg['result'] === true && $htmlFlg['result'] === true && $mailHtmlFlg['result'] === true && $rtn !== 1) {
                        //exeMail($db2con,$d1name,$filename,$dbname,$nowYmd,$nowTime,$password,$csvFlg['flg'],$xlsFlg['flg'],$tbdata,$wspkey,$mailFlg);
                        exeMail($db2con, $d1name, $mailAddLst, $filename, $dbname, $nowYmd, $nowTime, $password, $csvFlg['flg'], $xlsFlg['flg'], $htmlFlg['flg'], $tbdata, $wspkey, $mailFlg, $xlsCustomFlg, $templateFlg);
                    }