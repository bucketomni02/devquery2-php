<?php
/*
 *-------------------------------------------------------* 
 * RPGで約15秒おきに実行される
 * スケジュールマスターを呼んでメール配信を行う
 *-------------------------------------------------------*
 */


/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
//error_log('*******************monitor.php***********************1');
include_once("common/inc/config.php");
//error_log('*******************monitor.php***********************2');
include_once("common/inc/common.inc.php");
//error_log('*******************monitor.php***********************3');
include_once("licenseInfo.php");

/*
 *-------------------------------------------------------* 
 * 共通変数
 *-------------------------------------------------------*
 */
//スケジュールマスター格納配列
$OnceDB2WSCD = array();
//現在日付時刻曜日
$nowDate     = date('d');
$nowTime     = date('Hi');
$nowYobi     = date('w');
$nowYmd      = date('Ymd');
$nowYymm     = date('Ym');

$graphDate = date("Y-m-d", strtotime($nowYmd));
$graphTime = substr($nowTime, 0, 2) . ':' . substr($nowTime, 2, 2);

$newTime = strtotime('-3 minutes');

$newYmd  = date('Ymd', $newTime);
$newYymm = date('Ym', $newTime);
$newYobi = date('w', $newTime);
$newDate = date('d', $newTime);
$newTime = date('Hi', $newTime);
//e_log("now time" . $nowTime);
//e_log("new time" . $newTime);


/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */
$LOGEXETIME = date("Y-m-d H:i:s");
e_log('メール呼び出し：'.$LOGEXETIME);
//ライセンスのスケジュール実行権限がない場合、ここで処理終了
if ($licenseScheduleBtn === false) {
    exit();
}

//DB接続
$db2con = cmDb2Con();
//ライブラリセット
cmSetPHPQUERY($db2con);

//スケジュールマスター取得(ONCE)
$OnceDB2WSCD     = getOnceDB2WSCD($db2con, '1', $nowYmd, $nowTime, $newTime, $newYmd);
//スケジュールマスター取得(WEEKLY)
$WeeklyDB2WSCD   = getWeeklyDB2WSCD($db2con, '2', $nowYobi, $nowTime, $newTime, $newYobi);
//スケジュールマスター取得(MONTH)
$MonthDB2WSCD    = getMonthDB2WSCD($db2con, '3', $nowDate, $nowTime, $nowYmd, $newTime, $newDate);
//スケジュールマスター取得(INTERVAL)
$IntervalDB2WSCD = getIntervalDB2WSCD($db2con, '4');

//取得したスケジュールをループ→各処理
if ($OnceDB2WSCD['result'] === true) {
    exeSchedule($db2con, $OnceDB2WSCD['data'], $nowYmd, $nowTime, $licenseCl, $newYmd, $newTime);
} else {
    e_log(showMsg($OnceDB2WSCD['result'], array(
        'exeMail.php',
        'getOnceDB2WSCD'
    )), '1');
}
if ($WeeklyDB2WSCD['result'] === true) {
    exeSchedule($db2con, $WeeklyDB2WSCD['data'], $nowYmd, $nowTime, $licenseCl, $newYmd, $newTime);
} else {
    e_log(showMsg($WeeklyDB2WSCD['result'], array(
        'exeMail.php',
        'getWeeklyDB2WSCD'
    )), '1');
}
if ($MonthDB2WSCD['result'] === true) {
    exeSchedule($db2con, $MonthDB2WSCD['data'], $nowYmd, $nowTime, $licenseCl, $newYmd, $newTime);
} else {
    e_log(showMsg($MonthDB2WSCD['result'], array(
        'exeMail.php',
        'getMonthDB2WSCD'
    )), '1');
}
if ($IntervalDB2WSCD['result'] === true) {
    exeSchedule($db2con, $IntervalDB2WSCD['data'], $nowYmd, $nowTime, $licenseCl, $newYmd, $newTime);
} else {
    e_log(showMsg($IntervalDB2WSCD['result'], array(
        'exeMail.php',
        'getIntervalDB2WSCD'
    )), '1');
}

    $minutes = getGraphTime($db2con,$graphDate,$graphTime,'1'); //20170303 0928
    $hours = getGraphTime($db2con,$graphDate,$graphTime,'2');//20170303 0730
    $days = getGraphTime($db2con,$graphDate,$graphTime,'3');//20170103 0930
    $months = getGraphTime($db2con,$graphDate,$graphTime,'4');//20170103 0930
    $lastday = date('t',strtotime($graphDate));
    $d = date_parse_from_format("Y-m-d", $graphDate);
    $today = $d['day'];
    $lastDayofMonth = array();
    if((int)$lastday === (int)$today){
        $lastDayofMonth = getGraphTime($db2con,$graphDate,$graphTime,'4',true);//20170103 0930
    }
    //取得したスケジュールをループ→各処理
    if ($minutes['result'] === true) {
        exeGphSchedule($db2con, $minutes['data'], $nowYmd, $nowTime);
    } else {
        e_log(showMsg($minutes['result'], array(
            'monitor.php',
            'exeGphSchedule'
        )), '1');
    }
    //取得したスケジュールをループ→各処理
    if ($hours['result'] === true) {
        exeGphSchedule($db2con, $hours['data'], $nowYmd, $nowTime);
    } else {
        e_log(showMsg($hours['result'], array(
            'monitor.php',
            'exeGphSchedule'
        )), '1');
    }
    //取得したスケジュールをループ→各処理
    if ($days['result'] === true) {
        exeGphSchedule($db2con, $days['data'], $nowYmd, $nowTime);
    } else {
        e_log(showMsg($days['result'], array(
            'monitor.php',
            'exeGphSchedule'
        )), '1');
    }
    //取得したスケジュールをループ→各処理
    if ($months['result'] === true) {
        exeGphSchedule($db2con, $months['data'], $nowYmd, $nowTime);
    } else {
        e_log(showMsg($months['result'], array(
            'monitor.php',
            'exeGphSchedule'
        )), '1');
    }
    //取得したスケジュールをループ→各処理
    if(count($lastDayofMonth) !== 0){
	    if ($lastDayofMonth['result'] === true) {
	        exeGphSchedule($db2con, $lastDayofMonth['data'], $nowYmd, $nowTime);
	    } else {
	        e_log(showMsg($days['result'], array(
	            'monitor.php',
	            'exeGphSchedule'
	        )), '1');
	    }
    }
//DBCLOSE
cmDb2Close($db2con);
exit();


/*
 *-------------------------------------------------------* 
 *取得したスケジュールをループ→各処理
 *-------------------------------------------------------*
 */
function exeSchedule($db2con, $wscd, $nowYmd, $nowTime, $licenseCl, $newYmd, $newTime)
{
    $wscd = umEx($wscd, true);
    foreach ($wscd as $key => $value) {
        $rs;
        $d1name = $value['WSNAME'];
        $wsfrq  = $value['WSFRQ'];
        /*****/
        $wspkey = '';
        if ($value['WSPKEY'] !== '') {
            $wspkey = ($value['WSPKEY']);
        } else {
            $wspkey = '';
        }
        /*****/
        $wstime = $value['WSTIME'];
        
        //同じ時間ですでに実行されていないかチェック(返り値 true:未実行,false:実行済)
        $rs = checkDB2WSCD($db2con, $d1name, $nowYmd, $nowTime, $wspkey, $newYmd, $newTime, $wsfrq, $value);
        if ($rs['result'] === true) {
            if ($rs['data'] === 0) {
                
                //WSFRQが4の場合、前回実行時刻から今回の実行時刻を算出して、マッチしている時にクエリーを実行
                if ($wsfrq == '4') {
                    
                    //前回実行時刻と比較、時間が来てたら実行
                    $wstime = $value['WSTIME']; //実行間隔
                    $wsbday = $value['WSBDAY']; //前回実行日付
                    $wsbtim = $value['WSBTIM']; //前回実行時刻
                    $exetim = ''; //実行時刻
                    
                    //実行間隔から今実行するかどうかをチェック
                    $check = checkAfterTime($wstime, $nowTime);
                    //$checkがtrueの場合、実行
                    if ($check === true) {
                        $DB2WSCD = updDB2WSCD($db2con, $value['WSNAME'], $value['WSFRQ'], $value['WSODAY'], $value['WSWDAY'], $value['WSMDAY'], $value['WSTIME'], $wspkey, $nowYmd, $nowTime);
                        //クエリー実行→エクセル作成→メール配信
                        if ($DB2WSCD['result'] == true) {
                            cmCallPHPQUERYWS($db2con, $d1name, $wspkey, $nowYmd, $nowTime,'0');
                            //                                exeStart($db2con,$d1name,$wspkey,$nowYmd,$nowTime);
                        } else {
                            $errMsg = showMsg('FAIL_FUNC', array(
                                '実行日付の更新'
                            )); //" 実行日付の更新に失敗しました。";
                            fnCreateHistory($db2con, $value['WSNAME'], '', '', $nowYmd, $nowTime, $wspkey, $errMsg);
                            e_log(showMsg($DB2WSCD['result'], array(
                                'exeMail.php',
                                'updDB2WSCD'
                            )), '1');
                        }
                    }
                    
                    //WSFRQが4以外の場合、そのままクエリーを実行
                } else {
                    $DB2WSCD = updDB2WSCD($db2con, $value['WSNAME'], $value['WSFRQ'], $value['WSODAY'], $value['WSWDAY'], $value['WSMDAY'], $value['WSTIME'], $wspkey, $nowYmd, $nowTime);
                    //クエリー実行→エクセル作成→メール配信
                    if ($DB2WSCD['result'] == true) {
                        cmCallPHPQUERYWS($db2con, $d1name, $wspkey, $nowYmd, $nowTime, '0');
                        //                                exeStart($db2con,$d1name,$wspkey,$nowYmd,$nowTime);
                    } else {
                        e_log(showMsg($DB2WSCD['result'], array(
                            'exeMail.php',
                            'updDB2WSCD'
                        )), '1');
                        $errMsg = showMsg('FAIL_FUNC', array(
                            '実行日付の更新'
                        )); //" 実行日付の更新に失敗しました。";
                        fnCreateHistory($db2con, $value['WSNAME'], '', '', $nowYmd, $nowTime, $wspkey, $errMsg);
                    }
                }
                
            }
            
        } else {
            e_log(showMsg($rs['result'], array(
                'exeMail.php',
                'checkDB2WSCD'
            )), '1');
            $errMsg = showMsg('FAIL_FUNC', array(
                '送信日付のチェック'
            )); //" 送信日付のチェックに失敗しました。";
            fnCreateHistory($db2con, $value['WSNAME'], '', '', $nowYmd, $nowTime, $wspkey, $errMsg);
        }
        
    }
    
}

function checkDB2WSCD($db2con, $d1name, $nowYmd, $nowTime, $wspkey, $newYmd, $newTime, $wsfrq,$value)
{
    $rs = true;
	$data = array();
	$rtn = array();
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wscdColumns();
        $strSQL .= ' FROM DB2WSCD AS A ';
        $strSQL .= ' WHERE ';

        if($nowYmd === $newYmd){

            $strSQL .= ' WSNAME = ? ';
            $strSQL .= ' AND (WSBDAY BETWEEN ? AND ? ) ';
            $strSQL .= ' AND (WSBTIM BETWEEN ? AND ? )';
            $strSQL .= ' AND WSPKEY = ? ';
            $strSQL .= ' AND WSFRQ = ? ';
            $strSQL .= ' AND WSODAY = ? ';
            $strSQL .= ' AND WSWDAY = ? ';
            $strSQL .= ' AND WSMDAY = ? ';
            $strSQL .= ' AND WSTIME = ? ';

    		$params = array(
                $d1name,
                $newYmd,
                $nowYmd,
                $newTime,
                $nowTime,
                $wspkey,
                $wsfrq,
                $value['WSODAY'],
                $value['WSWDAY'],
                $value['WSMDAY'],
                $value['WSTIME']
            );

        }else{

            $strSQL .= ' WSNAME = ? ';
            $strSQL .= ' AND ( ';
            $strSQL .= ' ( ';
            $strSQL .= ' WSBDAY = ? ';
            $strSQL .= ' AND WSBTIM BETWEEN ? and ? ';
            $strSQL .= ' ) ';
            $strSQL .= ' OR ';
            $strSQL .= ' ( ';
            $strSQL .= ' WSBDAY = ? ';
            $strSQL .= ' AND WSBTIM BETWEEN ? and ? ';
            $strSQL .= ' ) ';
            $strSQL .= ' ) ';
            $strSQL .= ' AND WSPKEY = ? ';
            $strSQL .= ' AND WSFRQ = ? ';
            $strSQL .= ' AND WSODAY = ? ';
            $strSQL .= ' AND WSWDAY = ? ';
            $strSQL .= ' AND WSMDAY = ? ';
            $strSQL .= ' AND WSTIME = ? ';

    		$params = array(
                $d1name,
                $newYmd,
                $newTime,
                2359,
                $nowYmd,
                0,
                $nowTime,
                $wspkey,
                $wsfrq,
                $value['WSODAY'],
                $value['WSWDAY'],
                $value['WSMDAY'],
                $value['WSTIME']
            );

        }

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                    $rtn = array('result' => true,'data' => count($data));
                }
            }
	return $rtn;
}
/*
 *-------------------------------------------------------* 
 * スケジュールマスター取得(ONCE)
 *-------------------------------------------------------*
 */

function getOnceDB2WSCD($db2con, $pFrq, $pOday, $pTime, $nTime, $nOday)
{
    
    $data = array();
    $rtn  = array();
    
    
    $strSQL = ' SELECT ';
    $strSQL .= getDb2wscdColumns();
    $strSQL .= ' FROM DB2WSCD AS A ';
    $strSQL .= ' WHERE WSFRQ = ? ';
    
    if ($pOday === $nOday) {
        
        $strSQL .= ' AND ( ';
        $strSQL .= ' WSODAY =? ';
        $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
        $strSQL .= ' ) ';
        
        $strSQL .= ' AND WSSPND <> \'1\' ';
        
        $params = array(
            $pFrq,
            $pOday,
            $nTime,
            $pTime
        );
        
    } else {
        
        $strSQL .= ' AND ( ';
        $strSQL .= ' ( ';
        $strSQL .= ' WSODAY = ? ';
        $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
        $strSQL .= ' ) ';
        $strSQL .= ' OR ';
        $strSQL .= ' ( ';
        $strSQL .= ' WSODAY =? ';
        $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
        $strSQL .= ' ) ';
        $strSQL .= ' ) ';
        $strSQL .= ' AND WSSPND <> \'1\' ';
        
        $params = array(
            $pFrq,
            $pOday,
            0,
            $pTime,
            $nOday,
            $nTime,
            2359
        );
        
    }

    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array(
            'result' => 'FAIL_SQL'
        );
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array(
                'result' => 'FAIL_SQL'
            );
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array(
                'result' => true,
                'data' => $data
            );
        }
    }

    return $rtn;
    
}

/*
 *-------------------------------------------------------* 
 * スケジュールマスター取得(WEEKLY)
 *-------------------------------------------------------*
 */

function getWeeklyDB2WSCD($db2con, $pFrq, $pWday, $pTime, $nTime, $nWday)
{
    
    $data   = array();
    $rtn    = array();
    $strSQL = ' SELECT ';
    $strSQL .= getDb2wscdColumns();
    $strSQL .= ' FROM DB2WSCD AS A ';
    $strSQL .= ' WHERE WSFRQ = ? ';
    
    
    if ($pWday === $nWday) {
        
        $strSQL .= ' AND ( ';
        $strSQL .= ' SUBSTR(WSWDAY,' . ($pWday + 1) . ',1) = \'1\' ';
        $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
        $strSQL .= ' ) ';
        
        $params = array(
            $pFrq,
            $nTime,
            $pTime
        );
        
        //日付が変わっていた場合は
        //当日の0から現在時刻をBETWEEN
        //OR
        //前日のnTimeから2359をBETWEEN
    } else {
        
        $strSQL .= ' AND ';
        $strSQL .= ' ( ';
        $strSQL .= ' ( ';
        $strSQL .= ' SUBSTR(WSWDAY,' . ($pWday + 1) . ',1) = \'1\' ';
        $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
        $strSQL .= ' ) ';
        $strSQL .= ' OR ';
        $strSQL .= ' ( ';
        $strSQL .= ' SUBSTR(WSWDAY,' . ($nWday + 1) . ',1) = \'1\' ';
        $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
        $strSQL .= ' ) ';
        $strSQL .= ' ) ';
        
        
        $params = array(
            $pFrq,
            0,
            $pTime,
            $nTime,
            2359
        );
        
    }
    
    
    $strSQL .= ' AND WSSPND <> \'1\' ';
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array(
            'result' => 'FAIL_SQL'
        );
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array(
                'result' => 'FAIL_SQL'
            );
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array(
                'result' => true,
                'data' => $data
            );
        }
    }

    
    return $rtn;
    
}

/*
 *-------------------------------------------------------* 
 * スケジュールマスター取得(MONTH)
 *-------------------------------------------------------*
 */

function getMonthDB2WSCD($db2con, $pFrq, $pMday, $pTime, $nowYmd, $nTime, $nMday)
{
    $pMday_last        = '';
    $last_day_of_month = date('Ymt', strtotime($nowYmd));
    //当日が月末の場合、99を代入
    if ($nowYmd === $last_day_of_month) {
        $pMday_last = 99;
    }
    /*
    if($pMday_last !== ''){
    $str = ' AND (WSMDAY = ? OR WSMDAY = ? ) ' ;
    }else{
    $str = 'AND WSMDAY = ? ';
    }
    */
    if ($pMday_last !== '') {
        $str .= ' (WSMDAY = ? OR WSMDAY = ?) ';
    } else {
        $str .= ' WSMDAY =? ';
    }
    
    $rtn  = array();
    $data = array();
    
    $strSQL = ' SELECT ';
    $strSQL .= getDb2wscdColumns();
    $strSQL .= ' FROM DB2WSCD AS A ';
    $strSQL .= ' WHERE WSFRQ = ? ';
    
    if ($pMday === $nMday) {
        
        $strSQL .= ' AND ( ';
        $strSQL .= $str;
        $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
        $strSQL .= ' ) ';
        
        $strSQL .= ' AND WSSPND <> \'1\' ';
        
        if ($pMday_last !== '') {
            
            $params = array(
                $pFrq,
                $pMday,
                $pMday_last,
                $nTime,
                $pTime
            );
            
        } else {
            
            $params = array(
                $pFrq,
                $pMday,
                $nTime,
                $pTime
            );
            
        }
        
    } else {
        
        $strSQL .= ' AND ( ';
        $strSQL .= ' ( ';
        $strSQL .= $str;
        $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
        $strSQL .= ' ) ';
        $strSQL .= ' OR ';
        $strSQL .= ' ( ';
        $strSQL .= ' WSMDAY =? ';
        $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
        $strSQL .= ' ) ';
        $strSQL .= ' ) ';
        $strSQL .= ' AND WSSPND <> \'1\' ';
        
        if ($pMday_last !== '') {
            
            $params = array(
                $pFrq,
                $pMday,
                $pMday_last,
                0,
                $pTime,
                $nMday,
                $nTime,
                2359
            );
            
        } else {
            
            $params = array(
                $pFrq,
                $pMday,
                0,
                $pTime,
                $nMday,
                $nTime,
                2359
            );
            
        }
        
    }
    
    /*
    $strSQL .= ' AND WSTIME = ? ';
    $strSQL .= ' '.$str;
    */
    
    $strSQL .= ' AND WSSPND <> \'1\' ';
    /*
    if($pMday_last !== ''){
    array_push($params, $pMday_last);
    }
    */

    $stmt = db2_prepare($db2con, $strSQL);
    
    if ($stmt === false) {
        $rtn = array(
            'result' => 'FAIL_SQL'
        );
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array(
                'result' => 'FAIL_SQL'
            );
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    
    
    return $rtn;
}

/*
 *-------------------------------------------------------* 
 * スケジュールマスター取得(INTERVAL)
 *-------------------------------------------------------*
 */

function getIntervalDB2WSCD($db2con, $pFrq)
{
    $rtn    = array();
    $data   = array();
    $strSQL = ' SELECT ';
    $strSQL .= getDb2wscdColumns();
    $strSQL .= ' FROM DB2WSCD AS A ';
    $strSQL .= ' WHERE WSFRQ = ? ';
    $strSQL .= ' AND WSSPND <> \'1\' ';
    
    $params = array(
        $pFrq
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array(
            'result' => 'FAIL_SQL'
        );
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array(
                'result' => 'FAIL_SQL'
            );
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    return $rtn;
    
}
//間隔実行チェック
function checkAfterTime($interval, $nowTime)
{
    
    $rtn = true;
    
    //実行間隔を時刻と分に分け、分で取得
    $interval = sprintf('%04d', $interval);
    $h        = (int) substr($interval, 0, 2);
    $m        = (int) substr($interval, 2, 2);
    
    //現在時刻
    $hn = (int) substr($nowTime, 0, 2);
    $mn = (int) substr($nowTime, 2, 2);
    
    $checkmnm = false;
    if ($m > 0) {
        if ($mn % $m === 0) {
            $checkmnm = true;
        }
    } else {
        if ($mn === 0) {
            $checkmnm = true;
        }
    }
    
    if ($checkmnm !== true) {
        $rtn = false;
    } else {
        if ($h > 0) {
            if ($hn % $h !== 0) {
                
                $rtn = false;
            }
        }
    }
    return $rtn;
    
}
/*
 *-------------------------------------------------------* 
 * DB2WSCD 実行日時更新
 *-------------------------------------------------------*
 */

function updDB2WSCD($db2con, $wsname, $wsfrq, $wsoday, $wswday, $wsmday, $wstime, $wspkey, $nowYmd, $nowTime)
{
    $rtn = array();
    $rs  = '0';
    
    //構文
    $strSQL = ' UPDATE DB2WSCD ';
    $strSQL .= ' SET ';
    $strSQL .= ' WSBDAY = ?, ';
    $strSQL .= ' WSBTIM = ?, ';
    $strSQL .= ' WSSDAY = ?, ';
    $strSQL .= ' WSSTIM = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WSNAME = ? ';
    $strSQL .= ' AND WSFRQ = ? ';
    $strSQL .= ' AND WSODAY = ? ';
    $strSQL .= ' AND WSWDAY = ? ';
    $strSQL .= ' AND WSMDAY = ? ';
    $strSQL .= ' AND WSTIME = ? ';
    $strSQL .= ' AND WSPKEY = ? ';
    
    $params = array(
        $nowYmd,
        $nowTime,
        $nowYmd,
        $nowTime,
        $wsname,
        $wsfrq,
        $wsoday,
        $wswday,
        $wsmday,
        $wstime,
        $wspkey
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array(
            'result' => 'FAIL_SQL'
        );
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array(
                'result' => 'FAIL_SQL'
            );
        } else {
            $rtn = array(
                'result' => true
            );
        }
    }
    
    return $rtn;
}
/*******************graph schedule**********************/
function exeGphSchedule($db2con, $data , $nowDate, $nowTime){
      $queryIdList = array();
     $nowDate = str_replace("-","",$nowDate);
     $nowTime = str_replace(":","",$nowTime);
        $data = umEx($data,true);
        $rtn = '';
        foreach($data as $key => $value){
                $GPKQRY = $value['GPKQRY'];
                $GPKID = $value['GPKID'];
                $GPKXCO = $value['GPKXCO'];
                $GPKXID = $value['GPKXID'];
              //同じ時間ですでに実行されていないかチェック(返り値 true:未実行,false:実行済)
                $rs = chkJikoTime($db2con,$GPKQRY,$GPKID,$nowDate,$nowTime);
                if($rs['result'] === true){
                  if($rs['data'] === 0){
                            $rtn = '0';
                           $db2gpk = updDB2GPK($db2con,$GPKQRY,$GPKID,$nowDate,$nowTime);
                            if ($db2gpk['result'] !== true) {
                                $rtn  = '1' ;
                                e_log(showMsg($db2gpk['result'], array(
                                    'gphMonitor.php',
                                    'updDB2GPK'
                                  )), '1');
                            }else{
                                if (!in_array($GPKQRY, $queryIdList)){
                                    array_push($queryIdList,$GPKQRY);
                                }
                            }
                    }else{
                            $rtn  = '1' ;//もうやってる場合
                    }
                }else{
                        $rtn  = '1' ;
                        e_log(showMsg($db2gpk['result'], array(
                            'gphMonitor.php',
                            'updDB2GPK'
                        )), '1');
                }
        }
        if($rtn === '0'){
            e_log("成功".print_R($queryIdList,true));//データがあって無事に更新できた後にexeMailSend 行く
            foreach($queryIdList as $key => $value){
                    e_log("go");
                  cmCallPHPQUERYWS($db2con, $value,'',$nowDate, $nowTime,'2');
            }
        }
}
//DB2GPK のテブールを更新
function updDB2GPK($db2con,$GPKQRY,$GPKID,$nowDate,$nowTime){
    $rtn = array();
    $rs  = '0';
    //構文
    $strSQL  = ' UPDATE DB2GPK ';
    $strSQL .= ' SET ';
    $strSQL .= ' GPKBFD = ?, ';
    $strSQL .= ' GPKBFT = ? ,';
    $strSQL .= ' GPKFLG = ? ';
    $strSQL .= ' WHERE GPKQRY = ? AND GPKID = ? ';
    $params = array($nowDate,$nowTime,1,$GPKQRY,$GPKID);
    $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rtn = array(
                'result' => 'FAIL_SQL'
            );
        }else{
            $r = db2_execute($stmt,$params);
            if ($r === false) {
                $rtn = array(
                    'result' => 'FAIL_SQL'
                );
            } else {
                $rtn = array(
                    'result' => true
                );
            }
    }
    return $rtn;
}
  //同じ時間ですでに実行されていないかチェック(返り値 true:未実行,false:実行済)
function chkJikoTime($db2con,$GPKQRY,$GPKID,$nowDate,$nowTime){
    $data = array();
    $rtn  = array();
    $strSQL .= ' SELECT * FROM ';
    $strSQL .= ' DB2GPK ';
    $strSQL .= ' WHERE GPKQRY = ? ';
    $strSQL .= ' AND GPKID = ? ';
    $strSQL .= ' AND GPKBFD = ? ';
    $strSQL .= ' AND GPKBFT = ? ';
    $params = array($GPKQRY,$GPKID,$nowDate,$nowTime);
    e_log($strSQL);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array(
            'result' => 'FAIL_SQL'
        );
    } else {
            $rt = db2_execute($stmt, $params);
            if ($rt === false) {
                $rtn = array(
                    'result' => 'FAIL_SQL'
                );
            } else {
                if ($stmt) {
                    while ($row = db2_fetch_assoc($stmt)) {
                                $data[] = $row;
                    }
                }
            }
            $rtn = array(
                'result' => true,
                'data' => count($data)
            );
        }
    e_log("chkJikoTime".print_R($rtn,true));
    return $rtn;
}
/*
**flg = >1 分
**flg = >2 時
**flg = >3 日
**flg = >4 月
**未日ならlastDateはtrueになる
*/
function getGraphTime($db2con,$nowDate,$nowTime,$flg,$lastDate = false){
    $newSysTime = '';
    $GPKNUM = '';
    $months = '';
    $nowTime = $nowTime.':00';
    if($flg === '1'){
        $GPKNUM = 'minutes';
        $newSysTime = $nowDate.' '.$nowTime;
    }else if($flg === '2'){
        $GPKNUM = 'hours';
        $newSysTime = $nowDate.' '.$nowTime;
    }else if($flg == '3'){
        $GPKNUM = 'days';
        $newSysTime = $nowDate.' '.$nowTime;
    }else{
        $GPKNUM = 'month';
        $newSysTime = $nowDate.' '.$nowTime;
        if($lastDate === true){
            $months = ", MONTH(TODAYDATE) as THISMONTH ,REPLACE(E.GPTIME, '.', ':') AS THISTIME ";
        }
    }
    $strSQL = '';
    if($lastDate === true){
        $strSQL .= 'SELECT F.* FROM( ';
    }
    $strSQL .= ' SELECT E.*'.$months.' FROM( ';
    $strSQL .= ' SELECT D.*,TIMESTAMP(CONCAT(CONCAT(D.GPDATE,\'-\'),D.GPTIME))+ GPKNUM '.$GPKNUM.' AS TODAYDATE FROM ';
    $strSQL .= ' ( ';
    $strSQL .= ' SELECT  ';
    $strSQL .= ' C.GPKQRY,C.GPKID,C.GPKNUM,C.GPKCUD,C.GPKCUT,C.GPKBFD,C.GPKBFT, C.GPKEND,';
    $strSQL .= ' substr (char (C.GPDATE),1,4) ||\'-\'|| ';
    $strSQL .= ' substr (char (C.GPDATE),5,2) ||\'-\'|| ';  
    $strSQL .= ' substr (char (C.GPDATE),7,2) AS GPDATE, ';
    $strSQL .= ' substr (char (C.GPTIME),1,2) ||\'.\'|| ';
    $strSQL .= ' substr (char (C.GPTIME),3,2) ||\'.\'|| ';
    $strSQL .= ' substr (char (C.GPTIME),5,2) AS GPTIME FROM( ';
    $strSQL .= ' SELECT  ';
    $strSQL .= ' B.GPKQRY,B.GPKID,B.GPKNUM,B.GPKCUD,B.GPKCUT,B.GPKBFD,B.GPKBFT,B.GPKEND, ';
    $strSQL .= ' CASE  ';
    $strSQL .= ' WHEN LENGTH(TRIM(GPTIME)) = 1 THEN CONCAT(TRIM(CONCAT(\'000\',CHAR(GPTIME))),\'00\') ';
    $strSQL .= ' WHEN LENGTH(TRIM(GPTIME)) = 2 THEN CONCAT(TRIM(CONCAT(\'00\',CHAR(GPTIME))),\'00\') ';
    $strSQL .= ' WHEN LENGTH(TRIM(GPTIME)) = 3 THEN CONCAT(TRIM(CONCAT(\'0\',CHAR(GPTIME))),\'00\') ';
    $strSQL .= ' WHEN LENGTH(TRIM(GPTIME)) = 4 THEN CONCAT(TRIM(CONCAT(\'\',CHAR(GPTIME))),\'00\') ';
    $strSQL .= ' END AS GPTIME,GPDATE FROM( ';
    $strSQL .= ' SELECT  ';
    $strSQL .= ' A.GPKQRY,A.GPKID,A.GPKNUM,A.GPKCUD,A.GPKCUT,A.GPKBFD,A.GPKBFT,A.GPKEND, ';
    $strSQL .= ' CASE  ';
    $strSQL .= ' WHEN GPKFLG = 0 THEN GPKCUT ';
    $strSQL .= ' ELSE GPKBFT END AS GPTIME, ';
    $strSQL .= ' CASE  ';
    $strSQL .= ' WHEN GPKFLG = 0 THEN GPKCUD ';
    $strSQL .= ' ELSE GPKBFD END AS GPDATE ';
    $strSQL .= ' FROM DB2GPK AS A WHERE A.GPKDAT = ? AND ';
    $strSQL .= ' A.GPKNUM <> 0 ';
    $strSQL .= ' ) AS B ';
    $strSQL .= ' )AS C  ';
    $strSQL .= ' )AS D  ';
    $strSQL .= ' )AS E ';
    if($lastDate !== true){
//        $strSQL .= ' WHERE TODAYDATE = ? AND GPKEND <> \'1\'';
        $strSQL .= ' WHERE TODAYDATE <= ? AND GPKEND <> \'1\'';
      //  $nowTime = str_replace(":","",$nowTime);
        $params = array($flg,$newSysTime);
    }else{
        $d = date_parse_from_format("Y-m-d", $nowDate);
        $Months = $d['month'];
        $strSQL .= ' )AS F WHERE GPKEND = \'1\' AND THISMONTH = ? AND THISTIME = ?';
        $params = array($flg,$Months,$nowTime);
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array(
            'result' => 'FAIL_SQL'
        );
    } else {
        $rt = db2_execute($stmt, $params);
            if ($rt === false) {
                $rtn = array(
                    'result' => 'FAIL_SQL'
                );
            } else {
                   while ($row = db2_fetch_assoc($stmt)) {
                           $data[] = $row;
                    }
                    $rtn = array(
                        'result' => true,
                        'data' => $data
                    );
            }
    }
    return $rtn;
}

?>