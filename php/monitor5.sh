#!/bin/sh
if [ -f /usr/local/zendsvr/etc/zce.rc ];then
    . /usr/local/zendsvr/etc/zce.rc
else
    echo "/usr/local/zendsvr/etc/zce.rc doesn't exist!"
    exit 1;
fi
LIBPATH=$ZCE_PREFIX/lib

export LDR_CNTRL=MAXDATA=0x80000000
$ZCE_PREFIX/bin/php "/www/zendsvr/htdocs/devquery2/php/monitor.php"
unset LDR_CNTRL

