<?php
/*
*-------------------------------------------------------* 
* RPGで約15秒おきに実行される
* スケジュールマスターを呼んでメール配信を行う
*-------------------------------------------------------*
*/


/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/

include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");

error_log('*******************monitor.php***********************');

/*
*-------------------------------------------------------* 
* 共通変数
*-------------------------------------------------------*
*/
//スケジュールマスター格納配列
$OnceDB2WSCD = array();
//現在日付時刻曜日
$nowDate = date('d');
$nowTime = date('Hi');
$nowYobi = date('w');
$nowYmd  = date('Ymd');
$nowYymm = date('Ym');

$newTime = strtotime('-60 minutes');

$newYmd  = date('Ymd',$newTime);
$newYymm = date('Ym',$newTime);
$newYobi = date('w',$newTime);
$newDate = date('d',$newTime);
$newTime = date('Hi',$newTime);
/**test area**/
/*$nowDate = '09';
$nowTime = '1551';
$nowYobi = '2';
$nowYmd  = '20170509';
$nowYymm = date('Ym');

$newTime = strtotime('-60 minutes');

$newYmd  = '20170509';
$newYymm = date('Ym',$newTime);
$newYobi = date('w',$newTime);
$newDate = '09';
$newTime = '1450';*/
/**test area**/
$nowDate = '10';
$nowTime = '0001';
$nowYobi = '3';
$nowYmd  = '20170510';
$nowYymm = date('Ym');

$newTime = strtotime('-60 minutes');

$newYmd  = '20170509';
$newYymm = date('Ym',$newTime);
$newYobi = date('w',$newTime);
$newDate = '09';
$newTime = '2301';
/**test area**/
/**test area**/
echo '<br/>nowDate :'.$nowDate;
echo '<br/>nowTime :'.$nowTime;
echo '<br/>nowYobi :'.$nowYobi;
echo '<br/>nowYmd :'.$nowYmd;
echo '<br/>nowYymm :'.$nowYymm;
echo '<br/>newYmd :'.$newYmd;
echo '<br/>newYymm :'.$newYymm;
echo '<br/>newYobi :'.$newYobi;
echo '<br/>newDate :'.$newDate;
echo '<br/>newTime :'.$newTime;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//ライセンスのスケジュール実行権限がない場合、ここで処理終了
if($licenseScheduleBtn === false){
    exit();
}

//DB接続
$db2con = cmDb2Con();
//ライブラリセット
cmSetPHPQUERY($db2con);

//スケジュールマスター取得(ONCE)
$OnceDB2WSCD = getOnceDB2WSCD($db2con,'1',$nowYmd,$nowTime,$newTime,$newYmd);
//スケジュールマスター取得(WEEKLY)
$WeeklyDB2WSCD = getWeeklyDB2WSCD($db2con,'2',$nowYobi,$nowTime,$newTime,$newYobi);
//スケジュールマスター取得(MONTH)
$MonthDB2WSCD = getMonthDB2WSCD($db2con,'3',$nowDate,$nowTime,$nowYmd,$newTime,$newDate);
echo "<pre>";
print_r($MonthDB2WSCD);
echo "</pre>";
//スケジュールマスター取得(INTERVAL)
$IntervalDB2WSCD = getIntervalDB2WSCD($db2con,'4');

//取得したスケジュールをループ→各処理
 if($OnceDB2WSCD['result'] === true){
   // error_log("OnceDB2WSCD ".print_R($OnceDB2WSCD,true));
    exeSchedule($db2con,$OnceDB2WSCD['data'],$nowYmd,$nowTime,$licenseCl,$newYmd,$newTime);
}else{
    e_log(showMsg($OnceDB2WSCD['result'],array('exeMail.php','getOnceDB2WSCD')),'1');
}
if($WeeklyDB2WSCD['result'] === true){
    exeSchedule($db2con,$WeeklyDB2WSCD['data'],$nowYmd,$nowTime,$licenseCl,$newYmd,$newTime);
}else{
   e_log(showMsg($WeeklyDB2WSCD['result'],array('exeMail.php','getWeeklyDB2WSCD')),'1');
}
if($MonthDB2WSCD['result'] === true){
    exeSchedule($db2con,$MonthDB2WSCD['data'],$nowYmd,$nowTime,$licenseCl,$newYmd,$newTime);
}else{
   e_log(showMsg($MonthDB2WSCD['result'],array('exeMail.php','getMonthDB2WSCD')),'1');
}
if($IntervalDB2WSCD['result'] === true){
    exeSchedule($db2con,$IntervalDB2WSCD['data'],$nowYmd,$nowTime,$licenseCl,$newYmd,$newTime);
}else{
   e_log(showMsg($IntervalDB2WSCD['result'],array('exeMail.php','getIntervalDB2WSCD')),'1');
}



//DBCLOSE
cmDb2Close($db2con);
exit();


/*
*-------------------------------------------------------* 
*取得したスケジュールをループ→各処理
*-------------------------------------------------------*
*/
function exeSchedule($db2con,$wscd,$nowYmd,$nowTime,$licenseCl,$newYmd,$newTime){
    $wscd = umEx($wscd,true);
    foreach($wscd as $key => $value){
        $rs;
        $d1name = $value['WSNAME'];
        $wsfrq = $value['WSFRQ'];
/*****/
        $wspkey = '';
        if($value['WSPKEY'] !== ''){
            $wspkey = ($value['WSPKEY']);
        }else{
             $wspkey = '';
        }
/*****/
        $wstime = $value['WSTIME'];

error_log('FRQ='.$value['WSFRQ']);
echo('FRQ='.$value['WSFRQ']);

        //同じ時間ですでに実行されていないかチェック(返り値 true:未実行,false:実行済)
        $rs = checkDB2WSCD($db2con,$d1name,$nowYmd,$nowTime,$wspkey,$newYmd,$newTime,$wsfrq,$value);

        if($rs['result'] === true){
error_log('checkDB2WSCDのカウント='.$rs['data']);
error_log(print_r($value,true));

echo('checkDB2WSCDのカウント='.$rs['data']);

echo "<pre>";
print_r($value);
echo "</pre>";


            if($rs['data'] === 0){

echo "<pre>";
print_r('実行するクエリ'.$d1name);
echo "</pre>";

    			//WSFRQが4の場合、前回実行時刻から今回の実行時刻を算出して、マッチしている時にクエリーを実行
    			if($wsfrq == '4'){

    				//前回実行時刻と比較、時間が来てたら実行
    				$wstime = $value['WSTIME'];		//実行間隔
    				$wsbday = $value['WSBDAY'];		//前回実行日付
    				$wsbtim = $value['WSBTIM'];		//前回実行時刻
    				$exetim = '';					//実行時刻

    				//実行間隔から今実行するかどうかをチェック
    				$check = checkAfterTime($wstime,$nowTime);
    				//$checkがtrueの場合、実行
    				if($check === true){
    		            $DB2WSCD = updDB2WSCD($db2con,$value['WSNAME'],$value['WSFRQ'],$value['WSODAY'],$value['WSWDAY'],$value['WSMDAY'],$value['WSTIME'],$wspkey,$nowYmd,$nowTime);
    		            //クエリー実行→エクセル作成→メール配信
                        if($DB2WSCD['result'] == true){
                                cmCallPHPQUERYWS($db2con,$d1name,$wspkey,$nowYmd,$nowTime);
//                                exeStart($db2con,$d1name,$wspkey,$nowYmd,$nowTime);
                        }else{
                                $errMsg = showMsg('FAIL_FUNC',array('実行日付の更新'));//" 実行日付の更新に失敗しました。";
                                fnCreateHistory($db2con,$value['WSNAME'],'','',$nowYmd,$nowTime,$wspkey,$errMsg);
                                e_log(showMsg($DB2WSCD['result'],array('exeMail.php','updDB2WSCD')),'1');
                        }
    				}

    			//WSFRQが4以外の場合、そのままクエリーを実行
    			}else{
    	          $DB2WSCD = updDB2WSCD($db2con,$value['WSNAME'],$value['WSFRQ'],$value['WSODAY'],$value['WSWDAY'],$value['WSMDAY'],$value['WSTIME'],$wspkey,$nowYmd,$nowTime);
    	          //クエリー実行→エクセル作成→メール配信
                  if($DB2WSCD['result'] == true){
error_log('CLをキック');
echo('<p>CLをキック</p>');
                                cmCallPHPQUERYWS($db2con,$d1name,$wspkey,$nowYmd,$nowTime);
//                                exeStart($db2con,$d1name,$wspkey,$nowYmd,$nowTime);
                    }else{
                            e_log(showMsg($DB2WSCD['result'],array('exeMail.php','updDB2WSCD')),'1');
                            $errMsg = showMsg('FAIL_FUNC',array('実行日付の更新'));//" 実行日付の更新に失敗しました。";
                            fnCreateHistory($db2con,$value['WSNAME'],'','',$nowYmd,$nowTime,$wspkey,$errMsg);
                    }
    			}

            }

        }else{
            e_log(showMsg($rs['result'],array('exeMail.php','checkDB2WSCD')),'1');
            $errMsg = showMsg('FAIL_FUNC',array('送信日付のチェック'));//" 送信日付のチェックに失敗しました。";
            fnCreateHistory($db2con,$value['WSNAME'],'','',$nowYmd,$nowTime,$wspkey,$errMsg);
        }
    
    }

}

function checkDB2WSCD($db2con,$d1name,$nowYmd,$nowTime,$wspkey,$newYmd,$newTime,$wsfrq,$value){
    $rs = true;
	$data = array();
	$rtn = array();
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wscdColumns();
        $strSQL .= ' FROM DB2WSCD AS A ';
        $strSQL .= ' WHERE ';
        $strSQL .= ' WSNAME = ? ';
        $strSQL .= ' AND (WSBDAY BETWEEN ? AND ? ) ';
        $strSQL .= ' AND (WSBTIM BETWEEN ? AND ? )';
        $strSQL .= ' AND WSPKEY = ? ';
        $strSQL .= ' AND WSFRQ = ? ';
        $strSQL .= ' AND WSODAY = ? ';
        $strSQL .= ' AND WSWDAY = ? ';
        $strSQL .= ' AND WSMDAY = ? ';
        $strSQL .= ' AND WSTIME = ? ';

		$params = array(
            $d1name,
            $newYmd,
            $nowYmd,
            $newTime,
            $nowTime,
            $wspkey,
            $wsfrq,
            $value['WSODAY'],
            $value['WSWDAY'],
            $value['WSMDAY'],
            $value['WSTIME']
        );
/*
error_log('checkDB2WSCD');
error_log($strSQL);
error_log(print_r($params,true));
*/
echo('<p>'.$strSQL.'</p>');
print_r('<p>'.$params.'</p>',true);
		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                    $rtn = array('result' => true,'data' => count($data));
                }
            }
	return $rtn;
}
/*
*-------------------------------------------------------* 
* スケジュールマスター取得(ONCE)
*-------------------------------------------------------*
*/

function getOnceDB2WSCD($db2con,$pFrq,$pOday,$pTime,$nTime,$nOday){

	$data = array();
	$rtn = array();


        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wscdColumns();
        $strSQL .= ' FROM DB2WSCD AS A ';
        $strSQL .= ' WHERE WSFRQ = ? ' ;

        if($pOday === $nOday){

            $strSQL .= ' AND ( ';
            $strSQL .= ' WSODAY =? ';
            $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
            $strSQL .= ' ) ';

            $strSQL .= ' AND WSSPND <> \'1\' ';

    		$params = array(
    			$pFrq,
    			$pOday,
                $nTime,
                $pTime
    		);

        }else{

            $strSQL .= ' AND ( ';
            $strSQL .= ' ( ';
            $strSQL .= ' WSODAY = ? ';
            $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
            $strSQL .= ' ) ';
            $strSQL .= ' OR ';
            $strSQL .= ' ( ';
            $strSQL .= ' WSODAY =? ';
            $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
            $strSQL .= ' ) ';
            $strSQL .= ' ) ';
            $strSQL .= ' AND WSSPND <> \'1\' ';

    		$params = array(
    			$pFrq,
    			$pOday,
                0,
                $pTime,
                $nOday,
                $nTime,
                2359
    		);

        }
/*
error_log('ONCE_SQL');
error_log($strSQL);
error_log(print_r($params,true));
*/
		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                    $rtn = array('result' => 'FAIL_SQL');
          }else{
            	$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                        $rtn = array('result' => true,'data' => $data);
                }
        }
error_log('onceのカウント='.count($data));
	return $rtn;

}

/*
*-------------------------------------------------------* 
* スケジュールマスター取得(WEEKLY)
*-------------------------------------------------------*
*/

function getWeeklyDB2WSCD($db2con,$pFrq,$pWday,$pTime,$nTime,$nWday){

	$data = array();
	$rtn = array();
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wscdColumns();
        $strSQL .= ' FROM DB2WSCD AS A ';
        $strSQL .= ' WHERE WSFRQ = ? ' ;

        
        if($pWday === $nWday){

            $strSQL .= ' AND ( ';
            $strSQL .= ' SUBSTR(WSWDAY,'.($pWday+1).',1) = \'1\' ';
            $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
            $strSQL .= ' ) ';

    		$params = array(
    			$pFrq,
                $nTime,
                $pTime
    		);
    
        //日付が変わっていた場合は
        //当日の0から現在時刻をBETWEEN
        //OR
        //前日のnTimeから2359をBETWEEN
        }else{

            $strSQL .= ' AND ';
            $strSQL .= ' ( ';
                $strSQL .= ' ( ';
                    $strSQL .= ' SUBSTR(WSWDAY,'.($pWday+1).',1) = \'1\' ';
                    $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
                $strSQL .= ' ) ';
                $strSQL .= ' OR ';
                $strSQL .= ' ( ';
                    $strSQL .= ' SUBSTR(WSWDAY,'.($nWday+1).',1) = \'1\' ';
                    $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
                $strSQL .= ' ) ';
            $strSQL .= ' ) ';


    		$params = array(
    			$pFrq,
                0,
                $pTime,
                $nTime,
                2359
    		);

        }


        $strSQL .= ' AND WSSPND <> \'1\' ';

		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                    $rtn = array('result' => 'FAIL_SQL');
          }else{
            		$rt = db2_execute($stmt,$params);
                     if($rt === false){
                            $rtn = array('result' => 'FAIL_SQL');
                    }else{
                		if($stmt){
                			while($row = db2_fetch_assoc($stmt)){
                			    $data[] = $row;
                			}
                		}
                        $rtn = array('result' => true,'data' => $data);
                  }
            }
error_log('weeklyのカウント='.count($data));

	return $rtn;

}

/*
*-------------------------------------------------------* 
* スケジュールマスター取得(MONTH)
*-------------------------------------------------------*
*/

function getMonthDB2WSCD($db2con,$pFrq,$pMday,$pTime,$nowYmd,$nTime,$nMday){
        $pMday_last = '';
        $last_day_of_month = date('Ymt', strtotime($nowYmd));
        //当日が月末の場合、99を代入
        if($nowYmd === $last_day_of_month){
            $pMday_last = 99;
        }
/*
        if($pMday_last !== ''){
            $str = ' AND (WSMDAY = ? OR WSMDAY = ? ) ' ;
        }else{
            $str = 'AND WSMDAY = ? ';
        }
*/
        if($pMday_last !== ''){
            $str .= ' (WSMDAY = ? OR WSMDAY = ?) ';
        }else{
            $str .= ' WSMDAY =? ';
        }

        $rtn = array();
        $data = array();

        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wscdColumns();
        $strSQL .= ' FROM DB2WSCD AS A ';
        $strSQL .= ' WHERE WSFRQ = ? ' ;

        if($pMday === $nMday){

            $strSQL .= ' AND ( ';
            $strSQL .= $str;
            $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
            $strSQL .= ' ) ';

            $strSQL .= ' AND WSSPND <> \'1\' ';

            if($pMday_last !== ''){

        		$params = array(
        			$pFrq,
        			$pMday,
                    $pMday_last,
                    $nTime,
                    $pTime
        		);

            }else{

        		$params = array(
        			$pFrq,
        			$pMday,
                    $nTime,
                    $pTime
        		);

            }
            
        }else{

            $strSQL .= ' AND ( ';
            $strSQL .= ' ( ';
            $strSQL .= $str;
            $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
            $strSQL .= ' ) ';
            $strSQL .= ' OR ';
            $strSQL .= ' ( ';
            $strSQL .= ' WSMDAY =? ';
            $strSQL .= ' AND WSTIME BETWEEN ? and ? ';
            $strSQL .= ' ) ';
            $strSQL .= ' ) ';
            $strSQL .= ' AND WSSPND <> \'1\' ';

            if($pMday_last !== ''){

        		$params = array(
        			$pFrq,
        			$pMday,
                    $pMday_last,
                    0,
                    $pTime,
                    $nMday,
                    $nTime,
                    2359
        		);

            }else{

        		$params = array(
        			$pFrq,
        			$pMday,
                    0,
                    $pTime,
                    $nMday,
                    $nTime,
                    2359
        		);

            }
            
        }

/*
        $strSQL .= ' AND WSTIME = ? ';
        $strSQL .= ' '.$str;
*/

        $strSQL .= ' AND WSSPND <> \'1\' ';
/*
        if($pMday_last !== ''){
            array_push($params, $pMday_last);
        }
*/
/*
error_log('getMonthDB2WSCD');
error_log($strSQL);
error_log(print_r($params,true));
*/
echo('MONTH取得SQL');
echo('<p>'.$strSQL.'</p>');
var_dump($params);

        $stmt = db2_prepare($db2con,$strSQL);

        if($stmt === false){
             $rtn = array('result' => 'FAIL_SQL');
        }else{
         		$rt = db2_execute($stmt,$params);
                if($rt === false){
                        $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                        $rtn = array('result' => true,'data' => $data);
                }
            }

error_log('monthのカウント='.count($data));

	return $rtn;
}

/*
*-------------------------------------------------------* 
* スケジュールマスター取得(INTERVAL)
*-------------------------------------------------------*
*/

function getIntervalDB2WSCD($db2con,$pFrq){
	$rtn = array();
	$data = array();
        $strSQL  = ' SELECT ';
        $strSQL .= getDb2wscdColumns();
        $strSQL .= ' FROM DB2WSCD AS A ';
        $strSQL .= ' WHERE WSFRQ = ? ' ;
        $strSQL .= ' AND WSSPND <> \'1\' ';

		$params = array(
			$pFrq
		);
		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
            		if($stmt){
            			while($row = db2_fetch_assoc($stmt)){
            			    $data[] = $row;
            			}
            		}
                    $rtn = array('result' => true,'data' => $data);
                }
            }
	return $rtn;

}
//間隔実行チェック
function checkAfterTime($interval,$nowTime){

	$rtn = true;

	//実行間隔を時刻と分に分け、分で取得
	$interval = sprintf('%04d',$interval);
	$h = (int)substr($interval,0,2);
	$m = (int)substr($interval,2,2);

	//現在時刻
	$hn = (int)substr($nowTime,0,2);
	$mn = (int)substr($nowTime,2,2);

    $checkmnm = false;
    if($m > 0){
    	if($mn%$m === 0){
    		$checkmnm = true;
    	}
    }else{
        if($mn === 0){
            $checkmnm = true;
        }
    }

	if($checkmnm !== true){
		$rtn = false;
	}else{
		if($h > 0){
			if($hn%$h !== 0){

				$rtn = false;
			}
		}
	}
	return $rtn;

}
/*
*-------------------------------------------------------* 
* DB2WSCD 実行日時更新
*-------------------------------------------------------*
*/

function updDB2WSCD($db2con,$wsname,$wsfrq,$wsoday,$wswday,$wsmday,$wstime,$wspkey,$nowYmd,$nowTime){
    $rtn = array();
	$rs = '0';

	//構文
        $strSQL  = ' UPDATE DB2WSCD ';
        $strSQL .= ' SET ';
        $strSQL .= ' WSBDAY = ?, ';
        $strSQL .= ' WSBTIM = ?, ';
        $strSQL .= ' WSSDAY = ?, ';
        $strSQL .= ' WSSTIM = ? ';
        $strSQL .= ' WHERE ';
        $strSQL .= ' WSNAME = ? ';
        $strSQL .= ' AND WSFRQ = ? ';
        $strSQL .= ' AND WSODAY = ? ';
        $strSQL .= ' AND WSWDAY = ? ';
        $strSQL .= ' AND WSMDAY = ? ';
        $strSQL .= ' AND WSTIME = ? ';
        $strSQL .= ' AND WSPKEY = ? ';

		$params = array(
            $nowYmd,
            $nowTime,
            $nowYmd,
            $nowTime,
            $wsname,
            $wsfrq,
            $wsoday,
            $wswday,
            $wsmday,
            $wstime,
            $wspkey
		);
		$stmt = db2_prepare($db2con,$strSQL);
          if($stmt === false){
                $rtn = array('result' => 'FAIL_SQL');
          }else{
        		$rt = db2_execute($stmt,$params);
                if($rt === false){
                    $rtn = array('result' => 'FAIL_SQL');
                }else{
                     $rtn = array('result' => true);
                }
          }

	return $rtn;
}
?>