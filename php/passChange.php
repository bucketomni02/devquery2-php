<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$OLDPWD = $_POST['OLDPWD'];
$NEWPWD = $_POST['NEWPWD'];
$CFMPWD = $_POST['CFMPWD'];
$touch = (isset($_POST['TOUCH'])?$_POST['TOUCH']:'0');
e_log('login user:'.$_SESSION['PHPQUERY']['user'][0]['WUUID'].'post user:'.$_POST['WUUID']);
if($touch !== '1'){
    if(isset($_SESSION['PHPQUERY']['LOGIN']) && $_SESSION['PHPQUERY']['LOGIN'] === '1'){
        if($_POST['WUUID'] === ''){
            e_log('ue');
            $WUUID = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
        }else{
            $WUUID = $_POST['WUUID'];
        }
    }else{
        e_log('shita');
        $WUUID = $_POST['WUUID'];
    }
}else{
    if(isset($_SESSION['PHPQUERYTOUCH']['LOGIN']) && $_SESSION['PHPQUERYTOUCH']['LOGIN'] === '1'){
        e_log('ue');
        $WUUID = $_SESSION['PHPQUERYTOUCH']['user'][0]['WUUID'];
    }else{
        e_log('shita');
        $WUUID = $_POST['WUUID'];
    }
}
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$rs = 0;
$focus = '';
$today = date('Y-m-d');
$ins_today = str_replace("-","",$today);


/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

if($rtn === 0){
    if($_POST['WUUID'] === ''){
        if($_SESSION['PHPQUERY']['user'][0]['WUUID'] === null){
            $rtn = 1;
            $msg = showMsg('CHECK_LOGOUT',array('ユーザー'));
        }
    }
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmCheckUser($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

//バリデーションチェック

if($rtn === 0){
    if($OLDPWD === ""){
        $rtn = 1;
        //$msg = "古いパスワードを入れてください。";
		$msg =showMsg('FAIL_REQ',array('古いパスワード'));
        $focus = 'OLDPWD';
    }
}

if($rtn === 0){
    if($OLDPWD !== ""){
        e_log('user:'.$WUUID);
        $rs = fnCheckWUSROLDPWD($db2con,$WUUID,$OLDPWD);        
        if($rs === 0){    
            $rtn = 1;
            $msg = showMsg('FAIL_CHK',array('古いパスワード'));
            $focus = 'OLDPWD';
        }
    }
}

if($rtn === 0){
    if($NEWPWD === ""){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('新しいパスワード'));// "新しいパスワードを入れてください。";		
        $focus = 'NEWPWD';
    }
}
if($rtn === 0){
        $PWD = cmMer($NEWPWD);
        if($PWD === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_SPACE');
            $focus = 'NEWPWD';
        }
}

if($rtn === 0){
    $byte = checkByte($NEWPWD);
    if($byte[1] > 0){
        $rtn = 1;
        $msg =showMsg('FAIL_BYTE',array('新しいパスワード'));// '新しいパスワードは半角で入力してください。';
        $focus = 'NEWPWD';
    }
}


if($rtn === 0){
    if(!checkMaxLen($NEWPWD,10)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('新しいパスワード'));//$msg."<br/>* 新しいパスワード 10以内で入力してください。";
        $focus = 'NEWPWD';
    }
}


if($rtn === 0){
    if($CFMPWD === ""){
        $rtn = 1;
        $msg =showMsg('FAIL_REQ',array('パスワードの確認入力')); //"確認パスワードを入れてください。";
        $focus = 'CFMPWD';
    }
}


if($rtn === 0){
    if($CFMPWD !== $NEWPWD){
        $rtn = 1;
        $msg = showMsg('FAIL_CMP',array(array('新しいパスワード','と','パスワードの確認入力')));//"新しいパスワードとパスワードの確認入力が一致しませんでした。";
        $focus = 'CFMPWD';
    }
}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/

if($rtn === 0){   
    $res = fnUpdateWUSRPWD($db2con,$NEWPWD,$WUUID);
    if($res === '1'){
        $rtn = 1;
        $msg = showMsg('FAIL_UPD');// '更新処理に失敗しました。管理者にお問い合わせください。';
    }else{
        fnUpdDB2WUSRDAY($db2con,$ins_today,$WUUID);
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'USER' => $usrinfo,
    'RS' => $rs
);

echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* パスワードの更新
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WUUID  ユーザーID
* @param String  $NEWPWD 新しいパスワード
* 
*-------------------------------------------------------*
*/

function fnUpdateWUSRPWD($db2con,$NEWPWD,$WUUID){

    $rs = '0';

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = '1';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = '1';
        }else{
            //構文
            $strSQL  = ' UPDATE DB2WUSR ';
            $strSQL .= ' SET ';
            $strSQL .= ' WUPSWE = CAST(encrypt(CAST(? AS VARCHAR(10))) AS CHAR(32) FOR BIT DATA) ';
            $strSQL .= ' WHERE WUUID = ? ';

            $params = array(
                $NEWPWD,
                $WUUID
            );

            $stmt = db2_prepare($db2con,$strSQL);
            if ($stmt === false){
                $rs = '1';
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $rs = '1';
                }
            }
        }
    }
    return $rs;

}


/*
*-------------------------------------------------------* 
* 古いパスワードはデータベースにあるかどうかチェックすること
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WUUID  ユーザーID
* @param String  $OLDPWD パスワード
* 
*-------------------------------------------------------*
*/
function fnCheckWUSROLDPWD($db2con,$WUUID,$OLDPWD){

    $data = array();
    e_log('**********************************');
    e_log($WUUID);
    e_log($OLDPWD);

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $data = false;
        }else{
            $strSQL  = ' SELECT count(WUPSWE) AS COUNT ';
            $strSQL .= ' FROM DB2WUSR ';
            $strSQL .= ' WHERE WUUID = ? ';
            $strSQL .= ' AND DECRYPT_CHAR(WUPSWE) = ? ';

            $params = array(
                    $WUUID,
                    $OLDPWD
            );

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $data = false;
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = false;
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row['COUNT'];
                    }
                    $data = $data[0];
                }
            }
        }
    }
    return $data;
}


/*
*-------------------------------------------------------* 
* DB2WUSRのWUEDAY COLUMNの更新
*
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $ins_today TODAY
* @param String  $WUUID ユーザーID
*-------------------------------------------------------**/

function fnUpdDB2WUSRDAY($db2con,$ins_today,$WUUID){

    $rs = '0';

    //構文

    $strSQL  = ' UPDATE DB2WUSR SET ';
    $strSQL .= ' WUEDAY = ?';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUUID = ? ';

    $params = array(
        $ins_today,
        $WUUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = '1';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = '1';
        }
    }
    return $rs;    
}

