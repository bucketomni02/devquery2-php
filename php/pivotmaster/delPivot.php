<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$PMNAME = $_POST['PMNAME'];
$PMPKEY = $_POST['PMPKEY'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';

/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$PMNAME = cmHscDe($PMNAME);
$PMPKEY = cmHscDe($PMPKEY);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'15',$userData[0]['WUSAUT']);//'15' => ピボット設定
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ピボット設定の権限'));
            }
        }
    }
}
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$PMNAME,$PMPKEY);
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 3;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$PMNAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのピボットの権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('ピボット設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
/*
*-------------------------------------------------------* 
* 削除処理
*-------------------------------------------------------*
*/

//DB2PMST登録
if($rtn === 0){
    //存在確認
    $rs = fnGetDB2PMST($db2con,$PMNAME,$PMPKEY);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
        if(count($data) === 0){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_DEL',array('ピボット'));
        }
    }
}
//DB2PMST削除
if($rtn === 0){
    $pivotKey = $PMPKEY;
    $rs = fnDeleteDB2PMST($db2con,$PMNAME,$pivotKey);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }
}

//DB2PCOL削除
if($rtn === 0){
    $rs = fnDeleteDB2PCOL($db2con,$PMNAME,$pivotKey);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }
}

//DB2PCAL削除
if($rtn === 0){
    $rs = fnDeleteDB2PCAL($db2con,$PMNAME,$pivotKey);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }
}
//DB2PCOL削除
if($rtn === 0){
    $rs = fnDeleteDB2WSCD($db2con,$PMNAME,$pivotKey);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }
}

//DB2WAUT削除
if($rtn === 0){
    $rs = fnDeleteDB2WAUT($db2con,$PMNAME,$pivotKey);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }
}
//DB2WHIS削除
if($rtn === 0){
    $rs = fnDeleteDB2WHIS($db2con,$PMNAME,$pivotKey);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }
}

//DB2WSOC削除
if($rtn === 0){
    $rs = fnDeleteDB2WSOC($db2con,$PMNAME,$pivotKey);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }
}
if($rtn === 0){
    //お気に入り削除
    $rs = cmDeleteDB2QBMK($db2con,'',$PMNAME,$PMPKEY,'','');
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }    
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* DB2PMST
*-------------------------------------------------------*
*/

function fnGetDB2PMST($db2con,$pmname,$pmpkey){
    $data = array();
    $params = array($pmname,$pmpkey);

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2PMST AS A ' ;
    $strSQL .= ' WHERE A.PMNAME = ? AND A.PMPKEY = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* DB2PMST 削除
*-------------------------------------------------------*
*/

function fnDeleteDB2PMST($db2con,$PMNAME,$PMPKEY){


    $rs = true;
    //構文
    $strSQL  = ' DELETE FROM DB2PMST WHERE PMNAME = ? AND PMPKEY = ? ';

    $params = array(
        $PMNAME,
        $PMPKEY
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* DB2PCOL 削除
*-------------------------------------------------------*
*/

function fnDeleteDB2PCOL($db2con,$WPNAME,$WPPKEY){


    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2PCOL WHERE WPNAME = ? AND WPPKEY = ? ';
    $params = array(
        $WPNAME,
        $WPPKEY
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* DB2PCAL 削除
*-------------------------------------------------------*
*/

function fnDeleteDB2PCAL($db2con,$WCNAME,$WCCKEY){


    $rs = true;

    $strSQL  = ' DELETE FROM DB2PCAL WHERE WCNAME = ? AND WCCKEY = ? ';

    $params = array(
        $WCNAME,
        $WCCKEY
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* DB2WSCD 削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WSCD($db2con,$WSNAME,$WSPKEY){


    $rs = true;

    $strSQL  = ' DELETE FROM DB2WSCD WHERE WSNAME = ? AND WSPKEY = ? ';

    $params = array(
        $WSNAME,
        $WSPKEY
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }

    return $rs;

}
/*
*-------------------------------------------------------* 
* DB2WAUT 削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WAUT($db2con,$WANAME,$WAPKEY){


    $rs = true;

    $strSQL  = ' DELETE FROM DB2WAUT WHERE WANAME = ? AND WAPKEY = ? ';

    $params = array(
        $WANAME,
        $WAPKEY
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }

    return $rs;

}
/*
*-------------------------------------------------------* 
* DB2WHIS 削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WHIS($db2con,$WHNAME,$WHPKEY){


    $rs = true;

    $strSQL  = ' DELETE FROM DB2WHIS WHERE WHNAME = ? AND WHPKEY = ? ';

    $params = array(
        $WHNAME,
        $WHPKEY
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }

    return $rs;

}
/*
*-------------------------------------------------------* 
* DB2WSOC 削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WSOC($db2con,$SONAME,$SOPKEY){


    $rs = true;

    $strSQL  = ' DELETE FROM DB2WSOC WHERE SONAME = ? AND SOPKEY = ? ';

    $params = array(
        $SONAME,
        $SOPKEY
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }

    return $rs;

}