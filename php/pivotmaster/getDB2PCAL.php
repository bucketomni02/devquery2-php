<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$d1name = $_POST['D1NAME'];
$pmpkey = $_POST['PMPKEY'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmlspecialcharsをデコード
$d1name = cmHscDe($d1name);
$pmpkey = cmHscDe($pmpkey);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'15',$userData[0]['WUSAUT']);//'15' => ピボット設定
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ピボット設定の権限'));
            }
        }
    }
}
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$d1name,$pmpkey);
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 3;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$d1name,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのピボットの権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('ピボット設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $rs = fnGetDB2PCAL($db2con,$d1name,$pmpkey);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'aaData' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* DB2PCOL
*-------------------------------------------------------*
*/

function fnGetDB2PCAL($db2con,$wcname,$wcckey){
    $data = array();
    $params = array($wcname,$wcckey);

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2PCAL AS A ' ;
    $strSQL .= ' WHERE A.WCNAME = ? AND A.WCCKEY = ? ';
    $strSQL .= ' ORDER BY A.WCSEQN ASC ' ;

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}