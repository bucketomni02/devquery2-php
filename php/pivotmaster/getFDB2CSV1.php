<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$D1NAME = $_POST['D1NAME'];
$D1TEXT = $_POST['D1TEXT'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);
$D1TEXT = cmHscDe($D1TEXT);

$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

if($proc === 'EDIT'){
    if($rtn === 0){
        $rs = fnSelFDB2CSV1($db2con,$D1NAME);
        if($res['result'] === true){
            if(count($res['data']) === 0){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET_QRY',array('ピボット','クエリー'));
            }else{
                $data = $res['data'];
            }
        }else{
            $rtn = 1;
            $msg = showMsg($res['result']);
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $rs = fnGetAllCount($db2con,$D1NAME,$D1TEXT);
        if($rs['result'] !== true){
             $rtn = 1;
             $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){     
        $rs = fnGetFDB2CSV1($db2con,$D1NAME,$D1TEXT,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
             $rtn = 1;
             $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }

}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg)
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con, $D1NAME, $D1TEXT, $start = '', $length = '', $sort = '', $sortDir = ''){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.*,  ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.D1NAME ASC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM FDB2CSV1 as B ';


    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params,'%'.$D1NAME.'%');
    }

    if($D1TEXT != ''){
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params,'%'.$D1TEXT.'%');
    }
    
    $strSQL .= ' ) as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$D1NAME = '',$D1TEXT = ''){
    $data = array();

    $strSQL  = ' SELECT count(A.D1NAME) as COUNT ';
    $strSQL .= ' FROM FDB2CSV1 as A ' ;
    $strSQL .= ' WHERE D1NAME <> \'\' ';

    $params = array();

    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params,'%'.$D1NAME.'%');
    }

    if($D1TEXT != ''){
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params,'%'.$D1TEXT.'%');
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* FDB2CSV1取得(一行)
*-------------------------------------------------------*
*/

function fnSelFDB2CSV1($db2con, $D1NAME){
    $data = array();
    $params = array($D1NAME);

    $strSQL  = ' SELECT A.*,B.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' LEFT JOIN FDB2CSV1PG AS B ON A.D1NAME = B.DGNAME ';
    $strSQL .= ' WHERE D1NAME = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}