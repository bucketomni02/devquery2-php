<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$d1name = $_POST['D1NAME'];
$pmpkey = (isset($_POST['PMPKEY'])?$_POST['PMPKEY']:'');

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
//クエリー存在チェック
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$d1name,'');
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 1;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}

if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$d1name,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのピボットの権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('ピボット設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}

if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$d1name,$pmpkey);
    if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
    }else{
        if(count($rs['data']) === 0){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET',array('クエリー'));
        }else{
            $data = $rs['data'];
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'DATA' => umEx($data,true),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con,$d1name,$pmpkey){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.*,B.* ';
    $strSQL .= ' FROM FDB2CSV1 AS A ';
    $strSQL .= ' LEFT JOIN DB2PMST AS B ';
    $strSQL .= ' ON D1NAME = PMNAME AND PMPKEY = ? ';
    $strSQL .= ' WHERE D1NAME = ? ';


    $params = array($pmpkey,$d1name);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}