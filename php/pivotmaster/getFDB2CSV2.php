<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$d2name = $_POST['D1NAME'];
$type = $_POST['type']; // S or N

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'15',$userData[0]['WUSAUT']);//'15' => ピボット設定
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ピボット設定の権限'));
            }
        }
    }
}
if($rtn === 0){
    $rs = fnGetFDB2CSV2($db2con, $rs, $d2name,$type);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'aaData' => umEx($data),
    'RTN' => $rtn,
    'MSG'=> $msg
);
echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV2取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV2($db2con,&$rs = '0', $d2name = '',$type = ''){
    $data = array();
    $params = array();


/*
        $strSQL  = ' SELECT A.D2FILID, A.D2FLD, A.D2HED ';
        $strSQL .= ' FROM FDB2CSV2 AS A ' ;
        $strSQL .= ' WHERE A.D2NAME <> \'\' ';
        $strSQL .= ' AND D2NAME = ? ';

        array_push($params,$d2name);

        if($type === 'S'){
            $strSQL .= ' AND ( ';
            $strSQL .= ' D2TYPE <> ? ';
            $strSQL .= ' AND D2TYPE <> ? ';
            $strSQL .= ' AND D2TYPE <> ? ';
            $strSQL .= ' ) ';
        }else if($type === 'N'){
            $strSQL .= ' AND ( ';
            $strSQL .= ' D2TYPE = ? ';
            $strSQL .= ' OR D2TYPE = ? ';
            $strSQL .= ' OR D2TYPE = ? ';
            $strSQL .= ' ) ';
        }

        array_push($params,'S');
        array_push($params,'P');
        array_push($params,'B');

        $strSQL .= ' ORDER BY A.D2CSEQ ASC ' ;
*/

    $params = array(
        $d2name,
        $d2name
    );

    $strSQL  = ' SELECT A.* FROM ( ';
    $strSQL .= ' SELECT D2NAME,D2FILID,D2FLD,D2HED,D2CSEQ,D2WEDT, ';
    $strSQL .= ' D2TYPE,D2LEN,D2DNLF ';
    $strSQL .= ' FROM FDB2CSV2 ' ;
    $strSQL .= ' WHERE D2NAME = ? ';
    $strSQL .= ' AND D2CSEQ > 0 ';
    $strSQL .= ' UNION ALL ';
    $strSQL .= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID, ';
    $strSQL .= ' D5FLD AS D2FLD,D5HED AS D2HED,D5CSEQ AS D2CSEQ, ';
    $strSQL .= ' D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DNLF AS D2DNLF ';
    $strSQL .= ' FROM FDB2CSV5 ';
    $strSQL .= ' WHERE D5NAME = ? AND D5CSEQ > 0 ';
    $strSQL .= ' ) AS A ';

    if($type === 'S'){
        //縦軸横軸にタイプ制限はかけない
/*
        $strSQL .= ' WHERE ';
        $strSQL .= ' D2TYPE <> ? ';
        $strSQL .= ' AND D2TYPE <> ? ';
        $strSQL .= ' AND D2TYPE <> ? ';
*/
    }else if($type === 'N'){
        $strSQL .= ' WHERE ';
        $strSQL .= ' D2TYPE = ? ';
        $strSQL .= ' OR D2TYPE = ? ';
        $strSQL .= ' OR D2TYPE = ? ';

        array_push($params,'S');
        array_push($params,'P');
        array_push($params,'B');
    }



    $strSQL .= ' ORDER BY A.D2CSEQ ASC ';



    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,true));
        }
    }
    return $data;

}