<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$D1NAME = $_POST['D1NAME'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'15',$userData[0]['WUSAUT']);//'15' => ピボット設定
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ピボット設定の権限'));
            }
        }
    }
}
//クエリー存在チェック
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$D1NAME,'');
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 1;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのピボットの権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('ピボット設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}


if($rtn === 0){
    $rs = fnGetAllCount($db2con,$D1NAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $allcount = $rs['data'];
    }
}
if($rtn === 0){
    $rs = fnGetDB2PMST($db2con,$D1NAME,$start,$length,$sort,$sortDir);
    if($rs['result'] !== true){
        $rtn =1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg)
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/

function fnGetDB2PMST($db2con, $D1NAME, $start = '', $length = '', $sort = '', $sortDir = ''){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.*,  ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.PMPKEY ASC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM DB2PMST as B ';

    $strSQL .= ' WHERE PMNAME <> \'\' ';
    
    $strSQL .= ' AND PMNAME = ? ';
    array_push($params,$D1NAME);
    
    $strSQL .= ' ) as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$PMNAME = ''){
    $data = array();

    $strSQL  = ' SELECT count(A.PMNAME) as COUNT ';
    $strSQL .= ' FROM DB2PMST as A ' ;
    $strSQL .= ' WHERE PMNAME <> \'\' ';

    $params = array();

    $strSQL .= ' AND PMNAME = ? ';
    array_push($params,$PMNAME);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* FDB2CSV1取得(一行)
*-------------------------------------------------------*
*/

function fnSelFDB2CSV1($db2con, $D1NAME){
    $data = array();
    $params = array($D1NAME);

    $strSQL  = ' SELECT A.*,B.* ';
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' LEFT JOIN FDB2CSV1PG AS B ON A.D1NAME = B.DGNAME ';
    $strSQL .= ' WHERE D1NAME = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}