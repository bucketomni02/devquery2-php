<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*xsort/ysort <=>  0=>descending order,1=> ascending order
*-------------------------------------------------------*
*/

$proc =  $_POST['proc'];
$D1NAME = $_POST['D1NAME'];
$PMPKEY = $_POST['PMPKEY'];
$DATA = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';

$xcolumn  = array();     //縦軸
$xchecked = array();     //縦軸表示
$xschecked = array();     //合計
$xsorted = array();  // 昇順／降順ソート
$ycolumn  = array();     //横軸
$ychecked = array();     //横軸表示
$yschecked = array();     //合計
$ysorted = array();
$ccolumn  = array();     //集計値
$calc = array();        //計算式
$hiddenField = true;
$xsflg = false;         //縦軸を一つでも設定していたらtrue
$ysflg = false;         //横軸を一つでも設定していたらtrue
$csflg = false;         //集計値を一つでも設定していたらtrue

foreach($DATA as $key => $value){

    if(substr($key,0,6) === 'xcombo'){
        $exp = explode("-",$value);
        $tmp = array(
            'FILID' => ($exp[0] !== null)?$exp[0]:'',
            'FLD' => ($exp[1] !== null)?$exp[1]:''
        );
        $xcolumn[] = $tmp;

        //連番を取得して表示配列の値を取得。配列に追加
        //表示の場合はブランク、非表示の場合は1
        $no = str_replace(substr($key,0,6),"",$key);
        if(array_key_exists('xcheck'.$no,$DATA)){
            $hiddenField = false;
            $xchecked[] = '';
        }else{
            $xchecked[] = 1;
        }
        //集計の場合はブランク、集計じゃない場合は１
        if(array_key_exists('xscheck'.$no,$DATA)){
            $xschecked[] = '';
        }else{
            $xschecked[] = 1;
        }
           $xsorted[] = $DATA['xsort'.$no];

    }else if(substr($key,0,6) === 'ycombo'){
        $exp = explode("-",$value);
        $tmp = array(
            'FILID' => ($exp[0] !== null)?$exp[0]:'',
            'FLD' => ($exp[1] !== null)?$exp[1]:''
        );
        $ycolumn[] = $tmp;

        //連番を取得して表示配列の値を取得。配列に追加
        //表示の場合はブランク、非表示の場合は1
        $no = str_replace(substr($key,0,6),"",$key);

        if(array_key_exists('ycheck'.$no,$DATA)){
            $hiddenField = false;
            $ychecked[] = '';
        }else{
            $ychecked[] = 1;
        }
        //集計フラグ
        //集計の場合はブランク、集計じゃない場合は１
        if(array_key_exists('yscheck'.$no,$DATA)){
            $yschecked[] = '';
        }else{
            $yschecked[] = 1;
        }
           $ysorted[] = $DATA['ysort'.$no];

    }else if(substr($key,0,6) === 'ccombo'){
        $exp = explode("-",$value);
        $tmp = array(
            'FILID' => ($exp[0] !== null)?$exp[0]:'',
            'FLD' => ($exp[1] !== null)?$exp[1]:''
        );
        $ccolumn[] = $tmp;
    }else if(substr($key,0,4) === 'text'){
        $calc[] = $value;
    }
}
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'15',$userData[0]['WUSAUT']);//'15' => ピボット設定
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ピボット設定の権限'));
            }
        }
    }
}
//クエリー存在チェッ
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$D1NAME,$PMPKEY);
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 3;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのピボットの権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('ピボット設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    if($DATA['PKTEXT'] === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('ピボットキー'));
    }
}

if($rtn === 0){
    if(!checkMaxLen($DATA['PKTEXT'],14)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('ピボットキー'));
    }
}

if($rtn === 0){
    $byte = checkByte($DATA['PKTEXT']);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('ピボットキー'));
    }
}

if($rtn === 0){
    $text = substr($DATA['PKTEXT'],0,6);
    $pktext = strtolower($text);
    if($pktext === 'params'){
        $rtn = 1;
        $msg = "ピボットキーの".$DATA['PKTEXT']."に".$text."は入力できません。";
    }
}

if($rtn === 0){
      $PMTEXT = cmMer($DATA['PMTEXT']);
    if($PMTEXT === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('ピボット名'));
    }
}

if($rtn === 0){
    if(!checkMaxLen($DATA['PMTEXT'],50)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('ピボット名'));
    }
}

if($rtn === 0){
    foreach($xcolumn as $key => $value){

        $byte = checkByte($value['FILID']);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('縦軸'));
//'縦軸に登録できない文字が含まれています。';
            break;
        }

        $byte = checkByte($value['FLD']);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('縦軸'));
            //$msg = '縦軸に登録できない文字が含まれています。';
            break;
        }

        if(!checkMaxLen($value['FILID'],4)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('縦軸'));
            //$msg = '縦軸に登録できない文字が含まれています。';
            break;
        }

        if(!checkMaxLen($value['FLD'],10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('縦軸'));
            break;
        }
        if($value['FILID'] !== '' && $value['FLD'] !== ''){
            $xsflg = true;

            //FDB2CSV2に存在するかチェック(FILID=9999は無条件でtrue)
            if($value['FILID'] === '9999'){
                $rs = array();
            }else{
                $rs = cmCheckFDB2CSV2($db2con,$D1NAME,$value['FILID'],$value['FLD']);
            }
            if($rs === false){
                $rtn = 1;
                $msg = showMsg('FAIL_SYS');
            }else{
                if(count($rs) === 0){
                    //FDB2CSV5に存在するかチェック
                    $rs = cmCheckFDB2CSV5($db2con,$D1NAME,$value['FLD']);
                    if($rs === false){
                        $rtn = 1;
                        $msg = showMsg('FAIL_SYS');
                    }else{
                        if(count($rs) === 0){
                            $rtn = 1;
                            $msg = showMsg('FAIL_COL',array('クエリー','カラム'));                            
                        }
                    }
                }
            }
        }
    }
}

if($rtn === 0){
    foreach($ycolumn as $key => $value){

        $byte = checkByte($value['FILID']);
        if($byte[1] > 0){
            $rtn = 1;
            //$msg = '横軸に登録できない文字が含まれています。';
            $msg = showMsg('FAIL_BYTE',array('横軸'));
            break;
        }

        $byte = checkByte($value['FLD']);
        if($byte[1] > 0){
            $rtn = 1;
            //$msg = '横軸に登録できない文字が含まれています。';
            $msg = showMsg('FAIL_BYTE',array('横軸'));
            break;
        }

        if(!checkMaxLen($value['FILID'],4)){
            $rtn = 1;
            //$msg = '横軸に登録できない文字が含まれています。';
            $msg = showMsg('FAIL_MAXLEN',array('横軸'));
            break;
        }

        if(!checkMaxLen($value['FLD'],10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('横軸'));
            break;
        }

        if($value['FILID'] !== '' && $value['FLD'] !== ''){
            $ysflg = true;

            //FDB2CSV2に存在するかチェック(FILID=9999は無条件でtrue)
            if($value['FILID'] === '9999'){
                $rs = array();
            }else{
                $rs = cmCheckFDB2CSV2($db2con,$D1NAME,$value['FILID'],$value['FLD']);
            }
            if($rs === false){
                $rtn = 1;
                $msg = showMsg('FAIL_SYS');
            }else{
                if(count($rs) === 0){
                    //FDB2CSV5に存在するかチェック
                    $rs = cmCheckFDB2CSV5($db2con,$D1NAME,$value['FLD']);
                    if($rs === false){
                        $rtn = 1;
                        $msg = showMsg('FAIL_SYS');
                    }else{
                        if(count($rs) === 0){
                            $rtn = 1;
                            $msg = showMsg('FAIL_COL',array('クエリー','カラム'));
                        }
                    }
                }
            }
        }
    }
}

if($rtn === 0){

    foreach($ccolumn as $key => $value){

        $byte = checkByte($value['FILID']);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('集計値'));
            break;
        }

        $byte = checkByte($value['FLD']);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('集計値'));
            break;
        }

        if(!checkMaxLen($value['FILID'],4)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('集計値'));
            break;
        }

        if(!checkMaxLen($value['FLD'],10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('集計値'));
            break;
        }

        if($value['FILID'] !== '' && $value['FLD'] !== ''){
            $csflg = true;

            //FDB2CSV2に存在するかチェック(FILID=9999は無条件でtrue)
            if($value['FILID'] === '9999'){
                $rs = array();
            }else{
                $rs = cmCheckFDB2CSV2($db2con,$D1NAME,$value['FILID'],$value['FLD']);
            }
            if($rs === false){
                $rtn = 1;
                $msg = showMsg('FAIL_SYS');
            }else{
                if(count($rs) === 0){
                    //FDB2CSV5に存在するかチェック
                    $rs = cmCheckFDB2CSV5($db2con,$D1NAME,$value['FLD']);
                    if($rs === false){
                        $rtn = 1;
                        $msg = showMsg('FAIL_SYS');
                    }else{
                        if(count($rs) === 0){
                            $rtn = 1;
                            $msg = showMsg('FAIL_COL',array('クエリー','カラム'));
                        }
                    }
                }
            }
        }
    }
}

if($rtn === 0){
    //縦横集計値が一つも設定されてない場合、エラー
    if($xsflg === false && $ysflg === false && $csflg === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SET',array(array('集計','条件')));
    //縦横が一つも設定されてない場合、エラー
    }else if($xsflg === false && $ysflg === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SET',array(array('縦軸','横軸')));
    //縦横いずれかが設定してあって、集計値が設定されていない場合、エラー
    }else if(($xsflg === true || $ysflg === true) && $csflg === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SET',array('集計値'));
    }
}
    /*
    *
    *全体のフィールドに非表示チェックボックスされてるかどうか
    *
    */
    if($rtn === 0){
        if($hiddenField !== false){
            $rtn = 1;
            $msg = showMsg('PIVOT_CREATE_FAIL');
        }
    }
if($rtn === 0){

    foreach($calc as $key => $value){

        if(!checkMaxLen($value,100)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('計算式'));
            break;
        }

    }

}
/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/
//DB2PMST登録
if($proc === 'ADD'){
    if($rtn === 0){
        //存在確認
        $rs = fnGetDB2PMST($db2con,$D1NAME,$DATA['PKTEXT']);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
    if($rtn === 0){
        if(count($rs['data'])>0){
            $rtn = 1;
            $msg = showMsg('ISEXIST',array('ピボットキー'));
        }else{
			//TTA added else if condition for PMDFLG and PVNTFLG flag
            if ($DATA['PMDFLG'] == '') {
                $PVNTFLG_new = '';
            }else{
                $PVNTFLG_new = $DATA['PVNTFLG'];
            }
            $rs = fnInsertDB2PMST($db2con,$D1NAME,$DATA['PMTEXT'],$DATA['PKTEXT'],$DATA['PMDFLG']);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }
    }
}else{
    if($rtn === 0){
        //存在確認
        $rs = fnGetDB2PMST($db2con,$D1NAME,$DATA['PKTEXT']);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
    if($rtn === 0){
        if(count($rs['data']) === 0){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_UPD',array('ピボットキー'));
        }else{
			//TTA added else if condition for PMDFLG and PVNTFLG flag
            if ($DATA['PMDFLG'] == '') {
                $PVNTFLG_new = '';
            }else{
                $PVNTFLG_new = $DATA['PVNTFLG'];
            }
            $rs = fnUpdateDB2PMST($db2con,$D1NAME,$DATA['PMTEXT'],$DATA['PKTEXT'],$DATA['PMDFLG']);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }
    }
}


//DB2PCOL削除
if($rtn === 0){
    $rs = fnDeleteDB2PCOL($db2con,$D1NAME,$DATA['PKTEXT']);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

//DB2PCAL削除
if($rtn === 0){
    $rs = fnDeleteDB2PCAL($db2con,$D1NAME,$DATA['PKTEXT']);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
//DB2PCOL追加(縦軸)
$xchk = array();
if($rtn === 0){
    $seqn = 0;
    $PKTEXT = $DATA['PKTEXT'];
    foreach($xcolumn as $key => $value){
        $xcom =$value['FLD'].'_'.$value['FILID'];
        if($value['FILID'] !== '' && $value['FLD'] !== '' && !in_array($xcom,$xchk)){
            array_push($xchk,$xcom);
            $seqn++;
            $rs = fnInsertDB2PCOL($db2con,$D1NAME,$PKTEXT,'1',$seqn,$value['FILID'],$value['FLD'],$xchecked[$key],$xschecked[$key],$xsorted[$key]);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
                break;
            }
        }
    }
}

//DB2PCOL追加(横軸)
$ychk = array();
if($rtn === 0){
    $seqn = 0;
    $PKTEXT = $DATA['PKTEXT'];
    foreach($ycolumn as $key => $value){
        $ycom =$value['FLD'].'_'.$value['FILID'];
        if($value['FILID'] !== '' && $value['FLD'] !== '' && !in_array($ycom,$ychk)){
            array_push($ychk,$ycom);
            $seqn++;
            $rs = fnInsertDB2PCOL($db2con,$D1NAME,$PKTEXT,'2',$seqn,$value['FILID'],$value['FLD'],$ychecked[$key],$yschecked[$key],$ysorted[$key]);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
                break;
            }
        }
    }
}
$cchk = array();
//DB2PCOL追加(集計値)
if($rtn === 0){
    $seqn = 0;
    $PKTEXT = $DATA['PKTEXT'];
    foreach($ccolumn as $key => $value){
        $ccom =$value['FLD'].'_'.$value['FILID'];
        if($value['FILID'] !== '' && $value['FLD'] !== '' && !in_array($ccom,$cchk)){
            array_push($cchk,$ccom);
            $seqn++;
            $rs = fnInsertDB2PCOL($db2con,$D1NAME,$PKTEXT,'3',$seqn,$value['FILID'],$value['FLD'],'','','');
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
                break;
            }
        }
    }
}

//DB2PCAL追加
if($rtn === 0){

    $shikiArr = array('式1','式2','式3','式4','式5','式6','式7','式8','式9','式10');
    $seqn = 0;
    $PKTEXT = $DATA['PKTEXT'];
    $ccolumnSetting = cColumnSETTING($ccolumn);
    $numDataCheck = numDataCheck($db2con,$calc,$ccolumn,$ccolumnSetting);
    if($numDataCheck['RTN'] === true){
           $numData = $numDataCheck['calRes'];
            foreach($numData as $key => $value){
                if($value !== '' ){
                    $seqn++;
                    $shikiSub =  cmMer(substr($value, 0, strpos($value, ':')));
                        if(in_array($shikiSub,$shikiArr)){
                             $value = str_replace($shikiSub,'式'.$seqn,$value);
                        }
                    $rs = fnInsertDB2PCAL($db2con,$D1NAME,$PKTEXT,$seqn,$value);
                    if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs);
                        break;
                    }
                }
            }
    }else{
               $rtn = 1;
                $msg = showMsg($numDataCheck['RTN']);
    }
}


/*
//DB2PMST登録
if($proc === 'ADD'){
    if($rtn === 0){
        //存在確認
        $rs = fnGetDB2PMST($db2con,$D1NAME,$DATA['PKTEXT']);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
    if($rtn === 0){
        if(count($rs['data'])>0){
            $rtn = 1;
            $msg = showMsg('ISEXIST',array('ピボットキー'));
        }else{
            $rs = fnInsertDB2PMST($db2con,$D1NAME,$DATA['PMTEXT'],$DATA['PKTEXT']);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }
    }
}else{
    if($rtn === 0){
        //存在確認
        $rs = fnGetDB2PMST($db2con,$D1NAME,$DATA['PKTEXT']);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
    if($rtn === 0){
        if(count($rs['data']) === 0){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_UPD',array('ピボットキー'));
        }else{
            $rs = fnUpdateDB2PMST($db2con,$D1NAME,$DATA['PMTEXT'],$DATA['PKTEXT']);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }
    }
}
*/
$flg = 1;
if($rtn !== 0){
    $flg = 2;
    db2_rollback($db2con);
}else{
    db2_commit($db2con);
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'FLG' => $flg,
    'MSG' => $msg,
    'ccolumnSetting' => $ccolumnSetting,
    'DATA' =>$DATA,
    'numDataCheck' =>$numDataCheck
);

echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* CCOLUMNSETTING
*-------------------------------------------------------*
*/
function cColumnSETTING($ccolumn){
        $newArray = array();
        $bindIndex = 0;
        $check = array();
        foreach($ccolumn as $k => $va){
                $value = $va['FLD'];
               /* if($value !== ''){
                        if(!in_array($value,$check)){
                               $bindIndex++;
                                $newArray[] = array(
                                        'bindIndex' => $bindIndex,
                                        'oldbindIndex' => $k+1
                                );
                               array_push($check,$value);
                        }else{
                               $getOldKey = array_search($value,$check);
                                $newArray[] = array(
                                        'bindIndex' => $getOldKey+1,
                                        'oldbindIndex' => $k+1
                                );
                        }
                }*/
                if($value !== ''){
                        if(!in_array($value,$check)){
                            $bindIndex++;
                            $newArray[$k+1] = $bindIndex;
                            array_push($check,$value);
                        }else{
                                   $getOldKey = array_search($value,$check);
                                        $newArray[$k+1] = $getOldKey+1;
                         }
                }
        }
        return $newArray;
}
/*
*-------------------------------------------------------* 
* NUMBER DATA CHECKING
*-------------------------------------------------------*
*/
function numDataCheck($db2con,$calc,$ccolumn,$ccolumnSetting){
        $rtn = true;
        $result = array();
        $shikiArr = array('式1','式2','式3','式4','式5','式6','式7','式8','式9','式10');
        $numData = array();
        $ccolumnKeyArr = array();
        foreach($ccolumn as $key =>$value){
              if($value['FILID'] !== '' && $value['FLD'] !== ''){
                    array_push($ccolumnKeyArr,'$'.($key+1).'$');
              }
        }
          $calRes = array();
        $calcValue = '';
        foreach($calc as $key => $value){
                    $calcKeyArr = array();
                    if($value !== ''){
                            for($i = 1 ; $i <=10 ; $i++){
                                    $chkStr = '$'.$i.'$';
                                    if (strpos($value, $chkStr) !== false) {
                                            if(!in_array($chkStr,$calcKeyArr)){
                                                    if(in_array($chkStr,$ccolumnKeyArr)){
                                                            array_push($calcKeyArr,$chkStr);
                                                            $value = str_replace($chkStr,'$'.$ccolumnSetting[$i].'$',$value);
                                                     }else{
                                                            $rtn = 'PIVOT_FMUL_COL';
                                                     }
                                            }
                                    }
                                  if($rtn !== true) break;
                            }
                          if($rtn !== true){
                             break;
                          }else{
                                    array_push($calRes,$value);
                          }
                }
        }
    $strSQL = '';
    $calcResult = '';
     $valTest = array();
        if($rtn === true){
                foreach($calRes as $key => $value){
                    if($value !== ''){
                                    $formatFlg = true;
                                        $SHIKI = explode(":",$value);
                                        if(count($SHIKI) === 2){
                                                $SHIKIVAL  =$SHIKI[1];
                                                        if($SHIKIVAL !== ''){
                                                                $val =  splitNumFormat($SHIKIVAL);
                                                                     foreach($val as $k => $v){
                                                                            if($v !== ''){
                                                                                   $vl = str_replace('$',' ',$v,$i);//$i is to count replace '$' in string
                                                                                    if( $i % 2 !== 0 ){
                                                                                        $formatFlg = false;
                                                                                         break;
                                                                                    }else{
                                                                                            if(strpos($v, '$') !== false){//check contains '$' in string
                                                                                                 $v =  str_replace(' ','',$v);
                                                                                                 $vArr = str_split($v);
                                                                                                 if(count($vArr) === 3 && $v !== '$$$'){
                                                                                                         if($vArr[0] !== '$'  && $vArr[2] !== '$'){
                                                                                                            $formatFlg = false;
                                                                                                             break;
                                                                                                         }
                                                                                                  }else{
                                                                                                        $formatFlg = false;
                                                                                                         break;
                                                                                                  }
                                                                                            }
                                                                                    }
                                                                            }
                                                                   }
                                                                    e_log('check1'.$formatFlg.'formula'.$SHIKIVAL);
                                                                    if($formatFlg !== false){
                                                                                if($calcResult != ''){
                                                                                    $calcResult .= ',';
                                                                                }
                                                                                  $SHIKIVAL = str_replace('$',' ',$SHIKIVAL);
                                                                                  $calcResult .= $SHIKIVAL;
                                                                    }else{
                                                                            $rtn = 'PIVOT_QFMULA';
                                                                            break;
                                                                    }
                                                        }else{
                                                             $rtn = 'PIVOT_CERR';
                                                        }
                                        }else{
                                                     $rtn = 'PIVOT_CERR';
                                        }
                                  }else{
                                            $rtn = 'PIVOT_QFMULA';
                                            break;
                                  }
                }
        }

        if($rtn === true){
                if(count($calRes) > 0 ){
                        $data = array();
                        $params = array();

                        $strSQL  = ' SELECT  ';
                        $strSQL .= $calcResult;
                        $strSQL .= ' FROM DB2PCOL FETCH FIRST 1 ROWS ONLY ' ;
                        $stmt = db2_prepare($db2con,$strSQL);
                        if($stmt === false){
                             $rtn ='PIVOT_QFMULA';
                        }else{
                            $r = db2_execute($stmt,$params);
                            if($r === false){
                                 $rtn ='PIVOT_QFMULA';
                            }else{
                                    $row = db2_fetch_assoc($stmt);
                                    if($row === false){
                                         $rtn ='PIVOT_QFMULA';
                                    }
                            }
                        }
                }
        }
        if($rtn === true){
                $result = array('RTN' => true, 'data' => array($calcKeyArr,$ccolumnKeyArr),'calRes' =>$calRes,'calcResult' =>$calcResult,'sql' =>$strSQL);
        }else{
                $result = array('RTN' =>$rtn , 'data' => array($calcKeyArr,$ccolumnKeyArr),'calRes' =>$calRes,'calcResult' =>$calcResult,'sql' =>$strSQL);
         }
        return $result;
//        return array($calcKeyArr,$ccolumnKeyArr);
}

function  splitNumFormat($input){
        $operator = array('+','-','*');
        array_push($operator,'/');
        array_push($operator,'(');
        array_push($operator,')');
        $input =  str_replace(' ','',$input);
        $vArr = str_split($input);
        $str = '';
        foreach($vArr as $key =>$v){
            if(in_array($v,$operator)){
               $str .= ' ';
            }
               $str .= $v;
            if(in_array($v,$operator)){
               $str .= ' ';
            }
        }
        $output = preg_split( "/ (\-|\+|\*|\/|\)|\() /", $str );
        return $output;
}

/*
*-------------------------------------------------------* 
* DB2PMST
*-------------------------------------------------------*
*/

function fnGetDB2PMST($db2con,$pmname,$pmpkey){
    $data = array();
    $params = array($pmname,$pmpkey);

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2PMST AS A ' ;
    $strSQL .= ' WHERE A.PMNAME = ? AND A.PMPKEY = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* DB2PMST 削除
*-------------------------------------------------------*
*/

function fnDeleteDB2PMST($db2con,$PMNAME){


    $rs = true;

    //構文

    $strSQL  = ' DELETE FROM DB2PMST WHERE PMNAME = ? ';

    $params = array(
        $PMNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* DB2PCOL 削除
*-------------------------------------------------------*
*/

function fnDeleteDB2PCOL($db2con,$WPNAME,$WPPKEY){


    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2PCOL WHERE WPNAME = ? AND WPPKEY = ? ';

    $params = array(
        $WPNAME,
        $WPPKEY
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* DB2PCAL 削除
*-------------------------------------------------------*
*/

function fnDeleteDB2PCAL($db2con,$WCNAME,$WCCKEY){


    $rs = true;

    //構文

    $strSQL  = ' DELETE FROM DB2PCAL WHERE WCNAME = ? AND WCCKEY = ? ';

    $params = array(
        $WCNAME,
        $WCCKEY
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }

    return $rs;
}

/*
*-------------------------------------------------------* 
* DB2PCOL
*-------------------------------------------------------*
*/

function fnInsertDB2PCOL($db2con,$WPNAME,$WPPKEY,$WPPFLG,$WPSEQN,$WPFILID,$WPFLD,$WPFHIDE,$WPSUMG,$WPSORT){


    $rs = true;

    //構文
    $strSQL  = ' INSERT INTO DB2PCOL(WPNAME,WPPKEY,WPPFLG,WPSEQN,WPFILID,WPFLD,WPFHIDE,WPSUMG,WPSORT) ';
    $strSQL .= ' VALUES(?,?,?,?,?,?,?,?,?) ';

    $params = array(
        $WPNAME,
        $WPPKEY,
        $WPPFLG,
        $WPSEQN,
        $WPFILID,
        $WPFLD,
        $WPFHIDE,
        $WPSUMG,
        $WPSORT
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* DB2PCAL
*-------------------------------------------------------*
*/

function fnInsertDB2PCAL($db2con,$WCNAME,$WCCKEY,$WCSEQN,$WCCALC){


    $rs = true;

    //構文

    $strSQL  = ' INSERT INTO DB2PCAL ';
    $strSQL .= ' ( ';
    $strSQL .= 'WCNAME,';
    $strSQL .= 'WCCKEY,';
    $strSQL .= 'WCSEQN,';
    $strSQL .= 'WCCALC  ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES(?,?,?,?) ';
    $params = array(
        $WCNAME,
        $WCCKEY,
        $WCSEQN,
        $WCCALC
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* DB2PMST INSERT
*-------------------------------------------------------*
*/

function fnInsertDB2PMST($db2con,$PMNAME,$PMTEXT,$PMPKEY,$PMDFLG){

    $rs = true;

    //構文

    $strSQL  = ' INSERT INTO DB2PMST ';
    $strSQL .= '(';
    $strSQL .= 'PMNAME,';
    $strSQL .= 'PMPKEY,';
    $strSQL .= 'PMTEXT,';
    $strSQL .= 'PMDFLG';
    $strSQL .= ')';
    $strSQL .= ' VALUES(?,?,?,?) ';

    $params = array(
        $PMNAME,
        $PMPKEY,
        $PMTEXT,
		$PMDFLG
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* DB2PMST UPDATE
*-------------------------------------------------------*
*/

function fnUpdateDB2PMST($db2con,$PMNAME,$PMTEXT,$PMPKEY,$PMDFLG){

    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2PMST ';
    $strSQL .= ' SET ';
    $strSQL .= ' PMTEXT = ?,PMDFLG = ?';
    $strSQL .= ' WHERE PMNAME = ? AND PMPKEY = ? ';

    $params = array(
        $PMTEXT,
		$PMDFLG,
        $PMNAME,
        $PMPKEY
    );

    error_log("Value of SQL******".print_r($params,true));

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
         error_log("statement fail " . db2_stmt_errormsg());

        }
    }
    return $rs;
}
