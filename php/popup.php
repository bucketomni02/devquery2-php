<html>
<head>
<meta charset="UTF-8">
<title>PHP QUERY</title>
<script type="text/javascript" src="../../common/lib/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="../../common/lib/blockui.js"></script>
<script type="text/javascript" src="../../common/lib/common.js"></script>
<style type="text/css">

/* html,bodyタグの設定 */
html, body{
	margin: 0;
	padding: 0;
	height: 100%;
	width: 100%;
    font-family:helvetica,arial,verdana,sans-serif;
    font-size:13px;
}
/* 全体を囲う大枠 */
div#contents {
	position: relative;
	min-height: 100%;
}

/* ヘッダーの背景 */
div#header{ 
	width:100%;
}
div#header #title{
    width:100%;
    height:16px;
    padding:10px 0px;
    background:#013760;
}

div#header #title a{
    color:#fff;
    margin-left:10px;
}

div#header #content{
    width:100%;
    height:24px;
    padding:6px 0px;
}

div#header #content #csvexcel{
    margin-left:8px;
}

/* ボディ */
div#body{  
}

div#body #body-inner{
    overflow:auto;
    height:100%;
    border-top:1px solid #ccc;
}

/* フッター */
div#footer
{
	width:100%;
	height: 26px;
	position: absolute;
	bottom: 0px;
	padding: 6px 0px;
    border-top:1px solid #ccc;
    background-color:#fff;
}

.htmltable{
    border-collapse:collapse;
    font-family:helvetica,arial,verdana,sans-serif;
    font-size:13px;
}

.htmltable tr{

}

.htmltable th,
.htmltable td{
    border:1px solid #ccc;
}

.htmltable th{
    background-color:#f5f5f5;
    padding:7px 10px;
    white-space:nowrap;
    border-top:none;
    cursor:pointer;
}

.htmltable td{
    padding:5px 10px 4px 10px;
    white-space:nowrap;
}

.htmltable .rowcolor{
    background-color:yellow;
}


#footer #total{
    float:right;
    margin-top:5px;
    margin-right:5px;
}

#footer .controll-group{
    width:280px;
    float:left;
    margin-left:8px;
    margin-top:3px;
}

.page-first{
    height:16px;
    width:16px;
    margin:2px 8px;
    background-repeat:no-repeat;
    background-image:url('../../common/images/page-first.png');
    float:left;
    opacity:0.5;
}

.page-prev{
    height:16px;
    width:16px;
    margin:2px 8px;
    background-repeat:no-repeat;
    background-image:url('../../common/images/page-prev.png');
    float:left;
    opacity:0.5;
}

.page-next{
    height:16px;
    width:16px;
    margin:2px 8px;
    background-repeat:no-repeat;
    background-image:url('../../common/images/page-next.png');
    float:left;
    opacity:0.5;
}

.page-last{
    height:16px;
    width:16px;
    margin:2px 8px;
    background-repeat:no-repeat;
    background-image:url('../../common/images/page-last.png');
    float:left;
    opacity:0.5;
}

.pager{
    width:130px;
    float:left;
    padding-left:8px;
    border:1px solid #ccc;
    border-top:none;
    border-bottom:none;
}

.pager #pager-text{
    width:40px;
    margin-left:10px;
    margin-right:3px;
}


.htmltable th .sort{
    color:transparent;
    margin-left:5px;
}

.htmltable th .sort.active{
    color:#000;
}

.loadingmsg{
    margin-top:15px;
}

</style>

<script type="text/javascript">


//戻る禁止
history.forward();

var table_propaty = {
    columns:[],
    tblName:'',
    csvfilename:'',
    d1text:'',
    dispstart:0,
    dispend:100,
    sortcol:'',
    sortdir:'',
    sortindex:'',
    page:1,
    length:100,
    lastpage:0,
    timeout:30000
}

var loadconf = {
    message:'<img src="../../common/images/loading.gif"/><div class="loadingmsg">データを取得しています。</div>',
    css:{backgroundColor:'transparent'}
}

var errormsg = {
    'timeout':'サーバー通信がタイムアウトしました。<br/>ネットワーク環境を確認してもう一度やり直してください。',
    'system':'プログラムが異常終了しました。管理者にお問い合わせください。'
}

$(function(){

    $.blockUI(loadconf);

    resizeBody();
    getTitle();
    $(window).resize(function() {
        resizeBody();
    });

    //pager-text Enter時、ページ移動
    $('#pager-text').keypress(function(ev) {
    	if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
            //数値以外は削除
    	    $(this).val(this.val().replace(/[^0-9]+/,''));

            var page = parseInt(this.val());
            
            //入力された数値が最大ページ数より大きい場合、最大ページに変換
            if(page > table_propaty.lastpage){
                page = table_propaty.lastpage;
            }

            //入力された数値が1より小さい場合、1に変換
            if(page < 1){
                page = 1;
            }

            table_propaty.page = page;
            this.val(table_propaty.page);

            changeStartEnd();
            createData(true);
        }
    });

    //pager-text blur時、table_propaty.pageの値をテキストボックスに戻す
    $("#pager-text").blur(function(){
        this.val(table_propaty.page);
    });


});

function resizeBody(){
    $("#body").height(window.innerHeight - $("#header").height() - $("#footer").height() - 14);
}

function getTitle(){

    var h = location.hash.substr(1);
    var h_ary = h.split('/');
    table_propaty.tblName = decodeURIComponent(h_ary[0]);
    table_propaty.csvfilename = decodeURIComponent(h_ary[1]);

    var postdata = {
        'D1NAME':table_propaty.tblName
    };

    $.ajax({
        type:'POST',
        url:'../getFDB2CSV1.php',
        data:postdata,
        timeout:table_propaty.timeout,
        dataType:'json',
        success:function(res){
            if(res.RTN === false){
                alert(res.MSG);
            }else{
                $("#title a").html(res.DATA[0].D1TEXT);
                table_propaty.d1text = res.DATA[0].D1TEXT;
                createColumns();
            }
        },
        error:function(res){
            $.unblockUI();
            if(res.error().statusText === 'timeout'){
                alert(errormsg.timeout);
            }else{
                alert(errormsg.system);
            }
        }
    });

}

function createColumns(){

    var postdata = {
        'D1NAME':table_propaty.tblName
    };

    $.ajax({
        type:'POST',
        url:'../relay.php',
        data:postdata,
        dataType:'json',
        timeout:table_propaty.timeout,
        success:function(res){

            table_propaty.columns = res;

            //theadでカラム作成
            var htmlthead = '';
            htmlthead += '<tr>';

            for(var i = 0;i<res.length;i++){

                var hed = res[i].D2HED;

                htmlthead += '<th>';
                htmlthead += hed;

                //ソートのクラスと▲▼を指定
                var sortCls = ' sort ';
                var sortDir = '▼';
                htmlthead += '<a class="'+sortCls+'">';
                htmlthead += sortDir;
                htmlthead += '</a>';

                htmlthead += '</th>';

            };

            htmlthead += '</tr>';

            $("#htable thead").append(htmlthead);

            createData(false);

            //th クリック時　ソート
            $("#htable thead th").click(function(){

                //table_propaty.sortindexに値がある場合、そのインデックスのth a のactiveを削除
                if(table_propaty.sortindex !== ''){
                    $("#htable thead th:eq("+table_propaty.sortindex+") a").removeClass('active');
                }

                var index = $("#htable thead th").index(this);
                var sortcol = table_propaty.columns[index].D2FLD + '_' + table_propaty.columns[index].D2FILID;

                //クリックした列が前回を同じ場合、前回のソート情報の逆にする
                if(sortcol === table_propaty.sortcol){
                    var befdir = table_propaty.sortdir;
                    if(befdir === 'asc'){
                        table_propaty.sortdir = 'desc';
                    }else if(befdir === 'desc'){
                        table_propaty.sortdir = 'asc';
                    }
                }else{
                    table_propaty.sortdir = 'asc';
                }

                table_propaty.sortcol = sortcol;
                table_propaty.sortindex = index;

                //スタイルと向きを変更
                $(this).children('a').addClass('active');
                sortHtmlchange($(this).children('a'));

                //データ取得
                createData(true);
                
            });

        },
        error:function(res){
            $.unblockUI();
            if(res.error().statusText === 'timeout'){
                alert(errormsg.timeout);
            }else{
                alert(errormsg.system);
            }
        }
    });

}

function sortHtmlchange(elem){
    if(elem.html() === '▲'){
        elem.html('▼');
    }else{
        elem.html('▲');
    }
}

function createData(loadflg){

    if(loadflg === true){
        $.blockUI(loadconf);
    }

    $("#htable tbody").empty();

    var postdata = {
        'D1NAME':table_propaty.tblName,
        'csvfilename':table_propaty.csvfilename,
        'iDisplayStart':table_propaty.dispstart,
        'iDisplayLength':table_propaty.dispend,
        'iSortCol_0':table_propaty.sortcol,
        'sSortDir_0':table_propaty.sortdir,
        'sSearch':''
    };

    //データを取得
	$.ajax({
		type:'POST',
		url:'../getJsonQueryData.php',
		data:postdata,
		dataType:'json',
        timeout:table_propaty.timeout,
		success:function(res){

            /*if(res.msg !== ''){
                alert(res.msg);
            }*/
            if(res.rtn === 2){
                //Ext.getBody().unmask();
                Common.showErrMsg(json.MSG);
                //me.redirectTo('dashboard');
            }else if(res.rtn !== 0){
                //Ext.getBody().unmask();
                Common.showErrMsg(json.MSG);
            }
            else{

                var data = res.aaData;

                //tbodyでデータ部を作成
                var htmltbody = '';

                for(var i = 0;i<data.length;i++){

                    var row = data[i];
                    if(i%2 !== 0){
                        htmltbody += '<tr class="rowcolor">';
                    }else{
                        htmltbody += '<tr>';
                    }

                    for(var j = 0;j<row.length;j++){

                        var val = row[j];
                        var type = table_propaty.columns[j].D2TYPE;
                        var d2wedt = table_propaty.columns[j].D2WEDT;
                        
                        if(type === 'S' || type === 'P' || type === 'B'){
                            htmltbody += '<td align="right">';
                        }else{
                            htmltbody += '<td>';
                        }

                        //編集コードチェック
                        switch(d2wedt){
                            case '1':
                                val = format1(val);
                                break;
                            case '2':
                                val = format2(val);
                                break;
                            case '3':
                                val = format3(val);
                                break;
                            case '4':
                                val = format4(val);
                                break;
                            case '5':
                                val = format5(val);
                                break;
                        }
                        htmltbody += val;
                        htmltbody += '</td>';
                    };

                    htmltbody += '</tr>';

                };

                $("#htable tbody").append(htmltbody);

                //thを全てマウスアウト状態にする
                //$("#htable thead th").mouseout();
                
                //右下のやつ
                var total = res.iTotalRecords;
                var end = table_propaty.dispend;
                var start = ((table_propaty.page-1) * table_propaty.length) + 1; 
                $("#total").html('全'+total+'件中&nbsp;'+start+'件から'+end+'件を表示');

                //maxpager
                table_propaty.lastpage = Math.round(total/table_propaty.length);
                $("#pager-max").html(table_propaty.lastpage);

                //controllbutton制御
                //初期化
                $(".page-first").css("opacity","0.5").css("cursor","default");
                $(".page-prev").css("opacity","0.5").css("cursor","default");
                $(".page-next").css("opacity","0.5").css("cursor","default");
                $(".page-last").css("opacity","0.5").css("cursor","default");

                //ページが最後まで行ってなかったらnextとlastをenabled
                if(table_propaty.page < table_propaty.lastpage){
                    $(".page-next").css("opacity","1").css("cursor","pointer");
                    $(".page-last").css("opacity","1").css("cursor","pointer");
                }

                //ページが最初まで行ってなかったらprevとfirstをenabled
                if(table_propaty.page > 1){
                    $(".page-prev").css("opacity","1").css("cursor","pointer");
                    $(".page-first").css("opacity","1").css("cursor","pointer");
                }
            }

            $.unblockUI();

		},
        error:function(res){
            $.unblockUI();
            if(res.error().statusText === 'timeout'){
                alert(errormsg.timeout);
            }else{
                alert(errormsg.system);
            }
        }
	});

}

function first(){

    if($(".page-first").css("cursor") === 'pointer'){
        table_propaty.page = 1;
        $("#pager-text").val(table_propaty.page);
        changeStartEnd();
        createData(true);
    }
}
function prev(){

    if($(".page-prev").css("cursor") === 'pointer'){
        table_propaty.page = table_propaty.page - 1;
        $("#pager-text").val(table_propaty.page);
        changeStartEnd();
        createData(true);
    }
}
function next(){

    if($(".page-next").css("cursor") === 'pointer'){
        table_propaty.page = table_propaty.page + 1;
        $("#pager-text").val(table_propaty.page);
        changeStartEnd();
        createData(true);
    }
}
function last(){
    if($(".page-last").css("cursor") === 'pointer'){
        table_propaty.page = table_propaty.lastpage;
        $("#pager-text").val(table_propaty.page);
        changeStartEnd();
        createData(true);
    }
}
function changeStartEnd(){
    table_propaty.dispend = (table_propaty.page * table_propaty.length);
    table_propaty.dispstart = (table_propaty.dispend - (table_propaty.length));
}

function getTime(){

	var date = new Date();
	var y = String(date.getFullYear());
	var m = String(date.getMonth()+1);
	var d = String(date.getDate());
	var h = String(date.getHours());
	var i = String(date.getMinutes());
	var s = String(date.getSeconds());
	
	time = y+m+d+h+i+s;

    return time;
}

function createCSV(){

    $.blockUI(loadconf);

    var time = getTime();

    var postdata = {
        'proc':'Csv',
        'iSortCol_0':table_propaty.sortcol,
        'sSortDir_0':table_propaty.sortdir,
        'sSearch':'',
        'time':time,
        'csvfilename':table_propaty.csvfilename,
        'D1NAME':table_propaty.tblName,
        'D1TEXT':table_propaty.d1text
    };

    $.ajax({
        type:'POST',
        url:'../createdownloadfile.php',
        data:postdata,
        timeout:table_propaty.timeout,
        dataType:'json',
        success:function(res){
            if(res.RTN === false){
                alert(res.MSG);
            }else{
                download(table_propaty.d1text + '.csv',table_propaty.tblName + time + '.csv');
            }

            $.unblockUI();
        },
        error:function(res){
            $.unblockUI();
            if(res.error().statusText === 'timeout'){
                alert(errormsg.timeout);
            }else{
                alert(errormsg.system);
            }
        }
    });

}

function createEXCEL(){

    $.blockUI(loadconf);

    var time = getTime();

    $.ajax({
        url:'../getConfigData.php',
        dataType:'json',
        timeout:table_propaty.timeout,
        success:function(res){
            var excelVersion = res.EXCELVERSION;

            var postdata = {
                'proc':'Excel',
                'iSortCol_0':table_propaty.sortcol,
                'sSortDir_0':table_propaty.sortdir,
                'sSearch':'',
                'time':time,
                'csvfilename':table_propaty.csvfilename,
                'D1NAME':table_propaty.tblName,
                'D1TEXT':table_propaty.d1text
            };

            $.ajax({
                type:'POST',
                url:'../createdownloadfile.php',
                data:postdata,
                dataType:'json',
                timeout:table_propaty.timeout,
                success:function(res){
                    if(res.RTN === false){
                        alert(res.MSG);
                    }else{

                        var ext = '';
                        if(excelVersion === '2007'){
                            ext = '.xlsx';
                        }else{
                            ext = '.xls';
                        }

                        download(table_propaty.d1text + ext,table_propaty.tblName + time + ext);
                    }

                    $.unblockUI();
                },
                error:function(res){
                    $.unblockUI();
                    if(res.error().statusText === 'timeout'){
                        alert(errormsg.timeout);
                    }else{
                        alert(errormsg.system);
                    }
                }
            });

        },
        error:function(res){
            $.unblockUI();
            if(res.error().statusText === 'timeout'){
                alert(errormsg.timeout);
            }else{
                alert(errormsg.system);
            }
        }
    });

}

function download(dname,fname){
    var me = this;
    var url = 'download.php?';
    url = url + '&DLNAME=' + encodeURIComponent(dname) + '&FNAME=' + encodeURIComponent(fname);

    var userAgent = window.navigator.userAgent.toLowerCase();

    if (userAgent.match('chrome') !== null) {
    	document.location.href = window.location.origin + window.location.pathname + '../' + url;
    }else{
    	location.href('../'+url);
    }
}

//format

</script>

</head>
<body>

<div id="contents">
    <div id="header">
        <div id="title"><a></a></div>
        <div id="content">
            <div id="csvexcel">
                <input type="button" value="CSV" onclick="createCSV()">
                <input type="button" value="EXCEL" onclick="createEXCEL()">
            </div>
        </div>
    </div>
    <div id="body">
        <div id="body-inner">
            <div class="table-div">
                <table id="htable" class="htmltable">
                    <thead></thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="controll-group">
            <div class="page-first" onclick="first();"></div>
            <div class="page-prev" onclick="prev();"></div>
            <div class="pager">
                ページ
                <input type="text" id="pager-text" value="1" onKeyup="this.value=this.value.replace(/[^0-9]+/,'')">
                /
                <a id="pager-max"></a>
            </div>
            <div class="page-next" onclick="next();"></div>
            <div class="page-last" onclick="last();"></div>
        </div>
        <div id="total"></div>
    </div>
</div>

</body>
</html>