<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : ライブラリー
* PROGRAM NAME   : ライブラリー削除
* PROGRAM ID     : delDB2LIBL.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2017/05/08
* MODIFY DATE    : 
* ============================================================
**/

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$QRYGID = cmHscDe($_POST['QRYGID']);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
		if($userData[0]['WUAUTH'] === '2'){
			$rs = cmChkKenGen($db2con,'34',$userData[0]['WUSAUT']);//'34' => クエリーグループ権限
			if($rs['result'] !== true){
			    $rtn = 2;
			    $msg =  showMsg($rs['result'],array('クエリーグループの権限'));
			}
		}
        /*if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('ライブラリー除外の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'31',$userData[0]['WUSAUT']);//'31' => LibraryJyogaiMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライブラリー除外の権限'));
            }
        }*/
    }
}
//クエリーグループID存在チェック
if($rtn === 0){
    $rs = fnCheckDB2QRYG($db2con,$QRYGID);
    if( $rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('ID'));
    }
}

//クエリーグループの削除
if($rtn === 0){
    $rs = fnDeleteDB2QRYG($db2con,$QRYGID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
//クエリーグループのシーケンス削除
if($rtn === 0){
    $rs = fnDeleteDB2QRYGS($db2con,$QRYGID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
//クエリーグループのためスケジュールの設定消す
if($rtn===0){
	$rs=fnDelAllScheduleSetting($db2con,$QRYGID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* 削除対象ライブラリーが存在かどうかをチェックすること
* 
* RESULT
*    01：データがある場合 true
*    02：データがない場合 false
* 
* @param Object  $db2con DBコネクション
* @param String  $LIBLNM ライブラリーID
*-------------------------------------------------------*
*/



/*
*-------------------------------------------------------* 
* 削除対象クエリーグループを削除すること
* 
* RESULT
*    01：削除終了の場合 true
*    02：エラーの場合 false
* 
* @param Object  $db2con DBコネクション
* @param String  $QRYGID クエリーグループID
*-------------------------------------------------------*
*/

function fnDeleteDB2QRYG($db2con,$QRYGID){

    $rs = true;
    $params=array();

    //構文
    $strSQL  = ' DELETE FROM DB2QRYG ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' QRYGID = ? ' ;
   
  
    $params[] = $QRYGID;

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
/*
*クエリーグループからのシーケンスＴＤ削除
*＠QRYGIDクエリーグループＩＤ
*/
function fnDeleteDB2QRYGS($db2con,$QRYGID){

    $rs = true;
    $params=array();
    //構文
    $strSQL  = ' DELETE FROM DB2QRYGS ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' QRYGSID = ? ' ;
  
    $params[] = $QRYGID;

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}

function fnCheckDB2QRYG($db2con,$QRYGID){
    $data = array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' SELECT COUNT(A.QRYGID) AS COUNT ';
    $strSQL .= ' FROM DB2QRYG AS A ' ;
    $strSQL .= ' WHERE A.QRYGID = ? ' ;

    $params[] = $QRYGID;

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $totcount = $row['COUNT'];
            }
            if($totcount === 0){
                $rs = 'NOTEXIST_DEL';
            }
        }
    }
    return $rs;
}
/**
*-------------------------------------------------------* 
* DB2WSCDのスケジュールのデータ
* 
*-------------------------------------------------------*
*/
function fnDelAllScheduleSetting($db2con,$QRYGID){
    $rs = true;
	//スケジュールの設定を消す
	if($rs){
		$result=fnDelDB2WSCD($db2con,$QRYGID);
		if($result !==true){
			$rs=$result;
		}
	}
	//スケジュールのメール設定を消す
	if($rs){
		$result=fnDelDB2WAUT($db2con,$QRYGID);
		if($result !==true){
			$rs=$result;
		}
	}
	//スケジュールのその他を消す
	if($rs){
		$result=fnDelDB2WSOC($db2con,$QRYGID);
		if($result !==true){
			$rs=$result;
		}
	}
	//スケジュールの履歴を消す
	if($rs){
		$result=fnDelDB2WHIS($db2con,$QRYGID);
		if($result !==true){
			$rs=$result;
		}

	}
    return $rs;
}
function fnDelDB2WSCD($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' DELETE  FROM DB2WSCD AS A ' ;
    $strSQL .= ' WHERE WSNAME=? ' ;
    $strSQL .= ' AND WSQGFLG=\'1\' ' ;

    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
function fnDelDB2WSOC($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' DELETE  FROM DB2WSOC AS A ' ;
    $strSQL .= ' WHERE SONAME=? ' ;
    $strSQL .= ' AND SOQGFLG=\'1\' ' ;

    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
function fnDelDB2WHIS($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' DELETE  FROM DB2WHIS AS A ' ;
    $strSQL .= ' WHERE WHNAME=? ' ;
    $strSQL .= ' AND WHQGFLG=\'1\' ' ;

    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
function fnDelDB2WAUT($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' DELETE  FROM DB2WAUT AS A ' ;
    $strSQL .= ' WHERE WANAME=? ' ;
    $strSQL .= ' AND WAQGFLG=\'1\' ' ;

    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
		    $strSQL  = ' DELETE  FROM DB2WAUL AS A ' ;
		    $strSQL .= ' WHERE WLNAME=? ' ;
		    $params=array();
		    $params[] = $QRYGID;
		    $stmt = db2_prepare($db2con,$strSQL);
		    if($stmt === false){
		        $rs = 'FAIL_SEL';
		    }else{
		        $r=db2_execute($stmt,$params);
		        if($r === false){
		            $rs = 'FAIL_SEL';
		        }
			}

		}
    }
    return $rs;
}

