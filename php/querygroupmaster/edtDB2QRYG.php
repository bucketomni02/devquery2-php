<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['PROC']))?cmMer($_POST['PROC']):'';
$QRYGID = (isset($_POST['QRYGID']))?cmMer($_POST['QRYGID']):'';
$QRYGNAME = (isset($_POST['QRYGNAME']))?cmMer($_POST['QRYGNAME']):'';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
		if($userData[0]['WUAUTH'] === '2'){
			$rs = cmChkKenGen($db2con,'34',$userData[0]['WUSAUT']);//'34' => クエリーグループ権限
			if($rs['result'] !== true){
			    $rtn = 2;
			    $msg =  showMsg($rs['result'],array('クエリーグループの権限'));
			}
		}
        /*if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('クエリーグループの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'31',$userData[0]['WUSAUT']);//'31' => LibraryJyogaiMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライブラリー除外の権限'));
            }
        }*/
    }
}

//クエリーグループID必須チェック
if($rtn === 0){
    if($QRYGID  === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('ID'));
        $focus = 'QRYGID';
    }
}
if($rtn === 0){
    if (preg_match('/[^0-9A-Za-z_#@]+/', $QRYGID)) {
        $msg = showMsg('FAIL_KEYENTRY',array('ID'));
        $rtn = 1;
        $focus = 'QRYGID';
    }
}
//クエリーグループIDの桁数チェック
if($rtn === 0){
    if(!checkMaxLen($QRYGID,10)){
        $rtn = 1;        
       $msg = showMsg('FAIL_MAXLEN',array('ID'));
        $focus = 'QRYGID';
    }
}

//クエリーグループ名必須チェック
if($rtn === 0){
    if($QRYGNAME  === ''){
        $rtn = 1;
         $msg = showMsg('FAIL_REQ',array('名'));
        $focus = 'QRYGNAME';
    }
}
//クエリーグループ名の桁数チェック
if($rtn === 0){
    if(!checkMaxLen($QRYGNAME,50)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('名'));
        $focus = 'QRYGNAME';
        
    }
}
//クエリーグループIDがすでにFDB2CSV1にあるかどうかのチェック
if($rtn===0){
	$rs=fnExistFDB2CSV1($db2con,$QRYGID);
    if($rs ===  'ISEXIST'){
        $rtn = 1;
        $msg = showMsg($rs,array('FDB2CSV1でID'));
        $focus = 'QRYGID';
    }else if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('ID'));
        $focus = 'QRYGID';
    }
}


/*
*-------------------------------------------------------* 
* クエリーグループ更新処理
*-------------------------------------------------------*
*/

if($proc === 'ADD'){
   if($rtn === 0){        
        $rs = fnCheckDB2QRYG($db2con,$QRYGID);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('ID'));
            $focus = 'QRYGID';
        }
    }
    if($rtn === 0){
        //新クエリーグループを登録処理
        $rs = fnInsertDB2QRYG($db2con,$QRYGID,$QRYGNAME);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}else if($proc === 'UPD'){
    if($rtn === 0){
        //クエリーグループ存在チェック処理
        $rs = fnCheckDB2QRYG($db2con,$QRYGID);
        if($rs === true){
            $rtn = 1;
            $rs = 'NOTEXIST_GET';
            $msg = showMsg($rs,array('ID'));
            $focus = 'QRYGID';
        }else if($rs !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs,array('ID'));
            $focus = 'QRYGID';
        }
    }
    if($rtn === 0){
        //クエリーグループを更新処理
        $rs = fnUpdateDB2QRYG($db2con,$QRYGID,$QRYGNAME);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}

cmDb2Close($db2con);
/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'proc' => $proc,
    'FOCUS'=>$focus
);

echo(json_encode($rtnAry));

/**
*-------------------------------------------------------* 
* クエリーグループが更新すること
* 
* RESULT
*    01：更新終了場合     true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $QRYGID クエリーグループID
* @param String  $QRYGNAME クエリーグループ名
* 
*-------------------------------------------------------*
*/
function fnUpdateDB2QRYG($db2con,$QRYGID,$QRYGNAME){

    $rs = true;
    $params=array();
    //構文
    $strSQL  = ' UPDATE DB2QRYG ';
    $strSQL .= ' SET ';
    $strSQL .= ' QRYGNAME = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' QRYGID = ? ';
    $params[] = $QRYGNAME;
    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}
/**
*-------------------------------------------------------* 
* 新クエリーグループを登録すること
* 
* RESULT
*    01：登録終了の場合     true
*    02：エラー場合         false
* 
* @param Object  $db2con DBコネクション
* @param String  $QRYGID クエリーグループID
* @param String  $QRYGNAME クエリーグループ名

* 
*-------------------------------------------------------*
*/
function fnInsertDB2QRYG($db2con,$QRYGID,$QRYGNAME){

    $rs = true;
    $params=array();
	$QRYWUUID=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
    //構文
    $strSQL  = ' INSERT INTO DB2QRYG ';
    $strSQL .= ' ( ';
    $strSQL .= ' QRYGID, ';
    $strSQL .= ' QRYGNAME, ';
    $strSQL .= ' QRYGWUUID ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?,?) ';
    $params[]=$QRYGID;
    $params[]=$QRYGNAME;
	$params[]=$QRYWUUID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* クエリーグループが存在するかどうかをチェックすること
* 
* RESULT
*    01：存在する場合   true
*    02：存在ない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $QRYGID クエリーグループID
* 
*-------------------------------------------------------*
*/
function fnCheckDB2QRYG($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' SELECT COUNT(A.QRYGID) AS COUNT ' ;
    $strSQL .= ' FROM DB2QRYG AS A ' ;
    $strSQL .= ' WHERE A.QRYGID = ? ' ;
    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
          while($row = db2_fetch_assoc($stmt)){
                $totcount = $row['COUNT'];
            }
            if($totcount > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* クエリーグループIDがFDB2CSV1に同じならチェック
* 
* RESULT
*    01：存在する場合   true
*    02：存在ない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $QRYGID クエリーグループID
* 
*-------------------------------------------------------*
*/
function fnExistFDB2CSV1($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' SELECT COUNT(A.D1NAME) AS COUNT ' ;
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;
    $strSQL .= ' WHERE A.D1NAME = ? ' ;
    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
          while($row = db2_fetch_assoc($stmt)){
                $totcount = $row['COUNT'];
            }
            if($totcount > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}