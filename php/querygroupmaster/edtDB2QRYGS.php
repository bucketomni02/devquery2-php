<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../common/inc/common.validation.php");

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['PROC']))?cmMer($_POST['PROC']):'';
$QRYGID = (isset($_POST['QRYGID']))?cmMer($_POST['QRYGID']):'';
$DB2QRYGS=(isset($_POST['DATALIST'])?json_decode($_POST['DATALIST'],true):array());

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$oldDB2QRYGS=array();
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
		if($userData[0]['WUAUTH'] === '2'){
			$rs = cmChkKenGen($db2con,'34',$userData[0]['WUSAUT']);//'34' => クエリーグループ権限
			if($rs['result'] !== true){
			    $rtn = 2;
			    $msg =  showMsg($rs['result'],array('クエリーグループの権限'));
			}
		}
    }
}
//クエリーグループのSEQテーブルのチェック
if($rtn === 0){
    $rs = cmChkQueryGroup($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$QRYGID,"","","","");
    if($rs['result'] !== true){
        $rtn = 2;
		$msg = showMsg($rs['result'],array('クエリーグループ'));
    }
}
$seqArr=array();
$updData=array();
$updData=$DB2QRYGS;
$row = '';

//クエリーグループのため一番最後のクエリーがピボットが会った場合、スケジュールの設定を消す
if($rtn===0){
	if(count($updData)>0){
		$gData=array();
		$groupD1NAME='';
		$groupPMPKEY='';
		$LASTUPDD1NAME='';
		$maxSEQ=0;
		$maxKeyIndex=0;
		//更新のデータの一番大きいのクエリーIDをもらう
		foreach($updData as $key=>$value){
			if($rtn===0){
				if($key===0){
					$maxSEQ=(int)$value['QRYGSSEQ'];
				}else{
					if($maxSEQ<$value['QRYGSSEQ']){
						$maxSEQ=$value['QRYGSSEQ'];
						$maxKeyIndex=$key;
					}
				}
			}
		}
		if($rtn===0){
			$LASTUPDD1NAME=$updData[$maxKeyIndex]['QRYGSQRY'];
			//e_log('更新のデータの一番大きいのクエリーIDをもらう:'.$LASTUPDD1NAME);
			//DBのデータの一番大きいのクエリーIDをもらう
			$r=fnGetLastDB2QRYG($db2con,$QRYGID);
			if($r['result']!==true){
				$rs=$r['result'];
	            $rtn = 1;
	            $msg = showMsg($rs);
			}else{
				$gData=$r['data'];
				if(count($gData)>0){
					foreach($gData as $key=>$val){
						$groupD1NAME=$gData[0]['PMNAME'];
						$groupPMPKEY=$gData[0]['PMPKEY'];
						e_log('DBのデータの一番大きいのクエリーIDをもらう:'.cmMer($groupD1NAME).'=>'.cmMer($LASTUPDD1NAME));
						if(cmMer($groupD1NAME)!==cmMer($LASTUPDD1NAME) && cmMer($LASTUPDD1NAME)!==''){
							//スケジュール設定の全部のSELECT
							$result=fnSelAllScheduleSetting($db2con,$QRYGID,$groupPMPKEY);
							if($result['result'] !==true){
								$rtn=1;
								$rs=showMsg($result);
							}else{
								if($result['ISEXIST']===1){
									$result=fnDelAllScheduleSetting($db2con,$QRYGID,$groupPMPKEY);
									if($result !==true){
										$rtn=1;
										$rs=showMsg($result);
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
//クエリーグループのSEQテーブル消す
if($rtn===0){
    if(count($updData)>0){
        $rs=fnDelDB2QRYGS($db2con,$QRYGID);
        if($rs !== true){
            $rtn=1;
            $msg=showMsg($rs);
        }
    }
}

//クエリーグループのSEQテーブル挿入
if($rtn === 0){
    if(count($updData)>0){
		//DB2QRYGS登録
        $rs = fnInsertDB2QRYGS($db2con,$updData,$QRYGID);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}

if($rtn === 0){
    //お気に入り削除
    $rs = cmDeleteDB2QBMK($db2con,$QRYGID,'','','','');
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }    
}
cmDb2Close($db2con);
/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'proc' => $proc,
    'FOCUS'=>$focus,
    'ROW'=>$row
);

echo(json_encode($rtnAry));



/**
*-------------------------------------------------------* 
* DB2WSCDのスケジュールのデータ
* 
*-------------------------------------------------------*
*/
function fnDelAllScheduleSetting($db2con,$QRYGID,$PMPKEY){
    $rs = true;
	//スケジュールの設定のSELECTと消す
	if($rs){
		$result=fnDelDB2WSCD($db2con,$QRYGID,$PMPKEY);
		if($result !==true){
			$rs=$result;
		}
	}
	
	//スケジュールの設定を消す
	if($rs){
		$result=fnDelDB2WSCD($db2con,$QRYGID,$PMPKEY);
		if($result !==true){
			$rs=$result;
		}
	}
	//スケジュールのメール設定を消す
	if($rs){
		$result=fnDelDB2WAUT($db2con,$QRYGID,$PMPKEY);
		if($result !==true){
			$rs=$result;
		}
	}
	//スケジュールのその他を消す
	if($rs){
		$result=fnDelDB2WSOC($db2con,$QRYGID,$PMPKEY);
		if($result !==true){
			$rs=$result;
		}
	}
	//スケジュールの履歴を消す
	if($rs){
		$result=fnDelDB2WHIS($db2con,$QRYGID,$PMPKEY);
		if($result !==true){
			$rs=$result;
		}

	}
    return $rs;
}

/**
*一番最後のクエリーグループがピボットあるかどうか
*/
function fnGetLastDB2QRYG($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' SELECT EMP2.QRYGSID,PMNAME,PMPKEY,PMTEXT   FROM DB2QRYGS AS EMP2   ' ;
    $strSQL .= ' JOIN ' ;
    $strSQL .= ' (  ' ;
    $strSQL .= ' 	SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ' ;
    $strSQL .= ' 	FROM DB2QRYGS AS D  ' ;
    $strSQL .= ' 	GROUP BY QRYGSID  ' ;
    $strSQL .= ' ) EMP1  ' ;
    $strSQL .= ' ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ' ;
    $strSQL .= ' JOIN DB2PMST ON QRYGSQRY=PMNAME ' ;
    $strSQL .= ' WHERE EMP2.QRYGSID = ? ' ;
    $params[] = $QRYGID;
	e_log('getDB2QRYGSComFirmMsg.php strSQL'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
          while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return array('result'=>$rs,'data'=>umEx($data));
}


/**
*-------------------------------------------------------* 
* 新クエリーグループを登録すること
* 
* RESULT
*    01：登録終了の場合     true
*    02：エラー場合         false
* 
* @param Object  $db2con DBコネクション
* @param String  $QRYGID クエリーグループID
* @param String  $QRYGNAME クエリーグループ名
* 
*-------------------------------------------------------*
*/
function fnInsertDB2QRYGS($db2con,$DB2QRYGS,$QRYGID){

    $rs = true;
    //構文
    $strSQL  = ' INSERT INTO DB2QRYGS ( ';
    $strSQL .= '    QRYGSID, ';
    $strSQL .= '    QRYGSSEQ, ';
    $strSQL .= '    QRYGSQRY ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES  ( ?, ?, ?) ';
    foreach($DB2QRYGS as $dataField){
        if($dataField['QRYGSSEQ'] !==''){
            $params=array();
            $params[]=cmMer($QRYGID);
            $params[]=$dataField['QRYGSSEQ'];
            $params[]=$dataField['QRYGSQRY'];
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $rs = 'FAIL_INS';
                break;
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $rs = 'FAIL_INS';
                    break;
                }
            }
        }
    }
    return $rs;
}


/**
*-------------------------------------------------------* 
* DB2QRYGS消す
* 
* RESULT
*    01：存在する場合   true
*    02：存在ない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $QRYGID クエリーグループID
* 
*-------------------------------------------------------*
*/
function fnDelDB2QRYGS($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' DELETE  FROM DB2QRYGS AS A ' ;
    $strSQL .= ' WHERE A.QRYGSID = ? ' ;
    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }
    }
    return $rs;
}
/***
*update
**/
function  fnUpdDB2QRYGS($db2con,$QRYGSID,$QRYGSQRY){
    $rs = true;
    //構文
    $strSQL  = ' UPDATE DB2QRYGS ';
    $strSQL  .= ' SET QRYGSSEQ = ?  ';
    $strSQL .= ' WHERE QRYGSID = ? AND QRYGSQRY = ?';  
    $params[] =$QRYGSID;
    $params[] =$QRYGSQRY;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
             e_log(db2_stmt_errormsg());
            $rs = 'FAIL_UPD';
    }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_UPD';
            }
    }
    return $rs;
}
/***
*select
**/
function  fnGetDB2QRYGS($db2con,$QRYGSID){
    $rs = true;
    $data=array();
    //構文
    $strSQL  = ' SELECT * FROM DB2QRYGS  ';
    $strSQL .= ' WHERE QRYGSID = ? ';
    $strSQL .= ' AND QRYGSSEQ<> 0 ';  
    $params[] =$QRYGSID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return array('result'=>$rs,'data'=>$data);
}
/***
*スケジュールの設定のSELECT
***/
function fnSelAllScheduleSetting($db2con,$QRYGID,$PMPKEY){
    $data = array();
    $params=array();
	$ISEXIST=0;
    $rs = true;
    $totcount=0;
    $strSQL  = ' SELECT COUNT(*) AS COUNT  FROM DB2WSCD AS A ' ;
    $strSQL .= ' WHERE WSNAME=? AND WSPKEY=? ' ;
    $strSQL .= ' AND WSQGFLG=\'1\' ' ;
    $params[] = $QRYGID;
	$params[] = $PMPKEY;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
			while($row = db2_fetch_assoc($stmt)){
			    $totcount = $row['COUNT'];
			}
			if($totcount>0){
				$ISEXIST=1;
			}
			if($ISEXIST !== 1){
			    $strSQL  = ' SELECT COUNT(*) AS COUNT FROM DB2WAUT AS A ' ;
			    $strSQL .= ' WHERE WANAME=? AND WAPKEY=? ' ;
			    $strSQL .= ' AND WAQGFLG=\'1\' ' ;
				$params=array();
			    $params[] = $QRYGID;
				$params[] = $PMPKEY;
			    $stmt = db2_prepare($db2con,$strSQL);
			    if($stmt === false){
			        $rs = 'FAIL_SEL';
			    }else{
			        $r=db2_execute($stmt,$params);
			        if($r === false){
			            $rs = 'FAIL_SEL';
			        }else{
						while($row = db2_fetch_assoc($stmt)){
						    $totcount = $row['COUNT'];
						}
						if($totcount>0){
							$ISEXIST=1;
						}
						if($ISEXIST !== 1){
						    $strSQL  = ' SELECT COUNT(*) AS COUNT  FROM DB2WSOC AS A ' ;
						    $strSQL .= ' WHERE SONAME=? AND SOPKEY=? ' ;
						    $strSQL .= ' AND SOQGFLG=\'1\' ' ;
							$params=array();
						    $params[] = $QRYGID;
							$params[] = $PMPKEY;
						    $stmt = db2_prepare($db2con,$strSQL);
						    if($stmt === false){
						        $rs = 'FAIL_SEL';
						    }else{
						        $r=db2_execute($stmt,$params);
						        if($r === false){
						            $rs = 'FAIL_SEL';
						        }else{
									while($row = db2_fetch_assoc($stmt)){
									    $totcount = $row['COUNT'];
									}
									if($totcount>0){
										$ISEXIST=1;
									}
									if($ISEXIST !== 1){
									    $strSQL  = ' SELECT COUNT(*) AS COUNT  FROM DB2WHIS AS A ' ;
									    $strSQL .= ' WHERE WHNAME=? AND WHPKEY=? ' ;
									    $strSQL .= ' AND WHQGFLG=\'1\' ' ;
										$params=array();
									    $params[] = $QRYGID;
										$params[] = $PMPKEY;
									    $stmt = db2_prepare($db2con,$strSQL);
									    if($stmt === false){
									        $rs = 'FAIL_SEL';
									    }else{
									        $r=db2_execute($stmt,$params);
									        if($r === false){
									            $rs = 'FAIL_SEL';
									        }else{
												while($row = db2_fetch_assoc($stmt)){
												    $totcount = $row['COUNT'];
												}
												if($totcount>0){
													$ISEXIST=1;
												}

											}
									    }
										
									}

								}
						    }
							
						}

					}
			    }
				
			}			

		}
    }
    return array('result'=>$rs,'ISEXIST'=>$ISEXIST);
}

function fnDelDB2WSCD($db2con,$QRYGID,$PMPKEY){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' DELETE  FROM DB2WSCD AS A ' ;
    $strSQL .= ' WHERE WSNAME=? AND WSPKEY=? ' ;
    $strSQL .= ' AND WSQGFLG=\'1\' ' ;

    $params[] = $QRYGID;
	$params[] = $PMPKEY;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
function fnDelDB2WSOC($db2con,$QRYGID,$PMPKEY){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' DELETE  FROM DB2WSOC AS A ' ;
    $strSQL .= ' WHERE SONAME=? AND SOPKEY=? ' ;
    $strSQL .= ' AND SOQGFLG=\'1\' ' ;

    $params[] = $QRYGID;
	$params[] = $PMPKEY;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
function fnDelDB2WHIS($db2con,$QRYGID,$PMPKEY){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' DELETE  FROM DB2WHIS AS A ' ;
    $strSQL .= ' WHERE WHNAME=? AND WHPKEY=? ' ;
    $strSQL .= ' AND WHQGFLG=\'1\' ' ;

    $params[] = $QRYGID;
	$params[] = $PMPKEY;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
function fnDelDB2WAUT($db2con,$QRYGID,$PMPKEY){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' DELETE  FROM DB2WAUT AS A ' ;
    $strSQL .= ' WHERE WANAME=? AND WAPKEY=? ' ;
    $strSQL .= ' AND WAQGFLG=\'1\' ' ;

    $params[] = $QRYGID;
	$params[] = $PMPKEY;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
		    $strSQL  = ' DELETE  FROM DB2WAUL AS A ' ;
		    $strSQL .= ' WHERE WLNAME=? AND WLPKEY=? ' ;
		    $params=array();
		    $params[] = $QRYGID;
			$params[] = $PMPKEY;
		    $stmt = db2_prepare($db2con,$strSQL);
		    if($stmt === false){
		        $rs = 'FAIL_SEL';
		    }else{
		        $r=db2_execute($stmt,$params);
		        if($r === false){
		            $rs = 'FAIL_SEL';
		        }
			}

		}
    }
    return $rs;
}

