<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み//
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../licenseInfo.php");
//$licenseType = 'FREE';
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
$proc = (isset($_POST['proc']) ? $_POST['proc'] : '');
$QRYGID = (isset($_POST['QRYGID'])) ? $_POST['QRYGID'] : '';
$QRYGNAME = (isset($_POST['QRYGNAME'])) ? $_POST['QRYGNAME']:'';
$start = (isset($_POST['start'])) ? $_POST['start'] : '';
$length = (isset($_POST['length'])) ? $_POST['length'] : '';
$sort = (isset($_POST['sort'])) ? $_POST['sort'] : '';
$sortDir = (isset($_POST['sortDir'])) ? $_POST['sortDir'] : '';

//ページング情報や検索情報などの保持用
$session = array();
$session['QRYGID']  = $QRYGID;
$session['QRYGNAME']  = $QRYGNAME;
$session['start']  = $start;
$session['length']   = $length;
$session['sort']  = $sort;
$session['sortDir']    = $sortDir;

/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$userData=array();
$isBtnFlg="1";//button show
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
//htmlspecialcharsをデコード
$QRYGID = cmHscDe($QRYGID);
$QRYGNAME = cmHscDe($QRYGNAME);
$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID'])) ? $_SESSION['PHPQUERY']['user'][0]['WUUID'] : '';
$csv_d = array();
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $WUUID);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    } else {
        $userData = umEx($rs['data']);
		$isBtnFlg=($userData[0]['WUAUTH'] === '3')?"0":"1";
		if($userData[0]['WUAUTH'] === '2'){
			$rs = cmChkKenGen($db2con,'34',$userData[0]['WUSAUT']);//'34' => クエリーグループ権限
			if($rs['result'] !== true){
			    $rtn = 2;
			    $msg =  showMsg($rs['result'],array('クエリーグループの権限'));
			}
		}
        /*if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('クエリーグループの権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'31',$userData[0]['WUSAUT']);//'31' => LibraryJyogaiMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ライブラリー除外の権限'));
            }
        }*/
    }
}
if ($proc === 'EDIT') {
    if ($rtn === 0) {
        $rs = fnSelDB2QRYG($db2con, $QRYGID);
        if ($rs['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($rs['result'], array('ライブラリーID'));
        } else {
            $data = $rs['data'];
        }
    }
} else {
    if ($rtn === 0) {
        $rs = fnGetAllCount($db2con, $QRYGID, $QRYGNAME,$userData);
        if ($rs['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($rs['result']);
        } else {
            $allcount = $rs['TOTALCOUNT'];
        }
    }
    if ($rtn === 0) {
        $rs = fnGetDB2QRYG($db2con, $QRYGID, $QRYGNAME,$userData,$start, $length, $sort, $sortDir);
        if ($rs['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($rs['result']);
        } else {
            $data = $rs['data'];
        }
    }
}
cmDb2Close($db2con);
$userData[0]['WUPSWE'] = '';
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
	'isBtnFlg'=>$isBtnFlg,
    'iTotalRecords' => $allcount,
    'aaData' =>$data,
    'session' => $session,
	'userData'=>$userData
);
echo (json_encode($rtn));
/*
 *-------------------------------------------------------*
 * クエリーグループのグリッドデータ
 *-------------------------------------------------------*
*/
function fnGetDB2QRYG($db2con, $QRYGID, $QRYGNAME,$userData,$start = '', $length = '', $sort = '', $sortDir = '') {
    $data = array();
    $params = array();
	$WUAUTH=$userData[0]["WUAUTH"];
	$WUUID=$userData[0]["WUUID"];
    $strSQL = ' SELECT A.* ';
    $strSQL.= ' FROM ( SELECT B.QRYGID,B.QRYGNAME , ROWNUMBER() OVER( ';
    if ($sort !== '') {
        $strSQL.= ' ORDER BY B.' . $sort . ' ' . $sortDir . ' ';
    } else {
        $strSQL.= ' ORDER BY B.QRYGID ASC ';
    }
    $strSQL.= ' ) as rownum ';
	$strSQL.= ' FROM ';
	if($WUAUTH==="3"){
		$strSQL .= ' ( ';
		$strSQL .= '	SELECT DISTINCT QRYGID,QRYGNAME ';
		$strSQL .= '	FROM ';
	    $strSQL .= '    ( ';
	    $strSQL .= '    SELECT  QRYGID,QRYGNAME,QRYGWUUID ';
	    $strSQL .= '    FROM ';
	    $strSQL .= '        DB2QRYG ';
		$strSQL .= ' 		WHERE QRYGWUUID= ? ';
		$strSQL .= '	UNION ALL ';
		$strSQL .= '	SELECT QRYGID,QRYGNAME,QRYGWUUID ';
		$strSQL .= '		FROM DB2QRYG ';
	    $strSQL .= '    	WHERE QRYGID IN ( ';
	    $strSQL .= '        	SELECT DISTINCT ';
	    $strSQL .= '            	WGNAME ';
	    $strSQL .= '        	FROM ';
	    $strSQL .= '            DB2WGDF ';
	    $strSQL .= '        	WHERE ';
	    $strSQL .= '            	WGGID IN (SELECT ';
	    $strSQL .= '                            WUGID ';
	    $strSQL .= '                        FROM ';
	    $strSQL .= '                            DB2WUGR ';
	    $strSQL .= '                        WHERE ';
	    $strSQL .= '                            WUUID = ?  ';
	    $strSQL .= '                        ) ';
	    $strSQL .= '            AND WGQGFLG=\'1\' ';
	    $strSQL .= '    	)';  
	    $strSQL .= '    )  BB ';
		/*$strSQL .= '	SELECT QRYGID,QRYGNAME,QRYGWUUID ';
		$strSQL .= '		FROM DB2QRYG ';
	    $strSQL .= '    	WHERE QRYGID IN ( ';
	    $strSQL .= '        	SELECT DISTINCT ';
	    $strSQL .= '            	WGNAME ';
	    $strSQL .= '        	FROM ';
	    $strSQL .= '            DB2WGDF ';
	    $strSQL .= '        	WHERE ';
	    $strSQL .= '            	WGGID IN (SELECT ';
	    $strSQL .= '                            WUGID ';
	    $strSQL .= '                        FROM ';
	    $strSQL .= '                            DB2WUGR ';
	    $strSQL .= '                        WHERE ';
	    $strSQL .= '                            WUUID = ?  ';
	    $strSQL .= '                        ) ';
	    $strSQL .= '            AND WGQGFLG=\'1\' ';
	    $strSQL .= '            ) ';*/
		$strSQL .= ' ) B ';
		array_push($params,$WUUID);
		array_push($params,$WUUID);
	}else if($WUAUTH==="4"){
		$strSQL .=" (SELECT QRYGID,QRYGNAME FROM DB2QRYG";
		$strSQL .=" WHERE QRYGWUUID=? ) B";
		array_push($params,$WUUID);
	}else{
		$strSQL.= ' (SELECT QRYGID,QRYGNAME FROM  DB2QRYG )B ';
	}
    $strSQL.= ' WHERE QRYGID <> \'\' ';
    if ($QRYGID != '') {
        $strSQL.= ' AND QRYGID like ? ';
        array_push($params, '%' . $QRYGID . '%');
    }
    if ($QRYGNAME != '') {
        $strSQL.= ' AND QRYGNAME like ? ';
        array_push($params, '%' . $QRYGNAME . '%');
    }
    $strSQL.= ' ) as A ';
    //抽出範囲指定
    if (($start != '') && ($length != '')) {
        $strSQL.= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params, $start + 1);
        array_push($params, $start + $length);
    }
e_log("fnGetDB2QRYG strSQL=>".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true, 'data' => umEx($data,true));
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * 全件カウント取得
 *-------------------------------------------------------*
*/
function fnGetAllCount($db2con, $QRYGID, $QRYGNAME,$userData) {
    $data = array();
    $totcount=0;
    $params = array();
    $rs=true;
	$WUAUTH=$userData[0]["WUAUTH"];
	$WUUID=$userData[0]["WUUID"];

    $strSQL = ' SELECT count(A.QRYGID) AS COUNT ';
    $strSQL .= ' FROM ';
	if($WUAUTH==="3"){
		$strSQL .= ' ( ';
	    $strSQL .= '    SELECT DISTINCT QRYGID FROM ';
	    $strSQL .= '    ( ';
	    $strSQL .= '    SELECT  QRYGID ';
	    $strSQL .= '    FROM ';
	    $strSQL .= '        DB2QRYG ';
		$strSQL .= ' 		WHERE QRYGWUUID= ? ';
		$strSQL .= '	UNION ALL ';
		$strSQL .= '	SELECT QRYGID ';
		$strSQL .= '		FROM DB2QRYG ';
	    $strSQL .= '    	WHERE QRYGID IN ( ';
	    $strSQL .= '        	SELECT DISTINCT ';
	    $strSQL .= '            	WGNAME ';
	    $strSQL .= '        	FROM ';
	    $strSQL .= '            DB2WGDF ';
	    $strSQL .= '        	WHERE ';
	    $strSQL .= '            	WGGID IN (SELECT ';
	    $strSQL .= '                            WUGID ';
	    $strSQL .= '                        FROM ';
	    $strSQL .= '                            DB2WUGR ';
	    $strSQL .= '                        WHERE ';
	    $strSQL .= '                            WUUID = ?  ';
	    $strSQL .= '                        ) ';
	    $strSQL .= '            AND WGQGFLG=\'1\' ';
	    $strSQL .= '    	)';  
	    $strSQL .= '    )  AA ';
		$strSQL .= ' ) A ';

		/*$strSQL .= ' ( ';
		$strSQL .= '	SELECT QRYGID ';
		$strSQL .= '		FROM DB2QRYG ';
	    $strSQL .= '    	WHERE QRYGID IN ( ';
	    $strSQL .= '        	SELECT DISTINCT ';
	    $strSQL .= '            	WGNAME ';
	    $strSQL .= '        	FROM ';
	    $strSQL .= '            DB2WGDF ';
	    $strSQL .= '        	WHERE ';
	    $strSQL .= '            	WGGID IN (SELECT ';
	    $strSQL .= '                            WUGID ';
	    $strSQL .= '                        FROM ';
	    $strSQL .= '                            DB2WUGR ';
	    $strSQL .= '                        WHERE ';
	    $strSQL .= '                            WUUID = ?  ';
	    $strSQL .= '                        ) ';
	    $strSQL .= '            AND WGQGFLG=\'1\' ';
	    $strSQL .= '            ) ';
		$strSQL .= ' ) A ';*/
		array_push($params,$WUUID);
		array_push($params,$WUUID);
	}else if($WUAUTH==="4"){
		$strSQL .=" (SELECT QRYGID FROM DB2QRYG";
		$strSQL .=" WHERE QRYGWUUID=? ) A";
		array_push($params,$WUUID);
	}else{
		$strSQL.= ' (SELECT QRYGID FROM  DB2QRYG ) A ';
	}
    $strSQL.= ' WHERE QRYGID <> \'\' ';
    if ($QRYGID != '') {
        $strSQL.= ' AND QRYGID like ? ';
        array_push($params, '%' . $QRYGID . '%');
    }
    if ($QRYGNAME != '') {
        $strSQL.= ' AND QRYGNAME like ? ';
        array_push($params, '%' . $QRYGNAME . '%');
    }
e_log("getDB2QRYG.php strSQL=>".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs='FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs='FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $totcount = $row['COUNT'];
            }
            $data = array('result' => $rs, 'TOTALCOUNT' =>$totcount);
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 *クエリーグループのデータ
 *-------------------------------------------------------*
*/
function fnSelDB2QRYG($db2con, $QRYGID) {
    $data = array();
    $params = array();
    $rs = true;
    $strSQL = ' SELECT A.*  ';
    $strSQL.= ' FROM DB2QRYG as A ';
    if ($QRYGID !== '') {
        $strSQL.= ' WHERE QRYGID = ? ';
        $params[] = $QRYGID;
    }
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs='FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs='FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    $data = array('result' =>$rs,'data'=>umEx($data));
    return $data;
}
