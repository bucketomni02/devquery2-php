<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../common/inc/common.validation.php");

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['PROC']))?cmMer($_POST['PROC']):'';
$QRYGID = (isset($_POST['QRYGID']))?cmMer($_POST['QRYGID']):'';
$DB2QRYGS=(isset($_POST['DATALIST'])?json_decode($_POST['DATALIST'],true):array());

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$oldDB2QRYGS=array();
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
		if($userData[0]['WUAUTH'] === '2'){
			$rs = cmChkKenGen($db2con,'34',$userData[0]['WUSAUT']);//'34' => クエリーグループ権限
			if($rs['result'] !== true){
			    $rtn = 2;
			    $msg =  showMsg($rs['result'],array('クエリーグループの権限'));
			}
		}
    }
}
//クエリーグループのSEQテーブルのチェック
/*if($rtn === 0){
    $rs = fnCheckDB2QRYG($db2con,$QRYGID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('クエリーグループID'));
    }
}*/
//クエリーグループチェック
if($rtn === 0){
    $rs = cmChkQueryGroup($db2con,$userData[0]['WUUID'],$QRYGID,"","","","");
    if($rs['result'] !== true){
        $rtn = 2;
		$msg = showMsg($rs['result'],array('クエリーグループ'));
    }
}

$seqArr=array();
$updData=array();
$row = '';
//upd data
if($rtn===0){
    if(count($DB2QRYGS)>0){
        foreach($DB2QRYGS as $key =>$res){
			$QRYGSQRY=cmHscDe($res['QRYGSQRY']);
            $SEQ    = cmHscDe($res['QRYGSSEQ']);
            //SEQチェック
            if($SEQ !== ''){
                if($rtn === 0){
                    $exp['NAME']    = 'SEQ';
                    $exp['MAXLEN']  = 3;
                    $exp['PMSG']  = '数値';
                    $rs = comValidate($SEQ,$exp,false,true,true,true,false);
                    if($rs['RTN'] !== 0){
                        $rtn = $rs['RTN'];
                        $msg = $rs['MSG'];
                        $row=$key;
                        break;
                    }
                }
            }
            if($rtn===0){
                $updData[]=$res;
                if($res['QRYGSSEQ'] !==''){
                    $seqArr[]=$res['QRYGSSEQ'];
                }
            }
        }
    }
}
if($rtn===0){
    $r=fnGetDB2QRYGS($db2con,$userData,$QRYGID);
    if($r['result'] !==true){
        $rtn=1;
        $msg = showMsg($r['result']);
    }else{

        $oldDB2QRYGS=umEx($r['data']);
        foreach($oldDB2QRYGS as $res){
            $flg=true;
            $qrygsjoin=cmMer($res['QRYGSID']). '-' .cmMer($res['QRYGSQRY']);
            //テーブルで同じデータならarrayに入れてないチェック
            foreach($updData as $result){
                if(cmMer($result['QRYGSJOIN'])===$qrygsjoin){
                    $flg=false;
                    break;
                }
            }
            if($flg){
                $updData[]=$res;
                $seqArr[]=$res['QRYGSSEQ'];
            }
        }
    }
}
//QRYGSSEQ既にチェック
if($rtn === 0){
    if(count(array_unique($seqArr)) < count($seqArr)){
        $rtn = 1;
        $msg = showMsg('CHK_DUP',array('SEQ'));
    }
}

//クエリーグループのため一番最後のクエリーがピボットが会った場合、スケジュールの設定を消す
if($rtn===0){
	if(count($updData)>0){
		$gData=array();
		$groupD1NAME='';
		$groupPMPKEY='';
		$LASTUPDD1NAME='';
		$maxSEQ=0;
		$maxKeyIndex=0;
		//更新のデータの一番大きいのクエリーIDをもらう
		foreach($updData as $key=>$value){
			if($key===0){
				$maxSEQ=(int)$value['QRYGSSEQ'];
			}else{
				if($maxSEQ<$value['QRYGSSEQ']){
					$maxSEQ=$value['QRYGSSEQ'];
					$maxKeyIndex=$key;
				}
			}
		}
		$LASTUPDD1NAME=$updData[$maxKeyIndex]['QRYGSQRY'];
		e_log('更新のデータの一番大きいのクエリーIDをもらう:'.$LASTUPDD1NAME);
		//DBのデータの一番大きいのクエリーIDをもらう
		$r=fnGetLastDB2QRYG($db2con,$QRYGID);
		if($r['result']!==true){
			$rs=$r['result'];
            $rtn = 1;
            $msg = showMsg($rs);
		}else{
			$gData=$r['data'];
			if(count($gData)>0){
				$groupD1NAME=$gData[0]['PMNAME'];
				$groupPMPKEY=$gData[0]['PMPKEY'];
				e_log('DBのデータの一番大きいのクエリーIDをもらう:'.$groupD1NAME.'=>'.cmMer($LASTUPDD1NAME).'QRYGID');
				if(cmMer($groupD1NAME)!==cmMer($LASTUPDD1NAME) && cmMer($LASTUPDD1NAME)!==''){
					$r=fnSelAllScheduleSetting($db2con,$QRYGID,$groupPMPKEY);
					if($r['result']!==true){
						$rs=$r['result'];
			            $rtn = 1;
			            $msg = showMsg($rs);
					}else{
						if($r['ISEXIST']===1){
							$msg='このグループに対してピボットのスケジュールが登録されています。順番を変更するとピボットのスケジュールの登録が削除されますがよろしいですか？';
						}
					}
				}
			}
		}
	}
}

cmDb2Close($db2con);
/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'proc' => $proc,
    'FOCUS'=>$focus,
    'ROW'=>$row,
	'UPDDATA'=>$updData
);

echo(json_encode($rtnAry));



/***
*スケジュールの設定のSELECT
***/
function fnSelAllScheduleSetting($db2con,$QRYGID,$PMPKEY){
    $data = array();
    $params=array();
	$ISEXIST=0;
    $rs = true;
    $totcount=0;
    $strSQL  = ' SELECT COUNT(*) AS COUNT FROM DB2WSCD AS A ' ;
    $strSQL .= ' WHERE WSNAME=? AND WSPKEY=? ' ;
    $strSQL .= ' AND WSQGFLG=\'1\' ' ;
    $params[] = $QRYGID;
	$params[] = $PMPKEY;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
			while($row = db2_fetch_assoc($stmt)){
			    $totcount = $row['COUNT'];
			}
			if($totcount>0){
				$ISEXIST=1;
			}
			if($ISEXIST !== 1){
			    $strSQL  = ' SELECT COUNT(*) AS COUNT FROM DB2WAUT AS A ' ;
			    $strSQL .= ' WHERE WANAME=? AND WAPKEY=? ' ;
			    $strSQL .= ' AND WAQGFLG=\'1\' ' ;
				$params=array();
			    $params[] = $QRYGID;
				$params[] = $PMPKEY;
			    $stmt = db2_prepare($db2con,$strSQL);
			    if($stmt === false){
			        $rs = 'FAIL_SEL';
			    }else{
			        $r=db2_execute($stmt,$params);
			        if($r === false){
			            $rs = 'FAIL_SEL';
			        }else{
						while($row = db2_fetch_assoc($stmt)){
						    $totcount = $row['COUNT'];
						}
						if($totcount>0){
							$ISEXIST=1;
						}
						if($ISEXIST !== 1){
						    $strSQL  = ' SELECT COUNT(*) AS COUNT FROM DB2WSOC AS A ' ;
						    $strSQL .= ' WHERE SONAME=? AND SOPKEY=? ' ;
						    $strSQL .= ' AND SOQGFLG=\'1\' ' ;
							$params=array();
						    $params[] = $QRYGID;
							$params[] = $PMPKEY;
						    $stmt = db2_prepare($db2con,$strSQL);
						    if($stmt === false){
						        $rs = 'FAIL_SEL';
						    }else{
						        $r=db2_execute($stmt,$params);
						        if($r === false){
						            $rs = 'FAIL_SEL';
						        }else{
									while($row = db2_fetch_assoc($stmt)){
									    $totcount = $row['COUNT'];
									}
									if($totcount>0){
										$ISEXIST=1;
									}
									if($ISEXIST !== 1){
									    $strSQL  = ' SELECT COUNT(*) AS COUNT  FROM DB2WHIS AS A ' ;
									    $strSQL .= ' WHERE WHNAME=? AND WHPKEY=? ' ;
									    $strSQL .= ' AND WHQGFLG=\'1\' ' ;
										$params=array();
									    $params[] = $QRYGID;
										$params[] = $PMPKEY;
									    $stmt = db2_prepare($db2con,$strSQL);
									    if($stmt === false){
									        $rs = 'FAIL_SEL';
									    }else{
									        $r=db2_execute($stmt,$params);
									        if($r === false){
									            $rs = 'FAIL_SEL';
									        }else{
												while($row = db2_fetch_assoc($stmt)){
												    $totcount = $row['COUNT'];
												}
												if($totcount>0){
													$ISEXIST=1;
												}

											}
									    }
										
									}

								}
						    }
							
						}

					}
			    }
				
			}			

		}
    }
    return array('result'=>$rs,'ISEXIST'=>$ISEXIST);
}



/**
*-------------------------------------------------------* 
* クエリーグループが存在するかどうかをチェックすること
* 
* RESULT
*    01：存在する場合   true
*    02：存在ない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $QRYGID クエリーグループID
* 
*-------------------------------------------------------*
*/
function fnCheckDB2QRYG($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' SELECT COUNT(A.QRYGID) AS COUNT ' ;
    $strSQL .= ' FROM DB2QRYG AS A ' ;
    $strSQL .= ' WHERE A.QRYGID = ? ' ;
    $params[] = $QRYGID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
          while($row = db2_fetch_assoc($stmt)){
                $totcount = $row['COUNT'];
            }
            if($totcount === 0){
                $rs = 'NOTEXIST';
            }
        }
    }
    return $rs;
}

/**
*一番最後のクエリーグループがピボットあるかどうか
*/
function fnGetLastDB2QRYG($db2con,$QRYGID){
    $data = array();
    $params=array();
    $rs = true;
    $totcount=0;
    $strSQL  = ' SELECT EMP2.QRYGSID,PMNAME,PMPKEY,PMTEXT   FROM DB2QRYGS AS EMP2   ' ;
    $strSQL .= ' JOIN ' ;
    $strSQL .= ' (  ' ;
    $strSQL .= ' 	SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ' ;
    $strSQL .= ' 	FROM DB2QRYGS AS D  ' ;
    $strSQL .= ' 	GROUP BY QRYGSID  ' ;
    $strSQL .= ' ) EMP1  ' ;
    $strSQL .= ' ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ' ;
    $strSQL .= ' JOIN DB2PMST ON QRYGSQRY=PMNAME ' ;
    $strSQL .= ' WHERE EMP2.QRYGSID = ? ' ;
    $params[] = $QRYGID;
	e_log('getDB2QRYGSComFirmMsg.php strSQL'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
          while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return array('result'=>$rs,'data'=>umEx($data));
}
/***
*select old
**/
/*function  fnGetDB2QRYGS($db2con,$QRYGSID){
    $rs = true;
    $data=array();
    //構文
    $strSQL  = ' SELECT * FROM DB2QRYGS  ';
    $strSQL .= ' WHERE QRYGSID = ? ';
    $strSQL .= ' AND QRYGSSEQ<> 0 ';  
    $params[] =$QRYGSID;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return array('result'=>$rs,'data'=>$data);
}*/

function fnGetDB2QRYGS($db2con,$userData,$QRYGSID){
    $data = array();
    $params = array();
	$WUUID=$userData[0]['WUUID'];
	$WUAUTH=$userData[0]['WUAUTH'];
    $strSQL  = ' SELECT * FROM DB2QRYGS ';
    if($WUAUTH === '3' ){
        $strSQL .= '    WHERE QRYGSQRY IN ( ';
        $strSQL .= '   ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            ) ';
        $strSQL .= '			AND QRYGSID=? ';
        $params[]=$WUUID;
        $params[]=$WUUID;
        $params[]=$QRYGSID;
    }else if($WUAUTH === '4'){
        $strSQL .= '    WHERE QRYGSQRY IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';      
        $strSQL .= '            )';
		$strSQL .= '		AND QRYGSID = ? ';
        $params[]=$WUUID;
        $params[]=$QRYGSID;
    }else{
	    $strSQL.=' WHERE QRYGSID =? ';
        $params[]=$QRYGSID;
	}

    //e_log('*******取得：'.$strSQL.'parameter:'.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}

