<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$QRYGSID = (isset($_POST['QRYGSID']))?$_POST['QRYGSID']:'';
$QRYGSQRY=(isset($_POST['QRYGSQRY']))?$_POST['QRYGSQRY']:'';
$QRYGSQRYTEXT=(isset($_POST['QRYGSQRYTEXT']))?$_POST['QRYGSQRYTEXT']:'';
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$qrygsname = '';
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$QRYGSID = cmHscDe($QRYGSID);
$csv_d = array();

//ページング情報や検索情報などの保持用
$session = array();
$session['QRYGSID']  = $QRYGSID;
$session['QRYGSQRY']  = $QRYGSQRY;
$session['QRYGSQRYTEXT']  = $QRYGSQRYTEXT;
$session['start']   = $start;
$session['length']  = $length;
$session['sort']    = $sort;
$session['sortDir'] = $sortDir;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
		if($userData[0]['WUAUTH'] === '2'){
			$rs = cmChkKenGen($db2con,'34',$userData[0]['WUSAUT']);//'34' => クエリーグループ権限
			if($rs['result'] !== true){
			    $rtn = 2;
			    $msg =  showMsg($rs['result'],array('クエリーグループの権限'));
			}
		}
    }
}
//クエリーグループのSEQテーブルのチェック
if($rtn === 0){
    $rs = cmChkQueryGroup($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$QRYGSID,"","","","");
    if($rs['result'] !== true){
        $rtn = 2;
		$msg = showMsg($rs['result'],array('クエリーグループ'));
    }
}

if($rtn === 0){
    //ユーザー名取得
    $rs = fnChkQRYGID($db2con,$QRYGSID);
	if($rs['result'] !== true){
        $msg = showMsg($rs['result'],array('クエリーグループ'));
        $rtn = 1;
    }else{
        $qrygname = $rs['data'][0]['QRYGNAME'];
    }
}
if($rtn === 0){
    $rs = fnGetAllCount($db2con,$userData[0]['WUAUTH'],$QRYGSQRY,$QRYGSQRYTEXT,$licenseSql);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $allcount = $rs['data'];
    }
}
if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$userData[0]['WUAUTH'],$start,$length,$sort,$sortDir,$QRYGSID,$QRYGSQRY,$QRYGSQRYTEXT,$licenseSql);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'QRYGSNAME'  => cmHsc($qrygname),
    'QRYGSID'=>cmHsc($QRYGSID),
    'RTN' => $rtn,
    'MSG' => $msg,
    'session' => $session
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con,$wuaut,$start = '',$length = '',$sort = '',$sortDir = '',$QRYGSID,$QRYGSQRY,$QRYGSQRYTEXT,$licenseSql){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.QRYGSQRY ASC ';
    }

    $strSQL .= ' ) as rownum ';

    $strSQL .= ' FROM ';
    $strSQL .= ' ( ';
    $strSQL .= ' select C.* from ';//,case C.QRYGSSEQ when 0 then \'\' else C.QRYGSSEQ end as QRYGSSEQ
    $strSQL .= ' ( ';
    $strSQL .= ' SELECT DISTINCT(D1NAME) AS QRYGSQRY,D1TEXT AS QRYGSQRYTEXT';
    $strSQL .= ' ,QRYGSSEQ,QRYGSID ';
    $strSQL .= ' FROM ';
    if($wuaut === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '   ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            )';
        $strSQL .= '    )  B ';
        $params[]=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
        $params[]=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
    }else if($wuaut === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';     
        $strSQL .= '            )';
        $strSQL .= '    )  B ';
        $params[]=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
    }else{
        $strSQL .= '   FDB2CSV1   AS B ';
    }
    $strSQL .= ' LEFT OUTER JOIN DB2QRYGS AS E ON B.D1NAME = E.QRYGSQRY AND E.QRYGSID = ? '; 
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' WHERE B.D1CFLG = \'\' ';
    }
    $strSQL .= ' ) as C ';

    $strSQL .= ' ) as B ';

    $strSQL .= ' WHERE QRYGSQRY <> \'\' ';

    $params[]=$QRYGSID;

    if ($QRYGSQRY != '') {
        $strSQL.= ' AND QRYGSQRY like ? ';
        $params[]= '%' . $QRYGSQRY . '%';
    }
    if ($QRYGSQRYTEXT != '') {
        $strSQL.= ' AND QRYGSQRYTEXT like ? ';
        $params[]= '%' . $QRYGSQRYTEXT . '%';
    }

    $strSQL .= ' ) as A ';


    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        $params[]=$start + 1;
        $params[]=$start + $length;
    }
    //e_log('クエリーグループのFDB2CSV1データ取得：'.$strSQL.'parameter:'.print_r($params,true));

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$wuauth,$QRYGSQRY,$QRYGSQRYTEXT,$licenseSql){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT count(D1NAME) as COUNT ';
    $strSQL .= ' FROM FDB2CSV1 AS B';
    if($wuauth === '3' ){
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '   ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            )';
        $params[]=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
        $params[]=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
        if ($QRYGSQRY != '') {
            $strSQL.= ' AND D1NAME like ? ';
            $params[]= '%' . $QRYGSQRY . '%';
        }
        if ($QRYGSQRYTEXT != '') {
            $strSQL.= ' AND D1TEXT like ? ';
            $params[]= '%' . $QRYGSQRYTEXT . '%';
        }
    }else if($wuauth === '4'){
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';      
        $strSQL .= '            )';
        $params[]=$_SESSION['PHPQUERY']['user'][0]['WUUID'];
        if ($QRYGSQRY != '') {
            $strSQL.= ' AND D1NAME like ? ';
            $params[]= '%' . $QRYGSQRY . '%';
        }
        if ($QRYGSQRYTEXT != '') {
            $strSQL.= ' AND D1TEXT like ? ';
            $params[]= '%' . $QRYGSQRYTEXT . '%';
        }
    }else{
        $strSQL.=' WHERE D1NAME<> \'\' ';
        if ($QRYGSQRY != '') {
            $strSQL.= ' AND D1NAME like ? ';
            $params[]= '%' . $QRYGSQRY . '%';
        }
        if ($QRYGSQRYTEXT != '') {
            $strSQL.= ' AND D1TEXT like ? ';
            $params[]= '%' . $QRYGSQRYTEXT . '%';
        }
    }
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND D1CFLG = \'\' ';
    }
    //e_log('クエリーグループのカウントFDB2CSV1カウント取得：'.$strSQL,'parameter:'.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;

}
/*
*-------------------------------------------------------* 
* クエリーグループ名取得
*-------------------------------------------------------*
*/

function fnChkQRYGID($db2con,$QRYGSID){

    $data = array();
    $params = array();
    $rs=true;
    $strSQL = '';
    $strSQL .=' SELECT A.* FROM DB2QRYG AS A';
    $strSQL .=' WHERE A.QRYGID=? AND A.QRYGID<>\'\' ';
    $params[]=$QRYGSID;
    //e_log('クエリーグループ名チェック：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs='FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs='FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    $data=array('result'=>$rs,'data'=>$data);
    return $data;

}