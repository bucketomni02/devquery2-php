<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../licenseInfo.php");
//$licenseType = 'FREE';
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/

$QRYGID = (isset($_POST['QRYGID'])) ? $_POST['QRYGID'] : '';


/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
//htmlspecialcharsをデコード
$QRYGID = cmHscDe($QRYGID);
$QRYGNAME = cmHscDe($QRYGNAME);
$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID'])) ? $_SESSION['PHPQUERY']['user'][0]['WUUID'] : '';
$csv_d = array();
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $WUUID);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    } else {
        $userData = umEx($rs['data']);
    }
}
//クエリーグループのデータをもらう
if ($rtn === 0) {
    $rs = cmGetQueryGroup($db2con, $QRYGID);
    if ($rs['result'] !== true) {
        $rtn = $rs['rtn'];
        $msg = showMsg($rs['result'],array('クエリー'));
    } else {
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' =>$data
);
echo (json_encode($rtn));

