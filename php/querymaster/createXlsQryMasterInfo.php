<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$SD1NAME = (isset($_POST['SD1NAME'])?$_POST['SD1NAME']:'');
$SD1TEXT = (isset($_POST['SD1TEXT'])?$_POST['SD1TEXT']:'');
$RDOVAL  = (isset($_POST['RDOVAL'])?$_POST['RDOVAL']:'');
$SORT    = (isset($_POST['SORT'])?$_POST['SORT']:'');
$SORTDIR = (isset($_POST['SORTDIR'])?$_POST['SORTDIR']:'');


$rtn      = 0;
$msg      = '';
$fileName = '';
$ext      = '';
$data     = array();
$db2con   = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
//ログインユーザが管理ユーザーかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $auth = $rs['data'][0];
        $WUAUTH = cmMer($auth['WUAUTH']);
        if($WUAUTH === '2'){
            $SAUT = cmMer($auth['WUSAUT']);
            $rs = cmSelDB2AGRT($db2con,$SAUT);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg =  showMsg($rs['result']);
            }else{
                $authData = umEx($rs['data']);
                foreach($authData as $key => $value){
                        $authInfo[$value['AGMNID']] = $value['AGFLAG'];
                }
                if($authInfo['3'] !== '1' && $authInfo['6'] !== '1' && $authInfo['7'] !== '1'){
                    if($authInfo['8'] !== '1' && $authInfo['9'] !== '1' && $authInfo['10'] !== '1'){
                        if($authInfo['11'] !== '1' && $authInfo['12'] !== '1' && $authInfo['13'] !== '1'){
                            if($authInfo['14'] !== '1' && $authInfo['15'] !== '1' && $authInfo['16'] !== '1' && $authInfo['30'] !== '1'){
                                $rtn = 2;
                                 $msg =  showMsg('NOTEXIST',array('クエリー作成の権限'));
                            }
                        }
                    }
                }
            }
        }
    }
}
$header = array(
    'D1NAME' => getConstByLang('定義名'),//'定義名',
    'D1TEXT' => getConstByLang('テーブル名'),//'テーブル名',
    'D1WEBF' => getConstByLang('作成区分'),//'作成区分',
    'HTMLTEMPLATE' => getConstByLang('HTML'),//'HTML',
    'XLSTEMPLATE' => getConstByLang('EXCEL'),//'EXCEL',
    'SHOSAI' => getConstByLang('詳細'),//'詳細',
    'HOJO' => getConstByLang('補助'),//'補助',
    'CAN' => getConstByLang('候補'),//'候補',
    'SEIGYO' => getConstByLang('制御'),//'制御',
    'DQCUSR' => getConstByLang('作成者'),//'作成者',
    'DQCDAY' => getConstByLang('作成日'),//'作成日',
    'DQCTM' => getConstByLang('作成時刻'),//'作成時刻',
    'DQUUSR' => getConstByLang('更新者'),//'更新者',
    'DQUDAY' => getConstByLang('更新日'),//'更新日',
    'DQUTM' => getConstByLang('更新時刻'),//'更新時刻',
    'DQTMAX' => getConstByLang('実行時間最大'),//'実行時間最大',
    'DQTMIN' => getConstByLang('実行時間最小'),//'実行時間最小',
    'DQTAVG' => getConstByLang('実行時間平均'),//'実行時間平均',
    'DQEKAI' => getConstByLang('実行回数'),//'実行回数',
    'DQEUSR' => getConstByLang('最終実行者'),//'最終実行者',
    'DQEDAY' => getConstByLang('最終実行日'),//'最終実行日',
    'DQETM' => getConstByLang('最終実行時刻'),//'最終実行時刻'
    );
if($rtn === 0){
    $rs = fnGetQryMasterInfo($db2con,$WUAUTH,$RDOVAL,$SD1NAME,$SD1TEXT,$SORT,$SORTDIR);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $dataArr = umEx($rs['data']);
    }
}
if ($rtn === 0) {
    if (EXCELVERSION === '2007' || EXCELVERSION === '2003' || EXCELVERSION === 'XLSX') {
        
        include_once("../common/lib/PHPExcel/Classes/PHPExcel.php");
        include_once("../common/lib/PHPExcel/Classes/PHPExcel/IOFactory.php");
        include_once("../common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");
        include_once("../common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php");

        $res = createXls($header,$dataArr);
        if(count($res)>0){
            $fileName = $res['FILENM'];
            $ext      = $res['EXT'];
        }
    }else if(EXCELVERSION === 'html'){
        $res = creteHtmlXls($header,$dataArr);
        if(count($res>0)){
            $fileName = $res['FILENM'];
            $ext      = $res['EXT'];
        }
    }
}

$rtnAry = array(
    'RTN'     => $rtn,
    'MSG'     => $msg,
    'HEADER'  => $header,
    'DATA'    => $dataArr,
    'FILENAME'=> $fileName,
    'EXT'     => $ext
    
    
);
echo (json_encode($rtnAry));

// クエリーマスタ情報取得
function fnGetQryMasterInfo($db2con,$WUAUTH,$rdoVal,$SD1NAME,$SD1TEXT,$SORT,$SORTDIR){

    $data = array();
    $params = array();
    $ConstExist = getConstByLang('有');

    $strSQL  =  '   SELECT ';
    $strSQL .=  '      A.D1NAME, ';
    $strSQL .=  '      A.D1TEXT, ';
    $strSQL .=  '      (CASE A.D1WEBF '; 
    $strSQL .=  '          WHEN \'1\' THEN \'Web\' ';
    $strSQL .=  '          ELSE \'5250\' ';
    $strSQL .=  '      END) AS D1WEBF, ';
    $strSQL .=  '      (CASE ';
    $strSQL .=  '           WHEN A.D1DISF = \'1\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           WHEN A.DISCSN <> \'\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           WHEN A.D1EFLG = \'1\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           ELSE \'\' ';
    $strSQL .=  '       END) AS JIKKOSET, ';
    $strSQL .=  '       (CASE ';
    $strSQL .=  '           WHEN B.HTMLTN <> \'\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           ELSE \'\' ';
    $strSQL .=  '       END) AS HTMLTEMPLATE, ';
    $strSQL .=  '       (CASE ';
    $strSQL .=  '           WHEN A.D1TMPF <> \'\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           WHEN G.ECNAME <> \'\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           ELSE \'\' ';
    $strSQL .=  '       END) AS XLSTEMPLATE, ';
    $strSQL .=  '       (CASE ';
    $strSQL .=  '           WHEN C.DFNAME <> \'\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           WHEN H.DRKMTN <> \'\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           ELSE \'\' ';
    $strSQL .=  '       END) AS SHOSAI, ';
    $strSQL .=  '      (CASE '; 
    $strSQL .=  '           WHEN A.D1INFO <> \'\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           WHEN A.D1INFG = \'1\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           ELSE \'\' ';
    $strSQL .=  '       END) AS HOJO, ';
    $strSQL .=  '      (CASE ';
    $strSQL .=  '           WHEN D3NAME <> \'\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           ELSE \'\' ';
    $strSQL .=  '       END) AS CAN, ';
    $strSQL .=  '      (CASE ';
    $strSQL .=  '           WHEN D.DTNAME <> \'\' THEN \''.$ConstExist.'\'';
    $strSQL .=  '           ELSE \'\' ';
    $strSQL .=  '       END) AS SEIGYO, ';
    $strSQL .=  '       E.DQCUSR, ';
    $strSQL .=  '       (CASE ';
    $strSQL .=  '           WHEN E.DQCDAY IS NULL THEN \'\' ';
    $strSQL .=  '           WHEN TRIM(E.DQCDAY) = \'\' THEN \'\' ';
    $strSQL .=  '           ELSE VARCHAR_FORMAT(TIMESTAMP_FORMAT(E.DQCDAY,\'YYYYMMDD\'), \'YYYY/MM/DD\')    ';
    $strSQL .=  '       END ) AS DQCDAY, ';
    $strSQL .=  '       (CASE ';
    $strSQL .=  '           WHEN E.DQCTM <> \'\' THEN SUBSTR(E.DQCTM,1,2)||\':\'|| SUBSTR(E.DQCTM,3,2)||\':\'|| SUBSTR(E.DQCTM,5,2) ';
    $strSQL .=  '           ELSE \'\' ';
    $strSQL .=  '       END) AS DQCTM, ';
    $strSQL .=  '       E.DQUUSR, ';
    $strSQL .=  '       E.DQTMAX, ';
    $strSQL .=  '       E.DQTMIN, ';
    $strSQL .=  '       E.DQTAVG, ';
    $strSQL .=  '       (CASE ';
    $strSQL .=  '           WHEN E.DQUDAY IS NULL THEN \'\' ';
    $strSQL .=  '           WHEN TRIM(E.DQUDAY) = \'\' THEN \'\' ';
    $strSQL .=  '           ELSE VARCHAR_FORMAT(TIMESTAMP_FORMAT(E.DQUDAY,\'YYYYMMDD\'), \'YYYY/MM/DD\')    ';
    $strSQL .=  '       END ) AS DQUDAY, ';
    $strSQL .=  '       (CASE ';
    $strSQL .=  '           WHEN E.DQUTM <> \'\' THEN SUBSTR(E.DQUTM,1,2)||\':\'|| SUBSTR(E.DQUTM,3,2)||\':\'|| SUBSTR(E.DQUTM,5,2) ';
    $strSQL .=  '           ELSE \'\' ';
    $strSQL .=  '       END) AS DQUTM, ';
    $strSQL .=  '       (CASE WHEN E.DQEKAI IS NOT NULL THEN E.DQEKAI ELSE 0 END) AS DQEKAI , ';
    $strSQL .=  '       E.DQEUSR, ';
    $strSQL .=  '       (CASE ';
    $strSQL .=  '           WHEN E.DQEDAY IS NULL THEN \'\' ';
    $strSQL .=  '           WHEN TRIM(E.DQEDAY) = \'\' THEN \'\' ';
    $strSQL .=  '           ELSE VARCHAR_FORMAT(TIMESTAMP_FORMAT(E.DQEDAY,\'YYYYMMDD\'), \'YYYY/MM/DD\')    ';
    $strSQL .=  '       END ) AS DQEDAY, ';
    $strSQL .=  '       (CASE ';
    $strSQL .=  '           WHEN E.DQETM <> \'\' THEN SUBSTR(E.DQETM,1,2)||\':\'|| SUBSTR(E.DQETM,3,2)||\':\'|| SUBSTR(E.DQETM,5,2) ';
    $strSQL .=  '           ELSE \'\' ';
    $strSQL .=  '       END) AS DQETM ';
    if($WUAUTH === '3'){
        $strSQL .=  '   FROM ';
        $strSQL .=  '   ( ';
        $strSQL .=  '       SELECT  * ';
        $strSQL .=  '       FROM ';
        $strSQL .=  '           FDB2CSV1 ';
        $strSQL .=  '       WHERE D1NAME IN ( ';
        $strSQL .=  '           ( ';
        $strSQL .=  '               SELECT DISTINCT ';
        $strSQL .=  '                   WGNAME ';
        $strSQL .=  '               FROM ';
        $strSQL .=  '                   DB2WGDF ';
        $strSQL .=  '               WHERE ';
        $strSQL .=  '                   WGGID IN ( ';
        $strSQL .=  '                       SELECT ';
        $strSQL .=  '                           WUGID ';
        $strSQL .=  '                       FROM ';
        $strSQL .=  '                           DB2WUGR ';
        $strSQL .=  '                        WHERE ';
        $strSQL .=  '                            WUUID = ?  ';
        $strSQL .=  '                   ) ';
        $strSQL .=  '           ) UNION  ( ';
        $strSQL .=  '               SELECT ';
        $strSQL .=  '                   DQNAME ';
        $strSQL .=  '               FROM ';
        $strSQL .=  '                   DB2QHIS ';
        $strSQL .=  '               RIGHT JOIN FDB2CSV1 ';
        $strSQL .=  '               ON DQNAME = D1NAME ';
        $strSQL .=  '               WHERE ';
        $strSQL .=  '                   DQNAME <> \'\' ';
        $strSQL .=  '                   AND DQCUSR = ? ';
        $strSQL .=  '           ) '; 
        $strSQL .=  '       )';
        $strSQL .=  '   ) A ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .=  '   FROM ';
        $strSQL .=  '   ( ';
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE ';
        $strSQL .= '        D1NAME IN ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                DQNAME ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                DQNAME <> \'\' ';
        $strSQL .= '             AND DQCUSR = ? ';
        $strSQL .= '        )';
        $strSQL .=  '   ) A ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .=  '   FROM FDB2CSV1 A ';
    }
    $strSQL .=  '   LEFT JOIN DB2HTMLT B ';
    $strSQL .=  '   ON A.D1NAME = B.HTMLTD ';
    $strSQL .=  '   LEFT JOIN ';
    $strSQL .=  '   (SELECT DFNAME ';
    $strSQL .=  '    FROM DB2WDTL ';
    $strSQL .=  '    JOIN DB2WDFL ';
    $strSQL .=  '    ON DTNAME = DFNAME ';
    $strSQL .=  '    GROUP BY DFNAME)C ';
    $strSQL .=  '    ON A.D1NAME = C.DFNAME ';
    $strSQL .=  '   LEFT JOIN ';
    $strSQL .=  '   (SELECT ';
    $strSQL .=  '       DRKMTN ';
    $strSQL .=  '   FROM ';
    $strSQL .=  '       DB2DRGS ';
    $strSQL .=  '   GROUP BY ';
    $strSQL .=  '       DRKMTN ) H';
    $strSQL .=  '    ON A.D1NAME = H.DRKMTN ';
    $strSQL .=  '   LEFT JOIN ';
    $strSQL .=  '    (SELECT ';
    $strSQL .=  '        ECNAME ';
    $strSQL .=  '    FROM ';
    $strSQL .=  '        DB2ECON ';
    $strSQL .=  '    LEFT JOIN DB2EINS ';
    $strSQL .=  '    ON EINAME = ECNAME';
    $strSQL .=  '    GROUP BY ECNAME)G ';
    $strSQL .=  '    ON A.D1NAME = G.ECNAME ';
    $strSQL .=  '   LEFT JOIN ';
    $strSQL .=  '   (SELECT DTNAME ';
    $strSQL .=  '   FROM DB2COLM ';
    $strSQL .=  '   JOIN DB2COLT ';
    $strSQL .=  '   ON DCNAME = DTNAME ';
    $strSQL .=  '   GROUP BY DTNAME)D ';
    $strSQL .=  '   ON A.D1NAME = D.DTNAME ';
    $strSQL .=  '   LEFT JOIN DB2QHIS E ';
    $strSQL .=  '   ON A.D1NAME = E.DQNAME ';
    $strSQL .=  '   LEFT JOIN ';
    $strSQL .=  '   (SELECT D3NAME FROM FDB2CSV3 ';
    $strSQL .=  '   WHERE (D3CANL <> \'\' ';
    $strSQL .=  '       AND D3CANF <> \'\') ';
    $strSQL .=  '   OR (D3DFMT <> \'\') ';
    $strSQL .=  '   GROUP BY D3NAME ';
    $strSQL .=  '   UNION ';
    $strSQL .=  '   SELECT CNQRYN D3NAME FROM BQRYCND ';
    $strSQL .=  '   WHERE (CNCANL <> \'\' ';
    $strSQL .=  '       AND CNCANF <> \'\') ';
    $strSQL .=  '   OR (CNDFMT <> \'\') ';
    $strSQL .=  '   GROUP BY CNQRYN ';
    $strSQL .=  '   UNION ';
    $strSQL .=  '   SELECT DHNAME D3NAME ';
    $strSQL .=  '   FROM DB2HCND ';
    $strSQL .=  '   WHERE DHINFO <> \'\' ';
    $strSQL .=  '   GROUP BY DHNAME ';
    $strSQL .=  '   )F ';
    $strSQL .=  '   ON A.D1NAME = F.D3NAME ';
    $strSQL .=  ' WHERE ';
    $strSQL .=  ' A.D1NAME <> \'\' ';
    if($SD1NAME != ''){
        $strSQL .= ' AND A.D1NAME like ? ';
        array_push($params,'%'.$SD1NAME.'%');
    }
    if($SD1TEXT != ''){
        $strSQL .= ' AND A.D1TEXT like ? ';
        array_push($params,'%'.$SD1TEXT.'%');
    }
    // 5250件用
    if($rdoVal === '1'){
        $strSQL .= ' AND A.D1WEBF = \'\' ';
    }
    // web件用
    if($rdoVal === '2'){
        $strSQL .= ' AND A.D1WEBF = \'1\' ';
    }

    if($SORT !== ''){
        $strSQL .= ' ORDER BY A.'.$SORT.' '.$SORTDIR.' ';
    }else{
        $strSQL .= ' ORDER BY A.D1NAME ASC ';
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
            $err =db2_stmt_errormsg();
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $err =db2_stmt_errormsg();
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}


function createXls($headerData,$dataArr){

    $data = array();

    $book = new PHPExcel();
    $book->setActiveSheetIndex(0);
    $book->getActiveSheet()->setTitle("クエリー情報");
    $sheet = $book->getActiveSheet();
    $col = 0;
    $row = 1;

    // ヘーダ―印字
    foreach ($headerData as $key => $value) {
        $value = cmMer($value);
        $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
        $col++;
    }
    
    $row = 2;
    //データ印字
    while (list($key1, $value1) = each($dataArr)) {
        $col = 0; 
        foreach ($headerData as $hkey => $hvalue) {
            $val = cmMer($value1[$hkey]);
            if($hkey !== 'DQEKAI' && $hkey !== 'DQTMAX' && $hkey !== 'DQTMIN' && $hkey !== 'DQTAVG'){
                $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($val, PHPExcel_Cell_DataType::TYPE_STRING);
            }else{
                $sheet->getCellByColumnAndRow($col, $row)->setValueExplicit($val, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            }
            $col++;
        }
        unset($dataArr[$key1]);
        $row++;
    }
    $r = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 7)), 0, 7);

    $fileNm = 'qrymasterInfo_'.date('YmdHis').'_'.$r;
    
    if (EXCELVERSION === '2003') {
        $writer = PHPExcel_IOFactory::createWriter($book,'Excel5');
        $ext = 'xls';
    } else if (EXCELVERSION === '2007' || EXCELVERSION === 'XLSX') {
        $writer = PHPExcel_IOFactory::createWriter($book,'Excel2007');
        $ext = 'xlsx';
    }
    $writer->save(TEMP_DIR . cmMer($fileNm) . '.' . $ext);
    
   
    
    $data = array(
        'FILENM' => $fileNm ,
        'EXT'    => $ext
   );
    return $data;
}

function creteHtmlXls($headerData,$dataArr){
    $data = array();
    $file_name = 'qrymasterInfo_'.date('YmdHis');
    $ext       = 'xls';
   
    $fp = fopen(TEMP_DIR . cmMer($file_name).'.'.$ext, 'w');
   
    $decStyle = 'mso-number-format:\#\,\#\#0\'';
    $tableClass = '';
    if (isset($_SESSION['PHPQUERY'])) {
        if ($_SESSION['PHPQUERY']['LOGIN'] === '1') {
            $db2con = cmDb2Con();
            cmSetPHPQUERY($db2con);
            
            $DB2WUSR = cmGetDB2WUSR($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
            
            $DB2WUSR = umEx($DB2WUSR);
            
            cmDb2Close($db2con);
            
            $wuclr3 = $DB2WUSR[0]['WUCLR3'];
            $wuclr3 = (($wuclr3 === '') ? COLOR3 : $wuclr3);
            
            switch ($wuclr3) {
                case 'red':
                    $tableClass = 'table th{background-color:#b60f0b;color:#ffffff;}';
                    break;
                case 'blue':
                    $tableClass = 'table th{background-color:#005879;color:#ffffff;}';
                    break;
                case 'orange':
                    $tableClass = 'table th{background-color:#a66505;color:#ffffff;}';
                    break;
                case 'purple':
                    $tableClass = 'table th{background-color:#541fa7;color:#ffffff;}';
                    break;
                case 'green':
                    $tableClass = 'table th{background-color:#246534;color:#ffffff;}';
                    break;
                case 'fb':
                    $tableClass = 'table th{background-color:#192441;color:#ffffff;}';
                    break;
                case 'muted':
                    $tableClass = 'table th{background-color:#595959;color:#ffffff;}';
                    break;
                case 'dark':
                    $tableClass = 'table th{background-color:black;color:#ffffff;}';
                    break;
                case 'pink':
                    $tableClass = 'table th{background-color:#6b2345;color:#ffffff;}';
                    break;
                case 'brown':
                    $tableClass = 'table th{background-color:#4f2a1b;color:#ffffff;}';
                    break;
                case 'sea-blue':
                    $tableClass = 'table th{background-color:#013760;color:#ffffff;}';
                    break;
                case 'banana':
                    $tableClass = 'table th{background-color:#cb9704;color:#ffffff;}';
                    break;
                default:
                    $tableClass = 'table th{background-color:#f5f5f5;color:#666;}';
                    break;
            }
            
        }
    }
    if ($tableClass === '') {
        $tableClass = 'table th{background-color:rgb(79,129,189);color:#ffffff;}';
    }
    
    fwrite($fp, '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">');
    fwrite($fp, '<html><head>');
    
    if ($htmlFlg === true) {
        fwrite($fp, '<meta charset="UTF-8">');
    }
   
    fwrite($fp, '<meta http-equiv="Content-Type" content="application/vnd.ms-excel; " />');
    fwrite($fp, '<meta http-equiv="Content-Disposition" content="attachment; filename=output.xls" />');
    fwrite($fp, '<style type="text/css">');
    fwrite($fp, '<!-- .txt{mso-number-format:"\@";}-->');
    fwrite($fp, 'table{font-size:13px;}');
    fwrite($fp, $tableClass);
    fwrite($fp, '</style>');
    fwrite($fp, '</head><body>');
    fwrite($fp, '<table border="1">');
    fwrite($fp, '<thead><tr>');

    foreach ($headerData as $key => $value) {
        $value = str_replace(" ","&#32;",cmMer($value));
        $value = mb_convert_encoding($value, 'HTML-ENTITIES','UTF-8');
        fwrite($fp, '<th nowrap class="xls-header txt">' . $value . '</th>');
    }
    fwrite($fp, '</tr></thead><tbody>');
    foreach ($dataArr as $key => $row) {
        
        $bgcolor = '';
        if ($key % 2 !== 0) {
            $bgcolor = 'bgcolor="#dddddd"';
        }
        fwrite($fp, '<tr ' . $bgcolor . '>');
        
        foreach ($headerData as $hkey => $val) {
            $col = $row[$hkey];
            $col = str_replace(" ","&#32;",cmMer($col));
            $col = mb_convert_encoding(cmHsc($col),'HTML-ENTITIES','UTF-8');
            if($hkey === 'DQEKAI' || $hkey !== 'DQTMAX' || $hkey !== 'DQTMIN' || $hkey !== 'DQTAVG'){
                fwrite($fp, '<td nowrap align="right" style="' . $decStyle . '">' . $col . '</td>');
            }else{
                fwrite($fp, '<td nowrap align="left" class="txt">' . $col . '</td>');
            }
        }
        fwrite($fp, '</tr>');

    }
    fwrite($fp, '</tbody></table><table><tr><td></td></tr></table></body></html>');

    fclose($fp);

    $data = array(
        'FILENM' => $file_name ,
        'EXT'    => $ext
        
   );
    return $data;
}
