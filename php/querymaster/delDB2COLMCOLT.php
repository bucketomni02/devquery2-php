<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$DNAME = $_POST['DNAME'];


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'14',$userData[0]['WUSAUT']);//'14' =>制御レベル設定
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('制御レベル設定の権限'));
            }
        }
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$DNAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg('ログインユーザーに指定したクエリーに対しても制御レベル設定の権限がありません。');
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}

    if($rtn === 0){
        $rs = fnDelColmColt($db2con,$DNAME);
        if($rs === false){
            $rtn = 1;
            $msg = showMsg('FAIL_DEL');//'削除処理に失敗しました。管理者にお問い合わせください。';
        
    }
}


cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnAry));

/**
* 制御レベル情報削除
* 
* RESULT
*    01：false
* 
* @param Object  $db2con DBコネクション
* @param String  $DNAME  クエリ名
* 
*/

function fnDelColmColt($db2con,$DNAME){

    $rs = true;

    //構文
    $strSQL  = ' DELETE ';
    $strSQL .= ' FROM DB2COLM ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' DCNAME = ? ' ;

    $params = array(
        $DNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $rs = 'FAIL_DEL';
        }
        else{
            //構文
            $strSQL  = ' DELETE ';
            $strSQL .= ' FROM DB2COLT ';
            $strSQL .= ' WHERE ';
            $strSQL .= ' DTNAME = ? ' ;
            $params = array(
                $DNAME
            );

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $rs = 'FAIL_DEL';
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                     $rs = 'FAIL_DEL';
                }
            }  
        }
    }    
    return $rs;
}
