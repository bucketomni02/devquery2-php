<?php

include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

//ダウンロード中フラグを立てる
$_SESSION['PHPQUERY']['DOWNLOAD'] = '1';

$dlname = cmHscDe($_GET['DLNAME']);
$fname  = cmHscDe($_GET['FNAME']);
$ext    = cmHscDe($_GET['EXT']);



$fname = TEMP_DIR.(string)$fname;

if (file_exists($fname)) {

    $ua = $_SERVER['HTTP_USER_AGENT'];
    
    
    if(!(preg_match("/Chrome/i",$ua)) && !(preg_match("/Safari/i",$ua))){
        $dlname = mb_convert_encoding($dlname, "sjis-win","UTF-8");
        
    }else if((preg_match("/Edge/i",$ua))){
       $dlname = mb_convert_encoding($dlname, "sjis-win","UTF-8");
    }

    switch($ext){
        case 'xlsx':
            // Excel2007形式で出力する
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            break;
        case 'xlsm':
            // Excel2007形式で出力する
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            break;
        case 'xls':
            // Excel2003形式で出力する
            header('Content-Type: application/vnd.ms-excel');
            break;
        case 'csv':
            // CSV形式で出力する
            header('Content-Type: text/csv');
            break;
        case 'zip':
            // zip 形式で出力する
            header('Content-Type: application/zip');
            break;
    }
   
    header('Content-Disposition: attachment;filename="'.$dlname.'"');
    
    if (!readfile($fname)) {
        //die("Cannot read the file(".$fname.")");
       
    }
}else{
	echo($fname.'nofile');
    
}
     @unlink($fname);

//ダウンロード中フラグを落とす
$_SESSION['PHPQUERY']['DOWNLOAD'] = '0';

exit;