<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------*
* リクエスト
*-------------------------------------------------------*
*/

$D3NAME = $_POST['D3NAME'];
$D3FILID= $_POST['D3FILID'];
$D3JSEQ = $_POST['D3JSEQ'];
$D3CNT  = $_POST['D3CNT'];
$D1WEBF = $_POST['D1WEBF'];
$D3FLD  = $_POST['D3FLD'];
$PROC   = $_POST['proc'];
$DATA   = json_decode($_POST['DATA'],true);
$SHOWFLG1 = (isset($DATA['SHOWFLG1'])) ?  $DATA['SHOWFLG1'] : '';
$CONDITION = (isset($DATA['CONDITION'])) ?  $DATA['CONDITION'] : '';
$SCHED = (isset($DATA['SCHED'])) ?  $DATA['SCHED'] : '';
if($SHOWFLG1 === '0'){
    $CAL1 = (isset($DATA['CAL1'])) ?  $DATA['CAL1'] : '';
    $NUM1 = (isset($DATA['NUM1'])) ?  $DATA['NUM1'] : '';
    $TIME1 = (isset($DATA['TIME1'])) ?  $DATA['TIME1'] : '';
}else{
    $CAL1 = '';
    $NUM1 = 0;
    $TIME1 = '';
}
$SHOWFLG2 = (isset($DATA['SHOWFLG2'])) ?  $DATA['SHOWFLG2'] : '';
if($SHOWFLG2 === '0'){
    $CAL2 = (isset($DATA['CAL2'])) ?  $DATA['CAL2'] : '';
    $NUM2 = (isset($DATA['NUM2'])) ?  $DATA['NUM2'] : '';
    $TIME2 = (isset($DATA['TIME2'])) ?  $DATA['TIME2'] : '';
}else{
    $CAL2 = '';
    $NUM2 = 0;
    $TIME2 = '';
}
$D1CFLG = (isset($_POST['D1CFLG'])) ?  $_POST['D1CFLG'] : '';

/*
*-------------------------------------------------------*
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------*
* チェック処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
            $userData  = umEx($rs['data']);
            if($userData[0]['WUAUTH'] === '2'){
                $rs = cmChkKenGen($db2con,'13',$userData[0]['WUSAUT']);//'13' =>検索候補
                if($rs['result'] !== true){
                    $rtn = 2;
                    $msg =  showMsg($rs['result'],array('検索候補の権限'));
                }
            }
     }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $D3NAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D3NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg('ログインユーザーに指定したクエリーに対しても検索候補の権限がありません。');
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}

//検索条件が削除されるかどうかチェック
if($rtn===0){
    if($D1CFLG === '1'){
        $chkCnd = cmChkBSQLCND( $db2con, $D3NAME,$D3JSEQ);
        if( $chkCnd !== true ) {
            $rtn = 4;
            $msg = showMsg($chkCnd,array('検索条件'));
        }
    }else{
        $chkCnd = cmChkCnd ( $db2con, $D3NAME, $D3FILID, $D3JSEQ, $D3CNT, $D1WEBF,$D3FLD);
        if( $chkCnd['result'] !== true ) {
            $rtn = 4;
            $msg = showMsg($chkCnd['result'],array('検索条件'));
        }
    }
}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/
if($rtn === 0){
    if($PROC !== 'D'){
        if($DATA['FORMAT'] === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('フォーマット'));
        }
    }
}
$FROM = '';
if($CONDITION == 'RANGE'){
    $FROM = 'FROMの';
}
if($rtn === 0){
    if ($NUM1 === '') {
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array($FROM.'移動数'));
    }
}
if($rtn === 0){
    if(!checkNum($NUM1)){
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array($FROM.'移動数','数値'));
    }
}
if($rtn === 0){
    if (strpos($NUM1, '.') !== false) {
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array($FROM.'移動数','数値'));
    }
}
if($rtn === 0){
    if(!checkMaxLen($NUM1,3)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array($FROM.'移動数'));
    }
}
if($rtn === 0){
    if((int)$NUM1<0){
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array($FROM.'移動数','ゼロ以上'));
    }
}
if($CONDITION == 'RANGE'){
    if($rtn === 0){
        if ($NUM2 === '') {
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('TOの移動数'));
        }
    }
    if($rtn === 0){
        if(!checkNum($NUM2)){
            $rtn = 1;
            $msg = showMsg('CHK_FMT',array('TOの移動数','数値'));
        }
    }
    if($rtn === 0){
        if (strpos($NUM2, '.') !== false) {
            $rtn = 1;
            $msg = showMsg('CHK_FMT',array('TOの移動数','数値'));
        }
    }
    if($rtn === 0){
        if(!checkMaxLen($NUM2,3)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('TOの移動数'));
        }
    }
    if($rtn === 0){
        if((int)$NUM2<0){
            $rtn = 1;
            $msg = showMsg('CHK_FMT',array('TOの移動数','ゼロ以上'));
        }
    }
}
if($rtn === 0){
    if($D1WEBF !== '1'){
        //FDB2CSV3テーブル更新
        $rs = fnUpdateFDB2CSV3($db2con,$DATA['FORMAT'],$DATA['SURASHU'],$D3NAME,$D3FILID,$D3JSEQ,$D3CNT,$SHOWFLG1, $CAL1, $NUM1, $TIME1 ,$SHOWFLG2 , $CAL2, $NUM2, $TIME2,$SCHED);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('フィールド'));
        }
    }else{
        //BSQLCND
        if($D1CFLG==='1'){
            //FDB2CSV3テーブル更新
            $rs = fnUpdateBSQLCND($db2con,$DATA['FORMAT'],$DATA['SURASHU'],$D3NAME,$D3FILID,$D3JSEQ,$SHOWFLG1, $CAL1, $NUM1, $TIME1 ,$SHOWFLG2 , $CAL2, $NUM2, $TIME2,$SCHED);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('フィールド'));
            }
        }else{
            //BQRYCNDテーブル更新
            $rs = fnUpdateBQRYCND($db2con,$DATA['FORMAT'],$DATA['SURASHU'],$D3NAME,$D3FILID,$D3JSEQ,$SHOWFLG1, $CAL1, $NUM1, $TIME1 ,$SHOWFLG2 , $CAL2, $NUM2, $TIME2,$SCHED);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('フィールド'));
            }
        }
    }
}
cmDb2Close($db2con);
/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'NUM1'=>(int)$NUM1,
    'NUM2'=>(int)$NUM2,
    'DATA' =>$DATA
);

echo(json_encode($rtnAry));

/**
* FDB2CSV3テーブル更新
* 
* @param Object  $db2con    DBコネクション
* @param String  $FORMAT    フォーマット
* @param String  $SURASHU   スラッシュつけるつけない
* @param String  $D3NAME    D3NAME
* @param String  $D3FILID   D3FILID
* @param String  $D3JSEQ    D3JSEQ
* @param String  $D3CNT     D3CNT
* 
*/
function fnUpdateFDB2CSV3($db2con,$FORMAT,$SURASHU,$D3NAME,$D3FILID,$D3JSEQ,$D3CNT,$D3DFIN,$D3DFPM,$D3DFDY,$D3DFFL,$D3DTIN,$D3DTPM,$D3DTDY,$D3DTFL,$D3DSCH){

    $rs = true;
    $rsCount = array();

    $strSQL  = ' SELECT COUNT(*) AS COUNT';
    $strSQL .= ' FROM FDB2CSV3'; 
    $strSQL .= ' WHERE D3NAME = ? ';
    $strSQL .= ' AND D3FILID = ? ';
    $strSQL .= ' AND D3JSEQ = ? ';
    $strSQL .= ' AND D3CNT = ? ';

    $params = array(
        $D3NAME,
        $D3FILID,
        $D3JSEQ,
        $D3CNT
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rsCount[] = $row['COUNT'];
            }
            
            if($rsCount[0] > 0){
                //構文
                $strSQL  = ' UPDATE FDB2CSV3 ';
                $strSQL .= ' SET ';
                $strSQL .= ' D3DFMT = ?, ';
                $strSQL .= ' D3DSFL = ? , ';
                /**20161019**/
                $strSQL .= ' D3DFIN = ? ,';
                $strSQL .= ' D3DFPM = ? ,';
                $strSQL .= ' D3DFDY = ? ,';
                $strSQL .= ' D3DFFL = ? ,';
                $strSQL .= ' D3DTIN = ? ,';
                $strSQL .= ' D3DTPM = ? ,';
                $strSQL .= ' D3DTDY = ? ,';
                $strSQL .= ' D3DTFL = ? ,';
                $strSQL .= ' D3DSCH = ? ';
                /**20161019**/
                $strSQL .= ' WHERE D3NAME = ? ';
                $strSQL .= ' AND D3FILID = ? ';
                $strSQL .= ' AND D3JSEQ = ? ';
                $strSQL .= ' AND D3CNT = ? ';

                $params = array(
                    $FORMAT,
                    $SURASHU,
                    $D3DFIN,
                    $D3DFPM,
                    $D3DFDY,
                    $D3DFFL,
                    $D3DTIN,
                    $D3DTPM,
                    $D3DTDY,
                    $D3DTFL,
                    $D3DSCH,
                    $D3NAME,
                    $D3FILID,
                    $D3JSEQ,
                    $D3CNT
                );
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = 'FAIL_INS';
                }else{
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $rs = 'FAIL_INS';
                    }
                }

            }
            else{
                $rs = 'NOTEXIST_UPD';
            }
            
        }
    }
    return $rs;

}

/**
* BQRYCNDテーブル更新
* 
* @param Object  $db2con    DBコネクション
* @param String  $FORMAT    フォーマット
* @param String  $SURASHU   スラッシュつけるつけない
* @param String  $D3NAME    D3NAME
* @param String  $D3FILID   D3FILID
* @param String  $D3JSEQ    D3JSEQ
* 
*/
function fnUpdateBQRYCND($db2con,$FORMAT,$SURASHU,$D3NAME,$D3FILID,$D3JSEQ,$CNDFIN,$CNDFPM,$CNDFDY,$CNDFFL,$CNDTIN,$CNDTPM,$CNDTDY,$CNDTFL,$CNDSCH){
    $D3JSEQ = explode('-',$D3JSEQ);
    $CNMSEQ = $D3JSEQ[0];
    $CNSSEQ = $D3JSEQ[1];
    $rs = true;
    $rsCount = array();

    $strSQL  = ' SELECT COUNT(*) AS COUNT';
    $strSQL .= ' FROM BQRYCND'; 
    $strSQL .= ' WHERE CNQRYN = ? ';
    $strSQL .= ' AND CNFILID = ? ';
    $strSQL .= ' AND CNMSEQ = ? ';
    $strSQL .= ' AND CNSSEQ = ? ';

    $params = array(
        $D3NAME,
        $D3FILID,
        $CNMSEQ,
        $CNSSEQ
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rsCount[] = $row['COUNT'];
            }
            if($rsCount[0] > 0){
                //構文
                $strSQL  = ' UPDATE BQRYCND ';
                $strSQL .= ' SET ';
                $strSQL .= ' CNDFMT = ?, ';
                $strSQL .= ' CNDSFL = ? , ';
                /**20161019**/
                $strSQL .= ' CNDFIN = ? , ';
                $strSQL .= ' CNDFPM = ? , ';
                $strSQL .= ' CNDFDY = ? , ';
                $strSQL .= ' CNDFFL = ? , ';
                $strSQL .= ' CNDTIN = ? , ';
                $strSQL .= ' CNDTPM = ? , ';
                $strSQL .= ' CNDTDY = ? , ';
                $strSQL .= ' CNDTFL = ? ,';
                $strSQL .= ' CNDSCH = ?  ';
                /**20161019**/
                $strSQL .= ' WHERE CNQRYN = ? ';
                $strSQL .= ' AND CNFILID = ? ';
                $strSQL .= ' AND CNMSEQ = ? ';
                $strSQL .= ' AND CNSSEQ = ? ';

                $params = array(
                    $FORMAT,
                    $SURASHU,
                    $CNDFIN,
                    $CNDFPM,
                    $CNDFDY,
                    $CNDFFL,
                    $CNDTIN,
                    $CNDTPM,
                    $CNDTDY,
                    $CNDTFL,
                    $CNDSCH,
                    $D3NAME,
                    $D3FILID,
                    $CNMSEQ,
                    $CNSSEQ
                );
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = 'FAIL_INS';
                }else{
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $rs = 'FAIL_INS';
                    }
                }
            }
            else{
                $rs = 'NOTEXIST_UPD';
            }
            
        }
    }
    return $rs;

}

/**
* BSQLCNDテーブル更新
* 
* @param Object  $db2con    DBコネクション
* @param String  $FORMAT    フォーマット
* @param String  $SURASHU   スラッシュつけるつけない
* @param String  $D3NAME    D3NAME
* @param String  $D3FILID   D3FILID
* @param String  $D3JSEQ    D3JSEQ
* 
*/
function fnUpdateBSQLCND($db2con,$FORMAT,$SURASHU,$D3NAME,$D3FILID,$D3JSEQ,$CNDFIN,$CNDFPM,$CNDFDY,$CNDFFL,$CNDTIN,$CNDTPM,$CNDTDY,$CNDTFL,$CNDSCH){
    $rs = true;
    $TOTCOUNT=0;
    $rs = true;
    $data=fnBSQLCND_COUNT($db2con, $D3NAME,$D3JSEQ);
    if($data['result']!==true){
        $rs=$data['result'];
    }else{
        $TOTCOUNT=$data['COUNT'];
        if($TOTCOUNT>0){
            //構文
            $strSQL  = ' UPDATE BSQLCND ';
            $strSQL .= ' SET ';
            $strSQL .= ' CNDFMT = ?, ';
            $strSQL .= ' CNDSFL = ? , ';
            /**20161019**/
            $strSQL .= ' CNDFIN = ? , ';
            $strSQL .= ' CNDFPM = ? , ';
            $strSQL .= ' CNDFDY = ? , ';
            $strSQL .= ' CNDFFL = ? , ';
            $strSQL .= ' CNDTIN = ? , ';
            $strSQL .= ' CNDTPM = ? , ';
            $strSQL .= ' CNDTDY = ? , ';
            $strSQL .= ' CNDTFL = ? ,';
            $strSQL .= ' CNDSCH = ?  ';
            /**20161019**/
            $strSQL .= ' WHERE CNQRYN = ? ';
            $strSQL .= ' AND CNDSEQ = ? ';

            $params = array(
                $FORMAT,
                $SURASHU,
                $CNDFIN,
                $CNDFPM,
                $CNDFDY,
                $CNDFFL,
                $CNDTIN,
                $CNDTPM,
                $CNDTDY,
                $CNDTFL,
                $CNDSCH,
                $D3NAME,
                $D3JSEQ
            );
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $rs = 'FAIL_INS';
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $rs = 'FAIL_INS';
                }
            }

        }else{
            $rs = 'NOTEXIST_UPD';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* ヘルプ情報更新
*-------------------------------------------------------*
*/

function cmChkCnd( $db2con, $D3NAME, $D3FILID, $D3JSEQ, $D3CNT, $D1WEBF,$D3FLD){
    $data = array();
    $strSQL = '';

    if ( $D1WEBF === '1' ) {

        $D3JSEQ = explode('-',$D3JSEQ);
        $CNMSEQ = $D3JSEQ[0];
        $CNSSEQ = $D3JSEQ[1];

        $strSQL .=  '   SELECT ';
        $strSQL .=  '       A.CNQRYN , ';
        $strSQL .=  '       A.CNFILID , ';
        $strSQL .=  '       A.CNMSEQ , ';
        $strSQL .=  '       A.CNSSEQ , ';
        $strSQL .=  '       A.CNAOKB , ';
        $strSQL .=  '       A.CNFLDN , ';
        $strSQL .=  '       A.CNCKBN , ';
        $strSQL .=  '       A.CNSTKB , ';
        $strSQL .=  '       A.CNCANL , ';
        $strSQL .=  '       A.CNCANF , ';
        $strSQL .=  '       A.CNCANC , ';
        $strSQL .=  '       A.CNCANG , ';
        $strSQL .=  '       A.CNNAMG , ';
        $strSQL .=  '       A.CNNAMC , ';
        $strSQL .=  '       A.CNDFMT , ';
        $strSQL .=  '       A.CNDSFL , ';
        $strSQL .=  '       A.CNDFIN , ';
        $strSQL .=  '       A.CNDFPM , ';
        $strSQL .=  '       A.CNDFDY , ';
        $strSQL .=  '       A.CNDFFL , ';
        $strSQL .=  '       A.CNDTIN , ';
        $strSQL .=  '       A.CNDTPM , ';
        $strSQL .=  '       A.CNDTDY , ';
        $strSQL .=  '       A.CNDTFL , ';
        $strSQL .=  '       A.CNCAST , ';
        $strSQL .=  '       A.CNNAST ';
        $strSQL .=  '   FROM ';
        $strSQL .=  '       BQRYCND A ';
        $strSQL .=  '   WHERE '; 
        $strSQL .=  '       A.CNQRYN  = ? ';
        $strSQL .=  '   AND A.CNFILID = ? ';
        $strSQL .=  '   AND A.CNMSEQ  = ? ';
        $strSQL .=  '   AND A.CNSSEQ  = ? ';
        $strSQL .=  '   AND A.CNFLDN  = ? ';

        $params = array(
                    $D3NAME ,
                    $D3FILID ,
                    $CNMSEQ ,
                    $CNSSEQ ,
                    $D3FLD
                );
    } else {
        $strSQL .=  '   SELECT ';
        $strSQL .=  '       A.D3NAME , ';
        $strSQL .=  '       A.D3FILID , ';
        $strSQL .=  '       A.D3JSEQ , ';
        $strSQL .=  '       A.D3CNT , ';
        $strSQL .=  '       A.D3ANDOR , ';
        $strSQL .=  '       A.D3FLD , ';
        $strSQL .=  '       A.D3CND , ';
        $strSQL .=  '       A.D3DAT , ';
        $strSQL .=  '       A.D3USEL , ';
        $strSQL .=  '       A.D3TYPE , ';
        $strSQL .=  '       A.D3LEN , ';
        $strSQL .=  '       A.D3CANL , ';
        $strSQL .=  '       A.D3CANF , ';
        $strSQL .=  '       A.D3CANC , ';
        $strSQL .=  '       A.D3CANG , ';
        $strSQL .=  '       A.D3NAMG , ';
        $strSQL .=  '       A.D3NAMC , ';
        $strSQL .=  '       A.D3DFMT , ';
        $strSQL .=  '       A.D3DSFL , ';
        $strSQL .=  '       A.D3DFIN , ';
        $strSQL .=  '       A.D3DFPM , ';
        $strSQL .=  '       A.D3DFDY , ';
        $strSQL .=  '       A.D3DFFL , ';
        $strSQL .=  '       A.D3DTIN , ';
        $strSQL .=  '       A.D3DTPM , ';
        $strSQL .=  '       A.D3DTDY , ';
        $strSQL .=  '       A.D3DTFL , ';
        $strSQL .=  '       A.D3CAST , ';
        $strSQL .=  '       A.D3NAST '; 
        $strSQL .=  '   FROM  ';
        $strSQL .=  '       FDB2CSV3 A ';
        $strSQL .=  '   WHERE  ';
        $strSQL .=  '       A.D3NAME = ? ';
        $strSQL .=  '   AND A.D3FILID = ? ';
        $strSQL .=  '   AND A.D3JSEQ = ? ';
        $strSQL .=  '   AND A.D3FLD = ? ';

        $params = array(
            $D3NAME ,
            $D3FILID ,
            $D3JSEQ ,
            $D3FLD
        );
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $data = array('result' => true,'data' => $data);
            }else{
                $data = array('result' => 'NOTEXIST_GET');
            }
        }
    }
    return $data;
}
/*
*SQLクエリーのためチェック
*/
function cmChkBSQLCND( $db2con, $D3NAME,$D3JSEQ){
    $data = array();
    $rs=true;
    $TOTCOUNT=0;
    $data=fnBSQLCND_COUNT($db2con, $D3NAME,$D3JSEQ);
    if($data['result']!==true){
        $rs=$data['result'];
    }else{
        $TOTCOUNT=$data['COUNT'];
        if($TOTCOUNT<=0){
           $rs= 'NOTEXIST_GET';
        }
    }
    return $rs;
}
/*
*SQLクエリーのためチェック
*/
function fnBSQLCND_COUNT($db2con, $D3NAME,$D3JSEQ){
    $data = array();
    $rs=true;
    $TOTCOUNT=0;
    $strSQL = '';
    $strSQL .=  'SELECT COUNT(*) AS TOTCOUNT ';
    $strSQL .=  'FROM BSQLCND AS A WHERE ';
    $strSQL .=  'A.CNQRYN  = ? ';
    $strSQL .=  'AND A.CNDSEQ = ? ';

    $params = array($D3NAME,$D3JSEQ);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs= 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs= 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $TOTCOUNT = $row['TOTCOUNT'];
            }
        }
    }
    $data=array('result'=>$rs,'COUNT'=>$TOTCOUNT);
    return $data;
}
