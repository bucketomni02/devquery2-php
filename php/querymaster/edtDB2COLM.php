<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
/*
 *-------------------------------------------------------*
 * リクエスト
 *-------------------------------------------------------*
*/
$DCNAME = $_POST['DCNAME'];
$DATA = json_decode($_POST['DATA'], true);
$SUMFLG = $_POST['SUMFLG'];
$LVLCONFLG = $_POST['LVLCONFLG'];
$LVLFLG = $_POST['LVLFLG'];

/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
/*
 *-------------------------------------------------------*
 * チェック処理
 *-------------------------------------------------------*
*/
//htmldecode
$DCNAME = cmHscDe($DCNAME);
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    } else {
        $userData = umEx($rs['data']);
        if ($userData[0]['WUAUTH'] === '2') {
            $rs = cmChkKenGen($db2con, '14', $userData[0]['WUSAUT']); //'14' =>制御レベル設定
            if ($rs['result'] !== true) {
                $rtn = 3;
                $msg = showMsg($rs['result'], array('制御レベル設定の権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if ($LVLFLG !== '1') {
    if ($LVLCONFLG !== '1') {
        if ($rtn === 0) {
            $chkQry = cmChkQuery($db2con, '', $DCNAME, '');
            if ($chkQry['result'] !== true) {
                $rtn = 3;
                $msg = showMsg($chkQry['result'], array('クエリー'));
            }
        }
        if ($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4') {
            if ($rtn === 0) {
                $chkQryUsr = chkVldQryUsr($db2con, $DCNAME, $userData[0]['WUAUTH']);
                if ($chkQryUsr === 'NOTEXIST_GET') {
                    $rtn = 3;
                    //$msg = showMsg('ログインユーザーに指定したクエリーに対しての制御レベル設定の権限がありません。');
                    $msg = showMsg('FAIL_QRY_USR', array('制御レベル設定の権限'));
                } else if ($chkQryUsr !== true) {
                    $rtn = 1;
                    $msg = showMsg($chkQryUsr['result'], array('クエリー'));
                }
            }
        }
    }
}
//制御するレベルが設定されてない場合
if ($DATA !== '') {
   /* if ($rtn === 0) {
        if ($DATA['LABEL1'] === '' && $DATA['LABEL2'] === '' && $DATA['LABEL3'] === '' && $DATA['LABEL4'] === '' && $DATA['LABEL5'] === '' && $DATA['LABEL6'] === '') {
            $rtn = 1;
            $msg = showMsg('FAIL_SET', array('制御レベル'));
        }
    }  */
    //同じ制御レベルが選ばれた場合
    /*if ($rtn === 0) {
        for ($x = 1;$x <= 6;$x+= 1) {
            $cData = $DATA['LABEL' . $x];
            if ($cData !== '' && $DATA['LABEL' . $x] !== null) {
                for ($y = 1;$y <= 6;$y+= 1) {
                    if ($y !== $x) {
                        if ($cData === $DATA['LABEL' . $y]) {
                            $rtn = 1;
                            $msg = showMsg('FAIL_SET', array(array('違う', '制御レベル')));
                            break;
                        }
                    }
                }
            }
        }
    }*/

  if($rtn === 0){
    for ($y = 1;$y <= 6;$y++) {
            $ {"nVar" . $y} = array();//$DATA
          
            $ {"nVar" . $y} = $DATA['TEXTBOX'.$y];
             if($ {"nVar" . $y} != ''){
                if($DATA['LABEL'.$y] == ''){
		          $rtn = 1;
		          $msg = showMsg('FAIL_SET',array(array('レベル',$y)));
                  break;
                }
		       
		     }
        }
  }




  if($rtn === 0){
    for ($y = 1;$y <= 6;$y++) {
            $ {"nVar" . $y} = array();//$DATA
          
            $ {"nVar" . $y} = $DATA['TEXTBOX'.$y];
             if(!checkMaxLen($ {"nVar" . $y},90)){
		         $rtn = 1;
		         $msg = showMsg('FAIL_MAXLEN',array(array('ラベル手入力',$y)));
		       
		     }
        }
  }




	//同じ制御レベルが選ばれた場合
	$checkParamArr=array();
    if ($rtn === 0) {
        for ($x = 1;$x <= 6;$x+= 1) {
            $cData = $DATA['LABEL' . $x];
			if ($cData !== '' && $DATA['LABEL' . $x] !== null) {
				$cDataExp=explode(",",$cData);
				if(count($cDataExp)>=2){
					for($j=0;$j<count($cDataExp);$j++){
						$checkParamArr[]=$cDataExp[$j];
					}
				}else{
					$checkParamArr[]=$cData;
				}
			}
        }
    }
	if ($rtn === 0) {
		if(count($checkParamArr)>0){
			if(count(array_count_values($checkParamArr)) !==count($checkParamArr)){
                $rtn = 1;
                $msg = showMsg('FAIL_SET', array(array('違う', '制御レベル')));
			}
			
		}
		
	}
    if ($LVLFLG !== '1') {
		if ($rtn === 0) {
	        for ($x = 1;$x <= 6;$x++) {
	            $LGroup = explode(",", $DATA['LABEL' . $x]);
	            if (count($LGroup) >= 2) { //グループのレベル
	                $ {"exp" . $x} = explode("-", $LGroup[0]);
	                $ {"tmp" . $x} = array(
						'FILID' => ($ {"exp" . $x}[0] !== '') ? $ {"exp" . $x}[0] : 0,
						'FLD' => $DATA['LABEL' . $x]);
	            } else { //普通のレベル
	                $ {"exp" . $x} = explode("-", $DATA['LABEL' . $x]);
	                $ {"tmp" . $x} = array(
					'FILID' => ($ {"exp" . $x}[0] !== '') ? $ {"exp" . $x}[0] : 0,
					'FLD' => ($ {"exp" . $x}[1] !== null) ? $ {"exp" . $x}[1] : '');
	            }
	        }
		}
        if ($rtn === 0) {
            for ($x = 1;$x <= 6;$x++) {
                if ($ {"tmp" . $x}['FLD'] !== '') {
                    //FDB2CSV2に存在するかチェック(FILID=9999は無条件でtrue)
                    $groupField = explode(",", $ {"tmp" . $x}['FLD']);
                    if (count($groupField) >= 2) { //グループのレベル
                        for ($y = 0;$y < count($groupField);$y++) {
                            $expRes = explode("-", $groupField[$y]);
                            $filVal = cmMer($expRes[0]);
                            $fldVal = cmMer($expRes[1]);
                            $rs = cmColCheck($db2con, $DCNAME, $filVal, $fldVal);
                            if ($rs['result'] !== true) {
                                $rtn = 2;
                                $msg = showMsg($rs['result'], array('クエリー', 'カラム'));
                                break;
                            }
                        }
                    } else { //普通のレベル
                        $rs = cmColCheck($db2con, $DCNAME, cmMer($ {
                            "tmp" . $x
                        }
                        ['FILID']), cmMer($ {
                            "tmp" . $x
                        }
                        ['FLD']));
                        if ($rs['result'] !== true) {
                            $rtn = 2;
                            $msg = showMsg($rs['result'], array('クエリー', 'カラム'));
                            break;
                        }
                    }

                }
            }

       
        for ($y = 1;$y <= 6;$y++) {
                $ {"nVar" . $y} = array();//$DATA
                // if($DATA['TEXTBOX1'])
                $ {"nVar" . $y} = $DATA['TEXTBOX'.$y];
  
         }

        }
    
    }
    if ($LVLFLG === '1') {
        if ($rtn === 0) {
            $check = fnCheckDCNAME($db2con, $DCNAME);
            if ($check['result'] === 'FAIL_SEL') {
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }
    } else {
        if ($rtn === 0) {
            // 登録/更新前のチェック
            $check = fnCheckDCNAME($db2con, $DCNAME);
            if($nVar1 == ''){
                $nVar1 = '';
            }
            if($nVar2 == ''){
                $nVar2 = '';
            }
            if($nVar3 == ''){
                $nVar3 = '';
            }
            if($nVar4 == ''){
                $nVar4 = '';
            }
            if($nVar5 == ''){
                $nVar5 = '';
            }
            if($nVar6 == ''){
                $nVar6 = '';
            }
            if ($check['result'] === true) {
                //制御レベルテーブル更新
               
                $rs = fnUpdDB2COLM($db2con, $DCNAME, $tmp1, $tmp2, $tmp3, $tmp4, $tmp5, $tmp6,$nVar1,$nVar2,$nVar3,$nVar4,$nVar5,$nVar6,$SUMFLG);
                if ($rs !== true) {
                    $rtn = 1;
                    $msg = showMsg($rs);
                }
            } else if ($check['result'] === false) {
                //制御レベルテーブル登録
                $rs = fnInsDB2COLM($db2con, $DCNAME, $tmp1, $tmp2, $tmp3, $tmp4, $tmp5, $tmp6,$nVar1,$nVar2,$nVar3,$nVar4,$nVar5,$nVar6,$SUMFLG);
                if ($rs !== true) {
                    $rtn = 1;
                    $msg = showMsg($rs);
                }
            } else {
                //例外
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }
    }
}
cmDb2Close($db2con);
/**return**/
$rtnAry = array('RTN' => $rtn, 'MSG' => $msg, 'COLMDATA' => $DATA);
echo (json_encode($rtnAry));
/**
 * 制御レベルテーブル更新
 *
 * @param Object  $db2con    DBコネクション
 * @param String  $DCNAME    クエリーID
 * @param String  $tmp1      FILID+フィールド名
 * @param String  $tmp2      FILID+フィールド名
 * @param String  $tmp3      FILID+フィールド名
 * @param String  $tmp4      FILID+フィールド名
 * @param String  $tmp5      FILID+フィールド名
 * @param String  $tmp6      FILID+フィールド名
 *
 */
function fnUpdDB2COLM($db2con, $DCNAME, $tmp1, $tmp2, $tmp3, $tmp4, $tmp5, $tmp6,$nVar1,$nVar2,$nVar3,$nVar4,$nVar5,$nVar6,$SUMFLG) {
    error_log('NVARName**'.print_r($nVar1,true));
    $rs = true;
    //構文
    $strSQL = ' UPDATE DB2COLM ';
    $strSQL.= ' SET ';
    $strSQL.= ' DCFILID1 = ?, ';
    $strSQL.= ' DCFLD1 = ?, ';
    $strSQL.= ' DCFILID2 = ?, ';
    $strSQL.= ' DCFLD2 = ?, ';
    $strSQL.= ' DCFILID3 = ?, ';
    $strSQL.= ' DCFLD3 = ?, ';
    $strSQL.= ' DCFILID4 = ?, ';
    $strSQL.= ' DCFLD4 = ?, ';
    $strSQL.= ' DCFILID5 = ?, ';
    $strSQL.= ' DCFLD5 = ?, ';
    $strSQL.= ' DCFILID6 = ?, ';
    $strSQL.= ' DCFLD6 = ? ,';
    $strSQL.= ' DCSUMF = ?, ';
    $strSQL.= ' DCFNAME1 = ?, ';
    $strSQL.= ' DCFNAME2 = ?, ';
    $strSQL.= ' DCFNAME3 = ?, ';
    $strSQL.= ' DCFNAME4 = ?, ';
    $strSQL.= ' DCFNAME5 = ?, ';
    $strSQL.= ' DCFNAME6 = ? ';
    $strSQL.= ' WHERE DCNAME = ? ';
    $params = array($tmp1['FILID'], $tmp1['FLD'], $tmp2['FILID'], $tmp2['FLD'], $tmp3['FILID'], $tmp3['FLD'], $tmp4['FILID'], $tmp4['FLD'], $tmp5['FILID'], $tmp5['FLD'], $tmp6['FILID'], $tmp6['FLD'], $SUMFLG,$nVar1,$nVar2,$nVar3,$nVar4,$nVar5,$nVar6,$DCNAME);
    //error_log("DB2COLM upd:" . $strSQL . print_r($params, true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_UPD';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}
/**
 * 制御レベルテーブル登録
 *
 * @param Object  $db2con    DBコネクション
 * @param String  $DCNAME    クエリーID
 * @param String  $tmp1      FILID+フィールド名
 * @param String  $tmp2      FILID+フィールド名
 * @param String  $tmp3      FILID+フィールド名
 * @param String  $tmp4      FILID+フィールド名
 * @param String  $tmp5      FILID+フィールド名  
 * @param String  $tmp6      FILID+フィールド名
 *
 */
function fnInsDB2COLM($db2con, $DCNAME, $tmp1, $tmp2, $tmp3, $tmp4, $tmp5, $tmp6,$nVar1,$nVar2,$nVar3,$nVar4,$nVar5,$nVar6,$SUMFLG) {
    $rs = true;
     error_log('NVARName11**'.print_r($nVar1,true));
    //構文
    $strSQL = ' INSERT INTO DB2COLM ';
    $strSQL.= ' (DCNAME,DCFILID1,DCFLD1,DCFILID2,DCFLD2,DCFILID3,DCFLD3,DCFILID4,DCFLD4,DCFILID5,DCFLD5,DCFILID6,DCFLD6,DCSUMF,DCFNAME1,DCFNAME2,DCFNAME3,DCFNAME4,DCFNAME5,DCFNAME6) ';
    $strSQL.= ' VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    $params = array($DCNAME, $tmp1['FILID'], $tmp1['FLD'], $tmp2['FILID'], $tmp2['FLD'], $tmp3['FILID'], $tmp3['FLD'], $tmp4['FILID'], $tmp4['FLD'], $tmp5['FILID'], $tmp5['FLD'], $tmp6['FILID'], $tmp6['FLD'], $SUMFLG,$nVar1,$nVar2,$nVar3,$nVar4,$nVar5,$nVar6);
    $stmt = db2_prepare($db2con, $strSQL);
    //error_log("DB2COLM ins:" . $strSQL . print_r($params, true));
    if ($stmt === false) {
        $rs = 'FAIL_INS';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}
/**
 * 登録/更新前のチェック
 *
 * RESULT
 *    01：データ件数
 *    02：false
 *
 * @param Object  $db2con DBコネクション
 * @param String  $DCNAME クエリーID
 *
 */
function fnCheckDCNAME($db2con, $DCNAME) {
    $data = array();
    $strSQL = ' SELECT A.* ';
    $strSQL.= ' FROM DB2COLM AS A ';
    $strSQL.= ' WHERE A.DCNAME = ? ';
    $params = array($DCNAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $data = array('result' => false);
            } else {
                $data = array('result' => true);
            }
        }
    }
    return $data;
}
