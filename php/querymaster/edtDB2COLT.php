<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$DTNAME = $_POST['DTNAME'];
$DATA = json_decode($_POST['DATA'],true);
$LVLCONFLG = $_POST['LVLCONFLG'];
$LVLFLG = $_POST['LVLFLG'];
/*
*-------------------------------------------------------*
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$datacount = 0;
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$DTNAME = cmHscDe($DTNAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'14',$userData[0]['WUSAUT']);//'14' =>制御レベル設定
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('制御レベル設定の権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($LVLFLG !== '1' ){
  if($LVLCONFLG !== '1'){
    if($rtn === 0){
        $chkQry = cmChkQuery ( $db2con,'' , $DTNAME, '');
        if( $chkQry['result'] !== true ) {
            $rtn = 3;
            $msg = showMsg($chkQry['result'],array('クエリー'));
        }
     }
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$DTNAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての制御レベル設定の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('制御レベル設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
      }
   }
  }
}
if($rtn === 0){
    //集計コンボがブランクになってチェックボックスがチェックされている場合
    $comboFlg = false;
    for ($x = 0; $x < count($DATA); $x += 7) {
       if($DATA[$x+1] !== '' && $DATA[$x+1] !== null){
            $comboFlg = true;
       }else{
            if($DATA[$x+2]  === '1' 
                || $DATA[$x+3]  === '1' 
                || $DATA[$x+4]  === '1' 
                || $DATA[$x+5]  === '1' 
                || $DATA[$x+6]  === '1' ){
                $rtn  = 1;
                $msg = showMsg('FAIL_SET',array(array('集計','項目')));
                break;
            }

        }
    }        
    if($rtn === 0){
        if($comboFlg === false){
            $rtn = 1;
            $msg = showMsg('FAIL_SET',array(array('集計する','フィールド')));
        }
    }
}
if($rtn === 0){
    //同じコンボデータが選ばれた場合
    for ($i = 1; $i < count($DATA) ; $i += 7) { 
        for($j = ($i + 7) ;$j < count($DATA) ; $j += 7){
            if( $DATA[$i] !== null && $DATA[$i] !== '' && $DATA[$j] !== null && $DATA[$j] !== ''){
                if($DATA[$i] === $DATA[$j]){
                    $rtn  = 1;
                    $msg = showMsg('FAIL_SET',array(array('違う','集計項目')));
                    break;
                }
            }
        }
        if($rtn === 1){
            break;
        }
    }
}

if($rtn === 0){
    //チェックボックスがチェックされてないで集計コンボにデータがある場合
    for ($x = 0; $x < count($DATA); $x += 7) {
        if($DATA[$x+1] !== '' && $DATA[$x+1] !== null){
            if(($DATA[$x+2]  === '0' ||  $DATA[$x+2]  === '')
                && ($DATA[$x+3]  === '0' || $DATA[$x+3]  === '') 
                && $DATA[$x+4]  === '0' 
                && $DATA[$x+5]  === '0' 
                && $DATA[$x+6]  === '0' ){
                    $rtn  = 1;
                    $msg = showMsg('FAIL_SET',array('集計方法'));
                    break;
            }
        }
    }
}

if($rtn === 0){
    for ($x = 0; $x < count($DATA); $x += 7) {
        $exp1 =  explode("-",$DATA[$x + 1]);
        $tmp1 = array(
            'FILID' => ($exp1[0] !== '')?$exp1[0]:0,
            'FLD' => ($exp1[1] !== null)?$exp1[1]:''
        );
        if($tmp1['FILID'] !== 0 && $tmp1['FLD'] !== ''){

            //FDB2CSV2に存在するかチェック(FILID=9999は無条件でtrue)
        if($LVLFLG !== '1' ){
            $rs = cmColCheck($db2con,$DTNAME,cmMer($tmp1['FILID']),cmMer($tmp1['FLD']));
            if($rs ['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs ['result'],array('クエリー','カラム'));
                break;
            }
          }
        }
    }
  }



/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/
if($LVLFLG === '1' ){
    if($rtn === 0){
        $check = fnCheckDTNAME($db2con,$DTNAME);
        if($check['result'] === 'FAIL_SEL'){
             $rtn = 1;
             $msg = showMsg($rs);
        }
    }
}else{
    if($rtn === 0){
    //登録/更新前のチェック
    $check = fnCheckDTNAME($db2con,$DTNAME);

    if($check['result'] === true){ 
        //更新前データの削除
        $rs = fnDelDTNAME($db2con,$DTNAME);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
        else{
            for ($x = 0; $x < count($DATA); $x += 7) {
                //集計テーブル登録
                $rs = fnInsDB2COLT($db2con,$DTNAME,$DATA[$x],$DATA[$x+1],$DATA[$x+2],$DATA[$x+3],$DATA[$x+4],$DATA[$x+5],$DATA[$x+6]);
                if($rs !== true){
                    $rtn = 1;
                    $msg = showMsg($rs);
                }             
            }
        }
    }
    else if($check['result'] === false){

        for ($x = 0; $x < count($DATA); $x+=7) {
            //集計テーブル登録
            $rs = fnInsDB2COLT($db2con,$DTNAME,$DATA[$x],$DATA[$x+1],$DATA[$x+2],$DATA[$x+3],$DATA[$x+4],$DATA[$x+5],$DATA[$x+6]);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }             
        }

    }
    else{
        $rtn = 1;
        $msg = showMsg($check);
    }
  }
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'COLTDATA' => $DATA
);

echo(json_encode($rtnAry));

/**
* 更新前データの削除
* 
* @param Object  $db2con    DBコネクション
* @param String  $DTNAME    クエリーID
* 
*/

function fnDelDTNAME($db2con,$DTNAME){

    $rs = true;
    //構文
    $strSQL  = ' DELETE ';
    $strSQL .= ' FROM ';
    $strSQL .= ' DB2COLT ';
    $strSQL .= ' WHERE DTNAME = ? ';

    $params = array(
        $DTNAME
    );
    $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_DEL';
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                 $rs = 'FAIL_DEL';
            }
        }
    
    return $rs;

}

/**
* 集計テーブル登録
* 
* @param Object  $db2con    DBコネクション
* @param String  $DTNAME    クエリーID
* @param String  $DTSEQ     DTNAME SEQ
* @param String  $DTF       FILID + FLD
* @param String  $DTSUM     合計
* @param String  $DTAVLG    平均
* @param String  $DTMINV    最小
* @param String  $DTMAXV    最大
* @param String  $DTCONT    カウント
* 
*/

function fnInsDB2COLT($db2con,$DTNAME,$DTSEQ,$DTF,$DTSUM,$DTAVLG,$DTMINV,$DTMAXV,$DTCONT){

    $rs = true;
    $exp1 =  explode("-",$DTF);
    $tmp1 = array(
        'FILID' => ($exp1[0] !== '')?$exp1[0]:0,
        'FLD' => ($exp1[1] !== null)?$exp1[1]:''
    );

    //構文
    $strSQL  = ' INSERT INTO DB2COLT ';
    $strSQL .= ' (DTNAME,DTSEQ,DTFILID,DTFLD,DTSUM,DTAVLG,DTMINV,DTMAXV,DTCONT) ';
    $strSQL .= ' VALUES (?,?,?,?,?,?,?,?,?)';

    $params = array(
        $DTNAME,
        $DTSEQ,
        $tmp1['FILID'],
        $tmp1['FLD'],
        $DTSUM,
        $DTAVLG,
        $DTMINV,
        $DTMAXV,
        $DTCONT
        
    );

    $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_INS';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                 $rs = 'FAIL_INS';
            }
         }

    return $rs;

}

/**
* 登録/更新前のチェック
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $DTNAME クエリーID
* 
*/

function fnCheckDTNAME($db2con,$DTNAME){

    $data = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2COLT AS A ' ;
    $strSQL .= ' WHERE A.DTNAME = ? ' ;

    $params = array(
        $DTNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
           $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => false); 
            }
            else{
               $data = array('result' => true);
            }
        }
    }
    return $data;

}