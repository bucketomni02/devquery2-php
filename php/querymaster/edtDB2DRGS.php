<?php
/*
 *-------------------------------------------------------* 
 * クエリー連携設定のクエリー名チェック
 *-------------------------------------------------------*
 */
$rtn = 0;
$msg = '';

include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../getQryCnd.php");
include_once("../getSQLCnd.php");
include_once("../licenseInfo.php");

$DRKMTN       = (isset($_POST['DRKMTN']) ? $_POST['DRKMTN'] : ''); //MAINQUERY
$DRDMTN       = (isset($_POST['DRDMTN']) ? $_POST['DRDMTN'] : ''); //SEARCHQUERY
$PROC         = (isset($_POST['PROC']) ? $_POST['PROC'] : '');
$SERACHKEYARR = (isset($_POST['SERACHKEYARR']) ? json_decode($_POST['SERACHKEYARR'], true) : '');
$DRKFID       = (isset($_POST['DRKFID']) ? $_POST['DRKFID'] : '');
$DRKFLID      = (isset($_POST['DRKFLID']) ? $_POST['DRKFLID'] : '');
$DRDFLG       = (isset($_POST['DRDFLG']) ? $_POST['DRDFLG'] : '');
$DRSFLG       = (isset($_POST['DRSFLG']) ? $_POST['DRSFLG'] : '');
$db2con       = cmDb2Con();
cmSetPHPQUERY($db2con);
$rtn    = 0;
$msg    = '';
$D1CFLG = '';
//htmldecode
$DRDMTN = cmHscDe($DRDMTN); //定義名
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    } else {
        $userData = umEx($rs['data']);
        if ($userData[0]['WUAUTH'] === '2') {
            $rs = cmChkKenGen($db2con, '10', $userData[0]['WUSAUT']); //'10' => 詳細情報設定権限
            if ($rs['result'] !== true) {
                $rtn = 2;
                $msg = showMsg($rs['result'], array(
                    '詳細情報設定の権限'
                ));
            }
        }
    }
}
//定義が削除されたかどうかチェック
if ($rtn === 0) {
    $chkQry = cmChkQuery($db2con, '', $DRKMTN, '');
    if ($chkQry['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($chkQry['result'], array(
            '定義名'
        ));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$DRKMTN,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての詳細情報設定の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('制御レベル設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if ($rtn === 0) {
    $rs = cmCountDB2WDFL($db2con, $DRKMTN, $DRKFLID, $DRKFID); //詳細データあるかどうかのチェック
    if ($rs['result'] !== true) {
        $msg = showMsg($rs['result']);
        $rtn = 1;
    } else {
        $COUNT = $rs['COUNT'];
        if ($COUNT > 0) {
            $msg = showMsg('DRILL_FAIL');
            $rtn = 3;
        }
    }
}
if ($PROC !== 'D') { //削除フラグじゃなかったら、チェックする
    if ($rtn === 0) {
        if ($DRDMTN === '') {
            $rtn = 1;
            $msg = showMsg('FAIL_REQ', array(
                '定義名'
            ));
        }
    }
    if ($rtn === 0) {
        $byte = checkByte($DRDMTN);
        if ($byte[1] > 0) {
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE', array(
                '定義名'
            ));
        }
    }
    
    if ($rtn === 0) {
        if (!checkMaxLen($DRDMTN, 10)) {
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN', array(
                '定義名'
            ));
        }
    }
    //クエリー連携クエリー定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
    if ($rtn === 0) {
        //            $chkQry = cmChkQuery ( $db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'] , $DRDMTN, '');
        $chkQry = cmChkQuery($db2con, '', $DRDMTN, '');
        if ($chkQry['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($chkQry['result'], array(
                array(
                    'クエリー連携',
                    'クエリー'
                )
            ));
        }
    }
    if($rtn === 0){
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            if($rtn === 0){
                $chkQryUsr = chkVldQryUsr($db2con,$DRDMTN,$userData[0]['WUAUTH']);
                if($chkQryUsr === 'NOTEXIST_GET'){
                    $rtn = 1;
                    //$msg = showMsg('ログインユーザーに指定したクエリーに対してのクエリーの権限がありません。');
                    $msg = showMsg('FAIL_QRY_USR',array('詳細情報設定権限'));
                }else if($chkQryUsr !== true){
                    $rtn = 1;
                    $msg = showMsg($chkQryUsr['result'],array('クエリー'));
                }
            }
        }
    }
    if (count($SERACHKEYARR) === 0) {
        $rtn = 1;
        $msg = showMsg('FAIL_SET', array(
            '検索項目'
        ));
    }
}
    if ($rtn === 0) {
        $rs = fnGetD1TEXT($db2con, $DRKMTN);
        if ($rs['result'] !== true) {
            $msg = showMsg($rs['result'], array(
                '定義名'
            ));
            $rtn = 1;
        } else {
            $D1WEBK = $rs['data'][0]['D1WEBF'];
        }
    }
    
    if ($rtn === 0) {
        $rs = fnGetD1TEXT($db2con, $DRDMTN);
        if ($rs['result'] !== true) {
            $msg = showMsg($rs['result'], array(
                '定義名'
            ));
            $rtn = 1;
        } else {
            $D1WEBF = $rs['data'][0]['D1WEBF'];
            $D1CFLG = $rs['data'][0]['D1CFLG'];
        }
    }
    if ($licenseSql === false && $D1CFLG === '1') {
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET', array(
            array(
                'クエリー連携',
                'クエリー'
            )
        ));
    }
    
if ($PROC === 'U') {
    if ($rtn === 0) {
        if ($D1WEBF !== '1') { //黒い画面法の検索条件チェック
            $rs = fnGetAllCount($db2con, $DRDMTN, $SERACHKEYARR);
            if ($rs['result'] !== true) {
                $msg = showMsg($rs['result']);
                $rtn = 1;
            } else {
                $allcount = $rs['data'];
            }
        } else { //ウエブ法の検索条件チェック
            if($D1CFLG==='1'){
                $rs = fnBSQLCNDAllCount($db2con, $DRDMTN, $SERACHKEYARR);
                if ($rs['result']!== true) {
                    $msg = showMsg($rs[result]);
                    $rtn = 1;
                } else {
                    $allcount = $rs['COUNT'];
                }
            }else{
                $rs = fnWebGetAllCount($db2con, $DRDMTN, $SERACHKEYARR);
                if ($rs['result'] !== true) {
                    $msg = showMsg($rs['result']);
                    $rtn = 1;
                } else {
                    $allcount = $rs['data'];
                }
            }
        }
    }
    if ($rtn === 0) {
        if ($allcount === 0) {
            $rtn = 1;
            $msg = showMsg('FAIL_COL', array(
                array(
                    'クエリー連携',
                    'クエリー',
                    '検索項目'
                )
            ));
        }
    }
    if ($rtn === 0) {
        if ($D1WEBF === '1') {
            if($D1CFLG==='1'){
                $rs=fnBSQLHCND($db2con, $DRDMTN);
                if ($rs['result'] !== true) {
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                } else {
                    $datalist = $rs['data'];
                }
            }else{
                $rs = fnBQRYCNDDAT($db2con, $DRDMTN);
                if ($rs['result'] !== true) {
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                } else {
                    $data = $rs['data'];
                }
                $datalist = umEx($data);
                if (count($datalist) > 0) {
                    $datalist = fnCreateCndData($datalist);
                }

            }
        } else {
            $FDB2CSV3 = cmGetFDB2CSV3($db2con, $DRDMTN);
            if ($FDB2CSV3 === false) {
                $msg = showMsg('FAIL_SEL');
            } else {
                $datalist = $FDB2CSV3;
            }
        }
    }
    if ($rtn === 0) {
        if ($D1WEBF === '1') {
            $rs = cmChkWebHisu($datalist, $SERACHKEYARR);
            if ($rs !== true) {
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }
    }
    if ($rtn === 0) {
        $rs = fnDeleteDB2DRGS($db2con, $DRKMTN, $DRKFID, $DRKFLID);
        if ($rs !== true) {
            $msg = showMsg($rs);
            $rtn = 1;
        } else {
            $rs = fnInsertDB2DRGS($db2con, $DRKMTN, $DRKFID, $DRKFLID, $DRDMTN, $SERACHKEYARR, $D1WEBF, $D1WEBK, $DRDFLG,$D1CFLG,$DRSFLG);
            if ($rs !== true) {
                $msg = showMsg($rs);
                $rtn = 1;
            }
        }
    }
} else if ($PROC === 'D') {
    if ($rtn === 0) {
        $rs = fnDeleteDB2DRGS($db2con, $DRKMTN, $DRKFID, $DRKFLID);
        if ($rs !== true) {
            $msg = showMsg($rs);
            $rtn = 1;
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'DRDMTN' => $DRDMTN,
    'SERACHKEYARR' => $SERACHKEYARR,
    'DRKFID' => $DRKFID,
    'DRKFLID' => $DRKFLID,
    'DRKMTN' => $DRKMTN,
    'D1WEBF' => $D1WEBF,
    'data' => $data,
    'datalist' => $datalist
);

echo (json_encode($rtnAry));


/*
 *-------------------------------------------------------* 
 * 定義名取得
 *-------------------------------------------------------*
 */

function fnGetD1TEXT($db2con, $id)
{
    
    $data = array();
    
    $strSQL = ' SELECT A.D1TEXT ,A.D1WEBF,A.D1CFLG ';
    $strSQL .= ' FROM FDB2CSV1 AS A ';
    $strSQL .= ' WHERE D1NAME = ? ';
    
    $params = array(
        $id
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) > 0) {
                $data = array(
                    'result' => true,
                    'data' => umEx($data)
                );
            } else {
                $data = array(
                    'result' => 'NOTEXIST_GET'
                );
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------* 
 * 黒い画面全件カウント取得　
 *-------------------------------------------------------*
 */
function fnGetAllCount($db2con, $D3NAME, $SCHARR)
{
    $data = array();
    $flg  = dashContain($SCHARR['DRDJSQ']);
    if ($flg === false) {
        $strSQL = ' SELECT count(A.D3NAME) as COUNT ';
        $strSQL .= ' FROM FDB2CSV3 as A ';
        $strSQL .= ' WHERE D3NAME = ? ';
        $strSQL .= ' AND D3FILID = ? ';
        $strSQL .= ' AND D3JSEQ = ? ';
        $strSQL .= ' AND D3CNT = ? ';
        $strSQL .= ' AND D3FLD = ? ';
        $strSQL .= ' AND ( D3USEL = \'1\' ';
        $strSQL .= ' OR D3USEL = \'2\' ';
        //クエリー連携の場合
        $strSQL .= ' ) AND ( D3CND <> \'IS\'  AND D3CND<> \'RANGE\' ) ';
        
        $params = array(
            $D3NAME,
            $SCHARR['DRDFID'],
            $SCHARR['DRDJSQ'],
            $SCHARR['DRDCNT'],
            $SCHARR['DRDFNM']
        );
        $stmt   = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array(
                    'result' => 'FAIL_SEL'
                );
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row['COUNT'];
                }
                $data = array(
                    'result' => true,
                    'data' => $data[0]
                );
            }
        }
    } else {
        $data = array(
            'result' => true,
            'data' => 0
        );
    }
    return $data;
}
/*
 *-------------------------------------------------------* 
 * ウェブ全件カウント取得　
 *-------------------------------------------------------*
 */
function fnWebGetAllCount($db2con, $CNQRYN, $SCHARR)
{
    $data = array();
    $flg  = dashContain($SCHARR['DRDJSQ']);
    if ($flg === true) {
        $jsq    = explode("-", $SCHARR['DRDJSQ']);
        $CNMSEQ = $jsq[0];
        $CNSSEQ = $jsq[1];
        $strSQL = '';
        $strSQL .= '    SELECT count(A.CNQRYN) as COUNT ';
        $strSQL .= '    FROM BQRYCND as A ';
        $strSQL .= '    WHERE CNQRYN = ? ';
        $strSQL .= '    AND CNFILID = ? ';
        $strSQL .= '    AND CNMSEQ = ? ';
        $strSQL .= '    AND CNSSEQ = ? ';
        $strSQL .= '    AND CNFLDN = ? ';
        $strSQL .= '    AND ( CNSTKB = \'1\' ';
        $strSQL .= '    OR CNSTKB = \'2\' ';
        //クエリー連携の場合
        $strSQL .= ' ) AND ( CNCKBN <> \'IS\'  AND  CNCKBN <> \'RANGE\' ) ';
        $params = array(
            $CNQRYN,
            $SCHARR['DRDFID'],
            $CNMSEQ,
            $CNSSEQ,
            $SCHARR['DRDFNM']
        );
        $stmt   = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array(
                    'result' => 'FAIL_SEL'
                );
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row['COUNT'];
                }
                $data = array(
                    'result' => true,
                    'data' => $data[0]
                );
            }
        }
    } else {
        $data = array(
            'result' => true,
            'data' => 0
        );
    }
    return $data;
}
/*
 *-------------------------------------------------------* 
 * ウェブ全件カウント取得　
 *-------------------------------------------------------*
 */
function fnBSQLCNDAllCount($db2con, $CNQRYN, $SCHARR)
{
    $totCount=0;
    $rs=true;
    $data=array();
    $strSQL='';
    $strSQL .= '    SELECT count(A.CNQRYN) as COUNT ';
    $strSQL .= '    FROM BSQLCND as A ';
    $strSQL .= '    WHERE A.CNQRYN = ? ';
    $strSQL .= '    AND CNDSEQ = ? ';
    $strSQL .= '    AND CNDSTKB = \'1\' ';
    $params = array(
        $CNQRYN,
        $SCHARR['DRDJSQ']
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs='FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs='FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $totCount= $row['COUNT'];
            }
        }
    }
    return array('result'=>$rs,'COUNT'=>$totCount);
}
function dashContain($val)
{
    if (strpos($val, '-') !== false) {
        return true;
    } else {
        return false;
    }
}

//元のデータを削除
function fnDeleteDB2DRGS($db2con, $DRKMTN, $DRKFID, $DRKFLID)
{
    $rs     = true;
    //構文
    $strSQL = ' DELETE FROM DB2DRGS WHERE ';
    $strSQL .= ' DRKMTN = ?  AND DRKFID = ?  AND DRKFLID = ? ';
    $params = array(
        $DRKMTN,
        $DRKFID,
        $DRKFLID
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_DEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}

/*新しいデータを登録する
*@$DRSFLG:検索条件画面を挟む
***/
function fnInsertDB2DRGS($db2con, $DRKMTN, $DRKFID, $DRKFLID, $DRDMTN, $SCH, $D1WEBF, $D1WEBK, $DRDFLG,$D1CFLG,$DRSFLG)
{
    $rs     = true;
    //構文
    $strSQL = ' INSERT INTO DB2DRGS  ';
    $strSQL .='(';
    $strSQL .='DRKMTN,';
    $strSQL .='DRKFID,';
    $strSQL .='DRKFLID,';
    $strSQL .='DRDMTN,';
    $strSQL .='DRDFNM,';
    $strSQL .='DRDFID,';
    $strSQL .='DRDJSQ,';
    $strSQL .='DRDCNT,';
    $strSQL .='DRWEBF,';
    $strSQL .='DRWEBK,';
    $strSQL .='DRDFLG,';
    $strSQL .='DRSFLG';
    $strSQL .=')';
    $strSQL .='VALUES';
    $strSQL .='( ? , ? , ? ,? , ? , ? , ? ,? , ? , ? , ?, ?)';
    if ($D1WEBF !== '1') {
        $DRDJSQ = $SCH['DRDJSQ'];
        $DRDCNT = $SCH['DRDCNT'];
    } else {
        if($D1CFLG==='1'){
            $DRDJSQ=$SCH['DRDJSQ'];
            $DRDCNT=$SCH['DRDJSQ'];
        }else{
            $jsq    = explode("-", $SCH['DRDJSQ']);
            $DRDJSQ = $jsq[0];
            $DRDCNT = $jsq[1];
        }
    }
    $params = array(
        $DRKMTN,
        $DRKFID,
        $DRKFLID,
        $DRDMTN,
        $SCH['DRDFNM'],
        $SCH['DRDFID'],
        $DRDJSQ,
        $DRDCNT,
        $D1WEBF,
        $D1WEBK,
        $DRDFLG,
		$DRSFLG
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_DEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}