<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$d1name = (isset($_POST['D1NAME']))?$_POST['D1NAME']:'';
$ecflg = (isset($_POST['ECFLG']))?$_POST['ECFLG']:'';
$ecrown =(isset($_POST['ECROWN']))?$_POST['ECROWN']:'';
$d1edkb=(isset($_POST['D1EDKB']))?$_POST['D1EDKB']:'';
$phpqueryexcel = json_decode($_POST['PHPQUERYEXCEL'],true);

$D1SFLG=(isset($_POST['D1SFLG']))?$_POST['D1SFLG']:'';
$PROC  = (isset($_POST['PROC']))?$_POST['PROC']:'';
$phpqueryexcelRes=array();
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtnArray = array();        //リターン配列
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'9',$userData[0]['WUSAUT']);//'9' => EXCELテンプレート
            if($rs['result'] !== true){
                $rtn = 4;
                $msg =  showMsg($rs['result'],array('EXCELテンプレートの権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック
if ($rtn === 0) {
    $chkQry = cmChkQuery($db2con, '', $d1name, '');
    if ($chkQry['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($chkQry['result'], array(
            '定義名'
        ));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$d1name,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのEXCELテンプレートの権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('EXCELテンプレートの権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn===0){
    if($PROC==='ADD'){
        //バリデーションチェック
        if($rtn === 0){
            $ecrownblank =trim($ecrown);
            if($ecrownblank === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array(array('結果データ','の','出力行')));
                $focus = 'ECROWN';
            }
        }

        if($rtn === 0){
            $check = checkNum($ecrown);
            if($check === false){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('結果データ','数字'));
                $focus = 'ECROWN';
            }
        }

        if($rtn === 0){
            if(!checkMaxLen($ecrown,3)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('結果データ'));
                $focus = 'ECROWN';
            }
        }
        if($rtn===0){
            if($ecrown < 0){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('結果データ',array('0','以上の数値')));
                $focus = 'ECROWN';
            }
        }
    }
}
$dupcol=array();
$duprow=array();
$duprowcol=array();
$rowcol='';
if($rtn===0){
    foreach($phpqueryexcel as $key => $col){
        //DB2EINSの全部のチェック
        if($rtn===0){
            if( $col['EIROW']!=='' || $col['EITEXT']!=='' || $col['EISIZE']!=='' || $col['EICOLR']!=='' || $col['EIBOLD']!==''){
                //開始行
                if($rtn===0){
                    if(trim($col['EIROW']) === ''){
                        $rtn = 1;
                        $msg = showMsg('FAIL_REQ',array('開始行'));
                        break;
                    }
                }
                if($rtn===0){
                    if(trim($col['EIROW'])!==''){
                        $row=$col['EIROW'];
                        if(in_array($row,$duprow) && (int)$col['EIROW']>0){
                            $rtn = 1;
                             $msg = showMsg('CHK_DUP',array('開始行'));
                        }else{
                            //開始行半角チェック
                            $byte = checkByte($col['EIROW']);
                            if($rtn===0){
                                if($byte[1] > 0){
                                    $rtn = 1;
                                    $msg = showMsg('FAIL_BYTE',array('開始行'));
                                    $focus = 'EIROW';
                                    break;
                                }
                            }
                            //開始行の数値チェック
                            if($rtn===0){
                                if(!checkNum($col['EIROW'])){
                                    $rtn = 1;
                                    $msg = showMsg('CHK_FMT',array('開始行','数値'));
                                    $focus = 'EIROW';
                                    break;
                                }
                            }
                            //開始行ゼロチェック
                            if($rtn===0){
                                if($col['EIROW'] < 1){
                                    $rtn = 1;
                                    $msg = showMsg('CHK_FMT',array('開始行',array('1','以上の数値')));
                                    $focus = 'EIROW';
                                    break;
                                }else if($col['EIROW']>32768){
                                    $rtn = 1;
                                    $msg = showMsg('CHK_FMT',array('開始行',array('32768','以下の数値')));
                                    $focus = 'EIROW';
                                    break;
                                }
                            }
                            // 開始行入力桁数チェック
                            if($rtn===0){
                                if(!checkMaxLen($col['EIROW'],10)){
                                    $rtn = 1;
                                    $msg = showMsg('FAIL_MAXLEN',array('開始行'));
                                    $focus = 'EIROW';
                                    break;
                                }
                            }                            
                            if($rtn === 0){
                                $row=$col['EIROW'];
                                array_push($duprow,$row);
                            }
                        }
                    }
                }
                //挿入文字 $col['EITEXT']!=='' || $col['EISIZE']!==''
                if($rtn===0){
                    if(trim($col['EITEXT'])===''){
                        $rtn = 1;
                        $msg = showMsg('FAIL_REQ',array('挿入文字'));
                        break;
                    }
                }
                if($rtn===0){
                    if(!checkMaxLen($col['EITEXT'],256)){
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN',array('挿入文字'));
                        break;
                    }
                }
                //PX
                if(trim($col['EISIZE'])!==''){
                    if($rtn === 0){
                        $check = checkNum($col['EISIZE']);
                        if($check === false){
                            $rtn = 1;
                            $msg = showMsg('CHK_FMT',array('PX','数字'));
                            break;
                        }
                    }

                    if($rtn === 0){
                        if(!checkMaxLen($col['EISIZE'],3)){
                            $rtn = 1;
                            $msg = showMsg('FAIL_MAXLEN',array('PX'));
                            break;
                        }
                    }
                }
                /**
                *rowのデータがある「array」
                *
                */
                if($rtn===0){
                    array_push($phpqueryexcelRes,$col);
                }

            }
        }
        
        if ($rtn === 1){
            break;
        }
    }
}
//save
if ($rtn === 0){
    if($PROC==='DEL'){
        $data =fnDelDB2ECONDB2EINS($db2con,$d1name);
        $rs=$data['result'];
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }else{
            //delete template table
            $data=fnUpdateDefaultFDB2CSV1($db2con,$d1name,'',$D1SFLG,$PROC);
            $rs=$data['result'];
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }else{
                $files = glob(BASE_DIR."/php/template/".$d1name.".*");
                foreach ($files as $file) {
                    unlink($file);
                }
            }
        }
    }else if($PROC==='ADD'){
        $rs = fnUpdPHPQUERYEXCEL($db2con, $d1name,$d1edkb,$ecflg,$ecrown,$phpqueryexcelRes,$D1SFLG,$PROC);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}
cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnArray));

//save
function fnUpdPHPQUERYEXCEL($db2con, $d1name,$d1edkb,$ecflg,$ecrown,$phpqueryexcelRes,$D1SFLG,$PROC){
    $rs=true;
    $data=array();
    $strSQL  ='';
    $data =fnDelDB2ECONDB2EINS($db2con,$d1name);
    if($data['result'] === true){
        $strSQL  = ' INSERT INTO DB2ECON ';
        $strSQL .='(';
        $strSQL .='ECNAME,';
        $strSQL .='ECFLG,';
        $strSQL .='ECROWN';
        $strSQL .=')';
        $strSQL .= ' VALUES ( ?, ?, ? ) ' ;
        $stmt1 = db2_prepare($db2con,$strSQL);
        $params = array($d1name,$ecflg,$ecrown);
        if($stmt1 === false){
            $rs = 'FAIL_INS';
        }else{
            $data = db2_execute($stmt1,$params);
            $strSQL  = ' INSERT INTO DB2EINS ';
            $strSQL .='(';
            $strSQL .='EINAME,';
            $strSQL .='EISEQ,';
            $strSQL .='EICOL,';
            $strSQL .='EIROW,';
            $strSQL .='EITEXT,';
            $strSQL .='EISIZE,';
            $strSQL .='EICOLR,';
            $strSQL .='EIBOLD';
            $strSQL .=')';
            $strSQL .= ' VALUES ( ?, ?, ?, ?, ?, ?, ?, ? ) ' ;
            $stmt3 = db2_prepare($db2con,$strSQL);
            if($stmt3 === false){
                $rs = 'FAIL_INS';
            }else{
                $seqnumber = 0;
                foreach($phpqueryexcelRes as $col){
                    $params= array(
                        $d1name,
                        ++$seqnumber,
                        "",//column
                        $col['EIROW'],
                        $col['EITEXT'],
                        ($col['EISIZE']==='')?'0':$col['EISIZE'],
                        $col['EICOLR'],
                        $col['EIBOLD']
                    );
                    $r = db2_execute($stmt3,$params);
                    if($r === false){
                        $rs='FAIL_INS';
                        break;
                    }
                }
            }
            //delete template table
            if($rs===true){
                $data=fnUpdateDefaultFDB2CSV1($db2con, $d1name,$d1edkb,$D1SFLG,$PROC);
                $rs=$data['result'];
                if($rs === true){
                    $files = glob(BASE_DIR."/php/template/".$d1name.".*");
                    foreach ($files as $file) {
                        unlink($file);
                    }
                }
            }
        }
    }
    return $rs;
}
/*
*
*delete DB2ECON,DB2EINS
*/
function fnDelDB2ECONDB2EINS($db2con, $d1name){
    $data=array();
    $strSQL = '';
    $strSQL = ' DELETE FROM DB2ECON WHERE ECNAME = ? ';
    $params = array($d1name);
    $stmt = db2_prepare($db2con, $strSQL);
    if($stmt === false){
        $data=array('result' => 'FAIL_DEL');
    }else{
        $r =db2_execute($stmt,$params);
        if($r === false){
            $data=array('result' => 'FAIL_DEL');
        }else{
            $strSQL = ' DELETE FROM DB2EINS WHERE EINAME = ? ';
            $params = array($d1name);
            $stmt = db2_prepare($db2con, $strSQL);
            if($stmt===false){
                 $data=array('result' => 'FAIL_DEL');
            }else{
                $params = array($d1name);
                $r =db2_execute($stmt,$params);
                $data=($r===false)?array('result' => 'FAIL_DEL'):array('result'=>true);
            }
        }
    }
    return $data;
}
 
/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/
function fnUpdateDefaultFDB2CSV1($db2con,$d1name,$d1edkb,$D1SFLG,$PROC){
    $data = array();
    $D1TMPF='';//($PROC==='ADD')?'phpexcel':'';
    $params = array($d1edkb,0,'',0,'',$D1TMPF,$D1SFLG,$d1name);
    $strSQL = ' UPDATE FDB2CSV1 SET D1EDKB = ? ,D1SHET = ? ,D1TCOS = ? ,D1TROS = ? ,D1THFG = ? ,D1TMPF = ? ,D1SFLG = ? ' ;  
    $strSQL .= ' WHERE D1NAME = ? ';
	e_log("PPN TESTING:".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
			e_log("#############".db2_stmt_errormsg());
            $data = array('result' => 'FAIL_SEL');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}