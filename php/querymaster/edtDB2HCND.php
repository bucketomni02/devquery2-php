<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/
$PROC    = $_POST['PROC'];
$DHNAME  = $_POST['DHNAME'];
$DHFILID = $_POST['DHFILID'];
$DHMSEQ  = $_POST['DHMSEQ'];
$DHSSEQ  = $_POST['DHSSEQ'];
$D1WEBF  = $_POST['D1WEBF'];
$D3FLD   = $_POST['D3FLD'];
$DATA    = json_decode($_POST['DATA'],true);
$D1CFLG = (isset($_POST['D1CFLG'])) ?  $_POST['D1CFLG'] : '';

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$PROC    = cmHscDe($PROC);
$DHNAME  = cmHscDe($DHNAME);
$DHFILID = cmHscDe($DHFILID);
$DHMSEQ  = cmHscDe($DHMSEQ);
$DHSSEQ  = cmHscDe($DHSSEQ);
$D1WEBF  = cmHscDe($D1WEBF);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $chkUser = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($chkUser['result'] !== true){
        $rtn = 2;
        $msg = showMsg($chkUser['result'],array('ユーザー'));
    }else{
        $userData  = umEx($chkUser['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'13',$userData[0]['WUSAUT']);//'13' =>検索候補
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('検索候補の権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $DHNAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$DHNAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての検索候補の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('検索候補の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
//ヘルプつけたいの検索条件が削除されるかどうかチェック
if($rtn === 0){
    if($D1CFLG==='1'){
        $chkCnd=cmChkBSQLCND( $db2con, $DHNAME,$DHMSEQ);
        if( $chkCnd !== true ) {
            $rtn = 4;
            $msg = showMsg($chkCnd,array('クエリー'));
        }
    }else{
        $chkCnd = cmChkCnd ( $db2con, $DHNAME, $DHFILID, $DHMSEQ, $DHSSEQ, $D1WEBF,$D3FLD);
        if( $chkCnd['result'] !== true ) {
            $rtn = 4;
            $msg = showMsg($chkCnd['result'],array('検索条件'));
        }
    }
}
//バリデーションチェック

if($PROC === 'UPD'){
    if($rtn === 0){
        if($DATA['DHINFO'] === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('検索条件のヘルプ'));
            $focus = 'DHINFO';
        }
    }

    if($rtn === 0){
        if(!checkMaxLen($DATA['DHINFO'],1536)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('検索条件のヘルプ'));
            $focus = 'DHINFO';
        }
    }
}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/
if($rtn === 0){
    // 登録の場合
    if($PROC === 'UPD'){
        $rs = fnUpdateDB2HCND($db2con, $DHNAME, $DHFILID, $DHMSEQ, $DHSSEQ, $D1WEBF, $DATA);
        if ( $rs['result'] !== true ) {
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    } else if ( $PROC === 'DEL' ) {
    //登録解除の場合
        $rs = fnDeleteDB2HCND($db2con, $DHNAME, $DHFILID, $DHMSEQ, $DHSSEQ, $D1WEBF);
        if ( $rs['result'] !== true ) {
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ヘルプ情報更新
*-------------------------------------------------------*
*/

function cmChkCnd( $db2con, $DHNAME, $DHFILID, $DHMSEQ, $DHSSEQ, $D1WEBF,$D3FLD){
    $data = array();
    $strSQL = '';

    if ( $D1WEBF === '1' ) {
        $strSQL .=  '   SELECT ';
        $strSQL .=  '       A.CNQRYN , ';
        $strSQL .=  '       A.CNFILID , ';
        $strSQL .=  '       A.CNMSEQ , ';
        $strSQL .=  '       A.CNSSEQ , ';
        $strSQL .=  '       A.CNAOKB , ';
        $strSQL .=  '       A.CNFLDN , ';
        $strSQL .=  '       A.CNCKBN , ';
        $strSQL .=  '       A.CNSTKB , ';
        $strSQL .=  '       A.CNCANL , ';
        $strSQL .=  '       A.CNCANF , ';
        $strSQL .=  '       A.CNCANC , ';
        $strSQL .=  '       A.CNCANG , ';
        $strSQL .=  '       A.CNNAMG , ';
        $strSQL .=  '       A.CNNAMC , ';
        $strSQL .=  '       A.CNDFMT , ';
        $strSQL .=  '       A.CNDSFL , ';
        $strSQL .=  '       A.CNDFIN , ';
        $strSQL .=  '       A.CNDFPM , ';
        $strSQL .=  '       A.CNDFDY , ';
        $strSQL .=  '       A.CNDFFL , ';
        $strSQL .=  '       A.CNDTIN , ';
        $strSQL .=  '       A.CNDTPM , ';
        $strSQL .=  '       A.CNDTDY , ';
        $strSQL .=  '       A.CNDTFL , ';
        $strSQL .=  '       A.CNCAST , ';
        $strSQL .=  '       A.CNNAST ';
        $strSQL .=  '   FROM ';
        $strSQL .=  '       BQRYCND A ';
        $strSQL .=  '   WHERE '; 
        $strSQL .=  '       A.CNQRYN  = ? ';
        $strSQL .=  '   AND A.CNFILID = ? ';
        $strSQL .=  '   AND A.CNMSEQ  = ? ';
        $strSQL .=  '   AND A.CNSSEQ  = ? ';
        $strSQL .=  '   AND A.CNFLDN  = ? ';

        $params = array(
                    $DHNAME ,
                    $DHFILID ,
                    $DHMSEQ ,
                    $DHSSEQ ,
                    $D3FLD
                );
    } else {
        $strSQL .=  '   SELECT ';
        $strSQL .=  '       A.D3NAME , ';
        $strSQL .=  '       A.D3FILID , ';
        $strSQL .=  '       A.D3JSEQ , ';
        $strSQL .=  '       A.D3CNT , ';
        $strSQL .=  '       A.D3ANDOR , ';
        $strSQL .=  '       A.D3FLD , ';
        $strSQL .=  '       A.D3CND , ';
        $strSQL .=  '       A.D3DAT , ';
        $strSQL .=  '       A.D3USEL , ';
        $strSQL .=  '       A.D3TYPE , ';
        $strSQL .=  '       A.D3LEN , ';
        $strSQL .=  '       A.D3CANL , ';
        $strSQL .=  '       A.D3CANF , ';
        $strSQL .=  '       A.D3CANC , ';
        $strSQL .=  '       A.D3CANG , ';
        $strSQL .=  '       A.D3NAMG , ';
        $strSQL .=  '       A.D3NAMC , ';
        $strSQL .=  '       A.D3DFMT , ';
        $strSQL .=  '       A.D3DSFL , ';
        $strSQL .=  '       A.D3DFIN , ';
        $strSQL .=  '       A.D3DFPM , ';
        $strSQL .=  '       A.D3DFDY , ';
        $strSQL .=  '       A.D3DFFL , ';
        $strSQL .=  '       A.D3DTIN , ';
        $strSQL .=  '       A.D3DTPM , ';
        $strSQL .=  '       A.D3DTDY , ';
        $strSQL .=  '       A.D3DTFL , ';
        $strSQL .=  '       A.D3CAST , ';
        $strSQL .=  '       A.D3NAST '; 
        $strSQL .=  '   FROM  ';
        $strSQL .=  '       FDB2CSV3 A ';
        $strSQL .=  '   WHERE  ';
        $strSQL .=  '       A.D3NAME = ? ';
        $strSQL .=  '   AND A.D3FILID = ? ';
        $strSQL .=  '   AND A.D3JSEQ = ? ';
        $strSQL .=  '   AND A.D3FLD = ? ';

        $params = array(
            $DHNAME ,
            $DHFILID ,
            $DHSSEQ ,
            $D3FLD
        );
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $data = array('result' => true,'data' => $data);
            }else{
                $data = array('result' => 'NOTEXIST_GET');
            }
        }
    }
    return $data;
}

function fnUpdateDB2HCND($db2con, $DHNAME, $DHFILID, $DHMSEQ, $DHSSEQ, $D1WEBF, $DATA){
    $data = array();
    $strSQL = '';
    $rtn = 0;
    $msg = '';

    if ( $rtn === 0 ) {
        // DB2HCNDデータチェック
        $strSQL .= ' SELECT ';
        $strSQL .= '     DHNAME , ' ;
        $strSQL .= '     DHFILID , ' ;
        $strSQL .= '     DHMSEQ , ' ;
        $strSQL .= '     DHSSEQ , ' ;
        $strSQL .= '     DHINFO , ' ;
        $strSQL .= '     DHINFG ' ;
        $strSQL .= ' FROM DB2HCND'; 
        $strSQL .= ' WHERE DHNAME = ? ';
        $strSQL .= ' AND DHFILID = ? ';
        $strSQL .= ' AND DHMSEQ = ? ';
        $strSQL .= ' AND DHSSEQ = ? ';

        $params = array(
                $DHNAME ,
                $DHFILID ,
                $DHMSEQ ,
                $DHSSEQ
            );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true , 'CNDCNT' => count($data) );
            }
        }

        if ( $data['result'] !== true ){
            $rtn = 1;
        }
    }
    // ある場合、更新 ,ない場合、挿入
    if ( $rtn === 0 ){
        if( $data['CNDCNT'] === 0 ) {
            $strSQL = ' INSERT INTO  ';
            $strSQL .= '     DB2HCND ( ';
            $strSQL .= '     DHNAME , ' ;
            $strSQL .= '     DHFILID , ' ;
            $strSQL .= '     DHMSEQ , ' ;
            $strSQL .= '     DHSSEQ , ' ;
            $strSQL .= '     DHINFO , ' ;
            $strSQL .= '     DHINFG ' ;
            $strSQL .= '    ) ';
            $strSQL .= ' VALUES   ';
            $strSQL .= ' ( ?,?,?,?,?,? ) ';

            $params = array(
                    $DHNAME ,
                    $DHFILID ,
                    $DHMSEQ ,
                    $DHSSEQ ,
                    cmHscDe($DATA['DHINFO']) ,
                    cmHscDe($DATA['DHINFG'])
                );

        } else {
            $strSQL = ' UPDATE  ';
            $strSQL .= '     DB2HCND ';
            $strSQL .= ' SET ';
            $strSQL .= '    DHINFO = ? , ' ;
            $strSQL .= '    DHINFG = ? ' ;
            $strSQL .= ' WHERE DHNAME = ? ';
            $strSQL .= ' AND DHFILID = ? ';
            $strSQL .= ' AND DHMSEQ = ? ';
            $strSQL .= ' AND DHSSEQ = ? ';
            $params = array(
                    $DATA['DHINFO'] ,
                    $DATA['DHINFG'] ,
                    $DHNAME ,
                    $DHFILID ,
                    $DHMSEQ ,
                    $DHSSEQ 
                );
        }
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_INS');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_INS');
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;
}

function fnDeleteDB2HCND($db2con, $DHNAME, $DHFILID, $DHMSEQ, $DHSSEQ, $D1WEBF){
    $data = array();
    $strSQL = '';

    $strSQL .= ' DELETE ';
    $strSQL .= ' FROM DB2HCND ';
    $strSQL .= ' WHERE DHNAME = ? ';
    $strSQL .= ' AND DHFILID = ? ';
    $strSQL .= ' AND DHMSEQ = ? ';
    $strSQL .= ' AND DHSSEQ = ? ';
    $params = array(
            $DHNAME ,
            $DHFILID ,
            $DHMSEQ ,
            $DHSSEQ
        );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_DEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_DEL');
        }else{
            $data = array('result' => true);
        }
    }
    return $data;
}
/*
*SQLクエリーのためチェック
*/
function cmChkBSQLCND( $db2con, $D3NAME,$DHMSEQ){
    $data = array();
    $rs=true;
    $TOTCOUNT=0;
    $data=fnBSQLCND_COUNT($db2con, $D3NAME,$DHMSEQ);
    if($data['result']!==true){
        $rs=$data['result'];
    }else{
        $TOTCOUNT=$data['COUNT'];
        if($TOTCOUNT<=0){
           $rs= 'NOTEXIST_GET';
        }
    }
    return $rs;
}
/*
*SQLクエリーのためチェック
*/
function fnBSQLCND_COUNT($db2con, $D3NAME,$DHMSEQ){
    $data = array();
    $rs=true;
    $TOTCOUNT=0;
    $strSQL = '';
    $strSQL .=  'SELECT COUNT(*) AS TOTCOUNT ';
    $strSQL .=  'FROM BSQLCND AS A WHERE ';
    $strSQL .=  'A.CNQRYN  = ? ';
    $strSQL .=  'AND A.CNDSEQ = ? ';

    $params = array($D3NAME,$DHMSEQ);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs= 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs= 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $TOTCOUNT = $row['TOTCOUNT'];
            }
        }
    }
    $data=array('result'=>$rs,'COUNT'=>$TOTCOUNT);
    return $data;
}

