//文字の色付け（条件付き書式）
<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$d1name = (isset($_POST['D1NAME']))?$_POST['D1NAME']:'';
$phpquerycolor = json_decode($_POST['PHPQUERYCOLOR'],true);
$PROC  = (isset($_POST['PROC']))?$_POST['PROC']:'';
$checkRes=array();
$phpquerycolorRes=array();
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtnArray = array();        //リターン配列
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'9',$userData[0]['WUSAUT']);//'9' => EXCELテンプレート
            if($rs['result'] !== true){
                $rtn = 4;
                $msg =  showMsg($rs['result'],array('EXCELテンプレートの権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック
if ($rtn === 0) {
    $chkQry = cmChkQuery($db2con, '', $d1name, '');
    if ($chkQry['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($chkQry['result'], array(
            '定義名'
        ));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$d1name,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのEXCELテンプレートの権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('EXCELテンプレートの権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
//check
if ($rtn === 0) {
    foreach($phpquerycolor as $key => $col) {
        //DB2JKSKの全部のチェック        
        if ($rtn === 0) {
            if (trim($col['JKFILD']) === '') {
				if($col['JKOPTR']!=='' ||  $col['JKDATA']!=='' || $col['JKCOST']!=='' || $col['JKBGST']!==''){
	                $rtn = 1;
	                $msg = showMsg('FAIL_REQ', array('フィード'));
	                break;
				}
            }
        }
		
        /**
         *rowのデータがある「array」
         *
         */
        if ($rtn === 0) {
			if($col['JKFILD']!==''){
            	array_push($phpquerycolorRes, $col);
			}
        }

    }
}
e_log("edtDB2JKSK:".print_r($phpquerycolorRes,true));
//save
if ($rtn === 0){
    if($PROC==='DEL'){
        $data =fnDelDB2JKSK($db2con,$d1name);
        $rs=$data['result'];
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }else if($PROC==='ADD'){     
		//クエリー色設定の追加    
        $rs = fnInsDB2JKSK($db2con, $d1name,$phpquerycolorRes);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
      
    }
}
}
cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnArray));

//save
function fnInsDB2JKSK($db2con, $d1name,$phpquerycolorRes){
    $rs=true;
    $data=array();
    $strSQL  ='';
    $data =fnDelDB2JKSK($db2con,$d1name);    
    $strSQL  = ' INSERT INTO DB2JKSK';
    $strSQL .= '(';
    $strSQL .= 'JKNAME,';
    $strSQL .= 'JKSEQ,';
    $strSQL .= 'JKFILD,';
    $strSQL .= 'JKDATA,';
    $strSQL .= 'JKCOST,';
    $strSQL .= 'JKBGST,';
    $strSQL .= 'JKOPTR';
    $strSQL .= ')';
    $strSQL .= ' VALUES ( ?, ?, ?, ?, ?, ?, ? ) ' ;
    $stmt1 = db2_prepare($db2con,$strSQL);
    if($stmt1 === false){
        $rs = 'FAIL_INS';
    }else{
        $seqnumber = 0;
        foreach($phpquerycolorRes as $col){
            $params= array(
                $d1name,
                ++$seqnumber,                       
                $col['JKFILD'],
                $col['JKDATA'],                        
                $col['JKCOST'],
                $col['JKBGST'],
                $col['JKOPTR']
            );		
            $r = db2_execute($stmt1,$params);
            if($r === false){
                $rs='FAIL_INS';
                break;
            }
        }//foreach
    }//else
    return $rs;
}
/*
*
*delete DB2JKSK
*/
function fnDelDB2JKSK($db2con, $d1name){
    $data=array();
    $strSQL = '';  
        $r =db2_execute($stmt,$params);       
            $strSQL = ' DELETE FROM DB2JKSK WHERE JKNAME = ? ';
            $params = array($d1name);
            $stmt = db2_prepare($db2con, $strSQL);
            if($stmt===false){
                 $data=array('result' => 'FAIL_DEL');
            }else{
                $params = array($d1name);
                $r =db2_execute($stmt,$params);
                $data=($r===false)?array('result' => 'FAIL_DEL'):array('result'=>true);
            }
    return $data;
}
