<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/
$PROC = $_POST['PROC'];
$FLD = $_POST['FLD'];
$FILID = $_POST['FILID'];
$QYNAME = $_POST['QYNAME'];
$LIB = $_POST['LIB'];
$FIE = $_POST['FIE'];
$MBR = (isset($_POST['MBR'])? $_POST['MBR'] : '' );
$GRIDDATA = json_decode($_POST['GRIDDATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
            $userData  = umEx($rs['data']);
            if($userData[0]['WUAUTH'] === '2'){
                $rs = cmChkKenGen($db2con,'10',$userData[0]['WUSAUT']);//'10' => 詳細情報設定権限
                if($rs['result'] !== true){
                    $rtn = 2;
                    $msg =  showMsg($rs['result'],array('詳細情報設定の権限'));
                }
            }
        }
}
//定義が削除されたかどうかチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $QYNAME, '');
    if($chkQry['result'] !== true ) {
        $rtn = 2;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$QYNAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての詳細情報設定の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('詳細情報設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $rs = cmCountDB2DRGS($db2con,$QYNAME,$FLD,$FILID);//クエリー連携データあるかどうかのチェック
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $COUNT = $rs['COUNT'];
        if($COUNT>0){
            $msg = showMsg('SYOUSAI_FAIL');
            $rtn = 3;
        }
    }
}
if($PROC === 'U'){
    if($rtn === 0){
        //必須チェック
        if($LIB === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('ライブラリー'));
            $focus = 'LIB';
        }
    }
    if($rtn === 0){
        //必須チェック
        if($FIE === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('ファイル'));
            $focus = 'FIE';
        }
    }
    if($rtn === 0){
        $rs = fnGetFDB2CSV1($db2con,$QYNAME);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('クエリー'));
        }else{
            $fdb2csv1 = $rs['data'][0];
        }
    }
    if($rtn === 0){
        if(($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME)){
            $db2RDBCon = $db2con;
        }else{
            $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1['D1RDB'];
            $db2RDBCon = cmDB2ConRDB($fdb2csv1['D1LIBL']);
        }
    }
    if($rtn === 0){
        $rs = fnCheckTable($db2RDBCon,$LIB,$FIE);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ライブラリーまたはファイルの'));
        }
    }
    if($rtn === 0){
        $MBR = strtoupper($MBR);
        if($MBR === '*FIRST' || $MBR === ''){
            $MBRDATA = '';
        }else if($MBR === '*LAST'){
            $MBRDATA = fnGetFilLast($fdb2csv1['D1RDB'],$fdb2csv1['D1LIBL'],$LIB,$FIE);
        }else{
            $MBRDATA = $MBR;
            $rs = fnCheckMBR($db2RDBCon,$LIB,$FIE,$MBRDATA);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result'],array('メンバー'));
            }
        }
    }
    if($rtn === 0){   
        if($FILID !== 0 && $FLD !== ''){
            //FDB2CSV2に存在するかチェック(FILID=9999は無条件でtrue)
            if($FILID === '9999'){
                $rs = array();
            }else{
                $rs = cmCheckFDB2CSV2($db2con,$QYNAME,$FILID,$FLD);
                    if($rs === false){
                    $rtn = 1;
                    $msg = showMsg('FAIL_SYS');
                }else{
                    if(count($rs) === 0){
                        //FDB2CSV5に存在するかチェック
                        $rs = cmCheckFDB2CSV5($db2con,$QYNAME,$FLD);
                        if($rs === false){
                            $rtn = 1;
                            $msg = showMsg('FAIL_SYS');
                        }else{
                            if(count($rs) === 0){
                                $rtn = 1;
                                $msg = showMsg('FAIL_COL',array('クエリー','カラム'));
                            }
                        }
                    }
                }
            }
        }
    }
    if($rtn === 0){
        for($i = 0 ; $i < count($GRIDDATA) ; $i++){
            if($GRIDDATA[$i]['DFCHECK'] === '1' || $GRIDDATA[$i]['DFFKEY'] === '1' ){
                $rs = fnChkFld($db2RDBCon,$GRIDDATA[$i]['DFCOLM'],$LIB,$FIE);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg('選択した情報は指定したファイルにありません。'); 
                    break;
                }
            }
        }
    }
    if($rtn === 0){
        $chkFlg = false;
        for($i = 0 ; $i < count($GRIDDATA) ; $i++){
            if($GRIDDATA[$i]['DFCHECK'] === '1'){
                $chkFlg = true;
                break;
            }
        }
        if($chkFlg === false){
            $rtn = 1;
            $msg = showMsg('FAIL_SET',array('表示項目'));
        }
    }

    if($rtn === 0){
        $rs = fnUpdateDB2WDTL($db2con,$QYNAME,$FILID,$FLD,$LIB,$FIE,$MBR);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
        else{
            $result = fnDelDB2WDFL($db2con,$QYNAME,$FILID,$FLD);
            if($rs !== true){
                 $rtn = 1;
                 $msg = showMsg($rs);
             }
             else{
                for($i = 0 ; $i < count($GRIDDATA) ; $i++){
                    $rs = fnInsDB2WDFL($db2con,$QYNAME,$FILID,$FLD,$GRIDDATA[$i]);
                    if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs);
                        break;
                    } 
                }
             }
        }
    }
}else if($PROC = 'D'){
    if($rtn === 0){
        $result = fnDelDB2WDTL($db2con,$QYNAME,$FILID,$FLD);
        if($rs['result'] !== true){
             $rtn = 1;
             $msg = showMsg($rs);
        }else{
            $result = fnDelDB2WDFL($db2con,$QYNAME,$FILID,$FLD);
            if($rs['result'] !== true){
                 $rtn = 1;
                 $msg = showMsg($rs);
            }
        }
         
    }
}
if(($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME) === false){
    cmDb2Close($db2RDBCon);
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ヘルプ情報更新
*-------------------------------------------------------*
*/

function fnUpdateDB2WDTL($db2con,$QYNAME,$FILID,$FLD,$LIB,$FIE,$MBR){


    $rs = true;
    $rsCount = array();

    $strSQL  = ' SELECT COUNT(*) AS COUNT';
    $strSQL .= ' FROM DB2WDTL'; 
    $strSQL .= ' WHERE DTNAME = ? ';
    $strSQL .= ' AND DTFILID= ? ';
    $strSQL .= ' AND DTFLD= ? ';

    $params = array(
        $QYNAME,
        $FILID,
        $FLD
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rsCount[] = $row['COUNT'];
            }
            
            if($rsCount[0] > 0){
                //構文
                $strSQL  = ' UPDATE DB2WDTL ';
                $strSQL .= ' SET ';
                $strSQL .= ' DTLIBL = ?, ';
                $strSQL .= ' DTFILE = ?, ';
                $strSQL .= ' DTMBR  = ? ';
                $strSQL .= ' WHERE DTNAME = ? ';
                $strSQL .= ' AND DTFILID = ? ';
                $strSQL .= ' AND DTFLD = ? ';

                $params = array(
                    $LIB,
                    $FIE,
                    $MBR,
                    $QYNAME,
                    $FILID,
                    $FLD
                );
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = 'FAIL_UPD';
                }else{
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $rs = 'FAIL_UPD';
                    }
                }

            }
            else{
                    //構文
                    $strSQL  = ' INSERT INTO DB2WDTL ';
                    $strSQL .= ' ( ';
                    $strSQL .= ' DTNAME, ';
                    $strSQL .= ' DTFILID, ';
                    $strSQL .= ' DTFLD, ';
                    $strSQL .= ' DTLIBL, ';
                    $strSQL .= ' DTFILE, ';
                    $strSQL .= ' DTMBR ';
                    $strSQL .= ' ) ';
                    $strSQL .= ' VALUES(?,?,?,?,?,?) ';

                    $params = array(
                        $QYNAME,
                        $FILID,
                        $FLD,
                        $LIB,
                        $FIE,
                        $MBR
                    );
                    $stmt = db2_prepare($db2con,$strSQL);
                    if($stmt === false){
                         $rs = 'FAIL_INS';
                    }else{
                        $r = db2_execute($stmt,$params);
                        if($r === false){
                            $rs = 'FAIL_INS';
                        }
                    }
                }


            }
            
        }

    
    return $rs;

}

function fnInsDB2WDFL($db2con,$QYNAME,$FILID,$FLD,$GDATA){

    $rs = true;
    $rsCount = array();
    //構文
    $strSQL  = ' INSERT INTO DB2WDFL ';
    $strSQL .= ' ( ';
    $strSQL .= ' DFNAME, ';
    $strSQL .= ' DFFILID, ';
    $strSQL .= ' DFFLD, ';
    $strSQL .= ' DFCOLM, ';
    $strSQL .= ' DFFKEY, ';
    $strSQL .= ' DFCHECK ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES(?,?,?,?,?,?) ';
    $params = array(
       $QYNAME,
       $FILID,
       $FLD,       
       $GDATA['DFCOLM'],
       $GDATA['DFFKEY'],
       $GDATA['DFCHECK']
    );
     e_log(print_R($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            e_log(db2_stmt_errormsg());
            $rs = 'FAIL_INS';
        }
    }
        
    return $rs;

}

/**
* 更新前データの削除
* 
* @param Object  $db2con    DBコネクション
* @param String  $QYNAME    クエリーID
* 
*/

function fnDelDB2WDFL($db2con,$QYNAME,$FILID,$FLD){


    $rs = true;
    //構文
    $strSQL  = ' DELETE ';
    $strSQL .= ' FROM ';
    $strSQL .= ' DB2WDFL ';
    $strSQL .= ' WHERE DFNAME = ? ';
    $strSQL .= ' AND DFFILID = ? ';
    $strSQL .= ' AND DFFLD = ? ';

    $params = array(
        $QYNAME,
        $FILID,
        $FLD

    );
    $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_DEL';
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                 $rs = 'FAIL_DEL';
            }
        }
    
    return $rs;

}

/**
* 更新前データの削除
* 
* @param Object  $db2con    DBコネクション
* @param String  $QYNAME    クエリーID
* 
*/

function fnDelDB2WDTL($db2con,$QYNAME,$FILID,$FLD){

    $rs = true;
    //構文
    $strSQL  = ' DELETE ';
    $strSQL .= ' FROM ';
    $strSQL .= ' DB2WDTL ';
    $strSQL .= ' WHERE DTNAME = ? ';
    $strSQL .= ' AND DTFILID = ? ';
    $strSQL .= ' AND DTFLD = ? ';

    $params = array(
        $QYNAME,
        $FILID,
        $FLD

    );
    $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_DEL';
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                 $rs = 'FAIL_DEL';
            }
        }
    
    return $rs;

}


/*
*-------------------------------------------------------* 
* テーブルがあるかないかチェック
*-------------------------------------------------------*
*/

function fnCheckTable($db2con,$LIB,$FIL){

    $data = array();
    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM QSYS2/SYSTABLES ';
    $strSQL .= ' WHERE SYSTEM_TABLE_SCHEMA = ? ';
    $strSQL .= ' AND SYSTEM_TABLE_NAME = ? ';

    $params = array(
        $LIB,
        $FIL
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'FAIL_CMP'); 
            }
            else{
               $data = array('result' => true);
            }
        }
    }
   return $data;
}
function fnCheckMBR($db2con,$LIB,$FILE,$MBR){
    $data = array();
    $params = array();

    $strSQL .= '    SELECT ';
    $strSQL .= '        A.SYS_DNAME, ';
    $strSQL .= '        A.SYS_TNAME, ';
    $strSQL .= '        A.SYS_MNAME, ';
    $strSQL .= '        A.PARTNBR, ';
    $strSQL .= '        A.LABEL ';
    $strSQL .= '    FROM ';
    $strSQL .= '        QSYS2/SYSPSTAT A';
    $strSQL .= '    WHERE ';
    $strSQL .= '        A.TABLE_SCHEMA = ? ';
    $strSQL .= '    AND A.TABLE_NAME = ? ';
    $strSQL .= '    AND A.SYS_MNAME = ?';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log(db2_stmt_errormsg().'メンバーデータ取得：'.$strSQL);
    }else{
        $params = array($LIB,$FILE,$MBR);
        e_log('メンバーデータ取得SQL：'.$strSQL.print_r($params,true));
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
            e_log(db2_stmt_errormsg().'メンバーデータ取得：'.$strSQL.print_r($params,true));
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
        if(count($data) === 0){
             $data = array('result' => 'NOTEXIST');
        }else{
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}

function fnChkFld($db2con,$COLNM,$LIB,$FIL){

    $data = array();
    $strSQL  = ' SELECT SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
    if(SYSCOLCFLG==='1'){
        $strSQL .= '      , COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .= '      , COLUMN_HEADING ';
    }
    $strSQL .= ' FROM QSYS2/SYSCOLUMNS ';
    $strSQL .= ' WHERE TABLE_SCHEMA = ? ';
    $strSQL .= ' AND   TABLE_NAME = ? ';
    $strSQL .= ' AND   SYSTEM_COLUMN_NAME = ? ';

    $params = array(
        $LIB,
        $FIL,
        $COLNM
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'FAIL_CMP'); 
            }
            else{
               $data = array('result' => true);
            }
        }
    }
   return $data;
}
/*
 * FDB2CSV1の対象定義データ取得
 */
function fnGetFDB2CSV1($db2con,$QRYNM){
    $data = array();
    $strSQL  = '    SELECT * ';
    $strSQL .= '    FROM FDB2CSV1 ';
    $strSQL .= '    WHERE D1NAME = ?  ';
    

    $params = array(
        $QRYNM
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET'); 
            }
            else{
               $data = array('result' => true,'data' => umEx($data) );
            }
        }
    }
    return $data;
}
