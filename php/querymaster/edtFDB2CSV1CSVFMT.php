<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$proc = $_POST['proc'];
$DATA = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'16',$userData[0]['WUSAUT']);//'16' => CSV形式
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('CSV形式の権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック
if ($rtn === 0) {
    $chkQry = cmChkQuery($db2con, '', $D1NAME, '');
    if ($chkQry['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($chkQry['result'], array(
            '定義名'
        ));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのCSV形式の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('CSV形式の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/

$D1CENC = $DATA['D1CENC'];
$D1CKAI = $DATA['D1CKAI'];
$D1CHED = $DATA['D1CHED'];
$D1CDLM = $DATA['D1CDLM'];
$D1CECL = $DATA['D1CECL'];

if($rtn === 0){
    $rs = fnUpdateFDB2CSV1($db2con,$D1NAME,$D1CENC,$D1CKAI,$D1CHED,$D1CDLM,$D1CECL);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('フィールド'));
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ヘルプ情報更新
*-------------------------------------------------------*
*/

function fnUpdateFDB2CSV1($db2con,$D1NAME,$D1CENC,$D1CKAI,$D1CHED,$D1CDLM,$D1CECL){


    $rs = true;
    $rsCount = array();

    $strSQL  = ' SELECT COUNT(*) AS COUNT';
    $strSQL .= ' FROM FDB2CSV1'; 
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array(
        $D1NAME
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rsCount[] = $row['COUNT'];
            }
            
            if($rsCount[0] > 0){
                //構文
                $strSQL  = ' UPDATE FDB2CSV1 ';
                $strSQL .= ' SET ';
                $strSQL .= ' D1CENC = ?, ';
                $strSQL .= ' D1CKAI = ?, ';
                $strSQL .= ' D1CHED = ?, ';
                $strSQL .= ' D1CDLM = ?, ';
                $strSQL .= ' D1CECL = ?  ';
                $strSQL .= ' WHERE D1NAME = ? ';
                $params = array(
                    $D1CENC,
                    $D1CKAI,
                    $D1CHED,
                    $D1CDLM,
                    $D1CECL,
                    $D1NAME
                );
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = 'FAIL_INS';
                }else{
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $rs = 'FAIL_INS';
                    }
                }

            }
            else{
                $rs = 'NOTEXIST_UPD';
            }
            
        }
    }
    return $rs;
}
