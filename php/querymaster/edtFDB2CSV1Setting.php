<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */

if (isset($_POST['DATA'])) {
    $data   = json_decode($_POST['DATA'], true);
    $D1NAME = $data['D1NAME'];
    $D1DISF = $data['D1DISF'];
    $DISCSN = $data['DISCSN'];
    $D1EFLG = $data['D1EFLG'];
    $D1ETYP = $data['D1ETYP'];
} else {
    $D1NAME = '';
    $D1DISF = '';
    $DISCSN = '';
    $D1EFLG = '';
    $D1ETYP = '';
}
//$D1EFLG = $_POST['D1EFLG'];
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$data = array();
$rtn  = 0;
$msg  = '';
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */

//htmldecode
$D1NAME = cmHscDe($D1NAME);


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'7',$userData[0]['WUSAUT']);//'7' => クエリー実行設定
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('クエリー実行設定の権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $D1NAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのクエリー実行設定がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('クエリー実行設定'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    if($DISCSN !== ''){
        if(!checkMaxLen($DISCSN,30)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('カスタム'));
        }
    }
}

if ($rtn === 0) {
    //ユーザー名取得
    $rs = fnUpdFDB2CSV1Data($db2con, $D1NAME, $D1DISF, $DISCSN, $D1EFLG,$D1ETYP);
    if ($rs['result'] !== true) {
        $msg = showMsg($rs['result'], array(
            'クエリー'
        ));
        $rtn = 1;
    }
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'aaDATA' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg
);
echo (json_encode($rtn));

/**
 * D2TYPEの取得
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con  DBコネクション
 * @param String  $D1NAME  クエリ名
 * @param String  $D1DISF  ラジオflag
 * @param String  $DISCSN  カスタム
 * 
 */

function fnUpdFDB2CSV1Data($db2con, $D1NAME, $D1DISF, $DISCSN, $D1EFLG,$D1ETYP)
{
    
    $data   = array();
    $params = array();
    
    $strSQL  = ' UPDATE FDB2CSV1 SET D1DISF = ? ,DISCSN =? , D1EFLG = ? , D1ETYP = ? ';
    $strSQL .= ' WHERE D1NAME = ?';
    
    $params = array(
        $D1DISF,
        $DISCSN,
        $D1EFLG,
        $D1ETYP,
        $D1NAME
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            $data = array(
                'result' => true
            );
        }
    }
    return $data;
}