<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$values = json_decode($_POST['values'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'11',$userData[0]['WUSAUT']);//'11' =>ダウンロード制限
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('ダウンロード制限の権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $D1NAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのダウンロード制限の権限がありません。');
            $msg = showMsg('FAIL_QRY_USR',array('ダウンロード制限の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/

$rs = true;

//一つ以上にチェックがついているかのチェック
$trueflg = false;
if($rtn === 0){
foreach($values as $key => $value){
    if($value['value'] === true){
        $trueflg = true;
    }
  }
}

if($rtn === 0){
if($trueflg === false){
    $rs = false;
    $rtn = 1;
    $msg = showMsg('CHK_SEL',array('項目'));
  }
}

if($rtn === 0){
if($rs === true){
    foreach($values as $key => $value){
        if($value['filid'] !== '9999'){
            $rs = fnUpdateFDB2CSV2($db2con,$D1NAME,$value['filid'],$value['field'],$value['value']);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
                break;
            }
        }else{
            $rs = fnUpdateFDB2CSV5($db2con,$D1NAME,$value['field'],$value['value']);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
                break;
            }
        }
    }
  }
}


cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'values' => $values
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ダウンロード可否情報更新
*-------------------------------------------------------*
*/

function fnUpdateFDB2CSV2($db2con,$D2NAME,$D2FILID,$D2FLD,$value){
    $rs = true;

    //構文

    $strSQL  = ' UPDATE FDB2CSV2 ';
    $strSQL .= ' SET ';
    $strSQL .= ' D2DNLF = ? ';
    $strSQL .= ' WHERE D2NAME = ? ';
    $strSQL .= ' AND D2FILID = ? ';
    $strSQL .= ' AND D2FLD = ? ';

    $params = array(
        ($value === true)?'':'1',
        $D2NAME,
        $D2FILID,
        $D2FLD
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* ダウンロード可否情報更新
*-------------------------------------------------------*
*/

function fnUpdateFDB2CSV5($db2con,$D5NAME,$D5FLD,$value){


    $rs = true;

    //構文
    $strSQL  = ' UPDATE FDB2CSV5 ';
    $strSQL .= ' SET ';
    $strSQL .= ' D5DNLF = ? ';
    $strSQL .= ' WHERE D5NAME = ? ';
    $strSQL .= ' AND D5FLD = ? ';

    $params = array(
        ($value === true)?'':'1',
        $D2NAME,
        $D2FLD
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;

}