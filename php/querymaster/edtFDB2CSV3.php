<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../base/getTblMBR.php");

/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$D3NAME     = $_POST['D3NAME'];
$D3FILID    = $_POST['D3FILID'];
$D3JSEQ     = $_POST['D3JSEQ'];
$D3CNT      = $_POST['D3CNT'];
$D1WEBF     = $_POST['D1WEBF'];
$D3FLD      = $_POST['D3FLD'];
$proc       = $_POST['proc'];
$DATA       = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$D1CFLG='';//SQLクエリーかどうかのFLG
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$D3NAME     = cmHscDe($D3NAME);
$D3FILID    = cmHscDe($D3FILID);
$D3JSEQ     = cmHscDe($D3JSEQ);
$D3CNT      = cmHscDe($D3CNT);
$D1WEBF     = cmHscDe($D1WEBF);
$D3FLD      = cmHscDe($D3FLD);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
            $userData  = umEx($rs['data']);
            if($userData[0]['WUAUTH'] === '2'){
                $rs = cmChkKenGen($db2con,'13',$userData[0]['WUSAUT']);//'13' =>検索候補
                if($rs['result'] !== true){
                    $rtn = 2;
                    $msg =  showMsg($rs['result'],array('検索候補の権限'));
                }
            }
     }
}

/**20170626 akz**/
if($rtn === 0){
    $LIBSNM = cmMer($userData[0]['WULIBL']);
}
/**20170626 akz**/

//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $D3NAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D3NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての検索候補の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('検索候補の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}

//GET DATA FROM FDB2CSV1
if($rtn === 0){
    $fdb2csv1data = FDB2CSV1_DATA($D3NAME); 
    if($fdb2csv1data['result'] !== true){
        $rtn = 1;
        $msg = showMsg($fdb2csv1data['result'],array('クエリー'));
    }else{
        $fdb2csv1data = umEx($fdb2csv1data['data']);
        $fdb2csv1data = $fdb2csv1data[0];
        $D1CFLG=$fdb2csv1data['D1CFLG'];
    }
}

//検索条件が削除されるかどうかチェック
if($rtn === 0){
    if($D1CFLG==='1'){
       $chkCnd= cmChkBSQLCND( $db2con, $D3NAME,$D3JSEQ);
        if( $chkCnd !== true ) {
            $rtn = 4;
            $msg = showMsg($chkCnd,array('検索条件'));
        }
    }else{
        $chkCnd = cmChkCnd ( $db2con, $D3NAME, $D3FILID, $D3JSEQ, $D3CNT, $D1WEBF,$D3FLD);
        if( $chkCnd['result'] !== true ) {
            $rtn = 4;
            $msg = showMsg($chkCnd['result'],array('検索条件'));
        }
    }
}

//バリデーションチェック

if($proc === 'U'){
    if($rtn === 0){
        if($DATA['D3CANL'] === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('候補ライブラリー'));
            $focus = 'D3CANL';
        }
    }

    if($rtn === 0){
        $byte = checkByte($DATA['D3CANL']);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('候補ライブラリー'));
            $focus = 'D3CANL';
        }
    }

    if($rtn === 0){
        if(!checkMaxLen($DATA['D3CANL'],10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('候補ライブラリー'));
            $focus = 'D3CANL';
        }
    }

    if($rtn === 0){
        if($DATA['D3CANF'] === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('候補ファイル'));
            $focus = 'D3CANF';
        }
    }

    if($rtn === 0){
        $byte = checkByte($DATA['D3CANF']);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('候補ファイル'));
            $focus = 'D3CANF';
        }
    }

    if($rtn === 0){
        if(!checkMaxLen($DATA['D3CANF'],10)){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('候補ファイル'));
            $focus = 'D3CANF';
        }
    }
    if($rtn === 0){
        $byte = checkByte($DATA['D3CMBR']);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('候補メンバー'));
            $focus = 'D3CMBR';
        }
    }

    if($rtn === 0){
        if(!checkMaxLen($DATA['D3CMBR'],10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('候補メンバー'));
            $focus = 'D3CMBR';
        }
    }

    if($rtn === 0){
        if($DATA['D3CANC'] === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('候補フィールド'));
            $focus = 'D3CANC';
        }
    }

    if($rtn === 0){
        $byte = checkByte($DATA['D3CANC']);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('候補フィールド'));
            $focus = 'D3CANC';
        }
    }

    if($rtn === 0){
        if(!checkMaxLen($DATA['D3CANC'],10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('候補フィールド'));
            $focus = 'D3CANC';
        }
    }
    if($rtn === 0){
        if($DATA['D3NAMG'] <> ''){
            if($DATA['D3NAMC'] === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('名称表示フィールド名'));
                $focus = 'D3NAMC';
            }
        }
    }
    if($rtn === 0){
        $byte = checkByte($DATA['D3NAMC']);
        if($byte[1] > 0){
            $rtn = 1;
            $msg = showMsg('FAIL_BYTE',array('名称表示フィールド名'));
            $focus = 'D3NAMC';
        }
    }
    if($rtn === 0){
        if(!checkMaxLen($DATA['D3NAMC'],10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array('名称表示フィールド名'));
            $focus = 'D3NAMC';
        }
    }
    if($rtn === 0){
        if($DATA['D3CANC'] === $DATA['D3NAMC']){
            $rtn = 1;
            $msg = showMsg('MSGSAME',array('候補フィールド','名称表示フィールド名'));
            $focus = 'D3NAMC';
        }
    }
}
/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/
$D3CAST = (isset($DATA['D3CAST'])?'1':'');
$D3CANG = (isset($DATA['D3CANG'])?'1':'');
$D3NAMG = (isset($DATA['D3NAMG'])?'1':'');
$D3NAST = (isset($DATA['D3NAST'])?'1':'');

if($fdb2csv1data['D1RDB'] === '' || $fdb2csv1data['D1RDB'] === RDB || $fdb2csv1data['D1RDB'] === RDBNAME){
    $_SESSION['PHPQUERY']['RDBNM'] = '';
    $db2RDBCon = cmDb2ConLib($fdb2csv1data['D1LIBL']);
}else{
    $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1data['D1RDB'];
    $db2RDBCon = cmDB2ConRDB($fdb2csv1data['D1LIBL'] );
}
if($proc === 'U'){
    $LIBLIST = array();
    if($rtn === 0){
        if (strtoupper($DATA['D3CANL']) === '*LIBL' || strtoupper($DATA['D3CANL']) === '*USRLIBL'){
            $D1LIBL = $fdb2csv1data['D1LIBL'];
            $LIBLIST = explode(" ", preg_replace('/\s+/', ' ', cmMer($D1LIBL)));
        }
    }
    if($rtn === 0){
        if($LIBSNM !== ''){
            if(($fdb2csv1data['D1RDB'] === '' || $fdb2csv1data['D1RDB'] === RDB || $fdb2csv1data['D1RDB'] === RDBNAME) === false){
                $resCreate = createQTEMPDB2LBLS($db2RDBCon,$LIBSNM);
                if($resCreate !== true){
                    $rtn = 1;
                    $msg = showMsg($resCreate);
                }
            }
        }
    }
    if($rtn === 0){
        // libary check
        $rs = fnChkExistLib($db2RDBCon,strtoupper($DATA['D3CANL']),$LIBLIST,$LIBSNM);
        if($rs === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST',array('候補ライブラリー'));
        }else if($rs === 'FAIL_SEL'){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
    if($rtn === 0){
        $rs = fnChkExistFile($db2RDBCon,strtoupper($DATA['D3CANL']),$DATA['D3CANF'],$LIBLIST);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('候補ファイル'));
        }else {
            $FILEINFO = $rs['data'];
        }
    }
    if($rtn === 0){
        $DATA['D3CMBR'] = strtoupper($DATA['D3CMBR']);
        if($DATA['D3CMBR'] === '' || $DATA['D3CMBR'] === '*FIRST'){
            // 処理なし
            $D3CMBR = '';
        }else if($DATA['D3CMBR'] === '*LAST'){
                $D3CMBR = fnGetFilLast($fdb2csv1data['D1RDB'],$fdb2csv1data['D1LIBL'],$FILEINFO['SYSTEM_TABLE_SCHEMA'],$FILEINFO['SYSTEM_TABLE_NAME']);
        }else{
            $rs = fnChkTblCol($db2RDBCon);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $lblflg = false;
                foreach($rs['data'] as $val){
                    if($val === 'LABEL'){
                        $lblflg = true;
                        break;
                    }
                }
            }
            if($rtn === 0){
                $rs = fnChkExistMBR ($db2RDBCon,$FILEINFO['SYSTEM_TABLE_SCHEMA'],$FILEINFO['SYSTEM_TABLE_NAME'],$DATA['D3CMBR'],$lblflg);
                if($rs['result'] !== true){
                     $rtn = 1;
                     $msg = showMsg($rs['result'],array('候補メンバー'));
                }else{
                    $D3CMBR = $rs['data']['SYS_MNAME'];
                }
            }
        }
    }
    if($rtn === 0){
        $rs = fnChkExistFld ($db2RDBCon,$DATA['D3CANC'],$FILEINFO,$LIBLIST);
        if($rs['result'] === true){
             $rtn = 1;
             $msg = showMsg($rs['data'],array('候補フィールド'));
        }else{
            $D3CANCDATA = $rs['data'][0];
        }
    }
    if($rtn === 0){
        if($DATA['D3NAMC'] !== ''){
            $rs = fnChkExistFld ($db2RDBCon,$DATA['D3NAMC'],$FILEINFO,$LIBLIST);
            if($rs['result'] === true){
                 $rtn = 1;
                 $msg = showMsg($rs['data'],array('名称表示フィールド名'));
            }else{
                $D3NAMCDATA = $rs['data'][0];
            }
        }
    }
    if($rtn === 0){
        //チェック設定のValidation
        $rs = fnChkSetting($db2RDBCon,strtoupper($DATA['D3CANL']),$DATA['D3CANF'],$D3CMBR,$DATA['D3CANC'],$DATA['D3NAMC'],$D1LIBL,$D3CANCDATA,$D3NAMCDATA);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }

    }
}
if($D1WEBF !== '1'){
    if($rtn === 0){
        $rs = fnUpdateFDB2CSV3($db2con,strtoupper($DATA['D3CANL']),$DATA['D3CANF'],$DATA['D3CMBR'],$DATA['D3CANC'],$D3CAST,$D3CANG,$D3NAMG,$D3NAST,$DATA['D3NAMC'],$D3NAME,$D3FILID,$D3JSEQ,$D3CNT);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('フィールド'));
        }
    }
}else{
    if($D1CFLG==='1'){
        if($rtn === 0){
            $rs=fnUpdateBSQLCND($db2con,strtoupper($DATA['D3CANL']),$DATA['D3CANF'],$DATA['D3CMBR'],$DATA['D3CANC'],$D3CAST,$D3CANG,$D3NAMG,$D3NAST,$DATA['D3NAMC'],$D3NAME,$D3JSEQ);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('フィールド'));
            }
        }
    }else{
        if($rtn === 0){
            $rs = fnUpdateBQRYCND($db2con,strtoupper($DATA['D3CANL']),$DATA['D3CANF'],$DATA['D3CMBR'],$DATA['D3CANC'],$D3CAST,$D3CANG,$D3NAMG,$D3NAST,$DATA['D3NAMC'],$D3NAME,$D3FILID,$D3JSEQ,$D3CNT);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('フィールド'));
            }
        }
    }
}

cmDb2Close($db2con);
cmDb2Close($db2RDBCon);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'LIBLIST'=> $LIBLIST,
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ヘルプ情報更新
*-------------------------------------------------------*
*/

function fnUpdateFDB2CSV3($db2con,$D3CANL,$D3CANF,$D3CMBR,$D3CANC,$D3CAST,$D3CANG,$D3NAMG,$D3NAST,$D3NAMC,$D3NAME,$D3FILID,$D3JSEQ,$D3CNT){


    $rs = true;
    $rsCount = array();

    $strSQL  = ' SELECT COUNT(*) AS COUNT';
    $strSQL .= ' FROM FDB2CSV3'; 
    $strSQL .= ' WHERE D3NAME = ? ';
    $strSQL .= ' AND D3FILID = ? ';
    $strSQL .= ' AND D3JSEQ = ? ';
    $strSQL .= ' AND D3CNT = ? ';

    $params = array(
        $D3NAME,
        $D3FILID,
        $D3JSEQ,
        $D3CNT
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rsCount[] = $row['COUNT'];
            }
            
            if($rsCount[0] > 0){
                //構文
                $strSQL  = ' UPDATE FDB2CSV3 ';
                $strSQL .= ' SET ';
                $strSQL .= ' D3CANL = ?, ';
                $strSQL .= ' D3CANF = ?, ';
                $strSQL .= ' D3CMBR = ?, ';
                $strSQL .= ' D3CANC = ?, ';
                $strSQL .= ' D3CANG = ?, ';
                $strSQL .= ' D3NAMG = ?, ';
                $strSQL .= ' D3NAMC = ?, ';
                $strSQL .= ' D3CAST = ?, ';
                $strSQL .= ' D3NAST = ? ';
                $strSQL .= ' WHERE D3NAME = ? ';
                $strSQL .= ' AND D3FILID = ? ';
                $strSQL .= ' AND D3JSEQ = ? ';
                $strSQL .= ' AND D3CNT = ? ';

                $params = array(
                    $D3CANL,
                    $D3CANF,
                    $D3CMBR,
                    $D3CANC,
                    $D3CANG,
                    $D3NAMG,
                    $D3NAMC,
                    $D3CAST,
                    $D3NAST,
                    $D3NAME,
                    $D3FILID,
                    $D3JSEQ,
                    $D3CNT
                );
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = 'FAIL_INS';
                }else{
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $rs = 'FAIL_INS';
                    }
                }
            }
            else{
                $rs = 'NOTEXIST_UPD';
            }
        }
    }

    
    return $rs;

}
function fnUpdateBQRYCND($db2con,$D3CANL,$D3CANF,$D3CMBR,$D3CANC,$D3CAST,$D3CANG,$D3NAMG,$D3NAST,$D3NAMC,$D3NAME,$D3FILID,$D3JSEQ,$D3CNT){
    $D3JSEQ = explode('-',$D3JSEQ);
    $CNMSEQ = $D3JSEQ[0];
    $CNSSEQ = $D3JSEQ[1];
    $rs = true;
    $rsCount = array();

    $strSQL  = ' SELECT COUNT(*) AS COUNT';
    $strSQL .= ' FROM BQRYCND'; 
    $strSQL .= ' WHERE CNQRYN = ? ';
    $strSQL .= ' AND CNFILID = ? ';
    $strSQL .= ' AND CNMSEQ = ? ';
    $strSQL .= ' AND CNSSEQ = ? ';

    $params = array(
        $D3NAME,
        $D3FILID,
        $CNMSEQ,
        $CNSSEQ
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rsCount[] = $row['COUNT'];
            }
            if($rsCount[0] > 0){
                //構文
                $strSQL  = ' UPDATE BQRYCND ';
                $strSQL .= ' SET ';
                $strSQL .= ' CNCANL = ?, ';
                $strSQL .= ' CNCANF = ?, ';
                $strSQL .= ' CNCMBR = ?, ';
                $strSQL .= ' CNCANC = ?, ';
                $strSQL .= ' CNCANG = ?, ';
                $strSQL .= ' CNNAMG = ?, ';
                $strSQL .= ' CNNAMC = ?, ';
                $strSQL .= ' CNCAST = ?, ';
                $strSQL .= ' CNNAST = ?  ';
                $strSQL .= ' WHERE CNQRYN = ? ';
                $strSQL .= ' AND CNFILID = ? ';
                $strSQL .= ' AND CNMSEQ = ? ';
                $strSQL .= ' AND CNSSEQ = ? ';

                $params = array(
                    $D3CANL,
                    $D3CANF,
                    $D3CMBR,
                    $D3CANC,
                    $D3CANG,
                    $D3NAMG,
                    $D3NAMC,
                    $D3CAST,
                    $D3NAST,
                    $D3NAME,
                    $D3FILID,
                    $CNMSEQ,
                    $CNSSEQ
                );

                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = 'FAIL_INS';
                }else{
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $rs = 'FAIL_INS';
                        e_log('error:'.db2_stmt_errormsg.$strSQL.print_r($params,true));
                    }
                }
            }
            else{
                $rs = 'NOTEXIST_UPD';
            }
            
        }
    }

    
    return $rs;

}
/*
*SQLクエリー保存＆更新
*/
function fnUpdateBSQLCND($db2con,$D3CANL,$D3CANF,$D3CMBR,$D3CANC,$D3CAST,$D3CANG,$D3NAMG,$D3NAST,$D3NAMC,$D3NAME,$D3JSEQ){
    $TOTCOUNT=0;
    $rs = true;
    $data=fnBSQLCND_COUNT($db2con, $D3NAME,$D3JSEQ);
    if($data['result']!==true){
        $rs=$data['result'];
    }else{
        $TOTCOUNT=$data['COUNT'];
        if($TOTCOUNT>0){
            //構文
            $strSQL  = ' UPDATE BSQLCND ';
            $strSQL .= ' SET ';
            $strSQL .= ' CNCANL = ?, ';
            $strSQL .= ' CNCANF = ?, ';
            $strSQL .= ' CNCMBR = ?, ';
            $strSQL .= ' CNCANC = ?, ';
            $strSQL .= ' CNCANG = ?, ';
            $strSQL .= ' CNNAMG = ?, ';
            $strSQL .= ' CNNAMC = ?, ';
            $strSQL .= ' CNCAST = ?, ';
            $strSQL .= ' CNNAST = ?  ';
            $strSQL .= ' WHERE CNQRYN = ? ';
            $strSQL .= ' AND CNDSEQ = ? ';
            $params = array(
                $D3CANL,
                $D3CANF,
                $D3CMBR,
                $D3CANC,
                $D3CANG,
                $D3NAMG,
                $D3NAMC,
                $D3CAST,
                $D3NAST,
                $D3NAME,
                $D3JSEQ
            );
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $rs = 'FAIL_INS';
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $rs = 'FAIL_INS';
                    e_log('error:'.db2_stmt_errormsg.$strSQL.print_r($params,true));
                }
            }

        }else{
            $rs = 'NOTEXIST_UPD';
        }
    }
    return $rs;

}


function fnChkExistLib($db2con,$LIB,$LIBLIST,$LIBSNM){
    //e_log('ライブラリー取得：'.$LIB.'list:'.print_r($LIBLIST,true));
    $data = array();
    $params = array();
    $rs = true;
    if (($key = array_search('QTEMP', $LIBLIST)) !== false) {
        unset($LIBLIST[$key]);
    }
    if($LIB === 'QTEMP'){
        $rs = 'ISEXIST';
    }else{
        $strSQL  = ' SELECT A.SCHEMA_NAME ';
        $strSQL .= ' FROM  ' ;
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '        SYSTEM_SCHEMA_NAME SCHEMA_NAME ';
        $strSQL .= '        , SCHEMA_TEXT ';
        $strSQL .= '        FROM ';
        $strSQL .= '        '.SYSSCHEMASLIB.'/SYSSCHEMAS ';
        $strSQL .= '        WHERE SYSTEM_SCHEMA_NAME <> \'QTEMP\'';
        if($LIBSNM !== ''){
            if(SYSLIBCHK){
    	        $strSQL .= ' AND SCHEMA_NAME IN ';  
            }else{
                $strSQL .= ' AND SYSTEM_SCHEMA_NAME NOT IN ';
            }
            $strSQL .= '    ( ';
            $strSQL .= '        SELECT ';
            $strSQL .= '             CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
            $strSQL .= '        FROM ';
            $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
            if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
                $strSQL .= MAINLIB.'/DB2LBLS ';
                $strSQL .= '        WHERE LIBSNM = ? ';
                $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
                $strSQL .= '        AND LIBSFG = \'1\' ' ;
                array_push($params,$LIBSNM);
            }else{
                $strSQL .= ' QTEMP/DB2LBLS ';
            }
            $strSQL .= '    ) ';
        }
        $strSQL .= '    ) A ';
        if($LIB === '*USRLIBL' || $LIB === '*LIBL'){
            $strSQL .= ' WHERE A.SCHEMA_NAME IN  (';
            foreach($LIBLIST as $libVal){
               $strSQL .= ' ? ,';
                array_push($params,$libVal);
            }
            $strSQL = substr($strSQL, 0, -1);
            $strSQL .= ' )';
        }else{
            $strSQL .= ' WHERE A.SCHEMA_NAME = ? ' ;
            array_push($params,$LIB);
        }
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_SEL';
              e_log('ライブラリー取得：'.$strSQL.print_r($params,true).db2_stmt_errormsg());
        }else{
            e_log('ライブラリー取得：'.$strSQL.print_r($params,true));
            $r = db2_execute($stmt,$params);

            if($r === false){
                $rs = 'FAIL_SEL';
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                if($LIB === '*USRLIBL' || $LIB === '*LIBL'){
                    e_log('DATA:'.print_r($data,true));
                    if(count($data) === count($LIBLIST)){
                        $rs = 'ISEXIST';
                    }
                }else{
                     if(count($data) > 0){
                        $rs = 'ISEXIST';
                    }
                }
            }
        }
    }
    return $rs;
}

function fnChkExistFile($db2con,$LIB,$FILE,$LIBLIST){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT A.SYSTEM_TABLE_NAME ';
    $strSQL .= '      , A.SYSTEM_TABLE_SCHEMA ';
    $strSQL .= '      , A.TABLE_TEXT';
    $strSQL .= ' FROM  ' ;
    $strSQL .= '    QSYS2/SYSTABLES A ';
    $strSQL .= ' WHERE A.SYSTEM_TABLE_SCHEMA = ? ' ;
    $strSQL .= ' AND A.SYSTEM_TABLE_NAME = ? ' ;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($LIB === '*USRLIBL' || $LIB === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$FILE);
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
        }else{
            $params = array($LIB,$FILE);
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
            }
        }        
        if(count($data) === 0){
            $data = array('result' => 'NOTEXIST');
        }else{
            $data = umEx($data);
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}
function fnChkExistMBR($db2con,$LIB,$FILE,$MBR,$lblFlg){
    $data = array();
    $params = array();

    $strSQL .= '    SELECT ';
    $strSQL .= '        A.SYS_DNAME, ';
    $strSQL .= '        A.SYS_TNAME, ';
    $strSQL .= '        A.SYS_MNAME, ';
    $strSQL .= '        A.PARTNBR, ';
    if($lblFlg){
        $strSQL .= '        A.LABEL ';
    }else{
        $strSQL .= '        A.SYS_MNAME AS LABEL ';
    }
    $strSQL .= '    FROM ';
    $strSQL .= '        QSYS2/SYSPSTAT A';
    $strSQL .= '    WHERE ';
    $strSQL .= '        A.TABLE_SCHEMA = ? ';
    $strSQL .= '    AND A.TABLE_NAME = ? ';
    $strSQL .= '    AND A.SYS_MNAME = ?';
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log(db2_stmt_errormsg().'メンバーデータ取得：'.$strSQL);
    }else{
        $params = array($LIB,$FILE,$MBR);
        e_log('メンバーデータ取得SQL：'.$strSQL.print_r($params,true));
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
            e_log(db2_stmt_errormsg().'メンバーデータ取得：'.$strSQL.print_r($params,true));
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
        if(count($data) === 0){
             $data = array('result' => 'NOTEXIST');
        }else{
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}

function fnChkExistFld ($db2con,$Field,$FILEINFO,$LIBLIST){

    $TABLE_SCHEMA = $FILEINFO['SYSTEM_TABLE_SCHEMA'];
    $TABLE_NAME = $FILEINFO['SYSTEM_TABLE_NAME'];
    $COLNAME = $Field;

    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL  =' SELECT A.SYSTEM_COLUMN_NAME  AS COLUMN_NAME ';
    $strSQL .='       ,A.LENGTH ';
    $strSQL .='       ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG ==='1'){
        $strSQL .='       ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .='       ,A.COLUMN_HEADING ';
    }
    $strSQL .='       ,CASE A.DDS_TYPE';
    $strSQL .='         WHEN \'H\' THEN \'A\'';
    $strSQL .='         WHEN \'J\' THEN \'0\'';
    $strSQL .='         WHEN \'E\' THEN \'0\'';
    $strSQL .='         ELSE A.DDS_TYPE ';
    $strSQL .='         END AS DDS_TYPE';
    $strSQL .='       ,A.ORDINAL_POSITION ';
    $strSQL .='       ,A.CCSID ';
    $strSQL .=' FROM '. SYSCOLUMN2 . ' A ';
    $strSQL .=' WHERE A.TABLE_SCHEMA = ? ';
    //$strSQL .=' AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\')';
//    $strSQL .=' AND A.DDS_TYPE NOT IN(\'5\')';
    $strSQL .=' AND A.TABLE_NAME = ? ';
    $strSQL .=' AND A.SYSTEM_COLUMN_NAME = ? ';
    
    $params = array($TABLE_SCHEMA,$TABLE_NAME,$COLNAME);
    $stmt = db2_prepare($db2con,$strSQL);
    
    if($stmt === false){
        $data = array('result' => true,'data'=> 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => true,'data'=>'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $data = array('result' => 'ISEXIST' ,'data' => umEx($data));
            }else{
                $data = array('result' => true,'data'=>'NOTEXIST');
            }
        }
    }
    return $data;
}
function fnGetD1LIBL($db2con,$D1NAME){
    $data   = array();
    $params = array();
    $strSQL = '';
    $D1LIBL = '';

    $strSQL  =   '   SELECT D1NAME ';
    $strSQL .=   '        , D1LIBL ';
    $strSQL .=   '   FROM FDB2CSV1 ';
    $strSQL .=   '   WHERE D1NAME = ? ';

    $params = array($D1NAME);
    $stmt = db2_prepare($db2con, $strSQL);
    
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if(count($data) > 0){
                $D1LIBL = $data[0]['D1LIBL'];
                $data = array(
                    'result' => true,
                    'D1LIBL' => $D1LIBL
                );
             }else{
                $data = array(
                    'result' => 'NOTEXIST_GET',
                );
            }
        }
    }
    return $data;
}

function fnChkSetting($db2con,$D3CANL,$D3CANF,$D3CMBR,$D3CANC,$D3NAMC,$D1LIBL,$D3CANCData,$D3NAMCData){
    $data   = array();
    $params = array();
    $rtn = 0;
    //$rs = dropFileMBR($db2con,$D3CANL,$D3CANF,$D3CMBR,'QTEMP_'.$D3CANF);
    $rs = setFileMBR($db2con,$D3CANL,$D3CANF,$D3CMBR,'QTEMP_'.$D3CANF);
    if($rs !== true){
        e_log('テーブル設定失敗');
        $data = array('result' => showMsg($rs));
        $rtn = 1;
    }
    if($rtn === 0){
        $strSQL = ' SELECT   ';
        if($D3CANCData['CCSID'] === '65535'){
            if($D3CANData['COLUMN_NAME'] === 'BREAKLVL' || $D3CANData['COLUMN_NAME'] === 'OVERFLOW'){
                $strSQL .= 'CAST('.$D3CANC .' AS CHAR(4) CCSID 5026) AS '.$D3CANC;
            }else{
                $strSQL .= 'CAST('.$D3CANC .' AS CHAR('.$D3CANCData['LENGTH'].') CCSID 5026) AS '.$D3CANC;
            }

        }else{
            $strSQL .= $D3CANC ;
        }
        if($D3NAMC !== ''){
            if($D3NAMCData['CCSID'] === '65535'){

                if($D3NAMCData['COLUMN_NAME'] === 'BREAKLVL' || $D3NAMCData['COLUMN_NAME'] === 'OVERFLOW'){
                    $strSQL .= ', CAST('.$D3NAMC .' AS CHAR(4) CCSID 5026) AS '.$D3NAMC;
                }else{
                    $strSQL .= ', CAST('.$D3NAMC .' AS CHAR('.$D3NAMCData['LENGTH'].') CCSID 5026) AS '.$D3NAMC;
                }


            }else{
                $strSQL .= ' ,' . $D3NAMC;
            }
        }
        $strSQL .= ' FROM ';
        if($D3CANL === '*LIBL' || $D3CANL === '*USRLIBL'){
            //$strSQL .= ' FROM ' . $D3CANF;
            $strSQL .=  ' QTEMP/'.'QTEMP_'.$D3CANF .' ';
            //$db2LibCon = cmDb2ConLib($D1LIBL);
            e_log('SQL実行エラー：'.$D1LIBL.'SQL文：'.$strSQL);
            $stmt = db2_prepare($db2con, $strSQL);
        }else{
            $strSQL .=  ' QTEMP/'.'QTEMP_'.$D3CANF .' ';
            $stmt = db2_prepare($db2con, $strSQL);
        }
        if ($stmt === false) {
            $data = array(
                'result' => '指定の設定でSQLの実行に失敗しました。</br> 設定をご確認ください。</br>【エラーコード：'. db2_stmt_errormsg().'】'
            );
            e_log('SQL実行エラー：'.$D1LIBL.'SQL文：'.$strSQL);
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array(
                    'result' => '指定の設定でSQLの実行に失敗しました。</br> 設定をご確認ください。</br>【エラーコード：'. db2_stmt_errormsg().'】'
                );
            } else {
                $row =  db2_fetch_assoc($stmt);
                if($row === false && db2_stmt_errormsg() !== '' ){
                    $data = array(
                        'result' => '指定の設定でSQLの実行に失敗しました。</br> 設定をご確認ください。</br>【エラーコード：'. db2_stmt_errormsg().'】'
                    );
                }else{
                    $data[] = $row;
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    $data = umEx($data,false);
                    $data = array(
                        'result' => true,
                        'data' => $data
                    );
                }
            }
        }
    }
    if($D3CANL === '*LIBL' || $D3CANL === '*USRLIBL'){
        cmDb2Close($db2LibCon);
    }
    return $data;

}
/*
*-------------------------------------------------------* 
* ヘルプ情報更新
*-------------------------------------------------------*
*/

function cmChkCnd( $db2con, $D3NAME, $D3FILID, $D3JSEQ, $D3CNT, $D1WEBF,$D3FLD){
    $data = array();
    $strSQL = '';

    if ( $D1WEBF === '1' ) {

        $D3JSEQ = explode('-',$D3JSEQ);
        $CNMSEQ = $D3JSEQ[0];
        $CNSSEQ = $D3JSEQ[1];

        $strSQL .=  '   SELECT ';
        $strSQL .=  '       A.CNQRYN , ';
        $strSQL .=  '       A.CNFILID , ';
        $strSQL .=  '       A.CNMSEQ , ';
        $strSQL .=  '       A.CNSSEQ , ';
        $strSQL .=  '       A.CNAOKB , ';
        $strSQL .=  '       A.CNFLDN , ';
        $strSQL .=  '       A.CNCKBN , ';
        $strSQL .=  '       A.CNSTKB , ';
        $strSQL .=  '       A.CNCANL , ';
        $strSQL .=  '       A.CNCANF , ';
        $strSQL .=  '       A.CNCMBR , ';
        $strSQL .=  '       A.CNCANC , ';
        $strSQL .=  '       A.CNCANG , ';
        $strSQL .=  '       A.CNNAMG , ';
        $strSQL .=  '       A.CNNAMC , ';
        $strSQL .=  '       A.CNDFMT , ';
        $strSQL .=  '       A.CNDSFL , ';
        $strSQL .=  '       A.CNDFIN , ';
        $strSQL .=  '       A.CNDFPM , ';
        $strSQL .=  '       A.CNDFDY , ';
        $strSQL .=  '       A.CNDFFL , ';
        $strSQL .=  '       A.CNDTIN , ';
        $strSQL .=  '       A.CNDTPM , ';
        $strSQL .=  '       A.CNDTDY , ';
        $strSQL .=  '       A.CNDTFL , ';
        $strSQL .=  '       A.CNCAST , ';
        $strSQL .=  '       A.CNNAST ';
        $strSQL .=  '   FROM ';
        $strSQL .=  '       BQRYCND A ';
        $strSQL .=  '   WHERE '; 
        $strSQL .=  '       A.CNQRYN  = ? ';
        $strSQL .=  '   AND A.CNFILID = ? ';
        $strSQL .=  '   AND A.CNMSEQ  = ? ';
        $strSQL .=  '   AND A.CNSSEQ  = ? ';
        $strSQL .=  '   AND A.CNFLDN  = ? ';

        $params = array(
                    $D3NAME ,
                    $D3FILID ,
                    $CNMSEQ ,
                    $CNSSEQ ,
                    $D3FLD
                );
    } else {
        $strSQL .=  '   SELECT ';
        $strSQL .=  '       A.D3NAME , ';
        $strSQL .=  '       A.D3FILID , ';
        $strSQL .=  '       A.D3JSEQ , ';
        $strSQL .=  '       A.D3CNT , ';
        $strSQL .=  '       A.D3ANDOR , ';
        $strSQL .=  '       A.D3FLD , ';
        $strSQL .=  '       A.D3CND , ';
        $strSQL .=  '       A.D3DAT , ';
        $strSQL .=  '       A.D3USEL , ';
        $strSQL .=  '       A.D3TYPE , ';
        $strSQL .=  '       A.D3LEN , ';
        $strSQL .=  '       A.D3CANL , ';
        $strSQL .=  '       A.D3CANF , ';
        $strSQL .=  '       A.D3CMBR , ';
        $strSQL .=  '       A.D3CANC , ';
        $strSQL .=  '       A.D3CANG , ';
        $strSQL .=  '       A.D3NAMG , ';
        $strSQL .=  '       A.D3NAMC , ';
        $strSQL .=  '       A.D3DFMT , ';
        $strSQL .=  '       A.D3DSFL , ';
        $strSQL .=  '       A.D3DFIN , ';
        $strSQL .=  '       A.D3DFPM , ';
        $strSQL .=  '       A.D3DFDY , ';
        $strSQL .=  '       A.D3DFFL , ';
        $strSQL .=  '       A.D3DTIN , ';
        $strSQL .=  '       A.D3DTPM , ';
        $strSQL .=  '       A.D3DTDY , ';
        $strSQL .=  '       A.D3DTFL , ';
        $strSQL .=  '       A.D3CAST , ';
        $strSQL .=  '       A.D3NAST '; 
        $strSQL .=  '   FROM  ';
        $strSQL .=  '       FDB2CSV3 A ';
        $strSQL .=  '   WHERE  ';
        $strSQL .=  '       A.D3NAME = ? ';
        $strSQL .=  '   AND A.D3FILID = ? ';
        $strSQL .=  '   AND A.D3JSEQ = ? ';
        $strSQL .=  '   AND A.D3FLD = ? ';

        $params = array(
            $D3NAME ,
            $D3FILID ,
            $D3JSEQ ,
            $D3FLD
        );
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $data = array('result' => true,'data' => $data);
            }else{
                $data = array('result' => 'NOTEXIST_GET');
            }
        }
    }
    return $data;
}
/*
*SQLクエリーのためチェック
*/
function cmChkBSQLCND( $db2con, $D3NAME,$D3JSEQ){
    $data = array();
    $rs=true;
    $TOTCOUNT=0;
    $data=fnBSQLCND_COUNT($db2con, $D3NAME,$D3JSEQ);
    if($data['result']!==true){
        $rs=$data['result'];
    }else{
        $TOTCOUNT=$data['COUNT'];
        if($TOTCOUNT<=0){
           $rs= 'NOTEXIST_GET';
        }
    }
    return $rs;
}
/*
*SQLクエリーのためチェック
*/
function fnBSQLCND_COUNT($db2con, $D3NAME,$D3JSEQ){
    $data = array();
    $rs=true;
    $TOTCOUNT=0;
    $strSQL = '';
    $strSQL .=  'SELECT COUNT(*) AS TOTCOUNT ';
    $strSQL .=  'FROM BSQLCND AS A WHERE ';
    $strSQL .=  'A.CNQRYN  = ? ';
    $strSQL .=  'AND A.CNDSEQ = ? ';

    $params = array($D3NAME,$D3JSEQ);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs= 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs= 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $TOTCOUNT = $row['TOTCOUNT'];
            }
        }
    }
    $data=array('result'=>$rs,'COUNT'=>$TOTCOUNT);
    return $data;
}