<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$D1INFO = $_POST['D1INFO'];
$D1INFG = $_POST['D1INFG'];
$D1INFGR = $_POST['D1INFGR'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);
$D1INFO = cmHscDe($D1INFO);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
            $userData  = umEx($rs['data']);
            if($userData[0]['WUAUTH'] === '2'){
                $rs = cmChkKenGen($db2con,'12',$userData[0]['WUSAUT']);//'12' =>ユーザー補助
                if($rs['result'] !== true){
                    $rtn = 3;
                    $msg =  showMsg($rs['result'],array('ユーザー補助の権限'));
                }
            }
     }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $D1NAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのユーザー補助の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('検索候補の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}

//バリデーションチェック
if($rtn === 0){
    if(!checkMaxLen($D1INFO,1536)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('ヘルプの文章'));
        $focus = 'D1INFO';
    }
}
/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/


if($rtn === 0){
    $rs = fnUpdateFDB2CSV1($db2con,$D1NAME,$D1INFO,$D1INFG,$D1INFGR);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ヘルプ情報更新
*-------------------------------------------------------*
*/

function fnUpdateFDB2CSV1($db2con,$D1NAME,$D1INFO,$D1INFG,$D1INFGR){


    $rs = true;

    //構文
    $strSQL  = ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= ' D1INFO = ?, ';
    $strSQL .= ' D1INFG = ?, ';
	$strSQL .= ' D1INFGR = ? ';
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array(
        $D1INFO,
        $D1INFG,
 		$D1INFGR,
        $D1NAME
    );
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r =db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;

}