<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../base/getTblMBR.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$LIBNAME = (isset($_POST['LIBNAME']))?cmMer($_POST['LIBNAME']):'';
$LIBDATA = json_decode($_POST['LIBDATA'],true);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$RTCD = '';
$checkdata = false;
/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $auth = $rs['data'][0];
        $WUAUTH = cmMer($auth['WUAUTH']);
        if($WUAUTH === '2'){
            $SAUT = cmMer($auth['WUSAUT']);
            $rs = cmSelDB2AGRT($db2con,$SAUT);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg =  showMsg($rs['result']);
            }else{
                $authData = umEx($rs['data']);
                foreach($authData as $key => $value){
                        $authInfo[$value['AGMNID']] = $value['AGFLAG'];
                }
                if($authInfo['3'] !== '1' && $authInfo['6'] !== '1' && $authInfo['7'] !== '1'){
                    if($authInfo['8'] !== '1' && $authInfo['9'] !== '1' && $authInfo['10'] !== '1'){
                        if($authInfo['11'] !== '1' && $authInfo['12'] !== '1' && $authInfo['13'] !== '1'){
                            if($authInfo['14'] !== '1' && $authInfo['15'] !== '1' && $authInfo['16'] !== '1' && $authInfo['30'] !== '1'){
                                $rtn = 2;
                                 $msg =  showMsg('NOTEXIST',array('クエリー作成の権限'));
                            }
                        }
                    }
                }
            }
        }
    }
}


if($rtn === 0){
    //必須チェック
    e_log('LIB Data'.print_r(count($LIBDATA),true));
    if(count($LIBDATA) === 0){
        $rtn = 1;
        $msg = showMsg('FAIL_DISPLAY');
        
    }
}

if(count($LIBDATA) !== 0){
 if($rtn === 0){
  foreach($LIBDATA as $key => $col){
    if($col[CHKNAME] === true){
        $checkdata = true;
        
     }
  }
}

if($checkdata !== true){
     $rtn = 1;
     $msg = showMsg('FAIL_SLEC',array('定義名'));
    
 }
}


 if($rtn === 0){
  foreach($LIBDATA as $key => $col){
  e_log('CHECK RESULT'.print_r($col[CHKNAME],true));
   if($col[CHKNAME] === true){
    $rs = fnCheckQRYNAME($db2con,$col['ODOBNM']);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('定義名'.'"'.$col[ODOBNM].'"'));
        $focus = 'D1LIBITMLIBNM';
        break;
    
      }
    }
  }
}
    
cmDb2Close($db2con);

$db2con = cmDb2Con();

if($rtn === 0){
  foreach($LIBDATA as $key => $col){
   e_log('QRY CHECK RESULT'.print_r($col[CHKNAME],true));
   if($col[CHKNAME] === true){
    
    $rs = cmCallCNVQRYCS($db2con, $LIBNAME,$col['ODOBNM'],$col['ODOBTX'],$RTCD);
    e_log('EDT RESULT'.print_r($rs,true));
    if($rs === false){
         $rtn = 1;
         $msg = showMsg('FAIL_QRY_RUN');
       }
    }
  }
}


cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'LIBNAME' => $LIBNAME,
    'LIBDATA' => $LIBDATA
   
);
echo(json_encode($rtnAry));

function fnGetLibData($db2con,$LibName){

    $data = array();
    $rs = true;

    $strSQL = ' SELECT A.SCHEMA_NAME ';
    $strSQL .= ' FROM ( SELECT B.SCHEMA_NAME';
    $strSQL .= ' , B.SCHEMA_TEXT';
    $strSQL .= ' FROM  ';
    $strSQL .= '    (SELECT * FROM ';
    $strSQL .= '    QSYS2/SYSSCHEMAS ';
    //$strSQL .= ' WHERE SYSTEM_SCHEMA_NAME <> \'QTEMP\' ';
    $strSQL .= '     ) B';
    $strSQL .= ' WHERE B.SCHEMA_NAME <> \'\'';
    $strSQL .= ' ) as A ';
    $strSQL .= ' LEFT JOIN  (SELECT * FROM DB2LBLS ) AS C ON A.SCHEMA_NAME = C.LIBSID ';
    
    if($LibName != ''){
        $strSQL .= ' WHERE A.SCHEMA_NAME IN ( ? )';
          $params = array(
             $LibName
          );
         
     }

   $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                    $rs = 'NOTEXIST';
            }
            
        }
    }
    return $rs;
}

function fnCheckQRYNAME($db2con,$ODOBNM){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT D1NAME ';
    $strSQL .= ' FROM  FDB2CSV1 ' ;
    $strSQL .= ' WHERE D1NAME = ? ' ;
   
   $params = array(
        $ODOBNM
    );
 
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
                e_log('COUNT DATA'.count($data));
                if(count($data) !== 0){
                    $rs = 'ISEXIST';
                }
            
        }
    }
    return $rs;
}

