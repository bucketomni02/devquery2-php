<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D3NAME = $_POST['D3NAME'];
$D3FILID = $_POST['D3FILID'];
$D3JSEQ = $_POST['D3JSEQ'];
$D3CNT = $_POST['D3CNT'];
$D1WEBF = $_POST['D1WEBF'];
$D3FLD = $_POST['D3FLD'];
$D1CFLG=(isset($_POST['D1CFLG'])?$_POST['D1CFLG']:'');

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
            $userData  = umEx($rs['data']);
            if($userData[0]['WUAUTH'] === '2'){
                $rs = cmChkKenGen($db2con,'13',$userData[0]['WUSAUT']);//'13' =>検索候補
                if($rs['result'] !== true){
                    $rtn = 2;
                    $msg =  showMsg($rs['result'],array('検索候補の権限'));
                }
            }
     }
}
/** クエリー存在チェック**/
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$D3NAME,'');
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 1;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D3NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての検索候補の権限がありません。');
             $msg =  showMsg('FAIL_QRY_USR',array('検索候補の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($D1WEBF !== '1'){    
    if($rtn === 0){
        //ユーザー名取得
        $rs = getCldFDB2CSV3($db2con,$D3NAME,$D3FILID,$D3JSEQ,$D3CNT,$D3FLD);
        if($rs['result'] !== true){
            $msg = showMsg($rs['result'],array('クエリー'));
            $rtn = 1;
        }else{
            $data = $rs['data'];
            if(count($data) === 0){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET',array('フィールド'));
            }
        }
    }

}else{
    //SQLのクエリーと普通のクエリー
    if($D1CFLG==='1'){
        if($rtn === 0){
            $rs=getCldBSQLCND($db2con,$D3NAME,$D3JSEQ);
            if($rs['result'] !== true){
                $msg = showMsg($rs['result'],array('クエリー'));
                $rtn = 1;
            }else{
                $data = $rs['data'];
                if(count($data) === 0){
                    $rtn = 1;
                    $msg = showMsg('NOTEXIST_GET',array('フィールド'));
                }
            }
        }
    }else{
        if($rtn === 0){
            $rs = getCldBQRYCND($db2con,$D3NAME,$D3FILID,$D3JSEQ,$D3FLD);
            if($rs['result'] !== true){
                $msg = showMsg($rs['result'],array('クエリー'));
                $rtn = 1;
            }else{
                $data = $rs['data'];
                if(count($data) === 0){
                    $rtn = 1;
                    $msg = showMsg('NOTEXIST_GET',array('フィールド'));
                }
            }
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'aaDATA' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg,
    'D1WEBF' => $D1WEBF
);
echo(json_encode($rtn));

/**
* FDB2CSV3のカレンダ設定取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $D3NAME  D3NAME
* @param String  $D3FILID D3FILID
* @param String  $D3JSEQ  D3JSEQ
* @param String  $D3CNT 　D3CNT
* 
*/

function getCldFDB2CSV3($db2con,$D3NAME,$D3FILID,$D3JSEQ,$D3CNT,$D3FLD){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT D3DFMT,D3DSFL,D3CND AS CONDITION ,';
    $strSQL .= ' D3DFIN AS SHOWFLG1, ';
    $strSQL .= ' D3DFPM AS CAL1, ';
    $strSQL .= ' D3DFDY AS NUM1, ';
    $strSQL .= ' D3DFFL AS TIME1, ';
    $strSQL .= ' D3DTIN AS SHOWFLG2, ';
    $strSQL .= ' D3DTPM AS CAL2, ';
    $strSQL .= ' D3DTDY AS NUM2, ';
    $strSQL .= ' D3DTFL AS TIME2, ';
    $strSQL .= ' D3DSCH AS SCHED ';
    $strSQL .= ' FROM FDB2CSV3';
    $strSQL .= ' WHERE D3NAME = ? ';
    $strSQL .= ' AND D3FILID = ? ';
    $strSQL .= ' AND D3JSEQ = ? ';
    $strSQL .= ' AND D3CNT = ? ';
    $strSQL .= ' AND D3FLD = ? ';

    $params = array(
        $D3NAME,
        $D3FILID,
        $D3JSEQ,
        $D3CNT,
        $D3FLD
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/**
* BQRYCNDのカレンダ設定取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $D3NAME  D3NAME
* @param String  $D3FILID D3FILID
* @param String  $D3JSEQ  D3JSEQ
* 
*/
function getCldBQRYCND($db2con,$D3NAME,$D3FILID,$D3JSEQ,$D3FLD){

    $data = array();
    $params = array();
    $D3JSEQ = explode('-',$D3JSEQ);
    $CNMSEQ = $D3JSEQ[0];
    $CNSSEQ = $D3JSEQ[1];

    $strSQL  = ' SELECT CNDFMT,CNDSFL,CNCKBN AS CONDITION , ';
    $strSQL  .= ' CNDFIN AS SHOWFLG1, ';
    $strSQL  .= ' CNDFPM AS CAL1, ';
    $strSQL  .= ' CNDFDY AS NUM1, ';
    $strSQL  .= ' CNDFFL AS TIME1, ';
    $strSQL  .= ' CNDTIN AS SHOWFLG2, ';
    $strSQL  .= ' CNDTPM AS CAL2, ';
    $strSQL  .= ' CNDTDY AS NUM2, ';
    $strSQL  .= ' CNDTFL AS TIME2, ';
    $strSQL  .= ' CNDSCH AS SCHED ';
    $strSQL .= ' FROM BQRYCND';
    $strSQL .= ' WHERE CNQRYN = ? ';
    $strSQL .= ' AND CNFILID = ? ';
    $strSQL .= ' AND CNMSEQ = ? ';
    $strSQL .= ' AND CNSSEQ = ? ';
    $strSQL .= ' AND CNFLDN = ? ';

    $params = array(
        $D3NAME,
        $D3FILID,
        $CNMSEQ,
        $CNSSEQ,
        $D3FLD
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/**
* BSQLCNDのカレンダ設定取得
* 
*/
function getCldBSQLCND($db2con,$D3NAME,$D3JSEQ){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT CNDFMT,CNDSFL,\'\' AS CONDITION , ';
    $strSQL  .= ' CNDFIN AS SHOWFLG1, ';
    $strSQL  .= ' CNDFPM AS CAL1, ';
    $strSQL  .= ' CNDFDY AS NUM1, ';
    $strSQL  .= ' CNDFFL AS TIME1, ';
    $strSQL  .= ' CNDTIN AS SHOWFLG2, ';
    $strSQL  .= ' CNDTPM AS CAL2, ';
    $strSQL  .= ' CNDTDY AS NUM2, ';
    $strSQL  .= ' CNDTFL AS TIME2, ';
    $strSQL  .= ' CNDSCH AS SCHED ';
    $strSQL .= ' FROM BSQLCND';
    $strSQL .= ' WHERE CNQRYN = ? ';
    $strSQL .= ' AND CNDSEQ = ? ';
    $params = array(
        $D3NAME,$D3JSEQ
    );
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
