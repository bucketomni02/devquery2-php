<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$D2FLD = $_POST['D2FLD'];
$D2FILID = $_POST['D2FILID'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'14',$userData[0]['WUSAUT']);//'14' =>制御レベル設定
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('制御レベル設定の権限'));
            }
        }
    }
}

//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $D1NAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての制御レベル設定の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('制御レベル設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    //ユーザー名取得
    $rs = getD2TYPE($db2con,$D1NAME,$D2FLD,$D2FILID);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result'],array('クエリー'));
        $rtn = 1;
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'aaDATA' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));

/**
* D2TYPEの取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $D1NAME  クエリ名
* @param String  $D2FLD   カラムフラッグ
* @param String  $D2FILID カラムフラッグ
* 
*/

function getD2TYPE($db2con,$D1NAME,$D2FLD,$D2FILID){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT D2TYPE FROM ( ';
    $strSQL .= ' SELECT D2TYPE,D2NAME,D2FILID,D2FLD,D2CSEQ ';
    $strSQL .= ' FROM FDB2CSV2 ';
    $strSQL .= ' UNION ALL ';
    $strSQL .= ' SELECT D5TYPE ';
    $strSQL .= ' AS D2TYPE,D5NAME AS D2NAME,9999 AS D2FILID,D5FLD AS D2FLD,D5CSEQ AS D2CSEQ ';
    $strSQL .= ' FROM FDB2CSV5 ';
    $strSQL .= ' ) AS A ';
    $strSQL .= ' WHERE D2NAME = ?';
    $strSQL .= ' AND D2FLD=? ';
    $strSQL .= ' AND D2FILID= ? ';
    //$strSQL .= ' AND D2CSEQ > 0 ';

    $params = array(
        $D1NAME,
        $D2FLD,
        $D2FILID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET');
 
            }else{
  
               $data = array('result' => true,'data' => $data);
            }
        }
    }
    return $data;
}
