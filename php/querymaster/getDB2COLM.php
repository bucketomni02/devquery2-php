<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$DCNAME = $_POST['DCNAME'];
$DCNAMEOLD = $_POST['DCNAMEOLD'];
$COPYFLG = $_POST['COPYFLG'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/

$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);



//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'14',$userData[0]['WUSAUT']);//'14' =>制御レベル設定
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('制御レベル設定の権限'));
            }
        }
    }
}
if($rtn === 0){
    if($COPYFLG !== '1'){
        $DCNAME = $DCNAME;
    }else{
       $DCNAME = $DCNAMEOLD;
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $DCNAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$DCNAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての制御レベル設定の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('制御レベル設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}

if($rtn === 0){
    //クエリデータに集計されたデータの全件カウント取得
    $rs1 = cmGetDB2COLM($db2con,$DCNAME);
    if($rs1['result'] !== true){
        $msg = showMsg($rs1['result']);
        $rtn = 2;
    }
}



cmDb2Close($db2con);

/**return**/
$rtn = array(
    'aaData' => umEx($rs1['data'],true),
    'RTN' => $rtn,
    'MSG' => $msg,
    'USERAUTH' => $userData[0]['WUAUTH']
);

echo(json_encode($rtn));
