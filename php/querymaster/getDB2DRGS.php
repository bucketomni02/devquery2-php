<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$DRKMTN = (isset($_POST['DRKMTN'])?$_POST['DRKMTN']:'');//MAINQUERY
$DRKFID = (isset($_POST['DRKFID'])?$_POST['DRKFID']:'');
$DRKFLID = (isset($_POST['DRKFLID'])?$_POST['DRKFLID']:'');
//htmlspecialcharsをデコード
$DRKMTN = cmHscDe($DRKMTN);
$DRKFID = cmHscDe($DRKFID);
$DRKFLID = cmHscDe($DRKFLID);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'10',$userData[0]['WUSAUT']);//'10' => 詳細情報設定権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('詳細情報設定の権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $DRKMTN, '');
    if($chkQry['result'] !== true ) {
        $rtn = 2;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$DRKMTN,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 2;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての詳細情報設定の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('詳細情報設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $rs = cmCountDB2WDFL($db2con,$DRKMTN,$DRKFLID,$DRKFID);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $COUNT = $rs['COUNT'];
        if($COUNT>0){
            $msg = showMsg('DRILL_FAIL');
            $rtn = 1;
        }
    }
}
if($rtn === 0){
    $rs = fnGetDB2DRGS($db2con,$DRKMTN,$DRKFID,$DRKFLID);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'data' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg,
   'DRKMTN' =>$DRKMTN,
   'DRKFID' =>$DRKFID,
   'DRKFLID' =>$DRKFLID,
    'licenseDrilldown' => $licenseDrilldown
);
echo(json_encode($rtn));
/*
*-------------------------------------------------------* 
* DB2DRGS
*-------------------------------------------------------*
*/

function fnGetDB2DRGS($db2con,$DRKMTN,$DRKFID,$DRKFLID){
    $rs = true;
    //構文
    $strSQL  = ' SELECT * FROM DB2DRGS WHERE ';
    $strSQL  .= ' DRKMTN = ?  AND DRKFID = ?  AND DRKFLID = ? ';
    $params = array($DRKMTN,$DRKFID,$DRKFLID);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
