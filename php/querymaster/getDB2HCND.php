<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$DHNAME  = $_POST['DHNAME'];
$DHMSEQ  = $_POST['DHMSEQ'];
$DHSSEQ  = $_POST['DHSSEQ'];
$DHFILID = $_POST['DHFILID'];
$D1WEBF  = $_POST['D1WEBF'];
$D3FLD  = $_POST['D3FLD'];
$D1CFLG=(isset($_POST['D1CFLG'])?$_POST['D1CFLG']:'');
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$DHNAME  = cmHscDe($DHNAME);
$DHMSEQ  = cmHscDe($DHMSEQ);
$DHSSEQ  = cmHscDe($DHSSEQ);
$DHFILID = cmHscDe($DHFILID);
$D1WEBF  = cmHscDe($D1WEBF);
$D3FLD   = cmHscDe($D3FLD);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $chkUsr = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if( $chkUsr['result'] !== true ) {
        $rtn = 2;
        $msg = showMsg($chkUsr['result'],array('ユーザー'));
    }else{
        $userData  = umEx($chkUsr['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'13',$userData[0]['WUSAUT']);//'13' =>検索候補
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('検索候補の権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery($db2con,'', $DHNAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$DHNAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての検索候補の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('検索候補の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
//定義の検索条件ヘルプ情報取得
if($rtn === 0){
    if($D1CFLG==='1'){
        $rs = getBSQLCND($db2con,$DHNAME,$DHMSEQ);
        if( $rs['result'] !== true ){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('検索条件'));
        } else {
            $data = ($rs['data']);
        }
    }else{
        $rs = getDB2HCND($db2con, $DHNAME, $DHFILID, $DHMSEQ, $DHSSEQ, $D1WEBF,$D3FLD);
        if( $rs['result'] !== true ){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('検索条件'));
        } else {
            $data = ($rs['data']);
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => $data
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 定義の検索条件ヘルプ情報取得
*-------------------------------------------------------*
*/
function getDB2HCND($db2con, $DHNAME, $DHFILID, $DHMSEQ, $DHSSEQ, $D1WEBF,$D3FLD){
    $data = array();
    $params = array();
    $strSQL = '';
    // WEBで作成された定義の場合
    if( $D1WEBF === '1' ){
        $strSQL .=  '   SELECT ';
        $strSQL .=  '       A.CNQRYN AS DHNAME , ';
        $strSQL .=  '       A.CNFILID AS DHFILID , ';
        $strSQL .=  '       A.CNMSEQ AS DHMSEQ , ';
        $strSQL .=  '       A.CNSSEQ AS DHSSEQ , ';
        $strSQL .=  '       B.DHINFO AS DHINFO , ';
        $strSQL .=  '       B.DHINFG AS DHINFG ';
        $strSQL .=  '   FROM ';
        $strSQL .=  '       BQRYCND A ';
        $strSQL .=  '   LEFT JOIN DB2HCND B ';
        $strSQL .=  '   ON A.CNQRYN = B.DHNAME ';
        $strSQL .=  '   AND A.CNFILID  = B.DHFILID ';
        $strSQL .=  '   AND A.CNMSEQ = B.DHMSEQ ';
        $strSQL .=  '   AND A.CNSSEQ = B.DHSSEQ ';
        $strSQL .=  '   WHERE '; 
        $strSQL .=  '       A.CNQRYN  = ? ';
        $strSQL .=  '   AND A.CNFILID = ? ';
        $strSQL .=  '   AND A.CNMSEQ  = ? ';
        $strSQL .=  '   AND A.CNSSEQ  = ? ';
        $strSQL .=  '   AND A.CNFLDN  = ? ';

        $params = array(
                    $DHNAME ,
                    $DHFILID ,
                    $DHMSEQ ,
                    $DHSSEQ ,
                    $D3FLD
                );
    }else{
        $strSQL .=  '   SELECT ';
        $strSQL .=  '       A.D3NAME AS DHNAME , ';
        $strSQL .=  '       A.D3FILID AS DHFILID , ';
        $strSQL .=  '       \'0\' AS DHMSEQ , ';
        $strSQL .=  '       A.D3JSEQ AS DHSSEQ , ';
        $strSQL .=  '       B.DHINFO , ';
        $strSQL .=  '       B.DHINFG ';
        $strSQL .=  '   FROM ';
        $strSQL .=  '       FDB2CSV3 A ';
        $strSQL .=  '   LEFT JOIN DB2HCND B ';
        $strSQL .=  '   ON A.D3NAME = B.DHNAME ';
        $strSQL .=  '   AND A.D3FILID  = B.DHFILID ';
        $strSQL .=  '   AND A.D3JSEQ = B.DHSSEQ ';
        $strSQL .=  '   AND B.DHMSEQ = \'0\' ';
        $strSQL .=  '   WHERE '; 
        $strSQL .=  '       A.D3NAME = ? ';
        $strSQL .=  '   AND A.D3FILID = ? ';
        $strSQL .=  '   AND A.D3JSEQ = ? ';
        $strSQL .=  '   AND A.D3FLD = ? ';

        $params = array(
            $DHNAME ,
            $DHFILID ,
            $DHSSEQ ,
            $D3FLD
        );
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $data = array('result' => true,'data' => umEx($data,false));
            }else{
                $data = array('result' => 'NOTEXIST_GET');
            }
        }
    }
    return $data;
}
/**
* BSQLCNDのカレンダ設定取得
* 
*/
function getBSQLCND($db2con,$DHNAME,$DHMSEQ){
    $data = array();
    $params = array();
    $strSQL  = ' SELECT CNQRYN,';
    $strSQL .=  '\'\' AS DHFILID , ';
    $strSQL .=  '\'0\' AS DHMSEQ , ';
    $strSQL .=  'A.CNDSEQ AS DHSSEQ , ';
    $strSQL .=  'B.DHINFO AS DHINFO , ';
    $strSQL .=  'B.DHINFG AS DHINFG ';
    $strSQL .= ' FROM BSQLCND AS A ';
    $strSQL .= ' LEFT JOIN DB2HCND B ';
    $strSQL .= ' ON A.CNQRYN = B.DHNAME';
    $strSQL .= ' AND A.CNDSEQ= B.DHMSEQ';
    $strSQL .= ' WHERE A.CNQRYN = ? ';
    $strSQL .= ' AND A.CNDSEQ = ? ';
    $params = array(
        $DHNAME,$DHMSEQ
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $data = array('result' => true,'data' => umEx($data,false));
            }else{
                $data = array('result' => 'NOTEXIST_GET');
            }
        }
    }
    return $data;
}

