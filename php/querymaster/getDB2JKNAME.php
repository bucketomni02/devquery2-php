//文字の色付け（条件付き書式）
<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'9',$userData[0]['WUSAUT']);//'9' => EXCELテンプレート
            if($rs['result'] !== true){
                $rtn = 4;
                $msg =  showMsg($rs['result'],array('EXCELテンプレートの権限'));
            }
        }
    }
}

//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $D1NAME, '');
    if($chkQry['result'] !== true ) {
        $rtn = 2;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 2;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのEXCELテンプレートの権限がありません。');
             $msg =  showMsg('FAIL_QRY_USR',array('EXCELテンプレートの権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $rs = fnGetAllCount($db2con, $d1name);
    if($rs['result'] !== true){
        $rtn =1;
        $msg = showMsg($rs['result']);
    }else{
        $allcount = $rs['data'];
    }
}


if($rtn === 0){
    $rs = fnGetDB2JKSK($db2con,$D1NAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data)

);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得(1行)
*-------------------------------------------------------*
*/

function fnGetDB2JKSK($db2con,$D1NAME){

    $data = array();

    $params = array($D1NAME);

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2JKSK AS A' ;
    $strSQL .= ' WHERE JKNAME = ? ';
    $strSQL .= ' ORDER BY A.JKSEQ ASC ' ;

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnGetAllCount($db2con, $d1name){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT count(*) as COUNT ';
    $strSQL .= ' FROM DB2JKSK AS A' ;
    $strSQL .= ' WHERE JKNAME = ? ';
    $stmt = db2_prepare($db2con,$strSQL);
    $params = array(
        $d1name
    );
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}