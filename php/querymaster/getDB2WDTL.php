<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$FLD = $_POST['FLD'];
$FILID = $_POST['FILID'];
$QYNAME = $_POST['QYNAME'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'10',$userData[0]['WUSAUT']);//'10' => 詳細情報設定権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('詳細情報設定の権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery($db2con,'', $QYNAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$QYNAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 2;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての詳細情報設定の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('詳細情報設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    //ユーザー名取得
    $rs = cmGetDB2WDTL($db2con,$QYNAME,$FILID,$FLD);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'DATA' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));