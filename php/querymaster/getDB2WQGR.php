<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$wqunam = '';
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);

$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'4',$userData[0]['WUSAUT']);//'4' =>グループクエリー権限の権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループクエリー権限の権限'));
            }
        }
    }
}

//クエリーが存在するかチェック
if ($rtn === 0) {
    $chkQry = array();
    $chkQry    = cmChkQuery($db2con, '', $D1NAME, '');
    if ($chkQry['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($chkQry['result'], array(
            'クエリー'
        ));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 2;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのグループクエリー権限の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('グループクエリー権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $res = fnGetAllCount($db2con);
    if($res['result'] !== true){
        $rtn = 1;
        $msg = showMsg($res['result']);
    }else{
        $allcount = $res['data'];
    }
}
if($rtn === 0){        
    $res = fnGetDB2WQGR($db2con,$start,$length,$sort,$sortDir,$D1NAME);
    if($res['result'] === true){
        $data = $res['data'];
    }else{
        $rtn = 1;
        $msg = showMsg($res['result']);
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'RTN' => $rtn,
    'MSG' => $msg,
    'licenseType' => $licenseType
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/

function fnGetDB2WQGR($db2con,$start = '',$length = '',$sort = '',$sortDir = '',$D1NAME){

    $data = array();

    $params = array();

    $strSQL  ='SELECT';
    $strSQL .='    A.*';
    $strSQL .='FROM';
    $strSQL .='    (SELECT';
    $strSQL .='    B.*,';
    $strSQL .='        ROWNUMBER() OVER( ';  
    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.WQGID  ASC ';
    }
    $strSQL .=' ) as rownum ';
    $strSQL .='    FROM';
    $strSQL .='        (SELECT';
    $strSQL .='            C.*,';
    $strSQL .='            case  when WGNAME is null then \'0\' else \'1\' end as QRYID';
    $strSQL .='        FROM';
    $strSQL .='            (SELECT DISTINCT';
    $strSQL .='                (A.WQGID),';
    $strSQL .='                A.WQUNAM,';
    $strSQL .='                B.WGNAME';
    $strSQL .='            FROM';
    $strSQL .='                DB2WQGR   AS A LEFT OUTER JOIN DB2WGDF AS B ON A.WQGID= B.WGGID and WGNAME = ?';
    $strSQL .='            ) as C';
    $strSQL .='        ) as B';
    $strSQL .='    ) as A';
    array_push($params,$D1NAME);
    
    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    e_log("fnGetFDB2CSV1 **********".$strSQL); 

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
                $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con){
    $data = array();

    $strSQL  = ' SELECT count(*) as COUNT ';
    $strSQL .= ' FROM DB2WQGR';
    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}