<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'14',$userData[0]['WUSAUT']);//'14' =>制御レベル設定
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('制御レベル設定の権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $D1NAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての制御レベル設定の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('制御レベル設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    //ユーザー名取得
    $rs = getDMFDB2CSV2($db2con,$D1NAME);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result'],array('クエリー'));
        $rtn = 2;
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'DATA' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV2取得
*-------------------------------------------------------*
*/

function getDMFDB2CSV2($db2con,$D1NAME,$dnlFlg = false){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT * FROM ( ';
    $strSQL .= ' SELECT D2NAME,D2FILID,D2FLD,D2HED,D2CSEQ,D2WEDT, ';
    $strSQL .= ' D2TYPE,D2LEN,D2DNLF ';
    $strSQL .= ' FROM FDB2CSV2 ' ;
    $strSQL .= ' WHERE D2NAME = ? ';
    //$strSQL .= ' AND D2CSEQ > 0 ';
    $strSQL .= ' UNION ALL ';
    $strSQL .= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID, ';
    $strSQL .= ' D5FLD AS D2FLD,D5HED AS D2HED,D5CSEQ AS D2CSEQ, ';
    $strSQL .= ' D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DNLF AS D2DNLF ';
    $strSQL .= ' FROM FDB2CSV5 ';
    $strSQL .= ' WHERE D5NAME = ?  ';
    //$strSQL .= ' AND D5CSEQ > 0 ';
    $strSQL .= ' ) AS A ';
    
    if($dnlFlg === true){
        $strSQL .= ' WHERE ';
        $strSQL .= ' A.D2DNLF = \'\' ';
    }
    //$strSQL .= ' ORDER BY A.D2CSEQ ASC ';

    $strSQL .= ' ORDER BY CASE WHEN A.D2CSEQ=0 THEN NULL ELSE A.D2CSEQ END ASC ';

    $params = array(
        $D1NAME,
        $D1NAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }

            if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET');
 
            }else{
  
               $data = array('result' => true,'data' => $data);
            }
        }
    }
    return $data;
}
