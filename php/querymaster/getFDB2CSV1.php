<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$D1NAME = $_POST['D1NAME'];
$D1TEXT = $_POST['D1TEXT'];
$DQEKAI = $_POST['DQEKAI'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$rdoValue = $_POST['RDOVAL'];
//$licenseType = 'PLAN';   
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);
$D1TEXT = cmHscDe($D1TEXT);
$DQEKAI = cmHscDe($DQEKAI);

//ページング情報や検索情報などの保持用
$session = array();
$session['d1name']  = $D1NAME;
$session['d1text']  = $D1TEXT;
$session['dqekai']  = $DQEKAI;
$session['start']   = $start;
$session['length']  = $length;
$session['sort']    = $sort;
$session['sortDir'] = $sortDir;
$session['rdoValue'] = $rdoValue;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$authInfo = array();
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);	
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
		
    }else{
        $auth = $rs['data'][0];	
		 $WUUID = cmMer($auth['WUUID']);			
        $WUAUTH = cmMer($auth['WUAUTH']);
        if($WUAUTH === '2'){
            $SAUT = cmMer($auth['WUSAUT']);
            $rs = cmSelDB2AGRT($db2con,$SAUT);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg =  showMsg($rs['result']);
            }else{
                $authData = umEx($rs['data']);
                foreach($authData as $key => $value){
                        $authInfo[$value['AGMNID']] = $value['AGFLAG'];
                }
                if($authInfo['3'] !== '1' && $authInfo['6'] !== '1' && $authInfo['7'] !== '1'){
                    if($authInfo['8'] !== '1' && $authInfo['9'] !== '1' && $authInfo['10'] !== '1'){
                        if($authInfo['11'] !== '1' && $authInfo['12'] !== '1' && $authInfo['13'] !== '1'){
                            if($authInfo['14'] !== '1' && $authInfo['15'] !== '1' && $authInfo['16'] !== '1' && $authInfo['30'] !== '1'){
                                $rtn = 2;
                                 $msg =  showMsg('NOTEXIST',array('クエリー作成の権限'));
                            }
                        }
                    }
                }
            }
        }
    }
}

if($proc === 'EDIT'){
    $rs = fnSelFDB2CSV1($db2con,$D1NAME);
    if($rs['result'] === true){
        if(count($rs['data']) === 0){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET_QRY',array('ヘルプ','クエリー'));
        }else{
            $data = $rs['data'];
        }
    }else{
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }
    
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $rs = fnGetAllCount($db2con,$WUAUTH,$D1NAME,$D1TEXT,$DQEKAI,$rdoValue,$licenseSql);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetFDB2CSV1($db2con,$WUAUTH,$D1NAME,$D1TEXT,$DQEKAI,$start,$length,$sort,$sortDir,$rdoValue,$licenseSql);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}
$WUPSWE="";
if($rtn === 0){
	$rs = fnGetWUPSWE($db2con,$WUUID);
	if($rs['result'] !== true){
	    $rtn = 1;
	    $msg = showMsg($rs['result']);
	}else{
		$WUPSWE = $rs['WUPSWE'];
	   // $data = $rs['data'];
	}
}

//システムリスト
if($rtn === 0){
    $queryMenuList   = cmGetDB2COF($db2con);
    if ($queryMenuList['result'] === true) {
        $COFJWF = $queryMenuList['data'][0]['COFJWF'];
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(

    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg),
    'D1NAME' => $D1NAME,
    'D1TEXT' => $D1TEXT,
    'DQEKAI' => $DQEKAI,
    'licensePivot' =>$licensePivot,
    'licenseSeigyo' =>$licenseSeigyo,
    'licenseGraph' =>$licenseGraph,
    'licenseTemplate' =>$licenseTemplate,
    'licenseLog' =>$licenseLog,
    'licenseSql' =>$licenseSql,
    'AUTHINFO' => $authInfo,
    'WUAUTH' =>$WUAUTH,
    //'licenseSeigyoSpecialFlg'=>$licenseSeigyoSpecialFlg,
    'licenseGraph'=>$licenseGraph,
    'session' => $session,
	'WUUID'=>$WUUID, 
	'WUPSWE'=>$WUPSWE,
	'BASE_URL'=>BASE_URL,
	'AXESPARAM'=>AXESPARAM,
    'COFJWF'=>COFJWF,
    'ZERO_FLG'=>ZERO_FLG,
	'BACKZERO_FLG' => BACKZERO_FLG,
    'licenseType' => $licenseType
);

echo(json_encode($rtn));

function fnGetWUPSWE($db2con,$WUUID){
    $rs=true;
    $WUPSWE = "";
    $params = array();

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';
    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SEL';
        }
    }
    if($rs=true){
        $strSQL  = ' SELECT DECRYPT_CHAR(B.WUPSWE) AS WUPSWE ';
        $strSQL .= ' FROM DB2WUSR as B WHERE B.WUUID=? ';
        $stmt = db2_prepare($db2con,$strSQL);
		$params=array($WUUID);
        if($stmt === false){
            $rs='FAIL_SEL';
        }else{			
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs='FAIL_SEL';
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $WUPSWE = $row["WUPSWE"];
                }
            }
        }
    }
    return array('result'=>$rs,'WUPSWE'=>cmMer($WUPSWE));

}


/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con,$WUAUTH,$D1NAME,$D1TEXT,$DQEKAI,$start = '',$length = '',$sort = '',$sortDir = '',$rdoValue,$licenseSql){
    
    $data = array();

    $params = array();
    

//  $strSQL  = ' SELECT C.HTMLTN,D.DTNAME,E.D3CANL,F.DCNAME,A.* ';
    $strSQL  = ' SELECT C.HTMLTN,';
	$strSQL .= ' L.JKFILD, ';
    $strSQL .= ' CASE WHEN H.DQEKAI IS NOT NULL THEN H.DQEKAI ';
    $strSQL .= ' ELSE 0 ';
    $strSQL .= ' END ';
    $strSQL .= ' AS  DQEKAI ';
    $strSQL .= '  , H.DQEUSR, H.DQEDAY, H.DQETM, H.DQTMAX, H.DQTMIN, H.DQTAVG, ';//MSM add for クエリーの実行情報を初期化する 20190408
    $strSQL .= ' D.DTNAME ,';
    $strSQL .= "CASE WHEN J.DHINFO IS NOT NULL AND J.DHINFO <> '' THEN J.DHINFO ";
    $strSQL .= 'ELSE ( ';
    $strSQL .= 'CASE ';
    $strSQL .= "WHEN A.D1WEBF <> '1' THEN E.D3CANL ";
    $strSQL .= " WHEN A.D1CFLG = '1' THEN K.CNCANL ";
    $strSQL .= 'ELSE G.CNCANL ';
    $strSQL .= 'END ';
    $strSQL .= ') ';
    $strSQL .= 'END AS D3CANL  ';
    $strSQL .= ' ,F.DCNAME,A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.D1NAME ASC ';
    }

    $strSQL .= ' ) as rownum ';
    //$strSQL .= ' FROM '.cmLicenseFDB2CSV1($licenseType).' as B ';
    $strSQL .= '    FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '   ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '   FDB2CSV1 AS B';
    }
    
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params,'%'.$D1NAME.'%');
    }


    if($D1TEXT != ''){
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params,'%'.$D1TEXT.'%');
    }
    // 5250件用
    if($rdoValue === '1'){
        $strSQL .= ' AND D1WEBF = \'\' ';
    }
    // web件用
    if($rdoValue === '2'){
        $strSQL .= ' AND D1WEBF = \'1\' ';
    }
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND D1CFLG = \'\' ';
    }
    $strSQL .= ' ) as A ';
    $strSQL .= ' LEFT JOIN DB2HTMLT AS C ON A.D1NAME = C.HTMLTD ';
    $strSQL .= ' LEFT JOIN DB2QHIS AS H ON A.D1NAME = H.DQNAME ';
//     $strSQL .= ' LEFT JOIN (SELECT DISTINCT DTNAME FROM DB2WDTL) AS D ON A.D1NAME = D.DTNAME ';
    $strSQL .= ' LEFT JOIN (SELECT DTNAME FROM DB2WDTL UNION SELECT DRKMTN AS DTNAME FROM DB2DRGS ) AS D ON A.D1NAME = D.DTNAME ';
    $strSQL .= ' LEFT JOIN (SELECT D3NAME,';
//    $strSQL .= " MAX(D3CANL) ";
    $strSQL .= " CASE WHEN MAX(D3CANL) <> '' OR MAX(D3DFMT) <> '' THEN '1'  ELSE '' END ";
//    $strSQL .= " CASE WHEN MAX(D3CANL) <> '' THEN MAX(D3CANL) ELSE WHEN MAX(D3DFMT) <> '' THEN MAX(D3DFMT) ELSE '' END ";
    $strSQL .= ' AS D3CANL FROM FDB2CSV3 GROUP BY D3NAME) AS E ON A.D1NAME = E.D3NAME ';
    $strSQL .= ' LEFT JOIN (SELECT CNQRYN,';
//    $strSQL .= " MAX(CNCANL) ";
    $strSQL .= " CASE WHEN MAX(CNCANL) <> '' OR MAX(CNDFMT) <> '' THEN '1'  ELSE '' END ";
//    $strSQL .= " CASE WHEN MAX(CNCANL) <> '' THEN MAX(CNCANL) ELSE WHEN MAX(CNDFMT) <> '' THEN MAX(CNDFMT) ELSE '' END ";
    $strSQL .=  ' AS CNCANL FROM BQRYCND GROUP BY CNQRYN) AS G ON A.D1NAME = G.CNQRYN ';
    $strSQL .= ' LEFT JOIN (SELECT CNQRYN,';
    $strSQL .= " CASE WHEN MAX(CNCANL) <> '' OR MAX(CNDFMT) <> '' THEN '1'  ELSE '' END ";
    $strSQL .=  ' AS CNCANL FROM BSQLCND GROUP BY CNQRYN) AS K ON A.D1NAME = K.CNQRYN ';
    $strSQL .= ' LEFT JOIN DB2COLM AS F ON A.D1NAME = F.DCNAME ';
    $strSQL .= "  LEFT JOIN (SELECT CASE WHEN MAX(DHINFO) IS NOT NULL THEN '1' ELSE '' END AS DHINFO,DHNAME FROM DB2HCND WHERE DHINFO <> '' GROUP BY DHNAME) AS J ";
    $strSQL .= ' ON A.d1name = J.DHNAME  ';
	//文字の色付け（条件付き書式）
	$strSQL .= "LEFT OUTER JOIN ( SELECT CASE WHEN MAX ( JKSEQ ) IS NOT NULL THEN  '1'  ELSE  ''  END  AS  JKFILD,JKNAME  FROM  DB2JKSK  WHERE JKFILD <> '' AND JKFILD IS NOT NULL GROUP BY JKNAME ) AS L ON A.D1NAME=L.JKNAME ";

/*
    $strSQL .= ' LEFT JOIN DB2HTMLT as C ';
    $strSQL .= ' ON A.D1NAME = C.HTMLTD ';
*/
    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    e_log('実行SQL'.$strSQL);
    e_log('実行params'.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
            $err =db2_stmt_errormsg();
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $err =db2_stmt_errormsg();
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$WUAUTH,$D1NAME = '',$D1TEXT = '',$DQEKAI = '',$rdoValue,$licenseSql){
    $data = array();
    $params = array();
    $strSQL  = ' SELECT count(D1NAME) as COUNT ';
    $strSQL .= ' FROM  FDB2CSV1 AS B';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    if($WUAUTH === '3'){
        $strSQL .= '    AND D1NAME IN ( ';
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if( $WUAUTH === '4'){
        $strSQL .= '    AND D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '            )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params,'%'.$D1NAME.'%');
    }

    if($D1TEXT != ''){
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params,'%'.$D1TEXT.'%');
    }
    // 5250件用
    if($rdoValue === '1'){
        $strSQL .= ' AND D1WEBF = \'\' ';
    }
    // web件用
    if($rdoValue === '2'){
        $strSQL .= ' AND D1WEBF = \'1\' ';
    }
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND D1CFLG = \'\' ';
    }
    e_log("グループクエリーカウント：".$strSQL.'パラメータ：'.print_r($params,true));

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}

/*
*-------------------------------------------------------* 
* FDB2CSV1取得(1行)
*-------------------------------------------------------*
*/

function fnSelFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array($D1NAME);

    $strSQL  = ' SELECT B.* ';
    $strSQL .= ' FROM FDB2CSV1 AS B' ;
    $strSQL .= ' WHERE D1NAME = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}