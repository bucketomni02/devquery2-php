<?php



/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D1NAME =(isset($_POST['D1NAME'])?$_POST['D1NAME']:'');
$isQGflg =(isset($_POST['ISQGFLG'])?$_POST['ISQGFLG']:'');

$ver = phpversion();
$sver = substr($ver,0,3);
$phpversion = str_replace('.','',$sver);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data=array();
//$data = array(array('D1EFLG'=>''));
$QSET = (isset($_POST['QSET']))?$_POST['QSET']:'';
$ISEXCELQUERY=(isset($_POST['ISEXCELQUERY']))?$_POST['ISEXCELQUERY']:'';//9:EXCELテンプレート,7:クエリー実行設定
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);
//クエリーグループ
$LASTD1NAME='';
$isQGflg=($isQGflg==='true')?true:false;


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    if($QSET === '1'){
        $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if ($rs['result'] !== true) {//チェックパーワコーサー
            $rtn = 2;
            $msg = showMsg($rs['result'], array(
                'ユーザー'
            ));
        }else{
            $userData  = umEx($rs['data']);
            if($userData[0]['WUAUTH'] === '2'){
                $resultstr=($ISEXCELQUERY==='9')?'EXCELテンプレートの権限':'クエリー実行設定の権限';
                $rs = cmChkKenGen($db2con,$ISEXCELQUERY,$userData[0]['WUSAUT']);//'9' => EXCEL テンプレート
                if($rs['result'] !== true){
                    $rtn = 4;
                    $msg =  showMsg($rs['result'],array($resultstr));
                }
            }
        }
    }else{
        $rs = cmCheckUser($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
}
//クエリーグループ
if($rtn===0){
    if($isQGflg){
        $rs=cmGetQueryGroup($db2con,$D1NAME,"","");
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリー'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
        }
    }
}

//クエリーが存在するかチェック
if ($rtn === 0) {
    if($isQGflg){
        $userId=($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4')?
                $userData[0]['WUAUTH']:'';
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            if($res['LASTFLG']==='1'){
                $LASTD1NAME=$res['QRYGSQRY'];
            }
            $chkQry = cmChkQueryGroup($db2con,$userId,$D1NAME,$res['QRYGSQRY'],$res['LASTFLG'],'','');
            if($chkQry['result']=== 'NOTEXIST_GET'){
                $rtn = 3;
                //$msg = showMsg('ログインユーザーに指定したクエリーに対してのクエリー実行設定の権限がありません。');
                $msg =  showMsg('FAIL_QRY_USR',array('クエリーグループ実行設定'));
            }else if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリーグループ'));
                break;
            }
        }
    }else{
        $chkQry = array();
        $chkQry    = cmChkQuery($db2con, '', $D1NAME, '');
        if ($chkQry['result'] !== true) {
            $rtn = 1;
            $msg = showMsg($chkQry['result'], array(
                'クエリー'
            ));
        }
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        if(!$isQGflg){
            $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
            if($chkQryUsr === 'NOTEXIST_GET'){
                $rtn = 3;
                //$msg = showMsg('ログインユーザーに指定したクエリーに対してのクエリー実行設定の権限がありません。');
                $msg =  showMsg('FAIL_QRY_USR',array('クエリー実行設定'));
            }else if($chkQryUsr !== true){
                $rtn = 1;
                $msg = showMsg($chkQryUsr['result'],array('クエリー'));
            }
        }
    }
}
if($rtn === 0){
    //ユーザー名取得
    if($isQGflg){
        foreach($D1NAMELIST as $result){
            $rs = getFDB2CSV1Data($db2con,$result['QRYGSQRY']);
            if($rs['result'] !== true){
                $msg = showMsg($rs['result'],array('クエリー'));
                $rtn = 1;
                break;
            }else{
                $data[$result['QRYGSQRY']][] = $rs['data'][0];
            }
        }
    }else{
        $rs = getFDB2CSV1Data($db2con,$D1NAME);
        if($rs['result'] !== true){
            $msg = showMsg($rs['result'],array('クエリー'));
            $rtn = 1;
        }else{
            $data = $rs['data'];
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'aaData' => $data,
    'RTN' => $rtn,
    'MSG' => $msg,
	'phpversion' => (int)$phpversion
);
echo(json_encode($rtn));

/**
* FDB2CSV1テーブルの取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $D1NAME  クエリ名
* 
* 
*/

function getFDB2CSV1Data($db2con,$D1NAME){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT D1EDKB,D1DISF,DISCSN,D1EFLG,D1ETYP,D1TEXT,D1WEBF,D1CFLG FROM  FDB2CSV1';
    $strSQL .= ' WHERE D1NAME = ?';

    $params = array(
        $D1NAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET'); 
            }else{
               $data = array('result' => true,'data' => umEx($data));
            }
        }
    }
    return $data;
}
