<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$d1text = '';
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmlspecialcharsをデコード
//$D1NAME = cmHscDe($D1NAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック

if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'10',$userData[0]['WUSAUT']);//'10' => 詳細情報設定
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('詳細情報設定の権限'));
            }
        }
    }
}
//クエリー存在チェック
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$D1NAME,'');
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 3;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのダウンロード権限の権限がありません。');
             $msg =  showMsg('FAIL_QRY_USR',array('詳細情報設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $rs = fnGetAllData($db2con,$D1NAME,$start,$length,$sort,$sortDir);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $data = $rs['data'];
    }
}
if($rtn === 0){
    $rs = fnGetAllCount($db2con,$D1NAME);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $allcount = $rs['data'];
    }
}



cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'RTN' => $rtn ,
    'MSG' => $msg,
    'D1NAME' => $D1NAME,
    'data' => $data,
    'sort' => $sort,
    'sortDir' => $sortDir,
    'licenseDrilldown' => $licenseDrilldown
);

echo(json_encode($rtn));


/*
*-------------------------------------------------------* 
* GETFDB2CSV2&GETFDB2CSV5 By D1NAME取得
*-------------------------------------------------------*
*/

function fnGetAllData($db2con,$D1NAME,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();


    $strSQL  = ' SELECT C.* FROM ( ';
    $strSQL .= ' SELECT D.*, ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY D.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY D.D2CSEQ ASC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM ( ';
    $strSQL     .= '    SELECT A.* ,E.DTLIBL,E.DTFILE ';

    $strSQL     .= '   ,F.DRDMTN ';

    $strSQL     .= '    FROM ';  
    $strSQL     .= '    (SELECT A.D2FLD AS FLD,A.D2HED AS FLDMEI,A.D2NAME AS QYNAME,A.D2FILID AS FILID,A.D2CSEQ AS D2CSEQ ';
    $strSQL     .= '     FROM FDB2CSV2 AS A '; 
    $strSQL     .= '     WHERE D2NAME = ? '; 
    $strSQL     .= '     AND D2CSEQ > 0 '; 
    $strSQL     .= '     UNION ALL ';     
    $strSQL     .= '     SELECT B.D5FLD AS FLD,B.D5HED AS FLDMEI,B.D5NAME AS QYNAME,9999 AS FILID,B.D5CSEQ AS D2CSEQ '; 
    $strSQL     .= '     FROM FDB2CSV5 AS B '; 
    $strSQL     .= '     WHERE D5NAME = ? ';  
    $strSQL     .= '     AND   D5CSEQ >0) A ';
    $strSQL     .= '    LEFT JOIN DB2WDTL E ';
    $strSQL     .= '    ON A.QYNAME = E.DTNAME ';
    $strSQL     .= '    AND A.FILID = E.DTFILID ';
    $strSQL     .= '    AND A.FLD   = E.DTFLD ';

    $strSQL     .= '    LEFT JOIN DB2DRGS F ';
    $strSQL     .= '    ON A.QYNAME = F.DRKMTN ';
    $strSQL     .= '    AND A.FILID = F.DRKFLID ';
    $strSQL     .= '    AND A.FLD   = F.DRKFID ';

    $strSQL .= ' ) as D ';

  //$strSQL .= ' WHERE NAME <> \'\' '; 

    array_push($params,$D1NAME);
    array_push($params,$D1NAME);
    
    $strSQL .= ' ) as C ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE C.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }  



    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }

    return $data;

}


/*
*-------------------------------------------------------* 
* GET ALL COUNT By D1NAME取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$D1NAME){

    $data = array();

    $params = array();


    $strSQL  = ' SELECT count(C.FLD) as COUNT FROM ( ';
    $strSQL .= ' SELECT A.D2NAME AS FLD,A.D2HED AS FLDMEI ';
    $strSQL .= ' FROM FDB2CSV2 AS A ';
    $strSQL .= ' WHERE D2NAME = ? ';
    $strSQL .= ' AND D2CSEQ > 0 ';
    $strSQL .= ' UNION ALL ';    
    $strSQL .= ' SELECT B.D5NAME AS FLD,B.D5HED AS FLDMEI ';
    $strSQL .= ' FROM FDB2CSV5 AS B ';
    $strSQL .= ' WHERE D5NAME = ? ';
    $strSQL .= ' AND D5CSEQ > 0  '; 
    $strSQL .= ' ) as C ';

    array_push($params,$D1NAME);
    array_push($params,$D1NAME);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;
}
