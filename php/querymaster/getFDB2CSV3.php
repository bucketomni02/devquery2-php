<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$sort = (isset($_POST['sort'])?$_POST['sort']:'');
$sortDir = (isset($_POST['sortDir'])?$_POST['sortDir']:'');
$start = (isset($_POST['start'])?$_POST['start']:'');
$length = (isset($_POST['sortDir'])?$_POST['length']:'');
$drillDown = (isset($_POST['DRILLDOWN'])?$_POST['DRILLDOWN']:'');//クエリー連携設定

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
$D1WEBF = '';
$D1CFLG='';//SQLクエリーかどうかをチェック
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'13',$userData[0]['WUSAUT']);//'13' =>検索候補
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('検索候補の権限'));
            }
        }
    }
}
/** クエリー存在チェック**/
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$D1NAME,'');
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 3;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 1;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての検索候補の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('検索候補の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
   $rs =  fnGetD1TEXT($db2con,$D1NAME);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result'],array('クエリー'));
        $rtn = 1;
    }else{
        $D1WEBF = $rs['data'][0]['D1WEBF'];
        $D1CFLG = $rs['data'][0]['D1CFLG'];
    }
}

if($D1WEBF !== '1'){
    if($rtn === 0){
        $rs = fnGetAllCount($db2con,$D1NAME,$drillDown);
        if($rs['result'] !== true){
            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetFDB2CSV3($db2con,$D1NAME,$start,$length,$sort,$sortDir,$drillDown);
        if($rs['result'] !== true){
            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            $data = $rs['data'];
        }
    }
}else{
    //SQLのクエリーかどうかのチェック
    if($D1CFLG==='1'){
        if($rtn===0){
            $rs=fnGetBSQLCNDAllCount($db2con,$D1NAME,$drillDown);
            if($rs['result'] !== true){
                $msg = showMsg($rs['result']);
                $rtn = 1;
            }else{
                $allcount = $rs['data'];
            }
        }
        if($rtn === 0){
            $rs=fnGetBSQLCNDAllData($db2con,$D1NAME,$start,$length,$sort,$sortDir,$drillDown);
            if($rs['result'] !== true){
                $msg = showMsg($rs['result']);
                $rtn = 1;
            }else{
                $data = $rs['data'];
            }
        }
    }else{
        if($rtn === 0){
            $rs = fnWebGetAllCount($db2con,$D1NAME,$drillDown);
            if($rs['result'] !== true){
                $msg = showMsg($rs['result']);
                $rtn = 1;
            }else{
                $allcount = $rs['data'];
            }
        }
        if($rtn === 0){
            $rs = fnGetBQRYCND($db2con,$D1NAME,$start,$length,$sort,$sortDir,$drillDown);
            if($rs['result'] !== true){
                $msg = showMsg($rs['result']);
                $rtn = 1;
            }else{
                $data = $rs['data'];
            }
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg),
    'D1NAME' => $D1NAME,
    'D1WEBF' => $D1WEBF,
    'D1CFLG'=>$D1CFLG
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV3取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV3($db2con,$D1NAME,$start = '',$length = '',$sort = '',$sortDir = '',$drillDown){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* ';
    $strSQL .= '    , CASE  ';
    $strSQL .= '        WHEN B.D3DFMT=\'1\' AND B.D3DSFL= \'\'  THEN \'yyyy/mm/dd\' ';
    $strSQL .= '        WHEN B.D3DFMT=\'1\' AND B.D3DSFL= \'1\' THEN \'yyyymmdd\' ';
    $strSQL .= '        WHEN B.D3DFMT=\'2\' AND B.D3DSFL= \'\'  THEN \'yyyy/mm\' ';
    $strSQL .= '        WHEN B.D3DFMT=\'2\' AND B.D3DSFL= \'1\' THEN \'yyyymm\' ';
    $strSQL .= '        WHEN B.D3DFMT=\'3\' AND B.D3DSFL= \'\'  THEN \'yy/mm/dd\' ';
    $strSQL .= '        WHEN B.D3DFMT=\'3\' AND B.D3DSFL= \'1\' THEN \'yymmdd\' ';
	//20170613
    $strSQL .= ' 		WHEN B.D3DFMT=\'4\' AND B.D3DSFL= \'\'  THEN \'yyyy-mm-dd\' ';
    $strSQL .= '		WHEN B.D3DFMT=\'4\' AND B.D3DSFL= \'1\' THEN \'yyyymmdd\' ';
    $strSQL .= '		WHEN B.D3DFMT=\'5\' AND B.D3DSFL= \'\'  THEN \'yyyy-mm\' ';
    $strSQL .= '		WHEN B.D3DFMT=\'5\' AND B.D3DSFL= \'1\' THEN \'yyyymm\' ';
    $strSQL .= '		WHEN B.D3DFMT=\'6\' AND B.D3DSFL= \'\'  THEN \'yy-mm-dd\' ';
    $strSQL .= '		WHEN B.D3DFMT=\'6\' AND B.D3DSFL= \'1\' THEN \'yymmdd\' ';
    $strSQL .= '        ELSE \'\'';
    $strSQL .= '      END AS CLDFMT ';
    $strSQL .= ' ,C.D2HED, ROWNUMBER() OVER( ';

    if($sort !== ''){
        /*if($sort !== 'D2HED'){
            $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= ' ORDER BY C.'.$sort.' '.$sortDir.' ';
        }*/
        switch ($sort) {
            case 'D3JSEQ':
            case 'D3FLD':
            case 'D3CND':
            case 'D3CANL':
            case 'D3CANF':
            case 'D3CANC':
                $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
                break;
            case 'D2HED':
                $strSQL .= ' ORDER BY C.'.$sort.' '.$sortDir.' ';
                break;
            case 'CLDFMT':
                $strSQL .= ' ORDER BY B.D3DFMT '.$sortDir.' ';
                break;
        }
    }else{
        $strSQL .= ' ORDER BY B.D3JSEQ ASC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM FDB2CSV3 as B join FDB2CSV2 as C ';
    $strSQL .= ' ON B.D3NAME = C.D2NAME AND B.D3FILID = C.D2FILID ';
    $strSQL .= ' AND B.D3FLD = C.D2FLD ';
    $strSQL .= ' WHERE B.D3NAME = ? ';
    if($drillDown !== '1'){
        $strSQL .= ' AND ( B.D3USEL = \'1\' ';
        $strSQL .= ' OR B.D3USEL = \'2\' ';
        $strSQL .= ' OR B.D3USEL = \'\' ) ';
    }else{
        //クエリー連携の場合
//        $strSQL     .=  ' ) AND ( B.D3CND <> \'IS\'  AND B.D3CND<> \'RANGE\' ) ';
    }
    array_push($params,$D1NAME);

    $strSQL .= ' ) as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
         $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$D1NAME = '',$drillDown){
    $data = array();

    $strSQL  = ' SELECT count(A.D3NAME) as COUNT ';
    $strSQL .= ' FROM FDB2CSV3 as A ' ;
    $strSQL .= ' WHERE D3NAME = ? ';
    if($drillDown !== '1'){
        $strSQL .= ' AND ( D3USEL = \'1\' ';
        $strSQL .= ' OR D3USEL = \'2\' ';
        $strSQL .= ' OR D3USEL = \'\' ) ';
    }else{
        //クエリー連携の場合
//        $strSQL     .=  ' ) AND ( D3CND <> \'IS\'  AND D3CND<> \'RANGE\' ) ';
    }

    $params = array($D1NAME);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}

function fnWebGetAllCount($db2con,$D1NAME,$drillDown){
    $data = array();
    $strSQL = '';

    $strSQL .= '    SELECT count(A.CNQRYN) as COUNT ';
    $strSQL .= '    FROM BQRYCND as A '; 
    $strSQL .= '    WHERE CNQRYN = ? ';  
    if($drillDown !== '1'){
            $strSQL .= '    AND ( CNSTKB = \'1\' '; 
            $strSQL .= '    OR CNSTKB = \'2\' ';
            $strSQL     .=  ' OR CNSTKB = \'\'  )';
    }else{
        //クエリー連携の場合
    //    $strSQL     .=  ' ) AND ( CNCKBN <> \'IS\'  AND  CNCKBN <> \'RANGE\' ) ';
    }

    $params = array($D1NAME);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* FDB2CSV3取得
*-------------------------------------------------------*
*/

function fnGetBQRYCND($db2con,$D1NAME,$start = '',$length = '',$sort = '',$sortDir = '',$drillDown){

    $data = array();

    $params = array();
    $strSQL = '';
    $strSQL     .=  '   SELECT CND.* ';
    $strSQL     .=  '   FROM ( ';
    $strSQL     .=  '   SELECT  QRYCND.CNQRYN   AS D3NAME ';
    $strSQL     .=  '        ,  QRYCND.CNFILID  AS D3FILID ';
    $strSQL     .=  '        ,  CONCAT(CONCAT(QRYCND.CNMSEQ,\'-\'),QRYCND.CNSSEQ) AS D3JSEQ ';
    $strSQL     .=  '        ,  0 AS D3CNT ';
    $strSQL     .=  '        ,  QRYCND.CNAOKB   AS D3ANDOR ';
    $strSQL     .=  '        ,  QRYCND.CNFLDN   AS D3FLD ';
    $strSQL     .=  '        ,  QRYCND.CNCKBN   AS D3CND ';
    $strSQL     .=  '        ,  \'\'            AS D3DAT ';
    $strSQL     .=  '        ,  QRYCND.CNSTKB   AS D3USEL ';
    $strSQL     .=  '        ,  QRYCND.D2TYPE   AS D3TYPE ';
    $strSQL     .=  '        ,  QRYCND.D2LEN    AS D3LEN ';
    $strSQL     .=  '        ,  QRYCND.CNCANL   AS D3CANL ';
    $strSQL     .=  '        ,  QRYCND.CNCANF   AS D3CANF ';
    $strSQL     .=  '        ,  QRYCND.CNCANC   AS D3CANC ';
    $strSQL     .=  '        ,  QRYCND.CNCANG   AS D3CANG ';
    $strSQL     .=  '        ,  QRYCND.CNNAMG   AS D3NAMG ';
    $strSQL     .=  '        ,  QRYCND.CNNAMC   AS D3NAMC ';
    $strSQL     .=  '        ,  QRYCND.D2HED    AS D2HED ';
    $strSQL     .=  '        ,  QRYCND.CLDFMT   AS CLDFMT ';
    $strSQL     .=  '        ,  QRYCND.ROWNUM   AS ROWNUM ';
    $strSQL     .=  '   FROM ';
    $strSQL     .=  '       ( ';
    $strSQL     .=  '           SELECT A.CNQRYN ';
    $strSQL     .=  '                , A.CNFILID ';
    $strSQL     .=  '                , A.CNMSEQ ';
    $strSQL     .=  '                , A.CNSSEQ ';
    $strSQL     .=  '                , A.CNAOKB ';
    $strSQL     .=  '                , A.CNFLDN ';
    $strSQL     .=  '                , A.CNCKBN ';
    $strSQL     .=  '                , A.CNSTKB ';
    $strSQL     .=  '                , A.CNCANL ';
    $strSQL     .=  '                , A.CNCANF ';
    $strSQL     .=  '                , A.CNCANC ';
    $strSQL     .=  '                , A.CNCANG ';
    $strSQL     .=  '                , A.CNNAMG ';
    $strSQL     .=  '                , A.CNNAMC ';
    $strSQL     .=  '                , CASE  ';
    $strSQL     .=  '                  WHEN A.CNDFMT=\'1\' AND A.CNDSFL= \'\'  THEN \'yyyy/mm/dd\' ';
    $strSQL     .=  '                  WHEN A.CNDFMT=\'1\' AND A.CNDSFL= \'1\' THEN \'yyyymmdd\' ';
    $strSQL     .=  '                  WHEN A.CNDFMT=\'2\' AND A.CNDSFL= \'\'  THEN \'yyyy/mm\' ';
    $strSQL     .=  '                  WHEN A.CNDFMT=\'2\' AND A.CNDSFL= \'1\' THEN \'yyyymm\' ';
    $strSQL     .=  '                  WHEN A.CNDFMT=\'3\' AND A.CNDSFL= \'\'  THEN \'yy/mm/dd\' ';
    $strSQL     .=  '                  WHEN A.CNDFMT=\'3\' AND A.CNDSFL= \'1\' THEN \'yymmdd\' ';
	//20170613
    $strSQL		.=	' 				   WHEN A.CNDFMT=\'4\' AND A.CNDSFL= \'\'  THEN \'yyyy-mm-dd\' ';
    $strSQL		.=	'				   WHEN A.CNDFMT=\'4\' AND A.CNDSFL= \'1\' THEN \'yyyymmdd\' ';
    $strSQL		.=	'				   WHEN A.CNDFMT=\'5\' AND A.CNDSFL= \'\'  THEN \'yyyy-mm\' ';
    $strSQL		.=	'				   WHEN A.CNDFMT=\'5\' AND A.CNDSFL= \'1\' THEN \'yyyymm\' ';
    $strSQL		.=	'				   WHEN A.CNDFMT=\'6\' AND A.CNDSFL= \'\'  THEN \'yy-mm-dd\' ';
    $strSQL		.=	'				   WHEN A.CNDFMT=\'6\' AND A.CNDSFL= \'1\' THEN \'yymmdd\' ';
    $strSQL     .=  '                   ELSE \'\'';
    $strSQL     .=  '                  END AS CLDFMT ';
    $strSQL     .=  '                , B.D2HED ';
    $strSQL     .=  '                , B.D2LEN ';
    $strSQL     .=  '                , B.D2TYPE ';
    $strSQL     .=  '                , ROWNUMBER() OVER( ';
    if($sort !== ''){
        switch ($sort) {
            case 'D3JSEQ':
                $strSQL .= ' ORDER BY A.CNMSEQ '.$sortDir.' ,A.CNSSEQ  '.$sortDir;
                break;
            case 'D3FLD':
                $strSQL .= ' ORDER BY A.CNFLDN '.$sortDir.' ';
                break;
            case 'D2HED':
                $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
                break;
            case 'D3CND':
                $strSQL .= ' ORDER BY A.CNCKBN '.$sortDir.' ';
                break;
            case 'D3CANL':
                $strSQL .= ' ORDER BY A.CNCANL '.$sortDir.' ';
                break;
            case 'D3CANF':
                $strSQL .= ' ORDER BY A.CNCANF '.$sortDir.' ';
                break;
            case 'D3CANC':
                $strSQL .= ' ORDER BY A.CNCANC '.$sortDir.' ';
                break;
            case 'CLDFMT':
                $strSQL .= ' ORDER BY A.CNDFMT '.$sortDir.' ';
                break;
        }
       /* if($sort !== 'D2HED'){
            $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= ' ORDER BY C.'.$sort.' '.$sortDir.' ';
        }*/
    }else{
        $strSQL .= ' ORDER BY A.CNMSEQ ASC ,A.CNSSEQ ASC ';
    }
    $strSQL     .=  '                )as ROWNUM '; 
    $strSQL     .=  '           FROM BQRYCND AS A ';
    $strSQL     .=  '           LEFT JOIN (SELECT FLDINFO.* ';
    $strSQL     .=  '                       FROM ( '; 
    $strSQL     .=  '                           SELECT ';
    $strSQL     .=  '                               D2NAME ';
    $strSQL     .=  '                              ,D2FILID ';
    $strSQL     .=  '                              ,D2FLD ';
    $strSQL     .=  '                              ,D2HED ';
    $strSQL     .=  '                              ,D2CSEQ ';
    $strSQL     .=  '                              ,D2WEDT ';
    $strSQL     .=  '                              ,D2TYPE ';
    $strSQL     .=  '                              ,D2LEN ';
    $strSQL     .=  '                              ,D2DEC ';
    $strSQL     .=  '                              ,D2DNLF '; 
    $strSQL     .=  '                           FROM FDB2CSV2 '; 
    $strSQL     .=  '                           WHERE D2NAME = ? '; 
    array_push($params,$D1NAME);
    $strSQL     .=  '                           UNION ALL '; 
    $strSQL     .=  '                           SELECT '; 
    $strSQL     .=  '                                D5NAME AS D2NAME ';
    $strSQL     .=  '                               ,9999 AS D2FILID ';
    $strSQL     .=  '                               ,D5FLD AS D2FLD ';
    $strSQL     .=  '                               ,D5HED AS D2HED ';
    $strSQL     .=  '                               ,D5CSEQ AS D2CSEQ ';
    $strSQL     .=  '                               ,D5WEDT AS D2WEDT ';
    $strSQL     .=  '                               ,D5TYPE AS D2TYPE ';
    $strSQL     .=  '                               ,D5LEN AS D2LEN ';
    $strSQL     .=  '                               ,D5DEC AS D2DEC ';
    $strSQL     .=  '                               ,D5DNLF AS D2DNLF '; 
    $strSQL     .=  '                           FROM FDB2CSV5 '; 
    $strSQL     .=  '                           WHERE D5NAME = ?';
    array_push($params,$D1NAME); 
    $strSQL     .=  '                           ) AS FLDINFO '; 
    $strSQL     .=  '                      ) B ';
    $strSQL     .=  '           ON   A.CNQRYN  = B.D2NAME ';
    $strSQL     .=  '           AND  A.CNFILID = B.D2FILID ';
    $strSQL     .=  '           AND  A.CNFLDN  = B.D2FLD ';
    $strSQL     .=  '           WHERE A.CNQRYN = ? ';
    array_push($params,$D1NAME); 
    if($drillDown !== '1'){
    $strSQL     .=  '           AND   ( A.CNSTKB = \'1\' ';
    $strSQL     .=  '               OR  A.CNSTKB = \'2\' ';
        $strSQL     .=  '               OR  A.CNSTKB = \'\' )';
    }else{
        //クエリー連携の場合
  //      $strSQL     .=  ' ) AND ( A.CNCKBN <> \'IS\'  AND  A.CNCKBN <> \'RANGE\'  ';
    }

    $strSQL     .=  '       ) QRYCND ';
    $strSQL     .=  '   ORDER BY QRYCND.CNMSEQ ';
    $strSQL     .=  '           ,QRYCND.CNSSEQ ';
    $strSQL     .=  '  ) AS CND ';
    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL     .=  ' WHERE CND.ROWNUM BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    //e_log('SQL : '.$strSQL . print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
         $data = array('result' => 'FAIL_SEL');
    }else{
        
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* 定義名取得
* SQLクエリーかどうかのFLG
*-------------------------------------------------------*
*/

function fnGetD1TEXT($db2con,$id){

    $data = array();

    $strSQL  = ' SELECT A.D1TEXT ,A.D1WEBF,A.D1CFLG ';
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array($id);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)>0){
                $data = array('result' => true, 'data' => umEx($data));
            }else{
                $data = array('result' => 'NOTEXIST_GET');
            }
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* SQL定義名取得
* SQLクエリーかどうかのFLG
* return count
*-------------------------------------------------------*
*/
function fnGetBSQLCNDAllCount($db2con,$CNQRYN,$drillDown){

    $data = array();

    $strSQL  = ' SELECT COUNT(*) AS COUNT ';
    $strSQL .= ' FROM BSQLCND AS A ' ;
    $strSQL .= ' WHERE A.CNQRYN = ? ';
    $strSQL .= ' AND   ( A.CNDSTKB = \'1\' ';
    $strSQL .= ' OR A.CNDSTKB = \'2\' ';
    $strSQL .= ' OR  A.CNDSTKB = \'\' )';
    $params = array($CNQRYN);    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* SQL定義名取得
* SQLクエリーかどうかのFLG
* return count
*-------------------------------------------------------*
*/
function fnGetBSQLCNDAllData($db2con,$CNQRYN,$start,$length,$sort,$sortDir,$drillDown){

    $data = array();
    $strSQL .= 'SELECT E.* FROM ( ';
    $strSQL .= ' SELECT';
    $strSQL .= ' A.CNQRYN   AS D3NAME ,';
    $strSQL .= ' 0  AS D3FILID,';
    $strSQL .= ' A.CNDSEQ AS D3JSEQ,';
    $strSQL .= ' 0 AS D3CNT,';
    $strSQL .= ' \'\' AS D3ANDOR,';
    $strSQL .= ' A.CNDNM AS D3FLD,';
    $strSQL .= ' \'\' AS D3CND,';
    $strSQL .= ' A.CNDDAT AS D3DAT,';
    $strSQL .= ' A.CNDSTKB   AS D3USEL,';
    $strSQL .= ' A.CNDDTYP   AS D3TYPE,';
    $strSQL .= ' \'\'    AS D3LEN,';
    $strSQL .= ' A.CNCANL   AS D3CANL,';
    $strSQL .= ' A.CNCANF   AS D3CANF,';
    $strSQL .= ' A.CNCANC   AS D3CANC,';
    $strSQL .= ' A.CNCANG   AS D3CANG,';
    $strSQL .= ' A.CNNAMG   AS D3NAMG,';
    $strSQL .= ' A.CNNAMC   AS D3NAMC,';
    $strSQL .= ' A.CNDNM AS D2HED,';
    $strSQL .= ' CASE  ';
    $strSQL .= ' WHEN A.CNDFMT=\'1\' AND A.CNDSFL= \'\'  THEN \'yyyy/mm/dd\' ';
    $strSQL .= ' WHEN A.CNDFMT=\'1\' AND A.CNDSFL= \'1\' THEN \'yyyymmdd\' ';
    $strSQL .= ' WHEN A.CNDFMT=\'2\' AND A.CNDSFL= \'\'  THEN \'yyyy/mm\' ';
    $strSQL .= ' WHEN A.CNDFMT=\'2\' AND A.CNDSFL= \'1\' THEN \'yyyymm\' ';
    $strSQL .= ' WHEN A.CNDFMT=\'3\' AND A.CNDSFL= \'\'  THEN \'yy/mm/dd\' ';
    $strSQL .= ' WHEN A.CNDFMT=\'3\' AND A.CNDSFL= \'1\' THEN \'yymmdd\' ';
	//20170613
    $strSQL .= ' WHEN A.CNDFMT=\'4\' AND A.CNDSFL= \'\'  THEN \'yyyy-mm-dd\' ';
    $strSQL .= ' WHEN A.CNDFMT=\'4\' AND A.CNDSFL= \'1\' THEN \'yyyymmdd\' ';
    $strSQL .= ' WHEN A.CNDFMT=\'5\' AND A.CNDSFL= \'\'  THEN \'yyyy-mm\' ';
    $strSQL .= ' WHEN A.CNDFMT=\'5\' AND A.CNDSFL= \'1\' THEN \'yyyymm\' ';
    $strSQL .= ' WHEN A.CNDFMT=\'6\' AND A.CNDSFL= \'\'  THEN \'yy-mm-dd\' ';
    $strSQL .= ' WHEN A.CNDFMT=\'6\' AND A.CNDSFL= \'1\' THEN \'yymmdd\' ';
    $strSQL .= ' ELSE \'\'';
    $strSQL .= ' END AS CLDFMT, ';
    $strSQL .= ' ROWNUMBER() OVER(  ORDER BY A.CNQRYN ASC ,A.CNQRYN ASC)as ROWNUM ';
    $strSQL .= ' FROM BSQLCND AS A ' ;
    $strSQL .= ' WHERE A.CNQRYN = ? ';
    $strSQL .= ' AND   ( A.CNDSTKB = \'1\' ';
    $strSQL .= ' OR A.CNDSTKB = \'2\' ';
    $strSQL .= ' OR  A.CNDSTKB = \'\' ) )AS E ';
    $params = array($CNQRYN);
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE E.ROWNUM BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data,true));
        }
    }
    return $data;

}