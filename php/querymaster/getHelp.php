<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$D1NAME = $_POST['D1NAME'];
$D1TEXT = $_POST['D1TEXT'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$Query = (isset($_POST['Query'])?$_POST['Query']:'');
$isQGflg = (isset($_POST['ISQGFLG']) ? $_POST['ISQGFLG'] : '');//クエリーグループならＴＲＵＥ
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);
$D1TEXT = cmHscDe($D1TEXT);
//クエリーグループ
$D1NAMELIST=array();//クエリーグループの全部データ
$LASTD1NAME='';//クエリーグループの一番最後のクエリーＩＤ
$isQGflg=($isQGflg==='true')?true:false;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    if($Query === '1'){
        $rs = cmCheckUser($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }else{
            $userData  = umEx($rs['data']);
            if($userData[0]['WUAUTH'] === '2'){
                $rs = cmChkKenGen($db2con,'12',$userData[0]['WUSAUT']);//'12' =>ユーザー補助
                if($rs['result'] !== true){
                    $rtn = 3;
                    $msg =  showMsg($rs['result'],array('ユーザー補助の権限'));
                }
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
    if($isQGflg){
        $rs=cmGetQueryGroup($db2con,$D1NAME,'','');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリー'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
        }
    }
}
/** クエリー存在チェック**/
if($rtn === 0){
    if($isQGflg){
        $userId=($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4')?
                $userData[0]['WUAUTH']:'';
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,$userId,$D1NAME,$res['QRYGSQRY'],$res['LASTFLG'],'','');
            if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }//end of for loop
    }else{
        $chkQry = array();
        $chkQry = cmChkQuery($db2con,'',$D1NAME,'');
        if($chkQry['result'] === 'NOTEXIST_PIV'){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET',array('ピボット'));
        }else if($chkQry['result'] !== true){
            $rtn = 1;
            $msg = showMsg($chkQry['result'],array('クエリー'));
        }
    }
}
if(!$isQGflg){
    if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
        if($rtn === 0){
            $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
            if($chkQryUsr === 'NOTEXIST_GET'){
                $rtn = 1;
                //$msg = showMsg('ログインユーザーに指定したクエリーに対しての検索候補の権限がありません。');
                 $msg =  showMsg('FAIL_QRY_USR',array('ユーザー補助の権限'));
            }else if($chkQryUsr !== true){
                $rtn = 1;
                $msg = showMsg($chkQryUsr['result'],array('クエリー'));
            }
        }
    }
}
if($proc === 'EDIT'){
    if($rtn === 0){
        if($isQGflg){
            foreach($D1NAMELIST as $result){
                $rs = fnSelFDB2CSV1($db2con,$result['QRYGSQRY']);
                if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
                }else{
                    $resultList = cmHscDe($rs['data']);
                    $resultList=umEx($resultList);
                    $data[]=$resultList[0];
                }
                
            }
        }else{
            $rs = fnSelFDB2CSV1($db2con,$D1NAME);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = cmHscDe($rs['data']);
            }
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $rs = fnGetAllCount($db2con,$D1NAME,$D1TEXT);
        if($rs['result'] !== true){
            $rtn =1;
            $msg=showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetFDB2CSV1($db2con,$D1NAME,$D1TEXT,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
            $rtn =1;
            $msg=showMsg($rs['result']);
        }else{
            $data = cmHscDe($rs['data']);
        }
    }
}

cmDb2Close($db2con);

if($isQGflg){
    $data=$data;
}else{
    $data=umEx($data,$htmlFlg);
}
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' =>$data
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con,$D1NAME,$D1TEXT,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.D1NAME ASC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM FDB2CSV1 as B ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params,'%'.$D1NAME.'%');
    }

    if($D1TEXT != ''){
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params,'%'.$D1TEXT.'%');
    }
    
    $strSQL .= ' ) as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$D1NAME = '',$D1TEXT = ''){
    $data = array();

    $strSQL  = ' SELECT count(A.D1NAME) as COUNT ';
    $strSQL .= ' FROM FDB2CSV1 as A ' ;
    $strSQL .= ' WHERE D1NAME <> \'\' ';

    $params = array();

    if($D1NAME != ''){
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params,'%'.$D1NAME.'%');
    }

    if($D1TEXT != ''){
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params,'%'.$D1TEXT.'%');
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
             $data = array('result' => 'FAIL_SEL');
        }else{

            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}

/*
*-------------------------------------------------------* 
* FDB2CSV1取得(1行)
*-------------------------------------------------------*
*/

function fnSelFDB2CSV1($db2con,$D1NAME){

    $data = array();

    $params = array($D1NAME);

    $strSQL  = ' SELECT A.* ';;
    $strSQL .= ' FROM FDB2CSV1 as A ';
    $strSQL .= ' WHERE D1NAME = ? ';

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}