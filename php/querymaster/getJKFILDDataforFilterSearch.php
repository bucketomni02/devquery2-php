<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$d1name = $_POST['D1NAME'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

/*//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
        error_log('11111111111111');
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'11',$userData[0]['WUSAUT']);//'11' =>ダウンロード制限
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('ダウンロード権限の権限'));
            }
        }
    }
}*/
//クエリー存在チェック
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$d1name,'');
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 1;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
/*if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$d1name,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 2;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのダウンロード権限の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('ダウンロード権限の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}*/
if($rtn === 0){
    //ユーザー名取得
    $rs = cmGetFDB2CSV2($db2con,$d1name,false,false,false);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result'],array('クエリー'));
        $rtn = 2;
    }else{
        $data = $rs['data'];
		$testData=array();
		$testobj=array();
		$testobj["MASTER"]='';
		$testobj["colheader"]='　';
		$testobj["tip"]='';
		$testData[]=$testobj;
        for ($i = 0; $i < count($data); $i++) {
			$testobj=array();
			$testobj["MASTER"]=$data[$i]["D2FILID"] . '-' . $data[$i]["D2FLD"];
			$testobj["colheader"]=$data[$i]["D2FLD"];
			$testobj["tip"]=$data[$i]["D2HED"];
			$testData[]=$testobj;
        }

    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'aaData' => umEx($testData),
	'iTotalRecords'=>count($testData),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));
