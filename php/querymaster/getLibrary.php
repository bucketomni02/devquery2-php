<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../base/getTblMBR.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$LIBNAME = (isset($_POST['LIBNAME']))?cmMer($_POST['LIBNAME']):'';


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$RTCD   = '';
$flg = true;
$aaa = array();
/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
if($LIBNAME !== ''){
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

    //ログインユーザが削除されたかどうかチェック
   if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $auth = $rs['data'][0];
        $WUAUTH = cmMer($auth['WUAUTH']);
        if($WUAUTH === '2'){
            $SAUT = cmMer($auth['WUSAUT']);
            $rs = cmSelDB2AGRT($db2con,$SAUT);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg =  showMsg($rs['result']);
            }else{
                $authData = umEx($rs['data']);
                foreach($authData as $key => $value){
                        $authInfo[$value['AGMNID']] = $value['AGFLAG'];
                }
                if($authInfo['3'] !== '1' && $authInfo['6'] !== '1' && $authInfo['7'] !== '1'){
                    if($authInfo['8'] !== '1' && $authInfo['9'] !== '1' && $authInfo['10'] !== '1'){
                        if($authInfo['11'] !== '1' && $authInfo['12'] !== '1' && $authInfo['13'] !== '1'){
                            if($authInfo['14'] !== '1' && $authInfo['15'] !== '1' && $authInfo['16'] !== '1' && $authInfo['30'] !== '1'){
                                $rtn = 2;
                                 $msg =  showMsg('NOTEXIST',array('クエリー作成の権限'));
                            }
                        }
                    }
                }
            }
        }
    }
}

if($rtn === 0){
    //必須チェック
    if($LIBNAME === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array(array('ライブラリー','名')));
        $focus = 'D1LIBITMLIBNM';
        $flg = false;
    }
}

 if($rtn === 0){
        if(!checkMaxLen($LIBNAME,10)){
            $rtn = 1;
            $msg = showMsg('FAIL_MAXLEN',array(array('ライブラリー','名')));
            $flg = false;
        }
    }

 if($rtn === 0){

    $rs = fnGetLibData($db2con,$LIBNAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array(array('ライブラリー','名')));
        $focus = 'D1LIBITMLIBNM';
        $flg = false;
        $getData = $rs['data'];
        
    }else{
       $getData = $rs['data'];
    }
}
    
cmDb2Close($db2con);

$db2con = cmDb2Con();
if($flg === true){
  if($rtn === 0){
    $rs = cmCallQRYOUTF($db2con, $LIBNAME);
    e_log('TMPTBL'.print_r($rs['RTCD'],true));
    $Libdev = $rs['LibDEV'];
    $Tmptbl = $rs['TmpTBl'];
    $LIbTbl = $rs['LibDEV'].'.'.$rs['TmpTBl'];
    $RTCD   = $rs['RTCD'];
    if($rs['result'] === false){
        $rtn = 1;
        $msg = showMsg('FAIL_QRY_RUN');
    }else{
      if($RTCD !== '9'){
        $rs = SelectTmpTbl($db2con, $LIbTbl);
        if($rs['result'] === false){
           $rtn = 1;
           $msg = showMsg('FAIL_QRY_RUN');
        }else{
           if(count($rs['data']) === 0){
              $rtn = 1;
              $msg = showMsg('NOTEXIST_GET',array(array('ライブラリー','名')));
           }else{
              $data = $rs['data'];
           }
        
         }
       }else{
           $flg = false;
       }
      
     }
    
  }
  



cmDb2Close($db2con);


/**return**/
 if($RTCD !== '9'){
    $rtnAry = array(
        'RTN' => $rtn,
        'MSG' => $msg,
        'aaData' => umEx($data),
        'LIBNAME' => $LIBNAME,
        'GETDATA'  => $getData
       
    );
        echo(json_encode($rtnAry));
      }
    }
    if($flg !== true){
        $rtnAry = array(
        'RTN' => $rtn,
        'MSG' => $msg,
        'LIBNAME' => $LIBNAME,
        'GETDATA'  => $getData
       
    );
        echo(json_encode($rtnAry));
       
     }
}

function fnGetLibData($db2con,$LibName){

    $data = array();
    $rs = true;

    $strSQL = ' SELECT A.SCHEMA_NAME,A.SCHEMA_TEXT ';
    $strSQL .= ' FROM ( SELECT B.SCHEMA_NAME';
    $strSQL .= ' , B.SCHEMA_TEXT';
    $strSQL .= ' FROM  ';
    $strSQL .= '    (SELECT * FROM ';
    $strSQL .= '    QSYS2/SYSSCHEMAS ';
    //$strSQL .= ' WHERE SYSTEM_SCHEMA_NAME <> \'QTEMP\' ';
    $strSQL .= '     ) B';
    $strSQL .= ' WHERE B.SCHEMA_NAME <> \'\'';
    $strSQL .= ' ) as A ';
    $strSQL .= ' LEFT JOIN  (SELECT * FROM DB2LBLS ) AS C ON A.SCHEMA_NAME = C.LIBSID ';
    
    if($LibName != ''){
        $strSQL .= ' WHERE A.SCHEMA_NAME IN ( ? )';
          $params = array(
             $LibName
          );
         
     }

   $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                    $rs = 'NOTEXIST';
            }
            
        }
    }
    return array('result' => $rs,'data' => $data);
}

function SelectTmpTbl($db2con, $LIbTbl){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT ODOBNM,ODOBTX ';
    $strSQL .= ' FROM ' ;
    $strSQL .=  $LIbTbl;
    e_log('TMPTBL'.print_r($LIbTbl,true));
    e_log('STRSQL'.print_r($strSQL,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            e_log('ArrayLib'.print_r($data,true));
           
        }
    }
    return array('result' => $rs,'data' => $data);
}

