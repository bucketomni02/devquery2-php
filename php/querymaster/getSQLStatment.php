<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../base/createTblExeSQL.php");
include_once("../base/createExecuteSQL.php");

$data          = array();
$rtn           = 0;
$msg           = '';

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$QRYNM    = (isset($_POST['D1NAME'])?$_POST['D1NAME']:'');
$D1CFLG = (isset($_POST['D1CFLG'])?$_POST['D1CFLG']:'');
if($rtn === 0){
    //DB接続
    $db2con = cmDb2Con();
    cmSetPHPQUERY($db2con);
}

if($D1CFLG === '1'){
    //SQLで作ったクエリー
    if($rtn === 0){
        $resSQLInfo = getQrySQLData($db2con,$QRYNM);
        if($resSQLInfo['RTN'] !== 0){
            $rtn = 1;
            $msg = $resSQLInfo['MSG'];
        }else{
            $qryData = $resSQLInfo['QRYDATA'];
        }
    }
    if($rtn === 0){
        //$paramArr = '';
        $paramArr = array();
        $fdb2csv1Info = $qryData['FDB2CSV1'][0];
        $libArr = preg_split ("/\s+/", $fdb2csv1Info['D1LIBL'] );
        $LIBLIST = join(' ',$libArr);
        if($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME){
             $db2LibCon = cmDb2ConLib($LIBLIST);
        } else {
            $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1Info['D1RDB'];
            $db2LibCon = cmDB2ConRDB($LIBLIST);
        }
        if(count($qryData['BSQLCND']) > 0){
            $paramArr = bindParamArr($qryData['BSQLCND']);
        }
        $strSQL = $qryData['BSQLEXEDAT']['EXESQL'];
        if(count($paramArr['WITHPARAMDATA'])>0){
            $strSQL = bindWithParamSQLQry($strSQL,$paramArr['WITHPARAMDATA']);
        }
        $strExeSQLParam = bindParamSQLQry($strSQL,$paramArr['PARAMDATA']);

    }
}else{
    // 作成定義のSQL文生成
    $resExeSql = runExecuteSQL($db2con,$QRYNM);
    if($resExeSql['RTN'] !== 0){
        $rtn = 1;
        $msg = $resExeSql['MSG'];
    }else{
        $qryData = $resExeSql;
        $strExeSQLParam = $qryData['STREXECSQL'];
    }
}

cmDb2Close($db2con);

$rtn = array(
    'RTN'       => $rtn,
    'MSG'       => $msg,
    'strExeSQLParam' => $strExeSQLParam
);
echo(json_encode($rtn));

