<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../base/comGetFDB2CSV2_CCSID.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$FLD = $_POST['FLD'];
$FILID = $_POST['FILID'];
$QYNAME = $_POST['QYNAME'];
$LINKKEY = $_POST['LINKKEY'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$dDfcolm = array();
$dLibfil = array();
$dDfcheck = array();
$dShosaiData = array();
$dHeaderName = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmCheckUser($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$QYNAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('クエリー'));
    }else{
        $fdb2csv1 = $rs['data'][0];
    }
}
if($rtn === 0){
    if(($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME)){
        $db2RDBCon = $db2con;
    }else{
        $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1['D1RDB'];
        $db2RDBCon = cmDB2ConRDB($fdb2csv1['D1LIBL']);
    }
}
if($rtn === 0){
    //検索キーがチェックされる値を取得する
    $rsDffkey = getDFFKEY($db2con,$QYNAME,$FILID,$FLD);
    if($rsDffkey['result'] !== true){
        $msg = showMsg($rsDffkey['result']);
        $rtn = 1;
    }else{
        $dDffkey = $rsDffkey['data'][0];
        $rsCCSID = fnGetFDB2CSV2CCSID($db2con,$QYNAME);
        if($rsCCSID['RTN'] !== 0){
            $rtn = 1;
            $msg = showMsg($rsCCSID['MSG']);
        }else{
            $key = array_search($FLD.'_'.$FILID, $rsCCSID['FLDDATA']);
            $dDffkey['CCSID'] = $rsCCSID['DATA'][$key]['CCSID'];
            $dDffkey['LENGTH'] = $rsCCSID['DATA'][$key]['LENGTH'];
        }
    }
}
if($rtn === 0){
    //ライブライとテーブル名を取得する
    $rsLibfil = getLIBFIL($db2con,$QYNAME,$FILID,$FLD);
    if($rsLibfil['result'] !== true){
        $msg = showMsg($rsLibfil['result']);
        $rtn = 1;
    }else{
        $dLibfil = $rsLibfil['data'];
    }
}
if($rtn === 0){
     //表示項目を取得する
    $rsDfcheck = getDFCHECK($db2con,$QYNAME,$FILID,$FLD);
    if($rsDfcheck['result'] !== true){
        $msg = showMsg($rsDfcheck['result']);
        $rtn = 1;
    }else{
        $dDfcheck = $rsDfcheck['data'];
    }
}
if($rtn === 0){
    $rs = fnCheckTable($db2RDBCon,$dLibfil[0]['DTLIBL'],$dLibfil[0]['DTFILE']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ファイル'));
    }
}
if($rtn === 0){
    //詳細表示のヘーダを取得する
    if(($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME)){
        $rsHdName = getHeaderName($db2con,$QYNAME,$FILID,$FLD,$dLibfil);
    }else{
        $rsHdName = getRDBHeaderName($db2con,$db2RDBCon,$QYNAME,$FILID,$FLD,$dLibfil);
    }
    if($rsHdName['result'] !== true){
        $msg = showMsg($rsHdName['result']);
        $rtn = 1;
    }else{
        $fldNM       = $rsHdName['FLDNM'];
        $dHeaderName = $rsHdName['data'];
        if(count($dHeaderName) !== count($dDfcheck)){
            $rtn = 1;
            $msg = showMsg('FAIL_FILCOL ',array('ファイルのフィールド'));//'指定されたフィールドが指定したファイルから削除される可能性があります。<br>詳細設定画面に確認してください。';
        }
    }
}
if($rtn === 0){
    //詳細表示のデータを取得する
    $rsShosaiData = getShosaiData($db2RDBCon,$dDffkey,$dLibfil,$dDfcheck,$LINKKEY,$fldNM,$dHeaderName,$fdb2csv1);
    if($rsShosaiData['result'] !== true){
        $msg = showMsg($rsShosaiData['result']);
        $rtn = 1;
    }else{
        $dShosaiData = $rsShosaiData['data'];
    }
}
if(($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME)=== false){
    cmDb2Close($db2RDBCon);
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'DATA' => umEx($dShosaiData,true),
    'LIBFIL'=> umEx($dLibfil,true),
    'HEADERNAME'=> umEx($dHeaderName,true),
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));

/**
* 検索キーがチェックされる値を取得する
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $QYNAME  クエリ名
* @param String  $FILID   FILID
* @param String  $FLD     FLD
* 
*/

function getDFFKEY($db2con,$QYNAME,$FILID,$FLD){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT DFCOLM ';
    $strSQL .= ' FROM   DB2WDFL'; 
    $strSQL .= ' WHERE  DFNAME = ? ';
    $strSQL .= ' AND    DFFILID= ? ';
    $strSQL .= ' AND    DFFLD= ? ';
    $strSQL .= ' AND    DFFKEY= \'1\' ';
    $params = array(
        $QYNAME,
        $FILID,
        $FLD
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}

/**
* ライブライとテーブル名を取得する
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $QYNAME  クエリ名
* @param String  $FILID   FILID
* @param String  $FLD     FLD
* 
*/

function getLIBFIL($db2con,$QYNAME,$FILID,$FLD){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT DTLIBL, ';
    $strSQL .= '        DTFILE, ';
    $strSQL .= '        DTMBR ';
    $strSQL .= ' FROM   DB2WDTL'; 
    $strSQL .= ' WHERE  DTNAME = ? ';
    $strSQL .= ' AND    DTFILID= ? ';
    $strSQL .= ' AND    DTFLD= ? ';

    $params = array(
        $QYNAME,
        $FILID,
        $FLD
    );
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}

/**
* 表示項目を取得する
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $QYNAME  クエリ名
* @param String  $FILID   FILID
* @param String  $FLD     FLD
* 
*/

function getDFCHECK($db2con,$QYNAME,$FILID,$FLD){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT   * ';
    $strSQL .= ' FROM     DB2WDFL'; 
    $strSQL .= ' WHERE    DFNAME = ? ';
    $strSQL .= ' AND      DFFILID= ? ';
    $strSQL .= ' AND      DFFLD= ? ';
    $strSQL .= ' AND      DFCHECK= \'1\' ';

    $params = array(
        $QYNAME,
        $FILID,
        $FLD
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => umEx($data));
        }
    }
    return $data;
}

/**
* 詳細表示のデータを取得する
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con   DBコネクション
* @param String  $dDffkey  検索キー
* @param String  $dLibfil  ライブライとテーブル名
* @param String  $dDfcheck 表示項目  
* 
*/

function getShosaiData($db2con,$dDffkey,$dLibfil,$dDfcheck,$LINKKEY,$FLDNMARR,$HEADERARR,$fdb2csv1){
    //e_log('詳細取得フィールド'.print_r($dDfcheck,true));
    $data = array();
    $params = array();
    $rtn = 0;
    $dDfcheckStr ='';
    //getDFCHECKで取得した表示フィールドをSQL文用コンマでつける
    for( $i = 0 ; $i < count($dDfcheck); $i++){
        $key = array_search($dDfcheck[$i]['DFCOLM'], $FLDNMARR);
        if($HEADERARR[$key]['CCSID'] === '65535'){

            if($HEADERARR[$key]['COLUMN_NAME'] === 'BREAKLVL' || $HEADERARR[$key]['COLUMN_NAME'] === 'OVERFLOW'){
                $fldNm = 'CAST('.$dDfcheck[$i]['DFCOLM'].'  AS CHAR(4) CCSID 5026) AS '.$dDfcheck[$i]['DFCOLM'];
            }else{
                $fldNm = 'CAST('.$dDfcheck[$i]['DFCOLM'].'  AS CHAR('.$HEADERARR[$key]['LENGTH'].') CCSID 5026) AS '.$dDfcheck[$i]['DFCOLM'];
            }


        }else{
            $fldNm = $dDfcheck[$i]['DFCOLM'];
        }
        if($i < (count($dDfcheck) - 1)){
            $dDfcheckStr .=  $fldNm . ',';
        }
        else{
            $dDfcheckStr .= $fldNm ;
        }
    }
    if($dLibfil[0]['DTMBR'] === '' || $dLibfil[0]['DTMBR'] === '*FIRST'){
        $DTMBR = '';
    }else if($dLibfil[0]['DTMBR'] === '*LAST'){
        $DTMBR = fnGetFilLast($fdb2csv1['D1RDB'],$fdb2csv1['D1LIBL'],$dLibfil[0]['DTLIBL'],$dLibfil[0]['DTFILE']);
    }else{
        $DTMBR = $dLibfil[0]['DTMBR'];
    }
    if($rtn === 0){
        //$rs = dropFileMBR($db2con,$dLibfil[0]['DTLIBL'],$dLibfil[0]['DTFILE'],$DTMBR,'QTEMP_'.$dLibfil[0]['DTFILE']);
        $rs = setFileMBR($db2con,$dLibfil[0]['DTLIBL'],$dLibfil[0]['DTFILE'],$DTMBR,'QTEMP_'.$dLibfil[0]['DTFILE']);
        if($rs !== true){
            e_log('テーブル設定失敗');
            $data = array('result' => $rs);
            $rtn = 1;
        }
    }
    if($rtn === 0){
        //SQL文を作る
        $strSQL  = ' SELECT ';
        $strSQL .=  $dDfcheckStr ;
        $strSQL .= ' FROM ';
        $strSQL .=  'QTEMP/QTEMP_'.$dLibfil[0]['DTFILE'];
        $strSQL .= ' WHERE ';
        if($dDffkey['CCSID'] === '65535'){

            if($dDffkey[$key]['COLUMN_NAME'] === 'BREAKLVL' || $dDffkey[$key]['COLUMN_NAME'] === 'OVERFLOW'){
                $strSQL .= ' CAST('.$dDffkey['DFCOLM'].'  AS CHAR(10) CCSID 5026) = ? ';
            }else{
                $strSQL .= ' CAST('.$dDffkey['DFCOLM'].'  AS CHAR(10) CCSID 5026) = ? ';
            }


        }else{
            $strSQL .= $dDffkey['DFCOLM'].' = ? ';
        }
        $strSQL .= 'FETCH FIRST 1 ROW ONLY ';
        $params = array(
            $LINKKEY
        );
        error_log('Shosai data = '.$strSQL.'params = '.print_r($params,true).db2_stmt_errormsg());

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            e_log($strSQL.db2_stmt_errormsg());
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                e_log(db2_stmt_errormsg().$strSQL.print_r($params,true));
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => umEx($data));
            }
        }
    }
    /*if($rtn === 0){
        $rs = dropFileMBR($db2con,$dLibfil[0]['DTLIBL'],$dLibfil[0]['DTFILE'],$DTMBR,'QTEMP_'.$dLibfil[0]['DTFILE']);
    }*/
    return $data;
}

/**
* カラム名取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $QYNAME  クエリ名
* @param String  $FILID   FILID
* @param String  $FLD     FLD
* @param String  $dLibfil ライブラリ、ファイル
* 
*/

function getHeaderName($db2con,$QYNAME,$FILID,$FLD,$dLibfil){

    $data = array();
    $fldNmArr = array();
    $params = array();

    
    $strSQL  = ' SELECT COLUMN_NAME,COLUMN_HEADING ';
    $strSQL .= ' , CCSID ';
    $strSQL .= ' , LENGTH ';
    $strSQL .= ' FROM DB2WDTL  ';
    $strSQL .= ' JOIN ';
    $strSQL .= ' DB2WDFL ';
    $strSQL .= ' ON DTNAME = DFNAME  ';
    $strSQL .= ' AND DTFILID = DFFILID '; 
    $strSQL .= ' AND DTFLD = DFFLD ';
    $strSQL .= ' AND DTNAME = ? ';
    $strSQL .= ' AND DTFILID = ?  ';
    $strSQL .= ' AND DTFLD = ? ';
    $strSQL .= ' AND DFCHECK = \'1\' ';
    $strSQL .= ' LEFT JOIN  ';
    $strSQL .= '    (SELECT ';
    $strSQL .= '        SYSTEM_COLUMN_NAME AS COLUMN_NAME, ';
    if(SYSCOLCFLG==='1'){
        $strSQL .= '        COLUMN_TEXT AS COLUMN_HEADING, ';
    }else{
        $strSQL .= '        COLUMN_HEADING, ';
    }
    $strSQL .= '        ORDINAL_POSITION, ';
    $strSQL .= '        CCSID, ';
    $strSQL .= '        LENGTH ';
    $strSQL .= '    FROM  ';
    $strSQL .=  SYSCOLUMN2 ;
    $strSQL .= '    WHERE ';
    $strSQL .= '        TABLE_SCHEMA = ? AND ';
    $strSQL .= '        TABLE_NAME = ? ';
    $strSQL .= '        ) AS A ';
    $strSQL .= ' ON DFCOLM = A.COLUMN_NAME ';
    $strSQL .= ' WHERE COLUMN_NAME IS NOT NULL ';
    $strSQL .= ' ORDER BY A.ORDINAL_POSITION ';

    $params = array(
        $QYNAME,
        $FILID,
        $FLD,
        $dLibfil[0]['DTLIBL'],
        $dLibfil[0]['DTFILE']
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
                $fldNmArr[] = cmMer($row[COLUMN_NAME]);
            }
            $data = array('result' => true,'data' => umEx($data),'FLDNM' => $fldNmArr);
        }
    }
    return $data;
}
function getRDBHeaderName($db2con,$db2RDBcon,$QYNAME,$FILID,$FLD,$dLibfil){
    $rtn = 0;
    $fldNmArr = array();
    $syscolumn2 = array();
    
    $strSQL  =  ' SELECT  ';
    $strSQL .=  '           A.SYSTEM_COLUMN_NAME AS COLUMN_NAME '; 
    $strSQL .=  '          ,A.LENGTH '; 
    $strSQL .=  '          ,CASE A.DDS_TYPE ';
    $strSQL .=  '            WHEN \'H\' THEN \'A\' ';
    $strSQL .=  '            WHEN \'J\' THEN \'0\' ';
    $strSQL .=  '            WHEN \'E\' THEN \'0\' ';
    $strSQL .=  '            ELSE A.DDS_TYPE '; 
    $strSQL .=  '            END AS DDS_TYPE ';
    $strSQL .=  '          ,A.NUMERIC_SCALE '; 
    if(SYSCOLCFLG==='1'){
        $strSQL .=  '          ,A.COLUMN_TEXT AS  COLUMN_HEADING'; 
    }else{
        $strSQL .=  '          ,A.COLUMN_HEADING '; 
    }
    $strSQL .=  '          ,A.ORDINAL_POSITION '; 
    $strSQL .=  '          ,A.TABLE_SCHEMA '; 
    $strSQL .=  '          ,A.TABLE_NAME ';
    $strSQL .=  '          ,A.CCSID '; 
    $strSQL .=  '       FROM ' . SYSCOLUMN2 .' A '; 
    $strSQL .=  '       WHERE A.TABLE_SCHEMA = ? '; 
    $strSQL .=  '       AND A.TABLE_NAME = ? '; 
    //$strSQL .=  '       AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\') ';
//    $strSQL .=  '       AND A.DDS_TYPE NOT IN(\'5\') ';
        $strSQL .=  '       ORDER BY A.ORDINAL_POSITION ';

    $stmt = db2_prepare($db2RDBcon,$strSQL);
    if($stmt === false){
        $syscolumn2 = array('result' => 'FAIL_SEL');
        $rtn = 1;
    }else{
        $params = array(
            $dLibfil[0]['DTLIBL'],
            $dLibfil[0]['DTFILE']
        );
        $r = db2_execute($stmt,$params);
        
        if($r === false){
            $syscolumn2 = array('result' => 'FAIL_SEL');
            $rtn = 1;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $syscolumn2[] = $row;
            }
            $syscolumn2 = umEx($syscolumn2,false);
            $syscolumn2 = array('result' => true,'data' => $syscolumn2);
            //e_log('取得したデータ：'.print_r($syscolumn2,true));
        }
    }

    if($rtn === 0){
        e_log('一時テーブル作成開始');
        
        // 一時テーブル作成
        $createSQL  = 'DECLARE GLOBAL TEMPORARY TABLE SYSCOLUMN2 ';
        $createSQL .= ' ( ';
        $createSQL .= ' SYSTEM_COLUMN_NAME CHAR(10) , ';
        $createSQL .= ' LENGTH INTEGER , ';
        $createSQL .= ' DDS_TYPE CHAR(1), ';
        $createSQL .= ' NUMERIC_SCALE INTEGER DEFAULT NULL , ';
        $createSQL .= ' COLUMN_HEADING VARGRAPHIC(60) CCSID 1200 , ';
        $createSQL .= ' ORDINAL_POSITION INTEGER, ';
        $createSQL .= ' TABLE_SCHEMA VARCHAR(128), ';
        $createSQL .= ' TABLE_NAME VARCHAR(128),';
        $createSQL .= ' CCSID CHAR(5) ';
        $createSQL .= ' )  WITH REPLACE  ';
        //e_log($createSQL);
        $result = db2_exec($db2con, $createSQL);
        if ($result) {
            e_log( 'テーブル作成成功');
            if(count($syscolumn2['data'])>0){
                $syscolumn2 = umEx($syscolumn2['data']);
                
                $strInsSQL  = ' INSERT INTO QTEMP/SYSCOLUMN2 ';
                $strInsSQL .= ' ( ';
                $strInsSQL .= '     SYSTEM_COLUMN_NAME, ';
                $strInsSQL .= '     LENGTH, ';
                $strInsSQL .= '     DDS_TYPE, ';
                $strInsSQL .= '     NUMERIC_SCALE, ';
                $strInsSQL .= '     COLUMN_HEADING, ';
                $strInsSQL .= '     ORDINAL_POSITION, ';
                $strInsSQL .= '     TABLE_SCHEMA, ';
                $strInsSQL .= '     TABLE_NAME, ';
                $strInsSQL .= '     CCSID ';
                $strInsSQL .= ' ) ';
                $strInsSQL .= ' VALUES ( ?,?,?,?,?,?,?,?,?)';

                $stmt = db2_prepare($db2con,$strInsSQL);
                if($stmt === false ){
                    $data = array(
                                'result' => 'FAIL_INS',
                                'errcd'  => 'SYSCOLUMN2:'.db2_stmt_errormsg()
                            );
                    $result = 1;
                }else{
                    foreach($syscolumn2 as $syscolumn2data){
                        $params =array(
                                    $syscolumn2data['COLUMN_NAME'],
                                    $syscolumn2data['LENGTH'],
                                    $syscolumn2data['DDS_TYPE'],
                                    ($syscolumn2data['NUMERIC_SCALE'] == '') ? NULL : $syscolumn2data['NUMERIC_SCALE'],
                                    $syscolumn2data['COLUMN_HEADING'],
                                    $syscolumn2data['ORDINAL_POSITION'],
                                    $syscolumn2data['TABLE_SCHEMA'],
                                    $syscolumn2data['TABLE_NAME'],
                                    $syscolumn2data['CCSID']
                                );
                        //e_log($strInsSQL.print_r($params,true));
                        $res = db2_execute($stmt,$params);
                        if($res === false){
                            $data = array(
                                    'result' => 'FAIL_INS',
                                    'errcd'  => 'SYSCOLUMN2:'.db2_stmt_errormsg()
                                );
                            $result = 1;
                            break;
                        }else{
                            $result = 0;
                            $data = array('result' => true);
                        }
                    }
                }
            }
            if($result !== 0){
                $rtn = 1;
            }
        }else{
            e_log('テーブル作成失敗');
            e_log('テーブルerror'.db2_stmt_errormsg());
            $rtn = 1;
            $data = array(
                'result' => 'FAIL_SEL',
                'errcd'  => 'テーブル作成失敗'
                );
        }
    }
/*
    if($rtn === 0){
            $strSQL = '';

        $strSQL  =' SELECT \'\' AS SEQ ';
        $strSQL .='       ,A.SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
        $strSQL .='       ,A.LENGTH ';
        $strSQL .='       ,CASE A.DDS_TYPE';
        $strSQL .='         WHEN \'H\' THEN \'A\'';
        $strSQL .='         WHEN \'J\' THEN \'0\'';
        $strSQL .='         WHEN \'E\' THEN \'0\'';
        $strSQL .='         ELSE A.DDS_TYPE ';
        $strSQL .='         END AS DDS_TYPE';
        $strSQL .='       ,A.NUMERIC_SCALE ';
        $strSQL .='       ,A.COLUMN_HEADING ';
        $strSQL .='       ,\'\' AS FORMULA ';
        $strSQL .='       ,\'\' AS SORTNO ';
        $strSQL .='       ,\'\' AS SORTTYP ';
        $strSQL .='       ,\'\' AS EDTCD ';
        $strSQL .='       ,A.ORDINAL_POSITION ';
        $strSQL .=' FROM QTEMP/SYSCOLUMN2 A ';
        $strSQL .=' WHERE A.TABLE_SCHEMA = ? ';
        $strSQL .=' AND A.TABLE_NAME = ? ';
        //$strSQL .=' AND A.DDS_TYPE NOT IN(\'L\',\'T\',\'Z\',\'5\')';
        $strSQL .=' AND A.DDS_TYPE NOT IN(\'5\')';
                $strSQL .=' ORDER BY A.ORDINAL_POSITION ';
        
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
            //e_log('error'.db2_stmt_errormsg());
            $rtn = 1;
        }else{
            $params = array(
                $dLibfil[0]['DTLIBL'],
                $dLibfil[0]['DTFILE']
            );
            $r = db2_execute($stmt,$params);
            
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
                $rtn = 1;
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = umEx($data,false);
                $data = array('result' => true,'data' => $data);
            }
            e_log('temptable'.print_r($data,true));
        }
    }*/
    $data = array();
    if($rtn === 0){
        $params = array();
        $strSQL  = ' SELECT COLUMN_NAME,COLUMN_HEADING ';
        $strSQL .= ' , CCSID ';
        $strSQL .= ' , LENGTH ';
        $strSQL .= ' FROM DB2WDTL  ';
        $strSQL .= ' JOIN ';
        $strSQL .= ' DB2WDFL ';
        $strSQL .= ' ON DTNAME = DFNAME  ';
        $strSQL .= ' AND DTFILID = DFFILID '; 
        $strSQL .= ' AND DTFLD = DFFLD ';
        $strSQL .= ' AND DTNAME = ? ';
        $strSQL .= ' AND DTFILID = ?  ';
        $strSQL .= ' AND DTFLD = ? ';
        $strSQL .= ' AND DFCHECK = \'1\' ';
        $strSQL .= ' LEFT JOIN  ';
        $strSQL .= ' (SELECT SYSTEM_COLUMN_NAME AS COLUMN_NAME,COLUMN_HEADING,ORDINAL_POSITION, CCSID,LENGTH FROM QTEMP/SYSCOLUMN2 WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ?) AS A ';
        $strSQL .= ' ON DFCOLM = A.COLUMN_NAME ';
        $strSQL .= ' WHERE COLUMN_NAME IS NOT NULL ';
        $strSQL .= ' ORDER BY A.ORDINAL_POSITION ';

        $params = array(
            $QYNAME,
            $FILID,
            $FLD,
            $dLibfil[0]['DTLIBL'],
            $dLibfil[0]['DTFILE']
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
            e_log(db2_stmt_errormsg());
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                    $fldNmArr[] = cmMer($row[COLUMN_NAME]);
                }
                $data = array('result' => true,'data' => umEx($data),'FLDNM' => $fldNmArr);
            }
        }
    }
    return $data;
}
/*
*-------------------------------------------------------* 
* テーブルがあるかないかチェック
*-------------------------------------------------------*
*/

function fnCheckTable($db2con,$LIB,$FIL){

    $data = array();
    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM QSYS2/SYSTABLES ';
    $strSQL .= ' WHERE SYSTEM_TABLE_SCHEMA = ? ';
    $strSQL .= ' AND SYSTEM_TABLE_NAME = ? ';

    $params = array(
        $LIB,
        $FIL
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'FAIL_COL_HEAD'); 
            }
            else{
               $data = array('result' => true);
            }
        }
    }
   return $data;
}
/*
 * FDB2CSV1の対象定義データ取得
 */
function fnGetFDB2CSV1($db2con,$QRYNM){
    $data = array();
    $strSQL  = '    SELECT * ';
    $strSQL .= '    FROM FDB2CSV1 ';
    $strSQL .= '    WHERE D1NAME = ?  ';
    

    $params = array(
        $QRYNM
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET'); 
            }
            else{
               $data = array('result' => true,'data' => umEx($data) );
            }
        }
    }
    return $data;
}
