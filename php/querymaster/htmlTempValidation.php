<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$success = true;
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'8',$userData[0]['WUSAUT']);//'8' => HTMLテンプレート
            if($rs['result'] !== true){
                $rtn = 4;
                $msg =  showMsg($rs['result'],array('HTMLテンプレートの権限'));
            }
        }
    }
}
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$HTMLFILE = (isset($_POST['HTMLFILE']))?$_POST['HTMLFILE']:'';
$PROC  = (isset($_POST['PROC']))?$_POST['PROC']:'';
$aaData = array();
if($rtn === 0){
    if($PROC !== 'DEL'){
        if($rtn === 0){
            if($HTMLFILE === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_SLEC',array('ファイル'));//'ファイルを選択してください。';
            }
        }
        if($rtn === 0){
           $ext = explode ('.', $HTMLFILE);
           $ext = $ext [count ($ext) - 1];
           if($ext === 'html'){
                $rtn = 0;
            }else{
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('テンプレート',array('HTML','ファイル')));
            }
        }
    }else{
        if($HTMLFILE === ''){
            $rtn =3;
            $msg = showMsg('TMP_FUNC',array('テンプレート'));//'テンプレートの登録解除処理が完了しました。';
        }else{
            $rtn = 0;
        }
    }
}
/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'PROC' =>$PROC
);
echo(json_encode($rtnArray));