<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$rtn     = 0;
$msg     = '';
$success = true;
$db2con  = cmDb2Con();
cmSetPHPQUERY($db2con);
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */

$D1NAME  = (isset($_POST['D1NAME'])) ? $_POST['D1NAME'] : '';
$PROC    = (isset($_POST['PROC'])) ? $_POST['PROC'] : '';
$htmltag = array();
$aaData  = array();
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'8',$userData[0]['WUSAUT']);//'8' => HTMLテンプレート
            if($rs['result'] !== true){
                $rtn = 4;
                $msg =  showMsg($rs['result'],array('HTMLテンプレートの権限'));
            }
        }
    }
}
if ($rtn === 0) {
    $chkQry = array();
    $chkQry = cmChkQuery($db2con, '', $D1NAME, '');
    if ($chkQry['result'] === 'NOTEXIST_PIV') {
        $rtn = 2;
        $msg = showMsg('NOTEXIST_GET', array(
            'ピボット'
        ));
    } else if ($chkQry['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($chkQry['result'], array(
            'クエリー'
        ));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 4;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのHTMLテンプレートの権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('HTMLテンプレートの権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if ($rtn === 0) {
    if ($PROC === 'OPEN') {
        $data = fnGetDB2HTMLT($db2con, $D1NAME);
        if ($data['result'] === true) {
            //                        $aaData = umEx($data['data'],true);
            $aaData = umEx($data['data']);
        } else {
            $rtn = 1;
            $msg = showMsg($res['result'], array(
                'クエリー'
            ));
        }
    } else if ($PROC === 'DEL') {
        $res = fnDeleteDB2HTMLT($db2con, $D1NAME);
        if ($res['result'] === true) {
            $files = glob(BASE_DIR . "/php/htmltemplate/" . $D1NAME . ".*");
            foreach ($files as $file) {
                unlink($file);
            }
            $msg = showMsg('TMP_FUNC',array('テンプレート'));//'テンプレートの登録解除処理が完了しました。';
        } else {
            $rtn = 1;
            $msg = showMsg($res['result'], array(
                'クエリー'
            ));
        }
    } else {
        if (isset($_FILES)) {
            // $rtn = 1;
            $file_content　    = '';
            $temp_file_name     = $_FILES['htmlfile']['tmp_name'];
            $original_file_name = $_FILES['htmlfile']['name'];
            $htmltag            = fnGetDB2HTMLK_INSHI($db2con);
            if ($htmltag['result'] === true) {
                // Find file extention
                $ext = explode('.', $original_file_name);
                $ext = $ext[count($ext) - 1];
                if ($ext === 'html' || $original_file_name === '') {
                    if ($original_file_name === '') {
                        $HTMLTN       = '';
                        $content      = "../htmltemplate/" . $D1NAME . ".html";
                        //   $file_content = file_get_contents($content);
                        $file_content = htmlspecialchars_decode(file_get_contents($content)); //file_get_contents($content);
                    } else {
                        $file_content = htmlspecialchars_decode(file_get_contents($temp_file_name)); // file_get_contents($temp_file_name);
                        //  $file_content =file_get_contents($temp_file_name);
                        // Remove the extention from the original file name
                       // $file_name    = str_replace($ext, '', $original_file_name);
                        $HTMLTN       = $original_file_name;
                    }
                    //check kinshi tag contains in html template
                    $html_tagdata = umEx($htmltag['data']);
                    $kinshi       = false;
                    foreach ($html_tagdata as $key => $value) {
                        // $tag = '<'.$value['HTMLNM'];
                        $l_tag = '<' . $value['HTMLNM'];
                        $u_tag = '<' . strtoupper($value['HTMLNM']);
                        if (strpos($file_content, $l_tag) !== false || strpos($file_content, $u_tag) !== false) {
                            $kinshi = true;
                            $rtn    = 1;
                            $msg    = showMsg('HTML_LIMIT');//" HTMLに禁止タグが含まれています。";
                            break;
                        }
                    }
                    if (!mb_check_encoding($file_content, 'UTF-8')) {
                        $rtn = 1;
                        $msg = showMsg('HTML_UTF');//"HTMLテンプレートはUTF-8のファイルのみ登録できます。";
                    }
                    // テンプレート名入力桁数チェック
                    if (!checkMaxLen($HTMLTN, 50)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(
                            'テンプレート名'
                        ));
                    }
                    if ($rtn === 0) {
                        if ($original_file_name !== '') {
                            $res = fnInsertDB2HTMLT($db2con, $D1NAME, $HTMLTN);
                            if ($res['result'] === true) {
                                $files = glob(BASE_DIR . "/php/htmltemplate/" . $D1NAME . ".*");
                                foreach ($files as $file) {
                                    unlink($file);
                                }
                                $D1NAME = $D1NAME . '.' . $ext;
                                //unlink("../template/".$D1NAME);
                                $flg    = move_uploaded_file($temp_file_name, "../htmltemplate/$D1NAME");
                                if ($flg === true) {
                                    $msg = showMsg('TMP_FUNC',array('テンプレートの登録処理'));//'テンプレートの登録処理が完了しました。';
                                } else {
                                    $rtn     = 1;
                                    $success = false;
                                    $msg     = showMsg('FAIL_FILE_INS',array('テンプレートの登録処理'));// 'テンプレートの登録処理ができませんでした。';
                                }
                            } else {
                                $rtn = 1;
                                $msg = showMsg($res['result'], array(
                                    'クエリー'
                                ));
                            }
                        } else {
                            $msg =showMsg('TMP_FUNC',array('テンプレートの登録処理'));// 'テンプレートの登録処理が完了しました。';
                        }
                        
                    }
                } else {
                    $rtn = 1;
                    $msg = showMsg('CHK_FMT', array(
                        'テンプレート',
                        'HTMLファイル'
                    ));
                }
            } //end of html tag
        } //end of isset file
    }
}
/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'success' => $success,
    'aaData' => $aaData,
    'FILES' => $_FILES,
    'PROC' => $PROC
);
echo (json_encode($rtnArray));

/*
 *-------------------------------------------------------* 
 * FDB2CSV1取得
 *-------------------------------------------------------*
 */

function fnDeleteDB2HTMLT($db2con, $HTMLTD)
{
    
    $data = array();
    
    $params = array(
        $HTMLTD
    );
    $strSQL = 'DELETE FROM DB2HTMLT';
    $strSQL .= ' WHERE HTMLTD = ? ';
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            $data = array(
                'result' => true
            );
        }
    }
    return $data;
}

function fnInsertDB2HTMLT($db2con, $HTMLTD, $HTMLTN)
{
    $data = array();
    $res  = fnDeleteDB2HTMLT($db2con, $HTMLTD);
    if ($res['result'] === true) {
        $params = array(
            $HTMLTD,
            $HTMLTN
        );
        $strSQL = ' INSERT INTO DB2HTMLT ';
        $strSQL .='(';
        $strSQL .='HTMLTD,';
        $strSQL .='HTMLTN';
        $strSQL .=')';
        $strSQL .='VALUES';
        $strSQL .='(?,?)';
        $stmt   = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array(
                    'result' => 'FAIL_SEL'
                );
            } else {
                $data = array(
                    'result' => true
                );
            }
        }
    } else {
        $data = array(
            'result' => $res['result']
        );
    }
    return $data;
}