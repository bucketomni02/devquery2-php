<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D3NAME  = $_POST['D3NAME'];
$D3FILID = $_POST['D3FILID'];
$D3JSEQ  = $_POST['D3JSEQ'];
$D3CNT   = $_POST['D3CNT'];
$D1WEBF  = $_POST['D1WEBF'];
$D3FLD  = $_POST['D3FLD'];


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$SELLIST = array();
$htmlFlg = true;
$D1CFLG='';//SQLクエリーFLG
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D3NAME = cmHscDe($D3NAME);
$D3FILID = cmHscDe($D3FILID);
$D3JSEQ = cmHscDe($D3JSEQ);
$D3CNT = cmHscDe($D3CNT);
$D1WEBF= cmHscDe($D1WEBF);
$D3FLD= cmHscDe($D3FLD);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
            $userData  = umEx($rs['data']);
            if($userData[0]['WUAUTH'] === '2'){
                $rs = cmChkKenGen($db2con,'13',$userData[0]['WUSAUT']);//'13' =>検索候補
                if($rs['result'] !== true){
                    $rtn = 2;
                    $msg =  showMsg($rs['result'],array('検索候補の権限'));
                }
            }
     }
}
/** クエリー存在チェック**/
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$D3NAME,'');
    if($chkQry['result'] === 'NOTEXIST_PIV'){
        $rtn = 3;
        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
    }else if($chkQry['result'] !== true){
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D3NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての検索候補の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('検索候補の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $rs = fnGetD1LIBL($db2con,$D3NAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('クエリー'));
    }else{
        $D1CFLG=$rs['D1CFLG'];
        $D1LIBL = $rs['D1LIBL'];
        if(cmMer($D1LIBL) !== '' ){
            $D1LIBL = preg_replace('/\s+/', ' ', cmMer($D1LIBL));
            $SELLIST = explode(" ", $D1LIBL);
        }
    }

}
if($D1WEBF !== '1'){
    if($rtn === 0){
        $rs = fnSelFDB2CSV3($db2con,$D3NAME,$D3FILID,$D3JSEQ,$D3CNT,$D3FLD);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
            if(count($data) === 0){
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET',array('フィールド'));
            }
        }
    }
}else{
    //SQLのクエリーと普通のクエリー
    if($D1CFLG==='1'){
        if($rtn===0){
            $rs=fnGetBSQLCND($db2con,$D3NAME,$D3JSEQ);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
                if(count($data) === 0){
                    $rtn = 1;
                    $msg = showMsg('NOTEXIST_GET',array('フィールド'));
                }
            }
        }
    }else{
        if($rtn === 0){
            $rs = fnSelBQRYCND($db2con,$D3NAME,$D3FILID,$D3JSEQ,$D3CNT,$D3FLD);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
                if(count($data) === 0){
                    $rtn = 1;
                    $msg = showMsg('NOTEXIST_GET',array('フィールド'));
                }
            }
        }
    }
}


cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'LIBLIST' => $SELLIST,
    'aaData' => umEx($data,$htmlFlg)
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV3取得
*-------------------------------------------------------*
*/

function fnSelFDB2CSV3($db2con,$D3NAME,$D3FILID,$D3JSEQ,$D3CNT,$D3FLD){

    $data = array();
    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV3 AS A ';
    $strSQL .= ' WHERE D3NAME = ? AND ';
    $strSQL .= ' D3FILID = ? AND ';
    $strSQL .= ' D3JSEQ = ? AND ';
    $strSQL .= ' D3CNT = ?  AND ';
    $strSQL .= ' D3FLD = ? ';

    $params = array($D3NAME,$D3FILID,$D3JSEQ,$D3CNT,$D3FLD);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}
/*
*-------------------------------------------------------* 
* BQRYCND取得
*-------------------------------------------------------*
*/

function fnSelBQRYCND($db2con,$D3NAME,$D3FILID,$D3JSEQ,$D3CNT,$D3FLD){
    $D3JSEQ = explode('-',$D3JSEQ);
    $CNMSEQ = $D3JSEQ[0];
    $CNSSEQ = $D3JSEQ[1];
    $data = array();
    $strSQL  = ' SELECT A.CNQRYN AS D3NAME ';
    $strSQL .= '      , A.CNFILID AS D3FILID ';
    $strSQL .= '      , CONCAT(CONCAT(A.CNMSEQ,\'-\'),A.CNSSEQ) AS D3JSEQ ';
    $strSQL .= '      , A.CNCANL AS D3CANL ';
    $strSQL .= '      , A.CNCANF AS D3CANF ';
    $strSQL .= '      , A.CNCMBR AS D3CMBR ';
    $strSQL .= '      , A.CNCANC AS D3CANC ';
    $strSQL .= '      , A.CNCANG AS D3CANG ';
    $strSQL .= '      , A.CNNAMG AS D3NAMG ';
    $strSQL .= '      , A.CNNAMC AS D3NAMC ';
    $strSQL .= '      , A.CNCAST AS D3CAST ';
    $strSQL .= '      , A.CNNAST AS D3NAST ';
    $strSQL .= ' FROM BQRYCND AS A ';
    $strSQL .= ' WHERE CNQRYN = ?  ';
    $strSQL .= ' AND CNFILID = ?  ';
    $strSQL .= ' AND CNMSEQ = ?  ';
    $strSQL .= ' AND CNSSEQ = ? ';
    $strSQL .= ' AND CNFLDN = ? ';
    $params = array($D3NAME,$D3FILID,$CNMSEQ,$CNSSEQ,$D3FLD);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}
/*
*-------------------------------------------------------* 
* SQL定義名取得
* SQLクエリーかどうかのFLG
* return count
*-------------------------------------------------------*
*/
function fnGetBSQLCND($db2con,$CNQRYN,$D3JSEQ){
    $data = array();
    $strSQL  = ' SELECT A.CNQRYN AS D3NAME';
    $strSQL .= ' ,\'\' AS D3FILID';
    $strSQL .= ' ,A.CNDSEQ AS D3JSEQ';
    $strSQL .= ' ,A.CNCANL AS D3CANL';
    $strSQL .= ' ,A.CNCANF AS D3CANF ';
    $strSQL .= ' ,A.CNCMBR AS D3CMBR ';
    $strSQL .= ' ,A.CNCANC AS D3CANC ';
    $strSQL .= ' ,A.CNCANG AS D3CANG ';
    $strSQL .= ' ,A.CNNAMG AS D3NAMG ';
    $strSQL .= ' ,A.CNNAMC AS D3NAMC ';
    $strSQL .= ' ,A.CNCAST AS D3CAST ';
    $strSQL .= ' ,A.CNNAST AS D3NAST ';
    $strSQL .= ' FROM BSQLCND AS A ';
    $strSQL .= ' WHERE CNQRYN = ? ';
    $strSQL .= ' AND CNDSEQ = ? ';
    $params = array($CNQRYN,$D3JSEQ);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' =>$data);
        }
    }
    return $data;

}
/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$D1NAME = ''){
    $data = array();        
    $strSQL  = ' SELECT count(A.D3NAME) as COUNT ';
    $strSQL .= ' FROM FDB2CSV3 as A ' ;
    $strSQL .= ' WHERE D3NAME = ? ';

    $params = array($D1NAME);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}

function fnGetD1LIBL($db2con,$D1NAME){
    $data   = array();
    $params = array();
    $strSQL = '';
    $D1LIBL = '';
    $D1CFLG='';

    $strSQL  =   '   SELECT D1NAME ';
    $strSQL .=   '        , D1LIBL ';
    $strSQL .=   '        , D1CFLG ';
    $strSQL .=   '   FROM FDB2CSV1 ';
    $strSQL .=   '   WHERE D1NAME = ? ';

    $params = array($D1NAME);
    $stmt = db2_prepare($db2con, $strSQL);
    
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if(count($data) > 0){
                $D1LIBL = $data[0]['D1LIBL'];
                $D1CFLG = $data[0]['D1CFLG'];
                $data = array(
                    'result' => true,
                    'D1LIBL' => $D1LIBL,
                    'D1CFLG'=>$D1CFLG
                );
             }else{
                $data = array(
                    'result' => 'NOTEXIST_GET',
                );
            }
        }
    }
    return $data;
}
