<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$LIB  = $_POST['LIB'];
$FIE = $_POST['FIE'];
$QRYNM = $_POST['QRYNM'];
$LIBSNM = '';
$LIBSDB = '';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$LIB = cmHscDe($LIB);
$FIE = cmHscDe($FIE);
$QRYNM = cmHscDe($QRYNM);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'10',$userData[0]['WUSAUT']);//'10' => 詳細情報設定権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('詳細情報設定の権限'));
            }
        }
    }
}

if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('クエリー'));
    }else{
        $fdb2csv1 = $rs['data'][0];
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$QRYNM,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 1;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対しての詳細情報設定の権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('詳細情報設定の権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
/**20170626 akz**/
if($rtn === 0){
    $LIBSNM = cmMer($userData[0]['WULIBL']);
}
/**20170626 akz**/

if($rtn === 0){
    //必須チェック
    if($LIB === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('ライブラリー'));
        $focus = 'LIB';
    }
}


if($rtn === 0){
    //必須チェック
    if($FIE === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('ファイル'));
        $focus = 'FIE';
    }
}
if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$QRYNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('クエリー'));
    }else{
        $fdb2csv1 = $rs['data'][0];
    }
}
$filterLibList = array();
if($rtn === 0){
    /**20170525 akz**/
    if($fdb2csv1['D1RDB'] !== ''){
        $LIBSDB = $fdb2csv1['D1RDB'];
    }else{
        $LIBSDB = RDB;
    }
}
if($rtn === 0){
    if($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME){
        $db2RDBCon = $db2con;
    }else{
        $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1['D1RDB'];
        $db2RDBCon = cmDB2ConRDB();
    }
}
if($rtn === 0){
    if($LIBSNM !== '' ){
        if(($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME) === false){
            $resCreate = createQTEMPDB2LBLS($db2RDBCon,$LIBSNM);
            if($resCreate !== true){
                $rtn = 1;
                $msg = showMsg($resCreate);
            }
        }
    }
}

if($rtn === 0){
    if($rtn === 0){
        $rs = fnCheckTable($db2RDBCon,$LIB,$FIE,$LIBSNM);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ライブラリー、またはファイル'));
        }
    }
    if($rtn === 0){
        $rs = fnSelSYSCOLUMNS($db2RDBCon,$LIB,$FIE);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}

if($fdb2csv1['D1RDB'] === '' || $fdb2csv1['D1RDB'] === RDB || $fdb2csv1['D1RDB'] === RDBNAME){
    cmDb2Close($db2RDBCon);
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
	'RS' => $rs,
    'aaData' => umEx($data,$htmlFlg),
    'COLUMN_NAME' => $data['COLUMN_NAME'],
    'COLUMN_HEADING' => $data['COLUMN_HEADING'],
    'LIB' => $LIB,
    'FIE' => $FIE,
    'LIBSDB' => $LIBSDB,
);

echo(json_encode($rtn));
/*
*-------------------------------------------------------* 
* FDB2CSV3取得
*-------------------------------------------------------*
*/

function fnSelSYSCOLUMNS($db2con,$LIB,$FIE){

    $data = array();
    $strSQL  = ' SELECT SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
    if(SYSCOLCFLG==='1'){
        $strSQL .= '      , COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .= '      , COLUMN_HEADING ';
    }
    $strSQL .= ' FROM ';
    $strSQL .=  SYSCOLUMN2 ;
    //$strSQL .= ' FROM QSYS2/SYSCOLUMNS ';
    $strSQL .= ' WHERE table_schema = ? AND ';
    $strSQL .= ' table_name = ? ';

    $params = array($LIB,$FIE);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* SYSTABLEのチェック
*-------------------------------------------------------*
*/

function fnCheckTable($db2con,$LIB,$FIE,$LIBSNM = ''){

    $data = array();
    $params = array(
        $LIB,
        $FIE
    );
    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM QSYS2/SYSTABLES ';
    $strSQL .= ' WHERE SYSTEM_TABLE_SCHEMA = ? ';
    $strSQL .= ' AND SYSTEM_TABLE_NAME = ? ';
    /***ライブラリー除外**/
    //$strSQL .= ' AND SYSTEM_TABLE_SCHEMA NOT IN(SELECT LIBSID FROM QTEMP/DB2LBLS)';
    if($LIBSNM !== '' ){
      if(SYSLIBCHK === '1'){
        $strSQL .= ' AND SYSTEM_TABLE_SCHEMA IN (';
        $strSQL .= '    SELECT ';
        $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
        $strSQL .= '    FROM ';
        $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
        if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
            $strSQL .= MAINLIB.'/DB2LBLS ';
            $strSQL .= '        WHERE LIBSNM = ? ';
            $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
            $strSQL .= '        AND LIBSFG = \'1\' ' ;
            array_push($params,$LIBSNM);
        }else{
            $strSQL .= ' QTEMP/DB2LBLS ';
        }
        $strSQL .= ' )';
       }else{
            $strSQL .= ' AND SYSTEM_TABLE_SCHEMA NOT IN (';
            $strSQL .= '    SELECT ';
            $strSQL .= '         CAST( LIBSID  AS CHAR(10) CCSID 5026) AS LIBSID ';
            $strSQL .= '    FROM ';
            $_SESSION['PHPQUERY']['RDBNM'] = (isset($_SESSION['PHPQUERY']['RDBNM'])?$_SESSION['PHPQUERY']['RDBNM']:'');
            if($_SESSION['PHPQUERY']['RDBNM'] === '' || $_SESSION['PHPQUERY']['RDBNM'] === RDB || $_SESSION['PHPQUERY']['RDBNM'] === RDBNAME){
                $strSQL .= MAINLIB.'/DB2LBLS ';
                $strSQL .= '        WHERE LIBSNM = ? ';
                $strSQL .= '        AND LIBSDB = \''.RDB.'\' ';
                $strSQL .= '        AND LIBSFG = \'1\' ' ;
                array_push($params,$LIBSNM);
            }else{
                $strSQL .= ' QTEMP/DB2LBLS ';
            }
            $strSQL .= ' )';
           }
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        e_log(db2_stmt_errormsg());
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'FAIL_CMP'); 
            }
            else{
               $data = array('result' => true);
            }
        }
    }
    return $data;
}

/*
 * FDB2CSV1の対象定義データ取得
 */
function fnGetFDB2CSV1($db2con,$QRYNM){
    $data = array();
    $strSQL  = '    SELECT * ';
    $strSQL .= '    FROM FDB2CSV1 ';
    $strSQL .= '    WHERE D1NAME = ?  ';
    

    $params = array(
        $QRYNM
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET'); 
            }
            else{
               $data = array('result' => true,'data' => umEx($data) );
            }
        }
    }
    return $data;
}

