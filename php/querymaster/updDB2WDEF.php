<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$allcsv = $_POST['ALLCSV'];
$allexcel = $_POST['ALLEXCEL'];
$WDNAME = $_POST['WDNAME'];
$data = json_decode($_POST['DATA'],true);
$rtn = 0;
$msg = '';

/*
*-------------------------------------------------------* 
* 権限更新処理
*-------------------------------------------------------*
*/

//htmlspecialcharsをデコード
$WDNAME = cmHscDe($WDNAME);

$checkRs = false;
$defRs = false;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'6',$userData[0]['WUSAUT']);//'6' =>  ダウンロード権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ダウンロード権限の権限'));
            }
        }
    }
}
//クエリー存在チェック
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,'',$WDNAME,'');
    if($chkQry['result'] !== true){
        $rtn = 1;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$WDNAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 2;
            $msg = showMsg('ログインユーザーに指定したクエリーに対してのダウンロード権限の権限がありません。');
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
//CSV一括選択があった場合、最初に全てを更新
if($allcsv === "2"){
    if($rtn === 0){
        //FDB2CSV1にあってDB2WDEFに存在しない行をインサート
        $rs = fnInsertNoDB2WDEF($db2con,$WDNAME);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
    if($rtn === 0){
        //すべて１に更新
        $rs = fnUpdateAllCsv($db2con,$WDNAME,'1');
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}else if($allcsv === "3"){
    if($rtn === 0){
        //すべて''に更新
        $rs = fnUpdateAllCsv($db2con,$WDNAME,'');
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}

//Excel一括選択があった場合、最初に全てを更新
if($allexcel === "2"){
    if($rtn === 0){
        //FDB2CSV1にあってDB2WDEFに存在しない行をインサート
        $rs = fnInsertNoDB2WDEF($db2con,$WDNAME);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
    if($rtn === 0){
        //すべて１に更新
        $rs = fnUpdateAllExcel($db2con,$WDNAME,'1');
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }    
}else if($allexcel === "3"){
    if($rtn === 0){
        //すべて''に更新
        $rs = fnUpdateAllExcel($db2con,$WDNAME,'');
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}

foreach($data as $key => $value){
   // foreach($page as $key => $value){

        $WUUID = cmHscDe($value['WUUID']);
        $rs = fnCheckDB2WDEF($db2con,$WUUID,$WDNAME);
        if($rs === 'FAIL_SEL'){
            $rtn = 1;
            $msg = showMsg($rs);
            break;
        }else{
            $checkRs = $rs;
        }
        if($checkRs === true){
            $rs = fnUpdateDB2WDEF($db2con,$WUUID,$WDNAME,$value['WDDWNL'],$value['WDDWN2']);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
                break;
            }else{
                $defRs = $rs;
            }
        }else{
            $rs = fnInsertDB2WDEF($db2con,$WUUID,$WDNAME,$value['WDDWNL'],$value['WDDWN2']);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
                break;
            }else{
                $defRs = $rs;
            } 
        }
  //  }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'checkRs' => $checkRs,
    'defRs' => $defRs,
    'DATA' => $data
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ユーザー情報取得
*-------------------------------------------------------*
*/

function fnCheckDB2WDEF($db2con,$WDUID,$WDNAME){

    $data = true;
    $rs = array();

    $strSQL  = ' SELECT A.WDUID ';
    $strSQL .= ' FROM DB2WDEF AS A ' ;
    $strSQL .= ' WHERE A.WDUID = ? ' ;
    $strSQL .= ' AND A.WDNAME = ? ' ;

    $params = array(
        $WDUID,
        $WDNAME
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rs[] = $row;
            }
            if(count($rs) === 0){
                    $data = 'NOTEXIST_GET';
            }
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 定義追加
*-------------------------------------------------------*
*/

function fnInsertDB2WDEF($db2con,$WDUID,$WDNAME,$WDDWNL,$WDDWN2){


    $rs = true;

    //構文

    $strSQL  = ' INSERT INTO DB2WDEF ';
    $strSQL .= ' ( ';
    $strSQL .= ' WDUID, ';
    $strSQL .= ' WDNAME, ';
    $strSQL .= ' WDDWNL, ';
    $strSQL .= ' WDDWN2 ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ' ;
    $strSQL .= ' (?,?,?,?) ';

    $params = array(
        $WDUID,
        $WDNAME,
        $WDDWNL,
        $WDDWN2
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* 定義更新
*-------------------------------------------------------*
*/

function fnUpdateDB2WDEF($db2con,$WDUID,$WDNAME,$WDDWNL,$WDDWN2){


    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2WDEF ';
    $strSQL .= ' SET ';
    $strSQL .= ' WDDWNL = ?, ';
    $strSQL .= ' WDDWN2 = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WDUID = ? ';
    $strSQL .= ' AND WDNAME = ? ';

    $params = array(
        $WDDWNL,
        $WDDWN2,
        $WDUID,
        $WDNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* 定義削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WDEF($db2con,$WDUID,$WDNAME){


    $rs = true;

    //構文

    $strSQL  = ' DELETE FROM DB2WDEF ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WDUID = ? ';
    $strSQL .= ' AND WDNAME = ? ';

    $params = array(
        $WDUID,
        $WDNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* FDB2CSV1にあってDB2WDEFにない行を追加
*-------------------------------------------------------*
*/

function fnInsertNoDB2WDEF($db2con,$WDNAME){


    $rs = true;

    //構文
    $strSQL  = ' INSERT INTO DB2WDEF ';
    $strSQL .= ' ( ';
    $strSQL .= ' WDUID, ';
    $strSQL .= ' WDNAME ';
    $strSQL .= ' ) ';
    $strSQL .= ' SELECT WUUID,\''.$WDNAME.'\' ' ;
    $strSQL .= ' FROM DB2WUSR LEFT JOIN ';
    $strSQL .= ' ( ';
    $strSQL .= ' SELECT WDUID FROM DB2WDEF ';
    $strSQL .= ' WHERE WDNAME = ? ';
    $strSQL .= ' ) AS A ';
    $strSQL .= ' ON WUUID = WDUID ';
    $strSQL .= ' WHERE WDUID IS NULL ';

    $params = array(
        $WDNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* CSV権限全て更新
*-------------------------------------------------------*
*/

function fnUpdateAllCsv($db2con,$WDNAME,$WDDWNL){


    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2WDEF ';
    $strSQL .= ' SET ';
    $strSQL .= ' WDDWNL = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WDNAME = ? ';

    $params = array(
        $WDDWNL,
        $WDNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* Excel権限全て更新
*-------------------------------------------------------*
*/

function fnUpdateAllExcel($db2con,$WDNAME,$WDDWN2){


    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2WDEF ';
    $strSQL .= ' SET ';
    $strSQL .= ' WDDWN2 = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WDNAME = ? ';

    $params = array(
        $WDDWN2,
        $WDNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }

    return $rs;

}