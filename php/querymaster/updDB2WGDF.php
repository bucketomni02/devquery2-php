<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */

$D1NAME = cmHscDe($_POST['D1NAME']);
$data   = json_decode($_POST['DATA'], true);
$rtn    = 0;
$msg    = '';

/*
 *-------------------------------------------------------* 
 * 権限更新処理
 *-------------------------------------------------------*
 */

$checkRs = false;
$defRs   = false;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//オートコミットをOFFにする
if ($rtn === 0) {
    $res = db2_autocommit($db2con, DB2_AUTOCOMMIT_OFF);
    if ($res === false) {
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if ($rtn === 0) {
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'4',$userData[0]['WUSAUT']);//'4' =>  グループクエリー権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array(' グループクエリー権限の権限'));
            }
        }
    }
}
//クエリーが存在するかチェック
if ($rtn === 0) {
    $chkQry　 = array();
    $chkQry    = cmChkQuery($db2con, '', $D1NAME, '');
    if ($chkQry['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($chkQry['result'], array(
            'クエリー'
        ));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 2;
            $msg = showMsg('ログインユーザーに指定したクエリーに対してのグループクエリー権限の権限がありません。');
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if ($rtn === 0) {
    foreach ($data as $key => $value) {
        $GPNAME = cmHscDe($value['GPNAME']);
        /** クエリー存在チェック**/
        $rs     = fnCheckDB2WQGR($db2con, $GPNAME);
        if ($rs === 'NOTEXIST_GET') {
            $rtn = 3;
            $msg = showMsg($rs, array(
                'グループ'
            ));
        } else if ($rs !== true) {
            $rtn = 1;
            $msg = showMsg($rs, array(
                'グループ'
            ));
        }
        if ($rtn === 0) {
            //権限ありにした場合
            if ($value['GPID'] === "1") {
                
                //存在確認
                $checkRs = fnCheckDB2WGDF($db2con, $GPNAME, $D1NAME);
                
                //なかったらインサート
                if ($checkRs['result'] === true) {
                    $checkRs = $checkRs['data'];
                } else {
                    $rtn = 1;
                    $msg = showMsg($checkRs['result']);
                    break;
                }
                if ($rtn === 0 && ($checkRs === 0)) {
                    $defRs = fnInsertDB2WGDF($db2con, $GPNAME, $D1NAME);
                    if ($defRs !== true) {
                        $rtn = 1;
                        $msg = showMsg($defRs);
                        break;
                    }
                }
                //権限なしにした場合
            } else if ($value['GPID'] === "0") {
                
                //存在確認
                $checkRs = fnCheckDB2WGDF($db2con, $GPNAME, $D1NAME);
                
                //あったら削除
                if ($checkRs['result'] === true) {
                    $checkRs = $checkRs['data'];
                } else {
                    $rtn = 1;
                    $msg = showMsg($checkRs['result']);
                    break;
                }
                if ($rtn === 0 && ($checkRs > 0)) {
                    $defRs = fnDeleteDB2WGDF($db2con, $GPNAME, $D1NAME);
                    if ($defRs !== true) {
                        $rtn = 1;
                        $msg = showMsg($defRs);
                        break;
                    }
                }
                 //グループ権限付与時、ダウンロード権限自動付与
                if (CND_QRYGD === "1" ) {
                    $rsUsr = cmGetUsrName($db2con,$GPNAME);
                    if ($rsUsr['result'] !== 'NOTEXIST_GET') {
                        foreach($rsUsr['data'] as $usrNm){
                            $chk = cmChkQryKenGen($db2con,$usrNm['WUUID'],$D1NAME);
                            if($chk['result'] === 'NOTEXIST_GET'){
                                $rss = cmUpdQryKengen($db2con,$usrNm['WUUID'],$D1NAME,'','');
                            }
                        }
                    }
                }
            }
        }
    }
}

//グループ権限付与時、ダウンロード権限自動付与(GI)
if ($rtn === 0) {
    if (CND_QRYGD === "1" ) {
        $rsQryGrp = fnGetGrpName($db2con,$D1NAME);
        foreach ($rsQryGrp['data'] as $grpNm) {
            $rsUsr = cmGetUsrName($db2con,$grpNm['WGGID']);
            if ($rsUsr['result'] !== 'NOTEXIST_GET') {
                foreach($rsUsr['data'] as $usrNm){
                    $name = cmGetQryNameFromDB2WDEF($db2con,$usrNm['WUUID'],$D1NAME);
                    if ($name['result'] === 'NOTEXIST_GET') {
                        $rss = cmInsQryKengen($db2con,$usrNm['WUUID'],$D1NAME,'1','1');
                        if($rss['result'] !== true){
                            $rtn = 1;
                            $msg = $rss['result'];
                            break;
                        }
                    }else{
                        $rss = cmUpdQryKengen($db2con,$usrNm['WUUID'],$D1NAME,'1','1');
                        if($rss['result'] !== true){
                            $rtn = 1;
                            $msg = $rss['result'];
                            break;
                        }
                    }
                }
            }
        }
    }
}


//ブックマークの削除
if($rtn ===0){
    $defRs=cmCountDB2WGDF_DB2WUGR($db2con,$D1NAME);
    if($defRs['result']!==true){
        $rtn = 1;
        $msg = showMsg($defRs['result']);
    }else{
        if($defRs['TOTALCOUNT']<=0){
            $defRs=cmDelDB2BMK($db2con,'',$D1NAME,2);
            if($defRs['result']!==true){
                $rtn = 1;
                $msg = showMsg($defRs['result']);
            }
        }
    }
}
if ($rtn !== 0) {
    db2_rollback($db2con);
} else {
    db2_commit($db2con);
}
cmDb2Close($db2con);


/**return**/
$rtnAry = array(
    'checkRs' => $checkRs,
    'defRs' => $defRs,
    'DATA' => $data,
    'MSG' => $msg,
    'RTN' => $rtn
);

echo (json_encode($rtnAry));

/*
 *-------------------------------------------------------* 
 * ユーザー情報取得
 *-------------------------------------------------------*
 */

function fnCheckDB2WGDF($db2con, $WGGID, $WGNAME)
{
    
    $data = array();
    
    $strSQL = ' SELECT B.WGGID ';
    $strSQL .= ' FROM DB2WGDF AS B ';
    $strSQL .= ' WHERE B.WGGID = ? ';
    $strSQL .= ' AND B.WGNAME = ? ';
    
    $params = array(
        $WGGID,
        $WGNAME
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => count($data)
            );
        }
    }
    return $data;
}

/*
 *-------------------------------------------------------* 
 * 定義追加
 *-------------------------------------------------------*
 */

function fnInsertDB2WGDF($db2con, $WGGID, $WGNAME)
{
    $rs = true;
    
    //構文
    $strSQL = ' INSERT INTO DB2WGDF ';
    $strSQL .= ' ( ';
    $strSQL .= ' WGGID, ';
    $strSQL .= ' WGNAME ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?) ';
    
    $params = array(
        $WGGID,
        $WGNAME
    );
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_INS';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
    
}

/*
 *-------------------------------------------------------* 
 * 定義削除
 *-------------------------------------------------------*
 */

function fnDeleteDB2WGDF($db2con, $WGGID, $WGNAME)
{
    $rs = true;
    
    //構文
    $strSQL = ' DELETE FROM DB2WGDF ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WGGID = ? ';
    $strSQL .= ' AND WGNAME = ? ';
    
    $params = array(
        $WGGID,
        $WGNAME
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_DEL';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === fasle) {
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
    
}
/*
 *-------------------------------------------------------* 
 * グループ存在チェック いたらtrue
 *-------------------------------------------------------*
 */

function fnCheckDB2WQGR($db2con, $WQGID)
{
    
    $data = array();
    $rs   = true;
    
    $strSQL = ' SELECT B.WQGID ';
    $strSQL .= ' FROM DB2WQGR  AS B ';
    $strSQL .= ' WHERE B.WQGID = ? ';
    
    $params = array(
        $WQGID
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_SEL';
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $rs = 'FAIL_SEL';
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) === 0) {
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}
function fnGetGrpName($db2con,$qryId){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT WGGID ';
    $strSQL .= ' FROM DB2WGDF  ' ;
    $strSQL .= ' WHERE WGNAME = ? ' ;

     $params = array(
        $qryId
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true,'data'=> umEx($data));
            }
        }
    }
    return $data;
}