<?php

include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");

$userId  = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
$d1name  = $_POST['tableName'];
$columns = json_decode($_POST['columns'],true);
$isQGflg = (isset($_POST['ISQGFLG']) ? $_POST['ISQGFLG'] : false);//クエリーグループ
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
//クエリーグループ
$D1NAMELIST=array();//クエリーグループの全部データ
$LASTD1NAME='';//クエリーグループの一番最後のクエリーＩＤ
$isQGflg=($isQGflg==='true')?true:false;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmCheckUser($db2con,$userId);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
if($rtn===0){
    if($isQGflg){
        $rs=cmGetQueryGroup($db2con,$d1name,'','');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリー'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
        }
    }
}

if($rtn === 0){
    if($isQGflg){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,$userId,$d1name,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }else{
                //一番最後の
                if($res['LASTFLG']==='1'){
                    $fdb2csv1Data = $chkQry['FDB2CSV1'];
                    $d1name=$res['QRYGSQRY'];
                }
            }
        }//end of for loop
    }else{
        $chkQry = array();
        $chkQry = cmChkQuery($db2con,$userId,$d1name,'');
        if($chkQry['result'] !== true){
            $rtn = 2;
            $msg = showMsg($chkQry['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    //全削除してからインサート
    cmDeleteDB2WCOL($db2con,$userId,$d1name);

    foreach($columns as $key => $value){
        $i_WCUID   = $userId;
        $i_WCNAME  = $d1name;
        $i_WCFILID = $value['filid'];
        $i_WCFLD   = $value['fld'];
        $i_WCINDX  = $value['index'];
        $i_WCTEXT  = $value['text'];
        $i_WCTYPE  = $value['type'];
        $i_WCLOCK  = $value['locked'];
        $i_WCHIDE  = $value['hidden'];

        if($i_WCLOCK === true){
            $i_WCLOCK = '1';
        }else{
            $i_WCLOCK = '0';
        }

        if($i_WCHIDE === true){
            $i_WCHIDE = '1';
        }else{
            $i_WCHIDE = '0';
        }

    	cmAddDB2WCOL($db2con,$i_WCUID,$i_WCNAME,($key+1),$i_WCFILID,$i_WCFLD,$i_WCINDX,$i_WCTEXT,$i_WCTYPE,$i_WCLOCK,$i_WCHIDE);
    }
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtn));
