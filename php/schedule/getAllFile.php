<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$id = $_POST['id'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

$data = GetAllFile($db2con,$id);

cmDb2Close($db2con);

echo(json_encode(umEx($data)));

/*
*-------------------------------------------------------* 
* ファイル名を取得
*-------------------------------------------------------*
*/

function GetAllFile($db2con,$d1name){

    $rtn = 0;
    $fdb2csv1Rdb = array();
    $strSQL  = ' SELECT ';
    $strSQL .= '     B.D1RDB ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WHIS AS A ';
    $strSQL .= ' LEFT JOIN  FDB2CSV1 B ';
    $strSQL .= ' ON A.WHNAME = B.D1NAME ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     B.D1RDB <> \'\' ';
    $strSQL .= ' AND B.D1RDB <> \''.RDB.'\' ';
    $strSQL .= ' AND B.D1RDB <> \''.RDBNAME.'\' ';
    $strSQL .= ' AND A.WHQGFLG <> \'1\' ';
    $strSQL .= ' GROUP BY ';
    $strSQL .= '     B.D1RDB ';
    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $fdb2csv1Rdb = false;
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $fdb2csv1Rdb = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $fdb2csv1Rdb[] = $row;
            }
        }
        //e_log('RDBARRSQL:'.$strSQL);
        //e_log('RDBARR:'.print_r($fdb2csv1Rdb,true));
    }

    if($fdb2csv1Rdb !== false){
        if(count($fdb2csv1Rdb) >0){
            $fdb2csv1Rdb = umEx($fdb2csv1Rdb);
            e_log('一時テーブル作成開始');

            // 一時テーブル作成
            $createSQL  = 'DECLARE GLOBAL TEMPORARY TABLE TABLES ';
            $createSQL .= ' ( ';
            $createSQL .= ' TABLE_NAME VARCHAR(128)'; 
            $createSQL .= ' ,DBNAME VARCHAR(128)'; 
            $createSQL .= ' )  WITH REPLACE ';
            //e_log($createSQL);
            $result = db2_exec($db2con, $createSQL);
            if ($result) {
                foreach($fdb2csv1Rdb as $rdbnm){
                    $_SESSION['PHPQUERY']['RDBNM'] = $rdbnm['D1RDB'];
                    $rdbCon = cmDB2ConRDB(SAVE_DB);
                    $res = createTmpSysTBL($rdbCon,$db2con);
                    cmDb2Close($rdbCon);
                }
            }else{
                e_log('テーブル作成失敗');
                e_log('テーブルerror'.db2_stmt_errormsg());
                $rtn = 1;
                $data = array(
                    'result' => 'FAIL_SEL',
                    'errcd'  => 'テーブル作成失敗'
                    );
            }
        }
    }else{
        $rtn = 1;
    }

    /*if($rtn === 0){
        if(count($fdb2csv1Rdb) > 0){
            $tdata = array();
            $strSQL  =' SELECT * ';
            $strSQL .=' FROM QTEMP/TABLES A ';
            
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $tdata = array('result' => 'FAIL_SEL');
                //e_log('error'.db2_stmt_errormsg());
                $rtn = 1;
            }else{
                $params = array();
                $r = db2_execute($stmt,$params);
                
                if($r === false){
                    $tdata = array('result' => 'FAIL_SEL');
                    $rtn = 1;
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $tdata[] = $row;
                    }
                    $tdata = umEx($tdata,false);
                    $tdata = array('result' => true,'data' => $tdata);
                }
                e_log('temptable'.print_r($tdata,true));
            }
        }
    }*/
    $data = array();
    if($rtn === 0){
        $strSQL  = ' SELECT A.WHNAME,A.WHBDAY,A.WHBTIM, ';
        $strSQL .= ' A.WHOUTF ';
        $strSQL .= ' FROM DB2WHIS as A ' ;
        $strSQL .= ' LEFT JOIN (SELECT TABLE_NAME,DBNAME FROM SYSIBM/TABLES WHERE DBNAME = ? ) AS B ';
        $strSQL .= ' ON A.WHOUTF = B.TABLE_NAME ';
        if(count($fdb2csv1Rdb) > 0){
            $strSQL .= ' LEFT JOIN QTEMP/TABLES AS C ';
            $strSQL .= ' ON A.WHOUTF = C.TABLE_NAME ';
        }
        $strSQL .= ' WHERE WHNAME = ? ';
        $strSQL .= ' AND WHPKEY = ? ';
        $strSQL .= ' AND A.WHOUTF <> \'\' ';
        $strSQL .= ' AND A.WHQGFLG <> \'1\' ';
        if(count($fdb2csv1Rdb) > 0){
            $strSQL .= ' AND (B.DBNAME <> \'\' ';
            $strSQL .= '      OR C.DBNAME <> \'\') ';
        }else{
            $strSQL .= ' AND (B.DBNAME <> \'\') ';
        }
        $strSQL .= ' GROUP BY WHNAME,WHBDAY,WHBTIM,WHOUTF ';
        $strSQL .= ' ORDER BY WHBDAY DESC,WHBTIM DESC ';

        $params = array(SAVE_DB,$d1name,'');
        e_log('aaa'.$strSQL);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = false;
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = false;
            }else{
                while($row = db2_fetch_assoc($stmt)){                
                    $data[] = array(
                        'abbr' => $row['WHOUTF'],
                        'name' => cmYyyyMmDd($row['WHBDAY']).' '.cmHhIi($row['WHBTIM'])
                    );
                    //$data[$row['WHOUTF']] = cmYyyyMmDd($row['WHBDAY']).'&nbsp'.cmHhIi($row['WHBTIM']);
                }
            }
        }
    }
    return $data;
}
// create QTEMP.TABLES
function createTmpSysTBL($rdbCon,$db2con){
    $rtn = 0;
    $rstbldata = array();
    $strSQL  =  ' SELECT  ';
    $strSQL .=  '    A.TABLE_NAME '; 
    $strSQL .=  ' FROM SYSIBM/TABLES AS A'; 
    $strSQL .=  ' WHERE A.DBNAME = ? '; 

    $stmt = db2_prepare($rdbCon,$strSQL);
    if($stmt === false){
        $rstbldata = array('result' => 'FAIL_SEL');
        $rtn = 1;
    }else{
        $params = array(SAVE_DB);
        $r = db2_execute($stmt,$params);        
        if($r === false){
            $rstbldata = array('result' => 'FAIL_SEL');
            $rtn = 1;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rstbldata[] = $row;
            }
            $rstbldata = array('result' => true,'data' => $rstbldata);
            e_log('取得したデータ：'.print_r($rstbldata,true));
        }
    }

    if($rtn === 0){
        if(count($rstbldata['data'])>0){
            $rstbldata = umEx($rstbldata['data']);
            
            $strInsSQL  = ' INSERT INTO QTEMP/TABLES ';
            $strInsSQL .= ' ( ';;
            $strInsSQL .= '    TABLE_NAME ';
            $strInsSQL .= '    ,DBNAME ';
            $strInsSQL .= ' ) ';
            $strInsSQL .= ' VALUES ( ?,?)';

            $stmt = db2_prepare($db2con,$strInsSQL);
            if($stmt === false ){
                $data = array(
                            'result' => 'FAIL_INS',
                            'errcd'  => 'TABLES:'.db2_stmt_errormsg()
                        );
                $result = 1;
            }else{
                foreach($rstbldata as $tabledata){
                    $params =array(
                                $tabledata['TABLE_NAME'],
                                SAVE_DB
                            );
                    e_log($strInsSQL.print_r($params,true));
                    $res = db2_execute($stmt,$params);
                    if($res === false){
                        $data = array(
                                'result' => 'FAIL_INS',
                                'errcd'  => 'TABLES:'.db2_stmt_errormsg()
                            );
                        $result = 1;
                        break;
                    }else{
                        $result = 0;
                        $data = array('result' => true);
                    }
                }
            }
        }
        if($result !== 0){
            $rtn = 1;
        }
    }
    return $rtn;
}