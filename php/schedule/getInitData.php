<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$id = $_POST['id'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;

/*
*-------------------------------------------------------* 
* データ取得処理
*-------------------------------------------------------*
*/

$usrinfo = $_SESSION['PHPQUERY']['user'][0];

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

$nameText = fnGetNameText($db2con,$id);
$nameText = umEx($nameText);

$db2wdef = fnGetDB2WDEF($db2con,$id,$usrinfo['WUUID']);
$db2wdef = umEx($db2wdef);

cmDb2Close($db2con);

$rtn = array(
    'wdef' => $db2wdef,
    'name' => $nameText[0]['D1NAME'],
    'text' => $nameText[0]['D1TEXT'],
    'DSPROW1'      => DSPROW1,
    'DSPROW2'      => DSPROW2,
    'DSPROW3'      => DSPROW3,
    'DSPROW4'      => DSPROW4,
    'INTDSPROW' =>INTDSPROW
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* クエリー名を取得
*-------------------------------------------------------*
*/

function fnGetNameText($db2con,$tblName){

    $data = array();

    $strSQL  = ' SELECT A.D1NAME,A.D1TEXT ';
    $strSQL .= ' FROM FDB2CSV1 as A ' ;
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array($tblName);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 権限を取得
*-------------------------------------------------------*
*/

function fnGetDB2WDEF($db2con,$wdname,$wduid){

    $data = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WDEF as A ' ;
    $strSQL .= ' WHERE WDUID = ? ';
    $strSQL .= ' AND WDNAME = ? ';

    $params = array($wduid,$wdname);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}