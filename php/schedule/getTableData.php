<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../base/comGetFDB2CSV2_CCSID.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/
$d1name = $_POST['D1NAME'];
$tblName = $_POST['tblName'];
$iDisplayStart = $_POST['iDisplayStart'];
$iDisplayLength = $_POST['iDisplayLength'];
$iSortCol_0 = $_POST['iSortCol_0'];
$sSortDir_0 = $_POST['sSortDir_0'];
$sSearch = $_POST['sSearch'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$csv_d = array();
$msg = '';
$valueArr = array();
$tmpFDB2CSV2 = array();
/*
*-------------------------------------------------------* 
* データ取得処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmCheckUser($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
if($rtn === 0){
    $chkQry = array();
    $chkQry = cmChkQuery($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID'],$d1name,'');
    if($chkQry['result'] !== true){
        $rtn = 2;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($rtn === 0){
    $fdb2csv1Data = fnGetFDB2CSV1($db2con,$d1name);
    $fdb2csv1Data = umEx($fdb2csv1Data);
     //e_log('lbl:'.print_r($fdb2csv1Data,true).$d1name);
    if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME) === false){
        $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1Data[0]['D1RDB'];
        e_log('lbl:'.print_r($fdb2csv1Data[0],true));
        $db2conRdb = cmDB2ConRDB($fdb2csv1Data[0]['D1LIBL']);
    }
    if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME)){
        $systables = cmGetSystables($db2con,SAVE_DB,$tblName);
    }else{
        $systables = cmGetSystables($db2conRdb,SAVE_DB,$tblName);
    }

    if(count($systables) > 0){

        $name = fnGetName($db2con,$tblName);
        $fdb2csv2 = cmGetFDB2CSV2($db2con,$name,false,false,false);
        $fdb2csv2 = $fdb2csv2['data'];
        
        if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME)){
            //FDB2CSV2のカラムが'DEVQUERYH'のテーブルにない場合消す
            $tmpFDB2CSV2 = cmColNameDiff($db2con,$fdb2csv2,$tblName);
            $column_name = cmGetColumnName($db2con, $tblName);
            if ($column_name['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result'], array(
                    'クエリー'
                ));
            } else {
                //        $column_name = umEx($column_name['data']);
                $column_name = $column_name['data'];
            }
            $colnew_arr = array();
            for ($i = 0; $i < count($column_name); $i++) {
                $colnew_arr[] = $column_name[$i]['COLUMN_NAME'];
            }
            // id つけているかどうかチェック
            //e_log('column name :'.$colnew_arr[0]);
            $colnmArr = explode( '_', $colnew_arr[0] );
            if(count($colnmArr) > 1){
                if(end($colnmArr) === $fdb2csv2[0]['D2FILID']){
                    $idFlg = '';
                }else{
                    $idFlg = '1';
                }
            }else{
                $idFlg = '1';
            }
            $allcount = cmGetAllCount($db2con,$tmpFDB2CSV2,$tblName,$sSearch,$idFlg,array());
            $csv_d = cmGetDbFile($db2con,$tmpFDB2CSV2,$tblName,$iDisplayStart,$iDisplayLength,$iSortCol_0,$sSortDir_0,'',$sSearch,array(),false,$idFlg,'',array(),array());
        }else{
            //FDB2CSV2のカラムが'DEVQUERYH'のテーブルにない場合消す
            $tmpFDB2CSV2 = cmColNameDiff($db2conRdb,$fdb2csv2,$tblName);
            $column_name = cmGetColumnName($db2conRdb, $tblName);
            if ($column_name['result'] !== true) {
                $rtn = 1;
                $msg = showMsg($rs['result'], array(
                    'クエリー'
                ));
            } else {
                //        $column_name = umEx($column_name['data']);
                $column_name = $column_name['data'];
            }
            $colnew_arr = array();
            for ($i = 0; $i < count($column_name); $i++) {
                $colnew_arr[] = $column_name[$i]['COLUMN_NAME'];
            }
            // id つけているかどうかチェック
            //e_log('column name :'.$colnew_arr[0]);
            $colnmArr = explode( '_', $colnew_arr[0] );
            if(count($colnmArr) > 1){
                if(end($colnmArr) === $fdb2csv2[0]['D2FILID']){
                    $idFlg = '';
                }else{
                    $idFlg = '1';
                }
            }else{
                $idFlg = '1';
            }
            $allcount = cmGetAllCount($db2conRdb,$tmpFDB2CSV2,$tblName,$sSearch,$idFlg,array());
            $csv_d = cmGetDbFile($db2conRdb,$tmpFDB2CSV2,$tblName,$iDisplayStart,$iDisplayLength,$iSortCol_0,$sSortDir_0,'',$sSearch,array(),false,$idFlg,'',array(),array());
        }
        
        if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME) === false){
            cmDb2Close($db2conRdb);
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'res_iSortCol_0' => $iSortCol_0,
    'res_sSortDir_0' => $sSortDir_0,
    'res_sSearch' => $sSearch,
    'iTotalRecords' => $allcount['data'],
    'iTotalDisplayRecords' => $allcount['data'],
    'aaData' => umEx($csv_d['data'],true),
    'BACKZERO_FLG' => BACKZERO_FLG,
    'ZERO_FLG' => ZERO_FLG,
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* クエリー名を取得
*-------------------------------------------------------*
*/

function fnGetName($db2con,$tblName){

    $data = array();

    $strSQL  = ' SELECT A.WHNAME ';
    $strSQL .= ' FROM DB2WHIS as A ' ;
    $strSQL .= ' WHERE WHOUTF = ? ';

    $params = array($tblName);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = $data[0]['WHNAME'];
        }
    } 
    return $data;

}
function fnGetFDB2CSV1($db2con,$d1name){

    $data = array();

    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM FDB2CSV1 as A ' ;
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array($d1name);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    } 
    return $data;
}

