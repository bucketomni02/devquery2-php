<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$tblName = $_POST['tblName'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$tmpFDB2CSV2 = array();

/*
*-------------------------------------------------------* 
* ヘッダー取得処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

$name = fnGetName($db2con,$tblName);

$fdb2csv2 = cmGetFDB2CSV2($db2con,$name,false,false,false);
//e_log('fdb2csv2data:'.print_r($fdb2csv2,true).$name);
$fdb2csv1Data = fnGetFDB2CSV1($db2con,$name);
$fdb2csv1Data = umEx($fdb2csv1Data);
//e_log('lbl:'.print_r($fdb2csv1Data,true).$name);
if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME) === false){
    $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1Data[0]['D1RDB'];
    //e_log('lbl:'.print_r($fdb2csv1Data[0],true));
    $db2conRdb = cmDB2ConRDB($fdb2csv1Data[0]['D1LIBL']);
}
if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME)){
    //FDB2CSV2のカラムが'DEVQUERYH'のテーブルにない場合消す
    $tmpFDB2CSV2 = cmColNameDiff($db2con,$fdb2csv2['data'],$tblName,$fdb2csv1Data[0]['D1CFLG']);
}else{
    //FDB2CSV2のカラムが'DEVQUERYH'のテーブルにない場合消す
    $tmpFDB2CSV2 = cmColNameDiff($db2conRdb,$fdb2csv2['data'],$tblName,$fdb2csv1Data[0]['D1CFLG']);
    e_log($tmpFDB2CSV2,true);
}
if(($fdb2csv1Data[0]['D1RDB'] === '' || $fdb2csv1Data[0]['D1RDB'] === RDB || $fdb2csv1Data[0]['D1RDB'] === RDBNAME) === false){
    cmDb2Close($db2conRdb);
}
cmDb2Close($db2con);

echo(json_encode(umEx($tmpFDB2CSV2,true)));

/*
*-------------------------------------------------------* 
* クエリー名を取得
*-------------------------------------------------------*
*/

function fnGetName($db2con,$tblName){

    $data = array();

    $strSQL  = ' SELECT A.WHNAME ';
    $strSQL .= ' FROM DB2WHIS as A ' ;
    $strSQL .= ' WHERE WHOUTF = ? ';

    $params = array($tblName);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false ){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = $data[0]['WHNAME'];
        }
    }
    return $data;

}
function fnGetFDB2CSV1($db2con,$d1name){

    $data = array();

    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM FDB2CSV1 as A ' ;
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array($d1name);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    } 
    return $data;
}