<?php
/*
*-------------------------------------------------------* 
* 1時間ごとに呼ばれるジョブ
* ファイルが別で作成されていたものを一つに纏め、Classで管理
*-------------------------------------------------------*
*/

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
ini_set('error_log', SAVE_DIR . SISTEM . '/logdelQry.log');

//各処理の実行
$cls1 = new delFileMailTempFile();
$cls1->exe();

$cls2 = new delQryWorkTbl();
$cls2->exe();

$cls3 = new dropLogFile();
$cls3->exe();

$cls4 = new delQueryTempTbl();
$cls4->exe();

/*
*-------------------------------------------------------* 
* IFSのtmpファイル削除処理（旧delFileMailTempFile.php)
*-------------------------------------------------------*
*/

class delFileMailTempFile{

    public function exe(){

        /**PHPEXCELが異常終了した時にできるtmpファイルを削除する**/
        foreach (glob(PHP_DIR.'csv/*') as $val) {
            $this->checkFile($val);
        }
        foreach (glob(PHP_DIR.'xls/*') as $val) {
            $this->checkFile($val);
        }
        foreach (glob(PHP_DIR.'html/*') as $val) {
            $this->checkFile($val);
        }
        foreach (glob(PHP_DIR.'tmp_passzip/*') as $val) {
            $this->checkFile($val);
        }
        foreach (glob(PHP_DIR.'tmp_zip/*') as $val) {
            $this->checkFile($val);
        }
        foreach (glob(PHP_DIR.'downloadtmp/*') as $val) {
            $this->checkFile($val);
        }

    }

    private function checkFile($filename){

        $datetime1 = date("Y-m-d H:i:s", filectime($filename));
        $datetime2 = date("Y-m-d H:i:s");
        $interval = date_diff(new DateTime($datetime1), new DateTime($datetime2));
        $year=$interval->format('%R%y');
        $month=$interval->format('%R%m');
        $day=$interval->format('%R%a');
        $hour=$interval->format('%R%h');
        if($day >= 1){
            @unlink($filename);
        }

    }

}

/*
*-------------------------------------------------------* 
* ワークファイル及びロックファイルの削除処理（旧delQryWorkTbl.php)
*-------------------------------------------------------*
*/

class delQryWorkTbl{

    public function exe(){

        $rtn = 0;
        $EXETIME = date("Y-m-d H:i:s");
        e_log('ワークファイル削除呼び出し：'.$EXETIME);
        $db2con = cmDb2Con();
        cmSetPHPQUERY($db2con);
        // DB2WTBLにあるデータの中で一日前作成したテーブル情報が有ったら削除
        if($rtn === 0){
            $rsDelWtbl = $this->fnDelDB2WTBL($db2con,$EXETIME);
            if($rsDelWtbl !== true){
                $rtn = 1;
                e_log('DB2WTBLのデータ削除失敗。');
            }
        }
        if($rtn === 0){
            $rsDel = $this->fnDelTmpTbl($db2con,$EXETIME);
            if($rsDel === 1){
                $rtn = 1;
                e_log('削除失敗'.$scheduleTbl['WHOUTF']);
            }
            $rsScheduleTbl = $this->fnGetScheduleTblList($db2con);
            if($rsScheduleTbl['result'] !== true){
                $rtn = 1;
                e_log(showMsg($rsScheduleTbl['result']));
                //break;
            }else{
                $rsScheduleTbl = $rsScheduleTbl['data'];
                if(count($rsScheduleTbl) > 0){
                    foreach($rsScheduleTbl as $scheduleTbl){
                        $rsDel = $this->fnDelTmpTbl($db2con,$EXETIME,$scheduleTbl['WHOUTF'],$scheduleTbl['D1HOKN']);
                        if($rsDel === 1){
                            $rtn = 1;
                            e_log('削除失敗'.$scheduleTbl['WHOUTF']);
                            break;
                        }
                    }
                }
            }
            
            cmDb2Close($db2con);
        }

    }

    private function fnGetScheduleTblList($db2con){
        $rtn = 0;
        $scheduleTbl = array();
        $strSQL  =  ' SELECT     A.WHNAME, ';
        $strSQL .=  '             A.WHOUTF ';
        $strSQL .=  '             ,B.D1HOKN ';
        $strSQL .=  ' FROM  ';
        $strSQL .=  '     DB2WHIS AS A  ';
        $strSQL .=  ' LEFT JOIN  FDB2CSV1 AS B  ';
        $strSQL .=  ' ON A.WHNAME = B.D1NAME  ';
        $strSQL .=  ' RIGHT JOIN QSYS2/SYSTABLES C ';
        $strSQL .=  ' ON A.WHOUTF = C.TABLE_NAME '; 
        $strSQL .=  ' WHERE ';
        $strSQL .=  '    A.WHOUTF <> \'\' AND ';
        $strSQL .=  '    B.D1RDB IN (\'\',\''.RDBNM.'\',\''.RDBNAME.'\') AND ';
        $strSQL .=  '    B.D1HOKN > 0 ';
        $params = array($RDBNM);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $scheduleTbl = array('result' => 'FAIL_SEL');
            e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $scheduleTbl = false;
                e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row === false){
                        e_log('fetcherr:'.db2_stmt_errormsg());
                    }else{
                        $scheduleTbl[] = $row;
                    }
                }
                $scheduleTbl = array('result' => true,'data' => umEx($scheduleTbl));
            }
        }
        e_log('scheduleRDBLIST:'.print_r($scheduleTbl,true));
        return $scheduleTbl;
    }

    private function fnGetFDB2CSV1RDBList($db2con){
        $rtn = 0;
        $fdb2csv1Rdb = array();
        $strSQL  = ' SELECT ';
        $strSQL .= '     A.D1RDB ';
        $strSQL .= ' FROM ';
        $strSQL .= '   FDB2CSV1 A ';
        $strSQL .= ' WHERE ';
        $strSQL .= '     A.D1RDB <> \'\' ';
        $strSQL .= ' AND A.D1RDB <> \''.RDB.'\' ';
        $strSQL .= ' AND A.D1RDB <> \''.RDBNAME.'\' ';
        $strSQL .= ' GROUP BY ';
        $strSQL .= '     A.D1RDB ';
        $params = array();
        //e_log($strSQL);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $fdb2csv1Rdb = array('result' => 'FAIL_SEL');
            e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $fdb2csv1Rdb = false;
                e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row === false){
                        e_log('fetcherr:'.db2_stmt_errormsg());
                    }else{
                        $fdb2csv1Rdb[] = $row;
                    }
                }
                $fdb2csv1Rdb = array('result' => true,'data' => umEx($fdb2csv1Rdb));
            }
        }
        //e_log('RDBARR:'.print_r($fdb2csv1Rdb,true));
        return $fdb2csv1Rdb;
    }

    private function fnDelTmpTbl($db2con,$exeTime,$tblname = '',$cntDay = 0){
        $rtn = 0;
        if($rtn === 0){
            $rs = $this->fnGetTmpTblLst($db2con,$tblname);
            if($rs['result'] !== true){
                $rtn = 1;
                e_log('実行結果テーブル取得失敗。エラー内容：'.$rs['result']);
            }else{
                //e_log($tblname.print_r($rs['data'],true));
                $tmpTblList = $rs['data'];
            }
        }
        if($rtn === 0){
            if(count($tmpTblList) >0){
                foreach($tmpTblList as $tmpTbl){
                    $chkRs = $this->checkTime($tmpTbl['CREATE_TIME'],$exeTime,$cntDay);
                    if($chkRs){
                        e_log('削除対象：'.$tmpTbl['TABLE_NAME'].' date: '.$cntDay);
                        //var_dump(print_r($tmpTbl,true));
                    }
                    if($chkRs){
                        $strDelSQL  = ' DROP TABLE '.SAVE_DB.'/'.$tmpTbl['TABLE_NAME'];
                        $result = db2_exec($db2con, $strDelSQL);
                        if ($result) {
                            e_log($tmpTbl['TABLE_NAME'].'削除成功。');
                        }else{
                            e_log($tmpTbl['TABLE_NAME'].'削除失敗。');
                            e_log('エラー内容'.db2_stmt_errormsg());
                            //$rtn = 1;
                            //break;  
                        }
                    }
                }
            }
        }
        return $rtn;
    }

    private function fnGetTmpTblLst($db2con,$tblname){
        $tmpTbl = array();
        
        $strSQL  =  ' SELECT ';
        $strSQL .=  '     TABLE_NAME,VARCHAR_FORMAT(LAST_ALTERED_TIMESTAMP, \'YYYY-MM-DD HH24:MI:SS\') AS CREATE_TIME ';
        $strSQL .=  ' FROM ';
        $strSQL .=  '     QSYS2/SYSTABLES ';
        $strSQL .=  ' WHERE ';
        $strSQL .=  '     TABLE_SCHEMA = \''.SAVE_DB.'\' AND ';
        $strSQL .=  '     TABLE_OWNER <> \'QSECOFR\' AND ';
        $strSQL .=  '     TABLE_NAME NOT IN (SELECT WTBLNM FROM DB2WTBL) AND ';
        if($tblname === ''){
            $strSQL .=  '     TABLE_NAME LIKE \'@%\' ';
            $params = array();
        }else{
            $strSQL .=  '     TABLE_NAME = ? ';
            $params = array($tblname);
        }
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $tmpTbl = array('result' => 'FAIL_SEL');
            e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
             $r = db2_execute($stmt,$params);
            if($r === false){
                $tmpTbl = false;
                e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $tmpTbl[] = $row;
                }
                $tmpTbl = array('result' => true,'data' => umEx($tmpTbl));
            }
        }
        return $tmpTbl;
    }

    private function checkTime($createtime,$exeTime,$cntDay = 0){
        $delFlg = false;
        $datetime1 = date("Y-m-d H:i:s", strtotime($createtime));
        $datetime2 = $exeTime;
        if($cntDay > 0){
            $cntdayDate = date_add(new DateTime($datetime1),date_interval_create_from_date_string('\"'.$cntDay. 'days\"'));
            $cntdayDate = date_format($cntdayDate,"Y-m-d H:i:s");
            if(new DateTime($cntdayDate) < new DateTime($datetime2)){
                $delFlg = true;
            }
        }else{
            $interval = date_diff(new DateTime($datetime1), new DateTime($datetime2));
            $year=(int)$interval->format('%R%y');
            $month=(int)$interval->format('%R%m');
            $day=(int)$interval->format('%R%a');
            $hour=(int)$interval->format('%R%h');
            $min=(int)$interval->format('%R%i');
             if($year > 0 || $month > 0 || $day > 0 || $hour > 0){
                $delFlg = true;
            }else{
                if($min >= 10 ){
                    $delFlg = true;
                }
            }
        }
        return $delFlg;
    }

    private function fnDelDB2WTBL($db2con,$EXETIME){
        $result = true;
        $params = array();
        $strSQL .= ' DELETE ';
        $strSQL .= ' FROM ';
        $strSQL .= '     DB2WTBL ';
        $strSQL .= ' WHERE ';
        $strSQL .= '     WTBLCT < TIMESTAMP(\''.$EXETIME.'\')- 1 DAYS ';
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $result = 'FAIL_DEL';
            e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
             $r = db2_execute($stmt,$params);
            if($r === false){
                $result = 'FAIL_DEL';
                e_log('失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
            }
        }
        return $result;
    }

    private function is_leap_year($year) {
        return ((($year % 4) == 0) && ((($year % 100) != 0) || (($year % 400) == 0)));
    }

}

/*
*-------------------------------------------------------* 
* 実行ログの削除処理（旧dropLogFile.php)
*-------------------------------------------------------*
*/

class dropLogFile{

    public function exe(){

        $db2con = cmDb2Con();
        cmSetPHPQUERY($db2con);

        //保管日数取得
        $wcval = $this->fnGetWCVAL($db2con);
        $hokn  = $wcval[0]['WCVAL'];
        $this->fnDeleteLog($db2con, $hokn);

        cmDb2Close($db2con);

    }

    private function fnGetWCVAL($db2con)
    {
        $data = array();
        
        $strSQL = ' SELECT WCVAL ';
        $strSQL .= ' FROM DB2WCPN as A ';
        $strSQL .= ' WHERE WCKEY = ? ';
        
        $params = array(
            '001'
        );
        
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $data = false;
        } else {
            $r = db2_execute($stmt, $params);
            
            if ($r === false) {
                $data = false;
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
        }
        return $data;
        
    }

    private function fnDeleteLog($db2con, $HOKN)
    {
        
        $rs = '0';
        //0の場合はなにもしない
        if (cmMer($HOKN) != '0') {
            
            //今日の日付より$HOKNの日数分さかのぼった日付を取得
            $befdate = date("Ymd", strtotime(date('Ymd') . " -" . $HOKN . " day"));
            
            //$befdate以前のログを消去
            
            $strSQL = ' DELETE FROM DB2WLOG ';
            $strSQL .= ' WHERE WLDAY < ? ';
            
            $params = array(
                $befdate
            );
            
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $rs = '1';
            } else {
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $rs = '1';
                } else {
                    
                    //構文
                    $strSQL = ' DELETE FROM DB2WSDA ';
                    $strSQL .= ' WHERE SDDAY < ? ';
                    
                    $params = array(
                        $befdate
                    );
                    
                    $stmt = db2_prepare($db2con, $strSQL);
                    if ($stmt === false) {
                        $rs = '1';
                    } else {
                        $r = db2_execute($stmt, $params);
                        if ($r === false) {
                            $rs = '1';
                        } else {
                            //構文
                            $strSQL = ' DELETE FROM DB2WSDK ';
                            $strSQL .= ' WHERE SKDAY < ? ';
                            
                            $params = array(
                                $befdate
                            );
                            
                            $stmt = db2_prepare($db2con, $strSQL);
                            if ($stmt === false) {
                                $rs = '1';
                            } else {
                                $r = db2_execute($stmt, $params);
                                if ($r === false) {
                                    $rs = '1';
                                }
                                
                            }
                        }
                        
                    }
                }
            }
        }
        return $rs;
        
    }

}

/*
*-------------------------------------------------------* 
* 別区画のPHPQUERYHの削除処理（旧delQueryTempTbl.php)
*-------------------------------------------------------*
*/

class delQueryTempTbl{

    public function exe(){

        // 定義に使用しているすべてのライブラリーリスト取得
        //fnGetRDBList 
        // ある場合 ①RDBのSAVE_DBから作成されるのは10分以上の「＠」がつけている全部のテーブルを削除
        //          ②保険日数を超えているスケジュール実行にテーブルを削除
        // 1-②
        // 削除対象テーブルと日数を取得して削除；

        $rtn = 0;
        $EXETIME = date("Y-m-d H:i:s");

        $db2con = cmDb2Con();
        cmSetPHPQUERY($db2con);

        if($rtn === 0){
            $rdbrs = $this->fnGetFDB2CSV1RDBList($db2con);
            if($rdbrs['result'] !== true) {
                $rtn = 1;
                e_log(showMsg($rdbrs['result']));
            }else{
                $rdbrs = $rdbrs['data'];
            }
            if($rtn === 0){
                if(count($rdbrs)>0){
                    foreach($rdbrs as $rdb){
                        $_SESSION['PHPQUERY']['RDBNM'] = $rdb['D1RDB'];
                        $db2RDBCon = cmDB2ConRDB(SAVE_DB);
                        $rsDel = $this->fnDelTmpTbl($db2RDBCon,$EXETIME);
                        if($rsDel === 1){
                            $rtn = 1;
                            e_log('削除失敗'.$scheduleTbl['WHOUTF']);
                        }
                        $rsScheduleTbl = $this->fnGetScheduleTblList($db2con,$rdb['D1RDB']);
                        if($rsScheduleTbl['result'] !== true){
                            $rtn = 1;
                            e_log(showMsg($rsScheduleTbl['result']));
                            break;
                        }else{
                            $rsScheduleTbl = $rsScheduleTbl['data'];
                            if(count($rsScheduleTbl) > 0){
                                foreach($rsScheduleTbl as $scheduleTbl){
                                    $rsDel = $this->fnDelTmpTbl($db2RDBCon,$EXETIME,$scheduleTbl['WHOUTF'],$scheduleTbl['D1HOKN']);
                                    if($rsDel === 1){
                                        $rtn = 1;
                                        e_log('削除失敗'.$scheduleTbl['WHOUTF']);
                                        break;
                                    }
                                }
                            }
                        }
                        cmDb2Close($db2RDBCon);
                    }
                }
            }
        }


    }

    private function fnGetScheduleTblList($db2con,$RDBNM){
        $rtn = 0;
        $scheduleTbl = array();
        $strSQL  =  ' SELECT     A.WHNAME, ';
        $strSQL .=  '             A.WHOUTF ';
        $strSQL .=  '             ,B.D1HOKN ';
        $strSQL .=  ' FROM  ';
        $strSQL .=  '     DB2WHIS AS A  ';
        $strSQL .=  ' LEFT JOIN  FDB2CSV1 AS B  ';
        $strSQL .=  ' ON A.WHNAME = B.D1NAME  ';
        $strSQL .=  ' WHERE ';
        $strSQL .=  '    A.WHOUTF <> \'\' AND ';
        $strSQL .=  '    B.D1RDB = ? AND ';
        $strSQL .=  '    B.D1HOKN > 0 ';
        $params = array($RDBNM);

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $scheduleTbl = array('result' => 'FAIL_SEL');
            e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $scheduleTbl = false;
                e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row === false){
                        e_log('fetcherr:'.db2_stmt_errormsg());
                    }else{
                        $scheduleTbl[] = $row;
                    }
                }
                $scheduleTbl = array('result' => true,'data' => umEx($scheduleTbl));
            }
        }
        e_log('scheduleRDBLIST:'.print_r($scheduleTbl,true));
        return $scheduleTbl;
    }

    private function fnGetFDB2CSV1RDBList($db2con){
        $rtn = 0;
        $fdb2csv1Rdb = array();
        $strSQL  = ' SELECT ';
        $strSQL .= '     A.D1RDB ';
        $strSQL .= ' FROM ';
        $strSQL .= '   FDB2CSV1 A ';
        $strSQL .= ' WHERE ';
        $strSQL .= '     A.D1RDB <> \'\' ';
        $strSQL .= ' AND A.D1RDB <> \''.RDB.'\' ';
        $strSQL .= ' AND A.D1RDB <> \''.RDBNAME.'\' ';
        $strSQL .= ' GROUP BY ';
        $strSQL .= '     A.D1RDB ';
        $params = array();
        //e_log($strSQL);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $fdb2csv1Rdb = array('result' => 'FAIL_SEL');
            e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $fdb2csv1Rdb = false;
                e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    if($row === false){
                        e_log('fetcherr:'.db2_stmt_errormsg());
                    }else{
                        $fdb2csv1Rdb[] = $row;
                    }
                }
                $fdb2csv1Rdb = array('result' => true,'data' => umEx($fdb2csv1Rdb));
            }
        }
        //e_log('RDBARR:'.print_r($fdb2csv1Rdb,true));
        return $fdb2csv1Rdb;
    }

    private function fnDelTmpTbl($db2RDBCon,$exeTime,$tblname = '',$cntDay = 0){
        $rtn = 0;
        if($rtn === 0){
            $rs = $this->fnGetTmpTblLst($db2RDBCon,$tblname);
            if($rs['result'] !== true){
                $rtn = 1;
                e_log('実行結果テーブル取得失敗。エラー内容：'.$rs['result']);
            }else{
                $tmpTblList = $rs['data'];
            }
        }
        if($rtn === 0){
            if(count($tmpTblList) >0){
                foreach($tmpTblList as $tmpTbl){
                    $chkRs = $this->checkTime($tmpTbl['CREATE_TIME'],$exeTime,$cntDay);
                    if($chkRs){
                        e_log('削除対象：'.print_r($tmpTbl,true));
                        //var_dump(print_r($tmpTbl,true));
                    }
                    if($chkRs){
                        $strDelSQL  = ' DROP TABLE '.SAVE_DB.'/'.$tmpTbl['TABLE_NAME'];
                        $result = db2_exec($db2RDBCon, $strDelSQL);
                        if ($result) {
                            e_log($tmpTbl['TABLE_NAME'].'削除成功。');
                        }else{
                            e_log($tmpTbl['TABLE_NAME'].'削除失敗。');
                            e_log('エラー内容'.db2_stmt_errormsg());
                            $rtn = 1;
                            break;
                        }
                    }
                }
            }
        }
        return $rtn;
    }

    private function fnGetTmpTblLst($db2con,$tblname){
        $tmpTbl = array();
        
        $strSQL  =  ' SELECT ';
        $strSQL .=  '     TABLE_NAME,VARCHAR_FORMAT(LAST_ALTERED_TIMESTAMP, \'YYYY-MM-DD HH24:MI:SS\') AS CREATE_TIME ';
        $strSQL .=  ' FROM ';
        $strSQL .=  '     QSYS2/SYSTABLES ';
        $strSQL .=  ' WHERE ';
        $strSQL .=  '     TABLE_SCHEMA = \''.SAVE_DB.'\' AND ';
        if($tblname === ''){
            $strSQL .=  '     TABLE_NAME LIKE \'@%\' ';
            $params = array();
        }else{
            $strSQL .=  '     TABLE_NAME = ? ';
            $params = array($tblname);
        }
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $tmpTbl = array('result' => 'FAIL_SEL');
            e_log('準備失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
        }else{
             $r = db2_execute($stmt,$params);
            if($r === false){
                $tmpTbl = false;
                e_log('実行失敗 実行SQL：'.$strSQL.'\nエラー内容：'.db2_stmt_errormsg());
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $tmpTbl[] = $row;
                }
                $tmpTbl = array('result' => true,'data' => umEx($tmpTbl));
            }
        }
        e_log('TempTableList:'.print_r($tmpTbl,true));
        return $tmpTbl;
    }

    private function checkTime($createtime,$exeTime,$cntDay = 0){
        $delFlg = false;
        $datetime1 = date("Y-m-d H:i:s", strtotime($createtime));
        $datetime2 = $exeTime;
        $interval = date_diff(new DateTime($datetime1), new DateTime($datetime2));
        $year=$interval->format('%R%y');
        $month=$interval->format('%R%m');
        $day=$interval->format('%R%a');
        $hour=$interval->format('%R%h');
        $min=$interval->format('%R%i');

        if($cntDay > 0){
            if($day >= $cntDay ){
                $delFlg = true;
            }
        }else{
            if($min >= 10 ){
                $delFlg = true;
            }
        }
        return $delFlg;
    }

}