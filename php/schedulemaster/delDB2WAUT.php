<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WANAME = $_POST['WANAME'];
$WAPKEY = $_POST['WAPKEY'];
$WAMAIL = $_POST['WAMAIL'];
$WAQGFLG = $_POST['WAQGFLG'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$D1NAMELIST=array();

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/

//htmldecde
$WANAME = cmHscDe($WANAME);
$WAPKEY = cmHscDe($WAPKEY);
$WAMAIL = cmHscDe($WAMAIL);


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($WAQGFLG === '1'){
		$rs=cmGetQueryGroup($db2con,cmMer($WANAME),cmMer($WAPKEY),'');
		if($rs['result'] !== true){
			$rtn = 2;
			$msg = showMsg($rs['result'],array('クエリーグループ'));
		}else{
			$D1NAMELIST=umEx($rs['data'],true);
		}
	}
}
//クエリーグループ
//クエリー存在チェック
if($rtn === 0){
	if($WAQGFLG==='1'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
			$paramUser=($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4')?$userData[0]['WUUID']:'';
            $chkQry = cmChkQueryGroup($db2con,$paramUser,cmMer($WANAME),$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 3;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
			if($rtn===0){
				if($res['LASTFLG']==='1'){
					$LASTD1NAME=$res[LASTD1NAME];
				}
			}
        }//end of for loop
	}else{
	    $chkQry = array();
	    $chkQry = cmChkQuery($db2con,'',cmMer($WANAME),cmMer($WAPKEY));
	    if($chkQry['result'] === 'NOTEXIST_PIV'){
	        $rtn = 1;
	        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
	    }else if($chkQry['result'] !== true){
	        $rtn = 1;
	        $msg = showMsg($chkQry['result'],array('クエリー'));
	    }
	}

}
if($rtn === 0){
    if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
        $chkQryUsr = chkVldQryUsr($db2con,cmMer($WANAME),$userData[0]['WUAUTH']);
        if($chkQryUsr['result'] === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg('ログインユーザーに指定したクエリーに対してもスケージュルの権限がありません。');
        }else if($chkQryUsr['result'] !== true){
            $rtn = 3;
            $msg = showMsg($chkQryUsr['result']);
        }
    }
}
//ユーザー存在チェック
if($rtn === 0){
    $rs = fnCheckDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL);
    if($rs === false){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_UPD',array('削除対象の配信設定'));//'削除対象の配信設定が存在しません。';
    }
}


if($rtn === 0){
    $rs = fnDeleteDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL);
    if($rs === false){
        $rtn = 1;
        $msg = showMsg('FAIL_DEL');//'削除処理に失敗しました。管理者にお問い合わせください。';
    }
}
if($rtn === 0){
    $rs = fnDeleteDB2WAUL($db2con,$WANAME,$WAPKEY,$WAMAIL);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}


cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'WANAME' => $WANAME,
    'WAPKEY' => $WAPKEY,
    'WAMAIL' => $WAMAIL,
    'licenseQuery'=>$licenseQuery
);
echo(json_encode($rtnAry));

/**
* 配信設定存在チェック いたらtrue
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WANAME 定義名
* @param String  $WAPKEY PIVOTKEY
* @param String  $WAMAIL 宛先メールアドレス
* 
*/

function fnCheckDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL){

    $data = array();
    $rs = false;

    $strSQL  = ' SELECT A.WANAME ';
    $strSQL .= ' FROM DB2WAUT AS A ' ;
    $strSQL .= ' WHERE A.WANAME = ? ' ;
    $strSQL .= ' AND A.WAPKEY = ? ' ;
    $strSQL .= ' AND A.WAMAIL = ? ' ;

    $params = array(
        $WANAME,
        $WAPKEY,
        $WAMAIL
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = true;
            }
        }
    }

    return $rs;
}


/**
* 配信設定削除
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WANAME 定義名
* @param String  $WAPKEY PIVOTKEY
* @param String  $WAMAIL 宛先メールアドレス
* 
*/

function fnDeleteDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WAUT ';
    $strSQL .= ' WHERE WANAME = ? ' ;
    $strSQL .= ' AND WAPKEY = ? ' ;
    $strSQL .= ' AND WAMAIL = ? ' ;

    $params = array(
        $WANAME,
        $WAPKEY,
        $WAMAIL
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }
    return $rs;

}
function fnDeleteDB2WAUL($db2con,$WANAME,$WAPKEY,$WAMAIL){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WAUL ';
    $strSQL .= ' WHERE WLNAME = ? ' ;
    $strSQL .= ' AND WLPKEY = ? ' ;
    $strSQL .= ' AND WLMAIL = ? ' ;

    $params = array(
        $WANAME,
        $WAPKEY,
        $WAMAIL
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $rs = db2_execute($stmt,$params);
        if($rs === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}
