<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WSNAME = $_POST['WSNAME'];
$WSPKEY = $_POST['WSPKEY'];
$WSFRQ  = $_POST['WSFRQ'];
$WSODAY = $_POST['WSODAY'];
$WSWDAY = $_POST['WSWDAY'];
$WSMDAY = $_POST['WSMDAY'];
$WSTIME = $_POST['WSTIME'];
$WSQGFLG= $_POST['WSQGFLG'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$D1NAMELIST=array();

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/

//htmldecode
$WSNAME = cmHscDe($WSNAME);
$WSPKEY = cmHscDe($WSPKEY);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($WSQGFLG === '1'){
		$rs=cmGetQueryGroup($db2con,cmMer($WSNAME),cmMer($WSPKEY),'');
		if($rs['result'] !== true){
			$rtn = 2;
			$msg = showMsg($rs['result'],array('クエリーグループ'));
		}else{
			$D1NAMELIST=umEx($rs['data'],true);
		}
	}
}

//クエリー存在チェック
if($rtn === 0){
	if($WSQGFLG==='1'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,'',cmMer($WSNAME),$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 2;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }//end of for loop
	}else{
	    $chkQry = array();
	    $chkQry = cmChkQuery($db2con,'',cmMer($WSNAME),cmMer($WSPKEY));
	    if($chkQry['result'] === 'NOTEXIST_PIV'){
	        $rtn = 2;
	        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
	    }else if($chkQry['result'] !== true){
	        $rtn = 2;
	        $msg = showMsg($chkQry['result'],array('クエリー'));
	    }
	}
}

//スケジュール存在チェック
if($rtn === 0){
    $rs = fnCheckDB2WSCD($db2con,$WSNAME,$WSPKEY,$WSFRQ,$WSODAY,$WSWDAY,$WSMDAY,$WSTIME,$WSQGFLG);
    if($rs !== true ){
        $rtn = 1;
        $msg = showMsg($rs,array('スケジュール'));
    }
}
if($rtn === 0){
    $rs = fnDeleteDB2WSCD($db2con,$WSNAME,$WSPKEY,$WSFRQ,$WSODAY,$WSWDAY,$WSMDAY,$WSTIME,$WSQGFLG);
    if($rs === false){
        $rtn = 1;
        $msg = showMsg('FAIL_DEL');//'削除処理に失敗しました。管理者にお問い合わせください。';
    }
}


cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnAry));


/**
* スケジュール存在チェック いたらtrue
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WSNAME 定義名
* @param String  $WSPKEY PIVOTKEY
* @param String  $WSFRQ  実行頻度
* @param String  $WSODAY スケジュール-日
* @param String  $WSWDAY スケジュール-週
* @param String  $WSMDAY スケジュール-月
* @param String  $WSTIME 実行時分
* 
*/

function fnCheckDB2WSCD($db2con,$WSNAME,$WSPKEY,$WSFRQ,$WSODAY,$WSWDAY,$WSMDAY,$WSTIME,$WSQGFLG){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.WSNAME ';
    $strSQL .= ' FROM DB2WSCD AS A ' ;
    $strSQL .= ' WHERE A.WSNAME = ? ' ;
    $strSQL .= ' AND A.WSPKEY = ? ' ;
    $strSQL .= ' AND A.WSFRQ = ? ' ;
    $strSQL .= ' AND A.WSODAY = ? ' ;
    $strSQL .= ' AND A.WSWDAY = ? ' ;
    $strSQL .= ' AND A.WSMDAY = ? ' ;
    $strSQL .= ' AND A.WSTIME = ? ' ;
    $strSQL .= ' AND A.WSQGFLG = ? ' ;

    $params = array(
        $WSNAME,
        $WSPKEY,
        $WSFRQ,
        $WSODAY,
        $WSWDAY,
        $WSMDAY,
        $WSTIME,
		$WSQGFLG
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}

/**
* ユーザー削除
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WSNAME 定義名
* @param String  $WSPKEY PIVOTKEY
* @param String  $WSFRQ  実行頻度
* @param String  $WSODAY スケジュール-日
* @param String  $WSWDAY スケジュール-週
* @param String  $WSMDAY スケジュール-月
* @param String  $WSTIME 実行時分
* @param String  $WSQGFLG クエリーグループFLG
* 
*/

function fnDeleteDB2WSCD($db2con,$WSNAME,$WSPKEY,$WSFRQ,$WSODAY,$WSWDAY,$WSMDAY,$WSTIME,$WSQGFLG){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WSCD ';
    $strSQL .= ' WHERE WSNAME = ? ' ;
    $strSQL .= ' AND WSPKEY = ? ' ;
    $strSQL .= ' AND WSFRQ = ? ' ;
    $strSQL .= ' AND WSODAY = ? ' ;
    $strSQL .= ' AND WSWDAY = ? ' ;
    $strSQL .= ' AND WSMDAY = ? ' ;
    $strSQL .= ' AND WSTIME = ? ' ;
    $strSQL .= ' AND WSQGFLG = ? ' ;

    $params = array(
        $WSNAME,
        $WSPKEY,
        $WSFRQ,
        $WSODAY,
        $WSWDAY,
        $WSMDAY,
        $WSTIME,
		$WSQGFLG
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $rs = db2_execute($stmt,$params);
        if($rs === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}

/**
* 権限情報削除
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WDUID  ユーザーＩＤ
* 
*/

function fnDeleteDB2WDEF($db2con,$WDUID){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WDEF ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WDUID = ? ' ;

    $params = array(
        $WDUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $rs = db2_execute($stmt,$params);
        if($rs === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}
