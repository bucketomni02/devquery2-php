<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$DATA = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$check = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$D1NAME = cmHscDe($D1NAME);
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

//バリデーションチェック
if($rtn === 0){
    $byte = checkByte($DATA['D1DIRE']);
    if($byte[1] > 0){
        $rtn = 1;
        $msg =showMsg('FAIL_BYTE',array('出力先')); //'出力先は半角文字で入力してください。';
        $focus = 'D1DIRE';
    }
}

if($rtn === 0){
    if(!checkMaxLen($DATA['D1DIRE'],256)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('出力先'));// '出力先が入力桁数を超えています。';
        $focus = 'D1DIRE';
    }
}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/


if($rtn === 0){

    $rs = fnUpdateD1DIRE($db2con,$D1NAME,cmStr0($DATA['D1DIRE']));

    if($rs === false){
        $rtn = 1;
        $msg = showMsg('FAIL_INS');//'登録処理に失敗しました。管理者にお問い合わせください。';
    }

}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'PROC' => $proc,
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => $DATA
);

echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* 保管日数更新
*-------------------------------------------------------*
*/

function fnUpdateD1DIRE($db2con,$D1NAME,$D1DIRE){

    $rs = true;

    //構文
    $strSQL  = ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= ' D1DIRE = ? ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    $strSQL .= ' AND D1NAME = ? ';

    $params = array(
        $D1DIRE,
        $D1NAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }
    return $rs;

}