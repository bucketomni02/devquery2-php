<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$SOPKEY = $_POST['PMPKEY'];
$webf = $_POST['D1WEBF'];
$D1QGFLG = $_POST['D1QGFLG'];
$DATA = json_decode($_POST['DATA'],true);
$D1DIRE = '';
if(isset($DATA['D1DIRE'])){
        $D1DIRE = $DATA['D1DIRE'];
        $D1DIRE = str_replace(' ','',$D1DIRE);
}
$D1DIEX = '';
if(isset($DATA['D1DIEX'])){
        $D1DIEX = $DATA['D1DIEX'];
        $D1DIEX = str_replace(' ','',$D1DIEX);
}

/*   
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$check = '';
$D1NAMELIST=array();
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$D1NAME = cmHscDe($D1NAME);
$SOPKEY = cmHscDe($SOPKEY);


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($D1QGFLG==='1'){
		$rs=cmGetQueryGroup($db2con,$D1NAME,'','');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリーグループ'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
			/*foreach($D1NAMELIST as $key => $res){
				if($res['LASTFLG']==='1'){
					$D1NAME=$res['QRYGSQRY'];
				}
			}*/
        }
	}
}
//クエリーグループ
//バリデーションチェック
if($rtn === 0){
    $check = checkNum($DATA['D1HOKN']);
    if($check === false){
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array('保管日数','数字'));//'保管日数は数字で入力してください。';
        $focus = 'D1HOKN';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['D1HOKN'],4)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('保管日数'));//'保管日数が入力桁数を超えています。';
        $focus = 'D1HOKN';
    }
}

if($rtn ===0){
    if(cmMer($D1DIRE) !== ''){
        if($licenseIFS !== true){
            $rtn = 1;
            //$msg = "出力先は使用できません。";
            $msg = showMsg('FAIL_NUSE',array('CSV出力先'));
        }
    }
}

if($rtn === 0){
    if(!checkMaxLen($DATA['D1DIRE'],256)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('CSV出力先'));//'出力先が入力桁数を超えています。';
        $focus = 'D1DIRE';
    }
}

if($rtn === 0){
    $D1DIREARR = explode('/',$D1DIRE);
    if(count($D1DIREARR) > 0){
        foreach ($D1DIREARR as $value){
            
        }
        
    }
}

if($rtn === 0){
    if(substr($D1DIRE, -1) === '/'){
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array('CSV出力先','CSVファイル名'));// CSV出力先にCSVファイル名を入力してください。
        $focus = 'D1DIRE';
    }
}
if($rtn === 0){
    if($D1DIRE !== ''){
        if(cmMer($webf) === ''){
            $filepath    = substr($D1DIRE, 0, intval(strrpos($D1DIRE, '/')) + 1);
            if (!file_exists(cmMer($filepath))) {
                $rtn = 1;
                $msg = showMsg('FAIL_PATH',array('CSV出力先'));// CSV出力先はありません。
                $focus = 'D1DIRE';
            }
        }
    }
}

//Excelの出力先validation
//ライセンスがない場合
if($rtn ===0){
    if(cmMer($D1DIEX) !== ''){
        if($licenseIFS !== true){
            $rtn = 1;
            $msg = showMsg('FAIL_NUSE',array('EXCEL出力先'));
        }
    }
}

if($rtn ===0){
    if(cmMer($D1DIEX) !== '' && cmMer($D1DIRE) !== ''){
        if(cmMer($D1DIEX) === cmMer($D1DIRE)){        
            $rtn = 1;
            $msg = showMsg('FAIL_OVRWR');       
        }
    }
}
//入力桁数を超えている場合
if($rtn === 0){
    if(!checkMaxLen($DATA['D1DIEX'],256)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('EXCEL出力先'));//'出力先が入力桁数を超えています。';
        $focus = 'D1DIEX';
    }
}
if($rtn === 0){
    if(substr($D1DIEX, -1) === '/'){
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array('EXCEL出力先','EXCELファイル名'));// EXCEL出力先にEXCELファイル名を入力してください。
        $focus = 'D1DIEX';
    }
}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/

if($rtn === 0){
	if($D1QGFLG==='1'){
	    $data = fnGetNAME($db2con, $D1NAME,$D1QGFLG);
	    if (count($data) === 0) {
	        $rs = fnInsertSO0MAL($db2con, $D1NAME,cmStr0($DATA['D1HOKN']),$DATA['D1DIRE'],$DATA['D1DIEX'],$DATA['D1CTFL'],cmStr0($DATA['D10MAL']),
		                            $DATA['D1OUTSE'],$DATA['D1OUTSR'],$DATA['D1OUTSA'],$D1QGFLG);
	        if ($rs === false) {
	            $rtn = 1;
	            $msg = showMsg('FAIL_INS'); //'登録処理に失敗しました。管理者にお問い合わせください。';
	            
	        }
	    } else if (count($data) > 0) {
	        $rs = fnUpdateSO0MAL($db2con, $D1NAME,cmStr0($DATA['D1HOKN']),$DATA['D1DIRE'],$DATA['D1DIEX'],$DATA['D1CTFL'],cmStr0($DATA['D10MAL']),
		                            $DATA['D1OUTSE'],$DATA['D1OUTSR'],$DATA['D1OUTSA']);
	        if ($rs === false) {
	            $rtn = 1;
	            $msg = showMsg('FAIL_INS'); // '登録処理に失敗しました。管理者にお問い合わせください。';
	            
	        }
	    }

	}else{
		$rs = fnUpdateD1HOKN($db2con,$D1NAME,cmStr0($DATA['D1HOKN']),$DATA['D1DIRE'],$DATA['D1DIEX'],$DATA['D1CTFL'],cmStr0($DATA['D10MAL']),
		                            $DATA['D1OUTSE'],$DATA['D1OUTSR'],$DATA['D1OUTSA']);
		//e_log("getD1HOKN.php RS=>".$rs);
		if($rs === false){
			$rtn = 1;
			$msg = showMsg('FAIL_INS');//'登録処理に失敗しました。管理者にお問い合わせください。';
		}

	}

}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'PROC' => $proc,
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => $DATA
   // 'D1DIEX' => $D1DIEX,
   // 'D1DIRE' => $A
);

echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* 保管日数更新
*-------------------------------------------------------*
*/

function fnUpdateD1HOKN($db2con,$D1NAME,$D1HOKN,$D1DIRE,$D1DIEX,$D1CTFL,$D10MAL,$D1OUTSE,$D1OUTSR,$D1OUTSA){
    $rs = true;

    //構文
    $strSQL  = ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= ' D1HOKN = ? ,';
    $strSQL .= ' D1DIRE = ? ,';
    $strSQL .= ' D1DIEX = ? ,';
    $strSQL .= ' D10MAL = ? ,';
    $strSQL .= ' D1CTFL= ?  , ';
    //スケジュールの出力ファイル
    $strSQL .= ' D1OUTSE = ? ,';
    $strSQL .= ' D1OUTSR= ? , ';
    $strSQL .= ' D1OUTSA= ? ';
    $strSQL .= ' WHERE D1NAME <> \'\' ';
    $strSQL .= ' AND D1NAME = ? ';

    $params = array(
        $D1HOKN,
        $D1DIRE,
        $D1DIEX,
        $D10MAL,
        $D1CTFL,
        $D1OUTSE,
        $D1OUTSR,
        $D1OUTSA,
        $D1NAME
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }
    return $rs;
}
/*
*-------------------------------------------------------* 
*保管日数更新
*クエリーグループ
*-------------------------------------------------------*
*/

function fnUpdateSO0MAL($db2con,$D1NAME,$D1HOKN,$D1DIRE,$D1DIEX,$D1CTFL,$D10MAL,$D1OUTSE,$D1OUTSR,$D1OUTSA){
    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2WSOC ';
    $strSQL .= ' SET ';
    $strSQL .= ' SOHOKN = ? ,';
    $strSQL .= ' SODIRE = ? ,';
    $strSQL .= ' SODIEX = ? ,';
    $strSQL .= ' SO0MAL = ? ,';
    $strSQL .= ' SOCTFL= ?  , ';
    //スケジュールの出力ファイル
    $strSQL .= ' SOOUTSE = ? ,';
    $strSQL .= ' SOOUTSR= ? , ';
    $strSQL .= ' SOOUTSA= ? ';
    $strSQL .= ' WHERE SONAME <> \'\' ';
    $strSQL .= ' AND SONAME = ? AND SOPKEY = \'\' ';

    $params = array(
        $D1HOKN,
        $D1DIRE,
        $D1DIEX,
        $D10MAL,
        $D1CTFL,
        $D1OUTSE,
        $D1OUTSR,
        $D1OUTSA,
        $D1NAME
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }
    return $rs;
}
/*
*-------------------------------------------------------* 
*保管日数登録
*クエリーグループ
*-------------------------------------------------------*
*/

function fnInsertSO0MAL($db2con,$D1NAME,$D1HOKN,$D1DIRE,$D1DIEX,$D1CTFL,$D10MAL,$D1OUTSE,$D1OUTSR,$D1OUTSA,$D1QGFLG){
    $rs = true;

    //構文
    $strSQL  = ' INSERT INTO DB2WSOC ';
    $strSQL .= ' ( ';
	$strSQL .= '	SONAME,';
    $strSQL .= '	SOHOKN,';
    $strSQL .= '	SODIRE,';
    $strSQL .= '	SODIEX,';
    $strSQL .= '	SO0MAL,';
    $strSQL .= '	SOCTFL, ';
    //スケジュールの出力ファイル
    $strSQL .= '	SOOUTSE,';
    $strSQL .= '	SOOUTSR, ';
    $strSQL .= '	SOOUTSA, ';
    $strSQL .= '	SOQGFLG ';
	$strSQL .= ')';
	$strSQL .= 'VALUES(?,?,?,?,?,?,?,?,?,?)';

    $params = array(
		$D1NAME,
        $D1HOKN,
        $D1DIRE,
        $D1DIEX,
        $D10MAL,
        $D1CTFL,
        $D1OUTSE,
        $D1OUTSR,
        $D1OUTSA,
		$D1QGFLG
    );
	e_log("edtD1HOKN.php strSQL=>".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }
    return $rs;
}

function fnGetNAME($db2con, $SONAME,$D1QGFLG) {
    $data = array();
    $strSQL = ' SELECT A.SONAME ';
    $strSQL.= ' FROM DB2WSOC  AS A ';
    $strSQL.= ' WHERE SONAME = ? ';
    $strSQL.= ' AND SOQGFLG = ? AND SOPKEY=\'\' ';
    $params = array($SONAME,$D1QGFLG);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}