<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
 *-------------------------------------------------------* 
 * リクエスト
 *-------------------------------------------------------*
 */

$proc = $_POST['proc'];

$WANAME    = $_POST['WANAME'];
$WAPKEY    = $_POST['WAPKEY'];
$WAQGFLG   = $_POST['WAQGFLG'];
$schWAMAIL = $_POST['schWAMAIL'];
$schWAQUNM = $_POST['schWAQUNM'];
$DATA      = json_decode($_POST['DATA'], true);
$WAFONT    = (isset($DATA['WAFONT']) ? $DATA['WAFONT'] : '12');
$WACSV     = $_POST['WACSV'];
$WAXLS     = $_POST['WAXLS'];
$WAHTML    = $_POST['WAHTML'];
$WASPND    = $_POST['WASPND'];
$WASFLG    = $_POST['WASFLG'];
$WASNKB    = $_POST['WASNKB'];
$WATYPE    = $_POST['WATYPE'];
$WAQUNM    = $_POST['WAQUNM'];
$WAQUNM    = cmMer($WAQUNM);
$WAPIV     = (isset($_POST['WAPIV']) ? $_POST['WAPIV'] : '');//MSM add for pivot link flag
$WANGFLG   = (isset($_POST['WANGFLG']) ? $_POST['WANGFLG'] : '');//MSM add for Normal or Graph link flag 
$WAGPID    = (isset($_POST['WAGPID']) ? $_POST['WAGPID'] : '');//MSM add for Graph ID

/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$rtn       = 0;
$msg       = '';
$focus     = '';
$check     = '';
$D1NAMELIST=array();

/*
 *-------------------------------------------------------* 
 * チェック処理
 *-------------------------------------------------------*
 */

$WANAME    = cmHscDe($WANAME);
$WAPKEY    = cmHscDe($WAPKEY);
$schWAMAIL = cmHscDe($schWAMAIL);
$schWAQUNM = cmHscDe($schWAQUNM);
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($WAQGFLG==='1'){
		$rs=cmGetQueryGroup($db2con,$WANAME,$WAPKEY,'');
		if($rs['result'] !== true){
			$rtn = 2;
			$msg = showMsg($rs['result'],array('クエリーグループ'));
		}else{
			$D1NAMELIST=umEx($rs['data'],true);
		}
	}
}
//クエリーグループ
//クエリー存在チェック
if ($rtn === 0) {
	if($WAQGFLG==='1'){
		foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,'',$WANAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 2;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 2;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }//end of for loop
	}else{
	    $chkQry = array();
	    $chkQry = cmChkQuery($db2con, '', $WANAME, $WAPKEY);
	    if ($chkQry['result'] === 'NOTEXIST_PIV') {
	        $rtn = 2;
	        $msg = showMsg('NOTEXIST_GET', array(
	            'ピボット'
	        ));
	    } else if ($chkQry['result'] !== true) {
	        $rtn = 2;
	        $msg = showMsg($chkQry['result'], array(
	            'クエリー'
	        ));
	    }
	}

}
 $updFlg = false;
if ($proc === "UPD") {
    if ($schWAMAIL === trim($DATA['WAMAIL'])) {
        $updFlg = true;
    }
    $check = fnCheckDB2WAUT($db2con, $WANAME, $WAPKEY, $schWAMAIL);
    
    if ($check === false) {
        $rtn = 1;
        $msg = showMsg('NOTEXIST_UPD',array('この配信設定'));//'この配信設定が存在しません。';
    }
}
//チェック
if ($rtn === 0) {
    if (cmMer($DATA['WAMAIL']) === "") {
        $rtn   = 1;
        $msg   = showMsg('FAIL_REQ',array('アドレス、メールグループ'));// 'アドレス、メールグループは入力必須です。';
        $focus = 'WAMAIL';
    }
}

//バリデーションチェック
if ($rtn === 0) {
    $byte = checkByte($DATA['WAMAIL']);
    if ($byte[1] > 0) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_BYTE',array('アドレス、メールグループ'));//'アドレス、メールグループは半角文字で入力してください。';
        $focus = 'WAMAIL';
    }
}

if ($rtn === 0) {
    if (!checkMaxLen($DATA['WAMAIL'], 256)) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_MAXLEN',array('アドレス、メールグループ'));//'アドレス、メールグループが入力桁数を超えています。';
        $focus = 'WAMAIL';
    }
}

/*
*メールアドレス
*メールグループに入ってるかメール形になってるかのチェック
*
*/
if($rtn === 0){
    $WAMAIL = $DATA['WAMAIL'];
 $mailAry = explode(";",$WAMAIL);
   foreach($mailAry as $value){
        $gpFlg = false;
        $value = cmMer($value);
        if($value !== ''){
            $gValue = trim($value);
             $rs = chkMailGroup($db2con,$gValue);
             if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
             }else{
                    if($rs['data'] === 'NOGPEXIST'){
                            $mValue = trim($value);
                           $rs =  cmMailFormatChk($mValue);
                            if($rs === false){
                                $rtn = 1;
                                $msg = showMsg('FAIL_MAIL',array('アドレス、メールグループ'));//'アドレス、メールグループはメールグループに入ってないかメールアドレスが正しくありません。';
                                $focus = 'MAMAIL';
                            }
                    }
             }
        }
    }
}

//バリデーションチェック
if ($rtn === 0) {
    $byte = checkByte($DATA['WAMACC']);
    if ($byte[1] > 0) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_BYTE',array('CC'));//'CCアドレスは半角文字で入力してください。';
        $focus = 'WAMACC';
    }
}

if ($rtn === 0) {
    if (!checkMaxLen($DATA['WAMACC'], 256)) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_MAXLEN',array('CC'));//'CCアドレスが入力桁数を超えています。';
        $focus = 'WAMACC';
    }
}

/*
*ＣＣメールアドレス
*メールグループに入ってるかメール形になってるかのチェック
*
*/
if($rtn === 0){
 $WAMACC = $DATA['WAMACC'];
 $mailAry = explode(";",$WAMACC);
   foreach($mailAry as $value){
        $gpFlg = false;
        $value = cmMer($value);
        if($value !== ''){
            $gValue = trim($value);
             $rs = chkMailGroup($db2con,$gValue);
             if($rs['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result']);
             }else{
                    if($rs['data'] === 'NOGPEXIST'){
                            $mValue = trim($value);
                           $rs =  cmMailFormatChk($mValue);
                            if($rs === false){
                                $rtn = 1;
                                $msg = showMsg('FAIL_MAIL',array('CC'));//'CCアドレスはメールグループに入ってないかメールアドレスが正しくありません。';
                                $focus = 'MAMAIL';
                            }
                    }
             }
        }
    }
}
if($WASFLG === '1'){
        if ($rtn === 0) {
               $WAFRAD = trim($DATA['WAFRAD']);
              if ($WAFRAD !== '' ) {
                   $rs =  cmMailFormatChk($WAFRAD);
                    if($rs === false){
                        $rtn = 1;
                        $msg = showMsg('FAIL_CHK',array('差出人アドレス'));//'アドレス、メールグループはメールグループに入ってないかメールアドレスが正しくありません。';
                        $focus = 'WAFRAD';
                    }
              }
        }
}
if ($rtn === 0) {
    $byte = checkByte($DATA['WAFRAD']);
    if ($byte[1] > 0) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_BYTE',array('差出人アドレス'));//'差出人アドレスは半角文字で入力してください。';
        $focus = 'WAFRAD';
    }
}
if ($rtn === 0) {
    if (!checkMaxLen($DATA['WAFRAD'], 256)) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_MAXLEN',array('差出人アドレス'));//'差出人アドレスが入力桁数を超えています。';
        $focus = 'WAFRAD';
    }
}

if ($rtn === 0) {
    if (!checkMaxLen($DATA['WAFRNM'], 128)) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_MAXLEN',array('差出人名'));//'差出人名が入力桁数を超えています。';
        $focus = 'WAFRNM';
    }
}

if ($rtn === 0) {
    if (!checkMaxLen($DATA['WASUBJ'], 128)) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_MAXLEN',array('メールタイトル'));//'メールタイトルが入力桁数を超えています。';
        $focus = 'WASUBJ';
    }
}
if($WASFLG === '1'){
    if ($rtn === 0) {
        if (cmMer($DATA['WABODY']) === '') {
            $rtn   = 1;
            $msg = showMsg('FAIL_REQ',array('本文'));
            $focus = 'WABODY';
        }
    }
}
if ($rtn === 0) {
    if (!checkMaxLen($DATA['WABODY'], 1536)) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_MAXLEN',array('本文'));//'本文が入力桁数を超えています。';
        $focus = 'WABODY';
    }
}
if ($rtn === 0) {
    if (!checkMaxLen($WAQUNM, 100)) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_MAXLEN',array('ファイル名'));//'ファイル名が入力桁数を超えています。';
        $focus = 'WAQUNM';
    }
}
//バリデーションチェック
if ($rtn === 0) {
    $byte = checkByte($WAQUNM);
    if ($byte[1] > 0) {
        $rtn   = 1;
        $msg   = showMsg('FAIL_BYTE',array('ファイル名'));//'ファイル名は半角文字で入力してください。';
        $focus = 'WAQUNM';
    }
}
if ($rtn === 0) {
   if(strlen($WAQUNM) != strlen(utf8_decode($WAQUNM))){
        $rtn   = 1;
        $msg   = showMsg('FAIL_KANA',array(''));//'半角カタカナの入力はできません。';
    }
}
/*
if ($rtn === 0) {
    $kinshi = '\/:,;*?"<>|';
    //if(preg_match('(\|/|:|,|;|*|?|"|<|>|\|)', $WAQUNM) === 1){
    if (strpos($WAQUNM, '<') !== false){
        $rtn   = 1;
        $msg   = 'ファイル名に使用できない文字が含まれています。<br/>ファイル名には次の文字は使えません。<br/>　\ / : , ; * ? " < > | ';
        $focus = 'WAQUNM';
    }
}*/

  if ($rtn === 0) {
    if ($updFlg !== true) {
        $check = fnCheckDB2WAUT($db2con, $WANAME, $WAPKEY, $DATA['WAMAIL']);
        if ($check === true) {
            $rtn = 1;
            $msg = showMsg('ISEXIST',array('アドレス、メールグループ'));//'アドレス、メールグループはすでに登録されています。';
        }
    }
  }
    /**$WAQUNM = cmMer($WAQUNM)**/
    if ($rtn === 0 && $WAQUNM !== '') {
        if($schWAQUNM !== $WAQUNM){
            $check = fnCheckDB2WAUT_WAQUNM($db2con, $WANAME, $WAPKEY,$WAQUNM);
            if ($check === true) {
                $rtn = 1;
                $msg = showMsg('ISEXIST',array('このファイル名'));//'このファイル名はすでに登録されています。';
            }
        }
    }
/*
 *-------------------------------------------------------* 
 * 更新処理
 *-------------------------------------------------------*
 */
if ($rtn === 0) {
    
    if ($proc === "UPD") {
            $rs = fnUpdateDB2WAUT($db2con, $WANAME, $WAPKEY, $DATA['WAMAIL'], $DATA['WAMACC'], $WACSV, $WAXLS, $WAHTML, $WASNKB, $WATYPE, $WASPND, $WASFLG, $DATA['WAFRAD'], $DATA['WAFRNM'], $DATA['WASUBJ'], $DATA['WABODY'], $WAQUNM, $schWAMAIL,$WAFONT,$WAPIV,$WANGFLG,$WAGPID);//MSM add 3 parameter
    } else if ($proc === "ADD") {
            $rs = fnInsertDB2WAUT($db2con, $WANAME, $WAPKEY, $DATA['WAMAIL'], $DATA['WAMACC'], $WACSV, $WAXLS, $WAHTML, $WASNKB, $WATYPE, $WASPND, $WASFLG, $DATA['WAFRAD'], $DATA['WAFRNM'], $DATA['WASUBJ'], $DATA['WABODY'], $WAQUNM,$WAFONT,$WAQGFLG,$WAPIV,$WANGFLG,$WAGPID);//MSM add 3 parameter
    }
    
    if ($rs === false) {
        $rtn = 1;
        $msg = showMsg('FAIL_INS');//'登録処理に失敗しました。管理者にお問い合わせください。';
    }
    
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'PROC' => $proc,
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => $DATA,
    'WANAME' => $WANAME,
    'schWAMAIL' => $schWAMAIL,
    'mailAry' => $mailAry,
    'BASE_URL' => BASE_URL,
    'query_link' => $query_link
);

echo (json_encode($rtnAry));


/****
*
*メールグループ名に入ってるかのチェック
*
**/
function chkMailGroup($db2con,$MGNAME){
    if(!checkMaxLen($MGNAME,10)){
           $data = array('result' => true , 'data' => 'NOGPEXIST'); 
    }else{
            $data = array();
            $strSQL = "SELECT * FROM DB2MAGP WHERE MGNAME = ?";
            error_log($strSQL.$MGNAME);
            $params = array($MGNAME);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
               $data = array('result' => 'FAIL_SEL');
            } else {
                $r = db2_execute($stmt, $params);
                
                if ($r === false) {
                    $data = array('result' => 'FAIL_SEL');
                } else {
                    while ($row = db2_fetch_assoc($stmt)) {
                        $data[] = $row;
                    }
                    e_log(print_R($data,true));
                    if(count($data)=== 0){
                       $data = array('result' => true , 'data' => 'NOGPEXIST'); 
                    }else{
                       $data = array('result' => true,'data' => 'GPEXIST');
                    }
                 e_log(print_R($data,true));
                }
            }
        }
    return $data;
}
/**
 * 配信先情報登録
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * @param String  $WANAME 定義名
 * @param String  $WAPKEY PIVOTKEY
 * @param String  $WAMAIL 宛先メールアドレス
 * @param String  $WACSV  ＣＳＶ送付
 * @param String  $WAXLS  ＥＸＣＥＬ送付
 * @param String  $WASNKB 送信区分
 * @param String  $WATYPE メール形式
 * @param String  $WASPND サスペンド
 * @param String  $WASFLG 個別内容フラグ
 * @param String  $WAFRAD 差出人アドレス
 * @param String  $WAFRNM 差出人名
 * @param String  $WASUBJ メールタイトル
 * @param String  $WABODY 本文
 * 
 */

function fnInsertDB2WAUT($db2con, $WANAME, $WAPKEY, $WAMAIL, $WAMACC, $WACSV, $WAXLS, $WAHTML, $WASNKB, $WATYPE, $WASPND, $WASFLG, $WAFRAD, $WAFRNM, $WASUBJ, $WABODY, $WAQUNM,$WAFONT,$WAQGFLG,$WAPIV,$WANGFLG,$WAGPID)
{//MSM ADD for 3 new fields $WAPIV,$WANGFLG,$WAGPID
    
    $rs = true;
    
    //構文
    $strSQL = ' INSERT INTO DB2WAUT ';
    $strSQL .= ' (WANAME,WAPKEY,WAMAIL,WAMACC,WACSV,WAXLS,WAHTML,WASNKB,WATYPE,WASPND,WASFLG,WAFRAD,WAFRNM,WASUBJ,WABODY,WAQUNM,WAFONT,WAQGFLG,WAPIV,WANGFLG,WAGPID) ';
    $strSQL .= ' VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    
    $params = array(
        $WANAME,
        $WAPKEY,
        $WAMAIL,
        $WAMACC,
        $WACSV,
        $WAXLS,
        $WAHTML,
        $WASNKB,
        $WATYPE,
        $WASPND,
        $WASFLG,
        $WAFRAD,
        $WAFRNM,
        $WASUBJ,
        $WABODY,
        $WAQUNM,
        $WAFONT,
		$WAQGFLG,
        $WAPIV,
        $WANGFLG,
        $WAGPID
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = false;
    } else {
        $rs = db2_execute($stmt, $params);
    }
    return $rs;
    
}

/**
 * 配信先情報更新
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * @param String  $WANAME 定義名
 * @param String  $WAPKEY PIVOTKEY
 * @param String  $WAMAIL 宛先メールアドレス
 * @param String  $WACSV  ＣＳＶ送付
 * @param String  $WAXLS  ＥＸＣＥＬ送付
 * @param String  $WASNKB 送信区分
 * @param String  $WATYPE メール形式
 * @param String  $WASPND サスペンド
 * @param String  $WASFLG 個別内容フラグ
 * @param String  $WAFRAD 差出人アドレス
 * @param String  $WAFRNM 差出人名
 * @param String  $WASUBJ メールタイトル
 * @param String  $WABODY 本文
 * 
 */

function fnUpdateDB2WAUT($db2con, $WANAME, $WAPKEY, $WAMAIL, $WAMACC, $WACSV, $WAXLS, $WAHTML, $WASNKB, $WATYPE, $WASPND, $WASFLG, $WAFRAD, $WAFRNM, $WASUBJ, $WABODY, $WAQUNM, $schWAMAIL,$WAFONT,$WAPIV,$WANGFLG,$WAGPID)
{//MSM ADD for 3 new fields $WAPIV,$WANGFLG,$WAGPID
    
    $rs = true;
    
    //構文
    $strSQL  = ' UPDATE DB2WAUT ';
    $strSQL .= ' SET ';
    $strSQL .= ' WAMAIL = ?, ';
    $strSQL .= ' WAMACC = ?, ';
    $strSQL .= ' WACSV  = ?, ';
    $strSQL .= ' WAXLS  = ?, ';
    $strSQL .= ' WAHTML = ?, ';
    $strSQL .= ' WASNKB = ?, ';
    $strSQL .= ' WATYPE = ?, ';
    $strSQL .= ' WASPND = ?, ';
    $strSQL .= ' WASFLG = ?, ';
    $strSQL .= ' WAFRAD = ?, ';
    $strSQL .= ' WAFRNM = ?, ';
    $strSQL .= ' WASUBJ = ?, ';
    $strSQL .= ' WABODY = ?, ';
    $strSQL .= ' WAQUNM = ?, ';
    $strSQL .= ' WAFONT = ?, ';
    $strSQL .= ' WAPIV  = ?, ';
    $strSQL .= ' WANGFLG = ?, ';
    $strSQL .= ' WAGPID = ? ';
    $strSQL .= ' WHERE WANAME <> \'\' ';
    $strSQL .= ' AND WANAME = ? ';
    $strSQL .= ' AND WAPKEY = ? ';
    $strSQL .= ' AND WAMAIL = ? ';
    
    $params = array(
        $WAMAIL,
        $WAMACC,
        $WACSV,
        $WAXLS,
        $WAHTML,
        $WASNKB,
        $WATYPE,
        $WASPND,
        $WASFLG,
        $WAFRAD,
        $WAFRNM,
        $WASUBJ,
        $WABODY,
        $WAQUNM,
        $WAFONT,
        $WAPIV,
        $WANGFLG,
        $WAGPID,
        $WANAME,
        $WAPKEY,
        $schWAMAIL
    );
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = false;
    } else {
        $rs = db2_execute($stmt, $params);
        //e_log("edtDB2WAUT MSM after execute update => ".$strSQL.print_r($params,true));
        if ($rs !== false) {
            if ($WAMAIL !== $schWAMAIL) {
                $strSQL = ' UPDATE DB2WAUL ';
                $strSQL .= ' SET ';
                $strSQL .= ' WLMAIL = ? ';
                $strSQL .= ' WHERE WLNAME <> \'\' ';
                $strSQL .= ' AND WLNAME = ? ';
                $strSQL .= ' AND WLPKEY = ? ';
                $strSQL .= ' AND WLMAIL = ? ';
                $params = array(
                    $WAMAIL,
                    $WANAME,
                    $WAPKEY,
                    $schWAMAIL
                );
                $stmt   = db2_prepare($db2con, $strSQL);
                if ($stmt === false) {
                    $rs = false;
                } else {
                    $rs = db2_execute($stmt, $params);
                }
            }
        }
    }
    
    //echo(db2_stmt_errormsg());
    
    return $rs;
    
}


/**
 * 配信メール存在チェック いたらtrue
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con DBコネクション
 * @param String  $WANAME 定義名
 * @param String  $WAPKEY PIVOTKEY
 * @param String  $WAMAIL 宛先メールアドレス
 * 
 */
function fnCheckDB2WAUT($db2con, $WANAME, $WAPKEY, $WAMAIL)
{
    
    $data = array();
    $rs   = false;
    
    $strSQL = ' SELECT A.WANAME ';
    $strSQL .= ' FROM DB2WAUT AS A ';
    $strSQL .= ' WHERE A.WANAME = ? ';
    $strSQL .= ' AND A.WAPKEY = ? ';
    $strSQL .= ' AND A.WAMAIL = ? ';
    
    $params = array(
        $WANAME,
        $WAPKEY,
        $WAMAIL
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = false;
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $rs = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) > 0) {
                $rs = true;
            }
        }
    }
    return $rs;
}
//ファイル名重複チェック
function fnCheckDB2WAUT_WAQUNM($db2con, $WANAME, $WAPKEY,$WAQUNM)
{

    $data = array();
    $rs   = false;
    
    $strSQL = ' SELECT A.WAQUNM ';
    $strSQL .= ' FROM DB2WAUT AS A ';
    $strSQL .= ' WHERE A.WANAME = ? ';
    $strSQL .= ' AND A.WAPKEY = ? ';
    $strSQL .= ' AND A.WAQUNM = ? ';
    
    $params = array(
        $WANAME,
        $WAPKEY,
        $WAQUNM
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = false;
    } else {
        $r = db2_execute($stmt, $params);
        
        if ($r === false) {
            $rs = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            if (count($data) > 0) {
                $rs = true;
            }
        }
    }
    return $rs;
}