
<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$proc = $_POST['proc'];

$WANAME = $_POST['WANAME'];
$WAPKEY = $_POST['WAPKEY'];
$WAMAIL = $_POST['WAMAIL'];
$WAQGFLG = $_POST['WAQGFLG'];
$DATA = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$ArrD2HED = array();
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$WANAME = cmHscDe($WANAME);
$WAPKEY = cmHscDe($WAPKEY);
$WAMAIL = cmHscDe($WAMAIL);

$allWaut = array();
$DB2WAUL = array();
$D1NAMELIST=array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

// ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($WAQGFLG === '1'){
		$rs=cmGetQueryGroup($db2con,$WANAME,$WAPKEY,'');
		if($rs['result'] !== true){
			$rtn = 2;
			$msg = showMsg($rs['result'],array('クエリーグループ'));
		}else{
			$D1NAMELIST=umEx($rs['data'],true);
		}
	}
}
//クエリーグループ

// クエリー存在チェック
if($rtn === 0){
	if($WAQGFLG==='1'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,'',$WANAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 3;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
			if($rtn===0){
				if($res['LASTFLG']==='1'){
					$LASTD1NAME=$res['QRYGSQRY'];
				}
			}
        }//end of for loop
	}else{
	    $chkQry = array();
	    $chkQry = cmChkQuery($db2con,'',$WANAME,$WAPKEY);
	    if($chkQry['result'] === 'NOTEXIST_PIV'){
	        $rtn = 4;
	        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
	    }else if($chkQry['result'] !== true){
	        $rtn = 4;
	        $msg = showMsg($chkQry['result'],array('クエリー'));
	    }
	}
}

// 配信設定存在チェック処理
if($rtn === 0){
    $rs = fnCheckDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL);
    if($rs === 'NOTEXIST_GET'){
        $rtn = 3;
         $msg = showMsg($rs,array('配信設定'));
        //$msg = showMsg('NOTEXIST_GET',array('配信設定'))；
    }else if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('配信設定'));
        //$msg = showMsg('NOTEXIST_GET',array('配信設定'))；
    }    
}

if($proc === 'U'){
    if($rtn === 0){
        //チェック
        foreach($DATA as $key => $value){
            if($key === 'D2HED'){
                if($value === ""){
                    $rtn = 1;
                    $msg = showMsg('FAIL_REQ',array('項目'));//'バースト項目は入力必須です。';
                    $focus = 'D2HED';
                }else{
                    $exp = explode("-",$value);
                    $tmp = array(
                        'WABAFID' => ($exp[0] !== null)?$exp[0]:'0',
                        'WABAFLD' => ($exp[1] !== null)?$exp[1]:''
                    );
                    $ArrD2HED[] = $tmp;
                    $WABAFID = ($exp[0] !== null)?$exp[0]:'0';
                    $WABAFLD = ($exp[1] !== null)?$exp[1]:'';
                }
            }
        }
    }
    // 抽出範囲　に範囲設定場合
    if($DATA['FLG'] === '0'){
        if($rtn === 0){
            if( $DATA['WABAFR'] === ""){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array(array('開始','値範囲')));
                $focus = 'WABAFR';
            }
        }
        if($rtn === 0 ){
            if(!checkMaxLen($DATA['WABAFR'],256)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array(array('開始','値範囲')));
                $focus = 'WABAFR';
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($DATA['WABATO'],256)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array(array('終了','値範囲')));
                $focus = 'WABATO';
            }
        }
        if($rtn === 0){
            $rs = fnDeleteDB2WAUL($db2con,$WANAME,$WAPKEY,$WAMAIL);
            if($rs === false){
                   $rtn = 1;
                   $msg = showMsg('FAIL_INS');//'登録処理に失敗しました。管理者にお問い合わせください。';
            }
        }
        $WABINC = 0;
    }else if($DATA['FLG'] === '1'){
        $WABDATA = array_filter($DATA['WABDATA']);
        if(COUNT($WABDATA) === 0){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array(array('選択','データ')));
        }
        if($rtn === 0){
            foreach($WABDATA as $key => $value){
                if(!checkMaxLen($DATA['WABATO'],256)){
                    $rtn = 1;
                    $msg = showMsg('FAIL_MAXLEN',array(array('選択','データ')));
                    $focus = 'WABATO';
                    break;
                }
            }
        }
        if($rtn === 0){
            $WABINC = fnGetWABINC($db2con,$WANAME,$WAPKEY,$WABAFID,$WABAFLD,$WABDATA);
            if($WABINC === false){
                $rtn = 1;
                $msg = showMsg('FAIL_INS');//'登録処理に失敗しました。管理者にお問い合わせください。';
            }
        }
        if($rtn === 0){
            $rs = fnDeleteDB2WAUL($db2con,$WANAME,$WAPKEY,$WAMAIL);
            if($rs === false){
                   $rtn = 1;
                   //$msg = '登録処理に失敗しました。管理者にお問い合わせください。';
                   $msg = showMsg('FAIL_INS');//'登録処理に失敗しました。管理者にお問い合わせください。';
            }
        }
        if($rtn === 0){
            $DATA['WABAFR'] = '';
            $DATA['WABATO'] = '';
            $rs = fnInsertDB2WAUL($db2con,$WANAME,$WAPKEY,$WAMAIL,$WABDATA);
            if( $rs === false){
               $rtn = 1;
               $msg = showMsg('FAIL_INS');//'登録処理に失敗しました。管理者にお問い合わせください。';
            }
        }
    }
}else{
    if($rtn === 0){
        $WABAFID = '0';
        $WABAFLD = '';
        $DATA['FLG'] = '';
        $WABINC = 0;
        $rs = fnDeleteDB2WAUL($db2con,$WANAME,$WAPKEY,$WAMAIL);
        if($rs === false){
            $rtn = 1;
            $msg = showMsg('FAIL_INS');//'登録処理に失敗しました。管理者にお問い合わせください。';
        }
    }
}
if($rtn === 0){
    $rs = fnUpdateDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL,$WABAFID,$WABAFLD,$DATA['WABAFR'],$DATA['WABATO'],$DATA['FLG'],$WABINC);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg('FAIL_INS');//'登録処理に失敗しました。管理者にお問い合わせください。';
    }
}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/
/*if($rtn === 0){
    $WABDATA = array_filter($DATA['WABDATA']);
            $check = fnCheckDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL);

                         if($check === true){
                                        if($proc === 'U'){
                                                foreach($ArrD2HED as $key => $value){
                                                    $WABAFID = $value['WABAFID'];
                                                    $WABAFLD = $value['WABAFLD'];
                                                }
                                              if($DATA['FLG'] === '1'){
                                                    if(COUNT($WABDATA) === 0){
                                                            $rtn = 1;
                                                           $msg = '選択データは入力必須です。';
                                                    }
                                                    if($rtn === 0){
                                                            $WABINC = fnGetWABINC($db2con,$WANAME,$WAPKEY,$WABAFID,$WABAFLD,$WABDATA);
                                                            if($WAINC === false){
                                                                            $rtn = 1;
                                                                            $msg = '登録処理に失敗しました。管理者にお問い合わせください。';
                                                            }
                                                    }
                                             }
                                                if($rtn === 0){
                                                        $rs = fnDeleteDB2WAUL($db2con,$WANAME,$WAPKEY,$WAMAIL);
                                                        if($rs === false){
                                                               $rtn = 1;
                                                               $msg = '登録処理に失敗しました。管理者にお問い合わせください。';
                                                        }
                                                }
                                              if($DATA['FLG'] === '1' && $rtn === 0){
                                                            $DATA['WABAFR'] = '';
                                                            $DATA['WABATO'] = '';
                                                            $rs = fnInsertDB2WAUL($db2con,$WANAME,$WAPKEY,$WAMAIL,$WABDATA);
                                                            if( $rs === false){
                                                                   $rtn = 1;
                                                                   $msg = '登録処理に失敗しました。管理者にお問い合わせください。';
                                                            }else{
                                                                    $msg = '';
                                                            }
                                              }else{
                                                        $WABINC = 1;
                                                        $WABINC = 0;
                                               }

                                    }else{
                                        $WABAFID = '0';
                                        $WABAFLD = '';
                                        $DATA['FLG'] = '';
                                        $WABINC = 0;
                                        $rs = fnDeleteDB2WAUL($db2con,$WANAME,$WAPKEY,$WAMAIL);
                                        if($rs === false){
                                               $rtn = 1;
                                               $msg = '登録処理に失敗しました。管理者にお問い合わせください。';
                                        }
                                    }
                                    if($rtn === 0){
                                        $rs = fnUpdateDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL,$WABAFID,$WABAFLD,$DATA['WABAFR'],$DATA['WABATO'],$DATA['FLG'],$WABINC);
                                        if($rs === false){
                                            $rtn = 1;
                                            $msg = '登録処理に失敗しました。管理者にお問い合わせください。';
                                        }
                                }
                        }else{
                            $rtn = 1;
                           $msg = 'バースト項目対象配信設定が存在しません。';
                        }

}*/

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' =>$DATA,
    'DB2WAUL'=>$DB2WAUL,
    'WABINC' =>$WABINC
);

echo(json_encode($rtnAry));


/**
*-------------------------------------------------------*
* バースト項目設定
* 
* RESULT
*    01：更新終了の場合 true
*    02：エラーの場合   false
* 
* @param Object  $db2con  DBコネクション
* @param String  $WANAME  定義名
* @param String  $WAPKEY  PIVOTKEY
* @param String  $WABAFID 宛先メールアドレス
* @param String  $WABAFLD 定義名
* @param String  $WABAFR  PIVOTKEY
* @param String  $WABATO  宛先メールアドレス
*-------------------------------------------------------*
*/

function fnUpdateDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL,$WABAFID,$WABAFLD,$WABAFR,$WABATO,$WABFLG,$WABINC){

    $rs = true;

    $strSQL  = ' UPDATE DB2WAUT ';
    $strSQL .= ' SET ';
    $strSQL .= ' WABAFID = ?, ';
    $strSQL .= ' WABAFLD = ?, ';
    $strSQL .= ' WABAFR = ?, ';
    $strSQL .= ' WABATO = ? ,';
    $strSQL .= ' WABFLG = ? ,';
    $strSQL .= ' WABINC = ? ';
    $strSQL .= ' WHERE WANAME <> \'\' ';
    $strSQL .= ' AND WANAME = ? ';
    $strSQL .= ' AND WAPKEY = ? ';
    $strSQL .= ' AND WAMAIL = ? ';

    $params = array(
        $WABAFID,
        $WABAFLD,
        $WABAFR,
        $WABATO,
        $WABFLG,
        $WABINC,
        $WANAME,
        $WAPKEY,
        $WAMAIL
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $rs = db2_execute($stmt,$params);
        if($rs === false){
            $rs = 'FAIL_UPD';
        }
    }
    //var_dump($strSQL.print_r($params,true));
    return $rs;
}
/**
*-------------------------------------------------------*
* 配信設定存在チェック処理
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WANAME 定義名
* @param String  $WAPKEY PIVOTKEY
* @param String  $WAMAIL 宛先メールアドレス
*
*-------------------------------------------------------*
*/

function fnCheckDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.WANAME ';
    $strSQL .= ' FROM DB2WAUT AS A ' ;
    $strSQL .= ' WHERE A.WANAME = ? ' ;
    $strSQL .= ' AND A.WAPKEY = ? ' ;
    $strSQL .= ' AND A.WAMAIL = ? ' ;

    $params = array(
        $WANAME,
        $WAPKEY,
        $WAMAIL,
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }    
    return $rs;

}


function fnInsertDB2WAUL($db2con,$WLNAME,$WLPKEY,$WLMAIL,$WLDATA){
    $rs = true;
    $strSQL = ' INSERT INTO DB2WAUL (WLSEQL,WLNAME,WLPKEY,WLMAIL,WLDATA) ';
    $strSQL .= ' VALUES(?,?,?,?,?) ';
        foreach($WLDATA as $KEY =>$VALUE){
            if($VALUE !== ''){
                $params = array(
                     $KEY,
                    $WLNAME,
                    $WLPKEY,
                    $WLMAIL,
                    $VALUE
                );
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = false;
                }else{
                    $rs = db2_execute($stmt,$params);
                }
            }
        }
    return $rs;
}
function fnDeleteDB2WAUL($db2con,$WLNAME,$WLPKEY,$WLMAIL){

    $strSQL = ' DELETE FROM DB2WAUL WHERE WLNAME = ? AND WLPKEY = ? AND WLMAIL = ? ';
    $params = array(
        $WLNAME,
        $WLPKEY,
        $WLMAIL
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }
    if($rs !== false){
        $strSQL = ' UPDATE DB2WAUT SET WABINC = ? WHERE WANAME = ? AND WAPKEY = ? AND WAMAIL = ? ';
        $params = array(
            0,
            $WLNAME,
            $WLPKEY,
            $WLMAIL
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = false;
        }else{
            $rs = db2_execute($stmt,$params);
        }
    }

    return $rs;
}


//getting WABINC 
function fnGetWABINC($db2con,$WANAME,$WAPKEY,$WABAFID,$WABAFLD,$WABDATA){
    $userInput = array();
    //$WABDATA = array_filter($WABDATA);
    $data = array();
    $rs = false;

    $strSQL = ' SELECT WABINC,WABAFID,WABAFLD,WLDATA FROM( ';
    $strSQL .= ' SELECT WANAME,WAPKEY,WABAFID,WABAFLD,WABAFR,WABATO,WABFLG,WABINC,WLDATA FROM DB2WAUT AS A  ';
    $strSQL .= ' LEFT JOIN DB2WAUL AS B ON A.WANAME = B.WLNAME AND A.WAPKEY = B.WLPKEY AND A.WAMAIL = B.WLMAIL ';
    $strSQL .= ' WHERE WANAME = ? AND WAPKEY = ?   AND WABAFID = ? AND WABAFLD = ?';
    $strSQL .= ' GROUP BY WANAME,WAPKEY,WABAFID,WABAFLD,WABAFR,WABATO,WABFLG,WLNAME,WABINC,WLDATA) ';
    $strSQL .= ' AS A GROUP BY WABINC,WABAFID,WABAFLD,WLDATA  ORDER BY WABINC';

    $params = array(
        $WANAME,
        $WAPKEY,
        $WABAFID,
        $WABAFLD
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            
            //var_dump($logarr);
            $data = umEx($data);
            $result = array();
            foreach ($data as $value) {
                $id = $value['WABINC'];
                if (isset($result[$id])) {
                    $result[$id][] = $value;
                } else {
                    $result[$id] = array($value);
                }
            }
            $chk = 1;
            $WABINC = 1;
            $idx = 0;
            foreach($result as $key){
                $idx += 1;
                $arr = array();
                foreach($key as $value){
                    array_push($arr,$value['WLDATA']);
                }
                if($WABDATA == $arr){
                        $chk = 2;
                        $WABINC =$key[0]['WABINC'];
                        break;
                }
               if($chk === 2){
                        break;
                }
               if(count($result) === $idx && $chk !== 2){
                      $WABINC =((int)$key[0]['WABINC'])+1;
               }
            }
        }
        $rs = $WABINC;
    }
    return $rs;
}
