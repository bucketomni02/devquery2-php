<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$proc = $_POST['proc'];

$WSNAME = $_POST['WSNAME'];
$WSPKEY = $_POST['WSPKEY'];
$WSQGFLG = $_POST['WSQGFLG'];
$DATA = json_decode($_POST['DATA'],true);
$WSWDAY = $_POST['WSWDAY'];
$WSSPND = $_POST['WSSPND'];

$schWSNAME = $_POST['schWSNAME'];
$schWSPKEY = $_POST['schWSPKEY'];
$schWSFRQ  = $_POST['schWSFRQ'];
$schWSODAY = $_POST['schWSODAY'];
$schWSWDAY = $_POST['schWSWDAY'];
$schWSMDAY = $_POST['schWSMDAY'];
$schWSTIME = $_POST['schWSTIME'];
$schWSSPND = $_POST['schWSSPND'];
$SEVERLINK = $_POST['SEVERLINK'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$check = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$WSNAME = cmHscDe($WSNAME);
$WSPKEY1 = $WSPKEY;
$WSPKEY = cmHscDe($WSPKEY);
$WSPKEY2 = $WSPKEY;
$schWSNAME = cmHscDe($schWSNAME);
//$severlink = 

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
$WSPKEY3 = $WSPKEY;
if($rtn===0){
	if($WSQGFLG==='1'){
	    $rs=cmGetQueryGroup($db2con,$WSNAME,$WSPKEY,'');
	    if($rs['result'] !== true){
	        $rtn = 2;
	        $msg = showMsg($rs['result'],array('クエリーグループ'));
	    }else{
	        $D1NAMELIST=umEx($rs['data'],true);
	    }
	}
}
//クエリー存在チェック
if($rtn === 0){
	if($WSQGFLG==='1'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,'',$WSNAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 3;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }//end of for loop
	}else{
	    $chkQry = array();
	    $chkQry = cmChkQuery($db2con,'',$WSNAME,$WSPKEY);
	    if($chkQry['result'] === 'NOTEXIST_PIV'){
	        $rtn = 2;
	        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
	    }else if($chkQry['result'] !== true){
	        $rtn = 2;
	        $msg = showMsg($chkQry['result'],array('クエリー'));
	    }
	}
}
$WSPKEY4 = $WSPKEY;
if($rtn === 0){
    $myFlg = false; //自分のデータかどうかチェック
    //データ存在チェック
    if($proc === "UPD"){
        if($DATA['WSFRQ'] === '4'){
                $DATA['WSTIME'] = $DATA['WSIDAY'];
         }
        $WSODAY = str_replace('/','',cmStr0($DATA['WSODAY']));
        $WSMDAY = str_replace('/','',cmStr0($DATA['WSMDAY']));
        $WSTIME =ltrim(str_replace(':','',$DATA['WSTIME']),'0');
        if($WSTIME === ''){
            $WSTIME = '0';
        }
        $aaa = cmMer($schWSWDAY);

        $oldArr = array(array($schWSNAME,$schWSPKEY,$schWSFRQ,$schWSODAY,$schWSWDAY,$schWSMDAY,$schWSTIME));
        $oldArr = umEx($oldArr);
        $newArr = array(array($WSNAME,$WSPKEY,$DATA['WSFRQ'],$WSODAY,$WSWDAY,$WSMDAY,$WSTIME));
        $newArr = umEx($newArr);
        if($oldArr[0] === $newArr[0]){
            $myFlg = true;
        }
        $rsCount = fnCheckDB2WSCD($db2con,$schWSNAME,$schWSPKEY,$schWSFRQ,$schWSODAY,$schWSWDAY,$schWSMDAY,$schWSTIME,$schWSTIME);
        if($rsCount !== true){
            $rtn = 1;
            $msg = showMsg($rsCount,array('スケジュール'));
        }
    }
}
//チェック
if($DATA['WSFRQ'] === "1"){

    if($rtn === 0){
        if($DATA['WSODAY'] === ""){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('実行日付'));//'実行日付は入力必須です。';
            $focus = 'WSODAY';
        }
    }

    if($rtn === 0){
        if(!(checkDateVal($DATA['WSODAY']))){
            $rtn = 1;
            $msg = showMsg('FAIL_CMP',array('実行日付の日付'));//'実行日付の日付に誤りがあります。';
            $focus = 'WSODAY';
        }
    }

}

if($DATA['WSFRQ'] === "3"){
    if($rtn === 0){
        if($DATA['WSMDAY'] === ""){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('実行日'));//'実行日は入力必須です。';
            $focus = 'WSMDAY';
        }
    }
}

if($DATA['WSFRQ'] === "4"){
    if($rtn === 0){
        if($DATA['WSIDAY'] === ""){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('実行間隔'));//'実行間隔は入力必須です。';
            $focus = 'WSIDAY';
        }
    }

    if($rtn === 0){
        if(!(checkTimeVal($DATA['WSIDAY']))){
            $rtn = 1;
            $msg = showMsg('FAIL_CMP',array('実行時間'));//'実行間隔の時刻に誤りがあります。';
            $focus = 'WSIDAY';
        }
    }
}else{
    if($rtn === 0){
        if($WSWDAY === "0000000"){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('実行曜日'));//'実行曜日は入力必須です。';
            $focus = 'WSWDAY';
        }
    }

    if($rtn === 0){
        if(empty($DATA['WSTIME']) || $DATA['WSTIME'] === '' ){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('実行時間'));//'実行時刻は入力必須です。';
            $focus = 'WSTIME';
        }
    }

    if($rtn === 0){
        if(!(checkTimeVal($DATA['WSTIME']))){
            $rtn = 1;
            $msg = showMsg('FAIL_CMP',array('実行時間'));//'実行時刻の時刻に誤りがあります。';
            $focus = 'WSTIME';
        }
    }

}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/

if($rtn === 0){
    if($proc === "UPD"){
        e_log("Testing1");
        if($myFlg !== true){
            $rsCount = fnCheckDB2WSCD($db2con,$WSNAME,$WSPKEY,$DATA['WSFRQ'],$DATA['WSODAY'],$WSWDAY,$DATA['WSMDAY'],$DATA['WSTIME'],$DATA['WSIDAY']);
            if($rsCount === true){
                $rtn = 1;
                $msg = showMsg('CHK_DUP',array('スケジュール'));
            }else if($rsCount !== 'NOTEXIST_GET'){
                $rtn = 1;
                $msg = showMsg($rsCount,array('スケジュール'));
            }
        }
        if($rtn === 0){
            $rs = fnUpdateDB2WSCD($db2con,$DATA,$WSWDAY,$WSSPND,$WSNAME,$schWSNAME,$schWSPKEY,$schWSFRQ,$schWSODAY,$schWSWDAY,$schWSMDAY,$schWSTIME);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('スケジュール'));
            }
        }
    }else if($proc === "ADD"){
        e_log("Testing2");
        $rsCount = fnCheckDB2WSCD($db2con,$WSNAME,$WSPKEY,$DATA['WSFRQ'],$DATA['WSODAY'],$WSWDAY,$DATA['WSMDAY'],$DATA['WSTIME'],$DATA['WSIDAY']);
        if($rsCount === true){
            $rtn = 1;
            $msg = showMsg('CHK_DUP',array('スケジュール'));
        }else if($rsCount !== 'NOTEXIST_GET'){
            $rtn = 1;
            $msg = showMsg($rsCount,array('スケジュール'));
        }
        if($rtn === 0){
            $rs = fnInsertDB2WSCD($db2con,$DATA,$WSWDAY,$WSSPND,$WSNAME,$WSPKEY,$WSQGFLG);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('スケジュール'));
            }
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'PROC' => $proc,
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => $DATA,
    'CHECK' => $check,
    'CHECKARRAY' => $myFlg,
    'newArr' =>$newArr,
    'oldArr' =>$oldArr,
     'FOCUS' => $focus,
    'WSPKEY1' =>$WSPKEY1,
    'WSPKEY2' =>$WSPKEY2,
    'WSPKEY3' =>$WSPKEY3,
    'WSPKEY4' =>$WSPKEY4
);

echo(json_encode($rtnAry));

/**
* スケジュール追加
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param Array   $DATA   フォームデータ
* @param String  $WSWDAY スケジュール-週
* @param String  $WSSPND サスペンド
* @param String  $WSNAME 定義名
* @param String  $WSPKEY PIVOTKEY
* @param String  $WSQGFLG クエリーとクエリーグループを分けるFLG
* 
*/

function fnInsertDB2WSCD($db2con,$DATA,$WSWDAY,$WSSPND,$WSNAME,$WSPKEY,$WSQGFLG){

    $rs = true;

    $params = array();
    //構文

    $strSQL  = ' INSERT INTO DB2WSCD ';
    $strSQL .= ' (WSNAME,WSPKEY,WSFRQ,WSODAY,WSWDAY,WSMDAY,WSTIME,WSSPND,WSQGFLG) ';
    $strSQL .= ' VALUES (?,?,?,?,?,?,?,?,?)';

    array_push($params,$WSNAME);
    array_push($params,$WSPKEY);
    array_push($params,$DATA['WSFRQ']);

    switch($DATA['WSFRQ']){
        case '1';
            array_push($params,str_replace('/','',$DATA['WSODAY']));
            array_push($params,'');
            array_push($params,'0');
            break;
        case '2';
            array_push($params,'0');
            array_push($params,$WSWDAY);
            array_push($params,'0');
            break;
        case '3';
            array_push($params,'0');
            array_push($params,'');
            array_push($params,$DATA['WSMDAY']);
            break;
        case '4';
            array_push($params,'0');
            array_push($params,'');
            array_push($params,'0');
            $DATA['WSTIME'] = $DATA['WSIDAY'];
            break;
    }

    array_push($params,str_replace(':','',$DATA['WSTIME']));
    array_push($params,$WSSPND);
	array_push($params,$WSQGFLG);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs === 'FAIL_INS';
        }
    }
    return $rs;

}

/**
* スケジュール更新
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con 　　DBコネクション
* @param Array   $DATA   　　フォームデータ
* @param String  $WSWDAY 　　スケジュール-週
* @param String  $WSSPND 　　サスペンド
* @param String  $WSNAME 　　定義名
* @param Array   $schWSNAME　定義名
* @param String  $schWSPKEY　PIVOTKEY
* @param String  $schWSFRQ   実行頻度　
* @param String  $schWSODAY  スケジュール-日
* @param String  $schWSWDAY  スケジュール-週
* @param String  $schWSMDAY  スケジュール-週
* @param String  $schWSTIME  実行時分
* 
*/

function fnUpdateDB2WSCD($db2con,$DATA,$WSWDAY,$WSSPND,$WSNAME,$schWSNAME,$schWSPKEY,$schWSFRQ,$schWSODAY,$schWSWDAY,$schWSMDAY,$schWSTIME){

    $rs = true;

    $params = array();

    //構文
    $strSQL  = ' UPDATE DB2WSCD ';
    $strSQL .= ' SET ';
    $strSQL .= ' WSFRQ = ?, ';
    $strSQL .= ' WSBDAY = ?, ';
    $strSQL .= ' WSBTIM = ?, ';
    $strSQL .= ' WSSDAY = ?, ';
    $strSQL .= ' WSSTIM = ?, ';
    array_push($params,$DATA['WSFRQ'],0,0,0,0);

    $strSQL .= ' WSODAY = ?, ';
    $strSQL .= ' WSWDAY = ?, ';
    $strSQL .= ' WSMDAY = ?, ';

    switch($DATA['WSFRQ']){
        case '1';
            $WSWDAY = 0;
            $DATA['WSMDAY'] = 0;
            array_push($params,str_replace('/','',$DATA['WSODAY']));
            array_push($params,'');
            array_push($params,'0');
            break;
        case '2';
            $DATA['WSODAY'] = 0;
            $DATA['WSMDAY'] = 0;
            array_push($params,'0');
            array_push($params,$WSWDAY);
            array_push($params,'0');
            break;
        case '3';
            $DATA['WSODAY'] = 0;
            $WSWDAY = 0;
            array_push($params,'0');
            array_push($params,'');
            array_push($params,$DATA['WSMDAY']);
            break;
        case '4';
            $DATA['WSODAY'] = 0;
            $WSWDAY = 0;
            $DATA['WSMDAY'] = 0;
            array_push($params,'0');
            array_push($params,'');
            array_push($params,'0');
            $DATA['WSTIME'] = $DATA['WSIDAY'];
            break;
    }

    $strSQL .= ' WSTIME = ?, ';
    array_push($params,str_replace(':','',$DATA['WSTIME']));
    $strSQL .= ' WSSPND = ? ';
    array_push($params,$WSSPND);

    $strSQL .= ' WHERE WSNAME <> \'\' ';
    $strSQL .= ' AND WSNAME = ? ';
    $strSQL .= ' AND WSPKEY = ? ';
    $strSQL .= ' AND WSFRQ = ? ';
    $strSQL .= ' AND WSODAY = ? ';
    $strSQL .= ' AND WSWDAY = ? ';
    $strSQL .= ' AND WSMDAY = ? ';
    $strSQL .= ' AND WSTIME = ? ';

    array_push($params,$schWSNAME);
    array_push($params,$schWSPKEY);
    array_push($params,$schWSFRQ);
    array_push($params,$schWSODAY);
    array_push($params,$schWSWDAY);
    array_push($params,$schWSMDAY);
    array_push($params,$schWSTIME);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}

/**
* スケジュール存在チェック いたらtrue
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WSNAME 定義名
* @param String  $WSPKEY PIVOTKEY
* @param String  $WSFRQ  実行頻度
* @param Array   $WSODAY スケジュール-日
* @param String  $WSWDAY スケジュール-週
* @param String  $WSMDAY スケジュール-月
* @param String  $WSTIME 実行時分
* 
*/

function fnCheckDB2WSCD($db2con,$WSNAME,$WSPKEY,$WSFRQ,$WSODAY,$WSWDAY,$WSMDAY,$WSTIME,$WSIDAY){

    $data = array();
    $rs = true;
    $params = array(
        $WSNAME,
        $WSPKEY,
        $WSFRQ);
    switch($WSFRQ){
        case '1';
            $WSWDAY = 0;
            $WSMDAY = 0;
            $WSODAY = str_replace('/','',cmStr0($WSODAY));
            array_push($params,$WSODAY);
            array_push($params,'');
            array_push($params,'0');
            break;
        case '2';
            $WSODAY = 0;
            $WSMDAY = 0;
            array_push($params,'0');
            array_push($params,$WSWDAY);
            array_push($params,'0');
            break;
        case '3';
            $WSODAY = 0;
            $WSWDAY = 0;
             $WSMDAY = str_replace('/','',cmStr0($WSMDAY));
            array_push($params,'0');
            array_push($params,'');
            array_push($params,$WSMDAY);
            break;
        case '4';
            $WSODAY = 0;
            $WSWDAY = 0;
            $WSMDAY = 0;
            $WSTIME = $WSIDAY;
            array_push($params,'0');
            array_push($params,'');
            array_push($params,'0');
            break;
    }

    $strSQL  = ' SELECT A.WSNAME ';
    $strSQL .= ' FROM DB2WSCD AS A ' ;
    $strSQL .= ' WHERE A.WSNAME = ? ' ;
    $strSQL .= ' AND A.WSPKEY = ? ' ;
    $strSQL .= ' AND A.WSFRQ = ? ' ;
    $strSQL .= ' AND A.WSODAY = ? ' ;
    $strSQL .= ' AND A.WSWDAY = ? ' ;
    $strSQL .= ' AND A.WSMDAY = ? ' ;
    $strSQL .= ' AND A.WSTIME = ? ' ;
   array_push($params , str_replace(':','',$WSTIME));
 /*   $params = array(
        $WSNAME,
        $WSPKEY,
        $WSFRQ,
        str_replace('/','',cmStr0($WSODAY)),
        $WSWDAY,
        str_replace('/','',cmStr0($WSMDAY)),
        str_replace(':','',$WSTIME)
    );*/
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;

}