<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$proc = $_POST['proc'];

$WSNAME = $_POST['WSNAME'];
$WSPKEY = $_POST['WSPKEY'];
$DATA = json_decode($_POST['DATA'],true);
$WSWDAY = $_POST['WSWDAY'];
$WSSPND = $_POST['WSSPND'];

$schWSNAME = $_POST['schWSNAME'];
$schWSPKEY = $_POST['schWSPKEY'];
$schWSFRQ  = $_POST['schWSFRQ'];
$schWSODAY = $_POST['schWSODAY'];
$schWSWDAY = $_POST['schWSWDAY'];
$schWSMDAY = $_POST['schWSMDAY'];
$schWSTIME = $_POST['schWSTIME'];
$schWSSPND = $_POST['schWSSPND'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$check = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$WSNAME = cmHscDe($WSNAME);
$WSPKEY = cmHscDe($WSPKEY);
$schWSNAME = cmHscDe($schWSNAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
//クエリー存在チェック
    if($rtn === 0){
        $chkQry = array();
        $chkQry = cmChkQuery($db2con,'',$WSNAME,$WSPKEY);
        if($chkQry['result'] === 'NOTEXIST_PIV'){
            $rtn = 2;
            $msg = showMsg('NOTEXIST_GET',array('ピボット'));
        }else if($chkQry['result'] !== true){
            $rtn = 2;
            $msg = showMsg($chkQry['result'],array('クエリー'));
        }
    }
        //データ存在チェック
           if($proc === "UPD"){
                   $rsCount = fnCheckDB2WSCD($db2con,$schWSNAME,$schWSPKEY,$schWSFRQ,$schWSODAY,$schWSWDAY,$schWSMDAY,$schWSTIME,$schWSTIME);
                    if($rsCount === false){
                        $rtn = 1;
                        $msg = 'この配信設定が存在しません。';
                    }
            }
//チェック
if($DATA['WSFRQ'] === "1"){

    if($rtn === 0){
        if($DATA['WSODAY'] === ""){
            $rtn = 1;
            $msg = '実行日付は入力必須です。';
            $focus = 'WSODAY';
        }
    }

    if($rtn === 0){
        if(!(checkDateVal($DATA['WSODAY']))){
            $rtn = 1;
            $msg = '実行日付の日付に誤りがあります。';
            $focus = 'WSODAY';
        }
    }

}

if($DATA['WSFRQ'] === "3"){
    if($rtn === 0){
        if($DATA['WSMDAY'] === ""){
            $rtn = 1;
            $msg = '実行日は入力必須です。';
            $focus = 'WSMDAY';
        }
    }
}

if($DATA['WSFRQ'] === "4"){
    if($rtn === 0){
        if($DATA['WSIDAY'] === ""){
            $rtn = 1;
            $msg = '実行間隔は入力必須です。';
            $focus = 'WSIDAY';
        }
    }

    if($rtn === 0){
        if(!(checkTimeVal($DATA['WSIDAY']))){
            $rtn = 1;
            $msg = '実行間隔の時刻に誤りがあります。';
            $focus = 'WSIDAY';
        }
    }
}else{
    if($rtn === 0){
        if($WSWDAY === "0000000"){
            $rtn = 1;
            $msg = '実行曜日は入力必須です。';
            $focus = 'WSWDAY';
        }
    }

    if($rtn === 0){
        if($DATA['WSTIME'] === ""){
            $rtn = 1;
            $msg = '実行時刻は入力必須です。';
            $focus = 'WSTIME';
        }
    }

    if($rtn === 0){
        if(!(checkTimeVal($DATA['WSTIME']))){
            $rtn = 1;
            $msg = '実行時刻の時刻に誤りがあります。';
            $focus = 'WSTIME';
        }
    }

}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/

if($rtn === 0){
           if($proc === "UPD"){
                        if($DATA['WSFRQ'] === '4'){
                                $DATA['WSTIME'] = $DATA['WSIDAY'];
                         }
                         $WSODAY = str_replace('/','',cmStr0($DATA['WSODAY']));
                        $WSMDAY = str_replace('/','',cmStr0($DATA['WSMDAY']));
                        $WSTIME =str_replace(':','',$DATA['WSTIME']);
                        $aaa = cmMer($schWSWDAY);

                        $oldArr = array(array($schWSNAME,$schWSPKEY,$schWSFRQ,$schWSODAY,cmMer($schWSWDAY),$schWSMDAY,$schWSTIME));
                        $oldArr = umEx($oldArr);
                        $newArr = array(array($WSNAME,$WSPKEY,$DATA['WSFRQ'],$WSODAY,$WSWDAY,$WSMDAY,$WSTIME));
                        $newArr = umEx($newArr);
/*
                         $oldArr = array(cmMer($schWSNAME),cmMer($schWSPKEY),cmMer($schWSFRQ),cmMer($schWSODAY),$schWSWDAY,cmMer($schWSMDAY),cmMer($schWSTIME));
                         $newArr = array($WSNAME,$WSPKEY,$DATA['WSFRQ'],$WSODAY,$WSWDAY,$WSMDAY,$WSTIME);
*/                        if($schWSNAME !== $WSNAME && $schWSPKEY !== $WSPKEY && $schWSFRQ !== $DATA['WSFRQ'] 
                            && $schWSODAY !== $DATA['WSODAY'] && $schWSWDAY !== $WSWDAY && $schWSMDAY !== $DATA['WSMDAY'] && $schWSTIME !== $DATA['WSIDAY']){

                                
                                    $rsCount = fnCheckDB2WSCD($db2con,$WSNAME,$WSPKEY,$DATA['WSFRQ'],$DATA['WSODAY'],$WSWDAY,$DATA['WSMDAY'],$DATA['WSTIME'],$DATA['WSIDAY']);
                                    if($rsCount === true){
                                        $rtn = 1;
                                        $msg = '登録するデータが重複しています。';
                                    }
                           }
                    if($rtn !== 1){
                           $rs = fnUpdateDB2WSCD($db2con,$DATA,$WSWDAY,$WSSPND,$WSNAME,$schWSNAME,$schWSPKEY,$schWSFRQ,$schWSODAY,$schWSWDAY,$schWSMDAY,$schWSTIME);
                    }
        }else if($proc === "ADD"){
                $rsCount = fnCheckDB2WSCD($db2con,$WSNAME,$WSPKEY,$DATA['WSFRQ'],$DATA['WSODAY'],$WSWDAY,$DATA['WSMDAY'],$DATA['WSTIME'],$DATA['WSIDAY']);
                if($rsCount === false){
                        $rs = fnInsertDB2WSCD($db2con,$DATA,$WSWDAY,$WSSPND,$WSNAME,$WSPKEY);
                }else{
                        $rtn = 1;
                        $msg = '登録するデータが重複しています。';
                }
        }

        if($rs === false){
            $rtn = 1;
            $msg = '登録処理に失敗しました。管理者にお問い合わせください。';
        }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'PROC' => $proc,
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => $DATA,
    'CHECK' => $check,
    'CHECKARRAY' => $aaa,
    'newArr' =>$newArr,
    'oldArr' =>$oldArr,
    'newArr1' =>$newArr[0],
    'oldArr1' =>$oldArr[0],
    'FOCUS' => $focus
);

echo(json_encode($rtnAry));

/**
* スケジュール追加
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param Array   $DATA   フォームデータ
* @param String  $WSWDAY スケジュール-週
* @param String  $WSSPND サスペンド
* @param String  $WSNAME 定義名
* @param String  $WSPKEY PIVOTKEY
* 
*/

function fnInsertDB2WSCD($db2con,$DATA,$WSWDAY,$WSSPND,$WSNAME,$WSPKEY){

    $rs = true;

    $params = array();
    //構文

    $strSQL  = ' INSERT INTO DB2WSCD ';
    $strSQL .= ' (WSNAME,WSPKEY,WSFRQ,WSODAY,WSWDAY,WSMDAY,WSTIME,WSSPND) ';
    $strSQL .= ' VALUES (?,?,?,?,?,?,?,?)';

    array_push($params,$WSNAME);
    array_push($params,$WSPKEY);
    array_push($params,$DATA['WSFRQ']);

    switch($DATA['WSFRQ']){
        case '1';
            array_push($params,str_replace('/','',$DATA['WSODAY']));
            array_push($params,'');
            array_push($params,'0');
            break;
        case '2';
            array_push($params,'0');
            array_push($params,$WSWDAY);
            array_push($params,'0');
            break;
        case '3';
            array_push($params,'0');
            array_push($params,'');
            array_push($params,$DATA['WSMDAY']);
            break;
        case '4';
            array_push($params,'0');
            array_push($params,'');
            array_push($params,'0');
            $DATA['WSTIME'] = $DATA['WSIDAY'];
            break;
    }

    array_push($params,str_replace(':','',$DATA['WSTIME']));
    array_push($params,$WSSPND);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }
    return $rs;

}

/**
* スケジュール更新
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con 　　DBコネクション
* @param Array   $DATA   　　フォームデータ
* @param String  $WSWDAY 　　スケジュール-週
* @param String  $WSSPND 　　サスペンド
* @param String  $WSNAME 　　定義名
* @param Array   $schWSNAME　定義名
* @param String  $schWSPKEY　PIVOTKEY
* @param String  $schWSFRQ   実行頻度　
* @param String  $schWSODAY  スケジュール-日
* @param String  $schWSWDAY  スケジュール-週
* @param String  $schWSMDAY  スケジュール-週
* @param String  $schWSTIME  実行時分
* 
*/

function fnUpdateDB2WSCD($db2con,$DATA,$WSWDAY,$WSSPND,$WSNAME,$schWSNAME,$schWSPKEY,$schWSFRQ,$schWSODAY,$schWSWDAY,$schWSMDAY,$schWSTIME){

    $rs = true;

    $params = array();

    //構文
    $strSQL  = ' UPDATE DB2WSCD ';
    $strSQL .= ' SET ';
    $strSQL .= ' WSFRQ = ?, ';

    array_push($params,$DATA['WSFRQ']);

    $strSQL .= ' WSODAY = ?, ';
    $strSQL .= ' WSWDAY = ?, ';
    $strSQL .= ' WSMDAY = ?, ';

    switch($DATA['WSFRQ']){
        case '1';
            $WSWDAY = 0;
            $DATA['WSMDAY'] = 0;
            array_push($params,str_replace('/','',$DATA['WSODAY']));
            array_push($params,'');
            array_push($params,'0');
            break;
        case '2';
            $DATA['WSODAY'] = 0;
            $DATA['WSMDAY'] = 0;
            array_push($params,'0');
            array_push($params,$WSWDAY);
            array_push($params,'0');
            break;
        case '3';
            $DATA['WSODAY'] = 0;
            $WSWDAY = 0;
            array_push($params,'0');
            array_push($params,'');
            array_push($params,$DATA['WSMDAY']);
            break;
        case '4';
            $DATA['WSODAY'] = 0;
            $WSWDAY = 0;
            $DATA['WSMDAY'] = 0;
            array_push($params,'0');
            array_push($params,'');
            array_push($params,'0');
            $DATA['WSTIME'] = $DATA['WSIDAY'];
            break;
    }

    $strSQL .= ' WSTIME = ?, ';
    array_push($params,str_replace(':','',$DATA['WSTIME']));
    $strSQL .= ' WSSPND = ? ';
    array_push($params,$WSSPND);

    $strSQL .= ' WHERE WSNAME <> \'\' ';
    $strSQL .= ' AND WSNAME = ? ';
    $strSQL .= ' AND WSPKEY = ? ';
    $strSQL .= ' AND WSFRQ = ? ';
    $strSQL .= ' AND WSODAY = ? ';
    $strSQL .= ' AND WSWDAY = ? ';
    $strSQL .= ' AND WSMDAY = ? ';
    $strSQL .= ' AND WSTIME = ? ';

    array_push($params,$schWSNAME);
    array_push($params,$schWSPKEY);
    array_push($params,$schWSFRQ);
    array_push($params,$schWSODAY);
    array_push($params,$schWSWDAY);
    array_push($params,$schWSMDAY);
    array_push($params,$schWSTIME);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }
    return $rs;
}

/**
* スケジュール存在チェック いたらtrue
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WSNAME 定義名
* @param String  $WSPKEY PIVOTKEY
* @param String  $WSFRQ  実行頻度
* @param Array   $WSODAY スケジュール-日
* @param String  $WSWDAY スケジュール-週
* @param String  $WSMDAY スケジュール-月
* @param String  $WSTIME 実行時分
* 
*/

function fnCheckDB2WSCD($db2con,$WSNAME,$WSPKEY,$WSFRQ,$WSODAY,$WSWDAY,$WSMDAY,$WSTIME,$WSIDAY){

    $data = array();
    $rs = false;
    $params = array(
        $WSNAME,
        $WSPKEY,
        $WSFRQ);
    switch($WSFRQ){
        case '1';
            $WSWDAY = 0;
            $WSMDAY = 0;
            $WSODAY = str_replace('/','',cmStr0($WSODAY));
            array_push($params,$WSODAY);
            array_push($params,'');
            array_push($params,'0');
            break;
        case '2';
            $WSODAY = 0;
            $WSMDAY = 0;
            array_push($params,'0');
            array_push($params,$WSWDAY);
            array_push($params,'0');
            break;
        case '3';
            $WSODAY = 0;
            $WSWDAY = 0;
             $WSMDAY = str_replace('/','',cmStr0($WSMDAY));
            array_push($params,'0');
            array_push($params,'');
            array_push($params,$WSMDAY);
            break;
        case '4';
            $WSODAY = 0;
            $WSWDAY = 0;
            $WSMDAY = 0;
            $WSTIME = $WSIDAY;
            array_push($params,'0');
            array_push($params,'');
            array_push($params,'0');
            break;
    }

    $strSQL  = ' SELECT A.WSNAME ';
    $strSQL .= ' FROM DB2WSCD AS A ' ;
    $strSQL .= ' WHERE A.WSNAME = ? ' ;
    $strSQL .= ' AND A.WSPKEY = ? ' ;
    $strSQL .= ' AND A.WSFRQ = ? ' ;
    $strSQL .= ' AND A.WSODAY = ? ' ;
    $strSQL .= ' AND A.WSWDAY = ? ' ;
    $strSQL .= ' AND A.WSMDAY = ? ' ;
    $strSQL .= ' AND A.WSTIME = ? ' ;
   array_push($params , str_replace(':','',$WSTIME));
 /*   $params = array(
        $WSNAME,
        $WSPKEY,
        $WSFRQ,
        str_replace('/','',cmStr0($WSODAY)),
        $WSWDAY,
        str_replace('/','',cmStr0($WSMDAY)),
        str_replace(':','',$WSTIME)
    );*/
        error_log("edt schedule : ".$strSQL);
        error_log("edt schedule param: ".print_r($params,true));

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = true;
            }
        }
    }
    return $rs;

}