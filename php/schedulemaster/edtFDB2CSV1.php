<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$DATA = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
//バリデーションチェック
if($rtn === 0){
    $byte = checkByte($DATA['D1CMD']);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('実行前CMD'));//'実行前CMDは半角文字で入力してください。';
        $focus = 'D1CMD';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['D1CMD'],500)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('実行前CMD'));//'実行前CMDが入力桁数を超えています。';
        $focus = 'D1CMD';
    }
}

//バリデーションチェック
if($rtn === 0){
    $byte = checkByte($DATA['D1CMDE']);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('実行後CMD'));//'実行後CMDは半角文字で入力してください。';
        $focus = 'D1CMDE';
    }
}
if($rtn === 0){
    if(!checkMaxLen($DATA['D1CMDE'],500)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('実行後CMD'));//'実行後CMDが入力桁数を超えています。';
        $focus = 'D1CMDE';
    }
}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/


if($rtn === 0){
    $rs = fnUpdateFDB2CSV1($db2con,$DATA);
    if($rs === false){
        $rtn = 1;
        $msg = showMsg('FAIL_UPD');//'更新処理に失敗しました。管理者にお問い合わせください。';
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => $DATA
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ヘルプ情報更新
*-------------------------------------------------------*
*/

function fnUpdateFDB2CSV1($db2con,$DATA){

    $rs = true;

    //構文

    $strSQL  = ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= ' D1CMD = ?, ';
    $strSQL .= ' D1CMDE = ? ';
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array(
        $DATA['D1CMD'],
        $DATA['D1CMDE'],
        $DATA['D1NAME']
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $rs = db2_execute($stmt,$params);
    }

    return $rs;

}