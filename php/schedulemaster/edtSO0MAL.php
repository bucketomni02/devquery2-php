<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
include_once ("../licenseInfo.php");
/*
 *-------------------------------------------------------*
 * リクエスト
 *-------------------------------------------------------*
*/
$D1NAME = (isset($_POST['D1NAME']) ? $_POST['D1NAME'] : '');
$SOPKEY = (isset($_POST['PMPKEY']) ? $_POST['PMPKEY'] : '');
$D1QGFLG = (isset($_POST['D1QGFLG']) ? $_POST['D1QGFLG'] : '');
$DATA = json_decode($_POST['DATA'], true);
$SODIEX = '';
if (isset($DATA['SODIEX'])) {
    $SODIEX = $DATA['SODIEX'];
    $SODIEX = str_replace(' ', '', $SODIEX);
}
/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$check = '';
/*
 *-------------------------------------------------------*
 * チェック処理
 *-------------------------------------------------------*
*/
$D1NAME = cmHscDe($D1NAME);
$SOPKEY = cmHscDe($SOPKEY);
$SODIEX = cmHscDe($SODIEX);
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    }
}
//クエリーグループ
if($rtn===0){
	if($D1QGFLG==='1'){
		$rs=cmGetQueryGroup($db2con,$D1NAME,'','');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリーグループ'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
        }
	}
}
//クエリーグループ
/*
 *-------------------------------------------------------*
 * 更新処理
 *-------------------------------------------------------*
*/
//Excelの出力先validation
//ライセンスがない場合
if ($rtn === 0) {
    if (cmMer($SODIEX) !== '') {
        if ($licenseIFS !== true) {
            $rtn = 1;
            $msg = showMsg('FAIL_NUSE', array('EXCEL出力先'));
        }
    }
}
//入力桁数を超えている場合
if ($rtn === 0) {
    if (!checkMaxLen(cmMer($SODIEX), 256)) {
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN', array('EXCEL出力先')); //'出力先が入力桁数を超えています。';
        $focus = 'SODIEX';
    }
}
if ($rtn === 0) {
    if (substr(cmMer($SODIEX), -1) === '/') {
        $rtn = 1;
        $msg = showMsg('CHK_FMT', array('EXCEL出力先', 'EXCELファイル名')); // EXCEL出力先にEXCELファイル名を入力してください。
        $focus = 'SODIEX';
    }
}
if ($rtn === 0) {
    $data = fnGetNAME($db2con, $D1NAME, $SOPKEY,$D1QGFLG);
    if (count($data) === 0) {
        $rs = fnInsertSO0MAL($db2con, $D1NAME, $SOPKEY, cmStr0($DATA['SO0MAL']), $SODIEX, cmStr0($DATA['SOCTFL']),$D1QGFLG);
        if ($rs === false) {
            $rtn = 1;
            $msg = showMsg('FAIL_INS'); //'登録処理に失敗しました。管理者にお問い合わせください。';
            
        }
    } else if (count($data) > 0) {
        $rs = fnUpdateSO0MAL($db2con, $D1NAME, $SOPKEY, cmStr0($DATA['SO0MAL']), $SODIEX, cmStr0($DATA['SOCTFL']),$D1QGFLG);
        if ($rs === false) {
            $rtn = 1;
            $msg = showMsg('FAIL_INS'); // '登録処理に失敗しました。管理者にお問い合わせください。';
            
        }
    }
}
//スケジュールの出力ファイルを更新
if ($rtn === 0) {
    $r = fnUpdSoutput($db2con, $D1NAME, cmMer($DATA['D1OUTSE']), cmMer($DATA['D1OUTSR']),cmMer($DATA['D1OUTSA']),$D1QGFLG);
    if ($r !== true) {
        $rtn = 1;
        $msg = showMsg($r);
    }
}
cmDb2Close($db2con);
/**return**/
$rtnAry = array('PROC' => $proc, 'RTN' => $rtn, 'MSG' => $msg, 'DATA' => $DATA);
echo (json_encode($rtnAry));
/*
 *-------------------------------------------------------*
 * もう登録できたかどうかチェック
 *-------------------------------------------------------*
*/
function fnGetNAME($db2con, $SONAME, $SOPKEY,$D1QGFLG) {
    $data = array();
    $strSQL = ' SELECT A.SONAME ';
    $strSQL.= ' FROM DB2WSOC  AS A ';
    $strSQL.= ' WHERE SONAME = ? AND SOPKEY = ? ';
    $params = array($SONAME, $SOPKEY);

	if($D1QGFLG==='1'){
		$strSQL.= ' AND SOQGFLG=? ';
		array_push($params,$D1QGFLG);
	}else{
		$strSQL.= ' AND (SOQGFLG=\'0\' OR SOQGFLG=\'\') ';

	}
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * 登録
 *-------------------------------------------------------*
*/
function fnInsertSO0MAL($db2con, $D1NAME, $SOPKEY, $SO0MAL, $SODIEX, $SOCTFL,$D1QGFLG) {
    $rs = true;
    //構文
    $strSQL = ' INSERT INTO DB2WSOC(SONAME,SOPKEY,SO0MAL,SODIEX,SOCTFL,SOQGFLG) ';
    $strSQL.= ' VALUES ';
    $strSQL.= ' (?,?,?,?,?,?)';
    $params = array($D1NAME, $SOPKEY, $SO0MAL, $SODIEX, $SOCTFL,$D1QGFLG);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = false;
    } else {
        $rs = db2_execute($stmt, $params);
    }
    return $rs;
}
/*
 *-------------------------------------------------------*
 * 保管日数更新
 *-------------------------------------------------------*
*/
function fnUpdateSO0MAL($db2con, $SONAME, $SOPKEY, $SO0MAL, $SODIEX, $SOCTFL,$D1QGFLG) {
    $rs = true;
    //構文
    $strSQL = ' UPDATE DB2WSOC ';
    $strSQL.= ' SET ';
    $strSQL.= ' 	SO0MAL = ?, ';
    $strSQL.= ' 	SODIEX = ?, ';
    $strSQL.= ' 	SOCTFL = ? ';
    $strSQL.= ' WHERE SONAME <> \'\' ';
    $strSQL.= ' AND SONAME = ? ';
    $strSQL.= ' AND SOPKEY = ? ';
	if($D1QGFLG==='1'){
	    $strSQL.= ' AND SOQGFLG =\'1\' ';
	}else{
		$strSQL .='AND (SOQGFLG=\'0\' OR SOQGFLG=\'\' )';
	}
    $params = array($SO0MAL, $SODIEX, $SOCTFL,$SONAME, $SOPKEY);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = false;
    } else {
        $rs = db2_execute($stmt, $params);
    }
    return $rs;
}
/*
 *-------------------------------------------------------*
 *  FDB2CSV1をスケジュールの出力ファイルについて更新
 *-------------------------------------------------------*
*/
function fnUpdSoutput($db2con, $D1NAME, $D1OUTSE, $D1OUTSR, $D1OUTSA,$D1QGFLG) {
    $rs = true;
	if($D1QGFLG==='1'){
	    $strSQL = ' UPDATE DB2WSOC ';
	    $strSQL.= ' SET ';
	    $strSQL.= ' SOOUTSE = ?, ';
	    $strSQL.= ' SOOUTSR = ?, ';
	    $strSQL.= ' SOOUTSA = ?  ';
	    $strSQL.= ' WHERE SONAME <> \'\' ';
	    $strSQL.= ' AND SONAME = ? ';
	    $strSQL.= ' AND SOQGFLG = \'1\' ';
	}else{
	    $strSQL = ' UPDATE FDB2CSV1 ';
	    $strSQL.= ' SET ';
	    $strSQL.= ' D1OUTSE = ?, ';
	    $strSQL.= ' D1OUTSR = ?, ';
	    $strSQL.= ' D1OUTSA = ?  ';
	    $strSQL.= ' WHERE D1NAME <> \'\' ';
	    $strSQL.= ' AND D1NAME = ? ';
	}

    $params = array($D1OUTSE, $D1OUTSR, $D1OUTSA, $D1NAME);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rs = 'FAIL_UPD';
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}
