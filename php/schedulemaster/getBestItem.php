<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : スケジュールマスタ>配信設定 > バスト項目設定
* PROGRAM NAME   : 項目リスト取得処理
* PROGRAM ID     : getBestItem.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/02/24
* MODIFY DATE    : 
* ============================================================
**/

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D2NAME = $_POST['D2NAME'];
$WAQGFLG = $_POST['WAQGFLG'];
$PMPKEY = $_POST['PMPKEY'];
$WAMAIL = $_POST['WAMAIL'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$userData=array();
$D1NAMELIST=array();
$LASTD1NAME='';
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$WAMAIL = cmHscDe($WAMAIL);
$D2NAME = cmHscDe($D2NAME);
$PMPKEY = cmHscDe($PMPKEY);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
		$userData  = umEx($rs['data']);
	}
}
//クエリーグループ
if($rtn===0){
	if($WAQGFLG === '1'){
		$rs=cmGetQueryGroup($db2con,$D2NAME,$PMPKEY,'');
		if($rs['result'] !== true){
			$rtn = 2;
			$msg = showMsg($rs['result'],array('クエリーグループ'));
		}else{
			$D1NAMELIST=umEx($rs['data'],true);
		}
	}
}
//クエリーグループ
if($rtn === 0){
	if($WAQGFLG==='1'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,'',$D2NAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 3;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
			e_log('Hello $key=>'.$key.print_r($res,true));
			if($res['LASTFLG']==='1'){
				$LASTD1NAME=$res['QRYGSQRY'];
			}
        }//end of for loop

	}else{//クエリー存在チェック
        $chkQry = array();
        $chkQry = cmChkQuery($db2con,'',$D2NAME,$PMPKEY);
        if($chkQry['result'] === 'NOTEXIST_PIV'){
            $rtn = 3;
            $msg = showMsg('NOTEXIST_GET',array('ピボット'));
        }else if($chkQry['result'] !== true){
            $rtn = 3;
            $msg = showMsg($chkQry['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $chkData = fnGetDB2WAUT($db2con,$D2NAME,$WAMAIL,$PMPKEY);
    if(count($chkData) === 0){
        $rtn = 1;
        $msg =showMsg('NOTEXIST_GET',array('配信情報'));// '配信情報が取得できませんでした。</br>指定の配信情報は削除されている可能性があります。';
    }
    $PMPKEY = cmMer($PMPKEY);
}
if($rtn === 0){
    if($PMPKEY === ''){
		$paramD2NAME=($WAQGFLG==='1')?$LASTD1NAME:$D2NAME;
		e_log('Hello schedulemaster/getBestItem.php⇒'.$LASTD1NAME.'*****'.$D2NAME.'****'.$paramD2NAME);

        $rs = fnGetFDB2CSV2($db2con,$paramD2NAME);
        if($rtn === 0){
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
                if(count($data) === 0){
                    $rtn = 1;
                    $msg = showMsg('FAIL_COL',array('抽出範囲','フィールド','クエリー'));
                }
            }
        }
    }else{
		$paramD2NAME=($WAQGFLG==='1')?$LASTD1NAME:$D2NAME;
        $rs = fnGetDB2PCOL($db2con,$paramD2NAME,$PMPKEY);
        if($rtn === 0){
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $data = $rs['data'];
                if(count($data) === 0){
                    $rtn = 1;
                    $msg = showMsg('FAIL_COL',array('抽出範囲','フィールド','ピボット'));
                }
            }
        }
    }
}

cmDb2Close($db2con);
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData' => umEx($data,true)
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* Pivotに取得されたフィル―ド取得
*-------------------------------------------------------*
*/

function fnGetDB2PCOL($db2con,$D2NAME,$PMPKEY){

    $data = array();

    $strSQL  = ' SELECT ';
    $strSQL .= ' A.D2FILID ';
    $strSQL .= ' , A.D2FLD ';
    $strSQL .= ' , A.D2HED ';
    $strSQL .= ' FROM (';
    $strSQL .= ' SELECT C.* FROM ( ';
    $strSQL .= ' SELECT D2NAME,D2FILID,D2FLD,D2HED,D2CSEQ';
    $strSQL .= ' FROM FDB2CSV2 ' ;
    $strSQL .= ' WHERE D2NAME = ? ';
    $strSQL .= ' AND D2CSEQ > 0 ';
    $strSQL .= ' UNION ALL ';
    $strSQL .= ' SELECT D5NAME AS D2NAME,9999 AS D2FILID, ';
    $strSQL .= ' D5FLD AS D2FLD,D5HED AS D2HED,D5CSEQ AS D2CSEQ ';
    //$strSQL .= ' D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DNLF AS D2DNLF ';
    $strSQL .= ' FROM FDB2CSV5 ';
    $strSQL .= ' WHERE D5NAME = ? AND D5CSEQ > 0 ';
    $strSQL .= ' ) AS C) AS A ';
    $strSQL .= ' WHERE A.D2FLD IN (';
    $strSQL .= '        SELECT ';
    $strSQL .= '            B.WPFLD ';
    $strSQL .= '        FROM DB2PCOL as B ';
    $strSQL .= '        WHERE B.WPPFLG <> \'3\'';
    $strSQL .= '        AND B.WPNAME = ? ';
    $strSQL .= '        AND B.WPPKEY = ? ';
    $strSQL .= '        ORDER BY B.WPSEQN,B.WPPFLG)';
    $strSQL .= ' AND A.D2NAME = ? ';
    $strSQL .= ' ORDER BY A.D2CSEQ ';


    $params = array(
        $D2NAME
        ,$D2NAME
        ,$D2NAME
        ,$PMPKEY
        ,$D2NAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}
/*
*-------------------------------------------------------* 
* クエリーに取得されたフィル―ド取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV2($db2con,$D2NAME){

    $data = array();
    $param = array();

/*    $strSQL  = ' SELECT ';
    $strSQL .= ' A.D2FILID ';
    $strSQL .= ' , A.D2FLD ';
    $strSQL .= ' , A.D2HED ';
    $strSQL .= ' FROM FDB2CSV2 as A ';
    $strSQL .= ' WHERE A.D2NAME = ? ';
    $strSQL .= ' ORDER BY A.D2CSEQ ';*/

    $strSQL  = ' SELECT A.* FROM ( ';
    $strSQL .= ' SELECT D2FILID,D2FLD,D2HED,D2CSEQ';
    $strSQL .= ' FROM FDB2CSV2 ' ;
    $strSQL .= ' WHERE D2NAME = ? ';
    $strSQL .= ' AND D2CSEQ > 0 ';
    $strSQL .= ' UNION ALL ';
    $strSQL .= ' SELECT 9999 AS D2FILID, ';
    $strSQL .= ' D5FLD AS D2FLD,D5HED AS D2HED,D5CSEQ AS D2CSEQ ';
    //$strSQL .= ' D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DNLF AS D2DNLF ';
    $strSQL .= ' FROM FDB2CSV5 ';
    $strSQL .= ' WHERE D5NAME = ? AND D5CSEQ > 0 ';
    $strSQL .= ' ) AS A ';
    $strSQL .= ' ORDER BY A.D2CSEQ ASC ';

    $params = array(
        $D2NAME,
        $D2NAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}

/**
* ユーザーマスター取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WANAME 定義名
* @param String  $WAMAIL PIVOTKEY
* 
*/

function fnGetDB2WAUT($db2con,$WANAME,$WAMAIL,$WAPKEY){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WAUT as A ';
    $strSQL .= ' WHERE WANAME <> \'\' ';
    $strSQL .= ' AND WANAME = ? ';
    $strSQL .= ' AND WAMAIL = ? ';
    $strSQL .= ' AND WAPKEY = ? ';

    $params = array(
        $WANAME,
        $WAMAIL,
        $WAPKEY
    );
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;
}