<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$D1QGFLG = $_POST['D1QGFLG'];


//$SOPKEY = $_POST['PMPKEY'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);

//$SOPKEY = cmHscDe($SOPKEY);

$csv_d = array();
$D1NAMELIST=array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($D1QGFLG==='1'){
		$rs=cmGetQueryGroup($db2con,$D1NAME,'','');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリーグループ'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
        }
	}
}
e_log("getD1HOKN.php=>".$rtn."\t".print_r($D1NAMELIST,true));
e_log("getD1HOKN.php D1QGFLG=>".$D1QGFLG);

//クエリーグループ
//クエリー存在チェック
if($rtn === 0){
	if($D1QGFLG==='1'){
        foreach($D1NAMELIST as $key => $res){			
            $chkQry = array();
			//e_log("getD1HOKN.php WUAUTH=>".$userData[0]['WUAUTH']);
			$paramUser=($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4')?$userData[0]['WUUID']:'';
			//e_log("getD1HOKN.php paramUser=>".$paramUser);
            $chkQry = cmChkQueryGroup($db2con,$paramUser,$D1NAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
			//e_log("getD1HOKN.php chkQry=>".$chkQry);
            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 3;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }//end of for loop
	}else{
	    $chkQry = array();
	    $chkQry = cmChkQuery($db2con,'',$D1NAME,'');
	    if($chkQry['result'] === 'NOTEXIST_PIV'){
	        $rtn = 3;
	        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
	    }else if($chkQry['result'] !== true){
	        $rtn = 1;
	        $msg = showMsg($chkQry['result'],array('クエリー'));
	    }
	}

}
if($rtn === 0){
	if(( $userData[0]['WUAUTH'] === '3' ||$userData[0]['WUAUTH'] === '4') && $D1QGFLG!=='1'){
	    if($rtn === 0){
	        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
	        	//e_log("getD1HOKN.php chkQryUsrksk=>".$chkQryUsr);
			if($chkQryUsr === 'NOTEXIST_GET'){
	            $rtn = 3;
	            $msg = showMsg('ログインユーザーに指定したクエリーに対してもスケージュルの権限がありません。');
	        }else if($chkQryUsr !== true){
	            $rtn = 3;
	            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
	        }
	    }
	}
}
if($rtn === 0){
    $data = fnGetD1HOKN($db2con,$D1NAME,$D1QGFLG);
    if($data[0]['D1HOKN'] === 0){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET_QRY',array('保管情報','クエリー'));// '保管情報が取得できませんでした。</br>指定のクエリーは削除されている可能性があります。';
    }
    if($data[0]['D1DIRE'] === ""){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET_QRY',array('CSV出力情報','クエリー'));//'CSV出力情報が取得できませんでした。</br>指定のクエリーは削除されている可能性があります。';
    }

}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData' => umEx($data)
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 保管日数取得
*-------------------------------------------------------*
*/

function fnGetD1HOKN($db2con,$D1NAME,$D1QGFLG){

    $data = array();

	if($D1QGFLG==='1'){
	    $strSQL  = ' SELECT A.SOHOKN AS D1HOKN,A.SODIRE AS D1DIRE, ';
		$strSQL .= ' A.SODIEX AS D1DIEX,A.SOCTFL AS D1CTFL,SOOUTSA AS D1OUTSA, ';
		$strSQL .= ' A.SO0MAL AS D10MAL,A.SOOUTSE AS D1OUTSE,SOOUTSR AS D1OUTSR ';
	    $strSQL .= ' FROM DB2WSOC as A ';
	    $strSQL .= ' WHERE SONAME = ? AND SOPKEY=\'\' ';
	}else{
	    $strSQL  = ' SELECT A.D1HOKN,A.D1DIRE,A.D1DIEX,A.D1CTFL,A.D10MAL,A.D1OUTSE,D1OUTSR,D1OUTSA ';
	    $strSQL .= ' FROM FDB2CSV1 as A ';
	    $strSQL .= ' WHERE D1NAME = ? ';
	}

    $params = array(
        $D1NAME
    );
//e_log('schedulemaster/getD1HOKNaaa.php⇒'.$strSQL.print_r($params,true));

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;
}
