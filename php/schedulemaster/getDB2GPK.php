<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WANAME = $_POST['WANAME'];
$WAQGFLG = $_POST['WAQGFLG'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$WANAME = cmHscDe($WANAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
if($rtn === 0){//グループ
   // error_log("WAQGFLG 20181011 => ".$WAQGFLG);
    if($WAQGFLG === '1'){
        $data = fnGetDB2GPK_Group($db2con,$WANAME);
    }else{ 
        $data = fnGetDB2GPK($db2con,$WANAME);
    }
    //error_log("data for GID show 20181011 =>###### ".print_r($data,true)); 
    if(count($data) === 0){
        $rtn = 1;
        $msg =showMsg('NOTEXIST',array('グラフキー'));// グラフキーがありません
        //$msg = "グラフキーがありません。";
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData' => umEx($data)
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 保管日数取得
*-------------------------------------------------------*
*/

function fnGetDB2GPK($db2con,$GPKQRY){

    $data = array();

    $strSQL  = ' SELECT A.GPKID,A.GPKNM ';
    $strSQL .= ' FROM DB2GPK as A ';
    $strSQL .= ' WHERE GPKQRY = ? ';

    $params = array(
        $GPKQRY
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        //error_log("fnGetDB2GPK 20181011 => ".$strSQL."\n".print_r($params,true)); 
        if($r === false){
             $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;
}

function fnGetDB2GPK_Group($db2con,$WANAME){
    $data = array();
    $rs = cmGetLastQueryGroup($db2con,$WANAME);
    //error_log("cmGetQueryGroup result 20181011 => ".print_r($rs,true));
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('クエリー'));
    }else{
        $D1NAMELIST = umEx($rs['data'],true);
    }
    if($D1NAMELIST != ""){
        foreach($D1NAMELIST as $key => $res){
            $LASTD1NAME = $res['D1NAME'];
        }
        
        $graphdata = "";
        //error_log("LASTD1NAME 20181011 => ".print_r($LASTD1NAME,true)); 
        $graphdata = cmGetDB2GPK($db2con,$LASTD1NAME); 
        if($graphdata['result'] !== true){
            $rtn = 2;
            $msg = showMsg($graphdata['result'],array('クエリー'));
        }else{
            $getgraphid = umEx($graphdata['data'],true);
        }
        //error_log("getgraphid 20181011 => ".print_r($getgraphid,true)); 
        if($getgraphid != ""){
            $data[] = $getgraphid;
        }

        return $getgraphid;
    }
}

