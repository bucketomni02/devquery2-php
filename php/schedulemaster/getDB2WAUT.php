<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WANAME = $_POST['WANAME'];
$WAPKEY = $_POST['WAPKEY'];
$WAQGFLG = $_POST['WAQGFLG'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$allcount = 0;
$data = array();
$d1text = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$WANAME = cmHscDe($WANAME);
$WAPKEY = cmHscDe($WAPKEY);

$csv_d = array();
$D1NAMELIST=array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($WAQGFLG==='1'){
        $rs=cmGetQueryGroup($db2con,$WANAME,$WAPKEY,'');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリーグループ'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
        }
	}
}
//クエリーグループ
//クエリー存在チェック
if($rtn === 0){
	if($WAQGFLG==='1'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
			$paramUser=($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4')?$userData[0]['WUUID']:'';
            $chkQry = cmChkQueryGroup($db2con,$paramUser,$WANAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);

            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 3;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }//end of for loop
	}else{
	    $chkQry = array();
	    $chkQry = cmChkQuery($db2con,'',$WANAME,$WAPKEY);
	    if($chkQry['result'] === 'NOTEXIST_PIV'){
	        $rtn = 3;
	        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
	    }else if($chkQry['result'] !== true){
	        $rtn = 3;
	        $msg = showMsg($chkQry['result'],array('クエリー'));
	    }
	}

}
if(($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4') && $WAQGFLG!=='1'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$WANAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg('ログインユーザーに指定したクエリーに対してもスケージュルの権限がありません。');
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $allcount = fnGetAllCount($db2con,$WANAME,$WAPKEY);
    $data = fnGetDB2WAUT($db2con,$WANAME,$WAPKEY,$start,$length,$sort,$sortDir);

    if($WAPKEY ===''){
        //定義名取得
		if($WAQGFLG==='1'){
			$rs = fnGetD1TEXTQGROUP($db2con,$WANAME);
		}else{
	        $rs = fnGetD1TEXT($db2con,$WANAME);
		}
    }
    else{
        //定義名(ピボット)取得
		if($WAQGFLG==='1'){
			$rs = fnGetD1TEXTPIVOTQGROUP($db2con,$WANAME,$WAPKEY);
		}else{
	        $rs = fnGetD1TEXTPIVOT($db2con,$WANAME,$WAPKEY);
		}
    }

    if($rs[0]['PMNAME'] !== null){
         $rs[0]['D1TEXT'] = cmMer($rs[0]['D1TEXT']).'('.cmMer($rs[0]['PMTEXT']).')';
    }

    $d1text = cmMer($rs[0]['D1TEXT']);
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'D1TEXT' => cmHsc($d1text)
);

echo(json_encode($rtn));

/**
* ユーザーマスター取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $WANAME  定義名
* @param String  $WAPKEY  PIVOTKEY
* @param String  $start   データの開始位置
* @param String  $length  データの長さ
* @param String  $sort    ソートする項目
* @param String  $sortDir ASC or DESC
* 
*/

function fnGetDB2WAUT($db2con,$WANAME,$WAPKEY,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.WANAME ASC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM DB2WAUT as B ';
    $strSQL .= ' WHERE WANAME <> \'\' ';
    
    if($WANAME != ''){
        $strSQL .= ' AND WANAME = ? ';
        array_push($params,$WANAME);
    }
    if($WAPKEY != ''){
        $strSQL .= ' AND WAPKEY = ? ';
        array_push($params,$WAPKEY);
    }
    else{
        $strSQL .= ' AND WAPKEY = ? ';
        array_push($params,'');
    }
    
    $strSQL .= ' ) as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }

    return $data;

}

/**
* 全件カウント取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $WANAME  定義名
* 
*/

function fnGetAllCount($db2con,$WANAME = '',$WAPKEY = ''){
    $rs = array();
    $strSQL  = ' SELECT count(A.WANAME) as COUNT ';
    $strSQL .= ' FROM DB2WAUT as A ' ;
    $strSQL .= ' WHERE WANAME <> \'\' ';

    $params = array();

    if($WANAME != ''){
        $strSQL .= ' AND WANAME = ? ';
        array_push($params,$WANAME);
    }
    if($WAPKEY != ''){
        $strSQL .= ' AND WAPKEY = ? ';
        array_push($params,$WAPKEY);
    }
    else{
        $strSQL .= ' AND WAPKEY = ? ';
        array_push($params,'');
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rs[] = $row['COUNT'];
            }
            $rs = $rs[0];
        }
    }

    return $rs;

}

/**
* 定義名取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $id      定義名id
* 
*/

function fnGetD1TEXT($db2con,$id){

    $data = array();

    $strSQL  = ' SELECT A.D1TEXT';
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;
    $strSQL .= ' WHERE A.D1NAME = ? ';


    $params = array($id);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}

/**
* 定義名取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $id      定義名id
* 
*/

function fnGetD1TEXTPIVOT($db2con,$id,$WAPKEY){

    $data = array();

    $strSQL  = ' SELECT A.D1TEXT,B.PMNAME,B.PMTEXT';
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;
    $strSQL .= ' LEFT JOIN DB2PMST AS B ';
    $strSQL .= ' ON A.D1NAME = B.PMNAME ';
    $strSQL .= ' WHERE A.D1NAME = ? ';
    $strSQL .= ' AND B.PMPKEY = ? ';


    $params = array();
    array_push($params,$id);
    array_push($params,$WAPKEY);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}
/*
*-------------------------------------------------------* 
* 定義名取得のクエリーグループ
*-------------------------------------------------------*
*/

function fnGetD1TEXTQGROUP($db2con,$id){

    $data = array();

    $strSQL  = ' SELECT A.QRYGNAME AS D1TEXT';
    $strSQL .= ' FROM DB2QRYG AS A ' ;
    $strSQL .= ' WHERE A.QRYGID = ? ';

    $params = array($id);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}


/*
*-------------------------------------------------------* 
* 定義名取得のクエリーグループ
*-------------------------------------------------------*
*/

function fnGetD1TEXTPIVOTQGROUP($db2con,$WHNAME,$WHPKEY){

    $data = array();
	$params = array();
	$strSQL ='';

    $strSQL .= ' SELECT QRYG.QRYGNAME AS D1TEXT,QRYG.QRYGID AS D1NAME,PMNAME,PMTEXT FROM DB2QRYG AS QRYG ';
    $strSQL .= ' LEFT JOIN ( ';
    $strSQL .= '     SELECT EMP2.QRYGSID,EMP2.QRYGSSEQ,EMP2.QRYGSQRY,PMNAME,PMPKEY,PMTEXT FROM DB2QRYGS AS EMP2 ';
    $strSQL .= '     JOIN ';
    $strSQL .= '     ( ';
    $strSQL .= '         SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
    $strSQL .= '         FROM DB2QRYGS AS D ';
    $strSQL .= '         GROUP BY QRYGSID ';
    $strSQL .= '     )  EMP1 ';
    $strSQL .= '     ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
    $strSQL .= '     JOIN DB2PMST AS PMST ON EMP2.QRYGSQRY=PMST.PMNAME ';
    $strSQL .= ' ) AS QRYGS ';
    $strSQL .= ' ON QRYG.QRYGID=QRYGS.QRYGSID ';
    $strSQL .= ' WHERE QRYG.QRYGID<> \'\' AND QRYG.QRYGID=? ';
    $strSQL .= ' AND PMPKEY=? ';


    $params = array($WHNAME,$WHPKEY);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}
