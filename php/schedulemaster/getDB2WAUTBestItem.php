<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WANAME = $_POST['WANAME'];
$WAPKEY = $_POST['WAPKEY'];
$WAMAIL = $_POST['WAMAIL'];
$WAQGFLG = $_POST['WAQGFLG'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$D1NAMELIST=array();
$LASTD1NAME='';
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$WANAME = cmHscDe($WANAME);
$WAPKEY = cmHscDe($WAPKEY);
$WAMAIL = cmHscDe($WAMAIL);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($WAQGFLG === '1'){
		$rs=cmGetQueryGroup($db2con,$WANAME,$WAPKEY,'');
		if($rs['result'] !== true){
			$rtn = 2;
			$msg = showMsg($rs['result'],array('クエリーグループ'));
		}else{
			$D1NAMELIST=umEx($rs['data'],true);
		}
	}
}
e_log('Hello D1NAMELIST⇒'.print_r($D1NAMELIST,true));
//クエリーグループ
if($rtn === 0){
	if($WAQGFLG==='1'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,'',$WANAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 3;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
			if($rtn===0){
				if(cmMer($res['LASTFLG'])==='1'){
					$LASTD1NAME=$res['QRYGSQRY'];
				}
			}
        }//end of for loop
	}else{
    //クエリー存在チェック
        $chkQry = array();
        $chkQry = cmChkQuery($db2con,'',$WANAME,$WAPKEY);
        if($chkQry['result'] === 'NOTEXIST_PIV'){
            $rtn = 4;
            $msg = showMsg('NOTEXIST_GET',array('ピボット'));
        }else if($chkQry['result'] !== true){
            $rtn = 4;
            $msg = showMsg($chkQry['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $rs = fnGetDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('配信設定'));
    }else{
        $data = $rs['data'];
    }
}
if($rtn === 0){
    $db2waul = array();
    $rs =  fnGetDB2WAUL($db2con,$WANAME,$WAPKEY,$WAMAIL);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $db2waul = $rs['data'];
    }
}
if(cmMer($WAPKEY) !== ''){
    if($rtn === 0){
		$paramWANAME=($WAQGFLG==='1')?$LASTD1NAME:$WANAME;
        $rs = fnGetDB2PCOL($db2con,$paramWANAME,$WAPKEY);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $rs = $rs['data'];
            $pivotCol = array();
            foreach($rs as $value){
                array_push($pivotCol,$value['WPFLD']);
            }
        }
    }
    if($rtn === 0){
        if(count($data) !== 0){
            if(cmMer($data[0][WABAFLD]) !== ''){
                if(!in_array($data[0][WABAFLD],$pivotCol)){
                    $msg =showMsg('FAIL_COL',array('ピボット','抽出項目'));// 'ピボットに設定されていない項目が抽出項目に指定されています。<br/>ピボット設定を確認してください。';
                    $rtn = 3;
                }
            }
        }
    }
}else{
    if($rtn === 0){
		$paramWANAME=($WAQGFLG==='1')?$LASTD1NAME:$WANAME;
        $rs = fnGetFDB2CSV2($db2con,$paramWANAME);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $rs = $rs['data'];
            $qryCol = array();
            foreach($rs as $value){
                array_push($qryCol,$value['D2FLD']);
            }
        }
    }
    if($rtn === 0){
        if(count($data) !== 0){
            if(cmMer($data[0][WABAFLD]) !== ''){
                if(!in_array($data[0][WABAFLD],$qryCol)){
                    $msg = showMsg('FAIL_COL',array('クエリー','抽出項目')); //'クエリーに設定されていない項目が抽出項目に指定されています。<br/>クエリー設定を確認してください。';
                    $rtn = 3;
                }
            }
        }
    }
}

cmDb2Close($db2con);
/**return**/
$rtn = array(
    'RTN'       => $rtn,
    'MSG'       => $msg,
    'aaData'    => umEx($data),
    'DB2WAUL'   => umEx($db2waul)
);

echo(json_encode($rtn));

/*
}

/**
* 配信メール存在チェック処理
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WANAME 定義名
* @param String  $WAPKEY PIVOTKEY
* @param String  $WAMAIL 宛先メールアドレス
* 
*/

function fnGetDB2WAUT($db2con,$WANAME,$WAPKEY,$WAMAIL){

    $data = array();

    $strSQL  = ' SELECT A.WABAFID ';
    $strSQL .= ' , A.WABAFLD ' ;
    $strSQL .= ' , A.WABAFR ' ;
    $strSQL .= ' , A.WABATO ' ;
    $strSQL .= ' , A.WABFLG ' ;
    $strSQL .= ' FROM DB2WAUT AS A ' ;
    $strSQL .= ' WHERE A.WANAME = ? ' ;
    $strSQL .= ' AND A.WAPKEY = ? ' ;
    $strSQL .= ' AND A.WAMAIL = ? ' ;

    $params = array(
        $WANAME,
        $WAPKEY,
        $WAMAIL,
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true,'data' => $data);
            }
        }
    }
    return $data;

}

function fnGetDB2PCOL($db2con,$WANAME,$WAPKEY){
    $data = array();
    $strSQL  = ' SELECT WPFLD ';
    $strSQL .= ' FROM DB2PCOL ';
    $strSQL .= ' WHERE WPNAME = ? ';
    $strSQL .= ' AND WPPKEY = ? ';
    $strSQL .= ' ORDER BY WPPFLG ';
    $strSQL .= ' ,WPSEQN ';
    
    $params = array(
        $WANAME,
        $WAPKEY
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}


function fnGetFDB2CSV2($db2con,$D2NAME){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT A.* FROM ( ';
    $strSQL .= ' SELECT D2FILID,D2FLD,D2HED,D2CSEQ';
    $strSQL .= ' FROM FDB2CSV2 ' ;
    $strSQL .= ' WHERE D2NAME = ? ';
    $strSQL .= ' AND D2CSEQ > 0 ';
    $strSQL .= ' UNION ALL ';
    $strSQL .= ' SELECT 9999 AS D2FILID, ';
    $strSQL .= ' D5FLD AS D2FLD,D5HED AS D2HED,D5CSEQ AS D2CSEQ ';
    //$strSQL .= ' D5WEDT AS D2WEDT,D5TYPE AS D2TYPE,D5LEN AS D2LEN,D5DNLF AS D2DNLF ';
    $strSQL .= ' FROM FDB2CSV5 ';
    $strSQL .= ' WHERE D5NAME = ? AND D5CSEQ > 0 ';
    $strSQL .= ' ) AS A ';
    $strSQL .= ' ORDER BY A.D2CSEQ ASC ';

    $params = array(
        $D2NAME,
        $D2NAME
    );

    $stmt = db2_prepare($db2con,$strSQL);

    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}


function fnGetDB2WAUL($db2con,$WLNAME,$WLPKEY,$WLMAIL){
    $data = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WAUL AS A ' ;
    $strSQL .= ' WHERE A.WLNAME = ? ' ;
    $strSQL .= ' AND A.WLPKEY = ? ' ;
    $strSQL .= ' AND A.WLMAIL = ? ORDER BY WLSEQL' ;

    $params = array(
        $WLNAME,
        $WLPKEY,
        $WLMAIL
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}