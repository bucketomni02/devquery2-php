<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WHNAME = $_POST['WHNAME'];
$WHPKEY = (isset($_POST['WHPKEY'])?$_POST['WHPKEY']:'');
$WHQGFLG = (isset($_POST['WHQGFLG'])?$_POST['WHQGFLG']:'');
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$allcount = 0;
$data = array();
$d1text = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$WHNAME = cmHscDe($WHNAME);
$WHPKEY = cmHscDe($WHPKEY);

$csv_d = array();
$D1NAMELIST=array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($WHQGFLG === '1'){
		$rs=cmGetQueryGroup($db2con,$WHNAME,$WHPKEY,'');
		if($rs['result'] !== true){
			$rtn = 2;
			$msg = showMsg($rs['result'],array('クエリーグループ'));
		}else{
			$D1NAMELIST=umEx($rs['data'],true);
		}
	}
}
//クエリーグループ
//クエリー存在チェック
if($rtn === 0){
	if($WHQGFLG === '1'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
			$paramUser=($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4')?$userData[0]['WUUID']:'';
            $chkQry = cmChkQueryGroup($db2con,$paramUser,$WHNAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 3;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }//end of for loop
	}else{
	    $chkQry = array();
	    $chkQry = cmChkQuery($db2con,'',$WHNAME,$WHPKEY);
	    if($chkQry['result'] === 'NOTEXIST_PIV'){
	        $rtn = 3;
	        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
	    }else if($chkQry['result'] !== true){
	        $rtn = 3;
	        $msg = showMsg($chkQry['result'],array('クエリー'));
	    }
	}
}

if(($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4') && $WHQGFLG !== '1'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$WHNAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg('ログインユーザーに指定したクエリーに対してもスケージュルの権限がありません。');
        }else if($chkQryUsr !== true){
            $rtn = 3;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    $allcount = fnGetAllCount($db2con,$WHNAME,$WHPKEY);

    $data = fnGetDB2WHIS($db2con,$WHNAME,cmMer($WHPKEY),$start,$length,$sort,$sortDir);

    if(cmMer($WHPKEY) ===''){
        //定義名取得
		if($WHQGFLG==='1'){
			$rs = fnGetD1TEXTQGROUP($db2con,$WHNAME);
		}else{
	        $rs = fnGetD1TEXT($db2con,$WHNAME);
		}
    }else{
        //定義名(ピボット)取得
		if($WHQGFLG==='1'){
			$rs = fnGetD1TEXTPIVOTQGROUP($db2con,$WHNAME,cmMer($WHPKEY));
		}else{
	        $rs = fnGetD1TEXTPIVOT($db2con,$WHNAME,cmMer($WHPKEY));
		}
		e_log('Hello Kalayar Htun⇒'.print_r($rs,true));
        if($rs[0]['PMNAME'] !== null){
            $rs[0]['D1TEXT'] = cmMer($rs[0]['D1TEXT']).'('.cmMer($rs[0]['PMTEXT']).')';
        }
    }
    $d1text = cmMer($rs[0]['D1TEXT']);
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data),
    'D1TEXT' => cmHsc($d1text),
    'WHPKEY' => cmMer($WHPKEY)
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2WHIS($db2con,$WHNAME,$WHPKEY,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.WHBDAY DESC,B.WHBTIM DESC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM DB2WHIS as B ';
    $strSQL .= ' WHERE WHNAME <> \'\' ';
    
    //if($WHNAME != ''){
        $strSQL .= ' AND WHNAME = ? ';
        array_push($params,$WHNAME);
    //}
   // if($WHPKEY != ''){
        $strSQL .= ' AND WHPKEY = ? ';
        array_push($params,$WHPKEY);
   // }
    
    $strSQL .= ' ) as A ';

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$WHNAME = '',$WHPKEY = ''){
    $rs = array();

    $strSQL  = ' SELECT count(A.WHNAME) as COUNT ';
    $strSQL .= ' FROM DB2WHIS as A ' ;
    $strSQL .= ' WHERE WHNAME <> \'\' ';

    $params = array();

    //if($WHNAME != ''){
        $strSQL .= ' AND WHNAME = ? ';
        array_push($params,$WHNAME);
    //}
    //if($WHPKEY != ''){
        $strSQL .= ' AND WHPKEY = ? ';
        array_push($params,$WHPKEY);
    //}

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $rs[] = $row['COUNT'];
            }
            $rs = $rs[0];
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* 定義名取得
*-------------------------------------------------------*
*/

function fnGetD1TEXT($db2con,$id){

    $data = array();

    $strSQL  = ' SELECT A.D1TEXT';
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;
    $strSQL .= ' WHERE A.D1NAME = ? ';

    $params = array($id);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}
/*
*-------------------------------------------------------* 
* 定義名取得のクエリーグループ
*-------------------------------------------------------*
*/

function fnGetD1TEXTQGROUP($db2con,$id){

    $data = array();

    $strSQL  = ' SELECT A.QRYGNAME AS D1TEXT';
    $strSQL .= ' FROM DB2QRYG AS A ' ;
    $strSQL .= ' WHERE A.QRYGID = ? ';

    $params = array($id);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}


/*
*-------------------------------------------------------* 
* 定義名取得のクエリーグループ
*-------------------------------------------------------*
*/

function fnGetD1TEXTPIVOTQGROUP($db2con,$WHNAME,$WHPKEY){

    $data = array();
	$params = array();
	$strSQL ='';

    $strSQL .= ' SELECT QRYG.QRYGNAME AS D1TEXT,QRYG.QRYGID AS D1NAME,PMNAME,PMTEXT FROM DB2QRYG AS QRYG ';
    $strSQL .= ' LEFT JOIN ( ';
    $strSQL .= '     SELECT EMP2.QRYGSID,EMP2.QRYGSSEQ,EMP2.QRYGSQRY,PMNAME,PMPKEY,PMTEXT FROM DB2QRYGS AS EMP2 ';
    $strSQL .= '     JOIN ';
    $strSQL .= '     ( ';
    $strSQL .= '         SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
    $strSQL .= '         FROM DB2QRYGS AS D ';
    $strSQL .= '         GROUP BY QRYGSID ';
    $strSQL .= '     )  EMP1 ';
    $strSQL .= '     ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
    $strSQL .= '     JOIN DB2PMST AS PMST ON EMP2.QRYGSQRY=PMST.PMNAME ';
    $strSQL .= ' ) AS QRYGS ';
    $strSQL .= ' ON QRYG.QRYGID=QRYGS.QRYGSID ';
    $strSQL .= ' WHERE QRYG.QRYGID<> \'\' AND QRYG.QRYGID=? ';
    $strSQL .= ' AND PMPKEY=? ';


    $params = array($WHNAME,$WHPKEY);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}


/**
* 定義名取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con  DBコネクション
* @param String  $id      定義名id
* @param String  $WHPKEY  ピボットキー
* 
*/
function fnGetD1TEXTPIVOT($db2con,$id,$WHPKEY){

    $data = array();

    $strSQL  = ' SELECT A.D1TEXT,B.PMNAME,B.PMTEXT';
    $strSQL .= ' FROM FDB2CSV1 AS A ' ;
    $strSQL .= ' LEFT JOIN DB2PMST AS B ';
    $strSQL .= ' ON A.D1NAME = B.PMNAME ';
    $strSQL .= ' WHERE A.D1NAME = ? ';
    $strSQL .= ' AND B.PMPKEY = ? ';


    $params = array();
    array_push($params,$id);
    array_push($params,$WHPKEY);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}