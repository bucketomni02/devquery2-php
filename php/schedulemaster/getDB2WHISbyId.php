<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WHNAME = (isset($_POST['WHNAME'])?$_POST['WHNAME']:'');
$WHPKEY = (isset($_POST['WHPKEY'])?$_POST['WHPKEY']:'');
$WHMAIL = (isset($_POST['WHMAIL'])?$_POST['WHMAIL']:'');
$WHBDAY = (isset($_POST['WHBDAY'])?$_POST['WHBDAY']:'');
$WHBTIM = (isset($_POST['WHBTIM'])?$_POST['WHBTIM']:'');
$WHOUTF = (isset($_POST['WHOUTF'])?$_POST['WHOUTF']:'');
$WHQGFLG = (isset($_POST['WHQGFLG'])?$_POST['WHQGFLG']:'');

$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$allcount = 0;
$data = array();
$d1text = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$WHNAME = cmHscDe($WHNAME);
$WHPKEY = cmHscDe($WHPKEY);

$csv_d = array();
$D1NAMELIST=array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($WHQGFLG === '1'){
		$rs=cmGetQueryGroup($db2con,$WHNAME,$WHPKEY,'');
		if($rs['result'] !== true){
			$rtn = 2;
			$msg = showMsg($rs['result'],array('クエリーグループ'));
		}else{
			$D1NAMELIST=umEx($rs['data'],true);
		}
	}
}
//クエリーグループ
//クエリー存在チェック
    if($rtn === 0){
		if($WHQGFLG==='1'){
	        foreach($D1NAMELIST as $key => $res){
	            $chkQry = array();
	            $chkQry = cmChkQueryGroup($db2con,'',$WHNAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
	            if($chkQry['result'] === 'NOTEXIST_PIV'){
	                $rtn = 3;
	                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
	                break;
	            }else if($chkQry['result'] !== true){
	                $rtn = 3;
	                $msg = showMsg($chkQry['result'],array('クエリー'));
	                break;
	            }
	        }//end of for loop
		}else{
	        $chkQry = array();
	        $chkQry = cmChkQuery($db2con,'',$WHNAME,$WHPKEY);
	        if($chkQry['result'] === 'NOTEXIST_PIV'){
	            $rtn = 2;
	            $msg = showMsg('NOTEXIST_GET',array('ピボット'));
	        }else if($chkQry['result'] !== true){
	            $rtn = 2;
	            $msg = showMsg($chkQry['result'],array('クエリー'));
	        }
		}
    }
   if($rtn === 0){
        $rs = DB2WHISbyId($db2con,$WHNAME,$WHPKEY,$WHMAIL,$WHBDAY,$WHBTIM,$WHOUTF);
       if($rs['result'] !== true){
            $msg = showMsg($rs['result']);
            $rtn = 1;
        }else{
            $data = $rs['data'];
        }
    }
cmDb2Close($db2con);
$arr = array($WHNAME,$WHPKEY,$WHMAIL,$WHBDAY,$WHBTIM,$WHOUTF);
/**return**/
$rtn = array(
    
    'RTN' => $rtn,
    'MSG' => $msg,
    'ARR' => $arr,
    'aaData' => umEx($data)
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function DB2WHISbyId($db2con,$WHNAME,$WHPKEY,$WHMAIL,$WHBDAY,$WHBTIM,$WHOUTF){

    $data = array();

   $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM DB2WHIS  ';
    $strSQL .= ' WHERE WHNAME <> \'\' ';
    $strSQL .= ' AND WHNAME = ? ';
    $strSQL .= ' AND WHMAIL = ? ';
    $strSQL .= ' AND WHBDAY = ? ';
    $strSQL .= ' AND WHBTIM = ? ';
    $strSQL .= ' AND WHOUTF= ? ';
    $params = array($WHNAME,$WHMAIL,$WHBDAY,$WHBTIM,$WHOUTF);
    if($WHPKEY != ''){
        $strSQL .= ' AND WHPKEY = ? ';
        array_push($params,$WHPKEY);
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
         $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

