<?php

/*
 * -------------------------------------------------------* 
 * 外部ファイル読み込み
 * -------------------------------------------------------*
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseQuery = 10;
/*
 * -------------------------------------------------------* 
 * DataTableリクエスト
 * -------------------------------------------------------*
 */

$D1NAME  = $_POST['D1NAME'];
$D1TEXT  = $_POST['D1TEXT'];
$PMTEXT  = $_POST['PMTEXT'];
$start   = $_POST['start'];
$length  = $_POST['length'];
$sort    = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$session = array();
$session['d1name']  = $D1NAME;
$session['d1text']  = $D1TEXT;
$session['pmtext']  = $PMTEXT;
$session['start']   = $start;
$session['length']  = $length;
$session['sort']    = $sort;
$session['sortDir'] = $sortDir;
/*
 * -------------------------------------------------------* 
 * 変数
 * -------------------------------------------------------*
 */
$rtn          = 0;
$msg          = '';
$allcount     = array();
$data         = array();
$dataall      = array();
$d1text       = '';
$notPivotLine = '';
$pmpkey       = '';
$count        = 0;

/*
 * -------------------------------------------------------* 
 * 処理
 * -------------------------------------------------------*
 */
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    } else {
        $userData = umEx($rs['data']);
        if ($userData[0]['WUAUTH'] === '2') {
            $rs = cmChkKenGen($db2con, '17', $userData[0]['WUSAUT']); //'1' => scheduleMaster
            if ($rs['result'] !== true) {
                $rtn = 2;
                $msg = showMsg($rs['result'], array(
                    'スケジュールの権限'
                ));
            }
        }
    }
}
if ($rtn === 0) {
    if ($licensePivot === true) {
        $rsCount = fnGetAllCountPivot($db2con, $WUAUTH, $D1NAME, $D1TEXT, $PMTEXT,$licenseQueryGroup,$licenseSql);
        if($rsCount['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rsCount['result']);
        }else{
            $allcount = $rsCount['data'];
            $rsData = fnGetFDB2CSV1Pivot($db2con, $WUAUTH,$D1NAME, $D1TEXT, $PMTEXT, $start, $length, $sort, $sortDir,$licenseQueryGroup,$licenseSql);
            if($rsData['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rsData['result']);
            }else{
                $data = $rsData['data'];
                $index = 0;
                foreach ($data as $key => $row) {
                    if ($row['PMNAME'] !== '') {
                        $row['pivotFlg'] = 0;
                        $row['D1TEXT']   = cmMer($row['D1TEXT']) . '(' . cmMer($row['PMTEXT']) . ')';
                        $dataall[$index] = $row;
                        $index++;
                    } else {
                        $row['pivotFlg'] = 1;
                        $row['D1TEXT']   = cmMer($row['D1TEXT']);
                        $dataall[$index] = $row;
                        $index++;
                    }
                }
            }
        }
    } else {
        $rsCount = fnGetAllCount($db2con,$userData[0]['WUAUTH'], $D1NAME, $D1TEXT,$licenseQueryGroup,$licenseSql);
        if($rsCount['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rsCount['result']);
        }else{
            $allcount = $rsCount['data'][0];
            $rsData  = fnGetFDB2CSV1($db2con,$userData[0]['WUAUTH'], $D1NAME, $D1TEXT, $start, $length, $sort, $sortDir,$licenseQueryGroup,$licenseSql);
            if($rsData['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rsData['result']);
            }else{
                $dataall = $rsData['data'];
                foreach ($dataall as $key => $row) {
                    $row['pivotFlg'] = 1;
                    $row['PMPKEY']   = '';
                    $dataall[$key]   = $row;
                }
            }
        }
    }
    
    // 定義名取得
   // $rs     = fnGetD1TEXT($db2con, $D1NAME);
   // $d1text = cmMer($rs[0]['D1TEXT']);
}
cmDb2Close($db2con);

/** return* */
$rtn = array(
    
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($dataall, true),
    //'D1TEXT' => $d1text,
    //'D1NAME' => $D1NAME,
    'licenseIFS' => $licenseIFS,
    'session' => $session
);

echo (json_encode($rtn));

/**
 * ユーザーマスター取得(ピボット用)
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con  DBコネクション
 * @param String  $D1NAME  定義名
 * @param String  $D1TEXT  記述
 * @param String  $start   データの開始位置
 * @param String  $length  データの長さ
 * @param String  $sort    ソートする項目
 * @param String  $sortDir ASC or DESC
 * 
 */

function fnGetFDB2CSV1Pivot($db2con, $WUAUTH , $D1NAME , $D1TEXT, $PMTEXT, $start = '', $length = '', $sort = '', $sortDir = '',$licenseQueryGroup,$licenseSql)
{
    $data = array();
    $params = array();
    
    $strSQL = ' SELECT F.* ';
    $strSQL .= ' FROM ( SELECT B.*,PMNAME,PMTEXT,PMPKEY,ROWNUMBER() OVER( ';
    
    if ($sort !== '') {
        $strSQL .= ' ORDER BY B.' . $sort . ' ' . $sortDir . ' ';
    } else {
        $strSQL .= ' ORDER BY B.D1NAME ASC ';
    }
    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM ( ';
    $strSQL .= '    SELECT ';
    $strSQL .= '        A.D1NAME, ';
    $strSQL .= '        A.D1TEXT, ';
    $strSQL .= '        \'\' AS PMNAME, ';
    $strSQL .= '        \'\' AS PMPKEY, ';
    $strSQL .= '        \'\' AS PMTEXT, ';
    $strSQL .= '        A.D1WEBF, ';
	$strSQL .= '		A.D1QGFLG ';
    $strSQL .= '    FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME, ';
        $strSQL .= '            D1TEXT, ';
        $strSQL .= '            D1WEBF, ';
        $strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'1\' AS D1QGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }	
        $strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD  ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '		INNER JOIN ( ';
		$strSQL .= '			SELECT WGNAME FROM DB2WGDF ';
		$strSQL .= '			JOIN DB2WUGR ON WGGID=WUGID ';
		$strSQL .= '			WHERE WUUID=?';
		$strSQL .= '		) AS GP ON QRYG.QRYGID=GP.WGNAME';

        $strSQL .= '    ) A ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
		$strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '			D1WEBF,\'1\' AS D1QGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF FROM DB2QRYG AS AA ';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
        $strSQL .= '    ) A ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
		$strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '			D1WEBF,\'1\' AS D1QGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
        $strSQL .= '    ) A ';
    }
    $strSQL .= '    UNION ';    
    $strSQL .= '    SELECT ';
    $strSQL .= '        PMNAME AS D1NAME, ';
    $strSQL .= '        E.D1TEXT, ';
    $strSQL .= '        PMNAME, ';
    $strSQL .= '        PMPKEY, ';
    $strSQL .= '        PMTEXT, ';
    $strSQL .= '        E.D1WEBF, ';
    $strSQL .= '        E.D1QGFLG ';
    $strSQL .= '    FROM ';
    $strSQL .= '        DB2PMST AS D ';
    $strSQL .= '    RIGHT JOIN  ';
    //$strSQL .= '        FDB2CSV1 E  ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
        $strSQL .= '    ) E ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
		array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
		$strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
        $strSQL .= '    ) E ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '        ( ';
        $strSQL .= '        SELECT ';
		$strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
        $strSQL .= '        ) E ';
    }
    $strSQL .= '    ON D.PMNAME = E.D1NAME ';

    $strSQL .= '    UNION ';    
    $strSQL .= '    SELECT ';
    $strSQL .= '        EE.D1NAME, ';
    $strSQL .= '        EE.D1TEXT, ';
    $strSQL .= '        PMNAME, ';
    $strSQL .= '        PMPKEY, ';
    $strSQL .= '        PMTEXT, ';
    $strSQL .= '        EE.D1WEBF, ';
    $strSQL .= '        EE.D1QGFLG ';
    $strSQL .= '    FROM ';
    $strSQL .= '        DB2PMST AS D ';
    $strSQL .= '    JOIN  ';
    //$strSQL .= '        FDB2CSV1 E  ';
    if($WUAUTH === '3'){
		//クエリーグループJOIN
		$strSQL .= '	( ';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'1\' AS D1QGFLG,QRYGSQRY ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF,QRYGSQRY FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID,EMP2.QRYGSQRY ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD  ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '		INNER JOIN ( ';
		$strSQL .= '			SELECT WGNAME FROM DB2WGDF ';
		$strSQL .= '			JOIN DB2WUGR ON WGGID=WUGID ';
		$strSQL .= '			WHERE WUUID=?';
		$strSQL .= '		) AS GP ON QRYG.QRYGID=GP.WGNAME';
	    $strSQL .= '    ) AS  EE ON D.PMNAME = EE.QRYGSQRY ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
		//クエリーグループJOIN
		$strSQL .= '		(';
		$strSQL .= '			SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '				QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '				D1WEBF,\'1\' AS D1QGFLG,QRYGSQRY ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '			INNER JOIN (';
		$strSQL .= '				SELECT DISTINCT(AA.QRYGID),D1WEBF,QRYGSQRY FROM DB2QRYG AS AA ';
		$strSQL .= '				INNER JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT D1WEBF,EMP2.QRYGSID,EMP2.QRYGSQRY ';
		$strSQL .= '			  		FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '					JOIN ';
		$strSQL .= '					( ';
		$strSQL .= '						SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '						FROM DB2QRYGS AS DD ';
		$strSQL .= '						GROUP BY QRYGSID ';
		$strSQL .= '					) EMP1 ';
		$strSQL .= '					ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '					JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '				)AS BB ';
		$strSQL .= '				ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '			) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
	    $strSQL .= '    	)AS EE  ON D.PMNAME = EE.QRYGSQRY ';
    }else{
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		(';
		$strSQL .= '			SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '				QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '				D1WEBF,\'1\' AS D1QGFLG,QRYGSQRY ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG ';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '			INNER JOIN (';
		$strSQL .= '				SELECT DISTINCT(AA.QRYGID),D1WEBF,QRYGSQRY FROM DB2QRYG AS AA';
		$strSQL .= '				INNER JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT D1WEBF,EMP2.QRYGSID,EMP2.QRYGSQRY ';
		$strSQL .= '			  		FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '					JOIN ';
		$strSQL .= '					( ';
		$strSQL .= '						SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '						FROM DB2QRYGS AS DD ';
		$strSQL .= '						GROUP BY QRYGSID ';
		$strSQL .= '					) EMP1 ';
		$strSQL .= '					ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '					JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '				)AS BB ';
		$strSQL .= '				ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '			) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '		) AS EE ON D.PMNAME=EE.QRYGSQRY';
    }

    $strSQL .= ' ) AS B ';
    $strSQL .= ' WHERE D1NAME <> \'\' '; 
    
    if (cmMer($D1NAME) != '') {
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params, '%' . $D1NAME . '%');
    }
    
    if (cmMer($D1TEXT) != '') {
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params, '%' . $D1TEXT . '%');
    }
    if (cmMer($PMTEXT) != '') {
        $strSQL .= ' AND PMTEXT like ? ';
        array_push($params, '%' . $PMTEXT . '%');
    }
    $strSQL .= ' ) as F ';

    // 抽出範囲指定
    if (($start != '') && ($length != '')) {
        $strSQL .= ' WHERE F.rownum BETWEEN ? AND ? ';
        array_push($params, $start + 1);
        array_push($params, $start + $length);
    }
    e_log('スケジュールのピボット入りデータ：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);        
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/**
 * ピボット全件カウント取得
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con  DBコネクション
 * @param String  $D1NAME  定義名
 * @param String  $D1TEXT  記述
 * 
 */
function fnGetAllCountPivot($db2con,$WUAUTH, $D1NAME = '', $D1TEXT = '', $PMTEXT = '',$licenseQueryGroup,$licenseSql)
{
    $rs = array();
    $data = array();
    $params = array();
    $strSQL = '';
    $strSQL .= '    SELECT ';
    $strSQL .= '        COUNT(*) AS COUNT ';
    $strSQL .= '    FROM ';
    $strSQL .= '        (SELECT ';
    $strSQL .= '            A.D1NAME, ';
    $strSQL .= '            A.D1TEXT, ';
    $strSQL .= '            \'\' AS PMNAME, ';
    $strSQL .= '            \'\' AS PMPKEY, ';
    $strSQL .= '            \'\' AS PMTEXT, ';
    $strSQL .= '            A.D1WEBF, ';
    $strSQL .= '            A.D1QGFLG ';
    $strSQL .= '        FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
		$strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= 'AND D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'1\' AS D1QGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD  ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '		INNER JOIN ( ';
		$strSQL .= '			SELECT WGNAME FROM DB2WGDF ';
		$strSQL .= '			JOIN DB2WUGR ON WGGID=WUGID ';
		$strSQL .= '			WHERE WUUID=?';
		$strSQL .= '		) AS GP ON QRYG.QRYGID=GP.WGNAME';
        $strSQL .= '    ) A ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);

    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
		$strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= 'AND D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '			D1WEBF,\'1\' AS D1QGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF FROM DB2QRYG AS AA ';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
        $strSQL .= '    ) A ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '        ( ';
        $strSQL .= '        SELECT ';
		$strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= 'WHERE D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '			D1WEBF,\'1\' AS D1QGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';

        $strSQL .= '        ) A ';
    }
    $strSQL .= '    UNION  '; 
    $strSQL .= '    SELECT ';
    $strSQL .= '        PMNAME AS D1NAME, ';
    $strSQL .= '        D.D1TEXT, ';
    $strSQL .= '        PMNAME, ';
    $strSQL .= '        PMPKEY, ';
    $strSQL .= '        PMTEXT, ';
    $strSQL .= '        D.D1WEBF, ';
    $strSQL .= '        D.D1QGFLG ';
    $strSQL .= '    FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
		$strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= ' AND D1CFLG = \'\' ';
        }
        $strSQL .= '    ) D ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);

    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
		$strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= 'AND D1CFLG = \'\' ';
        }
        $strSQL .= '    ) D ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '        ( ';
        $strSQL .= '        SELECT ';
		$strSQL .= '            D1NAME, ';
		$strSQL .= '            D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= 'WHERE D1CFLG = \'\' ';
        }
        $strSQL .= '        ) D ';
    }
    $strSQL .= '    JOIN DB2PMST E ';
    $strSQL .= '    ON D.D1NAME = E.PMNAME ';
	//ピボットのため、クエリーグループ
    $strSQL .= '    UNION  '; 
    $strSQL .= '    SELECT ';
    $strSQL .= '        DD.D1NAME, ';
    $strSQL .= '        DD.D1TEXT, ';
    $strSQL .= '        PMNAME, ';
    $strSQL .= '        PMPKEY, ';
    $strSQL .= '        PMTEXT, ';
    $strSQL .= '        DD.D1WEBF, ';
    $strSQL .= '        DD.D1QGFLG ';
    $strSQL .= '    FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '            D1WEBF, ';
		$strSQL .= '            \'1\' AS D1QGFLG,QRYGSQRY ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF,QRYGSQRY FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID,EMP2.QRYGSQRY ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD  ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '		INNER JOIN ( ';
		$strSQL .= '			SELECT WGNAME FROM DB2WGDF ';
		$strSQL .= '			JOIN DB2WUGR ON WGGID=WUGID ';
		$strSQL .= '			WHERE WUUID=?';
		$strSQL .= '		) AS GP ON QRYG.QRYGID=GP.WGNAME';
        $strSQL .= '    ) DD ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);

    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '			D1WEBF,\'1\' AS D1QGFLG,QRYGSQRY ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF,QRYGSQRY FROM DB2QRYG AS AA ';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID,EMP2.QRYGSQRY ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
        $strSQL .= '    ) DD ';
    }else{
        $strSQL .= '        ( ';
		$strSQL .= '			SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '				QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '				D1WEBF,\'1\' AS D1QGFLG,QRYGSQRY ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '         FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='          FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '			INNER JOIN (';
		$strSQL .= '				SELECT DISTINCT(AA.QRYGID),D1WEBF,QRYGSQRY FROM DB2QRYG AS AA';
		$strSQL .= '				INNER JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT D1WEBF,EMP2.QRYGSID,EMP2.QRYGSQRY ';
		$strSQL .= '			  		FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '					JOIN ';
		$strSQL .= '					( ';
		$strSQL .= '						SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '						FROM DB2QRYGS AS DD ';
		$strSQL .= '						GROUP BY QRYGSID ';
		$strSQL .= '					) EMP1 ';
		$strSQL .= '					ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '					JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '				)AS BB ';
		$strSQL .= '				ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '			) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';

        $strSQL .= '        ) DD ';
    }

    $strSQL .= '    JOIN DB2PMST E ';
    $strSQL .= '    ON QRYGSQRY = E.PMNAME ';
    $strSQL .= '        ) AS i ';
    $strSQL .= '    WHERE ';
    $strSQL .= '        D1NAME <> \'\'  ';
    if (cmMer($D1NAME) != '') {
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params, '%' . $D1NAME . '%');
    }
    
    if (cmMer($D1TEXT) != '') {
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params, '%' . $D1TEXT . '%');
    }
    if (cmMer($PMTEXT) != '') {
        $strSQL .= ' AND PMTEXT like ? ';
        array_push($params, '%' . $PMTEXT . '%');
    }
    e_log('スケジュールのピボット入りカウント：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
           $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}
/**
 * 全件カウント取得
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con  DBコネクション
 * @param String  $D1NAME  定義名
 * @param String  $D1TEXT  記述
 * 
 */
function fnGetAllCount($db2con,$WUAUTH, $D1NAME = '', $D1TEXT = '',$licenseQueryGroup,$licenseSql)
{
    $data = array();
    $params = array();
    $strSQL = '';
    $strSQL .= '    SELECT ';
    $strSQL .= '        count(B.D1NAME) as COUNT ';
    $strSQL .= '    FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID) FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN DB2QRYGS AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '		INNER JOIN ( ';
		$strSQL .= '			SELECT WGNAME FROM DB2WGDF ';
		$strSQL .= '			JOIN DB2WUGR ON WGGID=WUGID ';
		$strSQL .= '			WHERE WUUID=?';
		$strSQL .= '		) AS GP ON QRYG.QRYGID=GP.WGNAME';
        $strSQL .= '    ) B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID) FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN DB2QRYGS AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
        $strSQL .= '    ) B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
		$strSQL .= '(';
        $strSQL .= ' 		SELECT D1NAME FROM FDB2CSV1   ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID) FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN DB2QRYGS AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= ') AS B';
    }
    $strSQL .= '    WHERE ';
    $strSQL .= '        D1NAME <> \'\' ';
    if ($D1NAME != '') {
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params, '%' . $D1NAME . '%');
    }
    if ($D1TEXT != '') {
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params, '%' . $D1TEXT . '%');
    }
    e_log('スケジュールのカウント：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $rs = db2_execute($stmt, $params);
        if($rs === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true , 'data' => $data );
        }
    }
    return $data;
}
/**
 * 定義名取得
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con  DBコネクション
 * @param String  $id      $D1NAME 定義名
 * 
 */

function fnGetD1TEXT($db2con, $id)
{
    
    $data   = array();
    $strSQL = ' SELECT A.D1TEXT ';
    $strSQL .= ' FROM FDB2CSV1 AS A ';
    $strSQL .= ' WHERE D1NAME = ? ';
    
    $params = array(
        $id
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
    
}

/**
 * ユーザーマスター取得
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con  DBコネクション
 * @param String  $D1NAME  定義名
 * @param String  $D1TEXT  記述 
 * @param String  $start   データの開始位置
 * @param String  $length  データの長さ
 * @param String  $sort    ソートする項目
 * @param String  $sortDir ASC or DESC
 * 
 */

function fnGetFDB2CSV1($db2con,$WUAUTH, $D1NAME, $D1TEXT, $start = '', $length = '', $sort = '', $sortDir = '',$licenseQueryGroup,$licenseSql)
{
    $data = array();
    
    $params = array();
    
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';
    
    if ($sort !== '') {
        $strSQL .= ' ORDER BY B.' . $sort . ' ' . $sortDir . ' ';
    } else {
        $strSQL .= ' ORDER BY B.D1NAME ASC ';
    }
    
    $strSQL .= ' ) as rownum ';
    $strSQL .= '    FROM ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME,D1TEXT, ';
		$strSQL .= '            D1WEBF, \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '			D1WEBF,\'1\' AS D1QGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD  ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '		INNER JOIN ( ';
		$strSQL .= '			SELECT WGNAME FROM DB2WGDF ';
		$strSQL .= '			JOIN DB2WUGR ON WGGID=WUGID ';
		$strSQL .= '			WHERE WUUID=?';
		$strSQL .= '		) AS GP ON QRYG.QRYGID=GP.WGNAME';

        $strSQL .= '    ) B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME,D1TEXT, ';
		$strSQL .= '            D1WEBF, \'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        AND D1CFLG = \'\' ';
        }
        $strSQL .= '        ) ';
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '			D1WEBF,\'1\' AS D1QGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF FROM DB2QRYG AS AA ';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';

        $strSQL .= '    ) B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '	( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME,D1TEXT, ';
		$strSQL .= '			D1WEBF,\'0\' AS D1QGFLG ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        //ライセンスのsqlクエリー実行権限はしない時
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT, ';
		$strSQL .= '			D1WEBF,\'1\' AS D1QGFLG ';
        if($licenseQueryGroup){//ライセンスのクエリーグループのフラグ
            $strSQL.= '     FROM DB2QRYG AS QRYG';
        }else{
            $strSQL.='      FROM (SELECT * FROM DB2QRYG WHERE QRYGID > \'9999999999\') AS QRYG';
        }
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID),D1WEBF FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN ';
		$strSQL .= '			( ';
		$strSQL .= '				SELECT D1WEBF,EMP2.QRYGSID ';
		$strSQL .= '			  	FROM DB2QRYGS AS EMP2 ';
		$strSQL .= '				JOIN ';
		$strSQL .= '				( ';
		$strSQL .= '					SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
		$strSQL .= '					FROM DB2QRYGS AS DD ';
		$strSQL .= '					GROUP BY QRYGSID ';
		$strSQL .= '				) EMP1 ';
		$strSQL .= '				ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
		$strSQL .= '				JOIN FDB2CSV1 ON QRYGSQRY=D1NAME ';
        if(!$licenseSql){
            $strSQL .= '        WHERE D1CFLG = \'\' ';
        }
		$strSQL .= '			)AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '    ) AS B ';
    }

    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    if ($D1NAME != '') {
        $strSQL .= ' AND D1NAME like ? ';
        array_push($params, '%' . $D1NAME . '%');
    }
    
    if ($D1TEXT != '') {
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params, '%' . $D1TEXT . '%');
    }
    
    $strSQL .= ' ) as A ';
    
    // 抽出範囲指定
    if (($start != '') && ($length != '')) {
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params, $start + 1);
        array_push($params, $start + $length);
    }
    e_log('スケジュールのデータ：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);        
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array('result' => true , 'data' => $data);
        }
    }
    return $data;
    
}