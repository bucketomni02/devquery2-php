<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WAMAIL = $_POST['WAMAIL'];
$WANAME = $_POST['WANAME'];
$WAPKEY = $_POST['WAPKEY'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$WAMAIL = cmHscDe($WAMAIL);
$WANAME = cmHscDe($WANAME);
$WAPKEY = cmHscDe($WAPKEY);

$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
if($rtn === 0){
    $data = fnGetDB2WAUT($db2con,$WANAME,$WAMAIL,$WAPKEY);
    if(count($data) === 0){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_GET',array('配信情報'));//'配信情報が取得できませんでした。</br>指定の配信情報は削除されている可能性があります。';
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData' => umEx($data)
);

echo(json_encode($rtn));

/**
* ユーザーマスター取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WANAME 定義名
* @param String  $WAMAIL PIVOTKEY
* 
*/

function fnGetDB2WAUT($db2con,$WANAME,$WAMAIL,$WAPKEY){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WAUT as A ';
    $strSQL .= ' WHERE WANAME <> \'\' ';
    $strSQL .= ' AND WANAME = ? ';
    $strSQL .= ' AND WAMAIL = ? ';
    $strSQL .= ' AND WAPKEY = ? ';

    $params = array(
        $WANAME,
        $WAMAIL,
        $WAPKEY
    );
    
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}