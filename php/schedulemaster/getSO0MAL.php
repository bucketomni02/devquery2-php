<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$SOPKEY = $_POST['PMPKEY'];
$D1QGFLG = $_POST['D1QGFLG'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$SO0MAL = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);
$SOPKEY = cmHscDe($SOPKEY);

$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 3;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($D1QGFLG==='1'){
		$rs=cmGetQueryGroup($db2con,$D1NAME,'','');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリーグループ'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
        }
	}
}
e_log("getSO0MAL.php=>".$rtn."\t".print_r($D1NAMELIST,true));
e_log("getSO0MAL.php D1QGFLG=>".$D1QGFLG);

//クエリーグループ
//定義が削除されたかどうかチェック
if($rtn === 0){
	if($D1QGFLG==='1'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
			e_log("getSO0MAL.php WUAUTH=>".$userData[0]['WUAUTH']);
			$paramUser=($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4')?$userData[0]['WUUID']:'';
			e_log("getSO0MAL.php paramUser=>".$paramUser);
            $chkQry = cmChkQueryGroup($db2con,$paramUser,$D1NAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
			e_log("getSO0MAL.php chkQry=>".$chkQry);
            if($chkQry['result'] !== true){
                $rtn = 3;
                $msg = showMsg($chkQry['result'],array('定義名'));
                break;
            }
        }//end of for loop
	}else{
	    $chkQry = cmChkQuery ( $db2con, '' , $D1NAME, '');
	    if($chkQry['result'] !== true ) {
	        $rtn = 3;
	        $msg = showMsg($chkQry['result'],array('定義名'));
	    }
	}
}
if($rtn === 0){
    if(($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4') && $D1QGFLG!=='1'){
        if($rtn === 0){
            $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
				e_log("getSO0MAL.php chkQryUsrksk=>".$chkQryUsr);
            if($chkQryUsr === 'NOTEXIST_GET'){
                $rtn = 3;
                $msg = showMsg('ログインユーザーに指定したクエリーに対してもスケージュルの権限がありません。');
            }else if($chkQryUsr !== true){
                $rtn = 3;
                $msg = showMsg($chkQryUsr['result'],array('クエリー'));
            }
        }
    }
}
if($rtn === 0){
    $rs = fnGetSO0MAL($db2con,$D1NAME,$SOPKEY,$D1QGFLG);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $data = $rs['data'];
	}
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData' => umEx($data)
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* 保管日数取得
*-------------------------------------------------------*
*/

function fnGetSO0MAL($db2con,$SONAME,$SOPKEY,$D1QGFLG){

    $data = array();

	if($D1QGFLG==='1'){
	    $strSQL ='';
	    $strSQL .=' SELECT A.SONAME,A.SOPKEY,A.SO0MAL,A.SODIEX,A.SOCTFL,A.SOHOKN, ';
		$strSQL .=' A.SODIRE,A.SOQGFLG,A.SOOUTSE AS D1OUTSE,A.SOOUTSR AS D1OUTSR,A.SOOUTSA AS D1OUTSA FROM DB2WSOC A ';
	    $strSQL .=' WHERE A.SONAME=? AND A.SOPKEY=?';
	}else{
	    $strSQL ='';
	    $strSQL .=' SELECT A.*,B.D1OUTSE,B.D1OUTSR,B.D1OUTSA FROM DB2WSOC A ';
	    $strSQL .=' JOIN FDB2CSV1 B on A.SONAME=B.D1NAME ';
	    $strSQL .=' WHERE A.SONAME=? AND A.SOPKEY=?';
	}


    $params = array(
        $SONAME,
        $SOPKEY
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            e_log("data data".print_R($data,true));
             $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

