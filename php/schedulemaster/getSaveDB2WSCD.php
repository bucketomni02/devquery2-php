<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */
$WSNAME  = $_POST['WSNAME'];
$D1TEXT  = $_POST['D1TEXT'];
$PMTEXT  = $_POST['PMTEXT'];
$start   = $_POST['start'];
$length  = $_POST['length'];
$sort    = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$rtn      = 0;
$msg      = '';
$allcount = 0;
$data     = array();
$dataall  = array();
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */
$csv_d    = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}

if ($rtn === 0) {
    if ($licensePivot === true) {
        $allcount = fnGetAllCount($db2con,$userData[0]['WUAUTH'], $WSNAME, $D1TEXT, $PMTEXT, true);
        $data     = fnGetDB2WSCD($db2con, $userData[0]['WUAUTH'],$WSNAME, $D1TEXT, $PMTEXT, true, $start, $length, $sort, $sortDir);
    } else {
        $allcount = fnGetAllCount($db2con,$userData[0]['WUAUTH'], $WSNAME, $D1TEXT, $PMTEXT, false);
        $data     = fnGetDB2WSCD($db2con,$userData[0]['WUAUTH'], $WSNAME, $D1TEXT, $PMTEXT, false, $start, $length, $sort, $sortDir);
    }
    $index = 0;
    foreach ($data as $key => $row) {
        
        if ($row['PMNAME'] !== '' && $row['PMNAME'] !== null) {
            $row['WSPKEY']   = cmMer($row['D1TEXT']) . '(' . cmMer($row['PMTEXT']) . ')';
            $dataall[$index] = $row;
            $index++;
        } else {
            $row['WSPKEY']   = cmMer($row['D1TEXT']);
            $dataall[$index] = $row;
            $index++;
        }
        
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($dataall,true)
);

echo (json_encode($rtn));

/**
 * ユーザーマスター取得
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con  DBコネクション
 * @param String  $start   データの開始位置
 * @param String  $length  データの長さ
 * @param String  $sort    ソートする項目
 * @param String  $sortDir ASC or DESC
 * 
 */

function fnGetDB2WSCD($db2con,$WUAUTH, $WSNAME, $D1TEXT, $PMTEXT, $pflg, $start = '', $length = '', $sort = '', $sortDir = '')
{
    
    $data = array();
    
    $params = array();
    
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* ,C.PMNAME,C.PMTEXT,D.D1TEXT,ROWNUMBER() OVER( ';
    
    if ($sort !== '') {
        
        if ($sort === 'WSODAY') {
            $strSQL .= ' ORDER BY B.WSODAY ' . $sortDir . ',B.WSWDAY ' . $sortDir . ',B.WSMDAY ' . $sortDir . ' ';
        } else {
            $strSQL .= ' ORDER BY B.' . $sort . ' ' . $sortDir . ' ';
        }
    } else {
        $strSQL .= ' ORDER BY B.WSNAME ASC ';
    }
    
    $strSQL .= ' ) as rownum ';
    $strSQL .= ' FROM DB2WSCD AS B ';
	$strSQL .= ' LEFT JOIN ';
	$strSQL .= ' ( ';
	$strSQL .= '	SELECT PMTEXT,PMPKEY,PMNAME ';
	$strSQL .= '	FROM DB2PMST ';
//クエリーグループの最後のクエリーのピボットＪＯＩＮ
	$strSQL .= '	UNION';
	$strSQL .= '	SELECT PMST.PMTEXT,PMST.PMPKEY,QRYGSID AS PMNAME ';
	$strSQL .= '	FROM DB2PMST AS PMST ';
	$strSQL .= '	JOIN(';
	$strSQL .= '		SELECT EMP2.QRYGSQRY,EMP2.QRYGSID';
	$strSQL .= '		FROM DB2QRYGS AS EMP2';
	$strSQL .= '		JOIN ';
	$strSQL .= '		(';
	$strSQL .= '			SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
	$strSQL .= '			FROM DB2QRYGS AS D ';
	$strSQL .= '			GROUP BY QRYGSID';
	$strSQL .= '		) EMP1';
	$strSQL .= '		ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
	$strSQL .= '		JOIN DB2QRYG ON QRYGID=EMP2.QRYGSID ';
	$strSQL .= '	) As DD';
	$strSQL .= '	ON PMST.PMNAME=DD.QRYGSQRY';
	$strSQL .= ' )AS C ';
    $strSQL .= ' ON B.WSNAME = C.PMNAME';
    $strSQL .= ' AND B.WSPKEY = C.PMPKEY ';
    $strSQL .= ' INNER JOIN ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME,D1TEXT ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT ';
		$strSQL .= '		FROM DB2QRYG AS QRYG';
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID) FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN DB2QRYGS AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '		INNER JOIN ( ';
		$strSQL .= '			SELECT WGNAME FROM DB2WGDF ';
		$strSQL .= '			JOIN DB2WUGR ON WGGID=WUGID ';
		$strSQL .= '			WHERE WUUID=?';
		$strSQL .= '		) AS GP ON QRYG.QRYGID=GP.WGNAME';
        $strSQL .= '    ) D ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
		array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME,D1TEXT ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '             ) ';
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT ';
		$strSQL .= '		FROM DB2QRYG AS QRYG';
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID) FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN DB2QRYGS AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
        $strSQL .= '    ) D ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
       // $strSQL .= ' FDB2CSV1 AS D   ';
        $strSQL .= '	( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME,D1TEXT ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME,';
		$strSQL .= '			QRYG.QRYGNAME AS D1TEXT ';
		$strSQL .= '		FROM DB2QRYG AS QRYG';
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID) FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN DB2QRYGS AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '    ) AS D ';
    }
    $strSQL .= ' ON B.WSNAME = D.D1NAME';
    $strSQL .= ' WHERE ';
    if ($pflg === false) {
        $strSQL .= ' WSPKEY = \'\' ';
    } else {
        $strSQL .= ' 1 = 1 ';
    }
    if ($WSNAME != '') {
        $strSQL .= ' AND WSNAME like ? ';
        array_push($params, '%' . $WSNAME . '%');
    }
    
    if ($D1TEXT != '') {
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params, '%' . $D1TEXT . '%');
    }
    if ($PMTEXT != '') {
        $strSQL .= ' AND PMTEXT like ? ';
        array_push($params, '%' . $PMTEXT . '%');
    }
    $strSQL .= ' ) as A ';
    
    //抽出範囲指定
    if (($start != '') && ($length != '')) {
        $strSQL .= ' where A.rownum BETWEEN ? AND ? ';
        array_push($params, $start + 1);
        array_push($params, $start + $length);
    }
    e_log('スケジュールの登録スケジュール：'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        e_log('error1:'.db2_stmt_errormsg());
        $data = false;
    } else {
        e_log('SQL2'.$strSQL.print_r($params,true));
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            e_log('error2:'.db2_stmt_errormsg());
            $data = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
    }
    return $data;
    
}

/**
 * 全件カウント取得
 * 
 * RESULT
 *    01：データ件数
 *    02：false
 * 
 * @param Object  $db2con  DBコネクション
 * 
 */

function fnGetAllCount($db2con,$WUAUTH, $WSNAME, $D1TEXT, $PMTEXT, $pflg)
{
    $rs     = array();
    $params = array();
    $strSQL = ' SELECT ';
    $strSQL .= ' count(*) as COUNT ';
    $strSQL .= ' FROM ';
    $strSQL .= ' DB2WSCD AS A ';
    //$strSQL .= ' LEFT JOIN DB2PMST AS B ';
    $strSQL .= ' LEFT JOIN ';
	$strSQL .= ' ( ';
	$strSQL .= '	SELECT PMTEXT,PMPKEY,PMNAME ';
	$strSQL .= '	FROM DB2PMST ';
//クエリーグループの最後のクエリーのピボットＪＯＩＮ
	$strSQL .= '	UNION';
	$strSQL .= '	SELECT PMST.PMTEXT,PMST.PMPKEY,QRYGSID AS PMNAME ';
	$strSQL .= '	FROM DB2PMST AS PMST ';
	$strSQL .= '	JOIN(';
	$strSQL .= '		SELECT EMP2.QRYGSQRY,EMP2.QRYGSID ';
	$strSQL .= '		FROM DB2QRYGS AS EMP2';
	$strSQL .= '		JOIN';
	$strSQL .= '		(';
	$strSQL .= '			SELECT QRYGSID,MAX(QRYGSSEQ) AS QRYGSSEQ ';
	$strSQL .= '			FROM DB2QRYGS AS D ';
	$strSQL .= '			GROUP BY QRYGSID';
	$strSQL .= '		) EMP1';
	$strSQL .= '		ON EMP2.QRYGSID=EMP1.QRYGSID AND EMP2.QRYGSSEQ=EMP1.QRYGSSEQ ';
	$strSQL .= '	) As DD ';
	$strSQL .= '	ON PMST.PMNAME=DD.QRYGSQRY ';
	$strSQL .= ' )AS B ';
    $strSQL .= ' ON A.WSNAME = B.PMNAME ';
    $strSQL .= ' AND A.WSPKEY = B.PMPKEY ';

//    $strSQL .= ' ON A.WSNAME = B.PMNAME  ';
//    $strSQL .= ' AND A.WSPKEY = B.PMPKEY   ';
    $strSQL .= ' INNER JOIN ';
    if($WUAUTH === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME ';
		$strSQL .= '		FROM DB2QRYG AS QRYG';
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID) FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN DB2QRYGS AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= '		INNER JOIN ( ';
		$strSQL .= '			SELECT WGNAME FROM DB2WGDF ';
		$strSQL .= '			JOIN DB2WUGR ON WGGID=WUGID ';
		$strSQL .= '			WHERE WUUID=?';
		$strSQL .= '		) AS GP ON QRYG.QRYGID=GP.WGNAME';
        $strSQL .= '    ) C ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            D1NAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            D1NAME IN ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS ';
        $strSQL .= '                RIGHT JOIN ';
        $strSQL .= '                    FDB2CSV1 ';
        $strSQL .= '                ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '        ) ';
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME ';
		$strSQL .= '		FROM DB2QRYG AS QRYG';
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID) FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN DB2QRYGS AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';

        $strSQL .= '    ) C ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
		$strSQL .= '(';
        $strSQL .= ' 		SELECT D1NAME FROM FDB2CSV1   ';
		//クエリーグループＵＮＩＯＮ
		$strSQL .= '		UNION ALL';
		$strSQL .= '		SELECT QRYG.QRYGID AS D1NAME ';
		$strSQL .= '		FROM DB2QRYG AS QRYG';
		$strSQL .= '		INNER JOIN (';
		$strSQL .= '			SELECT DISTINCT(AA.QRYGID) FROM DB2QRYG AS AA';
		$strSQL .= '			INNER JOIN DB2QRYGS AS BB ';
		$strSQL .= '			ON AA.QRYGID=BB.QRYGSID';
		$strSQL .= '		) QRYGS ON QRYG.QRYGID=QRYGS.QRYGID';
		$strSQL .= ') AS C';
    }
    $strSQL .= ' ON A.WSNAME = C.D1NAME ';
    $strSQL .= ' WHERE ';
    if ($pflg === false) {
        $strSQL .= '  WSPKEY = \'\' ';
    } else {
        $strSQL .= '  1=1';
    }
    if ($WSNAME != '') {
        $strSQL .= ' AND WSNAME like ? ';
        array_push($params, '%' . $WSNAME . '%');
    }
    
    if ($D1TEXT != '') {
        $strSQL .= ' AND D1TEXT like ? ';
        array_push($params, '%' . $D1TEXT . '%');
    }
    if ($PMTEXT != '') {
        $strSQL .= ' AND PMTEXT like ? ';
        array_push($params, '%' . $PMTEXT . '%');
    }
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        e_log('error11:'.db2_stmt_errormsg());
        $rs = false;
    } else {
        e_log('SQL22'.$strSQL.print_r($params,true));
        $r = db2_execute($stmt,$params);
        if ($r === false) {
            e_log('error12:'.db2_stmt_errormsg());
            $rs = false;
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $rs[] = $row['COUNT'];
            }
            $rs = $rs[0];
        }
    }
    return $rs;
}