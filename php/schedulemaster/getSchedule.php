<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WSNAME = $_POST['WSNAME'];
$WSPKEY = $_POST['WSPKEY'];
$WSQGFLG = $_POST['WSQGFLG'];
$WSFRQ  = $_POST['WSFRQ'];
$WSODAY = $_POST['WSODAY'];
$WSWDAY = $_POST['WSWDAY'];
$WSMDAY = $_POST['WSMDAY'];
$WSTIME = $_POST['WSTIME'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$allcount = 0;
$data = array();
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'17',$userData[0]['WUSAUT']);//'1' => scheduleMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('スケジュールの権限'));
            }
        }
    }
}
//クエリーグループ
if($rtn===0){
	if($WSQGFLG==='1'){
        $rs=cmGetQueryGroup($db2con,$WSNAME,$WSPKEY,'');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリーグループ'));
        }else{
            $D1NAMELIST=umEx($rs['data'],true);
        }
	}
}
//クエリーグループ
//クエリー存在チェック
if($rtn === 0){
	if($WSQGFLG==='1'){
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            $chkQry = cmChkQueryGroup($db2con,'',$WSNAME,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 3;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 3;
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }
        }//end of for loop
	}else{
	    $chkQry = array();
	    $chkQry = cmChkQuery($db2con,'',$WSNAME,$WSPKEY);
	    if($chkQry['result'] === 'NOTEXIST_PIV'){
	        $rtn = 3;
	        $msg = showMsg('NOTEXIST_GET',array('ピボット'));
	    }else if($chkQry['result'] !== true){
	        $rtn = 3;
	        $msg = showMsg($chkQry['result'],array('クエリー'));
	    }
	}
}
if($rtn === 0){
    $rs = fnGetDB2WSCD($db2con,$WSNAME,$WSPKEY,$WSFRQ,$WSODAY,$WSWDAY,$WSMDAY,$WSTIME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('スケジュル'));
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);
/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData' => umEx($data)
);

echo(json_encode($rtn));

/**
* ユーザーマスター取得
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $WANAME 定義名
* @param String  $WSPKEY PIVOTKEY
* @param String  $WSFRQ  実行頻度
* @param String  $WSODAY スケジュール-日
* @param String  $WSWDAY スケジュール-週
* @param String  $WSMDAY スケジュール-月
* @param String  $WSTIME 実行時分
* 
*/

function fnGetDB2WSCD($db2con,$WSNAME,$WSPKEY,$WSFRQ,$WSODAY,$WSWDAY,$WSMDAY,$WSTIME){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WSCD as A ';
    $strSQL .= ' WHERE WSNAME <> \'\' ';
    $strSQL .= ' AND WSNAME = ? ';
    $strSQL .= ' AND WSPKEY = ? ';
    $strSQL .= ' AND WSFRQ  = ? ';
    $strSQL .= ' AND WSODAY = ? ';
    $strSQL .= ' AND WSWDAY = ? ';
    $strSQL .= ' AND WSMDAY = ? ';
    $strSQL .= ' AND WSTIME = ? ';

    $params = array(
        $WSNAME,
        $WSPKEY,      
        $WSFRQ,
        $WSODAY,
        $WSWDAY,
        $WSMDAY,
        $WSTIME
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' =>'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
           $data = array('result' =>'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $data = array('result' =>'NOTEXIST_GET');
            }else{
                $data = array('result' =>true,'data'=> $data);
            }
        }
    }

    return $data;

}