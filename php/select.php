<?php

/**
* ユーザーごとの定義のカラム情報を取得
* ※呼ばれるごとにFDB2CSV1にない定義を削除
**/

include_once('common/inc/config.php');
include_once('common/inc/common.inc.php');


$userId = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
$d1name = $_POST['tableName'];

//POSTしていないのでコメント
//$json = $_POST['columns'];

$rtn = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//FDB2CSV1にない定義をDB2WCOLから削除
//↓5250クエリーの対応なので今後は削除
//fnDeleteDB2WCOL($db2con);

//カラム情報取得
$DB2WCOL = cmGetDB2WCOL($db2con,$userId,$d1name);

if(count($DB2WCOL) > 0){

    $DB2WCOL = umEx($DB2WCOL);

    $rtn = cmGetJsonDB2WCOL($DB2WCOL);

}else{
    $rtn = '[]';
}

echo($rtn);

/*
function fnDeleteDB2WCOL($db2con){

	$rs = '0';

	//構文
	$strSQL  = ' DELETE FROM DB2WCOL ';
	$strSQL .= ' WHERE WCNAME NOT IN ';
    $strSQL .= ' (SELECT D1NAME FROM FDB2CSV1) ';

	$params = array();

	$stmt = db2_prepare($db2con,$strSQL);
    if ($stmt === false){
	    $rs = '1';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === fasle){
            $rs = '1';
        }
    }
	return $rs;

}
*/