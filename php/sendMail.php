<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$ver = phpversion();
$sver = substr($ver,0,3);
$phpversion = str_replace('.','',$sver);
if((int)$phpversion >= 54){
	require("common/lib/PHPMailer/PHPMailerAutoload.php");
}else{
	require("common/lib/@PHPMailer/class.phpmailer.php");
}

e_log('AAAEmail');

$rtn = 0;
$msg = '';
$success = true;
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

$SMAILTO = (isset($_POST['SMAIL'])) ? $_POST['SMAIL'] : '';
$data       = json_decode($_POST['DATA'],true);

$secure = 'ssl';
/*$host = '74.125.203.108';
$port = 465;
$gmailAccount = 'mailerphpquery@gmail.com';
$gmailAccountPassword = 'mailerphpquery00';
$from = 'mailerphpquery@gmail.com';
*/
//$host= '192.168.1.12';
//$port = 25;
//$to = 'may_mu_khaing@omni-s.co.jp';
	if($data['mailflg'] === '1'){
		//チェック
		if ($rtn === 0) {
		    if (cmMer($data['WAMAIL']) === "") {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_REQ',array('アドレス、メールグループ'));// 'アドレス、メールグループは入力必須です。';
		        $focus = 'WAMAIL';
		    }
		}

		//バリデーションチェック
		if ($rtn === 0) {
		    $byte = checkByte($data['WAMAIL']);
		    if ($byte[1] > 0) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_BYTE',array('アドレス、メールグループ'));//'アドレス、メールグループは半角文字で入力してください。';
		        $focus = 'WAMAIL';
		    }
		}

		if ($rtn === 0) {
		    if (!checkMaxLen($data['WAMAIL'], 256)) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_MAXLEN',array('アドレス、メールグループ'));//'アドレス、メールグループが入力桁数を超えています。';
		        $focus = 'WAMAIL';
		    }
		}

		/*
		*メールアドレス
		*メールグループに入ってるかメール形になってるかのチェック
		*
		*/
		if($rtn === 0){
		    $WAMAIL = $data['WAMAIL'];
		 $mailAry = explode(";",$WAMAIL);
		   foreach($mailAry as $value){
		        $gpFlg = false;
		        $value = cmMer($value);
		        if($value !== ''){
		            $gValue = trim($value);
		             $rs = chkMailGroup($db2con,$gValue);
		             if($rs['result'] !== true){
		                    $rtn = 1;
		                    $msg = showMsg($rs['result']);
		             }else{
		                    if($rs['data'] === 'NOGPEXIST'){
		                            $mValue = trim($value);
		                           $rs =  cmMailFormatChk($mValue);
		                            if($rs === false){
		                                $rtn = 1;
		                                $msg = showMsg('FAIL_MAIL',array('アドレス、メールグループ'));//'アドレス、メールグループはメールグループに入ってないかメールアドレスが正しくありません。';
		                                $focus = 'MAMAIL';
		                            }
		                    }
		             }
		        }
		    }
		}

		//バリデーションチェック
		if ($rtn === 0) {
		    $byte = checkByte($data['WAMACC']);
		    if ($byte[1] > 0) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_BYTE',array('CC'));//'CCアドレスは半角文字で入力してください。';
		        $focus = 'WAMACC';
		    }
		}

		if ($rtn === 0) {
		    if (!checkMaxLen($data['WAMACC'], 256)) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_MAXLEN',array('CC'));//'CCアドレスが入力桁数を超えています。';
		        $focus = 'WAMACC';
		    }
		}

		/*
		*ＣＣメールアドレス
		*メールグループに入ってるかメール形になってるかのチェック
		*
		*/
		if($rtn === 0){
		 $WAMACC = $data['WAMACC'];
		 $mailAry = explode(";",$WAMACC);
		   foreach($mailAry as $value){
		        $gpFlg = false;
		        $value = cmMer($value);
		        if($value !== ''){
		            $gValue = trim($value);
		             $rs = chkMailGroup($db2con,$gValue);
		             if($rs['result'] !== true){
		                    $rtn = 1;
		                    $msg = showMsg($rs['result']);
		             }else{
		                    if($rs['data'] === 'NOGPEXIST'){
		                            $mValue = trim($value);
		                           $rs =  cmMailFormatChk($mValue);
		                            if($rs === false){
		                                $rtn = 1;
		                                $msg = showMsg('FAIL_MAIL',array('CC'));//'CCアドレスはメールグループに入ってないかメールアドレスが正しくありません。';
		                                $focus = 'MAMAIL';
		                            }
		                    }
		             }
		        }
		    }
		}
		if($WASFLG === '1'){
		        if ($rtn === 0) {
		               $WAFRAD = trim($data['WAFRAD']);
		              if ($WAFRAD !== '' ) {
		                   $rs =  cmMailFormatChk($WAFRAD);
		                    if($rs === false){
		                        $rtn = 1;
		                        $msg = showMsg('FAIL_CHK',array('差出人アドレス'));//'アドレス、メールグループはメールグループに入ってないかメールアドレスが正しくありません。';
		                        $focus = 'WAFRAD';
		                    }
		              }
		        }
		}
		if ($rtn === 0) {
		    $byte = checkByte($data['WAFRAD']);
		    if ($byte[1] > 0) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_BYTE',array('差出人アドレス'));//'差出人アドレスは半角文字で入力してください。';
		        $focus = 'WAFRAD';
		    }
		}
		if ($rtn === 0) {
		    if (!checkMaxLen($data['WAFRAD'], 256)) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_MAXLEN',array('差出人アドレス'));//'差出人アドレスが入力桁数を超えています。';
		        $focus = 'WAFRAD';
		    }
		}

		if ($rtn === 0) {
		    if (!checkMaxLen($data['WAFRNM'], 128)) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_MAXLEN',array('差出人名'));//'差出人名が入力桁数を超えています。';
		        $focus = 'WAFRNM';
		    }
		}

		if ($rtn === 0) {
		    if (!checkMaxLen($data['WASUBJ'], 128)) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_MAXLEN',array('メールタイトル'));//'メールタイトルが入力桁数を超えています。';
		        $focus = 'WASUBJ';
		    }
		}
		if($WASFLG === '1'){
		    if ($rtn === 0) {
		        if (cmMer($data['WABODY']) === '') {
		            $rtn   = 1;
		            $msg = showMsg('FAIL_REQ',array('本文'));
		            $focus = 'WABODY';
		        }
		    }
		}
		if ($rtn === 0) {
		    if (!checkMaxLen($data['WABODY'], 1536)) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_MAXLEN',array('本文'));//'本文が入力桁数を超えています。';
		        $focus = 'WABODY';
		    }
		}
		if ($rtn === 0) {
		    if (!checkMaxLen($WAQUNM, 100)) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_MAXLEN',array('ファイル名'));//'ファイル名が入力桁数を超えています。';
		        $focus = 'WAQUNM';
		    }
		}
		//バリデーションチェック
		if ($rtn === 0) {
		    $byte = checkByte($WAQUNM);
		    if ($byte[1] > 0) {
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_BYTE',array('ファイル名'));//'ファイル名は半角文字で入力してください。';
		        $focus = 'WAQUNM';
		    }
		}
		if ($rtn === 0) {
		   if(strlen($WAQUNM) != strlen(utf8_decode($WAQUNM))){
		        $rtn   = 1;
		        $msg   = showMsg('FAIL_KANA',array(''));//'半角カタカナの入力はできません。';
		    }
		}
	}
if($rtn === 0){
    $rs = sendMailTest($db2con,$data,$SMAILTO);
    //e_log('Mail Success'.print_r($rs['result'],true));
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg('FAIL_MAILNSENT');
        $msg .= ' '.$rs['msg'];
    }
}

cmDb2Close($db2con);

$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'mail' => $SMAILTO,
    'data' =>$data
  
);
//e_log('戻り値:'.print_r($rtnArray,true));


echo(json_encode($rtnArray)); 

/****
*
*メールグループ名に入ってるかのチェック
*
**/
function chkMailGroup($db2con,$MGNAME){
    if(!checkMaxLen($MGNAME,10)){
           $data = array('result' => true , 'data' => 'NOGPEXIST'); 
    }else{
            $data = array();
            $strSQL = "SELECT * FROM DB2MAGP WHERE MGNAME = ?";
            error_log($strSQL.$MGNAME);
            $params = array($MGNAME);
            $stmt = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
               $data = array('result' => 'FAIL_SEL');
            } else {
                $r = db2_execute($stmt, $params);
                
                if ($r === false) {
                    $data = array('result' => 'FAIL_SEL');
                } else {
                    while ($row = db2_fetch_assoc($stmt)) {
                        $data[] = $row;
                    }
                    if(count($data)=== 0){
                       $data = array('result' => true , 'data' => 'NOGPEXIST'); 
                    }else{
                       $data = array('result' => true,'data' => 'GPEXIST');
                    }
                 e_log(print_R($data,true));
                }
            }
        }
    return $data;
}

function sendMailTest($db2con,$data,$SMAILTO)
{
    $rs = true;
    $msg = '';
    //メール送信処理
    mb_language('japanese');
    mb_internal_encoding('UTF-8');
    $mail = new PHPMailer(); // create a new object
    $mail->CharSet  = 'iso-2022-jp';
    $mail->Encoding = '7bit';
    $mail->IsSMTP(); // enable SMTP
    //$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
	if($data['mailflg'] !== '1'){
		$host = $data['WMHOST'];
		$port = $data['WMPORT'];
		$gmailAccount = $data['WMUSER'];
		$gmailAccountPassword = $data['WMPASE'];

        if($gmailAccount !== ''){
            $mail->SMTPAuth = TRUE; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
        }

/*
		if($data['WMFRNM'] !== ''){
			$from = $data['WMFRNM'];
		}else if($data['WMFRAD'] !== ''){
			$from = $data['WMFRAD'];
		}else{
			$from = $data['WMUSER'];
		}
*/

        $from = $data['WMFRAD'];
        $fromName = $data['WMFRNM'];

		$subj = $data['WMSUBJ'];
		$body = $data['WMBODY'];
	    $mail->AddAddress($SMAILTO);
	}else{
		$mdata = getDB2WMAL($db2con);
		$mdata = $mdata['data'][0];
		$host = cmMer($mdata['WMHOST']);
		$port = cmMer($mdata['WMPORT']);
		$gmailAccount = cmMer($mdata['WMUSER']);
		$gmailAccountPassword = cmMer($mdata['WMPASE']);

        if($gmailAccount !== ''){
            $mail->SMTPAuth = TRUE; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
        }

/*
		if($data['WAFRNM'] !== ''){
			$from = $data['WAFRNM'];
		}else if($data['WAFRAD'] !== ''){
			$from = $data['WAFRAD'];
		}else{
			$from = $data['WMUSER'];
		}
*/
        $from = $data['WAFRAD'];
        $fromName = $data['WAFRNM'];


		$subj = $data['WASUBJ'];
		$body = $data['WABODY'];
    	$address = $data['WAMAIL'];
	    $ccAddress = $data['WAMACC'];
      
	    $mailarr = explode(";", $address);
		e_log("mailarr".print_r($mailarr,true));
	    foreach ($mailarr as $key => $value) {
	        $data = chkDB2MAGP($db2con, $value);
	        if ($data['result'] === true) {
	            if (count($data['data']) > 0) {
	                foreach ($data['data'] as $key => $value) {
	                    $mail->AddAddress(cmMer($value['MAMAIL'])); //宛先
	                    
	                }
	            } else {
	                $mail->AddAddress(cmMer($value)); //宛先
					e_log("mail".$value);
	            }
	        } else {
	            e_log(showMsg($data['result'], array('exeMail.php', 'chkDB2MAGP')), '1');
	        }
	    }
	    /**********************************************************/
	    // CC アドレスのセット
	    /**********************************************************/
	    if ($ccAddress !== null && $ccAddress !== '') {
	        $ccmailarr = explode(";", $ccAddress);
	        foreach ($ccmailarr as $key => $value) {
	            $data = chkDB2MAGP($db2con, $value);
	            if ($data['result'] === true) {
	                if (count($data['data']) > 0) {
	                    foreach ($data['data'] as $key => $value) {
	                        $mail->AddCC(cmMer($value['MAMAIL'])); //宛先
	                        
	                    }
	                } else {
	                    $mail->AddCC(cmMer($value)); //宛先
	                    
	                }
	            } else {
	                e_log(showMsg($data['result'], array('exeMail.php', 'chkDB2MAGP')), '1');
	            }
	        }
	    }
	//	e_log($host.' '.$port.' '.$gmailAccount.' '.$gmailAccountPassword.' '.$from.' '.$body);
	//	e_log("mddata".print_r($mdata,true));
	}
    $mail->Host = $host;
    $mail->Port = $port; // or 587
    //$mail->IsHTML(true);
    $mail->Username = $gmailAccount;
    $mail->Password = $gmailAccountPassword;
    $mail->SetFrom($from);
    $mail->FromName = mb_encode_mimeheader($fromName, 'JIS');
    $mail->Subject = mb_encode_mimeheader($subj, 'JIS');
    $mail->Body = mb_convert_encoding($body, 'ISO-2022-JP-MS');
    
    $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
   // $mailrs   = $mail->Send();
    if(!$mail->Send()){
		e_log($mail->ErrorInfo, '1');
        $msg = $mail->ErrorInfo;
        $rs = false;
    }else{
        $rs = true;
    } 
   return array('result' => $rs,'msg' => $msg);
}

/**return**/
/*$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'mail' => $SMAILTO
  
);
//e_log('戻り値:'.print_r($rtnArray,true));


echo(json_encode($rtnArray));   */

/*
 *-------------------------------------------------------*
 * DB2WMAL取得カラム
 *-------------------------------------------------------*
*/
function getDb2wmalColumns($WMPASS) {
    $strwmpass = '';
    if ($WMPASS !== '') {
        $strwmpass.= ' DECRYPT_CHAR(A.WMPASE) AS WMPASE, ';
    } else {
        $strwmpass = 'A.WMPASS AS WMPASE,';
    }
    $strSQL = ' A.WMHOST, ';
    $strSQL.= ' A.WMPORT, ';
    $strSQL.= ' A.WMUSER, ';
    $strSQL.= $strwmpass;
    $strSQL.= ' A.WMFRAD, ';
    $strSQL.= ' A.WMFRNM, ';
    $strSQL.= ' A.WMSUBJ, ';
    $strSQL.= ' A.WMBODY ';
    return $strSQL;
}
/*
 *-------------------------------------------------------*
 * メール設定取得
 *-------------------------------------------------------*
*/
function getDB2WMAL($db2con) {
    $data = array();
    $rtn = array();
    $WMPASS = '';
    $rs = fnGetWMPASS($db2con);
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array('パスワード'));
    } else {
        $WMPASS = cmMer($rs['WMPASS']);
    }
    if ($WMPASS !== '') {
        $sql = 'SET ENCRYPTION PASSWORD = \'' . RDB_KEY . '\'';
        db2_exec($db2con, $sql);
    }
    $strSQL = ' SELECT ';
    $strSQL.= getDb2wmalColumns($WMPASS);
    $strSQL.= ' FROM DB2WMAL AS A ';
    $params = array();
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}

function fnGetWMPASS($db2con) {
    $params = array();
    $WMPASS = array();
    $strSQL = ' SELECT A.WMPASS ';
    $strSQL.= ' FROM DB2WMAL AS A ';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $WMPASS[] = $row;
            }
            $WMPASS = $WMPASS[0]['WMPASS'];
            $data = array('result' => true, 'WMPASS' => $WMPASS);
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------*
 * メールグループについているかをチェック
 *-------------------------------------------------------*
*/
function chkDB2MAGP($db2con, $pName) {
    $rtn = array();
    $data = array();
    $strSQL = ' SELECT *';
    $strSQL.= ' FROM DB2MAAD ';
    $strSQL.= ' WHERE MANAME = ? ';
    $params = array($pName);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $rtn = array('result' => 'FAIL_SQL');
    } else {
        $rt = db2_execute($stmt, $params);
        if ($rt === false) {
            $rtn = array('result' => 'FAIL_SQL');
        } else {
            if ($stmt) {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
            $rtn = array('result' => true, 'data' => $data);
        }
    }
    return $rtn;
}