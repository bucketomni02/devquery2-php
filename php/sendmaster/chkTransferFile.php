<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$success = true;
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$filepath = $filename = (isset($_POST['transferFile']))?$_POST['transferFile']:'';
$PROC  = (isset($_POST['PROC']))?$_POST['PROC']:'';
$TSFILLIB = $_POST['TSFILLIB'];
$TSFILE = $_POST['TSFILE'];
$TSFILMBR = $_POST['TSFILMBR'];
$TSTYPE = (isset($_POST['TSTYPE']))?$_POST['TSTYPE']:'';
$TSLIBLIST = json_decode($_POST['TSLIBLIST'], true);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'33',$userData[0]['WUSAUT']);//'33' => Import
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('インポートの権限'));
            }
        }
    }
}
if ($rtn === 0) {
    $LIBSNM = cmMer($userData[0]['WULIBL']);
}
$RDBNM = (isset($_SESSION['PHPQUERY']['RDBNM']) ? $_SESSION['PHPQUERY']['RDBNM'] : '');
if ($RDBNM !== '') {
    $LIBSDB = $RDBNM;
} else {
    $LIBSDB = RDB;
}

if($rtn === 0){
    if(cmMer($filename) === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_SET',array('転送ファイル'));
        $focus = 'transferFile';
    }
}
if($rtn === 0){
    if(strripos($filename, '\\') !== false) {
        $filename =substr($filename,12);
    }
    $ext = explode ('.', $filename);
    $ext = $ext [count ($ext) - 1];
    if($ext === 'csv' || $ext === 'xlsx' || $ext === 'xls'){
        $rtn = 0;
    }else{
        $rtn = 1;
        $msg = showMsg('CHK_FMT',array('転送するファイル',array('ｃｓｖまたはEXCEL','ファイル')));
        $focus = 'transferFile';
    }
}
if($rtn === 0){
    if($TSFILLIB === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('候補ライブラリー'));
        $focus = 'TSFILLIB';
    }
}

if ($RDBNM === '') {
    if ($rtn === 0) {
        if ($TSFILLIB === '' || strtoupper($TSFILLIB) === '*LIBL' || strtoupper($TSFILLIB) === '*USRLIBL') {
            $TSFILLIB = strtoupper($TSFILLIB);
        } else {
            $rs = chkLIB($db2con, $TSFILLIB, $TSLIBLIST, $LIBSNM, $LIBSDB);
            if ($rs !== 0) {
                $rtn = 1;
                $msg = showMsg($rs, array(array( 'ライブラリー', '名')));
                $focus = 'TSFILLIB';
            }
        }
    }
    //プライマリ.ファイルチェック
    if ($rtn === 0) {
        $rs = chkFile($db2con, $TSFILLIB, $TSFILE, $TSLIBLIST);
		//e_log('Library check result***'.print_r($rs['result']));
        if ($rs['result'] !== 0) {
            $rtn = 1;
            error_log('file result false');
            $msg = showMsg($rs['result'], array(array( 'ファイル', '名')));
            $focus = 'TSFILE';
        } else {
            $PRYFILINFO = $rs['data'];
            $fileinfo = array('SYSTEM_TABLE_SCHEMA' => $PRYFILINFO['SYSTEM_TABLE_SCHEMA'], 'SYSTEM_TABLE_NAME' => $PRYFILINFO['SYSTEM_TABLE_NAME'], 'TABLEID' => 'P');
            $FILELIST[$fileinfo['TABLEID']] = $fileinfo;
        }
    }
    // プライマリ．メンバーチェック
    if ($rtn === 0) {
        if ($TSFILMBR === '' || strtoupper($TSFILMBR) === '*FIRST' || strtoupper($TSFILMBR) === '*LAST') {
            $TSFILMBR = strtoupper($TSFILMBR);
        } else {
            $rs = chkMBR($db2con, $fileinfo['SYSTEM_TABLE_SCHEMA'], $fileinfo['SYSTEM_TABLE_NAME'], $TSFILMBR);
            if ($rs['result'] !== 0) {
                $rtn = 1;
                $msg = showMsg($rs['result'], array(array( 'メンバー', '名')));
                $focus = 'TSFILMBR';
            }
        }
    }
}else{
    $db2conRDB = cmDB2ConRDB();
    if ($rtn === 0) {
        if ($TSFILLIB === '' || strtoupper($TSFILLIB) === '*LIBL' || strtoupper($TSFILLIB) === '*USRLIBL') {
            $TSFILLIB = strtoupper($TSFILLIB);
        } else {
            $rs = chkLIB($db2conRDB, $TSFILLIB, $TSLIBLIST, $LIBSNM, $LIBSDB);
            if ($rs !== 0) {
                $rtn = 1;
                $msg = showMsg($rs, array(array( 'ライブラリー', '名')));
                $focus = 'TSFILLIB';
            }
        }
    }
    //プライマリ.ファイルチェック
    if ($rtn === 0) {
        $rs = chkFile($db2conRDB, $TSFILLIB, $TSFILE, $TSLIBLIST);
        if ($rs['result'] !== 0) {
            $rtn = 1;
            error_log('file result false12121');

            $msg = showMsg($rs['result'], array(array( 'ファイル', '名')));
            $focus = 'TSFILE';
        } else {
            $PRYFILINFO = $rs['data'];
            $fileinfo = array('SYSTEM_TABLE_SCHEMA' => $PRYFILINFO['SYSTEM_TABLE_SCHEMA'], 'SYSTEM_TABLE_NAME' => $PRYFILINFO['SYSTEM_TABLE_NAME'], 'TABLEID' => 'P');
        }
    }
    // プライマリ．メンバーチェック
    if ($rtn === 0) {
        if ($TSFILMBR === '' || strtoupper($TSFILMBR) === '*FIRST' || strtoupper($TSFILMBR) === '*LAST') {
            // 処理なし
            
        } else {
            $rs = chkMBR($db2conRDB, $fileinfo['SYSTEM_TABLE_SCHEMA'], $fileinfo['SYSTEM_TABLE_NAME'], $TSFILMBR);
            if ($rs['result'] !== 0) {
                $rtn = 1;
                $msg = showMsg($rs['result'], array(array( 'メンバー', '名')));
                $focus = 'TSFILMBR';
            }
        }
    }
}
/*if($rtn === 0){
    error_log('file path = '.print_r($_FILES,true));
    //実行先のファイルのデリミタ
    $delimiter = "\t";
    //$delimiter = ",";
    // ファイルポインタを開く
    if (($handle = fopen($filepath, "r")) !== FALSE) {
        $fieldInfo = array();
        $row = 0;
        while (($data = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
            $num = count($data);
            $row++;
            $fieldData = array();
            for ($c=0; $c < $num; $c++) {
                $fieldData[] = $data[$c];
            }
             $fieldInfo[$row] = $fieldData;
        }
        fclose($handle);
        //echo('dataarray'.'<br>'.print_r($fieldInfo,true));
        $lanArr = array();
        $frmArr= array();
        $fieldArr= array();
        $msgArr = array();
        $lanSEQ = -1;
        error_log('FIELD INTO = '.print_r($fieldInfo,true));
        foreach($fieldInfo as $rowno => $lines){
            
        }
        //echo('実行ファイル：'.$filename);
    }
}*/

/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'transferFile' => $filename,
    'PROC'   => $PROC,
    'FOCUS' => $focus,
    'FILE'=>$_FILES,
    'TSFILLIB' => $TSFILLIB,
    'TSFILE' => $TSFILE
);
//e_log('戻り値:'.print_r($rtnArray,true));


echo(json_encode($rtnArray));
function chkLIB($db2con, $LIB, $LIBLIST, $LIBSNM, $LIBSDB) {
    $rtn = 0;
    if ($rtn === 0) {
        if (!checkMaxLen($LIB, 10)) {
            $rtn = 'FAIL_MAXLEN';
        }
    }
    if ($rtn === 0) {
        $byte = checkByte($LIB);
        if ($byte[1] > 0) {
            $rtn = 'FAIL_BYTE';
        }
    }
    if ($rtn === 0) {
        $rs = fnChkExistLib($db2con, $LIB, $LIBLIST, $LIBSNM, $LIBSDB);
        if ($rs === true) {
            $rtn = 'NOTEXIST';
        }
    }
    return $rtn;
}
function fnChkExistLib($db2con, $LIB, $LIBLIST, $LIBSNM, $LIBSDB) {
    $data = array();
    $params = array();
    $rs = true;
    if (($key = array_search('QTEMP', $LIBLIST)) !== false) {
        unset($LIBLIST[$key]);
    }
    if ($LIB === 'QTEMP') {
        $rs = 'ISEXIST';
    } else {
        $strSQL = ' SELECT A.SCHEMA_NAME ';
        $strSQL.= ' FROM  ';
        $strSQL.= '    ( ';
        $strSQL.= '        SELECT ';
        $strSQL.= '        SYSTEM_SCHEMA_NAME SCHEMA_NAME ';
        $strSQL.= '        , SCHEMA_TEXT ';
        $strSQL.= '        FROM ';
        $strSQL.= '        ' . SYSSCHEMASLIB . '/SYSSCHEMAS ';
        $strSQL.= '        WHERE SYSTEM_SCHEMA_NAME <> \'QTEMP\'';
        if ($LIBSNM !== '') {
            if (SYSLIBCHK === '1') {
                $strSQL.= ' AND SYSTEM_SCHEMA_NAME IN (SELECT LIBSID FROM DB2LBLS WHERE LIBSNM = ? AND LIBSDB = ?)';
                array_push($params, $LIBSNM, $LIBSDB);
            } else {
                $strSQL.= ' AND SYSTEM_SCHEMA_NAME NOT IN (SELECT LIBSID FROM DB2LBLS WHERE LIBSNM = ? AND LIBSDB = ?)';
                array_push($params, $LIBSNM, $LIBSDB);
            }
        }
        $strSQL.= '    ) A ';
        if ($LIB === '*USRLIBL' || $LIB === '*LIBL') {
            $strSQL.= ' WHERE A.SCHEMA_NAME IN  (';
            for ($i = 0;$i < count($LIBLIST);$i++) {
                $strSQL.= ' ? ,';
                array_push($params, $LIBLIST[$i]);
            }
            $strSQL = substr($strSQL, 0, -1);
            $strSQL.= ' )';
        } else {
            $strSQL.= ' WHERE A.SCHEMA_NAME = ? ';
            array_push($params, $LIB);
        }
        e_log('comChkBaseQryFileInfo.php⇒' . $strSQL . print_r($params, true));
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $rs = 'FAIL_SEL';
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $rs = 'FAIL_SEL';
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
                if ($LIB === '*USRLIBL' || $LIB === '*LIBL') {
                    if (count($data) === count($LIBLIST)) {
                        $rs = 'ISEXIST';
                    }
                } else {
                    if (count($data) > 0) {
                        $rs = 'ISEXIST';
                    }
                }
            }
        }
    }
    return $rs;
}
function chkFile($db2con, $LIB, $FILE, $LIBLIST) {
    $rtn = array('result' => 0);
    if ($rtn['result'] === 0) {
        if ($FILE === '') {
            $rtn = array('result' => 'FAIL_REQ');
        }
    }
    if ($rtn['result'] === 0) {
        if (!checkMaxLen($FILE, 10)) {
            $rtn = array('result' => 'FAIL_MAXLEN');
        }
    }
    if ($rtn['result'] === 0) {
        $byte = checkByte($FILE);
        if ($byte[1] > 0) {
            $rtn = array('result' => 'FAIL_BYTE');
        }
    }
    if ($rtn['result'] === 0) {
        $rs = fnChkExistFile($db2con, $LIB, $FILE, $LIBLIST);
        if ($rs['result'] === true) {
            $rtn = array('result' => 'NOTEXIST');
        } else {
            $rtn = array('result' => 0, 'data' => $rs['data'][0]);
        }
    }
    return $rtn;
}
function chkMBR($db2con, $LIB, $FILE, $MBR) {
    $rtn = array('result' => 0);
    if ($rtn['result'] === 0) {
        if (!checkMaxLen($MBR, 10)) {
            $rtn = array('result' => 'FAIL_MAXLEN');
        }
    }
    if ($rtn['result'] === 0) {
        $byte = checkByte($MBR);
        if ($byte[1] > 0) {
            $rtn = array('result' => 'FAIL_BYTE');
        }
    }
    if ($rtn['result'] === 0) {
        $rs = fnChkExistMBR($db2con, $LIB, $FILE, $MBR);
        if ($rs['result'] === true) {
            $rtn = array('result' => 'NOTEXIST');
        } else {
            $rtn = array('result' => 0, 'data' => $rs['data'][0]);
        }
    }
    return $rtn;
}
function fnChkExistMBR($db2con, $LIB, $FILE, $MBR) {
    $data = array();
    $params = array();
    $strSQL.= '    SELECT ';
    $strSQL.= '        A.SYS_DNAME, ';
    $strSQL.= '        A.SYS_TNAME, ';
    $strSQL.= '        A.SYS_MNAME, ';
    $strSQL.= '        A.PARTNBR, ';
    $strSQL.= '        A.LABEL ';
    $strSQL.= '    FROM ';
    $strSQL.= '        QSYS2/SYSPSTAT A';
    $strSQL.= '    WHERE ';
    $strSQL.= '        A.TABLE_SCHEMA = ? ';
    $strSQL.= '    AND A.TABLE_NAME = ? ';
    $strSQL.= '    AND A.SYS_MNAME = ?';
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
        e_log(db2_stmt_errormsg() . 'メンバーデータ取得：' . $strSQL);
    } else {
        $params = array($LIB, $FILE, $MBR);
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array('result' => 'FAIL_SEL');
            e_log(db2_stmt_errormsg() . 'メンバーデータ取得：' . $strSQL . print_r($params, true));
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
        }
        if (count($data) === 0) {
            $data = array('result' => true);
        } else {
            $data = array('result' => 'ISEXIST', 'data' => $data);
        }
    }
    return $data;
}
function fnChkExistFile($db2con, $LIB, $FILE, $LIBLIST) {
    $data = array();
    $params = array();
    $strSQL = ' SELECT A.SYSTEM_TABLE_NAME ';
    $strSQL.= '      , A.SYSTEM_TABLE_SCHEMA ';
    $strSQL.= '      , A.TABLE_TEXT';
    $strSQL.= ' FROM  ';
    $strSQL.= '    QSYS2/SYSTABLES A ';
    $strSQL.= ' WHERE A.SYSTEM_TABLE_SCHEMA = ? ';
    $strSQL.= ' AND A.SYSTEM_TABLE_NAME = ? ';
    $stmt = db2_prepare($db2con, $strSQL);
    error_log('check exist prepare '.$strSQL);

    if ($stmt === false) {
        $data = array('result' => 'FAIL_SEL');
    } else {
        if ($LIB === '*USRLIBL' || $LIB === '*LIBL') {
            foreach ($LIBLIST as $value) {
                error_log('check exist file = '.$strSQL.print_r($params,true));
                $params = array($value, $FILE);
                $r = db2_execute($stmt, $params);
                if ($r === false) {
                    $data = array('result' => 'FAIL_SEL');
                } else {
                    while ($row = db2_fetch_assoc($stmt)) {
                        $data[] = $row;
                    }
                    if (count($data) > 0) {
                        break;
                    }
                }
            }
        } else {
            $params = array($LIB, $FILE);
            error_log('check exist file = '.$strSQL.print_r($params,true));
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $data = array('result' => 'FAIL_SEL');
            } else {
                while ($row = db2_fetch_assoc($stmt)) {
                    $data[] = $row;
                }
            }
        }
        if (count($data) === 0) {
            $data = array('result' => true);
        } else {
            $data = array('result' => 'ISEXIST', 'data' => $data);
        }
    }
    return $data;
}