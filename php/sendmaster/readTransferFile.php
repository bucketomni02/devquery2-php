<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
//include_once ('../common/lib/spout/src/Spout/Autoloader/autoload.php');
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
include_once ("../common/lib/PHPExcel/Classes/PHPExcel.php");
include_once ("../common/lib/PHPExcel/Classes/PHPExcel/IOFactory.php");
include_once ("../common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");
include_once ("../common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php");
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$transferFile = (isset($_POST['transferFile'])) ? $_POST['transferFile'] : '';
$TSFILLIB = $_POST['TSFILLIB'];
$TSFILE = $_POST['TSFILE'];
$TSFILMBR = $_POST['TSFILMBR'];
$TSTYPE = (isset($_POST['TSTYPE']))?$_POST['TSTYPE']:'';
$TSLIBLIST = json_decode($_POST['TSLIBLIST'], true);
$success = true;
$num = 0;
$data = $coltype = $collen = array();
$msg = '';
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rtn = 0;
$csvblankcheck = '0';
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    } else {
        $userData = umEx($rs['data']);
        if ($userData[0]['WUAUTH'] === '2') {
            $rs = cmChkKenGen($db2con, '33', $userData[0]['WUSAUT']); //'33' => Import
            if ($rs['result'] !== true) {
                $rtn = 2;
                $msg = showMsg($rs['result'], array('インポートの権限'));
            }
        }
    }
}
if ($rtn === 0) {
    $res = db2_autocommit($db2con, DB2_AUTOCOMMIT_OFF);
    if ($res === false) {
        $rtn = 1;
        error_log('autocommit fail ******');
        $msg = showMsg('FAIL_SYS');
    }
}
if ($rtn === 0) {
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}

if (isset($_FILES)) {
    error_log("transfer file => ".print_r($_FILES,true));  
    $transferFile = $_FILES['transferFile']['name'];
    if ($transferFile !== '') {
        $ext = explode('.', $transferFile); // end(explode(".", $zipfile));  
        $extension = $ext[count($ext) - 1];
        error_log("extension => ". $extension);
        $name = $ext[0];
        $filepath = $_FILES['transferFile']['tmp_name'];
        $transferFile = $transferFile;
    } else {
        $transferFile = ''; 
    }
}
if($rtn === 0){
    if($extension === 'csv'){
        error_log('CSV version');
        //実行先のファイルのデリミタ
        $delimiter = ",";
        //$delimiter = ",";
        // ファイルポインタを開く
        if (($handle = fopen($filepath, "r")) !== FALSE) {
            $fieldInfo = array();
            $row = 0;
            while (($data = fgetcsv($handle, 0, $delimiter )) !== FALSE) {
                $original = $data;
                mb_convert_variables('UTF-8' , 'UTF-8' , $data);

                if($data !== $original){
                    $data = $original;
                    mb_convert_variables('UTF-8' , 'SJIS-win' , $data);
                }

                $num = count($data);
                $fieldData = array();
                for ($c=0; $c < $num; $c++) {
                    $fieldData[] = $data[$c];
                }
                $fieldInfo[$row] = $fieldData;
                $row++;
            }
            //array_shift($fieldInfo);
            fclose($handle);   
            error_log('COUNT = '.$num);         
        }
    }else if($extension === 'xls' || $extension === 'xlsx'){
        error_log('excel version');
        $inputFileType = PHPExcel_IOFactory::identify($filepath);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($filepath);
        $data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,false);
        error_log('FIELD INTO = '.print_r($data,true));
    }
}
error_log('FIELD INTO AFTER = '.print_r($fieldInfo,true));

if($rtn === 0){
    $rs = cmGetColumnCount($db2con,$TSFILLIB,$TSFILE );
    if ($rs['result'] !== true) {
        $rtn = 1;
        $msg = showMsg($rs['result'], array('クエリー'));
    } else {
        $column_count = $rs['data']['COUNT'];
    }
}
if($rtn === 0){
    $rs = getSYSCOLUMN2($db2con,$TSFILLIB,$TSFILE,$TSLIBLIST);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $coldata = $rs['data'];
        /*foreach($coldata as $col => $val){
            $coltype[] = $val['DDS_TYPE'];
            $collen[] = $val['LENGTH'];
        }*/
    }
}

error_log('coldata = '.print_r($coldata,true));


if($rtn === 0){
    foreach($fieldInfo as $row => $fldval){
        if(count($fldval) !== $column_count){
            $rtn = 1;
            $msg = '転送ファイルのコラムカウントと転送したいファイルのカウントが違います。';break;
        }else{
            foreach($fldval as $key => $val){
                error_log('length is ~ '.(int)$coldata[$key]['LENGTH']);
                if ($rtn === 0) {
                    if (!checkMaxLen($val, (int)$coldata[$key]['LENGTH'])) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(array($row+1, '行目', ':', $key+1 ,'列目')));
                        break;
                    }
                }
                $type = $coldata[$key]['DDS_TYPE'];
                error_log('type is ='.$type);
                if ($rtn === 0) {
                    if($type === 'P' || $type === 'S' || $type === 'B'){
                        if (!checkNum($val)) {
                            $rtn = 1;
                            $msg = showMsg('CHK_NUMBSEND', array(array($row+1, '行目', ':', $key+1 ,'列目のデータタイプ')));
                            break;
                        }else{
                            if($coldata[$key]['NUMERIC_SCALE'] === '0' && strpos($val, ".") !== false){
                                $rtn = 1;
                                $msg = showMsg('CHK_NUMBSEND', array(array($row+1, '行目', ':', $key+1 ,'列目のデータタイプ')));
                                break;
                            }
                        }
                    }
                }
            }
            if ($rtn === 0) {
                if($TSTYPE === '0'){
                    $rs = fninsrtDB2WTSF($db2con,$fldval,$TSFILLIB,$TSFILE,$TSTYPE);
                    if($rs['result'] !== true){
                        $rtn = 1;
                        if($rs['errcd'] === '23505'){
                            $msg = showMsg('ISEXIST', array(array($row+1, '行目のデータ')));
                        }break;
                    }
                }else if ($TSTYPE === '1'){
                    $rs = fndel4Replace($db2con,$TSFILLIB,$TSFILE,$TSTYPE);
                    if($rs['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rs['result']);
                        break;
                    }else{
                        $rs = fninsrtDB2WTSF($db2con,$fldval,$TSFILLIB,$TSFILE,$TSTYPE);
                        if($rs['result'] !== true){
                            $rtn = 1;
                            $msg = showMsg($rs['result']);
                            break;
                        }
                    }
                }else if($TSTYPE = '2'){
                    
                }else{
                    
                }
            }
        }
    }
}
if ($rtn === 1) {
    db2_rollback($db2con);
} else {
    db2_commit($db2con);
}
cmDb2Close($db2con);
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'TSFILLIB' => $TSFILLIB,
    'TSFILE' => $TSFILE,
    'success' => $success
    );
echo (json_encode($rtnAry));
//****************************************************
/*
 *-------------------------------------------------------*
 * カラムカウント取得
 *-------------------------------------------------------*
*/
function cmGetColumnCount($db2con, $tblschma,$tblName) {
    $data = array();
    $strSQL = ' SELECT COUNT(*) AS COUNT ';
    $strSQL.= ' FROM ' . SYSCOLUMN2 . '  ';
    $strSQL.= ' WHERE TABLE_SCHEMA = ? ';
    $strSQL.= ' AND TABLE_NAME = ? ';
    $params = array($tblschma, $tblName);
    error_log('table column count sql = '.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = false;
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = false;
        } else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;
}
//****************************************************
/*
 *-------------------------------------------------------*
 * データ追加
 *-------------------------------------------------------*
*/
function fndel4Replace($db2con,$TSFILLIB,$TSFILE,$TSTYPE){
    $data = $params = array();
    $msg = '';
    $strSQL .= '';
    $strSQL .= 'DELETE FORM  '.$TSFILLIB.'/'.$TSFILE;
    /*if($TSTYPE === '2'){
        $strSQL .= ' WHERE '.$TSDATA.' = ? ';
    }*/
    error_log('insert = '.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        error_log('stmt error = '.db2_stmt_errormsg());
        $data = array('result' => 'FAIL_DEL', 'errcd' => db2_stmt_error());
        $msg = db2_stmt_error();
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            error_log('stmt error = '.db2_stmt_errormsg());
            $data = array('result' => 'FAIL_DEL', 'errcd' => db2_stmt_error());
            $msg = db2_stmt_error();
        }else {
            $data = array('result' => true);
        }
    }
    return $data;
}
//****************************************************
/*
 *-------------------------------------------------------*
 * データ追加
 *-------------------------------------------------------*
*/
function fninsrtDB2WTSF($db2con, $tfArr = array(), $TSFILLIB,$TSFILE,$TSTYPE){
    $data = $params = array();
    $msg = '';
    $strSQL .= '';
    $strSQL .= 'INSERT INTO '.$TSFILLIB.'/'.$TSFILE.' VALUES ( ';
    foreach($tfArr as $key => $val){
        $strSQL .= ' ?';
        if($key !== count($tfArr)-1){
            $strSQL .= ', ';
        }
        array_push($params,$val);
    }
    $strSQL .= ' )';
    error_log('insert = '.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        error_log('stmt error = '.db2_stmt_errormsg());
        $data = array('result' => 'FAIL_INS', 'errcd' => db2_stmt_error());
        $msg = db2_stmt_error();
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            error_log('stmt error = '.db2_stmt_errormsg());
            $data = array('result' => 'FAIL_INS', 'errcd' => db2_stmt_error());
            $msg = db2_stmt_error();
        }else {
            $data = array('result' => true);
        }
    }
    return $data;
}
//****************************************************
/*
 *-------------------------------------------------------*
 * カラム情報取得
 *-------------------------------------------------------*
*/
function getSYSCOLUMN2($db2con,$TABLE_SCHEMA,$TABLE_NAME,$LIBLIST){
    $data = array();
    $params = array();
    $strSQL = '';

    $strSQL =' SELECT \'\' AS SEQ ';
    $strSQL .='       ,A.SYSTEM_COLUMN_NAME AS COLUMN_NAME ';
    $strSQL .='       ,A.LENGTH ';
    $strSQL .='       ,CASE A.DDS_TYPE';
    $strSQL .='         WHEN \'H\' THEN \'A\'';
    $strSQL .='         WHEN \'J\' THEN \'0\'';
    $strSQL .='         WHEN \'E\' THEN \'0\'';
    $strSQL .='         ELSE A.DDS_TYPE ';
    $strSQL .='         END AS DDS_TYPE';
    $strSQL .='       ,A.NUMERIC_SCALE ';
    if(SYSCOLCFLG==='1'){
        $strSQL .='       ,A.COLUMN_TEXT AS COLUMN_HEADING ';
    }else{
        $strSQL .='       ,A.COLUMN_HEADING ';
    }
    $strSQL .='       ,A.ORDINAL_POSITION ';
    $strSQL .=' FROM ' . SYSCOLUMN2 .' A ';
    $strSQL .=' WHERE A.TABLE_SCHEMA = ? ';
    $strSQL .=' AND A.TABLE_NAME = ? ';
    $strSQL .=' ORDER BY A.ORDINAL_POSITION ';
    
    e_log("getSYSCOLUMN2 akz ".$strSQL);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        if($TABLE_SCHEMA === '*USRLIBL' || $TABLE_SCHEMA === '*LIBL'){
            foreach($LIBLIST as $value){
               $params = array($value,$TABLE_NAME);
               //e_log("getSYSCOLUMN2 MSM1 ".$strSQL.print_r($params,true));
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = array('result' => 'FAIL_SEL');
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        break;
                    }
                }
            }
            //$data = umEx($data,true);
            $data = umEx($data);
            $data = array('result' => true,'data' => $data);
        }else{
            $params = array($TABLE_SCHEMA,$TABLE_NAME);
            //e_log("getSYSCOLUMN2 MSM2 ".$strSQL.print_r($params,true));
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                //$data = umEx($data,true);
                $data = umEx($data);
                $data = array('result' => true,'data' => $data);
            }
        }
        
    }
    return $data;
}