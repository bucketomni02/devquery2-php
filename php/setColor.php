<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("licenseInfo.php");
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/
$DATA = json_decode($_POST['DATA'],true);
$WUCLR1 = '';
$WUCLR2 = '';
$WUCLR3 = '';
if(isset($DATA['WUCLR1'])){
    $WUCLR1 = ($DATA['WUCLR1'] !== '')?$DATA['WUCLR1']:$licenseColor1;
}
if(isset($DATA['WUCLR2'])){
    $WUCLR2 = ($DATA['WUCLR2'] !== '')?$DATA['WUCLR2']:$licenseColor2;
}
if(isset($DATA['WUCLR3'])){
    $WUCLR3 = ($DATA['WUCLR3'] !== '')?$DATA['WUCLR3']:$licenseColor3;
}
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$usrinfo = $_SESSION['PHPQUERY']['user'];
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$usrId = $usrinfo[0]['WUUID'];

$db2con = cmDb2Con();

//INT処理
cmSetPHPQUERY($db2con);
// ユーザがログアウトかどうかチェック
if($rtn === 0){
    if($_SESSION['PHPQUERY']['user'][0]['WUUID'] === null){
        $rtn = 1;
        $msg = showMsg('CHECK_LOGOUT',array('ユーザー'));
    }
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){    
    $rs = fnGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

$rs = fnUpdateDB2WUSR($db2con,$usrId,$WUCLR1,$WUCLR2,$WUCLR3);
if($rs === '1'){
    $rtn = false;
    $msg =showMsg('FAIL_UPD');// '更新処理に失敗しました。管理者にお問い合わせください。';
}

cmDb2Close($db2con);

$rtnArray = array(
    'RTN'  => $rtn,
    'MSG'  => $msg,
    'DATA' => $DATA,
    'WUCLR1' => $WUCLR1,
    'WUCLR2' => $WUCLR2,
    'WUCLR3' => $WUCLR3
);

echo(json_encode($rtnArray));

/*
*-------------------------------------------------------* 
* カラー設定更新
*-------------------------------------------------------*
*/

function fnUpdateDB2WUSR($db2con,$WUUID,$WUCLR1,$WUCLR2,$WUCLR3){

    $rs = '0';

    $strSQL  = ' UPDATE DB2WUSR ';
    $strSQL .= ' SET ';
    $strSQL .= ' WUCLR1 = ?, ';
    $strSQL .= ' WUCLR2 = ?, ';
    $strSQL .= ' WUCLR3 = ? ';
    $strSQL .= ' WHERE WUUID = ? ';
    
    $params = array(
        $WUCLR1,
        $WUCLR2,
        $WUCLR3,
        $WUUID
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = '1';
    }else{    
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = '1';
        }
    }
    return $rs;

}
/*
*-------------------------------------------------------* 
* ログインユーザが削除されるかどうかチェック
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $id     ユーザーID
* 
*-------------------------------------------------------*
*/

function fnGetWUAUTH($db2con,$id){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WUSR as A ';
    $strSQL .= ' WHERE WUUID <> \'\' ';
    
    if($id != ''){
        $strSQL .= ' AND WUUID = ? ';
        array_push($params,$id);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;

}