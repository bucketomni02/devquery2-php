<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("licenseInfo.php");
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$WULANG = (isset($_POST['WULANG'])?$_POST['WULANG']:'');
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$usrinfo = $_SESSION['PHPQUERY']['user'];
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$usrId = $usrinfo[0]['WUUID'];

$db2con = cmDb2Con();

//INT処理
cmSetPHPQUERY($db2con);
// ユーザがログアウトかどうかチェック
if($rtn === 0){
    if($_SESSION['PHPQUERY']['user'][0]['WUUID'] === null){
        $rtn = 1;
        $msg = showMsg('CHECK_LOGOUT',array('ユーザー'));
    }
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){    
    $rs = fnGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}
if($WULANG === '001'){
    $WULANG = '';
}
$rs = fnUpdateDB2WUSR($db2con,$usrId,$WULANG);
if($rs !== true){
    $rtn = 1;
    $msg = showMsg($rs);
}

cmDb2Close($db2con);

$rtnArray = array(
    'RTN'  => $rtn,
    'MSG'  => $msg,
    'WULANG' => $WULANG,
);

echo(json_encode($rtnArray));

/*
*-------------------------------------------------------* 
* カラー設定更新
*-------------------------------------------------------*
*/

function fnUpdateDB2WUSR($db2con,$WUUID,$WULANG){

    $rs = true;

    $strSQL  = ' UPDATE DB2WUSR ';
    $strSQL .= ' SET ';
    $strSQL .= ' WULANG = ? ';
    $strSQL .= ' WHERE WUUID = ? ';
    
    $params = array(
        $WULANG,
        $WUUID
    );
    e_log($strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{    
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;

}
/*
*-------------------------------------------------------* 
* ログインユーザが削除されるかどうかチェック
* 
* RESULT
*    01：データ件数
*    02：false
* 
* @param Object  $db2con DBコネクション
* @param String  $id     ユーザーID
* 
*-------------------------------------------------------*
*/

function fnGetWUAUTH($db2con,$id){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2WUSR as A ';
    $strSQL .= ' WHERE WUUID <> \'\' ';
    
    if($id != ''){
        $strSQL .= ' AND WUUID = ? ';
        array_push($params,$id);
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
                $data = array('result' => 'NOTEXIST_GET');
            }else{
                $data = array('result' => true);
            }
        }
    }
    return $data;

}