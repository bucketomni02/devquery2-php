<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("getQryCnd.php");
include_once("getSQLCnd.php");
/*
*-------------------------------------------------------* 
* ライセンス管理　
*-------------------------------------------------------*
*/
include_once("licenseInfo.php");

/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/
$user = (isset($_POST['USER'])?$_POST['USER']:'');
$pass = (isset($_POST['PASS'])?$_POST['PASS']:'');
$url_ = $_POST['URL_CHK'];
$url_cs = $_POST['URL_CS'];
$URL_CHK = true;
$currentDate = date('Y/m/d');//20161020
$PivotFlg = true;       //ピボット実行権限チェックフラグ
$GraphFlg = true;
$isQGflg = false;//イー
$QueryGroupFlg = true;//190308
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtnArray = array();
$rtn = 0;
$msg = '';
$rs = '';
$url = '';
$D1WEBF = '';
$D1TEXT = '';
$usrinfo = array();
$cnddatArr = array();
$D1NAMELIST = array();
//ピボット実行権限チェック
if($licensePivot == false){
    $PivotFlg = false;
}
//グラフ実行権限チェック
if($licenseGraph == false){
    $GraphFlg = false;
}
//クエリーグループ実行権限チェック 190308
if($licenseQueryGroup == false){
    $QueryGroupFlg = false;
}
/*
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
if(AXESPARAM !== $url_){
    $URL_CHK = false;
    $rtn = 1;   
    if(AXESPARAM === 'HASH'){
        $url_cs = $url_cs.'/ユーザー名/パスワード';
    }else{
       $url_cs = $url_cs;
    }
    $msg =showMsg('LOGIN_AXESURL',array($url_cs));// "パラメータに誤りがあります。<br/>".$url_cs."/定義名 の形で渡してください。";
}
if($rtn !== 1){
    $usrinfo = LoginDB2WUSR($db2con,$rs,$user,$pass);
    $usrinfo = umEx($usrinfo);
    if(count($usrinfo) > 0){
        $_SESSION['PHPQUERY']['user'] = $usrinfo;
    }else{
        $rtn = 1;
        $msg =showMsg('FAIL_LGN');// 'IDまたはパスワードが正しくありません。';
    }
}
$htmldata = array();
$d1name = (isset($_POST['D1NAME']))?$_POST['D1NAME']:'';
if($rtn === 0 && $d1name !== ''){
    $rs =  fnGetDB2HTMLT($db2con,$d1name);
    if($rs['result'] === true){
        $htmldata = umEx($rs['data']);
    }else{
        $rtn = 1;
        $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));// 'クエリーの実行に失敗しました。';
    }
}
$fdb2csv1 = array();
if($rtn === 0){
    $rs = fnGetDB2QRYG($db2con,$d1name);
    if(count($rs['data']) > 0){
        $isQGflg = true;
    }
}
//ライセンスのクエリー連携実行権限がない場合、ここで処理終了
if ($isQGflg && $licenseQueryGroup == false) {
    $rtn = 2;
    $msg = showMsg('MSGLICEN', array(
        'クエリー連携実行'
    ));
}
$sqlChkflg = true;
if($rtn === 0){
    if($isQGflg){
        $rs = cmGetQueryGroup($db2con,$d1name,'','');
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリー'));
        }else{
            if(!$licenseSql){
                foreach($rs['data'] as $key => $res){
                    if($res['D1CFLG'] === ''){
                        $sqlChkflg = false;
                        array_push($D1NAMELIST,$rs['data'][$key]);
                    }      
                }
                $D1NAMELIST[count($D1NAMELIST)-1]['LASTFLG'] = '1';
                $D1NAMELIST = umEx($D1NAMELIST,true);
                //ライセンスのsqlクエリー実行権限がない場合、ここで処理終了
                if ($sqlChkflg) {
                    $rtn = 1;
                    $msg = showMsg('MSGLICEN', array('SQLクエリー連携実行'));
                }
            }else{
                $D1NAMELIST=umEx($rs['data'],true);
            }
        }
    }
}
if($rtn === 0){
    //クエリーデータ取得
    if($isQGflg){//クエリーグループ
        $LASTFLG=0;
        foreach($D1NAMELIST as $key => $res){
            $rs = fnGetFDB2CSV1Data($db2con,$res['QRYGSQRY']);
            if($rs['result'] !== true){
                $msg = showMsg($rs['result'],array('クエリー'));
                $rtn = 1;
            }else{
                $resultList = $rs['data'][0];
                $resultList['RDOFLG'] = 'STANDARD';
                $resultList['SEIGYOFLG'] = 0;//制御あるかどうかのため
                $resultList['DCSUMF'] = '';//合計のため
                $resultList['LASTFLG'] = $res['LASTFLG'];//一番最後のクエリー
                $fdb2csv1[$res['QRYGSQRY']][] = $resultList;
                if($res['LASTFLG'] === '1'){
                    $D1CFLG = $resultList['D1CFLG'];
                    $D1WEBF = $resultList['D1WEBF'];
                }
            }
        }
    }else{
        $rs = fnGetFDB2CSV1Data($db2con,$d1name);
        if($rs['result'] !== true){
            $msg = showMsg($rs['result'],array('クエリー'));
            $rtn = 1;
        }else{
            $fdb2csv1 = $rs['data'];
            $D1WEBF = $fdb2csv1[0]['D1WEBF'];
            $D1TEXT = $fdb2csv1[0]['D1TEXT'];
            $D1CFLG = $fdb2csv1[0]['D1CFLG'];

            $fdb2csv1[0]['RDOFLG'] = 'STANDARD';
            // SQL文で作成したクエリチェックフラグ取得
            //制御合計があるかどうかのチェック
            $fdb2csv1[0]['DCSUMF'] = '';//合計のため
            $fdb2csv1[0]['SEIGYOFLG'] = 0;//制御あるかどうかのため
            $fdb2csv1[0]['LASTFLG'] = 1;
        }
    }
}
//ライセンスのsqlクエリー実行権限がない場合、ここで処理終了
if (!$licenseSql && !$isQGflg && $D1CFLG === '1' ) {
    $rtn = 1;
    $msg = showMsg('MSGLICEN', array(
        'SQLクエリー'
    ));
}
/********************実行形式表示終了****************************/
if($isQGflg){//くえりーグループ
    $FDB2CSV3 = array();
    foreach($D1NAMELIST as $keyIndex => $result){
        $inData = array();
        $inData = cmChkQueryGroup($db2con,$usrinfo[0]['WUUID'],$d1name,$result['QRYGSQRY'],$result['LASTFLG'],$result['PMPKEY'],$result['GPHKEY']);
        if($chkQry['FDB2CSV1']['D1WEBF'] === ''){
            if($rtn === 0){
                $fdb3 = umEx(cmGetFDB2CSV3($db2con,$result['QRYGSQRY']));//rs はなんで必要か？
                $FDB2CSV3[$result['QRYGSQRY']][] = $fdb3;
                if(count($CSV2ARY)>0){
                    if($FDB2CSV3[$result['QRYGSQRY']][0] != $CSV2ARY[$result['QRYGSQRY']][0]){
                        $rtn = 2;
                        //クエリーの検索条件が変更されています。再度メニューから実行し直してください;
                        $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));
                        break;
                    }
                }
            }
        }else{
            $QRYGSQRY = $result['QRYGSQRY'];
            $rs = fnGetDB2SHSD($db2con,$usrinfo[0]['WUUID'],$d1name,$QRYGSQRY,$pmpkey,$gphkey);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }else{
                $SHSDDATA[$QRYGSQRY] = $rs['SHSDDATA'];
            }
            if($inData['FDB2CSV1']['D1CFLG'] === ''){
                if($rtn === 0){
                    $res = fnGetBQRYCND($db2con,$QRYGSQRY);
                    if($res['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                    }else{
                        $fdb3=umEx($res['data']);
                        $FDB2CSV3[$QRYGSQRY][] = $fdb3;
                    }
                   if(count($CSV2ARY)>0){
                        if($FDB2CSV3[$QRYGSQRY][0] != $CSV2ARY[$QRYGSQRY][0]){
                            $rtn = 2;
                            //クエリーの検索条件が変更されています。再度メニューから実行し直してください;
                            $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));
                            break;
                        }
                    }
                }
                if($rtn === 0){
                    $rs = fnBQRYCNDDAT($db2con,$QRYGSQRY);
                    if($rs['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rs['result']);
                    }else{
                        $data = $rs['data'];
                    }
                    $data = umEx($data);
                    $cnd=array();
                    if(count($data) > 0){
                        $cnd= fnCreateCndData($data);
                        if(count($SHSDDATA[$QRYGSQRY]) > 0){
                            $cnd = fnReplaceSchDtaWeb($cnd,$SHSDDATA[$QRYGSQRY],true);
                        }else if(count($WINDOWITEMDRILL)>0){//20180808【検索条件画面を挟む】
	                        $cnd = fnReplaceSchDtaWeb($cnd,$WINDOWITEMDRILL[$QRYGSQRY],false);
						}
                    }
                    $cnddatArr[$QRYGSQRY][]=$cnd;
                }
            }else{
                if($rtn === 0){
                    $res = fnGetBSQLCND($db2con,$QRYGSQRY);
                    if($res['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                        break;
                    }else{
                        $fdb3=umEx($res['data']);
                        $FDB2CSV3[$QRYGSQRY][]=$fdb3;
                    }                    
                    if(count($CSV2ARY)>0){
                        if($FDB2CSV3[$QRYGSQRY][0] != $CSV2ARY[$QRYGSQRY][0]){
                            $rtn = 2;
                            //クエリーの検索条件が変更されています。再度メニューから実行し直してください;
                            $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));
                            break;
                        }
                    }
                }
                if($rtn === 0){
                    $rs = fnBSQLHCND($db2con,$QRYGSQRY);
                    if($rs['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rs['result']);
                        break;
                    }else{
                        $data = $rs['data'];
                        if(count($data) > 0){
                            // 条件に関数のデータあるかのチェックチェック
                            foreach($data as $k => $dCnd){
                                if($dCnd['CNDDTYP'] === '6'){
                                    $resFuncData = getFuncdata($db2con,$dCnd['CNDDAT']);
                                    $data[$k]['CNDFUNC'] = $data[$k]['CNDDAT'];
                                    $data[$k]['CNDDAT'] = $resFuncData;
                                }
                            }
                            if(count($SHSDDATA[$QRYGSQRY]) > 0){
                                $data = fnReplaceSchDtaSQL($data,$SHSDDATA[$QRYGSQRY],true);
                            }else if(count($WINDOWITEMDRILL)>0){//20180808【検索条件画面を挟む】
                                $data = fnReplaceSchDtaSQL($data,$WINDOWITEMDRILL[$QRYGSQRY],false);
							}
                        }
                        $cnddatArr[$QRYGSQRY][]=$data;
                    }
                }
            }
        }
    }
    $data = $FDB2CSV3;
}
else{
    $data = array();
    if($rtn !== 1){
        if($D1WEBF != "1"){
            $data =  fnGetFDB2CSV3($db2con,$rs,$d1name);
            if($data !== false){
                $data = umEx($data);
            }else{
                $rtn = 1;
                $msg = showMsg('FAIL_FUNC',array('データ取得'));// 'データ取得に失敗しました。';
            }
        }else{
            if($D1CFLG === '1'){
                $res = fnBSQLHCND($db2con, $d1name);
                if ($res['result'] !== true) {
                    $rtn = 1;
                    $msg = showMsg($res['result']);
                } else {
                    $cnddatArr = $res['data'];
                }
            }else{
                $res = fnGetBQRYCND($db2con,$d1name);
                if($res['result'] !== true){
                    $rtn = 1;
                    $msg = showMsg($res['result']);
                }else{
                    $data = $res['data'];
                }
                if($rtn !== 1){
                    $rs = fnBQRYCNDDAT($db2con,$d1name);
                    if($rs['result'] !== true){
                        $rtn = 1;
                        $msg = showMsg($rs['result']);
                    }else{
                        $dataC = $rs['data'];
                    }
                    $dataC = umEx($dataC);
                    if(count($dataC) > 0){
                        $cnddatArr = fnCreateCndData($dataC);
                    }
                }
            }
        }
    }
}
if($isQGflg){
    foreach($D1NAMELIST as $key => $res){
        $chkQry = array();
        $chkQry = cmChkQueryGroup($db2con,$usrinfo[0]['WUUID'],$d1name,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
        if($chkQry['result'] !== true){
            $rtn = 2;
            $msg = showMsg($chkQry['result'],array('クエリー'));       
            break;
        }else{
            //一番最後の
            if($res['LASTFLG']==='1'){
                $fdb2csv1Data = $chkQry['FDB2CSV1'];
                $d1name = $res['QRYGSQRY'];
                //error_log('d1name '.$d1name);
                $lastQry = $chkQry['FDB2CSV1']['D1NAME'];
                $D1TEXT.= $chkQry['FDB2CSV1']['D1TEXT'];
                $D1CFLG = $chkQry['FDB2CSV1']['D1CFLG'];
                $D1WEBF = $chkQry['FDB2CSV1']['D1WEBF'];
            }else{
                $D1TEXT.= $chkQry['FDB2CSV1']['D1TEXT'] . ',';
            }
        }
    }//end of for loop
}
cmDb2Close($db2con);

/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'AXESPARAM' => AXESPARAM,
    'PivotFlg' => $PivotFlg,
    'GraphFlg' => $GraphFlg,
    'QueryGroupFlg' => $QueryGroupFlg,
    'URL_CHK' => $URL_CHK,
    'url_' => $url_,
    'htmldata' =>$htmldata,
    'url_cs' =>$url_cs, 
    'D1WEBF' =>$D1WEBF,
    'D1TEXT' => $D1TEXT,
    'D1CFLG' => $D1CFLG,
    'data' => $data,
    'cndData' => $cnddatArr,
    'currentDate' =>$currentDate,
    'isQGflg'=> $isQGflg,
    'lastQry'=> $lastQry,
    'D1NAMELIST'=> $D1NAMELIST,
    'FDB2CSV1' => $fdb2csv1

);
echo(json_encode($rtnArray));

/*
*-------------------------------------------------------* 
* ログインチェック
*-------------------------------------------------------*
*/

function LoginDB2WUSR($db2con,&$rs = false,$user,$pass){

    $data = array();

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $data = false;
    }else{        
        $r = db2_execute($stmt);
        if($r === false){
            $data = false;
        }else{
            $strSQL  = ' SELECT A.WUUID,A.WUUNAM,A.WUAUTH,A.WUDWNL,A.WUDWN2,A.WUSIZE FROM DB2WUSR AS A ';
            $strSQL .= ' WHERE WUUID = ? ' ;
            $strSQL .= ' AND DECRYPT_CHAR(WUPSWE) = ? ' ;

            $params = array(
                $user,
                $pass
            );

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $data = false;
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    $data = false;
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    if(count($data) > 0){
                        $rs = true;
                    }
                }
            }
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 定義ファイル（基本取得）
*-------------------------------------------------------*
*/

function fnGetFDB2CSV3($db2con,&$rs = true,$d1name){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT A.*,B.* ';
    $strSQL .= ' FROM FDB2CSV3 AS A JOIN FDB2CSV2 as B ' ;
    $strSQL .= ' ON A.D3NAME = B.D2NAME AND A.D3FILID = B.D2FILID ';
    $strSQL .= ' AND A.D3FLD = B.D2FLD ';
    $strSQL .= ' WHERE A.D3NAME = ? ';
    $strSQL .= ' AND A.D3USEL <> ? ';
    $strSQL .= ' AND A.D3USEL <> ? ';
    $strSQL .= ' ORDER BY A.D3JSEQ ASC ' ;

    $params = array(
        $d1name,
        '3',
        ''
    );
e_log('KLYH TESTING setUserInfo.php=>'.$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;
}

//FDB2CSV1データ取得
function fnGetFDB2CSV1Data($db2con,$D1NAME){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT * FROM  FDB2CSV1';
    $strSQL .= ' WHERE D1NAME = ?';

    $params = array(
        $D1NAME
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET'); 
            }else{
               $data = array('result' => true,'data' => umEx($data));
            }
        }
    }
    return $data;
}
//DB2QRYGデータ取得
function fnGetDB2QRYG($db2con,$D1NAME){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT * FROM  DB2QRYG';
    $strSQL .= ' WHERE QRYGID = ?';

    $params = array(
        $D1NAME
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET'); 
            }else{
               $data = array('result' => true,'data' => umEx($data));
            }
        }
    }
    return $data;
}

//保存された検索データー
function fnGetDB2SHSD($db2con,$UID,$QRG,$QID,$QPV,$QGP){
    $data = array();
    $params = array();

    $strSQL = ' SELECT * FROM DB2SHSD WHERE ';
    $strSQL .= ' SHSDUID = ? AND SHSDQRG = ? AND SHSDQID = ? AND SHSDQPV = ? AND SHSDQGP = ? ';
    $params = array($UID,$QRG,$QID,$QPV,$QGP);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $SHSDDATA = array();
            while($row = db2_fetch_assoc($stmt)){
                $SHSDDATA[] = $row;
            }
            $data = array('result' => true,'SHSDDATA' => umEx($SHSDDATA,true));
        }
    }
   return $data;
}