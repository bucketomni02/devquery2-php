<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$DATA = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('システム設定の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'28',$userData[0]['WUSAUT']);//'1' => systemMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('システム設定の権限'));
            }
        }
    }
}
if($rtn === 0){
        $COMTIM = (isset($DATA['COMTIM'])?cmMer($DATA['COMTIM']):'');
        $COFTML = (isset($DATA['COFTML'])?cmMer($DATA['COFTML']):'');
        $COFEXV = (isset($DATA['COFEXV'])?cmMer($DATA['COFEXV']):'1');
        $COFCEQ = (isset($DATA['COFCEQ'])?cmMer($DATA['COFCEQ']):'');
        $COFCNE = (isset($DATA['COFCNE'])?cmMer($DATA['COFCNE']):'');
        $COFCGT = (isset($DATA['COFCGT'])?cmMer($DATA['COFCGT']):'');
        $COFCLT = (isset($DATA['COFCLT'])?cmMer($DATA['COFCLT']):'');
        $COFCGE = (isset($DATA['COFCGE'])?cmMer($DATA['COFCGE']):'');
        $COFCLE = (isset($DATA['COFCLE'])?cmMer($DATA['COFCLE']):'');
        $COFCLK = (isset($DATA['COFCLK'])?cmMer($DATA['COFCLK']):'');
        $COFLLK = (isset($DATA['COFLLK'])?cmMer($DATA['COFLLK']):'');
        $COFRLK = (isset($DATA['COFRLK'])?cmMer($DATA['COFRLK']):'');
        $COFNLK = (isset($DATA['COFNLK'])?cmMer($DATA['COFNLK']):'');
        $COFLNLK = (isset($DATA['COFLNLK'])?cmMer($DATA['COFLNLK']):'');
        $COFRNLK = (isset($DATA['COFRNLK'])?cmMer($DATA['COFRNLK']):'');
        $COFRNG = (isset($DATA['COFRNG'])?cmMer($DATA['COFRNG']):'');
        $COFCLST = (isset($DATA['COFCLST'])?cmMer($DATA['COFCLST']):'');
        $COFNLST = (isset($DATA['COFNLST'])?cmMer($DATA['COFNLST']):'');
        $COFCIS = (isset($DATA['COFCIS'])?cmMer($DATA['COFCIS']):'');
        $COFRO1 = (isset($DATA['COFRO1'])?cmMer($DATA['COFRO1']):'');
        $COFRO2	 = (isset($DATA['COFRO2'])?cmMer($DATA['COFRO2']):'');
        $COFRO3 = (isset($DATA['COFRO3'])?cmMer($DATA['COFRO3']):'');
        $COFRO4 = (isset($DATA['COFRO4'])?cmMer($DATA['COFRO4']):'');
        $COFIRW = (isset($DATA['COFIRW'])?cmMer($DATA['COFIRW']):'4');
        $COFTOT = (isset($DATA['COFTOT'])?cmMer($DATA['COFTOT']):'');
        $COFLGD = (isset($DATA['COFLGD'])?cmMer($DATA['COFLGD']):'');
        $COFHMR = (isset($DATA['COFHMR'])?cmMer($DATA['COFHMR']):'');
        $COFLCT = (isset($DATA['COFLCT'])?cmMer($DATA['COFLCT']):'');
        $COFAXP = (isset($DATA['COFAXP'])?cmMer($DATA['COFAXP']):'0');
        $COFZFG = (isset($DATA['COFZFG'])?cmMer($DATA['COFZFG']):'0');
        $COFBZF = (isset($DATA['COFBZF'])?cmMer($DATA['COFBZF']):'0');
        $CORFIM = (isset($DATA['CORFIM'])?cmMer($DATA['CORFIM']):'');
        $CORFFM = (isset($DATA['CORFFM'])?cmMer($DATA['CORFFM']):'');
        $COROUT = (isset($DATA['COROUT'])?cmMer($DATA['COROUT']):'');
        $COFHEN = (isset($DATA['COFHEN'])?cmMer($DATA['COFHEN']):'0');
        $COCNDS = (isset($DATA['COCNDS'])?cmMer($DATA['COCNDS']):'');
        $COLIBM = (isset($DATA['COLIBM'])?cmMer($DATA['COLIBM']):'');
        $COFMEM = (isset($DATA['COFMEM'])?cmMer($DATA['COFMEM']):'');
        $COFPVC = (isset($DATA['COFPVC'])?cmMer($DATA['COFPVC']):'0');
        $COFSRSAVE = (isset($DATA['COFSRSAVE'])?cmMer($DATA['COFSRSAVE']):'');//検索条件を保存フラグ
        $COFUSR = $_SESSION['PHPQUERY']['user'][0]['WUUID'];
        $currentdate =date('Ymd his');//20161020 
        $currentdatetime = explode(" ", $currentdate);
        $COFDAT = $currentdatetime[0];
        $COFTIM = $currentdatetime[1];
        $COFPIV = (isset($DATA['COFPIV'])?cmMer($DATA['COFPIV']):'0');//ピボットの結合表示する
        $COFPIVDISP = (isset($DATA['COFPVDISP'])?cmMer($DATA['COFPVDISP']):'0');//ピボットの画面の表示列フラグ
        $COFCSV = (isset($DATA['COFCSV'])?cmMer($DATA['COFCSV']):'0');//CSV編集表示フラグ
        $COFHTM = (isset($DATA['COFHTM'])?cmMer($DATA['COFHTM']):'0');//HTML形式の編集コード表示フラグ
        $COFHTF = (isset($DATA['COFHTF'])?cmMer($DATA['COFHTF']):'0');//HTMLファイルの編集コード表示フラグ
        $COUQFG = (isset($DATA['COUQFG'])?cmMer($DATA['COUQFG']):'0');//HTMLファイルの編集コード表示フラグ
        $COOAFLG = (isset($DATA['COOAFLG'])?cmMer($DATA['COOAFLG']):'0');//ユーザーのその他権限表示フラグ
        $COQRYT = (isset($DATA['COQRYT'])?cmMer($DATA['COQRYT']):'0');//クエリー実行時間制限
        $COQRYMS = (isset($DATA['COQRYMS'])?cmMer($DATA['COQRYMS']):'0');//クエリー実行時間メモリー容量
        $COQRYLMT = (isset($DATA['COQRYLMT'])?cmMer($DATA['COQRYLMT']):'0');//クエリーの最大件数
        $COLBFLG = (isset($DATA['COLBFLG'])?cmMer($DATA['COLBFLG']):'0');//LIBRARY 
        $COLCFLG = (isset($DATA['COLCFLG'])?cmMer($DATA['COLCFLG']):'0');//カラム設定		
		$COASVR = (isset($DATA['COASVR'])?cmMer($DATA['COASVR']):'');
		$COAHOS = (isset($DATA['COAHOS'])?cmMer($DATA['COAHOS']):'');
		$COADO = (isset($DATA['COADO'])?cmMer($DATA['COADO']):'');
		$COAFLG = (isset($DATA['COAFLG'])?cmMer($DATA['COAFLG']):'');//Active Flag
        $COKGFLG = (isset($DATA['COKGFLG'])?cmMer($DATA['COKGFLG']):'');//クエリー作成時にダウンロード権限を付与(イー)
        $COGDFLG = (isset($DATA['COGDFLG'])?cmMer($DATA['COGDFLG']):'');//グループ権限付与時、ダウンロード権限自動付与(ギギー)
        $COFCLR  = (isset($DATA['COFCLR'])?cmMer($DATA['COFCLR']):'');//配信時のクロス集計の見出し色
        $COFJWF = (isset($DATA['COFJWF'])?cmMer($DATA['COFJWF']):'');//クエリー一覧から実行する際に別ウィンドウとか画面遷移とかの表示フラグ(MSM)
         //必須チェック
        if($rtn === 0){
            if($COMTIM === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('セッション'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COMTIM)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('セッション','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COMTIM,5)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('セッション'));
            }
        }
        if($rtn === 0){
            if (strpos($COMTIM, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('セッション','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COMTIM<1440){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('セッション','1440','以上'));//default 1440
            }
        }
        if($rtn === 0){
            if($COFTML === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('PHPスクリプト'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COFTML)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('PHPスクリプト','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFTML,5)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('PHPスクリプト'));
            }
        }
        if($rtn === 0){
            if (strpos($COFTML, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('PHPスクリプト','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COFTML<30){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('PHPスクリプト','３０以上'));//default 30
            }
        }
        if($rtn === 0){
            if($COFTOT === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('Ajax処理'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COFTOT)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('Ajax処理','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFTOT,10)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('Ajax処理'));
            }
        }
        if($rtn === 0){
            if (strpos($COFTOT, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('Ajax処理','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COFTOT<30){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('Ajax処理','３０以上'));
            }
        }
        if($rtn === 0){
            if($COFCEQ === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('EQ'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFCEQ,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('EQ'));
            }
        }
        if($rtn === 0){
            if($COFCNE === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('NE'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFCNE,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('NE'));
            }
        }
        if($rtn === 0){
            if($COFCGT === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('GT'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFCGT,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('GT'));
            }
        }
        if($rtn === 0){
            if($COFCLT === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('LT'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFCLT,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('LT'));
            }
        }
        if($rtn === 0){
            if($COFCGE === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('GE'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFCGE,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('GE'));
            }
        }
        if($rtn === 0){
            if($COFCLE === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('LE'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFCLE,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('LE'));
            }
        }
        if($rtn === 0){
            if($COFCLK === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('LIKE'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFCLK,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('LIKE'));
            }
        }
        if($rtn === 0){
            if($COFLLK === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('LLIKE'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFLLK,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('LLIKE'));
            }
        }
        if($rtn === 0){
            if($COFRLK === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('RLIKE'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFRLK,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('RLIKE'));
            }
        }
        if($rtn === 0){
            if($COFNLK === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('NLIKE'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFNLK,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('NLIKE'));
            }
        }
        if($rtn === 0){
            if($COFLNLK === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('LNLIKE'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFLNLK,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('LNLIKE'));
            }
        }
        if($rtn === 0){
            if($COFRNLK === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('RNLIKE'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFRNLK,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('RNLIKE'));
            }
        }
        if($rtn === 0){
            if($COFRNG === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('RANGE'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFRNG,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('RANGE'));
            }
        }
        if($rtn === 0){
            if($COFCLST === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('LIST'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFCLST,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('LIST'));
            }
        }
        if($rtn === 0){
            if($COFNLST === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('NLIST'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFNLST,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('NLIST'));
            }
        }
        if($rtn === 0){
            if($COFCIS === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('IS'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFCIS,30)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('IS'));
            }
        }
        if($rtn === 0){
            if($COFRO1 === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('表示行数１'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COFRO1)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数１','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFRO1,4)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('表示行数１'));
            }
        }
        if($rtn === 0){
            if (strpos($COFRO1, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数１','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COFRO1<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数１','１以上'));
            }
        }
        if($rtn === 0){
            if($COFRO2 === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('表示行数２'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COFRO2)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数２','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFRO2,4)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('表示行数２'));
            }
        }
        if($rtn === 0){
            if (strpos($COFRO2, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数２','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COFRO2<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数２','１以上'));
            }
        }
        if($rtn === 0){
            if($COFRO3 === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('表示行数３'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COFRO3)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数３','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFRO3,4)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('表示行数３'));
            }
        }
        if($rtn === 0){
            if (strpos($COFRO3, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数３','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COFRO3<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数３','１以上'));
            }
        }
        if($rtn === 0){
            if($COFRO4 === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('表示行数４'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COFRO4)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数４','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFRO4,4)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('表示行数４'));
            }
        }
        if($rtn === 0){
            if (strpos($COFRO4, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数４','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COFRO4<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('表示行数４','１以上'));
            }
        }
        if($rtn === 0){
            if($CORFIM === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('参照ファイルの最大数'));
            }
        }
        if($rtn === 0){
            if(!checkNum($CORFIM)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('参照ファイルの最大数','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($CORFIM,5)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('参照ファイルの最大数'));
            }
        }
        if($rtn === 0){
            if (strpos($CORFIM, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('参照ファイルの最大数','数値'));
            }
        }
        if($rtn === 0){
            if((int)$CORFIM<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('参照ファイルの最大数','１以上'));
            }
        }

        if($rtn === 0){
            if($CORFFM === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('参照ファイルの結合フィールドの最大数'));
            }
        }
        if($rtn === 0){
            if(!checkNum($CORFFM)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('参照ファイルの結合フィールドの最大数','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($CORFFM,5)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('参照ファイルの結合フィールドの最大数'));
            }
        }
        if($rtn === 0){
            if (strpos($CORFFM, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('参照ファイルの結合フィールドの最大数','数値'));
            }
        }
        if($rtn === 0){
            if((int)$CORFFM<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('参照ファイルの結合フィールドの最大数','１以上'));
            }
        }

        if($rtn === 0){
            if($COROUT === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('出力フィールドの最大数'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COROUT)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('出力フィールドの最大数','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COROUT,5)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('出力フィールドの最大数'));
            }
        }
        if($rtn === 0){
            if (strpos($COROUT, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('出力フィールドの最大数','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COROUT<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('出力フィールドの最大数','１以上'));
            }
        }

        if($rtn === 0){
            if($COCNDS === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('条件の最大数'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COCNDS)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('条件の最大数','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COCNDS,3)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('条件の最大数'));
            }
        }
        if($rtn === 0){
            if (strpos($COCNDS, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('条件の最大数','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COCNDS<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('条件の最大数','１以上'));
            }
        }
        if($rtn === 0){
            if($COLIBM === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('ライブラリリストの最大数'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COLIBM)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('ライブラリリストの最大数','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COLIBM,3)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('ライブラリリストの最大数'));
            }
        }
        if($rtn === 0){
            if (strpos($COLIBM, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('ライブラリリストの最大数','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COLIBM<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('ライブラリリストの最大数','１以上'));
            }
        }


        if($rtn === 0){
            if($COFMEM === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('メモリ'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COFMEM)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('メモリ','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFMEM,4)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('メモリ'));
            }
        }
        if($rtn === 0){
            if (strpos($COFMEM, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('メモリ','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COFMEM<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('メモリ','１以上'));
            }
        }

        if($rtn === 0){
            if(!checkMaxLen($COFHMR,4)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('HTMLメールの最大行数'));
            }
        }
        if($rtn === 0){
            if($COFHMR === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('HTMLメールの最大行数'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COFHMR)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('HTMLメールの最大行数'));
            }
        }
        if($rtn === 0){
            if (strpos($COFHMR, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('HTMLメールの最大行数'));
            }
        }
        if($rtn === 0){
            if((int)$COFHMR<1){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('HTMLメールの最大行数'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COFLGD)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('パスワード有効期限','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFLGD,4)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('パスワード有効期限'));
            }
        }
        if($rtn === 0){
            if (strpos($COFLGD, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('パスワード有効期限','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COFLGD<1  && $COFLGD !== ''){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('パスワード有効期限','１以上'));
            }
        }
        if($rtn === 0){
            if(!checkNum($COFLCT)){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('ログイン間違いの許容回数','数値'));
            }
        }
        if($rtn === 0){
            if(!checkMaxLen($COFLCT,1)){
                $rtn = 1;
                $msg = showMsg('FAIL_MAXLEN',array('ログイン間違いの許容回数'));
            }
        }
        if($rtn === 0){
            if (strpos($COFLCT, '.') !== false) {
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('ログイン間違いの許容回数','数値'));
            }
        }
        if($rtn === 0){
            if((int)$COFLCT<1 && $COFLCT !== ''){
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('ログイン間違いの許容回数','１以上'));
            }
        }
        if($rtn === 0){
            if($COOAFLG === '0'){
                $rs = chkExistSonotaUsr($db2con);
                if($rs !== true){
                    $rtn = 1;
                    $msg = showMsg($rs);
                }
            }
        }
        if($rtn === 0){
            if($COQRYT !== ''){
                if($rtn === 0){
                    if(!checkNuturalNum($COQRYT)){
                        $rtn = 1;
                        $msg = showMsg('CHK_FMT',array('クエリー実行時間','数値'));
                    }
                }
                if($rtn === 0){
                    if(!checkMaxLen($COQRYT,5)){
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN',array('クエリー実行時間'));
                    }
                }
                if($rtn === 0){
                    if($COQRYT >$COFTML ){
                        $rtn = 1;
                        $msg = showMsg('CHK_FMT',array('クエリー実行時間',array('PHPスクリプト','以下')));
                    }
                }
            }else{
                $DATA['COQRYT'] = -1;
            }
        }
        if($rtn === 0){
            if($COQRYMS !== ''){
                if($rtn === 0){
                    if(!checkNuturalNum($COQRYMS)){
                        $rtn = 1;
                        $msg = showMsg('CHK_FMT',array('クエリー実行メモリー容量','数値'));
                    }
                }
                if($rtn === 0){
                    if(!checkMaxLen($COQRYMS,10)){
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN',array('クエリー実行メモリー容量'));
                    }
                }
            }else{
                $DATA['COQRYMS'] = -1;
            }
        } 
        if($rtn === 0){
            if($COQRYLMT !== ''){
                if($rtn === 0){
                    if(!checkNuturalNum($COQRYLMT)){
                        $rtn = 1;
                        $msg = showMsg('CHK_FMT',array('クエリー実行件数','数値'));
                    }
                }
            }else{
                $DATA['COQRYLMT'] = 0;
            }
        }
		if($rtn === 0){
			if($COAHOS!==''){
				if($rtn===0){
		            if(!checkNum($COAHOS)){
		                $rtn = 1;
		                $msg = showMsg('CHK_FMT',array('ActiveDirectoryポート','数値'));
		            }
				}
		        if($rtn === 0){
		            if(!checkMaxLen($COAHOS,10)){
		                $rtn = 1;
		                $msg = showMsg('FAIL_MAXLEN',array('ActiveDirectoryポート'));
		            }
		        }
		        if($rtn === 0){
		            if (strpos($COAHOS, '.') !== false) {
		                $rtn = 1;
		                $msg = showMsg('CHK_FMT',array('ActiveDirectoryポート','数値'));
		            }
		        }
		        if($rtn === 0){
		            if((int)$COAHOS<1  && $COAHOS !== ''){
		                $rtn = 1;
		                $msg = showMsg('CHK_FMT',array('ActiveDirectoryポート','１以上'));
		            }
		        }
			}else{
				$DATA['COAHOS'] = 0;
			}
        }
		if($rtn === 0){
			if($COAFLG==='1'){
				if($rtn===0){
		            if($COASVR==''){
		                $rtn = 1;
		                $msg = showMsg('FAIL_REQ',array('ActiveDirectoryサーバー'));
		            }
				}
		        if($rtn === 0){
		            if($COAHOS==''){
		                $rtn = 1;
		                $msg = showMsg('FAIL_REQ',array('ActiveDirectoryポート'));
		            }
		        }
		        if($rtn === 0){
		            if ($COADO== '') {
		                $rtn = 1;
		                $msg = showMsg('FAIL_REQ',array('デフォルトドメイン'));
		            }
		        }		        
			}
        }
		if($rtn===0){			
			if($COAFLG=='1'){
				$rs = chkExistActiveUsr($db2con);
                if($rs !== true){
                    $rtn = 1;
                    $msg = showMsg($rs,array("Activeユーザー","ユーザー認証"));
                }
		}	
		}

}
//バリデーションチェック



/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/

if($rtn === 0){
    $rs = fnDeleteDB2COF($db2con);
    if($rs !== true){
            $rtn = 1;
        $msg = showMsg($rs);
    }else{
        $rs = fnInsertDB2COF($db2con,$DATA,$COFUSR,$COFDAT,$COFTIM);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}

/*MSM add for 利用停止時間 */

/*if($DATA['FMHR'] === ''&& $DATA['FMMN'] === '' && $DATA['TOHR'] === '' && $DATA['TOMN'] === '' ){
    $delFlg = 1 ;
    $rtn = 0;
}
else{
    if($rtn === 0){
       if($DATA['FMHR'] === ''|| $DATA['FMMN'] === '' || $DATA['TOHR'] === '' || $DATA['TOMN'] === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_CMP',array('利用停止時間'));
       }
    }
}

if($rtn === 0){
    $rs = fnInsertDB2UTIM($db2con,$DATA,$delFlg);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}*/

/*MSM add for 利用停止時間 */

$flg = 1;
if($rtn !== 0){
    $flg = 2;
    db2_rollback($db2con);
}else{
    db2_commit($db2con);
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' =>$DATA,
    'COFDAT' =>$COFDAT,
    'COFTIM' =>$COFTIM,
    'flg' =>$flg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* メールマスター削除
*-------------------------------------------------------*
*/

function fnDeleteDB2COF($db2con){


    $rs = true;

    //構文
    $strSQL  = 'DELETE FROM DB2COF';

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
       e_log("statement fail " . db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
         e_log("statement fail " . db2_stmt_errormsg());
        }
    }
    return $rs;

}
/*
 *-------------------------------------------------------*
 *ユーザーマスタにその他権限を持っているユーザーがあるかをチェック
 *-------------------------------------------------------*
 */
function chkExistSonotaUsr($db2con){
    $rs = true;
    $data = array();
    //構文
    $strSQL  = ' SELECT ';
    $strSQL .= '     COUNT(*) AS COUNT ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WUSR ';
    $strSQL .= ' WHERE ';
    $strSQL .= '     WUAUTH = \'2\' ';
    $params = array();
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
        e_log("statement fail " . db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
            e_log("statement fail " . db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            e_log('その他ユーザカウント：'.$data[0]['COUNT']);
            if($data[0]['COUNT'] > 0){
                $rs = 'FAIL_SET_OTHUSR';
            }
        }
    }
    return $rs;
}
/*
 *-------------------------------------------------------*
 *Check Active User
 *-------------------------------------------------------*
 */
function chkExistActiveUsr($db2con){
    $rs = true;
    $data = array();
    //構文
    $strSQL  = ' SELECT ';
    $strSQL .= '     COUNT(*) AS TOT  ';
    $strSQL .= ' FROM ';
    $strSQL .= '     DB2WUSR ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUAID <>\'\' ';
    $params = array();
    $stmt = db2_prepare($db2con,$strSQL);
	e_log("strSQL=>".$strSQL.print_r($params,true));
    if($stmt === false){
        $rs = 'FAIL_SEL';
        e_log("statement fail " . db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
            e_log("statement fail " . db2_stmt_errormsg());
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = umEx($data);
            e_log('Active User：'.$data[0]['TOT']);
            if($data[0]['TOT'] === 0){
                $rs = 'INVALID_ACTIVEUSR';
            }
        }
    }
    return $rs;
}
/*
*-------------------------------------------------------* 
* メールマスター更新
*-------------------------------------------------------*
*/

function fnInsertDB2COF($db2con,$DATA,$COFUSR,$COFDAT,$COFTIM){
            $rs = true;
            //構文
            $strSQL  = ' INSERT INTO DB2COF (COMTIM,COFTML,COFEXV,COFCEQ,COFCNE,COFCGT,COFCLT,COFCGE,COFCLE,COFCLK,COFLLK,COFRLK,COFNLK,COFLNLK,COFRNLK,COFRNG,COFCLST,COFNLST,COFCIS,COFRO1,COFRO2,COFRO3,COFRO4,COFIRW,COFTOT,COFLGD,COFHMR,COFLCT,COFAXP,COFZFG,COFBZF,CORFIM,CORFFM,COROUT,COCNDS,COLIBM,COFUSR,COFDAT,COFTIM,COFHEN,COFPIV,COFCSV,COFHTM,COFHTF,COFMEM,COFPVC,COUQFG,COOAFLG,COQRYT,COQRYMS,COQRYLMT,COLBFLG,COLCFLG,COLQCOL,COASVR,COAHOS,COADO,COAFLG,COPVFLG,COSRSV,COKGFLG,COGDFLG,COFCLR,COFJWF)';
            $strSQL .= ' VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ';
            $params = array(
                    $DATA['COMTIM'],
                    $DATA['COFTML'],
                    $DATA['COFEXV'],
                    $DATA['COFCEQ'],
                    $DATA['COFCNE'],
                    $DATA['COFCGT'],
                    $DATA['COFCLT'],
                    $DATA['COFCGE'],
                    $DATA['COFCLE'],
                    $DATA['COFCLK'],
                    $DATA['COFLLK'],
                    $DATA['COFRLK'],
                    $DATA['COFNLK'],
                    $DATA['COFLNLK'],
                    $DATA['COFRNLK'],
                    $DATA['COFRNG'],
                    $DATA['COFCLST'],
                    $DATA['COFNLST'],
                    $DATA['COFCIS'],
                    $DATA['COFRO1'],
                    $DATA['COFRO2'],
                    $DATA['COFRO3'],
                    $DATA['COFRO4'],
                    $DATA['COFIRW'],
                    $DATA['COFTOT'],
                    $DATA['COFLGD'],
                    $DATA['COFHMR'],
                    $DATA['COFLCT'],
                    $DATA['COFAXP'],
                    $DATA['COFZFG'],
                    $DATA['COFBZF'],
                    $DATA['CORFIM'],
                    $DATA['CORFFM'],
                    $DATA['COROUT'],
                    $DATA['COCNDS'],
                    $DATA['COLIBM'],
                    $COFUSR,
                    $COFDAT,
                    $COFTIM,
                   $DATA['COFHEN'],
                   $DATA['COFPIV'],
                   $DATA['COFCSV'],
                   $DATA['COFHTM'],
                   $DATA['COFHTF'],
                   $DATA['COFMEM'],
                   $DATA['COFPVC'],
                   $DATA['COUQFG'],
                   $DATA['COOAFLG'],
                   $DATA['COQRYT'],
                   $DATA['COQRYMS'],
                   $DATA['COQRYLMT'],
                   $DATA['LIBSFLG'],
                   $DATA['COLCFLG'],
                   $DATA['COLQCOL'],
				   $DATA['COASVR'],
                   $DATA['COAHOS'],
                   $DATA['COADO'],
                   $DATA['COAFLG'],
                   $DATA['COFPVDISP'],
                   $DATA['COFSRSAVE'],
                   $DATA['COKGFLG'],
                   $DATA['COGDFLG'],
                   $DATA['COFCLR'],
                   $DATA['COFJWF']
            );
			e_log("\n STRSQL = ".$strSQL."\n");
			e_log("\n PARAMS = ".print_r($params,true));
            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                 $rs = 'FAIL_UPD';
                    e_log("statement fail " . db2_stmt_errormsg());
            }else{
                $r = db2_execute($stmt,$params);
                if($r === false){
                    e_log("statement fail " . db2_stmt_errormsg());
                    $rs = 'FAIL_UPD';
                }
            }
    return $rs;

}

function fnInsertDB2UTIM($db2con,$DATA,$delFlg){

    $rs = true;

    $strSQL  = ' DELETE FROM DB2UTIM ';
    $params = array();
    $stmt = db2_prepare($db2con, $strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
        e_log("DB2UTIM statement fail1 " . db2_stmt_errormsg());
    }else{
        $r =db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
            e_log("DB2UTIM statement fail2 " . db2_stmt_errormsg());
        }else{
            if($delFlg === 0){
                $strSQL  = ' INSERT INTO DB2UTIM ';
                $strSQL .='(';
                $strSQL .='UTFRHR,';
                $strSQL .='UTFRMN,';
                $strSQL .='UTTOHR,';
                $strSQL .='UTTOMN,';
                $strSQL .='UTSCHF';
                $strSQL .=')';
                $strSQL .= ' VALUES (';
                $strSQL .= ' ?, ?, ?, ?,?';
                $strSQL .= ' ) ' ;
                $params = array(
                        $DATA['FMHR'],
                        $DATA['FMMN'],
                        $DATA['TOHR'],
                        $DATA['TOMN'],
                        $DATA['SCHEJIKO']
                );
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = 'FAIL_INS';

                }else{
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $rs = 'FAIL_INS';
                    }
                }
            }
        }
    }
    return $rs;

}