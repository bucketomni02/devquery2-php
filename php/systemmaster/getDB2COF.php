<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$rtn = 0;
$msg = '';
$WMPASS = '';

$ver = phpversion();
$sver = substr($ver,0,3);
$phpversion = str_replace('.','',$sver);
//$phpversion = (int)$phpversion; 

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('システム設定の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'28',$userData[0]['WUSAUT']);//'1' => systemMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('システム設定の権限'));
            }
        }
    }
}
if($rtn == 0){
    $res = fnGetDB2COF($db2con);
    if($res['result'] !== true){
        $msg = showMsg($res['result']);
        $rtn = 1;
    }else{
        $data = umEx($res['data']);
        $data = $data[0];
    }
}

/*MSM add for 利用停止時間 */
$rs = fnGetDB2UTIM($db2con);

if($rtn === 0){
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data1 = $rs['data'];
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'DATA' => $data,
    'DATA1' => $data1,
    'MSG' => $msg,
    'RTN' => $rtn,
	'phpversion' => (int)$phpversion
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* メール設定取得
*-------------------------------------------------------*
*/

function fnGetDB2COF($db2con){
    $rtn = 0;
    $data = array();
    $params = array();
    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM DB2COF AS A ' ;
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

function fnGetDB2UTIM($db2con){

    $data = array();

    $params = array();


    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2UTIM AS A ' ;


    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}