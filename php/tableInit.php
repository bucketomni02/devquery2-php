<?php
/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");
include_once("getQryCnd.php");
include_once("getSQLCnd.php");
include_once("base/createExecuteSQL.php");
include_once("chkCondParam.php");
include_once("chkSQLParam.php");
include_once("base/fnDB2QHIS.php");
include_once("CLExecutePG.php");
include_once("base/createTblExeSql.php");
include_once("base/getChkOutputFileData.php");

/*
 *-------------------------------------------------------* 
 * DataTableリクエスト
 *-------------------------------------------------------*
 */
$touch   = (isset($_POST['TOUCH']) ? $_POST['TOUCH'] : '0');
//$pivot  = (isset($_POST['PIVOT']) ? $_POST['PIVOT'] : '0');
$d1name  = (isset($_POST['D1NAME']) ? $_POST['D1NAME'] : '');
$pmpkey  = (isset($_POST['PMPKEY']) ? $_POST['PMPKEY'] : '');
$gphkey  = (isset($_POST['GPHKEY']) ? $_POST['GPHKEY'] : '');
// HTMLFLG
$htmlFlg = (isset($_POST['HTMLFLG']) ? $_POST['HTMLFLG'] : '0');
// SEIGYO SUM FLG
$DCSUMF  = (isset($_POST['DCSUMF']) ? $_POST['DCSUMF'] : '');


//以下はAXES対応
$axes         = (isset($_POST['AXES']) ? $_POST['AXES'] : '0');
$user         = (isset($_POST['USER']) ? $_POST['USER'] : '');
$pass         = (isset($_POST['PASS']) ? $_POST['PASS'] : '');
$paramFlg     = (isset($_POST['PARAMFLG']) ? $_POST['PARAMFLG'] : '');
/***クエリー連携***/
$DRILLFLG     = (isset($_POST['DRILLFLG']) ? $_POST['DRILLFLG'] : '');
$SEARCHKEYARR = (isset($_POST['SEARCHKEYARR']) ? json_decode($_POST['SEARCHKEYARR'], true) : array());
/***クエリー連携***/

// 検索条件
$data            = (isset($_POST['DATA']) ? json_decode($_POST['DATA'], true) : array());
//直接result画面を開いたときに1になる
$directResultFlg = (isset($_POST['directResultFlg']) ? $_POST['directResultFlg'] : '0');
// セッションユーザーデータ取得
$usrinfo         = (($touch == '1') ? $_SESSION['PHPQUERYTOUCH']['user'] : $_SESSION['PHPQUERY']['user']);
//制御の場合ファイルタ情報
$filterData      = (isset($_POST['FILTERDATA']) ? $_POST['FILTERDATA'] : '');

$logFlg          = (isset($_POST['LOGFLG']) ? $_POST['LOGFLG'] : '0');
$isQGflg            = (isset($_POST['ISQGFLG'])?$_POST['ISQGFLG']:false);//クエリーグループ
$SCHSAVE      = (isset($_POST['SCHSAVE']) ?  json_decode($_POST['SCHSAVE'], true) : array());
$SCHDFLG            = (isset($_POST['SCHDFLG'])?$_POST['SCHDFLG']:false);//クエリーグループ

//ピボットの場合SQLのため普通のtmp名
$oldFilename='';

/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$initRs          = '';
$doRs            = '';
$filename        = '';
$JSEQ            = '';
$rtn             = 0;
$msg             = '';
$text            = ''; // 定義表示テキスト
$text_nohtml     = '';
$outputfdata=array();//【入れ】出力ファイル全部データ
$outputfdatawin=array();//【入れ】出力ファイルワインドウのためデータ
$directGetFlg    = 0; //直接result画面を開いたとき、直前のセッションで検索を実行した場合に1になる
$data_session    = (isset($_POST['DATA']) ? $_POST['DATA'] : '');
//ボタン表示フラグ
$csvFlg          = true;
$excelFlg        = true;
$schFlg          = true;
$FDB2CSV1        = array();
$db2pcol         = array();
$db2pcal         = array();
$exeSQL          = array();

// 制御実行フラグ
$seigyoFlg = 0;
$seigyoSum = '';
//クエリーグループ
$D1NAMELIST=array();//クエリーグループの全部データ
$LASTD1NAME='';//クエリーグループの一番最後のクエリーＩＤ
$isQGflg=($isQGflg==='true')?true:false;
$filenamelist= array();//クエリーグループのためにはファイル名がリスト


/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
if ($rtn === 0) {
    if ($pmpkey !== '') {
        //ピボット実行権限チェック
        if ($licensePivot == false) {
            $rtn = 2;
            $msg = showMsg('MSGLICEN', array(
                'ピボット'
            ));
        }
    } else if ($gphkey !== '') {
        if ($licenseGraph == false) {
            $rtn = 2;
            $msg = showMsg('MSGLICEN', array(
                'グラフ'
            ));
        }
    }
    //ライセンスのクエリー連携実行権限がない場合、ここで処理終了
    if ($isQGflg && $licenseQueryGroup == false) {
        $rtn = 2;
        $msg = showMsg('MSGLICEN', array(
            'クエリー連携実行'
        ));
    }
}
$sqlChkflg = true;
if($rtn===0){
    if($isQGflg){
        $rs=cmGetQueryGroup($db2con,$d1name,$pmpkey,$gphkey);
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('クエリー'));
        }else{
            if(!$licenseSql){
                foreach($rs['data'] as $key => $res){
                    if($res['D1CFLG'] === ''){
                        $sqlChkflg = false;
                        array_push($D1NAMELIST,$rs['data'][$key]);
                    }      
                }
                $D1NAMELIST[count($D1NAMELIST)-1]['LASTFLG'] = '1';
                $D1NAMELIST = umEx($D1NAMELIST,true);
                //ライセンスのsqlクエリー実行権限がない場合、ここで処理終了
                if ($sqlChkflg) {
                    $rtn = 2;
                    $msg = showMsg('MSGLICEN', array(
                        'SQLクエリー連携実行'
                    ));
                }
            }else{
                $D1NAMELIST=umEx($rs['data'],true);
            }
        }
    }
}
if ($rtn === 0) {
    if($isQGflg){
        e_log('tableInit.php クエリーグループ権限チェック');
        foreach($D1NAMELIST as $key => $res){
            $chkQry = array();
            if($res['LASTFLG']==='1'){
                $LASTD1NAME=$res['QRYGSQRY'];
            }
            $chkQry = cmChkQueryGroup($db2con,$usrinfo[0]['WUUID'],$d1name,$res['QRYGSQRY'],$res['LASTFLG'],$res['PMPKEY'],$res['GPHKEY']);
            e_log(print_r($chkQry,true));
			if ($chkQry['result'] === 'NOTEXIST_GPH') {
                $rtn = 2;
                $msg = showMsg('NOTEXIST_GET', array(
                    'グラフ'
                ));
                break;
            }else if($chkQry['result'] === 'NOTEXIST_PIV'){
                $rtn = 2;
                $msg = showMsg('NOTEXIST_GET',array('ピボット'));
                break;
            }else if($chkQry['result'] !== true){
                $rtn = 2;
                e_log('クエリー権限エラー');
                $msg = showMsg($chkQry['result'],array('クエリー'));
                break;
            }else{
                $resultList = $chkQry['FDB2CSV1'];
                $resultList['DGBFLG']=false;
                $resultList['DGAFLG']=false;
                $resultList['LASTFLG']=$res['LASTFLG'];//一番最後のクエリー
                $FDB2CSV1[$res['QRYGSQRY']][]=$resultList;
            }
        }//end of for loop
    }else{
        $chkQry = array();
        $chkQry = cmChkQuery($db2con, $usrinfo[0]['WUUID'], $d1name, $pmpkey, $gphkey);
        if ($chkQry['result'] === 'NOTEXIST_GPH') {
            $rtn = 2;
            $msg = showMsg('NOTEXIST_GET', array(
                'グラフ'
            ));
        } else if ($chkQry['result'] === 'NOTEXIST_PIV') {
            $rtn = 2;
            $msg = showMsg('NOTEXIST_GET', array(
                'ピボット'
            ));
/*        } else if ($chkQry['COUNT'] === 0) {
            $rtn = 2;
            $msg = showMsg('FAIL_USR', array(
                'クエリー'
            ));
*/
        }else{

            //管理者ユーザーの場合、全てのクエリーを実行できる
            //管理者（グループ）または、一般ユーザーの場合は自分に編集権限のあるクエリーであれば実行できる
            //グループ権限とは関係なし

            if($usrinfo[0]['WUAUTH'] === '3' || $usrinfo[0]['WUAUTH'] === '4'){
                if($rtn === 0){
                    $chkQryUsr = chkVldQryUsr($db2con,$d1name,$usrinfo[0]['WUAUTH']);
                    if($chkQryUsr === 'NO_QRY'){//NOTEXIST_GET MSM change 20190405 クエリーが存在しないのか、権限がないのか、区別できるようにする
                        $rtn = 2;
                        $msg = showMsg('NO_QRY', array(
                            'クエリー'
                        ));
                    }else if($chkQryUsr === 'NO_PERMIT'){

                        $rtn = 2;
                        $msg = showMsg('NO_PERMIT', array(
                            'クエリー'
                        ));//MSM change 20190405 クエリーが存在しないのか、権限がないのか、区別できるようにする
                        //error_log("chkQryUsr NO_PERMIT MSG => ".$rtn);
                    }else if($chkQryUsr !== true){
                        $rtn = 1;
                        $msg = showMsg($chkQryUsr['result'],array('クエリー'));
                    }
                }

                //作成権限がない場合でもグループ権限があれば実行可能
                if($rtn !== 0){
                    if($chkQry['COUNT'] > 0){
                        $rtn = 0;
                    }
                }

            }

            if($rtn === 0){
                $FDB2CSV1 = $chkQry['FDB2CSV1'];
            }
        }
    }
}

if($rtn === 0){
    if($isQGflg){
        foreach($D1NAMELIST as $result){
            $getFDB2CSV1=$FDB2CSV1[$result['QRYGSQRY']][0];
            if($result['LASTFLG']==='1'){
                $text .=$getFDB2CSV1['D1TEXT']."【". $getFDB2CSV1['D1NAME']."】";
            }else{
                $text .=$getFDB2CSV1['D1TEXT']."【". $getFDB2CSV1['D1NAME']."】".",";
            }
        }
    }else{
        /*if ($gphkey !== '') {
            //$LASTD1NAMEというのはクエリーグループの一番最後のクエリー
            $DB2GPK =($LASTD1NAME !=='' )?$FDB2CSV1[$LASTD1NAME][0]['DB2GPK']: $FDB2CSV1['DB2GPK'];
            $text = $DB2GPK['GPKNM'];
        } else if ($pmpkey !== '') {
            //$LASTD1NAMEというのはクエリーグループの一番最後のクエリー
            $paramName=($LASTD1NAME!=='')?$LASTD1NAME:$d1name;
            $db2pcol = cmGetDB2PCOL($db2con, $paramName, $pmpkey);
            $db2pcal = cmGetDB2PCAL($db2con, $paramName, $pmpkey);
            $DB2PMST = ($LASTD1NAME!=='')?$FDB2CSV1[$LASTD1NAME][0]['DB2PMST']:$FDB2CSV1['DB2PMST'];
            $text = $DB2PMST['PMTEXT'];
        } else {
            $text =$FDB2CSV1['D1TEXT'];
        }*/
		if ($gphkey !== '') {
	        $DB2GPK = $chkQry['DB2GPK'];
	        $text = $DB2GPK['GPKNM'];
	    } else if ($pmpkey !== '') {
	        $db2pcol = cmGetDB2PCOL($db2con, $d1name, $pmpkey);
	        $db2pcal = cmGetDB2PCAL($db2con, $d1name, $pmpkey);
	        $DB2PMST = $chkQry['DB2PMST'];
	        $text = $DB2PMST['PMTEXT'];
	    } else {
	        $text = $FDB2CSV1['D1TEXT'];
	    }
    }
}

// 制御チェック
if ($pmpkey === '' && $gphkey === '') {
    //AXESFLG = 1 , HTMLFLG = 1の場合制御チェックやる
    if ($htmlFlg === '1' && $licenseSeigyo) {
        //$LASTD1NAMEというのはクエリーグループの一番最後のクエリー
        $paramName=($LASTD1NAME!=='')?$LASTD1NAME:$d1name;
        $rsSeigyoData = fnGetSeigyoData($db2con, $paramName, $DCSUMF);
        if ($rsSeigyoData['RESULT'] === 1) {
            $msg = showMsg($rsSeigyoData['MSG']);
            $rtn = 1;
        } else {
            $seigyoFlg = $rsSeigyoData['SEIGYOFLG'];
            $seigyoSum = $rsSeigyoData['SEIGYOSUM'];
            //e_log('SEIGYODATA:'.print_r($rsSeigyoData,true));
        }
    }
}
if ($rtn === 0) {
    $text_nohtml = $text;
    $text        = cmHsc($text);
    //$LASTD1NAMEというのはクエリーグループの一番最後のクエリー
    $D1WEBF      = ($LASTD1NAME !=='')?$FDB2CSV1[$LASTD1NAME][0]['D1WEBF']:$FDB2CSV1['D1WEBF'];
    // SQLで作成クエリーチェック
    $D1CFLG      = ($LASTD1NAME !=='')?$FDB2CSV1[$LASTD1NAME][0]['D1CFLG']:$FDB2CSV1['D1CFLG'];
    $RDBNM       =($LASTD1NAME !=='')?$FDB2CSV1[$LASTD1NAME][0]['D1RDB']: $FDB2CSV1['D1RDB'];
    //ファイル名生成
    if($isQGflg){//クエリーグループ
        foreach($D1NAMELIST as $key=>$result){
            $pfilename=makeRandStr(10, true);
            //クエリーグループに一番最後のデータをもらうため
            if($result['LASTFLG']==='1'){
                $filename=$pfilename;
            }
            $filenamelist[$result['QRYGSQRY']][]=$pfilename;
        }
    }else{//普通
        $filename    = makeRandStr(10, true);
    }
}
//ライセンスのsqlクエリー実行権限がない場合、ここで処理終了
if (!$licenseSql && !$isQGflg && $D1CFLG === '1' ) {
    $rtn = 2;
    $msg = showMsg('MSGLICEN', array(
        'SQLクエリー'
    ));
}

if($rtn===0){
    if($isQGflg){
        $schFlgCount=0;
        $RDBNameList=array();//別の区画のためＲＤＢ名を入れる
        $RDBNameObj=array();//別の区画のためＲＤＢ名を入れるObj
        $PHPNameList=array();//LOCALのためＤＢの名を入れる
        foreach($D1NAMELIST as $keyIndex=>$result){
            $getFDB2CSV1=$FDB2CSV1[$result['QRYGSQRY']][0];
            $innerSchFlg=true;
            if($getFDB2CSV1['D1WEBF'] !=='1'){
                //クエリー連携の検索項目チェック
                if ($rtn === 0) {
                    if ($DRILLFLG === '1') {
                        if($result['LASTFLG']==='1'){
                            $res=fnChkHisu($data[$result['QRYGSQRY']][0],$SEARCHKEYARR[$result['QRYGSQRY']][0]);
                            $drill = $res;
                            if ($res['result'] !== true) {
                                //resultflg 1 => '非表示、固定'
                                //resultflg 2 => 'RANGE、IS'
                                //resultflg 3=> '必須'
                                if ($res['resultflg'] === '1') {
                                    $rtn = 1;
                                    $msg = showMsg('DRILL_COL_FAIL', array('非表示','固定'));
                                    break;
                                } else if ($res['resultflg'] === '2') {
                                    $rtn = 1;
                                    $msg = showMsg('DRILL_COL_FAIL', array('RANGE','IS'));
                                    break;
                                }
                            }
                        }
                        
                    }
                }
                //FDB2CSV3を取得(セッションに保存,tableInitで比較に使用)
                if ($rtn === 0) {
                    $res = fnGetFDB2CSV3($db2con, $result['QRYGSQRY']);
                    if ($res['result'] !== true) {
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                    } else {
                        $FDB2CSV3[$result['QRYGSQRY']][] = $res['data'];
                    }
                }
                if ($rtn === 0) {
                    //直接result画面から来たときは直前のセッションを比較して、
                    //FDB2CSV3が同じなら直前の検索状態を表示する
                    // 直接パラメータで実行する場合Sessionデータとらない
                    if ($paramFlg === '') {
                        if ($directResultFlg === '1' && $touch === '0') {
                            if (isset($_SESSION['PHPQUERY']['schData'])) {
                                if ($_SESSION['PHPQUERY']['schData'] !== '') {
                                    if ($FDB2CSV3[$result['QRYGSQRY']][0] === $_SESSION['PHPQUERY']['FDB2CSV3'][$result['QRYGSQRY']][0]) {
                                        $data_session = $_SESSION['PHPQUERY']['schData'];
                                        $data         = json_decode($data_session, true);
                                        $directGetFlg = 1;
                                    }
                                }
                            }
                        } else if ($directResultFlg === '1' && $touch === '1') {
                            if (isset($_SESSION['PHPQUERYTOUCH']['schData'])) {
                                if ($_SESSION['PHPQUERYTOUCH']['schData'] !== '') {
                                    if ($FDB2CSV3[$result['QRYGSQRY']][0] === $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'][$result['QRYGSQRY']][0]) {
                                        $data_session = $_SESSION['PHPQUERYTOUCH']['schData'];
                                        $data         = json_decode($data_session, true);
                                        $directGetFlg = 1;
                                    }
                                }
                            }
                        }
                    }
                    //PC版のみ、検索状態と検索条件をセッションに保存
                    if ($touch === '0') {
                        $_SESSION['PHPQUERY']['schData']  = $data_session;
                        $_SESSION['PHPQUERY']['FDB2CSV3'][$result['QRYGSQRY']][] = $FDB2CSV3[$result['QRYGSQRY']][0];
                    } else if ($touch === '1') {
                        $_SESSION['PHPQUERYTOUCH']['schData']  = $data_session;
                        $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'][$result['QRYGSQRY']][] = $FDB2CSV3[$result['QRYGSQRY']][0];
                    }
                }
                if ($rtn === 0) {
                    //検索条件チェック（ブランクチェックのみ）
                    $getData=$data[$result['QRYGSQRY']][0];
                    foreach ($getData as $key => $value) {
                        if ($value['D3USEL'] === '1' && $value['D3DAT'] === '') {
                            $rtn = 1;
                            $msg = $value['D2HED'] . 'は入力必須です。';
                            break;
                        }
                    }
                }
                if ($rtn === 0) {
                    //検索条件フラグ
                    $FDB2CSV3[$result['QRYGSQRY']][0]= cmGetFDB2CSV3($db2con , $result['QRYGSQRY']);
                    if (isset($FDB2CSV3[$result['QRYGSQRY']][0] ) === false) {
                        $schFlgCount++;
                    }
                }
                //定義の条件変更することがあるかのチェック
                if ($rtn === 0) {
                    $OLD_FDB2CSV3 = (isset($_POST['csvoldAry']) ? json_decode($_POST['csvoldAry'], true) : '');
                    if ($OLD_FDB2CSV3[$result['QRYGSQRY']][0] !== '') {
                        if ($OLD_FDB2CSV3[$result['QRYGSQRY']][0] !== $FDB2CSV3[$result['QRYGSQRY']][0]) {
                            $rtn = 2;
                            $msg = showMsg('FAIL_MENU_REQ', array('クエリーの検索条件'
                            )); //"クエリーの検索条件が変更されています。<br/>再度メニューから実行し直してください";
                            break;
                        }
                    }
                }
                if($rtn===0){
                    /****DEMO/COUNT*****************************/
                    //init処理
                    cmInt($db2con, $initRs, $JSEQ, $result['QRYGSQRY']);
                    
                    //9以外の場合、ＤＯ処理
                    if ($initRs !== "9") {
                        cmSetQTEMP($db2con);
                        //初期画面の検索条件でFDB2CSV3を更新
                        $r  = updSearchCond($db2con, $data[$result['QRYGSQRY']][0], $result['QRYGSQRY']);
                        $rs = $r[0];
                        if ($rs === 1) {
                            $rtn = 1;
                            $msg = showMsg('FAIL_INT_BYTE'); //'検索データは128バイトまで入力可能です。';
                        } else {
                            //CL連携
                            //ライセンスのCL連携権限がある場合のみ実行
                            if ($licenseCl === true) {
                                e_log("setCmd start");
                                setCmd($db2con, $result['QRYGSQRY'], $getFDB2CSV1['D1WEBF']);
                                e_log("setCmd end");
                            }
                            cmSetPHPQUERY($db2con);
                            //DO処理
                            cmDo($db2con, $doRs, $JSEQ, $filenamelist[$result['QRYGSQRY']][0], $result['QRYGSQRY']);
                            
                            // 制御又はピボットの実行の場合
                            if ($result['PMPKEY'] === '') {
                                //制御の場合
                                e_log('制御データ取得');
                                if ($htmlFlg === '1' && $seigyoFlg === 1) {
                                    if($result['LASTFLG'] ==='1'){
                                        $rstemp = cmRecreateSeigyoTbl5250($db2con, $result['QRYGSQRY'], $filenamelist[$result['QRYGSQRY']][0], $filterData[$result['QRYGSQRY']][0]);
                                        if ($rstemp !== '') {
                                            $filterData[$result['QRYGSQRY']][0] = $rstemp;
                                        }
                                    }
                                }
                                //filter testing
                                if ($filterFlg ==='1'){
                                    //if($result['LASTFLG'] ==='1'){
                                        $rstemp = cmRecreateSeigyoTbl5250FS($db2con, $result['QRYGSQRY'], $filenamelist[$result['QRYGSQRY']][0], $filterData[$result['QRYGSQRY']][0]);
                                        if ($rstemp !== '') {
                                            $filterData[$result['QRYGSQRY']][0] = $rstemp;
                                            
                                        }
                                    //}
                                }
                            } else {
                                // ピボットの場合
                                e_log('ピボットデータ取得');
                                if($result['LASTFLG'] ==='1'){
                                    $rstemp = cmRecreateRnPivotTbl5250($db2con, $result['QRYGSQRY'], $result['PMPKEY'], $filenamelist[$result['QRYGSQRY']][0]);
                                    if ($rstemp !== '') {
                                        $filenamelist[$result['QRYGSQRY']][0] = $rstemp;
                                    }
                                }
                            }
                            
                            /****DEMO/COUNT*****************************/
                            /*******************************************/
                            //ログ出力
                            $logrs = '';
                            cmInsertDB2WLOG($db2con, $logrs, $usrinfo[0]['WUUID'], 'D', $result['QRYGSQRY'], $data[$result['QRYGSQRY']][0], '0','');
                            fnDB2QHISExe($db2con,  $result['QRYGSQRY'], $usrinfo[0]['WUUID']);
                            if ($doRs === "1") {
                                $D2HED = cmGetD2HED($db2con, $result['QRYGSQRY'], $JSEQ);
                                $rtn   = 1;
                                $msg   = cmMer($D2HED) . 'の入力に誤りがあります。';
                                break;
                            } else if ($doRs === "9") {
                                $rtn = 1;
                                $msg = showMsg('FAIL_FUNC', array(
                                    'クエリーの実行'
                                )); //'クエリーの実行に失敗しました。';
                                break;
                            }
                        }
                    }else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_FUNC', array(
                            'クエリーの事前処理'
                        )); //'クエリーの事前処理に失敗しました。';
                        break;
                    }
                }
            }else{
                // WEBSQLで作成したクエリーの場合
                if ($getFDB2CSV1['D1CFLG'] === '1') {
                    //クエリー連携の検索項目チェック
                    if ($rtn === 0) {
                        if ($DRILLFLG === '1') {
                            if($result['LASTFLG']==='1'){
                                $res=fnChkHisu($data[$result['QRYGSQRY']][0],$SEARCHKEYARR[$result['QRYGSQRY']][0]);
                                $drill = $res;
                                if ($res['result'] !== true) {
                                    //resultflg 1 => '非表示、固定'
                                    //resultflg 2 => 'RANGE、IS'
                                    //resultflg 3=> '必須'
                                    if ($res['resultflg'] === '1') {
                                        $rtn = 1;
                                        $msg = showMsg('DRILL_COL_FAIL', array('非表示','固定'));
                                        break;
                                    } else if ($res['resultflg'] === '2') {
                                        $rtn = 1;
                                        $msg = showMsg('DRILL_COL_FAIL', array('RANGE', 'IS'));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    //[MMM要]//クエリー連携の検索項目チェック
                    //条件情報【BSQLCND】を取得(セッションに保存,tableInitで比較に使用)
                    if ($rtn === 0) {
                        $res = fnGetBSQLCND($db2con, $result['QRYGSQRY']);
                        if ($res['result'] !== true) {
                            $rtn = 1;
                            $msg = showMsg($res['result']);
                            break;
                        } else {
                            $FDB2CSV3[$result['QRYGSQRY']][] = $res['data'];
                        }
                    }
                    //直接result画面から来たときは直前のセッションを比較して、
                    //FDB2CSV3が同じなら直前の検索状態を表示する
                    if ($rtn === 0) {
                        //error_log('パラメータデータ：'.print_r($data,true).'データカウント：'.count($data));
                        if ($paramFlg === '') {
                            if ($directResultFlg === '1' && $touch === '0') {
                                if (isset($_SESSION['PHPQUERY']['schData'])) {
                                    if ($_SESSION['PHPQUERY']['schData'] !== '') {
                                        if ($FDB2CSV3[$result['QRYGSQRY']][0] === $_SESSION['PHPQUERY']['FDB2CSV3'][$result['QRYGSQRY']][0]) {
                                            $data_session = $_SESSION['PHPQUERY']['schData'];
                                            $data         = json_decode($data_session, true);
                                            $directGetFlg = 1;
                                        }
                                    }
                                }
                            } else if ($directResultFlg === '1' && $touch === '1') {
                                if (isset($_SESSION['PHPQUERYTOUCH']['schData'])) {
                                    if ($_SESSION['PHPQUERYTOUCH']['schData'] !== '') {
                                        if ($FDB2CSV3[$result['QRYGSQRY']][0] === $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'][$result['QRYGSQRY']][0]) {
                                            $data_session = $_SESSION['PHPQUERYTOUCH']['schData'];
                                            $data         = json_decode($data_session, true);
                                            $directGetFlg = 1;
                                        }
                                    }
                                }
                            }
                        }
                        //PC版のみ、検索状態と検索条件をセッションに保存
                        if ($touch === '0') {
                            $_SESSION['PHPQUERY']['schData']  = $data_session;
                            $_SESSION['PHPQUERY']['FDB2CSV3'][$result['QRYGSQRY']][] = $FDB2CSV3[$result['QRYGSQRY']][0];
                        } else if ($touch === '1') {
                            $_SESSION['PHPQUERYTOUCH']['schData']  = $data_session;
                            $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'][$result['QRYGSQRY']][] = $FDB2CSV3[$result['QRYGSQRY']][0];
                        }
                    }
                    //検索条件無し又検索条件に非表示だけの場合
                    if ($rtn === 0) {
                        if ((count($FDB2CSV3[$result['QRYGSQRY']][0]) === 0 || $setCmdFlg === false)) {
                            $schFlgCount++;
                        } else {
                            if (count($FDB2CSV3[$result['QRYGSQRY']][0]) > 0) {
                                $condHiddenFlg = true;
                                $fdb3=$FDB2CSV3[$result['QRYGSQRY']][0];
                                foreach ($fdb3 as $fdb2csv3Data) {
                                    if ($fdb2csv3Data['CNSTKB'] != 3) {
                                        $condHiddenFlg = false;
                                    }
                                }
                                if ($condHiddenFlg === true) {
                                    $schFlgCount++;
                                }
                            }
                        }
                    }
                    //定義の条件変更することがあるかのチェック
                    if ($rtn === 0) {
                        $OLD_FDB2CSV3 = (isset($_POST['csvoldAry']) ? json_decode($_POST['csvoldAry'], true) : '');
                        if ($OLD_FDB2CSV3[$result['QRYGSQRY']][0] !== '') {
                            if ($OLD_FDB2CSV3[$result['QRYGSQRY']][0] != $FDB2CSV3[$result['QRYGSQRY']][0]) {
                                $rtn = 2;
                                $msg = showMsg('FAIL_MENU_REQ', array('クエリーの検索条件'
                                )); //"クエリーの検索条件が変更されています。<br/>再度メニューから実行し直してください";
                                break;
                            }
                        }
                    }
                    // パラメータデータ取得して、結果画面に検索条件表示非表示チェック
                    if ($rtn === 0) {
                        $setCmdFlg       = true;
                        $directResultFlg = (isset($_POST['directResultFlg']) ? $_POST['directResultFlg'] : '0');
                        $DIR             = false;
                        if ($directResultFlg === '1') {
                            $DIR        = true;
                            $setCmdData = array();
                            $DGBFLG     = false;
                            $DGAFLG     = false;
                            $FDB2CSV1[$result['QRYGSQRY']][0]['DGBFLG']=$DGBFLG;
                            $FDB2CSV1[$result['QRYGSQRY']][0]['DGAFLG']=$DGAFLG;
                            if (count($data[$result['QRYGSQRY']][0]) > 0) {
                                $setCmdData[$result['QRYGSQRY']][] = $data[$result['QRYGSQRY']][0];
                            } else {
                                $dataArr = array();
                                $rs      = fnBSQLHCND($db2con,$result['QRYGSQRY']);
                                if ($rs['result'] !== true) {
                                    $rtn = 1;
                                    $msg = showMsg($rs['result']);
                                    break;
                                } else {
                                    $dataArr = $rs['data'];
                                }
                                $dataArr=umEx($dataArr);
                                $setCmdData[$result['QRYGSQRY']][] =$dataArr;
                            }
                        } else {
                            $setCmdData[$result['QRYGSQRY']][] = $data[$result['QRYGSQRY']][0];
                        }
                        //配列にCOUNTが一つだけあるとき、データーが非表示かどうかチェックして検索ボタン表示する権限
                        if ($setCmdData[$result['QRYGSQRY']][0] !== '') {
                            if (count($setCmdData[$result['QRYGSQRY']][0]) === 1) {
                                if ($setCmdData[$result['QRYGSQRY']][0]['CNDSTKB'] == '3') {
                                    $setCmdFlg = false;
                                }
                            }
                        }
                    }
                    // CL実行があるか取得
                    if ($rtn === 0) {
                        cmSetPHPQUERY($db2con);
                        $rsCLChk = chkFDB2CSV1PG($db2con, $result['QRYGSQRY']);
                        if ($rsCLChk['result'] !== true) {
                            $rtn = 1;
                            $msg = $rsCLChk['result'];
                            break;
                        } else {
                            if (count($rsCLChk['data']) > 0) {
                                $fdb2csv1pg = $rsCLChk['data'][0];
                                if ($fdb2csv1pg['DGBLIB'] !== '' || $fdb2csv1pg['DGBPGM'] !== '') {
                                    $DGBFLG = true;
                                    $FDB2CSV1[$result['QRYGSQRY']][0]['DGBFLG']=$DGBFLG;
                                }
                                if ($fdb2csv1pg['DGALIB'] !== '' || $fdb2csv1pg['DGAPGM'] !== '') {
                                    $DGAFLG = true;
                                    $FDB2CSV1[$result['QRYGSQRY']][0]['DGAFLG']=$DGAFLG;
                                }
                            }
                        }
                    }
                    // パラメータ条件データチェック
                    if ($rtn === 0) {
                        cmSetPHPQUERY($db2con);
                        $clParamData = $data;
                        $resChkCond  = chkSQLParamData($db2con, $result['QRYGSQRY'], $data[$result['QRYGSQRY']][0]);
                        if ($resChkCond['RTN'] > 0) {
                            $rtn = 1;
                            $msg = $resChkCond['MSG'];
                            break;
                        } else {
                            $cndParamList[$result['QRYGSQRY']][] = $resChkCond['DATA']['BASEFRMPARAMDATA'];
                        }
                    }
                    if ($rtn === 0) {
                        $resSQLInfo = getQrySQLData($db2con, $result['QRYGSQRY']);
                        if ($resSQLInfo['RTN'] !== 0) {
                            $rtn = 1;
                            $msg = $resSQLInfo['MSG'];
                            break;
                        } else {
                            $qryData[$result['QRYGSQRY']][] = $resSQLInfo['QRYDATA'];
                        }
                    }
                    if($rtn===0){
                        cmDb2Close($db2con);
                        $paramArr     = '';
                        $fdb2csv1Info = $qryData[$result['QRYGSQRY']][0]['FDB2CSV1'][0];
                        $libArr       = preg_split("/\s+/", $fdb2csv1Info['D1LIBL']);
                        $LIBLIST      = join(' ', $libArr);
                        if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                            $sPHPKey=array_search(RDB,$PHPNameList);
                            //DB CONNECTION
                            if($keyIndex===0 || $sPHPKey===false){
                                $PHPNameList[]=RDB;
                                $option = array(
                                    'i5_naming' => DB2_I5_NAMING_ON
                                );
                                $db2LibCon = cmDb2ConOption($option);
                                $db2LibConTmp=$db2LibCon;
                                e_log('【SQL DB CONNECTION NEW】');
                            }else{
                                e_log("【SQL DB CONNECTION OVERRIDE】");
                                $db2LibCon=$db2LibConTmp;
                            }
                            $db2LibListChange=cmPHPCHGLIB($db2LibCon,$LIBLIST);
                            //$db2LibCon = cmDb2ConLib($LIBLIST);
                        } else {
                            $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1Info['D1RDB'];
                            //$db2LibCon= cmDB2ConRDB($LIBLIST);
                            $sRDBKey=array_search($fdb2csv1Info['D1RDB'], $RDBNameList);
                            if($sRDBKey===false){//ＲＤＢ名が新しいなら動く
                                $RDBNameList[]=$fdb2csv1Info['D1RDB'];
                                //DB CONNECTION
                                $option = array(
                                    'i5_naming' => DB2_I5_NAMING_ON
                                );
                                $db2LibCon = cmDb2ConOptionRDB($option);
                                $RDBNameObj[$fdb2csv1Info['D1RDB']][]=$db2LibCon;
                                e_log("【SQL RDB CONNECTION NEW】");
                            }else{
                                e_log("【SQL RDB CONNECTION OVERRIDE】");
                                $db2LibCon=$RDBNameObj[$fdb2csv1Info['D1RDB']][0];
                            }
                            $db2LibListChange=cmRDBCHGLIBL($db2LibCon,$LIBLIST);
                        }
                        if ($licenseCl === true) {
                            if ($FDB2CSV1[$result['QRYGSQRY']][0]['DGBFLG'] === true || $FDB2CSV1[$result['QRYGSQRY']][0]['DGAFLG'] === true) {
                                if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                    //init処理
                                    cmIntWC($db2LibCon, $initRs, 'INT', $result['QRYGSQRY']);
                                } else {
                                    $initRs = "0";
                                }
                            }
                        }
                        //9以外の場合
                        if ($initRs !== "9") {
                            //CL連携
                            //ライセンスのCL連携権限がある場合のみ実行
                            if ($licenseCl === true) {
                                if ($FDB2CSV1[$result['QRYGSQRY']][0]['DGBFLG'] === true || $FDB2CSV1[$result['QRYGSQRY']][0]['DGAFLG'] === true) {
                                    if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                        //e_log('CLParameterData'.print_r($setCmdData,true));
                                        setSQLCmd($db2LibCon, $result['QRYGSQRY'], $FDB2CSV1[$result['QRYGSQRY']][0]['D1WEBF'], $setCmdData[$result['QRYGSQRY']][0]);
                                    }
                                }
                                if ($FDB2CSV1[$result['QRYGSQRY']][0]['DGBFLG'] === true) {
                                    //CL実行
                                    if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                        cmIntWC($db2LibCon, $initRs, 'BEF', $result['QRYGSQRY']);
                                    } else {
                                        //RDBのCL実行呼び出し;
                                        $rsClExe = callClExecute($db2LibCon, $result['QRYGSQRY'], $clParamData[$result['QRYGSQRY']][0], 'BEF');
                                        if ($rsClExe === 0) {
                                            $initRs = "9";
                                        } else {
                                            $initRs = "0";
                                        }
                                    }
                                    
                                    if ($initRs !== "9") {
                                    } else {
                                        $rtn = 1;
                                        //$msg = showMsg('FAIL_FUNC',array('実行前CL連携の処理'));//'実行前CL連携の処理に失敗しました。';
                                        $msg = showMsg('FAIL_FUNC', array(
                                            array('実行前','CL連携','の','処理')
                                        ));
                                        break;
                                    }
                                }
                            }
                            if ($rtn === 0) {
                                if (count($qryData[$result['QRYGSQRY']][0]['BSQLCND']) > 0) {
                                    $paramArr = bindParamArr($cndParamList[$result['QRYGSQRY']][0]);
                                } else {
                                    $paramArr = array();
                                }
								/*DBのフィードがブランクの場合【1=1】に代る*/
								$repArr=array();
                                $strSQL = $qryData[$result['QRYGSQRY']][0]['BSQLEXEDAT']['EXESQL'];
                                if (count($paramArr) > 0) {
                                    if (count($paramArr['WITHPARAMDATA']) > 0) {
                                        $strSQL = bindWithParamSQLQry($strSQL, $paramArr['WITHPARAMDATA']);
                                    }
									$repArr=cmReplaceSQLParam($strSQL,$paramArr['PARAMDATA']);
                                    //$strExeSQLParam = bindParamSQLQry($strSQL, $paramArr['PARAMDATA']);
	                                $strExeSQLParam = bindParamSQLQry($repArr["strSQL"], $repArr["params"]);

                                } else {
                                    $strExeSQLParam = $strSQL;
									$repArr["params"]=$paramArr;
                                }
                                $rsExeSQL = execSQLQry($db2LibCon, $strExeSQLParam,$repArr["params"]);
                                if ($rsExeSQL['RTN'] !== 0) {
                                    if ($rsExeSQL['RTN'] === 2) {
                                        $rtn = 2;
                                        $msg = $rsExeSQL['MSG'];
										error_log('msg talbe init = '.$msg);
                                        break;
                                    } else {
                                        $rtn = 1;
                                        $msg = $rsExeSQL['MSG'];
										error_log('msg talbe init = '.$msg);

                                        break;
                                    }
                                } else {
                                    $estart = microtime(true);//190405
                                    $rsCreate = createSQLQryTbl($db2LibCon, $strExeSQLParam,$repArr["params"], $filenamelist[$result['QRYGSQRY']][0]);
                                    if ($rsCreate['RTN'] !== 0) {
                                        $rtn = 1;
                                        $msg = $rsCreate['MSG'];
                                        break;
                                    } else {
                                        //くエリーの最大件数190315
                                        $msgmax = '';
                                        if($rsCreate['MSGMAX'] !== '' ){
                                            $msgmax = $rsCreate['MSGMAX'];
                                        }
                                        $eend = microtime(true);//190405
                                        //error_log("RESULT 20181010 => ".print_r($result,true));
                                        $filenamelist[$result['QRYGSQRY']][0]= $rsCreate['TMPTBLNM'];
                                        if($result['GPHKEY'] != ''){// add for graph 20181010 MSM
                                            $oldFilename = $rsCreate['TMPTBLNM'];
                                        }
                                        if ($result['PMPKEY'] === '') {
                                            if ($htmlFlg === '1' && $seigyoFlg === 1 && $result['LASTFLG']==='1') {
                                                //制御取得
                                                $rsSeigyoCreate = createSeigyoSQLQryTbl($result['QRYGSQRY'], $filenamelist[$result['QRYGSQRY']][0], $filterData[$result['QRYGSQRY']][0]);
                                                $filenamelist[$result['QRYGSQRY']][0]       = $rsSeigyoCreate;
                                            }
                                        } else {
											$oldFilename=$filenamelist[$result['QRYGSQRY']][0];
                                            $rsPivotCreate = createPivotSQLQryTbl($result['QRYGSQRY'], $result['PMPKEY'], $filenamelist[$result['QRYGSQRY']][0], $filterData[$result['QRYGSQRY']][0]);
                                            $filenamelist[$result['QRYGSQRY']][0] = $rsPivotCreate;
                                        }
                                        
                                        $add_flg = ($getFDB2CSV1['D1OUTA'] === '1')?true:false;
                                        
                                        e_log('⑤クエリー実行の出力ファイルの作成');
                                        if($licenseFileOut){
                                            //出力ファイルがあった場合
                                            if($getFDB2CSV1['D1OUTLIB'] !=='' && $getFDB2CSV1['D1OUTFIL'] !=='' ){
                                                //出力ファイル動く
                                                if($result['LASTFLG'] ==='1'){//一番最後のクエリーのため動く
                                                    //実行時にユーザーが出力先を設定と確認設定ならぜひデータＯｂｊを持ってる
                                                    $outputfdatawin=array();
                                                    if($getFDB2CSV1['D1OUTJ']==='1' || $getFDB2CSV1['D1OUTR'] ==='2'){
                                                        $res=array();
                                                        $res['D1NAME']=$getFDB2CSV1['D1NAME'];
                                                        $res['D1OUTJ']=$getFDB2CSV1['D1OUTJ'];
                                                        $res['D1OUTLIB']=$getFDB2CSV1['D1OUTLIB'];
                                                        $res['D1OUTFIL']=$getFDB2CSV1['D1OUTFIL'];
                                                        $res['D1OUTR']=$getFDB2CSV1['D1OUTR'];
    													//
    													$res['D1OUTA']=$getFDB2CSV1['D1OUTA'];
    													//
                                                        $outputfdatawin[]=$res;
                                                    }
                                                    //実行時にユーザーが出力先を設定がないなら出力ファイル動く
                                                    if($getFDB2CSV1['D1OUTJ'] !=='1'){
                                                        if($getFDB2CSV1['D1OUTA'] !=='1'){
                                                            //ライブラリーとファイルをSYSからチェックは出力しないと確認のためだけチェック
    	                                                    if($getFDB2CSV1['D1OUTR'] !=='1'){
    	                                                        //ライブラリーとファイルをSYSからチェック
    	                                                        $r=fnChkExistFileOutput($db2LibCon,$getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL']);
    	                                                        if($r['result']!==true){
    	                                                            if($r['rtn']===4){
    	                                                                $rtn=($getFDB2CSV1['D1OUTR']==='2')?5:6;//出力ファイルのためCOMFIRMメーッセジを出す
    	                                                                $msg=($getFDB2CSV1['D1OUTR']==='2')?showMsg('FAIL_OUTC',array($getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL']))
    	                                                                        : showMsg('FAIL_OUT',array($getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL']));
    	                                                            }else if($r['rtn']===1){
    	                                                                $rtn=1;
    	                                                                $msg=showMsg($r['result']);
    	                                                                break;
    	                                                            }
    	                                                        }
    	                                                    }
                                                        }
                                                        //TMPテーブルあるかどうかチェック
                                                        if ($rtn === 0) {
                                                            $systables = cmGetSystables($db2LibCon, SAVE_DB, $filenamelist[$result['QRYGSQRY']][0]);
                                                            if (count($systables)<=0){
                                                                $msg = showMsg('FAIL_MENU_DEL', array('出力ファイルの実行中のデータ'));
                                                                $rtn = 1;
                                                                break;
                                                            }
                                                        }
                                                        //出力ファイル作成
                                                        if($rtn===0){
                                                            $r=createOutputFileTable($db2LibCon,$filenamelist[$result['QRYGSQRY']][0],$getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL'],$getFDB2CSV1['D1NAME'],$add_flg);
                                                            if ($r['RTN'] !== 0) {
                                                                if($r['RTN'] !== 11){
    		                                                        $rtn = 1;
    		                                                        $msg = showMsg($r['MSG']);
    		                                                        break;
    		                                                    } else {
    		                                                        $rtn = 11;
    		                                                        $msg = showMsg($r['MSG']);
    		                                                    }
                                                            }else{
                                                                $rtn = 7;
                                                                $msg = showMsg($r['MSG']);
                                                            }
                                                        }
                                                    }
                                                }else{
                                                    $isCreateTmp=true;
                                                    //ライブラリーとファイルをSYSからチェックは出力しないと確認のためだけチェック
                                                    if($getFDB2CSV1['D1OUTR'] !=='1'){
                                                        $r=fnChkExistFileOutput($db2LibCon,$getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL']);
                                                        if($r['result']!==true){
                                                            if($r['rtn']===4){
                                                                $isCreateTmp=false;
                                                            }else if($r['rtn']===1){
                                                                $rtn=1;
                                                                $msg=showMsg($r['result']);
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    //TMPテーブルあるかどうかチェック
                                                    if ($rtn === 0 && $isCreateTmp) {
                                                        $systables = cmGetSystables($db2LibCon, SAVE_DB, $filenamelist[$result['QRYGSQRY']][0]);
                                                        if (count($systables)<=0){
                                                            $msg = showMsg('FAIL_MENU_DEL', array('出力ファイルの実行中のデータ'));
                                                            $rtn = 1;
                                                            break;
                                                        }
                                                    }
                                                    //出力ファイル作成
                                                    if($rtn===0 && $isCreateTmp){
                                                        $r=createOutputFileTable($db2LibCon,$filenamelist[$result['QRYGSQRY']][0],$getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL'],$getFDB2CSV1['D1NAME'],$add_flg);
                                                        if ($r['RTN'] !== 0) {
                                                            $rtn = 1;
                                                            $msg = showMsg($r['MSG']);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //ライセンスのCL連携権限がある場合のみ実行
                                        if ($licenseCl === true) {
                                            if ($FDB2CSV1[$result['QRYGSQRY']][0]['DGAFLG'] === true) {
                                                //CL実行
                                                if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                                    cmIntWC($db2LibCon, $initRs, 'AFT', $result['QRYGSQRY']);
                                                } else {
                                                    //RDBのCL実行呼び出し;
                                                    $rsClExe = callClExecute($db2LibCon, $result['QRYGSQRY'], $clParamData[$result['QRYGSQRY']][0], 'AFT');
                                                    if ($rsClExe === 0) {
                                                        $initRs = "9";
                                                    } else {
                                                        $initRs = "0";
                                                    }
                                                }
                                                //9以外の場合、ＤＯ処理
                                                if ($initRs === "9") {
                                                    $rtn = 1;
                                                    $msg = showMsg('FAIL_FUNC', array(
                                                        '実行後CL連携の処理'
                                                    )); //'実行後CL連携の処理に失敗しました。';
                                                    break;
                                                }
                                            }
                                        }
                                        $logrs  = '';
                                        $db2con = cmDb2Con();
                                        cmSetPHPQUERY($db2con);
                                        if ($logFlg === '0') {
                                            $sql_wlsqlb = $strExeSQLParam;
                                            if(strlen($sql_wlsqlb) > 32730){
                                                $sql_wlsqlb = showMsg('INVALID_SQLLENGTH');
                                            }
                                            $jktime = $eend - $estart;
                                            $rs = cmInsertDB2WLOG($db2con, $logrs, $usrinfo[0]['WUUID'], 'D', $result['QRYGSQRY'], $data[$result['QRYGSQRY']][0], '1',$sql_wlsqlb);
                                            cmInsertDB2WSTL($db2con, $rs['DATE'],$rs['TIME'],$usrinfo[0]['WUUID'],$jktime,$rsCreate['QRYCNT']);//クエリー実行時間作成
                                            fnDB2QHISExe($db2con, $result['QRYGSQRY'], $usrinfo[0]['WUUID']);
                                            //クエリー実行時間の最大、最小、平均アップデート
                                            $dataHis = fnGetDB2QHIS($db2con,$result['QRYGSQRY']);
                                            $max = $dataHis['data'][0]['DQTMAX'];
                                            $min = $dataHis['data'][0]['DQTMIN'];
                                            if($max !== '.00' && $min !== '.00'){
                                                $max = max($max,$jktime);
                                                $min = min($min,$jktime);
                                            }else{
                                                $max = $jktime;
                                                $min = $jktime;
                                            }
                                            cmUpdDB2QHIS($db2con, $result['QRYGSQRY'], $max, $min, ($max + $min)/2); 
                                        }
                                    }
                                }
                                
                            }
                        } else {
                            $rtn = 1;
                            $msg = showMsg('FAIL_FUNC', array(
                                'CL連携の準備'
                            )); //'CL連携の準備に失敗しました。';
                            break;
                        }
                        //最後にクローズ
                        if($result['LASTFLG']==='1'){
                            cmDb2Close($db2LibCon);
                        }
                    }
                } else {
                    // WEB通常で作成したクエリーの場合
                    //クエリー連携の検索項目チェック
                    e_log('WEB通常で作成したクエリーの場合');
                    if ($rtn === 0) {
                        if ($DRILLFLG === '1') {
                            if($result['LASTFLG']==='1'){
                                $webData=$data[$result['QRYGSQRY']][0];
                                $res   =fnChkHisu($data[$result['QRYGSQRY']][0],$SEARCHKEYARR[$result['QRYGSQRY']][0]);
                                $drill   = $res;
                                if ($res['result'] !== true) {
                                    //resultflg 1 => '非表示、固定'
                                    //resultflg 2 => 'RANGE、IS'
                                    //resultflg 3=> '必須'
                                    if ($res['resultflg'] === '1') {
                                        $rtn = 1;
                                        $msg = showMsg('DRILL_COL_FAIL', array('非表示','固定'));
                                        break;
                                    } else if ($res['resultflg'] === '2') {
                                        $rtn = 1;
                                        $msg = showMsg('DRILL_COL_FAIL', array('RANGE','IS'));
                                        break;
                                    } else if ($res['resultflg'] === '3') {
                                        $rtn = 1;
                                        $msg = showMsg('DRILL_HISSU');
                                        break;
                                    }
                                }
                                for ($i = 0; $i < count($webData); $i++) {
                                    $datalist = $webData[$i]['CNDSDATA'];
                                    for ($j = 0; $j < count($datalist); $j++) {
                                        $DA = $datalist[$j];
                                        if ($rtn === 0) {
                                            if ($DA['CNDDATA'][0] === '' && $DA['CNDTYP'] === '1') {
                                                $rtn = 1;
                                                $msg = showMsg('DRILL_HISSU');
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    /*********/
                    //条件情報【BQRYNCNDとFDB2CSV2とFDB2CSV5】を取得(セッションに保存,tableInitで比較に使用)
                    if ($rtn === 0) {
                        $res = fnGetBQRYCND($db2con, $result['QRYGSQRY']);
                        if ($res['result'] !== true) {
                            $rtn = 1;
                            $msg = showMsg($res['result']);
                            break;
                        } else {
                            $FDB2CSV3[$result['QRYGSQRY']][] = $res['data'];
                        }
                    }
                    if ($rtn === 0) {
                        //直接result画面から来たときは直前のセッションを比較して、
                        //FDB2CSV3が同じなら直前の検索状態を表示する
                        //error_log('パラメータデータ：'.print_r($data,true).'データカウント：'.count($data));
                        if ($paramFlg === '') {
                            if ($directResultFlg === '1' && $touch === '0') {
                                if (isset($_SESSION['PHPQUERY']['schData'])) {
                                    if ($_SESSION['PHPQUERY']['schData'] !== '') {
                                        if ($FDB2CSV3[$result['QRYGSQRY']][0] === $_SESSION['PHPQUERY']['FDB2CSV3'][$result['QRYGSQRY']][0]) {
                                            $data_session = $_SESSION['PHPQUERY']['schData'];
                                            $data         = json_decode($data_session, true);
                                            $directGetFlg = 1;
                                        }
                                    }
                                }
                            } else if ($directResultFlg === '1' && $touch === '1') {
                                if (isset($_SESSION['PHPQUERYTOUCH']['schData'])) {
                                    if ($_SESSION['PHPQUERYTOUCH']['schData'] !== '') {
                                        if ($FDB2CSV3[$result['QRYGSQRY']][0] === $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'][$result['QRYGSQRY']][0]) {
                                            $data_session = $_SESSION['PHPQUERYTOUCH']['schData'];
                                            $data         = json_decode($data_session, true);
                                            $directGetFlg = 1;
                                        }
                                    }
                                }
                            }
                        }
                        //PC版のみ、検索状態と検索条件をセッションに保存
                        if ($touch === '0') {
                            $_SESSION['PHPQUERY']['schData']  = $data_session;
                            $_SESSION['PHPQUERY']['FDB2CSV3'][$result['QRYGSQRY']][] = $FDB2CSV3[$result['QRYGSQRY']][0];
                        } else if ($touch === '1') {
                            $_SESSION['PHPQUERYTOUCH']['schData']  = $data_session;
                            $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'][$result['QRYGSQRY']][] = $FDB2CSV3[$result['QRYGSQRY']][0];
                        }
                    }
                    if ($rtn === 0) {
                        //検索条件フラグ
                        if (count($FDB2CSV3[$result['QRYGSQRY']][0]) === 0 || $setCmdFlg === false) {
                            $schFlgCount++;
                        } else {
                            $condHiddenFlg=true;
                            $fdb3=$FDB2CSV3[$result['QRYGSQRY']][0];
                            foreach($fdb3 as $fdb2csv3Data){
                                if ($fdb2csv3Data['CNSTKB'] != 3) {
                                    $condHiddenFlg = false;
                                }
                            }
                            if ($condHiddenFlg === true) {
                                $schFlgCount++;
                            }
                        }
                    }
                    //定義の条件変更することがあるかのチェック
                    if ($rtn === 0) {
                        $OLD_FDB2CSV3 = (isset($_POST['csvoldAry']) ? json_decode($_POST['csvoldAry'], true) : '');
                        if ($OLD_FDB2CSV3[$result['QRYGSQRY']][0] !== '') {
                            if ($OLD_FDB2CSV3[$result['QRYGSQRY']][0] != $FDB2CSV3[$result['QRYGSQRY']][0]) {
                                $rtn = 2;
                                $msg = showMsg('FAIL_MENU_REQ', array(
                                    'クエリーの検索条件'
                                )); //"クエリーの検索条件が変更されています。<br/>再度メニューから実行し直してください";
                                break;
                            }
                        }
                    }
                    if ($rtn === 0) {
                        $setCmdFlg       = true;
                        $directResultFlg = (isset($_POST['directResultFlg']) ? $_POST['directResultFlg'] : '0');
                        $DIR             = false;
                        if ($directResultFlg === '1') {
                            $DIR        = true;
                            $setCmdData = array();
                            $DGBFLG     = false;
                            $DGAFLG     = false;
                            $FDB2CSV1[$result['QRYGSQRY']][0]['DGBFLG']=$DGBFLG;
                            $FDB2CSV1[$result['QRYGSQRY']][0]['DGAFLG']=$DGAFLG;
                            if (count($data[$result['QRYGSQRY']][0]) > 0) {
                                $setCmdData[$result['QRYGSQRY']][] = $data[$result['QRYGSQRY']][0];
                            } else {
                                $dataArr = array();
                                $rs      = fnBQRYCNDDAT($db2con, $result['QRYGSQRY']);
                                if ($rs['result'] !== true) {
                                    $rtn = 1;
                                    $msg = showMsg($rs['result']);
                                    break;
                                } else {
                                    $dataArr = $rs['data'];
                                }
                                $dataArr = umEx($dataArr);
                                if (count($dataArr) > 0) {
                                    $fnCdata=fnCreateCndData($dataArr);
                                    $setCmdData[$result['QRYGSQRY']][] = $fnCdata[0];
                                }
                            }
                        } else {
                            $setCmdData[$result['QRYGSQRY']][] = $data[$result['QRYGSQRY']][0];
                        }
                        //配列にCOUNTが一つだけあるとき、データーが非表示かどうかチェックして検索ボタン表示する権限
                        if ($setCmdData[$result['QRYGSQRY']][0] !== '') {
                            $key = key((array) $setCmdData[$result['QRYGSQRY']][0]);
                            if (count($key) === 1) {
                                $CNDSDATA = $setCmdData[$result['QRYGSQRY']][0][CNDSDATA];
                                if (count($CNDSDATA) === 1) {
                                    if ($CNDSDATA[0]['CNDTYP'] == '3') {
                                        $setCmdFlg = false;
                                    }
                                }
                            }
                        }

                    }
                    
                    if ($rtn === 0) {
                        cmSetPHPQUERY($db2con);
                        $rsCLChk = chkFDB2CSV1PG($db2con, $result['QRYGSQRY']);
                        if ($rsCLChk['result'] !== true) {
                            $rtn = 1;
                            $msg = $rsCLChk['result'];
                            break;
                        } else {
                            if (count($rsCLChk['data']) > 0) {
                                $fdb2csv1pg = $rsCLChk['data'][0];
                                if ($fdb2csv1pg['DGBLIB'] !== '' || $fdb2csv1pg['DGBPGM'] !== '') {
                                    $DGBFLG = true;
                                    $FDB2CSV1[$result['QRYGSQRY']][0]['DGBFLG']=$DGBFLG;
                                }
                                if ($fdb2csv1pg['DGALIB'] !== '' || $fdb2csv1pg['DGAPGM'] !== '') {
                                    $DGAFLG = true;
                                    $FDB2CSV1[$result['QRYGSQRY']][0]['DGAFLG']=$DGAFLG;
                                }
                            }
                        }
                    }                
                    if ($rtn === 0) {
                        cmSetPHPQUERY($db2con);
                        $clParamData = $data;
                        $resChkCond  = chkCondParamData($db2con, $result['QRYGSQRY'], $data[$result['QRYGSQRY']][0]);
                        if ($resChkCond['RTN'] > 0) {
                            $rtn = 1;
                            $msg = $resChkCond['MSG'];
                            break;
                        } else {
                            $cndParamList[$result['QRYGSQRY']][] = $resChkCond['DATA']['BASEDCNDDATA'];
                        }
                    }
                     $WLSQLB = '';
                    if($rtn===0){
                        if ($result['PMPKEY'] === '') {
                            if ($htmlFlg === '1' && $seigyoFlg === 1 && $result['LASTFLG']==='1') {
                                // 制御のためクエリー実行
                                e_log('制御SQL実行1:' . $d1name . 'ピボットキー' . $pmpkey);
                                if (count($cndParamList) === 0) {
                                    $resExeSql = runExecuteSQLSEIGYORN($db2con, $result['QRYGSQRY'], NULL, $filterData['QRYGSQRY'][0]);
                                } else {
                                    $resExeSql = runExecuteSQLSEIGYORN($db2con, $result['QRYGSQRY'], $cndParamList[$result['QRYGSQRY']][0], $filterData[$result['QRYGSQRY']][0]);
                                }
                                if($filterFlg ==='1' && count($cndParamList) === 0){
                                   $resExeSql = runExecuteSQLSEIGYORN($db2con, $result['QRYGSQRY'], NULL, $filterData['QRYGSQRY'][0]);
                                } else {
                                   $resExeSql = runExecuteSQLSEIGYORN($db2con, $result['QRYGSQRY'], $cndParamList[$result['QRYGSQRY']][0], $filterData[$result['QRYGSQRY']][0]);
                                }
                                if ($resExeSql['RTN'] !== 0) {
                                    $rtn = 1;
                                    $msg = $resExeSql['MSG'];
                                    break;
                                } else {
                                    $qryData[$result['QRYGSQRY']][] = $resExeSql;
                                    $WLSQLB = $resExeSql['STREXECSQL'];
                                  
                                    $exeSQL[$result['QRYGSQRY']][]  = array(
                                        'EXETESTSQL' => $resExeSql['STREXECTESTSQL'],
                                        'EXESQL' => $resExeSql['STREXECSQL'],
                                        'LOGSQL' => $resExeSql['LOG_SQL']
                                    );
                                }
                            } else {
                                $estart = microtime(true);
                                // クエリー実行
                                if (count($cndParamList[$result['QRYGSQRY']][0]) === 0) {
                                    $resExeSql = runExecuteSQL($db2con, $result['QRYGSQRY']);
                                } else {
                                    $resExeSql = runExecuteSQL($db2con, $result['QRYGSQRY'], $cndParamList[$result['QRYGSQRY']][0]);
                                }
                                if ($resExeSql['RTN'] !== 0) {
                                    $rtn = 1;
                                    $msg = $resExeSql['MSG'];
                                    break;
                                } else {
                                    $qryData[$result['QRYGSQRY']][] = $resExeSql;
                                    $WLSQLB = $resExeSql['STREXECSQL'];
                                   
                                    $exeSQL[$result['QRYGSQRY']][]  = array(
                                        'EXETESTSQL' => $resExeSql['STREXECTESTSQL'],
                                        'EXESQL' => $resExeSql['STREXECSQL'],
                                        'LOGSQL' => $resExeSql['LOG_SQL']
                                    );
                                    $eend = microtime(true);
                                    e_log( '①クエリー情報取得処理時間：' . ($eend - $estart) . '秒');
                                }
                            }
                        } else {
                            if (count($cndParamList[$result['QRYGSQRY']][0]) === 0) {
                                $resExeSql = runExecuteSQLPIVOT($db2con, $result['QRYGSQRY'], $result['PMPKEY']);
                            } else {
                                $resExeSql = runExecuteSQLPIVOT($db2con,$result['QRYGSQRY'], $result['PMPKEY'], $cndParamList[$result['QRYGSQRY']][0]);
                            }
                            if ($resExeSql['RTN'] !== 0) {
                                $rtn = 1;
                                $msg = $resExeSql['MSG'];
                                break;
                            } else {
                                $qryData[$result['QRYGSQRY']][] = $resExeSql;
                                $WLSQLB = $resExeSql['STREXECSQL'];
                               
                                $exeSQL[$result['QRYGSQRY']][]  = array(
                                    'EXESQL' => $resExeSql['STREXECSQL'],
                                    'LOGSQL' => $resExeSql['LOG_SQL']
                                );
                            }
                        }
                    }
                    if($rtn===0){
                        cmDb2Close($db2con);
                        $qryDataInner=$qryData[$result['QRYGSQRY']][0];
                        //RDB修正
                        if ($qryDataInner['SELRDB'] === '' || $qryDataInner['SELRDB'] === RDB || $qryDataInner['SELRDB'] === RDBNAME) {
                            $sPHPKey=array_search(RDB,$PHPNameList);
                            //DB CONNECTION
                            if($keyIndex===0 || $sPHPKey===false){
                                $PHPNameList[]=RDB;
                                $option = array(
                                    'i5_naming' => DB2_I5_NAMING_ON
                                );
                                $db2LibCon = cmDb2ConOption($option);
                                $db2LibConTmp=$db2LibCon;
                                e_log('【DB CONNECTION NEW】');
                            }else{
                                e_log('【DB CONNECTION OVERRIDE】');
                                $db2LibCon=$db2LibConTmp;
                            }
                             $db2LibListChange=cmPHPCHGLIB($db2LibCon,$qryDataInner['LIBLIST']);
                            //$db2LibCon = cmDb2ConLib($qryDataInner['LIBLIST']);
                        } else {
                            $_SESSION['PHPQUERY']['RDBNM'] = $qryDataInner['SELRDB'];
                            //$db2LibCon                     = cmDB2ConRDB($qryDataInner['LIBLIST']);
                            $sRDBKey=array_search($qryDataInner['SELRDB'], $RDBNameList);
                            if($sRDBKey===false){//ＲＤＢ名が新しいなら動く
                                $RDBNameList[]=$qryDataInner['SELRDB'];
                                //DB CONNECTION
                                $option = array(
                                    'i5_naming' => DB2_I5_NAMING_ON
                                );
                                $db2LibCon = cmDb2ConOptionRDB($option);
                                $RDBNameObj[$qryDataInner['SELRDB']][]=$db2LibCon;
                                e_log('【RDB CONNECTION NEW】');
                            }else{
                                e_log('【RDB CONNECTIOIN OVERRIDE】');
                                $db2LibCon=$RDBNameObj[$qryDataInner['SELRDB']][0];
                            }
                            $db2LibListChange=cmRDBCHGLIBL($db2LibCon,$qryDataInner['LIBLIST']);

                        }
                        if ($licenseCl === true) {
                            if ($FDB2CSV1[$result['QRYGSQRY']][0]['DGBFLG'] === true || $FDB2CSV1[$result['QRYGSQRY']][0]['DGAFLG'] === true) {
                                if ($qryDataInner['SELRDB'] === '' || $qryDataInner['SELRDB'] === RDB || $qryDataInner['SELRDB'] === RDBNAME) {
                                    //init処理
                                    cmIntWC($db2LibCon, $initRs, 'INT', $result['QRYGSQRY']);
                                } else {
                                    $initRs = "0";
                                }
                            }
                        }
                        //9以外の場合
                        if ($initRs !== "9") {
                            //CL連携
                            //ライセンスのCL連携権限がある場合のみ実行
                            if ($licenseCl === true) {
                                if ($FDB2CSV1[$result['QRYGSQRY']][0]['DGBFLG'] || $FDB2CSV1[$result['QRYGSQRY']][0]['DGAFLG'] === true) {
                                    if ($qryDataInner['SELRDB'] === '' || $qryDataInner['SELRDB'] === RDB || $qryDataInner['SELRDB'] === RDBNAME) {
                                        setCmd($db2LibCon, $result['QRYGSQRY'], $FDB2CSV1[$result['QRYGSQRY']][0]['D1WEBF'], $setCmdData[$result['QRYGSQRY']][0]);
                                    }
                                }
                                if ($FDB2CSV1[$result['QRYGSQRY']][0]['DGBFLG'] === true) {
                                    //CL実行
                                    if ($qryDataInner['SELRDB'] === '' || $qryDataInner['SELRDB'] === RDB || $qryDataInner['SELRDB'] === RDBNAME) {
                                        cmIntWC($db2LibCon, $initRs, 'BEF', $result['QRYGSQRY']);
                                    } else {
                                        //RDBのCL実行呼び出し;
                                        $rsClExe = callClExecute($db2LibCon, $result['QRYGSQRY'], $clParamData[$result['QRYGSQRY']][0], 'BEF');
                                        if ($rsClExe === 0) {
                                            $initRs = "9";
                                        } else {
                                            $initRs = "0";
                                        }
                                    }
                                    
                                    if ($initRs !== "9") {
                                    } else {
                                        $rtn = 1;
                                        //$msg = showMsg('FAIL_FUNC',array('実行前CL連携の処理'));//'実行前CL連携の処理に失敗しました。';
                                        $msg = showMsg('FAIL_FUNC', array(
                                            array('実行前','CL連携','の','処理')
                                        ));
                                        break;
                                    }
                                }
                                
                            }
                            $strSQLdata = '';
                            if ($rtn === 0) {
                                $estart = microtime(true);
                                if (($result['LASTFLG']==='1' && $htmlFlg === '1' && $seigyoFlg === 1) || $result['PMPKEY'] !== '') {
                                    if (CRTQTEMPTBL_FLG === 1) {
                                        if (count($qryDataInner['MBRDATALST']) > 0) {
                                            foreach ($qryDataInner['MBRDATALST'] as $mbrdata) {
                                                $strSQL = '';
                                                if ($mbrdata['FILTYP'] === 'L' || $mbrdata['FILTYP'] === 'V') {
                                                    $rs = dropTmpFile($db2LibCon, 'QTEMP', $mbrdata['TBLNM']);
                                                    $rs = createTmpFil($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'],$strSQL);
                                                    if ($rs !== true) {
                                                        e_log('テーブル設定失敗');
                                                        $rtn = 1;
                                                        $msg = showMsg($rs);
                                                        break;
                                                    }else{
                                                        if($strSQLdata !== ''){
                                                            $strSQLdata .= '<br/>';
                                                        }
                                                        $strSQLdata .= $strSQL;
                                                    }
                                                } else {
                                                    //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                                    $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'],$strSQL);
                                                    if ($rs !== true) {
                                                        e_log('テーブル設定失敗');
                                                        $rtn = 1;
                                                        $msg = showMsg($rs);
                                                        break;
                                                    }else{
                                                        if($strSQLdata !== ''){
                                                            $strSQLdata .= '<br/>';
                                                        }
                                                        $strSQLdata .= $strSQL;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if (count($qryDataInner['MBRDATALST']) > 0) {
                                            foreach ($qryDataInner['MBRDATALST'] as $mbrdata) {
                                                $strSQL = '';
                                                //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                                $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'],$strSQL);
                                                if ($rs !== true) {
                                                    e_log('テーブル設定失敗');
                                                    $rtn = 1;
                                                    $msg = showMsg($rs);
                                                    break;
                                                }else{
                                                    if($strSQLdata !== ''){
                                                        $strSQLdata .= '<br/>';
                                                    }
                                                    $strSQLdata .= $strSQL;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (count($qryDataInner['MBRDATALST']) > 0) {
                                        foreach ($qryDataInner['MBRDATALST'] as $mbrdata) {
                                            $strSQL = '';
                                            //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                            $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'],$strSQL);
                                            if ($rs !== true) {
                                                e_log('テーブル設定失敗');
                                                $rtn = 1;
                                                $msg = showMsg($rs);
                                                break;
                                            }else{
                                                    if($strSQLdata !== ''){
                                                        $strSQLdata .= '<br/>';
                                                    }
                                                    $strSQLdata .= $strSQL;
                                            }
                                        }
                                    }
                                }
                                $WLSQLB = $strSQLdata.'<br/><br/>'.$WLSQLB;
                                $eend = microtime(true);
                                e_log( '②クエリー実行チェック取得処理時間：' . ($eend - $estart) . '秒');
                                $estart = microtime(true);
                                $resExec = execQry($db2LibCon,$qryDataInner['STREXECTESTSQL'],$qryDataInner['EXECPARAM'],$qryDataInner['LIBLIST'],$qryDataInner['MBRDATALST']);
                                //$resExec = execQry($db2LibCon,$qryDataInner['STREXECSQL'],$qryDataInner['EXECPARAM'],$qryDataInner['LIBLIST'],$qryDataInner['MBRDATALST']);
                                if ($resExec['RTN'] !== 0) {
                                    if($resExec['RTN'] === 2) {
                                        $rtn = 2;
                                        $msg = showMsg($resExec['MSG']);
										error_log('execQry'.$msg);
                                        break;
                                    } else {
                                        $rtn = 1;
                                        $msg = showMsg($resExec['MSG']);
										error_log('execQry'.$msg);
                                        break; 
                                    }
                                } else {
                                    $eend = microtime(true);
                                    e_log( '③クエリー実行チェック取得処理時間：' . ($eend - $estart) . '秒');
                                    $estart = microtime(true);
                                    if ($qryDataInner['SELRDB'] === '' || $qryDataInner['SELRDB'] === RDB || $qryDataInner['SELRDB'] === RDBNAME) {
                                        $resCreateTbl = createTmpTable($db2LibCon, $qryDataInner['LIBLIST'], $qryDataInner['STREXECSQL'], $qryDataInner['EXECPARAM'], $filenamelist[$result['QRYGSQRY']][0], $qryDataInner['MBRDATALST']);
                                    } else {
                                        $db2con = cmDb2Con();
                                        cmSetPHPQUERY($db2con);
                                        $resCreateTbl = createTmpTableRDB($db2con, $db2LibCon, $qryDataInner['SELRDB'], $qryDataInner['LIBLIST'], $qryDataInner['STREXECSQL'], $qryDataInner['EXECPARAM'], $filenamelist[$result['QRYGSQRY']][0], $qryDataInner['MBRDATALST']);
                                        cmDb2Close($db2con);
                                    }
                                    if ($resCreateTbl['RTN'] !== 0) {
                                        $rtn = 1;
                                        $msg = showMsg($resCreateTbl['MSG']);
                                        break;
                                    } else {
                                        $msgmax = '';
                                        if($resCreateTbl['MSGMAX'] !== '' ){
                                            $msgmax = $resCreateTbl['MSGMAX'];
                                        }
                                        $add_flg = ($getFDB2CSV1['D1OUTA'] === '1')?true:false;
                                        $eend = microtime(true);
                                        e_log( '④クエリー実行結果の一時テーブル作成処理時間：test1' . ($eend - $estart) . '秒');
                                        e_log('⑤クエリー実行の出力ファイルの作成');
                                        if($licenseFileOut){
                                            //出力ファイルがあった場合
                                            if($getFDB2CSV1['D1OUTLIB'] !=='' && $getFDB2CSV1['D1OUTFIL'] !=='' ){
                                                //出力ファイル動く
                                                if($result['LASTFLG'] ==='1'){//一番最後のクエリーのため動く
                                                    //実行時にユーザーが出力先を設定と確認設定ならぜひデータＯｂｊを持ってる
                                                    $outputfdatawin=array();
                                                    if($getFDB2CSV1['D1OUTJ']==='1' || $getFDB2CSV1['D1OUTR'] ==='2'){
                                                        $res=array();
                                                        $res['D1NAME']=$getFDB2CSV1['D1NAME'];
                                                        $res['D1OUTJ']=$getFDB2CSV1['D1OUTJ'];
                                                        $res['D1OUTLIB']=$getFDB2CSV1['D1OUTLIB'];
                                                        $res['D1OUTFIL']=$getFDB2CSV1['D1OUTFIL'];
                                                        $res['D1OUTR']=$getFDB2CSV1['D1OUTR'];
    													//
    													$res['D1OUTA']=$getFDB2CSV1['D1OUTA'];
    													//
                                                        $outputfdatawin[]=$res;
                                                    }
                                                    //実行時にユーザーが出力先を設定がないなら出力ファイル動く
                                                    if($getFDB2CSV1['D1OUTJ'] !=='1'){
                                                        //ライブラリーとファイルをSYSからチェックは出力しないと確認のためだけチェック
                                                        if($getFDB2CSV1['D1OUTA'] !=='1') {
    	                                                    if($getFDB2CSV1['D1OUTR'] !=='1'){
    	                                                        //ライブラリーとファイルをSYSからチェック
    	                                                        $r=fnChkExistFileOutput($db2LibCon,$getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL']);
    	                                                        if($r['result']!==true){
    	                                                            if($r['rtn']===4){
    	                                                                $rtn=($getFDB2CSV1['D1OUTR']==='2')?5:6;//出力ファイルのためCOMFIRMメーッセジを出す
    	                                                                $msg=($getFDB2CSV1['D1OUTR']==='2')?showMsg('FAIL_OUTC',array($getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL']))
    	                                                                        : showMsg('FAIL_OUT',array($getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL']));
    	                                                            }else if($r['rtn']===1){
    	                                                                $rtn=1;
    	                                                                $msg=showMsg($r['result']);
    	                                                                break;
    	                                                            }
    	                                                        }
    	                                                    }
    	                                                }
                                                        //TMPテーブルあるかどうかチェック
                                                        if ($rtn === 0) {
                                                            $systables = cmGetSystables($db2LibCon, SAVE_DB, $filenamelist[$result['QRYGSQRY']][0]);
                                                            if (count($systables)<=0){
                                                                $msg = showMsg('FAIL_MENU_DEL', array('出力ファイルの実行中のデータ'));
                                                                $rtn = 1;
                                                                break;
                                                            }
                                                        }
                                                        //出力ファイル作成
                                                        if($rtn===0){
                                                            $r=createOutputFileTable($db2LibCon,$filenamelist[$result['QRYGSQRY']][0],$getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL'],$getFDB2CSV1['D1NAME'],$add_flg);
                                                            if ($r['RTN'] !== 0) {
                                                                if($r['RTN'] !== 11) {
                                                                    $rtn = 1;
    	                                                            $msg = showMsg($r['MSG']);
    	                                                            break;
                                                                } else {
                                                                    $rtn = 11;
    	                                                            $msg = showMsg($r['MSG']);
                                                                }
                                                            }else{
                                                                $rtn = 7;
                                                                $msg = showMsg($r['MSG']);
                                                            }
                                                        }
                                                    }
                                                }else{
                                                    $isCreateTmp=true;
                                                    //ライブラリーとファイルをSYSからチェックは出力しないと確認のためだけチェック
                                                    if($getFDB2CSV1['D1OUTR'] !=='1'){
                                                        $r=fnChkExistFileOutput($db2LibCon,$getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL']);
                                                        if($r['result']!==true){
                                                            if($r['rtn']===4){
                                                                $isCreateTmp=false;
                                                            }else if($r['rtn']===1){
                                                                $rtn=1;
                                                                $msg=showMsg($r['result']);
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    //TMPテーブルあるかどうかチェック
                                                    if ($rtn === 0 && $isCreateTmp) {
                                                        $systables = cmGetSystables($db2LibCon, SAVE_DB, $filenamelist[$result['QRYGSQRY']][0]);
                                                        if (count($systables)<=0){
                                                            $msg = showMsg('FAIL_MENU_DEL', array('出力ファイルの実行中のデータ'));
                                                            $rtn = 1;
                                                            break;
                                                        }
                                                    }
                                                    //出力ファイル作成
                                                    if($rtn===0 && $isCreateTmp){
                                                        $r=createOutputFileTable($db2LibCon,$filenamelist[$result['QRYGSQRY']][0],$getFDB2CSV1['D1OUTLIB'],$getFDB2CSV1['D1OUTFIL'],$getFDB2CSV1['D1NAME'],$add_flg);
                                                        if ($r['RTN'] !== 0) {
                                                            $rtn = 1;
                                                            $msg = showMsg($r['MSG']);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //ライセンスのCL連携権限がある場合のみ実行
                                        if ($licenseCl === true) {
                                            if ($FDB2CSV1[$result['QRYGSQRY']][0]['DGAFLG'] === true) {
                                                //CL実行
                                                if ($qryDataInner['SELRDB'] === '' || $qryDataInner['SELRDB'] === RDB || $qryDataInner['SELRDB'] === RDBNAME) {
                                                    cmIntWC($db2LibCon, $initRs, 'AFT', $result['QRYGSQRY']);
                                                } else {
                                                    //RDBのCL実行呼び出し;
                                                    $rsClExe = callClExecute($db2LibCon, $result['QRYGSQRY'], $clParamData[$result['QRYGSQRY']][0], 'AFT');
                                                    if ($rsClExe === 0) {
                                                        $initRs = "9";
                                                    } else {
                                                        $initRs = "0";
                                                    }
                                                }
                                                //9以外の場合、ＤＯ処理
                                                if ($initRs === "9") {
                                                    $rtn = 1;
                                                    $msg = showMsg('FAIL_FUNC', array(
                                                        '実行後CL連携の処理'
                                                    )); //'実行後CL連携の処理に失敗しました。';
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                $logrs  = '';
                                $db2con = cmDb2Con();
                                cmSetPHPQUERY($db2con);
                                if ($logFlg === '0') {
                                    $sql_wlsqlb = $WLSQLB;
                                    if(strlen($sql_wlsqlb) > 32730){
                                        $sql_wlsqlb = showMsg('INVALID_SQLLENGTH');
                                    }
                                    $jktime = $eend - $estart;
                                    $rs = cmInsertDB2WLOG($db2con, $logrs, $usrinfo[0]['WUUID'], 'D', $result['QRYGSQRY'], $data[$result['QRYGSQRY']][0], '1',$sql_wlsqlb);
                                    cmInsertDB2WSTL($db2con, $rs['DATE'],$rs['TIME'],$usrinfo[0]['WUUID'], $jktime,$resCreateTbl['QRYCNT']);//クエリー実行時間作成
                                    fnDB2QHISExe($db2con, $result['QRYGSQRY'], $usrinfo[0]['WUUID']);
                                    //クエリー実行時間の最大、最小、平均アップデート
                                    $dataHis = fnGetDB2QHIS($db2con,$result['QRYGSQRY']);
                                    $max = $dataHis['data'][0]['DQTMAX'];
                                    $min = $dataHis['data'][0]['DQTMIN'];
                                    if($max !== '.00' && $min !== '.00'){
                                        $max = max($max,$jktime);
                                        $min = min($min,$jktime);
                                    }else{
                                        $max = $jktime;
                                        $min = $jktime;
                                    }
                                    cmUpdDB2QHIS($db2con, $result['QRYGSQRY'], $max, $min, ($max + $min)/2); 
                                }
                            }
                        } else {
                            $rtn = 1;
                            $msg = showMsg('FAIL_FUNC', array(
                                'CL連携の準備'
                            )); //'CL連携の準備に失敗しました。';
                            break;
                        }
                        //最後にクローズ
                        if($result['LASTFLG']==='1'){
                            cmDb2Close($db2LibCon);
                        }
						//クエリーグループの一番最クエリーIDしか消す
						if($result['LASTFLG']!=='1'){
							$rs=dropTmpFile($db2con, SAVE_DB,$filenamelist[$result['QRYGSQRY']][0]);
							if($rs===false){
	                            $rtn = 1;
	                            $msg = showMsg('FAIL_FUNC', array(
	                                SAVE_DB.'/'.$filenamelist[$result['QRYGSQRY']][0].'のドロップ'
	                            )); //'CL連携の準備に失敗しました。';
	                            break;
							}
						}

                    }
                }
            }
        }
        //クエリーグループのため検索ボッタンＦＬＧ
        if($schFlgCount === count($D1NAMELIST)){
            $schFlg=false;
        }
    }else{
        if ($D1WEBF !== '1') {
            //クエリー連携の検索項目チェック
            if ($rtn === 0) {
                if ($DRILLFLG === '1') {
                    $res   = fnChkHisu($data, $SEARCHKEYARR);
                    $drill = $res;
                    if ($res['result'] !== true) {
                        //resultflg 1 => '非表示、固定'
                        //resultflg 2 => 'RANGE、IS'
                        //resultflg 3=> '必須'
                        if ($res['resultflg'] === '1') {
                            $rtn = 1;
                            $msg = showMsg('DRILL_COL_FAIL', array(
                                '非表示',
                                '固定'
                            ));
                        } else if ($res['resultflg'] === '2') {
                            $rtn = 1;
                            $msg = showMsg('DRILL_COL_FAIL', array(
                                'RANGE',
                                'IS'
                            ));
                        }
                    }
                }
            }
            
            //FDB2CSV3を取得(セッションに保存,tableInitで比較に使用)
            if ($rtn === 0) {
                $res = fnGetFDB2CSV3($db2con, $d1name);
                if ($res['result'] !== true) {
                    $rtn = 1;
                    $msg = showMsg($res['result']);
                } else {
                    $FDB2CSV3 = $res['data'];
                }
            }
            
            if ($rtn === 0) {
                //直接result画面から来たときは直前のセッションを比較して、
                //FDB2CSV3が同じなら直前の検索状態を表示する
                
                // 直接パラメータで実行する場合Sessionデータとらない
                if ($paramFlg === '') {
                    if ($directResultFlg === '1' && $touch === '0') {
                        if (isset($_SESSION['PHPQUERY']['schData'])) {
                            if ($_SESSION['PHPQUERY']['schData'] !== '') {
                                if ($FDB2CSV3 === $_SESSION['PHPQUERY']['FDB2CSV3']) {
                                    $data_session = $_SESSION['PHPQUERY']['schData'];
                                    $data         = json_decode($data_session, true);
                                    $directGetFlg = 1;
                                }
                            }
                        }
                    } else if ($directResultFlg === '1' && $touch === '1') {
                        if (isset($_SESSION['PHPQUERYTOUCH']['schData'])) {
                            if ($_SESSION['PHPQUERYTOUCH']['schData'] !== '') {
                                if ($FDB2CSV3 === $_SESSION['PHPQUERYTOUCH']['FDB2CSV3']) {
                                    $data_session = $_SESSION['PHPQUERYTOUCH']['schData'];
                                    $data         = json_decode($data_session, true);
                                    $directGetFlg = 1;
                                }
                            }
                        }
                    }
                }
                //PC版のみ、検索状態と検索条件をセッションに保存
                if ($touch === '0') {
                    $_SESSION['PHPQUERY']['schData']  = $data_session;
                    $_SESSION['PHPQUERY']['FDB2CSV3'] = $FDB2CSV3;
                } else if ($touch === '1') {
                    $_SESSION['PHPQUERYTOUCH']['schData']  = $data_session;
                    $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'] = $FDB2CSV3;
                }
                
            }
            //error_log('検索データ：'.print_r($data,true));
            if ($rtn === 0) {
                //検索条件チェック（ブランクチェックのみ）
                foreach ($data as $key => $value) {
                    if ($value['D3USEL'] === '1' && $value['D3DAT'] === '') {
                        $rtn = 1;
                        $msg = $value['D2HED'] . 'は入力必須です。';
                        break;
                    }
                }
            }
            if ($rtn === 0) {
                //検索条件フラグ
                $FDB2CSV3 = cmGetFDB2CSV3($db2con , $d1name);
                if (isset( $FDB2CSV3[0] ) === false) {
                    $schFlg = false;
                }
            }
            //定義の条件変更することがあるかのチェック
            if ($rtn === 0) {
                $OLD_FDB2CSV3 = (isset($_POST['csvoldAry']) ? json_decode($_POST['csvoldAry'], true) : '');
                if ($OLD_FDB2CSV3 !== '') {
                    if ($OLD_FDB2CSV3 !== $FDB2CSV3) {
                        $rtn = 2;
                        $msg = showMsg('FAIL_MENU_REQ', array(
                            'クエリーの検索条件'
                        )); //"クエリーの検索条件が変更されています。<br/>再度メニューから実行し直してください";
                    }
                }
            }
            if ($rtn === 0) {
                /****DEMO/COUNT*****************************/
                /*******************************************/
                
                //init処理
                cmInt($db2con, $initRs, $JSEQ, $d1name);
                
                //9以外の場合、ＤＯ処理
                if ($initRs !== "9") {
                    
                    cmSetQTEMP($db2con);
                    //初期画面の検索条件でFDB2CSV3を更新
                    $r  = updSearchCond($db2con, $data, $d1name);
                    $rs = $r[0];
                    if ($rs === 1) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_INT_BYTE'); //'検索データは128バイトまで入力可能です。';
                    } else {
                        //CL連携
                        //ライセンスのCL連携権限がある場合のみ実行
                        if ($licenseCl === true) {
                            e_log("setCmd start");
                            setCmd($db2con, $d1name, $D1WEBF);
                            e_log("setCmd end");
                        }
                        cmSetPHPQUERY($db2con);
                        //DO処理
                        cmDo($db2con, $doRs, $JSEQ, $filename, $d1name);
                        
                        // 制御又はピボットの実行の場合
                        if ($pmpkey === '') {
                            //制御の場合
                            e_log('制御データ取得');
                            if ($htmlFlg === '1' && $seigyoFlg === 1) {
                                $rstemp = cmRecreateSeigyoTbl5250($db2con, $d1name, $filename, $filterData);
                                if ($rstemp !== '') {
                                    $filename = $rstemp;
                                }
                            }
                        } else {
                            // ピボットの場合
                            e_log('ピボットデータ取得');
                            $rstemp = cmRecreateRnPivotTbl5250($db2con, $d1name, $pmpkey, $filename);
                            if ($rstemp !== '') {
                                $filename = $rstemp;
                            }
                        }
                        
                        /****DEMO/COUNT*****************************/
                        /*******************************************/
                        //ログ出力
                        $logrs = '';
                      
                        cmInsertDB2WLOG($db2con, $logrs, $usrinfo[0]['WUUID'], 'D', $d1name, $data, '0','');
                        fnDB2QHISExe($db2con, $d1name, $usrinfo[0]['WUUID']);
                        if ($doRs === "1") {
                            $D2HED = cmGetD2HED($db2con, $d1name, $JSEQ);
                            $rtn   = 1;
                            $msg   = cmMer($D2HED) . 'の入力に誤りがあります。';
                        } else if ($doRs === "9") {
                            $rtn = 1;
                            $msg = showMsg('FAIL_FUNC', array(
                                'クエリーの実行'
                            )); //'クエリーの実行に失敗しました。';
                        }
                        
                    }
                } else {
                    $rtn = 1;
                    $msg = showMsg('FAIL_FUNC', array(
                        'クエリーの事前処理'
                    )); //'クエリーの事前処理に失敗しました。';
                }
            }
        } else {
            // WEBSQLで作成したクエリーの場合
            if ($D1CFLG === '1') {
                //クエリー連携の検索項目チェック
                if ($rtn === 0) {
                    if ($DRILLFLG === '1') {
                        $res   = fnChkHisu($data, $SEARCHKEYARR);
                        $drill = $res;
                        if ($res['result'] !== true) {
                            //resultflg 1 => '非表示、固定'
                            //resultflg 2 => 'RANGE、IS'
                            //resultflg 3=> '必須'
                            if ($res['resultflg'] === '1') {
                                $rtn = 1;
                                $msg = showMsg('DRILL_COL_FAIL', array(
                                    '非表示',
                                    '固定'
                                ));
                            } else if ($res['resultflg'] === '2') {
                                $rtn = 1;
                                $msg = showMsg('DRILL_COL_FAIL', array(
                                    'RANGE',
                                    'IS'
                                ));
                            }
                        }
                    }
                }
                //[MMM要]//クエリー連携の検索項目チェック
                //条件情報【BSQLCND】を取得(セッションに保存,tableInitで比較に使用)
                if ($rtn === 0) {
                    $res = fnGetBSQLCND($db2con, $d1name);
                    if ($res['result'] !== true) {
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                    } else {
                        $FDB2CSV3 = $res['data'];
                    }
                }
                //直接result画面から来たときは直前のセッションを比較して、
                //FDB2CSV3が同じなら直前の検索状態を表示する
                if ($rtn === 0) {
                    //error_log('パラメータデータ：'.print_r($data,true).'データカウント：'.count($data));
                    if ($paramFlg === '') {
                        if ($directResultFlg === '1' && $touch === '0') {
                            if (isset($_SESSION['PHPQUERY']['schData'])) {
                                if ($_SESSION['PHPQUERY']['schData'] !== '') {
                                    if ($FDB2CSV3 === $_SESSION['PHPQUERY']['FDB2CSV3']) {
                                        $data_session = $_SESSION['PHPQUERY']['schData'];
                                        $data         = json_decode($data_session, true);
                                        $directGetFlg = 1;
                                    }
                                }
                            }
                        } else if ($directResultFlg === '1' && $touch === '1') {
                            if (isset($_SESSION['PHPQUERYTOUCH']['schData'])) {
                                if ($_SESSION['PHPQUERYTOUCH']['schData'] !== '') {
                                    if ($FDB2CSV3 === $_SESSION['PHPQUERYTOUCH']['FDB2CSV3']) {
                                        $data_session = $_SESSION['PHPQUERYTOUCH']['schData'];
                                        $data         = json_decode($data_session, true);
                                        $directGetFlg = 1;
                                    }
                                }
                            }
                        }
                    }
                    //PC版のみ、検索状態と検索条件をセッションに保存
                    if ($touch === '0') {
                        $_SESSION['PHPQUERY']['schData']  = $data_session;
                        $_SESSION['PHPQUERY']['FDB2CSV3'] = $FDB2CSV3;
                    } else if ($touch === '1') {
                        $_SESSION['PHPQUERYTOUCH']['schData']  = $data_session;
                        $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'] = $FDB2CSV3;
                    }
                }
                //検索条件無し又検索条件に非表示だけの場合
                if ($rtn === 0) {
                    if (count($FDB2CSV3) === 0 || $setCmdFlg === false) {
                        $schFlg = false;
                    } else {
                        if (count($FDB2CSV3) > 0) {
                            $condHiddenFlg = true;
                            foreach ($FDB2CSV3 as $fdb2csv3Data) {
                                if ($fdb2csv3Data['CNSTKB'] != 3) {
                                    $condHiddenFlg = false;
                                    break;
                                }
                            }
                            if ($condHiddenFlg === true) {
                                $schFlg = false;
                            }
                        }
                    }
                }
                //定義の条件変更することがあるかのチェック
                if ($rtn === 0) {
                    $OLD_FDB2CSV3 = (isset($_POST['csvoldAry']) ? json_decode($_POST['csvoldAry'], true) : '');
                    if ($OLD_FDB2CSV3 !== '') {
                        if ($OLD_FDB2CSV3 != $FDB2CSV3) {
                            $rtn = 2;
                            $msg = showMsg('FAIL_MENU_REQ', array(
                                'クエリーの検索条件'
                            )); //"クエリーの検索条件が変更されています。<br/>再度メニューから実行し直してください";
                        }
                    }
                }
                // パラメータデータ取得して、結果画面に検索条件表示非表示チェック
                if ($rtn === 0) {
                    $setCmdFlg       = true;
                    $directResultFlg = (isset($_POST['directResultFlg']) ? $_POST['directResultFlg'] : '0');
                    $DIR             = false;
                    if ($directResultFlg === '1') {
                        $DIR        = true;
                        $setCmdData = array();
                        $DGBFLG     = false;
                        $DGAFLG     = false;
                        if (count($data) > 0) {
                            $setCmdData = $data;
                        } else {
                            $dataArr = array();
                            $rs      = fnBSQLHCND($db2con, $d1name);
                            if ($rs['result'] !== true) {
                                $rtn = 1;
                                $msg = showMsg($rs['result']);
                            } else {
                                $dataArr = $rs['data'];
                            }
                            $setCmdData = umEx($dataArr);
                        }
                    } else {
                        $setCmdData = $data;
                    }
                    //配列にCOUNTが一つだけあるとき、データーが非表示かどうかチェックして検索ボタン表示する権限
                    if ($setCmdData !== '') {
                        if (count($setCmdData) === 1) {
                            if ($setCmdData[0]['CNDSTKB'] == '3') {
                                $setCmdFlg = false;
                            }
                        }
                    }
                }
                // CL実行があるか取得
                if ($rtn === 0) {
                    cmSetPHPQUERY($db2con);
                    $rsCLChk = chkFDB2CSV1PG($db2con, $d1name);
                    if ($rsCLChk['result'] !== true) {
                        $rtn = 1;
                        $msg = $rsCLChk['result'];
                    } else {
                        if (count($rsCLChk['data']) > 0) {
                            $fdb2csv1pg = $rsCLChk['data'][0];
                            if ($fdb2csv1pg['DGBLIB'] !== '' || $fdb2csv1pg['DGBPGM'] !== '') {
                                $DGBFLG = true;
                            }
                            if ($fdb2csv1pg['DGALIB'] !== '' || $fdb2csv1pg['DGAPGM'] !== '') {
                                $DGAFLG = true;
                            }
                        }
                    }
                }
                // パラメータ条件データチェック
                if ($rtn === 0) {
                    cmSetPHPQUERY($db2con);
                    $clParamData = $data;
                    $resChkCond  = chkSQLParamData($db2con, $d1name, $data);
                    if ($resChkCond['RTN'] > 0) {
                        $rtn = 1;
                        $msg = $resChkCond['MSG'];
                    } else {
                        $cndParamList = $resChkCond['DATA']['BASEFRMPARAMDATA'];
                    }
                }
                if ($rtn === 0) {
                    $resSQLInfo = getQrySQLData($db2con, $d1name);
                    if ($resSQLInfo['RTN'] !== 0) {
                        $rtn = 1;
                        $msg = $resSQLInfo['MSG'];
                    } else {
                        $qryData = $resSQLInfo['QRYDATA'];
                    }
                }
                if ($rtn === 0) {
                    cmDb2Close($db2con);
                    $paramArr     = '';
                    $fdb2csv1Info = $qryData['FDB2CSV1'][0];
                    $libArr       = preg_split("/\s+/", $fdb2csv1Info['D1LIBL']);
                    $LIBLIST      = join(' ', $libArr);
                    if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                        $db2LibCon = cmDb2ConLib($LIBLIST);
                    } else {
                        $_SESSION['PHPQUERY']['RDBNM'] = $fdb2csv1Info['D1RDB'];
                        $db2LibCon                     = cmDB2ConRDB($LIBLIST);
                    }
                    if ($licenseCl === true) {
                        if ($DGBFLG === true || $DGAFLG === true) {
                            if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                //init処理
                                cmIntWC($db2LibCon, $initRs, 'INT', $d1name);
                            } else {
                                $initRs = "0";
                            }
                        }
                    }
                    //9以外の場合
                    if ($initRs !== "9") {
                        //CL連携
                        //ライセンスのCL連携権限がある場合のみ実行
                        if ($licenseCl === true) {
                            if ($DGBFLG === true || $DGAFLG === true) {
                                if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                    setSQLCmd($db2LibCon, $d1name, $D1WEBF, $setCmdData);
                                }
                            }
                            if ($DGBFLG === true) {
                                //CL実行
                                if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                    cmIntWC($db2LibCon, $initRs, 'BEF', $d1name);
                                } else {
                                    //RDBのCL実行呼び出し;
                                    $rsClExe = callClExecute($db2LibCon, $d1name, $clParamData, 'BEF');
                                    if ($rsClExe === 0) {
                                        $initRs = "9";
                                    } else {
                                        $initRs = "0";
                                    }
                                }
                                
                                if ($initRs !== "9") {
                                } else {
                                    $rtn = 1;
                                    //$msg = showMsg('FAIL_FUNC',array('実行前CL連携の処理'));//'実行前CL連携の処理に失敗しました。';
                                    $msg = showMsg('FAIL_FUNC', array(
                                        array(
                                            '実行前',
                                            'CL連携',
                                            'の',
                                            '処理'
                                        )
                                    ));
                                }
                            }
                            
                        }
                        
                        if ($rtn === 0) {
                            if (count($qryData['BSQLCND']) > 0) {
                                $paramArr = bindParamArr($cndParamList);
                            } else {
                                $paramArr = array();
                            }

                            $strSQL = $qryData['BSQLEXEDAT']['EXESQL'];
							/*DBのフィードがブランクの場合【1=1】に代る*/
							$repArr=array();
                            if (count($paramArr) > 0) {
                                if (count($paramArr['WITHPARAMDATA']) > 0) {
                                    $strSQL = bindWithParamSQLQry($strSQL, $paramArr['WITHPARAMDATA']);
                                }
								$repArr=cmReplaceSQLParam($strSQL,$paramArr['PARAMDATA']);
                                //$strExeSQLParam = bindParamSQLQry($strSQL, $paramArr['PARAMDATA']);2018-05-23
                                $strExeSQLParam = bindParamSQLQry($repArr["strSQL"], $repArr["params"]);

                            } else {
                                $strExeSQLParam = $strSQL;
								$repArr["params"]=$paramArr;
                            }
                            //$rsExeSQL = execSQLQry($db2LibCon, $strExeSQLParam, $paramArr);
                            $rsExeSQL = execSQLQry($db2LibCon, $strExeSQLParam, $repArr["params"]);
                            if ($rsExeSQL['RTN'] !== 0) {
                                if ($rsExeSQL['RTN'] === 2) {
                                    $rtn = 2;
                                    $msg = $rsExeSQL['MSG'];
										error_log('msg talbe init1 = '.$msg);

                                } else {
                                    $rtn = 1;
                                    $msg = $rsExeSQL['MSG'];
										error_log('msg talbe init2 = '.$msg);

                                }
                            } else {
                                $estart = microtime(true);//190405
                                $rsCreate = createSQLQryTbl($db2LibCon, $strExeSQLParam,$repArr["params"], $filename);
                                if ($rsCreate['RTN'] !== 0) {
                                    $rtn = 1;
                                    $msg = $rsCreate['MSG'];
                                } else {
                                    //くエリーの最大件数チェック
                                    $msgmax = '';
                                    if($rsCreate['MSGMAX'] !== '' ){
                                        $msgmax = $rsCreate['MSGMAX'];
                                    }
                                    $eend = microtime(true);//190405
                                    $filename = $rsCreate['TMPTBLNM'];
                                    if($gphkey !== ''){//add for graph drilldown 20181010 MSM
                                        $oldFilename=$filename;
                                        $rsGraphCreate = createPivotSQLQryTbl($d1name, $gphkey, $filename, $filterData);
                                        $filename      = $rsGraphCreate;
                                        error_log("oldFilename => ".$oldFilename."\n csvFilename => ".$rsGraphCreate);
                                    }
                                    if ($pmpkey === '') {
                                        if ($htmlFlg === '1' && $seigyoFlg === 1) {
                                            //制御取得
                                            $rsSeigyoCreate = createSeigyoSQLQryTbl($d1name, $filename, $filterData);
                                            $filename       = $rsSeigyoCreate;
                                        }
                                    } else {
										$oldFilename=$filename;
                                        $rsPivotCreate = createPivotSQLQryTbl($d1name, $pmpkey, $filename, $filterData);
                                        $filename      = $rsPivotCreate;
                                    }
                                    
                                    $add_flg = ($FDB2CSV1['D1OUTA'] === '1')?true:false;

                                    //出力ファイルがあった場合
                                    e_log('⑤クエリー実行の出力ファイルの作成');
                                    if($licenseFileOut){
                                        if($FDB2CSV1['D1OUTLIB'] !=='' && $FDB2CSV1['D1OUTFIL'] !=='' ){
                                            //実行時にユーザーが出力先を設定と確認設定ならぜひデータＯｂｊを持ってる
                                            $outputfdatawin=array();
                                            if($FDB2CSV1['D1OUTJ']==='1' || $FDB2CSV1['D1OUTR'] ==='2'){
                                                $res=array();
                                                $res['D1NAME']=$FDB2CSV1['D1NAME'];
                                                $res['D1OUTJ']=$FDB2CSV1['D1OUTJ'];
                                                $res['D1OUTLIB']=$FDB2CSV1['D1OUTLIB'];
                                                $res['D1OUTFIL']=$FDB2CSV1['D1OUTFIL'];
                                                $res['D1OUTR']=$FDB2CSV1['D1OUTR'];
    											//
    											$res['D1OUTA']=$FDB2CSV1['D1OUTA'];
    											//
                                                $outputfdatawin[]=$res;
                                            }										
                                            //実行時にユーザーが出力先を設定がないなら出力ファイル動く
                                            if($FDB2CSV1['D1OUTJ'] !=='1'){
                                                //ライブラリーとファイルをSYSからチェックは出力しないと確認のためだけチェック
                                                if($FDB2CSV1['D1OUTA'] !=='1'){
                                                    if($FDB2CSV1['D1OUTR'] !=='1'){
    	                                                //ライブラリーとファイルをSYSからチェック
    	                                                $r=fnChkExistFileOutput($db2LibCon,$FDB2CSV1['D1OUTLIB'],$FDB2CSV1['D1OUTFIL']);
    	                                                if($r['result']!==true){
    	                                                    if($r['rtn']===4){
    	                                                        $rtn=($FDB2CSV1['D1OUTR']==='2')?5:6;//出力ファイルのためCOMFIRMメーッセジを出す
    	                                                        $msg=($FDB2CSV1['D1OUTR']==='2')?showMsg('FAIL_OUTC',array($FDB2CSV1['D1OUTLIB'],$FDB2CSV1['D1OUTFIL']))
    	                                                                : showMsg('FAIL_OUT',array($FDB2CSV1['D1OUTLIB'],$FDB2CSV1['D1OUTFIL']));
    	                                                    }else if($r['rtn']===1){
    	                                                        $rtn=1;
    	                                                        $msg=showMsg($r['result']);
    	                                                    }
    	                                                }
                                                    }
                                                }
                                                //TMPテーブルあるかどうかチェック
                                                if ($rtn === 0) {
                                                    $systables = cmGetSystables($db2LibCon, SAVE_DB,$filename);
                                                    if (count($systables)<=0){
                                                        $msg = showMsg('FAIL_MENU_DEL', array('出力ファイルの実行中のデータ'));
                                                        $rtn = 1;
                                                    }
                                                }
                                                //出力ファイル作成
                                                if($rtn===0){
                                                    $r=createOutputFileTable($db2LibCon,$filename,$FDB2CSV1['D1OUTLIB'],$FDB2CSV1['D1OUTFIL'],$d1name,$add_flg);
                                                    if ($r['RTN'] !== 0) {
                                                        if($r['RTN'] !== 11){
                                                            $rtn = 1;
                                                            $msg = showMsg($r['MSG']);
                                                        } else {
                                                            $rtn = 11;
                                                            $msg = showMsg($r['MSG']);
                                                        }
                                                    }else{
                                                        $rtn = 7;
                                                        $msg = showMsg($r['MSG']);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //ライセンスのCL連携権限がある場合のみ実行
                                    if ($licenseCl === true) {
                                        if ($DGAFLG === true) {
                                            //CL実行
                                            if ($fdb2csv1Info['D1RDB'] === '' || $fdb2csv1Info['D1RDB'] === RDB || $fdb2csv1Info['D1RDB'] === RDBNAME) {
                                                cmIntWC($db2LibCon, $initRs, 'AFT', $d1name);
                                            } else {
                                                //RDBのCL実行呼び出し;
                                                $rsClExe = callClExecute($db2LibCon, $d1name, $clParamData, 'AFT');
                                                if ($rsClExe === 0) {
                                                    $initRs = "9";
                                                } else {
                                                    $initRs = "0";
                                                }
                                            }
                                            //9以外の場合、ＤＯ処理
                                            if ($initRs === "9") {
                                                $rtn = 1;
                                                $msg = showMsg('FAIL_FUNC', array(
                                                    '実行後CL連携の処理'
                                                )); //'実行後CL連携の処理に失敗しました。';
                                            }
                                        }
                                    }
                                    $logrs  = '';
                                    $db2con = cmDb2Con();
                                    cmSetPHPQUERY($db2con);
                                    if ($logFlg === '0') {
                                        $sql_wlsqlb = $strExeSQLParam;
                                        if(strlen($sql_wlsqlb) > 32730){
                                            $sql_wlsqlb = showMsg('INVALID_SQLLENGTH');
                                        }
                                        
                                        $jktime = $eend - $estart;
                                        $rs = cmInsertDB2WLOG($db2con, $logrs, $usrinfo[0]['WUUID'], 'D', $d1name, $data, '1',$sql_wlsqlb);//strExeSQLParam = sql statement
                                        cmInsertDB2WSTL($db2con, $rs['DATE'],$rs['TIME'],$usrinfo[0]['WUUID'],$jktime,$rsCreate['QRYCNT']);//クエリー実行時間作成
                                        fnDB2QHISExe($db2con, $d1name, $usrinfo[0]['WUUID']);
                                        //クエリー実行時間の最大、最小、平均アップデート
                                        $dataHis = fnGetDB2QHIS($db2con,$d1name);
                                        $max = $dataHis['data'][0]['DQTMAX'];
                                        $min = $dataHis['data'][0]['DQTMIN'];
                                        if($max !== '.00' && $min !== '.00'){
                                            $max = max($max,$jktime);
                                            $min = min($min,$jktime);
                                        }else{
                                            $max = $jktime;
                                            $min = $jktime;
                                        }
                                        cmUpdDB2QHIS($db2con, $d1name, $max, $min, ($max + $min)/2); 
                                    }
                                }
                            }
                            
                        }
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_FUNC', array(
                            'CL連携の準備'
                        )); //'CL連携の準備に失敗しました。';
                    }
                    cmDb2Close($db2LibCon);
                }
            } else {
                // WEB通常で作成したクエリーの場合
                //クエリー連携の検索項目チェック
                if ($rtn === 0) {
                    if ($DRILLFLG === '1') {
                        $webData = $data;
                        $res     = fnChkWebHisu($data, $SEARCHKEYARR);
                        $drill   = $res;
                        if ($res['result'] !== true) {
                            //resultflg 1 => '非表示、固定'
                            //resultflg 2 => 'RANGE、IS'
                            //resultflg 3=> '必須'
                            if ($res['resultflg'] === '1') {
                                $rtn = 1;
                                $msg = showMsg('DRILL_COL_FAIL', array(
                                    '非表示',
                                    '固定'
                                ));
                            } else if ($res['resultflg'] === '2') {
                                $rtn = 1;
                                $msg = showMsg('DRILL_COL_FAIL', array(
                                    'RANGE',
                                    'IS'
                                ));
                            } else if ($res['resultflg'] === '3') {
                                $rtn = 1;
                                $msg = showMsg('DRILL_HISSU');
                            }
                        }
                        
                        for ($i = 0; $i < count($webData); $i++) {
                            $datalist = $webData[$i]['CNDSDATA'];
                            for ($j = 0; $j < count($datalist); $j++) {
                                $DA = $datalist[$j];
                                if ($rtn === 0) {
                                    if ($DA['CNDDATA'][0] === '' && $DA['CNDTYP'] === '1') {
                                        $rtn = 1;
                                        $msg = showMsg('DRILL_HISSU');
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                /*********/
                //条件情報【BQRYNCNDとFDB2CSV2とFDB2CSV5】を取得(セッションに保存,tableInitで比較に使用)
                if ($rtn === 0) {
                    $res = fnGetBQRYCND($db2con, $d1name);
                    if ($res['result'] !== true) {
                        $rtn = 1;
                        $msg = showMsg($res['result']);
                    } else {
                        $FDB2CSV3 = $res['data'];
                    }
                }
                if ($rtn === 0) {
                    //直接result画面から来たときは直前のセッションを比較して、
                    //FDB2CSV3が同じなら直前の検索状態を表示する
                    //error_log('パラメータデータ：'.print_r($data,true).'データカウント：'.count($data));
                    if ($paramFlg === '') {
                        if ($directResultFlg === '1' && $touch === '0') {
                            if (isset($_SESSION['PHPQUERY']['schData'])) {
                                if ($_SESSION['PHPQUERY']['schData'] !== '') {
                                    if ($FDB2CSV3 === $_SESSION['PHPQUERY']['FDB2CSV3']) {
                                        $data_session = $_SESSION['PHPQUERY']['schData'];
                                        $data         = json_decode($data_session, true);
                                        $directGetFlg = 1;
                                    }
                                }
                            }
                        } else if ($directResultFlg === '1' && $touch === '1') {
                            if (isset($_SESSION['PHPQUERYTOUCH']['schData'])) {
                                if ($_SESSION['PHPQUERYTOUCH']['schData'] !== '') {
                                    if ($FDB2CSV3 === $_SESSION['PHPQUERYTOUCH']['FDB2CSV3']) {
                                        $data_session = $_SESSION['PHPQUERYTOUCH']['schData'];
                                        $data         = json_decode($data_session, true);
                                        $directGetFlg = 1;
                                    }
                                }
                            }
                        }
                    }
                    //PC版のみ、検索状態と検索条件をセッションに保存
                    if ($touch === '0') {
                        $_SESSION['PHPQUERY']['schData']  = $data_session;
                        $_SESSION['PHPQUERY']['FDB2CSV3'] = $FDB2CSV3;
                    } else if ($touch === '1') {
                        $_SESSION['PHPQUERYTOUCH']['schData']  = $data_session;
                        $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'] = $FDB2CSV3;
                    }
                }
                if ($rtn === 0) {
                    //検索条件フラグ
                    if (count($FDB2CSV3) === 0 || $setCmdFlg === false) {
                        $schFlg = false;
                    } else {
                        if (count($FDB2CSV3) > 0) {
                            $condHiddenFlg = true;
                            foreach ($FDB2CSV3 as $fdb2csv3Data) {
                                if ($fdb2csv3Data['CNSTKB'] != 3) {
                                    $condHiddenFlg = false;
                                    break;
                                }
                            }
                            if ($condHiddenFlg === true) {
                                $schFlg = false;
                            }
                        }
                    }
                }
                //定義の条件変更することがあるかのチェック
                if ($rtn === 0) {
                    $OLD_FDB2CSV3 = (isset($_POST['csvoldAry']) ? json_decode($_POST['csvoldAry'], true) : '');
                    if ($OLD_FDB2CSV3 !== '') {
                        if ($OLD_FDB2CSV3 != $FDB2CSV3) {
                            $rtn = 2;
                            $msg = showMsg('FAIL_MENU_REQ', array(
                                'クエリーの検索条件'
                            )); //"クエリーの検索条件が変更されています。<br/>再度メニューから実行し直してください";
                        }
                    }
                }
                if ($rtn === 0) {
                    $setCmdFlg       = true;
                    $directResultFlg = (isset($_POST['directResultFlg']) ? $_POST['directResultFlg'] : '0');
                    $DIR             = false;
                    if ($directResultFlg === '1') {
                        $DIR        = true;
                        $setCmdData = array();
                        $DGBFLG     = false;
                        $DGAFLG     = false;
                        if (count($data) > 0) {
                            $setCmdData = $data;
                        } else {
                            $dataArr = array();
                            $rs      = fnBQRYCNDDAT($db2con, $d1name);
                            if ($rs['result'] !== true) {
                                $rtn = 1;
                                $msg = showMsg($rs['result']);
                            } else {
                                $dataArr = $rs['data'];
                            }
                            $dataArr = umEx($dataArr);
                            if (count($dataArr) > 0) {
                                $setCmdData = fnCreateCndData($dataArr);
                            }
                        }
                    } else {
                        $setCmdData = $data;
                    }
                    //配列にCOUNTが一つだけあるとき、データーが非表示かどうかチェックして検索ボタン表示する権限
                    if ($setCmdData !== '') {
                        $key = key((array) $setCmdData);
                        if (count($key) === 1) {
                            $CNDSDATA = $setCmdData[0][CNDSDATA];
                            if (count($CNDSDATA) === 1) {
                                if ($CNDSDATA[0]['CNDTYP'] == '3') {
                                    $setCmdFlg = false;
                                }
                            }
                        }
                    }
                }
                if ($rtn === 0) {
                    cmSetPHPQUERY($db2con);
                    $rsCLChk = chkFDB2CSV1PG($db2con, $d1name);
                    if ($rsCLChk['result'] !== true) {
                        $rtn = 1;
                        $msg = $rsCLChk['result'];
                    } else {
                        if (count($rsCLChk['data']) > 0) {
                            $fdb2csv1pg = $rsCLChk['data'][0];
                            if ($fdb2csv1pg['DGBLIB'] !== '' || $fdb2csv1pg['DGBPGM'] !== '') {
                                $DGBFLG = true;
                            }
                            if ($fdb2csv1pg['DGALIB'] !== '' || $fdb2csv1pg['DGAPGM'] !== '') {
                                $DGAFLG = true;
                            }
                        }
                    }
                }
                
                if ($rtn === 0) {
                    cmSetPHPQUERY($db2con);
                    $clParamData = $data;
                    $resChkCond  = chkCondParamData($db2con, $d1name, $data);
                    if ($resChkCond['RTN'] > 0) {
                        $rtn = 1;
                        $msg = $resChkCond['MSG'];
                    } else {
                        $cndParamList = $resChkCond['DATA']['BASEDCNDDATA'];
                    }
                }
                if ($rtn === 0) {
                    $WLSQLB = '';
                   
                    if ($pmpkey === '') {
                        error_log("***********************".$filename."pmpkey=>".$pmpkey."htmlflg=>".$htmlFlg.'seigyoflg=>'.$seigyoFlg.'filterflg=>'.$filterFlg);

                        if ($htmlFlg === '1' && $seigyoFlg === 1) {
                            // 制御のためクエリー実行
                            e_log('制御SQL実行:' . $d1name . 'ピボットキー' . $pmpkey);
                            if (count($cndParamList) === 0) {
                                $resExeSql = runExecuteSQLSEIGYORN($db2con, $d1name, NULL, $filterData);
                            } else {
                                $resExeSql = runExecuteSQLSEIGYORN($db2con, $d1name, $cndParamList, $filterData);
                            }
                            if ($resExeSql['RTN'] !== 0) {
                                $rtn = 1;
                                $msg = $resExeSql['MSG'];
                            } else {
                                $qryData = $resExeSql;
                                $WLSQLB = $qryData['STREXECSQL'];
                              
                                $exeSQL  = array(
                                    'EXETESTSQL' => $qryData['STREXECTESTSQL'],
                                    'EXESQL' => $qryData['STREXECSQL'],
                                    'LOGSQL' => $qryData['LOG_SQL']
                                );
                            }
                        } else {
                            $estart = microtime(true);
                            // クエリー実行
                            if (count($cndParamList) === 0) {
                                $resExeSql = runExecuteSQL($db2con, $d1name);
                            } else {
                                $resExeSql = runExecuteSQL($db2con, $d1name, $cndParamList);
                            }
                            if ($resExeSql['RTN'] !== 0) {
                                $rtn = 1;
                                $msg = $resExeSql['MSG'];
                            } else {
                                $qryData = $resExeSql;
                                $WLSQLB = $qryData['STREXECSQL'];
                               
                                $exeSQL  = array(
                                    'EXETESTSQL' => $qryData['STREXECTESTSQL'],
                                    'EXESQL' => $qryData['STREXECSQL'],
                                    'LOGSQL' => $qryData['LOG_SQL']
                                );
                                $eend = microtime(true);
                                e_log( '①クエリー情報取得処理時間：' . ($eend - $estart) . '秒');
                            }
                        }
                    } else {
                        error_log('ピボットSQL実行:' . $d1name . 'ピボットキー' . $pmpkey);
                        if (count($cndParamList) === 0) {
                            $resExeSql = runExecuteSQLPIVOT($db2con, $d1name, $pmpkey);
                        } else {
                            $resExeSql = runExecuteSQLPIVOT($db2con, $d1name, $pmpkey, $cndParamList);
                        }
                        error_log('ピボットSQL実行Query:'.print_r($resExeSql,true));
                        if ($resExeSql['RTN'] !== 0) {
                            $rtn = 1;
                            $msg = $resExeSql['MSG'];
                        } else {
                            $qryData = $resExeSql;
                            $WLSQLB = $qryData['STREXECSQL'];    //pivotsql mmk
                            $exeSQL  = array(
                                'EXESQL' => $qryData['STREXECSQL'],
                                'LOGSQL' => $qryData['LOG_SQL']
                            );
                        }
                    }
                }
                if ($rtn === 0) {
                    cmDb2Close($db2con);
                    //$db2LibCon = cmDb2ConLib($qryData['LIBLIST']);
                    //RDB修正
                    if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                        $db2LibCon = cmDb2ConLib($qryData['LIBLIST']);
                    } else {
                        $_SESSION['PHPQUERY']['RDBNM'] = $qryData['SELRDB'];
                        $db2LibCon                     = cmDB2ConRDB($qryData['LIBLIST']);
                    }
                    if ($licenseCl === true) {
                        if ($DGBFLG === true || $DGAFLG === true) {
                            if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                //init処理
                                cmIntWC($db2LibCon, $initRs, 'INT', $d1name);
                            } else {
                                $initRs = "0";
                            }
                        }
                    }
                    //9以外の場合
                    if ($initRs !== "9") {
                        //CL連携
                        //ライセンスのCL連携権限がある場合のみ実行
                        if ($licenseCl === true) {
                            if ($DGBFLG === true || $DGAFLG === true) {
                                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                    setCmd($db2LibCon, $d1name, $D1WEBF, $setCmdData);
                                }
                            }
                            if ($DGBFLG === true) {
                                //CL実行
                                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                    cmIntWC($db2LibCon, $initRs, 'BEF', $d1name);
                                } else {
                                    //RDBのCL実行呼び出し;
                                    $rsClExe = callClExecute($db2LibCon, $d1name, $clParamData, 'BEF');
                                    if ($rsClExe === 0) {
                                        $initRs = "9";
                                    } else {
                                        $initRs = "0";
                                    }
                                }
                                
                                if ($initRs !== "9") {
                                } else {
                                    $rtn = 1;
                                    //$msg = showMsg('FAIL_FUNC',array('実行前CL連携の処理'));//'実行前CL連携の処理に失敗しました。';
                                    $msg = showMsg('FAIL_FUNC', array(
                                        array(
                                            '実行前',
                                            'CL連携',
                                            'の',
                                            '処理'
                                        )
                                    ));
                                }
                            }
                            
                        }
                        $strSQL = '';
                        $strSQLdata = '';
                        if ($rtn === 0) {
                            $estart = microtime(true);
                            if (($htmlFlg === '1' && $seigyoFlg === 1) || $pmpkey !== '') {
                                if (CRTQTEMPTBL_FLG === 1) {
                                    if (count($qryData['MBRDATALST']) > 0) {
                                        foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                            $strSQL = '';
                                            if ($mbrdata['FILTYP'] === 'L' || $mbrdata['FILTYP'] === 'V') {
                                                $rs = dropTmpFile($db2LibCon, 'QTEMP', $mbrdata['TBLNM']);
                                                $rs = createTmpFil($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM']);
                                                if ($rs !== true) {
                                                    e_log('テーブル設定失敗');
                                                    $rtn = 1;
                                                    $msg = showMsg($rs);
                                                    break;
                                                }
                                            } else {
                                                //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                                $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'],$strSQL);
                                                if ($rs !== true) {
                                                    e_log('テーブル設定失敗');
                                                    $rtn = 1;
                                                    $msg = showMsg($rs);
                                                    break;
                                                }else{
                                                    if($strSQLdata !== ''){
                                                        $strSQLdata .= '<br/>';
                                                    }
                                                    $strSQLdata .= $strSQL;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (count($qryData['MBRDATALST']) > 0) {
                                        foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                            $strSQL = '';
                                            //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                            $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'],$strSQL);
                                            if ($rs !== true) {
                                                e_log('テーブル設定失敗');
                                                $rtn = 1;
                                                $msg = showMsg($rs);
                                                break;
                                            }else{
                                                if($strSQLdata !== ''){
                                                    $strSQLdata .= '<br/>';
                                                }
                                                $strSQLdata .= $strSQL;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (count($qryData['MBRDATALST']) > 0) {
                                    foreach ($qryData['MBRDATALST'] as $mbrdata) {
                                        $strSQL = '';
                                        //$rs = dropFileMBR($db2LibCon,$mbrdata['LIBL'],$mbrdata['FILE'],$mbrdata['FILMBR'],$mbrdata['TBLNM']);
                                        $rs = setFileMBR($db2LibCon, $mbrdata['LIBL'], $mbrdata['FILE'], $mbrdata['FILMBR'], $mbrdata['TBLNM'],$strSQL);
                                        if ($rs !== true) {
                                            e_log('テーブル設定失敗');
                                            $rtn = 1;
                                            $msg = showMsg($rs);
                                            break;
                                        }else{
                                                if($strSQLdata !== ''){
                                                    $strSQLdata .= '<br/>';
                                                }
                                                $strSQLdata .= $strSQL;
                                        }
                                    }
                                }
                            }
                            $eend = microtime(true);
                       
                            $WLSQLB = $strSQLdata.'<br/><br/>'.$WLSQLB;
                            e_log( '②クエリー実行チェック取得処理時間：' . ($eend - $estart) . '秒');
                            $estart = microtime(true);
                            $resExec = execQry($db2LibCon, $qryData['STREXECTESTSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'], $qryData['MBRDATALST']);//Old
                            //$resExec = execQry($db2LibCon, $qryData['STREXECSQL'], $qryData['EXECPARAM'], $qryData['LIBLIST'], $qryData['MBRDATALST']);
                            if ($resExec['RTN'] !== 0) {
                                if ($resExec['RTN'] === 2) {
                                    $rtn = 2;
                                    $msg = showMsg($resExec['MSG']);
										error_log('execQry1'.$msg);

                                } else {
                                    $rtn = 1;
                                    $msg = showMsg($resExec['MSG']);
										error_log('execQry1'.$msg);

                                }
                            } else {
                                $eend = microtime(true);
                                e_log( '③クエリー実行チェック取得処理時間：' . ($eend - $estart) . '秒');
                                $estart = microtime(true);
                                //$resCreateTbl = createTmpTable($db2LibCon, $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $filename);
                                if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                    $resCreateTbl = createTmpTable($db2LibCon, $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $filename, $qryData['MBRDATALST']);
                                } else {
                                    $db2con = cmDb2Con();
                                    cmSetPHPQUERY($db2con);
                                    $resCreateTbl = createTmpTableRDB($db2con, $db2LibCon, $qryData['SELRDB'], $qryData['LIBLIST'], $qryData['STREXECSQL'], $qryData['EXECPARAM'], $filename, $qryData['MBRDATALST']);
                                    cmDb2Close($db2con);
                                }
                                if ($resCreateTbl['RTN'] !== 0) {
                                    $rtn = 1;
                                    $msg = showMsg($resCreateTbl['MSG']);
                                } else {
                                    //くエリーの最大件数チェック
                                    $msgmax = '';
                                    if($resCreateTbl['MSGMAX'] !== '' ){
                                        $msgmax = $resCreateTbl['MSGMAX'];
                                    }
                                    $add_flg = ($FDB2CSV1['D1OUTA'] === '1')?true:false;
                                    $eend = microtime(true);
                                    e_log( '④クエリー実行結果の一時テーブル作成処理時間：test' . ($eend - $estart) . '秒');
                                    //実行時にユーザーが出力先を設定と確認設定ならぜひデータＯｂｊを持ってる
                                    //出力ファイルがあった場合
                                    e_log('⑤クエリー実行の出力ファイルの作成');
                                    if($licenseFileOut){
                                        if($FDB2CSV1['D1OUTLIB'] !=='' && $FDB2CSV1['D1OUTFIL'] !=='' ){
                                            //実行時にユーザーが出力先を設定と確認設定ならぜひデータＯｂｊを持ってる
                                            $outputfdatawin=array();
                                            if($FDB2CSV1['D1OUTJ']==='1' || $FDB2CSV1['D1OUTR'] ==='2'){
                                                $res=array();
                                                $res['D1NAME']=$FDB2CSV1['D1NAME'];
                                                $res['D1OUTJ']=$FDB2CSV1['D1OUTJ'];
                                                $res['D1OUTLIB']=$FDB2CSV1['D1OUTLIB'];
                                                $res['D1OUTFIL']=$FDB2CSV1['D1OUTFIL'];
                                                $res['D1OUTR']=$FDB2CSV1['D1OUTR'];
    											//
    											$res['D1OUTA']=$FDB2CSV1['D1OUTA'];
    											//
                                                $outputfdatawin[]=$res;
                                            }
                                            //実行時にユーザーが出力先を設定がないなら出力ファイル動く
                                            if($FDB2CSV1['D1OUTJ'] !=='1'){
                                                //ライブラリーとファイルをSYSからチェックは出力しないと確認のためだけチェック
                                                if($FDB2CSV1['D1OUTA'] !=='1') {
    	                                            if($FDB2CSV1['D1OUTR'] !=='1'){
    	                                                //ライブラリーとファイルをSYSからチェック
    	                                                $r=fnChkExistFileOutput($db2LibCon,$FDB2CSV1['D1OUTLIB'],$FDB2CSV1['D1OUTFIL']);
    	                                                if($r['result']!==true){
    	                                                    if($r['rtn']===4){
    	                                                        $rtn=($FDB2CSV1['D1OUTR']==='2')?5:6;//出力ファイルのためCOMFIRMメーッセジを出す
    	                                                        $msg=($FDB2CSV1['D1OUTR']==='2')?showMsg('FAIL_OUTC',array($FDB2CSV1['D1OUTLIB'],$FDB2CSV1['D1OUTFIL']))
    	                                                                : showMsg('FAIL_OUT',array($FDB2CSV1['D1OUTLIB'],$FDB2CSV1['D1OUTFIL']));
    	                                                    }else if($r['rtn']===1){
    	                                                        $rtn=1;
    	                                                        $msg=showMsg($r['result']);
    	                                                    }
    	                                                }
    	                                            }
    	                                        }
                                                //TMPテーブルあるかどうかチェック
                                                if ($rtn === 0) {
                                                    $systables = cmGetSystables($db2LibCon, SAVE_DB,$filename);
                                                    if (count($systables)<=0){
                                                        $msg = showMsg('FAIL_MENU_DEL', array('出力ファイルの実行中のデータ'));
                                                        $rtn = 1;
                                                    }
                                                }
                                                //出力ファイル作成
                                                if($rtn===0){
                                                    $r=createOutputFileTable($db2LibCon,$filename,$FDB2CSV1['D1OUTLIB'],$FDB2CSV1['D1OUTFIL'],$d1name,$add_flg);
                                                    if ($r['RTN'] !== 0) {
                                                        if($r['RTN'] !== 11) {
                                                            $rtn = 1;
                                                            $msg = showMsg($r['MSG']);
                                                        } else {
                                                            $rtn = 11;
                                                            $msg = showMsg($r['MSG']);
                                                        }
                                                    }else{
                                                        $rtn = 7;
                                                        $msg = showMsg($r['MSG']);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //ライセンスのCL連携権限がある場合のみ実行
                                    if ($licenseCl === true) {
                                        if ($DGAFLG === true) {
                                            //CL実行
                                            if ($qryData['SELRDB'] === '' || $qryData['SELRDB'] === RDB || $qryData['SELRDB'] === RDBNAME) {
                                                cmIntWC($db2LibCon, $initRs, 'AFT', $d1name);
                                            } else {
                                                //RDBのCL実行呼び出し;
                                                $rsClExe = callClExecute($db2LibCon, $d1name, $clParamData, 'AFT');
                                                if ($rsClExe === 0) {
                                                    $initRs = "9";
                                                } else {
                                                    $initRs = "0";
                                                }
                                            }
                                            //9以外の場合、ＤＯ処理
                                            if ($initRs === "9") {
                                                $rtn = 1;
                                                $msg = showMsg('FAIL_FUNC', array(
                                                    '実行後CL連携の処理'
                                                )); //'実行後CL連携の処理に失敗しました。';
                                            }
                                        }
                                    }
                                }
                            }
                            $logrs  = '';
                            $db2con = cmDb2Con();
                            cmSetPHPQUERY($db2con);
                            if ($logFlg === '0') {
                                if(strlen($WLSQLB) > 32730){
                                    $WLSQLB = showMsg('INVALID_SQLLENGTH');
                                }
                                $jktime = $eend - $estart;
                                $rs = cmInsertDB2WLOG($db2con, $logrs, $usrinfo[0]['WUUID'], 'D', $d1name, $data, '1',$WLSQLB);
                                cmInsertDB2WSTL($db2con, $rs['DATE'],$rs['TIME'],$usrinfo[0]['WUUID'],$jktime,$resCreateTbl['QRYCNT']);//クエリー実行時間作成
                                fnDB2QHISExe($db2con, $d1name, $usrinfo[0]['WUUID']);
                                //クエリー実行時間の最大、最小、平均アップデート
                                $dataHis = fnGetDB2QHIS($db2con,$d1name);
                                $max = $dataHis['data'][0]['DQTMAX'];
                                error_log('get type max = '.getType($max). 'max =====>'.$max);

                                $min = $dataHis['data'][0]['DQTMIN'];
                                error_log('get type min= '.getType($min). 'min =====>'.$min);

                                if($max !== '.00' && $min !== '.00'){
                                    error_log(' not zero');
                                    $max = max($max,$jktime);
                                    $min = min($min,$jktime);
                                }else{
                                    error_log(' zero');

                                    $max = $jktime;
                                    $min = $jktime;
                                }
                                cmUpdDB2QHIS($db2con, $d1name, $max, $min, ($max + $min)/2); 
                            }
                        }
                    } else {
                        $rtn = 1;
                        $msg = showMsg('FAIL_FUNC', array(
                            'CL連携の準備'
                        )); //'CL連携の準備に失敗しました。';
                    }
                    cmDb2Close($db2LibCon);
                }
            }
        }
    }
}

//CSV、EXCELダウンロト権限チェックためフラグ取得
if ($rtn === 0 || $rtn===5 ||  $rtn===6 || $rtn===7 || $rtn === 11) {
    $DB2WDEF =$chkQry['DB2WDEF'];
    if ($DB2WDEF['WDDWNL'] !== "1") {
        $csvFlg = false;
    }
    if ($DB2WDEF['WDDWN2'] !== "1") {
        $excelFlg = false;
    }
}
// HTMLで表示ためデータ取得
$htmldata = array();
if ($rtn === 0 || $rtn===5 || $rtn=== 6 || $rtn===7 || $rtn === 11) {
    $rs = ($LASTD1NAME !=="")?fnGetDB2HTMLT($db2con,$LASTD1NAME):fnGetDB2HTMLT($db2con, $d1name);
    if ($rs['result'] === true) {
        $htmldata = umEx($rs['data']);
    } else {
        $rtn = 1;
        $msg = showMsg('FAIL_FUNC', array(
            'クエリーの実行'
        )); //'クエリーの実行に失敗しました。';
    }
}
//クエリーグループに一番最後のデータをもらうため
if($isQGflg){//クエリーグループ
    foreach($D1NAMELIST as $key=>$result){
        //クエリーグループに一番最後のデータをもらうため
        if($result['LASTFLG']==='1'){
            $filename=$filenamelist[$result['QRYGSQRY']][0];
        }
    }
}
//セッションに検索データと条件データをリセット
if ($rtn !== 0 || $rtn!==5 || $rtn!==6 || $rtn!==7 || $rtn !== 11) {
    if ($touch === '0') {
        $_SESSION['PHPQUERY']['schData']  = '';
        $_SESSION['PHPQUERY']['FDB2CSV3'] = array();
    } else if ($touch === '1') {
        $_SESSION['PHPQUERYTOUCH']['schData']  = '';
        $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'] = array();
    }
}
if(($rtn === 0 || $rtn===5 || $rtn=== 6 || $rtn===7 || $rtn === 11) && $SCHDFLG === '1'){
    $schData = ($data_session !== '') ? json_decode($data_session, true) : array();
    if($isQGflg){
       foreach($SCHSAVE as $key => $value){
            $queryName = '';
            foreach($value as $k => $v){
                $queryName = $k;
            }
            $schDataArr = $schData[$queryName][0];
            $res = fnDeleteDB2SHSD($db2con,$SCHSAVE[$key],$usrinfo[0]['WUUID']);
            if($res !== true){
                $rtn = 1;
                $msg = showMsg($res);
                break;
            }else{
                $res = fnSaveDB2SHSD($db2con,$SCHSAVE[$key],$schDataArr,$usrinfo[0]['WUUID']);
                if($res !== true){
                    $rtn = 1;
                    $msg = showMsg($res);
                    break;
                }
            }
       }

    }else{
        $res = fnDeleteDB2SHSD($db2con,$SCHSAVE,$usrinfo[0]['WUUID']);
        if($res !== true){
            $rtn = 1;
            $msg = showMsg($res);
        }else{
            $res = fnSaveDB2SHSD($db2con,$SCHSAVE,$schData,$usrinfo[0]['WUUID']);
            if($res !== true){
                $rtn = 1;
                $msg = showMsg($res);
            }
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtnArray = array(
    'SCHDFLG' => $SCHDFLG,
    'RTN' => $rtn,
    'MSG' => $msg,
    'MSGMAX' => $msgmax,
    'res' => $res,
    'usrinfo' => $usrinfo,
    'd1name' => $d1name,
    'pmpkey' => $pmpkey,
    'gphkey' => $gphkey,
    'initRs' => $initRs,
    'doRs' => $doRs,
    'filename' => $filename,
	'oldFilename'=>$oldFilename,
    'text' => $text,
    'text_nohtml' => $text_nohtml,
    'csvFlg' => $csvFlg,
    'excelFlg' => $excelFlg,
    'schFlg' => $schFlg,
    'FDB2CSV1' => $FDB2CSV1,
    'SCHSAVE' => $SCHSAVE,
    'data' => ($data_session !== '') ? json_decode($data_session, true) : '',
    'directGetFlg' => $directGetFlg,
    'FDB2CSV3' => $FDB2CSV3,
    'OLD_FDB2CSV3' => $OLD_FDB2CSV3,
    'DSPROW1' => DSPROW1,
    'DSPROW2' => DSPROW2,
    'DSPROW3' => DSPROW3,
    'DSPROW4' => DSPROW4,
    'INTDSPROW' => INTDSPROW,
    'HTMLDATA' => $htmldata,
    'DB2CSV1PM' => $DB2CSV1PM,
    'D1WEBF' => $D1WEBF,
    'D1CFLG' => $D1CFLG,
    'directResultFlg' => $directResultFlg,
    'DGBFLG' => $DGBFLG,
    'DGAFLG ' => $DGAFLG,
    'db2pcol' => $db2pcol,
    'db2pcal' => $db2pcal,
    'axes' => $axes,
    'SEARCHKEYARR' => $SEARCHKEYARR,
    'EXESQL' => $exeSQL,
	'chkQry' => $chkQry,
    'DRILLFLG' => $DRILLFLG,
    'webData' => $webData,
    'AXESPARAM' => AXESPARAM,
    'SEIGYOFLG' => $seigyoFlg,
    'D1NAMELIST'=>$D1NAMELIST,//クエリーグループのためクエリーリスト
    'LASTD1NAME'=>$LASTD1NAME,//クエリーグループのため一番最後のクエリー
    'filenamelist'=>$filenamelist,//ktesting消すてもいい
    '$schFlgCount'=>$schFlgCount,//ktesting消すでもいい
    'OUTPUTFDATA'=>$outputfdatawin,//出力ファイルワインドウのデータ
    'filterdata'=>$filterData,
    'LASTFLG' =>$result['LASTFLG'],
    'SEIGYOSUM' => $seigyoSum,
    'PVFLG'      => CND_PVFLG,
	'SRSVFLG'  =>CND_SRSV

);
echo (json_encode($rtnArray));

//検索条件データ削除
function fnDeleteDB2SHSD($db2con,$SCHSAVE,$userid){
    $UID = $userid;
    $rs = true;
    foreach($SCHSAVE as $key => $value){
        //構文
        $QRG = $value['QRG'];
        $QID = $key;
        $QPV = $value['PIV'];
        $QGP = $value['GPH'];
        //構文
        $strSQL  = ' DELETE FROM DB2SHSD WHERE ';
        $strSQL  .= ' SHSDUID = ? AND SHSDQRG = ? AND SHSDQID = ? AND SHSDQPV = ? AND SHSDQGP = ? ';
        $params = array($UID,$QRG,$QID,$QPV,$QGP);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_DEL';
            break;
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_DEL';
                break;
            }
        }
    }
    return $rs;

}
//検索条件データ保存
function fnSaveDB2SHSD($db2con,$SCHSAVE,$schData,$userid){
    $UID = $userid;
    $rtn = true;
    $rs = 0;
    foreach($SCHSAVE as $key => $value){
        if($value['SAVE'] === '1'){
            $QRG = $value['QRG'];
            $QID = $key;
            $QPV = $value['PIV'];
            $QGP = $value['GPH'];
            if($value['CFLG'] === ''){//WEB QUERY
                $strSQL2 = '';
                $strSQL2 .= 'INSERT INTO DB2SHSD';
                $strSQL2 .= '(';
                $strSQL2 .= 'SHSDUID,';
                $strSQL2 .= 'SHSDQRG,';
                $strSQL2 .= 'SHSDQID,';
                $strSQL2 .= 'SHSDQPV,';
                $strSQL2 .= 'SHSDQGP,';
                $strSQL2 .= 'SHSDSQ1,';
                $strSQL2 .= 'SHSDSQ2,';
                $strSQL2 .= 'SHSDSQ3,';
                $strSQL2 .= 'SHSDDTA';
                $strSQL2 .= ')';
                $strSQL2 .= 'VALUES';
                $strSQL2 .= '(?,?,?,?,?,?,?,?,?)';
                $shsdstmt   = db2_prepare($db2con, $strSQL2);
                if ($shsdstmt === false) {
                    e_log("prepare fail " . db2_stmt_errormsg());
                    $rs = 1;
                    $rtn = 'FAIL_INS';
                    break;
                }
                 if($rs === 0){
                    foreach($schData as $k => $val){
                        $SQ1 = $val['CNDIDX'];
                        $cndsdata = $val['CNDSDATA'];
                        foreach($cndsdata as $cdk => $cdval){
                            $SQ2 = $cdval['CDATAIDX'];
                            $KBN = $cdval['CNDKBN'];
                            $cnddata = $cdval['CNDDATA'];
                            if($KBN === 'LIST' || $KBN === 'NLIST'){
                                $cnddata = array_filter($cnddata);
                                if(count($cnddata) === 0){
                                    array_push($cnddata,'');
                                }
                            }
                            foreach($cnddata as $dk => $dval){
                               $SQ3 = $dk + 1;
                               $shsdparam = array($UID,$QRG,$QID,$QPV,$QGP,$SQ1,$SQ2,$SQ3,$dval);
                                $r = db2_execute($shsdstmt, $shsdparam);
                                if ($r === false) {
                                    e_log("execute fail " . db2_stmt_errormsg());
                                    $rs = 1;
                                    $rtn = 'FAIL_INS';
                                    break;
                                }
                            }
                            if($rs !== 0){
                                break;
                            }
                        }
                        if($rs !== 0){
                            break;
                        }
                    }
                 }
            }else{
                foreach($schData as $k => $val){
                    $SQ1 = 0;
                    $SQ2 = 0;
                    $SQ3 = $val['CNDSEQ'];
                    $dval = $val['CNDDAT'];
                    $strSQL2 = '';
                    $strSQL2 .= 'INSERT INTO DB2SHSD';
                    $strSQL2 .= '(';
                    $strSQL2 .= 'SHSDUID,';
                    $strSQL2 .= 'SHSDQRG,';
                    $strSQL2 .= 'SHSDQID,';
                    $strSQL2 .= 'SHSDQPV,';
                    $strSQL2 .= 'SHSDQGP,';
                    $strSQL2 .= 'SHSDSQ1,';
                    $strSQL2 .= 'SHSDSQ2,';
                    $strSQL2 .= 'SHSDSQ3,';
                    $strSQL2 .= 'SHSDDTA';
                    $strSQL2 .= ')';
                    $strSQL2 .= 'VALUES';
                    $strSQL2 .= '(?,?,?,?,?,?,?,?,?)';
                    $shsdstmt   = db2_prepare($db2con, $strSQL2);
                    if ($shsdstmt === false) {
                        e_log("prepare fail " . db2_stmt_errormsg());
                        $rs = 1;
                        $rtn = 'FAIL_INS';
                        break;
                    }
                    if($rs === 0){
                        $shsdparam = array($UID,$QRG,$QID,$QPV,$QGP,$SQ1,$SQ2,$SQ3,$dval);
                        $r = db2_execute($shsdstmt, $shsdparam);
                        if ($r === false) {
                            e_log("execute fail " . db2_stmt_errormsg());
                            $rs = 1;
                            $rtn = 'FAIL_INS';
                            break;
                        }
                    }
                }
            }
        }
    }
    return $rtn;
}

function fnSaveDB2SHSVBk($db2con,$SCHSAVE,$schData,$userid){
    $UID = $userid;
    $rtn = true;
    $rs = 0;
    foreach($SCHSAVE as $key => $value){
        if($value['SAVE'] === '1'){
            $QRG = $value['QRG'];
            $QID = $key;
            $QPV = $value['PIV'];
            $QGP = $value['GPH'];
            if($value['CFLG'] === ''){//WEB QUERY
                foreach($schData as $k => $val){
                    $SQ1 = $val['CNDIDX'];
                    $cndsdata = $val['CNDSDATA'];
                    $strSQL = " INSERT INTO DB2SHSV VALUES(?,?,?,?,?,?,?,?) ";
                    $shsvstmt   = db2_prepare($db2con, $strSQL);
                    if ($shsvstmt === false) {
                        e_log("prepare fail " . db2_stmt_errormsg());
                        $rs = 1;
                        $rtn = 'FAIL_INS';
                        break;
                    }
                    if($rs === 0){
                        foreach($cndsdata as $cdk => $cdval){
                            $SQ2 = $cdval['CDATAIDX'];
                            $KBN = $cdval['CNDKBN'];
                            $shsvparam = array($UID,$QRG,$QID,$QPV,$QGP,$SQ1,$SQ2,$KBN);
                            $r = db2_execute($shsvstmt, $shsvparam);
                            if ($r === false) {
                                e_log("execute fail " . db2_stmt_errormsg());
                                $rs = 1;
                                $rtn = 'FAIL_INS';
                                break;
                            }
                            if($rs === 0){
                                $strSQL2 = '';
                                $strSQL2 .= 'INSERT INTO DB2SHSD';
                                $strSQL2 .= '(';
                                $strSQL2 .= 'SHSDUID,';
                                $strSQL2 .= 'SHSDQRG,';
                                $strSQL2 .= 'SHSDQID,';
                                $strSQL2 .= 'SHSDQPV,';
                                $strSQL2 .= 'SHSDQGP,';
                                $strSQL2 .= 'SHSDSQ1,';
                                $strSQL2 .= 'SHSDSQ2,';
                                $strSQL2 .= 'SHSDSQ3,';
                                $strSQL2 .= 'SHSDDTA';
                                $strSQL2 .= ')';
                                $strSQL2 .= 'VALUES';
                                $strSQL2 .= '(?,?,?,?,?,?,?,?,?)';
                                $shsdstmt   = db2_prepare($db2con, $strSQL2);
                                if ($shsdstmt === false) {
                                    e_log("prepare fail " . db2_stmt_errormsg());
                                    $rs = 1;
                                    $rtn = 'FAIL_INS';
                                    break;
                                }
                                if($rs === 0){
                                    $cnddata = $cdval['CNDDATA'];
                                    if($KBN === 'LIST' || $KBN === 'NLIST'){
                                        $cnddata = array_filter($cnddata);
                                        if(count($cnddata) === 0){
                                            array_push($cnddata,'');
                                        }
                                    }
                                    foreach($cnddata as $dk => $dval){
                                       $SQ3 = $dk + 1;
                                       $shsdparam = array($UID,$QRG,$QID,$QPV,$QGP,$SQ1,$SQ2,$SQ3,$dval);
                                        $r = db2_execute($shsdstmt, $shsdparam);
                                        if ($r === false) {
                                            e_log("execute fail " . db2_stmt_errormsg());
                                            $rs = 1;
                                            $rtn = 'FAIL_INS';
                                            break;
                                        }
                                    }
                                }else{
                                    break;
                                }
                            }else{
                                break;
                            }
                            if($rs === 1){
                                break;
                            }
                        }
                    }else{
                        break;
                    }
                    if($rs === 1){
                        break;
                    }
                }
            }else{
                foreach($schData as $k => $val){
                    $SQ1 = 0;
                    $SQ2 = 0;
                    $SQ3 = $val['CNDSEQ'];
                    $dval = $val['CNDDAT'];
                    $strSQL2 = '';
                    $strSQL2 .= 'INSERT INTO DB2SHSD';
                    $strSQL2 .= '(';
                    $strSQL2 .= 'SHSDUID,';
                    $strSQL2 .= 'SHSDQRG,';
                    $strSQL2 .= 'SHSDQID,';
                    $strSQL2 .= 'SHSDQPV,';
                    $strSQL2 .= 'SHSDQGP,';
                    $strSQL2 .= 'SHSDSQ1,';
                    $strSQL2 .= 'SHSDSQ2,';
                    $strSQL2 .= 'SHSDSQ3,';
                    $strSQL2 .= 'SHSDDTA';
                    $strSQL2 .= ')';
                    $strSQL2 .= 'VALUES';
                    $strSQL2 .= '(?,?,?,?,?,?,?,?,?)';
                    $shsdstmt   = db2_prepare($db2con, $strSQL2);
                    if ($shsdstmt === false) {
                        e_log("prepare fail " . db2_stmt_errormsg());
                        $rs = 1;
                        $rtn = 'FAIL_INS';
                        break;
                    }
                    if($rs === 0){
                        $shsdparam = array($UID,$QRG,$QID,$QPV,$QGP,$SQ1,$SQ2,$SQ3,$dval);
                        $r = db2_execute($shsdstmt, $shsdparam);
                        if ($r === false) {
                            e_log("execute fail " . db2_stmt_errormsg());
                            $rs = 1;
                            $rtn = 'FAIL_INS';
                            break;
                        }
                    }
                }
            }
        }
    }
    return $rtn;
}



/*
 *-------------------------------------------------------* 
 * 定義名取得
 *-------------------------------------------------------*
 */

function fnGetD1TEXT($db2con, $D1NAME)
{
    
    $data = array();
    
    //$strSQL  = ' SELECT A.D1TEXT ';
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1 AS A ';
    $strSQL .= ' WHERE D1NAME = ? ';
    
    $params = array(
        $D1NAME
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => umEx($data),
                'D1TEXT' => $data[0]['D1TEXT']
            );
        }
    }
    return $data;
}
/**
 *
 *グラフ取得
 *
 *
 */
function fnGetGPHEXT($db2con, $GPKQRY, $GPKID)
{
    
    $data = array();
    
    //$strSQL  = ' SELECT A.PMTEXT ';
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM DB2GPK AS A ';
    $strSQL .= ' WHERE GPKQRY = ? AND GPKID = ? ';
    
    $params = array(
        $GPKQRY,
        $GPKID
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data,
                'GPKNM' => $data[0]['GPKNM']
            );
        }
    }
    return $data;
}
/*
 *-------------------------------------------------------* 
 * 定義名取得
 *-------------------------------------------------------*
 */

function fnGetPMTEXT($db2con, $PMNAME, $PMPKEY)
{
    
    $data = array();
    
    //$strSQL  = ' SELECT A.PMTEXT ';
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM DB2PMST AS A ';
    $strSQL .= ' WHERE PMNAME = ? AND PMPKEY = ? ';
    
    $params = array(
        $PMNAME,
        $PMPKEY
    );
    
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data,
                'PMTEXT' => $data[0]['PMTEXT']
            );
        }
    }
    return $data;
}

/*
 *-------------------------------------------------------* 
 * CSV、EXCEL権限取得
 *-------------------------------------------------------*
 */

function fnGetDB2WDEF($db2con, $WDUID, $WDNAME)
{
    
    $data   = array();
    $strSQL = ' SELECT A.WDDWNL,A.WDDWN2 ';
    $strSQL .= ' FROM DB2WDEF AS A ';
    $strSQL .= ' WHERE WDUID = ? ';
    $strSQL .= ' AND WDNAME = ? ';
    $params = array(
        $WDUID,
        $WDNAME
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data[0]
            );
        }
    }
    return $data;
}

// FDB2CSV3を取得
function fnGetFDB2CSV3($db2con, $d3name)
{
    
    $data = array();
    
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV3 AS A ';
    $strSQL .= ' WHERE A.D3NAME = ? ';
    
    $params = array(
        $d3name
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    return $data;
}

function chkFDB2CSV1PG($db2con, $d1name)
{
    $data = array();
    
    $strSQL = ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV1PG A ';
    $strSQL .= ' WHERE A.DGNAME  = ? ';
    
    $params = array(
        $d1name
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => umEx($data, false)
            );
        }
    }
    return $data;
}

function fnChkHisu($data, $VALUES)
{
    //$VALUES    = $SCH;
    $result    = true;
    $resultflg = '';
    $res       = 0;
    $rs        = array();
    //for ($key = 0; $key < count($data); $key++) {
    foreach($data as $key => $dataVal){
        if ($dataVal['D3FLD'] === $VALUES['DRDFNM'] && $dataVal['D3FILID'] === $VALUES['DRDFID'] && $dataVal['D3CNT'] === $VALUES['DRDCNT']) {
            if ($dataVal['D3USEL'] === '' || $dataVal['D3USEL'] === '3') {
                $res       = 1;
                $result    = false;
                $resultflg = '1';
                break;
            }
            if ($res === 0) {
                if ($dataVal['D3CND'] === 'RANGE' || $dataVal['D3CND'] === 'IS') {
                    $res       = 1;
                    $result    = false;
                    $resultflg = '2';
                    break;
                }
            }
        }
    }
    $rs = array(
        'resultflg' => $resultflg,
        'result' => $result
    );
    return $rs;
}
/***
 *
 *クエリー連携ウエブ画面からの検索項目データチェック
 *
 ***/
function fnChkWebHisu($webData, $SCH)
{
    $VALUES = $SCH;
    $jsq    = explode("-", $SCH['DRDJSQ']);
    $DRDJSQ = $jsq[0];
    $DRDCNT = $jsq[1];
    $rs     = array();
    if ($SCH['DRDFID'] === '0') {
        $VALUES['DRDFNM'] = 'P.' . $SCH['DRDFNM'];
    } else if ($SCH['DRDFID'] === '9999') {
        $VALUES['DRDFNM'] = 'K.' . $SCH['DRDFNM'];
    } else {
        $VALUES['DRDFNM'] = 'S.' . $SCH['DRDFID'] . '.' + $SCH['DRDFNM'];
    }
    $result    = true;
    $resultflg = '';
    for ($i = 0; $i < count($webData); $i++) {
        $datas = $webData[$i]['CNDSDATA'];
        for ($j = 0; $j < count($datas); $j++) {
            $DATA = $datas[$j];
            if ($result === true) {
                if ($webData[$i]['CNDIDX'] === $DRDJSQ && $DATA['CNDFLD'] === $VALUES['DRDFNM'] && $DATA['CNFILID'] === $VALUES['DRDFID'] && $DATA['CDATAIDX'] === $DRDCNT) {
                    if ($DATA['CNDTYP'] === '' || $DATA['CNDTYP'] === '3') {
                        $result    = false;
                        $resultflg = '1';
                        break;
                    }
                }
            }
            if ($result === true) {
                if ($webData[$i]['CNDIDX'] === $DRDJSQ && $DATA['CNDFLD'] === $VALUES['DRDFNM'] && $DATA['CNFILID'] === $VALUES['DRDFID'] && $DATA['CDATAIDX'] === $DRDCNT) {
                    if ($DATA['CNDKBN'] === 'RANGE' || $DATA['CNDKBN'] === 'IS') {
                        $result    = false;
                        $resultflg = '2';
                        break;
                    }
                }
            }
            if ($result === true) {
                if ($DATA['CNDDATA'][0] === '' && $DATA['CNDTYP'] === '1') {
                    $result    = false;
                    $resultflg = '3';
                    break;
                }
            }
        }
        if ($result === false) {
            break;
        }
    }
    $rs = array(
        'resultflg' => $resultflg,
        'result' => $result
    ); //check KLYH 
    return $rs;
}
//制御チェックと制御データ取得
function fnGetSeigyoData($db2con, $d1name, $dcsumf)
{
    $rtn       = 0;
    $seigyoFlg = 0;
    $seigyoSum = '';
    $rsmaster  = array();
    $rsshukei  = array();
    // htmlテンプレート
    if ($rtn === 0) {
        $rshtml = cmGetDB2HTMLT($db2con, $d1name);
        if ($rshtml['result'] !== true) {
            $msg = showMsg($rshtml['result']);
            $rtn = 1;
        } else {
            if (count($rshtml['data']) == 0) {
                //制御の合計をユーザーが変更する場合
                if ($dcsumf !== "default") {
                    $rs = cmUpdDB2COLM($db2con, $d1name, $dcsumf);
                    if ($rs['result'] !== true) {
                        $msg = showMsg($rs['result']);
                        $rtn = 1;
                    }
                }
                //制御チェック
                if ($rtn === 0) {
                    //集計するフィールド情報取得
                    $rsmaster = cmGetDB2COLM($db2con, $d1name);
                    if ($rsmaster['result'] !== true) {
                        $msg = showMsg($rsmaster['result']);
                        $rtn = 1;
                    } else {
                        $rsMasterArr = umEx($rsmaster['data']);
                        //集計するメソッド情報取得
                        $rsshukei    = cmGetDB2COLTTYPE($db2con, $d1name);
                        if ($rsshukei['result'] !== true) {
                            $msg = showMsg($rsshukei['result']);
                            $rtn = 1;
                        } else {
                            $rsshukeiArr = umEx($rsshukei['data']);
                        }
                        if (count($rsMasterArr) !== 0 && count($rsshukeiArr) !== 0) {
                            $seigyoFlg = 1;
                        }
                        if(count($rsMasterArr) > 0){
                            $seigyoSum = $rsMasterArr[0]['DCSUMF'];
                        }
                    }
                }
            }
        }
    }
    $rtnArr = array(
        'RESULT' => $rtn,
        'MSG' => $msg,
        'SEIGYOFLG' => $seigyoFlg,
        'RSMASTER' => $rsMasterArr,
        'RSSHUKEI' => $rsshukeiArr,
        'SEIGYOSUM' => $seigyoSum

    );
    return $rtnArr;
}
