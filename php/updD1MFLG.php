<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("./common/inc/config.php");
include_once("./common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$D1NAME = $_POST['D1NAME'];
$D1MFLG = $_POST['D1MFLG'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$D1NAME = cmHscDe($D1NAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'15',$userData[0]['WUSAUT']);//'15' => ピボット設定
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ピボット設定の権限'));
            }
        }
    }
}


/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/

$rs = '0';


$rs = fnUpdateD1MFLG($db2con,$D1NAME,$D1MFLG);

if($rs === '1'){
    $rtn = 1;
    $msg = showMsg('FAIL_UPD');//'更新処理に失敗しました。管理者にお問い合わせください。';
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* 元クエリー非表示設定
*-------------------------------------------------------*
*/

function fnUpdateD1MFLG($db2con,$D1NAME,$D1MFLG){


    $rs = '0';


    $strSQL  = ' UPDATE FDB2CSV1 ';
    $strSQL .= ' SET ';
    $strSQL .= ' D1MFLG = ? ';
    $strSQL .= ' WHERE D1NAME = ? ';

    $params = array(
        $D1MFLG,
        $D1NAME
    );

    $stmt = db2_prepare($db2con,$strSQL);

    if($stmt === false){
        $rs = '1';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = '1';
        }
    }
    return $rs;

}