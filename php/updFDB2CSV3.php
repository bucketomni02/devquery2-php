<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("common/inc/config.php");
include_once("common/inc/common.inc.php");
include_once("licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$touch = (isset($_POST['TOUCH'])?$_POST['TOUCH']:'0');
$d1name = $_POST['D1NAME'];
$pmpkey = (isset($_POST['PMPKEY']) ? $_POST['PMPKEY'] : '');
$csvfilename = $_POST['csvfilename'];
$data = json_decode($_POST['DATA'],true);
$data_session = $_POST['DATA'];

$filterData = (isset($_POST['FILTERDATA']) ? $_POST['FILTERDATA'] : '');
$htmlFlg = (isset($_POST['HTMLFLG']) ? $_POST['HTMLFLG'] : '0');
$logFlg  = (isset($_POST['LOGFLG']) ? $_POST['LOGFLG'] : '0');
//mppn
$filtersearchData = (isset($_POST['FILTERSEARCHDATA']) ? json_decode($_POST['FILTERSEARCHDATA'],true) : array());
$filtersearchFlg = (isset($_POST['FILTERSEARCHLG']) ? $_POST['FILTERSEARCHLG'] : '0');
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$usrinfo = (($touch == '1')?$_SESSION['PHPQUERYTOUCH']['user']:$_SESSION['PHPQUERY']['user']);
$rtnArray = array();        //リターン配列
$rs = 0;
$RTCD = '';
$JSEQ = '';
$d3datArray = array();
$rtn = 0;
$msg = '';
$FDB2CSV1 = array();
$FDB2CSV3 = array();
$seigyoFlg = 0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();

//INT処理
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmCheckUser($db2con,$usrinfo[0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }
}

//FDB2CSV3を取得(セッションに保存,tableInitで比較に使用)
$FDB2CSV3 = fnGetFDB2CSV3($db2con,$d1name);

//PC版のみ、検索条件と検索状態をセッションに保存
if($touch === '0'){
    $_SESSION['PHPQUERY']['schData'] = $data_session;
    $_SESSION['PHPQUERY']['FDB2CSV3'] = $FDB2CSV3;
}else if($touch === '1'){
    $_SESSION['PHPQUERYTOUCH']['schData'] = $data_session;
    $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'] = $FDB2CSV3;
}

$FDB2CSV3DB = array();
if($rtn === 0){
    $FDB2CSV3DB = cmGetFDB2CSV3($db2con,$d1name);
    $CSV2ARY = (isset($_POST['CSV2ARY']))?json_decode($_POST['CSV2ARY'],true):'';
    if($CSV2ARY !== ''){
        if($FDB2CSV3DB != $CSV2ARY){
            $rtn = 2;
            $msg = showMsg('FAIL_MENU_REQ',array('クエリーの検索条件'));//"クエリーの検索条件が変更されています。<br/>再度メニューから実行し直してください";
        }
    }
}

//検索条件チェック（ブランクチェックのみ）
foreach($data as $key => $value){
    if($value['D3USEL'] === '1' && $value['D3DAT'] === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array($value['D2HED']));//$value['D2HED'].'は入力必須です。';
    }
}

if($htmlFlg === '1'){
    $rsSeigyoData = cmGetSeigyoData($db2con,$d1name);
    if($rsSeigyoData['RESULT'] !== 0){
        $msg = showMsg($rsSeigyoData['MSG']);
        $rtn = 1;
    }else{
        $seigyoFlg = $rsSeigyoData['SEIGYOFLG'];
    }
}

if($rtn === 0){

    cmInt($db2con,$RTCD,$JSEQ,$d1name);

    //INTが成功の場合、検索条件更新
    if($RTCD !== 9){
        cmSetQTEMP($db2con);

        $r = updSearchCond($db2con,$data,$d1name);
        $rs = $r[0];

        if($rs === 1){
            $rtn = 1;
            $msg = showMsg('FAIL_INT_BYTE');//'検索データは128バイトまで入力可能です。';
        }else{

            $d3datArray = $r[1];

            // CL連携
            if($rs === 0){
                //ライセンスのCL連携権限がある場合のみ実行
                if($licenseCl === true){
                    setCmd($db2con, $d1name, $d3datArray);
                }
            }

        }
    }else{
        $rtn = 1;
        $msg = showMsg('FAIL_FUNC',array('クエリーの事前処理'));//'クエリーの事前処理に失敗しました。';
    }

    //更新成功の場合、DO処理
    if($rs === 0){
        cmSetPHPQUERY($db2con);
        cmDo($db2con,$RTCD,$JSEQ,$csvfilename,$d1name);
        if($pmpkey === '' ){
            if($htmlFlg === '1' && $seigyoFlg == 1){
                e_log('REACH SEIGYOデータ');
                $rstemp = cmRecreateSeigyoTbl5250($db2con,$d1name,$csvfilename,$filterData);
                if($rstemp !== ''){
                    $csvfilename = $rstemp;
                }
            }
            if($filtersearchFlg ==='1'){
               e_log('seach data testing mppn1');
               $rstemp = cmRecreateSeigyoTbl5250FS($db2con,$d1name,$csvfilename,$filterData,$filtersearchData);
               if($rstemp !== ''){
                    $csvfilename = $rstemp;
                }
            }
        }else{
            e_log('ピボットデータ取得');
            $rstemp = cmRecreateRnPivotTbl5250($db2con,$d1name,$pmpkey,$csvfilename);
            if($rstemp !== ''){
                $csvfilename = $rstemp;
            }
        }
        //ログ出力
        $logrs = '';
        if($logFlg === '0'){
            cmInsertDB2WLOG($db2con,$logrs,$usrinfo[0]['WUUID'],'D',$d1name,$CSV2ARY,'0','');
        }

        if($RTCD === '1'){
            $rtn = 1;
            $D2HED = cmGetD2HED($db2con,$d1name,$JSEQ);
            $msg = showMsg('FAIL_CMP',array(''.cmMer($D2HED)));//cmMer($D2HED).'の入力に誤りがあります。';
        }else if($RTCD === '9'){
            $rtn = 1;
            $msg = showMsg('FAIL_FUNC',array('クエリーの実行'));//'クエリーの実行に失敗しました。';
        }
    }else{
        $rtn = $rs;
    }

}

if($rtn !== 0){
    if($touch === '0'){
        $_SESSION['PHPQUERY']['schData'] = '';
        $_SESSION['PHPQUERY']['FDB2CSV3'] = array();
    }else if($touch === '1'){
        $_SESSION['PHPQUERYTOUCH']['schData'] = '';
        $_SESSION['PHPQUERYTOUCH']['FDB2CSV3'] = array();
    }
}

cmDb2Close($db2con);
$rtnArray = array(
    'D1NAME' => $d1name,
    'RTN' => $rtn,
    'MSG' => $msg,
    'DATA' => $data,
    'D3DAT' => $d3datArray,
    'FDB2CSV3' =>$FDB2CSV3,
    'CSV2ARY' =>$CSV2ARY,
    'INTDSPROW' =>INTDSPROW,
    'filename' => $csvfilename
);

echo(json_encode($rtnArray));

function fnGetFDB2CSV3($db2con,$d3name){

    $data = array();
    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM FDB2CSV3 AS A ' ;
    $strSQL .= ' WHERE A.D3NAME = ? ';

    $params = array(
        $d3name
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    return $data;

}