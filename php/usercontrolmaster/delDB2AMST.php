<?php
/**
* ============================================================
* SYSTEM NAME    : PHPQUEYR2
* SUBSYSTEM NAME : メール配信グループ設定
* PROGRAM NAME   : メールアドレス削除
* PROGRAM ID     : delDB2MAAD.php
* DEVELOPED BY   : OSC
* CREATE DATE    : 2016/02/23
* MODIFY DATE    : 
* ============================================================
**/

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$AMNAME = cmHscDe($_POST['AMNAME']);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('権限作成の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'27',$userData[0]['WUSAUT']);//'1' => UsercontrolMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('権限作成の権限'));
            }
        }
    }
}
//メールグループ存在チェック
if($rtn === 0){
    $rs = fnCheckDB2AMST($db2con,$AMNAME);
    if( $rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('権限ID'));
    }
}




//メールアドレスの削除
if($rtn === 0){
    $rs = fnDeleteDB2AMST($db2con,$AMNAME);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

if($rtn === 0){
    $rs = fnDeleteDB2AGRT($db2con,$AMNAME);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

if($rtn === 0){
        $rs = fnDeleteDB2WUSRDel($db2con,$AMNAME);//ユーザー権限ブラク更新
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);
echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* 削除対象メールアドレスが存在かどうかをチェックすること
* 
* RESULT
*    01：データがある場合 true
*    02：データがない場合 false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* @param String  $MAMAIL メールアドレス
*-------------------------------------------------------*
*/



/*
*-------------------------------------------------------* 
* 削除対象メールアドレスを削除すること
* 
* RESULT
*    01：削除終了の場合 true
*    02：エラーの場合 false
* 
* @param Object  $db2con DBコネクション
* @param String  $MANAME メールグループID
* @param String  $MAMAIL メールアドレス
*-------------------------------------------------------*
*/

function fnDeleteDB2AMST($db2con,$AMNAME){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2AMST ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' AMNAME = ? ' ;
   
  
    $params = array(
        $AMNAME
       
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}

function fnDeleteDB2AGRT($db2con,$AMNAME){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2AGRT ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' AGNAME = ? ' ;
   
  
    $params = array(
        $AMNAME
       
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}

function fnCheckDB2AMST($db2con,$AMNAME){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.AMNAME ';
    $strSQL .= ' FROM DB2AMST AS A ' ;
    $strSQL .= ' WHERE A.AMNAME = ? ' ;

    $params = array(
        $AMNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_DEL';
            }
        }
    }
    return $rs;
}
//パワーユーザー権限にブラク更新
function fnDeleteDB2WUSRDel($db2con,$WUSAUT){
    $rs = true;

    //構文
    $strSQL  = " UPDATE DB2WUSR SET WUSAUT = ?";
    $strSQL .= " WHERE ";
    $strSQL .= " WUSAUT = ? " ;
    $params = array('',$WUSAUT);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}