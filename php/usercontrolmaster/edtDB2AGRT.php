<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$AGNAME = $_POST['AGNAME'];
$values = json_decode($_POST['VALUES'],true);
$userList = array();
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

//htmldecode
$AGNAME = cmHscDe($AGNAME);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('権限作成の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'27',$userData[0]['WUSAUT']);//'1' => UsercontrolMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('権限作成の権限'));
            }
        }
    }
}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/

$rs = true;

//一つ以上にチェックがついているかのチェック
/*$trueflg = false;
foreach($values as $key => $value){
    if($value['value'] === true){
        $trueflg = true;
    }
}
if($trueflg === false){
    $rs = false;
    $rtn = 1;
    $msg = showMsg('CHK_SEL',array('マスター'));
}*/
if($rtn === 0){
    $rs = fnDeleteDB2AGRT($db2con,$AGNAME);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
if($rtn === 0){
    foreach($values as $key => $value){
         $flg = '';
         if($value['value'] === true){
              $flg = '1';
            if($value['field'] === '1'){
                $rs = fnGetDB2WUSRKENGEN($db2con,$AGNAME);
                if($rs['result'] !== true){
                    $rtn = 2;
                    $msg = showMsg($rs['result'],array('権限'));
                }else{
                     $userList = $rs['data'];
                     if(count($userList) > 0){
                            $rs = fnUpdateDB2WUSRKENGEN($db2con,$userList);
                            if($rs !== true){
                                $rtn = 1;
                                $msg = showMsg($rs);
                            }
                     }
                }
            }
         }
        if($rtn === 0){
            $rs = fnUpdateDB2AGRT($db2con,$AGNAME,$value['field'],$flg);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
                break;
            }
        }else{
                break;
        }
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'userList' => $userList
);

echo(json_encode($rtnAry));

function fnDeleteDB2AGRT($db2con,$AGNAME){
    $rs = true;
    //構文
   $strSQL  = 'DELETE FROM DB2AGRT WHERE AGNAME = ? ';
    $params = array($AGNAME);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}
/*
*-------------------------------------------------------* 
* 権限マスター情報更新
*-------------------------------------------------------*
*/

function fnUpdateDB2AGRT($db2con,$AGNAME,$AGMNID,$flg){
    $rs = true;
       //構文
        $strSQL  = ' INSERT INTO DB2AGRT(AGNAME,AGMNID,AGFLAG)  ';
        $strSQL .= ' VALUES(?,?,?) ';
        $params = array($AGNAME,$AGMNID,$flg);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_UPD';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_UPD';
            }
        }
    return $rs;
}
function fnGetDB2WUSRKENGEN($db2con,$WUSAUT){

    $data = array();

    $params = array('2',$WUSAUT);
    $rs = true;


        $strSQL  = ' SELECT ';
        $strSQL .= ' A.WUUID  ';
        $strSQL .= ' FROM DB2WUSR as A ';
        $strSQL .= ' WHERE WUAUTH = ? ';
        $strSQL .= ' AND WUSAUT = ? ';
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                    $data = array('result' => true,'data' => $data);
            }
        }
    return $data;
}

function fnUpdateDB2WUSRKENGEN($db2con,$userList){

    $rs     = true;
    //構文
    $strSQL = ' UPDATE DB2WUSR ';
    $strSQL .= ' SET WULGNC = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUUID = ? ';
    foreach($userList as $key => $value){
        $params = array('0',$value['WUUID']);
        $stmt = db2_prepare($db2con, $strSQL);
        if ($stmt === false) {
            $rs = 'FAIL_UPD';
             break;
        } else {
            $r = db2_execute($stmt, $params);
            if ($r === false) {
                $rs = 'FAIL_UPD';
                break;
            }
        }
    }
    return $rs;
}