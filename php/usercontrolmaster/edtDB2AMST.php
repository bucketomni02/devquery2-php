<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['PROC']))?cmMer($_POST['PROC']):'';
$AMNAME = (isset($_POST['AMNAME']))?cmMer($_POST['AMNAME']):'';
$AMTEXT = (isset($_POST['AMTEXT']))?cmMer($_POST['AMTEXT']):'';

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/
/*
$AMNAME = cmHscDe($AMNAME);
$AMTEXT = cmHscDe($AMTEXT);*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('権限作成の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'27',$userData[0]['WUSAUT']);//'1' => UsercontrolMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('権限作成の権限'));
            }
        }
    }
}

//メールグループID必須チェック
if($rtn === 0){
    if($AMNAME  === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('権限ID'));
        $focus = 'AMNAME';
         
    }
}
//メールグループIDの半角チェック
if($rtn === 0){
    $byte = checkByte($AMNAME);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('権限ID'));
        $focus = 'AMNAME';
        
    }
}
//メールグループIDの桁数チェック
if($rtn === 0){
    if(!checkMaxLen($AMNAME,10)){
        $rtn = 1;
        
       $msg = showMsg('FAIL_MAXLEN',array('権限ID'));
        $focus = 'AMNAME';
    }
}
 if($rtn === 0){
        if(cmMer($AMNAME) !== ''){
            if (cmMer($AMNAME) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('権限ID'));
            }
        }
    }
//メールグループIDのメール形式チェック
/*$a = '';    
if($rtn === 0){
    if (preg_match("/^([a-zA-Z0-9])*([a-zA-Z0-9\._-])+@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$AMNAME)){
        $rtn = 1;
        $msg = showMsg('FAIL_FMT',array('ID','権限')); 
$a = '1';
        $focus = 'AMNAME';
    }
}*/
//メールグループ名必須チェック
if($rtn === 0){
    if($AMTEXT  === ''){
        $rtn = 1;
         $msg = showMsg('FAIL_REQ',array('権限名'));
        
        $focus = 'AMTEXT';
    }
}
//メールグループ名の桁数チェック
if($rtn === 0){
    if(!checkMaxLen($AMTEXT,50)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('権限名'));
        $focus = 'AMTEXT';
        
    }
}
 if($rtn === 0){
        if(cmMer($AMTEXT) !== ''){
            if (cmMer($AMTEXT) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_REQ',array('権限名'));
            }
        }
    }

/*
*-------------------------------------------------------* 
* メールグループ更新処理
*-------------------------------------------------------*
*/

if($proc === 'ADD'){
   if($rtn === 0){
        //メールグループ存在チェック処理
        $rs = fnCheckDB2AMST($db2con,$AMNAME);
        
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('権限ID'));
            $focus = 'AMNAME';
        }
    }
    if($rtn === 0){
        //新メールグループを登録処理
        $rs = fnInsertDB2AMST($db2con,$AMNAME,$AMTEXT);
       
        if($rs !== true){
            $rtn = 1;
            $aa = 'NameError';
            $msg = showMsg($rs);
        }
    }
}else if($proc === 'UPD'){
    if($rtn === 0){
        //メールグループ存在チェック処理
        $rs = fnCheckDB2AMST($db2con,$AMNAME);
        if($rs === true){
            $rtn = 1;
            $rs = 'NOTEXIST_GET';
            $msg = showMsg($rs,array('権限ID'));
            $focus = 'AMNAME';
        }else if($rs !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs,array('権限ID'));
            $focus = 'AMNAME';
        }
    }
    if($rtn === 0){
        //メールグループを更新処理
        $rs = fnUpdateDB2AMST($db2con,$AMNAME,$AMTEXT);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs);
        }
    }
}
cmDb2Close($db2con);
/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
     'proc' => $proc,
     'aa'  => $aa
   
);

echo(json_encode($rtnAry));

/**
*-------------------------------------------------------* 
* メールグループが更新すること
* 
* RESULT
*    01：更新終了場合     true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $MGTEXT メールグループ名
* 
*-------------------------------------------------------*
*/
function fnUpdateDB2AMST($db2con,$AMNAME,$AMTEXT){

    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2AMST ';
    $strSQL .= ' SET ';
    $strSQL .= ' AMTEXT = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' AMNAME = ? ';

    $params = array($AMTEXT,$AMNAME);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }

    return $rs;
}
/**
*-------------------------------------------------------* 
* 新メールグループを登録すること
* 
* RESULT
*    01：登録終了の場合     true
*    02：エラー場合         false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $MGTEXT メールグループ名
* 
*-------------------------------------------------------*
*/
function fnInsertDB2AMST($db2con,$AMNAME,$AMTEXT){

    $rs = true;
    //構文
    $strSQL  = ' INSERT INTO DB2AMST ';
    $strSQL .= ' ( ';
    $strSQL .= ' AMNAME, ';
    $strSQL .= ' AMTEXT ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?) ';

    $params = array($AMNAME,$AMTEXT);
   
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* メールグループが存在するかどうかをチェックすること
* 
* RESULT
*    01：存在する場合   true
*    02：存在ない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* 
*-------------------------------------------------------*
*/
function fnCheckDB2AMST($db2con,$AMNAME){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.AMNAME ' ;
    $strSQL .= ' FROM DB2AMST AS A ' ;
    $strSQL .= ' WHERE A.AMNAME = ? ' ;

    $params = array($AMNAME);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r=db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
          while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}