<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$AMNAME = $_POST['AMNAME'];
$AMTEXT = $_POST['AMTEXT'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmlspecialcharsをデコード
$AMNAME = cmHscDe($AMNAME);
$AMTEXT = cmHscDe($AMTEXT);
$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID']))?$_SESSION['PHPQUERY']['user'][0]['WUUID']:'';
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$WUUID);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('権限作成の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'27',$userData[0]['WUSAUT']);//'1' => UsercontrolMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('権限作成の権限'));
            }
        }
    }
}

if($proc === 'EDIT'){
    if($rtn === 0){
        $rs = fnSelDB2AMST($db2con,$AMNAME);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('権限ID'));
        }else{
            $data = $rs['data'];
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $rs = fnGetAllCount($db2con,$AMNAME,$AMTEXT);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetDB2AMST($db2con,$AMNAME,$AMTEXT,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}

cmDb2Close($db2con);


/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,$htmlFlg),
    '_SESSION'=>$_SESSION,
    'WUUID' => $WUUID
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2AMST($db2con,$AMNAME,$AMTEXT,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();
        $strSQL  = ' SELECT A.* ';
        $strSQL .= ' FROM ( SELECT B.AMNAME,B.AMTEXT , ROWNUMBER() OVER( ';

        if($sort !== ''){
            $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= ' ORDER BY B.AMNAME ASC ';
        }

        $strSQL .= ' ) as rownum ';
//        $strSQL .= ' FROM DB2AMST as B ';
        $strSQL .= ' FROM DB2AMST as B ';

        $strSQL .= ' WHERE AMNAME <> \'\' ';
        if($AMNAME != ''){
            $strSQL .= ' AND AMNAME like ? ';
            array_push($params,'%'.$AMNAME.'%');
        }

        if($AMTEXT != ''){
            $strSQL .= ' AND AMTEXT like ? ';
            array_push($params,'%'.$AMTEXT.'%');
        }
        
        $strSQL .= ' ) as A ';
        
        //抽出範囲指定
        if(($start != '')&&($length != '')){
            $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
            array_push($params,$start + 1);
            array_push($params,$start + $length);
        }
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$AMNAME = '',$AMTEXT = ''){
    $data = array();

    $strSQL  = ' SELECT count(A.AMNAME) as COUNT ';
    $strSQL .= ' FROM  DB2AMST as A ' ;
    $strSQL .= ' WHERE AMNAME <> \'\' ';

    $params = array();

    if($AMNAME != ''){
        $strSQL .= ' AND AMNAME like ? ';
        array_push($params,'%'.$AMNAME.'%');
    }

    if($AMTEXT != ''){
        $strSQL .= ' AND AMTEXT like ? ';
        array_push($params,'%'.$AMTEXT.'%');
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* ユーザーマスター取得(一行)
*-------------------------------------------------------*
*/

function fnSelDB2AMST($db2con,$AMNAME){

    $data = array();

    $params = array($AMNAME);
    $rs = true;


        $strSQL  = ' SELECT ';
        $strSQL .= ' A.AMNAME,A.AMTEXT  ';
        $strSQL .= ' FROM DB2AMST as A ';
        $strSQL .= ' WHERE AMNAME = ? ';

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                if(count($data)=== 0){
                    $data = array('result' => 'NOTEXIST_GET');
                }else{
                    $data = array('result' => true,'data' => $data);
                }
            }
        }
    return $data;
}