<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
$AMNAME =  (isset($_POST['AMNAME'])?$_POST['AMNAME']:'');
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$AMNAME = cmHscDe($AMNAME);
//ログインユーザが削除されたかどうかチェック
$rtn = 0;
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('権限作成の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'27',$userData[0]['WUSAUT']);//'1' => UsercontrolMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('権限作成の権限'));
            }
        }
    }


 $masterAry = array(
        array(0,  '1',getConstByLang('ユーザー作成')),
        array(10,  '2',getConstByLang('グループ作成')),
        array(20,  '3',getConstByLang('クエリー作成')),
        array(30, '34',getConstByLang('クエリーグループ作成')),//クエリーグループ
		array(40,  '4',getConstByLang('グループクエリー権限')),
        array(50,  '5',getConstByLang('グループユーザー権限')),
        array(60,  '6',getConstByLang('ダウンロード権限')),
        array(70,  '7',getConstByLang('クエリー実行設定')),
        array(80,  '8',getConstByLang('HTMLテンプレート')),
        array(90,  '9',getConstByLang('EXCELテンプレート')),
        array(100, '10',getConstByLang('詳細情報設定')),
        array(110, '11',getConstByLang('ダウンロード制限')),
        array(120, '12',getConstByLang('ユーザー補助')),
        array(130, '13',getConstByLang('検索候補')),
        array(140, '14',getConstByLang('制御レベル設定')),
        array(150, '15',getConstByLang('ピボット設定')),
        array(160, '16',getConstByLang('CSV形式')),
        array(170, '17',getConstByLang('スケジュール')),
        array(180, '18',getConstByLang('メール配信グループ設定')),
        array(190, '19',getConstByLang('CL連携')),
        array(200, '20',getConstByLang('メールサーバー')),
        array(210, '21',getConstByLang('会社情報')),
        array(220, '22',getConstByLang('利用停止時間')),
        array(230, '23',getConstByLang('実行ログ')),
        array(240, '24',getConstByLang('HTML禁止タグ')),
        array(250, '25',getConstByLang('区画')),
        array(260, '26',getConstByLang('言語')),
        array(270, '27',getConstByLang('権限')),
        array(280, '28',getConstByLang('システム設定')),
        array(290, '29',getConstByLang('ライセンス情報')),
        array(300, '30',getConstByLang('グラフ作成')),
        array(310, '31',getConstByLang('ライブラリー除外')),
        array(320, '32',getConstByLang('エクスポート')),
        array(330, '33',getConstByLang('インポート'))

        /*array(30,  '4',getConstByLang('グループクエリー権限')),
        array(40,  '5',getConstByLang('グループユーザー権限')),
        array(50,  '6',getConstByLang('ダウンロード権限')),
        array(60,  '7',getConstByLang('クエリー実行設定')),
        array(70,  '8',getConstByLang('HTMLテンプレート')),
        array(80,  '9',getConstByLang('EXCELテンプレート')),
        array(90, '10',getConstByLang('詳細情報設定')),
        array(100, '11',getConstByLang('ダウンロード制限')),
        array(110, '12',getConstByLang('ユーザー補助')),
        array(120, '13',getConstByLang('検索候補')),
        array(130, '14',getConstByLang('制御レベル設定')),
        array(140, '15',getConstByLang('ピボット設定')),
        array(150, '16',getConstByLang('CSV形式')),
        array(160, '17',getConstByLang('スケジュール')),
        array(170, '18',getConstByLang('メール配信グループ設定')),
        array(180, '19',getConstByLang('CL連携')),
        array(190, '20',getConstByLang('メールサーバー')),
        array(200, '21',getConstByLang('会社情報')),
        array(210, '22',getConstByLang('利用停止時間')),
        array(220, '23',getConstByLang('実行ログ')),
        array(230, '24',getConstByLang('HTML禁止タグ')),
        array(240, '25',getConstByLang('区画')),
        array(250, '26',getConstByLang('言語')),
        array(260, '27',getConstByLang('権限')),
        array(270, '28',getConstByLang('システム設定')),
        array(280, '29',getConstByLang('ライセンス情報')),
        array(290, '30',getConstByLang('グラフ作成')),
        array(300, '31',getConstByLang('ライブラリー除外')),
        array(310, '32',getConstByLang('エクスポート')),
        array(320, '33',getConstByLang('インポート'))*/
    );
 array_multisort($masterAry);
$data = array();
if($rtn === 0){
    $rs = cmSelDB2AMST($db2con,$AMNAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('権限ＩＤ'));
    }else{
        $data = $rs['data'];
        if(count($data)=== 0){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_GET',array('権限ＩＤ'));
        }
    }
}
$AGRYARY = array();
if($rtn === 0){
    $rs = cmSelDB2AGRT($db2con,$AMNAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = umEx($rs['data']);
        foreach($data as $key => $value){
                $AGRYARY[$value['AGMNID']] = $value['AGFLAG'];
        }
    }
}
$rtnAry = array(
    'MASTERARY' => $masterAry,
    'AGRYARY' => $AGRYARY,
    'RTN' =>$rtn,
    'MSG' =>$msg
);
echo json_encode($rtnAry);
