<?php
/*
 *  指定したユーザはログインユーザーがダウロード権限あるかをチェック
 */
function chkQryUser($db2con,$WDUID){
    $rs = true;
    $data = array();
    $strSQL = '';
    $strSQL .= '    SELECT DISTINCT ';
    $strSQL .= '        WUUID ';
    $strSQL .= '    FROM ';
    $strSQL .= '        DB2WUGR ';
    $strSQL .= '    WHERE ';
    $strSQL .= '        WUGID IN (SELECT ';
    $strSQL .= '                        WUGID ';
    $strSQL .= '                    FROM ';
    $strSQL .= '                        DB2WUGR ';
    $strSQL .= '                    WHERE ';
    $strSQL .= '                        WUUID = ? ';
    $strSQL .= '                    ) ';
    $strSQL .= '    AND WUUID = ? ';
    $params = array(
        $_SESSION['PHPQUERY']['user'][0]['WUUID'],
        $WDUID
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}