<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WUUID = $_POST['WUUID'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/

//htmldecode
$WUUID = cmHscDe($WUUID);


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('ユーザ作成の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'1',$userData[0]['WUSAUT']);//'1' => userMaster
            
                if($rs['result'] !== true){
                    $usrKengenFlg = '';
                }
               if($usrKengenFlg === ""){
                    $res = cmChkKenGen($db2con,'6',$userData[0]['WUSAUT']);//'6' => ダウロード権限
                    if($res['result'] !== true){
                       $downloadKengenFlg = '';
                    }
               }
               if($usrKengenFlg === '' && $downloadKengenFlg === ''){
                    $rtn = 2;
                    $msg =  showMsg($rs['result'],array('ユーザ作成の権限'));
               }else if($usrKengenFlg === ''){
                    $rtn = 3;
                    $msg =  showMsg($rs['result'],array('ユーザ作成の権限'));
                }

              }

         }
    }

//ユーザー存在チェック
if($rtn === 0){
    $rs = fnCheckDB2WUSR($db2con,$WUUID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('ユーザー'));
    }
}

//管理者が一人しかいない場合はエラー
if($rtn === 0){
    $rs = fnCheckAdmin($db2con,$WUUID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('管理者','管理者ユーザー'));
    }
}

if($rtn === 0){
    $rs = fnDeleteDB2WUSR($db2con,$WUUID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

if($rtn === 0){
    //権限情報も削除
    $rs = fnDeleteDB2WDEF($db2con,$WUUID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

//ユーザー別クエリー定義情報も削除
if($rtn === 0){
    $rs = fnDeleteDB2WUGR($db2con,$WUUID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

//ユーザー別カラム定義情報も削除
if($rtn === 0){
    $rs = fnDeleteDB2WCOL($db2con,$WUUID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

//ダッシュボードとブックマークのため削除
if($rtn === 0){
    $rs = fnDelDB2BSB_DB2BMK($db2con,$WUUID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
//お気に入りのクエリー削除
if($rtn === 0){
    $rs = fnDelDB2QBMK($db2con,$WUUID);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* ユーザー存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WUSR($db2con,$WUUID){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.WUUID ';
    $strSQL .= ' FROM DB2WUSR AS A ' ;
    $strSQL .= ' WHERE A.WUUID = ? ' ;

    $params = array(
        $WUUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_DEL';
            }
        }            
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* 管理者チェック　２人以上いればtrue
*-------------------------------------------------------*
*/

function fnCheckAdmin($db2con,$WUUID){

    $rs = true;

    $data = array();

    //ユーザーの権限を取得
    $strSQL  = ' SELECT A.WUAUTH ';
    $strSQL .= ' FROM DB2WUSR AS A ' ;
    $strSQL .= ' WHERE A.WUUID = ? ' ;

    $params = array(
        $WUUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'CHK_AUT';
            }
        }
    }

    if($rs === true){
        //削除対象が管理者だった場合、管理者権限をもつユーザーを検索
        if($data[0]['WUAUTH'] === '1'){

            $data = array();

            $strSQL  = ' SELECT A.WUUID ';
            $strSQL .= ' FROM DB2WUSR AS A ' ;
            $strSQL .= ' WHERE A.WUAUTH = ? ' ;

            $params = array(
                '1'
            );

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $rs = 'FAIL_SEL';
            }else{
                $r = db2_execute($stmt,$params);

                if($r === false){
                    $rs = 'FAIL_SEL';
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    if(count($data) === 1){
                        $rs = 'CHK_AUT';
                    }
                }
            }
        }
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* ユーザー削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WUSR($db2con,$WUUID){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WUSR ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUUID = ? ' ;

    $params = array(
        $WUUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}
/*
*ダッシュボードとブックマークのため削除
*/
function fnDelDB2BSB_DB2BMK($db2con,$WUUID){
	$rs = true;
	$strSQL='';
    $strSQL  = ' DELETE FROM DB2BMK AS A WHERE ';
    $strSQL .= ' EXISTS( SELECT * FROM DB2DSB AS B WHERE A.BMKDSBID=B.DSBID AND B.DSBUID=?) ';
	$params = array(
		$WUUID
	);
	$stmt = db2_prepare($db2con,$strSQL);
	if($stmt === false){
		$rs = 'FAIL_DEL';
	}else{
		$r = db2_execute($stmt,$params);
		if($r === false){
			$rs = 'FAIL_DEL';
		}else{
			$strSQL  = ' DELETE FROM DB2DSB AS A WHERE ';
			$strSQL  .= ' A.DSBUID=? ';
			$stmt = db2_prepare($db2con,$strSQL);
			if($stmt === false){
				$rs = 'FAIL_DEL';
			}else{
				$r = db2_execute($stmt,$params);
				if($r === false){
					$rs = 'FAIL_DEL';
				}
			}
		}
	}
	return $rs;
}
/*
*-------------------------------------------------------* 
* ユーザーごとのお気に入りクエリー削除
*-------------------------------------------------------*
*/

function fnDelDB2QBMK($db2con,$WDUID){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2QBMK ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' QBMUSR = ? ' ;

    $params = array(
        $WDUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}
/*
*-------------------------------------------------------* 
* 権限情報削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WDEF($db2con,$WDUID){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WDEF ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WDUID = ? ' ;

    $params = array(
        $WDUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;

}
/*
*-------------------------------------------------------* 
* ユーザー別クエリー定義情報削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WUGR($db2con,$WUUID){

    $rs = true;

    //構文

    $strSQL  = ' DELETE FROM DB2WUGR ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUUID = ? ' ;

    $params = array(
        $WUUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* ユーザー別カラム定義情報削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WCOL($db2con,$WCUID){

    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WCOL ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WCUID = ? ' ;

    $params = array(
        $WCUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}