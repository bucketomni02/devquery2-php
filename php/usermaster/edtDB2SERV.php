<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['proc']))?$_POST['proc']:'';
$WUSERV = (isset($_POST['WUSERV']))?$_POST['WUSERV']:'';
$WDUID = (isset($_POST['WDUID']))?$_POST['WDUID']:'';
$DATA = json_decode($_POST['DATA'],true);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$WUSERV = cmHscDe($WUSERV);
$DATA['WUSERV'] = trim($DATA['WUSERV']);


$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'6',$userData[0]['WUSAUT']);//'6' => ダウロード権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('標準ダウンロード先の権限'));
            }
        }
    }
}

if($rtn === 0){
   if(!checkMaxLen($WUSERV,256)){
       $rtn = 1;
       $msg = showMsg('FAIL_MAXLEN',array(array('標準ダウンロード','先')));
       $focus = 'WUSERV';
   }
}

if($rtn ===0){
    if(cmMer($DATA['WUSERV']) !== ''){
        if($licenseIFS !== true){
            $rtn = 1;
            $msg = showMsg('FAIL_NUSE',array('標準ダウンロード先'));
        }
    }
}

/*if($rtn === 0){
    $WUSERV = explode('/',$DATA['WUSERV']);
    if(count($WUSERV) > 0){
        foreach ($WUSERV as $value){
            if (preg_match('/[\\/:*?"<>|]/', $value)) {
                $rtn = 1;
                break;
            }
        }
        if($rtn !== 0){
            $rtn = 1;
            $msg = showMsg('FAIL_VLDD',array('標準ダウンロード先',array('/[\/:*?"<>|]/')));// '標準ダウンロード先に使用できない文字が含まれています。</br>標準ダウンロード先には次の文字しか使えません。<br>【\,/,:,*,?,",<,>,|】';
            $focus = 'WUSERV';
        }
    }
}*/

if($rtn === 0){
    $rs = fnCheckDB2SERV($db2con,$WUSERV);
    if( $rs !== true){
        $rtn = 1;
        $msg = showMsg('NOTEXIST_UPD',array(array('標準ダウンロード','先')));
        $focus = 'WUSERV';
    }
}

if($rtn === 0){
    //更新対象ＨＴＭＬ名を変更する処理
    $rs = fnUpdateDB2SERV($db2con,$WDUID,$DATA['WUSERV'],$DATA['WUCTFL'],$DATA['WUCNFL']);
    if( $rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}
      
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'ServerName' =>$WUSERV,
    'ID' =>$WDUID
);

echo(json_encode($rtnAry));

/**
*-------------------------------------------------------* 
* メールグアドレスが存在するかどうかのチェックすること
* 
* RESULT
*    01：存在する場合     true
*    02：存在しない場合   false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $HTMLNM ＨＴＭＬ名
* 
*-------------------------------------------------------*
*/
function fnCheckDB2SERV($db2con,$WDUID){
    $data = array();
    $rs = true;
    
    $strSQL  = ' SELECT A.WUSERV ' ;
    $strSQL .= ' FROM DB2WUSR AS A ' ;
    $strSQL .= ' WHERE A.WUUID = ? ';
    
    $params = array($WDUID);
    $stmt = db2_prepare($db2con,$strSQL);
   // $stmt = false;
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;
}

/**
*-------------------------------------------------------* 
* 新メールグアドレスを登録すること
* 
* RESULT
*    01：登録終了の場合   true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $HTMLNM ＨＴＭＬ名
* 
*-------------------------------------------------------*
*/
/*function fnInsertDB2SERV($db2con,$WUSERV){
    $HTMLID = getHTMLIDRandom($db2con);
    $rs = true;
    //構文
    $strSQL  = ' INSERT INTO DB2HTMLK ';
    $strSQL .= ' ( ';
    $strSQL .= ' HTMLID, ';
    $strSQL .= ' HTMLNM ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?) ';

    $params = array($HTMLID,strtolower($HTMLNM));

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}*/

/*function getHTMLIDRandom($db2con){
        $rs = true;
        $count  =0;
        $HTMLID = '';
        do{
                $count++;
                $HTMLID = $rand = substr(md5(microtime()),rand(0,26),6);
                 $res = fnCheckDB2HTMLK($db2con,$HTMLID,'HTMLID','');
                if($res !== true){
                    $test = false;
                    $rs = true;
                }else{
                    $rs = false;
                }
        }while($rs !== false);
        return $HTMLID;
}*/
/**
*-------------------------------------------------------* 
* メールグアドレスを更新すること
* 
* RESULT
*    01：更新終了の場合   true
*    02：エラーの場合     false
* 
* @param Object  $db2con DBコネクション
* @param String  $MGNAME メールグループID
* @param String  $HTMLNM ＨＴＭＬ名
* 
*-------------------------------------------------------*
*/
function fnUpdateDB2SERV($db2con,$WDUID,$WUSERV,$WUCTFL,$WUCNFL){

    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2WUSR ';
    $strSQL .= ' SET ';
    $strSQL .= ' WUSERV = ? ,';
    $strSQL .= ' WUCTFL = ? ,';
    $strSQL .= ' WUCNFL = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WUUID = ? ';
    $params = array($WUSERV,$WUCTFL,$WUCNFL,$WDUID);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}

/*function fnDeleteDB2SERV($db2con,$WDUID,$WUSERV){
    $rs = true;
    $strSQL = 'DELETE WUSERV FROM DB2WUSR ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' HTMLID = ? ';
    $strSQL .= ' AND WUSERV = ? ';
    $params = array($WDUID,$WUSERV);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}*/
