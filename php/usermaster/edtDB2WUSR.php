<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc   = $_POST['PROC'];
$WUUID  = cmMer($_POST['WUUID']);
$WUPSWD = $_POST['WUPSWD'];
$WUPSWDCONF = $_POST['WUPSWDCONF'];
$WUUNAM = $_POST['WUUNAM'];
$WUAUTH = $_POST['WUAUTH'];
$WUSAUT = $_POST['WUSAUT'];
$WULIBL = $_POST['WULIBL'];
$WUIPPU = (isset($_POST['WUIPPU']) ? $_POST['WUIPPU'] : ''); //一般ユーザー
$WUDWNL = $_POST['WUDWNL'];
$WUDWN2 = $_POST['WUDWN2'];
$today  = date('Y-m-d');
$WUEDAY = str_replace("-","",$today);
$WULGDL = $_POST['WULGDL'];
$WUUQFG = (isset($_POST['WUUQFG']) ? $_POST['WUUQFG'] : '');
$WUUQFG = cmMer($WUUQFG);
$WUUBFG = (isset($_POST['WUUBFG']) ? $_POST['WUUBFG'] : '');
$WUUBFG = cmMer($WUUBFG);
//MSM add 3 new field for ACTIVE USER ,WUAID,WUADO,WUAFLG
$WUAID = (isset($_POST['WUAID']) ? $_POST['WUAID'] : '');
$WUAID = cmMer($_POST['WUAID']);
$WUADO = (isset($_POST['WUADO']) ? $_POST['WUADO'] : '');
$WUADO = cmMer($_POST['WUADO']);
$WUAFLG = (isset($_POST['WUAFLG']) ? $_POST['WUAFLG'] : '');
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('ユーザ作成の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'1',$userData[0]['WUSAUT']);//'1' => userMaster
            if($rs['result'] !== true){
                $usrKengenFlg = '';
            }
            if($usrKengenFlg === ""){
                $res = cmChkKenGen($db2con,'6',$userData[0]['WUSAUT']);//'6' => ダウロード権限
                if($res['result'] !== true){
                   $downloadKengenFlg = '';
                }
            }
            if($usrKengenFlg === '' && $downloadKengenFlg === ''){
                $rtn = 2;
				$_SESSION['PHPQUERY']['kengenFlg'] = false;
                $msg =  showMsg($rs['result'],array('ユーザ作成の権限'));
            }else if($usrKengenFlg === ''){
                $rtn = 3;
				$_SESSION['PHPQUERY']['kengenFlg'] = false;
                $msg =  showMsg('NOTEXIST',array('ユーザ作成の権限'));
            }
        }
    }
}

if($rtn === 0){
    //必須チェック
    if($WUUID === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('ID'));
        $focus = 'WUUID';
    }
}
//バリデーションチェック
if($rtn === 0){
    if(!checkMaxLen($WUUID,20)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('ID'));
        $focus = 'WUUID';
    }
}
if($rtn === 0){
    $byte = checkByte($WUUID);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('ID'));
        $focus = 'WUUID';
    }
}

if($rtn === 0){
    if($WUPSWD === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('パスワード'));
        $focus = 'WUPSWD';
    }
}
if($rtn === 0){
    $PWD = cmMer($WUPSWD);
    if($PWD === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_SPACE');
        $focus = 'WUPSWD';
    }
}
if($rtn === 0){
    $byte = checkByte($WUPSWD);
    if($byte[1] > 0){
        $rtn = 1;
        $msg = showMsg('FAIL_BYTE',array('パスワード'));
        $focus = 'WUPSWD';
    }
}

if($rtn === 0){
    if(!checkMaxLen($WUPSWD,20)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('パスワード'));
        $focus = 'WUPSWD';
    }
}

if($rtn === 0){
    if($WUUNAM === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_REQ',array('ユーザー名'));
        $focus = 'WUUNAM';
    }
}
//MSM 変更フラグをチェックしたらドメインを入れるチェック WUADO,WUAFLG
if($rtn === 0){
    if($WUAFLG === '1'){
        if($WUADO === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_REQ',array('ドメイン'));
            $focus = 'WUADO';
        }        
    }
}

if($rtn === 0){
    if($WUAUTH === '2'){
        $rs = cmSelDB2AMST($db2con,$WUSAUT);
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }else{
            if(count($rs['data']) === 0){
                $rtn = 1;
                $msg =  showMsg('NOTEXIST',array('選択されているその他の権限のデータ'));
            }
        }
    }
}

if($rtn === 0){
    if($WUAUTH === '2' && $WUSAUT === ''){
        $rtn = 1;
        $msg = showMsg('FAIL_SLECJS',array('その他権限')); 
    }
}

if($rtn === 0){
    if(!checkMaxLen($WUUNAM,32)){
        $rtn = 1;
        $msg = showMsg('FAIL_MAXLEN',array('ユーザー名'));
        $focus = 'WUUNAM';
    }
}

//パスワードチェック
if($rtn === 0){
    if($WUPSWD !== $WUPSWDCONF){
        $rtn = 1;
        $msg = showMsg('FAIL_CMP',array('パスワードの確認入力'));
        $focus = 'WUPSWDCONF';
    }
}

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/
if($proc === 'UPD'){
    //ユーザー存在チェック
    if($rtn === 0){
        $rs = fnCheckDB2WUSR($db2con,$WUUID);
        if($rs === true){
            $rtn = 1;
            $msg = showMsg('NOTEXIST_UPD',array('ユーザー'));
            $focus = 'WUUID';
        }else if($rs !== 'ISEXIST'){
            $rtn = 1;
            $msg = showMsg($rs,array('ユーザー'));
            $focus = 'WUUID';
        }
    }
    if($WUAUTH === '1'){
       $WULGDL= '0';//回数をブラク更新
    }else if($WUAUTH === '2'){
        $rs = cmChkKenGen($db2con,'1',$WUSAUT);//'1' => userMaster
        if($rs['result'] === true){
              $WULGDL= '0';//回数をブラク更新
        }
    }
    if($WUAUTH !== '1'){
        //管理者が一人しかいない場合はエラー
        if($rtn === 0){
            $rs = fnCheckWUAUTH($db2con,$WUUID);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs,array('管理者',array('管理者','ユーザー')));
            }
        }
    }
    if($WUAUTH !== '0' && $WUIPPU !== ''){
        //クエリー移動されるユーザーがちゃんと一般ユーザーになってるかどうかのチェック
        if($rtn === 0){
            $rs = fnCheckWUIPPU($db2con,$WUIPPU);
            if($rs['result'] !== true){
                $rtn = 1;
                $msg = showMsg($rs['result']);
            }
        }
    }
    //ユーザー情報更新
    if($rtn === 0){
        if($WULGDL == '0'){
            if($WUAID != ""){//MSM　add for ユーザー exist or not
                $chk_WUAID = fnCheckWUAID($db2con,$WUAID,$WUUID);
                if($chk_WUAID === "NOTEXIST"){
                    $rs = fnUpdateDB2WUSRLOGDEL($db2con,$WUUID,$WUPSWD,$WUUNAM,$WUAUTH,$WUEDAY,$WULGDL,$WUSAUT,$WULIBL,$WUUQFG,$WUUBFG,$WUAID,$WUADO,$WUAFLG);
                    if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs);
                    }
                    
                }else{
                    $rtn = 1;
                    $msg = showMsg("同じユーザーIDが存在しています。");
                }
            }else{
                $rs = fnUpdateDB2WUSRLOGDEL($db2con,$WUUID,$WUPSWD,$WUUNAM,$WUAUTH,$WUEDAY,$WULGDL,$WUSAUT,$WULIBL,$WUUQFG,$WUUBFG,$WUAID,$WUADO,$WUAFLG);
                if($rs !== true){
                    $rtn = 1;
                    $msg = showMsg($rs);
                }
            }
            
        }
        else{
            if($WUAID != ""){//MSM　add for ユーザー exist or not
                $chk_WUAID = fnCheckWUAID($db2con,$WUAID,$WUUID);
                if($chk_WUAID === "NOTEXIST"){
                    $rs = fnUpdateDB2WUSR($db2con,$WUUID,$WUPSWD,$WUUNAM,$WUAUTH,$WUEDAY,$WUSAUT,$WULIBL,$WUUQFG,$WUUBFG,$WUAID,$WUADO,$WUAFLG);
                    if($rs !== true){
                        $rtn = 1;
                        $msg = showMsg($rs);
                    }
                    
                }else{
                    $rtn = 1;
                    $msg = showMsg("同じユーザーIDが存在しています。");
                }
            }else{
                $rs = fnUpdateDB2WUSR($db2con,$WUUID,$WUPSWD,$WUUNAM,$WUAUTH,$WUEDAY,$WUSAUT,$WULIBL,$WUUQFG,$WUUBFG,$WUAID,$WUADO,$WUAFLG);
                if($rs !== true){
                    $rtn = 1;
                    $msg = showMsg($rs);
                }
            }   
        }
    }
    //一般ユーザークエリー移動
    if($rtn === 0){
        if($WUAUTH !== '0' && $WUIPPU !== ''){
            $rs = fnTranferQUERY($db2con,$WUUID,$WUIPPU);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }
    }

}else if($proc === 'ADD'){
    //ユーザー存在チェック
    if($rtn === 0){
        $rs = fnCheckDB2WUSR($db2con,$WUUID);
        if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs,array('ID'));
            $focus = 'WUUID';
        }
    }
    if($rtn === 0){
        if($WUAID != ""){//MSM　add for ユーザー exist or not
            $chk_WUAID = fnCheckWUAID($db2con,$WUAID,$WUUID);
            if($chk_WUAID === "NOTEXIST"){
                $rs = fnInsertDB2WUSR($db2con,$WUUID,$WUPSWD,$WUUNAM,$WUAUTH,$WUEDAY,$WUSAUT,$WULIBL,$WUUQFG,$WUUBFG,$WUAID,$WUADO,$WUAFLG);
                if($rs !== true){
                    $rtn = 1;
                    $msg = showMsg($rs);
                }
                
            }else{
                $rtn = 1;
                $msg = showMsg("同じユーザーIDが存在しています。");
            }
        }else{
            $rs = fnInsertDB2WUSR($db2con,$WUUID,$WUPSWD,$WUUNAM,$WUAUTH,$WUEDAY,$WUSAUT,$WULIBL,$WUUQFG,$WUUBFG,$WUAID,$WUADO,$WUAFLG);
            if($rs !== true){
                $rtn = 1;
                $msg = showMsg($rs);
            }
        }
        
    }
}

if($rtn !== 0){
    $flg = 2;
    db2_rollback($db2con);
}else{
    db2_commit($db2con);
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'WUUID' => $WUUID,
    'WUAUTH' => $WUAUTH,
    'LOGINID' => $_SESSION['PHPQUERY']['user'][0]['WUUID'],
    'WUUQFG' => $WUUQFG
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ユーザー数をリターン
*-------------------------------------------------------*
*/

function fnCountDB2WUSR($db2con){

    $data = array();

    $strSQL  = ' SELECT COUNT(*) as COUNT ';
    $strSQL .= ' FROM DB2WUSR AS A ' ;

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data[0]['COUNT']);
        }
    }
    return $data;

}


/*
*-------------------------------------------------------* 
* ユーザー存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WUSR($db2con,$WUUID){

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.WUUID ';
    $strSQL .= ' FROM  DB2WUSR AS A ' ;
    $strSQL .= ' WHERE A.WUUID = ? ' ;

    $params = array(
        $WUUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = 'ISEXIST';
            }
        }
    }
    return $rs;

}


/*
*-------------------------------------------------------* 
* 管理者チェック　２人以上いればtrue
*-------------------------------------------------------*
*/

function fnCheckWUAUTH($db2con,$WUUID){

    $rs = true;

    $data = array();

    //ユーザーの権限を取得
    $strSQL  = ' SELECT A.WUAUTH ';
    $strSQL .= ' FROM DB2WUSR AS A ' ;
    $strSQL .= ' WHERE A.WUUID = ? ' ;

    $params = array(
        $WUUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'CHK_AUTUPD';
            }
        }
    }

    if($rs === true){
        //削除対象が管理者だった場合、管理者権限をもつユーザーを検索
        if($data[0]['WUAUTH'] === '1'){

            $data = array();

            $strSQL  = ' SELECT A.WUUID ';
            $strSQL .= ' FROM DB2WUSR  AS A ' ;
            $strSQL .= ' WHERE A.WUAUTH = ? ' ;

            $params = array(
                '1'
            );

            $stmt = db2_prepare($db2con,$strSQL);
            if($stmt === false){
                $rs = 'FAIL_SEL';
            }else{
                $r = db2_execute($stmt,$params);

                if($r === false){
                    $rs = 'FAIL_SEL';
                }else{
                    while($row = db2_fetch_assoc($stmt)){
                        $data[] = $row;
                    }
                    if(count($data) === 1){
                        $rs = 'CHK_AUTUPD';
                    }
                }
            }
        }
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* ユーザー更新
*-------------------------------------------------------*
*/ //,$WUAID,$WUADO,$WUAFLG                                                   
function fnUpdateDB2WUSRLOGDEL($db2con,$WUUID,$WUPSWD,$WUUNAM,$WUAUTH,$WUEDAY,$WULGDL,$WUSAUT,$WULIBL,$WUUQFG,$WUUBFG,$WUAID,$WUADO,$WUAFLG){

    $rs = true;

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SQL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SQL';
        }
    }
    if($rs === true)
    {
        //構文
        $strSQL  = ' UPDATE DB2WUSR ';
        $strSQL .= ' SET ';
        $strSQL .= '    WUPSWE = CAST(encrypt(CAST(? AS VARCHAR(20))) AS CHAR(64) FOR BIT DATA), ';
        $strSQL .= '    WUUNAM = ?, ';
        $strSQL .= '    WUAUTH = ? ,';
        $strSQL .= '    WUEDAY = ? ,' ;
        $strSQL .= '    WULGNC = ? ,' ;
        $strSQL .= '    WUSAUT = ? ,' ;
        $strSQL .= '    WULIBL = ? ,' ;
        $strSQL .= '    WUUQFG = ? ,' ;
        $strSQL .= '    WUUBFG = ? ,' ;
        $strSQL .= '    WUAID = ? ,' ;
        $strSQL .= '    WUADO = ? ,' ;
        $strSQL .= '    WUAFLG = ? ' ;
        $strSQL .= ' WHERE ';
        $strSQL .= '    WUUID = ? ' ;

        $params = array(
            $WUPSWD,
            $WUUNAM,
            $WUAUTH,
            $WUEDAY,
            $WULGDL,
            $WUSAUT,
            $WULIBL,
            $WUUQFG,
            $WUUBFG,
            $WUAID,
            $WUADO,
            $WUAFLG,
            $WUUID    
        );

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_UPD';
        }else{
             $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_UPD';
            }
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* ユーザー更新
*-------------------------------------------------------*
*/
//MSM add ,$WUAID,$WUADO,$WUAFLG
function fnUpdateDB2WUSR($db2con,$WUUID,$WUPSWD,$WUUNAM,$WUAUTH,$WUEDAY,$WUSAUT,$WULIBL,$WUUQFG,$WUUBFG,$WUAID,$WUADO,$WUAFLG){

    $rs = true;

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SQL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SQL';
        }
    }
    if($rs === true)
    {
        //構文
        $strSQL  = ' UPDATE DB2WUSR ';
        $strSQL .= ' SET ';
        $strSQL .= '    WUPSWE = CAST(encrypt(CAST(? AS VARCHAR(20))) AS CHAR(64) FOR BIT DATA), ';
        $strSQL .= '    WUUNAM = ?, ';
        $strSQL .= '    WUAUTH = ? ,';
        $strSQL .= '    WUEDAY = ? ,' ;
        $strSQL .= '    WUSAUT = ? ,' ;
        $strSQL .= '    WULIBL = ? ,' ;
        $strSQL .= '    WUUQFG = ? ,' ;
        $strSQL .= '    WUUBFG = ? ,' ;
        $strSQL .= '    WUAID = ? ,' ;
        $strSQL .= '    WUADO = ? ,' ;
        $strSQL .= '    WUAFLG = ? ' ;
        $strSQL .= ' WHERE ';
        $strSQL .= '    WUUID = ? ' ;
    
        $params = array(
            $WUPSWD,
            $WUUNAM,
            $WUAUTH,
            $WUEDAY,
            $WUSAUT,
            $WULIBL,
            $WUUQFG,
            $WUUBFG,
            $WUAID,
            $WUADO,
            $WUAFLG,
            $WUUID   
        );
        //e_log("MSM123 fnUpdateDB2WUSR => ".$strSQL.print_r($params,true));
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_UPD';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_UPD';
            }
        }
    }
    return $rs;

}
/*
*-------------------------------------------------------* 
* クエリー移動
*-------------------------------------------------------*
*/

function fnTranferQUERY($db2con,$WUUID,$WUIPPU){

    $rs = true;
    if($rs === true){
        //自分が作成してるクエリーを他の一般ユーザーに渡す
        $strSQL  = ' UPDATE DB2QHIS ';
        $strSQL .= ' SET ';
        $strSQL .= '    DQCUSR = ? ';
        $strSQL .= ' WHERE ';
        $strSQL .= '    DQCUSR = ? ' ;

        $params = array(
            $WUIPPU,$WUUID
        );
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_UPD';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_UPD';
            }
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* ユーザー追加
*-------------------------------------------------------*
*/

function fnInsertDB2WUSR($db2con,$WUUID,$WUPSWD,$WUUNAM,$WUAUTH,$WUEDAY,$WUSAUT,$WULIBL,$WUUQFG,$WUUBFG,$WUAID,$WUADO,$WUAFLG){


    $rs = true;

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';
    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = false;
    }else{        
        $r = db2_execute($stmt);
        if($r === false){
            $rs = false;
        }
    }
    if($rs === true)
    {
        $strSQL  = ' INSERT INTO DB2WUSR ';
        $strSQL .= ' ( ';
        $strSQL .= '    WUUID, ';
        $strSQL .= '    WUPSWE, ';
        $strSQL .= '    WUUNAM, ';
        $strSQL .= '    WUAUTH, ';
        $strSQL .= '    WUEDAY, ';
        $strSQL .= '    WUSAUT, ';
        $strSQL .= '    WULIBL, ';
        $strSQL .= '    WUUQFG, ';
        $strSQL .= '    WUUBFG, ';
        $strSQL .= '    WUAID, ';//MSM add ,$WUAID,$WUADO,$WUAFLG
        $strSQL .= '    WUADO, ';
        $strSQL .= '    WUAFLG ';
        $strSQL .= ' ) ';
        $strSQL .= ' VALUES ' ;
        $strSQL .= ' (?,CAST(encrypt(CAST(? AS VARCHAR(20))) AS CHAR(64) FOR BIT DATA),?,?,?,?,?,?,?,?,?,?) ';

        $params = array(
            $WUUID,
            $WUPSWD,
            $WUUNAM,
            $WUAUTH,
            $WUEDAY,
            $WUSAUT,
            $WULIBL,
            $WUUQFG,
            $WUUBFG,
            $WUAID,
            $WUADO,
            $WUAFLG
        );
        //e_log("MSM123 fnInsertDB2WUSR => ".$strSQL.print_r($params,true));
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $rs = 'FAIL_INS';
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $rs = 'FAIL_INS';
            }
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* DB2WDEFに追加
* ※EXCELとかCSVダウンロート権限の登録　【まだ使わない】
*-------------------------------------------------------*
*/

function fnInsertDB2WDEF($db2con,$WDUID,$WDNAME){


    $rs = true;

    //構文
    $strSQL  = ' INSERT INTO DB2WDEF ';
    $strSQL .= ' ( ';
    $strSQL .= '    WDUID, ';
    $strSQL .= '    WDNAME, ';
    $strSQL .= '    WDDWNL, ';
    $strSQL .= '    WDDWN2 ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ' ;
    $strSQL .= ' (?,?,?,?) ';

    $params = array(
        $WDUID,
        $WDNAME,
        '1',
        '1'
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* FDB2CSV1全件取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con){
    $data = array();

    $strSQL  = ' SELECT * ';
    $strSQL .= ' FROM FDB2CSV1 as A ' ;

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}
/*******
*
*クエリー移動されるユーザーがちゃんと一般ユーザーになってるかどうかのチェック
*
******/
function fnCheckWUIPPU($db2con,$WUUID){
    $data = array();

    $strSQL  = ' SELECT WUAUTH ';
    $strSQL .= ' FROM DB2WUSR WHERE WUUID = ?' ;

    $params = array($WUUID);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $wuauth = (int)$data[0]['WUAUTH'];
                if($wuauth === 0){
                    $data = array('result' => 'INVALID_IPPU');
                }else{
                    $data = array('result' => true);
                }
            }else{
                $data = array('result' => 'NO_IPPU');
            }
        }
    }
    return $data;
}


function fnCheckWUAID($db2con,$WUAID,$WUUID){

    $rs = true;

    $data = array();

    $strSQL  = ' SELECT A.WUAID ';
    $strSQL .= ' FROM DB2WUSR AS A ' ;
    $strSQL .= ' WHERE A.WUAID = ? ' ;
    $strSQL .= ' AND A.WUUID <> ? ' ;

    $params = array(
        $WUAID,
        $WUUID
    );
    e_log("fnCheckWUAID MSM => ".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            
            if(count($data) === 0){
                $rs = "NOTEXIST";
            }else{
                $rs = "EXIST";
            }
        }
    }
    return $rs;
}