<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
include_once("chkComUserMaster.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
$proc = (isset($_POST['proc']))?$_POST['proc']:'';
$WDSERV = (isset($_POST['WDSERV']))?$_POST['WDSERV']:'';
$WDNAME = (isset($_POST['D1NAME']))?$_POST['D1NAME']:'';
$WDUID = (isset($_POST['WDUID']))?$_POST['WDUID']:'';
$DATA = json_decode($_POST['DATA'],true);

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------* 
*/

$WDSERV = cmHscDe($WDSERV);
$DATA['WDSERV'] = trim($DATA['WDSERV']);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'6',$userData[0]['WUSAUT']);//'1' => ダウロード権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ダウンロード先の権限'));
            }
        }
    }
}

if($rtn === 0){
    if(!checkMaxLen($WDSERV,256)){
       $rtn = 1;
       $msg = showMsg('FAIL_MAXLEN',array(array('ダウンロード','先')));
       $focus = 'WDSERV';
    }
}

if($rtn ===0){
    if(cmMer($WDSERV) !== ''){
        if($licenseIFS !== true){
            $rtn = 1;
            $msg = showMsg('FAIL_NUSE',array('ダウンロード','先'));
        }
    }
}

if($rtn === 0){
    //ユーザが存在チェック
    if($rtn === 0){
        $rs = cmCheckUser($db2con,$WDUID);
        if($rs['result'] === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }else if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }
    }
}
if($rtn === 0){
    $chkQry = cmChkQuery($db2con,'',$WDNAME,'');
    if($chkQry['result'] !== true){
        $rtn = 1;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($rtn === 0){
    if($userData[0]['WUAUTH'] === '4'){
        if(cmMer($WDUID) !== $_SESSION['PHPQUERY']['user'][0]['WUUID'] ){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('ダウンロード権限の権限'));
        }
    }else if($userData[0]['WUAUTH'] === '3'){
        $rs = chkQryUser($db2con,$WDUID);
        if($rs === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('ダウンロード権限の権限'));
        }else if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }
    }
}
if($rtn === 0){
    if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4' ){
        $rs = chkVldQryUsr($db2con,$WDNAME,$userData[0]['WUAUTH']);
        if($rs === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg('指定したクエリーはこのユーザーにダウンロード権限がありません。');
        }else if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('クエリー'));
        }
    }
}
if($rtn === 0){
    if($rtn === 0){
        $checkRs = fnCheckFDB2SERV($db2con,$WDUID,$WDNAME);
        if($checkRs === true){
            e_log('update の方を行った');
            $defRs = fnUpdateFDB2SERV($db2con,$WDUID,$WDNAME,$DATA['WDSERV'],$DATA['WDCTFL'],$DATA['WDCNFL']);
             if($defRs !== true){
                $msg = showMsg($defRs);
                $rtn = 1;
            }
        }else if($checkRs === 'NOTEXIST_GET') {
            e_log("更新の方を行った");
            $defRs = fnInsertFDB2SERV($db2con,$WDUID,$WDNAME,$DATA['WDSERV'],$DATA['WDCTFL'],$DATA['WDCNFL']);
             if($defRs !== true){
                $msg = showMsg($defRs);
                $rtn = 1;
            }
        }else{
            $msg = showMsg($checkRs);
            $rtn = 1;
        }
    }
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'checkRs' => $checkRs,
    'defRs' => $defRs,
    'RTN' => $rtn,
    'MSG' => $msg,
    'WDSERV' =>$WDSERV,
    'WDNAME' =>$WDNAME,
    'WUUID' => $WDUID,
    'DATA' => $DATA
);

echo(json_encode($rtnAry));

function fnCheckFDB2SERV($db2con,$WDUID,$WDNAME){
    
    $rs = true;
    
    $strSQL  = ' SELECT A.WDUID ' ;
    $strSQL .= ' FROM DB2WDEF AS A ' ;
    $strSQL .= ' WHERE A.WDUID = ? ' ;
    $strSQL .= ' AND A.WDNAME = ? ';
      
    $params = array(
        $WDUID,
        $WDNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
   // $stmt = false;
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $DATA[] = $row;
            }
             if(count($DATA) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}


/*
*-------------------------------------------------------* 
* 定義追加
*-------------------------------------------------------*
*/
function fnInsertFDB2SERV($db2con,$WDUID,$WDNAME,$WDSERV,$WDCTFL,$WDCNFL){
   
    $rs = true;
    //構文
    $strSQL  = ' INSERT INTO DB2WDEF ';
    $strSQL .= ' ( ';
    $strSQL .= ' WDUID, ';
    $strSQL .= ' WDNAME, ';
    $strSQL .= ' WDSERV, ';
    $strSQL .= ' WDCTFL, ';
    $strSQL .= ' WDCNFL ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ';
    $strSQL .= ' (?,?,?,?,?) ';
   
    $params = array(
       $WDUID,
       $WDNAME,
       $WDSERV,
       $WDCTFL,
       $WDCNFL
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}

function fnUpdateFDB2SERV($db2con,$WDUID,$WDNAME,$WDSERV,$WDCTFL,$WDCNFL){

    $rs = true;

    //構文
    $strSQL  = ' UPDATE DB2WDEF ';
    $strSQL .= ' SET ';
    $strSQL .= ' WDSERV = ? ,';
    $strSQL .= ' WDCTFL = ? ,';
    $strSQL .= ' WDCNFL = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= ' WDUID = ? ';
    $strSQL .= ' AND WDNAME = ? ';
   
    $params = array($WDSERV,$WDCTFL,$WDCNFL,$WDUID,$WDNAME);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}
