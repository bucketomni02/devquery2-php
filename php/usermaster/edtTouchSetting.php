<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WUSIZE = $_POST['WUSIZE'];
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';

/*
*-------------------------------------------------------* 
* ユーザー更新処理
*-------------------------------------------------------*
*/

$WUUID = $_SESSION['PHPQUERYTOUCH']['user'][0]['WUUID'];

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ユーザー存在チェック
if($rtn === 0){
    $rs = fnCheckDB2WUSR($db2con,$WUUID);
    if($rs === false){
        $rtn = 1;
        $msg =showMsg('NOTEXIST_UPD',array('ユーザー'));// '更新対象のユーザーが存在しません。';
        $focus = 'WUUID';
    }
}

if($rtn === 0){
    $rs = fnUpdateDB2WUSR($db2con,$WUUID,$WUSIZE);
    if($rs === false){
        $rtn = 1;
        $msg =showMsg('FAIL_UPD');//'更新処理に失敗しました。管理者にお問い合わせください。';
    }else{
        $_SESSION['PHPQUERYTOUCH']['user'][0]['WUSIZE'] = $WUSIZE;
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));


/*
*-------------------------------------------------------* 
* ユーザー存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WUSR($db2con,$WUUID){

    $data = array();
    $rs = false;

    $strSQL  = ' SELECT A.WUUID ';
    $strSQL .= ' FROM DB2WUSR AS A ' ;
    $strSQL .= ' WHERE A.WUUID = ? ' ;

    $params = array(
        $WUUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = false;
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = false;
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) > 0){
                $rs = true;
            }
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* ユーザー更新
*-------------------------------------------------------*
*/

function fnUpdateDB2WUSR($db2con,$WUUID,$WUSIZE){

    $rs = true;

    //構文
    try {

        $strSQL  = ' UPDATE DB2WUSR ';
        $strSQL .= ' SET ';
        $strSQL .= ' WUSIZE = ? ';
        $strSQL .= ' WHERE ';
        $strSQL .= ' WUUID = ? ' ;

        $params = array(
            $WUSIZE,
            $WUUID
        );

        $stmt = db2_prepare($db2con,$strSQL);
        db2_execute($stmt,$params);

    } catch(Exception $e) {
        $rs = false;
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* ユーザー追加
*-------------------------------------------------------*
*/

function fnInsertDB2WUSR($db2con,$WUUID,$WUPSWD,$WUUNAM,$WUAUTH){


    $rs = true;

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    db2_exec($db2con,$sql);

    //構文
    try {

        $strSQL  = ' INSERT INTO DB2WUSR ';
        $strSQL .= ' ( ';
        $strSQL .= ' WUUID, ';
        $strSQL .= ' WUPSWE, ';
        $strSQL .= ' WUUNAM, ';
        $strSQL .= ' WUAUTH ';
        $strSQL .= ' ) ';
        $strSQL .= ' VALUES ' ;
        $strSQL .= ' (?,CAST(encrypt(CAST(? AS VARCHAR(10))) AS CHAR(32) FOR BIT DATA),?,?) ';

        $params = array(
            $WUUID,
            $WUPSWD,
            $WUUNAM,
            $WUAUTH
        );

        $stmt = db2_prepare($db2con,$strSQL);
        db2_execute($stmt,$params);

    } catch(Exception $e) {
        $rs = false;
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* DB2WDEFに追加
*-------------------------------------------------------*
*/

function fnInsertDB2WDEF($db2con,$WDUID,$WDNAME){


    $rs = true;

    //構文
    try {

        $strSQL  = ' INSERT INTO DB2WDEF ';
        $strSQL .= ' ( ';
        $strSQL .= ' WDUID, ';
        $strSQL .= ' WDNAME, ';
        $strSQL .= ' WDDWNL, ';
        $strSQL .= ' WDDWN2 ';
        $strSQL .= ' ) ';
        $strSQL .= ' VALUES ' ;
        $strSQL .= ' (?,?,?,?) ';

        $params = array(
            $WDUID,
            $WDNAME,
            '1',
            '1'
        );

        $stmt = db2_prepare($db2con,$strSQL);
        db2_execute($stmt,$params);

    } catch(Exception $e) {
        $rs = false;
    }

    return $rs;

}

/*
*-------------------------------------------------------* 
* FDB2CSV1全件取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con){
    $rs = array();

    try {

        $strSQL  = ' SELECT * ';
        $strSQL .= ' FROM FDB2CSV1 as A ' ;

        $params = array();

        $stmt = db2_prepare($db2con,$strSQL);
        db2_execute($stmt,$params);

        if($stmt){
            while($row = db2_fetch_assoc($stmt)){
                $rs[] = $row;
            }
        }else{
            $rs = false;
        }
    } catch(Exception $e) {
        $rs = 'ERROR:'.$e->getMessage();
    }

    return $rs;

}