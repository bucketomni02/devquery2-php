<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
$AMNAME =  '';
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
$rtn = 0;
$rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
if($rs['result'] !== true){
    $rtn = 2;
    $msg = showMsg($rs['result'],array('ユーザー'));
}
$data = array();
if($rtn === 0){
    $rs = cmSelDB2AMST($db2con,$AMNAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('権限ID'));
    }else{
         $data = umEx($rs['data'],true);
    }
}

$rtnAry = array(
    'DATA' => $data,
    'RTN' =>$rtn,
    'MSG' =>$msg
);
echo json_encode($rtnAry);

/*
*-------------------------------------------------------* 
* マスター一覧取得(一行)
*-------------------------------------------------------*
*/

function fnSelDB2AGRT($db2con,$AGNAME){

    $data = array();
    $params = array($AGNAME);
    $rs = true;
        $strSQL  = ' SELECT ';
        $strSQL .= ' A.AGNAME,A.AGMNID,A.AGFLAG  ';
        $strSQL .= ' FROM DB2AGRT AS A ';
        $strSQL .= ' WHERE A.AGNAME = ? ';
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;
}