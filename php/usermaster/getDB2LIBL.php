<?php
/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
$LIBLNM =  '';
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
$rtn = 0;
$rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
if($rs['result'] !== true){
    $rtn = 2;
    $msg = showMsg($rs['result'],array('ユーザー'));
}
$data = array();
if($rtn === 0){
    $rs = cmSelDB2LIBL($db2con,$LIBLNM);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('ライブラリーID'));
    }else{
         $data = umEx($rs['data'],true);
    }
}

$rtnAry = array(
    'DATA' => $data,
    'RTN' =>$rtn,
    'MSG' =>$msg
);
echo json_encode($rtnAry);
