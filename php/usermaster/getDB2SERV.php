<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
include_once("chkComUserMaster.php");
//$licenseType = 'FREE';

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$WUSERV = (isset($_POST['WUSERV']))?$_POST['WUSERV']:'';
$WDUID = (isset($_POST['WDUID']))?$_POST['WDUID']:'';
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmlspecialcharsをデコード
$WUSERV = cmHscDe($WUSERV);

$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID']))?$_SESSION['PHPQUERY']['user'][0]['WUUID']:'';
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$WUUID);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'6',$userData[0]['WUSAUT']);//'6' => ダウロード権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('標準ダウンロード先の権限'));
            }
        }
    }
}

if($proc === 'EDIT'){
    if($rtn === 0){
        $rs = fnSelDB2SERV($db2con,$WDUID);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('標準ダウンロード先'));
            
        }else{
            $data = $rs['data'];
        }
    }
    $htmlFlg = false;
}else{
    if($rtn === 0){
        $rs = fnGetAllCount($db2con,$WDUID,$WUSERV);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
            
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetDB2SERV($db2con,$WDUID,$WUSERV,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
             
        }else{
            $data = $rs['data'];
        }
    }
}

cmDb2Close($db2con);


/**return**/
$rtn = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'aaData' => umEx($data,$htmlFlg),
    'iTotalRecords' => $allcount,
    '_SESSION'=>$_SESSION,
    'WUUID' => $WDUID,
     'WUSERV' => $WUSERV
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2SERV($db2con,$WDUID,$WUSERV,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();
        $strSQL  = ' SELECT A.* ';
        $strSQL .= ' FROM ( SELECT B.WUSERV ROWNUMBER() OVER( ';

        if($sort !== ''){
            $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= ' ORDER BY B.WUUID ASC ';
        }

        $strSQL .= ' ) as rownum ';

        $strSQL .= ' FROM DB2WUSR as B ';

        $strSQL .= ' WHERE WUUID <> \'\' ';
        if($WUSERV != ''){
            $strSQL .= ' AND WUUID like ? ';
            array_push($params,'%'.$WUUID.'%');
        }

        
        
        $strSQL .= ' ) as A ';
        
        //抽出範囲指定
        if(($start != '')&&($length != '')){
            $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
            array_push($params,$start + 1);
            array_push($params,$start + $length);
        }
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$WDUID,$WUSERV){
    $data = array();

    $strSQL  = ' SELECT count(A.WUUID) as COUNT ';
    $strSQL .= ' FROM  DB2WUSR as A ' ;
    $strSQL .= ' WHERE WUUID <> \'\' ';

    $params = array();

    if($WUSERV != ''){
        $strSQL .= ' AND WUUID like ? ';
        array_push($params,'%'.$WUSERV.'%');
    }

    

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* ユーザーマスター取得(一行)
*-------------------------------------------------------*
*/

function fnSelDB2SERV($db2con,$WDUID){

    $data = array();

    $params = array($WDUID);
    $rs = true;


        $strSQL  = ' SELECT ';
        $strSQL .= ' A.WUUID,A.WUSERV,A.WUCTFL,A.WUCNFL ';
        $strSQL .= ' FROM DB2WUSR as A ';
        $strSQL .= ' WHERE WUUID = ? ';

        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
               
                    $data = array('result' => true,'data' => $data);
               
            }
        }
    return $data;
}