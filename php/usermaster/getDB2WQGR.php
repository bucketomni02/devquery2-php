<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/
//WQGID => WUUID
$WUUID = $_POST['WUUID'];
//echo "WUUID =>".$WUUID;
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$wqunam = '';
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$WUUID = cmHscDe($WUUID);

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('グループユーザー権限の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'5',$userData[0]['WUSAUT']);//'2' => groupMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('グループユーザー権限の権限'));
            }
        }
    }
}
//ユーザ存在チェック 
if($rtn === 0){
    $rs = fnCheckDB2WUSR($db2con,$WUUID);//fnCheckDB2WQGR
    if($rs === 'NOTEXIST_GET'){
        $rtn = 3;
        $msg = showMsg($rs,array('グループ'));
    }else if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs,array('グループ'));
    }
}
//count WUUID
if($rtn === 0){
    $res = fnGetAllCount($db2con);
    if($res['result'] === true){
        $allcount = $res['data'];
        $res = fnGetDB2WQGR($db2con,$start,$length,$sort,$sortDir,$WUUID,$WUAUTH);//fnGetDB2WUSR
        if($res['result'] === true){
            $data = $res['data'];
        }else{
            $rtn = 1;
            $msg = showMsg($res['result'],array('ユーザー'));
        }
    }else{
        $rtn = 1;
        $msg = showMsg($res['result']);
    }
}

//グループ名取得
if($rtn === 0){
    $res = fnGetWQUNAM($db2con,$WUUID);
    if($res['result'] === true){
 //       if(count($res['data'])>0){
            $wuunam = $res['data'][0]['WUUNAM'];
 /*       }else{
            //存在しないグループのチェック
                $rtn = 1;
                $msg = showMsg('NOTEXIST_GET',array('グループＩＤ'));
        }*/
    }else{
        $rtn = 1;
        $msg = showMsg($res['result'],array('グループＩＤ'));
    }
}

cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'WUUNAM' => cmHsc($wuunam),
    'RTN' => $rtn,
    'MSG' => $msg,
    'licenseType' =>$licenseType
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* DB2WUSR取得
*-------------------------------------------------------*
*/

function fnGetDB2WQGR($db2con,$start = '',$length = '',$sort = '',$sortDir = '',$WUUID,$WUAUTH){//fnGetDB2WUSR

    $data = array();
	
    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.WQGID ASC ';
    }

    $strSQL .= ' ) as rownum ';
    $strSQL .= ' from( ';
    $strSQL .= ' select C.*,case C.wuuid when \'\' then \'0\' else \'1\' end as DEF ';
    $strSQL .= ' from ('; 
    $strSQL .= ' SELECT D.wqgid,wqunam,ifnull(WUUID,\'\') as wuuid '; 
    $strSQL .= ' FROM DB2WQGR as D LEFT OUTER JOIN DB2WUGR as E ON D.WQGID = E.WUGID AND WUUID = ? '; 
    $strSQL .= ' ) as C ';

    $strSQL .= ' ) as B ';

    $strSQL .= ' WHERE WQGID <> \'\' ';
    if($WUAUTH === '3' || $WUAUTH === '4'){
        $strSQL .= '    AND WQGID IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            WUGID ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WUGR ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WUUID = ? ';
        $strSQL .= '    ) ';
        array_push($params,$WUUID); 
    }

    
    $strSQL .= ' ) as A ';
	array_push($params,$WUUID);

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
	//error_log("MSM1 GROUP FROM USER => ".$strSQL.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
             $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
               if(count($data)=== 0){
                    $data = array('result' => 'NOTEXIST_GET');
                }else{
                    $data = array('result' => true,'data' => $data);
                }
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con){
    $data = array();

    $strSQL  = ' SELECT count(A.WQGID) as COUNT ';
    $strSQL .= ' FROM DB2WQGR as A ' ;

    $params = array();

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }

    return $data;

}

/*
*-------------------------------------------------------* 
* グループ名取得
*-------------------------------------------------------*
*/

function fnGetWQUNAM($db2con,$id){

    $data = array();

    $strSQL  = ' SELECT A.WUUNAM ';
    $strSQL .= ' FROM DB2WUSR AS A ' ;//DB2WQGR
    $strSQL .= ' WHERE WUUID = ? ';

    $params = array($id);

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data)>0){
                $data = array('result' => true,'data' => $data);
            }else{
                $data = array('result' => 'NOTEXIST_GET');
            }
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* グループ存在チェック いたらtrue
*-------------------------------------------------------*
*/

function fnCheckDB2WUSR($db2con,$WUUID){//fnCheckDB2WQGR

    $data = array();
    $rs = true;

    $strSQL  = ' SELECT A.WUUID ';//WQGID
    $strSQL .= ' FROM DB2WUSR  AS A ' ;//DB2WQGR
    $strSQL .= ' WHERE A.WUUID = ? ' ;//WQGID

    $params = array(
        $WUUID
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}
