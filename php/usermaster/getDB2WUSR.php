<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc = (isset($_POST['proc'])?$_POST['proc']:'');
$id = $_POST['id'];
$name = $_POST['name'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];
$session = array();
$session['id']  = $id;
$session['name']  = $name;
$session['start']  = $start;
$session['length']   = $length;
$session['sort']  = $sort;
$session['sortDir']    = $sortDir;

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
$kengenflg = (isset($_POST['KENGENFLAG']) ? $_POST['KENGENFLAG'] : '');
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmlspecialcharsをデコード
$id = cmHscDe($id);
$name = cmHscDe($name);
$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID']))?$_SESSION['PHPQUERY']['user'][0]['WUUID']:'';
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$usrKengenFlg = '1';
$downloadKengenFlg = '1';
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $usrKengenFlg = '';
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'1',$userData[0]['WUSAUT']);//'1' => userMaster
            if($rs['result'] !== true){
                $usrKengenFlg = '';
            }
            if($rtn === 0){
                if($kengenflg !== "1"){
                    $res = cmChkKenGen($db2con,'6',$userData[0]['WUSAUT']);//'6' => ダウロード権限
                    if($res['result'] !== true){
                       $downloadKengenFlg = '';

                    }
                   if($usrKengenFlg === '' && $downloadKengenFlg === ''){
                        $rtn = 2;
						$_SESSION['PHPQUERY']['kengenFlg'] = false;
                        $msg =  showMsg($rs['result'],array('ユーザ作成の権限'));
                   }
                }
            }
            if($rtn === 0){
                if($kengenflg === "1"){
                    if($usrKengenFlg === ''){
                        $rtn = 3;
				        $_SESSION['PHPQUERY']['kengenFlg'] = false;
                        $msg =  showMsg($rs['result'],array('ユーザ作成の権限'));
                    }
                }
            }
        }
    }
}

if($proc === 'EDIT'){
    if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
        $rtn = 1;
        $msg = '指定しはユーザーに編集権限がありません。';
    }
    if($rtn === 0){
        $rs = fnSelDB2WUSR($db2con,$id);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }else{
            $data = $rs['data'];
        }
    }
    $htmlFlg = false;
}else{
    // 管理者又はユーザー管理権限の場合
    if($rtn === 0){
        $rs = fnGetAllCount($db2con,$userData[0]['WUAUTH'],$id,$name);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $allcount = $rs['data'];
        }
    }
    if($rtn === 0){
        $rs = fnGetDB2WUSR($db2con,$userData[0]['WUAUTH'],$id,$name,$start,$length,$sort,$sortDir);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result']);
        }else{
            $data = $rs['data'];
        }
    }
}

cmDb2Close($db2con);


/**return**/
$rtn = array(
    'RTN'               => $rtn,
    'MSG'               => $msg,
    'iTotalRecords'     => $allcount,
    'aaData'            => umEx($data,$htmlFlg),
    '_SESSION'          => $_SESSION,
    'WUUID'             => $WUUID,
    'usrKengenFlg'      => $usrKengenFlg,
    'downloadKengenFlg' => $downloadKengenFlg,
    'Flag'              => $kengenflg,
    'session'           => $session,
    'USROTHAUTFLG'      => USROTHAUTFLG,
    'kenGenFlg'         => $_SESSION['PHPQUERY']['kengenFlg']
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2WUSR($db2con,$wuauth,$id,$name,$start = '',$length = '',$sort = '',$sortDir = ''){

    $data = array();

    $params = array();
    $rs = true;

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';
    
    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SEL';
        }
    }

    if($rs === true){
        $strSQL  = ' SELECT A.* ';
        $strSQL .= ' FROM ( SELECT B.WUUID,B.WUUNAM,B.WUAUTH,B.WUDWNL,B.WUDWN2,DECRYPT_CHAR(B.WUPSWE) AS WUPSWE , B.WUSERV, B.WULIBL,B.WUUQFG,B.WUUBFG,B.WUAID, B.WUADO,B.WUAFLG ,ROWNUMBER() OVER( ';

        if($sort !== ''){
            $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
        }else{
            $strSQL .= ' ORDER BY B.WUUID ASC ';
        }

        $strSQL .= ' ) as rownum ';
//        $strSQL .= ' FROM DB2WUSR as B ';
        $strSQL .= ' FROM DB2WUSR as B ';

        $strSQL .= ' WHERE WUUID <> \'\' ';
        if($wuauth === '3'){
            $strSQL .= ' AND WUUID IN ';
            $strSQL .= '    ( ';
            $strSQL .= '    SELECT ';
            $strSQL .= '        DISTINCT (WUUID) ';
            $strSQL .= '    FROM ';
            $strSQL .= '        DB2WUGR ';
            $strSQL .= '    WHERE ';
            $strSQL .= '        WUGID IN (SELECT ';
            $strSQL .= '                        WUGID ';
            $strSQL .= '                    FROM ';
            $strSQL .= '                        DB2WUGR ';
            $strSQL .= '                    WHERE ';
            $strSQL .= '                        WUUID = ? ';
            $strSQL .= '                    ) ';
            $strSQL .= '    ) ';
            array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        }else if($wuauth === '4'){
            $strSQL .= ' AND WUUID = ? ';
            array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        }
        /*if($id != ''){
            $strSQL .= ' AND (WUUID like ? OR WUAID like ? )';//MSM add for WUAID search 20180717
            array_push($params,'%'.$id.'%');
            array_push($params,'%'.$id.'%');
        }*/
		//TTA add for ID only search 20181112
        if($id != ''){
            $strSQL .= ' AND WUUID like ? ';
            array_push($params,'%'.$id.'%');
        }

        if($name != ''){
            $strSQL .= ' AND WUUNAM like ? ';
            array_push($params,'%'.$name.'%');
        }
        
        $strSQL .= ' ) as A ';
        
        //抽出範囲指定
        if(($start != '')&&($length != '')){
            $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
            array_push($params,$start + 1);
            array_push($params,$start + $length);
        }
        $stmt = db2_prepare($db2con,$strSQL);
        e_log('ユーザー取得：'.$strSQL.print_r($params,true));
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);

            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    }else{
        $data = array('result' => $rs);
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$wuauth,$id = '',$name = ''){
    $data = array();
    $params = array();
    $strSQL  = ' SELECT count(*) as COUNT ';
    $strSQL .= ' FROM  DB2WUSR as A ' ;
    $strSQL .= ' WHERE WUUID <> \'\' ';
    if($wuauth === '3'){
        $strSQL .= ' AND WUUID IN ';
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT ';
        $strSQL .= '        DISTINCT (WUUID) ';
        $strSQL .= '    FROM ';
        $strSQL .= '        DB2WUGR ';
        $strSQL .= '    WHERE ';
        $strSQL .= '        WUGID IN (SELECT ';
        $strSQL .= '                        WUGID ';
        $strSQL .= '                    FROM ';
        $strSQL .= '                        DB2WUGR ';
        $strSQL .= '                    WHERE ';
        $strSQL .= '                        WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= '    ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($wuauth === '4'){
        $strSQL .= ' AND WUUID = ? ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    if($id != ''){
        $strSQL .= ' AND WUUID like ? ';
        array_push($params,'%'.$id.'%');
    }

    if($name != ''){
        $strSQL .= ' AND WUUNAM like ? ';
        array_push($params,'%'.$name.'%');
    }

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
        e_log('ユーザカウント取得エラー：'.$strSQL.'param:'.print_r($params,true).db2_stmt_errormsg());
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* ユーザーマスター取得(一行)
*-------------------------------------------------------*
*/

function fnSelDB2WUSR($db2con,$id){

    $data = array();

    $params = array();
    $rs = true;

    $sql = 'SET ENCRYPTION PASSWORD = \''.RDB_KEY.'\'';

    $stmt = db2_prepare($db2con,$sql);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_SEL';
        }
    }

    if($rs === true){
        $strSQL = '';
        $strSQL .= ' SELECT ';
        $strSQL .= '     A.WUUID, ';
        $strSQL .= '     A.WUUNAM, ';
        $strSQL .= '     A.WUAUTH, ';
        $strSQL .= '     A.WUDWNL, ';
        $strSQL .= '     A.WUDWN2, ';
        $strSQL .= '     DECRYPT_CHAR(A.WUPSWE) AS WUPSWE, ';
        $strSQL .= '     A.WUSAUT, ';
        $strSQL .= '     A.WULIBL, ';
        $strSQL .= '     A.WUUQFG, ';
        $strSQL .= '     A.WUUBFG, ';
        $strSQL .= '     A.WUAID, ';//MSM add WUAID,WUADO,WUAFLG
        $strSQL .= '     A.WUADO, ';
        $strSQL .= '     A.WUAFLG ';
        $strSQL .= ' FROM ';
        $strSQL .= '     DB2WUSR as A ';
        $strSQL .= ' WHERE ';
        $strSQL .= '     WUUID = ? ';
        array_push($params,$id);
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                if(count($data)=== 0){
                    $data = array('result' => 'NOTEXIST_GET');
                }else{
                    $data = array('result' => true,'data' => $data);
                }
            }
        }
    }else{
        $data = array('result' => $rs);
    }
    return $data;

}
