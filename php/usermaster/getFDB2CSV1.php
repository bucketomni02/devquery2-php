<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
//$licenseType = 'FREE';
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$WUUID = $_POST['WUUID'];
$start = $_POST['start'];
$length = $_POST['length'];
$sort = $_POST['sort'];
$sortDir = $_POST['sortDir'];

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$wuunam = '';
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmldecode
$WUUID = cmHscDe($WUUID);

$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'6',$userData[0]['WUSAUT']);//'6' =>  ダウンロード権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ダウンロード権限の権限'));
            }
        }
    }
}
if($rtn === 0){
    if($userData[0]['WUAUTH'] === '4'){
        if(cmMer($WUUID) !== cmMer($_SESSION['PHPQUERY']['user'][0]['WUUID'])){
            $msg = showMsg('指定したユーザは作成者ユーザーではないと取得できません。');
            $rtn = 3;
        }
    }
}
if($rtn === 0){
    //ユーザー名取得
    $rs = fnGetWUUNAM($db2con,$WUUID,$userData[0]['WUAUTH']);
    if($rs['result'] === 'NOTEXIST_GET'){
        $msg = showMsg($rs['result'],array('ユーザー'));
        $rtn = 3;
    }else if($rs['result'] === 'NOGP_GET'){
        $msg = showMsg('指定したユーザはグループユーザーではないと取得できません。');
        $rtn = 3;
    }else if($rs['result'] !== true){
        $msg = showMsg($rs['result'],array('ユーザー'));
        $rtn = 1;
    }else{
        $wuunam = $rs['data'][0]['WUUNAM'];
    }
}
if($rtn === 0){
    $rs = fnGetAllCount($db2con,$userData[0]['WUAUTH'],$licenseSql);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $allcount = $rs['data'];
    }
}
if($rtn === 0){
    $rs = fnGetFDB2CSV1($db2con,$userData[0]['WUAUTH'],$start,$length,$sort,$sortDir,$WUUID,$licenseSql);
    if($rs['result'] !== true){
        $msg = showMsg($rs['result']);
        $rtn = 1;
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'iTotalRecords' => $allcount,
    'aaData' => umEx($data,true),
    'DB2WDEF' => $DB2WDEF,
    'WUUNAM'  => cmHsc($wuunam),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* FDB2CSV1取得
*-------------------------------------------------------*
*/

function fnGetFDB2CSV1($db2con,$wuaut,$start = '',$length = '',$sort = '',$sortDir = '',$WDUID,$licenseSql){

    $data = array();

    $params = array();

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM ( SELECT B.* , ROWNUMBER() OVER( ';

    if($sort !== ''){
        $strSQL .= ' ORDER BY B.'.$sort.' '.$sortDir.' ';
    }else{
        $strSQL .= ' ORDER BY B.D1NAME ASC ';
    }

    $strSQL .= ' ) as rownum ';

    $strSQL .= ' FROM ';
    $strSQL .= ' ( ';
    $strSQL .= ' select C.*,case C.wduid when \'\' then \'0\' else \'1\' end as DEF from ';
    $strSQL .= ' ( ';
    $strSQL .= ' SELECT D1NAME,D1TEXT,ifnull(WDUID,\'\') as WDUID,WDNAME,WDDWNL,WDDWN2,WDSERV';
    $strSQL .= ' FROM ';
    if($wuaut === '3'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '   ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($wuaut === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';     
        $strSQL .= '            )';
        $strSQL .= '    )  B ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '   FDB2CSV1   AS B ';
    }
    $strSQL .= ' LEFT OUTER JOIN DB2WDEF ON D1NAME = WDNAME AND WDUID = ? '; 
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' WHERE B.D1CFLG = \'\' ';
    }
    $strSQL .= ' ) as C ';

    $strSQL .= ' ) as B ';

    $strSQL .= ' WHERE D1NAME <> \'\' ';
    
    $strSQL .= ' ) as A ';

    array_push($params,$WDUID);

    //抽出範囲指定
    if(($start != '')&&($length != '')){
        $strSQL .= ' WHERE A.rownum BETWEEN ? AND ? ';
        array_push($params,$start + 1);
        array_push($params,$start + $length);
    }
    e_log('FDB2CSV1データ取得：'.$strSQL,'parameter:'.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}

/*
*-------------------------------------------------------* 
* 全件カウント取得
*-------------------------------------------------------*
*/

function fnGetAllCount($db2con,$wuauth,$licenseSql){
    $data = array();
    $params = array();

    $strSQL  = ' SELECT count(D1NAME) as COUNT ';
    $strSQL .= ' FROM FDB2CSV1 AS B';
    if($wuauth === '3' ){
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '   ( ';
        $strSQL .= '        SELECT DISTINCT ';
        $strSQL .= '            WGNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2WGDF ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            WGGID IN (SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                        ) ';
        $strSQL .= '    )';
        $strSQL .= '    UNION  ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    ) ';         
        $strSQL .= '            )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($wuauth === '4'){
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';      
        $strSQL .= '            )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    } else{
        $strSQL.=' WHERE D1NAME<> \'\' ';
    }
    //ライセンスのsqlクエリー実行権限はしない時
    if(!$licenseSql){
        $strSQL .= ' AND D1CFLG = \'\' ';
    }
    e_log('FDB2CSV1カウント取得：'.$strSQL,'parameter:'.print_r($params,true));
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row['COUNT'];
            }
            $data = array('result' => true,'data' => $data[0]);
        }
    }
    return $data;

}

/*
*-------------------------------------------------------* 
* ユーザー名取得
*-------------------------------------------------------*
*/

function fnGetWUUNAM($db2con,$id,$wuauth){

    $data = array();
    $params = array();

    $strSQL  = ' SELECT A.WUUNAM ';
    if($wuauth === '3'){
        $strSQL  .= ' , B.WUUID ';
    }
    $strSQL .= ' FROM DB2WUSR AS A';
    if($wuauth === '3'){
        $strSQL .= ' LEFT JOIN ( ';
        $strSQL .= '    SELECT DISTINCT ';
        $strSQL .= '        WUUID ';
        $strSQL .= '    FROM ';
        $strSQL .= '        DB2WUGR ';
        $strSQL .= '    WHERE ';
        $strSQL .= '        WUGID IN (SELECT ';
        $strSQL .= '                        WUGID ';
        $strSQL .= '                    FROM ';
        $strSQL .= '                        DB2WUGR ';
        $strSQL .= '                    WHERE ';
        $strSQL .= '                        WUUID = ? ';
        $strSQL .= '                    ) ';
        $strSQL .= ' ) B ';
        $strSQL .= ' ON A.WUUID = B.WUUID ';
        array_push($params, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= ' WHERE A.WUUID = ? ' ;
    array_push($params, $id);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        e_log('user query shutoku :'.$strSQL.db2_stmt_errormsg());
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
          e_log('ユーザー名に取得：'.print_r($data,true));
          if(count($data)=== 0){
               $data = array('result' => 'NOTEXIST_GET');
            }else{
                if($wuauth === '3'){
                    if(cmMer($data[0]['WUUID']) === ''){
                        $data = array('result' => 'NOGP_GET');
                    }else{
                        $data = array('result' => true,'data' => $data);
                    }
                }else{
                    $data = array('result' => true,'data' => $data);
                }
            }
        }
    }
    return $data;

}