<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
include_once("chkComUserMaster.php");
//$licenseType = 'FREE';

/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$proc       = (isset($_POST['proc'])?$_POST['proc']:'');
$WDSERV     = (isset($_POST['WDSERV']))?$_POST['WDSERV']:'';
$WDNAME     = (isset($_POST['D1NAME']))?$_POST['D1NAME']:'';
$WDUID      = (isset($_POST['WDUID']))?$_POST['WDUID']:'';
$start      = $_POST['start'];
$length     = $_POST['length'];
$sort       = $_POST['sort'];
$sortDir    = $_POST['sortDir'];


/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$allcount = 0;
$data = array();
$rtn = 0;
$msg = '';
$htmlFlg = true;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

//htmlspecialcharsをデコード
$WDSERV = cmHscDe($WDSERV);

$WUUID = (isset($_SESSION['PHPQUERY']['user'][0]['WUUID']))?$_SESSION['PHPQUERY']['user'][0]['WUUID']:'';
$csv_d = array();

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$WUUID);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'6',$userData[0]['WUSAUT']);//'6' => ダウロード権限
                   
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ダウンロード先の権限'));
            }
        }
    }
}
if($rtn === 0){
    //ユーザが存在チェック
    if($rtn === 0){
        $rs = cmCheckUser($db2con,$WDUID);
        if($rs['result'] === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }else if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }
    }
}
if($rtn === 0){
    $chkQry = cmChkQuery($db2con,'',$WDNAME,'');
    if($chkQry['result'] !== true){
        $rtn = 1;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($rtn === 0){
    if($userData[0]['WUAUTH'] === '4'){
        if(cmMer($WDUID) !== $_SESSION['PHPQUERY']['user'][0]['WUUID'] ){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('ダウンロード権限の権限'));
        }
    }else if($userData[0]['WUAUTH'] === '3'){
        $rs = chkQryUser($db2con,$WDUID);
        if($rs === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('ダウンロード権限の権限'));
        }else if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }
    }
}
if($rtn === 0){
    if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4' ){
        $rs = chkVldQryUsr($db2con,$WDNAME,$userData[0]['WUAUTH']);
        if($rs === 'NOTEXIST_GET'){
            $rtn = 1;
            $msg = showMsg('指定したクエリーはこのユーザーにダウンロード権限がありません。');
        }else if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('クエリー'));
        }
    }
}
if($proc === 'EDIT'){
    if($rtn === 0){
        $rs = fnSelFDB2SERV($db2con,$WDUID,$WDNAME,$userData[0]['WUAUTH']);
        if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ダウンロード先'));
             
        }else{
            $data = $rs['data'];
        }
    }
    $htmlFlg = false;
}

cmDb2Close($db2con);


/**return**/
$rtn = array(
    'RTN'           => $rtn,
    'MSG'           => $msg,
    'aaData'        => umEx($data,$htmlFlg),
    'iTotalRecords' => $allcount,
    '_SESSION'      => $_SESSION,
    'D1NAME'        => $D1NAME,
    'WDSERV'        => $WDSERV
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ダウンロード先の情報取得
*-------------------------------------------------------*
*/

function fnSelFDB2SERV($db2con,$WDUID,$WDNAME,$WUAUTH){
    $data = array();
    $params = array($WDUID,$WDNAME);
    $rs = true;
        $strSQL  = ' SELECT ';
        $strSQL .= ' A.*  ';
        $strSQL .= ' FROM DB2WDEF as A ';
        $strSQL .= ' WHERE WDUID = ? ';
        $strSQL .= ' AND WDNAME = ? ';
        $stmt = db2_prepare($db2con,$strSQL);
        if($stmt === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            $r = db2_execute($stmt,$params);
            if($r === false){
                $data = array('result' => 'FAIL_SEL');
            }else{
                while($row = db2_fetch_assoc($stmt)){
                    $data[] = $row;
                }
                $data = array('result' => true,'data' => $data);
            }
        }
    return $data;
}
