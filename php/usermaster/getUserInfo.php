<?php
/*
*-------------------------------------------------------* 
* ライブラリー情報と権限情報
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
$AMNAME =  '';
$LIBLNM = '';
$COUQFG = '';
$userid = (isset($_POST['userid']) ? $_POST['userid'] : '');

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
//ログインユーザが削除されたかどうかチェック
$rtn = 0;
$msg = '';
$rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
if($rs['result'] !== true){
    $rtn = 2;
    $msg = showMsg($rs['result'],array('ユーザー'));
}
$KENGENINFO = array();
if($rtn === 0){
    $rs = cmSelDB2AMST($db2con,$AMNAME);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result'],array('権限ID'));
    }else{
         $KENGENINFO = umEx($rs['data'],true);
    }
}
$LIBRARYINFO = array();
if($rtn === 0){
    $rs = cmSelDB2LIBL($db2con,$LIBLNM);
    if($rs['result'] !== true && $rs['result'] !== 'NOTEXIST_GET'){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
         $LIBRARYINFO = umEx($rs['data'],true);
    }
}
$USERINFO = array();
if($rtn === 0){
   $rs = fnGetDB2WUSR($db2con,$userid);
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
         $USERINFO = umEx($rs['data'],true);
    }
}

//システムリスト
$queryMenuList   = cmGetDB2COF($db2con);
if ($queryMenuList['result'] === true) {
    $COUQFG = $queryMenuList['data'][0]['COUQFG'];
}
$rtnAry = array(
    'KENGENINFO' => $KENGENINFO,
    'LIBRARYINFO' => $LIBRARYINFO,
    'USERINFO' => $USERINFO,
    'COUQFG' => $COUQFG,
    'USROTHAUTFLG'  => USROTHAUTFLG,
    'RTN' =>$rtn,
    'MSG' =>$msg
);
echo json_encode($rtnAry);

/*
*-------------------------------------------------------* 
* 一般ユーザー取得
*-------------------------------------------------------*
*/

function fnGetDB2WUSR($db2con,$wuuid){

    $data = array();
    $rs = true;

    $strSQL = 'SELECT WUUID,WUUNAM FROM DB2WUSR WHERE WUUID <> ? AND WUAUTH <> ? ';
    $params = array($wuuid,0);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt,$params);

        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;
}