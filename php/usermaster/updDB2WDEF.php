<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("chkComUserMaster.php");
/*
*-------------------------------------------------------* 
* DataTableリクエスト
*-------------------------------------------------------*
*/

$allcsv     = $_POST['ALLCSV'];
$allexcel   = $_POST['ALLEXCEL'];
$WDUID      = $_POST['WDUID'];
$data       = json_decode($_POST['DATA'],true);
$rtn        = 0;
$msg        = '';

/*
*-------------------------------------------------------* 
* 権限更新処理
*-------------------------------------------------------*
*/

//htmldecode
$WDUID = cmHscDe($WDUID);

$checkRs = false;
$defRs = false;

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
e_log('LIBDATA'.print_r($data,true));
//オートコミットをOFFにする
if($rtn === 0){
    $res = db2_autocommit($db2con,DB2_AUTOCOMMIT_OFF);
    if($res === false){
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if($rtn === 0){
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}
//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'6',$userData[0]['WUSAUT']);//'6' =>  ダウンロード権限
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('ダウンロード権限の権限'));
            }
        }
    }
}
if($rtn === 0){
    //ユーザが存在チェック
    if($rtn === 0){
        $rs = cmCheckUser($db2con,$WDUID);
        if($rs['result'] === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }else if($rs['result'] !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }
    }
}
if($rtn === 0){
    if($userData[0]['WUAUTH'] === '4'){
        if(cmMer($WDUID) !== $_SESSION['PHPQUERY']['user'][0]['WUUID'] ){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('ダウンロード権限の権限'));
        }
    }else if($userData[0]['WUAUTH'] === '3'){
        $rs = chkQryUser($db2con,$WDUID);
        if($rs === 'NOTEXIST_GET'){
            $rtn = 3;
            $msg =  showMsg('NOTEXIST',array('ダウンロード権限の権限'));
        }else if($rs !== true){
            $rtn = 1;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }
    }
}
if($allcsv === "2"){
//CSV一括選択があった場合、最初に全てを更新
    if($rtn === 0){
        //FDB2CSV1にあってDB2WDEFに存在しない行をインサート)
        $rs = fnInsertNoDB2WDEF($db2con,$WDUID,$WUAUTH);
        if($rs !== true){
            $msg = showMsg($rs);
            $rtn = 1;
        }
    }
    if($rtn === 0){
        //すべて１に更新
        $rs = fnUpdateAllCsv($db2con,$WDUID,'1',$WUAUTH);
        if($rs !== true){
            $msg = showMsg($rs);
            $rtn = 1;
        }
    }
    
}else if($allcsv === "3"){
    if($rtn === 0){
        //すべて''に更新
        $rs = fnUpdateAllCsv($db2con,$WDUID,'',$WUAUTH);
        if($rs !== true){
            $msg = showMsg($rs);
            $rtn = 1;
        }
    }
}
//Excel一括選択があった場合、最初に全てを更新
if($allexcel === "2"){
    if($rtn === 0){
        //FDB2CSV1にあってDB2WDEFに存在しない行をインサート
        $rs = fnInsertNoDB2WDEF($db2con,$WDUID,$WUAUTH);
        if($rs !== true){
            $msg = showMsg($rs);
            $rtn = 1;
        }
    }
    if($rtn === 0){
        //すべて１に更新
        $rs = fnUpdateAllExcel($db2con,$WDUID,'1',$WUAUTH);
        if($rs !== true){
            $msg = showMsg($rs);
            $rtn = 1;
        }
    }
}else if($allexcel === "3"){
    if($rtn === 0){
        //すべて''に更新
        $rs = fnUpdateAllExcel($db2con,$WDUID,'',$WUAUTH);
        if($rs !== true){
            $msg = showMsg($rs);
            $rtn = 1;
        }
    }    
}
if($rtn === 0){
    foreach($data as $value){
        $D1NAME = cmHscDe($value['D1NAME']);
        /** クエリー存在チェック**/
        $chkQry = array();
        $chkQry = cmChkQuery($db2con,'',$D1NAME,'');
        if($chkQry['result'] !== true){
            $rtn = 1;
            $msg = showMsg($chkQry['result'],array('クエリー'));
            break;
        }
        if($rtn === 0){
            if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4' ){
                $rs = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
                if($rs === 'NOTEXIST_GET'){
                    $rtn = 1;
                    $msg = showMsg('指定したクエリーはこのユーザーにダウンロード権限がありません。');
                    break;
                }else if($rs !== true){
                    $rtn = 1;
                    $msg = showMsg($rs['result'],array('クエリー'));
                    break;
                }
            }
        }
        if($rtn === 0){
            $checkRs = fnCheckDB2WDEF($db2con,$WDUID,$D1NAME);
            if($checkRs === true){
                $defRs = fnUpdateDB2WDEF($db2con,$WDUID,$D1NAME,$value['WDDWNL'],$value['WDDWN2']);
                if($defRs !== true){
                    $msg = showMsg($defRs);
                    $rtn = 1;
                }
            }else if($checkRs === 'NOTEXIST_GET') {
                $defRs = fnInsertDB2WDEF($db2con,$WDUID,$D1NAME,$value['WDDWNL'],$value['WDDWN2']);
                if($defRs !== true){
                    $msg = showMsg($defRs);
                    $rtn = 1;
                }
            }else{
                $msg = showMsg($checkRs);
                $rtn = 1;
            }
        }
    }
}
if($rtn !== 0){
    db2_rollback($db2con);
}else{
    db2_commit($db2con);
}
cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'checkRs' => $checkRs,
    'defRs' => $defRs,
    'DATA' => $data,
    'ALLCSV' => $allcsv,
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* ユーザー情報取得
*-------------------------------------------------------*
*/

function fnCheckDB2WDEF($db2con,$WDUID,$WDNAME){

    $rs = true;

    $strSQL  = ' SELECT ';
    $strSQL .= '    A.WDUID ';
    $strSQL .= ' FROM ';
    $strSQL .= '    DB2WDEF AS A ' ;
    $strSQL .= ' WHERE A.WDUID = ? ' ;
    $strSQL .= ' AND A.WDNAME = ? ' ;

    $params = array(
        $WDUID,
        $WDNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* 定義追加
*-------------------------------------------------------*
*/

function fnInsertDB2WDEF($db2con,$WDUID,$WDNAME,$WDDWNL,$WDDWN2){

    $rs = true;
    //構文
    $strSQL  = ' INSERT INTO DB2WDEF ';
    $strSQL .= ' ( ';
    $strSQL .= '    WDUID, ';
    $strSQL .= '    WDNAME, ';
    $strSQL .= '    WDDWNL, ';
    $strSQL .= '    WDDWN2 ';
    $strSQL .= ' ) ';
    $strSQL .= ' VALUES ' ;
    $strSQL .= ' (?,?,?,?) ';

    $params = array(
        $WDUID,
        $WDNAME,
        $WDDWNL,
        $WDDWN2
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;

}

/*
*-------------------------------------------------------* 
* 定義更新
*-------------------------------------------------------*
*/

function fnUpdateDB2WDEF($db2con,$WDUID,$WDNAME,$WDDWNL,$WDDWN2){


    $rs = true;

    //構文

    $strSQL  = ' UPDATE DB2WDEF ';
    $strSQL .= ' SET ';
    $strSQL .= '    WDDWNL = ?, ';
    $strSQL .= '    WDDWN2 = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= '    WDUID = ? ';
    $strSQL .= ' AND WDNAME = ? ';

    $params = array(
        $WDDWNL,
        $WDDWN2,
        $WDUID,
        $WDNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* 定義削除
*-------------------------------------------------------*
*/

function fnDeleteDB2WDEF($db2con,$WDUID,$WDNAME){


    $rs = true;

    //構文
    $strSQL  = ' DELETE FROM DB2WDEF ';
    $strSQL .= ' WHERE ';
    $strSQL .= '    WDUID = ? ';
    $strSQL .= ' AND WDNAME = ? ';

    $params = array(
        $WDUID,
        $WDNAME
    );

    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_DEL';
        }
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* FDB2CSV1にあってDB2WDEFにない行を追加
*-------------------------------------------------------*
*/

function fnInsertNoDB2WDEF($db2con,$WDUID,$WUAUTH){


    $rs = true;
    $params = array(); 
    //構文

    $strSQL  = '    INSERT INTO DB2WDEF ';
    $strSQL .= '    ( ';
    $strSQL .= '        WDUID, ';
    $strSQL .= '        WDNAME ';
    $strSQL .= '    ) ';
    $strSQL .= '    SELECT ';
    $strSQL .= '        \''.$WDUID.'\', ';
    $strSQL .= '        D1NAME ' ;
    $strSQL .= '    FROM ';
    if($WUAUTH === '3' ){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT  * ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE D1NAME IN ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';         
        $strSQL .= '        )';
        $strSQL .= '    ) B  ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT  * ';
        $strSQL .= '        FROM ';
        $strSQL .= '            FDB2CSV1 ';
        $strSQL .= '        WHERE D1NAME IN ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                DQNAME ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                DQNAME <> \'\' ';
        $strSQL .= '             AND DQCUSR = ? ';
        $strSQL .= '        )';
        $strSQL .= '    ) B  ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else{
        $strSQL .= '        FDB2CSV1 AS B';
    }
    $strSQL .= '    LEFT JOIN ';
    $strSQL .= '    ( ';
    $strSQL .= '        SELECT WDNAME FROM DB2WDEF ';
    $strSQL .= '        WHERE WDUID = ? ';
    $strSQL .= '    ) AS A ';
    $strSQL .= '    ON B.D1NAME = A.WDNAME ';
    $strSQL .= '    WHERE A.WDNAME IS NULL ';
    array_push($params,$WDUID);
    $stmt = db2_prepare($db2con,$strSQL);
    e_log('全クエリー取得：'.$strSQL .print_r($params,true));
    if($stmt === false){
        $rs = 'FAIL_INS';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_INS';
        }
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* CSV権限全て更新
*-------------------------------------------------------*
*/

function fnUpdateAllCsv($db2con,$WDUID,$WDDWNL,$WUAUTH){
    $rs = true;
    //構文
    $strSQL  = ' UPDATE DB2WDEF ';
    $strSQL .= ' SET ';
    $strSQL .= '    WDDWNL = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= '    WDUID = ? ';
    $params = array(
        $WDDWNL,
        $WDUID
    );
    if($WUAUTH === '3'){
        $strSQL .= '    AND WDNAME IN ';
        $strSQL .= '        ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    AND WDNAME IN ';
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        e_log($strSQL.print_r($params,true).db2_stmt_errormsg());
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}

/*
*-------------------------------------------------------* 
* Excel権限全て更新
*-------------------------------------------------------*
*/

function fnUpdateAllExcel($db2con,$WDUID,$WDDWN2,$WUAUTH){
    $rs = true;
    //構文
    $strSQL  = ' UPDATE DB2WDEF ';
    $strSQL .= ' SET ';
    $strSQL .= '    WDDWN2 = ? ';
    $strSQL .= ' WHERE ';
    $strSQL .= '    WDUID = ? ';
    $params = array(
        $WDDWN2,
        $WDUID
    );
    if($WUAUTH === '3'){
        $strSQL .= '    AND WDNAME IN ';
        $strSQL .= '        ( ';
        $strSQL .= '            ( ';
        $strSQL .= '                SELECT DISTINCT ';
        $strSQL .= '                    WGNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2WGDF ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    WGGID IN ( ';
        $strSQL .= '                        SELECT ';
        $strSQL .= '                            WUGID ';
        $strSQL .= '                        FROM ';
        $strSQL .= '                            DB2WUGR ';
        $strSQL .= '                        WHERE ';
        $strSQL .= '                            WUUID = ?  ';
        $strSQL .= '                    ) ';
        $strSQL .= '            ) UNION  ( ';
        $strSQL .= '                SELECT ';
        $strSQL .= '                    DQNAME ';
        $strSQL .= '                FROM ';
        $strSQL .= '                    DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '                WHERE ';
        $strSQL .= '                    DQNAME <> \'\' ';
        $strSQL .= '                AND DQCUSR = ? ';
        $strSQL .= '            ) ';
        $strSQL .= '        ) ';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    AND WDNAME IN ';
        $strSQL .= '    ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_UPD';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_UPD';
        }
    }
    return $rs;
}
/*function chkQryUser($db2con,$WDUID){
    $rs = true;
    $data = array();
    $strSQL = '';
    $strSQL .= '    SELECT DISTINCT ';
    $strSQL .= '        WUUID ';
    $strSQL .= '    FROM ';
    $strSQL .= '        DB2WUGR ';
    $strSQL .= '    WHERE ';
    $strSQL .= '        WUGID IN (SELECT ';
    $strSQL .= '                        WUGID ';
    $strSQL .= '                    FROM ';
    $strSQL .= '                        DB2WUGR ';
    $strSQL .= '                    WHERE ';
    $strSQL .= '                        WUUID = ? ';
    $strSQL .= '                    ) ';
    $strSQL .= '    AND WUUID = ? ';
    $params = array(
        $_SESSION['PHPQUERY']['user'][0]['WUUID'],
        $WDUID
    );
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}
function chkVldQryUsr($db2con,$QRYNM,$WUAUTH){
    $rs = true;
    $data = array();
    $params = array();
    if($WUAUTH === '3'){
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        ( ';
        $strSQL .= '            SELECT DISTINCT ';
        $strSQL .= '                WGNAME ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2WGDF ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                WGGID IN ( ';
        $strSQL .= '                    SELECT ';
        $strSQL .= '                        WUGID ';
        $strSQL .= '                    FROM ';
        $strSQL .= '                        DB2WUGR ';
        $strSQL .= '                    WHERE ';
        $strSQL .= '                        WUUID = ?  ';
        $strSQL .= '                ) ';
        $strSQL .= '        ) UNION  ( ';
        $strSQL .= '            SELECT ';
        $strSQL .= '                DQNAME ';
        $strSQL .= '            FROM ';
        $strSQL .= '                DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '            WHERE ';
        $strSQL .= '                DQNAME <> \'\' ';
        $strSQL .= '            AND DQCUSR = ? ';
        $strSQL .= '        ) ';         
        $strSQL .= '    )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }else if($WUAUTH === '4'){
        $strSQL .= '    SELECT  * ';
        $strSQL .= '    FROM ';
        $strSQL .= '        FDB2CSV1 ';
        $strSQL .= '    WHERE D1NAME IN ( ';
        $strSQL .= '        SELECT ';
        $strSQL .= '            DQNAME ';
        $strSQL .= '        FROM ';
        $strSQL .= '            DB2QHIS RIGHT JOIN FDB2CSV1 ON DQNAME = D1NAME ';
        $strSQL .= '        WHERE ';
        $strSQL .= '            DQNAME <> \'\' ';
        $strSQL .= '         AND DQCUSR = ? ';
        $strSQL .= '    )';
        array_push($params,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    }
    $strSQL .= '    AND D1NAME = ? ';
    array_push($params,$QRYNM);
    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $rs = 'FAIL_SEL';
    }else{
        $r = db2_execute($stmt,$params);
        if($r === false){
            $rs = 'FAIL_SEL';
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            if(count($data) === 0){
                $rs = 'NOTEXIST_GET';
            }
        }
    }
    return $rs;
}*/