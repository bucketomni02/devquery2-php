<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
/*
*-------------------------------------------------------* 
* リクエスト
*-------------------------------------------------------*
*/

$DATA = json_decode($_POST['DATA'],true);
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$focus = '';
$delFlg = 0 ;
/*
*-------------------------------------------------------* 
* チェック処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('利用停止時間の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'22',$userData[0]['WUSAUT']);//'1' => utilTimeMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('利用停止時間の権限'));
            }
        }
    }
}
if($DATA['FMHR'] === ''&& $DATA['FMMN'] === '' && $DATA['TOHR'] === '' && $DATA['TOMN'] === '' ){
    $delFlg = 1 ;
}
else{
    if($rtn === 0){
       if($DATA['FMHR'] === ''|| $DATA['FMMN'] === '' || $DATA['TOHR'] === '' || $DATA['TOMN'] === ''){
            $rtn = 1;
            $msg = showMsg('FAIL_CMP',array('フィールド'));
       }
    }
}

/*
*-------------------------------------------------------* 
* 更新処理
*-------------------------------------------------------*
*/


if($rtn === 0){
    $rs = fnInsertDB2UTIM($db2con,$DATA,$delFlg);
    if($rs !== true){
        $rtn = 1;
        $msg = showMsg($rs);
    }
}

cmDb2Close($db2con);

/**return**/
$rtnAry = array(
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtnAry));

/*
*-------------------------------------------------------* 
* 会社情報更新
*-------------------------------------------------------*
*/

function fnInsertDB2UTIM($db2con,$DATA,$delFlg){

    $rs = true;

    $strSQL  = ' DELETE FROM DB2UTIM ';
    $stmt = db2_prepare($db2con, $strSQL);
    if($stmt === false){
        $rs = 'FAIL_DEL';
    }else{
        $r =db2_execute($stmt);
        if($r === false){
            $rs = 'FAIL_DEL';
        }else{
            if($delFlg === 0){
                $strSQL  = ' INSERT INTO DB2UTIM ';
                $strSQL .='(';
                $strSQL .='UTFRHR,';
                $strSQL .='UTFRMN,';
                $strSQL .='UTTOHR,';
                $strSQL .='UTTOMN,';
                $strSQL .='UTSCHF';
                $strSQL .=')';
                $strSQL .= ' VALUES (';
                $strSQL .= ' ?, ?, ?, ?,?';
                $strSQL .= ' ) ' ;
                $params = array(
                        $DATA['FMHR'],
                        $DATA['FMMN'],
                        $DATA['TOHR'],
                        $DATA['TOMN'],
                        $DATA['SCHEJIKO']
                );
                $stmt = db2_prepare($db2con,$strSQL);
                if($stmt === false){
                    $rs = 'FAIL_INS';
                }else{
                    $r = db2_execute($stmt,$params);
                    if($r === false){
                        $rs = 'FAIL_INS';
                    }
                }
            }
        }
    }
    return $rs;

}