<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../licenseInfo.php");
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$data = array();
$msg = '';
$rtn = 0;
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
            $rtn = 2;
            $msg =  showMsg('NOTEXIST',array('利用停止時間の権限'));
        }else if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'22',$userData[0]['WUSAUT']);//'1' => utilTimeMaster
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('利用停止時間の権限'));
            }
        }
    }
}
$rs = fnGetDB2UTIM($db2con);

if($rtn === 0){
    if($rs['result'] !== true){
        $rtn = 1;
        $msg = showMsg($rs['result']);
    }else{
        $data = $rs['data'];
    }
}
cmDb2Close($db2con);


/**return**/
$rtn = array(
    'aaData' => umEx($data),
    'RTN' => $rtn,
    'MSG' => $msg
);

echo(json_encode($rtn));

/*
*-------------------------------------------------------* 
* ユーザーマスター取得
*-------------------------------------------------------*
*/

function fnGetDB2UTIM($db2con){

    $data = array();

    $params = array();


    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2UTIM AS A ' ;


    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
        $data = array('result' => 'FAIL_SEL');
    }else{
        $r = db2_execute($stmt);
        if($r === false){
            $data = array('result' => 'FAIL_SEL');
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
            $data = array('result' => true,'data' => $data);
        }
    }
    return $data;

}