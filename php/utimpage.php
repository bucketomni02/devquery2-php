<html>

<head>
</head>

<body>
<?php
include_once("common/inc/config.php");
try {

    $user = RDB_USER;
    $password = RDB_PASSWORD;
    $database = RDB;
    $option = array(
        'autocommit' => DB2_AUTOCOMMIT_ON,
        'i5_dbcs_alloc' => DB2_I5_DBCS_ALLOC_ON
    );

    $db2con = db2_connect($database, $user, $password,$option);
    db2_exec($db2con, 'SET SCHEMA '.MAINLIB);

    $strSQL  = ' SELECT A.* ';
    $strSQL .= ' FROM DB2UTIM AS A ' ;


    $stmt = db2_prepare($db2con,$strSQL);
    if($stmt === false){
    }else{
        $r = db2_execute($stmt);
        if($r === false){
        }else{
            while($row = db2_fetch_assoc($stmt)){
                $data[] = $row;
            }
        }
    }
    if(intval($data[0]['UTFRHR']) < 10){
        $fhr = '0'.$data[0]['UTFRHR'];
    }
    else{
        $fhr = $data[0]['UTFRHR'];
    }
    if(intval($data[0]['UTFRMN']) < 10){
        $fmn = '0'.$data[0]['UTFRMN'];
    }
    else{
        $fmn = $data[0]['UTFRMN'];
    }
    if(intval($data[0]['UTTOHR'])< 10){
        $thr = '0'.$data[0]['UTTOHR'];
    }
    else{
        $thr = $data[0]['UTTOHR'];
    }
    if(intval($data[0]['UTTOMN'])< 10){
        $tmn = '0'.$data[0]['UTTOMN'];
    }
    else{
        $tmn = $data[0]['UTTOMN'];
    }
    $msg = $fhr.':'.$fmn.'~'.$thr.':'.$tmn.'は利用停止時間です。';

    } catch(Exception $e) {
        $this->pconn='die';die();
    }

?>

<p><?=$msg?></p>

</body>

</html>