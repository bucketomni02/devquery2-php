<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$rtn     = 0;
$msg     = '';
$success = true;
$db2con  = cmDb2Con();
cmSetPHPQUERY($db2con);
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */

$D1EDKB    = (isset($_POST['D1EDKB'])) ? $_POST['D1EDKB'] : '';
$D1NAME    = (isset($_POST['D1NAME'])) ? $_POST['D1NAME'] : '';
$D1SFLG    = (isset($_POST['D1SFLG'])) ? $_POST['D1SFLG'] : '';
$D1SHET    = (isset($_POST['D1SHET'])) ? $_POST['D1SHET'] : '';
$D1TCOS    = (isset($_POST['D1TCOS'])) ? $_POST['D1TCOS'] : '';
$D1TROS    = (isset($_POST['D1TROS'])) ? $_POST['D1TROS'] : '';
$EXCELFILE = (isset($_POST['EXCELFILE'])) ? $_POST['EXCELFILE'] : '';
$PROC      = (isset($_POST['PROC'])) ? $_POST['PROC'] : '';
//ログインユーザが削除されたかどうかチェック

 if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array(
            'ユーザー'
        ));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
            $rs = cmChkKenGen($db2con,'9',$userData[0]['WUSAUT']);//'9' => HTMLテンプレート
            if($rs['result'] !== true){
                $rtn = 4;
                $msg =  showMsg($rs['result'],array('EXCELテンプレートの権限'));
            }
        }
    }
}
//定義が削除されたかどうかチェック、ユーザが定義に権限があるかのチェック
if($rtn === 0){
    $chkQry = cmChkQuery ( $db2con,'' , $D1NAME, '');
    if( $chkQry['result'] !== true ) {
        $rtn = 3;
        $msg = showMsg($chkQry['result'],array('クエリー'));
    }
}
if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
    if($rtn === 0){
        $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
        if($chkQryUsr === 'NOTEXIST_GET'){
            $rtn = 3;
            //$msg = showMsg('ログインユーザーに指定したクエリーに対してのEXCELテンプレートの権限がありません。');
            $msg =  showMsg('FAIL_QRY_USR',array('EXCELテンプレートの権限'));
        }else if($chkQryUsr !== true){
            $rtn = 1;
            $msg = showMsg($chkQryUsr['result'],array('クエリー'));
        }
    }
}
$aaData    = array();
if ($rtn === 0) {

       if ($PROC !== 'DEL') {
            if ($rtn === 0) {
                if ($rtn === 0) {
                    if ($EXCELFILE === '') {
                        $rtn = 1;
                        $msg = showMsg('FAIL_SLEC',array('ファイル'));// 'ファイルを選択してください。';
                    }
                }
                if ($rtn === 0) {
                    //Excelファイルの中でfileという文字が入ったらエラーメッセージが出る指摘の対応でコンメントアウトした
                   /* if (strpos($EXCELFILE, 'file') !== false) {
                        $excel = substr($EXCELFILE, 12);
                        $ext   = explode('.', $excel);
                        $ext   = $ext[count($ext) - 1];
                    } else {
                        $ext = explode('.', $EXCELFILE);
                        $ext = $ext[count($ext) - 1];
                    }*/
                    
                    
                    $ext = explode('.', $EXCELFILE);
                    $ext = $ext[count($ext) - 1];
                    
                    if ($ext === 'xls' || $ext === 'xlsx' || $ext === 'xlsm') {
                        if ($D1EDKB === '' && $ext === 'xlsm' && false) {
                            $rtn = 1;
                            $msg = showMsg('FAIL_FMT', array(
                                '標準テンプレート',
                                'xlsm'
                            ));
                        } else {
                            
                            $rtn = 0;
                        }
                    } else {
                        $rtn = 1;
                        $msg = showMsg('CHK_FMT', array('テンプレート',array('EXCEL','ファイル')));
                    }
                }
            //以下チェックはダウンロード形式が標準時のみ
            //ダウンロード形式がカスタマイズの場合はシート、列、行をブランクに変換
            if ($D1EDKB === '1') {
                
                $D1SHET = 0;
                $D1TCOS = '';
                $D1TROS = 0;
                

            } else {
                //シート半角チェック
                if ($rtn === 0) {
                    $byte = checkByte($D1SHET);
                    if ($byte[1] > 0) {
                        $rtn   = 1;
                        $msg   = showMsg('FAIL_BYTE', array(
                            'シート'
                        ));
                        $focus = 'D1SHET';
                    }
                }
                //シートの数値チェック
                if ($rtn === 0) {
                    if (!checkNum($D1SHET)) {
                        $rtn   = 1;
                        $msg   = showMsg('CHK_FMT', array(
                            'シート',
                            '数値'
                        ));
                        $focus = 'D1SHET';
                    }
                }
                //シートゼロチェック
                if ($rtn === 0) {
                    if ($D1SHET < 1) {
                        $rtn   = 1;
                        $msg   = showMsg('CHK_FMT', array(
                            'シート',
                            array('1','以上の数値')
                        ));
                        $focus = 'D1SHET';
                    }
                }
                // シート入力桁数チェック
                if ($rtn === 0) {
                    if (!checkMaxLen($D1SHET, 10)) {
                        $rtn   = 1;
                        $msg   = showMsg('FAIL_MAXLEN', array(
                            'シート'
                        ));
                        $focus = 'D1SHET';
                    }
                }
                // 開始列のブランクチェック
                if ($rtn === 0) {
                    if ($D1TCOS === '') {
                        $rtn   = 1;
                        $msg   = showMsg('FAIL_REQ', array(
                            '開始列'
                        ));
                        $focus = 'D1TCOS';
                    }
                }
                //開始列半角チェック
                if ($rtn === 0) {
                    $byte = checkByte($D1TCOS);
                    if ($byte[1] > 0) {
                        $rtn   = 1;
                        $msg   = showMsg('FAIL_BYTE', array(
                            '開始列'
                        ));
                        $focus = 'D1TCOS';
                    }
                }
                //開始列の大文字入力チェック
                if ($rtn === 0) {
                    if (!preg_match("/^[A-Z]+$/", $D1TCOS)) {
                        //if(!ctype_upper($D1TCOS)){
                        $rtn   = 1;
                        $msg   = showMsg('CHK_FMT', array(
                            '開始列',
                            '大文字のアルファベット'
                        ));
                        $focus = 'D1TCOS';
                    }
                }
                // 開始列入力桁数チェック
                if ($rtn === 0) {
                    if (!checkMaxLen($D1TCOS, 3)) {
                        $rtn   = 1;
                        $msg   = showMsg('FAIL_MAXLEN', array(
                            '開始列'
                        ));
                        $focus = 'D1TCOS';
                    }
                }
                if ($rtn === 0) {
                    if ($D1TROS === '') {
                        $rtn   = 1;
                        $msg   = showMsg('FAIL_REQ', array(
                            '開始行'
                        ));
                        $focus = 'D1TROS';
                    }
                }
                //開始行半角チェック
                if ($rtn === 0) {
                    $byte = checkByte($D1TROS);
                    if ($byte[1] > 0) {
                        $rtn   = 1;
                        $msg   = showMsg('FAIL_BYTE', array(
                            '開始行'
                        ));
                        $focus = 'D1TROS';
                    }
                }
                //開始行の数値チェック
                if ($rtn === 0) {
                    if (!checkNum($D1TROS)) {
                        $rtn   = 1;
                        $msg   = showMsg('CHK_FMT', array(
                            '開始行',
                            '数値'
                        ));
                        $focus = 'D1TROS';
                    }
                }
                //開始行ゼロチェック
                if ($rtn === 0) {
                    if ($D1TROS < 1) {
                        $rtn   = 1;
                        $msg   = showMsg('CHK_FMT', array(
                            '開始行',
                            array('１','以上の数値')
                        ));
                        $focus = 'D1TROS';
                    }
                }
                // 開始行入力桁数チェック
                if ($rtn === 0) {
                    if (!checkMaxLen($D1TROS, 10)) {
                        $rtn   = 1;
                        $msg   = showMsg('FAIL_MAXLEN', array(
                            '開始行'
                        ));
                        $focus = 'D1TROS';
                    }
                }
            }
        }
    } else {
        if ($EXCELFILE === '') {
            $rtn = 3;
            $msg = showMsg('TMP_FUNC',array('テンプレート'));//'テンプレートの登録解除処理が完了しました。';
        } else {
            $rtn = 0;
        }
      
    }
}
/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'success' => $success,
    'aaData' => $aaData,
    'D1NAME' => $D1NAME,
    'PROC' => $PROC,
    'EXCELFILE' => $EXCELFILE
   // 'a' => $excel
);
echo (json_encode($rtnArray));