<?php

/*
 *-------------------------------------------------------* 
 * 外部ファイル読み込み
 *-------------------------------------------------------*
 */
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
 *-------------------------------------------------------* 
 * 変数
 *-------------------------------------------------------*
 */
$rtn     = 0;
$msg     = '';
$success = true;
$db2con  = cmDb2Con();
cmSetPHPQUERY($db2con);
/*
 *-------------------------------------------------------* 
 * 処理
 *-------------------------------------------------------*
 */
$D1EDKB = (isset($_POST['D1EDKB'])) ? $_POST['D1EDKB'] : '';
$D1NAME = (isset($_POST['D1NAME'])) ? $_POST['D1NAME'] : '';
$D1SHET = (isset($_POST['D1SHET'])) ? $_POST['D1SHET'] : '';
$D1TCOS = (isset($_POST['D1TCOS'])) ? $_POST['D1TCOS'] : '';
$D1TROS = (isset($_POST['D1TROS'])) ? $_POST['D1TROS'] : '';
$D1THFG = (isset($_POST['D1THFG'])) ? $_POST['D1THFG'] : '';
$D1SFLG = (isset($_POST['D1SFLG'])) ? $_POST['D1SFLG'] : '';
$PROC   = (isset($_POST['PROC'])) ? $_POST['PROC'] : '';
$aaData = array();


//ログインユーザが削除されたかどうかチェック

if(EXCELVERSION === '2007' || EXCELVERSION === '2003' || EXCELVERSION === 'html' || EXCELVERSION === 'XLSX') {
     include_once("../common/lib/PHPExcel/Classes/PHPExcel.php");
     include_once("../common/lib/PHPExcel/Classes/PHPExcel/IOFactory.php");
     include_once("../common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");
     include_once("../common/lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php");
    if ($rtn === 0) {
        $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if ($rs['result'] !== true) {
            $rtn = 2;
            $msg = showMsg($rs['result'], array(
                'ユーザー'
            ));
        }else{
            $userData  = umEx($rs['data']);
            if($userData[0]['WUAUTH'] === '2'){
                $rs = cmChkKenGen($db2con,'9',$userData[0]['WUSAUT']);//'9' => HTMLテンプレート
                if($rs['result'] !== true){
                    $rtn = 4;
                    $msg =  showMsg($rs['result'],array('EXCELテンプレートの権限'));
                }
            }
        }
    }
    if ($rtn === 0) {
        $chkQry = array();
        $chkQry = cmChkQuery($db2con, '', $D1NAME, '');
        if ($chkQry['result'] === 'NOTEXIST_PIV') {
            $rtn = 2;
            $msg = showMsg('NOTEXIST_GET', array(
                'ピボット'
            ));
        } else if ($chkQry['result'] !== true) {
            $rtn = 2;
            $msg = showMsg($chkQry['result'], array(
                'クエリー'
            ));
        }
    }
    if($rtn === 0){
         $data = fnGetFDB2CSV1($db2con, $D1NAME);
           if ($data['result'] === true) {
               //$aaData = umEx($data['data'],true);
              $aaData = umEx($data['data']);
              
                                                   
          } else {
                $rtn = 1;
                $msg = showMsg($res['result'], array(
                    'クエリー'
           ));
        }
    }
    if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4'){
        if($rtn === 0){
            $chkQryUsr = chkVldQryUsr($db2con,$D1NAME,$userData[0]['WUAUTH']);
            if($chkQryUsr === 'NOTEXIST_GET'){
                $rtn = 3;
                //$msg = showMsg('ログインユーザーに指定したクエリーに対してのEXCELテンプレートの権限がありません。');
                $msg =  showMsg('FAIL_QRY_USR',array('EXCELテンプレートの権限'));
            }else if($chkQryUsr !== true){
                $rtn = 1;
                $msg = showMsg($chkQryUsr['result'],array('クエリー'));
            }
        }
    }
 if ($rtn !== 1) {
    if ($PROC === 'OPEN') {
        $data = fnGetFDB2CSV1($db2con, $D1NAME);
        
        if ($data['result'] === true) {
            //                        $aaData = umEx($data['data'],true);
            $aaData = umEx($data['data']);
            
        } else {
            $rtn = 1;
            $msg = showMsg($res['result'], array(
                'クエリー'
            ));
        }
    } else if ($PROC === 'DEL') {
        $res = fnUpdateFDB2CSV1($db2con, '', '', 0, '', 0, '', $D1NAME, $D1SFLG, $PROC);
        if ($res['result'] === true) {
            $files = glob(BASE_DIR . "/php/template/" . $D1NAME . ".*");
           
            foreach ($files as $file) {
                unlink($file);
            }
            $msg = showMsg('TMP_FUNC',array('テンプレート'));//'テンプレートの登録解除処理が完了しました。';
        } else {
            $rtn = 1;
            $msg = showMsg($res['result'], array(
                'クエリー'
            ));
        }
    } else {
        //以下チェックはダウンロード形式が標準時のみ
        if (isset($_FILES)) {
            // $rtn = 1;
            $temp_file_name     = $_FILES['excelfile']['tmp_name'];
            $original_file_name = $_FILES['excelfile']['name'];
            
            // Find file extention
            $ext = explode('.', $original_file_name);
            $ext = $ext[count($ext) - 1];

            if ($ext === 'xls' || $ext === 'xlsx' || $ext === 'xlsm' || $original_file_name === '') {
                if($D1EDKB === '' && $ext === 'xlsm' && false){
                    $rtn = 1;
                    $msg = showMsg('FAIL_FMT', array(
                        '標準テンプレート',
                        'xlsm'
                    ));
                }  
                 else{
    				  if($D1EDKB === ''){
                        if ($original_file_name === '') {
                            $D1TMPF = '';
                        } else {
                            //xlsx.xlsがアップロードできないの問題で修正 2016/12/20
                            $D1TMPF    = $original_file_name;
                        }
                        // テンプレート名入力桁数チェック
                        if (!checkMaxLen($D1TMPF, 50)) {
                            $rtn = 1;
                            $msg = showMsg('FAIL_MAXLEN', array(
                                'テンプレート名'
                            ));
                        }
                        if ($rtn === 0) {
                       
                           $res = fnUpdateFDB2CSV1($db2con, $D1EDKB, $D1TMPF, $D1SHET, $D1TCOS, $D1TROS, $D1THFG, $D1NAME, $D1SFLG, $PROC);
                            if ($res['result'] === true) {
                                if ($original_file_name !== '') {
                                   //original_file_nameある場合
                                 
                                    $files = glob(BASE_DIR . "/php/template/" . $D1NAME . ".*");
                                 
                                      $D1NAME = $D1NAME . '.' . $ext;
                                     
                                      $flg    = move_uploaded_file($temp_file_name, "../downloadtmp/$D1NAME");
                                      if ($flg === true) {
                                            $msg = showMsg('TMP_FUNC',array('テンプレートの登録処理'));//'テンプレートの登録処理が完了しました。';
                                              $load_url  = BASE_DIR . '/php/downloadtmp/' . $D1NAME;
                                              $inputFileType = PHPExcel_IOFactory::identify($load_url);
                                              
                                              if($inputFileType === 'HTML'){
                                                  
                                                  if($aaData[0]['D1TMPF'] === ''){
                                                      
                                                      $PROC = 'DEL';
                                                      $res = fnUpdateFDB2CSV1($db2con, $aaData[0]['D1EDKB'], $aaData[0]['D1TMPF'], $aaData[0]['D1SHET'], $aaData[0]['D1TCOS'], $aaData[0]['D1TROS'], $aaData[0]['D1THFG'], $aaData[0]['D1NAME'], $aaData[0]['D1SFLG'], $PROC);
                                                      
                                                      
                                                  }else{
                                                   
                                                    $res = fnUpdateFDB2CSV1($db2con, $aaData[0]['D1EDKB'], $aaData[0]['D1TMPF'], $aaData[0]['D1SHET'], $aaData[0]['D1TCOS'], $aaData[0]['D1TROS'], $aaData[0]['D1THFG'], $aaData[0]['D1NAME'], $D1SFLG, $PROC);
                                                    
                                                  }
                                                        $filesexcel = BASE_DIR . "/php/downloadtmp/" . $D1NAME;
                                                        unlink($filesexcel);
                                                        $rtn     = 2;
                                                        $msg     = showMsg('FAIL_EXCELCHK');//'ファイルの種類がWebページのExcelはテンプレートとして使用できません。'HTMLテンプレート;
                                                     
                                                
                                              }else{
                                                    foreach ($files as $file) {
                                                        unlink($file);
                                                    }
                                                    shell_exec('cp ' . TEMP_DIR . '"' . $D1NAME . '"' . ' ' . ETEMP_DIR . '"' . $D1NAME . '"');
                                                    $msg = showMsg('TMP_FUNC',array('テンプレートの登録処理'));//'テンプレートの登録処理が完了しました。';
                                                    $filesexcel = BASE_DIR . "/php/downloadtmp/" . $D1NAME;
                                                    unlink($filesexcel);
                                             }
                                     } else {
    									    $filesexcel = BASE_DIR . "/php/downloadtmp/" . $D1NAME;
                                            unlink($filesexcel);
                                            $rtn     = 1;
                                            $success = false;
                                            $msg     = showMsg('FAIL_FILE_INS',array('テンプレートの登録処理'));//'テンプレートの登録処理ができませんでした。';
                                            
                                      }  
                               }else{
                                  
                                   $ext = explode('.', $aaData[0]['D1TMPF']);
                                   $ext = $ext[count($ext) - 1];
                                   $D1NAME = $D1NAME . '.' . $ext;
								   $load_url  = BASE_DIR . '/php/template/' . $D1NAME;
                                   $inputFileType = PHPExcel_IOFactory::identify($load_url);
								   if($inputFileType === 'HTML'){
                                          if($aaData[0]['D1TMPF'] === ''){
                                                      
                                               $PROC = 'DEL';
                                               $res = fnUpdateFDB2CSV1($db2con, $aaData[0]['D1EDKB'], $aaData[0]['D1TMPF'], $aaData[0]['D1SHET'], $aaData[0]['D1TCOS'], $aaData[0]['D1TROS'], $aaData[0]['D1THFG'], $aaData[0]['D1NAME'], $D1SFLG, $PROC);
                                           }else{
                                                   
                                               $res = fnUpdateFDB2CSV1($db2con, $aaData[0]['D1EDKB'], $aaData[0]['D1TMPF'], $aaData[0]['D1SHET'], $aaData[0]['D1TCOS'], $aaData[0]['D1TROS'], $aaData[0]['D1THFG'], $aaData[0]['D1NAME'], $D1SFLG, $PROC);
                                           }
									     $rtn     = 2;
                                         $msg     = showMsg('FAIL_EXCELCHK');//'ファイルの種類がWebページのExcelはテンプレートとして使用できません。'HTMLテンプレート;
								   }else{
								       $msg = showMsg('TMP_FUNC',array('テンプレートの登録処理'));//'テンプレートの登録処理が完了しました。';
                                      
								   }
							     }
							  
                            } else {
                                $msg = showMsg('TMP_FUNC',array('テンプレートの登録処理'));//'テンプレートの登録処理が完了しました。';
                                
                            }
                        } else {
                            $rtn = 1;
                            $msg = showMsg($res['result'], array(
                                'クエリー'
                            ));
                        }
               }else{
                    //カスタマイズのチェック
                    if ($original_file_name === '') {
                        $D1TMPF = '';
                    } else {
                        //xlsx.xlsがアップロードできないの問題で修正 2016/12/20
                        $D1TMPF    = $original_file_name;
                    }
                    // テンプレート名入力桁数チェック
                    if (!checkMaxLen($D1TMPF, 50)) {
                        $rtn = 1;
                        $msg = showMsg('FAIL_MAXLEN', array(
                            'テンプレート名'
                        ));
                    }
                    if ($rtn === 0) {
                        $res = fnUpdateFDB2CSV1($db2con, $D1EDKB, $D1TMPF, $D1SHET, $D1TCOS, $D1TROS, $D1THFG, $D1NAME, $D1SFLG, $PROC);
                        if ($res['result'] === true) {
                            if ($original_file_name !== '') {
                                $files = glob(BASE_DIR . "/php/template/" . $D1NAME . ".*");
                                foreach ($files as $file) {
                                    unlink($file);
                                }
                                $D1NAME = $D1NAME . '.' . $ext;
                                //unlink("../template/".$D1NAME);
                                $flg    = move_uploaded_file($temp_file_name, "../template/$D1NAME");
                                if ($flg === true) {
                                    $msg = showMsg('TMP_FUNC',array('テンプレートの登録処理'));//'テンプレートの登録処理が完了しました。';
                                     
                                } else {
                                    $rtn     = 1;
                                    $success = false;
                                    $msg     = showMsg('FAIL_FILE_INS',array('テンプレートの登録処理'));//'テンプレートの登録処理ができませんでした。';
                                    
                                }
                            } else {
                                $msg = showMsg('TMP_FUNC',array('テンプレートの登録処理'));//'テンプレートの登録処理が完了しました。';
                            }
                        } else {
                            $rtn = 1;
                            $msg = showMsg($res['result'], array(
                                'クエリー'
                            ));
                        }
                     }
				   }
                }
            } else {
                $rtn = 1;
                $msg = showMsg('CHK_FMT', array(
                    'テンプレート',
                    'EXCElファイル'
                ));
              }
           }
       }
    }
}
/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'success' => $success,
    'aaData' => $aaData,
    'D1NAME' => $D1NAME,
    'FILES' => $_FILES,
    'PROC' => $PROC
);
echo (json_encode($rtnArray));

/*
 *-------------------------------------------------------* 
 * FDB2CSV1取得
 *-------------------------------------------------------*
 */
function fnUpdateFDB2CSV1($db2con, $D1EDKB, $D1TMPF, $D1SHET, $D1TCOS, $D1TROS, $D1THFG, $d1name, $D1SFLG, $PROC)
{
    
    $data   = array();
    $params = array();
    $D1SHET = ($D1SHET === '') ? 0 : $D1SHET;
    $D1TROS = ($D1TROS === '') ? 0 : $D1TROS;
    $params = array(
        $D1EDKB,
        $D1SHET,
        $D1TCOS,
        $D1TROS,
        $D1THFG,
		$D1SFLG
    );
    $strSQL = ' UPDATE FDB2CSV1 SET D1EDKB = ? ,D1SHET = ? ,D1TCOS = ? ,D1TROS = ? ,D1THFG = ? ,D1SFLG = ? ';
    if ($D1TMPF !== '' || $PROC === 'DEL') {
        $strSQL .= ' ,D1TMPF = ? ';
        array_push($params, $D1TMPF);
    }
    $strSQL .= ' WHERE D1NAME = ? ';
    array_push($params, $d1name);
    $stmt = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            //$data = array('result' => true);
            //delete DBfnDelDB2ECONDB2EINS
            $data = fnDelDB2ECONDB2EINS($db2con, $d1name);
        }
    }
    return $data;
}
function fnGetFDB2CSV1($db2con, $d1name)
{
    $data = array();
    
    $params = array();
    $strSQL = ' SELECT * FROM FDB2CSV1';
    $strSQL .= ' WHERE D1NAME = ? ';
    $params = array(
        $d1name
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_SEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_SEL'
            );
        } else {
            while ($row = db2_fetch_assoc($stmt)) {
                $data[] = $row;
            }
            $data = array(
                'result' => true,
                'data' => $data
            );
        }
    }
    return $data;
}
/*
 *
 *delete DB2ECON,DB2EINS
 */
function fnDelDB2ECONDB2EINS($db2con, $d1name)
{
    $data   = array();
    $strSQL = '';
    $strSQL = ' DELETE FROM DB2ECON WHERE ECNAME = ? ';
    $params = array(
        $d1name
    );
    $stmt   = db2_prepare($db2con, $strSQL);
    if ($stmt === false) {
        $data = array(
            'result' => 'FAIL_DEL'
        );
    } else {
        $r = db2_execute($stmt, $params);
        if ($r === false) {
            $data = array(
                'result' => 'FAIL_DEL'
            );
        } else {
            $strSQL = ' DELETE FROM DB2EINS WHERE EINAME = ? ';
            $params = array(
                $d1name
            );
            $stmt   = db2_prepare($db2con, $strSQL);
            if ($stmt === false) {
                $data = array(
                    'result' => 'FAIL_DEL'
                );
            } else {
                $params = array(
                    $d1name
                );
                $r      = db2_execute($stmt, $params);
                $data   = ($r === false) ? array(
                    'result' => 'FAIL_DEL'
                ) : array(
                    'result' => true
                );
            }
        }
    }
    return $data;
}