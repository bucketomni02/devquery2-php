<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");
include_once("../licenseInfo.php");
/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/
$rtn = 0;
$msg = '';
$success = true;
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);

/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/
$zipfile = (isset($_POST['zipfile']))?$_POST['zipfile']:'';
$PROC  = (isset($_POST['PROC']))?$_POST['PROC']:'';
$LastBk_name  = (isset($_POST['LastBk_name']))?$_POST['LastBk_name']:'';//

//ログインユーザが削除されたかどうかチェック
if($rtn === 0){
    $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if($rs['result'] !== true){
        $rtn = 2;
        $msg = showMsg($rs['result'],array('ユーザー'));
    }else{
        $userData  = umEx($rs['data']);
        if($userData[0]['WUAUTH'] === '2'){
          
            $rs = cmChkKenGen($db2con,'33',$userData[0]['WUSAUT']);//'33' => Import
       
            if($rs['result'] !== true){
                $rtn = 2;
                $msg =  showMsg($rs['result'],array('インポートの権限'));
            }
        }
    }
}


if($rtn === 0){
    if($PROC === "1"){
        //システムの復元を押すと　最後のバックアップ(backup/) 　＝＞　今のファイル(real file) 戻る　overwrite
        $backup_path = "";//ziptmp/$LastBk_name/backup."/";
        $real_path = "";//$real_path = "../../".$cpy_file; //php/common/inc/common.format.php => 今のファイル(real file)
        $last_bk = "";
        $files = scandir("../ziptmp/"); 
        if(count($files) > 0){
            //error_log("backup folder list => ".print_r($files,true));
            $last_bk = end($files);
        }
        $path = "../ziptmp/$last_bk/backup";
        //error_log("path_file importzip => ".$path);
      
        $it = new RecursiveDirectoryIterator($path);
        foreach(new RecursiveIteratorIterator($it) as $file) {
           
            if(substr($file, -1) !== "."){
                //error_log("RecursiveIteratorIterator importzip => ".$file);
                $backup_path = $file;
                $real_path = str_replace($path, '../..', $file);
                error_log("backup_path importzip => ".$backup_path);
                error_log("real_path importzip => ".$real_path);
                copy($backup_path,$real_path);
            }
        }


        
        $rtn = 0;
        
    }else{
        if($rtn === 0){
            if(cmMer($zipfile) === ''){
                $rtn = 1;
                $msg = showMsg('FAIL_SET',array('インポートファイル'));
                $focus = 'zipfile';
            }
        }
        if($rtn === 0){
            if(strripos($zipfile, '\\') !== false) {
                $zipfile =substr($zipfile,12); //for cut "C:\fakepath\" => 12
            }
            $ext = explode ('.', $zipfile);
            $ext = $ext [count ($ext) - 1];
            if($ext === 'zip'){//
                $rtn = 0;
            }else{
                $rtn = 1;
                $msg = showMsg('CHK_FMT',array('インポートファイル',array(' zip ','ファイル')));
                $focus = 'zipfile';
            }
        }        
    }           
  
}
function listFolderFiles($path){
    $folder_dir = array();
    $files = array_diff(scandir($path), array('.', '..'));
    error_log("files directory importzip => ".print_r($files,true));
    foreach($files as $ff){
        
        if(is_dir($path.'/'.$ff)) {
            $folder_dir = listFolderFiles($path.'/'.$ff);
         }   
        error_log("listFolderFiles importzip => ".print_r($folder_dir,true));
    }
}
function dirToArray($dir) { 
   
   $result = array(); 

   $cdir = scandir($dir); 
   foreach ($cdir as $key => $value) 
   { 
      if (!in_array($value,array(".",".."))) 
      { 
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
         { 
            $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
         } 
         else 
         { 
            $result[] = $value; 
         } 
      } 
   } 
   
   return $result; 
} 


/**return**/
$rtnArray = array(
    'RTN' => $rtn,
    'MSG' => $msg,
    'zipfile' => $zipfile,
    '_FILES'   => $_FILES,
    'FOCUS' => $focus
);
//e_log('戻り値:'.print_r($rtnArray,true));


echo(json_encode($rtnArray));