<?php

/*
*-------------------------------------------------------* 
* 外部ファイル読み込み
*-------------------------------------------------------*
*/
include_once("../common/inc/config.php");
include_once("../common/inc/common.inc.php");

/*
*-------------------------------------------------------* 
* 変数
*-------------------------------------------------------*
*/

$FLG       = (isset($_POST['FLG']) ? $_POST['FLG'] : '');

$data = array();
$rtn = 0;
$msg = '';
/*
*-------------------------------------------------------* 
* 処理
*-------------------------------------------------------*
*/

$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
if($FLG === ''){
    //処理無し
}else{
    //ログインユーザが削除されたかどうかチェック
    if($rtn === 0){
        $rs = cmGetWUAUTH($db2con,$_SESSION['PHPQUERY']['user'][0]['WUUID']);
        if($rs['result'] !== true){
            $rtn = 2;
            $msg = showMsg($rs['result'],array('ユーザー'));
        }else{
            $userData  = umEx($rs['data']);
            if($userData[0]['WUAUTH'] === '3' || $userData[0]['WUAUTH'] === '4' ){
                $rtn = 2;
                if($FLG === 'EXP'){
                    $msgParam = 'エクスポット';
                }else if($FLG === 'IMP'){
                    $msgParam = 'インポット';
                }
                $msg =  showMsg('NOTEXIST',array($msgParam.'の権限'));
            }else if($userData[0]['WUAUTH'] === '2'){
                if($FLG === 'EXP'){
                    $msgParam = 'エクスポット';
                    $rs = cmChkKenGen($db2con,'32',$userData[0]['WUSAUT']);
                }else if($FLG === 'IMP'){
                    $msgParam = 'インポット';
                    $rs = cmChkKenGen($db2con,'33',$userData[0]['WUSAUT']);
                }
                if($rs['result'] !== true){
                    $rtn = 2;
                    $msg =  showMsg($rs['result'],array($msgParam.'の権限'));
                }
            }
        }
    }

	
	if($rtn === 0){

	    $last_bk = "";
	    $files = scandir("../ziptmp/"); 
	    if(count($files) > 0){
	        //error_log("backup folder list => ".print_r($files,true));
	        $last_bk = end($files);
			
			
	    }else{
            $last_bk = "";
        }
        if($last_bk === ".."){
            $last_bk = "";
        }

	}

}
cmDb2Close($db2con);

/**return**/
$rtn = array(
    'MSG' => $msg,
    'RTN' => $rtn,
	'LastBackup' =>$last_bk
);

echo(json_encode($rtn));
