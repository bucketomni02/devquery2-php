<?php
/*
 *-------------------------------------------------------*
 * 外部ファイル読み込み
 *-------------------------------------------------------*
*/
include_once ("../common/inc/config.php");
include_once ("../common/inc/common.inc.php");
require_once BASE_DIR . '/php/common/lib/mb_str_replace/mb_str_replace.php';
/*
 *-------------------------------------------------------*
 * DataTableリクエスト
 *-------------------------------------------------------*
*/
/*
 *-------------------------------------------------------*
 * 変数
 *-------------------------------------------------------*
*/
$zipfile = (isset($_POST['zipfile'])) ? $_POST['zipfile'] : '';

$data = array();
$datatest = array();
$libcheck = 1;
$kengencheck = 1;
$langcheck = 1;
$msg = '';
$success = true;
/*
 *-------------------------------------------------------*
 * 処理
 *-------------------------------------------------------*
*/
$db2con = cmDb2Con();
cmSetPHPQUERY($db2con);
$rtn = 0;
$csvblankcheck = '0';
//ログインユーザが削除されたかどうかチェック
if ($rtn === 0) {
    $rs = cmGetWUAUTH($db2con, $_SESSION['PHPQUERY']['user'][0]['WUUID']);
    if ($rs['result'] !== true) {
        $rtn = 2;
        $msg = showMsg($rs['result'], array('ユーザー'));
    } else {
        $userData = umEx($rs['data']);
        if ($userData[0]['WUAUTH'] === '2') {
            $rs = cmChkKenGen($db2con, '33', $userData[0]['WUSAUT']); //'33' => Import
            if ($rs['result'] !== true) {
                $rtn = 2;
                $msg = showMsg($rs['result'], array('インポートの権限'));
            }
        }
    }
}
if ($rtn === 0) {
    $res = db2_autocommit($db2con, DB2_AUTOCOMMIT_OFF);
    if ($res === false) {
        $rtn = 1;
        $msg = showMsg('FAIL_SYS');
    }
}
if ($rtn === 0) {
    //トランザクションの開始
    db2_exec($db2con, 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
}

if (isset($_FILES)) {
    //error_log("zipFile1 => ".print_r($_FILES,true));  
    $zipfile = $_FILES['zipfile']['name'];
    if ($zipfile !== '') {
        $ext = explode('.', $zipfile); // end(explode(".", $zipfile));  
        $extension = $ext[count($ext) - 1];
        //error_log("extension array=> ".print_r($ext,true));
        $name = $ext[0];
        $tmp_zipfile = $_FILES['zipfile']['tmp_name'];
        $zipfile = $zipfile;
    } else {
        $zipfile = '';
    }

    $Bkfilename = date('YmdHis');//コピーするためフォルダー名
    $Bkfilename_rtn = date('Y/m/d')."　".date('H:i:s');
    $path = '../ziptmp/'.$Bkfilename;
    mkdir($path, 0700); //コピーするためにフォルダーを作る
    //zipuploadfile
    
    $location = $path."/".$zipfile; //XXXXX zipuploadfile/$zipfile
    if (move_uploaded_file($tmp_zipfile, $location)) {
        //copy($location, "../ziptmp/$Bkfilename/$zipfile"); //　バックアップのために コピーする

        try{
            /*$zip = new ZipArchive;  
            if($zip->open($location)) {  
                //$unzip_dir = trim($zip->getNameIndex(0), '/');
                $zip->extractTo($path);  
                $zip->close();  
                
                error_log("extraction successful ");
            }else{
                error_log("extraction unsuccessful");
                $rtn = 1;
                break;
            } */
            shell_exec($location);

        }catch(Exception $e){
            die('Error loading file "' . pathinfo($location, PATHINFO_BASENAME) . '": ' . 
                $e->getMessage());
            //shell_exec($location);
        }

        //error_log("unzip_name  => ".$unzip_name[0]);
        if(is_dir($path ."/". $unzip_name[0])){
            $files = scandir($path ."/". $unzip_name[0]); 
        }else{
            $rtn = 1;
            break;
        }
        
        //error_log("zip files => ".print_r($files,true));
        $backup = $path."/backup";
        mkdir($backup, 0700); //backupフォルダーを作る
        Unzip($path,$zipfile);
    }
}

function Unzip($dir, $file, $destiny=""){
    $dir .= DIRECTORY_SEPARATOR;
    $path_file = $dir . $file;
    $zip = zip_open($path_file);
    $_tmp = array();
    $count=0;
    if ($zip){
        while ($zip_entry = zip_read($zip)) {
            $_tmp[$count]["filename"] = zip_entry_name($zip_entry);
            $_tmp[$count]["stored_filename"] = zip_entry_name($zip_entry);
            $_tmp[$count]["size"] = zip_entry_filesize($zip_entry);
            $_tmp[$count]["compressed_size"] = zip_entry_compressedsize($zip_entry);
            $_tmp[$count]["folder"] = dirname(zip_entry_name($zip_entry));
            $_tmp[$count]["index"] = $count;
            $_tmp[$count]["method"] = zip_entry_compressionmethod($zip_entry);

            if (zip_entry_open($zip, $zip_entry, "r")){
                $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                if($destiny){
                    $path_file = str_replace("/",DIRECTORY_SEPARATOR, $destiny . zip_entry_name($zip_entry));
                }
                else{
                    $path_file = str_replace("/",DIRECTORY_SEPARATOR, $dir . zip_entry_name($zip_entry));
                }
                $new_dir = dirname($path_file);
                
                // Create Recursive Directory (if not exist)  
                if (!file_exists($new_dir)) {
                  mkdir($new_dir, 0700);
                }

                zip_entry_close($zip_entry);
            }
           
            $count++;
        }

        
        $cpy_file = "";
        error_log("Temp Directory => ".print_r($_tmp,true));
        foreach($_tmp as $z_file) { 

            if(substr($z_file["filename"], -1) !== "/"){
                $unzip_name = explode('/', $z_file["filename"]);
                $remove = $unzip_name[0]."/";
                error_log("unzip_name  => ".$unzip_name[0]);
                $cpy_file = str_replace($remove, '', $z_file["filename"]);
                
                $base_bk = $dir."backup/";
                $create_fld = explode('/', $cpy_file);
                //error_log("create_fld  => ".print_r($create_fld,true));

                //バックアップ用のフォルダを作成する
                for ($i=0; $i < count($create_fld)-1 ; $i++) { 
                    $backup = $base_bk."/".$create_fld[$i];
                    $base_bk = $backup;
                    //error_log("backup folder create => ".$backup);
                    if (!file_exists($backup)) {
                      mkdir($backup, 0700); //backupの中にフォルダーを作る
                    }
                    
                }

                $real_path = "../../".$cpy_file; //php/common/inc/common.format.php => 今のファイル(real file)
                $backup_path = $dir."backup/".$cpy_file;//backup

                copy($real_path, $backup_path); //　バックアップのために コピーする copy 今のファイル(real file) => backup
                if (!copy($real_path, $backup_path)){

                    $rtn = 1;
                    break;
                }else{
                    
                    $unzip_path = $dir.$name."/".$cpy_file;
                    //error_log("unzip_path for zipuploadMSM => ".$unzip_path);
                    if (file_exists($unzip_path)) {
                      copy($unzip_path, $real_path);  // overwrite unzip file => 今のファイル(real file)
                    }
                }

                
            }
        }
        zip_close($zip);
    }
}

if ($rtn === 1) {
    db2_rollback($db2con);
    //e_log('data rollback');
    
} else {
    db2_commit($db2con);
}
cmDb2Close($db2con);
$rtnAry = array('RTN' => $rtn, 'MSG' => $msg, 'success' => $success, 'Bkfilename' => $Bkfilename_rtn,);
echo (json_encode($rtnAry));
//User****************************************************